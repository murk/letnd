<?
/**
 * NewsNewHome
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2010
 * @version $Id$
 * @access public
 */
class NewsNewHome extends Module{

	function __construct(){
		parent::__construct();
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function on_load(){

		// TODO - Per treure el _ de les URLS, mira la resta, nomes està fet aquí
		$category_id = R::id('category_id');
		if (!$category_id){
			$category =  R::id('category');
			if ($category) $_GET['category_id'] = $category;
		}
		
		$this->set_friendly_params(array(
			'category_id'=>R::id('category_id')			
			));
		parent::on_load();
	}
	function get_records()
	{	    	
		$arxiu = CLIENT_DIR.'/templates/news/new_home.tpl';
		$this->set_news_fields();
		$listing = new ListRecords($this);

		if (show_preview()){
			$listing->condition = "(status <> 'archived')";
		}
		elseif ($this->config['ignore_publish_date']){
			$listing->condition = "(status = 'public')
							AND (expired=0 OR expired > (curdate()))";
		}
		else{
			$listing->condition = "(status = 'public')
							AND (entered=0 OR entered <= now())
							AND (expired=0 OR expired > (curdate()))";
		}
		if ($this->config['home_list_all'] == 0){
			$listing->condition .= " AND (in_home ='1')";
		}
							
		$category_id = R::id('category_id');
		$exclude_category = '';

		// si està posada categoria per defecte, la utilitzo. Puc posar varies cats separades per comes
		if($this->config['default_category_id'] && !$category_id) {

			$category_id = $this->config['default_category_id'];

			if (strpos($category_id, ',') !== false) {
				$category_id_array = explode( ',', $category_id );
				$category_id = implode( "','", $category_id_array );
			}

		}

		$listing->set_var('listing_category_id',$category_id);

		if ($category_id && $category_id[0] == '!'){
			$exclude_category = 'NOT ';
			$category_id = substr($category_id, 1);
		}

		if ($category_id)
			$listing->condition .= " AND category_id $exclude_category IN ('$category_id')";

		$listing->call('records_walk','url1,url1_name,url2,url2_name,url3,url3_name,content,content_list');
		$listing->order_by = 'ordre ASC, entered DESC';
		if (file_exists($arxiu)) {
			$listing->template = 'news/new_home';
		}				
		return $listing->list_records();
	}
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{
	    $this->set_news_fields();
		$show = new ShowForm($this);			
		

		if (show_preview()){
			// Al formulari es veuen tots, arxivades també
		}
		elseif ($this->config['ignore_publish_date']){
			$show->condition = "(status = 'public')
							AND (expired=0 OR expired > (curdate()))";
		}
		else{
			$show->condition = "(status = 'public')
							AND (entered=0 OR entered <= now())
							AND (expired=0 OR expired > (curdate()))";
		}
		
		$show->call('records_walk','url1,url1_name,url2,url2_name');
		$show->get_values();
		if ($show->has_results)
		{
			// AQUEST NO, SINO ES REPETEIX 2 COPS A LA PàGINA ----> $GLOBALS['gl_page']->title = $show->rs['new'];
			$GLOBALS['gl_page']->page_title = $show->rs['new'];
			$GLOBALS['gl_page']->page_description = add_dots_meta_description($show->rs['content'],$this->config['add_dots_length']);
			return $show->show_form();
		}
		else
		{
			Main::redirect('/');
		}	
	}
	function records_walk($url1,$url1_name,$url2,$url2_name,$url3,$url3_name,$content=false,$content_list=false){		
		// Això ho poso aquí així no cal posar-ho al tpl, simplement posar: <a href="<=$url1>" target="_blank"><=$url1_name></a>
		$ret['url1_name'] = $url1_name?$url1_name:$url1;
		$ret['url2_name'] = $url2_name?$url2_name:$url2;
		$ret['url3_name'] = $url3_name?$url3_name:$url3;
		
		// si es llistat passa lavariable content
		if ($content!==false){
			$ret['content']=$content_list?$content_list:add_dots('content', $content);
		}
			
		//$ret['url1']=str_replace('http://','',$url1);	
		//$ret['url1']=str_replace('http://','',$url1);
		return $ret;
	}
	function set_news_fields(){	
		global $gl_config;
		if (!$gl_config['show_entered_time'])
		{
			$this->set_field('entered', 'type', 'date');
		}
	}
	function is_admin_logged(){
		return isset($_SESSION['user_id']) && isset($_SESSION['group_id']) && $_SESSION['user_id'] != '';
	}
}