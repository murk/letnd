<?

use PHPMailer\PHPMailer\Exception;

class PvCron extends Module {
	var $is_cli = false, $conta = 0;
	var $dbs = [
		'gpmconsultors'       => 'http://gestio.gpmconsultors.com:8080',
		'arumsa'              => 'http://gestio.arumsa.com:8080',
		'consultoriapvgestio' => 'http://gestio.consultoriapv.com:8080'
	];

	public function __construct() {
		parent::__construct();
	}

	public function start() {
		echo '<link href="/admin/themes/inmotools/styles/export.css" rel="stylesheet" type="text/css">';
		$this->p('Cron: ' . now(), 1);
		$this->send_seguiment_avis();
	}

	public function send_seguiment_avis() {

		foreach ( $this->dbs as $db => $url ) {

			{
				$q = new Query( "$db.pv__seguimentitem" );

				Db::execute( "USE `$db`" );
				$cf = get_config_array( 'public', "all" );

				$results = $q->get( "seguimentitem_id, seguiment_id, requisit_id, avis_texte, avis_data", "avis_data = CURDATE() AND avis_emailed = 0" );


				$mailer = get_mailer( $cf );
				$mailer->IsHTML( true );
				$mailer->clearAddresses();

				foreach ( $results as $rs ) {

					$seguimentitem_id = $rs['seguimentitem_id'];
					$seguiment_id     = $rs['seguiment_id'];
					$requisit_id      = $rs['requisit_id'];
					$avis_texte       = $rs['avis_texte'];
					$avis_data        = format_date_form( $rs['avis_data'] );

					$client_id   = Db::get_first( "SELECT client_id FROM $db.pv__seguiment WHERE seguiment_id = %s", $seguiment_id );
					$email_rs    = Db::get_row( "SELECT email, email2, email_admin, client FROM $db.pv__client WHERE client_id = %s", $client_id );
					$requisit_rs = Db::get_row( "SELECT norma_id, requisit FROM $db.pv__requisit WHERE requisit_id = %s", $requisit_id );

					$email  = $email_rs['email'];
					$email2 = $email_rs['email2'];
					$email_admin = $email_rs['email_admin'];
					$client = $email_rs['client'];

					$norma_id = $requisit_rs['norma_id'];
					$requisit = $requisit_rs['requisit'];

					$link = "$url/admin/?action=list_records&tool_section=seguimentitem&menu_id=%s&seguiment_id=$seguiment_id&is_actiu=1&norma_id=$norma_id";

					$subject = $this->caption['c_avis_subject'];


					$body_base = "
						<div>
							<strong>{$this->caption['c_avis']}:</strong><br>
							$avis_data <br>
							$avis_texte
						</div><br>	
										
						<div>
						<strong>
							{$this->caption['c_requisit']}:</strong><br>
							$requisit
						</div><br>
																	
						<div>
							<strong>{$this->caption['c_fitxa']}:</strong><br>
							$link
						</div>
					";
					$body = sprintf($body_base, '2311');
					$body_admin = sprintf($body_base, '2303');

					// $mailer->SMTPSecure = 'tls';
					// $mailer->Port       = 465;

					if ( $email ) $mailer->AddAddress( $email );
					if ( $email2 ) $mailer->AddAddress( $email2 );

					if ( $email || $email2 ) {
						$mailer->Body    = $body;
						$mailer->Subject = $subject;
						$sent = $this->send_mail( $mailer );
						if ( $sent ) $q->update( [ 'avis_emailed' => 1 ], $seguimentitem_id );
						$this->p("Email enviat $email $email2", 2);
					}
					else {
						$mailer->Body    = "
							<div>
							<strong>Client:</strong><br>
							$client
							</div><br>
							$body";
						$mailer->Subject = $this->caption['c_avis_not_sent'];
						$mailer->clearAddresses();
						$mailer->addAddress( $cf['default_mail'] );
						$sent = $this->send_mail( $mailer );
						$this->p("Email no enviat $email $email2", 2);
					}


					if ($email_admin) {
						$mailer->Body    = $body_admin;
						$mailer->Subject = "$subject $client";
						$mailer->clearAddresses();
						$mailer->AddAddress( $email_admin );
						$sent = $this->send_mail( $mailer );
						if ( $sent ) $q->update( [ 'avis_emailed' => 1 ], $seguimentitem_id );
						$this->p("Email enviat $email_admin", 2);
					}
					else {
						$mailer->Body    = "
							<div>
							<strong>Client:</strong><br>
							$client
							</div><br>
							$body";
						$mailer->Subject = $this->caption['c_avis_not_sent_admin'];
						$mailer->clearAddresses();
						$mailer->addAddress( $cf['default_mail'] );
						$sent = $this->send_mail( $mailer );
						$this->p("Email no enviat a l'administrador, no hi ha cap email configurat", 2);
					}


				}

				if (!$results) {
					$this->p("Cap avis per $db", 2);
				}

				unset( $mailer );

			}

		}
	}

	private function send_mail( $mail ) {
		try {
			$sent = $mail->Send();

			return $sent;

		} catch ( Exception $e ) {
			echo $e->errorMessage();

			return false;

		} catch ( \Exception $e ) {
			echo $e->getMessage();

			return false;
		}
	}


	public function p( $txt, $level ) {

		if ( $level == 2 ) {
			$this->conta ++;
			$txt = '<em>' . $this->conta . '</em> ' . $txt . ' - <i>(' . $execution_time = Debug::get_execution_time() . ' s.) </i>';
		}
		if ( $level == 'error' ) {
			$txt .= ' (error)';
		}
		echo '<p class="m' . $level . '">' . $txt . '</p>';
		//@ob_flush();
		if ( $this->conta % 1000 == 0 && $this->conta != 0 ) {
			flush();
		}
	}
}