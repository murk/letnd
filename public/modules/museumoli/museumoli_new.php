<?
/**
 * MuseumoliNew
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2012
 * @version $Id$
 * @access public
 */
class MuseumoliNew extends Module{

	function __construct(){
		parent::__construct();
	}
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{
		return $this->_execute_module(str_replace('show_','get_',$this->action));
	}
	function _execute_module($action=''){
		// $this->name = 'instalacio'; // si vull canviar el nom del parent
		$module = Module::load('news','new', $this);
		$action = $action?$action:$this->action;
		return $module->do_action($action);	
	}
}
?>