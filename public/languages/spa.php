<?
// blocks

define('INMO_PRICE_FIRST','Hasta %s&nbsp;'.CURRENCY_NAME);
define('INMO_PRICE_MIDDLE','De %s a %s&nbsp;'.CURRENCY_NAME);
define('INMO_PRICE_LAST','Más de %s&nbsp;'.CURRENCY_NAME);
define('INMO_FLOOR_SPACE_FIRST','Hasta %s m<sup>2</sup>');
define('INMO_FLOOR_SPACE_MIDDLE','De %s a %s m<sup>2</sup>');
define('INMO_FLOOR_SPACE_LAST','Más de %s m<sup>2</sup>');
define('INMO_PERSON_FIRST','Hasta %s');
define('INMO_PERSON_MIDDLE','De %s a %s');
define('INMO_PERSON_LAST','Más de %s');
define ('LETND','Letnd');

$gl_caption['c_offers'] = 'Ofertas';


// captions

$gl_caption['c_send_mail'] = 'enviar mail';
$gl_caption['c_more_info'] = 'Más info';
$gl_caption['c_read_more'] = 'Leer más';
$gl_caption['c_title_search'] = 'buscar';
$gl_caption['c_yes'] = 'Sí';
$gl_caption['c_no'] = 'No';
$gl_caption['c_send'] = 'Enviar';
$gl_caption['c_clear'] = 'Borrar';
$gl_caption['c_send_to_friend']='enviar a un amigo';
$gl_caption['c_print']='imprimir';
$gl_caption['c_zoom_link']='ampliar imágenes';
$gl_caption['c_contact_link']='contactar';

// bloc inmo_categories
$gl_caption['c_sell'] = 'Venta';
$gl_caption['c_rent'] = 'Alquiler';
$gl_caption['c_temp'] = 'Alquiler turístico';
$gl_caption['c_moblat'] = 'Alquiler con muebles';
$gl_caption['c_selloption'] = 'Alquiler opción compra';
// seo
$gl_caption['c_seo_verd_1']='<a href="http://www.letnd.com" target="_blank" title="Diseño páginas web en Girona">Diseño y programación web:<img src="/common/images/logoletnd.png" width="95" height="16" alt="Letnd: Diseño web, posicionamiento web, páginas web en Girona" style="vertical-align:text-bottom" /></a>';
$gl_caption['c_seo_verd_2']='<a href="http://www.letnd.com" target="_blank" title="Diseño páginas web en Girona">Programación web:<img src="/common/images/logoletnd.png" width="95" height="16" alt="Letnd: Diseño web, posicionamiento web, páginas web en Girona" style="vertical-align:text-bottom" /></a>';
$gl_caption['c_seo_verd_3']='<a href="http://www.letnd.com" target="_blank" title="Diseño páginas web en Girona">Creado por:<img src="/common/images/logoletnd.png" width="95" height="16" alt="Letnd: Diseño web, posicionamiento web, páginas web en Girona" style="vertical-align:text-bottom" /></a>';
$gl_caption['c_seo_verd_4']='<a href="http://www.letnd.com" target="_blank" title="Diseño páginas web en Girona">Diseño web:<img src="/common/images/logoletnd.png" width="95" height="16" alt="Letnd: Diseño web, posicionamiento web, páginas web en Girona" style="vertical-align:text-bottom" /></a>';
$gl_caption['c_seo_blanc_1']='<a href="http://www.letnd.com" target="_blank" title="Diseño páginas web en Girona">Diseño y programación web:<img src="/common/images/logoblanc.png" width="95" height="16" alt="Letnd: Diseño web, posicionamiento web, páginas web en Girona" style="vertical-align:text-bottom" /></a>';
$gl_caption['c_seo_blanc_2']='<a href="http://www.letnd.com" target="_blank" title="Diseño páginas web en Girona">Programación web:<img src="/common/images/logoblanc.png" width="95" height="16" alt="Letnd: Diseño web, posicionamiento web, páginas web en Girona" style="vertical-align:text-bottom" /></a>';
$gl_caption['c_seo_blanc_3']='<a href="http://www.letnd.com" target="_blank" title="Diseño páginas web en Girona">Creado por:<img src="/common/images/logoblanc.png" width="95" height="16" alt="Letnd: Diseño web, posicionamiento web, páginas web en Girona" style="vertical-align:text-bottom" /></a>';
$gl_caption['c_seo_blanc_4']='<a href="http://www.letnd.com" target="_blank" title="Diseño páginas web en Girona">Diseño web:<img src="/common/images/logoblanc.png" width="95" height="16" alt="Letnd: Diseño web, posicionamiento web, páginas web en Girona" style="vertical-align:text-bottom" /></a>';

$gl_caption['c_seo_custom_2']='<a href="http://www.letnd.com" target="_blank" title="Diseño páginas web en Girona">Programación web:<img src="/'.CLIENT_DIR.'/templates/images/letnd.png" alt="Letnd: Diseño web, posicionamiento web, páginas web en Girona" /></a>';
$gl_caption['c_seo_custom_4']='<a href="http://www.letnd.com" target="_blank" title="Diseño páginas web en Girona">Diseño web:<img src="/'.CLIENT_DIR.'/templates/images/letnd.png" alt="Letnd: Diseño web, posicionamiento web, páginas web en Girona" /></a>';

$gl_caption['c_tercersetena'] = 'Diseño <a class="tercersetena" target="_blank" href="http://www.tercersetena.com">Tercersetena.com</a>';


// NO TRADUIT
$gl_caption['c_tipus_temp'] = 'Alquiler de temporada';
$gl_caption['c_tipus_moblat'] = 'Alquiler con muebles';
// FI NO TRADUIT

// TAMBÉ A ADMIN
// Caption default
$gl_caption['c_form_title'] = 'Editar datos';
$gl_caption['c_config_title'] = 'Editar configuración';
$gl_caption['c_edit'] = 'Editar';


$gl_caption['c_edit_button'] = 'Editar datos';
$gl_caption['c_view_button'] = 'Ver datos';
$gl_caption['c_edit_images_button'] = 'Editar imágenes';
$gl_caption['c_add_images_button'] = 'Añadir imágenes';



$gl_caption['c_edit_data'] = 'Editar datos';
$gl_caption['c_edit_images'] = 'Editar imágenes';
$gl_caption['c_add_images'] = 'Añadir imágenes';
$gl_caption['c_search_form'] = 'Buscar';
$gl_caption['c_send_new'] = 'Guardar';
$gl_caption['c_send_edit'] = 'Guardar cambios';
$gl_caption['c_delete'] = 'Eliminar';
$gl_caption['c_action'] = 'Acción';
$gl_caption['c_delete_selected'] = 'Borrar seleccionados';
$gl_caption['c_restore_selected'] = 'Recuperar seleccionados';
$gl_caption['c_confirm_deleted'] = 'Los elementos seleccionados se eliminarán definitivamente de la base de datos \\n\\n¿Desea continuar?';
$gl_caption['c_confirm_images_deleted'] = 'Las imágenes seleccionadas se eliminarán definitivamente \\n\\n¿Desea continuar?';
$gl_caption['c_confirm_form_bin'] = 'El registro se enviará a la papelera de reciclaje \\n\\n¿Desea continuar?';
$gl_caption['c_confirm_form_deleted'] = 'El registro se eliminará definitivamente de la base de datos \\n\\n¿Desea continuar?';
$gl_caption['c_move_to'] = 'Mover a:';
$gl_caption['c_print_all'] = 'Imprimir todo';
$gl_caption['c_print'] = 'Imprimir';
$gl_caption['c_order_by'] = 'Ordenar por:';
$gl_caption['c_select'] = 'Seleccionar';
$gl_caption['c_all'] = 'todo';
$gl_caption['c_nothing'] = 'nada';
$gl_caption['c_preview'] = 'Previsualizar';
$gl_caption['c_yes'] = 'Sí';
$gl_caption['c_no'] = 'No';
$gl_caption['c_options_bar'] = 'Opciones';
$gl_caption['c_file_maximum'] = 'máximo %s archivos a la vez';
$gl_caption['c_file_maximum_1'] = 'máximo 1 archivo a la vez';


$gl_caption['c_form_sent'] = 'Gracias por contactar con nosotros, responderemos a su mensaje lo antes posible';
$gl_caption['c_form_not_sent'] = 'El formulario no se ha podido enviar, compruebe que la dirección de email que ha escrito sea correcta';

$gl_caption['c_cookies_message'] = 'Esta web utiliza cookies propias y de terceros para mejorar la experiencia de navegación así como para tareas de análisis. Al continuar navegando, entendemos que acepta el';
$gl_caption['c_cookies_message_more'] = 'uso de cookies';
$gl_caption['c_cookies_message_accept'] = 'Acepto';

$gl_caption['c_cookies_text'] = '

<p class="accept-cookies-title">¿Qué son las cookies?</p>
<p>Una cookie es un pequeño archivo que se almacena en el ordenador del usuario y nos permite reconocerlo. El conjunto de cookies nos ayuda a mejorar la calidad de nuestra web, permitiéndonos controlar qué páginas encuentran nuestros usuarios útiles y cuáles no.</p>
<p>Las cookies son esenciales para el funcionamiento de Internet, aportando innumerables ventajas en la prestación de servicios interactivos, facilitándonos la navegación y usabilidad de nuestra web. Tenga en cuenta que las cookies no pueden dañar su equipo y que, a cambio, si son activadas nos ayudan a identificar y resolver los errores.</p>
<p class="accept-cookies-title">¿Que tipo de cookies utiliza esta página web?</p>
<p>Cookies propias: son aquellas que se envían al equipo del usuario desde un equipo o dominio gestionado por el editor y desde el que se presta el servicio solicitado por el usuario.</p>
<p>Cookies de terceros: son aquellas que se envían al equipo del usuario desde un equipo o dominio no gestionado por el editor sino por otra entidad que analiza los datos obtenidos.</p>
<p>Cookies persistentes: son un tipo de cookies en el que los datos siguen almacenados en el terminal y pueden ser accedidas y tratadas por el responsable de la cookie.</p>
<p>Cookies de analítica web: a través de la analítica web se obtiene información relativa al número de usuarios que acceden a la web, el número de páginas vistas, la frecuencia y repetición de las visitas, su duración, el navegador utilizado, el operador que presta el servicio, el idioma, el terminal que utiliza, o la ciudad en la que está asignada la dirección IP. Información que posibilita un mejor y más apropiado servicio por parte de esta web.</p>
<p class="accept-cookies-title">Aceptación del uso de cookies.</p>
<p>Asumimos que usted acepta el uso de cookies. Sin embargo, usted puede restringir, bloquear o borrar las cookies de esta web o cualquier otra página a partir de la configuración de su navegador.</p>
<p>A continuación le mostramos las páginas de ayuda de los navegadores principales:</p>
<ul>
<li>Internet Explorer: <a href="http://windows.microsoft.com/es-xl/internet-explorer/delete-manage-cookies#ie="ie-10"" target="_blank">windows.microsoft.com/es-xl/internet-explorer/delete-manage-cookies#ie="ie-10"</a></li>
<li>FireFox: <a href="http://support.mozilla.org/es/kb/Borrar%20cookies" target="_blank">support.mozilla.org/es/kb/Borrar%20cookies</a></li>
<li>Chrome: <a href="http://support.google.com/chrome/answer/95647?hl="es"" target="_blank">support.google.com/chrome/answer/95647?hl="es"</a></li>
<li>Safari: <a href="http://www.apple.com/es/privacy/use-of-cookies/" target="_blank">www.apple.com/es/privacy/use-of-cookies/</a></li>
</ul>
';

// contrasenyes
$gl_caption['c_password_forgotten'] = '¿Ha olvidado su contraseña?';
$gl_caption['c_password_insert_mail'] = 'Por favor, introduzca su dirección de email y le enviaremos una contraseña nueva a su cuenta de correo.';
$gl_caption['c_password_insert_mail_exists'] = 'Si ha olvidado su contraseña, por favor, clique aceptar y le enviaremos una contraseña nueva a su cuenta de correo.';
$gl_caption['c_password_mail_error'] = 'La dirección de email proporcionada no existe en nuestra base de datos, por favor, escriba la dirección correctamente';
$gl_caption['c_password_sent'] = 'Se ha enviado una nueva contraseña a su dirección de email';
$gl_caption['c_password_mail_subject'] = 'Su nueva contraseña';
$gl_caption['c_password_mail_text_1'] = 'Su nueva contraseña es';
$gl_caption['c_password_mail_text_2'] = 'Puede cambiar la contraseña en nuestra página web';
$gl_caption['c_welcome'] = 'Bienvenido';

$gl_caption['c_new_custumer_exists'] = 'Ya ha entrado los datos de una persona con este nombre y apellidos o fecha de nacimiento, puede:';
$gl_caption['c_new_custumer_exists_change_name'] = 'Corregir nombre o fecha de nacimiento';
$gl_caption['c_new_custumer_exists_return'] = 'Volver a la reserva y seleccionar persona de la lista';

// captcha
$gl_caption['c_recaptcha_error'] = 'Por favor valide el "CAPTCHA" correctamente';
?>