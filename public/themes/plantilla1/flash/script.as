﻿//fscommand("allowscale", false);
if (!images_dir) images_dir = 'D:/webs/intranets/web/clients/grupcorvi/works/works/images/';
if (!images_thumbs) images_thumbs =  '1447_thumb.JPG-1444_thumb.JPG-1445_thumb.JPG-1448_thumb.JPG-1446_thumb.JPG-1449_thumb.JPG-1450_thumb.JPG--';
if (!images_bigs) images_bigs =  '1447_medium.JPG-1444_medium.JPG-1445_medium.JPG-1448_medium.JPG-1446_medium.JPG-1449_medium.JPG-1450_medium.JPG--';
if (!imatge) imatge = '1274_medium.jpg';

_root.output.text = '' + images_dir;


ample_flash = 610;
ample_imatge = 600;
alt_imatge = 400;

fast_speed = 20;
slow_speed = 160;
not_speed = 20;
wait = false;
movies_arr = new Array();

images_bigs = images_bigs.split('-');
images_thumbs = images_thumbs.split('-');
num = images_bigs.length;
num_temp = num;
apartat = 1; // valor per defecte
for (x=0; x<num_temp; x++) {	
	if (images_bigs[x]==imatge) apartat = x+1;	
	if (images_bigs[x]=='')	{num--} // l'ultima coma fa que lultim item de l'array sigui vuit;
}



movies_arr["imatge"] = num;		   
nivell = 1;
			   
current_image = apartat + "_" + 100;		   
current_image_num = 100;
borderThickness=5;
cornerDiameter=10;
margeTiraTumbs=40;

var image_mcl:MovieClipLoader = new MovieClipLoader();

var mclListener:Object = new Object();

// quan hem carregat un thumb carreguem el següent, es l'unica manera de saber l'amplada de la imatge
mclListener.onLoadInit = function(target_mc:MovieClip) {
	
		target_mc = _root.tira_thumbs[nom_clip_thumb];
		newScale = 100*54/target_mc._height
		target_mc._xscale = newScale;
		target_mc._yscale = newScale;
		target_mc._x = thumb_x;
		target_mc.apartat = thumb_num+1;
		target_mc.cmd = "out";		
		
		with(target_mc.imatge_contenidor){
			w = _width; 
			h = _height;
			beginFill(0xFFFFFF, 100);
			moveTo(-borderThickness, -borderThickness);
			lineTo(w + (borderThickness), - borderThickness);
			lineTo(w + (borderThickness), h + (borderThickness));
			lineTo(-borderThickness, h + (borderThickness));
			lineTo(-borderThickness,-borderThickness);
			endFill();
		}
		
		
		thumb_x = thumb_x + target_mc._width + 8;	
		target_mc._visible=true;
		
		
		target_mc.onRollOver = function ()
		{
			this.cmd = "over";
			this.play();
		};
		target_mc.onRollOut = navItem.onReleaseOutside = function ()
		{
			this.cmd = "out";
			this.play();
		};
		target_mc.onRelease = function ()
		{
			show_image(target_mc.apartat);
		};
			
								   
		thumb_num++;
		carrega_thumbs();
};
image_mcl.addListener(mclListener);
					   
thumb_num = 0;
thumb_x = 0;
nom_clip_thumb = '';
if (num!=1) 
{carrega_thumbs();
}
// si nomès hi ha una imatge no carreguem cap thumb i carreguem diractament les imatges
else
{ 
carrega_imatges_apartat(num);
show_image(apartat);
}

// carreguem thumbs d'un en un
function carrega_thumbs()
{
	// carreguem següent thumb
	if (thumb_num<num) {	
		nom_clip_thumb = "thumb_" + (thumb_num+1);		
		ruta_thumb = images_dir + images_thumbs[thumb_num];			
		
		var mc = _root.tira_thumbs.attachMovie('thumb_efecte', nom_clip_thumb, nivell);
		mc.stop();
		mc._y = 7;
		image_mcl.loadClip(ruta_thumb, mc.imatge_contenidor);	
		
		nivell++;	
	}
	// ja s'han carregat tots els thumbs, ara carreguem les imatges i centrem la tira dels thumbs
	else
	{ 	
	 	ample_total = thumb_x - 8;
		
		if (ample_total>ample_flash){
			_root.tira_thumbs.onEnterFrame = function () {
				ypos = _root._ymouse;
				if (ypos>400){
				diferencia = ample_total-ample_flash+margeTiraTumbs;
				factor = diferencia/ample_flash;
				xpos = _root._xmouse;
				this._x = -xpos*factor+(margeTiraTumbs/2);
				}
			}
		}
		else
		{
			// centrar thumbs
			x_new = (ample_flash-ample_total)/2;
			_root.tira_thumbs._x = x_new;
		}			
		carrega_imatges_apartat(num);
		show_image(apartat);
	}
}

// carreguem totes les imatges de l'apartat actual
function carrega_imatges_apartat(num)
{
	thumb_x = 0;
	// primer carreguem l'imatge del apartat (imatge que l'usuari a clicat en l'html)
		nom_clip = "image_" + (apartat);		
		ruta = images_dir + images_bigs[apartat-1];
		// dupliquem imatges efectes per cada imatge
		_root["imatge_efecte"].duplicateMovieClip(nom_clip, nivell);
		_root[nom_clip]["imatge_gran"].loadMovie(ruta);
		_root[nom_clip].gotoAndStop(1);
		_root[nom_clip]._visible=false;			
		nivell++;
	
	
	for (x=0; x<num; x++) {
		if (x+1!=apartat){	
			nom_clip = "image_" + (x+1);
			
			ruta = images_dir + images_bigs[x];
			// dupliquem imatges efectes per cada imatge
			_root["imatge_efecte"].duplicateMovieClip(nom_clip, nivell);
			_root[nom_clip]["imatge_gran"].loadMovie(ruta);
			_root[nom_clip].gotoAndStop(1);
			_root[nom_clip]._visible=false;		
			//_root[nom_clip]._x=x*200;			
			//_root[nom_clip]._y=200;			
			nivell++;
		}
		_root.prevNext.swapDepths(1000);
		
	}
}


function show_image(n){
	
	apartat = n;
	imatge_repetida=false;	
	n_num = n;
	n = "image_" + (n);
	// si estem en espera parar carregador
	if (_root.wait)
	{
		// si estem en espera i tornem aclicar
		//sobre la mateixa imatge no fem res
		if (_root.current_image==n){
			return;
		}
		else
		{
			_root.wait = false;
			_root.cargador.gotoAndStop(1);		
		}
	}
	
	// parem la imatge actual	
	if (_root.current_image!=n)
	{	
		_root[current_image].gotoAndPlay("surt");
	}
	
	// comprobo si es la mateixa imatge per no mostrar un altre cop
	if (_root.current_image == n){
	imatge_repetida=true;}
	
	// canviem el valor de la actual així el carregador sab quina es
	
	_root.current_image_num = n_num;
	_root.current_image = n;	
	trace (_root[n]["imatge_gran"].getBytesLoaded())
	// si esta carregada la mostrem
	if ( (_root[n]["imatge_gran"].getBytesLoaded()>=_root[n]["imatge_gran"].getBytesTotal())
		  && (_root[n]["imatge_gran"].getBytesTotal()!=-1) ){				
		

		
		if (!imatge_repetida){
		_root[n]._visible=true;
		_root[n].gotoAndPlay(2);
		}
		
		// falla la primera imatge, el _root[n]["imatge_gran"]._width dona 0 a la primera imatge
		// centrar imatges
		if (_root[n]["imatge_gran"]._width){
			x_new = (ample_imatge-_root[n]["imatge_gran"]._width)/2;
			
			_root[n]._x = x_new+5;
			// centrar imatges
			y_new = (alt_imatge-_root[n]["imatge_gran"]._height)/2;
			_root[n]._y = y_new;
		}

		
		
	} else
	{// iniciem carregador i posem wait a true		
		_root.wait = true;
		_root.cargador.gotoAndPlay(2);
	}	
}

function next_image(step)
{
	next_num = apartat+step;
	if (next_num>0 && next_num<=num)
	{
		show_image(next_num);
	}
}

// carreguem totes les imatges de l'apartat actual
function amaga_imatges()
{
	for (x=0; x<_root.num; x++) {	
		nom_clip = "image_" + (x+1);
		if (nom_clip!=_root.current_image){
		_root[nom_clip]._visible=false;
		}
	}
}


