<?php
global $gl_tool;
//echo "el tool és:".$gl_tool;

$query = "SELECT all__page.page_id as page_id, link, page
				FROM all__page_language, all__page
				WHERE all__page_language.page_id = all__page.page_id
				AND status = 'public'
				AND bin <> 1
				AND menu = 1
				AND language = '" . LANGUAGE . "'
				ORDER BY ordre";

$results = Db::get_rows($query);
Debug::add('Consulta pages menu', $query);

if ($results) {
    foreach($results as $rs) {
        ((isset($_GET["page_id"]) && ($_GET["page_id"] == $rs["page_id"]))
		|| ($rs['link']=='/' && $gl_is_home))?$selected = 1:$selected = 0;
        if (strpos($rs['link'],'javascript:')===0) {
			$l['link'] = $rs['link'];
        }
        elseif ($rs['link']) {
            $sep = strstr($rs['link'], '?')?'&':'?';
            $l['link'] = $rs['link'] . $sep . 'language=' . LANGUAGE;
            if ($rs['link'] != '/') { // la home page no porta el page_id
                    $l['link'] .= '&page_id=' . $rs['page_id'];
            }
        } else {
            $l['link'] = '/?page_id=' . $rs['page_id'] . '&language=' . LANGUAGE;
        }
        //per llistar les subfamilies
		if ( //sI ES LA PRIMERA VEGADA QUE ENTRO o ja estic a dintre
		$_GET['page_id'] == 6 AND $rs['page_id'] == 6
		)
		{
            $l['products'] = get_products_block();
        }
		else if (
		//$_GET['family_id'] AND $rs['page_id'] == 6
		$gl_tool == "product" AND $rs['page_id'] == 6
		) { //sI JA HI HERA I CANVIO DE FAMIÍLIA
		  $l['products'] = get_products_block();
		     }
		else {
            $l['products'] = array();
        }
        $l['selected'] = $selected;
        $l['item'] = $rs['page'];
        $loop2[] = $l;
    }

    $loop[] = array ('loop' => $loop2,
        'item' => '');
}

/**
 *
 * @access public
 * @return void
 */
function get_products_block()
{
    $query = "SELECT Distinct family, product__family_language.family_id
				FROM product__family_language, product__family
				WHERE product__family_language.family_id = product__family.family_id
				AND language = '" . LANGUAGE . "'
				AND product__family.parent_id= 0
				AND bin = 0
				ORDER BY ordre ASC";

    $results = Db::get_rows($query);
    Debug::add('Provant families', $query);
    // poso els resultats a un array per fer el loop
    foreach($results as $rs) {
        (isset($_GET["family_id"]) && ($_GET["family_id"] == $rs["family_id"]))?$selected = 1:$selected = 0;
        $l['p_link'] = '/?tool=product&tool_section=product&family_id=' . $rs['family_id'] . '&language=' . LANGUAGE . '"';
        // $l['link'] = $rs['family_id'];
        $l['p_item'] = $rs['family'];
        $l['p_selected'] = $selected;
        $loop[] = $l;
    }
    return $loop;
}

?>