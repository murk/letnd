<?php

/* 23-1-17

	Canvis objecte arxius

	Afegir nou camp per poder canviar el nom de l'arxiu que surt a la web

	Afegir categories igual que a imatges:

	Exemple de taula per categories
	CREATE TABLE `news__new_filecat` (
		`filecat_id` INT(11) NOT NULL AUTO_INCREMENT,
		`language` CHAR(3) NOT NULL DEFAULT '' COLLATE 'utf8_spanish2_ci',
		`filecat` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_spanish2_ci',
		`ordre` INT(11) NULL DEFAULT NULL,
		PRIMARY KEY (`filecat_id`)
	)
	COLLATE='utf8_spanish2_ci'
	ENGINE=MyISAM;

	UPDATE news__new_file SET filecat_id = 1;

*/

$tables = Db::get_rows_array( 'SHOW TABLES' );

foreach ( $tables as $rs ) {
	$table = $rs[0];
	if ( substr( $table, - 5 ) === '_file' ) {

		$update_sql[] = "ALTER TABLE $table
							ADD COLUMN `filecat_id` INT(11) NOT NULL DEFAULT '1';";
		$update_sql[] = "ALTER TABLE $table
							ADD COLUMN `title` VARCHAR(255) NOT NULL AFTER `name`;";
		$update_sql[] = "ALTER TABLE $table
							ADD COLUMN `public` TINYINT(1) NOT NULL DEFAULT '1';";
		$update_sql[] = "UPDATE $table
							set `title` = `name`;";

	}
}

// Banner per defecte no es posa públic
if(is_table('web__banner_file')){
	$update_sql[]="ALTER TABLE `web__banner_file`
	CHANGE COLUMN `public` `public` TINYINT(1) NOT NULL DEFAULT '0' AFTER `filecat_id`;";
}

// Custumer de inmo no te public, els arxius nomes els veu el propi custumer
if(is_table('custumer__custumer_file')){
	$update_sql[]="ALTER TABLE `custumer__custumer_file`
	DROP COLUMN `public`;";
}

// Newsletter no te public, quan ja envies el missatge ja deus tindre clar que van tots els adjunts
if(is_table('newsletter__message_file')){
	$update_sql[]="ALTER TABLE `newsletter__message_file`
	DROP COLUMN `public`;";
}

?>