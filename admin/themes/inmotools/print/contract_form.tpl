<script language="JavaScript" src="/admin/jscripts/validacio.js?v=<?=$version?>"></script>
<script language="JavaScript" src="/admin/modules/print/jscripts/functions.js?v=<?=$version?>"></script>
<script type="text/javascript">
$(function() {
	set_focus(document.theForm);
});
</script>
<form name="form_delete" method="post" action="<?=$link_action_delete?>">
<input name="<?=$id_field?>" type="hidden" value="<?=$id?>">
</form><form name="theForm" method="post" action="<?=$link_action?>" onsubmit="<?=$js_string?>" encType="multipart/form-data" >
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="form"> 
  <tr>
    <td> 
        <?if ($form_new):?> 
        <div id="form_buttons_holder"><table id ="form_buttons">
          <tr>
            <td class="buttons_top"><div class="right"><input type="submit" name="Submit" value="<?=$c_send_new?>" class="btn_form1"></div></td>
          </tr>
        </table></div>
        <?endif //form_new?>
		<?if ($form_edit):?>
            <div id="form_buttons_holder"><table id ="form_buttons">
              <tr>
                <td valign="bottom"><div class="menu"><div><?=$c_edit_data?></div></td>
                <td class="buttons_top"><div class="right" style="margin-bottom:3px;"><input type="submit" name="Submit" value="<?=$c_send_edit?>" class="btn_form1" /><div class="btn_sep"></div><a class="btn_form1" href="javascript:open_print_contract('<?=$contract_id?>')"><?=$c_print_contract_button?></a><a class="btn_form_delete" href="javascript:submit_form('<?=$c_confirm_form_bin?>', true);"><?=$c_delete?></a></div></td>
              </tr>
          </table></div>
          <?endif //form_edit?> 
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr> 
            <td><table width="100%" cellpadding="4" cellspacing="0">
              <tr>
                <td height="1" valign="top" class="formCaptionTitle"><strong>
                  <?=$c_contract_title?><?if ($contract_id):?>
                    <?=$c_contract_id?> <?=$contract_id?><?endif //contract_id?>
</strong></td>
              </tr>
            </table>
              <table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                  <td><table width="100%" cellpadding="4" cellspacing="0" class="formstbl">
                    <tr>
                      <td width="150" class="formsCaption"><strong>
                        <?=$c_signed?>:</strong></td>
                      <td class="forms"><input name="contract_id[<?=$contract_id?>]" type="hidden" value="<?=$contract_id?>" />
                          <input name="property_id[<?=$contract_id?>]" type="hidden" value="<?=$property_id?>" />
                        <input name="old_property_id[<?=$contract_id?>]" type="hidden" value="<?=$property_id?>" />
                          <input name="contract_id_javascript" type="hidden" value="<?=$contract_id?>" />
                          <?=$signed?></td>
                    </tr>
                    <tr>
                      <td valign="top" class="formsCaption"><strong>
                        <?=$c_kind_id?>: </strong></td>
                      <td class="forms"><table border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td valign="bottom"><?=$kind_id?>&nbsp;&nbsp;</td>
							<td class="cregistered" style="display:<?=$registered_visible?>"><strong>
                              <?=$c_registered?>:
                              &nbsp;&nbsp; </strong><?=$registered?></td>                            
                          </tr>
                        </table></td>
                    </tr>
                    <tr>
                      <td valign="top" class="formsCaption"><strong>
                        <?=$c_duration?>: </strong></td>
                      <td class="forms"><?=$duration?> <?=$c_months?></td>
                    </tr>
					<tr>
						<td valign="top" class="formsCaption"><strong><?= $c_diposit ?></strong></td>
                      <td class="forms"><?=$diposit?> €</td>
                    </tr>
					<tr>
                      <td valign="top" class="formsCaption"><strong>
                        <?=$c_place?>: </strong></td>
                      <td class="forms"><?=$place?></td>
                    </tr>
                    <tr>
                      <td valign="top" class="formsCaption"><strong>
                        <?=$c_language_id?>: </strong></td>
                      <td class="forms"><?=$language_id?></td>
                    </tr>
                    <tr>
                      <td valign="top" class="formsCaption"><strong>
                        <?=$c_state?>: </strong></td>
                      <td class="forms"><table border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td><?=$state?></td>
                            <td class="crescinded" style="display:<?=$rescinded_visible?>"><strong> &nbsp;&nbsp;
                                <?=$c_rescinded?>:&nbsp;&nbsp; </strong></td>
                            <td class="crescinded" style="display:<?=$rescinded_visible?>"><?=$rescinded?></td>
                          </tr>
                        </table></td>
                    </tr>
                    <tr>
                      <td valign="top" class="formsCaption"><strong>
                        <?=$c_expires?>: </strong></td>
                      <td class="forms"><?if ($expires):?><strong>
                        <?=$expires?>
</strong>&nbsp;
<?=$c_expires2?>
<?endif //expires?></td>
                    </tr>
                    <tr style="display:<?=$ipc_visible?>" class="cipc">
                      <td valign="top" class="formsCaption"><strong>
                        <?=$c_ipc?>: </strong></td>
                      <td class="forms"><?if ($ipc):?><table border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td valign="top">
<strong>
<?=$ipc?></strong>&nbsp;<?=$c_ipc2?></td>
  </tr>
  <tr>
    <td height="25" valign="bottom"><strong>
      <?=$c_ipc3?>: </strong></td>
  </tr>
  <tr>
    <td valign="top"><?foreach($ipcs as $l): extract ($l)?>
                            <?=$rise?><input type="checkbox" name="ipcs[<?=$rise?>]" value="1" id="checkbox" <?=$rised?>/><br />
                        <?endforeach //$loop?></td>
  </tr>
</table><?endif //ipc?></td>
                    </tr>
                    <tr>
                      <td valign="top" class="formsCaption"><strong>
                        <?=$c_contract_observations?>: </strong></td>
                      <td class="forms"><?=$contract_observations?></td>
                    </tr>
                  </table><div class="sep_dark"></div>
                    <table width="100%" cellpadding="4" cellspacing="0">
                      <tr>
                        <td class="formCaptionTitle"><strong>
                          <?=$c_property_id?>: </strong><a href="javascript:select_properties()">
                            <?=$c_select_property_link?>
                            </a>
                          <input name="selected_property_ids" type="hidden" value="<?=$selected_property_ids?>" />
                          <input name="selected_property_names" type="hidden" value="<?=$selected_property_names?>" />
                          - <a href="javascript:show_form_property('', true);">
                          <?=$c_add_property_link?>
                          </a></td>
                      </tr>
                    </table>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td align="center"><br /></td>
                      </tr>
                      <tr>
                        <td align="center" id="property" class="contract-property"><?=$property?></td>
                      </tr>
                      <tr>
                        <td align="center" id="owners"><?=$owners?></td>
                      </tr>
                    </table><div class="sep_dark"></div>
                    <table width="100%" cellpadding="4" cellspacing="0">
                      <tr>
                        <td class="formCaptionTitle"><strong>
                          <?=$c_buyers?>:</strong>
                          <input name="selected_buyer_ids" type="hidden" value="<?=$selected_buyer_ids?>" />
                          <input name="selected_buyer_names" type="hidden" value="<?=$selected_buyer_names?>" />
                          <a href="javascript:select_buyers()">
                          <?=$c_select_buyers_link?>
                          </a> - <a href="javascript:show_form_new_custumer(<?=$contract_id?>,'contract_new_buyer')">
                          <?=$c_add_buyers_link?>
                          </a></td>
                      </tr>
                    </table>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td align="center"><br /></td>
                      </tr>
                      <tr>
                        <td align="center" id="buyers"><?=$buyers?></td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
              <?if ($form_new):?>
              <table width="100%" cellpadding="4" cellspacing="2" class="formsButtonstbl">
                <tr> 
                  <td align="center" class="formsButtons"> <input type="submit" name="Submit" value="<?=$c_send_new?>" class="btn_form1"> 
                  </td>
                </tr>
              </table>
              <?endif //form_new?><?if ($form_edit):?>
              <table width="100%" cellpadding="4" cellspacing="2" class="formsButtonstbl">
                <tr> 
                  <td align="center" class="formsButtons"><div class="centerFloat1">
                        <div class="centerFloat2"><input type="submit" name="Submit" value="<?=$c_send_edit?>" class="btn_form1" /><div class="btn_sep"></div><a class="btn_form1" href="javascript:open_print_contract('<?=$contract_id?>')"><?=$c_print_contract_button?></a><a class="btn_form_delete" href="javascript:submit_form('<?=$c_confirm_form_bin?>', true);"><?=$c_delete?></a></div></div></td>
                </tr>
              </table>
            <?endif //form_edit?></td>
          </tr>
        </table></td>
  </tr>
</table></form>