<?php
/* 2-5-18
Afegir mètode pagament per deposit

*/

if ( is_table( 'product__order' ) && ! is_field( 'product__order', 'deposit_payment_method' ) ) {

	$update_sql[] = "ALTER TABLE `product__order`
	ADD COLUMN `deposit_payment_method` ENUM('none','paypal','account','ondelivery','directdebit','4b','lacaixa','cash','physicaltpv') NOT NULL DEFAULT 'none' AFTER `payment_method`;";

}
else {
	if ( is_table( 'product__order' ) ) {
		trigger_error( 'Ja existeixen el camp deposit_payment_method' );
	}
}