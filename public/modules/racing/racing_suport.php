<?
/**
 * RacingSuport
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2017
 * @version $Id$
 * @access public
 */
class RacingSuport extends Module{
	var $category_id;
	function __construct(){
		parent::__construct();
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function on_load(){

		// TODO - Per treure el _ de les URLS, mira la resta, nomes està fet aquí
		$this->category_id = R::id('category_id');
		if ($this->category_id ){

			$this->set_friendly_params(array(
				'category_id'=>$this->category_id
				));
		}
		else {
			$this->category_id  =  R::id('category');

			if ($this->category_id) {
				$this->set_friendly_params( array(
					'category' => $this->category_id
				) );
			}
		}

		parent::on_load();
	}
	function get_records()
	{
		$listing = new ListRecords($this);

		$listing->condition = "(status = 'public')";
		
		
		$category_id = $this->category_id;

		// si està posada categoria per defecte, la utilitzo
		if($this->config['default_category_id'] && !$category_id)
			$category_id = $this->config['default_category_id'];
		Debug::p( $category_id );

		$listing->set_var('listing_category_id',$category_id);	
			
		if ($category_id) $category = Db::get_first("
				SELECT category 
				FROM racing__category_language
				WHERE language = '". LANGUAGE ."' 
				AND category_id = '" . $category_id ."'");
				
		else  $category = '';
		
		$listing->set_var('listing_category',$category);	
		
		if ($category_id) {
			$listing->condition .= " AND category_id = '" . $category_id . "'";

		}
		
		$listing->call('records_walk','url1,url1_name,url2,url2_name,url3,url3_name');
		$listing->order_by = 'ordre ASC, suport, suport_id';

		if ($this->parent == 'sitemap') {//Sitemap
			$listing->paginate = false;
			$listing->set_field('modified', 'type', 'none');
			$listing->set_records();
			return $listing->loop;
		}
		
		return $listing->list_records();
	}
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{
		$show = new ShowForm($this);


		// si no hi ha id, agafo per defecte la primera noticia de la categoria, i si no hi ha categoria, de la primera categoria
		if (!$show->id){
			// si no hi ha categoria agafo la primera ordenat per nom i id
			
			$category_id = $this->category_id;
			if (!$category_id )
			{
				$query = "SELECT racing__category.category_id as category_id
					FROM racing__category_language, racing__category
					WHERE racing__category_language.category_id = racing__category.category_id
					AND language = '" . LANGUAGE . "'
					ORDER BY category, category_id
					LIMIT 0,1";
				$category_id = Db::get_first($query);
			}
			$query = "SELECT racing__suport.suport_id as suport_id
				FROM racing__suport_language, racing__suport
				WHERE racing__suport_language.suport_id = racing__suport.suport_id
				AND status = 'public'
				AND bin <> 1
				AND category_id = '" . $category_id . "'
				AND language = '" . LANGUAGE . "'
				ORDER BY ordre ASC, suport, suport_id
				LIMIT 0,1";
			$show->id = Db::get_first($query);
		}	

		$show->condition = "(status = 'public')";

		
		$show->call('records_walk','url1,url1_name,url2,url2_name,url3,url3_name');
		$show->get_values();		
		if ($show->has_results)
		{
			if ($this->config['override_gl_page_title'] == '1') {
				Page::set_title($show->rs['suport']);
				Page::set_body_subtitle(''); // si faig override del tiol, no te sentit deixar el subtitol general de la pagina
			}
			elseif ($this->config['override_gl_page_title'] == '2') {
				Page::set_body_subtitle($show->rs['suport']);
			}
			elseif ($this->config['override_gl_page_title'] == '3') {
				Page::set_title($show->rs['suport']);
				Page::set_body_subtitle($show->rs['tema']);
			}


			Page::set_page_title($show->rs['page_title'],$show->rs['suport']);
			Page::set_page_description($show->rs['page_description'],$show->rs['content']);
			Page::set_page_keywords($show->rs['page_keywords']);
			
			$show->set_var('show_category_id',$show->rs['category_id']);	
				
			Page::add_breadcrumb($show->rs['suport']);

			$content =  $show->show_form();

			Page::set_page_og($show->rs);
			return $content;
		}
		else
		{
			Main::error_404();
		}
	}
	function records_walk($url1,$url1_name,$url2,$url2_name,$url3,$url3_name){
		// Això ho poso aquí així no cal posar-ho al tpl, simplement posar: <a href="<=$url1>" target="_blank"><=$url1_name></a>
		$ret['url1_name'] = $url1_name?$url1_name:$url1;
		$ret['url2_name'] = $url2_name?$url2_name:$url2;
		$ret['url3_name'] = $url3_name?$url3_name:$url3;
			
		//$ret['url1']=str_replace('http://','',$url1);	
		//$ret['url1']=str_replace('http://','',$url1);
		return $ret;
	}
}
?>