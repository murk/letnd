<?php
/* 7-5-18
Inmo, configuració dels index de la BBDD

*/

if ( is_table( 'booking__book' ) ) {

	$update_sql .= "
ALTER TABLE `booking__book`
	ADD INDEX `date_in` (`date_in`),
	ADD INDEX `date_out` (`date_out`),
	ADD INDEX `is_owner` (`is_owner`),
	ADD INDEX `synced` (`synced`);
ALTER TABLE `booking__custumer_to_promcode`
	ADD INDEX `custumer_id` (`custumer_id`),
	ADD INDEX `promcode_id` (`promcode_id`);
ALTER TABLE `booking__extra`
	ADD INDEX `bin` (`bin`);
ALTER TABLE `booking__extra_to_book`
	ADD INDEX `book_id` (`book_id`),
	ADD INDEX `extra_id` (`extra_id`);
ALTER TABLE `booking__promcode`
	ADD INDEX `bin` (`bin`);
ALTER TABLE `booking__promcode_language`
	ADD INDEX `promcode_id` (`promcode_id`),
	ADD INDEX `language` (`language`);
ALTER TABLE `booking__propertyrange`
	ADD INDEX `date_start` (`date_start`),
	ADD INDEX `date_end` (`date_end`);
ALTER TABLE `booking__season`
	ADD INDEX `color_id` (`color_id`);
	";
;
}
if ( is_table( 'booking__book' ) ) {

	$update_sql .= "
ALTER TABLE `custumer__custumer`
	CHANGE COLUMN `bin` `bin` TINYINT(1) NOT NULL DEFAULT '0';
ALTER TABLE `custumer__custumer`
	ADD INDEX `removed` (`removed`),
	ADD INDEX `bin` (`bin`),
	ADD INDEX `user_id` (`user_id`),
	ADD INDEX `activated` (`activated`);
ALTER TABLE `custumer__custumer_file`
	ADD INDEX `custumer_id` (`custumer_id`),
	ADD INDEX `filecat_id` (`filecat_id`),
	ADD INDEX `main_file` (`main_file`);
ALTER TABLE `custumer__custumer_to_property`
	ADD INDEX `custumer_id` (`custumer_id`),
	ADD INDEX `property_id` (`property_id`);
ALTER TABLE `custumer__demand`
	ADD INDEX `custumer_id` (`custumer_id`);
ALTER TABLE `custumer__demand_to_property`
	ADD INDEX `demand_id` (`demand_id`),
	ADD INDEX `property_id` (`property_id`),
	ADD INDEX `message_id` (`message_id`);
ALTER TABLE `custumer__history`
	ADD INDEX `bin` (`bin`);
	";
;
}
if ( is_table( 'inmo__property' ) ) {

	$update_sql .= "
ALTER TABLE `inmo__category`
	ADD INDEX `active` (`active`);
ALTER TABLE `inmo__category_language`
	ADD INDEX `category_id` (`category_id`),
	ADD INDEX `language` (`language`);
ALTER TABLE `inmo__comarca`
	ADD INDEX `provincia_id` (`provincia_id`);
ALTER TABLE `inmo__configadmin_language`
	ADD INDEX `configadmin_id` (`configadmin_id`),
	ADD INDEX `language` (`language`);
ALTER TABLE `inmo__configpublic_language`
	ADD INDEX `configpublic_id` (`configpublic_id`),
	ADD INDEX `language` (`language`);
ALTER TABLE `inmo__country_language`
	ADD INDEX `country_id` (`country_id`),
	ADD INDEX `language` (`language`);
ALTER TABLE `inmo__municipi`
	ADD INDEX `comarca_id` (`comarca_id`);
ALTER TABLE `inmo__property`
	CHANGE COLUMN `bin` `bin` TINYINT(1) NOT NULL DEFAULT '0' AFTER `modified`,
	ADD INDEX `place_id` (`place_id`),
	ADD INDEX `category_id` (`category_id`),
	ADD INDEX `municipi_id` (`municipi_id`),
	ADD INDEX `comarca_id` (`comarca_id`),
	ADD INDEX `provincia_id` (`provincia_id`),
	ADD INDEX `zone_id` (`zone_id`),
	ADD INDEX `bin` (`bin`),
	ADD INDEX `user_id` (`user_id`);
ALTER TABLE `inmo__property_file`
	ADD INDEX `property_id` (`property_id`),
	ADD INDEX `filecat_id` (`filecat_id`);
ALTER TABLE `inmo__property_image`
	ADD INDEX `property_id` (`property_id`),
	ADD INDEX `imagecat_id` (`imagecat_id`),
	ADD INDEX `main_image` (`main_image`);
ALTER TABLE `inmo__property_image_language`
	ADD INDEX `image_id` (`image_id`),
	ADD INDEX `language` (`language`);
ALTER TABLE `inmo__property_language`
	ADD INDEX `property_id` (`property_id`),
	ADD INDEX `language` (`language`);
ALTER TABLE `inmo__property_to_portal`
	ADD INDEX `property_id` (`property_id`),
	ADD INDEX `apicat` (`apicat`),
	ADD INDEX `fotocasa` (`fotocasa`),
	ADD INDEX `ceigrup` (`ceigrup`),
	ADD INDEX `vreasy` (`vreasy`);
ALTER TABLE `inmo__property_to_tv`
	ADD INDEX `property_id` (`property_id`),
	ADD INDEX `tv_id` (`tv_id`);
ALTER TABLE `inmo__property_video`
	ADD INDEX `property_id` (`property_id`),
	ADD INDEX `main_video` (`main_video`);
ALTER TABLE `inmo__provincia`
	ADD INDEX `country_id` (`country_id`);
ALTER TABLE `inmo__zone`
	ADD INDEX `municipi_id` (`municipi_id`);

	";
;
}

if (is_table('product__product')){
	$update_sql .= "
ALTER TABLE `product__product`
	CHANGE COLUMN `bin` `bin` TINYINT NOT NULL DEFAULT '0';
";
}
if (is_table('news__new')){
	$update_sql .= "
ALTER TABLE `news__new`
	CHANGE COLUMN `bin` `bin` TINYINT NOT NULL DEFAULT '0';
";
}