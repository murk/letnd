<?php
// menus eina
if (!defined('DIRECTORY_MENU_DOCUMENT')){
	define('DIRECTORY_MENU_DOCUMENT', 'Archivos');
	define('DIRECTORY_MENU_DOCUMENT_NEW', 'Entrar nuevo archivo');
	define('DIRECTORY_MENU_DOCUMENT_LIST', 'Listar y editar archivos');
	define('DIRECTORY_MENU_DOCUMENT_BIN', 'Papelera de reciclage');
	define('DIRECTORY_MENU_CATEGORY', 'Carpetas');
	define('DIRECTORY_MENU_CATEGORY_NEW', 'Entrar nueva carpeta');
	define('DIRECTORY_MENU_CATEGORY_LIST', 'Listar carpetas');
	define('DIRECTORY_MENU_DOCUMENTUSER', 'Archivos');
	define('DIRECTORY_MENU_DOCUMENTUSER_LIST', 'Categorias');
	define('DIRECTORY_COMMENT_DOCUMENTUSER', 'Sólo visualización de archivos sin permisos de edición');
}

$gl_messages_new['no_category']='Para poder insertar archivos tiene que haber almenos una carpeta';

// captions seccio
$gl_caption_document['c_prepare'] = 'En preparación';
$gl_caption_document['c_review'] = 'Para revisar';
$gl_caption_document['c_public'] = 'Pública';
$gl_caption_document['c_archived'] = 'Archivada';
$gl_caption_document['c_document'] = 'Archivo';
$gl_caption_document['c_description'] = 'Descripción';
$gl_caption_document['c_entered'] = 'Fecha creación';
$gl_caption_document['c_modified'] = 'Fecha modificación';
$gl_caption_document['c_status'] = 'Estado';
$gl_caption_document['c_category_id'] = 'Carpeta';
$gl_caption_document['c_filter_category_id'] = 'Todas las carpetas';
$gl_caption_document['c_filter_status'] = 'Todos los estados';
$gl_caption_document['c_file']='Archivos';
$gl_caption_document['c_videoframe']='Video (youtube, vimeo...)';
$gl_caption_document['c_download_all']='Decargar carpeta';
$gl_caption_document['c_download_selected']='Decargar seleccionados';
$gl_caption_document['c_download']='Descargar';
$gl_caption_document['c_selected_zip_name']='archivos_seleccionados';
$gl_caption_document['c_zip_name']='archivos';

$gl_caption_category['c_category'] = 'Carpeta';
$gl_caption_category['c_category_description'] = 'Descripción';
$gl_caption_document['c_list_documents_button']='Ver archivos';
$gl_caption['c_document_icon']='';

$gl_caption['c_concession']='Permisos';
$gl_caption['c_concession_everybody']='Todos';
$gl_caption['c_concession_change']='Cambiar permisos';
$gl_caption['c_selected']='Permisos establecidos';
$gl_caption_concession['c_group_id'] = 'Grup';
?>
