<?
/**
 * NauticaEmbarcacion
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2011
 * @version $Id$
 * @access public
 */
class NauticaEmbarcacion extends Module{

	function __construct(){
		parent::__construct();
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function get_records()
	{
	    /*
		$results = Db::get_rows("SELECT embarcacion_id,shipyard
		FROM nautica__embarcacion ORDER BY shipyard");
		
		foreach ($results as $rs){
			$is = Db::get_first("SELECT shipyard_id
		FROM nautica__shipyard WHERE shipyard='".$rs['shipyard']."'");
			if ($is){
				$id = $is;
			}
			else{
				Db::execute("INSERT INTO nautica__shipyard (`shipyard`) values('".$rs['shipyard']."');");
				$id = Db::insert_id();
			}
				Debug::p($id);
			Db::execute("UPDATE `nautica__embarcacion` SET `shipyard_id`='".$id."' WHERE  `embarcacion_id`=" . $rs['embarcacion_id']);
			
		}
		return '';
		
		$results = Db::get_rows("SELECT embarcacion_id,flag
		FROM nautica__embarcacion ORDER BY flag");
		
		foreach ($results as $rs){
			$is = Db::get_first("SELECT flag_id
					FROM nautica__flag_language WHERE flag='".$rs['flag']."'");
			if ($is){
				$id = $is;
			}
			else{
				Db::execute("INSERT INTO nautica__flag () values ()");
				$id = Db::insert_id();
				Db::execute("INSERT INTO nautica__flag_language (`flag`,language,flag_id) values('".$rs['flag']."','cat',".$id.");");
			}
				Debug::p($id);
			Db::execute("UPDATE `nautica__embarcacion` SET `flag_id`='".$id."' WHERE  `embarcacion_id`=" . $rs['embarcacion_id']);
			
		}
		return '';
		
		*/
		$listing = new ListRecords($this);
		$listing->order_by = 'entered DESC, shipyard_id ASC';
		$listing->add_filter('shipyard_id');
		$listing->add_filter('tipus_id');
		$listing->add_filter('status');		
		/*if (isset($_GET['status']) && $_GET['status']!='null') {
			$listing->set_field('status','list_admin','input');
		}*/
	    return $listing->list_records();
	}
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form($is_pdf=false)
	{
	    $show = new ShowForm($this);
		$this->set_field('entered','default_value',now());
		
		$max_dossier = Db::get_first("SELECT MAX(dossier) FROM nautica__embarcacion") + 1;
		$this->set_field('dossier','default_value',$max_dossier );
		
		if ($this->config['auto_ref']){
			$this->set_field('dossier','form_admin','text');
		}
		
		if ($is_pdf) return $show;
		
		$language_cg =   array (
			'select_table' => 'all__language',
			'select_fields' => 'code,language',
			'select_condition' => 'public=1 ORDER BY ordre ASC',
			'javascript' => 'onchange="$(\'#print_pdf\').attr(\'href\',\'/\'+$(this).val()+\'/nautica/embarcacion/print_pdf/embarcacion_id/' . $show->id . '/\')" style="float:left;margin: 20px 0 20px 20px;"',
			'class' => 'btn_form1'
		  );
		$language_select =
			get_select('language', '', $this->config['default_print_language']?$this->config['default_print_language']:LANGUAGE,$language_cg);
	
		$show->get_values();
		$show->set_var('language_select',$language_select);
		if ($show->rs){
			$show->set_var('title_form_nautica',$show->rs['dossier'] . ' - ' . $show->rs['model']);
		}
		else{
			$show->set_var('title_form_nautica','');			
		}
	    return $show->show_form();
	}

	function save_rows()
	{
	    $save_rows = new SaveRows($this);
		//$save_rows->field_unique = "dossier";
	    $save_rows->save();
	}

	function write_record()
	{
	    $writerec = new SaveRows($this);
		//$writerec->field_unique = "dossier";
		
			
		// referencia automatica
		if ($this->config['auto_ref'] && $this->action=='add_record'){
			$max_dossier = Db::get_first("SELECT MAX(dossier) FROM nautica__embarcacion") + 1;
			$this->set_field('dossier','override_save_value',$max_dossier);
		}
		
	    $writerec->save();
	}

	function manage_images()
	{
	    $image_manager = new ImageManager($this);
	    $image_manager->execute();
	}
	function print_pdf(){		
		
		$this->action='show_record';
		$show = $this->get_form(true);
		$show->show_langs=false;
		
		$show->get_values();
		
		$this->set_details($show);
		$show->rs['weight'] = $show->rs['weight']/1000; // passo a tonelades
		$show->rs['length_f'] = format_currency($show->rs['length']*3.2808399,2); // passo a feet
		$show->rs['breadth_f'] = format_currency($show->rs['breadth']*3.2808399,2); // passo a feet
		$show->rs['depth_f'] = format_currency($show->rs['depth']*3.2808399,2); // passo a feet
		$show->rs['depth2_f'] = format_currency($show->rs['depth2']*3.2808399,2); // passo a feet
		
		$show->set_record();
		
		$pdf = new PDF('P', 'mm', 'A4', true, 'UTF-8', false);
		$pdf->is_admin=$GLOBALS['gl_is_admin'];
		$pdf->path = CLIENT_PATH . 'templates/print/';
		$pdf->show_pdf($show);
	}
	// copia exacta del public
	function set_details(&$show){
		// Obtenir valors i array per elbucle de detalls
		$rs = &$show->rs;
		
		/* TOTS ELS CAMPS DE LA TAULA
		$fields = array(	
			embarcacion_id,clau,dossier,entered,status,tipus_id,shipyard_id,model,buildyear,engine,fuel,length,breadth,weight,depth,depth2,flag_id,hour_use,price,vhf,depthsounder,speedlog,radar,gps,plotter,autopilot,trimmtabs,searchlight,electrical_capstan,gangway,liferaft,tender,offshore_connexion,battery_charger,generator,cover,camping_cover,bimini_top,bowthruster,hot_water,davit,anemometer,mainsail,genoa,mizzen,jib,stormsail,forsail2,spinaker,spi_boom,roller_genoa,equipament,music_equipment,television,attached_engine,fridge,heating,air_conditioning,teka,wc,black_water,roller_mainsail
			
		);
		*/
		$fields_arr['electronics'] = array(	
			'vhf','depthsounder','speedlog','radar','gps','plotter','anemometer','autopilot','music_equipment','television'
		);
		$fields_arr['extras'] = array(	
			'trimmtabs', 'searchlight', 'electrical_capstan', 'gangway', 'liferaft', 'tender', 'attached_engine', 'offshore_connexion', 'fridge', 'battery_charger', 'generator', 'cover', 'camping_cover', 'bimini_top', 'bowthruster', 'hot_water', 'davit','heating','air_conditioning','teka','wc','black_water'
		);
		$fields_arr['sails'] = array(	
			'mainsail','genoa','mizzen','jib','stormsail','forsail2','spinaker','spi_boom','roller_mainsail','roller_genoa'
		);
		
		$rs['electronics'] = $rs['extras'] = $rs['sails'] = array();
		
		foreach($fields_arr as $key=>$fields)
		{
			$is_odd = false;
			foreach($fields as $field)
			{
				if ($rs[$field] && $rs[$field]!='0000') // l'any pot ser '0000' quan no s'ha entrat cap
				{
					$is_odd=$is_odd?false:true; // intercanviio el valor de is_odd
					
					$new_array = array('c_detail' => $show->caption['c_'.$field],'detail' => &$rs[$field],'units' => '','is_odd' => $is_odd);
					
					// si el camp te unitat li poso enla variable units
					//$units = (isset($rs[$field.'_unit']))?$field.'_unit':'';
					//$units = (isset($rs[$field.'_period']))?$field.'_period':$units;
					//if ($units) $new_array['units'] = &$rs[$units];
					
					$rs[$key][]= $new_array;				
				}
			}
		}
	}
	
}
if (is_file(CLIENT_PATH . 'templates/print/nautica_embarcacion_pdf.php')){
	include(CLIENT_PATH . 'templates/print/nautica_embarcacion_pdf.php');	
}
else {
	include(DOCUMENT_ROOT . 'admin/modules/nautica/nautica_embarcacion_pdf.php');
}
?>