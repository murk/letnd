<?php
if (!defined('INVESTIN_MENU_PAGINA')){
	// menus eina

	define('INVESTIN_MENU_PAGINA','Edició de pàgines');
	define('INVESTIN_MENU_PAGINA_NEW','Entrar nova pàgina');
	define('INVESTIN_MENU_PAGINA_NEW_SECOND','Entrar nova pàgina secundària');
	define('INVESTIN_MENU_PAGINA_LIST','Llistar pàgines');
	define('INVESTIN_MENU_PAGINA_LIST_SECOND','Llistar pàgines secundàries');
	define('INVESTIN_MENU_PAGINA_BIN','Paperera de reciclatge');

	define('WEB_MENU_BANNER','Banners');
	define('WEB_MENU_BANNER_NEW','Nou banner');
	define('WEB_MENU_BANNER_LIST','Llistar banners');
	define('WEB_MENU_BANNER_LIST_BIN','Paperera');

	define('INVESTIN_MENU_ESPAI','Zones');
	define('INVESTIN_MENU_ESPAI_NEW','Entrar nova zona');
	define('INVESTIN_MENU_ESPAI_LIST','Llistar zones');
	define('INVESTIN_MENU_ESPAI_BIN','Paperera de reciclatge');
}

/*--------------- nomès castellà i català, no traduit a la resta -------------------*/

/*pagines*/
$gl_caption_pagina['c_edit_title'] = 'Dades de la pàgina';
$gl_caption_pagina['c_page_title_title'] = 'Dades de la pàgina';
$gl_caption_pagina['c_column_title1'] = 'Primera columna';
$gl_caption_pagina['c_column_title2'] = 'Segona columna';
$gl_caption_pagina['c_column_title3'] = 'Tercera columna';
$gl_caption_pagina['c_column_title4'] = 'Vídeo';
$gl_caption_pagina['c_column_title5'] = 'Google';
$gl_caption_pagina['c_column_title6'] = 'Més informació';

$gl_caption_pagina['c_breadcrumb_edit_level1'] = '"editar pàgina"';
$gl_caption_pagina['c_breadcrumb_edit_level2'] = '"editar pàgina"';
$gl_caption_pagina['c_breadcrumb_edit_level3'] = '"editar pàgina"';
$gl_caption_pagina['c_breadcrumb_edit_list'] = '"llistat de subapartats"';

$gl_caption_pagina['c_subtitle_level2'] = 'Llistar pàgines';
$gl_caption_pagina['c_subtitle_level3'] = 'Llistar pàgines';

$gl_caption_pagina['c_filter_status'] = 'Tots els estats';
$gl_caption_pagina['c_filter_category_id'] = 'Totes les categories';
$gl_caption_pagina['c_filter_paginatpl_id'] = 'Totes les plantilles';

$gl_caption_pagina['c_edit_content_button'] = 'Editar pàgina';
$gl_caption_pagina['c_edit_pagina_button'] = 'Editar dades';



$gl_caption_pagina['c_has_content'] = 'Té contingut';
$gl_caption_pagina['c_ordre'] = 'Ordre';
$gl_caption_pagina['c_entered'] = 'Creat';
$gl_caption_pagina['c_modified'] = 'Última Modificació';
$gl_caption_pagina['c_paginatpl_id'] = 'Plantilla fitxa';
$gl_caption_pagina['c_listtpl_id'] = 'Plantilla subapartats';
$gl_caption_pagina['c_link'] = 'Link';
$gl_caption_pagina['c_pagina_file_name'] = $GLOBALS['gl_caption']['c_file_name'];
$gl_caption_pagina['c_body_supertitle'] = 'Supertítol';
$gl_caption_pagina['c_body_supertitle2'] = 'Supertítol';
$gl_caption_pagina['c_body_supertitle3'] = 'Supertítol';
$gl_caption_pagina['c_title'] = 'Títol';
$gl_caption_pagina['c_title2'] = 'Títol';
$gl_caption_pagina['c_title3'] = 'Títol';
$gl_caption_pagina['c_body_subtitle'] = 'Subtítol';
$gl_caption_pagina['c_body_subtitle2'] = 'Subtítol';
$gl_caption_pagina['c_body_subtitle3'] = 'Subtítol';
$gl_caption_pagina['c_pagina'] = 'Nom de la pàgina en el menú';
$gl_caption_pagina['c_header_1'] = 'Frase capçalera 1';
$gl_caption_pagina['c_header_2'] = 'Frase capçalera 2';
$gl_caption_pagina['c_subpagina'] = 'Subapartats';
$gl_caption_pagina_list['c_pagina'] = 'Nom';
$gl_caption_pagina['c_col_valign'] = $gl_caption_pagina['c_col_valign2'] = $gl_caption_pagina['c_col_valign3'] = 'Alineació vertical';
$gl_caption_pagina['c_col_valign_top'] = $gl_caption_pagina['c_col_valign2_top'] = $gl_caption_pagina['c_col_valign3_top'] = 'Dalt';
$gl_caption_pagina['c_col_valign_middle'] = $gl_caption_pagina['c_col_valign2_middle'] = $gl_caption_pagina['c_col_valign3_middle'] = 'Mig';
$gl_caption_pagina['c_col_valign_bottom'] = $gl_caption_pagina['c_col_valign2_bottom'] = $gl_caption_pagina['c_col_valign3_bottom'] = 'Baix';
$gl_caption_pagina['c_body_content'] = 'Contingut';
$gl_caption_pagina['c_body_content2'] = 'Contingut';
$gl_caption_pagina['c_body_content3'] = 'Contingut';
$gl_caption_pagina['c_prepare'] = 'En preparació';
$gl_caption_pagina['c_review'] = 'Per revisar';
$gl_caption_pagina['c_public'] = 'Públic';
$gl_caption_pagina['c_archived'] = 'Arxivat';
$gl_caption['c_status'] = 'Estat';
$gl_caption_pagina['c_pagina_id'] = 'Id';
$gl_caption_pagina['c_parent_id'] = 'Pertany a';
$gl_caption_pagina['c_custom_id'] = 'Galeria de personalització';
$gl_caption_pagina['c_level'] = '';
$gl_caption_pagina['c_blocked'] = 'Block.';

$gl_caption_pagina['c_add_pagina_button'] = 'Entrar nova pàgina aquí';
$gl_caption_pagina['c_list_subpagines'] = 'Llistar tots';
$gl_caption_pagina['c_add_subpagina'] = 'Afegir';
$gl_caption_pagina['c_add_subpagina2'] = 'Afegir subapartat';
$gl_caption_pagina['c_url1_name']='Nom URL';
$gl_caption_pagina['c_url1']='URL';
$gl_caption_pagina['c_url2_name']='Nom URL(2)';
$gl_caption_pagina['c_url2']='URL(2)';
$gl_caption_pagina['c_url3_name']='Nom URL(3)';
$gl_caption_pagina['c_url3']='URL(3)';
$gl_caption_pagina['c_videoframe']='Vídeo (youtube, metacafe...)<br />( 340 x 255 )';
$gl_caption_pagina['c_file']='Arxius';
$gl_caption_pagina['c_category_id']='Categoria';

$gl_caption['c_last_listing']='Llistat anterior';


$gl_caption_category['c_ordre']='Ordre';
$gl_caption_category['c_category']='Categoria';
$gl_caption_category['c_is_clubone'] = 'Menú socios';

$gl_messages['c_restore_items'] = "Els elements marcats no s'han podut recuperar, ja que primer s'ha de recuperar la pàgina superior a la que pertanyen";



/*Espais*/


$gl_caption_espai['c_municipi_id'] = "Municipi";
$gl_caption_espai['c_zone'] = "Zona";
$gl_caption_espai['c_zone_id'] = "Zona";
$gl_caption_espai['c_status'] = "Estat";
$gl_caption_espai['c_prepare'] = 'En preparació';
$gl_caption_espai['c_review'] = 'Per revisar';
$gl_caption_espai['c_public'] = 'Públic';
$gl_caption_espai['c_archived'] = 'Arxivat';

$gl_caption_espai['c_form_level1_title_de_figueres'] = "Localització";
$gl_caption_espai['c_de_figueres'] = "De figueres";
$gl_caption_espai['c_de_franca'] = "De frança";
$gl_caption_espai['c_de_girona'] = "De girona ";
$gl_caption_espai['c_de_barcelona'] = "De barcelona";

$gl_caption_espai['c_form_level1_title_xarxa_aigua'] = "Serveis del polígon";

$gl_caption_espai['c_xarxa_aigua'] = "Xarxa aigua";
$gl_caption_espai['c_xarxa_llum'] = "Xarxa de llum";
$gl_caption_espai['c_xarxa_telefon'] = "Xarxa de telefonia / internet";
$gl_caption_espai['c_seguretat'] = "Seguretat";
$gl_caption_espai['c_residus'] = "Residus";

$gl_caption_espai['c_form_level1_title_superficies'] = "Tipologia de parcel.la";
$gl_caption_espai['c_superficies'] = "Superfícies";
$gl_caption_espai['c_tipus'] = "Tipus";

$gl_caption_espai['c_form_level1_title_parcela_minima'] = "Planejament";
$gl_caption_espai['c_parcela_minima'] = "Parcel·la mínima";
$gl_caption_espai['c_ocupacio_maxima'] = "Ocupació màxima";
$gl_caption_espai['c_edificabilitat'] = "Edificabilitat";
$gl_caption_espai['c_alcada_maxima'] = "Alçada màxima";

$gl_caption_espai['c_form_level1_title_associacio_empresaris'] = "Associació d'empresaris / col.lectiu de representació";
$gl_caption_espai['c_associacio_empresaris'] = "Associació d'empresaris / col.lectiu de representació";

$gl_caption_espai['c_form_level1_title_altres'] = "Altres";
$gl_caption_espai['c_altres'] = "Altres";


$gl_caption_espai['c_form_level1_title_a_ap7'] = "Infraestructures: transport per carretera";
$gl_caption_espai['c_a_ap7'] = "Autopista AP7";
$gl_caption_espai['c_a_n2'] = "Ctra N-II";
$gl_caption_espai['c_a_c31'] = "Ctra comarcal C-31";
$gl_caption_espai['c_a_n260'] = "Ctra N-260";
$gl_caption_espai['c_a_c260'] = "Ctra C-260";
$gl_caption_espai['c_a_estacio_busos_figueres'] = "Estació d'autobus de Figueres";
$gl_caption_espai['c_a_estacio_autobusos_vilablareix'] = "Estació d'autobus de Vilablareix";

$gl_caption_espai['c_form_level1_title_a_figueres_tren'] = "Infraestructures: transport per ferrocarril";
$gl_caption_espai['c_form_level2_title_a_figueres_tren'] = "Figueres";
$gl_caption_espai['c_form_level2_title_a_girona_tren'] = "Girona";
$gl_caption_espai['c_form_level2_title_altres_transport_ferrocarril'] = "Altres";
$gl_caption_espai['c_a_figueres_tren'] = "Estació de viatgers Figueres tren convencional";
$gl_caption_espai['c_a_figueres_ave'] = "Estació de viaters Figueres-Vilafant Tren d'Alta Velocitat";
$gl_caption_espai['c_a_girona_tren'] = "Estació de viatgers Girona tren convencional";
$gl_caption_espai['c_a_girona_ave'] = "Estació de viaters Girona Tren d'Alta Velocitat";
$gl_caption_espai['c_a_contenidors_vilamalla'] = "Estació intermodal de contenidors de Vilamalla";
$gl_caption_espai['c_a_contenidors_girona'] = "Estació intermodal de contenidors";

$gl_caption_espai['c_form_level1_title_a_aeroport_barcelona'] = "Infraestructures: transport aeri";
$gl_caption_espai['c_a_aeroport_barcelona'] = "Aeroport de Barcelona";
$gl_caption_espai['c_a_aeroport_girona'] = "Aeroport de Girona";

$gl_caption_espai['c_form_level1_title_a_port_palamos'] = "Infraestructures: transport marítim";
$gl_caption_espai['c_a_port_palamos'] = "Port de Palamós";
$gl_caption_espai['c_a_port_barcelona'] = "Port de Barcelona";

$gl_caption_espai['c_form_level1_title_transport_public'] = "Transport públic";
$gl_caption_espai['c_transport_public'] = "Línia regular d'autobús amb parada en tres punts diferents del polígon";

$gl_caption_espai['c_altres_transport_carretera'] = "Altres";
$gl_caption_espai['c_altres_transport_ferrocarril'] = "Altres";
$gl_caption_espai['c_altres_transport_aeri'] = "Altres";
$gl_caption_espai['c_altres_transport_maritim'] = "Altres";


// mapes
$gl_caption['c_search_adress']='Buscar adreça';
$gl_caption['c_latitude']='latitud';
$gl_caption['c_longitude']='longitud';
$gl_caption['c_found_direction']='adreça trobada';
$gl_caption['c_edit_map'] = 'Mapa';
$gl_caption['c_save_map'] = 'Guardar ubicació';
$gl_caption['c_click_map'] = 'Fes un click en un punt o més del mapa per guardar la ubicació.';
$gl_caption['c_form_map_title'] = 'Ubicació';
$gl_messages['point_saved'] = 'Nova ubicació guardada correctament';
$gl_messages['point_not_saved'] = 'No s\'ha pogut guardar l\'ubicació';
$gl_caption['c_remove_polyline']='Esborrar traçat';
?>