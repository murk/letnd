<?php
// menus eina
if (!defined('PVGMAO_MENU_MAQUINA')) {
	define( 'PVGMAO_MENU_MAQUINA', 'Màquines' );
	define( 'PVGMAO_MENU_MAQUINA_NEW', 'Entrar nova màquina' );
	define( 'PVGMAO_MENU_MAQUINA_LIST', 'Llistar màquines' );
	define( 'PVGMAO_MENU_MAQUINA_BIN', 'Paperera de reciclatge' );

	define( 'PVGMAO_MENU_HISTORY_LIST', 'Historial d\'estats' );

	define( 'PVGMAO_MENU_MANTENIMENTPROGRAMAT', 'Manteniments preventius' );
	define( 'PVGMAO_MENU_MANTENIMENTPROGRAMAT_NEW', 'Entrar nou manteniment' );
	define( 'PVGMAO_MENU_MANTENIMENTPROGRAMAT_LIST', 'Tots els manteniments' );
	define( 'PVGMAO_MENU_MANTENIMENTPROGRAMAT_PENDING_LIST', 'Manteniments pendents' );
	define( 'PVGMAO_MENU_MANTENIMENTPROGRAMAT_FINISHED_LIST', 'Manteniments realitzats' );

	define( 'PVGMAO_MENU_MANTENIMENTCORRECTIU', 'Manteniments correctius' );
	define( 'PVGMAO_MENU_MANTENIMENTCORRECTIU_NEW', 'Entrar nou correctiu' );
	define( 'PVGMAO_MENU_MANTENIMENTCORRECTIU_LIST', 'Manteniments' );
	define( 'PVGMAO_MENU_MANTENIMENTCORRECTIU_BIN', 'Paperera de reciclatge' );

	define( 'PVGMAO_MENU_MANTENIMENT', 'Tipus de manteniments' );
	define( 'PVGMAO_MENU_MANTENIMENT_NEW', 'Entrar nou manteniment' );
	define( 'PVGMAO_MENU_MANTENIMENT_LIST', 'Llistar manteniments' );


	define( 'PVGMAO_MENU_', '' );
	define( 'PVGMAO_MENU__NEW', 'Entrar nova' );
	define( 'PVGMAO_MENU__LIST', 'Llistat' );

	define( 'PVGMAO_MENU_OPERARI', 'Mantenidors - Usuari' );
	define( 'PVGMAO_MENU_OPERARI_NEW', 'Entrar nou' );
	define( 'PVGMAO_MENU_OPERARI_LIST', 'Llistar tots' );

	define( 'PVGMAO_MENU_TIPUS', 'Tipus de màquines' );
	define( 'PVGMAO_MENU_TIPUS_NEW', 'Entrar nou tipus' );
	define( 'PVGMAO_MENU_TIPUS_LIST', 'Llistar tipus' );

	define( 'PVGMAO_MENU_ESTAT', 'Estats' );
	define( 'PVGMAO_MENU_ESTAT_NEW', 'Entrar nou estat' );
	define( 'PVGMAO_MENU_ESTAT_LIST', 'Llistar estats' );

	define( 'PVGMAO_MENU_UBICACIO', 'Ubicacions' );
	define( 'PVGMAO_MENU_UBICACIO_NEW', 'Entrar nova ubicació' );
	define( 'PVGMAO_MENU_UBICACIO_LIST', 'Llistar ubicacions' );

	define( 'PVGMAO_MENU_ESPECIALITAT', 'Especialitat' );
	define( 'PVGMAO_MENU_ESPECIALITAT_NEW', 'Entrar nova especialitat' );
	define( 'PVGMAO_MENU_ESPECIALITAT_LIST', 'Llistar especialitats' );
}

// captions seccio
$gl_caption['c_canviar_client'] = 'Canviar client ...';
$gl_caption['c_titol_canviar_client'] = 'Escull un client per continuar';

// Màquina
$gl_caption_maquina['c_tipus_id'] = 'Tipus';
$gl_caption_maquina['c_codi_maquina'] = 'Codi';
$gl_caption_maquina['c_estat_id'] = 'Estat';
$gl_caption_maquina['c_maquina'] = 'Denominació';
$gl_caption_maquina['c_marca'] = 'Marca';
$gl_caption_maquina['c_model'] = 'Model';
$gl_caption_maquina['c_potencia'] = 'Potència';
$gl_caption_maquina['c_potencia_unitats'] = 'Unitats';
$gl_caption_maquina_list['c_potencia_unitats'] = '';
$gl_caption_maquina['c_potencia_unitats_kw'] = 'kW';
$gl_caption_maquina['c_potencia_unitats_w'] = 'W';
$gl_caption_maquina['c_potencia_unitats_hp'] = 'HP';
$gl_caption_maquina['c_data_adquisicio'] = 'Data adquisició';
$gl_caption_maquina['c_numero_serie'] = 'Número de sèrie';
$gl_caption_maquina['c_proveidor'] = 'Proveïdor';
$gl_caption_maquina['c_ubicacio_id'] = 'Ubicació';
$gl_caption['c_observacio'] = 'Observacions';
$gl_caption_maquina['c_tasques_preventives'] = 'Tasques preventives';
$gl_caption_maquina['c_usuari_id'] = 'Treballador assignat';
$gl_caption_maquina_list['c_usuari_id'] = 'Treballador';
$gl_caption_maquina['c_operari_id'] = 'Reponsable';
$gl_caption_maquina['c_operari_extern_id'] = 'Mantenidor extern';
$gl_caption_maquina['c_client_id'] = 'Client';
$gl_caption['c_maquinaitems'] = "Manteniments";

$gl_caption['c_form_level1_title_marca'] = "Identificació màquina";
$gl_caption['c_form_level1_title_maquinaitems'] = "Manteniments periòdics";

// Manteniment
$gl_caption['c_manteniment'] = "Manteniment";
$gl_caption['c_data_inici'] = "Data inicial";
$gl_caption['c_cada'] = "Cada";
$gl_caption['c_periode'] = "Període";
$gl_caption['c_periode_dia'] = "Dies";
$gl_caption['c_periode_mes'] = "Mesos";



// Estat
$gl_caption_estat['c_estat_id'] = '';
$gl_caption_estat['c_estat'] = 'Estat';

// Tipus
$gl_caption['c_tipus_id'] = '';
$gl_caption['c_tipus'] = 'Tipus';

// Ubicació
$gl_caption_ubicacio['c_ubicacio:id'] = '';
$gl_caption_ubicacio['c_ubicacio'] = 'Ubicació';

// Operari
$gl_caption['c_nom'] = 'Nom';
$gl_caption['c_cognoms'] = 'Cognoms';
$gl_caption['c_dni'] = 'DNI - CIF';
$gl_caption['c_address'] = 'Adreça';
$gl_caption['c_zip'] = 'Codi postal';
$gl_caption['c_city'] = 'Població';
$gl_caption['c_provincia_id'] = 'Provincia';
$gl_caption['c_country_id'] = 'País';
$gl_caption['c_phone'] = 'Telèfon';
$gl_caption['c_tipus_operari'] = 'Tipus';
$gl_caption['c_tipus_operari_intern'] = 'Intern';
$gl_caption['c_tipus_operari_extern'] = 'Extern';
$gl_caption['c_tipus_operari_usuari'] = 'Usuari';
$gl_caption['c_especialitat_id'] = $gl_caption['c_especialitat'] = 'Especialitat';


$gl_caption['c_client_id'] = 'Client';

$gl_caption['c_pvgmao_see_estat_history'] = "Veure historial d'estats";

// Manteniments programats
$gl_caption['c_maquina_id'] = 'Màquina';
$gl_caption['c_maquinaitem_id'] = 'Manteniment';
$gl_caption['c_manteniment_id'] = 'Manteniment';
$gl_caption['c_operari_id'] = 'Mantenidor';
$gl_caption['c_data_previst'] = 'Data prevista';
$gl_caption['c_data_realitzat'] = 'Data realització';
$gl_caption['c_cost'] = 'Cost';
$gl_caption['c_observacio'] = 'Observacions';
$gl_caption['c_client_id'] = 'Client';

$gl_caption['c_filter_maquina_id']='Totes les màquines';
$gl_caption['c_filter_manteniment_id']='Tots els manteniments';
$gl_caption['c_filter_operari_id']='Tots els mantenidors';

$gl_caption['c_mantenimentprogramat_search_title'] = "Manteniments entre les dates previstes";
$gl_caption['c_dates_from'] = "Del";
$gl_caption['c_dates_to'] = "al";

// Manteniments correctius
$gl_caption['c_correctiu'] = "Correctiu";
$gl_caption['c_causa'] = "Causa";

// History
$gl_caption_history['c_estat_id'] = "Estat";
$gl_caption_history['c_date'] = "Data";
$gl_caption_history['c_tipus'] = "Tipus";

$gl_caption['c_filter_estat_id']='Totes les màquines';

$gl_caption['c_file'] = "Arxius";
