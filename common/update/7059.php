<?php
/* 14-9-18
Flag per forçar regenerar css i jscripts
*/

if ( is_table( 'all__configadmin' ) && ! Db::get_first( "SELECT * FROM all__configadmin WHERE name='force_css_jscript_generation'" ) ) {
	$update_sql[] = "
		INSERT INTO all__configadmin (`name`, `value`) VALUES ('force_css_jscript_generation', '1');";

}
else {
	if ( is_table( 'all__configadmin' ) ) {
		trigger_error( 'Ja existeixen el camp force_css_jscript_generation' );
	}
}