<?
/**
 * * Gestiona debug desde command line
 *
 *
 *
 */

class Debug {
/**
	 * Debug::start()
	 * Static function
	 * Defineixo si haig de fer debug o no
	 * @access public
	 * @return void
	 */
	static $clockwork;

	static public function init_clockwork() {
		include( DOCUMENT_ROOT . 'common/includes/Clockwork/Support/Letnd/ClockworkMiddleware.php' );
		self::$clockwork = Clockwork\Support\Letnd\ClockworkMiddleware::get_clock();
	}
	static public function start(){

		global $argv;

		error_reporting(0);
		
		$debug =  isset($argv[1]) && $argv[1] == 'debug';
		define("DEBUG", $debug);

		if ( DEBUG ) {
			Debug::init_clockwork();
			Debug::info('HOST_URL', HOST_URL);
		}

		set_error_handler(array('Debug', 'on_error'));
		register_shutdown_function(array('Debug','shutdown'));

	}

	static public function add($message, $output, $is_title=false)
	{
		Debug::p( $output, $message );
	}

	static public function p_all()
	{
	    $execution_time = Debug::get_execution_time();
		echo "Temps d'execució:" . $execution_time;
	}

	/**
	 * Debug::p()
	 *
	 * @param mixed $output
	 * @param string $message
	 * @param boolean $print_anyway Si es true imprimeix inclus quan DEBUG = false
	 * @param boolean $echo Si es falç no imprimeix, però si que retorna valor
	 * @param bool $is_title
	 * @param string $link
	 */
	static public function p($output, $message = '', $print_anyway = false, $echo = true, $is_title=false, $link='')
	{

		if ( ! DEBUG && ! $print_anyway ) {
			return;
		}

		$out_file = '';

		if ( $message ) {
			$out_file .= '<font class="red" color="#967e09"><strong>' . $message . '</strong></font>: ';
		}

		if (is_array($output)){

			$out_file .= '<pre>';
			$out_file .= $is_title?var_export($output, true):htmlentities(var_export($output, true));
			$out_file .= '</pre>';

		}
		else {
			if ($output === false)
				$output = 'false';
			$out_file .= $is_title ? ( $output ) : ( htmlentities( $output ) );
		}
		echo $out_file . '<br>';
	}
	static public function on_error($type, $message, $file, $line )
	{
		$err_text = '';
		switch ( $type ) {
			case E_USER_ERROR:
				$err_text .= "<b>ERROR</b> [$type] $message";
				$err_text .= "  Error fatal a la linea $line a l'arxiu $file";
				break;

			case E_WARNING:
				$err_text .= "<b>WARNING</b> [$type] $message";
				break;

			case E_NOTICE:
				$err_text .= "<b>NOTICE</b> [$type] $message";
				break;

			default:
				$err_text .= "<b>Error desconegut</b>: [$type] $message";
				break;
		}

		$backtrace = Debug::get_error_backtrace();
		$err_text .= "$backtrace<br /><br>";
		// send_mail_admintotal( 'Error CLI', $err_text );
		echo  $err_text;

		/* execute PHP internal error handler */
		return false;
	}
	static public function shutdown()
	{

		$error      = error_get_last();
		if($error !== NULL) {

			$err_text = '';
			extract( $error );
			echo  "  Error CLI shutdown: $message, arxiu: $file, linea: $line <br>";

			send_mail_admintotal( 'Error CLI shutdown - ' . $_SERVER["HTTP_HOST"], "Error CLI shutdown: $message, arxiu: $file, linea: $line" );
			exit;
		}

		return false;
	}

	static public function get_error_backtrace(){
		$errs = debug_backtrace(0);
		$err_text = "<br>Backtrace: <br><br>";

		foreach ($errs as $err){
			extract($err);

				$file_name = explode ('\\',$file);
				if (!is_array($file_name)) $file_name = explode ('/',$file);
				$file_name = $file_name[count($file_name)-1];
				$err_text .= 'Arxiu: ' . $file . ', linea: ' . $line . "<br>";

		}

		return $err_text;
	}

	static public function get_clock_link($file, $line, $message) {

		return "<a target=\"hidden\" href=\"http://localhost:63342/api/file?file=$file&line=$line\">
											$message:$line
										</a>";
	}

	static public function get_execution_time()
	{
	    // paro el rellotge abans de volcar tot el debug
	    $time_end = microtime();

	    $time_start = explode(' ', DOCUMENT_TIME_START);
	    $time_end = explode(' ', $time_end);

	    return number_format(($time_end[1] + $time_end[0] - ($time_start[1] + $time_start[0])), 3, ',', '.');
	}

	static function debugg($message, $output, $link = false)
	{
		if (!DEBUG) return;

		if (!is_array($output)) {$output = [ $output ];}
		Debug::$clockwork->debug($message, $output);
	}

	static function info($message, $output)
	{
		if (!DEBUG) return;

		if (!is_array($output)) {$output = [ $output ];}
		Debug::$clockwork->info($message, $output);
	}

	static function notice($message, $output)
	{
		if (!DEBUG) return;

		if (!is_array($output)) {$output = [ $output ];}
		Debug::$clockwork->notice($message, $output);
	}

	static function error($message, $output)
	{
		if (!DEBUG) return;

		if (!is_array($output)) {$output = [ $output ];}
		Debug::$clockwork->error($message, $output);
	}
	static function startQuery($query){

		if (!DEBUG) return;

		Debug::$clockwork->getDataSources()[1]->startQuery($query);
	}
	static function stopQuery(){

		if (!DEBUG) return;

		Debug::$clockwork->getDataSources()[1]->stopQuery();
	}

	// Provocar errors
	static function throw_warning( $message ){
		trigger_error($message, E_USER_WARNING);
	}
	static function throw_error( $message ){
		trigger_error($message, E_USER_ERROR);
	}



	// TODO-i Es poden borrar



	static public function add_zray_title( $title, $place ) {

		$title = ucfirst( $title );
		Debug::p(array("<b>$title</b>", ''), $place);

	}
	static public function print_zray( $messages, $place ) {

		// per posar la syntax abreviada
		if ( ! is_array( current( $messages ) ) ) {
			$new_messages = array($messages);
			$messages = $new_messages; // llegeix la variable el zray
		}
	}
	static public function add_zray( $messages, $place ) {

		// per posar la syntax abreviada
		if ( ! is_array( current( $messages ) ) ) {
			$new_messages = array($messages);
			$messages = $new_messages; // llegeix la variable el zray
		}
	}
}