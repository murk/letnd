<?

 /* 
 * - Les carpetes es veuen sempre si es te permis
 * - Els arxius si no tenen permisos propis agafen el permis de la carpeta on son
 * - Els arxiu si tenen permisos propis agafen els permisos propis i ignoren els de la carpeta
 * - Els arxiu amb permisos propis:
 *			si la carpeta es per tothom es veu		
 *			si la carpeta te permisos però no te permis per l'usuari no es veu
 */

class DirectoryCommon {
	
	public static function get_category_condition($and = false) {
		$user_id = $_SESSION['user_id'];
		
		// 1 - Hi ha un registre amb category_id i user_id
		
		// 2 - No hi ha cap registre per tant tothom te permis
		
		$query = "(
						directory__category.category_id IN (
								SELECT directory__concession.category_id 
								FROM directory__concession 
								WHERE user_id = ".$user_id.") 
						OR 
						directory__category.category_id NOT IN (
								SELECT DISTINCT directory__concession.category_id 
								FROM directory__concession)
					)";
		if ($and) $query = ' AND ' . $query;
		return $query;
	}
	
	public static function get_document_condition($category_id, $and = false, $check_category = true) {
		$user_id = $_SESSION['user_id'];
		
		// LA CARPETA TE PERMIS, SI NO TE PERMIS LA CARPETA TORNEM AL INICI
		// 1 - Hi ha un registre amb category_id i user_id - La carpeta te permis
		
		// 2 - No hi ha cap registre per tant tothom te permis - La carpeta te permis

		// TODO-i https://www.apigirona.com/admin/?action=search&tool=directory&tool_section=document&menu_id=312&q=&Submit=ok   peta, però haig de tindre en compte els permisos quan es busca per carpetes
		if ($check_category) {
			$query = "SELECT count(*) FROM directory__concession
					WHERE user_id = " . $user_id . " AND category_id = " . $category_id;

			if ( ! Db::get_first( $query ) ) {

				$query = "SELECT count(*) FROM directory__concession
					WHERE category_id = " . $category_id;

				if ( Db::get_first( $query ) ) {

					// no te permis per la carpeta
					Main::redirect( '/admin/?menu_id=312' );
				}
			}
		}
		
		
		// EL DIRECTORI TE PERMIS 
		
		$query = "(
						directory__document.document_id IN (
								SELECT directory__concession.document_id 
								FROM directory__concession 
								WHERE user_id = ".$user_id.") 
						OR 
						directory__document.document_id NOT IN (
								SELECT DISTINCT directory__concession.document_id 
								FROM directory__concession)
					)";
		if ($and) $query = ' AND ' . $query;
		return $query;
	}
	
	
	static public function get_group_condition()
	{
	// Es veuen els usuaris del propi grup per poder-se donar permisos,
	// No passa res si no es donen permisos a un mateix, perque desde el menu d'administracio del les carpetes i permisos sempre ho pot veure el que te premisos per aquella secció ( permisos del modul de usuaris i no el de directoris )
		$group_condition = $_SESSION['group_id'] > 2?3:$_SESSION['group_id'];
		$group_condition = 'group_id >=' . $group_condition;
		return $group_condition;
	}
	
	
	// quan es document_id, retorno els permisos del document si en te, i '' si en te la carpeta o no en te
	static public function get_concession_names($category_id, $retun_users=false,$retun_groups=false, $document_id=false){
		
		$ret = '';
		if ($document_id){

			$query = "SELECT group_id, user_id
						FROM directory__concession
						WHERE document_id = '".$document_id."'
		";

		}
		else{

			$query = "SELECT group_id, user_id
						FROM directory__concession
						WHERE category_id = '".$category_id."'
		";

		}
		
		$results  = Db::get_rows($query);

				
		if (!$results) {
			return '';
		}
		else{
			$html = '';
			$user_ids = implode_field($results, 'user_id', ',');
			$group_ids = implode_field($results, 'group_id', ',');
			
			// obtinc users
			$query = "SELECT CONCAT(name,' ',surname) as name, user_id AS id
						FROM user__user 
						WHERE
						" . DirectoryCommon::get_group_condition() . " 
						AND user_id IN (".  $user_ids . ")				
						ORDER BY surname ASC";
			$results_concession = Db::get_rows($query);
			
			if ($retun_users) return $results_concession;
		
			$html .= '<ul>';
			foreach ($results_concession as $rs){
				$html .='<li>' . $rs['name'] . '</li>';
			}	
			$html .= '</ul>';
			
			// obtinc grups
			$query = "SELECT name, group_id AS id
						FROM user__group
						WHERE
						" . DirectoryCommon::get_group_condition() . " 
						AND group_id IN (".  $group_ids . ")				
						ORDER BY ordre ASC, name ASC";
			$results_concession = Db::get_rows($query);
			
			if ($retun_groups) return $results_concession;
		
			$html .= '<ul>';
			foreach ($results_concession as $rs){
				$html .='<li>' . $rs['name'] . '</li>';
			}	
			$html .= '</ul>';
			$ret =  $html;
		
		}
		
		$ret = '<div id = "concession_'.($document_id?$document_id:$category_id).'">' . $ret . '</div>';
		return $ret;
		
	}

}



$directory_category_id = R::text_id('category_id');
if ($directory_category_id == 'null') $directory_category_id = false;

$GLOBALS['gl_page']->last_sub_menu[312] =

	Db::get_rows("
		SELECT 
			category as item, 
			CONCAT('&tool=directory&tool_section=document&category_id=',directory__category.category_id) as id,
			IF (directory__category.category_id = '".$directory_category_id."','1','0') as selected
		FROM directory__category_language, directory__category
		WHERE language = '".LANGUAGE."' " .
		DirectoryCommon::get_category_condition(true). "
		AND directory__category_language.category_id = directory__category.category_id	
		ORDER BY ordre asc, category asc");

?>