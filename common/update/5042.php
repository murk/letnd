<?

/* 24-10-13 
  
 * Inmo: Els missatges enviats desde la newsletter van a l'historial del client
 * Inmo: S'han afegit el camp "titol" a l'historial del client
 * Inmo: millora en el filtratge dels inmobles a l'insertar inmobles a newsletter o a clients

 */

if (is_table('custumer__history')) {	
	
	$update_sql[] = "
		ALTER TABLE `custumer__history`
	ADD COLUMN `history_title` VARCHAR(255) NOT NULL AFTER `tipus`,
	ADD COLUMN `message_id` INT(11) NOT NULL DEFAULT '0' AFTER `property_id`;";	
}


?>