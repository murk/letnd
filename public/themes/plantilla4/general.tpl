<!DOCTYPE html>
<html lang="<?=LANGUAGE_CODE?>">
	<head>
		
		<?/* 
		Nomes per webs responsive:		
		
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
		<meta name="format-detection" content="telephone=no" />
		
		*/?>
		
		
		<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>	
		
		<?=load_css('general',
			'/common/jscripts/prettyphoto3.1.4/css/prettyPhoto.css,
			/common/jscripts/jquery-ui-1.8.20/css/ui-lightness/jquery-ui-1.8.20.custom.css')?>
		<?=get_header($this)?>
		
		<?/* JSCRIPT */?>		
		<script type="text/javascript">
		
			var a_vars = Array();var pagename='<?=addslashes($title)?>';var phpmyvisitesSite = 1;var phpmyvisitesURL = "<?=HOST_URL?>common/includes/phpmv2/phpmyvisites.php";
			
			var gl_language = '<?=$language?>';
			var gl_is_home = <?=$is_home?'true':'false'?>;	
			var gl_tool = '<?=$tool?>';	
			var gl_tool_section = '<?=$tool_section?>';	
			var gl_action = '<?=$action?>';	
			var gl_page_text_id = '<?=$page_text_id?>';
			var gl_dir_templates = '<?=DIR_TEMPLATES?>';
			$(function(){
				<?=$javascript?>
			});	
		</script>
		
		<?=load_jscript('
			/common/includes/phpmv2/phpmyvisites.js,
			/common/jscripts/prettyphoto3.1.4/js/jquery.prettyPhoto.js,
			/common/jscripts/caroufredsel/jquery.touchSwipe.min.js,
			/common/jscripts/caroufredsel/jquery.carouFredSel-6.2.1-packed.js,
			/common/jscripts/jquery.blockUI-2.35.min.js,
			/common/jscripts/jquery-ui-1.8.20/js/jquery-ui-1.8.20.datepicker.min.js,
			/common/jscripts/jquery-ui-1.8.20/languages/jquery.ui.datepicker-'.$language_code.'.js,
			/public/jscripts/hide_m.js,
			/admin/languages/'.$language.'.js,
			/admin/jscripts/validacio.js,
			/common/jscripts/functions.js,
			'.DIR_TEMPLATES.'jscripts/contact.js'
		)?>		

	</head>
	
<body>

	<header class="container">
				<div id="header_phone"><h2><?=PAGE_PHONE?></h2></div>
		<div id="header">
			<div id="header_left"><img src="/<?=CLIENT_DIR?>/images/logo_web.png" alt="<?=COMPANY_NAME?>" /></div>
			<div id="header_right">
			<? if ($blocks_ht):?>
			<? foreach($blocks_ht as $b): extract ($b)?>
				<?=$block?>
				<? endforeach //$blocks?>
			<? endif //$blocks_ht?>
			</div>
		</div>
		<div class="cls"></div>
		<div id="header_middle">
			<div id="header_middle_left">
					<div>
						<?for($i = 1;$i<10;$i++):?>
							<img src="/<?=CLIENT_DIR?>/templates/images/img<?=$i?>.jpg" width="400" height="230" alt="" />
						<?endfor?>
					</div>
			</div>
			<div id="header_middle_right"><div id="header_middle_right_top">
			<?=$page_slogan?>
			</div><div id="header_middle_right_bottom" style="background: url('/<?=CLIENT_DIR?>/images/header_web_2.jpg') no-repeat;"><form action="/index.php?tool=inmo&amp;tool_section=property" method="get" name="search" id="search" onsubmit="if (this.q.value=='<?=$c_search?>' || this.q.value=='') {return false};">
				<input name="tool" type="hidden" value="inmo" />
				<input name="tool_section" type="hidden" value="property" />
				<input name="action" type="hidden" value="list_records_search_search" />        
				<div class="search_button"><input type="image" name="search_button" id="search_button" src="<?=DIR_TEMPLATES?>images/lupa.gif" alt="<?=$c_search?>" /></div><div class="search_input"><input class="search" name="q" type="text" size="27" value="<?=$c_search?>" onfocus="this.value=''" /></div>
			</form></div></div></div>
		<div id="header_bottom">
			<? if ($blocks_hb):?>
				<? foreach($blocks_hb as $b): extract ($b)?>
				<?=$block?>
				<? endforeach //$blocks?>
				<? endif //$blocks_hb?>
		</div>
	</header>


	<main id="container">

		<!-- Tenca header -->
		<div id="content">
			<!--inclou el left, mt, mb, right i content_center -->
			<div id="left">      
		<? $blocks_total = count($blocks_l); $blocks_conta=1; ?>
			<? if ($blocks_l):?>
				<?foreach($blocks_l as $b): extract ($b)?>
				<?=$block?><?if ($blocks_conta<$blocks_total){$blocks_conta++?><div class="sep"></div><?} //$blocks_conta?>
				<?endforeach //$blocks  ?>
				<? endif //$blocks_l?>
			</div>
			<!-- tenca left -->
			<div id="middle">
			<h2>
				<?=$title;?>
			</h2>
			<? if ($message):?>
				<div id="message">
				<?=$message?></div>
				<? endif //message?>
			<?=$content;?>
			</div>
			<!-- tenca middle -->
			<div class="cls"></div>
			<? if ($blocks_mb):?>
			<div class="sep"></div>
			<div id="middle_bottom">
				<? foreach($blocks_mb as $b): extract ($b)?>
				<?=$block?>
				<? endforeach //$blocks?>
			</div>
			<? endif //$blocks_mb?>
			<!-- tenca middle_bottom -->
		</div>
		<!-- Tenca content -->
		<div id="footer">
		<div id="footer_top">e-mail. <script type="text/javascript">h_m('fo','contacte','co','in','esgarr','otxa','finqu','m','true','','')</script><script type="text/javascript">h_m('','','','','','','','','','','')</script>
		</div><div id="footer_bottom">
				<?= $page_address ?> <?= PAGE_NUMSTREET ?>, <?= PAGE_POSTCODE ?> <?= PAGE_MUNICIPI ?>
					, <?= PAGE_PROVINCIA ?>, <?= PAGE_COUNTRY ?> - <?= PAGE_PHONE ?>

					<div class="aicat">
						<img src="/clients/finquesgarrotxa/templates/images/aicat.png" width="201" height="31" alt="Aicat 5186"/>
					</div>
			<div class="letnd"><?=$c_seo_blanc_4?></div></div></div>
		<!-- tenca footer-->
	</main>
	<!-- tenca container-->



		<iframe name="save_frame" id="save_frame" class="<?=$showiframe?>"></iframe>
		
		<?if(can_accept_cookies()):?>			
			<?=GOOGLE_ANALYTICS?>			
		<?else: //?>
			<?=get_cookies_message()?>
		<?endif //cookies?>
		<script type="text/javascript" src="http://maps.google.com/maps/api/js?key=AIzaSyAaIGZEloZFoSY8TuTDyL27MDbuDvW5AoM"></script>
		<?/* FI JSCRIPT */?>
</body>
</html>
