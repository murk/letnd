<?php

function save_rows()
{
    $save_rows = new SaveRows;
    $save_rows->save();
}

function list_records()
{
	global $gl_content, $gl_menu_id;//$gl_action


	if (isset($_GET['mod']) && ($_GET['mod']=='view_graph')) {
		include (DOCUMENT_ROOT . 'common/includes/phpmv2/index.php');
		die();
	};


	$_REQUEST['site'] = 1;
	if (isset($_REQUEST['period']) ){
		$_SESSION['period'] = $_REQUEST['period'];
	}
	elseif (isset($_SESSION['period']))
	{
		$_REQUEST['period'] = $_SESSION['period'];
	}
	if (isset($_REQUEST['date']) ){
		$_SESSION['date'] = $_REQUEST['date'];
	}
	elseif (isset($_SESSION['date']))
	{
		/*Una matriz asociativa que consiste en los contenidos de $_GET, $_POST, y $_COOKIE.

Esta es una variable 'superglobal', o global automática. Esto simplemente quiere decir que está disponible en todos los contextos a lo largo de un script. No necesita hacer global $_REQUEST; para acceder a ella dentro de funciones o métodos.

Si la directiva register_globals está definida, entonces estas variables también estarán disponibles en el contexto global del script; i.e., por separado de la matriz $_REQUEST. Para información relacionada, consulte el capítulo de seguridad titulado Uso de Registros Globales. Estas globales individuales no son superglobales.

*/
		$_REQUEST['date'] = $_SESSION['date'];
	}

	 switch (true)
    {
        case ($gl_menu_id == '6002'):
            $_REQUEST['mod'] = 'view_visits';
            break;
        case ($gl_menu_id == '6008'):
            $_REQUEST['mod'] = 'view_frequency';
            break;
        case ($gl_menu_id == '6003'):
            $_REQUEST['mod'] = 'view_pages';
            break;
        case ($gl_menu_id == '6004'):
            $_REQUEST['mod'] = 'view_followup';
            break;
        case ($gl_menu_id == '6005'):
            $_REQUEST['mod'] = 'view_source';
            break;
        case ($gl_menu_id == '6006'):
            $_REQUEST['mod'] = 'view_settings';
            break;
        case ($gl_menu_id == '6007'):
            $_REQUEST['mod'] = 'view_referers';
            break;
	}



	ob_start(); // Start output buffering
    include (DOCUMENT_ROOT.'common/includes/phpmv2/index.php');
    $gl_content = ob_get_contents(); // Get the contents of the buffer
    ob_end_clean(); // End buffering and discard
	Db::connect();

}
function list_records_visits()
{
	global $gl_content;//contingut
	global $gl_action; //recull l'acció--
	ob_start(); // Start output buffering
 	include (DOCUMENT_ROOT . 'common/includes/phpmv2/index.php');  ////include (DOCUMENT_ROOT . 'common/includes/phpmv2/index.php' . $vars);
	//http://w.canxiquet2/admin/index.php?site=1&period=1&mod=view_visits&date=2007-01-30&menu_id=6002
	//get_all_get_params($exclude_array = '')
	//get_all_get_params(array('menu_id');


    $gl_content = ob_get_contents(); // Get the contents of the buffer
    ob_end_clean(); // End buffering and discard
	Db::connect();
}

function show_form()
{
    $show = new ShowForm;
    $show->show_form();
}

function write_record()
{
    $writerec = new SaveRows;
    $writerec->save();
}

function manage_images()
{
    $image_manager = new ImageManager;
    $image_manager->execute();
}

?>