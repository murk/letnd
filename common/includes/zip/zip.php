<?php

/**
 * Creació de Zips descàrrega automàtica
 */
class Zip {
	var $zip;
	var $file_name;
	var $file;

	function __construct( $file_name ) {

		$temp_path = GLOBAL_TEMP_PATH;

		$this->clean_files($temp_path);

		$file      = tempnam( $temp_path, 'tmp' );
		rename( $file, $file . '.zip' );
		$file .= '.zip';

		$this->file_name = $file_name;
		$this->file      = $file;


		$this->zip = new ZipArchive();
		$this->zip->open( $file );
	}

	public function addFile( $name, $new_name ) {
		$new_name = iconv( 'UTF-8', 'IBM850', $new_name );
		$this->zip->addFile( $name, $new_name );
	}

	public function output() {
		$this->zip->close();

		ignore_user_abort( true );

		header( "Content-disposition: attachment; filename=" . $this->file_name . ".zip" );
		header( "Pragma: public" );
		header( "Expires: 0" );
		header( "Cache-Control: must-revalidate, post-check=0, pre-check=0" );
		header( "Cache-Control: private", false );
		header( "Content-Type: application/zip" );
		header( "Content-Transfer-Encoding: binary" );
		readfile( $this->file );

		unlink( $this->file );

		Debug::p_all();
		die();

	}

	function clean_files ( $dir ) {

		// Borrar arxius temporals
		$t = time();
		$h = opendir( $dir );
		while ( $file = readdir( $h ) ) {
			if ( substr( $file, - 4 ) == '.zip' )
			{
				$path = $dir . '/' . $file;
				if ( $t - filemtime( $path ) > 3600 ) {
					@unlink( $path );
				}
			}
		}
		closedir( $h );
	}
}

?>