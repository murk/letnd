<?php
// menus eina
define('CONTACT_MENU_CONTACT','Kontakt');
define('CONTACT_MENU_LIST','Kontakt-E-Mail-Adressen');

// captions seccio
$gl_caption_contact ['c_company'] = 'Firma';
$gl_caption_contact ['c_name'] = 'Voornaam';
$gl_caption_contact ['c_surnames'] = 'Naam';
$gl_caption_contact ['c_mail'] = 'E-Mail';
$gl_caption_contact ['c_to_address'] = 'To';
$gl_caption_contact ['c_phone'] = 'Telefoon';
$gl_caption_contact ['c_matter'] = 'Betreft';
$gl_caption_contact ['c_comment'] = 'Opmerkingen';
$gl_caption_contact ['c_accept_info'] = 'Allow send information';
$gl_caption_contact_list ['c_accept_info'] = 'Allow info';
$gl_caption ['c_sendmail'] = 'E-Mail zenden';
$gl_messages ['added'] = 'E-mail succesvol verzonden.';
$gl_caption ['c_sendok'] = $gl_messages ['added'];
$gl_messages ['not_saved '] = 'Het was niet mogelijk deze email te verzenden.';

$gl_caption_contact['c_nif']='ID/BTW  NR.';
$gl_caption_contact['c_company']='Firma';

$gl_caption_contact['c_contact_language']='Contact page language';

$gl_caption_contact['c_export_button']= 'Exportar';
?>