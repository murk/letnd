<?php
// menus eina
if (!defined('FOODTOURS_MENU_TOUR')){
	define('FOODTOURS_MENU_TOUR','Tours');
	define('FOODTOURS_MENU_TOUR_NEW','Nou tour');
	define('FOODTOURS_MENU_TOUR_LIST','Llistar tours');
	define('FOODTOURS_MENU_TOUR_BIN','Paperera de reciclatge');
	define('FOODTOURS_MENU_PLACE','Llocs');
	define('FOODTOURS_MENU_PLACE_NEW','Nou lloc');
	define('FOODTOURS_MENU_PLACE_LIST','Llistar llocs');
	define('FOODTOURS_MENU_ELEMENT','Inclosos / no inclosos');
	define('FOODTOURS_MENU_ELEMENT_NEW','Nou ítem');
	define('FOODTOURS_MENU_ELEMENT_LIST','Llistar ítems');
}

// captions seccio
$gl_caption['c_subtitle'] = "Subtítol";

$gl_caption_tour['c_prepare'] = 'En preparació';
$gl_caption_tour['c_review'] = 'Per revisar';
$gl_caption_tour['c_public'] = 'Públic';
$gl_caption_tour['c_archived'] = 'Arxivat';
$gl_caption_tour['c_tour'] = 'Tour';
$gl_caption_tour['c_content'] = 'Descripció';
$gl_caption_tour['c_entered'] = 'Data creació';
$gl_caption_tour['c_modified'] = 'Data modificació';
$gl_caption_tour['c_status'] = 'Estat';
$gl_caption_tour['c_filter_category_id'] = 'Totes les categories';
$gl_caption_tour['c_filter_status'] = 'Tots els estats';
$gl_caption_tour['c_filter_in_home'] = 'Totes, home o no';
$gl_caption_tour['c_ordre'] = 'Ordre';
$gl_caption_tour['c_url1_name']='Nom URL(1)';
$gl_caption_tour['c_url1']='URL(1)';
$gl_caption_tour['c_url2_name']='Nom URL(2)';
$gl_caption_tour['c_url2']='URL(2)';
$gl_caption_tour['c_url3_name']='Nom URL(3)';
$gl_caption_tour['c_url3']='URL(3)';
$gl_caption_tour['c_videoframe']='Video (youtube, metacafe...)';
$gl_caption_tour['c_file']='Arxius';
$gl_caption_tour['c_in_home']='Surt a la Home';
$gl_caption_tour['c_in_home_0']='No surt a la Home';
$gl_caption_tour['c_in_home_1']='Surt a la Home';
$gl_caption_tour['c_content_list']='Descripció curta al llistat';
$gl_caption_tour['c_price']='Preu';
$gl_caption_tour['c_price_children']='Preu nens';
$gl_caption_tour['c_price_text']='Comentari preu';

$gl_caption_tour['c_form_level1_title_page_title']='Eines SEO';

$gl_caption_tour['c_form_level1_title_includes'] = "Inclou - exclou";
$gl_caption_tour['c_form_level1_title_bites'] = "Bites & Tastings";
$gl_caption_tour['c_form_level1_title_duration']='Tour info';
$gl_caption_tour['c_form_level1_title_price']='Preus';
$gl_caption_tour['c_form_level1_title_tour']='Descripcions';
$gl_caption_tour['c_form_level1_title_url1_name']='Enllaços i arxius adjunts';
$gl_caption_tour['c_includes'] = 'Inclou';
$gl_caption_tour['c_excludes'] = 'Exclou';
$gl_caption_tour['c_edit_dies'] = 'Dies';

$gl_caption_element['c_element'] = 'Ítem';

$gl_caption['c_ordre'] = 'Ordre';
$gl_caption['c_tour_file_name'] = $GLOBALS['gl_caption']['c_file_name'];
$gl_caption['c_tour_old_file_name'] = $GLOBALS['gl_caption']['c_old_file_name'];


// TRADUIR
$gl_caption['c_highlights'] = "Tour highlights";
$gl_caption['c_bites'] = "Bites & Tastings";
$gl_caption['c_spoken_language'] = "Language";
$gl_caption['c_spoken_language_eng'] = "English";
$gl_caption['c_spoken_language_cat'] = "Catalan";
$gl_caption['c_spoken_language_spa'] = "Spanish";
$gl_caption['c_spoken_language_fra'] = "French";
$gl_caption['c_spoken_language_dut'] = "Netherlands";
$gl_caption['c_duration'] = "Duration";
$gl_caption['c_place'] = "Lloc";
$gl_caption['c_timetable'] = "Horari";
$gl_caption['c_people'] = "Persones";
?>