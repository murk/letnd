<?if (!defined('PRODUCT_DISCOUNT_MAX_RESULTS')) Module::get_config_values('product');?>
<article>
  <h2>When purchasing items at <?=COMPANY_NAME?>, you can pay through:
  </h2>
  
  <p><strong>Paypal payment</strong><br />
  The fastest and most extended payment method, pay with credit card or bank account through your Paypal account</p>
  
  <p><strong>Payment by credit or debit card:</strong><br />
  In order to pay, you can use your credit or debit card, be it Visa or Mastercard.</p>
  
  <p><strong>Cash on delivery:</strong><br />
  Payment is done when the carrier hands your order. 
  </p>
  
  <p><strong>Bank transfer:</strong><br />
  You must transfer the total amount due  at: <?=PRODUCT_BANC_NAME?>, account number: <?=PRODUCT_ACCOUNT_NUMBER?>, and as soon as <?=COMPANY_NAME?> receives the transfer, your order will be issued.
  </p>
</article>