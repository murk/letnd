
<table border="0" cellspacing="0" cellpadding="0" class="letnd-form">
	<tr>
		<td align="left" valign="top" class="inmo-form-details-left">
			<?if ($property):?>
				<h2>
				<?=$property?>
				</h2>
			<?endif //property?>
			<?if ($property_title):?>
				<h3>
				<?=$property_title?>         
				</h3>
			<?endif //property_title?> 
			<div class="inmo-list-ref"><?=$c_ref?> <?=$ref?></div>
			<p><?=$description?></p>		
			
			<p class="letnd-info"><a title="<?=htmlspecialchars($property)?>" href="<?= HTTP ?><?= SHORT_HOST_URL ?><?=$show_record_link?>"><?=$c_more_info?></a></p>
			
		</td>
		<td class="inmo-form-details-right" valign="top">
			<table cellpadding="0" cellspacing="0" class="inmo-form-details">
			<?if ($floor_space):?>
					<tr>
					<td valign="top"><strong class="detailsdes"><?=$c_floor_space?>:</strong></td>
					<td><?=$floor_space?> m2</td>
					</tr>
					<?endif //floor_space?>
				<?if ($land):?> 
					<tr>
					<td valign="top"><strong class="detailsdes"><?=$c_land?>:</strong></td>
					<td><?=$land?> <?=$land_units?></td>
					</tr>
				<?endif //land?> 
					<?if ($category_id):?>
					<tr>
					<td valign="top"><strong class="detailsdes"><?=$c_category_id?>:</strong></td>
					<td><?=$category_id?></td>
					</tr>
				<?endif //category_id?>
				<?if ($comarca_id):?>
					<tr>
					<td valign="top"><strong class="detailsdes"><?=$c_zone?>:</strong>&nbsp;</td>
					<td><?=$comarca_id?><br>
						<?=$provincia_id?> (<?=$country_id?>)</td>
					</tr>
				<?endif //comarca_id?>
				<?if ($details):?>
				<?foreach($details as $d):?><?if ($$d):?>
					<tr>
						<td align="left" nowrap="nowrap"><strong><?=${'c_'.$d}?>:</strong></td>
						<td width="50%" align="left"><?=$$d?></td>
					</tr>
					<?endif //$$d?>
				<?endforeach //details?>
				<?endif //$details?>
			</table>
		</td>
	</tr>
</table>


<table border="0" cellspacing="0" cellpadding="0" class="letnd-form">
	<tr>
		<td valign="top" class="inmo-form-price">
			<table border="0" cellspacing="0" cellpadding="0" width="100%">
				<tr>
					<td class="inmo-form-price-td">
						<?if ($price):?><strong><?=$c_price?>: </strong>
							<?=$price?>
							<?=CURRENCY_NAME?>&nbsp;
						<?endif //price?>
						<?if ($price_consult):?>
							<?=$price_consult?>
						<?endif //price_consult?>
					</td>
				</tr>
			</table>			
		</td>
	</tr>
	<tr>
		<td valign="top" class="inmo-form-image">
			<a href="<?= HTTP ?><?= SHORT_HOST_URL ?><?=$show_record_link?>"><img <?=get_image_attrs($image_src_details, $image_alt?$image_alt:$property, 650, false, false)?> /></a>
		</td>
	</tr>
</table>