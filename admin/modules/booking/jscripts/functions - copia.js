

var booking = {
	start: true,
	date_in: false,
	date_out: false,
	painted: {},
	b_original_class: false,
	inicialized: false,
	month_width:0,
	month_loaded:{},
	month_loaded_count:0,
	first_month_viewing:'',
	paint_class:'selected',
	unpaint_class:'booked',
	caller: '',
	attr:'',	
	is_season_calendar:false,
	property_id: 0,
	is_set_booking: false,
	init: function (){
	
		if (this.inicialized) return;
		
		// fixo la mida per quan es carreguin mes mesos
		this.month_width = $('.mes-holder').width();
		$('#booking').width($('#booking').width());
		$('.mes-holder').width(this.month_width);
		
		$('.mes-holder').each(function( index ) {
			if (!booking.first_month_viewing) booking.first_month_viewing = this.id;
			booking.month_loaded[this.id] = true;
			booking.month_loaded_count++;	
		});
		
		this.inicialized = true;
	},
	set_booking: function(book_id,date_in,date_out,season_id){
				
		//var date_in = this.date_in;
		//var date_out = this.date_out;
		
		if (typeof(season_id)=='undefined') season_id = false;
		dp(date_in);
		this.date_in = false;
		this.date_out = false;
		this.attr = book_id;
		this.is_set_booking = true;
		
		this.paint_class = 'booked';
		this.unpaint_class = 'selected';		
		
		var date = date_in.split('/');
		var dia = date[2];
		var mes = date[1];
		var any = date[0];
				
		var id = '#'+dia+'-'+mes+'-'+any;
		dp(id);
		this.set_day(dia,mes,any,'b');
				
		var date = date_out.split('/');
		var dia = date[2];
		var mes = date[1];
		var any = date[0];
				
		var id = '#'+dia+'-'+mes+'-'+any;
		dp(id);
		this.set_day(dia,mes,any,'a');
		
		// resetejo per poder tornar a reservar de nou
		this.paint_class = 'selected';
		this.unpaint_class = 'booked';
		
		this.date_in = false;
		this.date_out = false;
		
	},
	show_form_edit: function(id, book_id){
		
		if (this.is_set_booking) return;
		
		// quan he creat book desde js
		if (!book_id) book_id = $(id).attr('data-book-id');
		
		var link = '/admin/?tool=booking&tool_section=book&action=get_form_ajax&book_id='+book_id;
			
		$.ajax({
			url: link
		})
			.done(function(data) {			
				$('#show-form').html(data);				
			});
	},
	show_form_new: function(formated_date_in,formated_date_out){
		
		if (this.is_set_booking) return;
		
		if (this.is_season_calendar){
			var link = '/admin/?template=window&tool=booking&tool_section=propertyrange&action=show_form_new&property_id='+this.property_id + '&date_start='+formated_date_in + '&date_end='+formated_date_out;
			showPopWin(link,600,400,false,true);
		}
		else{
			$('#dates').html(formated_date_in+' - '+formated_date_out);
			$('#date_in_0').val(formated_date_in);
			$('#date_out_0').val(formated_date_out);		
		}
	},
	do_action: function (dia, mes, any, half, book_id){
				
		var id = '#'+dia+'-'+mes+'-'+any;
		if (typeof(book_id)=='undefined') book_id = false;
		
		if (
			$(id+'-a,' +id+'-b').hasClass('middle-booked') ||
			$(id+'-'+half).hasClass('first-booked') ||
			$(id+'-'+half).hasClass('last-booked')
		){	
			this.caller = 'booked';
			if (!this.date_in) this.show_form_edit(id+'-'+half, book_id);
		}
		else if (
			$(id+'-a,' +id+'-b').hasClass('middle-selected') ||
			$(id+'-'+half).hasClass('first-selected') ||
			$(id+'-'+half).hasClass('last-selected')		
		){
			this.caller = 'selected';	
			this.set_day (dia, mes, any, half, id)	
		}
		else{
			this.caller = 'free';
			this.set_day (dia, mes, any, half, id)
		}
		dp(this.caller);
		
	},
	set_day: function (dia, mes, any, half, id){
		
		
		var other_half = half=='a'?'b':'a';
		
		if (this.date_in && this.date_out) this.paint_days(false);
		
		//book_id = document.theForm.book_id_javascript.value;
		
		var date_entered = new Date(any,mes-1,dia);	
		
		
		if (!this.date_in){
		
			
			// marco el primer click
			//if (!this.date_out){		
			
			if (this.caller != 'booked'){	
				
				this.date_in = date_entered;
				
				this.painted['a'] = {'id':id+'-a','old_class':$(id+'-a').attr('class')};
				this.painted['b'] = {'id':id+'-b','old_class':$(id+'-b').attr('class')};
											
				// el mig que he clicat el marco perque ja se que no es booked				
				$(id+'-'+half).addClass('middle-' + this.paint_class);
				$(id+'-'+half).removeClass('middle-' + this.unpaint_class);
				$(id+'-'+half).removeClass((half=='a'?'first':'last')+'-free');	
				$(id+'-'+half).removeClass('free');
						
				// l'altre mig el marco si no es booked	
				if (
					!$(id+'-'+other_half).hasClass('first-booked') &&
					!$(id+'-'+other_half).hasClass('last-booked')
					)
				{
					
					$(id+'-'+other_half).addClass('middle-' + this.paint_class);
					$(id+'-'+other_half).removeClass('middle-' + this.unpaint_class);
					$(id+'-'+other_half).removeClass((other_half=='a'?'first':'last')+'-free');	
					$(id+'-'+other_half).removeClass('free');
				}
			}
		}
		else {
			// quan clico sobre un dia marcat el desmarco i resetejo tot
			if  (date_entered.getTime() === this.date_in.getTime()){	
				$(this.painted.a.id).attr('class',this.painted.a.old_class);
				$(this.painted.b.id).attr('class',this.painted.b.old_class); 
				this.date_in = false;		
				this.cancel_paint();
				
			}
			else if  (date_entered < this.date_in){	
				this.date_out = this.date_in;		
				this.date_in = date_entered;		
			}
			else {	
				this.date_out = date_entered;			
			}
			
		}
		
		if (this.date_in && this.date_out) {
			
			
			if (this.paint_days('test')) {
				
				// restauro classes del primer dia clicat
				$(this.painted.a.id).attr('class',this.painted.a.old_class);
				$(this.painted.b.id).attr('class',this.painted.b.old_class);
				
				this.paint_days(this.paint_class);				
				
			}
			else {	
			
				if (this.date_in == date_entered) this.date_in = this.date_out;
				this.cancel_paint();
			}
			
		}
	},
	paint_days: function (paint){
	
		var conta = 1;
		var formated_date_in = '';
		for (var d = new Date(this.date_in.valueOf()); d <= this.date_out; d.setDate(d.getDate() + 1)) {
		
			var dia = d.getDate();
			var mes = d.getMonth()+1;
			var any = d.getFullYear();
			
			var id = '#'+dia+'-'+mes+'-'+any+'-a';
			var id2 = '#'+dia+'-'+mes+'-'+any+'-b';
			
			// defineixo classes per afegir i treure
			
			var middle_class = 'middle-' + this.paint_class;
			var first_class = 'first-' + this.paint_class;
			var last_class = 'last-' + this.paint_class;
			
			var unmiddle_class = 'middle-' + this.unpaint_class;
			var unfirst_class = 'first-' + this.unpaint_class;
			var unlast_class = 'last-' + this.unpaint_class;
			
			
			// canvio la classes del primer quadre del primer dia
			if (conta==1){
				$(id).removeClass('free');
				$(id).addClass('first-free');	
				id=false;
				formated_date_in = dia+'/'+mes+'/'+any;
			}			
			
			// comprobo si es pot reservar
			if (paint=='test'){
				if($(id).hasClass('middle-booked')) return false;
			}
			// pinto els dies
			else if (paint){
				
				if (id){
					if (this.painted[id] && this.painted[id].old_class)
						$(id).attr('class',this.painted[id].old_class);
					else
						this.painted[id] = {'id':id,'old_class':$(id).attr('class')};
						
					$(id).addClass( middle_class );
					//$(id).removeClass(unmiddle_class);
					//$(id).removeClass('free');
					
					if (this.attr) $(id).attr('data-book-id',this.attr);
				}
				if (id2){
					if (this.painted[id2] && this.painted[id2].old_class)
						$(id2).attr('class',this.painted[id2].old_class);
					else
						this.painted[id2] = {'id':id2,'old_class':$(id2).attr('class')};
					
					//$(id2).removeClass('free');
					if (this.attr) $(id2).attr('data-book-id',this.attr);
					
					if (!id){
						$(id2).addClass( first_class );
						//$(id2).removeClass( unfirst_class );
					}
					else{
						$(id2).addClass( middle_class );
						//$(id2).removeClass( unmiddle_class );
					}
				}
			}
			// desselecciono els dies
			else {
				if (id){
					$(id).attr('class',this.painted[id].old_class);
				}
				if (id2){
					$(id2).attr('class',this.painted[id2].old_class);
				}
			}				
			conta++;
		}
		
				
		if (paint==='test'){
		
		}
		//
		// He seleccionat una franja
		//
		else if (paint){	
			// canvio la classes de l'ultim dia	
			$(id).removeClass( middle_class );			
			$(id).addClass( last_class );
			$(id).removeClass( unlast_class );
			
			$(id2).removeClass( middle_class );			
			$(id2).addClass('last-free');
			
			if (this.attr) $(id2).attr('data-book-id','');
			
			formated_date_out = dia+'/'+mes+'/'+any;
			
			this.show_form_new(formated_date_in,formated_date_out);
		}
		else{
			this.date_in = this.date_out = false;
		}
		
		return true;
		
	},
	cancel_paint: function (){
		this.date_out = false;
		//this.date_in = this.date_out = this.a_original_class = this.b_original_class = false;		
	},
	show_month: function(direction,property_id){
	
		var viewing = this.first_month_viewing.split('-');
		
		var viewing_month = viewing[0];
		var viewing_year = viewing[1];
		var month=0;
		var year=0
		dp(viewing);
		if (direction == 'next'){
			month = Number(viewing_month)+1;
			year = viewing_year;
			if (month>12) {
				month = 1;
				year++;
			}			
			this.first_month_viewing = month + '-' + year;
			
			month = Number(viewing_month)+3;
			year = viewing_year;
			if (month>12) {
				month = month-12;
				year++;
			}
		}
		if (direction == 'prev'){
			month = viewing_month-1;
			year = viewing_year;
			if (month<1) {				
				month = 12;
				year--;
			}
			this.first_month_viewing = month + '-' + year;
		}
		dp(month + '-' + year);
		if (typeof(this.month_loaded[month + '-' + year])=='undefined'){
		
			var link = this.is_season_calendar?'season':'book';
			link = '/admin/?tool=booking&tool_section=book&action=get_month&process='+link+'&month='+month+'&year='+year+'&property_id='+property_id;
			
			$.ajax({
				url: link
			})
				.done(function(data) {
				
					if (direction=='next'){
						$('#booking-tr').append(data);
					}
					else{
						$('#booking-tr').prepend(data);
						$('#booking-table').css('margin-left',-booking.month_width);
					}
					$('.mes-holder').width(booking.month_width);
					
					booking.month_loaded[month + '-' + year] = true;
					booking.month_loaded_count++;
					
					$('#booking-table').width(booking.month_width*booking.month_loaded_count);
					
					booking.animate_month(direction);	
					
				});
			
		}
		else{
			booking.animate_month(direction);
		}
		
	},
	animate_month: function(direction){
	
		var dir = direction=='next'?-1:1
		var obj = {};
		obj['marginLeft'] =  dir*this.month_width + parseInt($('#booking-table').css('margin-left'));
				
		$('#booking-table').stop().animate(obj, 500);
	}
}

function xvalidar(obj){
	d_in_arr = obj.date_in.value.split('-');
	d_out_arr = obj.date_out.value.split('-');
	
	d = d_in_arr;
	d_in = new Date(d[0],d[1]-1,d[2]);
	d = d_out_arr;
	d_out = new Date(d[0],d[1]-1,d[2]);
	if (d_out>d_in) return true;	
	alert('La fecha de salida debe ser mayor a la fecha de entrada');
	return false;
}




function show_form_new_custumer(book_id){
	showPopWin('/admin/?action=show_form_new_custumer&menu_id=103&book_id='+book_id+'&process=book_new_custumer', 850, 520, null, true, false, '');
}


function get_custumer_jscript(custumer_id, custumer_name, gl_process)
{
	custumers_ids = document.theForm['selected_custumer_ids'].value;
	custumers_ids = custumers_ids?(custumers_ids + ',' + custumer_id):custumer_id;
	document.theForm['selected_custumer_ids'].value = custumers_ids;

	custumers_names = document.theForm['selected_custumer_names'].value;
	custumers_names = custumers_names?(custumers_names + ',' + custumer_name):custumer_name;
	document.theForm['selected_custumer_names'].value = custumers_names;
	

	window.save_frame.location = '/admin/?tool=booking&tool_section=book&action=get_custumers&selected_addresses_ids=' + custumers_ids ;
}
