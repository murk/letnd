<script language="JavaScript" src="/common/jscripts/momentjs/moment.min.js"></script>
<script language="JavaScript" src="/admin/modules/utility/jscripts/functions_sell.js?v=<?= $version ?>"></script>

<link href="/admin/themes/inmotools/styles/utility.css" rel="stylesheet"
      type="text/css">

<form id="main_form" name="theFormNO">
	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="form">
		<tr>
			<td><? if($form_new): ?><? if($save_button): ?>
					<div id="form_buttons_holder">
						<table id="form_buttons">
							<tr>
								<td class="buttons_top">
									<div class="right">
										<? /* <input type="submit" name="Submit" value="<?= $c_send_new ?>" class="btn_form1"/> */ ?>

										<a class="btn_form1" onclick="Sell.print()">
											<?= $c_print ?>
										</a>
									</div>
								</td>
							</tr>
						</table>
					</div>
				<? endif //save_button?>
				<? endif //form_new?>
				<? /* <? if($form_edit): ?>
 					<div id="form_buttons_holder">
 						<table id="form_buttons">
 							<tr>
 								<td valign="bottom">
 									<div class="menu">
 										<div><?= $c_edit_data ?></div><? if($has_images): ?>
 											<a href="<?= $image_link ?>"><?= $c_edit_images ?></a>
 										<? endif //has_images?><? foreach($form_tabs as $r): extract( $r ) ?><a
 											href="?action=<?= $tab_action ?>&<?= $id_field ?>=<?= $id ?>&menu_id=<?= $menu_id ?>"><?= $tab_caption ?></a><? endforeach //$loop?>
 									</div>
 								</td>
 								<td class="buttons_top">
 									<div class="right">
 										<? if($back_button): ?><a class="btn_form_back"
 										                          href="<?= $back_button ?>"></a><? endif //back_button?>
 										<? if($save_button): ?><input type="submit" name="Submit"
 										                              value="<?= $c_send_edit ?>"
 										                              class="btn_form1"><? endif //save_button?><? if($delete_button): ?>
 										<a class="btn_form_delete"
 										   href="javascript:submit_form('<? if($has_bin): ?><?= $c_confirm_form_bin ?><? else: ?><?= $c_confirm_form_deleted ?><? endif //has_bin?>', true);">
 											<?= $c_delete ?>
 											</a><? endif //delete_button?><? if($preview_button): ?><a class="btn_form1"
 										                                                               href="<?= $preview_link ?>"
 										                                                               target="_blank">
 											<?= $c_preview ?>
 											</a><? endif //preview_button?><? if($print_button): ?><a class="btn_form1 btn_print"
 										                                                              href="<?= $print_link ?>">
 											<?= $c_print ?>
 											</a> <? endif //print_button?></div>
 								</td>
 							</tr>
 						</table>
 					</div>
 				<? endif //form_edit?> */ ?>
				<table width="100%" border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td><? if($c_form_title): ?>
								<table width="100%" cellpadding="4" cellspacing="0">
									<tr>
										<td class="formCaptionTitle"><strong>
												<?= $c_form_title ?> <? if($form_subtitle): ?>
													<span><?= $form_subtitle ?></span><? endif //form_subtitle?>
											</strong></td>
									</tr>
								</table>
							<? endif //c_form_title?>

							<? /*
							////////////////////////////////////////
							Camps principals
							////////////////////////////////////////
							*/ ?>
							<div id="price_div_holder">
								<table width="100%" cellpadding="4" cellspacing="0" class="formstbl">

									<tr>
										<td width="170" valign="top" class="formsCaption">
											<?= $c_price ?>
										</td>
										<td valign="top" class="forms">
											<label class="sufix active"><?= $price ?><span> €</span></label>
										</td>
									</tr>

									<tr>
										<td width="170" valign="top" class="formsCaption">
											<?= $c_price_bought ?>
										</td>
										<td valign="top" class="forms">
											<label class="sufix active"><?= $price_bought ?><span> €</span></label>
										</td>
									</tr>
									<tr>
										<td width="170" valign="top" class="formsCaption">
											<?= $c_cadastral_price ?>
										</td>
										<td valign="top" class="forms">
											<label class="sufix active"><?= $cadastral_price ?><span> €</span></label>
										</td>
									</tr>
									<tr>
										<td width="150" valign="top" class="formsCaption">
											<?= $c_date_bought ?>
										</td>
										<td valign="top" class="forms">
											<?= $date_bought ?>
										</td>
									</tr>
									<tr>
										<td width="150" valign="top" class="formsCaption">
											<?= $c_date_sold ?>
										</td>
										<td valign="top" class="forms">
											<?= $date_sold ?>
										</td>
									</tr>
									<tr>
										<td width="150" valign="top" class="formsCaption">
											<?= $c_municipi ?>
										</td>
										<td valign="top" class="forms">
											<?= $municipi ?>
										</td>
									</tr>

								</table>
							</div>

							<? /*
							////////////////////////////////////////
							Altres camps
							////////////////////////////////////////
							*/ ?>
							<div id="other_fields">
								<table width="100%" cellpadding="4" cellspacing="0" class="formstbl">
									<tr>
										<td width="150" valign="top" class="formsCaption">
											<?= $c_other_expenses ?>
										</td>
										<td valign="top" class="forms">
											<label class="sufix active"><?= $other_expenses ?><span> €</span></label>
										</td>
									</tr>
									<tr>
										<td width="150" valign="top" class="formsCaption">
											<?= $c_mortgage_cancel ?>
										</td>
										<td valign="top" class="forms">
											<label class="sufix active chain"><?= $mortgage_cancel_percent ?><span> %</span></label>
											<label class="sufix"><?= $mortgage_cancel ?><span> €</span></label>
										</td>
									</tr>
									<tr>
										<td width="150" valign="top" class="formsCaption">
											<?= $c_estate_commission ?>
										</td>
										<td valign="top" class="forms">
											<label class="sufix active chain"><?= $estate_commission_percent ?><span> %</span></label>
											<label class="sufix"><?= $estate_commission ?><span> €</span></label>
										</td>
									</tr>
									<tr>
										<td width="150" valign="top" class="formsCaption">
											<?= $c_habitability_card ?>
										</td>
										<td valign="top" class="forms">
											<input type="checkbox" name="has_habitability_card" id="has_habitability_card"/>
											<label class="sufix"><?= $habitability_card ?><span> €</span></label>
										</td>
									</tr>
									<tr>
										<td width="150" valign="top" class="formsCaption">
											<?= $c_efficiency ?>
										</td>
										<td valign="top" class="forms">
											<input type="checkbox" name="has_efficiency" id="has_efficiency"/>
											<label class="sufix"><?= $efficiency ?><span> €</span></label>
										</td>
									</tr>
								</table>
							</div>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</form>

<? /*
////////////////////////////////////////
Report
////////////////////////////////////////
*/ ?>
<div id="report">
	<h5><?= $c_report_title_sell ?></h5>
	<table id="report_title">
		<tr>
			<th><?= $c_price ?>:</th>
			<td><span id="price"></span></td>
		</tr>
		<tr id="price_bought_holder">
			<th><?= $c_price_bought ?>:</th>
			<td><span id="price_bought"></span></td>
		</tr>

		<tr class="sep">
			<td colspan="2"></td>
		</tr>
		<tr>
			<th><?= $c_price_real ?>:</th>
			<td><span id="price_real"></span></td>
		</tr>
		<tr id="price_bought_holder">
			<th><?= $c_price_bought_real ?>:</th>
			<td><span id="price_bought_real"></span></td>
		</tr>
		<tr>
			<th><?= $c_earning ?>:</th>
			<td><span id="earning"></span></td>
		</tr>
	</table>
	<div class="report_sep"></div>

	<h6><?= $c_report_title_expenses ?></h6>
	<table>
		<tr>
			<th><?= $c_mortgage_cancel ?>:</th>
			<td id="mortgage_cancel"></td>
		</tr>
		<tr>
			<th><?= $c_estate_commission ?>:</th>
			<td id="estate_commission"></td>
		</tr>
		<tr id="habitability_card_holder">
			<th><?= $c_habitability_card ?>:</th>
			<td id="habitability_card"></td>
		</tr>
		<tr id="efficiency_holder">
			<th><?= $c_efficiency ?>:</th>
			<td id="efficiency"></td>
		</tr>
		<tr id="plusvalua_holder">
			<th><?= $c_plusvalua ?>:</th>
			<td id="plusvalua"></td>
		</tr>
		<tr id="other_expenses_holder">
			<th><?= $c_other_expenses ?>:</th>
			<td id="other_expenses"></td>
		</tr>

		<tr class="sep">
			<td colspan="2"></td>
		</tr>
		<tr class="total">
			<th><?= $c_total ?>:</th>
			<td id="total_expenses"></td>
		</tr>
	</table>
	<div class="indent report_sep"></div>

	<? /* Renda */ ?>
	<h6><?= $c_report_title_renda ?></h6>
	<table>
		<tr>
			<th><?= $c_total_days ?>:</th>
			<td id="total_days"></td>
		</tr>
		<tr class="indent">
			<th><?= $c_days_before_1996 ?>:</th>
			<td id="days_before_1996"></td>
		</tr>
		<tr class="indent">
			<th><?= $c_days_before_2006 ?>:</th>
			<td id="days_before_2006"></td>
		</tr>
		<tr class="indent">
			<th><?= $c_days_after_2006 ?>:</th>
			<td id="days_after_2006"></td>
		</tr>

		<tr class="sep">
			<td colspan="2"></td>
		</tr>
		<tr>
			<th><?= $c_earning ?>:</th>
			<td id="earning2"></td>
		</tr>
		<tr class="indent">
			<th><?= $c_earning_before_2006 ?>:</th>
			<td id="earning_before_2006"></td>
		</tr>
		<tr class="indent">
			<th><?= $c_earning_after_2006 ?>:</th>
			<td id="earning_after_2006"></td>
		</tr>

		<tr class="sep">
			<td colspan="2"></td>
		</tr>
		<tr>
			<th><?= $c_total_to_tribute ?>:</th>
			<td id="total_to_tribute"></td>
		</tr>
		<tr class="indent" id="to_tribute_before_2006_holder">
			<th><?= $c_to_tribute_before_2006 ?>:</th>
			<td id="to_tribute_before_2006"></td>
		</tr>
		<tr class="indent2" id="earning_before_2006_reductible_holder">
			<th><?= $c_earning_before_2006_reductible ?>:</th>
			<td id="earning_before_2006_reductible"></td>
		</tr>
		<tr class="indent2" id="coeficient_before_1996_holder">
			<th><?= $c_coeficient_before_1996 ?>:</th>
			<td id="coeficient_before_1996"></td>
		</tr>
		<tr class="indent" id="to_tribute_before_2006_anyway_holder">
			<th><?= $c_to_tribute_before_2006_anyway ?>:</th>
			<td id="to_tribute_before_2006_anyway"></td>
		</tr>
		<tr class="indent" id="to_tribute_after_2006_holder">
			<th><?= $c_to_tribute_after_2006 ?>:</th>
			<td id="to_tribute_after_2006"></td>
		</tr>

		<tr class="sep">
			<td colspan="2"></td>
		</tr>
		<tr>
			<th><?= $c_tribute_summary ?>:</th>
			<td></td>
		</tr>
		<tr class="indent" id="tribute_zone_1_holder">
			<th><?= $c_tribute_zone_1 ?>:</th>
			<td id="tribute_zone_1"></td>
		</tr>
		<tr class="indent" id="tribute_zone_2_holder">
			<th><?= $c_tribute_zone_2 ?>:</th>
			<td id="tribute_zone_2"></td>
		</tr>
		<tr class="indent" id="tribute_zone_3_holder">
			<th><?= $c_tribute_zone_3 ?>:</th>
			<td id="tribute_zone_3"></td>
		</tr>

		<tr class="sep">
			<td colspan="2"></td>
		</tr>
		<tr class="total">
			<th><?= $c_tribute ?>:</th>
			<td id="tribute"></td>
		</tr>
	</table>

	<div class="vinculant"><?= $c_no_vinculant ?></div>
</div>


<script type="text/javascript">
	var sell_id = <?= $id ?>
		,c_days = '<?= addslashes( strtolower( $c_days) )?>'
</script>
