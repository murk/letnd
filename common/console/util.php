<?
namespace Console;

use Db;
use Unirest\Request;

class Util {
	public static function get_server_clients( $client ) {

		self::connect_mother();
		$query = "SELECT  * FROM `letnd`.`client__client`";
		$order = "ORDER BY fiscalname ASC, domain ASC";

		if ( $client ) {
			$result = Db::get_row( "$query WHERE client_dir = %s", "clients/$client" );
			if ( $result ) {
				Db::reconnect();

				return $result;
			}
			$results = Db::get_rows( "$query WHERE client_dir LIKE %s $order", "%$client%" );
		}
		else {
			$results = Db::get_rows( "$query $order" );
		}


		$question_results = $client_results = [];
		foreach ( $results as $rs ) {
			$client = $rs['fiscalname'];
			if ( $client && $rs['domain'] ) {
				$client .= ' - ';
			}
			$client                     .= $rs['domain'];
			$question_results []        = $client;
			$client_results [ $client ] = $rs;
		}
		Db::reconnect();

		return [ 'questions' => $question_results, 'clients' => $client_results ];
	}

	public static function is_client_on_server( $client_rs, $io ) {
		if ( $client_rs ) return true;
		$io->text( "Nomes es pot executar per clients online" );

		return false;
	}


	public static function connect_mother() {
		// Get database parameters
		global $configuration;
		$dbhost  = '81.25.126.213';//$configuration['server']['db_host_mother'];
		$dbname  = $configuration['server']['db_name_mother'];
		$dbuname = $configuration['server']['db_user_name_mother'];
		$dbpass  = $configuration['server']['db_password_mother'];
		// Start connection

		Db::connect( $dbhost, $dbuname, $dbpass, $dbname );
	}


	public static function is_db( $db ) {
		if ( ! $db ) return false;
		$query = "show databases like '" . $db . "'";

		return Db::get_first( $query ) != false;
	}


	public static function connect_vagrant( $dbname ) {

		global $configuration;
		$dbhost  = '10.5.5.10';
		$dbuname = 'root';
		$dbpass  = $configuration['db_password_vagrant'];

		$cn = mysqli_connect( $dbhost, $dbuname, $dbpass, $dbname );
		if ( $cn ) {
			Db::cn( $cn );

			return true;
		}
		else {
			return false;
		}
	}



	public static function init_unirest( &$class ) {

		if ( isset ( $GLOBALS['gl_version'] ) ) {
			$version = $GLOBALS['gl_version'];
		}
		else {
			$version = Db::get_first( "SELECT value FROM webletnd.all__configadmin WHERE name = 'version';" );
		}


		$class->headers = [
			'Accept'       => 'application/json',
			'Content-Type' => 'application/json',
			'user-agent'   => 'Letnd-' . $version
		];
		Request::verifyPeer( false );
	}

}