
<? if ($loop):?>
<h3 class="titol"><?=$title?></h3>
    <table width="100%" border="0" cellspacing="0" cellpadding="10">
      <? foreach($loop as $l): extract ($l)?>
      <tr>
        <td><? if ($image_name):?>
              <a href="<?=$link?>"><img src="/<?=CLIENT_DIR?>/inmo/property/images/<?=$image_name_thumb?>" width="150" border="0" alt="<?=htmlspecialchars($property)?>" /></a>
                    <? endif //image_name?></td>
        <td width="100%" valign="top"><a href="<?=$link?>" class="menuLlnk0">
        <strong>
        <?=$ref?>
        </strong>
        <? if ($property):?><br /> 
          <?=$property?>
          <? endif //$property?>
    </a></td>
      </tr><? endforeach //$loop?>
    </table>
<? if ($more):?>
<div><a href="<?=$more_link?>" class="menuFitxa">
      &gt;&gt;
      <?=$more?>
...    </a></div>
<? endif //$more?>
<? endif //$loop?>
