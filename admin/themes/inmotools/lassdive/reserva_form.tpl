<script language="JavaScript" src="/admin/modules/product/jscripts/functions.js?v=<?= $version ?>"></script>
<script language="JavaScript" src="/admin/jscripts/validacio.js?v=<?= $version ?>"></script>

<script type="text/javascript">

	$(function () {
		order.prepared_emailed = '<?=$prepared_emailed?>';
		order.c_sending_email = <?=json_encode( $c_sending_email )?>;
		order.c_email_sended_correctly = <?=json_encode( $c_email_sended_correctly )?>;
		order.c_mail_sent = <?=json_encode( $c_mail_sent )?>;
		order.customer_language = '<?=$customer_language?>';
		order.init();
	});

</script>


<form name="form_delete" method="post" action="<?= $link_action_delete ?>" target="save_frame">
	<input name="<?= $id_field ?>" type="hidden" value="<?= $id ?>">
</form>
<form id="theForm" name="theForm" method="post" action="<?= $link_action ?>" onsubmit="<?= $js_string ?>" target="save_frame">
	<input name="order_id[<?= $id ?>]" type="hidden" value="<?= $id ?>"/>
	<input name="order_id_javascript" type="hidden" value="<?= $id ?>"/>
	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="form">
		<tr>
			<td><? if ( $form_new ): ?><? if ( $save_button ): ?>
					<div id="form_buttons_holder">
						<table id="form_buttons">
							<tr>
								<td class="buttons_top">
									<div class="right">
										<input type="submit" name="Submit" value="<?= $c_send_new ?>" class="btn_form1"/>
									</div>
								</td>
							</tr>
						</table>
					</div>
				<? endif //save_button?>
				<? endif //form_new?>
				<? if ( $form_edit ): ?>
					<div id="form_buttons_holder">
						<table id="form_buttons">
							<tr>
								<td valign="bottom">
									<div class="menu">
										<div><?= $c_edit_data ?></div><? if ( $has_images ): ?>
											<a href="<?= $image_link ?>"><?= $c_edit_images ?></a>
										<? endif //has_images?></div>
								</td>
								<td class="buttons_top">
									<div class="right">
										<? if ( $save_button ): ?>
											<input type="submit" name="Submit" value="<?= $c_send_edit ?>" class="btn_form1">
											<div class="btn_sep"></div><? endif //save_button?><? if ( $delete_button ): ?>
										<a class="btn_form_delete" href="javascript:submit_form('<? if ( $has_bin ): ?><?= $c_confirm_form_bin ?><? else: ?><?= $c_confirm_form_deleted ?><? endif //has_bin?>', true);">
											<?= $c_delete ?>
											</a><? endif //delete_button?><? if ( $preview_button ): ?>
										<a class="btn_form1" href="<?= $preview_link ?>" target="_blank">
											<?= $c_preview ?>
											</a><? endif //preview_button?><? if ( $print_button ): ?>
											<a class="btn_form1 btn_print" href="<?= $print_link ?>">
												<?= $c_print ?>
											</a> <? endif //print_button?></div>
								</td>
							</tr>
						</table>
					</div>
				<? endif //form_edit?></td>
		</tr>
		<tr>
			<td>
				<table width="100%" cellpadding="0" cellspacing="0">
					<tr>
						<td class="formCaptionTitle"><strong>
								<?= $c_order_title_admin ?></strong>&nbsp;
							<?= $c_order_num ?><?= $id ?>
							&nbsp;
						</td>
					</tr>
				</table>
				<table width="100%" cellpadding="0" cellspacing="0" class="formstbl">
					<tr>
						<td width="150" class="formsCaption" valign="top"><strong>
								<?= $c_entered_order ?>:</strong></td>
						<td valign="top" class="forms">
							<span class="padding-right-4"><?= $entered_order ?></span>
							<span id="prepared_emailed" class="hidden vert"><?= $c_mail_sent ?>
								: <?= $prepared_emailed ?></span>
							<a id="prepared_mail" class="hidden" href="javascript:order.send_mail('prepared')"><?= $c_send_mail_prepared ?></a>
							<a id="prepared_mail_re" class="hidden padding-left-2" href="javascript:order.send_mail('prepared')"><?= $c_resend_mail ?></a>
						</td>
					</tr>
					<tr>
						<td width="150" class="formsCaption" valign="top"><strong>
								<?= $c_order_status ?>:</strong></td>
						<td valign="top" class="forms"><?= $order_status ?></td>
					</tr>
					<tr>
						<td width="150" class="formsCaption" valign="top"><strong>
								<?= $c_deposit_status ?>:</strong></td>
						<td valign="top" class="forms"><?= $deposit_status ?></td>
					</tr>

					<tr>
						<td valign="top" class="formsCaption"><strong>
								<?= $c_comment ?>: </strong></td>
						<td class="forms"><?= $comment ?></td>
					</tr>
					<? if ( $customer_comment ): ?>
						<tr>
							<td valign="top" class="formsCaption"><strong>
									<?= $c_customer_comment ?>: </strong></td>
							<td class="forms"><?= $customer_comment ?></td>
						</tr>
					<? endif // $customer_comment ?>
				</table>


				<div class="sep_dark"></div>
				<table width="100%" cellpadding="4" cellspacing="0">
					<tr>
						<td class="formCaptionTitle"><strong>
								<?= $c_reserva_form_activitats ?>
							</strong></td>
					</tr>
				</table>
				<table width="100%" border="0" cellpadding="0" cellspacing="0" class="formstbl">
					<tr class="subListRecord-holder">
						<td style="padding-top:18px;padding-bottom:18px;">
							<?= $orderitems ?>
						</td>
					</tr>
				</table>


				<table class="order-totals" border="0" align="right" cellpadding="0" cellspacing="0">
					<? if ( $promcode_discount_value != '0.00' ): ?>
						<tr>
							<td>
								<?= $c_reserva_discount ?>
							</td>
							<td align="right">
								<?= $promcode_discount ?>&nbsp;<?= CURRENCY_NAME ?>
							</td>
						</tr>
					<? endif //promcode_discount?>
					<tr class="total">
						<td>
							<?= $c_all_basetax ?>:
						</td>
						<td align="right">
							<strong><?= $all_basetax ?> <?= CURRENCY_NAME ?></strong>
						</td>
					</tr>

					<? if ( $deposit != '0.00' ): ?>
						<tr>
							<td class="padding-top-2">
								<?= $c_reserva_deposit ?>
							</td>
							<td align="right" class="padding-top-2">
								<?= $deposit ?>&nbsp;<?= CURRENCY_NAME ?>
							</td>
						</tr>
					<? endif //promcode_discount?>

					<? if ($is_gift_card ): ?>

						<tr>
							<td class="gift-card" colspan="2">
								<?= $c_is_gift_card ?>
							</td>
						</tr>
					<? endif // $is_gift_card_value ?>

				</table>




				<div class="sep_dark"></div>
				<table width="100%" cellpadding="4" cellspacing="0">
					<tr>
						<td class="formCaptionTitle"><strong>
								<?= $c_order_title3 ?>
							</strong></td>
					</tr>
				</table>
				<table width="100%" border="0" cellpadding="8" cellspacing="0" class="formstbl">
					<tr>
						<td style="padding-top:18px;padding-bottom:18px;">

							<?= $address_invoice ?>

						</td>
					</tr>
				</table>
				<div class="sep_dark"></div>

				<? if( !$is_same_address ): ?>

					<table width="100%" cellpadding="4" cellspacing="0">
						<tr>
							<td class="formCaptionTitle"><strong>
									<?= $c_order_title4 ?>
								</strong></td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="8" cellspacing="0" class="formstbl">
						<tr>
							<td style="padding-top:18px;padding-bottom:18px;"><?= $address_delivery ?></td>
						</tr>
					</table>
					<div class="sep_dark"></div>

				<? endif // $address_delivery != $address_invoice ?>


				<table width="100%" cellpadding="4" cellspacing="0">
					<tr>
						<td class="formCaptionTitle"><strong>
								<?= $c_order_title5 ?>
							</strong></td>
					</tr>
				</table>


				<table width="100%" cellpadding="0" cellspacing="0" class="formstbl">
					<tr>
						<td width="150" class="formsCaption" valign="top"><strong>
								<?= $c_deposit_payment_method ?>:</strong></td>
						<td valign="top" class="forms"><?= $deposit_payment_method ?></td>
					</tr>
					<tr>
						<td width="150" class="formsCaption" valign="top"><strong>
								<?= $c_payment_method ?>:</strong></td>
						<td valign="top" class="forms"><?= $payment_method ?></td>
					</tr>
					<tr>
						<td width="150" class="formsCaption" valign="top"><strong>
								<?= $c_source_id ?>:</strong></td>
						<td valign="top" class="forms"><?= $source_id ?></td>
					</tr>
					<tr>
						<td width="150" class="formsCaption" valign="top"><strong>
								<?= $c_operator_id ?>:</strong></td>
						<td valign="top" class="forms"><?= $operator_id ?></td>
					</tr>
				</table>






				<? if ( $form_new ): ?>
					<table width="100%" cellpadding="4" cellspacing="2" class="formsButtonstbl">
						<tr>
							<td align="center" class="formsButtons">
								<input type="submit" name="Submit" value="<?= $c_send_new ?>" class="btn_form1"/></td>
						</tr>
					</table>
				<? endif //form_new?>
				<? if ( $form_edit ): ?>
					<table width="100%" cellpadding="4" cellspacing="2" class="formsButtonstbl">
						<tr>
							<td align="center" class="formsButtons">
								<input type="submit" name="Submit" value="<?= $c_send_edit ?>" class="btn_form1"/>
								&nbsp;&nbsp;<a class="btn_form_delete" href="javascript:submit_form('<?= $c_confirm_form_bin ?>', true);">
									<?= $c_delete ?>
								</a>&nbsp;
							</td>
						</tr>
					</table>
				<? endif //form_edit?></td>
		</tr>
	</table>
</form>
