<style>
	#qrcode{
		width:36px;
		height:36px;
		position:absolute;
		right:30px;
		top:6px;
		border:2px solid #fff;
	}
</style>
<script language="JavaScript" src="/admin/jscripts/validacio.js?v=<?=$version?>"></script>
<script language="JavaScript" src="/admin/jscripts/euro_calc.js?v=<?=$version?>"></script>
<script language="JavaScript" src="/common/jscripts/jquery.dropshadow.js?v=<?=$version?>"></script>
<script language="JavaScript" src="/common/jscripts/jquery.printElement.js?v=<?=$version?>"></script>
<script language="JavaScript" src="/admin/modules/product/jscripts/chained_selects.js?v=<?=$version?>"></script>
<script type="text/javascript">
	$(function() {
		set_focus(document.theForm);	
		
		$('#qrcode').click(function(){
		
			$('#qrcode').printElement(
				{
					/*iframeElementOptions:
						{
						styleToAdd:'position:absolute;width:200px;height:200px;bottom:0px;'
						},*/
					leaveOpen:true,
					printMode:'popup',
					printBodyOptions:
						{
						styleToAdd:'body{margin:0;padding:0;margin-left:<?=PRODUCT_QR_MARGIN_LEFT?>cm;margin-top:<?=PRODUCT_QR_MARGIN_TOP?>;} #qrcode{width:<?=PRODUCT_QR_WIDTH?>cm;height:<?=PRODUCT_QR_HEIGHT?>cm;}'	
						<?/*styleToAdd:'body{margin:0;padding:0;margin-left:0.2cm;;margin-top:0.1cm;} #qrcode{width:0.9cm;height:0.9cm;}'  */?>
						}
				}
			)
		
		});
		
	});
</script>
<form name="form_delete" method="post" action="<?=$link_action_delete?>" target="save_frame">
	<input name="<?=$id_field?>" type="hidden" value="<?=$id?>">
</form><form name="theForm" method="post" action="<?=$link_action?>" onsubmit="<?=$js_string?>" encType="multipart/form-data" target="save_frame">
	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="form"> 
		<tr>
			<td>
				<?if ($form_new):?>
				<div id="form_buttons_holder"><table id ="form_buttons">
						<tr> 
							<td class="buttons_top"><div class="right"><input type="submit" name="Submit" value="<?=$c_send_new?>" class="btn_form1">            </td>
									</tr>
									</table></div>
								<?endif //form_new?>
								<?if ($form_edit):?>
								<div id="form_buttons_holder"><table id ="form_buttons">
										<tr>
											<td valign="bottom"><div class="menu"><div><?=$c_edit_data?></div><?if ($has_images):?>
													<a href="<?=$image_link?>"><?=$c_edit_images?></a>
													<?endif //has_images?>
									                <? foreach($form_tabs as $r): extract( $r ) ?>
										                <? if ( $tab_action ): ?>
											                <a href="?action=<?= $tab_action ?>&<?= $id_field ?>=<?= $id ?>&menu_id=<?= $menu_id ?>"><?= $tab_caption ?></a>
									                    <? else: // ?>
															<div><?= $tab_caption ?></div>
										                <? endif // $tab_action ?>
									                <? endforeach //$form_tabs?>
												</div></td>
											<td class="buttons_top"><div class="right">
										<? if($show_previous_link): ?><a class="boto2 btn_show_previous_link"
										                          href="<?= $show_previous_link ?>"></a><? endif //show_previous_link?>
										<? if($show_next_link): ?><a class="boto2 btn_show_next_link"
										                          href="<?= $show_next_link ?>"></a><? endif //show_next_link?>
										<? if($back_button): ?><a class="btn_form_back"
										                          href="<?= $back_button ?>"></a><? endif //back_button?><input type="submit" name="Submit" value="<?=$c_send_edit?>" class="btn_form1" /><a class="btn_form_delete" href="javascript:submit_form('<?=$c_confirm_form_bin?>', true);">
														<?=$c_delete?>
													</a><a class="btn_form1" href="<?=$preview_link?>" target="_blank">
														<?=$c_preview?>
													</a></div></td>
										</tr>
									</table></div>
								<?endif //form_edit?>         
								<table width="100%" cellpadding="0" cellspacing="0" style="position:relative">
									<tr> 
										<td class="formCaptionTitle"><strong>
												<?=$c_data?>&nbsp;&nbsp;</strong><?=$product_form_title?>
												<?if ($form_edit):?>
												<a href="javascript:" title="<?=$c_print?>"><?=ProductProduct::get_qr_image($id)?></a>
												<?endif //form_edit?>
										</td>
									</tr>
								</table>
								<table width="100%" cellpadding="4" cellspacing="0" class="formstbl">
									<tr>
										<td width="150" class="formsCaption"><strong>
												<?=$c_ref?>:</strong></td>
										<td class="forms"><?=$ref?></td>
										<td width="150" class="formsCaption"><strong>
												<?=$c_ref_supplier?>:</strong></td>
										<td class="forms"><?=$ref_supplier?></td>
									</tr>
								</table>
								<table width="100%" cellpadding="0" cellspacing="0" class="formstbl">
									<tr>
										<td width="150" class="formsCaption"><strong>
												<?=$c_product_private?>:</strong></td>
										<td class="forms"><input name="product_id[<?=$product_id?>]" type="hidden" value="<?=$product_id?>" />
											<input name="product_id_javascript" type="hidden" value="<?=$product_id?>" />
											<?=$product_private?></td>
									</tr>
									<tr>
										<td valign="top" class="formsCaption"><strong>
												<?=$c_observations?>: </strong></td>
										<td class="forms"><?=$observations?></td>
									</tr>
								</table>
								<?if($is_digital_product_on):?>
								<table width="100%" cellpadding="0" cellspacing="0">
									<tr>
										<td class="formsCaptionHor"><?=$c_data_digital?></td>
									</tr>
								</table>
								<table width="100%" cellpadding="0" cellspacing="0" class="formstbl">
									<tr>
										<td width="150" valign="top" class="formsCaption"><strong><strong>
													<?=$c_download?>:</strong></strong></td>
										<td class="forms"><?=$download?></td>
									</tr>
								</table>
								<?endif //is_digital_product_on?>
								<table width="100%" cellpadding="0" cellspacing="0">
									<tr>
										<td class="formsCaptionHor"><?=$c_data_product?></td>
									</tr>
								</table>
								<table width="100%" cellpadding="0" cellspacing="0" class="formstbl">
									<tr>
										<td width="150" class="formsCaption"><strong>
												<?=$c_ean?>:</strong></td>
										<td class="forms"><?=$ean?></td>
									</tr>
									<tr>
										<td valign="top" class="formsCaption"><strong>
												<?=$c_family_id?>:</strong></td>
										<td class="forms"><?=$family_id?></td>
									</tr>
									<tr>
										<td valign="top" class="formsCaption"><strong>
												<?=$c_subfamily_id?>:</strong></td>
										<td class="forms"><?=$subfamily_id?></td>
									</tr>
									<tr>
										<td valign="top" class="formsCaption"><strong>
												<?=$c_subsubfamily_id?>:</strong></td>
										<td class="forms"><?=$subsubfamily_id?></td>
									</tr>
									<?if($is_sector_on):?>	  
									<tr>
										<td valign="top" class="formsCaption"><strong>
												<?=$c_sector_id?>:</strong></td>
										<td class="forms"><?=$sector_id?></td>
									</tr>
									<?endif //is_sector_on?>
									<? if ( $is_pvp_on ): ?>

										<tr>
											<td class="formsCaption">
												<?= $c_pvp ?> (<?= $c_is_public_tax_included ?>):
											</td>
											<td class="forms">


												<table border="0" cellspacing="0" cellpadding="0">
													<tr>
														<td style="vertical-align:middle;padding-right:20px;"><?= $pvp ?> <?= CURRENCY_NAME ?></td>
														<td style="vertical-align:middle;;padding-right:40px;"><?= $price_consult ?> <?= $c_price_consult ?></td>
														<td style="vertical-align:middle;;padding-right:5px;"><?= $c_price_cost ?></td>
														<td style="vertical-align:middle;"><?= $price_cost ?> <?= CURRENCY_NAME ?>
															(<?= $c_tax_not_included ?>)
														</td>
													</tr>
												</table>
											</td>
										</tr>

									<? endif // $is_pvp_on ?>
									<? if ( $is_pvd_on ): ?>

										<tr>
											<td class="formsCaption">
												<?= $c_pvd ?> (<?= $c_is_wholesaler_tax_included ?>):
											</td>
											<td class="forms">
												<table border="0" cellspacing="0" cellpadding="0">
													<tr>
														<td><?= $c_pvd1 ?></td>
														<td><?= $c_pvd2 ?></td>
														<td><?= $c_pvd3 ?></td>
														<td><?= $c_pvd4 ?></td>
														<td><?= $c_pvd5 ?></td>
													</tr>
													<tr>
														<td><?= $pvd ?> <?= CURRENCY_NAME ?>&nbsp;</td>
														<td><?= $pvd2 ?> <?= CURRENCY_NAME ?>&nbsp;</td>
														<td><?= $pvd3 ?> <?= CURRENCY_NAME ?>&nbsp;</td>
														<td><?= $pvd4 ?> <?= CURRENCY_NAME ?>&nbsp;</td>
														<td><?= $pvd5 ?> <?= CURRENCY_NAME ?>&nbsp;</td>
													</tr>
												</table>

											</td>
										</tr>

									<? endif // $is_pvd_on ?>
									<? if ( $is_pvp_on || $is_pvd_on ): ?>

										<tr>
											<td class="formsCaption"><strong>
													<?= $c_tax_id ?>:</strong></td>
											<td class="forms"><?= $tax_id ?></td>
										</tr>
									<? endif // $is_pvp_on ?>

									<? if ( $is_sell_on ): ?>

										<tr>
											<td class="formsCaption">
												<?= $c_quantity ?>:
											</td>
											<td class="forms">
												<table border="0" cellspacing="0" cellpadding="0">
													<tr>
														<td><?= $c_min_quantity ?></td>
														<td><?= $c_max_quantity ?></td>
														<? if ( $c_quantityconcept_id ): ?>
														<td><?= $c_quantityconcept_id ?><? endif // $c_quantityconcept_id ?>
													</tr>
													<tr>
														<td><?= $min_quantity ?>&nbsp;</td>
														<td><?= $max_quantity ?>&nbsp;</td>
														<? if ( $c_quantityconcept_id ): ?>
															<td><?= $quantityconcept_id ?>
															&nbsp;</td><? endif // $c_quantityconcept_id ?>
													</tr>
												</table>

											</td>
										</tr>

									<? endif // $is_sell_on ?>
									<tr>
										<td class="formsCaption"><strong>
												<?=$c_destacat?>:</strong></td>
										<td class="forms"><?=$destacat?></td>
									</tr>
									<tr>
										<td class="formsCaption"><strong>
												<?=$c_is_new_product?>:</strong></td>
										<td class="forms"><?=$is_new_product?></td>
									</tr>
									<tr>
										<td class="formsCaption"><strong>
												<?=$c_prominent?>:</strong></td>
										<td class="forms"><?=$prominent?></td>
									</tr>

									<? if ( $is_pvp_on || $is_pvd_on ): ?>
									<tr>
										<td class="formsCaption"><strong><strong>
													<?=$c_price_unit?>
												</strong> </strong>:</td>
										<td class="forms"><?=$price_unit?></td>
									</tr>
									<? endif // $is_pvp_on ?>
									<tr>
										<td class="formsCaption"><strong><strong>
													<?=$c_status?>
												</strong> </strong>:</td>
										<td class="forms"><?=$status?></td>
									</tr>
						<tr>
							<td class="formsCaption"><strong><strong>
										<?=$c_ordre?>:</strong></strong></td>
							<td class="forms"><?=$ordre?></td>
						</tr>
						<tr>
							<td valign="top" class="formsCaption"><strong>
									<?=$c_videoframe?>:</strong></td>
							<td class="forms"><?=$videoframe?></td>
						</tr>
						<tr>
							<td valign="top" class="formsCaption"><strong><strong>
										<?=$c_file?>:</strong></strong></td>
							<td class="forms"><?=$file?></td>
						</tr>

					</table>


					<?if($others_private):?>
					<table width="100%" cellpadding="4" cellspacing="0" class="formstbl">
						<?foreach($others_private as $l): extract ($l)?>
						<?if($other_private_conta%2==0) echo "<tr>"?>
							<td width="150" class="formsCaption">
								<strong><?=$other_private_caption?>:</strong>
							</td>

							<td class="forms">
								<?=$other_private?>
							</td>

						<?if($other_private_conta%2==1) echo "</tr>"?>
						<?endforeach //$others_private?>
						<?if($other_private_conta%2==0) echo '<td class="formsCaption">	</td><td class="forms"> </td></tr>'?>
					</table>
					<?endif //others?>

					<div class="sep_dark"></div>
					<?if($is_sell_on):?>
						<table width="100%" cellpadding="4" cellspacing="0">
							<tr>
								<td class="formCaptionTitle"><strong>
										<?=$c_data_web?>
									</strong></td>
							</tr>
						</table>
						<table width="100%" cellpadding="4" cellspacing="0" class="formstbl">
							<tr>
								<td width="150" class="formsCaption"><strong><?=$c_sell?>:</strong></td>
								<td class="forms"><?=$sell?></td>
								<td width="150" class="formsCaption"><strong><?=$c_free_send?>:</strong></td>
								<td class="forms"><?=$free_send?></td>
							</tr>
							<tr>
								<td class="formsCaption"><strong>
										<?=$c_discount?>:</strong></td>
								<td class="forms"><?=$discount?></td>
								<td class="formsCaption"><strong><?=$c_discount_fixed_price?>:</strong></td>
								<td class="forms"><?=$discount_fixed_price?></td>
							</tr>
							<tr>
								<td class="formsCaption"><strong>
										<?=$c_discount_wholesaler?>:</strong></td>
								<td class="forms"><?=$discount_wholesaler?></td>
								<td class="formsCaption"><strong><?=$c_discount_fixed_price_wholesaler?>:</strong></td>
								<td class="forms"><?=$discount_fixed_price_wholesaler?></td>
							</tr>
							<tr>
								<td class="formsCaption"><strong>
										<?=$c_sells?>:</strong></td>
								<td class="forms"><?=$sells?></td>
								<td class="formsCaption">&nbsp;</td>
								<td class="forms">&nbsp;</td>
							</tr>
						</table>
						<table width="100%" cellpadding="0" cellspacing="0">
							<tr>
								<td class="formsCaptionHor"><?=$c_stockcontrol?></td>
							<tr>
						</table>
						<table width="100%" cellpadding="4" cellspacing="0" class="formstbl">
							<td width="150" class="formsCaption"><strong>
									<?=$c_stock?>:</strong></td>
							<td class="forms"><?=$stock?></td>
							</tr>
							<td width="150" class="formsCaption"><strong>
									<?=$c_last_stock_units_public?>:</strong></td>
							<td class="forms"><?=$last_stock_units_public?></td>
							</tr>
							<td width="150" class="formsCaption"><strong>
									<?=$c_last_stock_units_admin?>:</strong></td>
							<td class="forms"><?=$last_stock_units_admin?></td>
							</tr>
						</table>
						<div class="sep_dark"></div>
					<?endif //is_sell_on?>

					<table width="100%" cellpadding="4" cellspacing="0">
						<tr> 
							<td class="formCaptionTitle"><strong><?=$c_descriptions?></strong></td>
						</tr>
					</table>
					<table cellpadding="0" cellspacing="0" class="formstbl">
						<tr> 
							<td width="150" valign="top" class="formsCaption"><strong>
									<?=$c_product_title?>:</strong></td>
							<td><table cellpadding="4" cellspacing="0"><?foreach($product_title as $l): extract ($l)?>
									<tr> 
										<td class="forms"><?=$lang?></td><td>
											<?=$product_title?> 
										</td>
									</tr>
									<?endforeach //$product_title?>
								</table></td>
						</tr>
						<tr> 
							<td width="150" valign="top" class="formsCaption"><strong>
									<?=$c_product_subtitle?>:</strong></td>
							<td><table cellpadding="4" cellspacing="0"><?foreach($product_subtitle as $l): extract ($l)?>
									<tr> 
										<td class="forms"><?=$lang?></td><td>
											<?=$product_subtitle?> 
										</td>
									</tr>
									<?endforeach //$product_title?>
								</table></td>
						</tr>
					</table>
					<table width="100%" cellpadding="0" cellspacing="0">
						<tr> 
							<td class="formsCaptionHor"><?=$c_description?></td>
						<tr> 
					</table>
					<table width="100%" cellpadding="0" cellspacing="0" class="formstbl">
						<?foreach($product_description as $l): extract ($l)?>
						<tr> 
							<td class="formsHor"><?=$lang?><br><?=$product_description?>
							</td>
						</tr>
						<?endforeach //$product_description?>
					</table>


				<? if ( $is_characteristics_on ): ?>

					<div class="sep_dark"></div>
					<table width="100%" cellpadding="0" cellspacing="0">
						<tr>
							<td class="formCaptionTitle"><strong><?= $c_caracteristics ?></strong></td>
						</tr>
					</table>
					<table width="100%" cellpadding="4" cellspacing="0" class="formstbl">
						<tr>
							<td class="formsCaption"><strong><strong>
										<?= $c_brand_id ?>
									</strong> </strong>:
							</td>
							<td class="forms"><?= $brand_id ?></td>
							<td class="formsCaption"><strong><strong>
										<?= $c_guarantee ?>
									</strong></strong>:
							</td>
							<td class="forms"><?= $guarantee ?>
								<?= $guarantee_period ?></td>
						</tr>
						<tr>
							<td width="150" class="formsCaption"><strong> <strong>
										<?= $c_year ?>
									</strong>:</strong></td>
							<td class="forms"><?= $year ?></td>
							<td width="150" class="formsCaption"><strong>
									<?= $c_format ?>:</strong></td>
							<td class="forms"><?= $format ?></td>
						</tr>
						<tr>
							<td class="formsCaption"><strong>
									<?= $c_weight ?>:</strong></td>
							<td class="forms"><?= $weight ?>
								<?= $weight_unit ?></td>
							<td class="formsCaption"><strong><?= $c_height ?>:</strong></td>
							<td class="forms"><?= $height ?>
								<?= $height_unit ?></td>
						</tr>
						<tr>
							<td class="formsCaption"><strong>
									<?= $c_temperature ?>:</strong></td>
							<td class="forms"><?= $temperature ?>
								<?= $temperature_unit ?></td>
							<td class="formsCaption"><strong>
									<?= $c_util_life ?>:</strong></td>
							<td class="forms"><?= $util_life ?>
								<?= $util_life_unit ?></td>
						</tr>
						<tr>
							<td class="formsCaption"><strong>
									<?= $c_color_id ?>:</strong></td>
							<td class="forms"><?= $color_id ?></td>
							<td class="formsCaption"><strong>
									<?= $c_material_id ?>:</strong></td>
							<td class="forms"><?= $material_id ?></td>
						</tr>
						<tr>
							<td class="formsCaption"><strong>
									<?= $c_power ?>:</strong></td>
							<td class="forms"><?= $power ?></td>
							<td class="formsCaption"><strong>
									<?= $c_alimentacion ?>:</strong></td>
							<td class="forms"><?= $alimentacion ?></td>
						</tr>
						<tr>
							<td class="formsCaption"><strong>
									<?= $c_certification ?>:</strong></td>
							<td class="forms"><?= $certification ?></td>
							<td class="formsCaption"><strong>
									<?= $c_ishandmade ?>:</strong></td>
							<td class="forms"><?= $ishandmade ?></td>
						</tr>
					</table>

				<? endif // $is_characteristics_on ?>

					<div class="sep_dark"></div>
					<table width="100%" cellpadding="0" cellspacing="0">
						<tr> 
							<td class="formCaptionTitle"><strong><?=$c_other_caracteristics?></strong></td>
						</tr>
					</table>
					
					
					<?if($others):?>
					<table width="100%" cellpadding="4" cellspacing="0" class="formstbl">
						<?foreach($others as $l): extract ($l)?>
						<?if($other_conta%2==0) echo "<tr>"?>
							<td class="formsCaption">
								<strong><?=$other_caption?>:</strong>
							</td>
							
							<td class="forms">
							
								<?if(is_array($other)):?>
								<table cellpadding="4" cellspacing="0">
									<?foreach($other as $l): extract ($l)?>
										<tr>
										<td class="forms"><?=$lang?></td>
										<td><?=$$other_name?></td>
										</tr>
										<?endforeach //$property?>
								</table>
								<?else: //?>
									<?=$other?>
								<?endif //is_array?>
								
							</td>	
							
						<?if($other_conta%2==1) echo "</tr>"?>
						<?endforeach //$others?>						
						<?if($other_conta%2==0) echo '<td class="formsCaption">	</td><td class="forms"> </td></tr>'?>
					</table>
					<?endif //others?>
					
					<div class="sep_dark"></div>
					
					
					<?if($show_seo):?>
					<table width="100%" cellpadding="0" cellspacing="0">
						<tr> 
							<td class="formCaptionTitle"><strong><?=$c_seo?></strong></td>
						</tr>
					</table>
					<table width="100%" cellpadding="0" cellspacing="0">
						<tr> 
							<td class="formsCaptionHor"><?=$c_page_title?></td>
						<tr> 
					</table>
					<table width="100%" cellpadding="0" cellspacing="0" class="formstbl">
						<?foreach($page_title as $l): extract ($l)?>
						<tr> 
							<td class="formsHor"><?=$lang?><br><?=$page_title?>
							</td>
						</tr>
						<?endforeach //$page_title?>
					</table>
					<table width="100%" cellpadding="0" cellspacing="0">
						<tr> 
							<td class="formsCaptionHor"><?=$c_page_description?></td>
						<tr> 
					</table>
					<table width="100%" cellpadding="0" cellspacing="0" class="formstbl">
						<?foreach($page_description as $l): extract ($l)?>
						<tr> 
							<td class="formsHor"><?=$lang?><br><?=$page_description?>
							</td>
						</tr>
						<?endforeach //$page_Description?>
					</table>
					<table width="100%" cellpadding="0" cellspacing="0">
						<tr> 
							<td class="formsCaptionHor"><?=$c_page_keywords?></td>
						<tr> 
					</table>
					<table width="100%" cellpadding="0" cellspacing="0" class="formstbl">
						<?foreach($page_keywords as $l): extract ($l)?>
						<tr> 
							<td class="formsHor"><?=$lang?><br><?=$page_keywords?>
							</td>
						</tr>
						<?endforeach //$page_keywords?>
					</table>	
					<table width="100%" cellpadding="0" cellspacing="0">
						<tr> 
							<td class="formsCaptionHor"><?=$c_product_file_name?></td>
						<tr> 
					</table>
					<table width="100%" cellpadding="0" cellspacing="0" class="formstbl">
						<?foreach($product_file_name as $l): extract ($l)?>
						<tr> 
							<td class="formsHor"><?=$lang?><br><?=$product_file_name?>
							</td>
						</tr>
						<?endforeach //$page_file_name?>
					</table>
					<?endif //show_seo?>
					<?if ($form_new):?>
					<table width="100%" cellpadding="4" cellspacing="2" class="formsButtonstbl">
						<tr> 
							<td align="center" class="formsButtons"> <input type="submit" name="Submit" value="<?=$c_send_new?>" class="btn_form1">                  </td>
						</tr>
					</table>
					<?endif //form_new?><?if ($form_edit):?>
					<table width="100%" cellpadding="4" cellspacing="2" class="formsButtonstbl">
						<tr> 
							<td align="center" class="formsButtons"><div class="centerFloat1">
									<div class="centerFloat2"><input type="submit" name="Submit" value="<?=$c_send_edit?>" class="btn_form1">&nbsp;&nbsp;<a class="btn_form_delete" href="javascript:submit_form('<?=$c_confirm_form_bin?>', true);">
											<?=$c_delete?>
										</a>&nbsp;<a class="btn_form1" href="<?=$preview_link?>" target="_blank">
											<?=$c_preview?>
										</a></div></div></td>
						</tr>
					</table>
					<?endif //form_edit?></td>
		</tr>
	</table></form>