<?
/**
 * ApiCurs
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2011
 * @version $Id$
 * @access public
 *
 * categories - > son l'especialitat
 * data -> start_date, end_date, horaris
 * durada -> text
 * modalitat -> text o select ?
 * Lloc -> textarea
 * Adreça
 * Tipologia -> textarea
 * Convalidacio -> textarea
 * temari -> html
 * Ponents -> Noms i descripcions de persones
 * Sistema d'avaluació
 * Preu
 * Descomptes
 * Titol
 * Inscripcions -> html
 *
 * videoframe
 *
 */
class ApiCurs extends Module{

	public function __construct(){
		parent::__construct();
	}
	public function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	public function get_records()
	{
	    $listing = new ListRecords($this);

		$listing->add_filter('status');

		$listing->order_by = 'ordre ASC, curs ASC';

	    return $listing->list_records();
	}
	public function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	public function get_form()
	{
		$this->set_field('entered','default_value',now(true));
		$this->set_field('modified','default_value',now(true));

	    $show = new ShowForm($this);

		$show->set_form_level1_titles('curs','timetable','file', 'url1_name', 'page_title');

	    return $show->show_form();

	}

	public function save_rows()
	{
	    $save_rows = new SaveRows($this);
	    $save_rows->save();
	}

	public function write_record()
	{
	    $writerec = new SaveRows($this);

		$writerec->set_field('modified','override_save_value','now()');

	    $writerec->save();
	}

	public function manage_images()
	{
	    $image_manager = new ImageManager($this);
	    $image_manager->execute();
	}
}
