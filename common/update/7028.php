<?

/* 24-8-17
 Afegir opció a newsletter de enviar només als clients amb l'idimo de la newsletter


 */

if ( is_table( 'newsletter__message' ) && ! is_field( 'newsletter__message', 'send_to_same_language_only' ) ) {

	$update_sql[] = "ALTER TABLE `newsletter__message`
	ADD COLUMN `send_to_same_language_only` TINYINT(1) NOT NULL DEFAULT '0' AFTER `language_id`,
	ADD COLUMN `send_to_no_language_too` TINYINT(1) NOT NULL DEFAULT '0' AFTER `send_to_same_language_only`;";

} else {
	if ( is_table( 'newsletter__message' ) ) {
		trigger_error( 'Ja existeixen el camp send_to_same_language_only' );
	}
}


?>