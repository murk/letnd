<?
/**
 * AdmintotalBug
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2010
 * @version $Id$
 * @access public
 */
class AdmintotalBug extends Module{
	var $condition = '',$q='';
	function __construct(){
		parent::__construct();
		if ($GLOBALS['gl_is_local'] && VERSION==4)  redirect('/admin/?menu_id=1052');
		$this->connect_mother();
		$this->order_by = 'status ASC,priority DESC,entered DESC';
	}
	function set_category_options(){
		$dir = dir(DOCUMENT_ROOT . "admin/modules");
		$this->tools = array();
		while ($dir_entry = $dir->read())
		{
			 if (!is_file($dir->path . "/" . $dir_entry) && ($dir_entry != '.') && ($dir_entry != '..'))
				{
					$this->tools[]=$dir_entry;
					$this->caption['c_category_'.$dir_entry]=$dir_entry;
				} 
		}
		$this->tools = implode(',',$this->tools);
		
		// afegeixo apartats que no son eines
		$this->tools = 'admin,' . $this->tools;
		$this->caption['c_category_admin']='ADMIN';
		$this->tools = 'public,' . $this->tools;
		$this->caption['c_category_public']='PUBLIC';
		$this->tools = 'classes,' . $this->tools;
		$this->caption['c_category_classes']='CLASSES';
		
		$this->set_field('category','select_fields',$this->tools);
		
	}
	function list_records()
	{	
		$this->set_category_options();
		$list_records = $this->get_search_form() . $this->get_records();
		$this->connect_mother();
		$show_form = $this->do_action('get_form_new');
		$GLOBALS['gl_content'] = $show_form . '<div class="sep"></div>' . $list_records;		
		Db::reconnect();
	}
	function get_records()
	{
	    $listing = new ListRecords($this);
		
		$listing->call_function['content'] = 'add_dots';
	
		if (isset($_GET['selected_menu_id'])) // no se que es ara, estava a news
		{
			$listing->condition = 'category = ' . R::text_id('category');
		}
		else
		{
		$listing->add_filter('category');
		}
		
		// search
		if ($this->condition) {
			$listing->condition = $this->condition; // ignoro condicio de dalt "categorry_id", que no se que es
		}
		
		$listing->add_filter('client_id');
		$listing->add_filter('status');
		$listing->add_filter('priority');
		$listing->add_filter('user_id');
		$listing->order_by = $this->order_by;
		$listing->list_records(false);
		$listing->parse_template(false); // així no fa el process, pero si processa els resultats
		//debug::p($listing->results);
		
		$camps_marcats = array(0,1,2,5,6);
		
		foreach ($listing->tpl->loops['loop'] as $key=>$val){
				$rs = &$listing->tpl->loops['loop'][$key];
				$rs2 = &$listing->results[$key];
				foreach ($camps_marcats as $c){
					$camp = &$rs['fields'][$c]['val'][0]['val'];
					if ($rs2['priority']=='low'){
						$camp = $this->get_low($camp);
					}
					if ($rs2['priority']=='higher'){
						$camp = $this->get_bolder($camp);
					}
					if ($rs2['priority']=='high'){
						$camp = $this->get_bold($camp);
					}
					if ($rs2['status']=='works'){
						$camp = $this->get_underline($camp);
					}
				}
			}			
	    return $listing->tpl->process();
	}
	
	

	//
	// FUNCIONS BUSCADOR
	//
	function search(){

		$q = $this->q = R::escape('q');
		$search_condition = '';

		if ($q)
		{
			// Sempre busco a tot arreu, abans si detectava que eren refrencies no hi  buscava, posaré checkbox de "buscar nomès referencies"

			$search_condition = "(
				category like '%" . $q . "%'
				OR bug like '%" . $q . "%'
				OR content like '%" . $q . "%')
				" ;

			$GLOBALS['gl_page']->title = TITLE_SEARCH . '<strong>&nbsp;&nbsp;"' . $q . '"</strong>';
		}
		$this->condition = $search_condition;
		Debug::add('Search condition', $this->condition);
		$this->do_action('list_records');
	}

	function get_search_form(){
		$tpl = new phemplate(PATH_TEMPLATES);
		$this->caption['c_by_ref']='';
		$tpl->set_vars($this->caption);
		$tpl->set_file('search.tpl');
		$tpl->set_var('menu_id',$GLOBALS['gl_menu_id']);
		$tpl->set_var('q',  htmlspecialchars(R::get('q')));
		return $tpl->process();
	}
	
	
	function show_form()
	{	
		$this->set_category_options();
		$GLOBALS['gl_content'] = $this->get_form();	
		Db::reconnect();
	}
	function get_form()
	{
		//$this->set_field('entered','default_value',now(true));
		$show = new ShowForm($this);
		$show->order_by = $this->order_by;
		$show->set_field('category','select_caption',' ');
		$show->set_field('client_id','select_caption',' ');		
	
		// poso el mateix valor al form que al filtre
		if (isset($_GET['category'])) $show->set_field('category','default_value',$_GET['category']);
		if (isset($_GET['client_id'])) $show->set_field('client_id','default_value',$_GET['client_id']);
		if (isset($_GET['status'])) $show->set_field('status','default_value',$_GET['status']);
		if (isset($_GET['priority'])) $show->set_field('priority','default_value',$_GET['priority']);
		
	    return $show->show_form();
	}

	function save_rows()
	{
	    $save_rows = new SaveRows($this);
	    $save_rows->save();	
		Db::reconnect();
		$GLOBALS['gl_reload'] = true;
	}

	function write_record()
	{	
	    $writerec = new SaveRows($this);
		$writerec->save();
		Db::reconnect();
		if ($this->acion== 'save_record') redirect('/admin/?menu_id=1062');
		else $GLOBALS['gl_reload'] = true;
	}

	function manage_images()
	{
	    $image_manager = new ImageManager($this);
	    $image_manager->execute();	
		Db::reconnect();
	}
	/**
	 * Conecta a la BBDD mare al SERVIDOR DE LETND
	 *
	 * @access public
	 */
	function connect_mother()
	{
		// Get database parameters
		global $configuration;
		$dbhost = '81.25.126.213';//$configuration['server']['db_host_mother'];
		$dbname = $configuration['server']['db_name_mother'];
		$dbuname = $configuration['server']['db_user_name_mother'];
		$dbpass = $configuration['server']['db_password_mother'];
		// Start connection
		Db::connect($dbhost, $dbuname, $dbpass, $dbname);
	}
	function get_bolder($v){
		return "<strong>$v</strong>";
	}
	function get_bold($v){
		return "<strong style='color:green'>$v</strong>";
	}
	function get_underline($v){
		return "<s style='color:#b33e3e'>$v</s>";
	
	}
	function get_low($v){
		return "<i style='color:grey'>$v</i>";
	
	}
}
?>