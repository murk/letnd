<?
$gl_caption['c_subject_booked'] = 'Reserva immoble - #';
$gl_caption['c_subject_booked_owner'] = 'Reserva inmueble - #';
$gl_caption['c_no_date_selected'] = "No s'ha seleccionat cap data";
$gl_caption['c_booked_message_1'] = "La reserva s'ha efectuat amb èxit.";
$gl_caption['c_booked_message_2'] = "A continuació entri la resta de l'informació";
$gl_caption['c_form_book_title'] = "Dades de la reserva";
$gl_caption['c_form_book_custumer_title'] = "Dades personals";
$gl_caption['c_book_finish_button'] = "Finalitzar reserva";
$gl_caption['c_book_form_title'] = "Reserva #%s";

$gl_caption['c_edit_book'] = "Editar reserva";

$gl_caption['c_mail_book_details'] = "Detalls de la reserva";
$gl_caption['c_mail_custumer_details'] = "Dades personals";
$gl_caption['c_mail_extras'] = "Extres";
$gl_caption ['c_ref']='Ref.';
$gl_caption['c_promcode_button'] = 'Aplicar';


$gl_caption_payment['c_payment_title'] = 'Efectuant la compra';
$gl_caption_payment['c_method_title1']="Esculli el metode de pagament que desitja utilitzar.";
$gl_caption_payment['c_method_title2']="L'import total és de <strong>%s&nbsp;%s</strong> IVA inclòs.";
$gl_caption_payment['c_method_title_initial']="L'import del pagament inicial és de <strong>%s&nbsp;%s</strong> IVA inclòs. S'haurà d'efectuar la resta del pagament, %s&nbsp;%s, abans de la data d'entrada";
$gl_caption_payment['c_method_title_rest']="L'import del %s pagament és de <strong>%s&nbsp;%s</strong> IVA inclòs.";
$gl_caption_payment['c_method_title_number_2']="segon";
$gl_caption_payment['c_method_title_number_3']="tercer";
$gl_caption_payment['c_method_title_number_4']="quart";

$gl_caption_payment['c_method_paypal']="Pagament amb PayPal";
$gl_caption_payment['c_method_account']="Pagament per transferència bancària";
$gl_caption_payment['c_method_lacaixa']="Targeta de crèdit";

$gl_caption_payment['c_show_paypal']="S'està efectuant el pagament a Paypal, si no comença el process automàticament faci clic aquí: ";
$gl_caption_payment['c_show_lacaixa']="S'està efectuant el pagament per targeta, si no comença el process automàticament faci clic aquí: ";

$gl_caption['c_account_title']= 'Dades de la transferència' ;
$gl_caption['c_account_amount']= 'Import' ;
$gl_caption['c_account_concept']= 'Concepte' ;

$gl_caption_book['c_payment_whole']="Pagament";
$gl_caption_book['c_payment_1']="Primer pagament";
$gl_caption_book['c_payment_2']="Segon pagament";
$gl_caption_book['c_payment_3']="Tercer pagament";
$gl_caption_book['c_payment_4']="Quart pagament";
$gl_caption_book['c_payment_paid']="Pagat";
$gl_caption_book['c_payment_pending']="Pendent";
$gl_caption_book['c_payment_pay']="Pagar";
?>