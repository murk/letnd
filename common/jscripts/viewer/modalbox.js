var ModalBox = {
	init: function(options){
		this.options = Object.extend({
			resizeDuration: 0,
			resizeTransition: Fx.Transitions.sineInOut,
			initialWidth:  window.getWidth(),
			initialHeight:  window.getHeight(),
			finalWidth: window.getWidth(),
			finalHeight: window.getHeight()
		}, options || {});

		this.eventKeyDown = this.keyboardListener.bindAsEventListener(this);
		this.eventPosition = this.position.bind(this);

		this.overlay = new Element('div').setProperty('id', 'lbOverlay').injectInside(document.body);
		
		this.capaexterior =  new Element('div').setProperty('id', 'lbContainer').injectInside(document.body);	
		
		this.center = new Element('div').setProperty('id', 'lbCenter').setStyles({width: this.options.initialWidth+'px', height: this.options.initialHeight+'px', marginLeft: '-'+(this.options.initialWidth/2)+'px', display: 'none'}).injectInside(this.capaexterior);
		this.contentContainer = new Element('div').setProperty('id', 'lbContentContainer').injectInside(this.center);
		
		this.fx = {
			overlay: this.overlay.effect('opacity', {duration: 500}).hide()
		};

	},

	open: function(boxContent){
		this.boxContent = boxContent;
		this.position();
		this.setup(true);
		this.contentContainer.setHTML(this.boxContent);
		this.top = window.getScrollTop();
		this.center.setStyles({top: this.top+'px', display: ''});
		this.fx.overlay.start(0.85);	
		this.contentContainer.style.width = this.options.finalWidth + 'px';
		this.contentContainer.style.height = this.options.finalHeight + 'px';
		
	},

	position: function(){
		this.overlay.setStyles({top: window.getScrollTop()+'px', height: window.getHeight()+'px'});
	},

	setup: function(open){
		var elements = $A(document.getElementsByTagName('object'));
		if (window.ie) elements.extend(document.getElementsByTagName('select'));
		elements.each(function(el){ el.style.visibility = open ? 'hidden' : 'visible'; });
		var fn = open ? 'addEvent' : 'removeEvent';
		window[fn]('scroll', this.eventPosition)[fn]('resize', this.eventPosition);
		document[fn]('keydown', this.eventKeyDown);
		this.step = 0;
	},

	keyboardListener: function(event){
		switch (event.keyCode){
			case 27: case 88: case 67: this.close(); break;
		}
	},

	close: function(){
		if (this.step < 0) return;
		this.step = -1;
		if (this.preload){
			this.preload.onload = Class.empty;
			this.preload = null;
		}
		for (var f in this.fx) this.fx[f].stop();
		this.center.style.display = 'none';
		this.center.setStyles({width: this.options.initialWidth+'px', height: this.options.initialHeight+'px', marginLeft: '-'+(this.options.initialWidth/2)+'px', display: 'none'});
		
		this.fx.overlay.chain(this.setup.pass(false, this)).start(0);
		//return false;
	}
};

window.addEvent('domready', ModalBox.init.bind(ModalBox));