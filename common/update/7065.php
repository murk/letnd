<?
/* 18-12-18

 Apicat import - Opció per nomes importar el primer cop i prou

 */


if ( is_table( 'inmo__configadmin' ) && ! Db::get_first( "SELECT * FROM inmo__configadmin WHERE name='apicat_import_only_first_time'" ) ) {
	$update_sql[] = "
		INSERT INTO inmo__configadmin (`name`, `value`) VALUES ('apicat_import_only_first_time', '0');";

} else {
	if ( is_table( 'inmo__configadmin' ) ) {
		trigger_error( 'Ja existeixen el camp apicat_import_only_first_time' );
	}
}