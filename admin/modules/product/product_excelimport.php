<?
/**
 * ProductExcelimport
 *
 * 
 * Importacions desde excel de clients
 * 
 * 
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 4/2013
 * @version $Id$
 * @access public
 */
class ProductExcelimport extends Module{
	var $querys = '', $cg;

	function list_records() {

		// objecte excel
		include (DOCUMENT_ROOT . 'common/includes/phpoffice/PHPExcel.php');
		$objPHPExcel = PHPExcel_IOFactory::load('D:\Letnd\clients\lateralvintage\TC.xlsx');
		$results = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
		
		Db::execute('TRUNCATE `product__family`;');
		Db::execute('TRUNCATE `product__family_language`;');
		
		Debug::p($results, 'text');
		$results = array_slice($results, 1);
		
		$families = array();
		$ids = array();
		foreach($results as $rs){
			$fam_1 = trim($rs['B']);
			$fam_2 = trim($rs['D']);
			$fam_3 = trim($rs['F']);
			$id_fam_2 = trim($rs['E']);
			$id_fam_3 = trim($rs['G']);
			
			if ($fam_2) {
				
				if (!isset($families[$fam_1][$fam_2])) {
					$families[$fam_1][$fam_2]=array();
					$ids[$fam_1][$fam_2] = $id_fam_2;
				}
				if ($fam_3){
					$families[$fam_1][$fam_2][] = $fam_3;
					$ids[$fam_1][$fam_2][$fam_3] = $id_fam_3;
				}
			}
		}
		
		/* IMPORTACIO */
		echo "<pre>";
		$conta = count($families);
		echo "$conta\n";
		foreach ($families as $familia => $subfamilies){
			
			$conta = count($subfamilies);
			echo "\t$conta -$familia\n";
			$parent_id = $this->inserta_familia($familia,0,$ids);
			
			foreach ($subfamilies as $subfamilia => $subsubfamilies){			
				
				$conta = count($subsubfamilies);
				$todo_id = isset($ids[$familia][$subfamilia])?$ids[$familia][$subfamilia]:'0';
				echo "\t\t$conta -$subfamilia\n";
				$sub_parent_id = $this->inserta_familia($subfamilia,$parent_id,$todo_id);
			
				foreach ($subsubfamilies as $subsubfamilia){			

					if ($subsubfamilia) {
						$todo_id = isset($ids[$familia][$subfamilia][$subsubfamilia])?$ids[$familia][$subfamilia][$subsubfamilia]:'0';
						echo "\t\t\t-$subsubfamilia\n";						
						$this->inserta_familia($subsubfamilia,$sub_parent_id,$todo_id);
					}

				}
			}
			
		}
		
		
		echo "</pre>";
		
		echo '<pre style="color:#0350ba">';
		// comprovo resultats insertats a les families
		$families = Db::get_rows("SELECT family, product__family.family_id as family_id, todocoleccion_id FROM product__family, product__family_language WHERE parent_id = 0 AND language = 'spa' AND product__family.family_id = product__family_language.family_id");
		
		$conta = count($families);
		echo "$conta\n";
		
		foreach ($families as $rs){
			
			$parent_id = $rs['family_id'];
			$todocoleccion_id = $rs['todocoleccion_id'];
						
			$subfamilies = Db::get_rows("SELECT family, product__family.family_id as family_id, todocoleccion_id FROM product__family, product__family_language WHERE parent_id = $parent_id AND language = 'spa' AND product__family.family_id = product__family_language.family_id");
			
			$conta = count($subfamilies);
			echo "\t$todocoleccion_id -${rs['family']}\n";
			
			foreach ($subfamilies as $rs){	
				$parent_id = $rs['family_id'];
				$todocoleccion_id = $rs['todocoleccion_id'];
				
				$subsubfamilies = Db::get_rows("SELECT family, product__family.family_id as family_id, todocoleccion_id FROM product__family, product__family_language WHERE parent_id = $parent_id AND language = 'spa' AND product__family.family_id = product__family_language.family_id");						
				$conta = count($subsubfamilies);
				echo "\t\t$todocoleccion_id -${rs['family']}\n";
			
				foreach ($subsubfamilies as $rs){	
					
					$todocoleccion_id = $rs['todocoleccion_id'];		

					if ($subsubfamilia) {
						echo "\t\t\t$todocoleccion_id -${rs['family']}\n";		
					}

				}
			}
			
		}
		
		
		echo "</pre>";
		
		die();
	}
	
	function inserta_familia($familia,$parent_id,$todo_id) {
		
		$familia = str_replace(',_','_',$familia);
		$familia = str_replace('_',' ',$familia);
		$familia = Db::qstr($familia);
		
		Db::execute("
			INSERT INTO `lateralvintage`.`product__family` 
				(`parent_id`,todocoleccion_id) VALUES 
				($parent_id,'$todo_id');");
		
		$familia_id = Db::insert_id();
		
		Db::execute(
				"INSERT INTO `lateralvintage`.`product__family_language` 
					(`family_id`, `language`, `family`) VALUES 
					($familia_id, 'spa', $familia);");
		return $familia_id;
	}
}
?>