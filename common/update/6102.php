<?

/* 3-11-16

	PRODUCT - Enviar mails


 */

if ( is_table( 'product__order' ) && ! is_field( 'product__order', 'prepared_emailed' ) ) {

	$update_sql[] = "ALTER TABLE `product__order`
	ADD COLUMN `prepared_emailed` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00' AFTER `entered_order`;";

} else {
	if ( is_table( 'product__order' ) ) {
		trigger_error( 'Ja existeixen el camp prepared_emailed' );
	}
}

?>