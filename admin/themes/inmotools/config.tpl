<script language="JavaScript" type="text/javascript" src="/admin/jscripts/validacio.js?v=<?=$version?>"></script>
<script language="JavaScript" type="text/javascript" src="/admin/jscripts/euro_calc.js?v=<?=$version?>"></script>
<script type="text/javascript">
$(function() {
	set_focus(document.theForm);
});
</script>
<form name="theForm" method="post" action="<?=$link_action?>" onsubmit="<?=$js_string?>" encType="multipart/form-data" target="save_frame">
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="form">
    <tr>
      <td>
          <div id="form_buttons_holder"><table id="form_buttons">
            <tr>
              <td class="buttons_top"><div class="right"><input type="submit" name="Submit" value="<?=$c_send_new?>" class="btn_form1" /></div>
              </td>
            </tr>
        </table></div>
          <table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td><?if ($c_form_title):?>
                <table width="100%" cellpadding="4" cellspacing="0">
                  <tr>
                    <td class="formCaptionTitle"><strong>
                      <?=$c_config_title?>
                      </strong></td>
                  </tr>
                </table>
                <?endif //c_form_title?>
              <table width="100%" cellpadding="4" cellspacing="0" class="formstbl">
                <?foreach($groups as $l): extract ($l)?>
                  <tr>
                    <td width="20%" valign="top" class="formsCaptionBig"nowrap="nowrap">
                      <?=$key?>:</td>
                    <td valign="top" class="forms"><?if ($val):?><table width="100%" border="0" cellpadding="0" cellspacing="0">
                        <?foreach($val as $l): extract ($l)?>
                          <tr>
                            <?if ($lang):?>
                              <td><span class="formsCaption"><?=$lang?></span></td>
                          </tr><tr>
                              <?endif //lang?>
                          <td><?=$val?></td>
                          </tr>
                          <?endforeach //$val?>
                      </table><?endif //val?></td>
                  </tr>						 
                <?foreach($fields as $l): extract ($l)?>
				  <tr>
                    <td width="150" valign="top" class="formsCaption" style="padding-left:50px;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td nowrap="nowrap"><?=$key?>:</td>
  </tr>
</table>
</td>
                    <td valign="top" class="forms"><?if ($val):?><table width="100%" border="0" cellpadding="0" cellspacing="0">
                        <?foreach($val as $l): extract ($l)?>
                          <tr>
                            <?if ($lang):?>
                              <td><span class="formsCaption"><?=$lang?></span></td>
                          </tr><tr>
                              <?endif //lang?>
                          <td><?=$val?></td>
                          </tr>
                          <?endforeach //$val?>
                      </table><?endif //val?></td>
                  </tr>
                  <?endforeach //$field?>
                  <?endforeach //$group?>
              </table>
                <table width="100%" cellpadding="4" cellspacing="2" class="formsButtonstbl">
                <tr>
                  <td align="center" class="formsButtons"><div class="centerFloat1">
                        <div class="centerFloat2">
                      <input type="submit" name="Submit2" value="<?=$c_send_new?>" class="btn_form1" /></div></div>
                  </td>
                </tr>
              </table></td>
          </tr>
        </table></td>
    </tr>
</table>
</form>
