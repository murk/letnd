<link href="/admin/themes/inmotools/styles/image_drag.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="/common/jscripts/source/iutil.js?v=<?=$version?>"></script>
<script type="text/javascript" src="/common/jscripts/source/iautoscroller.js?v=<?=$version?>"></script>
<script type="text/javascript" src="/common/jscripts/source/iselect.js?v=<?=$version?>"></script>
<script type="text/javascript">
		<?if ($results):?>
		<?else:?>
			image_uploader_link = '<?=$uploader_link?>';
		<?endif//results?>
</script>
<script language="JavaScript" src="/admin/jscripts/list_drag.js?v=<?=$version?>"></script>
<div id="sort" class="sort">
<?if (!$results):?>
<?if ($options_filters):?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td class="opcionsTitol"><strong><?=$c_options_bar?></strong></td>
    </tr>
    <tr>
      <td class="opcions"><table border="0" cellpadding="2" cellspacing="0">
        <tr valign="middle">
		<?foreach($options_filters as $options_filter):?>
          <td><strong><?=$options_filter?></strong></td>
		  <?endforeach //$options_filters?>
        </tr>
      </table></td>
    </tr>
</table>
<?endif //options_filters?>
<?endif //!$results?>
<?if ($results):?><?if ($split_count):?>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr valign="middle"> 
    <td width="20%" align="left" nowrap class="pageResults"><?=$split_results_count?></td>
    <td align="right" nowrap class="pageResults"><?=$split_results?></td>
  </tr>
</table>
<?endif //$split_count?>
<?if ($options_bar):?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td class="opcionsTitol"><strong><?=$c_options_bar?></strong></td>
    </tr>
    <tr>
      <td class="opcions"><table border="0" cellpadding="2" cellspacing="0">
        <tr valign="middle">
		<?foreach($options_move_tos as $options_move_to):?>
          <td><strong><?=$options_move_to?></strong></td>
		  <?endforeach //$options_move_tos?>
		<?foreach($options_filters as $options_filter):?>
          <td><strong><?=$options_filter?></strong></td>
		  <?endforeach //$options_filters?>
		<?if ($save_button):?>
          <td><strong><a class="botoOpcionsGuardar" href="javascript:submit_form_save('save_rows_save_records<?=$action_add?>')">
            <?=$c_send_edit?>
          </a></strong></td>
		  <?endif //save_button?><?if ($select_button):?>
          <td><?=$c_select?>
            &nbsp;<strong><a class="opcions" href="javascript:select_all(true, document.list)">
              <?=$c_all?>
              </a>&nbsp;-&nbsp;<a class="opcions" href="javascript:select_all(false, document.list)">
                <?=$c_nothing?>
              </a></strong></td>
		  <?endif //select_button?><?if ($delete_button):?>
          <?if ($has_bin):?>
          <?if ($is_bin):?>
          <td><strong><a class="botoOpcions" href="javascript:submit_list('save_rows_delete_selected<?=$action_add?>', '<?=$c_confirm_deleted?>', true)">
            <?=$c_delete_selected?>
          </a></strong></td>
          <td><strong><a class="botoOpcions" href="javascript:submit_list('save_rows_restore_selected<?=$action_add?>')">
            <?=$c_restore_selected?>
          </a></strong></td>
          <?else:?>
          <td><strong><a class="botoOpcionsBorrar" href="javascript:submit_list('save_rows_bin_selected<?=$action_add?>')">
            <?=$c_delete_selected?>
          </a></strong></td>
          <?endif // is_bin?>
          <?else:?>
          <td><strong><a class="botoOpcionsBorrar" href="javascript:submit_list('save_rows_delete_selected<?=$action_add?>', '<?=$c_confirm_deleted?>', true)">
            <?=$c_delete_selected?>
          </a></strong></td>
          <!--<td>
              <td>&nbsp;</td><strong><a class="botoOpcions" href="/admin?menu_id=<?=$menu_id?>&sort_by=0&sort_order=<?=$sort_order0?>">
                <?=$c_move_to?>
               
            <input type="submit" name="bin_selected" value="<?=$c_delete_selected?>" class="boto1">
              </a></strong></td>-->
          <?endif // has_bin?><?endif //delete_button?>
		  <?if ($print_button):?>
          <td><strong><a class="botoOpcions" href="<?=$print_link?>">
            <?=$c_print_all?>
          </a></strong></td>
		  <?endif //delete_button?>
        </tr>
      </table></td>
    </tr>
</table>
<?endif //$options_bar?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td height="10"></td>
  </tr>
</table>
  <form method="post" action="<?=$form_link?>" name="list" onsubmit="return check_selected(this)" target="save_frame">
  <input name="action" type="hidden" value="save_rows"><?=$out_loop_hidden?>
  <?foreach($loop as $l): extract ($l)?>
      <div class="sortableitem" id="sortableitem">
	  <table border="0" cellspacing="0" cellpadding="0" class="listRecord">
    <tr><?if ($has_images):?>
      <td valign="top" class="listImage" width="<?=$im_admin_thumb_w?>">	  
	  <?if ($image_name):?><div><img src="<?=$image_src_admin_thumb?>"></div>
	  <?else: // image_name?><div class="noimage" style="min-height:<?=$im_admin_thumb_h?>px;"></div>
	  <?endif // image_name?>	  
	  </td>
      <?endif // has_images?>
       <?if ($options_checkboxes):?> 
      <td width="30" valign="top"><input name="selected[<?=$conta?>]" type="checkbox" value="<?=$id?>" onclick="return false;" /></td><?endif // options_checkboxes?>
      
<?foreach($fields as $l): extract ($l)?>
      <td valign="top">           
                  <table width="100%" border="0" cellpadding="0" cellspacing="0">
						<?foreach($val as $l): extract ($l)?>
                      <?if ($lang):?><tr>
                            <td colspan="2"><span class="formsCaption">
                            <?=$lang?>
                            </span></td>
						</tr>
                            <?endif //lang?><tr>
                        <td>&nbsp;</td><td width="100%"><?=$val?></td>
                      </tr>
						<?endforeach //$val?>
            </table></td><?endforeach //$field?></tr>
</table>
	  </div>  
<?endforeach //$loop?>
  </form>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td height="10"></td>
  </tr>
</table> <?if ($split_count):?>
  <table width="100%" border="0" cellpadding="0" cellspacing="0">
    <tr valign="middle">
      <td width="20%" align="left" nowrap class="pageResults"><?=$split_results_count?></td>
      <td align="right" nowrap class="pageResults"><?=$split_results?></td>
    </tr>
  </table><?endif //$split_count?>
  <?if ($options_bar):?>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td class="opcionsTitol"><strong>
          <?=$c_options_bar?>
        </strong></td>
      </tr>
      <tr>
        <td class="opcionsLast"><table border="0" cellpadding="2" cellspacing="0">
            <tr valign="middle">
              <?foreach($options_move_tos as $options_move_to):?>
              <td><strong>
                <?=$options_move_to?>
              </strong></td>
              <?endforeach //$options_move_tos?>
              <?foreach($options_filters as $options_filter):?>
              <td><strong>
                <?=$options_filter?>
              </strong></td>
              <?endforeach //$options_filters?>
              <?if ($save_button):?>
              <td><strong><a class="botoOpcionsGuardar" href="javascript:submit_form_save('save_rows_save_records<?=$action_add?>')">
                <?=$c_send_edit?>
              </a></strong></td>
              <?endif //save_button?>
              <?if ($select_button):?>
              <td><?=$c_select?>
                &nbsp;<strong><a class="opcions" href="javascript:select_all(true, document.list)">
                  <?=$c_all?>
                  </a>&nbsp;-&nbsp;<a class="opcions" href="javascript:select_all(false, document.list)">
                    <?=$c_nothing?>
                  </a></strong></td>
              <?endif //select_button?>
              <?if ($delete_button):?>
              <?if ($has_bin):?>
              <?if ($is_bin):?>
              <td><strong><a class="botoOpcions" href="javascript:submit_list('save_rows_delete_selected<?=$action_add?>', '<?=$c_confirm_deleted?>', true)">
                <?=$c_delete_selected?>
              </a></strong></td>
              <td><strong><a class="botoOpcions" href="javascript:submit_list('save_rows_restore_selected<?=$action_add?>')">
                <?=$c_restore_selected?>
              </a></strong></td>
              <?else:?>
              <td><strong><a class="botoOpcionsBorrar" href="javascript:submit_list('save_rows_bin_selected<?=$action_add?>')">
                <?=$c_delete_selected?>
              </a></strong></td>
              <?endif // is_bin?>
              <?else:?>
              <td><strong><a class="botoOpcionsBorrar" href="javascript:submit_list('save_rows_delete_selected<?=$action_add?>', '<?=$c_confirm_deleted?>', true)">
                <?=$c_delete_selected?>
              </a></strong></td>
              <!--<td>
              <td>&nbsp;</td><strong><a class="botoOpcions" href="/admin?menu_id=<?=$menu_id?>&sort_by=0&sort_order=<?=$sort_order0?>">
                <?=$c_move_to?>
               
            <input type="submit" name="bin_selected" value="<?=$c_delete_selected?>" class="boto1">
              </a></strong></td>-->
              <?endif // has_bin?>
              <?endif //delete_button?>
              <?if ($print_button):?>
              <td><strong><a class="botoOpcions" href="<?=$print_link?>">
                <?=$c_print_all?>
              </a></strong></td>
              <?endif //delete_button?>
            </tr>
        </table></td>
      </tr>
    </table>
    <?endif //$options_bar?>
<?endif //results?>
</div>