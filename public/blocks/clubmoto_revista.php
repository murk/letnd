<?php
$query = "SELECT clubmoto__pagina.pagina_id as pagina_id, pagina
				FROM clubmoto__pagina_language, clubmoto__pagina
				WHERE clubmoto__pagina_language.pagina_id = clubmoto__pagina.pagina_id
				".$GLOBALS['gl_clubmoto_status']."
				AND level = 1
				AND bin <> 1
				AND language = '" . LANGUAGE . "'
				ORDER BY ordre ASC, pagina desc";

$results = Db::get_rows($query);
Debug::add('Results clubmoto_revista block',$results);

if (!$results) {
	$show_block = false;
	return;
}

// busco la revista que estem mirant
$selected_id = false;

$pagina_id = R::id('pagina_id');

if ($pagina_id){
	$selected_id = Db::get_first("SELECT parent_id FROM clubmoto__pagina
				WHERE pagina_id = '" . $pagina_id . "'");
}
// home, no faig else perque pot ser que get pagina no sigui bo
if (!$selected_id){
	$selected_id = $results[0]['pagina_id'];
}

if ($results)
{
	$conta = 0;
	$loop_count = count($results);
    foreach($results as $rs)
    {
        ($selected_id == $rs["pagina_id"])?$selected = 1:$selected = 0;
		
		$first_apartat = clubmoto_get_first_apartat($rs['pagina_id']);
		if ($first_apartat) {
			$l['link'] = clubmoto_get_pagina_link($first_apartat);
			$l['selected'] = $selected;
			$l['item'] = $rs['pagina'];
			$l['id'] = $rs['pagina_id'];
			$l['conta'] = $conta++;
			$l['class'] = '';
			$l['loop_count'] = $loop_count;
			$loop[] = $l;
		}
		
		// la revista seleccionada per tindre-la a ma
		if ($selected) $vars['selected_revista'] = $rs['pagina'];
    }
	$loop[0]['class'] = 'first';	
	$loop[count($loop)-1]['class'] = 'last';
}

?>