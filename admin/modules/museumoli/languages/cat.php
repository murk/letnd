<?php
// menus eina
if (!defined('MUSEUMOLI_MENU_VISITA')){
	define('MUSEUMOLI_MENU_VISITA','Visites');
	define('MUSEUMOLI_MENU_VISITA_NEW','Nova visita');
	define('MUSEUMOLI_MENU_VISITA_LIST','Llistar visites');
	define('MUSEUMOLI_MENU_VISITA_LIST_BIN','Paperera de reciclatge');
	
	define('MUSEUMOLI_MENU_VISITATALLER','Visita-taller');
	define('MUSEUMOLI_MENU_VISITATALLER_NEW','Nova visita-taller');
	define('MUSEUMOLI_MENU_VISITATALLER_LIST','Llistar visites-taller');
	define('MUSEUMOLI_MENU_VISITATALLER_LIST_BIN','Paperera de reciclatge');
	
	define('MUSEUMOLI_MENU_VISITANT','Tipus de visitants');
	define('MUSEUMOLI_MENU_VISITANT_NEW','Nou tipus de visitant');
	define('MUSEUMOLI_MENU_VISITANT_LIST','Llistar tipus visitants');
	define('MUSEUMOLI_MENU_VISITANT_LIST_BIN','Paperera de reciclatge');
	
	define('MUSEUMOLI_MENU_CURS','Cursos');
	define('MUSEUMOLI_MENU_CURS_NEW','Nou curs');
	define('MUSEUMOLI_MENU_CURS_LIST','Llistar cursos');
	define('MUSEUMOLI_MENU_CURS_LIST_BIN','Paperera de reciclatge');
	
	define('MUSEUMOLI_MENU_DOCUMENTACIOCATEGORY_NEW','Nova categoria');
	define('MUSEUMOLI_MENU_DOCUMENTACIOCATEGORY_LIST','Llistar categories');
	define('MUSEUMOLI_MENU_DOCUMENTACIOCATEGORY_LIST_BIN','Paperera de reciclatge');
	
	define('MUSEUMOLI_MENU_DOCUMENTACIO','Documentació');
	define('MUSEUMOLI_MENU_DOCUMENTACIO_NEW','Nou document');
	define('MUSEUMOLI_MENU_DOCUMENTACIO_LIST','Llistar documents');
	define('MUSEUMOLI_MENU_DOCUMENTACIO_LIST_BIN','Paperera de reciclatge');
	
	define('MUSEUMOLI_MENU_COLABORADOR','Colaboradors');
	define('MUSEUMOLI_MENU_COLABORADOR_NEW','Nou colaborador');
	define('MUSEUMOLI_MENU_COLABORADOR_LIST','Llistar colaboradors');
	define('MUSEUMOLI_MENU_COLABORADOR_LIST_BIN','Paperera de reciclatge');
	
	define('MUSEUMOLI_MENU_EXPOSICIOCATEGORY_NEW','Nova categoria');
	define('MUSEUMOLI_MENU_EXPOSICIOCATEGORY_LIST','Llistar categories');
	define('MUSEUMOLI_MENU_EXPOSICIOCATEGORY_LIST_BIN','Paperera de reciclatge');
	
	define('MUSEUMOLI_MENU_EXPOSICIO','Exposicions');
	define('MUSEUMOLI_MENU_EXPOSICIO_NEW','Nova exposició');
	define('MUSEUMOLI_MENU_EXPOSICIO_LIST','Llistar exposicions');
	define('MUSEUMOLI_MENU_EXPOSICIO_LIST_BIN','Paperera de reciclatge');
	
	define('MUSEUMOLI_MENU_SENTENCE','Frases Home');
	define('MUSEUMOLI_MENU_SENTENCE_NEW','Nova frase');
	define('MUSEUMOLI_MENU_SENTENCE_LIST','Llistar frases');
	define('MUSEUMOLI_MENU_SENTENCE_LIST_BIN','Paperera');
	
	define('MUSEUMOLI_MENU_NEW', 'Notícies');
	define('MUSEUMOLI_MENU_NEW_NEW', 'Entrar nova notícia');
	define('MUSEUMOLI_MENU_NEW_LIST', 'Llistar i editar notícies');
	define('MUSEUMOLI_MENU_NEW_BIN', 'Paperera de reciclatge');
	define('MUSEUMOLI_MENU_CATEGORY', 'Categories');
	define('MUSEUMOLI_MENU_CATEGORY_NEW', 'Entrar nova categoria');
	define('MUSEUMOLI_MENU_CATEGORY_LIST', 'Llistar categories');
}

$gl_messages['no_delete_taller']='El registre 12 i 19 no es poden esborrar, pertanyen a "Visites+taller" i "Cursos i seminaris"';

// captions seccio
$gl_caption['c_visitant_id'] = '';
$gl_caption['c_videoframe'] = 'Vídeo';
$gl_caption['c_preu'] = 'Preu per persona';
$gl_caption['c_preu_alumne'] = 'Preu per alumne';
$gl_caption['c_url1'] = 'URL 1';
$gl_caption['c_url2'] = 'URL 2';
$gl_caption['c_status'] = 'Estat';
$gl_caption['c_parent_module'] = '';
$gl_caption['c_entered'] = 'Creat';
$gl_caption['c_ordre'] = 'Ordre';
$gl_caption['c_bin'] = '';
$gl_caption['c_visitant_id'] = 'Tipus de visitant';
$gl_caption['c_visitant'] = 'Tipus de visitant';
$gl_caption['c_visita'] = 'Visita';
$gl_caption_exposicio['c_visita'] = 'Exposició';
$gl_caption_curs['c_visita'] = 'Curs';
$gl_caption_colaborador['c_visita'] = 'Nom colaborador';
$gl_caption['c_datatext'] = 'Horari';
$gl_caption['c_year'] = 'Any';
$gl_caption_exposicio['c_datatext'] = 'Inauguració';
$gl_caption['c_mes_info'] = 'Informació general';
$gl_caption_curs['c_mes_info'] = 'Programa';
$gl_caption_colaborador['c_mes_info'] = 'Altra informació';
$gl_caption['c_reserva'] = 'Reserves';
$gl_caption['c_documentacio'] = 'Documentació';
$gl_caption['c_nivell_educatiu'] = 'Nivell educatiu';
$gl_caption['c_duracio'] = 'Durada';
$gl_caption['c_inscripcio'] = 'Inscripció';
$gl_caption['c_tipus_curs'] = 'Tipus';
$gl_caption['c_tipus_curs_curs'] = 'Cursos i tallers';
$gl_caption['c_tipus_curs_seminari'] = 'Seminaris';
$gl_caption['c_tipus_curs_moli'] = 'Molí - Art paper';
$gl_caption['c_tipus_colaborador'] = 'Tipus';
$gl_caption['c_tipus_colaborador_artista'] = 'Artistes';
$gl_caption['c_tipus_colaborador_entitat'] = 'Entitats';
$gl_caption['c_tipus_colaborador_empresa'] = 'Empreses i institucions';


$gl_caption['c_prepare'] = 'En preparació';
$gl_caption['c_review'] = 'Per revisar';
$gl_caption['c_public'] = 'Públic';
$gl_caption['c_archived'] = 'Arxivat';
$gl_caption['c_new'] = 'Notícia';
$gl_caption['c_title'] = 'Títol';
$gl_caption['c_subtitle'] = 'Subtítol';
$gl_caption['c_content'] = 'Descripció a la fitxa';
$gl_caption['c_status'] = 'Estat';
$gl_caption['c_category_id'] = 'Categoria';
$gl_caption['c_filter_category_id'] = 'Totes les categories';
$gl_caption['c_filter_status'] = 'Tots els estats';
$gl_caption['c_ordre'] = 'Ordre';


$gl_caption['c_category_id'] = 'Categoria';
$gl_caption['c_ordre'] = 'Ordre';
$gl_caption_image['c_name'] = 'Nom';
$gl_caption['c_url1_name']='Nom URL(1)';
$gl_caption['c_url1']='URL(1)';
$gl_caption['c_url2_name']='Nom URL(2)';
$gl_caption['c_url2']='URL(2)';
$gl_caption['c_videoframe']='Vídeo (youtube, metacafe...)';
$gl_caption['c_file']='Documentació';
$gl_caption_curs['c_file_download']='Descarregar programa';
$gl_caption['c_in_home']='Surt a la home';
$gl_caption['c_content_list']='Descripció a l\'index de visites';

$gl_caption['c_sentence_title']='Títol';
$gl_caption['c_sentence']='Frase';
$gl_caption['c_sentence_url_name']='Nom d l\'enllaç';
$gl_caption['c_sentence_url']='Enllaç';

$gl_caption['c_mes_informacio']='Més informació';
?>
