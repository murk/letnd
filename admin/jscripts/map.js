var gl_map;  
var gl_geocoder;
var gl_geocoder_results = false;
var gl_marker;
var gl_latlng;
var gl_infowindow;

$(function(){

	$('#show_map').change( function(){
		var show_map = $('#show_map').prop('checked');	
		$('#show_map_point').prop('disabled',!show_map)
	});
	
	
});

function loadMap() { 
	if (typeof(gl_center_latitude)=='undefined') gl_center_latitude = gl_latitude;
	if (typeof(gl_center_longitude)=='undefined') gl_center_longitude = gl_longitude;
    gl_geocoder = new google.maps.Geocoder();	
    gl_latlng = new google.maps.LatLng(gl_latitude , gl_longitude);
    gl_latlng_center = new google.maps.LatLng(gl_center_latitude , gl_center_longitude);
	
    var myOptions = { //opcions, comm el zoom, centrat, tipus de mapa
	  scrollwheel: false,
      zoom: gl_zoom,
      center: gl_latlng_center,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
	  mapTypeControlOptions: {style: google.maps.MapTypeControlStyle.DROPDOWN_MENU}
    }
    gl_map = new google.maps.Map(document.getElementById("google_map"), myOptions);
	
	//si  no hi ha punt guardat, intenta buscar automaticament el punt
	if (gl_is_default_point){  
		search_point();	
	}
	
	
	gl_marker = new google.maps.Marker({
		position: gl_latlng, 
		map: gl_map
	}); 
	
	if (gl_geocoder_results){
		mostra_info();		
	}
	
	google.maps.event.addListener(gl_map, 'click', function(event) {guarda_coordenades(event)});
	google.maps.event.addListener(gl_map, 'center_changed', function(event) {guarda_centrat(this)});
}

function search_point() {
	var address = document.getElementById("map_address").value;
	if (!address) return;
	gl_geocoder.geocode({'address': address}, function (results, status) {
		if (status == google.maps.GeocoderStatus.OK) {
			gl_geocoder_results = true;
			gl_map.setZoom(14); //canvio el zoom
			gl_map.setCenter(results[0].geometry.location);
			gl_latlng = results[0].geometry.location;

			if (gl_marker) {
				guarda_coordenades(false);
				mostra_info();
			}

		} else {
			showPopWin('', 300, 100, null, false, false, MAP_FRAME_TEXT_4);
			window.setTimeout("hidePopWin(false);", 800)
		}
	});
	return false;
}	
	
function guarda_coordenades(event){
	
	if (gl_infowindow) gl_infowindow.close();
	point = event?event.latLng:gl_latlng;
	
	gl_marker.setOptions({
			position: point
		});
	gl_map.panTo(point);

	gl_latitude = point.lat();
	gl_longitude = point.lng();
	$('#latitut').html(gl_latitude);
	$('#longitut').html(gl_longitude);
} 
function guarda_centrat(obj){
	gl_center_latitude =  obj.getCenter().lat();
	gl_center_longitude = obj.getCenter().lng();
	$('#center_latitut').html(gl_center_latitude);
	$('#center_longitut').html(gl_center_longitude);	
}

function mostra_info(){
	gl_infowindow = new google.maps.InfoWindow({ 
		content: '<strong>'+MAP_FRAME_TEXT_1+':</strong><br>' + document.getElementById("map_address").value + '<br><br>'+MAP_FRAME_TEXT_2+' <a href="javascript:save_map()">'+MAP_FRAME_TEXT_3+'</a>' 
	});
	gl_infowindow.open(gl_map,gl_marker);
	gl_geocoder_results = false;
}
  
function save_map() {
  if (gl_infowindow) gl_infowindow.close();
  var zoom_actual = gl_map.getZoom(); 
  
  var show_map = $('#show_map').prop('checked')?1:0;
  var show_map_point = $('#show_map_point').prop('checked')?1:0;
  
  showPopWin('', 300, 100, null, false, false, MODALBOX_SAVE);
  window.save_frame.location.href = '/admin/?action=save_record_map'+
					'&show_map='+show_map+
					'&show_map_point='+show_map_point+
					'&menu_id='+gl_menu_id+
					'&tool='+gl_tool+
					'&tool_section='+gl_tool_section+
					'&'+gl_id_field+'='+gl_id+
					'&zoom='+zoom_actual+
					'&latitude='+gl_latitude+
					'&longitude='+gl_longitude+
					'&center_latitude='+gl_center_latitude+
					'&center_longitude='+gl_center_longitude;
}
$(document).ready(
	function ()
		{
			loadMap();
		}
)  