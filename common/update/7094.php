<?
/*
11-10-2019

*/

if ( is_table( 'product__orderitem' ) && ! is_field( 'product__orderitem', 'product_old_base' ) ) {

	$update_sql[] = "ALTER TABLE `product__orderitem`
	ADD COLUMN `product_old_base` DECIMAL(11,2) NOT NULL AFTER `product_base`,
	ADD COLUMN `product_old_basetax` DECIMAL(11,2) NOT NULL AFTER `product_basetax`,
	ADD COLUMN `product_discount` DECIMAL(11,2) NOT NULL AFTER `product_old_basetax`,
	ADD COLUMN `product_discount_fixed` TINYINT(1) NOT NULL DEFAULT 0 AFTER `product_discount`;
";

}
else {
	if ( is_table( 'product__orderitem' ) ) {
		trigger_error( 'Ja existeixen el camp product_old_base' );
	}
}