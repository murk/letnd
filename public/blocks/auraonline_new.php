<?php
global $gl_is_home;

$category_id = R::id('category_id');

/*************************
si no hi ha category_id, no mostro el block
*************************/
if (!$category_id){	
	$show_block = false;
	return;
}

$query = "SELECT news__new.new_id as new_id, new, subtitle
				FROM news__new_language, news__new
				WHERE news__new_language.new_id = news__new.new_id
				AND status = 'public'
				AND bin <> 1
				AND category_id = '" . $category_id . "'
				AND language = '" . LANGUAGE . "'
				ORDER BY entered DESC, new_id";

$results = Db::get_rows($query);
Debug::add('Results block: news_new',$results);
$selected_found = false;
if ($results)
{
	$conta = 0;
	$loop_count = count($results);
    foreach($results as $rs)
    {
        if (isset($_GET["new_id"]) && ($_GET["new_id"] == $rs["new_id"]))
		{
			$selected = 1;
			$selected_found = true;
		}
		else
		{
			$selected = 0;
		}
        
        
		if (USE_FRIENDLY_URL){
			$l['link'] = '/' . LANGUAGE . '/news/new/' . $rs['new_id'] . '/category_id/' . $category_id;
			if (isset($_GET['page_file_name'])) $l['link'] .= '/' . $_GET['page_file_name'] . '.html';
		}
		else{
			$l['link'] = '/?tool=news&tool_section=new&new_id=' . $rs['new_id'] . '&language=' . LANGUAGE;
			if (isset($_GET['page_id'])) $l['link'] .= '&page_id=' . $rs['page_id'];
		}
        $l['selected'] = $selected;
        $l['item'] = $rs['new'];
		$l['id'] = $rs['new_id'];
		$l['conta'] = $conta++;
		$l['class'] = '';
		$l['loop_count'] = $loop_count;
        $loop2[] = $l;
    }
	$loop2[0]['class'] = 'first';	
	$loop2[count($loop2)-1]['class'] = 'last';
	if (!$selected_found) $loop2[0]['selected'] = 1;
    $loop[] = array ('loop' => $loop2,
        'item' => '');
}

?>