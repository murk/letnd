<?
define("MENU_HOME_PUBLIC","Web");
define("MENU_HOME","Page d\'accueil");
define("MENU_LOGOUT","Se déconnecter");


// configurable
define("USER_TEXT","utilisateur");
define('USER_LOGIN_INVALID','Le nom ou mot de passe ne sont pas valides');
define('USER_LOGIN_VALID','Bienvenue à ' . HOST_URL);

// la linea de sota no es pot tocar, que te dit que no la toquis!!!!!!!
// menus
define('EXPERIENCIA_MENU_ROOT','Expérience');
define('NEWSLETTER_MENU_ROOT','Lettre d\'infos');
define('TRANSLATION_MENU_ROOT','Traductions');
define('FACTURACIO_MENU_ROOT','Facturation');
define('STATISTICS_MENU_ROOT','Statistiques');
define('USER_MENU_ROOT','Utilisateurs');
define('ADMINTOTAL_MENU_ROOT','Admin total');
define('WEB_MENU_ROOT','Gestion Letnd');
define('INMO_MENU_ROOT','Biens');
define('OWNER_MENU_ROOT','Propriétaires');
define('REPORT_MENU_ROOT','Documents');
define('CONFIG_MENU_ROOT','Configuration');
define('NEWS_MENU_ROOT','Infos');
define('GALERY_MENU_ROOT','Galerie de photos');
define('RENOVATS_MENU_ROOT','Cartes-cadeau');
define('SHOPWINDOW_MENU_ROOT','Vitrines');
define('UTILITY_MENU_ROOT','Utilitaires');
define ('MLS_MENU_ROOT','MLS');
define('CUSTUMER_MENU_ROOT','Clients');
define('WORKS_MENU_ROOT','Tâches');
define('PRODUCT_MENU_ROOT','Produits');
define('RATE_MENU_ROOT','Tarifs');
define('CARS_MENU_ROOT','Voitures');
define('NEW_MENU_ROOT','Nouvelles');
define('PORTFOLI_MENU_ROOT','Portefeuille');
define('CONTACT_MENU_ROOT','Contacter');
define('PRINT_MENU_ROOT','Documents');
define('BOOKING_MENU_ROOT','Réservations');
define('MISSATGE_MENU_ROOT','Messages');
define('CUSTOMER_MENU_ROOT','Clients et commandes');
define('DIRECTORY_MENU_ROOT','Archives');
define('SELLPOINT_MENU_ROOT','Points de vente');



define('DB_SPLIT_TITLE_FIRST_PAGE', 'Début');
define('DB_SPLIT_TITLE_PREVIOUS_PAGE', 'Page précédente');
define('DB_SPLIT_TITLE_NEXT_PAGE', 'Page suivante');
define('DB_SPLIT_TITLE_LAST_PAGE', 'Fin');
define('DB_SPLIT_TITLE_PAGE_NO', 'Page %d');
define('DB_SPLIT_TITLE_PREV_SET_OF_NO_PAGE', '%d pages précédentes');
define('DB_SPLIT_TITLE_NEXT_SET_OF_NO_PAGE', '%d pages suivantes');
define('DB_SPLIT_BUTTON_FIRST', '&lt;&lt;DÉBUT');
define('DB_SPLIT_BUTTON_PREV', '&lt;&lt;&nbsp;Précédent');
define('DB_SPLIT_BUTTON_NEXT', 'Suivant&nbsp;&gt;&gt;');
define('DB_SPLIT_BUTTON_LAST', 'FIN&gt;&gt;');
define('DB_SPLIT_DISPLAY_NUMBER_OF_RESULTS', 'Résultats <b>%d</b> à <b>%d</b> affichés (sur <b>%d</b>)');
define ('TITLE_SEARCH','Résultats de la recherche');

define('DB_SPLIT_PAGE_TITLE', 'Page');
define('DB_SPLIT_PAGE_DESCRIPTION', 'Résultats %d à %d sur %d');

$month_names = array("Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre");
$week_names = array("Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi");


// Caption default
$gl_caption['c_form_title'] = 'Modifier les données';
$gl_caption['c_print_title'] = 'Données';
$gl_caption['c_config_title'] = 'Modifier la configuration';
$gl_caption['c_edit'] = 'Modifier';


$gl_caption['c_edit_button'] = 'Modifier les données';
$gl_caption['c_view_button'] = 'Afficher les données';
$gl_caption['c_edit_images_button'] = 'Modifier les photos';
$gl_caption['c_add_images_button'] = 'Ajouter des photos';



$gl_caption['c_edit_data'] = 'Modifier les données';
$gl_caption['c_edit_images'] = 'Modifier les photos';
$gl_caption['c_add_images'] = 'Ajouter des photos';
$gl_caption['c_search_form'] = 'Rechercher';
$gl_caption['c_send_new'] = 'Enregistrer';
$gl_caption['c_send_edit'] = 'Enregistrer les modifications';
$gl_caption['c_delete'] = 'Effacer';
$gl_caption['c_action'] = 'Action';
$gl_caption['c_delete_selected'] = 'Effacer les éléments sélectionnés';
$gl_caption['c_restore_selected'] = 'Récupérer les éléments sélectionnés';
$gl_caption['c_confirm_deleted'] = 'Les éléments sélectionnés vont être définitivement effacés de la base de données. \\n\\nVoulez-vous continuer ?';
$gl_caption['c_confirm_images_deleted'] = 'Les photos sélectionnées vont être définitivement effacées.\\n\\nVoulez-vous continuer ?';
$gl_caption['c_confirm_form_bin'] = 'Le fichier va être envoyé à la corbeille. \\n\\nVoulez-vous continuer ?';
$gl_caption['c_confirm_form_deleted'] = 'Le fichier va être définitivement effacé de la base de données. \\n\\nVoulez-vous continuer ?';
$gl_caption['c_move_to'] = 'Déplacer vers :';
$gl_caption['c_print_all'] = 'Tout imprimer';
$gl_caption['c_print'] = 'Imprimer';
$gl_caption['c_order_by'] = 'Classer par :';
$gl_caption['c_select'] = 'Sélectionner';
$gl_caption['c_multislect_selectable'] = 'Champs';
$gl_caption['c_multislect_selected'] = 'Champs  sélectionnées';
$gl_caption['c_all'] = 'tout';
$gl_caption['c_nothing'] = 'rien';
$gl_caption['c_preview'] = 'Prévisualiser';
$gl_caption['c_yes'] = 'Oui';
$gl_caption['c_no'] = 'Non';
$gl_caption['c_options_bar'] = 'Options';
$gl_caption['c_file_maximum'] = 'maximum %s fichiers à la fois';
$gl_caption['c_file_maximum_1'] = 'maximum 1 fichier à la fois';

// buscadors
$gl_caption['c_search_title'] = 'Rechercher';
$gl_caption['c_by_ref'] = '(ou références séparées par une espace, exemple : "456 245 45")';
$gl_caption['c_by_words'] = 'Par mots';

// sobreescrits caption images
$gl_caption_image['c_edit'] = 'Photos de';
$gl_caption_image['c_insert'] = 'Ajouter des photos de votre ordinateur';
$gl_caption_image['c_insert_advanced'] = 'Avancé (java)';
$gl_caption_image['c_delete_selected'] = 'Éliminer les photos sélectionnées';
$gl_caption_image['c_confirm_deleted'] = 'Les photos vont être définitivement effacées.\\n\\n\\nVoulez-vous continuer ?';
$gl_caption_image['c_select_all'] = 'Tout sélectionner';
$gl_caption_image['c_deselect_all'] = 'Tout désélectionner';

// nous images
$gl_caption_image['c_stop'] = 'Arrêter';
$gl_caption_image['c_refresh'] = 'Actualiser';
$gl_caption_image['c_add'] = 'Ajouter des photos';
$gl_caption_image['c_thumbnails'] = 'Photos';
$gl_caption_image['c_icons'] = 'Icônes';
$gl_caption_image['c_list'] = 'Liste';
$gl_caption_image['c_details'] = 'Détails';
$gl_caption_image['c_progress_dialog_title_text'] = 'Ajout d\'images en cours';

// camps images
$gl_caption_image['c_ordre'] = 'ordre';
$gl_caption_image['c_main_image'] = 'principal';
$gl_caption_image['c_name'] = 'Nom';
$gl_caption_image['c_imagecat_id'] = 'Categorie';
$gl_caption_image['c_image_alt'] = 'Alt';
$gl_caption_image['c_image_title'] = 'Titre';
$gl_caption_image['c_edit_titles_images'] = 'Modifier titres d\'image';
$gl_caption_image['c_hide_titles_images'] = 'Masquer titres des images';

// sobreescrits caption traduccions i traduccions_news.tpl
$gl_caption_translation['c_send_edit'] = 'Envoyer à traduire';
$gl_caption_translation['c_page_words_title'] = 'Nombre total de mots à traduire sur cette page';
$gl_caption_translation['c_all_page_words_title'] = 'Nombre total de mots à traduire sur toutes les pages';
$gl_caption_translation['c_total'] = 'Total';

// sobreescrits end


// Message default
$gl_messages['title_edit']='Modifier';
$gl_messages['title_copy']='Copier les données';
$gl_messages['added']='Le fichier a été correctement ajouté';
$gl_messages['add_exists']='Existe déjà un fichier avec ce(tte) %1$s: %2$s';
$gl_messages['saved']='Le fichier a été correctement modifié';
$gl_messages['not_saved']='Le fichier n\'a pas été modifié';
$gl_messages['no_records']='Il n\'y a aucun fichier dans cette liste';
$gl_messages['no_records_bin']='Il n\'y a aucun fichier dans la corbeille';
$gl_messages['list_saved']='Les modifications ont été correctement apportées aux fichiers';
$gl_messages['list_not_saved']='Aucun fichier n\'a été modifié et aucune modification n\'a été détectée';
$gl_messages['list_deleted']='Les fichiers ont été effacés';
$gl_messages['list_bined']='Les fichiers ont été envoyés à la corbeille';
$gl_messages['list_restored']='Les fichiers ont été récupérés';
$gl_messages['concession_no_acces']='Vous n\'êtes pas autorisé à accéder à cette section';
$gl_messages['concession_no_write']='Vous n\'êtes pas autorisé à modifier les données de cette section';
$gl_messages['select_item']='Cliquez sur un des fichiers de la liste pour le sélectionner';
$gl_messages['related_not_deleted']='Les fichiers indiqués n\'ont pas pu être effacés car d\'autres fichiers leur sont associés';



// sobreescrits message image
$gl_messages_image['list_saved']='Les modifications ont été correctement apportées aux photos.';
$gl_messages_image['list_not_saved']='Aucun fichier n\'a été modifié et aucune modification n\'a été détectée';
$gl_messages_image['list_deleted']='Les images ont été effacées';
// sobreescrits end


// NO TRADUIT
$gl_messages['deleted']='Le registre a été supprimé';
$gl_messages['bined']='Le dossier a été envoyé à la corbeille';
$gl_messages['restored']='Le registre a récupéré avec succès';
$gl_caption['c_ok'] = 'Accepter';
$gl_caption['c_cancel'] = 'Résilier';
$gl_caption ['c_close']='Fermer';
$gl_messages['post_max_size']='Le poids de la forme (contenu et des pièces jointes) dépasse la limite ';
$gl_messages['post_file_max_size']='Le poids de la pièce jointe dépasse la limite ';
$gl_messages['assigned']='Registres sélectionnés';
$gl_caption['c_black']='Noir';
$gl_caption['c_white']='Blanc';
$gl_caption['c_silver']='Argent';
$gl_caption['c_grey']='Vert';
$gl_caption['c_blue']='Bleu';
$gl_caption['c_red']='Rouge';
$gl_caption['c_orange']='Orange';
$gl_caption['c_green']='Vert';
$gl_caption['c_purple']='Lila';
$gl_caption['c_pink']='Rose';
$gl_caption['c_golden']='Or';
$gl_caption['c_yellow']='Jaune';
$gl_caption['c_maroon']='Grana';
$gl_caption['c_brown']='Brun';
//creo un array global per els mesos
$gl_caption_months = array("Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre");

$gl_caption ['c_2']='deux';
$gl_caption ['c_3']='trois';
$gl_caption ['c_4']='quatre';
$gl_caption ['c_5']='cinq';
$gl_caption ['c_6']='six';
$gl_caption ['c_7']='sept';
$gl_caption ['c_8']='huit';
$gl_caption ['c_9']='neuf';
$gl_caption['c_10']='dix';
$gl_caption['c_11']='onze';
$gl_caption['c_12']='douze';
$gl_caption['c_13']='treize';
$gl_caption['c_14']='quatorze';
$gl_caption['c_15']='quinze';
$gl_caption['c_16']='seize';
$gl_caption['c_17']='dix-sept';
$gl_caption['c_18']='dix-huit';
$gl_caption['c_19']='dix-neuf';
$gl_caption['c_20']='vingt';
$gl_caption['c_30']='trente';
$gl_caption['c_40']='quarante';
$gl_caption['c_50']='cinquante';
$gl_caption['c_60']='soixante';
$gl_caption['c_70']='soixante-dix';
$gl_caption['c_80']='quatre-vingts';
$gl_caption['c_90']='quatre-vingt dix';
$gl_caption['c_1000']='mile';


$gl_caption['c_add_button'] = 'Ajouter entrée';

$gl_caption['c_login'] = 'Login';
$gl_caption['c_password'] = 'Mot de passe';
$gl_caption['c_mail'] = 'E-mail';
$gl_caption['c_mail_repeat'] = 'Répéter E-mail';
$gl_caption['c_password_repeat'] = 'Répéter mot de passe';
$gl_caption['c_noscript'] = '<p><strongVotre navigateur ne supporte pas JavaScript ou vous avez désactivé. </strong></p><p><a href="//www.google.com/support/websearch/bin/answer.py?answer=23852">Javascript doit être activé</a> pour utiliser Letnd</p>';
$gl_caption['c_cookie'] = '<p><strongVotre navigateur ne supporte pas ou a désactivé les cookies. </strong></p><p><a href="//www.google.com/support/websearch/bin/answer.py?answer=35851">Les cookies doivent être activés</a> pour utiliser Letnd</p>';

$gl_messages_image['images_uploaded']='Images enregistrées';
$gl_messages['form_sent']='Le formulaire a été soumis avec succès';
$gl_messages['form_not_sent']='Le formulaire n\'a pas pu être envoyé';


$gl_caption['c_search_adress'] = 'Rechercher une adresse';
$gl_caption['c_map_frame_1'] = 'Trouvé par hasard';
$gl_caption['c_map_frame_2'] = 'Si l\'emplacement est correct';
$gl_caption['c_map_frame_3'] = 'cliquez ici pour vous enregistrer';
$gl_caption['c_map_frame_4'] = 'Aucun résultat approprié n\'a été trouvé';

$gl_caption['c_list_view_1'] = 'Normale';
$gl_caption['c_list_view_2'] = 'Grandes icônes';
$gl_caption['c_list_view_3'] = 'Liste';

$gl_caption['c_edit_list_button_1'] = 'Non éditer liste';
$gl_caption['c_edit_list_button_'] = 'Éditer liste';
$gl_caption['c_add_dots'] = 'Couper le texte';

// SEO
$gl_caption['c_seo']='Outils SEO ( Optimisation web )';
$gl_caption['c_page_title']='Titre de la page. Max. 66-69 caractères. Optimum <65 caractères:';
$gl_caption['c_page_description']='Description de la page. max. 156-158 caractères:';
$gl_caption['c_page_keywords']='Mots-clés:';
$gl_caption['c_file_name']='Nommez la page URL. Ex. www.lamevaweb.com/<b>nom-de-la-pagina</b>.htm max. 90 caractères:';
$gl_caption['c_file_name_excel']='Nom page';
$gl_caption['c_old_file_name']='Ancien nom de l\'URL de la page.';



// SOCIAL
$gl_caption['c_edit_social'] = 'Réseaux sociaux ';
$gl_caption['c_edit_social_title'] = 'Publié';
$gl_caption['c_social_language'] = 'Langue';
$gl_caption['c_facebook_message'] = 'Message';
$gl_caption['c_facebook_picture'] = 'Image';
$gl_caption['c_facebook_link'] = 'Link';
$gl_caption['c_facebook_link_explanation'] = 'Agafa els texts directament de la pàgina';
$gl_caption['c_facebook_name'] = 'Description 1<sup>a</sup> ligne';
$gl_caption['c_facebook_caption'] = 'Description 2<sup>a</sup> ligne';
$gl_caption['c_facebook_image_title'] = 'Image';
$gl_caption['c_facebook_description'] = 'Description';
$gl_caption['c_facebook_save'] = 'Publier su facebook';
$gl_caption['c_facebook_publish_as'] = 'Publier en tant que';
$gl_caption['c_facebook_publish_as_image'] = 'Image';
$gl_caption['c_facebook_publish_as_link'] = 'Link';
$gl_caption['c_facebook_publish_save_default']='Par défault cette option';
$gl_caption['c_facebook_publish_user']='Mur';
$gl_caption['c_facebook_publish_pagina']='Page';
$gl_caption['c_facebook_publish_id']='Poster à';
$gl_caption['c_facebook_login']='Connectez-vous à Facebook';
$gl_caption['c_facebook_delete_text']='<p>Les messages postés sur Facebook ne sont modifiables qu\'à à partir de Facebook.</p><p>Si vous publiez de Letnd doit d\'abord supprimer la publication précédente</p>';
$gl_caption['c_facebook_delete']='Effacer Publishing sur Facebook';
$gl_caption['c_facebook_logout']='Logout';
$gl_caption['c_facebook_cannot_delete']='Impossible de supprimer la publication a été créé comme un autre utilisateur Facebook correspondant à';
$gl_caption['c_facebook_activate']='Le coût de l\'activation de ce service est de 60 € pour s\'inscrire à Facebook';

$gl_caption['c_twitter_message'] = 'Message';
$gl_caption['c_twitter_save'] = 'Publier sur Twitter ';
$gl_caption['c_twitter_login']='Connectez-vous sur Twitter ';
$gl_caption['c_twitter_delete_text']='<p>Les messages postés sur Twitter ne sont modifiables qu\'à à partir de Twitter. .</p><p>Si vous publiez de Letnd doit d\'abord supprimer la publication précédente </p>';
$gl_caption['c_twitter_delete']='Effacer la publication de Twitter';
$gl_caption['c_twitter_cannot_delete']='Impossible de supprimer la publication: elle a été créé comme un autre utilisateur de Twitter correspondant à';

$gl_caption['c_button_form_twitter_published']=
$gl_caption['c_button_form_facebook_published']=
$gl_caption['c_button_form_twitter']=
$gl_caption['c_button_form_facebook']=	'';
$gl_caption['c_chars']=	'caractères';

$gl_messages['social_facebook_no_user']='Vous n\'êtes pas connecté, s\'il vous plaît essayer à nouveau';
$gl_messages['social_facebook_no_page']='Aucune page avec cette id';
$gl_messages['social_facebook_published']='Message a été envoyé avec succès à Facebook';
$gl_messages['social_facebook_not_init']='Impossible de se connecter à Facebook';
$gl_messages['social_facebook_error']='Il y avait une erreur dans Facebook:';
$gl_messages['social_facebook_deleted']='Le message a été supprimé de Facebook ';
$gl_messages['social_facebook_not_deleted']='Il n\'y a pas de messages à supprimer';

$gl_messages['social_twitter_empty']='Le message est vide, vous ne pouvez poster sur Twitter';
$gl_messages['social_twitter_no_user']='Vous n\'êtes pas connecté, s\'il vous plaît essayer à nouveau';
$gl_messages['social_twitter_published']='Message a été envoyé avec succès à Twitter';
$gl_messages['social_twitter_not_init']='Impossible de se connecter à Twitter';
$gl_messages['social_twitter_error']='Il y avait une erreur sur Twitter: ';
$gl_messages['social_twitter_deleted']='Le message a été éliminée du Twitter';
$gl_messages['social_twitter_not_deleted']='Il n\'y a pas de messages à supprimer';

// FI NO TRADUIT


$gl_caption['c_file_manager']=	'File manager';
$gl_messages['no_records_search']='No match found for this search';
$gl_caption['c_back'] = 'Retour';

$gl_caption['c_asc'] = 'asc';
$gl_caption['c_desc'] = 'desc';

$gl_caption['c_reply'] = 'Répondre';

$gl_caption['c_file_file']='Fichier';
$gl_caption['c_file_title']='Titre';
$gl_caption['c_file_show_home']='List';
$gl_caption['c_file_ordre']='Ordre';
$gl_caption['c_file_public']='Public';
$gl_caption['c_file_filecat_id']='Cat.';

$gl_caption['c_entered'] = 'Created';
$gl_caption['c_modified'] = 'Modified';