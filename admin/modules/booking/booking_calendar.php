<?
/**
 * BookingCalendar
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) Juliol 2014
 * @version $Id$
 * @access public
 * 
 * 
 * Funcions del calendari per reutilitzar també a public
 * 
 */
class BookingCalendar {
	
	var $propertys, $one_property, $today_form_date, $tomorrow_form_date, $message_type, $is_season_calendar = false, $seasons_colors = array(), $process, $property_id = false, $month_seasons;
	public static $colors = array(
						'204,206,1',				
						'175,177,7',
						'143,145,2',

						'253,196,21',
						'244,150,0',
						'195,120,0',

						'233,81,14',				
						'255,0,0',
						'207,0,0',

						'227,50,140',				
						'187,10,126',
						'149,27,128',

						'82,191,211',
						'1,164,169',
						'4,136,140',

						'144,181,255',
						'3,86,254',
						'3,59,173',
					);
	
	public function __construct($process='', $one_property = true, $property_id = false){
		$this->process = $process;
		$this->propertys = $this->get_properties($property_id);
		$this->one_property = $one_property;
	}
	
	// Mostrar calendari
	public function get_calendar_array()
	{
		global $gl_message;
		// variables

		$num_propertys = count($this->propertys);

		// Quan es reserva des de una cerca
		if (R::get('date_in') && R::get('date_out')){
			$date_in = unformat_date(R::get('date_in'));
			$date_out = unformat_date(R::get('date_out'));
		
			$begin = new DateTime($date_in);
			$end = new DateTime($date_out);
			
			$interval = DateInterval::createFromDateString('1 month');
			$period = new DatePeriod($begin, $interval, $end);
			
			$today_day = $begin->format( "d" );
			$month = $begin->format( "n" );
			$year = $begin->format( "Y" );
			// no perque es el dia de la setmana del day in, i necessito el del primer dia del mes
			// $week_day = $begin->format( "w" );
			
			
			$date = mktime(0, 0, 0, $month, 1, $year);
			$week_day = date('w', $date);
			
			$number_months = 0;
			foreach ( $period as $dt ){
				$number_months ++; 
			}
			
			if ($number_months < 3) $number_months = 3;

			
			//$date_in = explode('/',$date_in);
			//$date_out = explode('/',$date_out);
		}
		else {
			
			$number_months = 3;
			
			$date = time();

			$today_day = date('d', $date);
			$month = date('n', $date);
			$year = date('Y', $date);

			$date = mktime(0, 0, 0, $month, 1, $year);
			//$month = date('n', $date);
			//$year = date('Y', $date);
			$week_day = date('w', $date);
			
		}

		if ($this->message_type != 'error')
		{
			$this->today_form_date = "$year-$month-" . ($today_day);
			$this->tomorrow_form_date = "$year-$month-" . ($today_day + 1);
		}


			//Debug::p($today_day, 'dia');
			//Debug::p($month, 'mes');
			//Debug::p($year, 'any');
			//Debug::p($week_day, 'dia setmana');

		return array(
			'seasons_styles' => BookingCalendar::get_season_styles($this->seasons_colors), 
			'calendar' => $this->get_calendar($month,$year,$week_day,$number_months), 
			'propertys' => $this->propertys,				
			'today_form_date' => $this->today_form_date, 
			'tomorrow_form_date' => $this->tomorrow_form_date, 
			'message' => $gl_message, 
			'message_type' => $this->message_type,
			'is_season_calendar' => $this->is_season_calendar
			);


	}
	
	// obtindre mes per ajax 
	public function get_month(){
		$month = R::id('month');
		$year = R::id('year');
		
		$date = mktime(0, 0, 0, $month, 1, $year);
		$week_day = date('w', $date);
		
		$this->is_season_calendar = $this->process=='season';
		
		if ($this->is_season_calendar){
			Main::load_class('booking', 'common');
			$this->seasons_colors = BookingCommon::get_seasons_colors();
		}
		
		$this->one_property = true;
		$this->propertys = $this->get_properties();
		
		die($this->get_calendar($month,$year,$week_day, 1));
	}
	
	// obtinc colors, per defecte o al config
	public static function get_colors() {
		
		$results = Db::get_rows("SELECT color_id, color FROM booking__color");
		
		$ret = array();
		
		foreach ($results as $rs) {
			$ret[$rs['color_id']] = $rs['color'];
		}
		
		return $ret;
	
	}
	
	// obtinc calendari per temporades
	public function get_season_calendar() {
		
		$this->is_season_calendar = true;		
		$this->propertys = $this->get_properties();
		
		return $this->get_calendar_array();
	}
	static public function get_season_styles($seasons_colors) {
		$ret = '';
		// Debug::p($seasons_colors, 'text');		
		if($seasons_colors){
			
			$ret = '<style>';
			
			foreach ($seasons_colors as $key => $bg){
				
				
				$ret .= '
					.season' . $key . ',
					.day.middle-booked.season' . $key . ',
					.day.first-booked.season' . $key . ',
					.day.last-booked.season' . $key . '
						{ 
							background-color: rgba('.$bg.',0.7); 
							border-color: rgba('.$bg.',0.8);
							color:#000;
						}
					.day.middle-booked.season' . $key . ':hover,
					.day.first-booked.season' . $key . ':hover,
					.day.last-booked.season' . $key . ':hover,
					table.mes td:hover .day.middle-booked.season' . $key . '
						{ 
							background-color: rgba('.$bg.',0.9) !important; 
						}
					.day.weekend.middle-booked.season' . $key . ',
					.day.weekend.first-booked.season' . $key . ',
					.day.weekend.last-booked.season' . $key . '
						{ 
							background-color: rgba('.$bg.',0.8); 
							border-color: rgba('.$bg.',1); 
							color:#000;
						}
					.day.weekend.middle-booked.season' . $key . ':hover,
					.day.weekend.first-booked.season' . $key . ':hover,
					.day.weekend.last-booked.season' . $key . ':hover,
					table.mes td:hover .day.weekend.middle-booked.season' . $key . '
						{ 
							background-color: rgba('.$bg.',1) !important; 
						}
					table.price_details .season' . $key . '{
						background-color:transparent;
						border-right: 2px solid rgba('.$bg.',1);
					}
					';
			}
			
			$ret .= '</style>';
		}
		return $ret;
	}
	// comprovo dies ocupats
	private function get_month_reserves($month,$year,$property_id){
		
		// aquests querys faig <= o >= perque també s'ha de mostrar al calendari encara que comenci l'ultim dia
		$query_seasons = "SELECT *, date_start as date_in, date_end as date_out, propertyrange_id as book_id
								FROM booking__propertyrange
								WHERE (
									'$year-$month-1' <= date_end
									AND LAST_DAY('$year-$month-1') >= date_start
									)
								AND property_id = $property_id
								ORDER BY date_start";
		//Debug::p($query_seasons, 'text');
		
		if ($this->is_season_calendar){
			
			// canvio els noms del camps nomes per visulitzar el calendari, no implica res mes
			return Db::get_rows($query_seasons);
		}
		
		else{
			
			$this->month_seasons = Db::get_rows($query_seasons);
			
			return Db::get_rows("SELECT *
								FROM booking__book
								WHERE (
									'$year-$month-1' <= date_out
									AND LAST_DAY('$year-$month-1') >= date_in
									)
								AND property_id = $property_id
								AND ( status = 'booked' || status = 'confirmed_email' || status = 'confirmed_post' || status = 'contract_signed')
								ORDER BY date_in");
		}
		
	}
	// construeixo el calendari
	private function get_calendar($month,$year,$week_day, $number_months = 3){
		
		global $month_names;
		
		if ($this->is_season_calendar) {
			//$number_months = 6;
		}
		
		$max_weeks = 0;
		$calendar = array();
		
		// bucle mesos
		for ($i = 1; $i <= $number_months; $i++)
		{
			// ho moc a dins bucle -> $reserves = $this->get_month_reserves($month,$year);
			//debug::p($reserves,'reserves');
			$days_in_month = date('t', mktime(0, 0, 0, $month, 1, $year));
			$calendar[$month]['month'] = $month_names[$month-1];
			$calendar[$month]['month_value'] = $month;
			$calendar[$month]['year'] = $year;
			$conta = 1;
			$week = 1;
			foreach ($this->propertys as $pr)
			{
				$reserves = $this->get_month_reserves($month,$year,$pr['property_id']);
				$p = &$calendar[$month]['propertys'][$pr['property_id']];
				$p = $pr;
				$week_day_pr = $week_day;
				// empleno començament mes
				if ($this->one_property)
				{
					$week_day_empty = $week_day_pr;
					if ($week_day_empty == 0)
						$week_day_empty = 7;
					while ($conta < $week_day_empty && $week_day_empty != 1)
					{
						$this->one_property?$d = &$p['weeks'][$week]['days'][$conta]:$d = &$p['days'][$conta];
						$d['class1'] = $d['class2'] = 'empty';
						$d['day'] = $d['onclick1'] = $d['onclick2'] = $d['id1'] = $d['id2'] = '';
						$conta++;
					}
				}
				for ($day = 1; $day <= $days_in_month; $day++, $conta++)
				{
					$this->one_property?$d = &$p['weeks'][$week]['days'][$conta]:$d = &$p['days'][$conta];
					$d['day'] = $day;
					$d['id1'] = $d['id2'] = '';
					$d['is_weekend'] = ($week_day_pr == 0 || $week_day_pr == 6) ? ' weekend' : '';
					
					$class_booked_free = ''; // la defineixo a get_reservat
					
					$d['is_booked'] = $this->get_reservat($reserves, $day, $month, $year,
						$d, $class_booked_free);
					
					$class1 = "day" . $d['is_weekend'] . $d['is_booked'];
					$class_booked_free = "day" . $d['is_weekend'] . $class_booked_free;					

					$set_day_click = "onclick=\"booking.do_action(" . $day . ',' . $month . ',' . $year;

					
					$d['onclick1'] = $set_day_click . ",'a'";
					$d['onclick2'] = $set_day_click . ",'b'";
					
					if ($d['is_booked'] == ' first-booked')
					{
						$d['class1'] = $class_booked_free;
						$d['class2'] = $class1 . $d['class_season1'];
						$d['onclick2'] .= "," . $d['book_id1'] . "";
						$d['id2'] .= $d['book_id1'];
						//$d['onclick1'] = $set_day_click . ",'a');\"";
						//$d['onclick2'] = "onclick=\"show_form('" . $d['book_id1'] . "');\"";
					} elseif ($d['is_booked'] == ' last-booked')
					{
						$d['class1'] = $class1 . $d['class_season2'];
						$d['class2'] = $class_booked_free;
						$d['onclick1'] .= "," . $d['book_id2'] . "";
						$d['id1'] .= $d['book_id2'];
						//$d['onclick1'] = "onclick=\"show_form('" . $d['book_id2'] . "');\"";
						//$d['onclick2'] = $set_day_click . ",'b');\"";
					} elseif ($d['is_booked'] == 'first_and_last')
					{
						$d['class1'] = "day" . $d['is_weekend'] . ' last-booked' . $d['class_season2'];
						$d['class2'] = "day" . $d['is_weekend'] . ' first-booked' . $d['class_season1'];
						$d['onclick1'] .= "," . $d['book_id2'] . "";
						$d['onclick2'] .= "," . $d['book_id1'] . "";						
						$d['id1'] .= $d['book_id2'];						
						$d['id2'] .= $d['book_id1'];
						//$d['onclick1'] = "onclick=\"show_form('" . $d['book_id2'] . "');\"";
						//$d['onclick2'] = "onclick=\"show_form('" . $d['book_id1'] . "');\"";
					}
					else
					{
						if ( $d['book_id']){
							
							$set_day_click .= "," . $d['book_id'] . "";
							$d['onclick1'] .= "," . $d['book_id'] . "";
							$d['onclick2'] .= "," . $d['book_id'] . "";
							$d['id1'] .= $d['book_id'];
							$d['id2'] .= $d['book_id'];
							$d['class1'] = $class1 . $d['class_season'];
							$d['class2'] = $class1 . $d['class_season'];
						}
						else{
							$d['class1'] = $class1 . $d['class_season1'];
							$d['class2'] = $class1 . $d['class_season2'];							
						}
						
					}
					
					$d['onclick1'] .= ");\"";
					$d['onclick2'] .= ");\"";
					
					$week_day_pr++;
					if ($week_day_pr == 7)
						$week_day_pr = 0;
					if ($week_day_pr == 1)
						$week++;
				}
				// empleno final mes
				if ($this->one_property)
				{
					$week_day_empty = $week_day_pr;
					if ($week_day_empty == 0)
						$week_day_empty = 7;

					while ($week_day_empty < 8)
					{
						$this->one_property?$d = &$p['weeks'][$week]['days'][$conta]:$d = &$p['days'][$conta];
						$d['class1'] = $d['class2'] = 'empty';
						$d['day'] = $d['onclick1'] = $d['onclick2'] = $d['id1'] = $d['id2'] = '';
						$conta++;
						$week_day_empty++;
						// assumiré que sempre hi ha sis setmanes (almenys els pròxims 4 anys es cert)
						if (($week_day_empty == 8) && ($week < 6))
						{
							$week_day_empty = 1;
							$week++;
						}
					}
					$max_weeks = $max_weeks > ($conta-1) / 7?$max_weeks:($conta-1) / 7;
				}
			}
			$week_day = $week_day_pr; // així el pròxim més continua en aquest dia de la setmana
			$month++;
			if ($month == 13)
			{
				$month = 1;
				$year++;
			} ;
		}

		$tpl = new phemplate(PATH_TEMPLATES, 'booking/book_form_calendar.tpl');
		$tpl->set_loop('calendar',$calendar);
		return $tpl->process();
		
	}
	// Comprobo quines reserves te un dia concret
	// 
	// Pot ser:
	//		un dia del mig
	//		el primer dia d'una reserva
	//      l'ultim dia d'una reserva
	//      el primer d'una reserva i l'ultim de l'anterior reserva
	//
	private function get_reservat($reserves, $day, $month, $year, &$d, &$class_booked_free)
	{
		static $count = 0;
		$data = strtotime("$year-$month-$day");
		
		$is_first = $is_last = $d['book_id'] = $d['class_season'] = $d['class_season1'] = $d['class_season2'] = '';		
		
		
			
		$class_season = $this->get_season_class($data, $d);
		
		foreach ($reserves as $v)
		{
			if ($this->seasons_colors){
				$class_season = ' season' . $v['season_id'];
			}
			
			
			// echo strtotime($v['date_in']) . " $data " . strtotime($v['date_out']) . " <br>";
			if (strtotime($v['date_in']) < $data && $data < strtotime($v['date_out']))
			{
				$d['class_season'] = $class_season;
				$d['book_id'] = $v['book_id'];						
				return ' middle-booked';
			}
			if (strtotime($v['date_in']) == $data)
			{
				$is_first = true;
				$d['class_season1'] = $class_season;
				$d['book_id1'] = $v['book_id'];
			}
			if (strtotime($v['date_out']) == $data)
			{
				$is_last = true;
				$d['class_season2'] = $class_season;
				$d['book_id2'] = $v['book_id'];
			}
		}
		
		
		
		if ($is_first && $is_last)
			return 'first_and_last';
		if ($is_first){
			$class_booked_free = ' first-free';
			return ' first-booked';
		}
		if ($is_last){
			$class_booked_free = ' last-free';
			return ' last-booked';
		}
		
		return ' free';
	}
	/*
	 * CALENDARI SEASONS - > retorna la classe amb el numero de temporada per fer els coloraines segons temporades -> es calcula nomes pels dies dins una temporada
	 * 
	 * CALENDARI RESERVES - > retorna clase "disabled" si no està definida la temporada, per que no es pugui reservar -> es calcula per cada dia
	*/
	function get_season_class(&$data, &$d) {
		
		
		
		
		if ($this->seasons_colors) return;
				
		$d['class_season'] = $d['class_season1'] = $d['class_season2'] = " disabled";
		foreach ($this->month_seasons as $v)
		{
			if (strtotime($v['date_in']) < $data && $data < strtotime($v['date_out']))
			{
				$d['class_season'] = '';
				$d['class_season1'] = '';
				$d['class_season2'] = '';
				return;
			}
			if (strtotime($v['date_in']) == $data)
			{
				$d['class_season'] = '';
				$d['class_season2'] = '';
			}
			if (strtotime($v['date_out']) == $data)
			{
				$d['class_season'] = '';
				$d['class_season1'] = '';
			}
		}
	}
	
	
	
	
	
	// Per quan vull llistar les reserves de varies propietats
	// De moment nomes faig servir amb una propietat, per llistar varies s'hauria de fer una taula amb un mes per fila i els dies per columna
	private function get_properties($property_id = false)
	{
		global $gl_db_max_results;
		
		if (!$property_id) $property_id = R::id('property_id');
		
		$query = "SELECT property_id, ref, property_private, '' AS checked, property_private AS property FROM inmo__property";
		if ($property_id)
		{
			$query .= " WHERE property_id = " . $property_id;
		}
		$query .= " ORDER BY ref DESC LIMIT 0, " . $gl_db_max_results;
		$propertys = Db::get_rows($query);
		$propertys[0]['checked'] = ' checked';
		
		return $propertys;
	}
	
}
?>