<?php

class ProductFamily extends Module {
	var $q = '';
	var $form_tabs;

	function __construct() {
		parent::__construct();
	}

	function on_load() {

		if ( ! $this->config['has_private_familys'] ) {
			$this->unset_field( 'is_family_private' );
			$this->unset_field( 'pass_code' );

		}

		parent::on_load();
	}

	public static function get_family_private_condition() {


		$pass_code = isset($_SESSION['product']['family_pass_code'])?$_SESSION['product']['family_pass_code']:'';

		$pass_code = $pass_code ? "AND pass_code!='$pass_code'" : '';
		$condition = "
		 AND (		 
		    family_id NOT IN ( 
		        SELECT family_id FROM product__family WHERE is_family_private = 1 $pass_code
		    )
		    AND
		    subfamily_id NOT IN ( 
		        SELECT family_id FROM product__family WHERE is_family_private = 1 $pass_code
		    )
		    AND
		    subsubfamily_id NOT IN ( 
		        SELECT family_id FROM product__family WHERE is_family_private = 1 $pass_code
		    )		    
	    )
		 ";

		return $condition;
	}


	public static function check_family_private( $family_id ) {

		// Comprovar també get de family_ids
		if ( $family_id ) {
			$family_ids = [ $family_id ];
		}
		else {
			$family_ids = R::get('family_ids');

			if ( ! $family_ids ) {
				return;
			}
			R::escape_array( $family_ids );

		}

		// Comprovo cap a munt que cap familia sigui privada, i si es privada que concordi amb el codi
		$q = new Query( 'product__family' );
		$session_pass_code = isset($_SESSION['product']['family_pass_code'])?$_SESSION['product']['family_pass_code']:false;

		foreach ( $family_ids as $family_id ) {

			while ( $family_id ) {
				$rs = $q->get_row( 'parent_id, pass_code,is_family_private', $family_id );

				// Si no hi ha rs es que s'ha posat una familia que ja no existeix, per tant no hi ha problema de permisos
				if ( ! $rs ) break;

				$family_id         = $rs['parent_id'];
				$pass_code         = $rs['pass_code'];
				$is_family_private = $rs['is_family_private'];
				if ( $is_family_private && $session_pass_code !== $pass_code ) {
					// Si la familia es privada, redirigeixo a la home
					Main::redirect( '/' );
				}
			}
		}
	}

	public function check_pass_code() {

		$vars = [
			'success'         => false,
			'error_message'   => $this->caption['c_pass_code_error'],
			'success_message' => $this->caption['c_pass_code_success'],
		];

		$pass_code = trim( R::post( 'pass_code' ) );
		if ( ! $pass_code ) json_end( $vars );
		$q = new Query( 'product__family' );

		$rs = $q->get_row( 'pass_code', "pass_code = %s", $pass_code );

		if ( $rs['pass_code'] == $pass_code ) {
			$_SESSION['product']['family_pass_code'] = $pass_code;
			$vars['success']                         = true;
		}

		json_end( $vars );
	}

	public function logout(){
		$vars = [
			'success'         => true
		];
		if (isset($_SESSION['product']['family_pass_code']))
		unset($_SESSION['product']['family_pass_code']);
		json_end( $vars );
	}
}