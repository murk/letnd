<?php
// no es poden posar ; entre mig de la consulta ja que parteix les consultes per el ; a no ser que fem servir $update_sql en array()
if (is_table("news__new_language") && !is_field('news__new_language','content_list')){
	
	$update_sql .="ALTER TABLE `news__new_language`
	ADD COLUMN `content_list` TEXT NOT NULL AFTER `subtitle`;";
}	
?>