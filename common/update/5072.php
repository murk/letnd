<?

/* 31-03-13 
  Afegir cadastre a immo

 */

if (is_table('inmo__property')) {	
	
	$update_sql[]= "
		ALTER TABLE `inmo__property` ADD `ref_cadastre` VARCHAR( 20 ) NOT NULL DEFAULT '' AFTER `ref`;
		";		
	
}

if (is_table('missatge__missatge')) {	
	
	$update_sql[]= "
		ALTER TABLE `missatge__missatge`
	ADD COLUMN `entered` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `blocked`;
		";	
	
	$update_sql[]= "
		ALTER TABLE `missatge__missatge`
	ADD COLUMN `link` VARCHAR(250) NOT NULL AFTER `entered`;
		";	
	
	$update_sql[]= "
		ALTER TABLE `missatge__missatge`
	ADD COLUMN `link_text` VARCHAR(250) NOT NULL AFTER `link`;
		";	
}


?>