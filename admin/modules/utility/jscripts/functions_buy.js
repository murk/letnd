"use strict";

jQuery.validator.addMethod(
	"vdate",
	function (value, element) {
		return Date.parseExact(value, "dd/MM/yyyy");
	},
	$.validator.messages.date
);

var Buy = {
	is_others_visible: true,
	price_input: '',
	percent_inputs: [
		'notary',
		'register',
		'agency',
		'mortgage'
	],
	percent_mortgage_inputs: [
		'mortgage_open',
		'mortgage_notary',
		'mortgage_register'
	],
	init: function () {

		this.price_input = '#price_' + buy_id;
		$(this.price_input).focus();
		// dp(this.price_input);
		this.init_masks();
		this.init_percents();
		this.calculate_percents();
		this.init_validation();
		this.write_report();
		this.init_all_keyups();
	},

	init_masks: function () {

		//$("#mortgage_start_date_0").inputmask(
		//	"d/m/y",
		//	{
		//		"placeholder": "dd/mm/yyyy"
		//	}
		//);

		var decimal_masks =
				Buy.price_input +
				',#notary_percent_' + buy_id +
				',#notary_' + buy_id +
				',#register_percent_' + buy_id +
				',#register_' + buy_id +
				',#agency_percent_' + buy_id +
				',#agency_' + buy_id +
				',#mortgage_percent_' + buy_id +
				',#mortgage_' + buy_id +
				',#mortgage_open_percent_' + buy_id +
				',#mortgage_open_' + buy_id +
				',#mortgage_notary_percent_' + buy_id +
				',#mortgage_notary_' + buy_id +
				',#mortgage_register_percent_' + buy_id +
				',#mortgage_register_' + buy_id +
				',#mortgage_initial_interest_' + buy_id
			;

		// Mascares decimals
		$(decimal_masks).inputmask(
			{
				'alias': 'integer',
				'groupSeparator': '.',
				'radixPoint': ',',
				'autoGroup': true,
				'digits': 2,
				'digitsOptional': true,
				'placeholder': '',
				'decimalProtect': true
			}
		);

		// Mascares enters
		$('#mortgage_length_' + buy_id).inputmask(
			{
				'alias': 'numeric',
				'groupSeparator': '.',
				'autoGroup': true,
				'integerDigits': 2,
				'digits': 0,
				'min': 1,
				'max': 70
			}
		);
	},

	init_validation: function () {

		// Accions del boto preu
		$(Buy.price_input)
			.keyup(function (e) {
				if (e.which == 13) {
					$(this).blur();
				}
				//else if ($(this).hasClass('valid')){
				else {
					//Buy.calculate_percents();
					//Buy.write_report();
				}
			})
			.blur(function () {
				if (!Buy.is_others_visible && $(this).hasClass('valid')) {
					$('#other_fields').slideDown(500);
					$('#notary_0').focus();
					Buy.is_others_visible = true;
				}
			});

		// Validacions
		var required_fields = [
			"price[" + buy_id + "]"
		];

		var rules = {};

		for (var f in required_fields) {
			rules[required_fields[f]] = {
				required: true
			}
		}
		$('#main_form')
			.validate({
				rules: rules,
				tooltip_options: {
					'_all_': {placement: 'right'}
				},
				submitHandler: function (form) {
					// així no fa mai un submit
				},
				invalidHandler: function (form, validator) {
					setTimeout(
						function () {
							$(".tooltip").removeClass('in');
						}, 700);
				}
			});
		//$('#main_form').valid();
	},
	init_percents: function () {
		for (var key in this.percent_inputs) {
			var val = this.percent_inputs[key];
			this.add_percent(val, false);
		}
		for (var key in this.percent_mortgage_inputs) {
			var val = this.percent_mortgage_inputs[key];
			this.add_percent(val, true);
		}
	},

	calculate_percents: function () {
		for (var key in this.percent_inputs) {
			var val = this.percent_inputs[key];
			this.calculate_percent(val, false);
		}
		for (var key in this.percent_mortgage_inputs) {
			var val = this.percent_mortgage_inputs[key];
			this.calculate_percent(val, true);
		}
	},
	unmask_decimal: function (id) {
		dp(id);
		var val = $(id).inputmask('unmaskedvalue');
		if (typeof val == 'undefined') return '';
		val = val.replace(',', '.');
		return Number(val);
	},

	calculate_percent: function (name, is_mortgage) {
		var currency_id = '#' + name + '_0',
			percent_id = '#' + name + '_percent_0',
			is_percent_active,
			current_id,
			other_id,
			new_val,
			price = is_mortgage ? this.unmask_decimal('#mortgage_' + buy_id) : this.unmask_decimal(Buy.price_input),
			value;

		if (!price) return;

		is_percent_active = $(percent_id).hasClass('active');


		// calculo percentatge
		if (is_percent_active) {
			current_id = percent_id;
			other_id = currency_id;
			value = this.unmask_decimal(current_id);
			new_val = value * price; // =/100 - després de round
			// dp(value, 'Es per cent');
			// dp(new_val);
		}
		// calculo import
		else {
			current_id = currency_id;
			other_id = percent_id;
			value = this.unmask_decimal(current_id);
			new_val = value / price * 10000; // = *100
			// dp(value, 'NO Es per cent');
		}
		// dp(current_id);
		//// dp(other_id);
		if ($.isNumeric(value)) {
			new_val = Math.round(new_val);
			new_val = new_val / 100;
			$(other_id).val(new_val);
		}
		else {
			$(other_id).val('');
		}
	},

	add_percent: function (name, is_mortgage) {
		var focus_val;
		$('#' + name + '_' + buy_id + ',#' + name + '_percent_' + buy_id)
			.focus(function () {
				focus_val = this.value;
				// dp('focus val', focus_val);

			})
			.keyup(function () {
				// dp('keyup val', this.value);
				if (this.value != focus_val) {
					var
						id = '#' + this.id,
						other_id =
							id == '#' + name + '_' + buy_id ?
							'#' + name + '_percent_' + buy_id :
							'#' + name + '_' + buy_id;

					$(other_id).removeClass('active');
					$(other_id).parent().removeClass('active');
					$(id).addClass('active');
					$(id).parent().addClass('active');
					// dp(id);
					// dp(other_id);
				}

				Buy.calculate_percent(name, is_mortgage);
			});
	},

	write_report: function () {

		dp('REPORT \n\n\n');


		// calculs
		var
			price = Buy.unmask_decimal(Buy.price_input),
			notary_percent = Buy.unmask_decimal('#notary_percent_' + buy_id),
			notary = Buy.unmask_decimal('#notary_' + buy_id),
			register_percent = Buy.unmask_decimal('#register_percent_' + buy_id),
			register = Buy.unmask_decimal('#register_' + buy_id),
			agency_percent = Buy.unmask_decimal('#agency_percent_' + buy_id),
			agency = Buy.unmask_decimal('#agency_' + buy_id),
			mortgage_percent = Buy.unmask_decimal('#mortgage_percent_' + buy_id),
			mortgage = Buy.unmask_decimal('#mortgage_' + buy_id),
			mortgage_open_percent = Buy.unmask_decimal('#mortgage_open_percent_' + buy_id),
			mortgage_open = Buy.unmask_decimal('#mortgage_open_' + buy_id),
			mortgage_initial_interest = Buy.unmask_decimal('#mortgage_initial_interest_' + buy_id),
			mortgage_length = Buy.unmask_decimal('#mortgage_length_' + buy_id),
			mortgage_notary_percent = Buy.unmask_decimal('#mortgage_notary_percent_' + buy_id),
			mortgage_notary = Buy.unmask_decimal('#mortgage_notary_' + buy_id),
			mortgage_register_percent = Buy.unmask_decimal('#mortgage_register_percent_' + buy_id),
			mortgage_register = Buy.unmask_decimal('#mortgage_register_' + buy_id),
			mortgage_valuation = Buy.unmask_decimal('#mortgage_valuation_' + buy_id),
		//mortgage_start_date = $('#mortgage_start_date_' + buy_id).val(),
			total_expenses,
			total_financed,
			down_payment,
			tax,
			tax_val,
			taxtype,
			judicial_acts = 0,
			judicial_acts_mortgage = 0
			;

		down_payment = price - mortgage;
		tax_val = $('#taxtype_id_' + buy_id).val();
		tax = tax_val * price / 100;

		taxtype = $('#taxtype_id_' + buy_id + ' option:selected').text();
		taxtype = taxtype.split(' - ');
		taxtype = taxtype[1];

		if (taxtype == 'IVA') {
			judicial_acts = 1.5 * price / 100;
		}
		total_expenses = notary + register + agency + tax + judicial_acts;

		if (mortgage) {
			judicial_acts_mortgage = 1.5 / 100 * mortgage * 1.86;
			total_expenses += mortgage_notary + mortgage_register + mortgage_valuation + mortgage_open + judicial_acts_mortgage;
		}

		// resum
		this.set_report_val('price', '€', price);

		// despeses fixes
		this.set_report_val('down_payment', '€', down_payment);
		this.set_report_val('notary', '€', notary);
		this.set_report_val('register', '€', register);
		this.set_report_val('agency', '€', agency);
		this.set_report_val('tax', '€', tax);
		this.set_report_val('tax_val', '%', tax_val);
		this.set_report_val('taxtype', '', taxtype);
		this.set_report_val('judicial_acts', '€', judicial_acts);

		// despeses hipoteca
		if (mortgage) {
			this.set_report_val('mortgage_notary', '€', mortgage_notary);
			this.set_report_val('mortgage_register', '€', mortgage_register);
			this.set_report_val('mortgage_valuation', '€', mortgage_valuation);
			this.set_report_val('mortgage_open', '€', mortgage_open);
			this.set_report_val('judicial_acts_mortgage', '€', judicial_acts_mortgage);
		}

		// total
		this.set_report_val('total_expenses', '€', total_expenses);
		this.set_report_val('total_no_financed', '€', down_payment + total_expenses);

		// hipoteca
		this.show_mortgage(mortgage, mortgage_initial_interest, mortgage_length);
	},
	init_all_keyups: function () {
		$('#main_form .keyup').keyup(function () {
			delay(function () {
				Buy.calculate_percents();
				Buy.write_report();
			}, 200);
		});
		$('#main_form select').change(function () {
			Buy.calculate_percents();
			Buy.write_report();
		});
	},
	format_number: function (num, is_decimal) {

		if (typeof is_decimal == 'undefined') is_decimal = true;
		if (isNaN(num)) num = 0;
		var num_decimals = 2, decimal;
		num = Math.round(num * Math.pow(10, num_decimals)).toString();
		decimal = num.slice(num.length - 2, num.length);
		num = num.slice(0, num.length - 2);
		num = num.replace(/(\d)(?=(\d{3})+$)/g, '$1.');

		if (is_decimal) {
			if (!num) num = 0;
			if (decimal == '0') decimal = '00';
			num = num + ',' + decimal;
		}

		return num;
	},

	set_report_val: function (name, sufix, val, format) {

		if (typeof format == 'undefined') format = true;

		var holder = '#' + name + '_holder';
		val == 0 || val == '0,00' ? $(holder).hide() : $(holder).show();
		if (format) val = this.format_number(val, sufix=='€' || sufix == '%');

		$('#' + name).text(val + ' ' + sufix);
		// dp(val);
	},
	flip_date: function (dates) {
		dates = dates.split('/');
		dates = dates[2] + '-' + dates[1] + '-' + dates[0];
		return new Date(dates);
	},

	show_mortgage: function (mortgage, mortgage_initial_interest, mortgage_length) {

		if (!mortgage) {
			$('.mortgage_holder').hide();
			return;
		}
		else {
			$('.mortgage_holder').show();
		}
		var mortgage_start_date = new Date(); //this.flip_date(mortgage_start_date);

		var mortgage_object = MortgageCalculator.calculateMortgage({
			"salePrice": mortgage,
			"interestRate": mortgage_initial_interest,
			"loanTermMonths": mortgage_length * 12,
			"startDate": mortgage_start_date,
			"downPayment": "0%"
		});
		//dp(mortgage_object);
		//var mortgage_object = new Mortgage(mortgage, mortgage_initial_interest, mortgage_length);
		this.set_report_val('mortgage_initial_interest', '€', mortgage_initial_interest);
		this.set_report_val('mortgage_length', c_mortgage_years, mortgage_length);
		this.set_report_val('mortgage_length2', c_mortgage_years, mortgage_length);
		this.set_report_val('mortgage', '€', mortgage);
		this.set_report_val('mortgage2', '€', mortgage);
		this.set_report_val('monthly_payment', '€', mortgage_object.monthlyPayment,false);
		this.set_report_val('monthly_payment2', '€', mortgage_object.monthlyPayment,false);

		var table = '<table>';

		table = table
		+ '<th>' + c_table_quote + '</th>'
		+ '<th>' + c_table_year + '</th>'
		+ '<th>' + c_table_payment + '</th>'
		+ '<th>' + c_table_interest + '</th>'
		+ '<th>' + c_table_balance + '</th>'
		+ '<th>' + c_table_total_returned + '</th>'
		+ '<th>' + c_table_total_paid + '</th>'
		;

		for (var m in mortgage_object.paymentSchedule) {

			var p = mortgage_object.paymentSchedule[m];
			//dp(p)

			table = table
			+ '<tr>'
			+ '<td>' + p.loanMonth + '</td>'
			+ '<td>' + p.loanYear + '</td>'
			+ '<td>' + p.interest + '</td>'
			+ '<td>' + p.principal + '</td>'
			+ '<td>' + p.remainingLoanBalnce + '</td>'
			+ '<td>' + p.principalToDate + '</td>'
			+ '<td>' + p.paymentTotalToDate + '</td>'
			+ '</tr>';
		}
		table += '</table>';
		$('#mortgage_table').html(table);
	},
	print: function () {
		print();
	}

};
$(function () {
	Buy.init();
});

var delay = (function () {
	var timer = 0;
	return function (callback, ms) {
		clearTimeout(timer);
		timer = setTimeout(callback, ms);
	};
})();