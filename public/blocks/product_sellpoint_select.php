<?php

// per fer filtres de qualsevol camp de inmo__property $field<>0 <--------- de moment nomes numeric

$field             = isset( $vars['field'] ) ? $vars['field'] : 'comunitat_id'; // comunitat_id, provincia_id, country_id
$country_condition = isset( $vars['country_condition'] ) ? $vars['country_condition'] : false; // europe, rest_world
$page_id           = isset( $vars['page_id'] ) ? $vars['page_id'] : false;

$condition = '';
$table     = 'product__sellpoint';

// O faig així per que prefereixo no posar la consulta a la BBDD, per seguretat, es podria fer un insert, delete, update ...
if ( $country_condition == 'europe' ) {
	$condition = "AND country_id IN ( SELECT country_id FROM all__country WHERE continent = 'EU' AND country_id <> 'ES' )";
} else if ( $country_condition == 'rest_world' ) {
	$condition = "AND country_id IN ( SELECT country_id FROM all__country WHERE continent <> 'EU' AND country_id <> 'ES' )";
}

$field_name_select = '';

if ( $field == 'comunitat_id' ) {
	$field_name_select = "SELECT name FROM all__comunitat where $field = ";
} else if ( $field == 'provincia_id' ) {
	$field_name_select = "SELECT name FROM all__provincia where $field = ";
} else if ( $field == 'country_id' ) {
	$field_name_select = "SELECT country FROM all__country where $field = ";
} else {
	Debug::p( $field, 'No es pot fer servir aquest camp' );
	$show_block = false;

	return;
}


$vars['var_name'] = $field;

$query = "SELECT Distinct $field
				FROM $table
				WHERE bin = 0 AND status = 'public' AND $field <> '0' AND $field <> ''
				$condition
				ORDER BY $field";

$results = Db::get_rows( $query );


$field_value            = R::get( $field );
$vars['block_selected'] = $field_value ? 1 : 0;

if ( $results ) {
	foreach ( $results as $rs ) {

		// Poso en minúscules per que no peti la url
		$rs_field_value = strtolower( $rs[ $field ] );

		( $field_value == $rs_field_value ) ? $selected = 1 : $selected = 0;

		// pel filtre
		if ( isset( $_GET[ $field . 's' ] ) &&
		     in_array( $rs_field_value, $_GET[ $field . 's' ] )
		) {
			$selected               = 1;
			$vars['block_selected'] = 1;
		}


		$l['link'] = get_link( array(
			'tool'            => 'product',
			'tool_section'    => 'sellpoint',
			'friendly_params' => array( $field => $rs_field_value ),
			'detect_page'     => $page_id ? false : true,
			'page_id'         => $page_id
		) );

		$item = $rs_field_value;
		$item = Db::get_first( "$field_name_select '$item'" );

		$l['selected'] = $selected;
		$l['value']    = $rs_field_value;
		$l['item']     = $item;
		$l['class']    = '';
		$loop2[]       = $l;
	}


	$loop2[0]['class']                     = 'first';
	$loop2[ count( $loop2 ) - 1 ]['class'] = 'last';

	$loop[] = array(
		'loop' => $loop2,
		'item' => ''
	);
}