<?

/**
 * LassdiveReserva
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2018
 * @version $Id$
 * @access public
 */
class LassdiveReserva extends Module {

	var $search, $excel, $order_module, $is_search, $condition;

	public function __construct() {
		parent::__construct();
	}

	public function list_records() {

		Main::load_class( 'lassdive', 'reserva_list', 'admin' );

		$reserves_listing           = new LassdiveReservaList();
		$reserves_listing->is_admin = true;
		$reserves_listing->list_records( $this );

	}

	public function show_form() {

		Main::load_class( 'lassdive', 'reserva_show', 'admin' );

		$reserves_show           = new LassdiveReservaShow();
		$reserves_show->is_admin = true;
		$reserves_show->show_form( $this );

	}

	public function save_rows() {

		Main::load_class( 'lassdive', 'reserva_save', 'admin' );

		$reserves_show           = new LassdiveReservaSave();
		$reserves_show->is_admin = true;
		$reserves_show->save_rows();

	}


	public function write_record() {

		Main::load_class( 'lassdive', 'reserva_save', 'admin' );

		$reserves_show           = new LassdiveReservaSave();
		$reserves_show->is_admin = true;
		$reserves_show->write_record();

	}

	function get_search_results_products() {

		Main::load_class( 'lassdive', 'reserva_ajax' );
		LassdiveReservaAjax::get_checkboxes_results_products( true );

	}

	function get_checkboxes_long_values() {
		$this->get_search_results_users();
	}

	function get_search_results_users() {

		Main::load_class( 'lassdive', 'reserva_ajax' );
		$condition = LassdiveCommon::get_query_all_users();
		LassdiveReservaAjax::get_checkboxes_results_users( $condition );

	}

	function get_search_results_centres() {

		Main::load_class( 'lassdive', 'reserva_ajax' );
		LassdiveReservaAjax::get_checkboxes_results_centres();

	}

	public function get_timetable() {

		$reservaitem_id = R::id( 'reservaitem_id' );

		Main::load_class( 'lassdive', 'reserva_timetable', 'admin' );

		$timetable           = new LassdiveReservaTimetable ( $this->caption );
		$timetable->is_admin = true;
		$ret                 = $timetable->get_table( $reservaitem_id );

		json_end( $ret );

	}

	public function get_user_ids_popup() {

		$reservaitem_id = R::text_id( 'reservaitem_id' );

		Main::load_class( 'lassdive', 'reserva_timetable', 'admin' );

		$timetable            = new LassdiveReservaTimetable ( $this->caption );
		$timetable->is_admin = true;
		$ret                  = $timetable->get_user_ids_popup( $reservaitem_id );

		json_end( $ret );

	}

	function save_timetable_ajax() {

		Main::load_class( 'lassdive', 'reserva_timetable', 'admin' );
		LassdiveReservaTimetable::save_timetable_ajax( $this->caption );

	}

	function ____________genera_lassdive_orederitems() {
		$query   = "SELECT orderitem_id, quantity FROM product__orderitem ORDER BY orderitem_id ASC";
		$results = Db::get_rows( $query );
		$query   = '';
		$conta   = 1;
		foreach ( $results as $rs ) {
			$quantity     = (int) $rs['quantity'];
			$orderitem_id = $rs['orderitem_id'];
			for ( $x = 0; $x < $quantity; $x ++ ) {
				$query .= "INSERT INTO `lassdive__orderitem` (`orderitem_id`, `is_done`) VALUES ('$orderitem_id', '1');\n";
				echo "$conta - INSERT INTO `lassdive__orderitem` (`orderitem_id`, `is_done`) VALUES ('$orderitem_id', '1');<br>";
			}
			echo "<br><br>";
			$conta ++;
		}
		db::multi_query( $query );
		die();
	}
}