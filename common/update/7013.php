<?

/* 9-5-17
  Actualitzacions inmo agents

 */

if ( is_table( 'user__user' ) && ! is_field( 'user__user', 'latitude' ) ) {

	$update_sql[] = "
		ALTER TABLE `user__user`
			ADD COLUMN `latitude` DECIMAL(9,7) NOT NULL AFTER `bin`,
			ADD COLUMN `longitude` DECIMAL(9,7) NOT NULL AFTER `latitude`,
			ADD COLUMN `zoom` TINYINT(2) NOT NULL AFTER `longitude`,
			ADD COLUMN `center_latitude` DECIMAL(9,7) NOT NULL AFTER `zoom`,
			ADD COLUMN `center_longitude` DECIMAL(9,7) NOT NULL AFTER `center_latitude`;";

} else {
	if ( is_table( 'user__user' ) ) {
		trigger_error( 'Ja existeixen el camp latitude' );
	}
}
?>