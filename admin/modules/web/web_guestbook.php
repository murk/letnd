<?

/**
 * WebGuestbook
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 3/12/2013
 * @version $Id$
 * @access public
 */
class WebGuestbook extends Module {

	function __construct() {
		parent::__construct();
	}

	function list_records() {
		$GLOBALS['gl_content'] = $this->get_records();
	}

	function get_records() {
		$listing = new ListRecords( $this );
		$listing->add_swap_edit();

		$listing->set_field( 'prefered_language', 'select_caption', $this->caption['c_prefered_language'] );
		$listing->set_field( 'status', 'select_caption', $this->caption['c_status'] );

		$listing->add_filter( 'status' );
		$listing->add_filter( 'destacat' );
		if ( isset( $_GET['destacat'] ) && $_GET['destacat'] != 'null' ) {
			$listing->set_field( 'destacat', 'list_admin', 'input' );
		}
		$listing->add_filter( 'prefered_language' );

		$listing->order_by = 'entered DESC';

		return $listing->list_records();
	}

	function show_form() {
		$GLOBALS['gl_content'] = $this->get_form();
	}

	function get_form() {
		$show = new ShowForm( $this );

		return $show->show_form();
	}

	function save_rows() {
		$save_rows = new SaveRows( $this );
		$save_rows->save();
	}

	function write_record() {
		$writerec = new SaveRows( $this );
		$writerec->save();
	}

	function manage_images() {
		$image_manager = new ImageManager( $this );
		$image_manager->execute();
	}
}