
<!-- IGUAL LIST.TPL excepte 'is_pdf' i print -->
<?if (!$results):?>
	<?if(!$is_pdf):?>
	<?if ($options_bar):?>
	<div id='opcions_holder'><table width="100%" border="0" cellspacing="0" cellpadding="0" id="opcions">
		<tr>
			<td class="opcions"><div id="opcions_fixed_bar"><div class="more"></div></div><table class="filters">
			<tr valign="middle"><form>
					<td><?=$trimestre_filter?></td>
					</form>
				<?foreach($options_filters as $options_filter):?><form>
					<td><?=$options_filter?></td>
					</form>
				<?endforeach //$options_filters?>
				</tr></table><?if ($options_filters):?><div class="sep"></div><?endif //options_filters?>
				<table border="0" cellpadding="2" cellspacing="0"><tr valign="middle">
				<?if ($options_buttons):?>
				<td class="buttons">
				<?foreach($options_buttons as $options_button):?><?=$options_button?><?endforeach //$options_button?>
				</td><?endif //options_buttons?>
			</tr>
			</table></td>
		</tr>
	</table>
	</div>
	<?endif //$options_bar?>
	<?endif //$is_pdf?>
<?endif //!$results?>
<?if ($results):?>
	<?if(!$is_pdf):?>

<?if ($options_bar):?>
<div id='opcions_holder'><table width="100%" border="0" cellspacing="0" cellpadding="0" id="opcions">
		<tr>
			<td class="opcionsTitol"><strong><?=$c_options_bar?></strong></td>
		</tr>
		<tr>
			<td class="opcions"><div id="opcions_fixed_bar"><div class="more"></div></div><table class="filters">
				<tr valign="middle">
		<?foreach($options_move_tos as $options_move_to):?>
					<td><?=$options_move_to?></td>
			<?endforeach //$options_move_tos?><form>
					<td><?=$trimestre_filter?></td>
					</form>
		<?foreach($options_filters as $options_filter):?><form>
					<td><?=$options_filter?></td>
					</form>
			<?endforeach //$options_filters?>
				<?if($has_add_dots):?>
					<form>
					<td>
						<label>
							<?=$c_add_dots?>
							<input <?=$add_dots_checked?> type="checkbox"onchange="javascript:save_frame.window.location='<?=$add_dots_link?>'"  class="filter" name="add_dots" />
						</label>
					</td>
					</form>
				<?endif //has_add_dots?>
					</tr></table><?if ($options_filters):?><div class="sep"></div><?endif //options_filters?>
					<table border="0" cellpadding="2" cellspacing="0"><tr valign="middle">
			<?if ($options_buttons):?>
			<td class="buttons">
			<?foreach($options_buttons as $options_button):?><?=$options_button?><?endforeach //$options_button?>
			</td><?endif //options_buttons?>
		<?if ($save_button):?>
					<td><strong><a class="botoOpcionsGuardar" href="javascript:submit_form_save('save_rows_save_records<?=$action_add?>')">
						<?=$c_send_edit?>
					</a></strong></td>
			<?endif //save_button?><?if ($select_button):?>
					<td><?=$c_select?>
						&nbsp;<strong><a class="opcions" href="javascript:select_all(true, document.list)">
							<?=$c_all?>
							</a>&nbsp;-&nbsp;<a class="opcions" href="javascript:select_all(false, document.list)">
								<?=$c_nothing?>
							</a></strong></td>
			<?endif //select_button?><?if ($delete_button):?>
					<?if ($has_bin):?>
					<?if ($is_bin):?>
					<td><strong><a class="botoOpcionsBorrar" href="javascript:submit_list('save_rows_delete_selected<?=$action_add?>', '<?=$c_confirm_deleted?>', true)">
						<?=$c_delete_selected?>
					</a></strong></td>
					<td><strong><a class="botoOpcionsGuardar" href="javascript:submit_list('save_rows_restore_selected<?=$action_add?>')">
						<?=$c_restore_selected?>
					</a></strong></td>
					<?else:?>
					<td><strong><a class="botoOpcionsBorrar" href="javascript:submit_list('save_rows_bin_selected<?=$action_add?>')">
						<?=$c_delete_selected?>
					</a></strong></td>
					<?endif // is_bin?>
					<?else:?>
					<td><strong><a class="botoOpcionsBorrar" href="javascript:submit_list('save_rows_delete_selected<?=$action_add?>', '<?=$c_confirm_deleted?>', true)">
						<?=$c_delete_selected?>
					</a></strong></td>
					<?/*<td>
							<td>&nbsp;</td><strong><a class="botoOpcions" href="/admin?menu_id=<?=$menu_id?>&sort_by=0&sort_order=<?=$sort_order0?>">
								<?=$c_move_to?>
							
						<input type="submit" name="bin_selected" value="<?=$c_delete_selected?>" class="boto1">
							</a></strong></td>*/?>
					<?endif // has_bin?><?endif //delete_button?>
			<?if ($print_button):?>
			  <td><strong><a class="botoOpcions" href="javascript:open_print_window_seguimentitem('/admin/?action=get_pdf&tool_section=seguimentitem&seguiment_id=<?=$seguiment_id?>&norma_id=<?=$norma_id?>')">
						<?=$c_print_all?>
					</a></strong></td>
			<?endif //print_button?>
			<?if ($has_swap_edit):?>
					<td><strong><a class="<?=$swap_class?>" target="save_frame" href="<?=$swap_edit_link?>">
						<?=$swap_edit_button?>
					</a></strong></td>
			<?endif //has_swap_edit?>
				</tr>
			</table>
			<div class="right">
               <a class="btn_form_back" href="<?=$back_to_seguimentitem?>"></a></div>			
			</td>
		</tr>
</table></div>
<div class="sep"></div>
<?endif //$options_bar?>
  <form method="post" action="<?=$form_link?>" name="list" onsubmit="return check_selected(this)" target="save_frame">

<!-- FI IGUAL LIST.TPL -->
<?endif //is_pdf?>
<input name="action" type="hidden" value="save_rows"><?=$out_loop_hidden?>
<?extract($loop[0])?>

<link rel="stylesheet" type="text/css" href="themes/inmotools/pv/styles.css" media="all" />
<div id="pv" class="<?=$pdf?>">
	<table>
	    <thead>
			<tr>
				<td colspan="<?=$colspan?>">
					<table class="pv-head">
						<tr>
							<td class="pv-head-logo"><img src="themes/inmotools/pv/logo_<?= PvCommon::get_consultor() ?>.png" width="<?= SEGUIMENT_LOGO_WIDTH ?>"/></td>
							<td class="pv-head-titol" style="background-color:<?= CORPORATE_COLOR ?>;">
								<div>
									<strong> 
											Requisits reglamentaris
										</strong>
									Fitxa - <?=$codi_lloc?>
								</div>
							</td>
							<td class="pv-head-codi">
								<div><strong><?=$codi_seguiment?></strong>
									</div>
							</td>
							<td class="pv-head-logo last"><? if ( $image_name_details ): ?><img <?= get_image_attrs( "/clients/" . PvCommon::get_consultor() . "/pv/client/images/" . $image_name_details, '', '120', '53', false ) ?> /><? endif //image?></td>
						</tr>
					</table>	
				</td>
			</tr>
			<tr class="pv-cols-head-sep">
				<td colspan="<?=$colspan?>"></td>
			</tr>
	    </thead>
		<?if($is_pdf):?>			
		<tfoot>
			<tr class="pv-foot">
				<td colspan="<?=$colspan?>">
					<?if(PvCommon::get_consultor() == 'arumsa'):?>
						<div><span>Avinguda Catalunya 96, entresòl 2. Tàrrega</span>
						<span>Tel: 973 28 33 1clie5 - info@arumsa.com</span>
						<span>http://www.arumsa.com</span></div>
					<?elseif(PvCommon::get_consultor() == 'gpmconsultors') : //?>
						<div><span>C/ Ull de llebre, 13, 1-B, P.I. Clot de Moja 08734 Olèrdola</span>
						<span>Tel: 93.817.45.24</span>
						<span>http://www.gpmconsultors.com</span></div>
					<?else: //?>
						<div><span>C/ Llibertat, 159 2-C  17820  Banyoles</span>
						<span>Tel: 972 58 33 38  Fax: 972 58 33 39</span>
						<span>http://www.consultoriapv.com</span></div>
					<?endif // arumsa?>
				</td>
			</tr>
		</tfoot>
	    <tbody>
		<?endif //is_pdf?>
		
		
		<?/* 
		//
		// CONTINGUT 
		//		
		*/?>
		
			<tr class="pv-seguiment-requisits-norma">
				<td colspan="<?=$colspan?>">
				
					<div class="pv-fitxa-titol">1 - <?=$c_titol_norma?></div>
					<?foreach($loop as $l): extract ($l)?>
					
						<?/* 
						//
						// TEXT NORMES 
						//		
						*/?>
						<?if($titol_norma):?>
						<div class="pv-fitxa-text"><?=$titol_norma?></div>
						<?endif //titol_norma?>
						
					<?endforeach //$loop?>
					
					<div class="pv-fitxa-titol">2 - <?=$c_descripcio_norma?></div>
					<?foreach($loop as $l): extract ($l)?>
						
						<?if($descripcio_norma):?>
						<div class="pv-fitxa-text"><?=$descripcio_norma?></div>
						<?endif //descripcio_norma?>
						
					<?endforeach //$loop?>
						
					<div id="comentari_client_titol" class="pv-fitxa-titol<?=$comentari_client_class?>">3 - <?=$c_comentari_client?></div>
					<div id="comentari_client" class="pv-fitxa-text-client<?=$comentari_client_class?>"><?=$comentari_client?></div>
					
					<?if($is_editable):?>
						<a class="botoOpcions pv-boto-comentari" href="javascript:pv_open_window('<?=$edit_comentari_client?>')"> <?=$c_edit_comentari_client?> </a>
					<?endif //is_editable?>
				</td>
			</tr>
			<tr class="pv-cols-head">
				<td class="first"><?=$c_requisit?></td>
				<td class="pv-small"><?=$c_evaluacio?></td>
				<td><?=$c_descripcio_requisit?></td>
				<td class="pv-small last"><?=$c_comentari?></td>
			</tr>
			<tr class="pv-cols-head-sep2">
				<td colspan="<?=$colspan?>"></td>
			</tr>
		
		<?foreach($loop as $l): extract ($l)?>
			<tr class="pv-tr-norma<?=$norma_last?>">
				<td class="pv-requisit">
					
					<table class="table-requisit">
						<tr>
							<th><?=$num_requisit?></th><td><?=$requisit?></td>
						</tr>
					</table>
					
				</td>
				<td class="pv-evaluacio <?=$evaluacio_class?>">
					<input name="seguimentitem_id[<?=$seguimentitem_id?>]" type="hidden" value="<?=$seguimentitem_id?>" />

							<? /* EVALUACIO */ ?>
							<div class="pv-evaluacio <?= $evaluacio_class ?>">
								<?= $evaluacio ?>
								<div id="texte1_evaluacio_<?= $id ?>" class="<?= $evaluacio_text_class ?>">

									<p class="pv-obertura-codi">
										<?= $no_conforme_codi_lloc ?>
									</p>
									<?if(!$is_editable):?>
										<p class="pv-obertura clearfix">
										<strong><?=$c_obertura?>:</strong>
										<?=$obertura?>
										</p>
									<?endif //is_editable?>
								</div>
								<?if($is_editable):?>
									<p class="pv-obertura margin clearfix">
									<strong><?=$c_obertura?>:</strong>
									<?=$obertura?>
									</p>
								<?endif //is_editable?>
								<div id="texte2_evaluacio_<?=$id?>" class="<?=$evaluacio_text_class?>">

									<p class="pv-tancament clearfix">
										<strong><?= $c_tancament ?>:</strong>
										<?= $tancament ?>
									</p>
									<? if ( $is_editable ): ?>

										<p class="pv-tancat">
											<?= $c_tancat ?> <?= $tancat ?>
										</p>
									<? endif //is_editable?>

									<p class="pv-tancament"><?= $evaluacio_texte ?></p>
								</div>
							</div>

							<? /* AVISOS */ ?>

							<? if ( $avis || $is_editable ): ?>
							<div class="pv-avis <?= $avis_class ?>">
								<? if ( $avis ): ?>
									<p class="clearfix">

										<strong><?= $c_avis ?></strong>
										<? if ( $is_editable ): ?>
											<?= $avis ?>
										<? endif // $is_editable ?>

										<? if ( $avis_data ): ?>
											<span id="input_avis_<?= $id ?>" class="<?= $avis_text_class ?>"><?= $avis_data ?></span>
										<? endif // $avis_data ?>
									</p>
								<? endif //$avis?>

								<div id="texte_avis_<?= $id ?>" class="<?= $avis_text_class ?>">
									<p><?= $avis_texte ?></p>
									<? if ( $is_editable ): ?>
										<p><?= $c_avis_en_tramit ?>: <?= $avis_en_tramit ?></p>
									<? endif // $avis_en_tramit ?>
								</div>
							</div>
							<? endif //$avis?>
					
				</td>
				<!-- <td class="pv-tancament">
					
							<?=$obertura?>
					
				</td>
				<td class="pv-tancament">
					
							<?=$tancament?>
					
				</td> -->
				<td>
					
							<?=$descripcio_requisit?>
					
				</td>
				<td>
					
							<?=$comentari?>
					
				</td>
			</tr>
			<? endforeach //loop?>
		</tbody>
	</table>
</div>	
		
		
<?if(!$is_pdf):?>
	


  </form>
<?endif //is_pdf?>
<?endif //results?>