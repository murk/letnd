<?
/**
 * ColomColom
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2011
 * @version $Id$
 * @access public
 */
class ColomColom extends Module{

	function __construct(){
		parent::__construct();
	}
	function on_load(){
	    $this->table = 'all__configpublic';
	    $this->id_field = 'configpublic_id';
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function get_records()
	{
		$listing = new ShowConfig($this);
	    return $listing->show_config();
	}

	function save_rows()
	{
	    $save_rows = new SaveRows($this);
	    $save_rows->save();
	}
}
?>