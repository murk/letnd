<?php

if (!defined('TITLE_COMARCA')){
	define('TITLE_COMARCA', 'Propiedades en: ');
	define('TITLE_PRICE', 'Propiedades hasta %s '.CURRENCY_NAME);
	define('TITLE_PRICE_2', 'Propiedades de %s a %s '.CURRENCY_NAME);
	define('TITLE_PRICE_3', 'Propiedades de más de %s '.CURRENCY_NAME);

	define('TITLE_FLOORSPACE', 'Propiedades hasta %s m<sup>2</sup>');
	define('TITLE_FLOORSPACE_2', 'Propiedades de %s a %s m<sup>2</sup>');
	define('TITLE_FLOORSPACE_3', 'Propiedades de más de %s m<sup>2</sup>');

	define('TITLE_PERSON', 'Hasta %s personas');
	define('TITLE_PERSON_2', 'De %s a %s personas');
	define('TITLE_PERSON_3', 'Más de %s personas');
}

// NO TRADUIT
$gl_caption['c_property_list__view_all'] = 'Ver todos los inmuebles visitados';
// FI NO TRADUIT
?>