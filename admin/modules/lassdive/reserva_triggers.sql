### ACTIVITATS
DELIMITER //
CREATE TRIGGER `z_lassdive_activitat_delete`
AFTER DELETE ON `lassdive__activitat`
FOR EACH ROW
  BEGIN
    DELETE FROM product__family
    WHERE family_id = OLD.activitat_id;
    DELETE FROM product__family_language
    WHERE family_id = OLD.activitat_id;
  END
//
DELIMITER ;

### ACTIVITATS
DELIMITER //
CREATE TRIGGER `z_lassdive_activitat_insert`
AFTER INSERT ON `lassdive__activitat`
FOR EACH ROW INSERT INTO product__family (family_id, parent_id) VALUES (NEW.activitat_id, NEW.parent_id)
//
DELIMITER ;

### ACTIVITATS
DELIMITER //
CREATE TRIGGER `z_lassdive_activitat_language_insert`
AFTER INSERT ON `lassdive__activitat_language`
FOR EACH ROW INSERT INTO product__family_language (family_id, family, family_description, language)
VALUES (NEW.activitat_id, NEW.activitat, NEW.subtitle, NEW.language)
//
DELIMITER ;

### ACTIVITATS
DELIMITER //
CREATE TRIGGER `z_lassdive_activitat_language_update`
AFTER UPDATE ON `lassdive__activitat_language`
FOR EACH ROW UPDATE product__family_language
SET product__family_language.family           = NEW.activitat,
  product__family_language.family_description = NEW.subtitle
WHERE product__family_language.family_id = NEW.activitat_id
      AND product__family_language.language = NEW.language

//
DELIMITER ;

### ACTIVITATS
DELIMITER //
CREATE TRIGGER `z_lassdive_activitat_update`
AFTER UPDATE ON `lassdive__activitat`
FOR EACH ROW UPDATE product__family
SET product__family.parent_id = NEW.parent_id
WHERE product__family.family_id = NEW.activitat_id
//
DELIMITER ;

## Insertar un resgistre per cada quantitat de orderitem
DELIMITER //
CREATE TRIGGER `z_lassdive_orderitem_insert`
AFTER INSERT ON `product__orderitem`
FOR EACH ROW
  BEGIN
    DECLARE x INT DEFAULT 0;
    IF (new.quantity > 0)
    THEN
      WHILE x < new.quantity DO
        INSERT INTO lassdive__orderitem (orderitem_id) VALUES (NEW.orderitem_id);
        SET x = x + 1;
      END WHILE;
    ELSE
      INSERT INTO lassdive__orderitem (orderitem_id) VALUES (NEW.orderitem_id);
    END IF;

  END;
//
DELIMITER ;


## Borrar relacionats a lassdive__orderitem
DELIMITER //
CREATE TRIGGER `z_lassdive_orderitem_delete`
AFTER DELETE ON `product__orderitem`
FOR EACH ROW
  BEGIN
    DELETE FROM lassdive__orderitem
    WHERE orderitem_id = OLD.orderitem_id;
  END
//
DELIMITER ;


## Borrar relacionats a lassdive__reservaitem_to_user
DELIMITER //
CREATE TRIGGER `z_lassdive_reservaitem_delete`
AFTER DELETE ON `lassdive__orderitem`
FOR EACH ROW
  BEGIN
    DELETE FROM lassdive__reservaitem_to_user
    WHERE reservaitem_id = OLD.reservaitem_id;
  END
//
DELIMITER ;