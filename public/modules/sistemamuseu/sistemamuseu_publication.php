<?
/**
 * SistemamuseuPublication
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2010
 * @version $Id$
 * @access public
 */
class SistemamuseuPublication extends Module{

	function __construct(){
		global $gl_show_block_always;
		$GLOBALS['gl_show_block_page_recursive']=true;
		parent::__construct();
		$this->parent = 'publication';
		
		if ($this->process != 'destacat'){
			$this->set_friendly_params(array('publicationkind_id'=>$publicationkind_id));
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function get_records()
	{
	    $listing = new ListRecords($this);
		$listing->call_function['description'] = 'add_dots';			
		$listing->order_by = 'title ASC';
		
		if ($this->process == 'destacat'){	
			//$listing->condition .= "AND in_home=1 AND status='public'";				
			$listing->order_by = 'destacat DESC, ordre ASC, title ASC';
			$listing->limit = 4;
			$listing->template='sistemamuseu/destaquem.tpl';			
		}
		elseif (!isset($_GET['publicationkind_id'])){
			$query = "
			SELECT sistemamuseu__publicationkind.publicationkind_id AS id, publicationkind as item
			FROM sistemamuseu__publicationkind 
			LEFT OUTER JOIN sistemamuseu__publicationkind_language using (publicationkind_id) 
			WHERE sistemamuseu__publicationkind.bin = 0
			AND sistemamuseu__publicationkind_language.language = '".LANGUAGE."'
			AND EXISTS(
				SELECT publicationkind_id 
				FROM sistemamuseu__publication_to_publicationkind 
				INNER JOIN sistemamuseu__publication USING (publication_id)
				WHERE sistemamuseu__publication_to_publicationkind.publicationkind_id =
					sistemamuseu__publicationkind.publicationkind_id
					AND sistemamuseu__publication.bin = 0
					AND sistemamuseu__publication.status = 'public'
					LIMIT 1
				)
			ORDER BY publicationkind LIMIT 1";
			$publicationkind_id = Db::get_first($query);
			if (!$publicationkind_id) $publicationkind_id=0;
		}
		else{
			$publicationkind_id = R::id('publicationkind_id');
		}
		$listing->condition = "status='public' AND parent_section = '" . $this->parent . "'";
		if ($this->process != 'destacat'){
			$listing->before_table = 'sistemamuseu__publication_to_publicationkind, ';	
			$listing->condition .= 'AND
				sistemamuseu__publication.publication_id = sistemamuseu__publication_to_publicationkind.publication_id
					AND sistemamuseu__publication_to_publicationkind.publicationkind_id = ' . $publicationkind_id . " AND status='public'";
		}
		$listing->list_records(false);
		
		$page_id=$this->parent=='expert'?'7':'4';
		
		if ($this->process == 'destacat'){
			foreach ($listing->results as $key=>$val){
				$rs = &$listing->results[$key];
				$rs['exhibition_title'] = &$rs['title'];
				$rs['exhibition_subtitle'] = &$rs['subtitle'];
				$rs['exhibition_description'] = &$rs['description'];
				$rs['kind'] = &$rs['publicationkind'];
				$rs['museu_link'] = sistemamuseu_get_museu_link($rs['museu_id']);
				//$rs ['destacat_link'] = '?tool=sistemamuseu&amp;tool_section='.$this->parent.'&amp;action=show_record&amp;publication_id='.$rs['publication_id'].'&amp;language='.LANGUAGE.'&amp;page_id='.$page_id.'&amp;publicationkind_id='. $this->get_publicationkind($rs['publication_id']);	
				
				$rs ['destacat_link'] = '/' . LANGUAGE . '/sistemamuseu/'.$this->parent.'/'.$rs['publication_id'].'/publicationkind_id/'. $this->get_publicationkind($rs['publication_id']) . '/' . get_page_file_name($page_id);	
			}
		}
		else{
			foreach ($listing->results as $key=>$val){
				$rs = &$listing->results[$key];
				$rs['museu_link'] = sistemamuseu_get_museu_link($rs['museu_id']);					
			}		
		}
		return $listing->parse_template();
	}
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{
	    $show = new ShowForm($this);
		$show->condition = "status = 'public'";
		$show->get_values();		
		if ($show->has_results)
		{
			$GLOBALS['gl_page']->page_title = $show->rs['title'];
			$GLOBALS['gl_page']->page_description = add_dots_meta_description($show->rs['description']);
			$show->set_var('addthis_description', htmlspecialchars ($show->rs['description']) );
			$show->set_var('museu_link', sistemamuseu_get_museu_link($show->rs['museu_id']) );
			return $show->show_form();
		}
		else
		{
			$this->do_action('list_records');
		}
		
	}
	function get_publicationkind($id){	

		$query = "
			SELECT publicationkind_id
			FROM sistemamuseu__publication_to_publicationkind 
			WHERE publication_id = ". $id;
		return Db::get_first($query);
	}
}
?>