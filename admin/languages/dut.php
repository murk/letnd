<?
define("MENU_HOME_PUBLIC","Web");
define("MENU_HOME","Homepage");
define("MENU_LOGOUT","Log out");


// configurable
define("USER_TEXT","user");
define('USER_LOGIN_INVALID','The user name or password are not valid');
define('USER_LOGIN_VALID','Wellcome to ' . HOST_URL);

// la linea de sota no es pot tocar, que te dit que no la toquis!!!!!!!
// menus
define('EXPERIENCIA_MENU_ROOT','Experience');
define('NEWSLETTER_MENU_ROOT','Newsletter');
define('TRANSLATION_MENU_ROOT','Translations');
define('FACTURACIO_MENU_ROOT','Invoicing');
define('STATISTICS_MENU_ROOT','Statistics');
define('USER_MENU_ROOT','Users');
define('ADMINTOTAL_MENU_ROOT','Admin total');
define('WEB_MENU_ROOT','Letnd management');
define('INMO_MENU_ROOT','Properties');
define('PRINT_MENU_ROOT','Print');
define('OWNER_MENU_ROOT','Owners');
define('REPORT_MENU_ROOT','Documents');
define('CONFIG_MENU_ROOT','Configuration');
define('NEWS_MENU_ROOT','News');
define('GALERY_MENU_ROOT','Image gallery');
define('RENOVATS_MENU_ROOT','Gift postcards');
define('SHOPWINDOW_MENU_ROOT','Shop windows');
define('UTILITY_MENU_ROOT','Utilities');
define ('MLS_MENU_ROOT','MLS');
define('CUSTUMER_MENU_ROOT','Customers');
define('WORKS_MENU_ROOT','Works');
define('PRODUCT_MENU_ROOT','Products');
define('CANXIQUET_MENU_ROOT','Menus');
define('RATE_MENU_ROOT','Rates');



define('ESPECTACLES_MENU_ROOT','Shows');
define('ESPECTACLES_MENU_ESPECTACLES','Shows');
define('ESPECTACLES_MENU_LIST','List of shows');
define('ESPECTACLES_MENU_LIST_BIN','Recycle bin');
define('ESPECTACLES_MENU_NEW','Insert show');


define('ACTUALITAT_MENU_ESDEVENIMENT','Events');
define('ACTUALITAT_MENU_ESDEVENIMENT_LIST','List of events');
define('ACTUALITAT_MENU_ESDEVENIMENT_LIST_SEND','Send event');
define('ACTUALITAT_MENU_ESDEVENIMENT_LIST_BIN','Recycle bin');
define('ACTUALITAT_MENU_ESDEVENIMENT_NEW','Insert event');
define('ACTUALITAT_MENU_NOTICIA','News');
define('ACTUALITAT_MENU_NOTICIA_LIST','List of news');
define('ACTUALITAT_MENU_NOTICIA_LIST_BIN','Recycle bin');
define('ACTUALITAT_MENU_NOTICIA_NEW','Insert news');
define('ACTUALITAT_MENU_ROOT','Lastest news');

define('FITXES_MENU_FITXES','Record');
define('FITXES_MENU_LIST','List of records');
define('FITXES_MENU_LIST_SEND','Send record');
define('FITXES_MENU_LIST _BIN','Recycle bin');
define('FITXES_MENU_NEW','Insert record');
define('FITXES_MENU_ROOT','Records');


define('CARTA_MENU_ROOT','Menus');
define('CARTA_MENU_PLATS','Menu');
define('CARTA_MENU_PLATS_NEW','Insert dish');
define('CARTA_MENU_PLATS_LIST','Menu');
define('CARTA_MENU_PLATS_BIN','Recycle bin');

define('CARTA_MENU_MENUS','Set menus');
define('CARTA_MENU_MENUS_NEW','Insert sert menu');
define('CARTA_MENU_MENUS_LIST','Set menus');
define('CARTA_MENU_MENUS_BIN','Recycle bin');


define('MENUDELDIA_MENU_ROOT','Daily menu');
define('MENUDELDIA_MENU_MENUDELDIA','Daily menu');
define('MENUDELDIA_MENU_MENUDELDIA_MAIL_END','Send');




define('DB_SPLIT_TITLE_FIRST_PAGE', 'Start');
define('DB_SPLIT_TITLE_PREVIOUS_PAGE', 'Vorige pagina');
define('DB_SPLIT_TITLE_NEXT_PAGE', 'Volgende pagina');
define('DB_SPLIT_TITLE_LAST_PAGE', 'Einde');
define('DB_SPLIT_TITLE_PAGE_NO', 'Pagnia %d');
define('DB_SPLIT_TITLE_PREV_SET_OF_NO_PAGE', 'Vorige %d paginas');
define('DB_SPLIT_TITLE_NEXT_SET_OF_NO_PAGE', 'Volgende %d paginas');
define('DB_SPLIT_BUTTON_FIRST', '&lt;&lt;BEGIN');
define('DB_SPLIT_BUTTON_PREV', '&lt;&lt;&nbsp;Vorige');
define('DB_SPLIT_BUTTON_NEXT', 'Volgende&nbsp;&gt;&gt;');
define('DB_SPLIT_BUTTON_LAST', 'EINDE&gt;&gt;');
define('DB_SPLIT_DISPLAY_NUMBER_OF_RESULTS', 'Tonen van <b>%d</b> tot <b>%d</b> (van <b>%d</b>)');
define ('TITLE_SEARCH','Zoekresultaten');

define('DB_SPLIT_PAGE_TITLE', 'Pagina');
define('DB_SPLIT_PAGE_DESCRIPTION', 'Tonen van %d tot %d van %d');

$month_names = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
$week_names = array("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday");


// Caption default
$gl_caption['c_form_title'] = 'Edit data';
$gl_caption['c_print_title'] = 'Data';
$gl_caption['c_config_title'] = 'Edit configuration';
$gl_caption['c_edit'] = 'Edit details';


$gl_caption['c_edit_button'] = 'Edit data';
$gl_caption['c_view_button'] = 'View data';
$gl_caption['c_edit_images_button'] = 'Edit images';
$gl_caption['c_add_images_button'] = 'Add images';



$gl_caption['c_edit_data'] = 'Edit data';
$gl_caption['c_edit_images'] = 'Edit images';
$gl_caption['c_add_images'] = 'Add images';
$gl_caption['c_search_form'] = 'Search';
$gl_caption['c_send_new'] = 'Save';
$gl_caption['c_send_edit'] = 'Save changes';
$gl_caption['c_delete'] = 'Delete';
$gl_caption['c_action'] = 'Action';
$gl_caption['c_delete_selected'] = 'Delete selected';
$gl_caption['c_restore_selected'] = 'Restore selected';
$gl_caption['c_confirm_deleted'] = 'The selected elements will be completely deleted from the database \\n\\nDo you wish to continue?';
$gl_caption['c_confirm_images_deleted'] = 'The selected images will be completely deleted from the database \\n\\nDo you wish to continue?';
$gl_caption['c_confirm_form_bin'] = 'The record will be sent to the recycle bin \\n\\nDo you wish to continue?';
$gl_caption['c_confirm_form_deleted'] = 'The record will be completely deleted from the database \\n\\nDo you wish to continue?';
$gl_caption['c_move_to'] = 'Move to:';
$gl_caption['c_print_all'] = 'Print all';
$gl_caption['c_print'] = 'Print';
$gl_caption['c_order_by'] = 'Sort by:';
$gl_caption['c_select'] = 'Select';
$gl_caption['c_multislect_selectable'] = 'Fields';
$gl_caption['c_multislect_selected'] = 'Selected fields';
$gl_caption['c_all'] = 'all';
$gl_caption['c_nothing'] = 'nothing';
$gl_caption['c_preview'] = 'Preview';
$gl_caption['c_yes'] = 'Ja';
$gl_caption['c_no'] = 'Neen';
$gl_caption['c_options_bar'] = 'Options';
$gl_caption['c_file_maximum'] = 'maximum %s files at a time';
$gl_caption['c_file_maximum_1'] = 'maximum 1 file at a time';

// buscadors
$gl_caption['c_search_title'] = 'Buscar';
$gl_caption['c_by_ref'] = '(o referències separades per espais, ej: "456 245 45")';
$gl_caption['c_by_words'] = 'Per paraules';

// sobreescrits caption images
$gl_caption_image['c_edit'] = 'Images from';
$gl_caption_image['c_insert'] = 'Add images from your computer';
$gl_caption_image['c_delete_selected'] = 'Delete selected images';
$gl_caption_image['c_confirm_deleted'] = 'The images with be completely deleted.\\n\\n\\nDo you wish to continue?';
$gl_caption_image['c_select_all'] = 'Select all';
$gl_caption_image['c_deselect_all'] = 'Deselect all';

// nous images
$gl_caption_image['c_stop'] = 'Stop';
$gl_caption_image['c_refresh'] = 'Refresh';
$gl_caption_image['c_add'] = 'Add images';
$gl_caption_image['c_thumbnails'] = 'Images';
$gl_caption_image['c_icons'] = 'Icons';
$gl_caption_image['c_list'] = 'List';
$gl_caption_image['c_details'] = 'Details';
$gl_caption_image['c_progress_dialog_title_text'] = 'Adding images';

// camps images
$gl_caption_image['c_ordre'] = 'order';
$gl_caption_image['c_main_image'] = 'main';
$gl_caption_image['c_name'] = 'Name';
$gl_caption_image['c_imagecat_id'] = 'Category';
$gl_caption_image['c_image_alt'] = 'Alt';
$gl_caption_image['c_image_title'] = 'Title';

// sobreescrits caption traduccions i traduccions_news.tpl
$gl_caption_translation['c_send_edit'] = 'Submit for translation';
$gl_caption_translation['c_page_words_title'] = 'Total no. of words for translation on this page';
$gl_caption_translation['c_all_page_words_title'] = 'Total no. of words for translation on all the pages';
$gl_caption_translation['c_total'] = 'Total';

// sobreescrits end


// Message default
$gl_messages['title_edit']='Edit';
$gl_messages['title_copy']='Copy details';
$gl_messages['added']='The record has been successfully added.';
$gl_messages['add_exists']='A record with this "%s" already exists';
$gl_messages['saved']='The record has been modified successfully';
$gl_messages['not_saved']='The record has not been changed';
$gl_messages['no_records']='There is no record in this listing';
$gl_messages['no_records_bin']='There is no record in the recycle bin';
$gl_messages['list_saved']='The changes to the records have been carried out successfully';
$gl_messages['list_not_saved']='No record has been changed. No change has been detected';
$gl_messages['list_deleted']='The records have been deleted';
$gl_messages['list_bined']='The records have been moved to the recycling bin';
$gl_messages['list_restored']='The records have been successfully restored';
$gl_messages['concession_no_acces']='You do not have permission for this section';
$gl_messages['concession_no_write']='You do not have write permission for this section';
$gl_messages['select_item']='Click on a record in the list to select';
$gl_messages['related_not_deleted']='The marked records cannot be deleted, because they have related records';



// sobreescrits message image
$gl_messages_image['list_saved']='The changes to the images have been carried out successfully.';
$gl_messages_image['list_not_saved']='No record has been changed. No change has been detected';
$gl_messages_image['list_deleted']='The images have been deleted';
// sobreescrits end


$gl_caption['c_login'] = 'Login';
$gl_caption['c_password'] = 'Password';
$gl_caption['c_mail'] = 'E-mail';
$gl_caption['c_mail_repeat'] = 'Repeat E-mail';
$gl_caption['c_password_repeat'] = 'Repeat password';



// SOCIAL
$gl_caption['c_list_view_1'] = 'Normal';
$gl_caption['c_list_view_2'] = 'Big icons';
$gl_caption['c_list_view_3'] = 'List';
$gl_caption['c_edit_social'] = 'Xarxes socials';
$gl_caption['c_edit_social_title'] = 'Publicar';

// FI NO TRADUIT


$gl_caption['c_file_manager']=	'File manager';
$gl_messages['no_records_search']='No match found for this search';
$gl_caption['c_back'] = 'Back';

$gl_caption['c_asc'] = 'asc';
$gl_caption['c_desc'] = 'desc';

$gl_caption['c_reply'] = 'Respondre';

$gl_caption['c_file_file']='File';
$gl_caption['c_file_title']='Title';
$gl_caption['c_file_show_home']='In list';
$gl_caption['c_file_ordre']='Order';
$gl_caption['c_file_public']='Public';
$gl_caption['c_file_filecat_id']='Cat.';

$gl_caption['c_entered'] = 'Created';
$gl_caption['c_modified'] = 'Modified';