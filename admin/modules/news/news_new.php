<?
/**
 * NewsNew
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2010
 * @version $Id$
 * @access public
 */
class NewsNew extends Module{
	var $is_newsletter = false;
	function __construct(){
		parent::__construct();
	}
	function on_load(){
		if (strpos(HOST_URL,'aixequempersianes')===false){
			
			// social
			Main::load_class('social');
			$this->form_tabs = 
					array('0' => 
						array(
							'tab_action'=>'show_form_social&amp;menu_id=' . $GLOBALS['gl_menu_id'],
							'tab_caption'=>$this->caption['c_edit_social']
						)
					);
		}

		if (strpos(HOST_URL,'blunik')!==true){
			$this->set_field_position( 'file', 12 );
		}

		if (!Db::get_first('SELECT count(*) FROM news__formtpl LIMIT 1')) {
			$this->unset_field( 'formtpl_id' );
		}

		if ($this->config['has_files_show_home'] == '1')
			$this->set_field( 'file', 'has_show_home', '1' );

	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function get_records()
	{
	    $listing = new ListRecords($this);
		
		
		$listing->call('records_walk','content,content_list', true);
		//$listing->add_button ('edit_content_button', 'action=show_form_copy', 'boto1');

        switch ($this->process) { // process-> propietat invetada que passo pel post del tpl
            case 'select_news': // llistat dins iframe per seleccionar productes
                $page = &$GLOBALS['gl_page'];
                $page->template = 'clean.tpl';
                $page->title = '';
				$this->is_newsletter = true;
				$this->set_field('content', 'list_admin', '0');
                $listing->set_options(0, 0, 0, 0, 0, 0, 0, 0, 1);
				$listing->condition = "(status = 'public')
							AND (entered=0 OR entered <= now())
							AND (expired=0 OR expired > (curdate()))";
                $GLOBALS['gl_message'] = $GLOBALS['gl_messages']['select_item'];

                break;
        	default:
        } // switch*/
	
		if (isset($_GET['selected_menu_id']))
		{
			$listing->condition = 'category_id = ' . $_GET['category_id'];
		}
		else
		{
		$listing->add_filter('category_id');
		}
		$listing->add_filter('status');
		
		$listing->add_filter('in_home');
		$listing->add_filter('destacat');
		
		// social
		if (!$this->is_newsletter && strpos(HOST_URL,'aixequempersianes')===false)
			Social::init_listing($listing);
		
		if (isset($_GET['in_home']) && $_GET['in_home']!='null') {
			$listing->set_field('in_home','list_admin','input');
		}
		if (isset($_GET['destacat']) && $_GET['destacat']!='null') {
			$listing->set_field('destacat','list_admin','input');
		}
		
		$listing->order_by = 'ordre ASC, entered DESC';		
	    return $listing->list_records();
	}
	function records_walk(&$listing, $content=false,$content_list=false){		
		
		if ($this->is_newsletter){
			$ret = $this->list_records_walk_select ($listing->rs['new_id'], $listing->rs['new']);
		}
		
		// si es llistat passa la variable content
		if ($content!==false){
			$ret['content']=$content_list?$content_list:add_dots('content', $content);
		}
		// social
		if (!$this->is_newsletter && strpos(HOST_URL,'aixequempersianes')===false)
			$this->set_buttons_social($listing);
		
		return $ret;
	}
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{
	    // si no hi ha cap categoria faig un redirect a entrar categoria nova
		if (!Db::get_first("SELECT count(*) FROM news__category LIMIT 0,1")){
			$_SESSION['message'] = $this->messages['no_category'];
			redirect('/admin/?menu_id=3006');
		}
		$this->set_field('entered','default_value',now(true));
		$this->set_field('modified','default_value',now(true));
		
		if ($this->config['videoframe_info_size']) $this->caption['c_videoframe'] = $this->caption['c_videoframe'] . '<br /> ( ' . $this->config['videoframe_info_size'] . ' )';
		$show = new ShowForm($this);
		$show->form_tabs = $this->form_tabs;
		$show->set_form_level1_titles('page_title');
		$show->set_options( 1, 1, 1 );

		//activo o desactivo el seo
		if (!$_SESSION['show_seo']) {
			$show->unset_field('page_title');	
			$show->unset_field('page_description');
			$show->unset_field('page_keywords');
			$show->unset_field('new_file_name');		    
			$show->unset_field('new_old_file_name');
		}
	    return $show->show_form();
	}

	function save_rows()
	{
	    $save_rows = new SaveRows($this);
	    $save_rows->save();
	}

	function write_record()
	{
	    $writerec = new SaveRows($this);
		$writerec->set_field('modified','override_save_value','now()');			
		$writerec->field_unique = "new_file_name";
		$writerec->save();
	}

	function manage_images()
	{
	    $image_manager = new ImageManager($this);
		$image_manager->form_tabs = $this->form_tabs;
	    $image_manager->execute();
	}
	
	/////////////
	// SOCIAL
	///////////////
	function show_form_social() {
		$id = R::id($this->id_field);
		$language = Social::get_language();

		$query = "
			SELECT 
				new, 
				subtitle, 
				content_list,
				content,
				new_file_name as file_name
			FROM " . $this->table . "_language 
			WHERE " . $this->id_field . "='" . $id . "'
			AND language = '" . $language . "'";
		$rs = Db::get_row($query);
		
		if (!$rs) return;
		$link = HOST_URL . $language . '/news/new/' . $id . '.htm';
		
		$facebook_params = array(
			'message' => '',
			'name' => htmlspecialchars($rs['new']),
			'caption' => htmlspecialchars($rs['subtitle']),
			'description' => htmlspecialchars( p2nl($rs['content_list']?$rs['content_list']:$rs['content']))
		);
		$twitter_params = array(
			'status' => htmlspecialchars($rs['new'])
		);
		
		$social = new Social($this, $id, $facebook_params, $twitter_params, $link);
		$GLOBALS['gl_content'] = $social->get_form();
	}
	function write_record_social() {
		$social = new Social($this);		
		$social->publish();
	}
	function set_buttons_social(&$listing) {
		
		Social::set_buttons_listing($listing);
		
	}
	
	/////////////
	// NEWSLETTER
	///////////////
    // Per poder escollir productes desde una finestra amb frame, de moment desde newsletter
    function select_frames()
    {
        $page = &$GLOBALS['gl_page'];

        $page->menu_tool = false;
        $page->menu_all_tools = false;
        $page->template = '';

        $this->set_file('news/new_select_frames.tpl');
        $this->set_vars($this->caption);
        $this->set_var('menu_id', $GLOBALS['gl_menu_id']);
        // $this->set_var('custumer_id',$_GET['custumer_id']);
        $GLOBALS['gl_content'] = $this->process();
    }
	// per posar el onclick al llistat
	function list_records_walk_select ($product_id, $product_title)
	{
		$product_title= addslashes($product_title); // cambio ' per \'
		$product_title= htmlspecialchars($product_title); // cambio nomÃƒÂ¨s cometes dobles per  &quot;
		$ret['tr_jscript'] = 'onClick="parent.insert_address(' . $product_id . ',\'' . $product_title . '\')"';
		return $ret;
	}
}
?>