<?
/**
 * InsideTour
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 1/8/2018
 * @version $Id$
 * @access public
 */
class InsideTour extends Module{
	
	function __construct(){
		parent::__construct();
	}
	function on_load(){
		parent::on_load();		
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function get_records()
	{
	    $listing = new ListRecords($this);
		$listing->call('records_walk','content', true);

		if (isset($_GET['selected_menu_id']))
		{
			$listing->condition = 'category_id = ' . $_GET['category_id'];
		}
		else
		{
		$listing->add_filter('category_id');
		}
		
		$listing->add_filter('status');		
		$listing->add_filter('in_home');
		
		$listing->group_fields = array('place');
		
		if (isset($_GET['in_home']) && $_GET['in_home']!='null') {
			$listing->set_field('in_home','list_admin','input');
		}
		
		$listing->order_by = 'ordre ASC, tour ASC, entered DESC';
		
	    return $listing->list_records();
	}
	function records_walk(&$listing, $content=false,$content_list=false){		
		
		$ret = array();
		
		// si es llistat passa la variable content
		if ($content!==false){
			$ret['content']=$content_list?$content_list:add_dots('content', $content);
		}
		return $ret;
	}
	
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{	
		$this->set_field('entered','default_value',now(true));
		$this->set_field('modified','default_value',now(true));
		
		if ($this->config['videoframe_info_size']) $this->caption['c_videoframe'] = $this->caption['c_videoframe'] . '<br /> ( ' . $this->config['videoframe_info_size'] . ' )';
		
	    $show = new ShowForm($this);
		
		$show->set_form_level1_titles('tour','bites','duration','price','includes','url1_name','page_title');
		//$show->set_form_level2_titles('tour','url1_name');
		
	    return $show->show_form();
	}

	function save_rows()
	{
	    $save_rows = new SaveRows($this);
	    $save_rows->save();
	}

	function write_record()
	{		
	    $writerec = new SaveRows($this);
		$writerec->set_field('modified','override_save_value','now()');			
		$writerec->field_unique = "tour_file_name";
	    $writerec->save();
	}

	function manage_images()
	{
	    $image_manager = new ImageManager($this);
	    $image_manager->execute();
	}
}