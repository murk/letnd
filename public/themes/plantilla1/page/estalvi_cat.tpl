<link href="/clients/agenciaportselva/templates/styles/general.css" rel="stylesheet" type="text/css" />
<script language="JavaScript" src="/public/jscripts/ahorro.js"></script>
<script language="JavaScript" src="/public/jscripts/hcomun.js"></script>
<table width="10%" border="0" cellpadding="0" cellspacing="0" class="fonsgeneralsclars">
<form action="" name="formAhorro" id="formAhorro" >
  <tr>
    <td width="10"><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="10" height="1" /></td>
    <td><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="10" height="10" /></td>
    <td width="10"><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="10" height="1" /></td>
  </tr>
  <tr>
    <td width="10">&nbsp;</td>
    <td align="left" class="titolFitxa" ><strong class="text20verdanaboldcolor">C&Agrave;LCUL DE L'ESTALVI FISCAL</strong> </td>
    <td width="10">&nbsp;</td>
  </tr>
  <tr>
    <td width="10">&nbsp;</td>
    <td><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="10" height="10" /></td>
    <td width="10">&nbsp;</td>
  </tr>
  <tr>
    <td width="10">&nbsp;</td>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="15"><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="15" height="10" /></td>
          <td align="left"><span class="text10verdanawhite">Un pr&eacute;stec hipotecari et permet deduccions fiscals a la teva declaraci&oacute; de la renda segons la teva situaci&oacute; personal.<br>
            <br/>
            Aquesta calculadora et permet con&egrave;ixer el teu estalvi fiscal en la compra de la teva vivenda mitjan&ccedil;ant un pr&eacute;stec hipotecari en declaracions conjuntes o individuals</span></td>
        </tr>
      </table></td>
    <td width="10">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="10" height="30" /></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td width="10">&nbsp;</td>
    <td><table width="70%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="15" align="left"><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="15" height="7" /></td>
          <td align="left">Import del prestec: </td>
          <td width="10"><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="10" height="7" /></td>
          <td align="left"><span class="formsButtons">
            <input name="importeh" onfocus="borrar(this);" onkeypress="PonPuntos(event, this, 'positive',11,'c');" size="20" maxlength="10" />
            <input id="cuota" name="cuota" type="hidden" />
          </span></td>
        </tr>
        <tr>
          <td width="15" align="left">&nbsp;</td>
          <td align="left"><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="10" height="7" /></td>
          <td width="10">&nbsp;</td>
          <td align="left">&nbsp;</td>
        </tr>
        <tr>
          <td width="15" align="left">&nbsp;</td>
          <td align="left">Termini d'amortitzaci&oacute;: </td>
          <td width="10">&nbsp;</td>
          <td align="left"><span class="formsButtons"><input name="anos" class="formsButtons" onfocus="borrar(this);" value="30" size="20" maxlength="20" /> 
            anys</span></td>
        </tr>
        <tr>
          <td width="15" align="left">&nbsp;</td>
          <td align="left"><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="10" height="7" />
            <input onclick="pts('c')" value="pts" name="radio" class="formulari" style="border:0px;" type="hidden" />
            <input name="radio" type="hidden" class="formulari" style="border:0px;" onclick="euro('c')" value="euro" checked="checked" /></td>
          <td width="10">&nbsp;</td>
          <td align="left">&nbsp;</td>
        </tr>
        <tr>
          <td width="15" align="left">&nbsp;</td>
          <td align="left">Tipus d'interes </td>
          <td width="10">&nbsp;</td>
          <td align="left"><span class="formsButtons">
            <input name="interes"class="formsButtons" onfocus="borrar(this);" size="20" maxlength="4" />
          %</span></td>
        </tr>
      </table></td>
    <td width="10">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td align="left"><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="10" height="14" /></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><table width="40%" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
          <td width="19"><img src="/clients/agenciaportselva/templates/images/bcalcular.gif" alt="imprimir" width="19" height="19" border="0" onclick="return calcular_onclick('c')" style="cursor:pointer" /> </td>
          <td width="10"><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="10" height="14" /></td>
          <td>calcular</td>
          <td width="10"><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="10" height="14" /></td>
          <td width="19"><img src="/clients/agenciaportselva/templates/images/bprint.gif" alt="imprimir" width="19" height="19" border="0" onClick="javascript:window.print()" style="cursor:pointer" /></td>
          <td width="10"><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="10" height="14" /></td>
          <td>imprimir</td>
        </tr>
      </table></td>
    <td>&nbsp;</td>
  </tr>
</form>
  <tr>
    <td>&nbsp;</td>
    <td><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="10" height="14" /></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td align="left"><span class="text15verdanaboldcolor">Els teus resultats </span></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="10" height="10" /></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0"><form name="formAhorroRes" id="formAhorroRes">
        <tr>
          <td width="15" align="left"><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="15" height="7" /></td>
          <td width="20%" align="left">Deducci&oacute;: </td>
          <td width="10"><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="10" height="7" /></td>
          <td align="center">Individual</td>
          <td width="10" align="center"><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="10" height="7" /></td>
          <td align="center">Dos titulars.Conjunta </td>
          <td width="10" align="center"><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="10" height="7" /></td>
          <td align="center">Dos titulars. Individual </td>
        </tr>
        <tr>
          <td width="15" align="left">&nbsp;</td>
          <td width="20%" align="left"><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="10" height="7" /></td>
          <td width="10">&nbsp;</td>
          <td align="left">&nbsp;</td>
          <td width="10" align="left">&nbsp;</td>
          <td align="center">&nbsp;</td>
          <td width="10" align="left">&nbsp;</td>
          <td align="center">&nbsp;</td>
        </tr>
        <tr>
          <td width="15" align="left">&nbsp;</td>
          <td width="20%" align="left">Primer any : </td>
          <td width="10">&nbsp;</td>
          <td align="center"><strong>
            <input name="iprimera" type="text" id="iprimera" value="0" readonly="readonly"/>
          </strong></td>
          <td width="10" align="left">&nbsp;</td>
          <td align="center"><strong>
            <input name="dcprimera" type="text" id="dcprimera" value="0" readonly="readonly"/>
          </strong></td>
          <td width="10" align="left">&nbsp;</td>
          <td align="center"><strong>
            <input name="diprimera" type="text" id="diprimera" value="0" readonly="readonly"/>
          </strong></td>
        </tr>
        <tr>
          <td width="15" align="left">&nbsp;</td>
          <td width="20%" align="left"><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="10" height="7" /></td>
          <td width="10">&nbsp;</td>
          <td align="left">&nbsp;</td>
          <td width="10" align="left">&nbsp;</td>
          <td align="center">&nbsp;</td>
          <td width="10" align="left">&nbsp;</td>
          <td align="center">&nbsp;</td>
        </tr>
        <tr>
          <td width="15" align="left">&nbsp;</td>
          <td width="20%" align="left">Estalvi total  </td>
          <td width="10">&nbsp;</td>
          <td align="center"><strong>
            <input name="itotal" type="text" id="itotal" value="0" readonly="readonly"/>
          </strong></td>
          <td width="10" align="left">&nbsp;</td>
          <td align="center"><strong>
            <input name="dctotal" type="text" id="dctotal" value="0" readonly="readonly" />
          </strong></td>
          <td width="10" align="left">&nbsp;</td>
          <td align="center"><strong>
            <input name="ditotal" type="text" id="ditotal" value="0" readonly="readonly"/>
          </strong></td>
        </tr></form>
      </table></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="1" height="50" /></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td align="left" class="text10verdanawhite">La informaci&oacute; obtinguda en aquesta simulaci&oacute; de c&agrave;lcul &eacute;s totalment orientativa i no implica cap comprom&iacute;s, ni cap vincle jur&iacute;dic-legal. </td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="10" height="40" /></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td align="center"><table width="218" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td><a href="?page_id=8&language=cat&tipus=sell" class="noborder"><img src="/clients/agenciaportselva/templates/images/bhipotecacat.gif" alt="calcul de quotes" width="89" height="18" border="0" /></a></td>
        <td width="40"><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="40" height="1" /></td>
        <td width="19"><a href="?page_id=10&language=cat&tipus=sell" class="noborder"><img src="/clients/agenciaportselva/templates/images/bdespesescat.gif" alt="estalvi fiscal" width="89" height="18" border="0" /></a></td>
        </tr>
    </table></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="10" height="14" /></td>
    <td>&nbsp;</td>
  </tr>
</table></form>