<?php
// menus eina
if (!defined('NEWS_MENU_NEW')){
	define('NEWS_MENU_NEW', 'Nouvelles');
	define('NEWS_MENU_NEW_NEW', 'Entrez nouvelle');
	define('NEWS_MENU_NEW_LIST', 'Énumérer et modifier nouvelles');
	define('NEWS_MENU_NEW_BIN', 'Corbeille');
	define('NEWS_MENU_CATEGORY', 'Catégories');
	define('NEWS_MENU_CATEGORY_NEW', 'Entrez nouvelle catégorie');
	define('NEWS_MENU_CATEGORY_LIST', 'Énumérer les catégories');
}

$gl_messages_new['no_category']='Pour insérer nouvelles doit avoir au moins une catégorie';

// captions seccio
$gl_caption_new['c_prepare'] = 'En préparation';
$gl_caption_new['c_review'] = 'Pour examiner';
$gl_caption_new['c_public'] = 'Public';
$gl_caption_new['c_archived'] = 'Filed';
$gl_caption_new['c_new'] = 'Nouvelle';
$gl_caption_new['c_title'] = 'Titre';
$gl_caption_new['c_subtitle'] = 'Sous-titre';
$gl_caption_new['c_content'] = 'Contenu';
$gl_caption_new['c_entered'] = 'Date';
$gl_caption_new['c_expired'] = 'Expire';
$gl_caption_new['c_modified'] = 'Date de modification';
$gl_caption_new['c_status'] = 'État';
$gl_caption_new['c_category_id'] = 'Catégorie';
$gl_caption_new['c_filter_category_id'] = 'Toutes les catégories';
$gl_caption_new['c_filter_status'] = 'Tous les États';
$gl_caption_new['c_filter_in_home'] = 'Tout dans la Home';
$gl_caption_new['c_filter_destacat'] = 'Tous les soulignés';
$gl_caption_new['c_ordre'] = 'Ordre';

$gl_caption_category['c_category'] = 'Catégorie';
$gl_caption_category['c_new_button'] = 'Voir nouvelles';
$gl_caption_category['c_ordre'] = 'Ordre';
$gl_caption_image['c_name'] = 'Nom';
$gl_caption_new['c_url1_name']='Nom URL(1)';
$gl_caption_new['c_url1']='URL(1)';
$gl_caption_new['c_url2_name']='Nom URL(2)';
$gl_caption_new['c_url2']='URL(2)';
$gl_caption_new['c_url3_name']='Nom URL(3)';
$gl_caption_new['c_url3']='URL(3)';
$gl_caption_new['c_videoframe']='Vidéo (youtube, metacafe...)';
$gl_caption_new['c_file']='Archives';
$gl_caption_new['c_in_home']='À la home';
$gl_caption_new['c_in_home_0']='Non à la Home';
$gl_caption_new['c_in_home_1']='À la home';
$gl_caption_new['c_content_list']='Brève description dans la liste';
$gl_caption_new['c_destacat']='Noticia souligné';
$gl_caption_new['c_destacat_0']='Non souligné';
$gl_caption_new['c_destacat_1']='Souligné';

$gl_caption['c_new_file_name'] = $GLOBALS['gl_caption']['c_file_name'];
$gl_caption['c_new_old_file_name'] = $GLOBALS['gl_caption']['c_old_file_name'];
$gl_caption_new['c_form_level1_title_page_title'] = $GLOBALS['gl_caption']['c_seo'];

$gl_caption_new['c_selected']='Nouvelles sélectionné';

// TODO-i Traduir al frances
$gl_caption['c_listtpl_id'] = 'Listing template';
$gl_caption['c_formtpl_id'] = 'Template';

$gl_caption['c_next_new'] = "Nouvelles suivantes";
$gl_caption['c_previous_new'] = "Nouvelles précédentes";
?>
