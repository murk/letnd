<?php
function save_rows()
{
    $save_rows = new SaveRows;
    $save_rows->save();
}

function list_records()
{
 	global $gl_page, $tpl, $gl_action, $gl_content, $gl_caption, $gl_config;

	$on = "'onsale'";
	$so = "'sold'";
	$listing = new ListRecords;
	$tpl->set_var("client_dir", CLIENT_DIR);
	$listing->condition = " status = 'onsale' OR status = 'sold' ORDER BY ordre ASC";
    $listing->list_records();
	if ($listing->has_results)
    {
        return $gl_content;
    }
    else //Si no te resultas passo un tpl, conforme no te resultats
    {
		$tpl->set_file('cars/nocars.tpl');
		$gl_content = $tpl->process();
    }
}

function show_form()
{
    $show = new ShowForm;
    $show->show_form();
}

function write_record()
{
    $writerec = new SaveRows;
    $writerec->save();
}

function manage_images()
{
    $image_manager = new ImageManager;
    $image_manager->execute();
}
function list_records_images()
{

    global $gl_action, $gl_page, $tpl, $gl_db_max_results, $gl_content, $gl_has_images;
    $gl_db_max_results = 1000000; // trec la paginació, no crec que faci falta
    $gl_has_images = false; // una imatge no té una taula d'imatges relacionada :-)
    $listing = new ListRecords(); 
	$listing->set_config('product_image'); //crido el arxiu de config d'imatges	
    $listing->id_field = 'image_id';
    $listing->has_bin = false;
    $listing->cols = PRODUCT_IM_LIST_COLS; //Llistat de columnes a ensenyar

    $tpl->set_var("client_dir", CLIENT_DIR);
    $tpl->set_var("im_details_w", PRODUCT_IM_DETAILS_W);
    $tpl->set_var("im_details_h", PRODUCT_IM_DETAILS_H);
    $tpl->set_var("im_medium_w", PRODUCT_IM_MEDIUM_W);
    $tpl->set_var("im_medium_h", PRODUCT_IM_MEDIUM_H);
    $tpl->set_var("product_id", $_GET['product_id']);

    $listing->condition = "product_id = " . R::id('product_id'); //condicio per el loop
    $listing->no_records_message = "";

    $listing->call("get_file_name", "image_id,name");
    $listing->order_by = 'ordre asc';
    $listing->list_records();
    Debug::add("list records image", $gl_page);

    return $gl_content;
}

function get_file_name($image_id, $name, $status)
{
	global $gl_caption;
    $ext = strrchr(substr($name, -5, 5), '.');
    $image_details = $image_id . '_details' . $ext;
    $image_thumb = $image_id . '_thumb' . $ext;
    $image_medium = $image_id . '_medium' . $ext;

   	return array("image_name_details" => $image_details,
        "image_name_thumb" => $image_thumb,
        "image_name_medium" => $image_medium,
        "client_dir" => CLIENT_DIR);
}
?>