<?php
$tipus = isset($vars['tipus'])?$vars['tipus']:false;
$page_id = isset($vars['page_id']) ? $vars['page_id'] : false;

include_once('inmo_block_functions.php');
$condition = '';
$condition .= inmo_block_get_tipus($tipus);

$vars['var_name'] = 'place_id';

$query = "SELECT Distinct place, web__place.place_id
				FROM web__place, inmo__property
				WHERE web__place.place_id = inmo__property.place_id
				AND web__place.status = 'public'
				AND bin = 0 AND (inmo__property.status = 'onsale' OR inmo__property.status = 'sold' OR inmo__property.status = 'reserved') " . $condition . "
				ORDER BY place";

$results = Db::get_rows($query);


$place_id = R::number('place_id');
$vars['block_selected'] = $place_id?1:0;

if ($results) {
    foreach($results as $rs) {
        $place_id == $rs["place_id"]?$selected = 1:$selected = 0;


		$l['link'] = get_link(array(
				'tool' => 'inmo',
				'tool_section' => 'property',
				'friendly_params' => array('tipus'=>$tipus==INMO_DEFAULT_TIPUS?'':$tipus,'place_id'=>$rs['place_id']),
				'link_vars' => array('tipus'=>$tipus),
				'detect_page' => $page_id?false:true,
				'page_id' => $page_id
			));

        $l['selected'] = $selected;
        $l['value'] = $rs['place_id'];
        $l['item'] = $rs['place'];
		$l['class'] = '';
        $loop2[] = $l;
    }

	$loop2[0]['class'] = 'first';
	$loop2[count($loop2)-1]['class'] = 'last';

$loop[] = array ('loop' => $loop2,
    'item' => '');
}

$vars['var_name'] = 'place_id';


?>