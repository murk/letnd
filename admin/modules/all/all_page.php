<?
/**
 * AllPage
 *
 
 Versió millorada de web_page.php, l'he traspassat aquí ja que edite4m la taula all__page i abandono l'altre que nomès està posat a aimctaic i aram.
 
 Aquest modul anirà a totes les botigues i tots els productes
 
 També serà el mòdul WYSIWYG 
 
 
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2012
 * @version Versió millorada 
 * @access public 
 */
class AllPage extends Module{
	var $page_ids = array(), $menus, $levels_count = array(), $dir, $delete_button=0;
	function __construct(){
		parent::__construct();
	}
	function on_load(){
	
		$this->dir = CLIENT_PATH.'/templates/page/';
		$this->set_var('show_data_page',true);


		if (!$this->config['show_body_subtitle']){
			$this->unset_field( 'body_subtitle' );
			$this->set_var( 'body_subtitle', false );
		}

		if (!$this->config['show_body_description']){
			$this->unset_field( 'body_description' );
			$this->set_var( 'body_description', false );
		}

		return;
		
		if ($this->parent == 'WebPage'){
		
			$this->set_field('level','list_admin','0');
			$this->set_field('template','list_admin','0');
			$this->set_field('page','list_admin','text');
			$this->set_field('title','list_admin','text');
			//$this->set_field('page_file_name','list_admin','0');
			//$this->set_field('page_old_file_name','list_admin','0');
			$this->set_field('body_subtitle','list_admin','0');
			$this->set_field('body_description','list_admin','0');
			$this->set_field('ordre','list_admin','0');
			$this->set_field('has_content','list_admin','0');
			$this->set_field('menu','list_admin','0');
			$this->set_field('blocked','list_admin','0');
			$this->set_field('status','list_admin','0');
			
			$this->set_field('level','form_admin','0');
			$this->set_field('template','form_admin','0');
			$this->set_field('page','form_admin','text');
			$this->set_field('title','form_admin','text');
			//$this->set_field('page_file_name','form_admin','0');
			//$this->set_field('page_old_file_name','form_admin','0');
			$this->set_field('body_subtitle','form_admin','0');
			$this->set_field('body_description','form_admin','0');
			$this->set_field('ordre','form_admin','0');
			$this->set_field('has_content','form_admin','0');
			$this->set_field('menu','form_admin','0');
			$this->set_field('blocked','form_admin','0');
			$this->set_field('status','form_admin','0');
			
			$this->set_field('page_title','list_admin','text');
			$this->set_field('page_description','list_admin','text');
			$this->set_field('page_keywords','list_admin','text');
			
			$this->set_var('show_data_page',false);
		}
		
		
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function get_records()
	{
	
		$listing = new ListRecords($this);
		$listing->paginate = false;
		if ($this->parent == 'WebPage')
			$listing->has_images=false;
			
		$listing->set_options(1, 0, 0, 0, 0, 0, 0, 1, 1, 1);
		
		$listing->set_field('page','type','none');
		
		
		//$this->menus = '1'; // s'ha d'agafar del config
		$this->menus = isset($_GET['menu'])?$_GET['menu']:
			(isset($_SESSION['menu'])?$_SESSION['menu']:'1');
		$_GET['menu'] = $_SESSION['menu'] = $this->menus;
		$listing->add_filter ('menu');

		$bin = $listing->is_bin ? '1' : '0';

		$this->get_page_ids(0, 1, $bin);
		
		// si no hi ha cap al nivell 1, busco al nivell 2
		if (!$this->page_ids) {
			$parent_of_level = Db::get_first("
				SELECT parent_id 
				FROM all__page 
				WHERE bin = $bin
				AND menu IN (" . $this->menus . ")
				ORDER BY menu ASC, ordre ASC, page_id
				LIMIT 1");
			$this->get_page_ids($parent_of_level, 2, $bin);
		}
		
		$page_ids = $this->page_ids?implode($this->page_ids,','):'0';
		
		$listing->condition = 'all__page.page_id IN ('.$page_ids.')';
		$listing->extra_fields = 'all__page.page_id as page_id2'; // per comptabilitat mysql 4
        $listing->condition .= "ORDER BY FIELD(page_id2, " . $page_ids . ")";		

		if ($this->parent != 'WebPage'){
			$listing->add_button ('edit_page_button', 'action=show_form_edit', 'boto2');
		}
		$listing->add_button ('edit_content_button', 'action=show_form_edit', 'boto1');
		$listing->call('list_records_walk','page_id,page,level,parent_id,has_content,template',true);
		
	    return $listing->list_records();
	}
	function list_records_walk(&$listing,$page_id,$page,$level,$parent_id,$has_content,$template){
		$style = '';
		if ($level == 1) $style = "font-weight:bold;";
		if ($level > 2) $style = "color:#5e6901;";
		if ($level > 3) $style = "color:#bd6b14;";
		$key = $parent_id . '_' . $level;
		if (!isset($this->levels_count[$key])) {
			$this->levels_count[$key] = 0;
		}
		$this->levels_count[$key]++;
		$ret['page'] = '<div style="margin-left:'.(30*($level-1)).'px;'.$style.'">'.$this->levels_count[$key].' - ' .$page.'</div>';

		
		if ($this->parent != 'WebPage'){

			// $listing->buttons['edit_content_button']['show'] = ($has_content=='1');
			// $listing->buttons['edit_page_button']['show'] = false;

			$listing->buttons['edit_content_button']['show'] = ( $has_content == 1 && $template );
			$listing->buttons['edit_page_button']['show']    = ! ( $has_content == 1 && $template );
		}
		
		return $ret;
	}
	function get_page_ids($parent_id, $level, $bin){
		$query = "SELECT page_id
						FROM all__page
						WHERE parent_id = " . $parent_id . "
						AND bin = $bin
						AND menu IN (" . $this->menus . ")
						ORDER BY menu ASC, ordre ASC, page_id";
		Debug::add($query,'Query get_page_recursive');	
		$results = Db::get_rows($query);

		if ($results)
		{
			foreach($results as $rs)
			{
				$this->page_ids[]=$rs['page_id'];
				$this->get_page_ids($rs['page_id'], $level+1, $bin);
			}
		}
	}

	
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{
	    $show = new ShowForm($this);
		if ($this->parent == 'WebPage')
			$show->has_images=false;
		$show->set_options (1, $this->delete_button, 0, 0);
		
		$show->add_field('content');
		$show->set_field('content','type','html');
		$show->set_field('content','form_admin','out');
		$show->set_field('content','textarea_rows','40');
		
		$show->call('show_records_walk','page_id,has_content,template',true);
	    return $show->show_form();
	}
	function show_records_walk(&$show,$page_id,$has_content,$template){
		// contingut   -  plantilla
		//    1               1       1 - te contingut i plantilla tpl
		//    1               0       2 - te contingut però no plantilla
		//    0               0		  3 - ni contingut ni plantilla
		//    0               1		  3 - faig el mateix que el 3, no te contingut però si plantilla ( aquest cas no pot ser, està mal configurat )
		
		$show->set_var('show_seo',$_SESSION['show_seo']);
		
		if ($show->rs['link']=='/'){		
			$show->unset_field('page_file_name');
			$show->set_var('page_file_name','');
			
			$show->unset_field('page_old_file_name');
			$show->set_var('page_old_file_name','');	
		}
		elseif ($show->rs['link']=='link:') {			
			$show->set_field('page_file_name','form_admin','text');
			$show->unset_field('page_old_file_name');
		}
		
		/* 3 - ni contingut ni plantilla */
		///////////////////////////////////////
		if (!$has_content) {
			$show->unset_field('body_subtitle');
			$show->unset_field('body_description');
			$show->unset_field('content');

			$show->set_var('body_subtitle','');
			$show->set_var('body_description','');
			$show->set_var('content','');

			/* MOSTRO igualment per si acas, pot servir el títol per sortir en algun menu
			$show->unset_field('title');
			$show->set_var('title','');
			*/

			
			$show->set_var('show_seo',false);	
			
			return array();
		}
		
		/* 2 - te contingut però no plantilla */
		///////////////////////////////////////
		// ( cm ara un modul de noticies o contacte )
		
		if ($has_content && !$template) {			
			$show->unset_field('content');	
			$show->set_var('content','');
			
			return array();
		}
		
		$ret['content'] = '';

		if ($this->config['can_edit_pages'] == '0') return $ret;
		
		/* 1 - te contingut i plantilla tpl */
		///////////////////////////////////////
		
		// obtinc els camps html tal com ho faig desde les classes ( no ho puc fer automatic per que no es un camp de la bbdd si no un arxiu .tpl )
		foreach ($GLOBALS['gl_languages']['public'] as $lang){
			$cg = $show->fields['content'];
			$cg['name'] = 'content';
			$cg['form_admin'] = 'input';
			$cg['input_name'] = 'content[' . $page_id . '][' . $lang . ']';
			$cg['input_language'] = $lang;
			$content = file_get_contents($this->dir . $template.'_'. $lang .'.tpl');
			$input =  new FormObject($cg, $content, 'form', $page_id, $show);
			$ret['content'][]= array(
					'content' => $input->get_input(false),
					'lang' => $GLOBALS['gl_languages']['names'][$lang]
				);
		}
		return $ret;
	}

	function save_rows()
	{
	    $save_rows = new SaveRows($this);
	    $save_rows->save();
	}

	function write_record()
	{
	    $writerec = new SaveRows($this);
		$writerec->set_field('modified','override_save_value','now()');
	    $writerec->save();

		if ($this->config['can_edit_pages'] == '0') return;
		
		Main::load_class('file');
		foreach ($GLOBALS['gl_languages']['public'] as $lang){
			if ($writerec->get_value('content',$lang)!==false){
			$this->save_files($writerec->get_value('template').'_'.$lang, $writerec->get_value('content',$lang));
			}
		}
	}
	function save_files($template, $val)
	{
		$dir = $this->dir;
		$nom_arxiu =  $template . '.tpl';
		$f = new File();
		
		$vell = $f->read($dir . $nom_arxiu);
		//debug::p($vell);
		//debug::p($val);
		if ($vell == $val) return; // si no ha canviat res no guardo
		
		// copio arxiu vell en un nou segons la data
		if (VERSION==5) date_default_timezone_set('UTC');
		$arxiu_vell= date("d-m-Y_G-i-s") . '_' . $nom_arxiu;
		$f->file = $dir . 'old/' . $arxiu_vell;
		$f->write($vell);

		$f->file = $dir . $nom_arxiu;
		$f->write($val);

		$GLOBALS['gl_message'] = "L'arxiu s'ha guardat correctament";
		
		$this->cleanFiles($dir,$nom_arxiu);
	}
	function cleanFiles($dir,$nom_arxiu)
	{
		// Borrar los ficheros temporales
		$t = time();
		$h = opendir($dir);
		$num = 0;
		$older_file = '';
		$previous = '';
		while ($file = readdir($h))
		{
			if (substr($file, - strlen ($nom_arxiu)) == $nom_arxiu)
			{
				$num++;
				$path = $dir . '/' . $file;
				if ($older_file) {
					if (filemtime($path)<filemtime($older_file)) {
						$older_file = $path;
					}
				}
				else{
					$older_file = $path;
				}

			}
		}

		// si hi ha mes de X arxius, borro el mes vell
		if ($num>10) {
			@unlink($older_file);
		}
		closedir($h);
	}

	function manage_images()
	{
	    $image_manager = new ImageManager($this);
	    $image_manager->execute();
	}
}
?>