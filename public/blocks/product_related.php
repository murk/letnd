<?php
/*
Aquest block s'ha de cridar passant la variable desde el formulari del producte:

		<?= get_block( 'product_related', 'product_id=' . $product_id ) ?>


*/

$product_id = isset($vars['product_id'])?$vars['product_id']:'0';
$show_whole_family = isset($vars['show_whole_family'])?$vars['show_whole_family']:'0';



// si no n'hi ha cap o estic a la pàgina de veure tots els productes visitats, donc no mostro el block
if (isset($_GET['products_related_more']) || (!$product_id)) {
	$show_block = false;

	if (!$product_id){
		Debug::throw_warning("Block product_related: En aquest block s'ha de posar l'id del producte per poder trobar els relacionats");
	}

	return;
}

if (!defined('PRODUCT_RELATED_MAX_RESULTS')) Module::get_config_values('product');



$module = Module::load('product','product');
$module->parent = 'BlockListing';

if ( $show_whole_family ) {

	$query = "SELECT family_id, subfamily_id, subsubfamily_id FROM product__product WHERE product_id = '$product_id'";
	$rs    = Db::get_row( $query );

	$family_id = $rs['family_id'];
	$subfamily_id = $rs['subfamily_id'];
	$subsubfamily_id = $rs['subsubfamily_id'];


	$module->condition =
		"AND product__product.product_id IN (
	
		SELECT product2_id FROM product__product_related WHERE product_id IN
		 ( 
				SELECT product_id 
				FROM product__product 
				WHERE  family_id = '$family_id'
				AND subfamily_id = '$subfamily_id'
				AND subsubfamily_id = '$subsubfamily_id'
		)
		ORDER BY ordre
		
	)";
}
else {
	$module->condition =
		"AND product__product.product_id IN (
	
		SELECT product2_id FROM product__product_related WHERE product_id = '$product_id'
		ORDER BY ordre
		
	)";
}

// en sumo un per saber si va l'enllaç de veure més
$module->limit = '0,' . (PRODUCT_RELATED_MAX_RESULTS + 1);

$module->order_by = 'related_ordre ASC, product_title ASC';

$listing = $module->do_action('get_records');

$listing->extra_fields = "(SELECT product__product_related.ordre FROM product__product_related WHERE product__product_related.product2_id = product__product.product_id AND product_id = '$product_id' LIMIT 1) AS related_ordre";

$listing->set_records();
$loop = $listing->loop;

if (!$loop) {
	$show_block = false;
	return;
}

$vars['more'] = false;

Debug::p( [
			[ 'count($loop)', count($loop) ],
			[ 'Related max results', PRODUCT_RELATED_MAX_RESULTS ],
		], 'ProductRelatedBlock');

if (count($loop) > PRODUCT_RELATED_MAX_RESULTS)
{
	$loop  = array_slice($loop, 0, -1);

	$vars['more'] = $gl_caption['c_product_list_view_all'];

	if (USE_FRIENDLY_URL) {

		$vars['more_link'] =
					get_link( array(
								'tool' => 'product',
								'tool_section' => 'product',
								'friendly_params' => array('product_id'=>$product_id, 'product_related_more'=>'true'),
								//'link_vars' => array('tipus'=>$rs['tipus_value']), // poden ser tot barrejats, per tant no pot anar a un tipus concret,
								'detect_page' => true
						));
	}
	else {

		$vars['more_link'] = '/?tool=product&amp;tool_section=product&amp;product_visited_more=true&amp;language=' . LANGUAGE;
	}
}