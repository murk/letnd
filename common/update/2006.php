<?php
// no es poden posar ; entre mig de la consulta ja que parteix les consultes per el ;

// nous camps a propietats
$update_sql =
"
ALTER TABLE `inmo__property` 
ADD `living` TINYINT( 2 ) NOT NULL AFTER `wc` ,
ADD `dinning` TINYINT( 2 ) NOT NULL AFTER `living` ;

ALTER TABLE `inmo__property` 
ADD `service_room` TINYINT( 1 ) NOT NULL AFTER `airconditioned` ,
ADD `service_bath` TINYINT( 1 ) NOT NULL AFTER `service_room` ,
ADD `study` TINYINT( 1 ) NOT NULL AFTER `service_bath` ,
ADD `courtyard` TINYINT( 1 ) NOT NULL AFTER `study` ,
ADD `courtyard_area` INT( 5 ) NOT NULL AFTER `courtyard` ;

ALTER TABLE `inmo__property` 
ADD `parket` TINYINT( 1 ) NOT NULL AFTER `status` ,
ADD `ceramic` TINYINT( 1 ) NOT NULL AFTER `parket` ,
ADD `gres` TINYINT( 1 ) NOT NULL AFTER `parket` ,
ADD `estrato` ENUM( 'e1', 'e2', 'e3', 'e4', 'e5', 'e6' ) NOT NULL AFTER `ceramic` ;

ALTER TABLE `inmo__property` 
ADD `laminated` TINYINT( 1 ) NOT NULL AFTER `parket` ;

INSERT INTO `all__configadmin` (`configadmin_id`, `name`, `value`) VALUES (NULL, 'is_colombia', '0');
INSERT INTO `all__configpublic` (`configpublic_id`, `name`, `value`) VALUES (NULL, 'is_colombia', '0');
";
?>