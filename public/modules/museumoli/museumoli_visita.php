<?
/**
 * MuseumoliVisita
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2010
 * @version $Id$
 * @access public
 */
class MuseumoliVisita extends Module{
	var $taller=false;
	function __construct(){
		parent::__construct();
		$this->parent = "MuseumoliVisita";
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function get_records()
	{
		$listing = new ListRecords($this);
		$listing->condition = "(status = 'public') AND parent_module = '" . $this->parent. "'";
		$listing->paginate = false;
		
			Debug::p($this->parent, 'text');
		if ($this->parent =='MuseumoliExposicio'){
			$listing->order_by = 'ordre ASC, entered DESC';
			$listing->template = 'museumoli/exposicio_list';
			if (isset($_GET['action']) && $_GET['action']=='arxiu'){			
				$listing->condition = "(status = 'archived') AND parent_module = '" . $this->parent. "'";
			}
			return $listing->list_records();
		}
		elseif ($this->parent =='MuseumoliCurs'){	
			$listing->order_by = 'ordre ASC, entered DESC';		
			$listing->template = 'museumoli/curs_list';
			if (isset($_GET['action']) && $_GET['action']=='arxiu'){			
				$listing->condition = "(status = 'archived') AND parent_module = '" . $this->parent. "'";
			}
						
			$tipus_curs = R::escape('tipus');
			if ($tipus_curs){
				$listing->condition .= " AND tipus_curs = '" . $tipus_curs . "'";
			}
			return $listing->list_records();
		}
		elseif ($this->parent =='MuseumoliVisita'){	
			$listing->order_by = 'ordre ASC, entered DESC';		
			$listing->cols = 3;
			$listing->call('records_walk','content,content_list');	
			$taller_link ='/' . LANGUAGE . '/museumoli/visita/taller/' . $_GET['page_file_name'].'.html';
			
			$listing->set_records();			
			foreach ($listing->loop as $key=>$val){
				foreach ($listing->loop[$key]['loop'] as $k=>$v){
					$rs = &$listing->loop[$key]['loop'][$k];
					if ($rs['id'] == 19) $rs['show_record_link']  = $taller_link;
				}
			}
			return $listing->process();
		}
		elseif ($this->parent =='MuseumoliVisitataller'){
			$listing->order_by = 'ordre ASC, entered DESC';
			$listing->cols = 5;
			$listing->template = 'museumoli/visita_taller_list';			
			$listing->call('records_walk','content,content_list');				
			return $listing->list_records();
		}
		elseif ($this->parent =='MuseumoliColaborador'){
			$listing->order_by = 'visita ASC';
			
			$tipus_colaborador = R::escape('tipus');
			Debug::p($tipus_colaborador, 'text');
			if($tipus_colaborador=='empresa'){
				$listing->template = 'museumoli/empresa_list';		
			}
			else{
				$listing->template = 'museumoli/colaborador_list';		
			}
			
			if ($tipus_colaborador){
				$tipus_colaborador = $_GET['tipus'];
				$listing->condition .= " AND tipus_colaborador = '" . $tipus_colaborador . "'";
			}
			return $listing->list_records();
		}
		
	}
	function taller(){
		$this->parent ='MuseumoliVisitataller';
		$this->do_action('list_records');
	}
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{
		$show = new ShowForm($this);
		
		
		$show->condition = "(status = 'public')";
		$show->get_values();		
		if ($show->has_results)
		{
			if ($this->config['override_gl_page_title']) $GLOBALS['gl_page']->title = $show->rs['visita'];
			$GLOBALS['gl_page']->page_title = $show->rs['visita'];
			$GLOBALS['gl_page']->page_description = add_dots_meta_description($show->rs['content']);
			Page::add_breadcrumb($show->rs['visita']);
			
			$tipus_colaborador = $show->rs['tipus'];
			
			if ($this->parent =='MuseumoliColaborador'){			
							
				if($tipus_colaborador=='empresa'){
				}
				else{
					$show->template = 'museumoli/colaborador_form';			
				}
			}
			
			return $show->show_form();
		}
		else
		{
			Main::redirect('/');
		}
	}
	function records_walk($content=false,$content_list=false){	
		
		// si es llistat passa lavariable content
		if ($content!==false){
			$ret['content']=$content_list?$content_list:add_dots('content', $content);
		}
		//$ret['url1']=str_replace('http://','',$url1);	
		//$ret['url1']=str_replace('http://','',$url1);
		return $ret;
	}
}
?>