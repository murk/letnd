<?php
/* 21-3-19
Afegeixo mètode de pagament per compte bancari


*/

if ( is_table( 'booking__configpublic' ) && ! Db::get_first( "SELECT * FROM booking__configpublic WHERE name='banc_name'" ) ) {
	$update_sql[] = "
		INSERT INTO booking__configpublic (`name`, `value`) VALUES ('banc_name', '');";

}
else {
	if ( is_table( 'booking__configpublic' ) ) {
		trigger_error( 'Ja existeixen el camp banc_name' );
	}
}

if ( is_table( 'booking__configpublic' ) && ! Db::get_first( "SELECT * FROM booking__configpublic WHERE name='account_number'" ) ) {
	$update_sql[] = "
		INSERT INTO booking__configpublic (`name`, `value`) VALUES ('account_number', '');";

}
else {
	if ( is_table( 'booking__configpublic' ) ) {
		trigger_error( 'Ja existeixen el camp account_number' );
	}
}
