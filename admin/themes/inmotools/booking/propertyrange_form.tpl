<link href="/admin/themes/inmotools/styles/inmo_booking.css?v=<?=$version?>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="/admin/jscripts/validacio.js?v=<?=$version?>"></script>
<script type="text/javascript" src="/admin/modules/booking/jscripts/functions.js?v=<?=$version?>"></script>
<script type="text/javascript">

	var equal_dates_error = '<?=addslashes($c_equal_dates_error)?>';
	
	$(function() {
		propertyrange.season_prices = <?=$season_prices?>;
		propertyrange.id = <?=$id?>;
		propertyrange.is_form_new = <?=$js_is_form_new?>;
		propertyrange.init();
	});
	
</script>
<div class="form">
	
	<form id="theForm" name="theForm" method="post" action="<?=$link_action?>"  onsubmit="<?=$js_string?>" target="save_frame">
	
		<input name="propertyrange_id[<?=$id?>]" type="hidden" value="<?=$id?>" />
		<input name="propertyrange_id_javascript" type="hidden" value="<?=$id?>" />
		<?=$property_id?>
		
		<table width="100%" cellpadding="0" cellspacing="0">
			<tr>
				<td class="formCaptionTitle"><strong>
						<?=$c_form_title?>
					</strong>
			</tr>
		</table>
		
	
		<table width="100%" cellpadding="0" cellspacing="0" class="formstbl">
			<tr>
				<td width="150" class="formsCaption"><strong>
						<?=$c_date_start?>:</strong></td>
				<td class="forms">
					<?=$date_start?>
				</td>
			</tr>
			<tr>
				<td width="150" class="formsCaption"><strong>
						<?=$c_date_end?>:</strong></td>
				<td class="forms">
					<?=$date_end?>
				</td>
			</tr>
			<tr>
				<td width="150" class="formsCaption"><strong>
						<?=$c_season_id?>:</strong></td>
				<td class="forms">
					<?=$season_id?>
				</td>
			</tr>
			<tr>
				<td width="150" class="formsCaption"><strong>
						<?=$c_price_night?>:</strong></td>
				<td class="forms">
					<?=$price_night?> €
				</td>
			</tr>
			<tr>
				<td width="150" class="formsCaption"><strong>
						<?=$c_price_weekend?>:</strong></td>
				<td class="forms">
					<?=$price_weekend?> €
				</td>
			</tr>
			<tr>
				<td width="150" class="formsCaption"><strong>
						<?=$c_price_more_week?>:</strong></td>
				<td class="forms">
					
					<table cellspacing="0" cellpadding="0" border="0">
						<tr>
							<td><?=$price_week?> € <td>
							<td style="vertical-align:middle;padding-left:20px;"><?=$price_week_type?><td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td width="150" class="formsCaption"><strong>
						<?=$c_minimum_nights?>:</strong></td>
				<td class="forms">
					<?=$minimum_nights?>
				</td>
			</tr>
			<tr>
				<td width="150" class="formsCaption"><strong>
						<?=$c_whole_week?>:</strong></td>
				<td class="forms">
					<?=$whole_week?>
					
					<table id="start_day">
						<tr>
							<th><?=$c_start_day?><th>
						</tr>
						<tr>
							<td><?=$start_day?><td>
						</tr>
					</table>
					
				</td>
			</tr>
		</table>
		
		<table width="100%" cellpadding="4" cellspacing="2" class="formsButtonstbl">
			<tr>
				<td align="center" class="formsButtons"><div class="centerFloat1">
						<div class="centerFloat2"><? if ($form_new):?><? if ($save_button):?>
						<input type="submit" name="Submit2" value="<?=$c_send_new?>" class="btn_form1" />
						<? endif //save_button?><? endif //form_new?>
						<? if ($form_edit):?>
						<? if ($save_button):?>
							<? /* TODO-i Mirar si es posa back prev i next */ ?>
								<input type="submit" name="Submit" value="<?=$c_send_edit?>" class="btn_form1"><? endif //save_button?><?/* <? if ($delete_button):?><a class="btn_form_delete" href="javascript:submit_form('<?if ($has_bin):?><?=$c_confirm_form_bin?><?else:?><?=$c_confirm_form_deleted?><?endif //has_bin?>', true);">
					<?=$c_delete?>
					</a><? endif //delete_button?> */?>
						<? endif //form_edit?></div></td>
			</tr>
		</table>
	</form>
</div>