<?
// menus herramienta
if (!defined('NEWSLETTER_MENU_CUSTUMER')){
define('NEWSLETTER_MENU_CUSTUMER','Direcciones');
define('NEWSLETTER_MENU_CUSTUMER_LIST','Listado de direcciones');
define('NEWSLETTER_MENU_CUSTUMER_NEW','Insertar dirección');
define('NEWSLETTER_MENU_CUSTUMER_BIN','Papelera de reciclaje');

define('NEWSLETTER_MENU_GROUP','Grupo');
define('NEWSLETTER_MENU_GROUP_NEW','Insertar grupo');
define('NEWSLETTER_MENU_GROUP_LIST','Listado de grupos');


define('NEWSLETTER_MENU_MESSAGE','E-mail');
define('NEWSLETTER_MENU_MESSAGE_SEND','Enviar e-mail');
define('NEWSLETTER_MENU_MESSAGE_SEND_SAVED','Borradores');
define('NEWSLETTER_MENU_MESSAGE_SEND_FROM_HISTORY','Mensajes enviados');
define('NEWSLETTER_MENU_MESSAGE_NOT_SENT','Mensajes no enviados');
define('NEWSLETTER_MENU_MESSAGE_BIN','Papelera de reciclaje');

define('NEWSLETTER_MENU_TEMPLATE','Plantillas');
define('NEWSLETTER_MENU_TEMPLATE_NEW_TEXT','Insertar plantilla texto');
define('NEWSLETTER_MENU_TEMPLATE_LIST_TEXT','Listado plantillas texto');
define('NEWSLETTER_MENU_CONFIGADMIN','Configuración');



define('NEWSLETTER_CONFIGADMIN_MUST_CONFIGURE','Antes de enviar el E-mail debe configurar la aplicación');
define('NEWSLETTER_GROUP_MUST_INSERT','Debes crear al menos un grupo para insertar direcciones de correo');

define('NEWSLETTER_MAIL_SEND_ONLY_TESTING','Ejecutando en modo test, no se enviará ningún mensaje');
define('NEWSLETTER_MAIL_SEND_SENDING','Empezando el proceso de envío');
define('GETTING_ADDRESSES','Obteniendo las addresses de correo ...');
define('GOT_ADDRESSES','¡direcciones obtenidas!<br>');
define('GETTING_MESSAGE','Generando mensajes ...');
define('GETTING_MESSAGE_ONE','Generando mensaje ...');
define('GOT_MESSAGE','¡Mensajes generados!<br>');
define('GOT_MESSAGE_ONE','Mensaje generado!');
define('SENDING_MESSAGE','Enviando mensajes ...<br>');
define('SENDING_MESSAGE_ONE','Enviando mensaje a:');
define('MESSAGE_SENT','¡Mensajes enviados');
define('MESSAGE_NOT_SENT','¡Mensajes no enviados!');
define('NO_MESSAGE_SENT','¡Ningún mensaje enviado!');
define('MESSAGE_SENT_ONE_A','<b>Mensaje </b>');
define('MESSAGE_SENT_ONE_B','<b>  enviado a: </b>');
define('MESSAGE_NOT_SENT_ONE_A','<b>El mensaje </b>');
define('MESSAGE_NOT_SENT_ONE_B','<b> no se ha podido enviar: </b>');
define('MESSAGE_SAVED_NOT_SENT','<b>El mensaje ha sido guardado en \"' . NEWSLETTER_MENU_MESSAGE_NOT_SENT . '\"</b><br>');
define('MESSAGE_SAVED_NOT_SENT_ONE','<b>El mensaje ha sido guardado en \"' . NEWSLETTER_MENU_MESSAGE_NOT_SENT . '\"</b><br>');
define('MESSAGE_SAVED_NOT_SENT_TEST','<b>El mensaje ha sido guardado en \"' . NEWSLETTER_MENU_MESSAGE_SEND_SAVED . '\"</b><br>');
define('MESSAGE_QUEUE_STOPPED',"Cola de envio detenida, esperando per poder enviar ... ");
}

// captions seccio
$gl_caption_custumer['c_mail'] = 'Dirección de E-mail';
$gl_caption_custumer['c_name'] = 'Nombre';
$gl_caption_custumer['c_surname1'] = 'Primer apellido';
$gl_caption_custumer['c_surname2'] = 'Segundo apellido';
$gl_caption['c_group_id'] = 'Grup0';
$gl_caption_custumer['c_vat'] = 'DNI/NIF/NIE';
$gl_caption_custumer['c_phone'] = 'Teléfonos';
$gl_caption_custumer['c_phone1'] = 'Tel. 1';
$gl_caption_custumer['c_phone2'] = 'Tel. 2';
$gl_caption_custumer['c_phone3'] = 'Fax';
$gl_caption_custumer['c_adress'] = 'Calle';
$gl_caption_custumer['c_numstreet'] = 'Número';
$gl_caption_custumer['c_block'] = 'Bloque';
$gl_caption_custumer['c_flat'] = 'Piso';
$gl_caption_custumer['c_door'] = 'Puerta';
$gl_caption_custumer['c_town'] = 'Población';
$gl_caption_custumer['c_province'] = 'Provincia';
$gl_caption_custumer['c_country'] = 'País';
$gl_caption_custumer['c_bankdata'] = 'Datos bancarios';
$gl_caption_custumer['c_bank'] = 'Entidad Bancaria';
$gl_caption_custumer['c_comta'] = 'IBAN';
$gl_caption_custumer['c_account_holder'] = 'Titular';
$gl_caption_custumer['c_bic'] = 'BIC';
$gl_caption_custumer['c_mail'] = 'Correo electrónico';
$gl_caption_custumer['c_zip'] = 'Código postal';
$gl_caption_custumer['c_observations1'] = 'Observaciones';
$gl_caption_custumer['c_surnames'] = 'Apellidos';
$gl_caption_custumer['c_address'] = 'Dirección';

$gl_caption_custumer['c_action'] = 'Acción';
$gl_caption_custumer['c_edit'] = 'Editar';
$gl_caption_custumer['c_delete_selected'] = 'Borrar seleccionados';
$gl_caption_custumer['c_restore_selected'] = 'Recuperar seleccionados';
$gl_caption_custumer['c_confirm_deleted'] = 'Los elementos seleccionados se borrarán definitivamente de la base de datos \\n\\ ¿Deseas continuar?';
$gl_caption_custumer['c_select_addresses'] = 'Haz un click en una dirección de la lista para seleccionar';

$gl_caption_custumer['c_search'] = 'Buscar dirección';
$gl_caption_custumer['c_groups'] = 'Filtrar por grupo';
$gl_caption_custumer['c_selected'] = 'Direcciones seleccionadas:';
$gl_caption_custumer['c_move_to_group_id'] = 'Mover al grupo';
$gl_caption_custumer['c_enteredc']= 'Creado';
$gl_caption_custumer['c_group caption']= '[ningún grupo]';
$gl_caption_custumer['c_select_all_groups'] = 'Todos los grupos';
$gl_caption_custumer['c_prefered_language'] = 'Idioma';


$gl_messages_custumer['select_search_no_records']='No hay ninguna dirección que contenga: ';
$gl_messages_custumer['select_address']='Haz un click en una dirección de la lista para seleccionar';


$gl_caption_group['c_group_name'] = 'Grupo';
$gl_caption_group['c_description'] = 'Descripción';
$gl_caption_group['c_button_addresses'] = 'Ver direcciones';


$gl_caption_message['c_message'] = 'Mensaje';
$gl_caption_message['c_message_body'] = 'Cuerpo del mensaje';
$gl_caption_message['c_language_id'] = 'Idioma';
$gl_caption_message['c_send_to_same_language_only'] = "Enviar sólo a clientes con este idioma";
$gl_caption_message['c_send_to_no_language_too'] = "Enviar también a clientes sin idioma establecido";
$gl_caption_message['c_templatehtml_id'] = 'Plantilla';
$gl_caption_message['c_templatetext_id'] = 'Plantilla';
$gl_caption_message['c_from_address'] = 'E-mail';
$gl_caption_message['c_from_address_repeat'] = 'Repetir e-mail';
$gl_caption_message['c_from_name'] = 'Nombre';
$gl_caption_message['c_reply_to'] = 'E-mail';
$gl_caption_message['c_reply_to_repeat'] = 'Repetir e-mail';
$gl_caption_message['c_reply_to_name'] = 'Nombre';
$gl_caption_message['c_subject'] = 'Asunto';
$gl_caption_message['c_format'] = 'Formato';
$gl_caption_message['c_send_method'] = 'Forma de envío';
$gl_caption_message['c_CC'] = 'Copia a cada dirección (CC)';
$gl_caption_message['c_BCC'] = 'Copia oculta en cada dirección (BCC)';
$gl_caption_message['c_one_by_one'] = 'Un mensaje diferente para cada dirección';
$gl_caption_message['c_addresses_options'] = 'Opciones';
$gl_caption_message['c_from_title'] = 'De';
$gl_caption_message['c_reply_to_title'] = 'Contestar a';
$gl_caption_message['c_send'] = 'Enviar Mensaje';
$gl_caption_message['c_save'] = 'Guardar en Borradores';
$gl_caption_message['c_text'] = 'texto';
$gl_caption_message['c_html'] = 'html';
$gl_caption_message['c_picture'] = 'Imágenes';
$gl_caption_message['c_send_caption'] = 'Destinatarios';
$gl_caption_message['c_send_to_addresses'] = 'Enviar a los seleccionados';
$gl_caption_message['c_select_addresses'] = 'Seleccionar direcciones ...';

$gl_caption_message['c_select_property'] = 'Enviar inmuebles';
$gl_caption_message['c_select_inmo_property_link'] = 'Seleccionar inmuebles ...';
$gl_caption_message['c_select_inmo_property_list_records'] = 'listado';
$gl_caption_message['c_select_inmo_property_show_record'] = 'fichas';

$gl_caption_message['c_select_product'] = 'Enviar productos';
$gl_caption_message['c_select_product_product_link'] = 'Seleccionar productos ...';
$gl_caption_message['c_select_product_product_list_records'] = 'listado';
$gl_caption_message['c_select_product_product_show_record'] = 'fichas';

$gl_caption_message['c_group_id'] = 'Enviar al grupo';
$gl_caption_message['c_preview'] = 'Previsualizar';
$gl_caption_message['c_text_options'] = 'Las siguientes opciones se toman de la configuración automáticamente, de todas maneras se pueden cambiar por este mensaje';
$gl_caption_message['c_file'] = 'Adjuntar archivo';
$gl_caption_message['c_content_extra'] = 'Contenido incluido';
$gl_caption_message['c_entered'] = 'Creado';
$gl_caption_message['c_modified'] = 'Modificado';
$gl_caption_message['c_template_caption']='[ninguna plantilla]';
$gl_caption_message['c_messages_sent'] = 'Enviados';
$gl_caption_message['c_messages_sent_message'] = 'de';
$gl_caption_message['c_edit_button'] = 'Editar i enviar';
$gl_caption_message['c_not_send_button'] = 'enviar restantes';
$gl_caption_message['c_re_send_button'] = 'Editar i reenviar';


$gl_caption_configadmin = $gl_caption_message;
$gl_caption_configadmin['c_templatehtml_id'] = 'Plantilla en html';
$gl_caption_configadmin['c_templatetext_id'] = 'Plantilla de texto';
$gl_caption_configadmin['c_group_1'] = 'De';
$gl_caption_configadmin['c_group_2'] = 'Contestar a';
$gl_caption_configadmin['c_group_3'] = 'Datos SMTP';
$gl_caption_configadmin['c_m_username'] = 'Nombre de usuario';
$gl_caption_configadmin['c_m_password'] = 'Contraseña';
$gl_caption_configadmin['c_m_password_repeat'] = 'Repetir contraseña';
$gl_caption_configadmin['c_m_host'] = 'Servidor';
$gl_caption_configadmin['c_only_test'] = 'Modo test';
$gl_caption_configadmin['c_only_test_description'] = 'Si marcas esta casilla, no se enviará ningún e-mail, solo se simularà el envío';
$gl_caption_configadmin['c_custumer_table'] = 'Tabla de direcciones';
$gl_caption_configadmin['c_newsletter__custumer'] = 'newsletter__custumer';
$gl_caption_configadmin['c_custumer__custumer'] = 'custumer__custumer';


$gl_caption_templatetext['name'] = 'Nombre';
$gl_caption_templatetext['header'] = 'Cabecera';
$gl_caption_templatetext['footer'] = 'Pie';
$gl_caption_templatetext['send'] = 'Enviar';

$gl_caption['c_max_sends_reached'] = "Se ha alcanzado el máximo de emails enviados, enviar más podría ser motivo de SPAM";
$gl_caption['c_max_sends_reached_2'] = "Se aconseja enviar el resto en un mínimo de 2 horas";
$gl_caption['c_max_sends_reached_button'] = "Enviar igualmente el resto ahora";