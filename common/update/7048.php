<?php
/* 15-5-18
Productes, imatges de variacions

*/

if( is_table( 'contact__contact' ) && !is_field( 'contact__contact', 'accept_info' ) ){

	$update_sql[]="ALTER TABLE `contact__contact`
	ADD COLUMN `accept_info` TINYINT NOT NULL DEFAULT '0' AFTER `contact_id`;
";

}else{
	if( is_table( 'contact__contact' ) ){
		trigger_error( 'Ja existeixen el camp accept_info' );
	}
}