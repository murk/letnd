<?
/**
 * BookingPromcode
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2014
 * @version $Id$
 * @access public
 */
class BookingPromcode extends Module{

	function __construct(){
		parent::__construct();
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function get_records()
	{
	    $listing = new ListRecords($this);
	    return $listing->list_records();
	}
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{
	    $show = new ShowForm($this);
		
        // missatge de iva inclos o no inclos
		//$show->caption['c_discount_pvp'] .= ' (' . ($this->caption['c_tax_included']) . ')';
		
	    return $show->show_form();
	}

	function save_rows()
	{
	    $save_rows = new SaveRows($this);
		$save_rows->field_unique = "promcode";
	    $save_rows->save();
	}

	function write_record()
	{
	    $writerec = new SaveRows($this);
		$writerec->field_unique = "promcode";
	    $writerec->save();
	}

	function manage_images()
	{
	    $image_manager = new ImageManager($this);
	    $image_manager->execute();
	}
}
?>