<script language="JavaScript" src="/admin/jscripts/validacio.js?v=<?= $version ?>"></script>
<script language="JavaScript" src="/admin/jscripts/euro_calc.js?v=<?= $version ?>"></script>
<script language="JavaScript" src="/common/jscripts/jquery.dropshadow.js?v=<?= $version ?>"></script>
<script type="text/javascript">
	$(function () {
		set_focus(document.theForm);
	});
</script>
<form name="form_delete" method="post" action="<?= $link_action_delete ?>" target="save_frame">
	<input name="<?= $id_field ?>" type="hidden" value="<?= $id ?>">
</form>
<form name="theForm" method="post" action="<?= $link_action ?>" onsubmit="<?= $js_string ?>" encType="multipart/form-data" target="save_frame">

	<input name="page_id[<?= $id ?>]" type="hidden" value="<?= $id ?>"/>
	<input name="page_id_javascript" type="hidden" value="<?= $id ?>"/>

	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="form">
		<tr>
			<td>
				<? if ( $form_new ): ?>
					<div id="form_buttons_holder">
						<table id="form_buttons">
							<tr>
								<td class="buttons_top">
									<div class="right">
										<input type="submit" name="Submit" value="<?= $c_send_new ?>" class="btn_form1">
									</div>
								</td>
							</tr>
						</table>
					</div>
				<? endif //form_new?>
				<? if ( $form_edit ): ?>
					<div id="form_buttons_holder">
						<table id="form_buttons">
							<tr>
								<td valign="bottom">
									<div class="menu">
										<div><?= $c_edit_data ?></div><? if ( $has_images ): ?>
											<a href="<?= $image_link ?>"><?= $c_edit_images ?></a>
										<? endif //has_images?></div>
								</td>
								<td class="buttons_top">
									<div class="right">
										<input type="submit" name="Submit" value="<?= $c_send_edit ?>" class="btn_form1"/>
									</div>
								</td>
							</tr>
						</table>
					</div>
				<? endif //form_edit?>
					<table width="100%" cellpadding="0" cellspacing="0">
						<tr>
							<td class="formCaptionTitle"><strong>
									<?= $c_data ?>
								</strong></td>
						</tr>
					</table>

					<table width="100%" cellpadding="0" cellspacing="0" class="formstbl">
						<tr>
							<td valign="top" class="formsCaption"><strong>
									<?= $c_status ?>:</strong></td>
							<td><?= $status ?></td>
						</tr>
						<tr>
							<td valign="top" class="formsCaption"><strong>
									<?= $c_page ?>:</strong><?= $template ?></td>
							<td>
								<table width="100%" border="0" cellpadding="0" cellspacing="0">
									<? foreach ( $page as $l ): extract( $l ) ?>

										<? if ($lang): ?><tr><td class="langCaption"><?= $lang ?></td></tr><? endif ?>
<tr>											<td><?= $page ?></td>
										</tr>
									<? endforeach //$page_title?>
								</table>
							</td>
						</tr><? if ( $title ): ?>

							<tr>
								<td valign="top" class="formsCaption"><strong>
										<?= $c_title ?>: </strong></td>
								<td>
									<table width="100%" border="0" cellpadding="0" cellspacing="0">
										<? foreach ( $title as $l ): extract( $l ) ?>

											<? if ($lang): ?><tr><td class="langCaption"><?= $lang ?></td></tr><? endif ?>
											<tr>
												<td><?= $title ?></td>
											</tr>
										<? endforeach //$page_title?>
									</table>
								</td>
							</tr>
						<? endif // $title ?><? if ( $body_subtitle ): ?>

							<tr>
								<td valign="top" class="formsCaption"><strong>
										<?= $c_body_subtitle ?>: </strong></td>
								<td>
									<table width="100%" border="0" cellpadding="0" cellspacing="0">
										<? foreach ( $body_subtitle as $l ): extract( $l ) ?>

											<? if ($lang): ?><tr><td class="langCaption"><?= $lang ?></td></tr><? endif ?><tr>
												<td><?= $body_subtitle ?></td>
											</tr>
										<? endforeach //$page_title?>
									</table>
								</td>
							</tr>
						<? endif // $body_subtitle ?><? if ( $body_description ): ?>

							<tr>
								<td valign="top" class="formsCaption"><strong>
										<?= $c_body_description ?>: </strong></td>
								<td>
									<table width="100%" border="0" cellpadding="0" cellspacing="0">
										<? foreach ( $body_description as $l ): extract( $l ) ?>

											<? if ($lang): ?><tr><td class="langCaption"><?= $lang ?></td></tr><? endif ?><tr>
												<td><?= $body_description ?></td>
											</tr>
										<? endforeach //$page_title?>
									</table>
								</td>
							</tr>
						<? endif // $body_description ?>
					</table>
					<? if ( $content ): ?>
						<table width="100%" cellpadding="0" cellspacing="0" class="formstbl">
							<tr>
								<td class="formsCaptionHor2"><?= $c_content ?></td>
							</tr>
							<tr>
							<td>
									<table width="100%" border="0" cellpadding="0" cellspacing="0">
							<? foreach ( $content as $l ): extract( $l ) ?>
								<? if ($lang): ?><tr><td class="langCaption"><?= $lang ?></td></tr><? endif ?>
								<tr>
										<td><?= $content ?></td>
								</tr>
							<? endforeach //$content?>
									</table>
								</td>
							</tr>
						</table>
					<? endif //$content?>



				<? if ( $show_seo ): ?>
					<div class="sep_dark"></div>
					<table width="100%" cellpadding="0" cellspacing="0">
						<tr>
							<td class="formCaptionTitle">
								<strong><?= $c_seo ?></strong><? if ( ! $show_data_page ): ?> - <?= $page[0]['page'] ?><? endif //show_data_page?>
							</td>
						</tr>
					</table>
					<table width="100%" cellpadding="0" cellspacing="0" class="formstbl">
						<tr>
							<td valign="top" class="formsCaption"><strong>
									<?= $c_page_title ?>:</strong></td>
							<td>
								<table width="100%" border="0" cellpadding="0" cellspacing="0">
									<? foreach ( $page_title as $l ): extract( $l ) ?>

										<? if ($lang): ?><tr><td class="langCaption"><?= $lang ?></td></tr><? endif ?>
<tr>											<td><?= $page_title ?></td>
										</tr>
									<? endforeach //$page_title?>
								</table>
							</td>
						</tr>
						<tr>
							<td valign="top" class="formsCaption"><strong>
									<?= $c_page_description ?>: </strong></td>
							<td>
								<table width="100%" border="0" cellpadding="0" cellspacing="0">
									<? foreach ( $page_description as $l ): extract( $l ) ?>

										<? if ($lang): ?><tr><td class="langCaption"><?= $lang ?></td></tr><? endif ?>
<tr>											<td><?= $page_description ?></td>
										</tr>
									<? endforeach //$page_title?>
								</table>
							</td>
						</tr>
						<tr>
							<td valign="top" class="formsCaption"><strong>
									<?= $c_page_keywords ?>: </strong></td>
							<td>
								<table width="100%" border="0" cellpadding="0" cellspacing="0">
									<? foreach ( $page_keywords as $l ): extract( $l ) ?>

										<? if ($lang): ?><tr><td class="langCaption"><?= $lang ?></td></tr><? endif ?>
<tr>											<td><?= $page_keywords ?></td>
										</tr>
									<? endforeach //$page_title?>
								</table>
							</td>
						</tr>
						<? if ( $page_file_name ): ?>
							<tr>
								<td valign="top" class="formsCaption"><strong>
										<?= $c_page_file_name ?>: </strong></td>
								<td>
									<table width="100%" border="0" cellpadding="0" cellspacing="0">
										<? foreach ( $page_file_name as $l ): extract( $l ) ?>

											<? if ($lang): ?><tr><td class="langCaption"><?= $lang ?></td></tr><? endif ?><tr>
												<td><?= $page_file_name ?></td>
											</tr>
										<? endforeach //$page_title?>
									</table>
								</td>
							</tr>
						<? endif //$page_file_name?>
						<? if ( $page_old_file_name ): ?>
							<tr>
								<td valign="top" class="formsCaption"><strong>
										<?= $c_page_old_file_name ?>: </strong></td>
								<td>
									<table width="100%" border="0" cellpadding="0" cellspacing="0">
										<? foreach ( $page_old_file_name as $l ): extract( $l ) ?>

											<? if ($lang): ?><tr><td class="langCaption"><?= $lang ?></td></tr><? endif ?><tr>
												<td><?= $page_old_file_name ?></td>
											</tr>
										<? endforeach //$page_title?>
									</table>
								</td>
							</tr>
						<? endif //$page_old_file_name?>
					</table>
				<? endif //$show_seo?>




				<? if ( $form_new ): ?>
					<table width="100%" cellpadding="4" cellspacing="2" class="formsButtonstbl">
						<tr>
							<td align="center" class="formsButtons">
								<input type="submit" name="Submit" value="<?= $c_send_new ?>" class="btn_form1">
							</td>
						</tr>
					</table>
				<? endif //form_new?><? if ( $form_edit ): ?>
					<table width="100%" cellpadding="4" cellspacing="2" class="formsButtonstbl">
						<tr>
							<td align="center" class="formsButtons">
								<div class="centerFloat1">
									<div class="centerFloat2">
										<input type="submit" name="Submit" value="<?= $c_send_edit ?>" class="btn_form1">
									</div>
								</div>
							</td>
						</tr>
					</table>
				<? endif //form_edit?></td>
		</tr>
	</table>
</form>
