<?
/**
 * PvSeguimentfitxa
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2011
 * @version $Id$
 * @access public
 */
class PvSeguimentfitxa extends Module{
	var $is_actiu, $is_pdf;
	function __construct(){
		parent::__construct();
	}
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{
	    
		$seguiment_id = R::id('seguiment_id');
		$norma_id = R::id('norma_id');			
		
		if (!$seguiment_id || !$norma_id) return '';
		
		$seguimentfitxa_id = Db::get_first("
			SELECT seguimentfitxa_id 
			FROM pv__seguimentfitxa 
			WHERE seguiment_id = '" . $seguiment_id . "'
			AND norma_id = '".$norma_id . "'");
		
		if (!$seguimentfitxa_id){
			Db::execute("
				INSERT INTO `pv__seguimentfitxa` (`seguiment_id`, `norma_id`) 
				VALUES ('" . $seguiment_id . "', '" . $norma_id . "');");
			$seguimentfitxa_id = Db::insert_id();
		}
		$_GET['seguimentfitxa_id'] = $seguimentfitxa_id;
		
		$show = new ShowForm($this);
		$show->set_options(1, 0);
		$show->has_bin = false;
	    return $show->show_form();
	}


	function write_record()
	{
	    $writerec = new SaveRows($this);
	    $writerec->save();
		$comentari_client =  $writerec->get_value('comentari_client');
		
		/*$comentari_client = Db::get_first("
			SELECT comentari_client 
			FROM pv__seguimentfitxa 
			WHERE seguimentfitxa_id = '" . $writerec->id . "'");*/
		
		if ($comentari_client){
			set_inner_html('comentari_client',$comentari_client);
			print_javascript("
				top.$('#comentari_client').show();
				top.$('#comentari_client_titol').show();
			");
		}
	}	
}
?>