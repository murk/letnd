<?php
function save_rows()
{
    $save_rows = new SaveRows;
    $save_rows->save();
}

function list_records()
{
    $listing = new ListRecords;
	$listing->has_bin = false;
    $listing->list_records();
}

function show_form()
{
    $show = new ShowForm;
	$show->has_bin = false;
    $show->show_form();
}

function write_record()
{
    $writerec = new SaveRows;
    $writerec->save();
}

function manage_images()
{
    $image_manager = new ImageManager;
    $image_manager->execute();
}
?>