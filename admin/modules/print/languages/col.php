<?php
// menus eina
define('PRINT_MENU_KIND','Tipo de contracto');
define('PRINT_MENU_KIND_NEW','Entrar nuevo tipo');
define('PRINT_MENU_KIND_LIST','Listar tipos');
define('PRINT_MENU_CONTRACT','Contratos');
define('PRINT_MENU_CONTRACT_NEW','Entrar nuevo contracto');
define('PRINT_MENU_CONTRACT_LIST','Listar contractos');
define('PRINT_MENU_CONTRACT_BIN','Papelera de reciclatje');
define('PRINT_MENU_SHOWWINDOW', 'Escaparate');
define('PRINT_MENU_SHOWWINDOW_LIST', 'Listar fincas');


// captions seccio
$gl_caption_contract['c_property_id'] = 'Immueble';
$gl_caption_contract['c_ref'] = 'Ref. immueble';
$gl_caption_contract['c_property_private'] = 'Nombre privado';
$gl_caption_contract['c_category_id'] = 'Tipo immueble';
$gl_caption_contract['c_signed'] = 'Signado';
$gl_caption_contract['c_registered'] = 'Fecha de compraventa';
$gl_caption_contract['c_registered2'] = 'Compraventa';
$gl_caption_contract['c_rescinded'] = 'Rescindido';
$gl_caption_contract['c_duration'] = 'Duración';
$gl_caption_contract['c_months'] = 'meses';
$gl_caption_contract['c_days'] = 'dias';
$gl_caption_contract['c_years'] = 'anyos';
$gl_caption_contract['c_kind_id'] = 'Tipos';
$gl_caption_contract['c_language_id'] = 'Idioma';
$gl_caption_contract['c_state'] = 'Estado';
$gl_caption_contract['c_current'] = 'Vigente';
$gl_caption_contract['c_canceled'] = 'Rescindido';
$gl_caption_contract['c_ipc'] = 'Ipc';
$gl_caption_contract['c_ipc2'] = 'dias para aplicar el próximo ipc';
$gl_caption_contract['c_ipc3'] = 'Ipc aplicado';
$gl_caption_contract['c_ipc4'] = 'Ipc en';
$gl_caption_contract['c_expires'] = 'Caduca';
$gl_caption_contract['c_expires2'] = 'dias para caducar el contrato';
$gl_caption_contract['c_expires3'] = 'Caduca en';
$gl_caption_contract['c_contract_id'] = 'Ref.';
$gl_caption_contract['c_select_property_link'] = 'Seleccionar inmueble...';
$gl_caption_contract['c_add_property_link'] = 'Entrar nuevo immueble...';
$gl_caption_contract['c_select_buyers_link'] = 'Seleccionar comprador - inquilino...';
$gl_caption_contract['c_add_buyers_link'] = 'Entrar nuevo comprador - inquilino...';
$gl_caption_contract['c_buyers'] = 'Compradores - Alquilinos';
$gl_caption_contract['c_owner_title'] = 'Propietarios';
$gl_caption_contract['c_select_owners_link'] = 'Seleccionar propietario...';
$gl_caption_contract['c_add_owners_link'] = 'Entrar nuevo propietario...';
$gl_caption_contract['c_contract_title'] = 'Datos del contrato';
$gl_caption_contract['c_contract_title_list'] = 'Contrato';
$gl_caption_contract['c_filter_kind_id'] = 'Totdos los tipos';
$gl_caption_contract['c_contract_observations'] = 'Observaciones';

$gl_caption_contract ['c_custumer_id']='Nombre cliente';
$gl_caption_contract ['c_phone'] ='Teléfono';

$gl_caption_kind['c_kind'] = 'Tipos';


$gl_caption_showwindow['c_land'] = 'Terreno';
$gl_caption_showwindow['c_municipi_id'] = 'Situación';
$gl_caption_showwindow['c_bathroom'] = 'Baños';
$gl_caption_showwindow['c_wc'] = 'Lavabos';
$gl_caption_showwindow['c_garage_area'] = 'Garaje de %s m2';
$gl_caption_showwindow['c_storage_area'] = 'Trastero de %s m2';
$gl_caption_showwindow['c_terrace_area'] = 'Terraza de %s m2';
$gl_caption_showwindow['c_garden_area'] = 'Jardín de %s m2';
$gl_caption_showwindow['c_swimmingpool_area'] = '%s m<sup>2</sup>';
$gl_caption_showwindow['c_2garage_area'] = '%s m2';
$gl_caption_showwindow['c_2storage_area'] = '%s m2';
$gl_caption_showwindow['c_2terrace_area'] = '%s m2';
$gl_caption_showwindow['c_2garden_area'] = '%s m2';
$gl_caption_showwindow['c_2swimmingpool_area'] = '%s m2';
$gl_caption_showwindow['c_bathroom_singular'] = ' baño';
$gl_caption_showwindow['c_bathroom_plural'] = ' baños';
$gl_caption_showwindow['c_wc_singular'] = ' lavabo';
$gl_caption_showwindow['c_wc_plural'] = ' lavabos';

?>