<?php

$gallery_id        = isset( $vars['gallery_id'] ) ? $vars['gallery_id'] : false;
$block_max_results = isset( $vars['block_max_results'] ) ? $vars['block_max_results'] : false;
$block_max_images  = isset( $vars['block_max_images'] ) ? $vars['block_max_images'] : false;

$module = Module::load( 'web', 'gallery' );

$module->parent     = 'Block';
$module->gallery_id = $gallery_id;
$module->limit      = $block_max_results;

$loop = $module->do_action( 'get_records' );
if ( $block_max_images ) {
	foreach ( $loop as &$l ) {
		$l['images'] = array_slice( $l['images'], 0 , $block_max_images);
	}
}

$vars += $module->caption;


if ( $loop ) {
	Debug::p( $loop );
}
else {
	$show_block = false;
}