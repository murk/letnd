<?
/**
 * AdmintotalRedirect
 *
 * /admin/?tool=admintotal&tool_section=redirect
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2011
 * @version $Id$
 * @access public
 */
class AdmintotalRedirect extends Module{

	function __construct(){
		parent::__construct();
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function get_records()
	{
		if (!is_table('all__redirect'))
			die ( 'No hi ha la taula all__redirect' );

		$results = Db::get_rows( 'SELECT * FROM all__redirect;' );

		$html = '';

		foreach ( $results as $rs ) {
			extract( $rs );
			$html .="Redirect 301 $old_url $new_url<br>";
		}

		return $html;

	}
}