<style type="text/css">
	#import div {
		padding: 20px;
		border-top: 1px solid #D3D3D3;
	}
	#import table td {
		padding: 2px 5px;
	}
	#import table th {
		padding: 2px 5px;
		text-align: left;
	}
	h6 {
		font-size: 20px;
		margin: 10px 5px;
	}
	#confirm {
		display: none;
		padding: 25px;
		overflow: hidden;
		width: 80%;
	}
	#group_exists {
		display: none;
		padding: 25px;
		font-size: 16px;
	}
	.done {
		display: none;
		padding: 25px;
		font-size: 20px;
	}
	td.bures-gris {
		color: #a8a8a8;
	}
</style>
<script type="text/javascript" src="/common/jscripts/notify/pnotify.custom.min.js"></script>
<link href="/common/jscripts/notify/pnotify.custom.css?v=<?= $version ?>" rel="stylesheet" type="text/css"/>

<form name="theForm" method="post" action="/admin/?menu_id=2202&action=add_record" enctype="multipart/form-data" target="save_frame">
	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="form">
		<tr>
			<td>
				<div id="form_buttons_holder">
					<table id="form_buttons">
						<tr>
							<td valign="bottom">
								<div class="menu"></div>
							</td>
							<td class="buttons_top">
								<div class="right"></div>
							</td>
						</tr>
					</table>
				</div>


				<? ////////////   TITOL  ////////////////?>


				<table width="100%" cellpadding="0" cellspacing="0">
					<tr>
						<td class="formCaptionTitle"><strong>
								Cartes
								&nbsp;&nbsp;</strong></td>
					</tr>
				</table>


				<? ////////////   CAMPS  ////////////////?>

				<table width="100%" cellpadding="0" cellspacing="0" class="formstbl">

					<tr>
						<td colspan="2" class="formCaptionTitle">Cartes</td>
					</tr>
					<tr>
						<td valign="top" class="formsCaption">
							Català
						</td>
						<td valign="top" class="forms">
							<input class="file" type="file" accept=".pdf" id="carta-cat" name="carta-cat"/>
						</td>
					</tr>
					<tr>
						<td valign="top" class="formsCaption">
							Español
						</td>
						<td valign="top" class="forms">
							<input class="file" type="file" accept=".pdf" id="carta-spa" name="carta-spa"/>
						</td>
					</tr>
					<tr>
						<td valign="top" class="formsCaption">
							English
						</td>
						<td valign="top" class="forms">
							<input class="file" type="file" accept=".pdf" id="carta-eng" name="carta-eng"/>
						</td>
					</tr>
					<tr>
						<td valign="top" class="formsCaption">
							Français
						</td>
						<td valign="top" class="forms">
							<input class="file" type="file" accept=".pdf" id="carta-fra" name="carta-fra"/>
						</td>
					</tr>

					<tr>
						<td colspan="2" class="formCaptionTitle">Menú degustació</td>
					</tr>


					<tr>
						<td valign="top" class="formsCaption">
							Català 
						</td>
						<td valign="top" class="forms">
							<input class="file" type="file" accept=".pdf" id="menu-cat" name="menu-cat"/>
						</td>
					</tr>


					<tr>
						<td valign="top" class="formsCaption">
							Español 
						</td>
						<td valign="top" class="forms">
							<input class="file" type="file" accept=".pdf" id="menu-spa" name="menu-spa"/>
						</td>
					</tr>


					<tr>
						<td valign="top" class="formsCaption">
							English 
						</td>
						<td valign="top" class="forms">
							<input class="file" type="file" accept=".pdf" id="menu-eng" name="menu-eng"/>
						</td>
					</tr>


					<tr>
						<td valign="top" class="formsCaption">
							Français 
						</td>
						<td valign="top" class="forms">
							<input class="file" type="file" accept=".pdf" id="menu-fra" name="menu-fra"/>
						</td>
					</tr>

				</table>

				<table width="100%" cellpadding="4" cellspacing="2" class="formsButtonstbl">
					<tr>
						<td align="center" class="formsButtons">
							<div class="centerFloat1">
								<div class="centerFloat2">
									<input type="submit" name="guardar" value="Guardar" class="btn_form1">
								</div>
							</div>
						</td>
					</tr>
				</table>

			</td>
		</tr>
	</table>
</form>