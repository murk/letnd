<?
/**
 * DirectoryDocument
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2010
 * @version $Id$
 * @access public
 */
class DirectoryDocument extends Module{
	var $category_id;
	function __construct(){
		parent::__construct();
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function on_load(){

		// NOMES FUNCIONA EL MÒDUL QUAN TOT ES PÚBLIC.
		// Falta fer-ho per funcionar amb permisos, s'ha de fer una pàgina de login i registrar-se com a product
		if ($this->config['is_all_public'] == '0' ) {
			Main::error_404( 'Not found' );
		}

		$this->category_id = R::id('category_id');
		if ($this->category_id ){

			$this->set_friendly_params(array(
				'category_id'=>$this->category_id
				));
		}
		else {
			$this->category_id  =  R::id('category');

			if ($this->category_id) {
				$this->set_friendly_params( array(
					'category' => $this->category_id
				) );
			}
		}

		if ($this->config['file_has_langs']) {
			$this->set_field( 'file', 'show_langs', $this->config['file_show_langs'] );
		}

		parent::on_load();
	}
	function get_records()
	{
		$listing = new ListRecords($this);

		// obtinc la primera categoria amb el mateix ordre que tinc al block
		if ( ! $this->category_id && $this->config['list_first_category'] ) {

			$order = " ORDER BY ordre ASC, category ";

			$query = "SELECT directory__category_language.category_id AS category_id
				FROM directory__category_language, directory__category
				WHERE directory__category_language.category_id =  directory__category.category_id
				AND language = '" . LANGUAGE . "'" . $order .
			         " LIMIT 1";
			$this->category_id = Db::get_first($query);
		}


		
		$category_id = $this->category_id;

		$listing->set_var('listing_category_id',$category_id);	
			
		if ($category_id) $category = Db::get_first("
				SELECT category 
				FROM directory__category_language
				WHERE language = '". LANGUAGE ."' 
				AND category_id = '" . $category_id ."'");
				
		else  $category = '';
		
		$listing->set_var('listing_category',$category);

		$listing->condition = "status='public'";
		if ($category_id) {
			$listing->condition .= " AND category_id = '" . $category_id . "'";
		}
		
		// $listing->call('records_walk','', true);
		$listing->order_by = 'document ASC';
		
		if ($this->parent == 'sitemap') {//Sitemap
			$listing->paginate = false;
			$listing->set_field('modified', 'type', 'none');
			$listing->set_records();
			return $listing->loop;
		}
		
		return $listing->list_records();
	}
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{
		return;

		// NO S'HA COMPROVAT MAI, en teoria està bé


		$show = new ShowForm($this);
		


		// si no hi ha id, agafo per defecte el primer document de la categoria, i si no hi ha categoria, de la primera categoria
		if (!$show->id){
			// si no hi ha categoria agafo la primera ordenat per nom i id
			
			$category_id = $this->category_id;
			if (!$category_id )
			{
				$query = "SELECT directory__category.category_id as category_id
					FROM directory__category_language, directory__category
					WHERE directory__category_language.category_id = directory__category.category_id
					AND language = '" . LANGUAGE . "'
					ORDER BY category, category_id
					LIMIT 0,1";
				$category_id = Db::get_first($query);
			}
			$query = "SELECT directory__document.document_id as document_id
				FROM directory__document_language, directory__document
				WHERE directory__document_language.document_id = directory__document.document_id
				AND status = 'public'
				AND bin <> 1
				AND category_id = '" . $category_id . "'
				AND language = '" . LANGUAGE . "'
				ORDER BY entered DESC, document_id
				LIMIT 0,1";
			$show->id = Db::get_first($query);
		}

		$show->condition = "status = 'public'";

		
		// $show->call('records_walk','', true);

		$show->get_values();		
		if ($show->has_results)
		{
			if ($this->config['override_gl_page_title'] == '1') {
				Page::set_title($show->rs['document']);
				Page::set_body_subtitle(''); // si faig override del tiol, no te sentit deixar el subtitol general de la pagina
			}
			elseif ($this->config['override_gl_page_title'] == '2') {
				Page::set_body_subtitle($show->rs['document']);
			}
			elseif ($this->config['override_gl_page_title'] == '3') {
				Page::set_title($show->rs['document']);
				// NO TE SUBTITOL Page::set_body_subtitle($show->rs['subtitle']);
			}


			Page::set_page_title($show->rs['page_title'],$show->rs['document']);
			Page::set_page_description($show->rs['page_description'],$show->rs['description']);
			// Page::set_page_keywords($show->rs['page_keywords']);
			
			$show->set_var('show_category_id',$show->rs['category_id']);	
				
			Page::add_breadcrumb($show->rs['document']);
			return $show->show_form();
		}
		else
		{
			Main::error_404();
		}
	}
	function records_walk(&$module){

	}
}
?>