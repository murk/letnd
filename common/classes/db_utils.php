<?php

/**
 * Útils per BBDD
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) Gener 2016
 * @version $Id$
 * @access public
 *
 */
class Utils {

	static public function set_last_list_condition( $condition, $tool, $tool_section, $order_by ) {

		// sessions
		if ( ! isset( $_SESSION['last_list'] ) )
			$_SESSION['last_list'] = array();
		if ( ! isset( $_SESSION['last_list']['condition'] ) )
			$_SESSION['last_list']['condition'] = array();
		if ( ! isset( $_SESSION['last_list']['condition'] ) )
			$_SESSION['last_list']['order_by'] = array();

		$_SESSION['last_list']['condition'][ $tool . '_' . $tool_section ] = $condition;
		$_SESSION['last_list']['order_by'][ $tool . '_' . $tool_section ] = $order_by;

	}

	static public function get_last_list_condition( $tool, $tool_section ) {

		if ( ! isset( $_SESSION['last_list']['condition'][ $tool . '_' . $tool_section ] ) )
			return false;

		return $_SESSION['last_list']['condition'][ $tool . '_' . $tool_section ];

	}

	static public function get_last_list_order_by( $tool, $tool_section ) {

		if ( ! isset( $_SESSION['last_list']['order_by'][ $tool . '_' . $tool_section ] ) )
			return false;

		return $_SESSION['last_list']['order_by'][ $tool . '_' . $tool_section ];

	}

	/**
	 * @param $tool - Eina on guardem
	 * @param $name - Nom de la opció
	 *
	 * @param bool $default - Opcional per valor per defecte
	 *
	 * @return mixed
	 */
	static public function get_custom_config_item( $tool, $name, $default = false ) {

		$is_post = R::post( 'field_option_button' );
		$val = R::post( $name );

		// Els checboxes que no estan marcats no s'envien pel formulari, però haig de guardar el valor
		if ( $is_post && $val == false ) {
			$val = 'false'; // false o '' no crea la cookie, per tant poso 'false' */
		}
		elseif ($val === false) {
			$val = $default;
		}

		// si existeix la cookie i no s'ha passat valor per post, agafo el valor de la cookie
		if ( !$is_post && isset( $_COOKIE[ $tool ] ) && isset( $_COOKIE[ $tool ][ $name ] ) ) {
			$val = $_COOKIE[ $tool ][ $name ];
		}

		/*if ( $val == '' || $val === false)
			$val = 'false'; // sino no crea la cookie*/

		setcookie( $tool . "[" . $name . "]", $val, time() + ( 10 * 365 * 24 * 60 * 60 ), '/' ); // 10 anys
		$_COOKIE[ $tool ][ $name ] = $val; // per tindre access en aquesta pàgina, sino les cookies no s'hi accedeix fins que es carrega la següent

		if ($val == 'false') $val = false;

		return $val;
	}

	// navegacio pel llistat
	// TODO-i No va a productes ( per diferencies amb com funciona la bbdd, probar amb vagrant )
	// TODO-i No van el buscador avançat d'inmobles a admin
	static function set_prev_next_links(&$module) {

		$module->set_var('show_previous_link','');
		$module->set_var('show_next_link','');
		$id_field = $module->id_field;

		if (!$module->id && !$module->is_index){
			return;
		}
		// Hack només per history
		if ($module->table == 'custumer__history') return;

		$last_condition = Utils::get_last_list_condition($module->tool, $module->tool_section);


		$condition = "";
		if ($last_condition){

			$condition = $last_condition . " AND ";

			if (!$module->order_by) $module->order_by =  Utils::get_last_list_order_by($module->tool, $module->tool_section);
		}
		else{

	        // TODO Fer automatic is_field($module->table, 'bin')  per totes les eines i així treure el has_bin
			if (is_field($module->table, 'bin')) $condition .= "bin=0 AND ";
			if ($module->original_condition) $condition .= $module->original_condition . " AND ";
		}

		//$query = "SET @num = 0;";
		//Db::execute($query);

		// si no hi ha ordre, ho faig per id, faltaria trobar l'id mes proxim
		if (!$module->order_by) $module->order_by = $id_field;

		// Consulta JOIN de l'idioma - Poso sempre que hi hagi language_fields
		$language_fields_query = '';

		// TODO-i Es pot treure????
		/*if (!empty ($module->language_fields[0]))
        {
            $language_fields_query .= " LEFT OUTER JOIN " . $module->table . "_language using (" . $module->
                id_field . ")";
            $condition .= $module->table .
                "_language.language = '" . $GLOBALS['gl_language'] . "' AND ";
        }*/



		$condition = substr($condition, 0, -5);
		if ($condition) $condition = ' WHERE ' . $condition;

		$query = " 			
			SELECT position
			FROM
			( SELECT " .$id_field. " as id, @num := @num + 1 AS position
				FROM " . $module->before_table . $module->table . ', (SELECT @num:=0) r' .
				$language_fields_query . "
				" .$condition . "
				ORDER BY " .$module->order_by. "
			) AS subselect
			WHERE `id` = " .$module->id. ";";

		Db::disable_errors();
		$position = Db::get_first($query);
		Db::enable_errors();

		// TODO-i Comprovar endavant i enrera
		// Debug::p( $query );
		// Debug::p( $position );

		if ($position!==false) {

			//$query = "SET @num = 0;";
			//Db::execute($query);

			/*
			$query = "
				SELECT " .$id_field. ", position
					FROM
					( SELECT @num := @num + 1 AS 'position',  " .$id_field. " as " .$id_field. "
						FROM " . $module->before_table .$module->table .
						$language_fields_query . "
						" .$condition. "
						ORDER BY " .$module->order_by. "
					) AS subselect
					WHERE `position` IN ($position - 1, $position + 1)
					ORDER BY position ASC;";

			$results = Db::get_rows($query);*/

			$position = $position - 2;
			$limit = $position < 0 ? '1, 1' : $position . ', 3';

			$query = "
				SELECT " . $id_field . "
					FROM " . $module->before_table . $module->table .
			         $language_fields_query . "
						" . $condition . "
						ORDER BY " . $module->order_by . "
					LIMIT " . $limit . ";";

			Db::disable_errors();
			$results = Db::get_rows( $query );
			Db::enable_errors();

			// giro quan estem al primer registre de la llista
			if (count($results)==1 && isset($results[0]['position']) && $results[0]['position']>$position){
				$results[1] = $results[0];
				unset($results[0]);
			}

			// Debug::p($query);
			// Debug::p($results);

			foreach ($results as &$rs){

				if (isset($module->fields[$module->tool_section . '_file_name'])){
					$rs[$module->tool_section . '_file_name'] = Db::get_first("
						SELECT " . $module->tool_section . "_file_name
							FROM " . $module->tool . "__" . $module->tool_section . "_language 
							WHERE " . $module->tool_section . "_id = " . $rs[$id_field] . "
							AND language = '" . LANGUAGE . "'");
				}

				// TODO-i A públic només va amb show_:record, no serveix per editar formularis
				if ($module->is_site_public) {
					$rs['link'] = $module->get_show_record_link( $rs );
				}
				else {
					// TODO-i  hauria d'estar vinculat al de add_button
					$params = !$module->is_index?'&' . $module->base_link:'';
			        $params_arr = array();
			        parse_str($params, $params_arr);
			        $params_arr = array_keys($params_arr);
			        array_push($params_arr, $id_field, 'page' . $module->name);
					if ($params) $params .= '&amp;';

					$rs['link'] = get_all_get_params($params_arr,'?','&') . $params . $id_field . "=" . $rs[$id_field];
				}

			}
			if ( $position < 0 ) {
				$module->set_var('show_previous_link', false);
				if (isset($results['0'])) $module->set_var('show_next_link',$results['0']['link']);
			}
			else {
				if (isset($results['0'])) $module->set_var('show_previous_link',$results['0']['link']);
				if (isset($results['2'])) $module->set_var('show_next_link',$results['2']['link']);
			}


		}

	}


}