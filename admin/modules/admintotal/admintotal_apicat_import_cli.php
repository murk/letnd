<?
/*

CRON - cada hora
15 5 * * * php -f "/var/www/letnd.com/datos/motor/admin/modules/admintotal/admintotal_apicat_import_cli.php" > /var/www/letnd.com/datos/motor/data/logs/apicat_import.htm

SERVIDOR
------------
php -f "/var/www/letnd.com/datos/motor/admin/modules/admintotal/admintotal_apicat_import_cli.php" > /var/www/letnd.com/datos/motor/data/logs/apicat_import.htm
php -d memory_limit=512M "/var/www/letnd.com/datos/motor/admin/modules/admintotal/admintotal_apicat_import_cli.php"

PER EXCLOURE IMATGES

php /var/www/letnd.com/datos/motor/admin/modules/admintotal/admintotal_apicat_import_cli.php exclude_images > /var/www/letnd.com/datos/motor/data/logs/apicat_import.htm

PER EXCLOURE IMATGES I ARXIUS

php /var/www/letnd.com/datos/motor/admin/modules/admintotal/admintotal_apicat_import_cli.php exclude_images do_not_download_files > /var/www/letnd.com/datos/motor/data/logs/apicat_import.htm

WEB
--------------
/admin/?tool=admintotal&tool_section=apicat_import&action=start

WEB TEST - Cap imatge o Limita imatges
-------------------------------------
/admin/?tool=admintotal&tool_section=apicat_import&action=start&exclude_images=1
/admin/?tool=admintotal&tool_section=apicat_import&action=start&max_images=2

VAGRANT
-------------------------
php -f "/letnd/admin/modules/admintotal/admintotal_apicat_import_cli.php" > /letnd/data/logs/apicat_import_linux.htm
php -d include_path=/php_includes "/letnd/admin/modules/admintotal/admintotal_apicat_import_cli.php"

PER EXCLOURE IMATGES

php /letnd/admin/modules/admintotal/admintotal_apicat_import_cli.php exclude_images > /letnd/data/logs/apicat_import_linux.htm

PER EXCLOURE IMATGES I ARXIUS

php /letnd/admin/modules/admintotal/admintotal_apicat_import_cli.php exclude_images do_not_download_files > /letnd/data/logs/apicat_import_linux.htm

DOS
C:\server\php5\php.exe -f "D:\Letnd\admin\modules\admintotal\admintotal_apicat_import_cli.php" > d:\letnd\data\logs\apicat_import_windows.htm

DEBUG CLI EX
php -f "/letnd/admin/modules/admintotal/admintotal_apicat_import_cli.php" debug > /letnd/data/logs/apicat_import_linux.htm
*/

$dir = str_replace ( '/admin/modules/admintotal','/common/', __DIR__ );
$dir = str_replace ( '\admin\modules\admintotal','\\common\\', $dir );

include ($dir . 'cli_init.php');

include (DOCUMENT_ROOT . 'admin/modules/admintotal/admintotal_apicat_import.php');

include (DOCUMENT_ROOT . 'common/classes/upload_files.php');

$apicat_import = New AdmintotalApicatImport;
$apicat_import->is_cli = true;
$apicat_import->_start();