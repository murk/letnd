<?php
// menus eina


define('TRANSLATION_MENU_TRANSLATION','Listado de traducciones:');
define('TRANSLATION_MENU_LIST','Traducciones por fecha:');
define('TRANSLATION_MENU_LIST_STATE','Traducciones por fecha:');
define ('SAVED_TRANSLATION','Traducción guardada');
define ('SAVED_TRANSLATION_ERROR','Traducción guardada con ERROR');
define ('FINISHED_TRANSLATION','Traducción finalizada');


//tpl llistar
$gl_caption_translation['c_join_date'] = 'Fecha de alta';
$gl_caption_translation['c_numwords'] = 'Número de palabras: ';
$gl_caption_translation['c_translation_id'] = 'Ref.';
$gl_caption_translation['c_client_id'] = 'Cliente';
$gl_caption_translation['c_id_fake'] = 'Registro';
$gl_caption_translation['c_2'] = 'En traducción';
$gl_caption_translation['c_3'] = 'Finalizado';
$gl_caption_translation['c_1'] = 'Pendiente';
$gl_caption_translation['c_4'] = 'Error';
$gl_caption_translation['c_send_edit'] = $gl_caption['c_send_edit'];
$gl_caption_translation['c_original_text']='texto original';
$gl_caption_translation['c_translation']='Traducción';
//tpl editar
$gl_caption_translation['c_languagetranslate']= 'Idioma a traducir: ';
$gl_caption_translation['c_state'] = 'Estado actual:';
$gl_caption_translation['c_originaltext'] = 'Texto original:';
$gl_caption_translation['c_original_language'] = 'Idioma original:';
$gl_caption_translation['c_originaltranslator'] = 'Traductor inicial:';
$gl_caption_translation['c_changestate']= 'Cambiar el estado actual: ';

$gl_caption_translation['c_filter_state2']= 'Elige estado:';
$gl_caption_translation['c_filter_state']= 'Todos los estados';
$gl_caption_translation['c_filter_original_language2']= 'Elige idioma original:';
$gl_caption_translation['c_filter_original_language']= 'Todos los idiomas';

?>