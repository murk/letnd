<?php
// menus eina

if ( ! defined( 'UTILITY_MENU_UTILITIES' ) ) {
	define( 'UTILITY_MENU_UTILITIES', 'Utilitats' );
	define( 'UTILITY_MENU_UTILITIES_QUOTA', 'Càlcul de quotes' );
	define( 'UTILITY_MENU_UTILITIES_EXPENSE', 'Càlcul despeses' );
	define( 'UTILITY_MENU_UTILITIES_SAVE', 'Estalvi fiscal' );
	define( 'UTILITY_MENU_UTILITIES_CALCULATOR', 'Calculadora' );

	define( 'UTILITY_MENU_BUY', 'Càlcul de despeses compra' );
	define( 'UTILITY_MENU_SELL', 'Càlcul de despeses venda' );

	define ( 'c_workout', 'Calcular' );
}

$gl_caption_utilities['c_titlec']         = "Càlcul de quotes per les hipoteques.";
$gl_caption_utilities['c_price']          = "Preu (" . CURRENCY_NAME . "):";
$gl_caption_utilities['c_deposit']        = "Entrada (" . CURRENCY_NAME . "):";
$gl_caption_utilities['c_interest']       = "Interes (%):";
$gl_caption_utilities['c_years']          = "Anys:";
$gl_caption_utilities['c_cuota']          = "Cuota (" . CURRENCY_NAME . "):";
$gl_caption_utilities['c_notice']         = "La informació obtinguda en aquesta simulació de càlcul és totalment orientativa i no implica cap compromís, ni cap vincle jurídic-legal.";
$gl_caption_utilities['c_titles']         = "Càlcul de l'estalvi fiscal.";
$gl_caption_utilities['c_import_prestec'] = "Import del prestec.";
$gl_caption_utilities['c_period_years']   = "Termini d'amortització:";
$gl_caption_utilities['c_coin']           = "Moneda";
$gl_caption_utilities['c_eur']            = "Euros";
$gl_caption_utilities['c_pts']            = "Pessetes";
$gl_caption_utilities['c_workout']        = "Deducció:";
$gl_caption_utilities['c_single']         = "Individual";
$gl_caption_utilities['c_couplegroup']    = "Dos titulars. Conjunta:";
$gl_caption_utilities['c_couplesingle']   = "Dos titulars. Individuals:";
$gl_caption_utilities['c_saveyear']       = "Estalvi primer any:";
$gl_caption_utilities['c_completesave']   = "Estalvi total de l'operació:";
$gl_caption_utilities['c_info']           = "La informació obtinguda es totalment orientativa i no implica cap compromís jurídic i/o legal.";

$gl_caption_utilities['c_calculator'] = "Calculadora";

/**
 * Nou
 *
 */

$gl_caption_buy['c_form_title']  = "Càlcul despeses de compra d'un immoble";
$gl_caption_sell['c_form_title'] = "Càlcul despeses de venda d'un immoble";

/* Compra */
$gl_caption['c_price']                     = "Preu de l'immoble";
$gl_caption['c_notary']                    = "Despeses notari";
$gl_caption['c_notary_percent']            = "%";
$gl_caption['c_register']                  = "Despeses registre";
$gl_caption['c_register_percent']          = "%";
$gl_caption['c_taxtype_id']                = "Impostos aplicables";
$gl_caption['c_agency']                    = "Despeses gestió";
$gl_caption['c_agency_percent']            = "%";
$gl_caption['c_mortgage']                  = "Hipoteca";
$gl_caption['c_mortgage_percent']          = "%";
$gl_caption['c_mortgage_open']             = "Obertura";
$gl_caption['c_mortgage_open_percent']     = "%";
$gl_caption['c_mortgage_initial_interest'] = "Interès inicial";
$gl_caption['c_mortgage_euribor']          = "Increment euribor";
$gl_caption['c_mortgage_start_date']       = "Data inicial";
$gl_caption['c_mortgage_length']           = "Durada";
$gl_caption['c_mortgage_years']            = "Anys";
$gl_caption['c_mortgage_register']         = "Registre";
$gl_caption['c_mortgage_register_percent'] = "%";
$gl_caption['c_mortgage_notary']           = "Notari";
$gl_caption['c_mortgage_notary_percent']   = "%";
$gl_caption['c_mortgage_valuation']        = "Taxació";

/* Reports compra */
$gl_caption['c_report_title']         = "Despeses de compra";
$gl_caption['c_expense_not_financed'] = "Import no finançat";
$gl_caption['c_mortgage_title']       = "Hipoteca";
$gl_caption['c_down_payment']         = "Entrada";
$gl_caption['c_tax']                  = "Impostos";
$gl_caption['c_total_expenses']       = "Despeses";
$gl_caption['c_total']                = "Total";
$gl_caption['c_judicial_acts']        = "Actes jurídics documentats";
$gl_caption['c_mortgage_title']       = "Hipoteca";
$gl_caption['c_no_vinculant']         = "Aquests cálculs són orientatius i en cap cas vinculants";
$gl_caption['c_monthly_payment']      = "Mensualitat";

$gl_caption['c_judicial_acts_mortgage'] = "Actes jurídics responsabilitat hipotecària";
$gl_caption['c_table_quote']            = "Quota";
$gl_caption['c_table_year']             = "Any";
$gl_caption['c_table_payment']          = "Amortització";
$gl_caption['c_table_interest']         = "Interessos";
$gl_caption['c_table_balance']          = "Capital pendent";
$gl_caption['c_table_total_returned']   = "Capital amortitzat";
$gl_caption['c_table_total_paid']       = "Total pagat";

/* Venda */
$gl_caption_sell['c_price']           = "Preu de venda";
$gl_caption_sell['c_price_real']      = "Preu de venda net";
$gl_caption['c_price_bought']         = "Preu de compra";
$gl_caption['c_price_bought_real']    = "Preu de compra net";
$gl_caption['c_price_bought_updated'] = "Preu compra actualitzat";
$gl_caption['c_earning']              = "Benefici";

$gl_caption['c_cadastral_price']           = "Valor cadastral";
$gl_caption['c_date_bought']               = "Data de compra";
$gl_caption['c_date_sold']                 = "Data de venda";
$gl_caption['c_mortgage_cancel_percent']   = "%";
$gl_caption['c_other_expenses']            = "Altres despeses de compra";
$gl_caption['c_mortgage_cancel']           = "Comissió de cancel·lació";
$gl_caption['c_municipi']                  = "Municipi";
$gl_caption['c_estate_commission']         = "Comissió agència";
$gl_caption['c_estate_commission_percent'] = "%";
$gl_caption['c_habitability_card']         = "Cèdula d'habitabilitat";
$gl_caption['c_efficiency']                = "Certificat d'eficiència energètica";

/* Reports venda */
$gl_caption['c_report_title_sell']              = "Despeses de venda";
$gl_caption['c_report_title_expenses']          = "Despeses varies";
$gl_caption['c_report_title_expenses_top']      = "Despeses";
$gl_caption['c_report_title_renda']             = "Impost sobre la renda";
$gl_caption['c_report_title_renda_top']         = "Renda";
$gl_caption['c_plusvalua']                      = "Plusvàlua";
$gl_caption['c_days']                           = "Dies";
$gl_caption['c_total_days']                     = "Total de dies";
$gl_caption['c_days_before_1996']               = "Dies fins el 31/12/1996";
$gl_caption['c_days_before_2006']               = "Dies fins el 19/01/2006";
$gl_caption['c_days_after_2006']                = "Dies des de 19/01/2006 fins data venda";
$gl_caption['c_earning_before_2006']            = "Benefici fins el 19/01/2006";
$gl_caption['c_earning_after_2006']             = "Benefici des de 19/01/2006 fins data venda";
$gl_caption['c_total_to_tribute']               = "Total a tributar";
$gl_caption['c_to_tribute_before_2006']         = "Tributa sobre beneficis d'abans del 19/01/2006";
$gl_caption['c_earning_before_2006_reductible'] = "Quantitat reductible de beneficis d'abans del 19/01/2006";
$gl_caption['c_to_tribute_before_2006_anyway']  = "Tributa sobre beneficis d'abans del 19/01/2006 per excedir els 400.000 € de venda";
$gl_caption['c_to_tribute_after_2006']          = "Tributa sobre beneficis de després de 19/01/2006";
$gl_caption['c_coeficient_before_1996']         = "Reducció";
$gl_caption['c_tribute_summary']                = "Resum pagament";
$gl_caption['c_tribute_zone_1']                 = "Fins a 6.000 € - 19%";
$gl_caption['c_tribute_zone_2']                 = "De 6.000 a 50.000 € - 21%";
$gl_caption['c_tribute_zone_3']                 = "Més de 50.000 € - 23%";
$gl_caption['c_tribute']                        = "Total a pagar";

?>