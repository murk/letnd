<?php

// No fa un loop per cada tipus, només un sól loop com tots els altres filtres. Va bé per posar-ho amb checkboxes com a Finques Catalonia

$page_id = isset($vars['page_id']) ? $vars['page_id'] : false;

$vars['var_name'] = 'category_id';

$query = "SELECT Distinct category, inmo__category_language.category_id
				FROM inmo__category_language, inmo__property, inmo__category
				WHERE inmo__category_language.category_id = inmo__property.category_id
				AND inmo__category_language.category_id = inmo__category.category_id
				AND language = '" . LANGUAGE . "'
				AND inmo__property.bin = 0 AND  (status = 'onsale' OR status = 'sold' OR status = 'reserved')
 				ORDER by ordre, category";

$results = Db::get_rows($query);
Debug::add('Consulta categoris menu', $query);

$category_id = R::number('category_id');
$vars['block_selected'] = $category_id?1:0;

if ($results)
{
    foreach($results as $rs)
    {
        $category_id == $rs["category_id"]?$selected = 1:$selected = 0;


		// pel filtre
		if ( isset( $_GET['category_ids'] ) &&
		     in_array( $rs["category_id"], $_GET['category_ids'] )
		) {
			$selected = 1;
			$vars['block_selected'] = 1;
		}

       		
		$l['link'] = 
				get_link( array(
						'tool' => 'inmo', 
						'tool_section' => 'property', 
						'category' => 'category_id',
						'category_id' => $rs['category_id'],
						'detect_page' => $page_id?false:true,
						'page_id' => $page_id
				));
		
		
        $l['selected'] = $selected;
		$l['value'] = $rs['category_id'];
        $l['item'] = $rs['category'];
        $loop2[] = $l;
    }
    $loop[] = array ('loop'=>$loop2,
	'item' => '');
}
?>