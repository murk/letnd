<?php
$tipus = isset($vars['tipus'])?$vars['tipus']:false;
$page_id = isset($vars['page_id']) ? $vars['page_id'] : false;

include_once('inmo_block_functions.php');
$condition = '';
$condition .= inmo_block_get_tipus($tipus);

$vars['var_name'] = 'municipi_id';

$query = "SELECT Distinct municipi, inmo__municipi.municipi_id
				FROM inmo__municipi, inmo__property
				WHERE inmo__municipi.municipi_id = inmo__property.municipi_id
				AND bin = 0 AND (status = 'onsale' OR status = 'sold' OR status = 'reserved') " . $condition . "
				ORDER BY municipi";

$results = Db::get_rows($query);


$municipi_id = R::number('municipi_id');
$vars['block_selected'] = $municipi_id?1:0;

if ($results) {
    foreach($results as $rs) {
        $municipi_id == $rs["municipi_id"]?$selected = 1:$selected = 0;

		// pel filtre
		if ( isset( $_GET['municipi_ids'] ) &&
		     in_array( $rs["municipi_id"], $_GET['municipi_ids'] )
		) {
			$selected = 1;
			$vars['block_selected'] = 1;
		}
		
		/*
		if (USE_FRIENDLY_URL) {
			$friendly_params['municipi_id'] = $rs['municipi_id'];
			if ($tipus) $friendly_params['tipus'] = $tipus;
			$l['link'] = Page::get_link('inmo','property',false,false,$friendly_params,false,'',false,false);
		} else {
			$l['link'] = '/?tool=inmo&amp;tool_section=property&amp;municipi_id=' . $rs['municipi_id'] . '&amp;language=' . LANGUAGE;
		}
		 * 
		 */
				
		$l['link'] = get_link(array(
				'tool' => 'inmo', 
				'tool_section' => 'property', 
				'friendly_params' => array('tipus'=>$tipus==INMO_DEFAULT_TIPUS?'':$tipus, 'municipi_id'=> $rs['municipi_id']),
				'link_vars' => array('tipus'=>$tipus),
				'detect_page' => $page_id?false:true,
				'page_id' => $page_id
		));
		
        $l['selected'] = $selected;
        $l['value'] = $rs['municipi_id'];
        $l['item'] = $rs['municipi'];
		$l['class'] = '';

        $loop2[] = $l;
    }
	
$loop2[0]['class'] = 'first';	
$loop2[count($loop2)-1]['class'] = 'last';
	
$loop[] = array ('loop' => $loop2,
    'item' => '');
}


?>