<?php
// nomes que hi hagi un menu malament en tota l'estructura ja no es mostrarà

$page = &$GLOBALS['gl_page'];
// agafo el page_id com a parent id, així la primera consulta del bucle es el page actual
$parent_id = $page->page_id;

// valors per defecte
// SHOW_HOME  -> serveix per posar o no posar la home quan la pàgina no penja directe de la home ( en un menu secundari, en una notícia, etc.. )
$show_home = isset($vars['show_home'])?$vars['show_home']:'0';
$minim_items = isset($vars['minim_items'])?$vars['minim_items']:'2';
$max_length = isset($vars['max_length'])?$vars['max_length']:false; // màxim de caracters, quan passa de caràters poso punts suspensius

$vars['menu'] = $page->menu;

$conta = 0;
$loop_count = $page->level;
if ($page->breadcrumb) $loop_count +=count($page->breadcrumb);
if ($show_home) $loop_count ++;

do{
	$query = "SELECT all__page.page_id as page_id, link, page, page_text_id, parent_id, page_file_name, has_content
				FROM all__page_language, all__page
				WHERE all__page_language.page_id = all__page.page_id
				AND status = 'public'
				AND bin <> 1
				AND all__page.page_id = '" . $parent_id . "'
				AND language = '" . LANGUAGE . "'";
				
				
	
	$rs = Db::get_row($query);
	if (!$rs){
		// nomès que hi hagi un esglaó malament ja no mostro el breadcrumb, sempre hi hauria d'haver resultats fins a l'arrel		
		if ($page->breadcrumb){
			break;
		}
		else {
			$show_block = false;
			return;
		}
	}
	else
	{	
		if ($rs['has_content'])
			$l['link'] = Page::get_page_link($rs['page_id'],$rs['link'],$rs['page_file_name'],$rs['has_content']);
		else
			$l['link'] = '';
		$l['selected'] = 0;
		$l['item'] = $rs['page'];
		$l['id'] = $rs['page_id'];
		$l['page_text_id'] = $rs['page_text_id'];
		$l['conta'] = $conta++;
		$l['class'] = '';
		$l['loop_count'] = $loop_count;
		$loop[] = $l;
		
		$parent_id = $rs['parent_id'];
	}
} while ($parent_id!=0);


if ($show_home) {
	// si resulta que al final, tot penja de la home, doncs desconto 1 a loop_count -> a dalt li he sumat si show_home
	if (isset($rs['link']) && $rs['link']=='/'){
		$loop_count--;
		for ($i = 0; $i < count($loop); ++$i) {
			$loop[$i]['loop_count'] = $loop_count;
		}
	}
	// si no penja de la home, li poso la home per que quedi: home > pagina > pagina_sub
	else{	
		$query = "SELECT all__page.page_id as page_id, link, page, page_text_id, parent_id
				FROM all__page_language, all__page
				WHERE all__page_language.page_id = all__page.page_id
				AND status = 'public'
				AND bin <> 1
				AND link = '/'
				AND language = '" . LANGUAGE . "'";
		$rs = Db::get_row($query);
		if ($rs){
		$l['selected'] = 0;
			$l['link'] = $rs['link'];
			$l['item'] = $rs['page'];
			$l['id'] = $rs['page_id'];
			$l['page_text_id'] = $rs['page_text_id'];
			$l['conta'] = $conta++;
			$l['class'] = '';
			$l['loop_count'] = $loop_count;
			$loop[] = $l;
		}
		// si no tinc definida cap home, trec el link a la home
		else{
			$loop_count--;
			for ($i = 0; $i < count($loop); ++$i) {
				$loop[$i]['loop_count'] = $loop_count;
			}
		
		}
	}
}
else{
	// si resulta que els pages pengen de la home, borro la home
	if (isset($rs['link']) && $rs['link']=='/'){
	
		unset($loop[count($loop)-1]);
		
		$loop_count--;	
		for ($i = 0; $i < count($loop); ++$i) {
			$loop[$i]['loop_count'] = $loop_count;
		}
	}
}

// si nomès hi ha un resultat o cap no val la pena mostrar el breadcrumb
if ($loop_count<$minim_items){
	$show_block = false;
	return;
}


// canvio l'ordre de l'array ja que en els pages vaig pujant del nivell actual fins a nivell 1
$loop = array_reverse($loop);

// Poso be els conta que estan invertits
array_walk($loop, function(&$val, $index) {
    $val['conta'] = $index;
    return $val;
});

// per poder incloure desde cada modul més links al breadcrumb
if ($page->breadcrumb){
	foreach ($page->breadcrumb as $l){
	$l['id'] = ''; // de moment no fa falta especificar-ho desde el modul
	$l['page_text_id'] = '';
	$l['conta'] = $conta++;
	$l['class'] = ''; // serveix pel primer i últim registre i ho especifico al final del block
	$l['loop_count'] = $loop_count;
	$loop[] = $l;
	}
}

// si m'he passat de caracters, borro els items de l'array que sobrin, i poso punts suspensius
if ($max_length){
	$total_chars = 0;

	foreach ($loop as $key => &$l){

		$chars = strlen($l['item']);
		
		if ($total_chars+$chars>$max_length){
			//$l['item'] = substr($l['item'], 0, $max_length-$total_chars) . ' ...';
			$l['item'] = add_dots_meta_description($l['item'], $max_length-$total_chars);
			
			$loop = array_slice($loop, 0, $key+1);
			
			break;
		}
		
		$total_chars +=$chars;
	}
}

$loop[0]['class'] = 'first';	
$loop[count($loop)-1]['class'] = 'last';	
$loop[count($loop)-1]['selected'] = 1; // aquí el seleccionat sempre és l'ultim
?>