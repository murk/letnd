<?
/**
 * BookingSeason
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 6-2014
 * @version $Id$
 * @access public
 */
class BookingSeason extends Module{
	private $colors, $season_colors, $used_colors, $not_used_colors;
	function __construct(){
		parent::__construct();
	}
	
	function list_records()
	{
		$GLOBALS['gl_content'] = 
			'<link href="/admin/themes/inmotools/styles/inmo_booking.css" rel="stylesheet" type="text/css">' .
			'<script src="/admin/modules/booking/jscripts/functions.js"></script>' . $this->get_records();
	}
	function get_records()
	{
		
		$listing = new ListRecords($this);			
		$listing->add_swap_edit();
		$listing->has_bin = false;
		if ($listing->is_editable)
			$listing->show_langs = true;
		
		$listing->order_by = 'ordre ASC, season ASC, season_id ASC';
		
		
		$this->get_colors();	
		
		$styles = BookingCalendar::get_season_styles($this->seasons_colors);	
		
		
		$listing->call('records_walk','season_id',$this);
		
		$GLOBALS['gl_page']->javascript .= 'bookseason.init();';
		
	    return
			$styles . 
			$listing->list_records();
	}
	function get_colors(){
		
		Main::load_class('booking','common');		
		Main::load_class('booking','calendar');
		
		$this->colors = BookingCalendar::get_colors();		
		$this->seasons_colors = BookingCommon::get_seasons_colors();
		$this->not_used_colors = array_diff($this->colors, $this->seasons_colors);
		$this->used_colors = array_diff($this->colors, $this->not_used_colors); // igual que el seasons_colors, pero amb ales key coma color_id
		
		//Debug::p($this->colors, 'colors');
		//Debug::p($this->seasons_colors, 'seasons_colors');
		//Debug::p($this->not_used_colors, 'not_used_colors');
	}
	
	function records_walk(&$module) {
		
		$data = &$module->data;
		$rs = &$module->rs;
		$id = $module->rs['season_id'];
		$color_id = isset($module->rs['color_id'])?$module->rs['color_id']:'0';
		
		if ($module->list_show_type=='form' || $module->is_editable){
			
			$selected_color = current($this->not_used_colors);
			
			foreach ($this->colors as $k => $c){
				if ($k == $color_id){
					$selected_color = $c;
					break;
				}
			}
			
			$color = '<input type="hidden" value="'.$color_id.'" name="old_color_id['.$id.']"><select class="season-color" style="background-color:rgba('.$selected_color.',0.8)" name="color_id['.$id.']" id="color_id_'.$id.'">';
						
			foreach ($this->not_used_colors as $k => $c){
				$selected = $color_id==$k?' selected':'';
				$color .= '<option data-color="'.$c.'" value="'.$k.'" style="background-color:rgba('.$c.',0.8);border-color:rgba('.$c.',0.8);"'.$selected.'> </option>';
			}
			if ($this->used_colors) {
				$color .= '<optgroup label="'.$this->caption['c_already_used'].'"></optgroup>';
			}
			foreach ($this->used_colors as $k => $c){
				$selected = $color_id==$k?' selected':'';
				$color .= '<option data-color="'.$c.'" value="'.$k.'" style="background-color:rgba('.$c.',0.8);border-color:rgba('.$c.',0.8);"'.$selected.'> </option>';
			}
			
			$color .= '</select>';
			$ret['color_id'] = $color;
			
		}
		else {
			$ret['color_id'] = '<div class="season-list season'.$module->rs['season_id'].'"></div>';
		}
		
		return $ret;
	}
	
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{
		$this->get_colors();
		
	    $show = new ShowForm($this);
		$show->call('records_walk','season_id',$this);
		$show->has_bin = false;
		
		$GLOBALS['gl_page']->javascript .= 'bookseason.init();';
		
	    return 
			'<link href="/admin/themes/inmotools/styles/inmo_booking.css" rel="stylesheet" type="text/css">' .
			'<script src="/admin/modules/booking/jscripts/functions.js"></script>' . 
			//$styles . 
			$show->show_form();
	}
	
	function save_rows()
	{
	/*
	    if ($this->process=='save_property_extras') {
			$this->save_property_seasons();
			return;
		}
		*/		
		$save_rows = new SaveRows($this);
	    $save_rows->save();
		
		$GLOBALS['gl_reload'] = true;
	}

	function write_record()
	{
	    $writerec = new SaveRows($this);
	    $writerec->save();
	}
}
?>