<?
/*
28-10-2019

*/



if ( is_table( 'product__customer' ) && ! is_field( 'product__customer', 'has_no_vat' ) ) {

	$update_sql[] = "ALTER TABLE `product__customer`
	ADD COLUMN `has_no_vat` TINYINT(1) NOT NULL DEFAULT '0' AFTER `has_equivalencia`;
";

}
else {
	if ( is_table( 'product__customer' ) ) {
		trigger_error( 'Ja existeixen el camp has_vat' );
	}
}

