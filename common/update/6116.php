<?

/*
	Nou config per SSL
 */

if ( is_table( 'all__configadmin' ) && ! Db::get_first( "SELECT * FROM all__configadmin WHERE name='has_ssl'" ) ) {
	$update_sql[] = "
		INSERT INTO all__configadmin (`name`, `value`) VALUES ('has_ssl', '0');";

} else {
	if ( is_table( 'all__configadmin' ) ) {
		trigger_error( 'Ja existeixen el camp has_ssl' );
	}
}

?>