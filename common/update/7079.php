<?
/*
21-05-2019

*/

if ( is_table( 'inmo__property' ) && ! is_field( 'inmo__property', 'swimmingpool_area' ) ) {

	$update_sql[] = "ALTER TABLE `inmo__property`
	ADD COLUMN `swimmingpool_area` INT(3) NOT NULL DEFAULT '0' AFTER `swimmingpool`;";

}
else {
	if ( is_table( 'inmo__property' ) ) {
		trigger_error( 'Ja existeixen el camp swimmingpool_area' );
	}
}