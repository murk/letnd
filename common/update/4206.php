<?php
// no es poden posar ; entre mig de la consulta ja que parteix les consultes per el ; a no ser que fem servir $update_sql en array()
	
if (is_table("newsletter__message") && !is_table("newsletter__custumer_to_group")){	
	$update_sql[]="CREATE TABLE `newsletter__custumer_to_group` (
		`custumer_id` INT(11) NOT NULL DEFAULT '0',
		`group_id` INT(11) NOT NULL DEFAULT '0'
	)
	COLLATE='utf8_spanish2_ci'
	ENGINE=MyISAM;";
	
	$cf = get_config_array('admin', 'newsletter');
	$table = $cf['custumer_table'];
	$field = explode('__',$table);
	$field = $field[1] . '_id';
	
	// passo tots els registres de "un a molts" a "molts a molts"
	$update_sql[]="INSERT INTO `newsletter__custumer_to_group` (`custumer_id`, `group_id`) SELECT ".$field.", group_id FROM ".$table." WHERE group_id <> 0";
	// borro group_id
	$update_sql[]="ALTER TABLE `".$table."`	DROP COLUMN `group_id`;";
	if ($table!='newsletter__custumer') $update_sql[]="ALTER TABLE `newsletter__custumer` DROP COLUMN `group_id`;";
	
	$update_sql[]="CREATE TABLE `newsletter__group_language` (
						`group_id` INT(11) NOT NULL DEFAULT '0',
						`language` CHAR(3) NOT NULL COLLATE 'utf8_spanish2_ci',
						`group_name` VARCHAR(100) NOT NULL COLLATE 'utf8_spanish2_ci',
						`description` VARCHAR(255) NOT NULL COLLATE 'utf8_spanish2_ci'
					)
					COLLATE='utf8_spanish2_ci';";
					
	// poso tots els registres del grup, de l'idioma de l'admin a cada un dels idiomes restant publics	
	global $gl_languages;
	
	foreach($gl_languages['names'] as $lang=>$val){
		$update_sql[]="INSERT INTO newsletter__group_language (group_id, language, group_name, description) SELECT group_id, '".$lang."',group_name, description  FROM newsletter__group;";
	}
	$update_sql[]="ALTER TABLE `newsletter__group`
	DROP COLUMN `group_name`,
	DROP COLUMN `description`;";
	
	$update_sql[]="ALTER TABLE `newsletter__custumer`
	ADD COLUMN `prefered_language` CHAR(3) NOT NULL DEFAULT '".LANGUAGE."' AFTER `observations1`";
}
// idioma preferit	
if (is_table("product__customer")){	
	$update_sql[]="ALTER TABLE `product__customer`
	ADD COLUMN `prefered_language` CHAR(3) NOT NULL DEFAULT '".LANGUAGE."' AFTER `dni`";	
}
// idioma preferit	
if (is_table("custumer__custumer")){	
	$update_sql[]="ALTER TABLE `custumer__custumer`
	ADD COLUMN `prefered_language` CHAR(3) NOT NULL DEFAULT '".LANGUAGE."' AFTER `mail`";	
}
?>