<?
/**
 * WebGallery
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 24/05/2014
 * @version $Id$
 * @access public
 */
class WebGallery extends Module{
	var $gallery_id = false, $limit = false;
	function __construct(){
		parent::__construct();
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function get_records()
	{
	    $this->set_gallery_fields();
		$listing = new ListRecords($this);
		
		
		// per poder previsualitzar desde admin i enviar desde newsletter
		if (isset($_GET['loginnewsletter']) 
				&& $_GET['loginnewsletter'] == 'letndmarc'
				&& $_GET['passwordnewsletter'] == '5356aopfqkiios5cnoqws90se'
		)
		{
			if (is_file(PATH_TEMPLATES . 'newsletter/gallery_list.tpl')) 
					$listing->template = 'newsletter/gallery_list';
			
		}
		
		$listing->condition = "(status = 'public')";

		if ( $this->gallery_id ) {
			$listing->condition .= " AND gallery_id = '{$this->gallery_id}'";
		}
		
		
		$listing->call('records_walk','content,content_list');
		$listing->order_by = 'ordre ASC, gallery ASC';
		
		if ($this->parent == 'sitemap') {//Sitemap
			$listing->paginate = false;
			$listing->set_field('modified', 'type', 'none');
			$listing->set_records();
			return $listing->loop;
		}

		if ($this->parent == 'Block') {
			$listing->limit = $this->limit;
			$listing->set_records();
			return $listing->loop;
		}
		
		return $listing->list_records();
	}
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{
	    $this->set_gallery_fields();
		$show = new ShowForm($this);
		
		
		// per poder previsualitzar desde admin i enviar desde newsletter
		if (isset($_GET['loginnewsletter']) 
				&& $_GET['loginnewsletter'] == 'letndmarc'
				&& $_GET['passwordnewsletter'] == '5356aopfqkiios5cnoqws90se'
		)
		{
			if (is_file(PATH_TEMPLATES . 'newsletter/gallery_form.tpl')) 
					$show->template = 'newsletter/gallery_form';
			
		}	
		
		$show->condition = "(status = 'public')";
		
		$show->get_values();		
		if ($show->has_results)
		{
			if ($this->config['override_gl_page_title'] == '1') Page::set_title($show->rs['gallery']);
			if ($this->config['override_gl_page_title'] == '2') Page::set_body_subtitle($show->rs['gallery']);
			
				
			Page::set_page_title($show->rs['page_title'],$show->rs['gallery']);
			Page::set_page_description($show->rs['page_description'],$show->rs['content']);
			Page::set_page_keywords($show->rs['page_keywords']);
			
				
			Page::add_breadcrumb($show->rs['gallery']);

			$content =  $show->show_form();

			Page::set_page_og($show->rs);
			return $content;
		}
		else
		{
			Main::redirect('/');
		}
	}
	function records_walk($content=false,$content_list=false){				
		
		// si es llistat passa lavariable content
		if ($content!==false){
			$ret['content']=$content_list?$content_list:add_dots('content', $content,$this->config['add_dots_length']);
		}
		
		return $ret;
	}
	
	function set_gallery_fields(){	
		if (!$this->config['show_entered_time'])
		{
			$this->set_field('entered', 'type', 'date');
		}
	}
}
?>