<?
/**
 * PortfoliPortfoliHome
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2/2014
 * @version $Id$
 * @access public
 */
class PortfoliPortfoliHome extends Module{

	function __construct(){
		parent::__construct();
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function get_records()
	{

	    // es llisten totes directament, sense home
	    if ($this->config['home_list_all']){
		    $module = Module::load('portfoli', 'portfoli');
		    $ret = $module->do_action('get_records');
		    return $ret;
	    }
	    else {

		    $module  = Module::load( 'portfoli', 'portfoli', $this );
		    $listing = $module->do_action( 'get_records' );
		    $listing->condition .= " AND destacat = '1'";
		    $listing->paginate = false;
		    $listing->set_records();
		    $destacats = $listing->loop;

		    unset( $listing );

		    $listing = $module->do_action( 'get_records' );
		    $listing->set_loop( 'destacats', $destacats );

		    //$listing->condition .= "AND destacat = '0'";

		    return $listing->list_records();
	    }
	}
}
?>