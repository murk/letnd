<table class="custumers">
	<? foreach( $loop as $l ): extract( $l ) ?>
		<tr>
			<? if( $name ): ?>
				<td valign="top"><a href="/admin/?menu_id=5003&action=show_form_edit&custumer_id=<?= $custumer_id ?>"><strong><?= $name ?> <?= $surname1 ?> <?= $surname2 ?></strong></a></td>
			<? endif //$name ?>

			<td valign="top" class="custumers-police">
				<table>
					<tr <?= $treatment?'':'class="vermell"' ?>>
						<th><?= $c_treatment_police ?>:</th>
						<td><?= $treatment ?></td>
					</tr>
					<tr <?= $vat?'':'class="vermell"' ?>>
						<th><?= $c_vat ?>:</th>
						<td><?= $vat ?></td>
					</tr>
					<tr <?= $vat_date == '0000-00-00' || !$vat_date?'class="vermell"':'' ?>>
						<th><?= $c_vat_date ?>:</th>
						<td><?= $vat_date ?></td>
					</tr>
					<tr <?= $birthdate == '0000-00-00' || !$birthdate?'class="vermell"':'' ?>>
						<th><?= $c_birthdate ?>:</th>
						<td><?= $birthdate ?></td>
					</tr>
					<tr <?= $nationality?'':'class="vermell"' ?>>
						<th><?= $c_nationality ?>:</th>
						<td><?= $nationality ?></td>
					</tr>
				</table>
			</td>
		</tr>
	<? endforeach //$loop?>
</table>