<?php
// TODO-i Comprovar el maxlength de tots els int amb mascara, al posar la mascara necessita un numero mes, ex. year son 5 digits
// TODO-i Als arxius canviar el checkbox per esporrar per un hidden i una imatge que esborri, llavors posar el text en tatxat i desactivar els inputs
// TODO-i  Posar be la paperera de reciclatge a API
// TODO-i Canviar el titol de la pàgina quan s'està editant un registre


//define(DEBUG, true); per activar debug
include ('../common/classes/main.php');
Main::set_vars(); // defineixo les 4 variables necessaries abans de fer res
Debug::start(); // començo el debug si s'escau

include ('../common/main.php'); // carrego arxius que tenen variables globals, a la llarga ha d'anar a Main::load_files();
Main::load_files(); // carrego includes i classes

Main::check_redirect(); // faig redirects 301 si convé

$gl_page = new Page(); // començo la pàgina

User::check_is_logged();// comprovar login o si es vol deixar la sessió
User::on_login();// si acabem de fer el login obrim l'arxiu on_login per comprovar missatges o altres funcions
User::is_not_allowed()?User::goto_readable():false; // si no hi ha permis redirigeixo a seccio amb premis

Main::show_missatges();// faig accions al entrar

Module::get_config_values();// variables de configuracio de la taula modul__config, quan tot siguin moduls anirà darrera ::load();
Module::load(); // carrego arxiu del modul
$gl_module = Module::init(); // inicio modul general o modul v3 creats amb classes
$gl_module->set_tpl();// defineixo tpl
$gl_module->set_language(); // carrego arxiu variables idioma
$gl_module->set_config(); // carrego arxiu config del modul
$gl_module->set_functions(); // carrego arxiu de funcions per el modul
$gl_module->on_load(); // crido la funció on_load quan he carregat tot el modul

UploadFiles::initialize($gl_module); // iniciar uploads si n'hi ha

CallFunction::call(); // crido una funcio desde get, just abans de do_action per poder anular o no el do_action

$gl_module->do_action(); // segons l'action crido el metode amb el mateix nom

$gl_page->is_frame?
	$gl_page->show_message($gl_message): // mostro missatge al top frame
	$gl_page->show(); // mostro pagina

Debug::p_all(); // mostro les variables del debug si toca
?>