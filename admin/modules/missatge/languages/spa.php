<?php
// menus eina
define( 'MISSATGE_MENU_NEW', 'Crear Mensaje' );
define( 'MISSATGE_MENU_MISSATGE', 'Mensajes' );
define( 'MISSATGE_MENU_LIST', 'Listar Mensajes' );
define( 'MISSATGE_MENU_BIN', 'Papelera Reciclaje' );

// captions seccio
$gl_caption_missatge['c_status']          = 'Estado';
$gl_caption_missatge['c_subject']         = 'Asunto';
$gl_caption_missatge['c_missatge']        = 'Mensaje';
$gl_caption_missatge['c_blocked']         = '';
$gl_caption_missatge['c_bin']             = '';
$gl_caption_missatge['c_mls_from_id']     = 'De';
$gl_caption_missatge['c_mls_to_id']       = 'Para';
$gl_caption_missatge['c_creator_user_id'] = 'De';

$gl_caption_missatge['c_status_prepare'] = 'En preparación';
$gl_caption_missatge['c_status_waiting'] = 'Para leer';
$gl_caption_missatge['c_status_readed']  = 'Leído';
$gl_caption_missatge['c_status_send']    = 'Enviado';

$gl_caption_missatge ['c_kind']         = 'Tipo';
$gl_caption_missatge ['c_kind_mls']     = 'Mls';
$gl_caption_missatge ['c_kind_general'] = 'General';

$gl_caption_missatge ['c_entered']  = 'Fecha creación';
$gl_caption_missatge ['c_modified'] = 'Fecha modificación';
$gl_caption_missatge ['c_sent']     = 'Enviado';

$gl_caption['c_user_id']       = "O usuario";
$gl_caption_list['c_user_id']  = "Usuarios";
$gl_caption['c_group_id']      = "Grupo";
$gl_caption_list['c_group_id'] = "Grupo";

$gl_caption['c_form_level1_title_group_id'] = "Enviar a:";
$gl_caption['c_form_level1_title_subject']  = "Missatge";

$gl_caption['c_reply_button'] = $gl_caption['c_reply'] ;
?>
                  

