<?php
global $gl_tool, $gl_is_home;

$query = "SELECT all__page.page_id as page_id, link, page, page_file_name, has_content
				FROM all__page_language, all__page
				WHERE all__page_language.page_id = all__page.page_id
				AND status = 'public'
				AND bin <> 1
				AND menu = 1
				AND language = '" . LANGUAGE . "'
				ORDER BY ordre";

$results = Db::get_rows($query);
Debug::add('Consulta pages menu', $query);

if ($results)
{
    foreach($results as $rs)
    {
        ((isset($_GET["page_id"]) && ($_GET["page_id"] == $rs["page_id"]))
		|| ($rs['link']=='/' && $gl_is_home))?$selected = 1:$selected = 0;
        // definir el link de l'item del menu
        $l['link'] = Page::get_page_link($rs['page_id'],$rs['link'],$rs['page_file_name'],$rs['has_content']);

        // per llistar en venta
        if (
            strpos($rs['link'], 'tipus=sell') !== false
                )
        {
            $l['loop_sub'] = get_sub($l, $rs['page_id'], 'sell');
        }
        // per llistar de lloguer
        elseif (
            strpos($rs['link'], 'tipus=rent') !== false
                )
        {
            $l['loop_sub'] = get_sub($l, $rs['page_id'], 'rent');
        }
        else
        {
            $l['loop_sub'] = array();
        }
        $l['selected'] = $selected;
        $l['item'] = $rs['page'];
        $l['id'] = $rs['page_id'];
        $loop2[] = $l;
    }

    $loop[] = array ('loop' => $loop2,
        'item' => '');
}

/**
 *
 * @access public
 * @return void
 */
function get_sub(&$l_loop, $page_id, $tipus)
{
    $return = (!isset($_GET['tipus']) || ($_GET['tipus'] != $tipus));
    $loop = array();
    $query = "SELECT Distinct category, inmo__category_language.category_id
					FROM inmo__category_language, inmo__property, inmo__category
					WHERE inmo__category_language.category_id = inmo__property.category_id
					AND inmo__category_language.category_id = inmo__category.category_id
					AND language = '" . LANGUAGE . "'
					AND inmo__property.bin = 0 AND (status = 'onsale')
					AND tipus='" . $tipus . "'
	 				ORDER by ordre";

    $results = Db::get_rows($query);
    Debug::add('Consulta categoris menu', $query);
    $is_first = true;

    if ($results)
    {
        foreach($results as $rs)
        {
            (isset($_GET["category_id"]) && ($_GET["category_id"] == $rs["category_id"]))?$selected = 1:$selected = 0;
           			
			$l['link'] = get_link(array(
				'tool' => 'inmo', 
				'tool_section' => 'property', 
				'category' => 'category_id',
				'category_id' => $rs['category_id'],
				'link_vars' => array('tipus'=>$rs["tipus"]),
				'detect_page' => true
			));
			
            // poso el link del parent que sigui igual a la primera categoria trobada
            if ($is_first) $l_loop['link'] = $l['link'];
            $is_first = false;

            if ($return) return array(); // si no estem actualment en aquest menu no mostrem submenu
            $l['selected'] = $selected;
            $l['item'] = $rs['category'];
            $l['tipus'] = $tipus;
            $loop[] = $l;
        }
    }
    return $loop;
}

?>