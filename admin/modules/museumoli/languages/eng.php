<?php
// menus eina
if (!defined('MUSEUMOLI_MENU_VISITA')){
	define('MUSEUMOLI_MENU_VISITA','Visites');
	define('MUSEUMOLI_MENU_VISITA_NEW','Nova visita');
	define('MUSEUMOLI_MENU_VISITA_LIST','Llistar visites');
	define('MUSEUMOLI_MENU_VISITA_LIST_BIN','Paperera de reciclatge');
	
	define('MUSEUMOLI_MENU_VISITATALLER_NEW','Nova visita-taller');
	define('MUSEUMOLI_MENU_VISITATALLER_LIST','Llistar visites-taller');
	define('MUSEUMOLI_MENU_VISITATALLER_LIST_BIN','Paperera de reciclatge');
	
	define('MUSEUMOLI_MENU_VISITANT','Tipus de visitants');
	define('MUSEUMOLI_MENU_VISITANT_NEW','Nou tipus de visitant');
	define('MUSEUMOLI_MENU_VISITANT_LIST','Llistar tipus visitants');
	define('MUSEUMOLI_MENU_VISITANT_LIST_BIN','Paperera de reciclatge');
	
	define('MUSEUMOLI_MENU_CURS','Cursos');
	define('MUSEUMOLI_MENU_CURS_NEW','Nou curs');
	define('MUSEUMOLI_MENU_CURS_LIST','Llistar cursos');
	define('MUSEUMOLI_MENU_CURS_LIST_BIN','Paperera de reciclatge');
	
	define('MUSEUMOLI_MENU_DOCUMENTACIOCATEGORY_NEW','Nova categoria');
	define('MUSEUMOLI_MENU_DOCUMENTACIOCATEGORY_LIST','Llistar categories');
	define('MUSEUMOLI_MENU_DOCUMENTACIOCATEGORY_LIST_BIN','Paperera de reciclatge');
	
	define('MUSEUMOLI_MENU_DOCUMENTACIO','Documentació');
	define('MUSEUMOLI_MENU_DOCUMENTACIO_NEW','Nou document');
	define('MUSEUMOLI_MENU_DOCUMENTACIO_LIST','Llistar documents');
	define('MUSEUMOLI_MENU_DOCUMENTACIO_LIST_BIN','Paperera de reciclatge');
	
	define('MUSEUMOLI_MENU_COLABORADOR','Colaboradors');
	define('MUSEUMOLI_MENU_COLABORADOR_NEW','Nou colaborador');
	define('MUSEUMOLI_MENU_COLABORADOR_LIST','Llistar colaboradors');
	define('MUSEUMOLI_MENU_COLABORADOR_LIST_BIN','Paperera de reciclatge');
	
	define('MUSEUMOLI_MENU_EXPOSICIOCATEGORY_NEW','Nova categoria');
	define('MUSEUMOLI_MENU_EXPOSICIOCATEGORY_LIST','Llistar categories');
	define('MUSEUMOLI_MENU_EXPOSICIOCATEGORY_LIST_BIN','Paperera de reciclatge');
	
	define('MUSEUMOLI_MENU_EXPOSICIO','Exposicions');
	define('MUSEUMOLI_MENU_EXPOSICIO_NEW','Nova exposició');
	define('MUSEUMOLI_MENU_EXPOSICIO_LIST','Llistar exposicions');
	define('MUSEUMOLI_MENU_EXPOSICIO_LIST_BIN','Paperera de reciclatge');
	
	define('MUSEUMOLI_MENU_SENTENCE','Frases Home');
	define('MUSEUMOLI_MENU_SENTENCE_NEW','Nova frase');
	define('MUSEUMOLI_MENU_SENTENCE_LIST','Llistar frases');
	define('MUSEUMOLI_MENU_SENTENCE_LIST_BIN','Paperera');
	
	define('MUSEUMOLI_MENU_NEW', 'Notícies');
	define('MUSEUMOLI_MENU_NEW_NEW', 'Entrar nova notícia');
	define('MUSEUMOLI_MENU_NEW_LIST', 'Llistar i editar notícies');
	define('MUSEUMOLI_MENU_NEW_BIN', 'Paperera de reciclatge');
	define('MUSEUMOLI_MENU_CATEGORY', 'Categories');
	define('MUSEUMOLI_MENU_CATEGORY_NEW', 'Entrar nova categoria');
	define('MUSEUMOLI_MENU_CATEGORY_LIST', 'Llistar categories');
}

$gl_messages['no_delete_taller']='El registre 12 i 19 no es poden esborrar, pertanyen a "Visites+taller" i "Cursos i seminaris"';

// captions seccio
$gl_caption['c_visitant_id'] = '';
$gl_caption['c_videoframe'] = 'Video';
$gl_caption['c_preu'] = 'Price per person';
$gl_caption['c_preu_alumne'] = 'Price per student';
$gl_caption['c_url1'] = 'URL 1';
$gl_caption['c_url2'] = 'URL 2';
$gl_caption['c_status'] = 'State';
$gl_caption['c_parent_module'] = '';
$gl_caption['c_entered'] = 'Created';
$gl_caption['c_ordre'] = 'Order';
$gl_caption['c_bin'] = '';
$gl_caption['c_visitant_id'] = 'Kind of visiter';
$gl_caption['c_visitant'] = 'Kind of visiter';
$gl_caption['c_visita'] = 'Visit';
$gl_caption_exposicio['c_visita'] = 'Exhibition';
$gl_caption_curs['c_visita'] = 'Course';
$gl_caption_colaborador['c_visita'] = 'Collaborator name';
$gl_caption['c_datatext'] = 'Timetable';
$gl_caption['c_year'] = 'Year';
$gl_caption_exposicio['c_datatext'] = 'Inauguration';
$gl_caption['c_mes_info'] = 'General information';
$gl_caption_curs['c_mes_info'] = 'Program';
$gl_caption_colaborador['c_mes_info'] = 'Other information';
$gl_caption['c_reserva'] = 'Reservations';
$gl_caption['c_documentacio'] = 'Documentation';
$gl_caption['c_nivell_educatiu'] = 'Education level';
$gl_caption['c_duracio'] = 'Duration';
$gl_caption['c_inscripcio'] = 'Enrollment';
$gl_caption['c_tipus_curs'] = 'Type';
$gl_caption['c_tipus_curs_curs'] = 'Courses and workshops';
$gl_caption['c_tipus_curs_seminari'] = 'Seminaries';
$gl_caption['c_tipus_curs_moli'] = 'Mill - Paper art';
$gl_caption['c_tipus_colaborador'] = 'Type';
$gl_caption['c_tipus_colaborador_artista'] = 'Artists';
$gl_caption['c_tipus_colaborador_entitat'] = 'Entities';
$gl_caption['c_tipus_colaborador_empresa'] = 'Companies and institutions';


$gl_caption['c_prepare'] = 'En preparació';
$gl_caption['c_review'] = 'Per revisar';
$gl_caption['c_public'] = 'Públic';
$gl_caption['c_archived'] = 'Arxivat';
$gl_caption['c_new'] = 'New';
$gl_caption['c_title'] = 'Title';
$gl_caption['c_subtitle'] = 'Subtitle';
$gl_caption['c_content'] = 'Description at the form';
$gl_caption['c_status'] = 'Estat';
$gl_caption['c_category_id'] = 'Categoria';
$gl_caption['c_filter_category_id'] = 'Totes les categories';
$gl_caption['c_filter_status'] = 'Tots els estats';
$gl_caption['c_ordre'] = 'Ordre';


$gl_caption['c_category_id'] = 'Category';
$gl_caption['c_ordre'] = 'Ordre';
$gl_caption_image['c_name'] = 'Name';
$gl_caption['c_url1_name']='Nom URL(1)';
$gl_caption['c_url1']='URL(1)';
$gl_caption['c_url2_name']='Nom URL(2)';
$gl_caption['c_url2']='URL(2)';
$gl_caption['c_videoframe']='Video (youtube, metacafe...)';
$gl_caption['c_file']='Documentation';
$gl_caption_curs['c_file_download']='Download program';
$gl_caption['c_in_home']='Surt a la home';
$gl_caption['c_content_list']='Descripció a l\'index de visites';

$gl_caption['c_sentence_title']='Title';
$gl_caption['c_sentence']='Sentence';
$gl_caption['c_sentence_url_name']='Name of link';
$gl_caption['c_sentence_url']='Link';

$gl_caption['c_mes_informacio']='More information';
?>
