<?php
// no es poden posar ; entre mig de la consulta ja que parteix les consultes per el ; a no ser que fem servir $update_sql en array()

$gl_version= 4300; // passem a versió 4.3
	
	
/* 28-6-12 
Actualitzacio dels caption de les imatges
*/

global $gl_languages;
$tables = Db::get_rows('SHOW TABLES');

	foreach($tables as $row){
		$table = current($row);
		if (substr($table,-6)=='_image'){
			$update_sql[]="
			CREATE TABLE `".$table."_language` (
				`image_id` INT(11) NOT NULL DEFAULT '0',
				`language` CHAR(3) NOT NULL COLLATE 'utf8_spanish2_ci',
				`image_alt` VARCHAR(150) NOT NULL COLLATE 'utf8_spanish2_ci'
			)
			COLLATE='utf8_spanish2_ci'
			ENGINE=MyISAM;";
			
			foreach($gl_languages['names'] as $lang=>$val){
				$update_sql[]="
				INSERT INTO ".$table."_language (image_id, language) SELECT DISTINCT image_id, '".$lang."' FROM ".$table.";";
			}
			
		}
	}
?>