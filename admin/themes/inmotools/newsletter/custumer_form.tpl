<script language="JavaScript" src="/admin/jscripts/validacio.js?v=<?=$version?>"></script>
<script type="text/javascript">
$(function() {
	set_focus(document.theForm);
});
</script>
<form action="<?=$link_action_delete?>" method="post" name="form_delete" id="form_delete" target="save_frame">
    <input name="<?=$id_field?>" type="hidden" value="<?=$id?>" />
  </form>
  <form action="<?=$link_action?>" method="post" name="theForm" id="theForm" onsubmit="<?=$js_string?>">
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="form">  
    <tr>
      <td><?if ($form_new):?>
        <div id="form_buttons_holder"><table id ="form_buttons">
          <tr> 
            <td class="buttons_top"><div class="right"><input type="submit" name="Submit" value="<?=$c_send_new?>" class="btn_form1"></div></td>
          </tr>
        </table></div>
        <?endif //form_new?><?if ($form_edit):?>
        <div id="form_buttons_holder"><table id ="form_buttons">
          <tr> 
            <td class="buttons_top"><div class="right"><input type="submit" name="Submit" value="<?=$c_send_edit?>" class="btn_form1"><a class="btn_form_delete" href="javascript:submit_form('<?=$c_confirm_form_bin?>', true);"><?=$c_delete?></a></div></td>
          </tr>
        </table></div>
        <?endif //form_edit?>
        <table width="100%" cellpadding="4" cellspacing="0">
          <tr>
            <td class="formCaptionTitle"><strong> </strong>
                <table border="0" cellpadding="0" cellspacing="0">
                  <tr>
                    <td><strong>
                      <?=$c_name?>:</strong></td>
                    <td>&nbsp;
                        <?=$name?>
                      &nbsp;</td>
                    <td><strong>&nbsp;
                          <?=$c_surnames?>:&nbsp;</strong></td>
                    <td>&nbsp;
                        <?=$surname1?>
                      &nbsp;</td>
                    <td>&nbsp;
                        <?=$surname2?></td>                   
                  </tr>
              </table></td>
          </tr>
        </table>
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td><table width="100%" cellpadding="4" cellspacing="0" class="formstbl">
              <tr>
                <td width="150" valign="top" class="formsCaption"><strong>
                  <?=$c_mail;?>:</strong></td>
                <td valign="top" class="forms"><?=$mail;?></td>
              </tr>
              <tr>
                <td height="30" valign="top" class="formsCaption"><strong>
                  <?=$c_group_id;?>:</strong></td>
                <td height="30" valign="top" class="forms"><?=$group_id;?></td>
              </tr>

            </table><div class="sep_soft"></div>              
              <table width="100%" cellpadding="4" cellspacing="0" class="formstbl">
              <tr>
                <td width="150" valign="top" class="formsCaption"><strong>
                  <?=$c_phone?>:</strong></td>
                <td class="forms"><table border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td><?=$c_phone1?></td>
                    <td><?=$c_phone2;?></td>
                    <td><?=$c_phone3;?></td>
                  </tr>
                  <tr>
                    <td><?=$phone1?>
                      <input name="custumer_id[<?=$custumer_id?>]" type="hidden" value="<?=$custumer_id?>" />                      &nbsp;&nbsp;</td>
                    <td><?=$phone2;?>                      &nbsp;&nbsp;</td>
                    <td><?=$phone3;?>                      &nbsp;&nbsp;</td>
                  </tr>
                </table></td>
              </tr>
              <tr>
                <td valign="top" class="formsCaption"><strong>
                  <?=$c_address?>: </strong></td>
                <td class="forms"><table border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td><?=$c_adress?></td>
                      <td><?=$c_numstreet;?></td>
                      <td><?=$c_block;?></td>
                      <td><?=$c_flat;?></td>
                      <td><?=$c_door;?></td>
                    </tr>
                    <tr>
                      <td><?=$adress?>
                        &nbsp;&nbsp;</td>
                      <td><?=$numstreet;?>
                        &nbsp;&nbsp;</td>
                      <td><?=$block;?>
                        &nbsp;&nbsp;</td>
                      <td><?=$flat;?>
                        &nbsp;&nbsp;</td>
                      <td><?=$door;?></td>
                    </tr>
                </table>                  </td>
              </tr>
              <tr>
                <td valign="top" class="formsCaption"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;
                        <?=$c_town;?>:</td>
                  </tr>
                </table></td>
                <td class="forms"><table border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td><?=$town;?></td>
                  </tr>

                </table></td>
              </tr>
              <tr>
                <td valign="top" class="formsCaption"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;
                        <?=$c_province;?>:</td>
                  </tr>
                </table></td>
                <td class="forms"><table border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td><?=$province;?></td>
                  </tr>

                </table></td>
              </tr>
              <tr>
                <td valign="top" class="formsCaption"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;
                        <?=$c_zip;?>:</td>
                  </tr>
                </table></td>
                <td class="forms"><table border="0" cellspacing="0" cellpadding="0">

                  <tr>
                    <td><?=$zip;?></td>
                  </tr>

                </table></td>
              </tr>
              <tr>
                <td valign="top" class="formsCaption"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;
                        <?=$c_country;?>:</td>
                  </tr>
                </table></td>
                <td class="forms"><table border="0" cellspacing="0" cellpadding="0">

                  <tr>
                    <td><?=$country;?></td>
                  </tr>

                </table></td>
              </tr>
              <tr>
                <td valign="top" class="formsCaption"><strong>
                  <?=$c_vat;?>:</strong></td>
                <td class="forms"><?=$vat;?></td>
              </tr>
				<tr>
					<td valign="top" class="formsCaption"><strong>
							<?=$c_prefered_language?>:</strong></td>
					<td class="forms"><?=$prefered_language?></td>
				</tr>
              <tr>
                <td valign="top" class="formsCaption"><strong>
                  <?=$c_observations1?>: </strong></td>
                <td class="forms"><?=$observations1?></td>
              </tr>
            </table>
            </td>
          </tr>
        </table>
        <?if ($form_new):?>
			<table width="100%" cellpadding="4" cellspacing="2" class="formsButtonstbl">
                <tr> 
                  <td align="center" class="formsButtons"><div class="centerFloat1">
                        <div class="centerFloat2"><input type="submit" name="Submit" value="<?=$c_send_new?>" class="btn_form1"></div></div> 
                  </td>
                </tr>
          </table>
              <?endif //form_new?><?if ($form_edit):?>
              <table width="100%" cellpadding="4" cellspacing="2" class="formsButtonstbl">
                <tr> 
                  <td align="center" class="formsButtons"><div class="centerFloat1">
                        <div class="centerFloat2"><input type="submit" name="Submit" value="<?=$c_send_edit?>" class="btn_form1">&nbsp;&nbsp;<a class="btn_form_delete" href="javascript:submit_form('<?=$c_confirm_form_bin?>', true);">
                      <?=$c_delete?>
                      </a></div></div></td>
                </tr>
              </table>
              <?endif //form_edit?></td>
    </tr>
</table>
</form>