#/usr/bin/ruby
# This library handles extensions to base Ruby types
# that allow them to emit their equivalent as valid JavaScript
# code.
require 'date'

# You can use JSWrapper to act as a top-level container
# that holds sub-objects. A Hash object would also serve,
# though, since it's turned into a JavaScript Object.
class JSWrapper
  def initialize
    @contents = []
  end
  def add(name, value)
    @contents.push(Hash[name, value])
  end
  def as_js(name='')
    out_str = ''
    unless name.empty?
      out_str << 'var ' << name << '={'
    else
      out_str << '{'
    end
    @contents.each do |item|
      out_str << item.keys[0] << ':' << item[item.keys[0]].to_js << ','
    end
    out_str = out_str[0..out_str.rindex(',')-1] << '};'
    return out_str
  end
end

class Object
  def to_js
    h = Hash.new
    for var in self.instance_variables.sort
      h[var.sub('@', '')] = self.instance_eval(var)
    end
    return h.to_js
  end
end

class NilClass
  def to_js
    return 'null'
  end
end

class TrueClass
  def to_js
    return 'true'
  end
end

class FalseClass
  def to_js
    return 'false'
  end
end

class String
  def to_js
    out_str = ''
    # Escape any backslashes
    out_str = self.to_s.gsub(/([\\])/, '\1\1')
    # Turn newline characters into their symbolic equivalent
    out_str = out_str.gsub(/\n/, '\\n')
    # Turn cr/lf pairs into just lf (Unix style, acceptable on browsers)
    out_str = out_str.gsub(/\r\n/, "\n")
    # Turn tabs into their symbolic equivalents
    out_str = out_str.gsub(/\t/, '\\t')
    out_str = out_str.gsub('"', '\\"')
    out_str = '"' << out_str << '"'
    return out_str
  end
end

class Numeric
  def to_js
    out_str = ''
    out_str << self.to_s
    return out_str
  end
end

class Array
  def to_js
    out_str = '['
    self.each do |item|
      out_str << item.to_js << ","
    end
    out_str = out_str[0..out_str.rindex(',')-1] << ']'
    return out_str
  end
end

class Hash
  def to_js
    out_str = '{'
    self.keys.sort.each do |item|
      out_str << item << ':' << self[item].to_js << ","
    end
    out_str = out_str[0..out_str.rindex(',')-1] << '}'
    return out_str
  end
end

class Struct
  def to_js
    h = Hash.new
    self.members.each do |member|
      h[member]=self[member]
    end
    return h.to_js
  end
end

class Time
  def to_js
    out_str = 'new Date("' << self.localtime.localtime.to_s << '")'
    return out_str
  end
end

class Date
  def to_js
    return Time.local(self.year, self.mon, self.mday).to_js
  end
end

class Range
  def to_js
    h = Hash.new
    h['first'], h['last'] = self.first, self.last
    return h.to_js
  end
end
