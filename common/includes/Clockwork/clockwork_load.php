<?

global $configuration, $gl_is_letnd;

include( DOCUMENT_ROOT . 'common/includes/Clockwork/Psr/Log/LogLevel.php' );
include( DOCUMENT_ROOT . 'common/includes/Clockwork/Psr/Log/LoggerInterface.php' );
include( DOCUMENT_ROOT . 'common/includes/Clockwork/Psr/Log/AbstractLogger.php' );
include( DOCUMENT_ROOT . 'common/includes/Clockwork/Request/Log.php' );
include( DOCUMENT_ROOT . 'common/includes/Clockwork/Request/Request.php' );
include( DOCUMENT_ROOT . 'common/includes/Clockwork/Request/Timeline.php' );
include( DOCUMENT_ROOT . 'common/includes/Clockwork/Storage/StorageInterface.php' );
include( DOCUMENT_ROOT . 'common/includes/Clockwork/Storage/Storage.php' );
include( DOCUMENT_ROOT . 'common/includes/Clockwork/Storage/SqlStorage.php' );
include( DOCUMENT_ROOT . 'common/includes/Clockwork/DataSource/DataSourceInterface.php' );
include( DOCUMENT_ROOT . 'common/includes/Clockwork/DataSource/DataSource.php' );
include( DOCUMENT_ROOT . 'common/includes/Clockwork/DataSource/PhpDataSource.php' );
include( DOCUMENT_ROOT . 'common/includes/Clockwork/DataSource/LetndDataSource.php' );
include( DOCUMENT_ROOT . 'common/includes/Clockwork/Helpers/StackTrace.php' );
include( DOCUMENT_ROOT . 'common/includes/Clockwork/Helpers/StackFrame.php' );
include( DOCUMENT_ROOT . 'common/includes/Clockwork/Helpers/Serializer.php' );
include( DOCUMENT_ROOT . 'common/includes/Clockwork/Clockwork.php' );

if ( $gl_is_letnd ) {
	$db_name      = $configuration['db_name_mother'];
	$db_host      = $configuration['db_host_mother'];
	$db_user_name = $configuration['db_user_name_mother'];
	$db_password  = $configuration['db_password_mother'];
}
else {
	$db_name      = $configuration['dbname'];
	$db_host      = $configuration['dbhost'];
	$db_user_name = $configuration['dbuname'];
	$db_password  = $configuration['dbpass'];
}