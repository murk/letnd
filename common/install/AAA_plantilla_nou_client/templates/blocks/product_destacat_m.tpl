<section id="products-home">
	<h6><?= $title ?></h6>

	<div class="product-list">
		<? foreach ( $loop as $l ): extract( $l ) ?>

			<? $hidden_class = $conta >= 4 ? 'hidden' : '' // TODO-i Si es un slider i van amagats a partir d'un X items ?>

			<? include( PATH_TEMPLATES . 'product/product_list_item.tpl' ) ?>

		<? endforeach //$loop?>
	</div>
</section>