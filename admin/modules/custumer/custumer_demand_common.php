<?
/*
TODO
living
dinning

service_room
service_bath
study
courtyard

parket
ceramic
gres

laminated

parking

facing

sea_view
clear_view

-- level_count
marble



TODO - Afegir a la cerca de solicituds tots els camps nous
TODO - Borrar camps  a les bbdd ( xxcategory_id, xxmunicipi_id )
TODO - Afegir efficiency
TODO - Office, camp nou per naus ( INVESTIN )


*/

// Camps per la cerca exacta
// Camps pel llistat de solicituds
// Han de coincidir amb detalls fitxa propietats i fitxa solicituds

$GLOBALS ['gl_demand_details'] = array(
	'room' => '>=',
	'bathroom' => '>=',
	'living' => '>=',
	'dinning' => '>=',
	'wc' => '>=',
	'level_count' => '=', // plantes potser que  sigui millor mes o menys, per tant no puc possar >=
	'gres' => '=',
	'ceramic' => '=',
	'parket' => '=',
	'laminated' => '=',
	'marble' => '=',
	'laundry' => '=',
	'garage' => '=',
	'study' => '=',
	'parking' => '=',
	'storage' => '=',
	'courtyard' => '=',
	'garden' => '=',
	'furniture' => '=',
	'terrace' => '=',
	'centralheating' => '=',
	'citygas' => '=',
	'airconditioned' => '=',
	'swimmingpool' => '=',
	'elevator' => '=',
	'sea_view' => '=',
	'clear_view' => '=',
	'service_room' => '=',
	'balcony' => '=',
	'facing' => '=',
	'service_bath' => '=',
	);

function demand_get_row_details ($name, $value) {

	if (!$value || $value == '-')
	return '';

	else
	return '
	<tr class="listDetails">
		<td valign="top"><strong class="listItem">' . $name . ': </strong>&nbsp;</td>
		<td valign="top">' . $value . '</td>
	</tr>';
}
function demand_get_rows_details (&$tpl, $loop_vars) {

	$vars = &$tpl->vars;
	$html = '';
	$fields = $GLOBALS ['gl_demand_details'];

	foreach ( $fields as $field => $sign ) {
		$html .= demand_get_row_details( $vars[ 'c_' . $field ],  $loop_vars[ $field ] );
	}

	return $html;
}



function demand_records_walk(&$listing, $land, $land_units, $demand_id = false){

	$action = $GLOBALS['gl_action'];

	$is_input = $GLOBALS ['gl_write'] && $listing->list_show_type == 'form';
	//Debug::p( $GLOBALS ['gl_write'] );
	$html          = '';
	$input_name    = 'municipi_id[' . $demand_id . ']';
	$input_id      = 'municipi_id_' . $demand_id;
	$textarea_cols = 4;
	$tool          = $GLOBALS['gl_tool'];
	$tool_section  = $GLOBALS['gl_tool_section'];
	$name          = 'municipi_id';
	$id            = $demand_id;

	// Municipi
	if ( $demand_id ) {

		// Comarques soles
		$checked_results1 = Db::get_rows( "
 			SELECT municipi_id, inmo__comarca.comarca_id, comarca
 				FROM inmo__comarca, custumer__demand_to_municipi
 				WHERE demand_id = " . $demand_id . "
 				AND custumer__demand_to_municipi.comarca_id = inmo__comarca.comarca_id
 				AND municipi_id = 0
 				ORDER BY comarca ASC" );

		// Municipis
		$checked_results2 = Db::get_rows( "
 			SELECT inmo__municipi.municipi_id, inmo__comarca.comarca_id, municipi, comarca
 				FROM inmo__municipi, inmo__comarca, custumer__demand_to_municipi
 				WHERE demand_id = " . $demand_id . "
 				AND custumer__demand_to_municipi.municipi_id = inmo__municipi.municipi_id
 				AND custumer__demand_to_municipi.comarca_id = inmo__comarca.comarca_id
 				ORDER BY municipi ASC" );


		$checked_results = array_merge( $checked_results1, $checked_results2 );

		// Debug::p( $checked_results );

		if ( $checked_results ) {
			$i = 1;
			foreach ( $checked_results as $rs ) {
				$text = $rs['municipi_id'] ? $rs['municipi'] . ' - ' . $rs['comarca'] : $rs['comarca'];
				if ( $is_input ) {
					$value = $rs['comarca_id'] . '_' . $rs['municipi_id'];
					$html .= '<td><input type="checkbox" value="' . $value . '" checked="checked" name="' . $input_name . "[" . $value . ']" id="' . $input_id . "_" . $value . '" /></td><td align="left"><label for="' . $input_id . "_" . $value . '">' . $text . '&nbsp;&nbsp;&nbsp;&nbsp;</label></td>';

					if ( $i == $textarea_cols ) {
						$html = '<tr>' . $html . '</tr>';
						$i    = 0;
					}

				} else {
					$html .= '<li>' . $text . '</li>';
				}
				$i ++;
			}
		}


	}

	if ( $is_input ) {
		$html = '<table border="0" cellspacing="0" cellpadding="0">' . $html . '</table>';
		$html .= '<table class="checkboxes_long" id="' . $input_id . '_table" border="0" cellspacing="0" cellpadding="0"></table><label class="active prefix checkboxes_long"><span>+</span><input id="' . $input_id . '_add" type="text"></label>';
		$GLOBALS['gl_page']->javascript .=
			"
				add_checkboxes_long ('" . $tool . "', '" . $tool_section . "', '" . $GLOBALS['gl_menu_id'] . "','', '" . $name
			. "', '" . $id . "', '', false);
				";
	} else {
		if ( $html )
			$html = '<ul>' . $html . '</ul>';
	}

	$ret['municipi_id'] = $html;

	$ret['land'] = ( $land_units == 'ha' ) ? $land / 10000 : $land;

	return $ret;
}

?>