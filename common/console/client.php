<?

namespace Console\Command;

// use Nette\Security\Passwords;
use Console;
use Console\Util;
use Db;
use Query;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Style\SymfonyStyle;

// use Symfony\Component\Console\Question\Question;


final class Client extends Command {

	private $output;
	private $input;

	protected function configure() {
		$this->setName( 'client' )
		     ->setDescription( 'Utilitats clients' )
		     ->addArgument( 'client', InputArgument::REQUIRED, "Client on realitzar les accions" )
		     ->addArgument( 'action', InputArgument::REQUIRED, "Accio a realitzar:  'regen', 'version', 'version-down', 'id', 'data', 'config', 'add-var', 'rename-var', 'delete-var', 'download', 'upload'" )
		     ->addArgument( 'arg1', InputArgument::OPTIONAL, "" )
		     ->addArgument( 'arg2', InputArgument::OPTIONAL, "" )
		     ->addArgument( 'arg3', InputArgument::OPTIONAL, "" )
		     ->addArgument( 'arg4', InputArgument::OPTIONAL, "" )
		     ->addArgument( 'arg5', InputArgument::OPTIONAL, "" )
		     ->setAliases( [ 'clients' ] );
	}

	protected function execute( InputInterface $input, OutputInterface $output ) {
		$this->input  = $input;
		$this->output = $output;
		$io           = new SymfonyStyle( $input, $output );

		$actions = [
			'regen',
			'version',
			'version-down',
			'id',
			'data',
			'config',
			'add-var',
			'rename-var',
			'delete-var',
			'download',
			'upload'
		];

		$client = $input->getArgument( 'client' );
		$action = $input->getArgument( 'action' );
		$arg1   = $input->getArgument( 'arg1' );
		$arg2   = $input->getArgument( 'arg2' );
		$arg3   = $input->getArgument( 'arg3' );
		$arg4   = $input->getArgument( 'arg4' );
		$arg5   = $input->getArgument( 'arg5' );

		if ( ! in_array( $action, $actions ) ) {


			$helper   = $this->getHelper( 'question' );
			$question = new ChoiceQuestion(
				'Acció a realitzar?',
				$actions );

			$question->setErrorMessage( 'Accio %s no es valid.' );

			$action = $helper->ask( $input, $output, $question );


		}
		$action = str_replace( '-', '_', $action );

		$clients = Util::get_server_clients( $client );

		if ( ! isset( $clients['questions'] ) ) {
			$client_rs = $clients;
		}
		elseif ( empty( $clients['questions'] ) ) {
			$client_rs = $clients['clients'];
		}
		else {
			$helper   = $this->getHelper( 'question' );
			$question = new ChoiceQuestion(
				'Quin client?',
				$clients['questions'] );

			$question->setErrorMessage( 'El client no és valid' );

			$client_dir = $helper->ask( $input, $output, $question );
			$client_rs  = $clients['clients'][ $client_dir ];
		}

		//$output->writeln( "Acció: $action" );
		//$output->writeln( "Client: {$client_rs['client_dir']}" );
		if ( $client_rs ) {
			$io->title( "$action: {$client_rs['fiscalname']} - {$client_rs['domain']}" );
		}
		else {
			$io->title( "$action: $client ( no trobat online )" );
		}
		$this->$action( $action, $client, $client_rs, $io, ...[ $arg1, $arg2, $arg3, $arg4, $arg5 ] );

		return 0;
	}

	private function regen( $action, $client, $client_rs, SymfonyStyle $io ) {

		if ( ! Util::is_client_on_server( $client_rs, $io ) ) return;

		Util::connect_mother();

		$q = new Query( "{$client_rs['dbname']}.all__configpublic" );
		$q->update( [ 'value' => 1 ], "name='force_css_jscript_generation'" );
		$io->text( "Forçat a una nova versio del client online" );
		$io->text( $q->query );

		Db::reconnect();
	}

	private function id( $action, $client, $client_rs, SymfonyStyle $io ) {

		if ( ! Util::is_client_on_server( $client_rs, $io ) ) return;

		$io->text( "Id client: ${client_rs['client_id']}" );
	}

	private function data( $action, $client, $client_rs, SymfonyStyle $io ) {

		if ( ! Util::is_client_on_server( $client_rs, $io ) ) return;

		foreach ( $client_rs as $key => $val ) {
			if ( $val ) {
				$io->text( "$key: $val" );
			}
		}
	}

	private function version_down( $action, $client, $client_rs, SymfonyStyle $io ) {

		$q = new Query( "$client.all__configadmin" );

		if ( Util::is_db( $client ) ) {
			$q->update( 'value = value - 1', "name='version'" );
			$update_query = $q->query;

			$version = $q->get_first( 'value', "name='version'" );

			$io->text( $update_query );
			$io->text( "LOCALHOST: Canviat a versio de letnd $version" );
		}
		if ( Util::connect_vagrant( $client ) ) {
			$q->update( 'value = value - 1', "name='version'" );
			$update_query = $q->query;

			$version = $q->get_first( 'value', "name='version'" );

			$io->text( $update_query );
			$io->text( "VAGRANT: Canviat a versio de letnd $version" );
			Db::reconnect();
		}
	}

	private function version( $action, $client, $client_rs, SymfonyStyle $io ) {

		$q = new Query( "$client.all__configadmin" );

		if ( Util::is_db( $client ) ) {
			$version = $q->get_first( 'value', "name='version'" );
			$io->text( "LOCALHOST: Versio de letnd $version" );
		}
		if ( Util::connect_vagrant( $client ) ) {
			$version = $q->get_first( 'value', "name='version'" );
			$io->text( "VAGRANT: Versio de letnd $version" );
			Db::reconnect();
		}
	}

	private function config( $action, $client, $client_rs, SymfonyStyle $io, ...$args ) {

		if ( ! Util::is_client_on_server( $client_rs, $io ) ) return;

		Util::connect_mother();

		$tool  = $args[0];
		$place = $args[1];
		$name  = $args[2];
		$value = $args[3];

		$table = "`{$client_rs['dbname']}`.{$tool}__config{$place}";

		// Update
		if ( ! is_null( $value ) ) {
			$q     = new Query( $table );
			$query = $q->prepare_update( [ 'value' => $value ], "name=%s", $name );

			$helper   = $this->getHelper( 'question' );
			$question = new ConfirmationQuestion( "Executar aquest query? $query", false );

			if ( ! $helper->ask( $this->input, $this->output, $question ) ) {
				$io->text( "Abortat!" );

				return;
			}

			$q->execute_update( $query );
			$io->text( "Actualitzat el valor: \"$value\" a $table $name" );

			Db::reconnect();
		}

		// View
		if ( $name ) {
			$value = Db::get_first( "SELECT value FROM $table WHERE name = %s", $name );
			$io->text( utf8_decode( "El valor de $table: $name és: $value" ) );
		}

		if ( ! $name || ! $value ) {

			if ( $name ) {
				$results = Db::get_rows( "SELECT name, value FROM $table WHERE name LIKE %s ORDER BY name", "%$name%" );
			}
			else {
				$results = Db::get_rows( "SELECT name, value FROM $table ORDER BY name" );
			}
			$arr = [];
			foreach ( $results as $rs ) {
				$name   = $rs['name'];
				$value  = $rs['value'];
				$arr [] = [ $name, $value ];
			}
			$io->table(
				[ 'Name', 'Value' ],
				$arr
			);
		}

		Db::reconnect();
	}

	private function rename_var( $action, $client, $client_rs, SymfonyStyle $io, ...$args ) {

		$q      = new Query( "$client.client__configpublic" );
		$q_lang = new Query( "$client.client__configpublic_language" );


		$old_var = trim( $args[0] );
		$new_var = trim( $args[1] );

		if ( $old_var == 'help' ){
			$io->text( 'client <nom client> rename-var <nom variable vell> <nom variable nou>' );
			return;
		}

		if ( ! $new_var || ! $old_var ) {
			$io->text( 'Falta un argument pel nom de variable nou' );

			return;
		}


		if ( Util::is_db( $client ) ) {

			$updated = $this->rename_var_update( $q, $q_lang, $old_var, $new_var, $io );
			if ( $updated ) {
				$io->text( "LOCALHOST: Canviat la variable $old_var per $new_var" );
			}
			else {
				$io->text( "LOCALHOST: No existeix $old_var" );
			}
		}
		if ( Util::connect_vagrant( $client ) ) {

			$updated = $this->rename_var_update( $q, $q_lang, $old_var, $new_var, $io );
			if ( $updated ) {
				$io->text( "VAGRANT: Canviat la variable $old_var per $new_var" );
			}
			else {
				$io->text( "VAGRANT: No existeix $old_var" );
			}
			Db::reconnect();
		}
	}

	private function rename_var_update( Query $q, Query $q_lang, $old_var, $new_var, SymfonyStyle $io ) {

		if ( ! $q->get_first( 'name', "name= %s", $old_var ) ) return false;

		$q->update( [ 'name' => $new_var ], "name= %s", $old_var );
		$update_query = $q->query;
		$io->text( $update_query );

		$q_lang->update( [ 'name' => $new_var ], "name= %s", $old_var );
		$update_query = $q_lang->query;
		$io->text( $update_query );

		return true;
	}

	private function add_var( $action, $client, $client_rs, SymfonyStyle $io, ...$args ) {

		$q      = new Query( "$client.client__configpublic" );
		$q_lang = new Query( "$client.client__configpublic_language" );


		$new_var      = trim( $args[0] );
		$type         = trim( $args[1] );
		$page_text_id = trim( $args[2] );
		$order        = trim( $args[3] );
		$num          = (int) trim( $args[4] );

		if ( $new_var == 'help' ){
			$io->text( 'client <nom client> add-var <nom variable> <tipus [text|htmlbasic|html]> <page_text_id> <ordre> <repeticions>' );
			return;
		}

		if ( ! $new_var ) {
			$io->text( 'Falta un argument pel nom de variable' );

			return;
		}

		if ( $num == 0 ) $num = 1;

		$query           = '';

		for ( $i = 1; $i <= $num; $i ++ ) {

			if ( $num > 1 ) {
				$name = "${new_var}_${i}";
			}
			else {
				$name = $new_var;
			}

			$query .= "\n\n" . $q->prepare_insert(
					[
						'name'  => $name,
						'type'  => $type,
						'page_text_id'  => $page_text_id,
						'ordre' => $order,
					]
				) . ";";

			foreach ( $this->get_client_languages( $client ) as $rs ) {

				$query .= "\n\n" . $q_lang->prepare_insert(
						[
							'name'     => $name,
							'language' => $rs['code']
						]
					) . ";";

			}
			$order ++;
		}

		$helper   = $this->getHelper( 'question' );
		$question = new ConfirmationQuestion( "Executar aquest query? $query", false );

		if ( ! $helper->ask( $this->input, $this->output, $question ) ) {
			$io->text( "Abortat!" );

			return;
		}


		if ( Util::is_db( $client ) ) {

			Db::multi_query( $query );
			$io->text( "LOCALHOST: Afegida la variable $new_var" );

		}
		if ( Util::connect_vagrant( $client ) ) {

			Db::multi_query( $query );
			$io->text( "LOCALHOST: Afegida la variable $new_var" );
			Db::reconnect();
		}
	}

	private function delete_var( $action, $client, $client_rs, SymfonyStyle $io, ...$args ) {

		$q      = new Query( "$client.client__configpublic" );
		$q_lang = new Query( "$client.client__configpublic_language" );


		$old_var = trim( $args[0] );

		if ( $old_var == 'help' ){
			$io->text( 'client <nom client> delete-var <nom variable>' );
			return;
		}

		if ( ! $old_var ) {
			$io->text( 'Falta un argument pel nom de variable a borrar' );

			return;
		}

		$query = $q->prepare_delete( "name= %s", $old_var );

		$helper   = $this->getHelper( 'question' );
		$question = new ConfirmationQuestion( "Executar aquest query? $query", false );

		if ( ! $helper->ask( $this->input, $this->output, $question ) ) {
			$io->text( "Abortat!" );

			return;
		}

		if ( Util::is_db( $client ) ) {

			$updated = $this->delete_var_execute( $q, $q_lang, $old_var, $io );
			if ( $updated ) {
				$io->text( "LOCALHOST: Eliminada la variable $old_var" );
			}
			else {
				$io->text( "LOCALHOST: No existeix $old_var" );
			}
		}
		if ( Util::connect_vagrant( $client ) ) {

			$updated = $this->delete_var_execute( $q, $q_lang, $old_var, $io );
			if ( $updated ) {
				$io->text( "VAGRANT: Eliminada la variable $old_var" );
			}
			else {
				$io->text( "VAGRANT: No existeix $old_var" );
			}
			Db::reconnect();
		}
	}

	private function delete_var_execute( Query $q, Query $q_lang, $old_var, SymfonyStyle $io ) {

		if ( ! $q->get_first( 'name', "name= %s", $old_var ) ) return false;

		$q->delete( "name= %s", $old_var );
		$delete_query = $q->query;
		$io->text( $delete_query );

		$q_lang->delete( "name= %s", $old_var );
		$delete_query = $q_lang->query;
		$io->text( $delete_query );

		return true;
	}

	private function get_client_languages( $client ) {

		// Assumeixo que només tinc la BBDD en un lloc, però dono preferència al vagrant
		$q = new Query( "$client.all__language" );
		if ( Util::connect_vagrant( $client ) ) {
			$languages = $q->get( 'code', 'public > 0' );
			Db::reconnect();

			return $languages;
		}
		if ( Util::is_db( $client ) ) {
			$languages = $q->get( 'code', 'public > 0' );

			return $languages;
		}

		return [];
	}


	private function download( $action, $client, $client_rs, SymfonyStyle $io ) {

		if ( ! Util::is_client_on_server( $client_rs, $io ) ) return;

		$io->text( "Encara per fer" );
	}

	private function upload( $action, $client, $client_rs, SymfonyStyle $io ) {

		if ( ! Util::is_client_on_server( $client_rs, $io ) ) return;

		$io->text( "Encara per fer" );
	}
}