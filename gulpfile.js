/*
Webs en Sass:
gulp --directoriClient

Webs en Css:
gulp css --directoriclient


TODO-i Crear el functions.min.js per la web pública
*/
let gulp = require('gulp'),
	//browserSync = require('browser-sync').create(),
	livereload = require('gulp-livereload'),
	sass = require('gulp-sass'),
	concat = require('gulp-concat'),
	cache = require('gulp-cached'),
	//wait = require('gulp-wait'),

	// post processors
	postcss = require('gulp-postcss'),
	autoprefixer = require('autoprefixer'),
	cleanCSS = require('postcss-clean'), // mes ràpid que el nano
	sourcemaps = require('gulp-sourcemaps'),

	// Sincronitza
	del = require('del'),
	path = require('path'),
	newer = require('gulp-newer'),

	fs = require('fs'),
	colors = require('ansi-colors'),
	args = process.argv.slice(2),
	base_dir,
	base_dir_templates,
	client,
	task;


if (args[0]) {

	if (args[1]){
		client = args[1];
		task = args[0];
	}
	else {
		client = args[0];
		task = 'default';
	}

	client = client.substring(2);

	if (client === 'admin') {
		base_dir = 'admin/themes/inmotools/';
		base_dir_templates = 'admin/themes/inmotools/';
	}
	else {
		base_dir = 'clients/' + client + '/';
		base_dir_templates = 'clients/' + client + '/templates/';

		if (!fs.existsSync(base_dir_templates)) {
			log_error('\n\n Error: el directori ' + base_dir_templates + ' no existeix\n');
			process.exit();
		}

	}
}
else {
	log_error('\n\n Error: S\'ha de posar el nom del client\n');
	process.exit();
}

log_init('\n\n Iniciat: ' + base_dir_templates + '\n');

let src = {
	scss: base_dir_templates + 'scss/*.scss',
	scss_target: base_dir_templates + 'styles/',
	scss_target_dist: base_dir_templates + 'styles/styles.min.css',
	scss_base_file: base_dir_templates + 'scss/styles.scss',

	css: base_dir_templates + 'styles/*.css',
	// stylus: base_dir_templates + 'stylus/*.styl',
	// stylus_base_file: base_dir_templates + 'stylus/styles.styl',
	// stylus_target: base_dir_templates + 'styles',

	tpls: base_dir_templates + '**/*.tpl',

	js: base_dir_templates + 'jscripts/*.js',
	js_target: base_dir_templates + 'jscripts/',

	images: base_dir_templates + 'images/*',

	sync_dir: base_dir_templates,
	sync_dir_dest: 'C:/z-copies-clients-actius/' + client + '/',

	git_dir: '.git',
	git_dir_dest: 'C:/z-copies-clients-actius/git/'
};

if (client === 'admin') src['css'] = base_dir_templates + '**/**/*.css';


// WATCH
gulp.task('watch', function () {

	/*	browserSync.init({
			ghostMode: false,
			scrollProportionally: false
		});*/

	livereload({start: true});

	if (client === 'admin' || task === 'css') {
		gulp.watch(src.css, ['css_watch']);
	}
	else {
		gulp.watch(src.scss, ['sass']);
	}
	gulp.watch(src.images, ['images']);
	gulp.watch(src.tpls, ['tpls']);
	gulp.watch(src.js, ['js']);
});

// tpls
gulp.task('tpls', function () {
	return gulp.src(src.tpls)
		.pipe(cache('tpl'))
		.pipe(livereload())
		//, .pipe(browserSync.stream())
		;
});

// images
gulp.task('images', function () {
	return gulp.src(src.images)
		.pipe(cache('images'))
		.pipe(livereload())
		//, .pipe(browserSync.stream())
		;
});

// js
gulp.task('js', function () {
	return gulp.src(src.js)
		.pipe(cache('js'))
		.pipe(livereload())
		//, .pipe(browserSync.stream())
		;
});

// CSS
gulp.task('css_watch', function () {

	gulp.src(src.css)
		.pipe(cache('css'))
		//.pipe(wait(150))
		.pipe(livereload())
	//, .pipe(browserSync.stream())
	;

});

// SASS
gulp.task('sass', function () {

	let plugins = [
		autoprefixer({
			"browsers": [
				"> 0.5%",
				"not dead"
			]
		}),
		cleanCSS()
	];

	let sass_dev = gulp.src(src.scss_base_file)
		.pipe(sourcemaps.init())
		.pipe(sass(
			{
				sourceComments: false,
				outputStyle: 'expanded' // nested, expanded, compact, compressed
			}).on('error', sass.logError))
		.pipe(sourcemaps.write({includeContent: false, sourceRoot: '../scss'}))
		.pipe(gulp.dest(src.scss_target))
		.pipe(livereload())
		;

	let sass_dist = gulp.src(src.scss_base_file)
		.pipe(sass(
			{
				sourceComments: false,
				outputStyle: 'compressed' // nested, expanded, compact, compressed
			}).on('error', sass.logError))
		.pipe(postcss(plugins))
		.pipe(concat('styles.min.css'))
		.pipe(gulp.dest(src.scss_target))
		.pipe(livereload()) // Sembla que si també ho poso aquí, ho fa 2 cops però recarrega bé el source i no styles.css
	;

	return sass_dev;
});


// SYNCRONITZACIO
gulp.task('sync_watch', function () {

	log_sync(src.sync_dir, 'Directori backup');

	let watcher = gulp.watch([src.sync_dir + '**/*', src.sync_dir + '!**/*_tmp*'], ['sync']);

	watcher.on('change', function (ev) {
		if (ev.type === 'deleted') {

			let deleted_file = path.relative('./', ev.path);
			deleted_file = deleted_file.replace(/\\/g, '/');
			//log('base_dir', base_dir);
			deleted_file = deleted_file.replace(base_dir_templates, src.sync_dir_dest);
			log('Arxiu a borrar', deleted_file);
			let borrats = del([deleted_file], {force: true});
			console.log(borrats);
		}
	});

});

gulp.task('sync', function () {

	let sync_dir_dest = src.sync_dir_dest + get_backup_date_dir() + '/';

	return gulp.src([src.sync_dir + '**/**/*', src.sync_dir + '!**/**/*_tmp'])
		.pipe(newer(sync_dir_dest))
		.pipe(gulp.dest(sync_dir_dest));
});

// Nomes còpia un nivell
gulp.task('backup_git', function () {

	return gulp.src([src.git_dir + '**/*', src.git_dir_dest + '!**/*_tmp'])
		.pipe(newer(src.git_dir_dest))
		.pipe(gulp.dest(src.git_dir_dest));
});


// FUNCIONS
function log(arg1, arg2) {
	console.log(
		colors.white.bgMagenta.bold(arg1),
		colors.white.bgBlue.bold(arg2)
	);
}
function log_sync(arg1, arg2) {
	console.log(
		colors.white.bgGreen.bold(arg1),
		colors.bgBlack(arg2)
	);
}
function log_init(arg1) {
	console.log(
		colors.white.bgBlue.bold(arg1)
	);
}
function log_error(arg1) {
	console.log(
		colors.white.bgRed.bold(arg1)
	);
}

function get_backup_date_dir() {

	let date = new Date();
	let day = date.getDate();
	day = day < 10 ? '0' + day : day;
	let month = date.getMonth();
	month = month < 10 ? '0' + month : month;

	log('dia', day + '.' + month + '.' + date.getFullYear());
	return day + '.' + month + '.' + date.getFullYear();
}

// INICIA
// gulp.task('default', ['watch', 'css_watch']); // -- LINUX
gulp.task('sass-sync', ['watch', 'sass', 'sync_watch']);
gulp.task('default', ['watch', 'sass']);
gulp.task('css', ['watch', 'css_watch']);