<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN">
<html>
<head>
<title><?=$page_title?></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

	<?=load_admin_css()?>

<script language="JavaScript" src="/admin/languages/<?=$language?>.js?v=<?=$version?>"></script>
</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" id="print">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td><?/*  No surt be als formularis, per tant l'he tret----- if ($title):?> <div class="titol"><?=$title?></div><?endif //title */ ?><?=$content?></td>
  </tr>
  <tr> 
    <td><img src="/admin/themes/inmotools/images/spacer.gif" width="1" height="5"></td>
  </tr>
</table>
<script language="JavaScript">
window.print();
</script>
</body>
</html>
