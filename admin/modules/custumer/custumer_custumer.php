<?php
global $gl_action;

include_once (DOCUMENT_ROOT . '/admin/modules/inmo/inmo_common.php');
include_once (DOCUMENT_ROOT . '/admin/modules/inmo/inmo_property_functions.php');
include_once( DOCUMENT_ROOT . 'admin/modules/custumer/custumer_demand_common.php' );

switch (true) {
    case strstr($gl_action, "_property"):
        include('custumer_property.php');
        break;
    case strstr($gl_action, "_demand"):
		set_language_vars('inmo', 'property', 'demand');
		change_tool('custumer', 'demand');
		include('custumer_demand.php');
        break;
    default:
        include('custumer_custumer2.php');
}