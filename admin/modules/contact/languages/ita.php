<?php
// menus eina
define('CONTACT_MENU_CONTACT','Contacto');
define('CONTACT_MENU_LIST','E-mails de contacto');

// captions seccio
$gl_caption_contact ['c_company'] = 'Azienda';
$gl_caption_contact ['c_name'] = 'Nome';
$gl_caption_contact ['c_surnames'] = 'Cognomi';
$gl_caption_contact ['c_mail'] = 'E-mail';
$gl_caption_contact ['c_to_address'] = 'Per';
$gl_caption_contact ['c_phone'] = 'Telefono';
$gl_caption_contact ['c_matter'] = 'Questione';
$gl_caption_contact ['c_comment'] = 'Osservazioni';
$gl_caption_contact ['c_accept_info'] = 'Allow send information';
$gl_caption_contact_list ['c_accept_info'] = 'Allow info';
$gl_caption ['c_sendmail'] = 'Invia Mail';
$gl_messages ['added'] = 'Forma presentato con successo';
$gl_caption ['c_sendok'] = $gl_messages ['added'];
$gl_messages ['not_saved '] = 'Impossibile inviare la posta.';

$gl_caption_contact['c_nif']='N.I.F / D.NI.';
$gl_caption_contact['c_company']='Azienda';

$gl_caption_contact['c_contact_language']='Idioma página de contacto';
// NO TRADUIT

$gl_caption_contact['c_assign_custumer']='Agregar a clientes';
$gl_caption_contact['c_assign_newsletter']='Agregar a Newsletter';
$gl_caption_contact['c_entered']= 'Enviado';
?>