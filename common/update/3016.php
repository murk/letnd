<?php
// no es poden posar ; entre mig de la consulta ja que parteix les consultes per el ; a no ser que fem servir $update_sql en array()
if (is_table('inmo__property')){
		
	// afegir watermark a config de product    
	// BR, BL, TR, TL, C, R, L, T, B, *
	if (!Db::get_first("SELECT count(*) FROM inmo__configadmin WHERE name='watermark_alignment'")){
		$update_sql .="
		INSERT INTO `inmo__configadmin` (`name`, `value`) VALUES ('watermark', '0');
		INSERT INTO `inmo__configadmin` (`name`, `value`) VALUES ('watermark_alignment', 'BR');
		INSERT INTO `inmo__configadmin` (`name`, `value`) VALUES ('watermark_opacity', '50');
		INSERT INTO `inmo__configadmin` (`name`, `value`) VALUES ('watermark_edge', '5');
		";
	}
}			
?>