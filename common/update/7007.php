<?php
/* 12-4-17
Afegir camp has_calendar a les families

*/
if ( is_table( 'product__configadmin' ) && ! Db::get_first( "SELECT * FROM product__configadmin WHERE name='has_order_date'" ) ) {
	$update_sql[] = "
		INSERT INTO product__configadmin (`name`, `value`) VALUES ('has_order_date', '0');";

} else {
	if ( is_table( 'product__configadmin' ) ) {
		trigger_error( 'Ja existeixen el camp has_order_date' );
	}
}

if ( is_table( 'product__configadmin' ) && ! Db::get_first( "SELECT * FROM product__configadmin WHERE name='has_family_calendar'" ) ) {
	$update_sql[] = "
		INSERT INTO product__configadmin (`name`, `value`) VALUES ('has_family_calendar', '0');";

} else {
	if ( is_table( 'product__configadmin' ) ) {
		trigger_error( 'Ja existeixen el camp has_family_calendar ' );
	}
}

if ( is_table( 'product__family' ) && ! is_field( 'product__family', 'has_calendar' ) ) {

	$update_sql[] = "ALTER TABLE `product__family`
	ADD COLUMN `has_calendar` TINYINT(1) NOT NULL DEFAULT '0' AFTER `destacat`;
";

} else {
	if ( is_table( 'product__family' ) ) {
		trigger_error( 'Ja existeixen el camp has_calendar' );
	}
}

?>