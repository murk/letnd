<?
/**
 * ProductBrand
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2012
 * @version $Id$
 * @access public
 */
class ProductBrand extends Module{
	var $condition, $product_filter, $get_max_discount = true;
	function __construct(){
		parent::__construct();
	}
	function on_load(){		
		$this->set_friendly_params(array('brand_id'=>false));
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function get_records()
	{
		$product_filter = $this->product_filter;	
		
		$query = "SELECT count(*) 
						FROM product__product
						WHERE product__product.brand_id = product__brand.brand_id
						AND (status = 'onsale' OR status = 'nostock' OR status = 'nocatalog') 
						AND product__product.bin = 0 ";
						
		if ($product_filter == 'discount')	{
			$wholesaler = is_product_wholesaler()?'_wholesaler':'';
			$query .= '(discount'.$wholesaler.'>0 or discount_fixed_price'.$wholesaler.'>0)';
		}
		elseif ($product_filter == 'prominent')	$query .= 'AND prominent=1';

				
		$listing = new ListRecords($this);
		
		if ($this->parent == 'Block') {
			$listing->name = 'Block';
			$listing->paginate = false;
		}
		
		if ($product_filter != 'all_brands') $listing->condition = "(" . $query . ")>0";
		$listing->call('records_walk','',true);
		$listing->order_by = 'ordre ASC,brand_name ASC';
		
		if ($this->parent == 'Block') {//blocks
			$listing->set_records();
			return $listing->loop;
		}
		else{
			return $listing->list_records();
		}
	}
	function records_walk(&$listing){
		
		
		$product_filter = $this->product_filter;
		$this->set_var('product_filter',$product_filter);
		
		$rs = &$listing->rs;		
		
		$l = array();
		
		if (USE_FRIENDLY_URL){
		
			$friendly_params['brand_id'] = $rs['brand_id'];
			
			if (isset($_GET['page_id'])) $friendly_params['page_id'] = $_GET['page_id'];
			
			if ($product_filter == 'discount')
				$friendly_params['discount'] = 1;
			elseif ($product_filter == 'prominent')
				$friendly_params['promotion'] = 1;
		
			$l['link'] = Page::get_link('product','product',false,false,$friendly_params,false,'',false,false);			
			
		}
		else{		
			$l['link'] = '/?tool=product&tool_section=product&brand_id=' . $rs['brand_id'];
			
			if ($product_filter == 'discount')
				$l['link'] .= '&discount';
			elseif ($product_filter == 'prominent')
				$l['link'] .= '&promotion';
			
			$l['link'] .= '&language=' . LANGUAGE;
			
			if (isset($_GET['page_id'])) $l['link'] .= '&page_id=' . $rs['page_id'];
		}
		
		$ret['products_link'] = $l['link'];
		
		// agafo el descompte més alt de tots els productes
		
		if ($this->get_max_discount){
			$query_prominent = ($product_filter == 'prominent')?' AND prominent=1':''; 
			
			$query = "SELECT max(discount) as max_discount
							FROM product__product 
							WHERE product__product.brand_id = ".$rs['brand_id']. $query_prominent . "  
							AND (status = 'onsale' OR status = 'nostock' OR status = 'nocatalog') 
							AND product__product.bin = 0 ";
			$max_discount = Db::get_first($query);				
			//debug::p($query);
			
			$query = "SELECT max(100-(discount_fixed_price/pvp*100)) as max_discount_fixed
							FROM product__product 
							WHERE product__product.brand_id = ".$rs['brand_id']. $query_prominent . "  
							AND (status = 'onsale' OR status = 'nostock' OR status = 'nocatalog') 
							AND product__product.bin = 0
							AND discount_fixed_price!=0";
			$max_discount_fixed = Db::get_first($query);
			//debug::p($query);
			
			$ret['max_discount'] = format_int(round(max($max_discount,$max_discount_fixed)));
		}
		
		return $ret;
	}
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{
	    $show = new ShowForm($this);
	    return $show->show_form();
	}

	function promotion()
	{	    
		$this->product_filter = 'prominent';
		$this->do_action('list_records');
	}

	function discount()
	{    
		$this->product_filter = 'discount';
		$this->do_action('list_records');
	}
}
?>