<? /* TODO-i  si tot clicable: onclick="window.location.href='<?=$show_record_link?>'" */ ?>
<article class="item <?= $hidden_class ?>">

	<? /* TODO-i  SI NO son columnes posar la a A i FIGURE a dins l'IF */ ?>
	<a href="<?= $show_record_link ?>" title="<?= htmlspecialchars( $new ) ?>">
		<figure>
			<? if ( $images ): ?>
				<img <?= get_image_attrs( $image_src_thumb, $image_alt ? $image_alt : $new, 300, 200, false ) ?> />
			<? endif //$images?>
		</figure>
	</a>
	<a href="<?= $show_record_link ?>" title="<?= htmlspecialchars( $new ) ?>"><time datetime="<?= unformat_date_attr( $entered ) ?>"><?= $entered ?></time></a>

	<a href="<?= $show_record_link ?>" title="<?= htmlspecialchars( $new ) ?>"><h3><?= $new ?></h3></a>

	<div class="description" onclick="window.location.href='<?= $show_record_link ?>'">
		<?= $content ?>
	</div>

</article>