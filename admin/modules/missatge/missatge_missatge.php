<?
/**
 * MissatgeMissatge
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2011
 * @version $Id$
 * @access public
 *
 *
 * Els administradors poden veure i editar tots els missatges dels grups inferiors, però no dels usuaris administradors
 * Els usuaris només poden editar els missatges propis
 *
 * Veure missatge ( llistat )
 *
 *        1 -
 *          a -Es admin: Si l'ha escrit i si es usuari inferior pot veure'l
 *          b -Es usuari: Si l'ha escrit pot veure'l
 *        2 - Si no te cap grup relacionat i cap usuari relacionat es per tothom
 *        3 - Si el missatge te grups relacionats ha de pertanyer al grup
 *        4 - Si el missatge te usuaris relacionats ha d'estar en la llista
 *
 * Editar els missatges ( fitxa )
 *
 *        1- No es poden editar els de Letnd
 *        2- Admin edita tots els missatges dels usuaris d'un grup per sota
 *        3- L'usuari que l'ha creat
 *
 *  Borrar
 *         Només pot borrar missatges admin, però els pot borrar tots
 *
 */
class MissatgeMissatge extends Module{

	/**
	 * @return bool
	 */
	public function is_admin_total()
	{
		return $_SESSION['group_id'] == 1;
	}

	/**
	 * @return bool
	 */
	public function is_admin()
	{
		return $_SESSION['group_id'] == 2;
	}

	/**
	 * @return bool
	 */
	public function is_user()
	{
		return $_SESSION['group_id'] > 2;
	}

	/**
	 * @return string
	 */
	public function get_user_id()
	{
		return $_SESSION['user_id'];
	}

	/**
	 * @return string
	 */
	public function get_group_id()
	{
		return $_SESSION['group_id'];
	}

	function __construct(){

		parent::__construct();

	}
	function on_load(){

		// Només vull veure quin tipus es al llistat, quan el crea un usuari és sempre un missatge general
		if ( is_table( 'inmo__property' ) && $this->action == 'list_records' ) {
			$this->set_field( 'kind', 'select_fields', 'general,mls' );
		}
		else {
			// $this->set_field( 'kind', 'select_fields', 'general,mls' );
			// De moment només hi ha un tipus, per tant ho desconecto
			$this->unset_field( 'kind' );
		}


		if ($this->is_admin_total())
			$this->set_field( 'user_id', 'select_condition', '' );
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = '<script language="JavaScript" src="/admin/modules/missatge/jscripts/functions.js?v='.$GLOBALS['gl_version'].'"></script>' .$this->get_records();
	}
	function get_records()
	{
	    $listing = new ListRecords($this);

		$listing->set_field_position( 'status', 13 );

		if ( $this->is_user() ) {
			$listing->set_options( 1, 0, 0, 0, 0, 0, 1, 0, 1, 1 );
		}
		
		$listing->add_button ('reply_button', '', 'boto1', 'before','missatge_reply');

		$listing->call( 'records_walk', '', true );

		/*
		 *  1 -
		 *    a -Es admin: Si l'ha escrit i si es usuari inferior pot veure'l
		 *    b -Es usuari: Si l'ha escrit pot veure'l
		 *  2 - Si no te cap grup relacionat i cap usuari relacionat es per tothom
		 *  3 - Si el missatge te grups relacionats ha de pertànyer al grup
		 *  4 - Si el missatge te usuaris relacionats ha d'estar en la llista
		 */
		if ($this->is_admin()){

			$creator_condition = "
					creator_user_id = '" . $this->get_user_id() . "'
					||
					( SELECT group_id FROM user__user WHERE user__user.user_id = creator_user_id ) > 2
				";
		}
		else {

			$creator_condition = "
					creator_user_id = '" . $this->get_user_id() . "'
				";

		}

		if (!$this->is_admin_total()) {
			$listing->condition = "
				(
					(
						" . $creator_condition  . "
					)
					OR
					(
							(SELECT count(*) 
								FROM missatge__missatge_to_group 
								WHERE missatge__missatge_to_group.missatge_id = missatge__missatge.missatge_id) = 0
						AND
						
							(SELECT count(*) 
								FROM missatge__missatge_to_user 
								WHERE missatge__missatge_to_user.missatge_id = missatge__missatge.missatge_id) = 0
						AND 
							status = 'send'
					)
					OR
					(   
						(SELECT count(*) 
							FROM missatge__missatge_to_group
							WHERE missatge__missatge_to_group.missatge_id = missatge__missatge.missatge_id
							AND group_id = '" . $this->get_group_id() . "') > 0
						AND 
							status = 'send'
					)
					OR
					(   
						(SELECT count(*) 
							FROM missatge__missatge_to_user
							WHERE missatge__missatge_to_user.missatge_id = missatge__missatge.missatge_id
							AND user_id = '" . $this->get_user_id() . "') > 0
						AND 
							status = 'send'
					)
				)
								";
		}

		$listing->order_by = 'sent DESC';

	    return $listing->list_records();
	}

	function records_walk( &$listing ) {

		$creator_group_id = Db::get_first( "SELECT group_id FROM user__user WHERE user_id = " . $listing->rs['creator_user_id'] );

		// 0- No es poden editar els de Letnd ( per omissió queda descartat )
		// 1- Admintotal edita tot
		// 2- Admin edita tots els missatges dels usuaris d'un grup per sota
		// 3- L'usuari que l'ha creat
		// només pot borrar missatges admin

		$listing->buttons['edit_button']['show'] =
			$this->is_admin_total()
			||
			$this->is_admin() && $creator_group_id > 2
			||
			$this->get_user_id() == $listing->rs['creator_user_id'];

		$listing->buttons['reply_button']['show'] =  $listing->rs['creator_user_id'] != 0 && $this->get_user_id() != $listing->rs['creator_user_id'];
	}
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{
		// Formulari nou sempre es pot, miro condicions només per editar registre existent
		$show = new ShowForm($this);

		$show->get_values();

		if ($this->action != 'show_form_new' )
			$creator_group_id = Db::get_first( "SELECT group_id FROM user__user WHERE user_id = " . $show->rs['creator_user_id'] );

		if (
			$this->action == 'show_form_new' ||
			(
				// Aquestes les matinc igual que al records_walk
				$this->is_admin_total()
				||
				$this->is_admin() && $creator_group_id > 2
				||
				$this->get_user_id() == $show->rs['creator_user_id']
			)
		) {

		}
		else {
			Main::redirect( '/admin/?menu_id=80003' );
			die();
		}


		$reply_id = R::id( 'reply_id' );
		if ( $reply_id ) {
			$this->set_field( 'parent_reply_id', 'type', 'hidden' );
			$this->set_field( 'parent_reply_id', 'form_admin', 'input' );
			$this->set_field( 'parent_reply_id', 'default_value', $reply_id );
			$this->unset_field( 'group_id' );
			$this->unset_field( 'user_id' );
			$this->unset_field( 'status' );
			$this->unset_field( 'subject' );
			$this->unset_field( 'entered' );
		}
		else{
			$show->form_level1_titles = array( 'group_id','subject' );
			$show->set_field('entered','default_value',now(true));
		}

		$show->unset_field( 'creator_user_id' );
		$show->unset_field( 'modified' );

	    return $show->show_form();
	}

	function save_rows()
	{
		// L'usuari no pot guardar desde el llistat
		if (
		$this->is_user()) {
			die();
		}

		// TODO S'hauria de comprovar registre a registre si admin te permis per aquell id, però hauria de saber enviar un post amb les variables canviades, molt improbable sino sab programar


	    $save_rows = new SaveRows($this);
	    $save_rows->save();
	}

	function write_record()
	{
	    $writerec = new SaveRows($this);

		// Guardo la data d'enviament al canviar status a send
		$status = $writerec->get_value( 'status' );
		if ( $this->action == 'add_record' && $status == 'send' ) {
			$this->set_field( 'sent', 'override_save_value', 'now()');
		}
		if ( $this->action == 'save_record' && $status == 'send' ) {
			$query = "SELECT status FROM missatge__missatge WHERE missatge_id = " . $writerec->id;
			$old_status = Db::get_first( $query );
			if ($status != $old_status)
				$this->set_field( 'sent', 'override_save_value', 'now()');
		}

		// PERMISOS
		// Admin no pot guardar els creats per letnd - TODO - Nomes guardar els usuaris per sota ( no els altres admin )
		if ($this->is_admin() && $this->action != 'add_record') {
			$creator_user_id = Db::get_first( "SELECT creator_user_id FROM missatge__missatge WHERE missatge_id = " . $writerec->id );

			if ( $creator_user_id ) {
				$creator_group_id = Db::get_first( "SELECT group_id FROM user__user WHERE user_id = " . $creator_user_id );
			}
			else {
				$creator_group_id = 0;
			}

			if ( $creator_group_id < 2 ) {
				die('No es poden modificar els missatges de Letnd');
			}
		}
		// L'usuari nomes pot guardar el seu propi o quan crea un de nou
		if ($this->is_user() && $this->action != 'add_record') {
			$creator_user_id = Db::get_first( "SELECT creator_user_id FROM missatge__missatge WHERE missatge_id = " . $writerec->id );

			if ( $this->get_user_id() != $creator_user_id ) {
				die('Nomes es poden modificar els missatges propis');
			}
		}
		// FI PERMISOS

		// REPLY
		$reply_id = $writerec->get_value( 'parent_reply_id' );
		if ( $reply_id ) {
			// Busco el top, si el missatge parent no te especificat top, es que no es resposta a cap
			$query = "SELECT top_reply_id, subject FROM missatge__missatge WHERE missatge_id =  " . $reply_id;
			$rs = Db::get_row( $query );

			$top_reply_id = $rs['top_reply_id'] ? $rs['top_reply_id'] : $reply_id;

			$subject = $rs['subject'];
			if ( substr( $subject, 0, 4 ) !==  'Re: ') {
				$subject = 'Re: ' . $subject;
			}

			$this->set_field( 'top_reply_id', 'override_save_value', $top_reply_id );
			$this->set_field( 'status', 'override_save_value', "'send'");
			$this->set_field( 'sent', 'override_save_value', 'now()');
			$this->set_field( 'subject', 'override_save_value', Db::qstr($subject));
			$this->set_field( 'entered', 'override_save_value', 'now()');
		}
		// FI  REPLY


		// Obligo a guardar l'user_id del loguejat quan es crea
		$writerec->set_field( 'creator_user_id', 'override_save_value', $this->get_user_id() );
		$writerec->set_field('modified','override_save_value','now()');

	    $writerec->save();

		// REPLY
		// Marco que la resposta sigui només per l'usuari autor del missatge
		if ( $reply_id ) {

			$query = "SELECT creator_user_id FROM missatge__missatge WHERE missatge_id =  " . $reply_id;
			$reply_user_id = Db::get_first( $query );

			$query = "
					INSERT INTO missatge__missatge_to_user 
						(missatge_id, user_id)
						VALUES 
						(" . $writerec->id . ", " . $reply_user_id . ")";
			Db::execute( $query );

			if ( $this->process == 'save_from_missatge_list' ) {
				$_SESSION['message'] = $GLOBALS['gl_message'];
				print_javascript( 'top.window.location.reload();' );
				die();
			}
		}
		// FI  REPLY
	}

	function manage_images()
	{
	    $image_manager = new ImageManager($this);
	    $image_manager->execute();
	}
}
?>