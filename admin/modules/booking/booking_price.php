<?
/**
 * BookingPrice
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) Juliol 2014
 * @version $Id$
 * @access public
 * 
 * 
 * Funcions per obtindre preu per reutilitzar també a public
 * 
 */
class BookingPrice extends Module {
	
	var $is_ajax = false, $details, $action = false, $get_book_new_price = true;
	
	public function __construct(&$module = false){
		// No se si ha de cridar el parent construct, poder me'l vaig oblidar
		// parent::Module();

		if ($module && $module->tool=='booking'){
			$this->caption = $module->caption;
		}
		else{
			$this->tool  = 'booking';
			$this->tool_section  = 'price';
			$this->set_language();
		}

	}
	
	function get_price_ajax(&$module) {
		
		$this->is_ajax = true;
		
		$date_in2 = R::escape('date_in');
		$date_out2 = R::escape('date_out');
		$adult = R::escape('adult');
		$child = R::escape('child');
		$baby = R::escape('baby');
		
		$date_in = unformat_date($date_in2);
		$date_out = unformat_date($date_out2);

		// quan vreasy es owner i no es editable
		$dates_not_editable = !$date_out && !$date_in;
		
		$ret['invert_dates'] = false;
		
		// giro les dates si la de començament es major que la final
		if (is_date_bigger($date_in, $date_out)){
			
			$t = $date_in2;
			$date_in2 = $date_out2;
			$date_out2 = $t;
			
			$t = $date_in;
			$date_in = $date_out;
			$date_out = $t;
			$ret['invert_dates'] = true;
		}
		
		$book_id = R::id('book_id');
		$property_id = R::id('property_id');


		if ($dates_not_editable) {
			$query = "SELECT date_in, date_out FROM booking__book WHERE book_id = $book_id";
			$rs    = Db::get_row( $query );
			$date_in = $rs['date_in'];
			$date_out = $rs['date_out'];
		}

		
		$is_reserved = $module->check_is_reserved($date_in, $date_out, $date_in2, $date_out2, $book_id, $property_id, $adult, $child, $baby, false);
		
		if ($is_reserved){
			
			$ret['price_book']= false;
			$ret['error']= $module->reserved_message;
			
		}
		else{
			$prices = $this->get_price(
						$date_in,
						$date_out, 
						$book_id, 
						$property_id,
						false,
						true,
						$adult,
						$child
					);
			
			$ret['price_book']= $prices['price_book_value'];			
			$ret['price_extras']= $prices['price_extras_value'];	
			$ret['price_tt']= $prices['price_tt_value'];	
			$ret['error']= $prices['error'];
			$ret['detail']= $prices['detail'];
				
			if ($prices['new_date_in']){
				$ret['new_date_in']= $prices['new_date_in'];			
				$ret['new_date_out']= $prices['new_date_out'];
			}
			else{
				$ret['new_date_in']= false;			
				$ret['new_date_out']= false;
			}
			$ret['promcode_error'] = $prices['promcode_error'];
			if($prices['show_promcode']){
				$ret['promcode_discount'] = $prices['promcode_discount_value'];
			}
		}
		//Debug::p_all();
		die (json_encode($ret));
		
	}
	function is_owner($property_id, $book_rs) {


		// a admin el custumer_id em ve per get
		// te prioritat admin, perque pot ser que el que gestiona estigui a public i en sessio

		if ($GLOBALS['gl_is_admin']) {

			// Si guardo, ve per post
			$custumer_id = R::post( 'selected_custumer_id' );

			if ($custumer_id){
				$custumer_id = implode(',', $custumer_id);
			}

			// si ve per ajax
			if (!$custumer_id){
				$custumer_id = R::ids('selected_custumer_ids');
			}

			// si estem editant una reserva ja està grabat que és propietari
			if (!$custumer_id && isset($book_rs['is_owner']) && $book_rs['is_owner'] == 1){
				return true;
			}

			if (!$custumer_id)
				return false;
		}
		else {

			$custumer_id = isset($_SESSION['custumer']['custumer_id'])?$_SESSION['custumer']['custumer_id']:false;

			if (!$custumer_id)
				return false;
		}

		$owner_id = Db::get_first("SELECT custumer__custumer.custumer_id as custumer_id 
			FROM custumer__custumer_to_property, custumer__custumer
			WHERE custumer__custumer_to_property.custumer_id = custumer__custumer.custumer_id
			AND bin = 0
			AND property_id = '" . $property_id . "'
				AND custumer__custumer.custumer_id IN (" . $custumer_id . ")");

		if ($owner_id) return true; else return false;
	}
	function get_owner_details($property_id) {
		
		$ret['custumer_id'] = false;
		$ret['is_company'] = false;
		$ret['is_ue'] = true;
		$ret['country'] = false;

		$ignore_outside_ue = Db::get_first( "SELECT value FROM booking__configadmin WHERE name='ignore_outside_ue'" );
		
		// suposarem que si hi ha més d'un propietari tots son d'allà mateix
		// i si es empresa nomes hi ha un propietari
		$owner = Db::get_row("SELECT custumer__custumer.custumer_id as custumer_id, country, billing 
			FROM custumer__custumer_to_property, custumer__custumer
			WHERE custumer__custumer_to_property.custumer_id = custumer__custumer.custumer_id
			AND bin = 0
			AND property_id = '" . $property_id . "' LIMIT 1");
		
		if ($owner){
			$ret['custumer_id'] = $owner['custumer_id'];
			$ret['is_company'] = $owner['billing']=='company';
			$ret['country'] = $owner['country'];

			if ( !$ignore_outside_ue ) {

				$ret['is_ue'] = Db::get_first("
					SELECT count(*) 
					FROM all__country 
					WHERE country_id = '" . $owner['country'] . "'
						AND european_comunity = 1")==1;

			}

		}
			
		return $ret;
	}
	function get_price_owner($date_in,$date_out,$book_rs) {

		if (!($date_in && $date_out && $book_rs)){
			$date_in = $book_rs['date_in'];
			$date_out = $book_rs['date_out'];
		}

		$total_nights = ($date_in && $date_out)?$this->get_number_nights($date_in,$date_out):1;

		return array(
			'price_book_promcode_discounted_value' =>0, // igual que price_book_contract, però no te en compte el is_ue
			'price_book_promcode_discounted' =>format_currency(0),
			'price_book_contract_value' =>0,
			'price_book_contract' =>format_currency(0),
			'price_book_contract_agent_value' =>0,
			'price_book_contract_agent' =>format_currency(0),
			'price_book' =>format_currency(0),
			'price_extras' =>format_currency(0),
			'price_total' =>format_currency(0),
			'price_book_value' =>0,
			'price_extras_value' =>0,
			'price_total_value' =>0,
			'new_date_in' =>'',
			'new_date_out' =>'',
			'detail' =>'',
			'error' =>'',
			'show_promcode' =>false,
			'show_promcode_form' =>false,
			'promcode_error' =>'',
			'promcode_input' =>'',
			'promcode_button' =>'',
			'promcode_id' =>'',
			'promcode_discount' =>format_currency(0),
			'promcode_total' =>format_currency(0),
			'promcode_discount_value' =>0,
			'promcode_total_value' =>0,
			'price_tt' =>format_currency(0),
			'price_tt_value' =>0,
			'is_owner' =>true,
			'total_nights' =>$total_nights,

		);
	}
	/*
	 * 
	 * Funcions per obtenir preu
	 * 
	 * 
	 */
	function get_number_nights($date_in, $date_out) {
		
		$begin = new DateTime($date_in);
		$end = new DateTime($date_out);

		$interval = DateInterval::createFromDateString('1 day');

		$period = new DatePeriod($begin, $interval, $end);
		$total_nights = iterator_count($period);
		return $total_nights;
	}
	// si em canviat les dates, tornem a generar el preu de la reserva
	// o si no hi ha el post de dates, vol dir que no es editable per tant agafem valor de la bbdd
	function get_price_book_edit($date_in, $date_out, $property_id, &$price_total,&$errors, &$total_nights, &$book_rs) {
		
		//Debug::p($date_in, '$date_in');
		//Debug::p($date_out, '$date_out');
		//Debug::p($book_rs['date_in'], 'book_rs_in');
		//Debug::p($book_rs['date_out'], 'book_rs_out');

		$recalculate_rate = R::text_id( 'recalculate_rate' ) || R::text_id( 'recalculate_rate', false, '_POST' );
		
		$book_rs['date_in'] = str_replace('-', '/', $book_rs['date_in']);
		$book_rs['date_out'] = str_replace('-', '/', $book_rs['date_out']);
		$date_in = str_replace('-', '/', $date_in);
		$date_out = str_replace('-', '/', $date_out);
		$old_date_in = R::post('old_date_in');
		
		// al guardar formulari, ja em canviat a la bbdd, per tant comparo amb old_
		if ($old_date_in){		
			$old_date_out = R::post('old_date_out');
			$old_date_out = unformat_date(current($old_date_out));
			$old_date_in = unformat_date(current($old_date_in));
			$is_equal = $old_date_in==$date_in && $old_date_out==$date_out;
		}
		else{	
			$is_equal = $book_rs['date_in']==$date_in && $book_rs['date_out']==$date_out;
		}
		//Debug::p($is_equal, 'equal');
				
		if ( ($date_in=='' || $is_equal) && !$recalculate_rate ){
			$price_total = $book_rs['price_book'];
			$total_nights = $this->get_number_nights($book_rs['date_in'], $book_rs['date_out']);
			$this->get_book_new_price = false;
		}
		elseif ($date_in==''){
			$this->get_price_book_new($book_rs['date_in'], $book_rs['date_out'], $property_id, $price_total,$errors, $total_nights);
			$this->get_book_new_price = true;
		}
		else{
			$this->get_price_book_new($date_in, $date_out, $property_id, $price_total,$errors, $total_nights);
			$this->get_book_new_price = true;
		}
			
	}
	function get_price_book_new($date_in, $date_out, $property_id, &$price_total,&$errors, &$total_nights) {
		
		if ($date_in && $date_out){
		
			$total_nights = $this->get_number_nights($date_in, $date_out);
			
			// iterator no fa el reset al servidor, per tant haig de tornar a crear l'objecte
			// reset no funciona
			$begin = new DateTime($date_in);
			$end = new DateTime($date_out);

			$interval = DateInterval::createFromDateString('1 day');
			$period = new DatePeriod($begin, $interval, $end);
			//Debug::p($total_nights, 'text');			
			foreach ( $period as $dt ){

				$week_day = $dt->format( "N" ); // 1 per dilluns
				$date = $dt->format( "Y-m-d" );
				$rs = Db::get_row("
					SELECT propertyrange_id, price_night, price_weekend, price_week, price_week_type, minimum_nights, date_start, date_end, whole_week, start_day, season_id
						FROM booking__propertyrange
						WHERE '$date' >= date_start 
						AND '$date' < date_end
						AND property_id = " . $property_id);
								
				if ($rs) {
					extract($rs);


					$is_whole_week = ($minimum_nights == '7' || $minimum_nights == '14') && ($whole_week == '1');


					if (!isset($errors[$propertyrange_id])) {
						$errors[$propertyrange_id]['is_whole_week'] = $is_whole_week;
						$errors[$propertyrange_id]['nights'] = 0;
						$errors[$propertyrange_id]['price_week'] = $price_week;
						$errors[$propertyrange_id]['price_week_type'] = $price_week_type;
						$errors[$propertyrange_id]['start_day'] = $start_day;
						$errors[$propertyrange_id]['book_start_day'] = $week_day;
						$errors[$propertyrange_id]['date_start'] = $date_start;
						$errors[$propertyrange_id]['date_end'] = $date_end;
						$errors[$propertyrange_id]['date'] = $date;
						$errors[$propertyrange_id]['season_id'] = $season_id;
					}


					/*
					  if ($is_whole_week) {
					  }
					  else{
					  if (!isset($errors[$propertyrange_id])){
					  $errors[$propertyrange_id]['is_whole_week'] = false;
					  }					
					  }
					 * 
					 */
					$errors[$propertyrange_id]['nights'] ++;
					$errors[$propertyrange_id]['last_date'] = $date;
					// comprovo minim nits
					if ($minimum_nights > $total_nights) {
						$errors[$propertyrange_id]['error'] = sprintf($this->caption['c_minimum_nights_error'], $minimum_nights, format_date_form($date_start), format_date_form($date_end));
					}




					// Mes de 7 dies i està estipulat preu per setmana
					if ($price_week != '0.00' && $total_nights >= 7) {


						// si el preu està posat per setmana, es calcula despres
						if ($price_week_type == 'night') {
							$price = $price_week;
							$this->add_detail($price, $date, 'price_week',false,$rs);
						} 					
						else {
							$price = 0;
							$this->add_detail($price, $date, 'whole_week',false,$rs);
						}
					
					}
					// Estipulat preu cap de setmana i es dia 5 0 6 -> nit de divendres o nit de dissabte
					elseif ($price_weekend != '0.00' && ($week_day == 5 || $week_day == 6)) {


						$price = $price_weekend;
						$this->add_detail($price, $date, 'price_weekend',false,$rs);
					
					} 
					else {
						$price = $price_night;
						$this->add_detail($price, $date, 'price_night',false,$rs);
					}


					$price = (float) $price;
					//Debug::p($price, 'Preu ' . $date);
					//Debug::p($price, 'preu/nit');
					$price_total += $price;
				}

			}			
		
		}
		
	}
	
	function get_price_whole_week(&$error_text, $errors, $total_nights, &$price_total) {
		
		$join = $this->is_ajax?"\n":'<br>';
		foreach ($errors as $err) {

			extract($err);
			if($is_whole_week){
				// comprovo setmana sencera sense start day
				if ( $is_whole_week && $nights%7!=0) {
					$err['error'] = sprintf($this->caption['c_whole_week_error'],format_date_form($date_start),format_date_form($date_end));
				}

				// comprovo nomes start day -- Anula l'error anterior, 
				// pot ser que estigui be setmana sencera pero si falla el dia el missatge d'error ja si val
				if  ( $is_whole_week && $start_day && $start_day!=$book_start_day) {

					// el caption comença en diumenge
					if ($start_day==7) $start_day = 0;
					$week_day_text = $GLOBALS['week_names'][$start_day];

					$err['error'] = sprintf($this->caption['c_start_day_error'],$week_day_text,$week_day_text,format_date_form($date_start),format_date_form($date_end));
				}
			}
			
			if (isset($err['error'])){
				$error_text .= $err['error'] . $join;
			}
			else{
			
			}
				
			// si no hi ha error sumo preu si es per setmana
			// Aplica preu semanal mestres total de dies de la reserva passi aquest preu
			if ($price_week_type=='week' && $price_week!='0.00' && $total_nights>=7){

				$price_week = (float)$price_week;
				$price_temp = $price_week/7*$nights;
				$price_temp = number_format ( $price_temp , 2 , ".","" );
				$price_temp = (float)$price_temp;
				
				$price_total += $price_temp;
				$this->add_detail ($price_temp, $last_date, 'whole_week',$nights);
				
			}
		}
		
		if($error_text){
			$error_text = substr($error_text, 0, -strlen($join));
		}	
	}
	
	function get_price_extras(&$price_extra_total, $book_id, $property_id) {

		// extres guardades
		$extras = Db::get_rows("
		SELECT extra_id , extra_price, quantity
			FROM booking__extra_to_book
			WHERE book_id = $book_id");


		$saved_ids = '';


		foreach ($extras as $extra) {
			$saved_ids .= $extra['extra_id'] . ',';
			$price_extra = $extra['extra_price'];
			$quantity = $extra['quantity'];
			$price_extra = (float) $price_extra;


			if ($quantity == 0)
				$quantity = 1;


			$price_extra_total +=$price_extra * $quantity;
		}


		// extres no guardades ( per formulari nou, o alguna que s'hagi afegit despres, 
		//						nomes les obligatories, les altres no s'han triat )
		$saved_ids = substr($saved_ids, 0, -1);
		$query = "
		SELECT extra_price
			FROM booking__extra_to_property
			INNER JOIN (booking__extra)
			USING (extra_id)
			WHERE property_id = $property_id
			AND booking__extra.bin = 0
			AND extra_type = 'required'";
		if ($saved_ids)
			$query .= " AND extra_id NOT IN (" . $saved_ids . ")";


		$extras = Db::get_rows($query);

		foreach ($extras as $extra) {
			$price_extra = $extra['extra_price'];
			$price_extra = (float) $price_extra;


			$price_extra_total +=$price_extra;
		}
	}
	/*
	 * Funcio principal per obtenir preus
	 * 
	 * 
	 * 
	 */
	// si no hi ha adult o child no conto tasa ( per preus d'inmobles per ejemple )
	function get_price($date_in,$date_out, $book_id, $property_id, $set_javascript = false, $get_extras = true, $adult = false, $child = false) {


		$book_rs = false;		
		$price_total = 0;
		$price_extra_total = 0;
		$price_required_total = 0;
		$errors = array();
		$minimum_price = 0;
		$total_nights = 0;
		$error_text = '';
		
		if ($book_id)
			$book_rs = Db::get_row("
				SELECT adult, date_in, date_out, price_total, promcode_discount, payment_done_1, payment_done_2, payment_done_3, payment_done_4, price_book, price_extras, price_tt, price_total, is_owner
				FROM booking__book
				WHERE book_id = " . $book_id);

		if ($this->is_owner($property_id, $book_rs)) return $this->get_price_owner($date_in,$date_out,$book_rs);
		
		// preus book, quan book_id = 0 o action = add_record agafo els preus de book_new
		if ($book_id && $this->action!='add_record')
			$this->get_price_book_edit($date_in, $date_out, $property_id, $price_total, $errors, $total_nights, $book_rs);
		else
			$this->get_price_book_new($date_in, $date_out, $property_id, $price_total, $errors, $total_nights);
		
		// comprovo whole_week i poso ve errors
		if ($this->get_book_new_price)
			$this->get_price_whole_week($error_text,$errors, $total_nights, $price_total);	
		
		// extres
		if ($get_extras) {
			$this->get_price_extras($price_extra_total, $book_id, $property_id);
		}	
		
		// descompte promocional
		$promcode = $this->get_promcode($book_id, $price_total + $price_extra_total);
		
		// taxa turistica		
		$owner = $this->get_owner_details($property_id);
		$tt = $this->get_tt($book_rs, $book_id, $total_nights, $adult, $child, $property_id, $owner, $date_in);
		$promcode['price_total'] += $tt['price_tt'];
		
		// contractes
		$price_total_contract = $price_book_promcode_discounted = $price_total - $promcode['promcode_discount'];
		$price_total_contract_agent = 0;
		if (!$owner['is_ue']) {
			$price_total_contract_agent =  $price_total*0.2*1.21; // TODO-i S'ha de posar el 20% de l'agencia en algún lloc
			$price_total_contract = $price_total-$price_total_contract_agent;
		}
		$owner_is_ue = $owner['is_ue'];

		//Debug::p($price_extra_total, '$price_extra_total');
		if ($set_javascript)
			$this->set_page_javascript($book_id,$price_total,$price_extra_total,$promcode['price_total'], $property_id,$promcode['promcode_discount'],$tt['price_tt'],$book_rs);
		
		
		$ret = array(
			'owner_is_ue' =>$owner_is_ue,
			'price_book_promcode_discounted_value' =>$price_book_promcode_discounted, // igual que price_book_contract, però no te en compte el is_ue
			'price_book_promcode_discounted' =>format_currency($price_book_promcode_discounted),
			'price_book_contract_value' =>$price_total_contract,
			'price_book_contract' =>format_currency($price_total_contract),
			'price_book_contract_agent_value' =>$price_total_contract_agent,
			'price_book_contract_agent' =>format_currency($price_total_contract_agent),
			'price_book' =>format_currency($price_total), // aquest no te el descompte del promcode fet
			'price_extras' =>format_currency($price_extra_total),
			'price_total' =>format_currency($promcode['price_total']),
			'price_book_value' =>$price_total,
			'price_extras_value' =>$price_extra_total,
			'price_total_value' =>$promcode['price_total'],
			'new_date_in' =>'',
			'new_date_out' =>'',
			'detail' =>$get_extras?$this->get_detail():false,
			'error' =>$error_text,
			'show_promcode' =>$promcode['show_promcode'],
			'show_promcode_form' =>$promcode['show_promcode_form'],
			'promcode_error' =>$promcode['promcode_error'],
			'promcode_input' =>$promcode['promcode_input'],
			'promcode_button' =>$promcode['promcode_button'],
			'promcode_id' =>$promcode['promcode_id'],
			'promcode_discount' =>format_currency($promcode['promcode_discount']),
			'promcode_total' =>format_currency($promcode['promcode_total']),
			'promcode_discount_value' =>$promcode['promcode_discount'],
			'promcode_total_value' =>$promcode['promcode_total'],
			'price_tt' =>format_currency($tt['price_tt']),
			'price_tt_value' =>$tt['price_tt'],
			'is_owner' => false,
			'total_nights' =>$total_nights,
			
		);
		return $ret;
	}
	
	
	// TODO-i Calcular tasa turística en funció dels dies que han fet lar reserva https://www.easybrava.com/admin/?action=search&menu_id=152&q=978&Submit=ok
	// retorna variables de tt ...
	// actualitzo sempre
	function get_tt($book_rs, $book_id, $total_nigths, $adult, $child, $property_id, $owner, $date_in){

		Debug::p( [
					[ 'book_rs', $book_rs ],
					[ 'date_in', $date_in ],
				], 'bookPrice');

		// Es passa date_in quan es modifica una reserva, en canvi des de un contracte, no es passa date_in i agafo la que hi ha guardada
		if (!$date_in)
			$date_in = $book_rs['date_in'];

		// si adult no es editable al formulari ho agafo de la bbdd
		if ($adult == '' && $book_rs) $adult = $book_rs['adult'];
		// si no hi ha adult agafo 1 que es el defecte del formulari
		if ($adult == '' && !$book_rs) $adult = 1;
		
		$ret = array();
		$ret['custumers_tt']= array();
		$ret['price_tt'] = $price_tt = 0;
		$custumers = array();
		
		// si vinc de admin per ajax, agafo els custumers que s'han seleccionat 
		// en aquell moment i no els guardats
		$selected_custumer_ids = R::ids('selected_custumer_ids');
		if ($selected_custumer_ids){
			$custumers = explode(',',$selected_custumer_ids);
		}
		elseif ($book_id){
			$custumers_rows = Db::get_rows("
				SELECT custumer_id 
				FROM booking__book_to_custumer 
				WHERE book_id = " . $book_id);
			$custumers = array();
			foreach ($custumers_rows as $rs){
				$custumers []= $rs['custumer_id'];
			}
		}
		// si no hi ha adult o no hi ha nits retorno sense valor
			elseif (!$adult || !$total_nigths){
			return $ret;
		}
		
		// maxim 7 nits
		if ($total_nigths>7) $total_nigths = 7;
		
		// si no hi ha definit cap pais
		if (is_date_bigger($date_in, '2017/03/31')){
			$rate = 0.90;
		}
		else {
			$rate = 0.45;
		}
		if ($owner['country'] == 'ES' && $owner['is_company']) $rate = $rate+$rate*0.1;

		Debug::p( [
					[ 'rate', $rate ],
				], 'bookPrice');

		
		$ret['custumers_tt']['rate'] = $rate;
		$ret['custumers_tt']['total_nigths'] = $total_nigths;
		$ret['custumers_tt']['total_acustumers'] = $adult;
		
		foreach ($custumers as $id){
			$custumer = Db::get_row("
				SELECT birthdate, country
				FROM custumer__custumer
				WHERE custumer_id = " . $id);
			
			
			$years = $this->getAge($custumer['birthdate'], $date_in);
			
			if ($years>=16){
				$price_tt += $rate*$total_nigths;
				$adult--;
			}
			
		}
		
		// calculo per els adults que encara no tenim dades
		for ($i=1; $i<=$adult; $i++){
			$price_tt += $rate*$total_nigths;			
		}
		
		$ret['price_tt'] = $price_tt;
		Debug::add('TT',$ret);
		Debug::add('Property_id',$property_id);
		Debug::add('Owner',$owner);
		//Debug::p($ret,'TT');
		//Debug::p($property_id,'Property_id');
		//Debug::p($owner,'Owner');
		return $ret;
	}
	// TODO-i Posar aquí la date_in que es amb el que s'ha de basar el càlcul, i també hauràn de passar tots els no adults per aquí perque pot ser que en el moment de la reserva ja siguin adults
	function getAge($birth_date, $future = false){

		if ( $future ) {
			$compare_date = date("md", strtotime($future));
		}
		else {
			$compare_date = date("md");
		}

		if ($birth_date == '0000-00-00') return false;
		$adjust = ($compare_date >= date("md", strtotime($birth_date))) ? 0 : -1; // Si no hem arribat al día y mes en aquest any restem 1
		
		$years = date("Y") - date("Y", strtotime($birth_date)); // Calculem el número d'anys
		return $years + $adjust;
	}
	
	
	// retorna variables de promcode, el descompte i preu total menys descompte, error ...
	function get_promcode($book_id, $price_total){
		
		$is_admin = $GLOBALS['gl_is_admin'];
		
		$promcode = R::escape('promcode');
		// sino ve per get miro si per post, que es fa servir quan es guarda la reserva
		if (!$promcode) $promcode = R::escape('promcode',false,'_POST');
		
		$promcode_input = '';
		
		if (!$this->is_ajax && $book_id !== false){
			if ($is_admin){			
				$promcode_input = '';
				
				$custumers = 0;				
				if ($book_id !=0){
					// busco clients
					$custumers = Db::get_rows_array("
						SELECT custumer__custumer.custumer_id as custumer_id
							FROM booking__book_to_custumer, custumer__custumer
							WHERE booking__book_to_custumer.custumer_id = custumer__custumer.custumer_id
							AND bin = 0
							AND book_id = '" . $book_id . "'");

					$custumers = implode_field($custumers);
					if (!$custumers) $custumers = 0; // a admin pot ser que encara no hi hagi client a la reserva
				}
				// busco els promcode per tots o per els clients concrets
				$query_promcode = "
					SELECT promcode, discount_pvp, discount_type, name 
					FROM booking__promcode_language, booking__promcode 
					WHERE booking__promcode_language.promcode_id =  booking__promcode.promcode_id
					AND (start_date=0 OR start_date <= NOW())
					AND (end_date=0 OR end_date >= (CURDATE()))
					AND language = '" . LANGUAGE . "'
					AND  bin=0
					AND (custumer_id = 0 
					OR custumer_id IN (".$custumers."))";
						
				//Debug::p($query_promcode, 'text');
				$results = Db::get_rows($query_promcode);
				
				foreach ($results as $rs){
					$discount = $rs['discount_type']=='fixed'?' €':'%';
					$discount = $rs['discount_pvp'] . $discount;
					$promcode_input .= '<option value="'.$rs['promcode'].'">'.$rs['name'].' - '.$rs['promcode']. ' ' . $discount . '</option>';
				}
				$promcode_input = '<select id="promcode" name="promcode"><option val="0"></option>'.$promcode_input.'</select>';
				
			}
			else{
				$promcode_input = '<input type="text" id="promcode" name="promcode" />';
			}
		}
		else {
			$custumers = R::ids ('selected_custumer_ids');
		}
		
		$r = array(
			'show_promcode' =>false,
			'show_promcode_form' =>false,
			'promcode_error' =>false,
			'promcode_input' =>$promcode_input,
			'promcode_button' =>'<input type="button" id="promcode-button" name="promcode-button" value="'.$this->caption['c_promcode_button'].'" />',
			'promcode_id' =>0,
			'promcode_discount' =>0,
			'promcode_total' =>0,
			'price_total' =>$price_total,
		);
		
		// no hi ha id reserva no faig res
		if ( $book_id === false ) {
			return $r;
		}
		
		// no està logejat, mostro form i no faig res més
		if (!$is_admin && !BookingBook::is_logged()) {
			
			$r['show_promcode_form'] = true;
			
			// si es fa peticio ajax dono missatge d'error que s'ha de logejar per camjear codi
			if (!BookingBook::is_logged() && $this->is_ajax && $promcode){
				$r['promcode_error'] = $this->caption['c_promcode_err_not_logged'];
			}
			return $r;	
		}	
		
		
		
		// Si estem editant una reserva ja feta, agafa el promcode directe de la bbdd
		// A admin agafa de la base si no hi ha 'promcode' ( post promcode), si no recalcula i aplica un promcode diferent
		// o si es ajax agafa preu de nou com si no estigues guardat
			if (
				($is_admin && !$promcode) ||	
					
				(!$is_admin && $book_id != 0 && $GLOBALS['gl_action'] != 'add_record' && !$this->is_ajax)
				){

				$rs_book = Db::get_row("
							SELECT promcode_discount
								FROM booking__book 
								WHERE book_id = '".$book_id."'");

				$r['show_promcode_form'] = false;
				$r['promcode_id'] = false;
				
				// si es formulari nou de admin no hi ha rs
				if ($rs_book) {	
					$r['show_promcode']=$rs_book['promcode_discount']=='0.00'?'':1;
					$r['promcode_discount'] = $rs_book['promcode_discount'];			
					$r['price_total'] = $price_total -  $rs_book['promcode_discount'];					
				}


				$r['promcode_total'] = $price_total;
		
				return $r;
			}
		
		
		if ($is_admin) {			
			
			
		}
		else{
			
			// a public es un promcode unic, un cop entrat llavors agafa sempre el de session
			if ($promcode) $_SESSION['book']['promcode'] = $promcode;		
			$promcode = isset($_SESSION['book']['promcode'])?$_SESSION['book']['promcode']:false;
			
		}
		$r['show_promcode_form'] = true;
		
		// no hi ha promcode, mostro form per entrar-ne un
		if ($promcode===false) {
			return $r;
		}
		
		// FALTA FER PODER APLICAR PROMCODE UN COP RESERVA JA FETA A PUBLIC
		// Calculo PROMCODE segons el post ( ESTEM FENT RESERVA NOVA o APLICO NOU PROMCODE A RESERVA FETA )
		// si no arriba a despesa mínima es false
		// si no està entre les dates es false
		
		$custumer_id = $is_admin?$custumers:$_SESSION['custumer']['custumer_id'];
		
		$rs_promcode = Db::get_row("
							SELECT promcode_id, discount_pvp, discount_type, minimum_amount, one_time_only, start_date, end_date,
								((start_date=0 OR start_date <= NOW())
									AND (end_date=0 OR end_date >= (CURDATE()))) 
									as is_valid_date
								FROM booking__promcode 
								WHERE bin=0
								AND (custumer_id = 0 || custumer_id IN (".$custumer_id."))
								AND promcode = '".$promcode."'");
		
		if (!$rs_promcode){
			$r['promcode_error'] = $this->caption['c_promcode_err_not_valid'];
			unset($_SESSION['book']['promcode']);
		}
		elseif (!$rs_promcode['is_valid_date']){
			$r['promcode_error'] = sprintf($this->caption['c_promcode_err_expired'],$rs_promcode['start_date'],$rs_promcode['end_date']);
			unset($_SESSION['book']['promcode']);
		}
		elseif ($price_total<$rs_promcode['minimum_amount']){
			$r['promcode_error'] =  sprintf($this->caption['c_promcode_err_minimum'],number_format($rs_promcode['minimum_amount'], 2, ',','.')) . ' ' . CURRENCY_NAME;
			unset($_SESSION['book']['promcode']);
		}
		// si usuari ja ha disposat de la promocio
		elseif ($rs_promcode['one_time_only']=='1' && Db::get_first("SELECT custumer_id 
									FROM booking__custumer_to_promcode 
									WHERE custumer_id IN (".$custumer_id.")
									AND promcode_id = '".$rs_promcode['promcode_id']."'")){
			$r['promcode_error'] =  $this->caption['c_promcode_err_used'];
			unset($_SESSION['book']['promcode']);
		}
		else{
			
			$r['show_promcode']=true;
			$r['show_promcode_form'] = false;
			// calculo import segons tipus
			if ($rs_promcode['discount_type'] == 'fixed'){
				$discount = $rs_promcode['discount_pvp'];
			}
			else
			{			
				$discount = $price_total*$rs_promcode['discount_pvp']/100;		
			}
			$r['promcode_id'] = $rs_promcode['promcode_id'];
			$r['promcode_discount'] = $discount;
			$r['promcode_total'] = $price_total;			
			$r['price_total'] = $price_total - $discount;
		}
		return $r;
	}
	
	
	function get_minimum() {		
		
		$minimum_price = 0;
		foreach ($this->details as $d){
			
			$price = $d['price_value'];
			$nights = $d['nights'];
			
			// nights nomes surt a whole_week
			if ($d['type'] == 'whole_week' && $price) $price = $price/$nights;
			
			$minimum_price = $minimum_price<$d || !$minimum_price?$price:$minimum_price;
		}
		Debug::p($minimum_price, 'preu minim');
		return $minimum_price;
		
	}
	function set_page_javascript($book_id,$price_book_value,$price_extras_value,$price_total_value,$property_id,$promcode_discount, $price_tt, $book_rs) {

		$person = Db::get_first("SELECT person FROM inmo__property WHERE property_id = " . $property_id);

		if ($GLOBALS['gl_is_admin'] && $book_rs){
			$GLOBALS['gl_page']->javascript .= "
				bookingform.payment_1 = " . $book_rs['payment_done_1'] . ";
				bookingform.payment_2 = " . $book_rs['payment_done_2'] . ";
				bookingform.payment_3 = " . $book_rs['payment_done_3'] . ";
				bookingform.payment_4 = " . $book_rs['payment_done_4'] . ";
			";
		}

		$GLOBALS['gl_page']->javascript .= "
			bookingform.id = " . $book_id . ";
			bookingform.price_book = " . $price_book_value . ";
			bookingform.price_extras = " . $price_extras_value . ";
			bookingform.price_tt = " . $price_tt . ";
			bookingform.price_total = " . $price_total_value . ";
			bookingform.promcode_discount = " . $promcode_discount . ";
			bookingform.person = " . $person . ";
			bookingform.date_message_error = '" . addslashes($this->caption['c_no_date_selected']) . "';
			bookingform.equal_dates_error = '" . addslashes($this->caption['c_equal_dates_error']) . "';
			bookingform.too_many_person_error = '" . addslashes($this->caption['c_too_many_person_error']) . "';
			bookingform.payment_uncheck_message = '" . addslashes($this->caption['c_payment_uncheck_message']) . "';
			bookingform.promcode_err_not_main_custumer = '" . addslashes($this->caption['c_promcode_err_not_main_custumer']) . "';
			bookingform.init();
		";
	}
	
	// afegir a detall
	function add_detail($price,$date,$type,$nights = false,&$rs = false) {
				
		
		$price_formatted = $price?format_currency($price)  . ' €':'';
		
		$this->details [$date] = array(
			'price_value' => $price, 
			'price' => $price_formatted, 
			'date' => format_date_list($date) ,
			'type' => $type ,
			'nights' => $nights ,
			);
		
		
		if ($rs) {
			$season = Db::get_first("SELECT season FROM booking__season_language WHERE season_id = " . $rs['season_id']);
			$season_id = $rs['season_id'];
			$this->details [$date]['season'] = $season;
			$this->details [$date]['season_id'] = $season_id;
		}
	}
	
	// obtindre detall
	function get_detail() {

		if (!$this->details)
			return '';
		
		$table = New Table(4,'price_details');
		
		$table->set_col(1, 'td', 'season');
		
		$table->ths();
		
		$table->add_cell($this->caption['c_detail_season']);
		$table->add_cell($this->caption['c_detail_day']);
		$table->add_cell($this->caption['c_detail_type']);
		$table->add_cell($this->caption['c_detail_price']);
		
		
		$table->tds();
		
		$last_season = '';
		
		
		if ($this->details) {
			foreach ($this->details as $d) {
				extract($d);


				$table->set_col(1, 'td', 'season' . $season_id);


				if ($season != $last_season) {
					$table->set_row_class('season_change');
				}


				$table->add_cell($season == $last_season ? '' : $season);
				$table->add_cell($date);
				$table->add_cell($this->caption['c_detail_' . $type]);
				$table->add_cell($price);


				$last_season = $season;
			}
		}	
		
		$html = $table->get_table();
		
		return $html;
	}	
	
}