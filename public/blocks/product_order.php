<?php

$exclude_fields = isset($vars['exclude_fields'])?explode(',',$vars['exclude_fields']):array();
$vars['var_name'] = 'order';
$page_id = isset($vars['page_id']) ? $vars['page_id'] : false;

/*
Nomes està fet exclusivament per interfren, quan es necessitin més opcions s'haurà de modificar
$orders = array(
	(
		array
			(
				'order' => 'price',
				'way' => 'asc',
			)
	),
	(
		array
			(
				'order' => 'price',
				'way' => 'desc',
			)
	),
	(
		array
			(
				'order' => 'brand',
				'way' => 'asc',
			)
	),
	(
		array
			(
				'order' => 'brand',
				'way' => 'desc',
			)
	),
	(
		array
			(
				'order' => 'others9',
				'way' => 'asc',
			)
	),
	(
		array
			(
				'order' => 'others9',
				'way' => 'desc',
			)
	),
	(
		array
			(
				'order' => 'others4',
				'way' => 'asc',
			)
	),
	(
		array
			(
				'order' => 'others4',
				'way' => 'desc',
			)
	),
);*/
$orders = array(
	(
		array
			(
				'order' => 'price',
				'way' => 'asc',
			)
	),
	(
		array
			(
				'order' => 'price',
				'way' => 'desc',
			)
	),
	(
		array
			(
				'order' => 'product_title',
				'way' => 'asc',
			)
	),
	(
		array
			(
				'order' => 'product_title',
				'way' => 'desc',
			)
	),
	(
		array
			(
				'order' => 'brand',
				'way' => 'asc',
			)
	),
	(
		array
			(
				'order' => 'brand',
				'way' => 'desc',
			)
	),
);

if (isset($GLOBALS['gl_module']) && $GLOBALS['gl_module']->tool == 'product') {
	$caption = $GLOBALS['gl_module']->caption;
	$friendly_params = $GLOBALS['gl_module']->friendly_params;
}
else{	
	$module = Module::load('product','product');
	$module->set_other_config();
	$caption = $module->caption;
	$friendly_params = $module->friendly_params;
}

unset($friendly_params['page']);

foreach($exclude_fields as $exclude_field) {
	unset($orders[$exclude_field]);
}

$selected_order = R::get('orderby');
$vars['block_selected'] = $selected_order?1:0;
$vars['selected_item'] = '';
$conta = 0;

if ($orders) {
    foreach($orders as $rs) {
		extract($rs);
        ($order . '-' . $way == $selected_order)?$selected = 1:$selected = 0;
		$friendly_params['orderby'] = $order . '-' . $way;
		$l['link'] = get_link(array(
				'tool' => 'product',
				'tool_section' => 'product',
				'friendly_params' => $friendly_params,
				'exclude_params' => array('page'),
				'link_vars' => array('tipus'=>R::escape('tipus')),
				'detect_page' => $page_id?false:true,
				'page_id' => $page_id
			));
		//Debug::p($l['link'], 'text');
		
        $l['selected'] = $selected;
        $l['value'] = $order . '-' . $way;
        $l['item'] = $caption['c_' .$order] . ' ' . $caption['c_' .$way];

	    // Selecciono el primer si no n'hi ha cap
	    if ($selected || $conta == 0) $vars['selected_item'] = $l['item'];

		$l['class'] = '';
		$l['conta'] = $conta++;
        $loop[] = $l;
    }
}
	
	
$loop[0]['class'] = 'first';	
$loop[count($loop)-1]['class'] = 'last';

?>