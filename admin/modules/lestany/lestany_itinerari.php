<?
/**
 * LestanyItinerari
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2011
 * @version $Id$
 * @access public
 */
class LestanyItinerari extends Module{
	var $form_tabs;

	function __construct(){
		parent::__construct();
	}

	function on_load(){
		$this->form_tabs = array('0' => array('tab_action'=>'show_form_map','tab_caption'=>$this->caption['c_edit_map']));

		parent::on_load();
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function get_records()
	{
	    $listing = new ListRecords($this);

		$listing->add_button ('edit_map', 'action=show_form_map','boto1','after');

	    return $listing->list_records();
	}
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{
	    $show = new ShowForm($this);

		$show->form_tabs = $this->form_tabs;

		$this->set_field('entered','default_value',now(true));
		$this->set_field( 'modified', 'default_value', now( true ) );

		$show->set_form_level1_titles(
			'localitzacio',
			'descripcio_historica',
			'videoframe',
			'page_title'
		);

	    return $show->show_form();
	}

	function save_rows()
	{
	    $save_rows = new SaveRows($this);
	    $save_rows->save();
	}

	function write_record()
	{
	    $writerec = new SaveRows($this);
		$writerec->set_field( 'modified', 'override_save_value', 'now()' );
	    $writerec->save();
	}

	function manage_images()
	{
	    $image_manager = new ImageManager($this);
		$image_manager->form_tabs = $this->form_tabs;
	    $image_manager->execute();
	}





/*
 * MAPES
 */
	function show_form_map (){ //esenyo el napa
		global $gl_news;

		$rs = Db::get_row('SELECT * FROM lestany__itinerari WHERE itinerari_id = ' . $_GET['itinerari_id']);
		$rs['title'] = Db::get_first('SELECT nom FROM lestany__itinerari_language  WHERE itinerari_id = ' . $_GET['itinerari_id'] . ' AND language = LANGUAGE');
		$rs['address'] = "C/ Doctor Vilardell, 1, 08148 L’Estany";
		$rs['is_default_point'] = ($rs['latitude']!=0)?0:1;
		$rs['latitude']=$rs['latitude']!=0?$rs['latitude']:GOOGLE_CENTER_LATITUDE;
		$rs['longitude']=$rs['longitude']!=0?$rs['longitude']:GOOGLE_CENTER_LONGITUDE;
		$rs['center_latitude']=$rs['center_latitude']!=0?$rs['center_latitude']:GOOGLE_CENTER_LATITUDE;
		$rs['center_longitude']=$rs['center_longitude']!=0?$rs['center_longitude']:GOOGLE_CENTER_LONGITUDE;
		$rs['zoom']=$rs['zoom']?$rs['zoom']:GOOGLE_ZOOM;
		$rs['menu_id'] =  $_GET['menu_id'];
		$rs['id'] =  $rs[$this->id_field];
		$rs['id_field'] =  $this->id_field;
		$this->set_vars($rs);//passo els resultats del array
		$this->set_file('lestany/itinerari_map.tpl'); //crido el arxiu
		$this->set_vars($this->caption); //passo tots els captions
		$gl_news = $this->caption['c_click_map'];
		$content = $this->process();//processo
		$GLOBALS['gl_content'] = $content;
	}
	function save_record_map(){
		global $gl_page;
		extract($_GET);

		Db::execute ("UPDATE `lestany__itinerari` SET
			`latitude` = '$latitude',
			`longitude` = '$longitude',
			`center_latitude` = '$center_latitude',
			`center_longitude` = '$center_longitude',
			`zoom` = '$zoom'
			WHERE `itinerari_id` =$itinerari_id
			LIMIT 1") ;
		$gl_page->show_message($this->messages['point_saved']);
		Debug::p_all();
		die();
	}
}
?>