<?php
set_table('inmo__category','category_id');
set_language_vars('inmo','category','category');
$GLOBALS['gl_order_by'] = 'ordre asc, category asc';
function save_rows()
{
    $save_rows = new SaveRows;
    $save_rows->save();
}

function list_records()
{
    $listing = new ListRecords;
    $listing->show_langs = true;
    $listing->has_bin = false;
    $listing->order_by = $GLOBALS['gl_order_by'];
    $listing->list_records();
}

function show_form()
{
    $show = new ShowForm;
    $show->order_by = $GLOBALS['gl_order_by'];
    $show->has_bin = false;
    $show->show_form();
}

function write_record()
{
    $writerec = new SaveRows;
    $writerec->save();
}

function manage_images()
{
    $image_manager = new ImageManager;
    $image_manager->execute();
}

?>