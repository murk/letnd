<?php
// no es poden posar ; entre mig de la consulta ja que parteix les consultes per el ; a no ser que fem servir $update_sql en array()
	
if (is_table("product__family") && !is_table("product__configadmin_family")){
	
	$update_sql[]="CREATE TABLE IF NOT EXISTS `product__configadmin_family` (
  `configadmin_family_id` int(11) NOT NULL auto_increment,
  `name` varchar(50) collate utf8_spanish2_ci NOT NULL,
  `value` varchar(255) collate utf8_spanish2_ci NOT NULL,
  PRIMARY KEY  (`configadmin_family_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;";
	
	$update_sql[]="
INSERT INTO `product__configadmin_family` (`configadmin_family_id`, `name`, `value`) VALUES
	(1, 'im_thumb_w', '200'),
	(2, 'im_thumb_h', '150'),
	(3, 'im_thumb_q', '80'),
	(4, 'im_details_w', '300'),
	(5, 'im_details_h', '225'),
	(6, 'im_details_q', '80'),
	(7, 'im_medium_w', '900'),
	(8, 'im_medium_h', '675'),
	(9, 'im_medium_q', '70'),
	(10, 'im_big_w', '1200'),
	(11, 'im_big_h', '900'),
	(12, 'im_big_q', '70'),
	(13, 'im_list_cols', '1'),
	(14, 'im_custom_ratios', '');
";	
}
?>