<?
/**
 * BookingExtra
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 6-2014
 * @version $Id$
 * @access public
 */
class BookingExtra extends Module{
	var $is_contract = false;
	function __construct(){
		parent::__construct();
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function set_listing_commons(&$listing){
		$listing->group_fields = array('extra_type');
		$listing->order_by = "FIELD(booking__extra.extra_type,'optional','required','included'), extra ASC";
	}
	function get_records()
	{
		$property_id = R::id('property_id');
		
		$listing = new ListRecords($this);			
		$listing->add_swap_edit();
		if ($listing->is_editable){
			$listing->show_langs = true;
		}
		else{		
			$listing->set_field('has_quantity','type','yesno');
		}
		
		
		$this->set_listing_commons($listing);
		
	    return $listing->list_records();
	}
	
	
	/*
	 * 
	 *  Extres per una reserva concreta
	 * 
	 * 
	 * 
	 * 
	 */	
	
	function get_book_extras($book_id, $property_id, $is_editable= true, $get_array = false){
		
		Main::load_class('booking','common','admin');
		$common = New BookingCommon;
		$common->is_contract = $this->is_contract;
		$common->is_editable = $is_editable;
		return $common->get_book_extras($this, $book_id, $property_id, $get_array);
		
	}
	
	
	/*
	 * 
	 *  Extres per una propieta
	 *  
	 *  Es configuren de quins extres disposa una propietat en concret
	 * 
	 * 
	 */	
	function show_property_extras()
	{
		$property_id = R::id('property_id');
		$this->action="get_records";
		$listing = new ListRecords($this);		

		$listing->set_options(1, 0, 1, 1, 0, 0, 0, 0, 0, 1);
		$listing->set_field('extra_type', 'list_admin', 'text');
		$listing->set_field('extra', 'list_admin', 'text');
		$listing->set_field('has_quantity', 'list_admin', '0');
		$listing->paginate = false;

		$listing->call('records_walk_property', '', true);

		$listing->add_field('extra_price', array(
					'type'=> 'currency',
					'list_admin'=> 'input',
					'change_field_name' => '(SELECT extra_price FROM booking__extra_to_property WHERE extra_id =  booking__extra.extra_id AND property_id = ' . $property_id . ') as extra_price'
				)
			);

		$listing->add_field('extra_book_selected', array(
					'type'=> 'none',
					'list_admin'=> 'out'
				)
			);
		$listing->onmousedown = false;
		$listing->form_link = "/admin/?tool=booking&tool_section=extra&process=save_property_extras&property_id=" . $property_id;
	
		
		$this->set_listing_commons($listing);
		
	    $extra_list = $listing->list_records();		
				
		$this->set_vars(array(
			'extra_list' => $extra_list
			));
		
		$this->tpl->file = 'inmo/property_extra.tpl';
		
		$rs = Db::get_row('SELECT ref, property_private, property_id FROM inmo__property WHERE property_id = ' . $_GET['property_id']);
		
		$this->tpl->set_vars($rs);
		
		$GLOBALS['gl_content'] = $this->process();
	}
	function records_walk_property(&$listing) {
		$rs = &$listing->rs;
				
		$checked = is_null($rs['extra_price'])?'':' checked="checked"';		
		
		$ret['extra_book_selected'] = '<input type="checkbox" value="' . $rs['extra_id'] . '" name="extra_book_selected[' . $rs['extra_id'] . ']"'.$checked.'>';
		
		return $ret;
	}
	
	
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{
	    $show = new ShowForm($this);
	    return $show->show_form();
	}
	
	function save_property_extras() {
		
		$query = New Query('', 'booking__extra_to_property', 'property_id,extra_id', 'extra_price');
		$query->messages = &$this->messages;
		$query->set_config ('none','int','currency');		
		$query->save_related_post('extra_book_selected');		
		
	}
	
	function save_rows()
	{
	    if ($this->process=='save_property_extras') {
			$this->save_property_extras();
			return;
		}
		
		$this->set_field('extra_type', 'change_field_name', 'extra_type');
				
		$save_rows = new SaveRows($this);
		
	    $save_rows->save();
	}

	function write_record()
	{
		$this->set_field('extra_type', 'change_field_name', 'extra_type');
		
	    $writerec = new SaveRows($this);
	    $writerec->save();
	}
}
?>