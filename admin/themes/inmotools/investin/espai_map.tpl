<script type="text/javascript" src="//maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript">
var gl_is_default_point = <?=$is_default_point;?>;
var gl_latitude = '<?=$latitude;?>';
var gl_longitude = '<?=$longitude;?>';
var gl_center_latitude = <?=$center_latitude;?>;
var gl_center_longitude = <?=$center_longitude;?>;
var gl_zoom = <?=$zoom;?>;
var gl_menu_id =<?=$menu_id;?>;
var gl_id_field = '<?=$id_field;?>';
var gl_id =<?=$id;?>;
var gl_polyline_type = 'polygon';
MAP_FRAME_TEXT_1 = '<?=addslashes($c_map_frame_1)?>';
MAP_FRAME_TEXT_2 = '<?=addslashes($c_map_frame_2)?>';
MAP_FRAME_TEXT_3 = '<?=addslashes($c_map_frame_3)?>';
MAP_FRAME_TEXT_4 = '<?=addslashes($c_map_frame_4)?>';
</script>
<script type="text/javascript" src="/admin/jscripts/map_polyline.js?v=<?=$version?>"></script>
<div  class="form">
<form name="theForm" method="post" action="" onsubmit="">
  <div id="form_buttons_holder"><table id ="form_buttons">
  <tr>
    <td valign="bottom"><div class="menu">
      <a href="?action=show_form_edit&amp;menu_id=<?=$menu_id?>&amp;espai_id=<?=$espai_id?>"><?=$c_edit_data?></a>
      <a href="?action=list_records_images&amp;menu_id=<?=$menu_id?>&amp;espai_id=<?=$espai_id?>">
        <?=$c_edit_images?>
        </a>
      <div>
        <?=$c_edit_map?>
      </div></div></td>
    <td class="buttons_top">
            <div class="right" style="margin-bottom:3px;"><a href="javascript:save_map()" class="btn_form_save">
    <?=$c_save_map?>
    </a></div></td>
  </tr>  
</table></div></form>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td>
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td>
                <table width="100%" cellpadding="4" cellspacing="0">
                  <tr>
                    <td class="formCaptionTitle"><strong>
                      <?=$c_form_map_title?>:</strong>&nbsp;&nbsp <?=$title?> 
                    </td>
                  </tr>
              </table>
              <table width="100%" cellpadding="4" cellspacing="0" class="formstbl">
               
                  <tr>
                    <td align="center" valign="top" class="forms"><table width="99%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td id="custumers"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td align="center">
							<form onsubmit="return search_point()" class="google_map">
								<table cellpadding="0" cellspacing="0" border="0"><tr>
									<td style="padding-right:30px;"><a onclick="remove_all_markers();" class="btn_form_delete" /><?=$c_remove_polyline?></a></td>
									<td><strong class="formsCaption">
									<?=$c_search_adress?>: </strong><input id="map_address" name="map_address" type="text" value="<? if ($address):?><?=$address?><? endif //$address?>" size="70" class="search" /></td>
									<td><input type="submit" value="ok" class="boto-search" /></td>
								</tr></table>
							</form>
							<div><span class="formsCaption"><?=$c_latitude?>:&nbsp;</span><span id="latitut"><?=$latitude1?></span>&nbsp;&nbsp;&nbsp;<span class="formsCaption"><?=$c_longitude?>:&nbsp;</span><span id="longitut"><?=$longitude1?></span></div>
							<div id="google_map"></div>  							
						</td>
						</tr>
                      </table></td>
                      </tr>
                    </table>
                    </td>
                  </tr>
              </table></td>
          </tr>
        </table></td>
    </tr>
</table>
<table width="100%" cellpadding="4" cellspacing="2" class="formsButtonstbl">
  <tr>
    <td align="center" class="formsButtons"><div class="centerFloat1">
                        <div class="centerFloat2"><a href="javascript:save_map()" class="btn_form_save">
    <?=$c_save_map?>
    </a></div></div></td>
  </tr>
</table>
<table width="100%" cellpadding="4" cellspacing="0">
                  <tr>
                    <td class="formCaptionTitle"><strong>
                      Entrada directa de coordenades:</strong>&nbsp;&nbsp <?=$title?> 
                    </td>
                  </tr>
              </table>
<form method="post" target="save_frame" class="google_map" action="/admin/?action=save_rows_map_points&espai_id=<?=$espai_id?>&menu_id=<?=$menu_id?>">
  <table width="100%" cellpadding="4" cellspacing="2">
  <tr>
    <td align="center" class="formsCaptionBig"><div style="width:641px;text-align:left">
      <p>&nbsp;</p>
      <p><strong>Per guardar les cooredenades directament en el mapa cal posar-les separades en el seg&uuml;ent format: </strong>
      </p>
      <p>Latitud i longitud en nombres decimals amb un punt enlloc de coma, ej:: 41.3342</p>
      <p>Latitud i longitud separats per una coma, ej: 42.0556,2.1223</p>
      <p>Pars de longituds i latituds separats per un punt i coma</p>
      <p>No es necessari un salt de línea entre pars de longituts i latituds</p>
      <p><strong>Ejemple:</strong></p>
      <p>latitud1,longitud1;<br />
  latitud2,longitud2;<br />
  latitud3,longitud3;<br />
  42.119617324466994,2.530975341796875;<br />
  42.122,2.588;
  42.1094301,2.62435913;<br />
  ....
</p></div><textarea name="points" rows="20" class="wide" id="map_address"></textarea>
    </td>
  </tr>
  <tr>
    <td align="center" class="formsButtons"><div class="centerFloat1">
                        <div class="centerFloat2"><input type="submit" value="Guardar coordenades" class="btn_form_save" /></div></div></td>
  </tr>
</table></form>

</div>