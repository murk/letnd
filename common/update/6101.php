<?

/* 3-11-16

	PRODUCT - Transportista a la taula order


 */

if ( is_table( 'product__order' ) && ! is_field( 'product__order', 'courier_id' ) ) {

	$update_sql[] = "ALTER TABLE `product__order`
	ADD COLUMN `delivered_emailed` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00' AFTER `delivered`,
	ADD COLUMN `shipped_emailed` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00' AFTER `shipped`,
	ADD COLUMN `courier_id` INT(11) NOT NULL DEFAULT '0' AFTER `error_description`,
	ADD COLUMN `courier_follow_code` VARCHAR(100) NOT NULL DEFAULT '' AFTER `courier_id`;";

} else {
	if ( is_table( 'product__order' ) ) {
		trigger_error( 'Ja existeixen el camp courier_id' );
	}
}

?>