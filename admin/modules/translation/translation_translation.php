<?php
// include(DOCUMENT_ROOT . '/admin/modules/translation/translation_functions.php');
function list_records()
{
    Db::connect_mother();
    global $gl_action;
    $listing = new ListRecords;
    $listing->call('list_records_walk', 'id2');
    $listing->order_by = 'join_date ASC, id ASC';
    switch ($gl_action) {
        case 'list_records_state':
            $listing->set_options(1, 0, 0, 0, 0, 0, 1); // set_options ($options_bar = 1, $options_checkboxes = 1, $save_button = 1, $select_button = 1, $delete_button = 1, $print_button = 1, $edit_buttons = 1)
            $listing->add_filter('state');
            $listing->add_filter('original_language');
            break;
        case 'list_records':
            $listing->condition = "state != 3";
            $listing->set_options(0, 0, 0, 0, 0, 0, 1); // set_options ($options_bar = 1, $options_checkboxes = 1, $save_button = 1, $select_button = 1, $delete_button = 1, $print_button = 1, $edit_buttons = 1)
            break;
    }


    $listing->list_records();
    Db::reconnect();
}
function list_records_walk($id)
{
    return array('id_fake' => $id);
}

function show_form()
{
    Db::connect_mother();

	$query = "SELECT state FROM translation__translation WHERE translation_id = '" . $_GET['translation_id'] . "'";
    $results = Db::get_rows($query);

	if ($results[0]['state']==3) {
	  set_fields_to_text(); //aixó em passa tots els camps automàticament a text
	}
	$show = new ShowForm;
    $show->show_form();

    Db::reconnect();
    // un cop acabat tinc que saltar a save_rows
}
function write_record()
{
    global $gl_content, $gl_menu_id, $gl_page, $gl_message;
    Db::connect_mother();
    // Sempre guardo a la bd de letnd
    $writerec = new SaveRows;
    $writerec->save();
	$finish_date = date("Y-m-d");
	// per defecte sempre salvo, pero miro si el estat és finalitzat, si és finalitzat, guardo a la carpeta del client
    // i envio un correu al client i un a nosaltres
    if (current ($_POST['state']) == 3) {
		// 1er però comprovo, que hagin omplert com a mínim algo en els camps que havien de traduir.
        $i = 0;
        foreach (current($_POST['translation']) as $key => $val) {
            // el key correspont a spa, o eng... i el val al contingut
            // m'asseguro que hi hagi algo dins de cada camp que hagin traduit
            $error = check_correct_translate($val);
            // si no hi ha error guardo els resultats a un array associatiu
            if ($error) {
                break;
            } else {
                $trad[$i]['lang'] = $key;
                $trad[$i]['contingut'] = $val;
                $i++;
            }
            //
        }
        if (!$error) {
		$status=3;
            // salvo els resultats i desbloquejo
			//si el estat es igual a finalitzat, guardo també la data de finalització, el traductor i la companyia
			//tinc que afegir a la hora de guardar el traductor, la companyia i i el user_id
           $query10 = "UPDATE translation__translation SET  user_id = '" . $_SESSION['user_id'] . "',  translated_date = '" . $finish_date . "' WHERE translation_id =" . current ($_POST['translation_id']);
          Db::execute($query10);
            // recollo les dades per conectar
            $query = "SELECT translation__translation.client_id AS client_id, `table`, table_id, field, id,
				host AS dbhost, dbname, dbusername, dbpwd, mail1, comercialname
				FROM translation__translation, client__client
				WHERE translation_id = '" . current ($_POST['translation_id']) . "'
				AND translation__translation.client_id = client__client.client_id";
            $results = Db::get_rows($query);
            extract($results[0]);
            // salvo
            // em conecto a la bd del client
            // inserto els resusltats
            connect_client($results[0]['dbhost'], $results[0]['dbname'], $results[0]['dbusername'], $results[0]['dbpwd']);
            foreach ($trad as $rs) {
                $query2 = "UPDATE " . $results[0]['table'] . " SET " . $results[0]['field'] . " = '" . $rs['contingut'] . "' WHERE " . $results[0]['table_id'] . " = " . $results[0]['id'] . " AND language = '" . $rs['lang'] . "' ";
                Db::execute($query2);
            }
            // un cop acabat desbloquejo i envio un e-mail al client i a nosaltres.
            $query5 = "UPDATE " . erase_language($table) . " SET blocked = '0' WHERE " . $results[0]['table_id'] . " = " . $results[0]['id'];
            Db::execute ($query5);
            send_confirmation_mail_ok ($results[0]['comercialname'], $results[0]['mail1'], $status);
        } else {
            $status=4;
			// S'hi ha donat error no desbloquejo ni gravo i el que faig es posar el state a error i enviar un mail als traductors i a nosaltres
           // send_error_mail_translators();
			$query6 = "UPDATE translation__translation SET  state = '4', translated_date = '".$finish_date."' WHERE translation_id =" . current ($_POST['translation_id']);
			Db::execute($query6);
			send_error_mail_translators($status,current ($_POST['translation_id']));
            $gl_message = SAVED_TRANSLATION_ERROR;
        }
    }
    Db::reconnect();
}
function send_error_mail_translators ($status,$trans_id)
{
    // això s'haura d'arreglar pq després agafi el mail de l'agencia de traducció
    $mail = get_mailer(); //agafo la configuració que esta al arxiu funcions
    $mail->AddAddress ("marc@letnd.com"); //mail del client
	$mail->Subject = "Letnd - Traduccions erroneas - ";
    $mail->Body = '<DIV><FONT face=Arial size=2><IMG alt="" hspace=0
src="/admin/themes/inmotools/images/logo_intranet_top.gif"align=baseline border=0></FONT></DIV>
<DIV><FONT face=Arial size=2></FONT>&nbsp;</DIV>
<DIV><FONT face=Arial size=2>9 Sistema informa que la traducció: ' . $trans_id . ' s\'ha guardat amb un estat de:
<STRONG><FONT color=#808000 size=3>Error</FONT></STRONG>.</FONT></DIV>
<DIV><FONT face=Arial size=2></FONT>&nbsp;</DIV>
<DIV><FONT face=Arial size=2>Att.&nbsp;9 Sistema.</FONT></DIV>
<DIV><FONT face=Arial size=2></FONT>&nbsp;</DIV>
<DIV><FONT face=Arial size=1>9 Sistema Serveis Informàtics 2005
S.L.</FONT></DIV>
<DIV><FONT face=Arial size=1>C/ Juli Garreta 25C 2on a(17002) Girona</FONT></DIV>
<DIV><FONT face=Arial size=1>Telf. 777 888 999 / 665 731 656 / 661 333
289</FONT></DIV>';
    $mail->AltBody = "Informar que té traduccions finalitzades.";
    $exito = $mail->Send();
    $intentos = 1;
    while ((!$exito) && ($intentos < 5)) {
        sleep(5);
        // echo $mail->ErrorInfo;
        $exito = $mail->Send();
        $intentos = $intentos + 1;
    }
   //  return $exito;
   send_us_mail ($comercialname,$status); //també ens enviem un mail a nosaltres;

}
function send_confirmation_mail_ok ($comercialname, $mailclient,$status)
{
    $mail = get_mailer(); //agafo la configuració que esta al arxiu funcions
    $mail->AddAddress($mailclient); //mail del client
	$mail->Subject = "Letnd - Traduccions finalitzades - ";
    $mail->Body = '<DIV><FONT face=Arial size=2><IMG alt="" hspace=0
src="/admin/themes/inmotools/images/logo_intranet_top.gif"align=baseline border=0></FONT></DIV>
<DIV><FONT face=Arial size=2></FONT>&nbsp;</DIV>
<DIV><FONT face=Arial size=2>9 Sistema informa que li han arribat traduccions finalitzades
<STRONG><FONT color=#808000 size=3>correctament</FONT></STRONG>.</FONT></DIV>
<DIV><FONT face=Arial size=2></FONT>&nbsp;</DIV>
<DIV><FONT face=Arial size=2>Att.&nbsp;9 Sistema.</FONT></DIV>
<DIV><FONT face=Arial size=2></FONT>&nbsp;</DIV>
<DIV><FONT face=Arial size=1>9 Sistema Serveis Informàtics 2005
S.L.</FONT></DIV>
<DIV><FONT face=Arial size=1>C/ Juli Garreta 25C 2on a(17002) Girona</FONT></DIV>
<DIV><FONT face=Arial size=1>Telf. 777 888 999 / 665 731 656 / 661 333
289</FONT></DIV>';
    $mail->AltBody = "Informar que té traduccions finalitzades.";

    send_mail($mail, false);

    // return $exito;
    send_us_mail ($comercialname,$status); //també ens enviem un mail a nosaltres;
}
function send_us_mail ($comercialname,$status)
{
$stat = get_status($status);
    $mail = get_mailer();
    $mail->AddAddress('info@letnd.com'); //mail del client
	$mail->Subject = "Letnd - Traduccions finalitzades - "; //asunto
    $mail->Body = '<DIV><FONT face=Arial size=2><IMG alt="" hspace=0
src="/admin/themes/inmotools/images/logo_intranet_top.gif"align=baseline border=0></FONT></DIV>
<DIV><FONT face=Arial size=2></FONT>&nbsp;</DIV>
<DIV><FONT face=Arial size=2>El client ' . $comercialname . ' ha rebut traduccions amb un estat de:
<STRONG><FONT color=#808000 size=3>'.$stat.'</FONT></STRONG>.</FONT></DIV>
<DIV><FONT face=Arial size=2></FONT>&nbsp;</DIV>
<DIV><FONT face=Arial size=2>Att.&nbsp;9 Sistema.</FONT></DIV>
<DIV><FONT face=Arial size=2></FONT>&nbsp;</DIV>
<DIV><FONT face=Arial size=1>9 Sistema Serveis Informàtics 2005
S.L.</FONT></DIV>
<DIV><FONT face=Arial size=1>C/ Juli Garreta 25C 2on a(17002) Girona</FONT></DIV>
<DIV><FONT face=Arial size=1>Telf. 777 888 999 / 665 731 656 / 661 333
289</FONT></DIV>';
    $mail->AltBody = "Clients amb traduccions finalitzades.";
    $exito = send_mail($mail, false);
    return $exito;
}
function get_status ($status) {
switch($status){
	case '3': $status = 'Finalitzades';
		break;
	default:$status = 'Error';
}
return $status;
}
function erase_language ($string)
{
    $string = substr($string, 0, -9);
    return $string;
}
function connect_client($dbhost, $dbname, $dbuname, $dbpass)
{
    // Start connection
    Db::connect($dbhost, $dbuname, $dbpass, $dbname);
}
function check_correct_translate ($language)
{
	// Aquesta funcio el que fa es assegurarce que hi hagi algo escrit a tots els idiomes a traduir    	    {
    if ($language == "") {
        $error = true;
    }
	else {
	$error = false;
	}
    return $error;
}
?>