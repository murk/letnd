<?
/**
 * AtotLloc
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 5/11/13
 * @version $Id$
 * @access public
 */
class AtotLloc extends Module{

	function __construct(){
		parent::__construct();
		$this->order_by = 'ordre ASC, lloc ASC';
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function get_records()
	{
	    $listing = new ListRecords($this);
		$listing->has_bin = false;
		$listing->show_langs = true;
		$listing->order_by = $this->order_by;
		$listing->set_options(1, 1, 1, 1, 1, 1, 0);
	    return $listing->list_records();
	}
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{
	    $show = new ShowForm($this);
		$show->order_by = $this->order_by;
		$show->has_bin = false;
	    return $show->show_form();
	}

	function save_rows()
	{
	    $save_rows = new SaveRows($this);
	    $save_rows->save();
	}

	function write_record()
	{
	    $writerec = new SaveRows($this);
	    $writerec->save();
	}

	function manage_images()
	{
	    $image_manager = new ImageManager($this);
	    $image_manager->execute();
	}
}
?>