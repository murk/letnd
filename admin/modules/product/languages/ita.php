<?php
// menus eina
if (!defined('PRODUCT_MENU_PRODUCT')){
define('PRODUCT_MENU_PRODUCT','Prodotti');
define('PRODUCT_MENU_NEW','Nuovo prodotto');
define('PRODUCT_MENU_LIST','Lista prodotti');
define('PRODUCT_MENU_LIST_BIN','Cestino');
define('PRODUCT_MENU_FAMILY','Famiglie');
define('PRODUCT_MENU_FAMILY_NEW','Creare famiglia');
define('PRODUCT_MENU_FAMILY_LIST','Lista delle famiglie');
define('PRODUCT_MENU_FAMILY_LIST_BIN','Cestino');
define('PRODUCT_MENU_BRAND','Marcas');
define('PRODUCT_MENU_BRAND_NEW','Creare marca');
define('PRODUCT_MENU_BRAND_LIST','Lista delle marque');
define('PRODUCT_MENU_BRAND_LIST_BIN','Cestino');
define('PRODUCT_MENU_TAX','Tasse');
define('PRODUCT_MENU_TAX_NEW','Creare tasse');
define('PRODUCT_MENU_TAX_LIST','Lista delle tasse');
define('PRODUCT_MENU_TAX_LIST_BIN','Cestino');
//--------------- a traduir -----------------
define('PRODUCT_MENU_COLOR','Colori');
define('PRODUCT_MENU_COLOR_NEW','Crear colore');
define('PRODUCT_MENU_COLOR_LIST','Lista dei colori');
define('PRODUCT_MENU_COLOR_LIST_BIN','Cestino');
define('PRODUCT_MENU_MATERIAL','Materiale');
define('PRODUCT_MENU_MATERIAL_NEW','Creare materiale');
define('PRODUCT_MENU_MATERIAL_LIST','Lista dei materiale');
define('PRODUCT_MENU_MATERIAL_LIST_BIN','Cestino');
define('PRODUCT_MENU_VARIATION','Variazioni di prezzo');
define('PRODUCT_MENU_VARIATION_NEW','Creare variazione');
define('PRODUCT_MENU_VARIATION_LIST','Lista delle variazione');
define('PRODUCT_MENU_VARIATION_DEFAULT','Variazioni per difetto');
define('PRODUCT_MENU_VARIATION_CATEGORY','Categorie di variazioni');
define('PRODUCT_MENU_VARIATION_CATEGORY_NEW','Creare Categoria');
define('PRODUCT_MENU_VARIATION_CATEGORY_LIST','Lista delle Categorie');
define('PRODUCT_MENU_VARIATION_LIST_BIN','Cestino');

define('PRODUCT_MENU_PROMCODE','Codice di promozione');
define('PRODUCT_MENU_PROMCODE_NEW','Creare codice');
define('PRODUCT_MENU_PROMCODE_LIST','Lista dei codici');
define('PRODUCT_MENU_PROMCODE_LIST_BIN','Cestino');

define('PRODUCT_MENU_SECTOR','Settori');
define('PRODUCT_MENU_SECTOR_NEW','Creare settore');
define('PRODUCT_MENU_SECTOR_LIST','Lista dei settori');
define('PRODUCT_MENU_SECTOR_LIST_BIN','Cestino');

define('SELLPOINT_MENU_SELLPOINT','Punti di vendita');
define('SELLPOINT_MENU_SELLPOINT_NEW','Nuovo punto');
define('SELLPOINT_MENU_SELLPOINT_LIST','Lista dei punti');
define('SELLPOINT_MENU_SELLPOINT_BIN','Cestino');

define('PRODUCT_MENU_SPEC','Specificazioni');
define('PRODUCT_MENU_SPEC_NEW','Creare specificazione');
define('PRODUCT_MENU_SPEC_LIST','Lista delle specificazioni');
define('PRODUCT_MENU_SPEC_CATEGORY','Categorie specificazioni');
define('PRODUCT_MENU_SPEC_CATEGORY_NEW','Creare categoria');
define('PRODUCT_MENU_SPEC_CATEGORY_LIST','Lista delle categorie');
define('PRODUCT_MENU_SPEC_LIST_BIN','Cestino');

define('PRODUCT_MENU_QUANTITYCONCEPT','Concepts quantity');
define('PRODUCT_MENU_QUANTITYCONCEPT_NEW','New concept');
define('PRODUCT_MENU_QUANTITYCONCEPT_LIST','List concepts');
}



$gl_caption['c_images_variation_title']='Variation';
$gl_caption_color['c_color_name']='Nome del colore';
$gl_caption['c_color_id']='Colore';
$gl_caption['c_material_id']='Materiale';
$gl_caption['c_choose']='Scegli';
$gl_caption_product['c_temperature_unit']='';
$gl_caption_product['c_util_life_unit']='';
$gl_caption_product['c_others1']='';
$gl_caption_product['c_others2']='';
$gl_caption_product['c_others3']='';
$gl_caption_product['c_others4']='';
$gl_caption_product['c_auto_ref']='Automatica';

$gl_caption_product['c_format']='Formato';
$gl_caption_product['c_weight']='Peso';
$gl_caption_product['c_gram']='grammi';
$gl_caption_product['c_kilo']='chilo/s';
$gl_caption_product['c_weight_unit']='';
$gl_caption_product['c_height_unit']='';
$gl_caption_product['c_height']='Altezza';
$gl_caption_product['c_mm']='mm';
$gl_caption_product['c_cm']='cm';
$gl_caption_product['c_mt']='mt';
$gl_caption_product['c_km']='km';
$gl_caption_product['c_feet']='piedi';
$gl_caption_product['c_temperature']='Temperatura';
$gl_caption_product['c_celcius']='celcius';
$gl_caption_product['c_fahrenheit']='fahrenheit';
$gl_caption_product['c_util_life']='Vita utile';
$gl_caption_product['c_hours']='ore';
$gl_caption_product['c_days']='giorni';
$gl_caption_product['c_years']='ani';
$gl_caption_product['c_meters']='metri';
$gl_caption_product['c_km']='chilometri';
$gl_caption_product['c_months']='mesi';
$gl_caption_product['c_caracteristics']='caratteristiche';
$gl_caption_product['c_other_caracteristics']='Altri caratteristiche <a href="/admin/?menu_id=4110">( configurazione )</a>';
$gl_caption_product['c_power']='Potenza';
$gl_caption_product['c_alimentacion']='Fornitura';
$gl_caption_product['c_certification']='Certificato/i';
$gl_caption_material['c_material']='Nome del materiale';


//-----------------------------------
$gl_caption['c_ref'] = 'Rif';
$gl_caption_product['c_home']='Featured';
$gl_caption_product['c_ref_number'] = 'Rif. automatica';
$gl_caption_product['c_ref_supplier'] = 'Rif fornitore';
$gl_caption_product['c_product_private'] = 'Nome privato';
$gl_caption['c_family_id'] = 'Famiglia';
$gl_caption_product['c_subfamily_id'] = 'Subfamiglia';
$gl_caption['c_family_content_list'] = 'Breve descrizione alla lista';
$gl_caption['c_family_description']='Descripzione della famiglia';
$gl_caption_product['c_subfamily_id_format']='#%s#%s';
$gl_caption_product['c_pvp'] = 'PVP';
$gl_caption['c_price'] = 'Prezzo';
$gl_caption_product['c_destacat']='Featured';
$gl_caption_product['c_pvd'] = 'PVD';
$gl_caption_product['c_pvd1'] = 'Tarifa 1';
$gl_caption_product['c_pvd2'] = 'Tarifa 2';
$gl_caption_product['c_pvd3'] = 'Tarifa 3';
$gl_caption_product['c_pvd4'] = 'Tarifa 4';
$gl_caption_product['c_pvd5'] = 'Tarifa 5';
$gl_caption_product['c_price_consult'] = 'a consultare';
$gl_caption_product['c_price_consult_1'] = 'Prezzo a consultare';
$gl_caption_product['c_brand_id'] = $gl_caption_product['c_brand'] = 'Marca';
$gl_caption_product['c_sell'] = 'Vendita web?';
$gl_caption['c_guarantee']='Garanzia';
$gl_caption_product['c_guarantee_period']='Periodo di garanzia';
$gl_caption_product['c_da']='giorni';
$gl_caption_product['c_mo']='mesi';
$gl_caption_product['c_yr']='anni';
$gl_caption['c_status'] = 'Stato';
$gl_caption_product['c_free_send'] = 'Permetere trasporto libero';
$gl_caption_product['c_ordre'] = 'Ordine';
$gl_caption['c_product_title'] = 'Nome di prodotto';
$gl_caption['c_product_subtitle'] = 'Sottotitolo';
$gl_caption_product['c_product_description'] = 'Descripzione di prodotto';
$gl_caption['c_prepare'] = 'In preparazione';
$gl_caption['c_review'] = 'A rivedere';
$gl_caption['c_onsale'] = 'Di vendere';
$gl_caption['c_nostock'] = 'Esaurito';
$gl_caption['c_nocatalog'] = 'Fuori catalogo';
$gl_caption['c_archived'] = 'Archiviato';
$gl_caption['c_tax_id'] = 'I.V.A (%)';
$gl_caption['c_tax_included'] = 'IVA incluso';
$gl_caption['c_tax_not_included'] = 'IVA non incluso';
$gl_caption_product['c_offer'] = 'Offerta';
$gl_caption_product['c_discount'] = 'Disconto (%)';
$gl_caption_product['c_discount_wholesaler'] = 'Descuento distributore(%)';
$gl_caption_product['c_margin'] = 'Margine (%)';
$gl_caption_product['c_ean'] = 'EAN';
$gl_caption_product['c_price_unit'] = 'Prezzo per';
$gl_caption_product['c_unit'] = 'Unità';
$gl_caption_product['c_ten'] = 'Decina';
$gl_caption_product['c_hundred'] = 'Centinaio';
$gl_caption_product['c_thousand'] = 'Mille';
$gl_caption_product['c_couple'] = 'Paio';
$gl_caption_product['c_dozen'] = 'Dozzina';
$gl_caption_product['c_m2'] = 'm2';
$gl_caption_product['c_data']='Datti di prodotto';
$gl_caption_product['c_general_data']='Datti generali';
$gl_caption_product['c_descriptions']='Descripzioni';
$gl_caption_product['c_description']='descripzione';
$gl_caption_product['c_data_product']='Datti di prodotto';
$gl_caption_product['c_data_web']='Vendita Web';
$gl_caption_product['c_stock']='Stock';
$gl_caption_product['c_sells']='Unità vendute';
$gl_caption_product['c_file']='Archivi pubblici';
$gl_caption_product['c_discount_fixed_price']='Disconto prezzo fisso';
$gl_caption_product['c_discount_fixed_price_wholesaler']='Disconto prezzo fisso distributore';
$gl_caption_product['c_selected']='Prodotti selezionati';
$gl_caption_product['c_videoframe']='Video (youtube, metacafe...)';


$gl_caption['c_observations'] = 'Osservazioni';
$gl_caption['c_check_parent_id']='- nessuna famiglia -';
$gl_caption['c_check_brand_id']='Scegli marca';




$gl_caption['c_brand_name']='Nome di marca';
$gl_caption_family['c_parent_id']='Famiglia a qui appartiene';
$gl_caption_family['c_family']='Nome di la famiglia';
$gl_caption_family['c_family_alt']='Famiglia "title/alt"';
$gl_caption_family['c_family_subtitle'] = 'Subtítulo';
$gl_caption_family['c_view_subfamilys']='vedere famiglie';
$gl_caption_family['c_add_subfamily_button']=$gl_caption_family['c_add_subfamily']='Aggiungere sottofamiglia';
$gl_caption_family['c_filter_parent_id'] = 'Appartiene a qualsiasi famiglia';
$gl_caption_family['c_search_title'] = 'Cerca famiglie';




$gl_caption_tax['c_tax_name']='Nome della tassa';
$gl_caption_tax['c_tax_value']='Valore (%)';
$gl_caption_tax['c_equivalencia']='Equivalenza (%)';




$gl_caption_product['c_search_title']= 'Cerca di prodotti';
$gl_caption_product['c_by_nif']= 'Per Rif.';
$gl_caption_product['c_by_words']= 'Per parole.';


$gl_caption['c_observations'] = 'Osservazioni';
$gl_caption['c_ordre']='Ordine';
$gl_caption['c_withinstall']='Pvp con la installazione';

$gl_caption ['c_noproduct']= $gl_messages_product ['no_records']= 'In questo momento non ci sono prodotti per questa categoria.';
$gl_caption_product['c_model']='Modello';
$gl_caption['c_year']='Anno';
$gl_caption['c_ishandmade']='Fatto a mano';




// No traduit
if(!defined( 'CUSTOMER_MENU_CUSTOMER' )){
	define( 'CUSTOMER_MENU_CUSTOMER', 'Clienti' );
	define( 'PRODUCT_MENU_CUSTOMER', 'Clienti' );
	define( 'CUSTOMER_MENU_CUSTOMER_NEW', 'Inserire cliente' );
	define( 'CUSTOMER_MENU_CUSTOMER_LIST', 'Lista dei clienti' );
	define( 'CUSTOMER_MENU_CUSTOMER_BIN', 'Cestino' );

	define( 'CUSTOMER_MENU_ORDER', 'Orini' );
	define( 'PRODUCT_MENU_ORDER', 'Ordini' );
	define( 'CUSTOMER_MENU_ORDER_NEW', 'Nuovo ordine' );
	define( 'CUSTOMER_MENU_ORDER_LIST', 'Lista degli ordini' );
	define( 'CUSTOMER_MENU_ORDER_LIST_BIN', 'Cestino' );

	define( 'CUSTOMER_MENU_WHOLESALER', 'Distributori' );
	define( 'PRODUCT_MENU_WHOLESALER', 'Distributori' );
	define( 'CUSTOMER_MENU_WHOLESALER_NEW', 'Inserire distributore' );
	define( 'CUSTOMER_MENU_WHOLESALER_LIST', 'Lista dei distributori' );
	define( 'CUSTOMER_MENU_WHOLESALER_BIN', 'Cestino' );

	define( 'CUSTOMER_MENU_RATE', 'Tariffe di spedizione' );
	define( 'PRODUCT_MENU_RATE', 'Tariffe di spedizione' );
	define( 'CUSTOMER_MENU_RATE_NEW', 'Aggiungere paese' );
	define( 'CUSTOMER_MENU_RATE_PROVINCIA_NEW', 'Aggiungere provincia' );
	define( 'CUSTOMER_MENU_RATE_LIST', 'Lista delle tariffe' );
	define( 'PRODUCT_MENU_COUNTRY', 'Country selection' );
	define( 'CUSTOMER_MENU_COUNTRY_LIST', 'Country selection' );

	define( 'CUSTOMER_MENU_COURIER', 'Messaggeri' );
	define( 'PRODUCT_MENU_COURIER', 'Messaggeri' );
	define( 'CUSTOMER_MENU_COURIER_NEW', 'Creare messaggero' );
	define( 'CUSTOMER_MENU_COURIER_LIST', 'Lista dei messaggeri' );
	define( 'CUSTOMER_MENU_COURIER_LIST_BIN', 'Cestino' );
}

$gl_caption ['c_quantity']='Quantità';
$gl_caption ['c_product_title']='Prodotto';
$gl_caption ['c_product_tax']='I.V.A'; // això ha d'anar a la taula de productes
$gl_caption ['c_product_basetax']='Prezzo/unità';
$gl_caption ['c_product_base']='Prezzo/unità';
$gl_caption ['c_product_total_base']='Totale';
$gl_caption ['c_product_total_tax']='I.V.A'; // això ha d'anar a la taula de productes
$gl_caption ['c_product_total_basetax']='Totale';
$gl_caption ['c_total']='Totale';
$gl_caption ['c_total_base']='Sottototale';
$gl_caption ['c_total_tax']='I.V.A'; // això ha d'anar a la taula de productes
$gl_caption ['c_total_equivalencia']='Equivalenza de ricarico';
$gl_caption ['c_total_basetax']='Totale';
$gl_caption ['c_rate_basetax']='Tariffe di spedizione';
$gl_caption ['c_rate_base']='';
$gl_caption ['c_rate_tax']='';
$gl_caption ['c_deposit']='Deposito';
$gl_caption ['c_deposit_rest']='Remaining amount';
$gl_caption ['c_all_basetax']='Totale';
$gl_caption ['c_all_base'] = 'Base imponibile';
$gl_caption ['c_all_tax']='I.V.A';
$gl_caption ['c_all_equivalencia']='Equivalenza de ricarico';
$gl_caption ['c_promcode_basetax']='Disconto codice promozionale';
$gl_caption['c_product_old_base'] = "";
$gl_caption['c_product_old_basetax'] = "";
$gl_caption['c_product_discount'] = "Disconto";
$gl_caption['c_product_discount_fixed'] = "";

$gl_caption_customer['c_prefered_language'] = 'Lingua';
$gl_caption_customer['c_form_title'] = 'Datti di cliente';
$gl_caption_customer['c_customer_id'] = 'Num.';
$gl_caption_customer['c_customer_id_title'] = 'Numero';
$gl_caption_customer['c_treatment'] = 'Trato';
$gl_caption_customer['c_name'] = 'Nome';
$gl_caption_customer['c_company']='Azienda';
$gl_caption_customer['c_surname'] = 'Cognome';
$gl_caption_customer['c_birthdate'] = 'Nascita';
$gl_caption_customer['c_mail'] = "Indirizzo e-mail";
$gl_caption_customer['c_password'] = 'Password';
$gl_caption_customer['c_password_repeat'] = 'Ripetere password';
$gl_caption['c_telephone'] = 'Telefonino';
$gl_caption_customer['c_telephone_mobile'] = 'Cellulare';
$gl_caption['c_has_equivalencia'] = 'Equivalenza de ricarico';
$gl_caption['c_has_no_vat'] = 'Senza IVA';
$gl_caption_customer['c_wholesaler_rate'] = 'Tarifa';
$gl_caption_customer['c_wholesaler_rate_1'] = 'Tarifa 1';
$gl_caption_customer['c_wholesaler_rate_2'] = 'Tarifa 2';
$gl_caption_customer['c_wholesaler_rate_3'] = 'Tarifa 3';
$gl_caption_customer['c_wholesaler_rate_4'] = 'Tarifa 4';
$gl_caption_customer['c_wholesaler_rate_5'] = 'Tarifa 5';
$gl_caption['c_dni'] = 'CIF / NIF';
$gl_caption_customer['c_comment'] = 'Osservazione';
$gl_caption_customer['c_bin'] = '';
$gl_caption_customer['c_enteredc'] = "Giorno di ingresso";
$gl_caption['c_treatment_sra'] = "Sra.";
$gl_caption['c_treatment_sr'] = "Sr.";
$gl_caption ['c_same_address']='Stesso indirizzo di fatturazione';
$gl_caption_customer['c_list_orders'] = 'Vedere ordini';
$gl_caption_customer ['c_form_title2']='Datti di ingresso';
$gl_caption_customer ['c_form_title3']='Datti di fatturazione';
$gl_caption_customer ['c_form_title3_list']='Datti di fatturazione';
$gl_caption_customer ['c_form_title_wholesaler']='Distributore';
$gl_caption_customer ['c_form_title4']='Indirizzo di consegna';
$gl_caption_customer['c_add_button'] = 'Aggiungere indirizzo';

$gl_caption['c_order_id'] = $gl_caption['c_order'] = 'Ordine';
$gl_caption_order['c_customer_id'] = 'Cliente';
$gl_caption_order['c_total_base'] = 'Base imponibile';
$gl_caption_order['c_total_tax'] = 'I.V.A.';
$gl_caption_order['c_total_basetax'] = 'Totale';
$gl_caption_order['c_entered_order'] = 'Data di ordine';
$gl_caption_order['c_entered_order_short'] = 'Data';
$gl_caption_order['c_customer_id'] = 'Cliente';
$gl_caption_order['c_product_id'] = 'Prodotto';
$gl_caption_order['c_shipped'] = 'Data di spedizione';
$gl_caption_order['c_delivered'] = 'Data di consegnamento';
$gl_caption_order['c_order_status'] = 'Stato';
$gl_caption_order['c_deposit_payment_method'] = 'Payment method deposit';
$gl_caption_order_list['c_deposit_payment_method'] = 'Payment m. deposit';
$gl_caption_order['c_payment_method'] = 'Metodo de pagamento';
$gl_caption_order_list['c_payment_method'] = 'Payment m.';
$gl_caption_order['c_comment'] = 'Osservazioni';
$gl_caption['c_customer_comment'] = 'Customer comments';
$gl_caption['c_is_gift_card'] = 'Buono regalo';
$gl_caption_order['c_address_invoice_id'] = '';
$gl_caption_order['c_filter_order_status'] = 'Tutti gli stati';
$gl_caption_order['c_filter_payment_method'] = 'Tutti i metodi';
$gl_caption_order['c_customer_id_format'] = '%s %s';
$gl_caption_order['c_file'] = 'Public files';
$gl_caption_order['c_button-edit-admin'] = '';


$gl_caption_order['c_order_status_paying'] = 'In attesa di pago';
$gl_caption_order['c_order_status_failed'] = 'Pago non riuscito';
$gl_caption_order['c_deposit_payed'] = 'Deposit pagato';
$gl_caption_order['c_order_status_pending'] = 'Pago in attesa di accettazione';
$gl_caption_order['c_order_status_denied'] = 'Pago rifiutato';
$gl_caption_order['c_order_status_voided'] = 'Pago annullato';
$gl_caption_order['c_order_status_refunded'] = 'Pago restituito';
$gl_caption_order['c_order_status_completed'] = 'Pagato';
$gl_caption_order['c_order_status_preparing'] = 'In preparazione';
$gl_caption_order['c_order_status_shipped'] = 'Inviato';
$gl_caption_order['c_order_status_delivered'] = 'Consegnato';
$gl_caption_order['c_order_status_collect'] = 'In attesa di raccogliere';

$gl_caption_order['c_deposit_status'] ='Stato deposit';

$gl_caption_order['c_deposit_status_paying'] = 'In attesa di pago';
$gl_caption_order['c_deposit_status_failed'] = 'Pago non riuscito';
$gl_caption_order['c_deposit_status_deposit'] = 'Deposit pagato';
$gl_caption_order['c_deposit_status_pending'] = 'Pago in attesa di accettazione';
$gl_caption_order['c_deposit_status_denied'] = 'Pago rifiutato';
$gl_caption_order['c_deposit_status_voided'] = 'Pago annullato';
$gl_caption_order['c_deposit_status_refunded'] = 'Pago restituito';
$gl_caption_order['c_deposit_status_completed'] = 'Pagato';

$gl_caption_order['c_deposit_rest'] = 'Resto';

// Enviament
$gl_caption['c_send_date'] = "Consegnato";
$gl_caption['c_send_date_shipped'] = $gl_caption_order['c_shipped'];
$gl_caption['c_send_date_delivered'] = $gl_caption_order['c_delivered'];
$gl_caption['c_send_date_no_date_shipped'] = 'No shipped date';
$gl_caption['c_send_date_no_date_delivered'] = 'No delivered date';

$gl_caption['c_payment_method_none'] = 'Ninguno';
$gl_caption['c_payment_method_paypal'] = 'Paypal';
$gl_caption['c_payment_method_account'] = 'Transferéncia bancária';
$gl_caption['c_payment_method_ondelivery'] = 'Contrareembolso';
$gl_caption['c_payment_method_directdebit'] = 'Bonifico bancario';
$gl_caption['c_payment_method_4b'] = 'Pasarela 4B';
$gl_caption['c_payment_method_lacaixa'] = 'Carta di credito';
$gl_caption['c_payment_method_cash'] = 'Cash';
$gl_caption['c_payment_method_physicaltpv'] = 'Physical TPV';

$gl_caption['c_deposit_payment_method_none'] = 'Ninguno';
$gl_caption['c_deposit_payment_method_paypal'] = $gl_caption['c_payment_method_paypal'];
$gl_caption['c_deposit_payment_method_account'] = $gl_caption['c_payment_method_account'];
$gl_caption['c_deposit_payment_method_ondelivery'] = $gl_caption['c_payment_method_ondelivery'];
$gl_caption['c_deposit_payment_method_directdebit'] = $gl_caption['c_payment_method_directdebit'];
$gl_caption['c_deposit_payment_method_4b'] = $gl_caption['c_payment_method_4b'];
$gl_caption['c_deposit_payment_method_lacaixa'] = $gl_caption['c_payment_method_lacaixa'];
$gl_caption['c_deposit_payment_method_cash'] = $gl_caption['c_payment_method_cash'];
$gl_caption['c_deposit_payment_method_physicaltpv'] = $gl_caption['c_payment_method_physicaltpv'];


$gl_caption['c_collect'] = 'Collect on physical shop';
$gl_caption_list['c_collect'] = 'Collect';
$gl_caption_order['c_edit_button'] = 'Modificare ordine';
$gl_caption_order['c_show_record'] = 'Vedere ordine';
$gl_caption_order['c_show_invoice'] = 'Vedere fattura';
$gl_caption_order['c_show_invoice_refund'] = 'Vedere fattura divoluzione';
$gl_caption_order['c_pay_now'] = 'Pagare';
$gl_caption_order['c_no_shipped'] = 'Non inviaoo';
$gl_caption_order['c_no_delivered'] = 'Non consegnato';


$gl_caption_order ['c_order_title_admin']='Datti dell\'ordine';
$gl_caption_order ['c_order_title']='Informazioni sull\'ordine';
$gl_caption_order ['c_order_title2']='Prodotti';
$gl_caption_order ['c_order_title3']='Fatturazione';
$gl_caption_order ['c_order_title4']='Indirizzo di consegna';
$gl_caption_order ['c_order_title5']='Modalità di pagamento';
$gl_caption_order ['c_order_num']='num. ';
$gl_caption ['c_product_variation_ids']='';
$gl_caption ['c_product_variations']='Variazioni';
$gl_caption ['c_product_variation_categorys']='';

$gl_caption_order['c_invoice_id'] = 'Fattura';
$gl_caption_order['c_invoice_refund_id'] = 'Fattura divoluzione';
$gl_caption_order['c_invoice_date'] = 'Data';
$gl_caption_order['c_invoice_refund_date'] = 'Data';
$gl_caption_order['c_view_invoice'] = 'Vedere fattura';
$gl_caption_order['c_view_invoice_refund'] = 'Vedere fattura divoluzione';
$gl_caption_order ['c_order_title_invoice_customer']='Datti dei cliente';

$gl_caption_orderitem = $gl_caption_order;
$gl_caption_orderitem_list = $gl_caption_order_list;

$gl_caption ['c_address_id']='';
$gl_caption ['c_delivery_id']='';
$gl_caption_address ['c_customer_id']='';
$gl_caption_address['c_a_name'] = 'Nome';
$gl_caption_address['c_a_surname'] = 'Cognome';
$gl_caption_address['c_address'] = 'Indirizzo';
$gl_caption_address['c_zip'] = 'Codice postale';
$gl_caption_address['c_city'] = 'Comune';
$gl_caption_address['c_country_id'] = 'Paese';
$gl_caption_address['c_invoice'] = '';
$gl_caption_address['c_edit_address'] = 'Modificare indirizzo';
$gl_caption_address['c_new_address'] = 'Aggiungere indirizzo';

$gl_caption_variation['c_variation'] = 'Variazione';
$gl_caption_variation['c_variation_subtitle'] = 'Sottotitulo';
$gl_caption_variation['c_variation_description'] = 'Descrizione';
$gl_caption_variation['c_list_pick_default'] = 'Predet.';
$gl_caption_variation['c_variation_category_id'] = 'Categoria';
$gl_caption_variation['c_variation_status'] = 'Status';
$gl_caption['c_disabled'] = 'Disabled';
$gl_caption_variation['c_variation_pvp'] = 'Incremento PVP';
$gl_caption_variation['c_variation_pvd'] = 'Incremento PVD 1';
$gl_caption_variation['c_variation_pvd2'] = 'Incremento PVD 2';
$gl_caption_variation['c_variation_pvd3'] = 'Incremento PVD 3';
$gl_caption_variation['c_variation_pvd4'] = 'Incremento PVD 4';
$gl_caption_variation['c_variation_pvd5'] = 'Incremento PVD 5';
$gl_caption_variation_category['c_variation_category'] = 'Categoria';
$gl_caption_variation_category['c_variation_description'] = 'Descrizione';
$gl_caption_product['c_edit_variations'] = $gl_caption_product['c_edit_variations_button'] = 'Variaciones';
$gl_caption_product['c_edit_specs'] = $gl_caption_product['c_edit_specs_button'] = 'Specifiche';
$gl_caption_product['c_edit_relateds'] = $gl_caption_product['c_edit_relateds_button'] = 'Related';
$gl_caption['c_no_assigned'] = 'Non c\'è nessuna variazione definita';
$gl_caption_product['c_data_variations'] = 'Variazione di prodotto';
$gl_caption_product['c_price_base'] = 'Prezzo base di prodotto';
$gl_caption['c_data_digital'] = 'Prodotto digitale';
$gl_caption_product['c_download'] = 'File scaricabili';
$gl_caption_product['c_filter_family_id'] = 'Tutte le famiglie';
$gl_caption_product['c_filter_subfamily_id'] = 'Tutte le sottofamiglie';
$gl_caption_product['c_filter_status'] = 'Tutti gli statti';
$gl_caption_product['c_filter_destacat'] = 'Featured / Non featured';
$gl_caption_product['c_destacat_0'] = 'Non featured';
$gl_caption_product['c_destacat_1'] = 'Featured';
$gl_caption_product['c_filter_prominent'] = 'Promozione / Non promozione';
$gl_caption_product['c_prominent_0'] = 'Non promozione';
$gl_caption_product['c_prominent_1'] = 'Promozione';
$gl_caption_product['c_stockcontrol']='Controllo dello stock';
$gl_caption_product['c_last_stock_units_public']='Avviso ultime unità pubbliche';
$gl_caption_product['c_last_stock_units_admin']='Avviso ultime unità admin';
$gl_caption_brand['c_brand_description']='Descrizione';
$gl_caption['c_prominent']='Promozione';
$gl_caption_list['c_prominent']='Prom.';
$gl_caption_product['c_is_new_product']='Producto nuevo';
$gl_caption_product_list['c_is_new_product']='Nuevo';
$gl_caption_variation['c_variation_stock']='Stock';
$gl_caption_variation['c_variation_last_stock_units_public']='Avviso pubblico';
$gl_caption_variation['c_variation_last_stock_units_admin']='Avviso admin';
$gl_caption_variation['c_ref']='Rif';
$gl_caption_variation['c_ref_supplier']='Rif Proveedor';
$gl_caption_variation['c_variation_ref']='Rif';
$gl_caption_variation['c_variation_ref_number']='Rif. Automatica';
$gl_caption_variation['c_variation_ref_supplier']='Rif fornitore';

$gl_caption['c_product_file_name'] = $GLOBALS['gl_caption']['c_file_name'];
$gl_caption['c_product_old_file_name'] = $GLOBALS['gl_caption']['c_old_file_name'];

$gl_caption_variation['c_variation_sells']='Unità vendute';
$gl_caption['c_add_variations']='Aggiungere variazioni';
$gl_caption_variation['c_selected']='Variazioni per aggiungere';
$gl_caption['c_new_variations']='Variazioni aggiunti';
$gl_caption['c_variation_category_select']='Tutte le categorie';
$gl_caption_variation['c_filter_variation_category_id']='Tutte le categorie';


$gl_caption['c_send_password'] = 'Invia password per e-mail';
$gl_caption['c_password_sent'] = 'Si a inviato una nuova password per e-mail ';
$gl_caption['c_password_mail_subject'] = 'Sua nuova password';
$gl_caption['c_password_mail_text_1'] = 'Sua nuova password per accedera al nostro negozio è';
$gl_caption['c_password_mail_text_2'] = 'È possibile modificare la password nella sezione "Il tuo account" del nostro sito web';
$gl_messages_customer['password_sent'] = 'Password inviata al cliente';
$gl_messages_customer['password_not_sent'] = 'Impossibile inviare la password, cliccare su "Invia password via e-mail"';

$gl_caption_customer['c_activate_button'] = 'Abilita cliente';
$gl_caption_customer['c_deactivate_button'] = 'Disabilita cliente';
$gl_caption_customer['c_set_wholesaler_button'] = 'Diventare distributore';
$gl_caption_customer['c_unset_wholesaler_button'] = 'Rimuovere distributore';
$gl_caption_customer['c_activated_mail_message'] = 'Il suo conto su %s e stato abilitato, può acceder sul nostro negozio.';
$gl_caption_customer['c_activated_mail_subject'] = 'Il suo conto e stato abilitato';
$gl_caption_customer['c_filter_activated'] = 'Abilitati / Disabilitati';
$gl_caption_customer['c_activated_0'] = 'Disabilitati';
$gl_caption_customer['c_activated_1'] = 'Abilitati';

$gl_messages_customer['activated'] = 'Cliente abilitato';
$gl_messages_customer['deactivated'] = 'Cliente disabilitato';
$gl_messages_customer['not_activated'] = 'No se ha podido activar el cliente, por favor vuelve a intentarlo';
$gl_messages_customer['set_wholesaler'] = 'Il cliente e diventato distributore';
$gl_messages_customer['unset_wholesaler'] = 'Il cliente non è piu distributore';

$gl_caption_family['c_destacat']='Featured';

$gl_caption['c_promcode'] = 'Codice promozionale';
$gl_caption_promcode['c_discount_pvp'] = 'Disconto PVP';
$gl_caption_promcode['c_discount_pvd'] = 'Disconto tarifa 1';
$gl_caption_promcode['c_discount_pvd2'] = 'Disconto tarifa 2';
$gl_caption_promcode['c_discount_pvd3'] = 'Disconto tarifa 3';
$gl_caption_promcode['c_discount_pvd4'] = 'Disconto tarifa 4';
$gl_caption_promcode['c_discount_pvd5'] = 'Disconto tarifa 5';
$gl_caption_promcode['c_discount_type'] = 'Tipo di disconto';
$gl_caption_promcode['c_discount_type_fixed'] = 'Quantità fissa';
$gl_caption_promcode['c_discount_type_percent'] = 'Percentuale';
$gl_caption_promcode['c_minimum_amount'] = 'Spesa minima';
$gl_caption_promcode['c_start_date'] = 'Data di inizio';
$gl_caption_promcode['c_end_date'] = 'Scadenza';
$gl_caption['c_promcode_discount'] = 'Disconto codice promozionale';

$gl_caption['c_sector']='Settore';
$gl_caption['c_sector_id']='Settore';
$gl_caption_sector['c_sector_description']='Descrizione';


$gl_caption_brand['c_send_rate_spain']='Spagna';
$gl_caption_brand['c_send_rate_france']='Francia';
$gl_caption_brand['c_send_rate_andorra']='Andorra';
$gl_caption_brand['c_send_rate_world']='Resto dil mondo';
$gl_caption['c_minimum_buy_free_send']='Spesa minima';

/*  SELLPOINT  */
/*--------------- nomès castellà i català, no traduit a la resta -------------------*/


$gl_caption_sellpoint['c_sellpoint'] = 'Titolo';
$gl_caption_sellpoint['c_sellpoint_description'] = 'Descrizione';
$gl_caption_sellpoint['c_empresa'] = 'Firma';
$gl_caption_sellpoint['c_adress'] = 'Indirizzo';
$gl_caption_sellpoint['c_comunitat_id'] = 'Comunità';
$gl_caption_sellpoint['c_provincia_id'] = 'Provincia';
$gl_caption_sellpoint['c_provincia_id_caption'] = 'Nessuna provincia';
$gl_caption_sellpoint['c_poblacio'] = 'Comune';
$gl_caption_sellpoint['c_cp'] = 'Codice postale';
$gl_caption_sellpoint['c_telefon'] = 'Telefono';
$gl_caption_sellpoint['c_telefon2'] = 'Telefono 2';
$gl_caption_sellpoint['c_fax'] = 'Fax';
$gl_caption_sellpoint['c_correu'] = 'E-mail';
$gl_caption_sellpoint['c_correu2'] = 'E-mail 2';
$gl_caption_sellpoint['c_url1'] = 'Web';
$gl_caption_sellpoint['c_url1_name'] = 'Nome della web';



$gl_caption_sellpoint['c_filter_comarca_id'] = 'Tutte le regione';
$gl_caption_sellpoint['c_filter_municipi_id'] = 'Tutte le comune';
$gl_caption_sellpoint['c_filter_seller_kind'] = 'Tutti i tipi';


// mapes
$gl_caption_sellpoint['c_edit_map'] = 'Posizione';
$gl_caption_sellpoint['c_search_adress']='Cercare indirizzo';
$gl_caption_sellpoint['c_latitude']='latitudine';
$gl_caption_sellpoint['c_longitude']='longitudine';
$gl_caption_sellpoint['c_found_direction']='indirizzo trovata';
$gl_caption_sellpoint['c_save_map'] = 'Guardare posizione';
$gl_caption_sellpoint['c_click_map'] = 'Si prega di cliccare su un punto sulla mappa per impostare la posizione';
$gl_caption_sellpoint['c_form_map_title'] = 'Posizione';
$gl_messages['point_saved'] = 'Nuova posizione salvata correttamente';

$gl_caption_sellpoint['c_prepare'] = 'Preparazione';
$gl_caption_sellpoint['c_review'] = 'Per rivedere';
$gl_caption_sellpoint['c_public'] = 'Pubblico';
$gl_caption_sellpoint['c_archived'] = 'Archiviato';
$gl_caption_sellpoint['c_status'] = 'Status';

$gl_caption_sellpoint['c_international'] = 'Internazionale';
$gl_caption_sellpoint['c_country_id'] = 'Paese';
$gl_caption_sellpoint['c_seller_kind'] = 'Tipo';
$gl_caption_sellpoint['c_seller_kind_shop'] = 'Negozio';
$gl_caption_sellpoint['c_seller_kind_shops'] = 'Negozi';
$gl_caption_sellpoint['c_seller_kind_dealer'] = 'Distributore';
$gl_caption_sellpoint['c_seller_kind_dealers'] = 'Distributori';

$gl_caption_sellpoint['c_sellpoint_file_name'] = $GLOBALS['gl_caption']['c_file_name'];
$gl_caption_sellpoint['c_sellpoint_old_file_name'] = $GLOBALS['gl_caption']['c_old_file_name'];

$gl_caption_sellpoint['c_filter_provincia_id'] = 'Tutte le provincie';

// tarifes
$gl_caption['c_country_id'] = $gl_caption['c_country'] = 'Paese';
$gl_caption['c_provincia_id'] = $gl_caption['c_provincia'] = 'Provincia';
$gl_caption_rate['c_rate'] = 'Tassa di spedizione';
$gl_caption_rate['c_send_rate_world']='Altri paese';
$gl_caption_rate['c_rate_title']='Tasse';
$gl_caption_rate['c_tax_included'] = 'IVA compreso';
$gl_caption_rate['c_tax_not_included'] = 'IVA non compreso';
$gl_caption_rate['c_rate_encrease_ondelivery'] = $gl_caption_rate['c_send_rate_encrease_ondelivery_world'] = 'Aumento contrareembolso';

// Especificacions
$gl_caption['c_spec_category_id'] = $gl_caption['c_spec_category'] =  'Categoria';
$gl_caption['c_spec_type'] = 'Tipo';
$gl_caption['c_ordre'] = 'Ordine';
$gl_caption['c_spec'] = 'Specificazione';
$gl_caption['c_spec_unit'] = 'Unità';
$gl_caption['c_filter_spec_category_id'] = 'Tutte le categorie';


$gl_caption['c_spec_type_checkbox']     = 'Checkbox';
$gl_caption['c_spec_type_int']          = 'Nome';
$gl_caption['c_spec_type_text']         = 'Testo in lingue';
$gl_caption['c_spec_type_text_no_lang'] = 'Testo';
$gl_caption['c_spec_type_textarea']     = 'Testo multilinea';
$gl_caption['c_spec_type_select']       = 'Discesa';

$gl_caption['c_data_specs']       = 'Specificazione di prodotto';

// Couriers
$gl_caption['c_courier'] = $gl_caption['c_courier_id'] = "Messaggero";
$gl_caption['c_courier_url'] = "Web";
$gl_caption['c_courier_follow_code'] = "Codice di monitoraggio";
$gl_caption['c_default_courier'] = "Imposta come predefinito";
$gl_caption['c_default_courier_0'] = "";
$gl_caption['c_default_courier_1'] = "Default";

// Enviar emails segons/  estat
// admin
$gl_caption['prepared_emailed'] =
$gl_caption['delivered_emailed'] =
$gl_caption['shipped_emailed'] = '';
$gl_caption['c_prepared_emailed'] =
$gl_caption['c_delivered_emailed'] =
$gl_caption['c_shipped_emailed'] = '';
$gl_caption['c_send_mail_prepared'] = "Inviare email ordine pronta";
$gl_caption['c_send_mail_shipped'] = "Inviare email ordine inviata";
$gl_caption['c_send_mail_delivered'] = "Inviare email ordine consegnata";
$gl_caption['c_resend_mail'] = "Inviare di nuovo il mail";
$gl_caption['c_mail_sent'] = "Email inivato le";
$gl_caption['c_mail_send_error'] = "Impossibile inviare il messaggio, si prega di riprovare";
$gl_caption['c_sending_email'] = "Email in processo...";
$gl_caption['c_email_sended_correctly'] = "E-mail inviata correttamente";

// públic
$gl_caption['c_mail_subject_buyer_status_prepared'] = 'Votre ordine à %1$s è pronta';
$gl_caption['c_mail_subject_buyer_status_shipped'] = 'Votre ordine à %1$s è stata inviata';
$gl_caption['c_mail_subject_buyer_status_delivered'] = 'Votre ordine à %1$s è stata consegnata';
$gl_caption['c_mail_subject_seller_status_prepared'] = 'Copia di ordine pronta';
$gl_caption['c_mail_subject_seller_status_shipped'] = 'Copia di ordine inviata';
$gl_caption['c_mail_subject_seller_status_delivered'] = 'Copia di ordine consegnata';

$gl_caption['c_mail_body_prepared'] = '
	<p>Caro/a %1$s,</p>
	<p>Siamo grati per la tua fiducia in noi per i tuoi acquisti.</p>
	<p>Stiamo preparando il tuo ordine con il riferimento: <strong>%2$s</strong>. Presto faremo la spedizione corrispondente. Controlli le seguenti informazioni e contattaci se si notano errori.</p>
	<p>In qualsiasi momento puoi controllare l\'ordine accedendo <a href="%3$s">il vostro conto</a> sulla nostra web. Puoi anche contattarci tramite la seguente email:</p>
	<p><a href="mailto:%4$s">%4$s</a></p>
	<p>Grazie per aver acquistato a %5$s!</p>';

$gl_caption['c_mail_body_shipped'] = '
	<p>Caro/a %1$s,</p>
	<p>Ti informiamo che il tuo ordine <strong>%2$s</strong> è stata inviata.</p>';

$gl_caption['c_mail_body_shipped_2'] = '
	<p>La compagnia di spedizioni responsabile dell\'invio del tuo ordine è <a href="%3$s"><strong>%4$s</strong></a> e il numero di tracking è
		<strong>%5$s</strong>.</p >';

$gl_caption['c_mail_body_shipped_3'] = '
	<p>Puoi controllare il tuo ordine in "ordini" a "<a href="%6$s">Il mio conto</a>".</p >
	<p>Grazie per aver acquistato a %7$s!</p >';

$gl_caption['c_mail_body_delivered'] = '
	<p>Caro/a %1$s,</p>
	<p>Il vostro ordine <strong>%2$s</strong> è già stato consegnato all\'indirizzo indicato.</p>
	<p>Speriamo che tu sia completamente soddisfatto del tuo acquisto. Se per qualche ragione questo non è il caso, saremmo grati se ci avresti dato l\'opportunità di risolvere le tue preoccupazioni rispondendo direttamente a questa email:</p>
	<p><a href="mailto:%3$s">%3$s</a></p>
	<p>Grazie per aver acquistato a %4$s!</p>';


// Relacionats
$gl_caption['c_data_related']       = 'Prodotti correlati';
$gl_caption['c_product_relateds'] = "Prodotti correlati";
$gl_caption['c_select_related_products'] = "Ricerca e seleziona prodotti correlati";
$gl_caption_product_related['c_image'] = "";

// Quantitat màxima
$gl_caption['c_max_quantity'] = "Quantità massima per ordine";
$gl_caption_list['c_max_quantity'] = "Max";
$gl_caption['c_max_quantity_abbr'] = "Max.";
$gl_caption['c_min_quantity'] = "Quantità minima per ordine";
$gl_caption_list['c_min_quantity'] = "Min";
$gl_caption['c_min_quantity_abbr'] = "Min.";
$gl_caption['c_quantityconcept_id'] = "Concetto";
$gl_caption['c_quantityconcept'] = "Concetto";

// Calendari
$gl_caption['c_edit_calendar'] = "Calendario";
$gl_caption['c_has_calendar'] = "Attivare calendario";
$gl_caption['c_is_calendar_public'] = "Públic";
$gl_caption['c_familybusy_id'] = "";
$gl_caption['c_date_busy'] = "Giorno";
$gl_caption['c_familybusy_entered'] = "Data creazione";
$gl_caption['c_form_calendar_title'] = "Datte occupate";
$gl_caption['c_calendar_not_saved_error'] = "Impossibile salvare la data, si prega di provare di nuovo";
$gl_caption['c_calendar_saved_occupied_message'] = "Il giorno è stato ocupato";
$gl_caption['c_calendar_saved_freed_message'] = "Il giorno è stato disocupato";

$gl_caption['c_choose_date'] = "Un giorno dev'essere scelto";

// Famílies privades
$gl_caption['c_is_family_private'] = "Privada";
$gl_caption['c_pass_code'] = "Codi d'accés";

$gl_caption['c_pass_code_error'] = "The entered code is wrong";
$gl_caption['c_pass_code_success'] = "The code is correct";

// Cart
$gl_caption['c_product_date'] = "Giorno";

// Cercador comandes
$gl_caption['c_dates_from'] = 'From';
$gl_caption['c_dates_to'] = 'to';
$gl_caption['c_caption_filter_order_status'] = 'Status';
$gl_caption['c_order_status'] = 'Status';
$gl_caption['c_order_status_group_payed'] = 'Payed';
$gl_caption['c_order_status_group_not_payed'] = 'To pay';
$gl_caption['c_order_status_group_payed_deposit'] = 'Deposit payed';
$gl_caption['c_order_status_group_not_payed_deposit'] = 'Deposit to pay';
$gl_caption['c_view_format'] = "Show";
$gl_caption['c_view_format_all'] = "All";

$gl_caption['c_view_format_order'] = "Orders";
$gl_caption['c_view_format_orderitems'] = "Products";

$gl_caption['c_view_format_title'] = "Tipus de llistat";