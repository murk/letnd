<?

header("Content-Type: text/html; charset=utf-8");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="<?=$_GET['dir_templates']?>/styles/general.css" rel="stylesheet" type="text/css">
   <script type="text/javascript"> 
document.write( '<script src="http://maps.google.com/maps?file=api&v=2&key=' + 
      parent.gl_google_key + 
      '" type="text/javascript"><\/script>' ); 
    //<![CDATA[	
function mostra_mapa() {
      if (GBrowserIsCompatible()) {
//inicialitzo el mapa
var map = new GMap2(document.getElementById("map"));

//Permetro veure mapa amb tipus terreny
/*var crossLayer = new GTileLayer(new GCopyrightCollection(""), 0, 15);
crossLayer.getTileUrl =  function(tile, zoom) {
return "./include/tile_crosshairs.png";
}
var layerTerCross = [ G_PHYSICAL_MAP.getTileLayers()[0], crossLayer ];
var mtTerCross = new GMapType(layerTerCross,G_PHYSICAL_MAP.getProjection(), "Terreny");
map.addMapType(mtTerCross);*/

//permet modificar l'escala del mapa
//map.addControl(new GLargeMapControl())  //escala gran
map.addControl(new GSmallMapControl()); //escala petita
//permetre fer zoom amb el mouse
map.enableScrollWheelZoom();
//Fixo el mapa
map.setCenter(new GLatLng(parent.gl_google_center_latitude,parent.gl_google_center_longitude), parent.gl_zoom);
map.addControl(new GMapTypeControl());
var point = new GLatLng(parent.gl_google_latitude,parent.gl_google_longitude);		
//Marca finestreta		
var marker = new GMarker(point);
map.addOverlay(marker);	
marker.openInfoWindowHtml(parent.gl_google_info_html); 
      }
    }
	window.onload = mostra_mapa;
    //]]>	
    </script>
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}
-->
</style>
</head>
<body>
<div id="map"></div>
</body>
</html>