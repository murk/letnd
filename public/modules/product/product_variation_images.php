<?

/**
 * ProductVariationImages
 *
 * Funcions per visualitzar imatges per variacions
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2018
 * @version 1
 * @access public
 */
class ProductVariationImages extends Module {

	/**
	 * @param $variations
	 *
	 * @return array
	 */
	static function separate_image_variations( &$variations, $images ) {

		if (!$images) return [
			[],
			[],
			[]
		];

		$images = self::order_by_product_variation( $images );

		$image_variations = $no_image_variations = [];

		if ($variations) {
			foreach ( $variations as &$variation ) {
				$variation_ids = &$variation['variation_ids'];
				$variation_base_price = $variation['variation_base_price'];
				$variation_category_id = $variation['variation_category_id'];
				$has_images    = false;
				foreach ( $variation_ids as &$variation_id ) {
					$product_variation_id = $variation_id['product_variation_id'];


					$product_variation_image = self::get_product_variation_image( $product_variation_id, $images );


					if ( $product_variation_image ) {
						$has_images = true;
						$variation_id['variation_has_images'] = true;
						$variation_id['variation_on_click'] = " onclick='product.variation_set_images($variation_base_price,$variation_category_id, $product_variation_id )'";
						foreach ( $product_variation_image as $k => $v ) {
							$variation_id["product_variation_$k"] = $v;
						}
					}
					else {
						$variation_id['variation_has_images'] = false;
						$variation_id['variation_on_click'] = " onclick='product_set_price($variation_base_price,$variation_category_id, $product_variation_id )'";
					}
					if ( $variation_id['variation_nostock'] ) {
						$variation_id['variation_on_click'] = '';
					}
				}

				$variation['variation_category_has_images'] =  $has_images;

				if ( $has_images ) {
					$image_variations [] = $variation;
				}
				else {
					$no_image_variations [] = $variation;
				}
			}
		}

		$variations_images = self::get_variations_images( $images );

		return [
			$image_variations,
			$no_image_variations,
			$variations_images
		];
	}

	public static function get_product_variation_image( $product_variation_id, $images ) {
		if ( isset( $images[ $product_variation_id ] ) ) return $images[ $product_variation_id ][0];
		else return [];
	}

	private static function order_by_product_variation( $images_original ) {

		$images = [];

		foreach ( $images_original as $image ) {
			$query                = "
				SELECT product_variation_id 
				FROM product__product_image
				WHERE image_id = ${image['image_id']}";
			$product_variation_id = Db::get_first( $query );

			if ( ! $product_variation_id ) $product_variation_id = 0;

			if ( empty( $images[ $product_variation_id ] ) ) $images[ $product_variation_id ] = [];

			$images[ $product_variation_id ][] = $image;

		}

		$product_variation_ids = implode(',',array_keys($images));

		if ($product_variation_ids == '0') return $images;

		// Les poso amb el mateix ordre que surten al desplegable
		$images_ordered = [];
		$query = "
					SELECT product_variation_id FROM
						product__product_to_variation
						INNER JOIN product__variation USING (variation_id)
						INNER JOIN product__variation_language USING(variation_id)
					WHERE
					  product__product_to_variation.variation_id = product__variation.variation_id
					  AND BIN = 0
					  AND language = 'spa'
					  AND product_variation_id IN ($product_variation_ids)
					ORDER BY
					  variation_category_id ASC,
					  product__variation.ordre ASC,
					  product__variation_language.variation ASC
					  ";
		$results = Db::get_rows($query);

		foreach ( $results as $rs ) {
			$product_variation_id = $rs['product_variation_id'];
			$images_ordered[ $product_variation_id ] = $images[ $product_variation_id ];
		}

		return $images_ordered;
	}

	// Poso totes les imatges agrupades per variacions
	private static function get_variations_images( $images ) {
		$variation_images = [];
		foreach ( $images as $k => $v ) {

			$query                = "
				SELECT variation_default 
				FROM product__product_to_variation
				WHERE product_variation_id = $k";
			$variation_default = Db::get_first( $query );
			$is_default_variation = $variation_default == '1';

			$image_conta = 0;
			foreach ( $v as &$i ) {
				$i['image_conta'] = $image_conta++;
			}

			$variation_image = [
				'product_variation_id' => $k,
				'is_default_variation' => $is_default_variation,
				'variation_images'     => $v
			];

			if ($is_default_variation) {
				array_unshift ($variation_images, $variation_image);
			}
			else {
				$variation_images []= $variation_image;
			}
		}

		$variations_conta = 0;
		foreach ( $variation_images as &$image ) {
			$image['variations_conta'] = $variations_conta++;
		}

		return $variation_images;
	}

}