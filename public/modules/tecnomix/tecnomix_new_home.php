<?
/**
 * TecnomixNewHome
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2010
 * @version $Id$
 * @access public
 */
class TecnomixNewHome extends Module{

	function __construct(){
		parent::__construct();
		$this->parent = 'TecnomixNew';
	}
	/* funció que es crida per fer una acció del modul automatica o desde un altre modul ( list_records, show_form ) etc...*/
	function do_action($action = ''){
		switch ($this->parent){		
			case 'TecnomixNew':
			break;	
			case 'TecnomixInstalacio':	
			break;	
			case 'TecnomixEquip':
			break;	
			case 'TecnomixProjecte':
			break;
		}
		return parent::do_action($action);
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function get_records()
	{	    	
		$arxiu = CLIENT_DIR.'/templates/tecnomix/new_home.tpl';
		$this->set_news_fields();
		$listing = new ListRecords($this);
		$listing->condition = "parent_module = '" . $this->parent . "'";
		$listing->condition .= " AND status = 'public' AND in_home ='1'";
		
		$category_id = R::id('category_id');
		if ($category_id) $listing->condition .= " AND category_id = '" . $category_id . "'";
		
		$listing->call_function['content'] = 'add_dots';
		$listing->call('records_walk','url1,url1_name,url2,url2_name');
		$listing->order_by = 'entered DESC';
		if (file_exists($arxiu)) {
			$listing->template = 'tecnomix/new_home';
		}				
		return $listing->list_records();
	}
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{
	    $this->set_news_fields();
		$show = new ShowForm($this);
		$show->condition = "status = 'public'";
		$show->call('records_walk','url1,url1_name,url2,url2_name');
		$show->get_values();
		if ($show->has_results)
		{
			return $show->show_form();
		}
		else
		{
			$this->do_action('list_records');
		}	
	}
	function records_walk($url1,$url1_name,$url2,$url2_name){		
		// Això ho poso aquí així no cal posar-ho al tpl, simplement posar: <a href="<=$url1>" target="_blank"><=$url1_name></a>
		$ret['url1_name'] = $url1_name?$url1_name:$url1;
		$ret['url2_name'] = $url2_name?$url2_name:$url2;
			
		//$ret['url1']=str_replace('http://','',$url1);	
		//$ret['url1']=str_replace('http://','',$url1);
		return $ret;
	}
	function set_news_fields(){	
		global $gl_config;
		if (!$gl_config['show_entered_time'])
		{
			$this->set_field('entered', 'type', 'date');
		}
	}
}	
?>