<?php
/* 20-11-18
Banner, configuració dels camps

*/

if ( is_table( 'web__configadmin_banner' ) && ! Db::get_first( "SELECT * FROM web__configadmin_banner WHERE name='banner_text_type'" ) ) {
	$update_sql[] = "
		INSERT INTO web__configadmin_banner (`name`, `value`) VALUES ('banner_text_type', 'html');";

} else {
	if ( is_table( 'web__configadmin_banner' ) ) {
		trigger_error( 'Ja existeixen el camp banner_text_type' );
	}
}
if ( is_table( 'web__configadmin_banner' ) && ! Db::get_first( "SELECT * FROM web__configadmin_banner WHERE name='banner_text2_type'" ) ) {
	$update_sql[] = "
		INSERT INTO web__configadmin_banner (`name`, `value`) VALUES ('banner_text2_type', 'html');";

} else {
	if ( is_table( 'web__configadmin_banner' ) ) {
		trigger_error( 'Ja existeixen el camp banner_text2_type' );
	}
}