<?
/**
 * InmoProperty
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 17/10/2013
 * @version $Id$
 * @access public
 */
class InmoProperty extends Module{
	
	// municipi_id no hi es perque depend del client si vol mostrar-ho o no
	var	$details = array('room', 'bathroom', 'wc', 'furniture', 'laundry', 'parking', 'garage','courtyard', 'storage', 'balcony', 'terrace', 'elevator', 'garden', 'centralheating', 'citygas', 'airconditioned', 'swimmingpool','living','dinning','service_room','service_bath','study','parket','ceramic','gres','sea_view','clear_view','laminated','person','custom1','custom2','custom3','custom4','efficiency'
			);
	
	var	$main_details = array('floor_space', 'land', 'category_id', 'zone_id','tipus' 
			);
	var $is_bookmark = false;
	var $id = false;
	var $tipus_value = false; // hoo utilitzo en el buscador si es fora del formulari, com a easybrava

	function __construct(){
		parent::__construct();
		
		// compatibilitat enllaços vells
		if ($this->action=='show_record_search_search') $this->action='show_record';
	}
	function on_load(){
		
		if (isset($_GET['filecat_file_name'])) $_GET['category_id'] = $_GET['filecat_file_name'];
		
		$this->set_friendly_params( array(
			'category_id'=>R::number('category_id'),
			'tipus'=>R::text_id('tipus'),
			'place'=>R::text_id('place'),
			'comarca_id'=>R::number('comarca_id'),
			'municipi_id'=>R::number('municipi_id'),			
			'zone_id'=>R::number('zone_id'),		
			'offer'=>R::number('offer'),		
			'prices'=>R::text_id('prices'),		
			'room'=>R::text_id('room'),		
			'person'=>R::text_id('person'),		
			'block'=>R::text_id('block')			
		));
		
		parent::on_load();
		
	}
	function save_bookmark()
	{
		$property_id = R::id('property_id');
		
		if ($_COOKIE['inmo']['bookmarks']){
			$bookmarks = unserialize($_COOKIE['inmo']['bookmarks']);
		}
		else{
			$bookmarks = array();			
		}
		
		
		if(in_array($property_id, $bookmarks)){			
			foreach ($bookmarks as $k => $v){
				if ($v==$property_id) unset($bookmarks[$k]);
			}
			$js = 'top.$("#bookmark-'.$property_id.'").removeClass("active")';
		}
		else{
			$bookmarks []= $property_id;
			$js = 'top.$("#bookmark-'.$property_id.'").addClass("active")';
		}
		
		
		
		setcookie("inmo[bookmarks]", serialize($bookmarks), time() + (86400 * 30), '/'); // 30 dies
		
		
		print_javascript($js);
		
		Debug::p_all();
		die();
		
		//$message = 'bookmark_added';
		//$GLOBALS['gl_page']->show_message($message);
	}
	function bookmark()
	{
		$this->is_bookmark = true;
		$GLOBALS['gl_content'] = $this->do_action('get_records');
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function get_records()
	{		
		
		global $gl_page;
		$tipus = false;

		include_once(DOCUMENT_ROOT . 'admin/modules/inmo/inmo_search_functions.php');
		
		set_custom_config($this->config, $this->fields, $this->language_fields, $this->caption);
		
		$this->set_var('is_bookmark',$this->is_bookmark);
		
		$listing = new ListRecords($this);


		// per poder previsualitzar desde admin i enviar desde newsletter
		if (isset($_GET['loginnewsletter']) 
				&& $_GET['loginnewsletter'] == 'letndmarc'
				&& $_GET['passwordnewsletter'] == '5356aopfqkiios5cnoqws90se'
		)
		{			
			$listing->set_var('is_newsletter', true);
			$listing->condition = "bin=0"; // per estalviar haver de mirar si hi ha condició
			if (is_file(PATH_TEMPLATES . 'newsletter/property_list.tpl')) 
					$listing->template = 'newsletter/property_list';

		} else
		{		
			$listing->set_var('is_newsletter', false);

			if ( show_preview() ) {
				$listing->condition = "(status <> 'archived')";
			}
			else {
				$listing->condition = "(status = 'onsale' OR status = 'sold' OR status = 'reserved')";
			}

		}



		$listing->call_function['description'] = 'add_dots';
		$listing->call("list_records_walk", "property_id,ref,category_id,price_consult,price,status,land,land_units,tipus,entered");
		
		if ($this->config['default_order']){
			$listing->order_by = $this->config['default_order'];
		}
		else{
			$listing->order_by = 'ref desc, entered desc';	
		}
		
		
		switch ($this->process)
		{
			case 'list_records_search_search':	
				$listing->condition .=  " AND " . search_search_condition($listing);
				break;
		} // switch


		
		
		if ($this->parent == 'Block' || $this->parent == 'inmo__tv'){
			return $listing;
		}		
		// consultes menus automatiques segons block
		elseif (get_block_vars()) {
			include (get_block_vars());
		}
		elseif (!isset($_GET['loginnewsletter'])) {

			if (!empty($this->config['always_parse_template']))
				$listing->always_parse_template = $this->config['always_parse_template'];
			
			// per ordenar per municipi
			if (R::escape('orderby') == 'municipi-asc' || R::escape('orderby') == 'municipi-desc') {
				$listing->add_field('municipi', 
						array(
							'change_field_name'=> '(SELECT municipi FROM inmo__municipi WHERE inmo__property.municipi_id = inmo__municipi.municipi_id) AS municipi',
							'list_public' => '0'
						));
			}

			// consultes menus
			if (R::id('offer') == 1)
			{
				$listing->condition .= " AND offer = 1";
				$gl_page->title = $GLOBALS['gl_caption']['c_offers'];
			}

			$q = R::escape('q');
			if ($q && $this->process!='list_records_search_search')
			{	
				if ($listing->condition) $listing->condition .= " AND ";
				$listing->condition .= search_search_condition($listing);
			}

			$ref = R::escape('ref');
			if ($ref)
			{
				$ref_array = explode (",", $ref); // si l'array hem dona 1 probo de fer-ho amb espais, si es una sola referncia donarÃ  el mateix resultat
				
				 if (INMO_PROPERTY_AUTO_REF == 1) 
					 count($ref_array) == 1?$ref_array = explode (" ", $ref):false;
				
				
				
				$listing->condition .= " AND ref IN ('" . implode("','", $ref_array) . "')";
				$gl_page->title = TITLE_COMARCA . $ref;
			}

			$comarca_id = R::number('comarca_id');
			if ($comarca_id)
			{
				$listing->condition .= " AND comarca_id = " . $comarca_id;
				$gl_page->title = TITLE_COMARCA . get_comarca($comarca_id);
			}

			$municipi_id = R::number('municipi_id');
			if ($municipi_id)
			{
				$listing->condition .= " AND municipi_id = " . $municipi_id;
				$gl_page->title = TITLE_COMARCA . get_municipi($municipi_id);
			}

			$category_id = R::number('category_id');
			if ($category_id)
			{
				$listing->condition .= " AND category_id = " . $category_id;
				$gl_page->title = get_category($category_id);
			}

			$user_id = R::number('agent_id');
			if ($user_id)
			{
				$listing->condition .= " AND user_id = " . $user_id;
				$gl_page->title = get_user($user_id);
			}

			$language_id = R::number('language_id');
			if ($language_id)
			{
				$listing->condition .= "AND user_id IN ( SELECT user_id FROM user__user_to_language WHERE language_id = $language_id)";
				$gl_page->title = get_property_language($language_id);
			}

			$tipus = R::text_id('tipus');
			
			if($this->config['default_tipus'] && !$tipus) $tipus = $this->config['default_tipus'];
				
			if ($tipus)
			{
				$tipus_c = explode('-',$tipus);
				
				if (is_array($tipus_c)) $tipus_c = "tipus = '" . implode("' OR tipus = '" , $tipus_c) . "'"; 
				else $tipus_c = " tipus='" . $tipus_c . "'";
				
				$listing->condition .= " AND (" . $tipus_c . ")";
				
				if ($tipus == 'temp' && ($listing->order_by == 'price ASC' || $listing->order_by == 'price DESC' )) {
					$listing->order_by = str_replace('price ', 'price_tmp ', $listing->order_by);
					// per ordenar preus a temp
					if (R::escape('orderby')) {
						$listing->override_order_by_field = 'price_tmp';
					}
				}

				// Quan es una per defecte, no poso títol, ja agafa el del page
				if ( R::text_id( 'tipus' ) ) {
					if (isset($this->caption["c_$tipus"])) $gl_page->title = $this->caption["c_$tipus"];
				}
				
			}

			$place_id = R::text_id('place_id');
			if (!$place_id) $place_id = R::text_id('place');
			if ($place_id)
			{
				$listing->condition .= " AND place_id='" . $place_id . "'";
			}

			$zone_id = R::text_id('zone_id');
			if ($zone_id)
			{
				$listing->condition .= " AND zone_id='" . $zone_id . "'";
			}

			$floorspace1 = R::number('floorspace1');
			$floorspace2 = R::number('floorspace2');
			$floorspaces = R::text_id('floorspaces');

			if ($floorspaces){
				$floorspaces = explode('-',$floorspaces);
				$floorspace1 = $floorspaces[0];
				$floorspace2 = $floorspaces[1];
			}

			if ($floorspace1!==false)
			{
				// No te sentit ordenar així quan hi ha varis filtres
				// $listing->order_by = 'floor_space asc, ref desc, entered desc';
				if ($floorspace2)
				{
					$listing->condition .= " AND (floor_space BETWEEN " . $floorspace1 . " AND " . $floorspace2 . ")";
					if ($floorspace1 == 0)
					{
						$listing->condition .= " AND (floor_space <>0)";
						$gl_page->title = sprintf(TITLE_FLOORSPACE, $floorspace2);
					}
					else
					{
						$gl_page->title = sprintf(TITLE_FLOORSPACE_2, $floorspace1, $floorspace2);
					}
				}
				else
				{
					$listing->condition .= " AND floor_space > " . $floorspace1;
					$gl_page->title = sprintf(TITLE_FLOORSPACE_3, $floorspace1);
				}
			}

			// Persones
			$person1 = false;
			$person2 = false;
			$persons = R::text_id('persons');

			if ($persons){
				$persons = explode('-',$persons);
				$person1 = $persons[0];
				$person2 = $persons[1];
			}

			if ($person1!==false)
			{
				if ($person2)
				{
					$listing->condition .= " AND (person BETWEEN " . $person1 . " AND " . $person2 . ")";
					if ($person1 == 0)
					{
						$listing->condition .= " AND (person <>0)";
						$gl_page->title = sprintf(TITLE_PERSON, $person2);
					}
					else
					{
						$gl_page->title = sprintf(TITLE_PERSON_2, $person1, $person2);
					}
				}
				else
				{
					$listing->condition .= " AND person > " . $person1;
					$gl_page->title = sprintf(TITLE_PERSON_3, $person1);
				}
			}
			
			$listing->condition .= search_details_condition($listing, $this->details);
			$listing->condition .= search_book_condition($listing);
			$listing->condition .= search_price_condition($listing, $tipus); // ultima perque necessito descartar tot lo possible --- em podria carregar el punt
					
			
			if ($this->is_bookmark){
				if ($_COOKIE['inmo']['bookmarks']){
					$bookmarks = unserialize($_COOKIE['inmo']['bookmarks']);
				}
				else{
					$bookmarks = array();			
				}

				R::escape_array($bookmarks);
				$bookmarks = implode("','",$bookmarks);
				$listing->condition .= " AND property_id IN ('" . $bookmarks . "')";
			}

			// FILTRES

			// tipus filtre
			if (isset($_GET['tipuss']))  {
				R::escape_array($_GET['tipuss']);
				$listing->condition .= " AND (
				tipus IN ( '".implode("','",$_GET['tipuss'])."' )
				)
				";
			}
			// category_id filtre
			if (isset($_GET['category_ids']))  {
				R::escape_array($_GET['category_ids']);
				$listing->condition .= " AND (
				category_id IN ( ".implode(',',$_GET['category_ids'])." )
				)
				";
			}
			// room filtre
			if (isset($_GET['rooms']))  {
				R::escape_array($_GET['rooms']);
				$listing->condition .= " AND (
				room IN ( ".implode(',',$_GET['rooms'])." )
				)
				";
			}
			// municipi_id filtre
			if (isset($_GET['municipi_ids']))  {
				R::escape_array($_GET['municipi_ids']);
				$listing->condition .= " AND (
				municipi_id IN ( ".implode(',',$_GET['municipi_ids'])." )
				)
				";
			}
			// zone_id filtre
			if (isset($_GET['zone_ids']))  {
				R::escape_array($_GET['zone_ids']);
				$listing->condition .= " AND (
				zone_id IN ( ".implode(',',$_GET['zone_ids'])." )
				)
				";
			}
			// place_id filtre
			if (isset($_GET['place_ids']))  {
				R::escape_array($_GET['place_ids']);
				$listing->condition .= " AND (
				place_id IN ( ".implode(',',$_GET['place_ids'])." )
				)
				";
			}
		}

		// actualitzo tots els preus tmp abans de fer el llistat
		if ($tipus == 'temp') {
			$this->update_all_prices($listing->get_query());
		}

	    return $listing->list_records();
	}

	function update_all_prices($query) {
		Main::load_class('booking', 'common', 'admin');

		$query = $query['q'];
		$results = Db::get_rows( $query );

		// TODO - Posar una cache per no calcular tantes vegades el preu
		foreach ( $results as $rs ) {
			$ret = BookingCommon::get_property_price( $rs['property_id'], $rs['price'], false, true );
		}
	}

	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{	    
		
		 global $gl_page;
		 
		set_custom_config($this->config, $this->fields, $this->language_fields, $this->caption);
		
		$show = new ShowForm($this);

		// visualitzar desde MLS
		if (isset($_GET['mls'])){
			$gl_page->template = '';
			$this->tpl->path = DOCUMENT_ROOT . 'public/themes/common/';
		}

		// per poder previsualitzar desde admin i enviar desde newsletter
		if (isset($_GET['loginnewsletter']) 
				&& $_GET['loginnewsletter'] == 'letndmarc' 
				&& $_GET['passwordnewsletter'] == '5356aopfqkiios5cnoqws90se')
		{
			$show->set_var('is_newsletter', true);
			if (is_file(PATH_TEMPLATES . 'newsletter/property_form.tpl')) 
				$show->template = 'newsletter/property_form';			
		} else {
			$show->set_var( 'is_newsletter', false );

			if ( show_preview() ) {
				// Al formulari es veuen tots, arxivades també
			}
			else {
				$show->condition = "(status = 'onsale' OR status = 'sold' OR status = 'reserved')";
			}
		}
		$show->get_values();

		$this->id = $show->id;
		$this->tipus_value = $show->rs['tipus'];

		// miro si hi ha hagut algun resultat
		if ($show->has_results)
		{	
			$this->set_var('results',true);    
			$this->set_var('pdf_link',$this->_get_pdf_link($show->id));    
			// mostrar totes en el preview de la intranet
			$show->call('show_walk', 'custom1,custom2,custom3,custom4',true);
			//$content_images = list_records_images($show->rs['property_id']);
			$rs = &$show->rs;
			$tipus = $this->caption['c_tipus_' . $rs['tipus']]; // abans de show_form el rs te el valor que hi ha a la BBDD
			
			// mostrar extres
			$this->set_vars(array('extras_included'=>array(),'extras_required'=>array(),'extras_optional'=>array()));
			if ($show->rs['tipus']=='temp') {
				
				$module = Module::load('booking', 'extra');
				$module->parent = 'InmoProperty';
				$module->get_property_extras($this, $show->id);
			
			}
			
			$content = $show->show_form(); // despres de show form el rs te el valor traduit a el que vull mostrar a la web
			
			if ($this->is_index) {
				Page::set_page_title( $show->rs['page_title'], $tipus . ' ' . $rs['property'] . ' ' . $rs['provincia_id'] . ' ' . $rs['comarca_id'] );
				Page::set_page_description( $rs['page_description'], $rs['description'] );
				Page::set_page_keywords( $rs['page_keywords'] );

				Page::add_breadcrumb( $rs['property'] );

				if ( $show->config['show_as_title'] == 'ref' ) {
					//$gl_page->title = $this->caption['c_ref'] . ' ' . $rs['ref'];
					Page::set_title( $this->caption['c_ref'] . ' ' . $rs['ref'] );
				} else {
					//$gl_page->title = $rs[$show->config['show_as_title']];
					Page::set_title( $rs[ $show->config['show_as_title'] ] );
				}
				// TODO - Fer-ho igual a notícies i product i sell_point
				// Si tenim show_as_title i show_as_subtitle, no fa falta override_gl_page_title 1 i 2
				if ( $show->config['override_gl_page_title'] == '3' ) {
					Page::set_body_subtitle( $show->rs['property_title'] );
				} else {
					Page::set_body_subtitle( '' );// si faig override del tiol, no te sentit deixar el subtitol general de la pagina
				}
				Page::set_page_og( $show->rs );
			}

			//$content .= $content_images;
			unset($_SESSION['visited_properties'][$rs['property_id']]);
			$_SESSION['visited_properties'][$rs['property_id']] = $rs['property_id'];
			//Debug::p($tpl);	
			
			return $content;
			
		}
		else
		{
			// visualitzar desde MLS
			if (isset($_GET['mls'])){
				$this->set_var('results',false);
				return $show->show_form();
			}
			else{
				Main::error_404();
			}
		}	
		
		
		
	    
	}

	function save_rows()
	{
	    $save_rows = new SaveRows($this);
	    $save_rows->save();
	}

	function _get_pdf_link($property_id)
	{
	    $link = "/".LANGUAGE."/print/showwindow/".$property_id."/";
		return $link;
	}

	function write_record()
	{
	    $writerec = new SaveRows($this);
	    $writerec->save();
	}

	function manage_images()
	{
		if (isset($_GET['is_xml'])){
			header('Content-type: text/html; charset=utf-8');
			include(DOCUMENT_ROOT . 'common/jscripts/viewer/inmo_property_images.php');
		}
	}
	function list_records_walk($property_id, $ref, $category_id, $price_consult, $price, $status, $land, $land_units, $tipus, $entered)
	{
		return list_records_walk($property_id, $ref, $category_id, $price_consult, $price, $status, $land, $land_units, $tipus, $entered, $this->caption);
	}
	function show_walk(&$show, $custom1,$custom2,$custom3,$custom4)
	{
		
		extract($show->rs);
		$ret = array();
		
		if(!$show_map_point) {
			
			$ret['longitude'] = 0;
			$ret['latitude'] = 0;
			
		}

		// si no s'han guardat mai cap mapa, no es mostra a public
		if (!floatval($center_longitude) || !floatval($center_latitude)) {
			$ret[ 'show_map' ] = false;
		}

		// Obtenir valors i array per elbucle de detalls
		$details = &$this->details;
		foreach($details as $detail)
		{
			if (($this->fields[$detail]['type'] == 'checkbox') && ($$detail))
			{
				if ((isset(${$detail.'_area'})) && (${$detail.'_area'}))
				{
					$ret[$detail] = ${$detail.'_area'} . ' m<sup>2</sup>';
				}
				else
				{
					$ret[$detail] = $this->caption['c_yes'];
				}
			} elseif ($$detail)
			{
				$ret[$detail] = $$detail;
			}
		}

		if ($ret)
		{
			$ret['details'] = $details;
		}
		else
		{
			$ret['details'] = '';
		}

		$ret += get_ref($ref, $category_id);
		$ret += get_price_consult($price_consult, $show->rs['price'], $this->caption, $show->rs['property_id'], $show->rs['tipus']);
		$ret += get_status($status);
		$ret += get_land($land,$land_units);
		$ret += get_land_units($land_units);
		$ret += get_bookmark_link($show->rs['property_id']);
		$ret += get_recent($entered);
		

		return $ret;
	}
	function list_records_search_search() {
		$this->process="list_records_search_search";
		$this->do_action('list_records');
	}
}
?>