<?
/**
 * InvestinPagina
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2012
 * @version $Id$
 * @access public 
 
 */
class InvestinPagina extends Module{
	var $pagina_id, $list_template;
	function __construct(){
		parent::__construct();
	}
	function get_records()
	{
		
		$listing = new ListRecords($this);
		$listing->paginate = false;
		$listing->template = $this->list_template;
		
		$listing->always_parse_template = true;	
		$listing->order_by = 'ordre ASC, pagina, pagina_id';
		$listing->extra_fields = '(SELECT file_name FROM investin__paginatpl WHERE investin__paginatpl.paginatpl_id = investin__pagina.paginatpl_id) as file_name';
		$listing->condition = "parent_id = " . $this->pagina_id . $GLOBALS['gl_investin_status'];
		$listing->call('records_walk','pagina_id',true);
			
	    $ret = $listing->list_records();
	    return $ret;
	}
	
	function records_walk(&$listing,$pagina_id){
		$rs = &$listing->rs;
		
		if ($listing->list_show_type=='list'){
			$ret['body_content']= add_dots('body_content', $rs['body_content']);
		}
		
		// plantilles
		$ret['show_video']= (isset($rs['images'][1]) || $rs['videoframe']);
		if (!$rs['videoframe'] && isset($rs['images'][1])){
			$ret['image2'] = $rs['images'][1];
		}		
		
		// comú
		$ret['show_col_1']= ($rs['body_subtitle']||$rs['body_content']);
		$ret['show_col_2']= ($rs['body_subtitle2']||$rs['body_content2']);
		$ret['show_info']= ($rs['url1']||$rs['url2']||$rs['url3']||$rs['files']);
		
		return $ret;
	}
	function show_form()
	{
		
		// Faig un show form per les dades de la pàgina
		$show = new ShowForm($this);
		$show->condition = substr($GLOBALS['gl_investin_status'],5);
		
		$show->get_values();
		
		if (!$show->has_results) Main::error_404();
		
		$show->call('records_walk','pagina_id',true);
		
		
		$this->increment_counter();

		Page::set_title($show->rs['pagina']);
		Page::set_page_title($show->rs['page_title'],$show->rs['pagina']);
		Page::set_page_description($show->rs['page_description'],$show->rs['body_content']);
		Page::set_page_keywords($show->rs['page_keywords']);
		
		$show->template = 'investin/' . Db::get_first(
			"SELECT file_name FROM investin__paginatpl WHERE paginatpl_id = " . $show->rs['paginatpl_id']);
		
		// carrego subapartats
//		$module = Module::load('investin','pagina');
//		$module->pagina_id =  $show->id;
//		$module->list_template = 'investin/' . Db::get_first(
//			"SELECT file_name FROM investin__listtpl WHERE listtpl_id = " . $show->rs['listtpl_id']);
//		$list_content = $module->do_action('get_records');
		// fi subapartats
		
//		$show->set_var('list_content',$list_content);
		$content = $show->show_form();
		
		$GLOBALS['gl_content'] = $content;
	}
	function increment_counter(){
		$pagina_id = $this->pagina_id;
		include (DOCUMENT_ROOT . 'common/includes/browser/browser.php');
		$browser = new Browser();
		$name = $browser->getBrowser();
		
		
		$is_ok=( 
				$name != Browser::BROWSER_GOOGLEBOT &&
			$name != Browser::BROWSER_SLURP &&
			$name != Browser::BROWSER_W3CVALIDATOR &&
			$name != Browser::BROWSER_MSNBOT &&
			$name != Browser::BROWSER_UNKNOWN
				);		
		
		if ($is_ok){
			// si no està en sessió la conto, nomès un cop per usuari
			if (!isset($_SESSION['investin']['paginas'][$pagina_id])){
				$_SESSION['investin']['paginas'][$pagina_id] = true;
				Db::execute("UPDATE investin__pagina SET counter=(counter+1) WHERE  pagina_id='" .$pagina_id . "'");
			}			
		}		
	}
}
?>