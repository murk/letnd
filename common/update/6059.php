<?

/* 1-12-15

	Actualització product others, més llarg encara

 */
if ( is_table( 'product__product' ) ) {

	$update_sql [] = "
	ALTER TABLE `product__product`
	CHANGE COLUMN `others1` `others1` VARCHAR(2000) NOT NULL COLLATE 'utf8_spanish2_ci',
	CHANGE COLUMN `others2` `others2` VARCHAR(2000) NOT NULL COLLATE 'utf8_spanish2_ci',
	CHANGE COLUMN `others3` `others3` VARCHAR(2000) NOT NULL COLLATE 'utf8_spanish2_ci',
	CHANGE COLUMN `others4` `others4` VARCHAR(2000) NOT NULL COLLATE 'utf8_spanish2_ci',
	CHANGE COLUMN `others5` `others5` VARCHAR(2000) NOT NULL COLLATE 'utf8_spanish2_ci',
	CHANGE COLUMN `others6` `others6` VARCHAR(2000) NOT NULL COLLATE 'utf8_spanish2_ci',
	CHANGE COLUMN `others7` `others7` VARCHAR(2000) NOT NULL COLLATE 'utf8_spanish2_ci',
	CHANGE COLUMN `others8` `others8` VARCHAR(2000) NOT NULL COLLATE 'utf8_spanish2_ci';
";
	$update_sql [] = "
	ALTER TABLE `product__product_language`
	CHANGE COLUMN `others1` `others1` VARCHAR(2000) NOT NULL COLLATE 'utf8_spanish2_ci',
	CHANGE COLUMN `others2` `others2` VARCHAR(2000) NOT NULL COLLATE 'utf8_spanish2_ci',
	CHANGE COLUMN `others3` `others3` VARCHAR(2000) NOT NULL COLLATE 'utf8_spanish2_ci',
	CHANGE COLUMN `others4` `others4` VARCHAR(2000) NOT NULL COLLATE 'utf8_spanish2_ci',
	CHANGE COLUMN `others5` `others5` VARCHAR(2000) NOT NULL COLLATE 'utf8_spanish2_ci',
	CHANGE COLUMN `others6` `others6` VARCHAR(2000) NOT NULL COLLATE 'utf8_spanish2_ci',
	CHANGE COLUMN `others7` `others7` VARCHAR(2000) NOT NULL COLLATE 'utf8_spanish2_ci',
	CHANGE COLUMN `others8` `others8` VARCHAR(2000) NOT NULL COLLATE 'utf8_spanish2_ci';
";

}

?>