<?php
$menu = isset($vars['menu'])?$vars['menu']:'1'; // $vars['menu'] l'agafa de la taula all__block -> vars 

$query = "SELECT api__pagina.pagina_id as pagina_id, pagina, pagina_file_name
				FROM api__pagina_language, api__pagina
				WHERE api__pagina_language.pagina_id = api__pagina.pagina_id
				".$GLOBALS['gl_api_status']."
				AND parent_id = 0
				AND bin <> 1
				AND link=''
				AND menu = '" . $menu . "'
				AND language = '" . LANGUAGE . "'
				ORDER BY ordre ASC, pagina, pagina_id";		


$results = Db::get_rows($query);
Debug::add('Results api_api block',$results);

if (!$results) {
	$show_block = false;
	return;
}

$cols = 3;

if ($results)
{
	$conta = 0;	
	$loop = array();
	
	$loop_count = count($results);
    foreach($results as $rs)
    {
	
			$query_sub = "SELECT api__pagina.pagina_id as pagina_id, pagina, pagina_file_name
				FROM api__pagina_language, api__pagina
				WHERE api__pagina_language.pagina_id = api__pagina.pagina_id
				".$GLOBALS['gl_api_status']."
				AND parent_id = " . $rs['pagina_id'] . "
				AND bin <> 1
				AND link=''
				AND menu = '" . $menu . "'
				AND language = '" . LANGUAGE . "'
				ORDER BY ordre ASC, pagina, pagina_id";	
	
		
			$l['link'] = Page::get_link('api','pagina','show_record',$rs['pagina_id'],array(),false,$rs['pagina_file_name'],'');
			$l['item'] = $rs['pagina'];
			$l['id'] = $rs['pagina_id'];			
			$l['selected'] ='0';			
			
			$results = Db::get_rows($query_sub);
			
			$l['loop2'] = array();
			$loop2_count = count($results);
			$col=0;
			$col_conta=0;
			
			$num_per_col = floor($loop2_count/$cols);
			$rest = $loop2_count%$cols;
			//Debug::p($loop2_count,'Total');
			//Debug::p($num_per_col,'per col');
			//Debug::p($rest,'resto');
			
			foreach($results as $rs)
			{
				if (!isset($l['loop2'][$col])) $l['loop2'][$col] = array();
				$col_conta++;
				
				$l2['link'] = Page::get_link('api','pagina','show_record',$rs['pagina_id'],array(),false,$rs['pagina_file_name'],'');
				$l2['item'] = $rs['pagina'];
				$l2['id'] = $rs['pagina_id'];
				
				$l['loop2'][$col][] = $l2;
				
				if ($num_per_col==$col_conta && $rest){
					$rest--; // i passo un altre cop
				}
				// em fet el 1 de més per aquesta col o no hi ha resto
				elseif ( ($col_conta > $num_per_col) || ($num_per_col==$col_conta && !$rest) ){					
					$col_conta = 0;
					$col++;
				}
			}			
			
			$loop[] = $l;
    }
	$loop2[0]['selected'] = '1';
}

?>