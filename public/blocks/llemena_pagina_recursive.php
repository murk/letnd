<?php
$page = &$GLOBALS['gl_page'];


$pagina_id = R::id('pagina_id');

$condition = $pagina_id?("pagina_id = " . $pagina_id):("link='/'"); // agafo l'id de la home
$rs = Db::get_row('SELECT pagina_id,level,parent_id FROM llemena__pagina WHERE ' . $condition);

// faig servir el top id per marcar una pàgina, ja que pot ser que estiguem en un submenu i s'hagi de marcar la pagina d'on penja
$pagina_id = $vars['pagina_id'] = $rs['pagina_id']; // amb ['vars] passo la variable al tpl
$parent_id = $vars['parent_id'] = $rs['parent_id'];
$level = $vars['level'] = $rs['level'];

// quan no hi ha un GET[pagina_id], ho he de posar tot a 0 perque mostri els menus igualment
if (!$pagina_id) $pagina_id = 0;
if (!$parent_id) $parent_id = 0;
if (!$level) $level = 0;

// valors per defecte
$start_level = isset($vars['start_level'])?$vars['start_level']:'1'; // $vars['level'] l'agafa de la taula all__block -> vars, s'escriu: menu=2&start_level=2&last_level=1
$last_level = isset($vars['last_level'])?$vars['last_level']:'1'; // $vars['last_level'] l'agafa de la taula all__block -> vars
$menu = isset($vars['menu'])?$vars['menu']:'1'; // $vars['menu'] l'agafa de la taula all__block -> vars 
$set_top_title = isset($vars['set_top_title'])?$vars['set_top_title']:'1';

$parents = $selected_ids = array();
$parents[$level] = $parent_id; // el parent d'aquest nivell on som
$current_parent = $parent_id;
$selected_ids[$level] = $pagina_id; // el parent d'aquest nivell on som
$i = $level-1;

if ($level<$start_level){
	// si estem en un nivell superior del que volem mostrar, haig de cambiar el parent_id
	// En aquest cas nomès poden ser els fills dierctes d'aquest menu, per tant puc tindre un menu principal que mostra nivells 1-2 i un secuhdari que mostra 3-4, però el secundari no pot mostrar 4 nomès, ja que no sé quin es
	if (($start_level-$level) >1) {
		// si no hi ha resultats no presento el block
		if (!isset($GLOBALS['gl_show_block_llemena_recursive'])) $show_block = false;
		return array();
		}
	$current_parent = $pagina_id;	
}
else{
	// busco el parent del level d'inici i els tots els sleccionats fins el nivell inicial
	//  (si es un submenu puc començar del segon nivell o tercer, el primer nivell pot ser en un menu a dalt)
	while ($i>=$start_level){
		$parents[$i] = $current_parent = Db::get_first(
		"SELECT parent_id FROM llemena__pagina WHERE pagina_id = " . $parents[$i+1]);
		$selected_ids[$i] = $parents[$i+1]; // es el pagina_id actual, si a la consulta de dalt ho he posat ... pagina_id = " . $parents[$i+1]);
		$i--;
	}
}
Debug::add('Pagina_id',$pagina_id);
Debug::add('nivell',$level);	
Debug::add('parents',$parents);	
Debug::add('selected_ids',$selected_ids);	

include_once ('llemena_pagina_recursive_functions.php');
$loop = _block_llemena_pagina_get_children($current_parent, $start_level, $selected_ids, $menu, $last_level);

// si no hi ha resultats no presento el block, ej. serveix per quan tinc un block d'un 2 o 3 nivell i no hi ha resultats
if (!$loop && !isset($GLOBALS['gl_show_block_pagina_recursive'])) $show_block = false;

// funcionarà sempre i quan hi hagi el menu del primer nivell ( en principi crec que sempre hi serà )
// nomès es pels que son menu=1, el menu secondary no influeix en el top_id
// comprovo el $selected_ids[1], ja que quan no hi ha GET[pagina] no hi ha cap seleccionat
if ($start_level == 1 && $menu==1 && isset($selected_ids[1])) _block_llemena_pagina_set_top_vars($loop, $selected_ids[1]);
// si som al primer nivell, el top_title es igual que el title, per tant borro el title
// si set_top_title = 0, no cambio el title, em serveix per quan nomès tinc un nivell de menus
//if ($level == 1 && $set_top_title) $page->title = '';

//debug::p($loop);

?>