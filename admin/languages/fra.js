var NONE_SELECTED = "Aucune file na été sélectionnée ";
 
var VALIDAR_TEXT1 = ' est obligatoire ';
var VALIDAR_TEXT2 = ' a caractères non valables';
var VALIDAR_TEXT3 = ' cette adresse d\'e-mail est incorrecte';
var VALIDAR_TEXT4 = ' doit se composer dau moins 3 caractères et @';
var VALIDAR_TEXT5 = ' doit être un numéro';
var VALIDAR_TEXT6 = ' doit être un numéro de 5 chiffres';
var VALIDAR_TEXT7 = ' contient ';
var VALIDAR_TEXT8 = ' caractères, ne peut pas dépasser ';
var VALIDAR_TEXT9 = ' Il faut introduire l\'année ou n\'est pas numérique';
var VALIDAR_TEXT10 = ' L\'année n\'est pas correcte';
var VALIDAR_TEXT11 = ' La date n\'est pas correcte';
var VALIDAR_TEXT12 = ' est incorrect, tapez 8 chiffres et la lettre';
var VALIDAR_TEXT13 = ' est incorrect';
var VALIDAR_TEXT14 = ' la lettre n\'est pas correcte';
var VALIDAR_TEXT15 = ' est incorrect ';
var VALIDAR_TEXT16 = 'Doivais sélectionner ';
var VALIDAR_TEXT17 = 'Vous n\'avez pas rempli correctement le formulaire: ';
var VALIDAR_TEXT18 = ' vous doivais choisir au moins une option';
var VALIDAR_TEXT19 = ' Vous doivais choisir au moins une adresse ou un groupe';
var VALIDAR_TEXT20 = ' Le numéro de la carte de crédit est incorrect';
var VALIDAR_TEXT21 = ' La date d\'expiration de la carte de crédit est incorrecte';
var VALIDAR_TEXT22 = ' n\'est pas correcte';
var VALIDAR_TEXT23 = ' n\'est pas correcte';
var VALIDAR_TEXT24 = ' doit être un nombre';
var VALIDAR_TEXT25 = ' les mots de passe ne correspondent pas';
var VALIDAR_TEXT26 = ' ne correspond pas à la confirmation';
var VALIDAR_TEXT27 = ' peuvent contenir que des chiffres, des lettres minuscules "a" à "z" ou "_-" ou tiret de soulignement ("_" ou "-")';
var VALIDAR_TEXT28 = 'Vous devez accepter la politique de confidentialité';
var MODALBOX_SAVE = ' Envoi de données... ';
var MODALBOX_CLOSE = 'fermer';
var MULTIFILE_ACCEPT = ' Vous ne pouvez télécharger que les fichiers de type: ';
var MULTIFILE_NOT_ACCEPT = ' Vous non pouvez télécharger les fichiers de type: ';
var CONFIRM_TEXT = 'Etes-vous sûr que vous voulez faire cette action?';
var WAIT_TEXT = 'Un moment s\'il vous plaît... ';

var MONTH_NAMES = new Array("Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre");

var FILE_DELETE = "Supprimer";
// NO TRADUIT
var IMMO_MAP_POINT_SELECTED = "Faites un clic sur un point de la carte pour établir la situation";
var PRODUCT_CART_ADDED = "Le produit a été ajouté à votre panier";

var DROP_DEFAULT_MESSAGE = "Cliquez ou traîne image ici";
var DROP_DEFAULT_FILE_MESSAGE = "Cliquez ou traîne fichiers ici";
var DROP_INVALID_FILE_TYPE = "Ce type de fichier n'est pas valide";
var DROP_CANCEL_UPLOAD_CONFIRMATION = "Etes-vous sûr que vous voulez annuler cette image?";

var MESSAGE_CONFIRM_DELETE_NOT_SENT = 'Etes-vous sûr que vous voulez supprimé cette direction de la liste de destinataires?';

var NO_RESULTS = 'Aucun résultat';