<?php
// no es poden posar ; entre mig de la consulta ja que parteix les consultes per el ; a no ser que fem servir $update_sql en array()
if (is_table("contact__contact") && !is_field('contact__contact','to_address')){
	// poso el mail on s'havien per defecte els mails de contacte
	$from = Db::get_first("SELECT value FROM all__configpublic WHERE name = 'from_address';");
	// si hi ha el config de contact s'envien al from de contact
	if (is_table("contact__configpublic"))
		$from = Db::get_first("SELECT value FROM contact__configpublic WHERE name = 'from_address';");
	$update_sql .="ALTER TABLE `contact__contact`  ADD COLUMN `to_address` VARCHAR(250) NOT NULL AFTER `mail`;";
	$update_sql .="UPDATE `contact__contact` SET `to_address`='".$from."';";
}	
?>