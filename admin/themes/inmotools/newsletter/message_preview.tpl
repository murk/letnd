<html>
<style>
td.preview_text{
	color:#000;
	font-size: 12px;
}
table.preview-header{
	border-bottom:1px solid #d5d5d5;
}
</style>
<title>Preview</title>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table class="preview-header" width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#E5E5E5">
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td><img src="/admin/themes/inmotools/images/spacer.gif" width="1" height="7"></td>
        </tr>
      </table>
      <table border="0" cellspacing="0" cellpadding="2">
        <tr> 
          <td width="10">&nbsp;</td>
          <td width="70" class="preview_text"><strong>De:</strong></td>
          <td class="preview_text"><?=$from?></td>
        </tr>
        <tr> 
          <td>&nbsp;</td>
          <td class="preview_text"><strong>Fecha:</strong></td>
          <td class="preview_text"><?=$date?></td>
        </tr>
        <tr> 
          <td>&nbsp;</td>
          <td class="preview_text"><strong>Para:</strong></td>
          <td class="preview_text"><?=$to?></td>
        </tr>
        <tr> 
          <td>&nbsp;</td>
          <td class="preview_text"><strong>Asunto:</strong></td>
          <td class="preview_text"><?=$subject?></td>
        </tr>
      </table>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td><img src="/admin/themes/inmotools/images/spacer.gif" width="1" height="7"></td>
        </tr>
      </table></td>
  </tr>
</table>
<?if ($message_text):?>
<table width="97%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr> 
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><?=$message_text?></td>
  </tr>
</table>
<?endif //message_text?>
<?if ($message_html):?>
<?=$message_html?>
<?endif //message_html?>
</html>