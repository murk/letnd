<?
/*

CRON - cada hora
0 * * * * php -f "/var/www/letnd.com/datos/motor/admin/modules/admintotal/admintotal_ceigrup_cli.php" > /var/www/letnd.com/datos/motor/data/logs/ceigrup.htm

SERVIDOR
php -f "/var/www/letnd.com/datos/motor/admin/modules/admintotal/admintotal_ceigrup_cli.php" > /var/www/letnd.com/datos/motor/data/logs/ceigrup.htm
php -d memory_limit=512M "/var/www/letnd.com/datos/motor/admin/modules/admintotal/admintotal_ceigrup_cli.php"

WEB
/admin/?tool=admintotal&tool_section=ceigrup&action=start

VAGRANT
php -f "/letnd/admin/modules/admintotal/admintotal_ceigrup_cli.php" > /letnd/data/logs/ceigrup_linux.htm
php -d include_path=/php_includes "/letnd/admin/modules/admintotal/admintotal_ceigrup_cli.php"

DOS
C:\server\php5\php.exe -f "D:\Letnd\admin\modules\admintotal\admintotal_ceigrup_cli.php" > d:\letnd\data\logs\ceigrup_windows.htm

DEBUG CLI EX
php -f "/letnd/admin/modules/admintotal/admintotal_ceigrup_cli.php" debug > /letnd/data/logs/ceigrup_linux.htm

*/

$dir = str_replace ( '/admin/modules/admintotal','/common/', __DIR__ );
$dir = str_replace ( '\admin\modules\admintotal','\\common\\', $dir );

include ($dir . 'cli_init.php');

include (DOCUMENT_ROOT . 'admin/modules/admintotal/admintotal_ceigrup.php');

include (DOCUMENT_ROOT . 'common/classes/upload_files.php');

$ceigrup = New AdmintotalCeigrup;
$ceigrup->is_cli = true;
$ceigrup->_start();