<div class="mecanica">
<div class="mecanicatop">		
<h2>NO SÓM UNS</br> ALTRES MECÀNICS,</br> SOM ELS TEUS MECÀNICS.</h2>
</div>
<div class="mecanicaleft">
    <strong>Mironauto</strong> disposa de servei de canvi de pneumàtics, equilibrats de rodes, ja siguin amb llandes de ferro o alumini. També li podem inflar els seus pneumàtics amb nitrogen, (gas inert amb una mol·lècula més gran que la del oxigen. Aquest gas evita les explosions de pneumàtics per excés de temperatura i la pressió de la roda es manté per molt més temps, evitant la corrosió de les llandes, ja siguin de ferro o alumini).</br></br>Si vostè ha canviat els pneumàtics o el seu vehicle ha sofert un accident, podem alinear-li la direcció en tots els seus àngles (sempre que aquests siguin ajustables), segons les cotes originals del fabricant. També podem fer-li un diagnòstic del seu vehicle per saber exàctament el problema i trobar una solució amb la màxima eficàcia.   
  </div>
</div>
