<?php
// menus eina
define('PRINT_MENU_KIND','Type de contrat');
define('PRINT_MENU_KIND_NEW','Entrez nouveau type');
define('PRINT_MENU_KIND_LIST','Énumérer types');
define('PRINT_MENU_CONTRACT','Contrats');
define('PRINT_MENU_CONTRACT_NEW','Entrez un nouveau contrat');
define('PRINT_MENU_CONTRACT_LIST','Énumérer contrats');
define('PRINT_MENU_CONTRACT_BIN','Corbeille');
define('PRINT_MENU_SHOWWINDOW', 'Vitrines');
define('PRINT_MENU_SHOWWINDOW_LIST', 'Énumérer les propriétés');


// captions seccio
$gl_caption_contract['c_property_id'] = 'Propriété';
$gl_caption_contract['c_ref'] = 'Référence de propriété';
$gl_caption_contract['c_property_private'] = 'Nom privée';
$gl_caption_contract['c_category_id'] = 'Type de propriété';
$gl_caption_contract['c_signed'] = 'Signé';
$gl_caption_contract['c_registered'] = 'Date de vente';
$gl_caption_contract['c_registered2'] = 'Vente';
$gl_caption_contract['c_rescinded'] = 'Terminé';
$gl_caption_contract['c_duration'] = 'Durée';
$gl_caption_contract['c_months'] = 'mois';
$gl_caption_contract['c_days'] = 'journées';
$gl_caption_contract['c_years'] = 'ans';
$gl_caption_contract['c_kind_id'] = 'Type';
$gl_caption_contract['c_language_id'] = 'Langue';
$gl_caption_contract['c_state'] = 'État';
$gl_caption_contract['c_current'] = 'Courant';
$gl_caption_contract['c_canceled'] = 'Terminé';
$gl_caption_contract['c_ipc'] = 'Inflation';
$gl_caption_contract['c_ipc2'] = 'jour pour appliqué la inflation';
$gl_caption_contract['c_ipc3'] = 'Inflation appliquét';
$gl_caption_contract['c_ipc4'] = 'Inflation en';
$gl_caption_contract['c_expires'] = 'Expire';
$gl_caption_contract['c_expires2'] = 'jours por expirez le contrat ';
$gl_caption_contract['c_expires3'] = 'Expira en';
$gl_caption_contract['c_contract_id'] = 'Ref.';
$gl_caption_contract['c_select_property_link'] = 'Sélectionnez la propriété...';
$gl_caption_contract['c_add_property_link'] = 'Entrez nouvelle propriété...';
$gl_caption_contract['c_select_buyers_link'] = 'Sélectionnez Acheteur - Locataire...';
$gl_caption_contract['c_add_buyers_link'] = 'Entrez nouvel acheteur - locataire...';
$gl_caption_contract['c_buyers'] = 'acheteur - locataire';
$gl_caption_contract['c_owner_title'] = 'Propriétaires';
$gl_caption_contract['c_select_owners_link'] = 'Sélectionnez propriétaire...';
$gl_caption_contract['c_add_owners_link'] = 'Entrez le nouveau propriétaire...';
$gl_caption_contract['c_contract_title'] = 'Données de marché';
$gl_caption_contract['c_contract_title_list'] = 'Contrat';
$gl_caption_contract['c_filter_kind_id'] = 'Tous les types';
$gl_caption_contract['c_contract_observations'] = 'Observations';
$gl_caption_contract['c_print_contract_button']='Imprimer contrat';
$gl_caption_contract['c_place']='Lieu de signature';
$gl_caption_contract['c_diposit']='Dépôt (€)';
$gl_caption['c_error_contract']='Impossible de imprimer contrat, données manquantes';

$gl_caption_contract ['c_custumer_id']='Nom du client';
$gl_caption_contract ['c_phone'] ='Téléphone';

$gl_caption_kind['c_kind'] = 'Type';


$gl_caption_showwindow['c_land'] = 'Terrain';
$gl_caption_showwindow['c_municipi_id'] = 'Situation';
$gl_caption_showwindow['c_bathroom'] = 'Salles de bains';
$gl_caption_showwindow['c_wc'] = 'Toilettes';
$gl_caption_showwindow['c_garage_area'] = '%s m<sup>2</sup>';
$gl_caption_showwindow['c_storage_area'] = '%s m<sup>2</sup>';
$gl_caption_showwindow['c_terrace_area'] = '%s m<sup>2</sup>';
$gl_caption_showwindow['c_garden_area'] = '%s m<sup>2</sup>';
$gl_caption_showwindow['c_swimmingpool_area'] = '%s m<sup>2</sup>';
$gl_caption_showwindow['c_2garage_area'] = '%s m2';
$gl_caption_showwindow['c_2storage_area'] = '%s m2';
$gl_caption_showwindow['c_2terrace_area'] = '%s m2';
$gl_caption_showwindow['c_2garden_area'] = '%s m2';
$gl_caption_showwindow['c_2swimmingpool_area'] = '%s m2';
$gl_caption_showwindow['c_bathroom_singular'] = ' salle de bains';
$gl_caption_showwindow['c_bathroom_plural'] = ' salles de bains';
$gl_caption_showwindow['c_wc_singular'] = ' toilette';
$gl_caption_showwindow['c_wc_plural'] = ' toilettes';

?>