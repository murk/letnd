<?
/**
 * AmctaicTransparencia
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2011
 * @version $Id$
 * @access public
 */
class AmctaicTransparencia extends Module{

	public function __construct(){
		parent::__construct();
	}
	// Ho faig amb list records per simplificar enllaç
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	public function get_form()
	{
		$_GET['transparencia_id'] = 1;
		$this->action = 'show_record';
	    $show = new ShowForm($this);
	    return $show->show_form();
	}
}