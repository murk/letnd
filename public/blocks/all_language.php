<?php
$query = "SELECT language, code, code2, url
				FROM all__language
				WHERE public=1
				ORDER BY ordre";

$results = Db::get_rows($query);

$conta = 0;
$loop_count = count($results);
if (USE_FRIENDLY_URL){
	$base_link = $_SERVER["REQUEST_URI"];
	$base_link = ($base_link=='/'.LANGUAGE)?'/':str_replace('/'.LANGUAGE.'/','/',$base_link); // el primer m'asseguro si l'adreça es www.url.com/cat i el segon www.url.com/cat/ , ho faig així en lloc de un sol replace de '/'.LANGUAGE , per que podria ser això www.url/catelquesigui, m'haid'assegurar que es /cat o /cat/
	
	// m'asseguro, trec si hi ha per algun link intern el language al query string
	$base_link = str_replace('language='.LANGUAGE.'&','',$base_link);
	$base_link = str_replace('&language='.LANGUAGE,'',$base_link);
	$base_link = str_replace('?language='.LANGUAGE,'',$base_link); // si nomès hi ha el parametre de idioma
	$base_link = str_replace('&','&amp;',$base_link); // ho faig HTML correcte
}

if ($results && count($results) > 1) {
    foreach($results as $rs) {
        LANGUAGE == $rs["code"]?$selected = 1:$selected = 0;

        if (USE_FRIENDLY_URL){		
			$new_base_link = $base_link; // per si no faig l'if de sota
			$page_file_name = R::file_name();
			if ($page_file_name){
				$lang_page_file_name = Db::get_first("SELECT page_file_name FROM all__page_language WHERE language='" . $rs['code'] . "' AND page_id=" . $GLOBALS['gl_page']->page_id);
				$new_base_link = str_replace($page_file_name.'.html',$lang_page_file_name.'.html',$base_link);
				$new_base_link = str_replace(
					'/p/'.$GLOBALS['gl_tool'].'/'.$GLOBALS['gl_tool_section'].'/' . $page_file_name.'/',
					'/p/'.$GLOBALS['gl_tool'].'/'.$GLOBALS['gl_tool_section'].'/' . $lang_page_file_name.'/',
					$new_base_link);
				if ($GLOBALS['gl_tool']==DEFAULT_TOOL  && $GLOBALS['gl_tool']==DEFAULT_TOOL_SECTION){
				$new_base_link = str_replace(
					'/p/' . $page_file_name.'/',
					'/p/' . $lang_page_file_name.'/',
					$new_base_link);				
				}
			}
			$file_name = R::file_name('file_name');
			if ($file_name){
				$module = $GLOBALS['gl_module'];//debug::p($module);
				
				$f_tool = $module->child_tool?$module->child_tool:$module->tool;
				$f_tool_section = $module->child_tool_section?$module->child_tool_section:$module->tool_section;
				
				$id = Db::get_first("SELECT ".$f_tool_section."_id 
											FROM ".$f_tool."__".$f_tool_section."_language 
											WHERE language='" . LANGUAGE . "'
											AND ".$f_tool_section."_file_name = '" . $file_name . "'");
				
				// sino hi ha id vol dir que file_name correspon a l'id i no al file_name
				if (!$id)
					$id = $file_name;
					
				$lang_file_name = Db::get_first("
					SELECT ".$f_tool_section."_file_name 
						FROM ".$f_tool."__".$f_tool_section."_language
						WHERE language='" . $rs['code'] . "' 
						AND ".$f_tool_section."_id= '".$id."'
				");		
				
				// sino hi ha lang_file_name posem id i no file_name
				if (!$lang_file_name)
					$lang_file_name = $id;
				
				
				$new_base_link = str_replace($file_name.'.htm',$lang_file_name.'.htm',$new_base_link);
			}
			if  (($GLOBALS['gl_is_home']) && $rs['url'] ){
				$l['link'] = $rs['url'];
			}
	        else {
		        $l['link'] = $rs['url'] . '/' . $rs['code'] . $new_base_link;
	        }
		}
		else{
			$l['link'] = get_all_get_params(array('language'),'?','&') . 'language=' . $rs['code'];		
		}
		
		// si es la home, trec el link del menu d'idiomes
		if ($GLOBALS['gl_default_language']==$rs['code'] && $GLOBALS['gl_is_home']) $l['link'] = '/';
		
        $l['selected'] = $selected;
        $l['item'] = $rs['language'];
        $l['id'] = $rs['code'];
        $l['code2'] = $rs['code2'];
		$l['conta'] = $conta++;
		$l['loop_count'] = $loop_count;
        $loop2[] = $l;
		if ($selected) $vars['current_language'] = $l;
    }
	
	$loop2[0]['class'] = 'first';	
	$loop2[count($loop2)-1]['class'] = 'last';
	
    $loop[] = array ('loop' => $loop2,
        'item' => '');
}
?>