<?

/*15-11-13
  Afegir tipus de sellpoints
	
 
 */

if (is_table('print__template') && !is_field("print__template",'size')) {	
	
	$update_sql[] = "
		ALTER TABLE `print__template`
	ADD COLUMN `size` ENUM('A4','A3','A2','A1','A0','A5') NOT NULL DEFAULT 'A4' AFTER `type`;";
	
}


?>