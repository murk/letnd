<?php
function get_vars_inmo_property($content_ids, &$ids, &$names)
{
    $query = "SELECT property_id, ref, property_private
				FROM inmo__property
				WHERE property_id IN (" . implode(',', $content_ids) . ")
				ORDER BY ref DESC" ;
    $results = Db::get_rows($query);
    foreach($results as $rs)
    {
        $nam = $rs['ref'] . ' - ' . $rs['property_private'];
        $names[] = htmlspecialchars(str_replace(',', ' ', $nam), ENT_QUOTES);
        $ids[] = $rs['property_id'];
    }
}
function get_content_extra_inmo_property($ids, $action, $language)
{

	if ($action == 'show_record')
    {
	    $ids = explode(',', $ids);
	    $content = '';
        foreach ($ids as $id)
        {
           $content .= get_url_html(HOST_URL . '/?template=&tool=inmo&tool_section=property&action=show_record&loginnewsletter=letndmarc&passwordnewsletter=5356aopfqkiios5cnoqws90se&property_id='.$id.'&language=' . $language);
			
			
			//ob_start(); // Start output buffering
            //require(HOST_URL . '/?template=&tool=inmo&tool_section=property&action=show_record&loginnewsletter=letndmarc&passwordnewsletter=5356aopfqkiios5cnoqws90se&property_id='.$id.'&language=' . $language); // Include the file
            //$content .= ob_get_contents(); // Get the contents of the buffer
            //ob_end_clean(); // End buffering and discard
        }
    }
	else if ($action == 'list_records') {
		
			 $content = get_url_html(HOST_URL . '/?template=&tool=inmo&tool_section=property&action=list_records&loginnewsletter=letndmarc&passwordnewsletter=5356aopfqkiios5cnoqws90se&property_ids='.$ids.'&language=' . $language); 
			 
			 
            //ob_start(); // Start output buffering
            //require(HOST_URL . '/?template=&tool=inmo&tool_section=property&action=list_records_newsletter&loginnewsletter=letndmarc&passwordnewsletter=5356aopfqkiios5cnoqws90se&property_ids='.$ids.'&language=' . $language); // Include the file
            //$content = ob_get_contents(); // Get the contents of the buffer
            //ob_end_clean(); // End buffering and discard
	}
    return $content;
}

?>