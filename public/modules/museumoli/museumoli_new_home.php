<?
/**
 * __Tool__Section
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2010
 * @version $Id$
 * @access public
 */
class MuseumoliNewHome extends Module{

	function __construct(){
		parent::__construct();
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function get_records()
	{
		$num_news = 2;
		$module = Module::load('news','new', $this, false);
		$listing = new ListRecords($module);
		$listing->condition = "(status = 'public')
							AND (entered=0 OR entered <= now())
							AND (expired=0 OR expired > (curdate()))";
		
		$category_id = R::id('category_id');					
		if ($category_id) $listing->condition .= " AND category_id = '" . $category_id . "'";
		//$listing->call_function['content'] = 'add_dots';
		//$listing->call('records_walk','url1,url1_name,url2,url2_name,content,content_list');
		$listing->order_by = 'ordre ASC, entered DESC';
		
		$listing->list_records(false); 
		
		foreach ($listing->results as $key=>$rs){
			extract($rs);
			$ret=$this->records_walk($url1,$url1_name,$url2,$url2_name,$content,$content_list);
			$rs = array_merge($rs, $ret);
			$listing->results[$key] = $rs;
		}
		
		$listing->template = 'museumoli/new_home.tpl';
		
		return $listing->parse_template();
		
		/*
		$listing->parse_template(false);
		debug::p($listing->loop);
		// obtinc frases
		
		$module2 = Module::load('museumoli','sentence', &$this, false);
		
	    $listing2 = new ListRecords($module2);
		$listing2->condition = "(status = 'public')";
		$listing2->order_by = 'sentence_id ASC';
		$listing2->limit = '0, ' . $num_news;
	    
		$listing2->set_records();	
		
		$loop = array();
		for ($x=0; $x<$num_news; $x++){
			$listing->loop[$x]['is_new_new']=isset($listing->loop[$x])?true:false; 
			$listing2->loop[$x]['is_new_sentence']=isset($listing2->loop[$x])?true:false; 
			
			$loop[] = array_merge ($listing->loop[$x],$listing2->loop[$x]);
		}
		$this->set_loop('loop',$loop);
		$this->set_vars($this->caption);
		$this->set_file('museumoli/new_home.tpl');
		
		
		return $this->process();
		*/
	}
	function records_walk($url1,$url1_name,$url2,$url2_name,$content=false,$content_list=false){		
		// Això ho poso aquí així no cal posar-ho al tpl, simplement posar: <a href="<=$url1>" target="_blank"><=$url1_name></a>
		$ret['url1_name'] = $url1_name?$url1_name:$url1;
		$ret['url2_name'] = $url2_name?$url2_name:$url2;
		
		// si es llistat passa lavariable content
		if ($content!==false){
			$ret['content']=$content_list?$content_list:add_dots('content', $content);
		}
			
		//$ret['url1']=str_replace('http://','',$url1);	
		//$ret['url1']=str_replace('http://','',$url1);
		return $ret;
	}
}
?>