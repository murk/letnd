
function select_properties() {
  gl_submit_addresses = false;
  gl_default_input_prefix = 'selected_property';
  showPopWin('/admin/?action=list_records_select_frames_property&process=print&menu_id=5002&custumer_id=', 950, 530, null, true, false, '');
}

function select_buyers(related_table_id) {
  gl_submit_addresses = true;
  gl_default_input_prefix = 'selected_buyer';
  //gl_related_table_id = related_table_id;
  showPopWin('/admin/?action=list_records_select_frames_custumer&menu_id=103&process=get_buyers_jscript&contract_id=' + related_table_id, 850, 530, null, true, false, '');
}

function select_owners(related_table_id, selected_property_id) {
  gl_submit_addresses = true;
  gl_default_input_prefix = 'selected_owner';
  gl_related_table_id = related_table_id;
  showPopWin('/admin/?action=list_records_select_frames_custumer&menu_id=103&process=get_owners_jscript&contract_id=' + related_table_id + '&property_id=' + selected_property_id, 850, 530, null, true, false, '');
}

function on_insert_address(table_id, ref){

	if (gl_default_input_prefix != 'selected_property') return;
	obj=top.document.theForm?top.document.theForm:top.document.list;
	input_prefix = top.gl_default_input_prefix;

	obj['property_id['+ obj['contract_id_javascript'].value +']'].value = table_id;
	obj['selected_property_ids'].value = table_id;
	obj['selected_property_names'].value = ref;
	window.save_frame.location = '/admin?menu_id=206&process=get_property_jscript&property_id=' + obj['property_id['+ obj['contract_id_javascript'].value +']'].value;
	top.hidePopWin();
}
function get_property_jscript(table_id, ref){
	obj=top.document.theForm?top.document.theForm:top.document.list;
	obj['property_id['+ obj['contract_id_javascript'].value +']'].value = table_id;
	window.save_frame.location = '/admin?menu_id=206&process=get_property_jscript&property_id=' + obj['property_id['+ obj['contract_id_javascript'].value +']'].value;
}
function get_custumer_jscript(custumer_id, custumer_name, gl_process)
{
	custumer_type = (gl_process=='contract_new_buyer' || gl_process=='contract_edit_buyer')?'buyer':'owner';
	if (gl_process=='contract_new_buyer' || gl_process=='contract_new_owner') {
		custumers_ids = document.theForm['selected_' + custumer_type + '_ids'].value;
		custumers_ids = custumers_ids?(custumers_ids + ',' + custumer_id):custumer_id;
		document.theForm['selected_' + custumer_type + '_ids'].value = custumers_ids;

		custumers_names = document.theForm['selected_' + custumer_type + '_names'].value;
		custumers_names = custumers_names?(custumers_names + ',' + custumer_name):custumer_name;
		document.theForm['selected_' + custumer_type + '_names'].value = custumers_names;
	}
	else {
		custumers_ids = document.theForm['selected_' + custumer_type + '_ids'].value;
	}

	window.save_frame.location = '/admin/?process=get_' + custumer_type + 's_jscript&menu_id=206&selected_addresses_ids=' + custumers_ids ;
}
function show_form_edit_buyer(custumer_id){
	obj=top.document.theForm;
	contract_id = obj['contract_id['+ obj['contract_id_javascript'].value +']'].value;
	showPopWin('/admin/?action=show_form_edit_custumer&menu_id=103&process=contract_edit_buyer&custumer_id=' + custumer_id + '&contract_id=' + contract_id, 850, 520, null, true, false, '');

}
function show_form_edit_owner(custumer_id){
	obj=top.document.theForm;
	contract_id = obj['contract_id['+ obj['contract_id_javascript'].value +']'].value;
	showPopWin('/admin/?action=show_form_edit_custumer&menu_id=103&process=contract_edit_owner&custumer_id=' + custumer_id + '&contract_id=' + contract_id, 850, 520, null, true, false, '');

}
function show_form_new_custumer(contract_id, gl_process){
	showPopWin('/admin/?action=show_form_new_custumer&menu_id=103&contract_id='+contract_id+'&process=' + gl_process, 850, 520, null, true, false, '');
}
function show_form_property(property_id, add_record){
	edit_property_id = add_record?'':'&property_id=' + property_id + '&action=show_form_edit';
	showPopWin('/admin/?menu_id=102&template=window&show_tabs=0&show_search=0&process=contract_write_record_property' + edit_property_id, 850, 520, null, true, false, '');
}
function show_form_property_images(property_id){
	edit_property_id = '&property_id=' + property_id;
	showPopWin('/admin/?menu_id=103&template=window&show_tabs=0&show_search=0&process=contract_write_record_property&action=list_records_images' + edit_property_id, 850, 520, null, true, false, '');
}
var registered_old; // static
function kind_on_change (obj, kind)
{
	val = kind.options[kind.selectedIndex].value;
	registered = obj['registered['+ obj['contract_id_javascript'].value +']'];
	registered_old = registered_old?registered_old:registered.value; // static
	if (registered_old=='') {
		registered_old = get_now();
	}
	if (val==1 || val==2){
		registered.value = registered_old;
		$('.cregistered').show();
		$('.cipc').hide();
	}
	else
	{
		registered.value = '';
		$('.cregistered').hide();
		$('.cipc').show();
	}
}
var rescinded_old; // static
function state_on_change (obj, state)
{
	val = state.options[state.selectedIndex].value;
	rescinded = obj['rescinded['+ obj['contract_id_javascript'].value +']'];
	rescinded_old = rescinded_old?rescinded_old:rescinded.value; // static
	if (rescinded_old=='') {
		rescinded_old = get_now();
	}
	if (val=='canceled'){
		rescinded.value = rescinded_old;
		$('.crescinded').show();
	}
	else if (val=='current')
	{
		rescinded.value = '';
		$('.crescinded').hide();
	}
}
function get_now(){
	now = new Date();
	dia_now = now.getDate()<10?'0'+now.getDate():now.getDate();
	mes_now = (now.getMonth()+1)<10?'0'+(now.getMonth()+1):now.getMonth()+1;
	return dia_now + '/' + mes_now + '/' + now.getYear();
}
//imprimir contractes 
function open_print_contract (contract_id){	
	location.href= '/admin/?action=show_form_edit_printcontract&menu_id=207&contract_id='+contract_id;	
}
