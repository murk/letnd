<?php

/**
 * Classe per fer queries a la base de dades tenint en compte la nomenclatura Letnd
 * Funciona amb els mateixos mètodes que Db, però no cal entrar el nom de la taula o l'id_field
 *
 * Iniciar per Query - Directament per Db
 * --------------------------------------------
 * $q = new Query( 'inmo__property' );
 *
 * Querys
 * --------
 * <code>
 * $status = $q->get_first( 'status', 10 );
 * $status = $q->get_first( 'status', "property_id = %s", 10 );
 *
 * $status = Db::get_first( "SELECT status FROM inmo__property WHERE property_id = %s", 10 );
 *
 *
 * $rs = $q->get_row( 'property_id, status', 10 );
 * $rs = $q->get_row( 'property_id, status', "property_id = %s", 10 );
 *
 * $rs = Db::get_row( "SELECT property_id, status FROM inmo__property WHERE property_id = %s", 10 );
 *
 *
 * $results = $q->get_rows( 'status', "status = %s", "prepare" );
 *
 * $results = Db::get_rows( "SELECT status FROM inmo__property WHERE status = %s", "prepare" );
 * </code>
 *
 * Update
 * --------
 *  <code>
 *
 *
 * $q->update( [ 'status' => 'prepare' ], 10 );
 * $q->update( [ 'property_id' => 10, 'status' => 'prepare' ] );
 * $q->update( [ 'status' => 'prepare' ], "property_id=%s", 10 );
 *
 * Db::update( 'inmo__property', [ 'status' => 'prepare' ], "property_id=%s", 10 );
 *
 *
 * </code>
 *
 * Insert
 * --------
 *  <code>
 *  $image_id = $q->insert( [
					'property_id'   => $property_id,
					'name'          => $name_original,
					'name_original' => $name_original,
					'size'          => $size,
					'ordre'         => $ordre,
					'main_image'    => $main_image,
				] )->insert_id;
 *
 * Db::->insert( 'inmo__property', [
					'property_id'   => $property_id,
					'name'          => $name_original,
					'name_original' => $name_original,
					'size'          => $size,
					'ordre'         => $ordre,
					'main_image'    => $main_image,
				] );
 * $image_id  = Db::insert_id();
 * </code>
 *
 *
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) Maig 2014
 * @version $Id$
 * @access public
 *
 */
class Query {
	private $id_fields = array();
	private $id_fields_fixed = '';
	private $id_fields_variable = '';
	public $id_fields_fixed_value = '';
	private $id_fields_variable_values = '';
	private $other_fields = array();
	private $fields = array();
	public $related_table = '';
	public $table = '';
	public $id_field = '';
	private $config = array();
	private $is_module_config = false;
	public $query = ''; // per consultar el query que s'ha fet a insert o update o get
	private $querys = '';
	public $show_querys = false;
	public $messages = array();
	public $order_by = false;
	public $limit = false;
	public $delete_condition = '';
	public $insert_id = '';
	private $is_prepare = false;
	private $include_id_field = false;
	private $get_fields = '';
	private $get_condition = '';

	/**
	 * Query constructor.
	 *
	 * @param $table
	 * @param string $related_table
	 * @param string $id_fields
	 * @param string $other_fields
	 */
	function __construct( $table, $related_table = '', $id_fields = '', $other_fields = '' ) {

		if ( $table ) {
			$this->table = $table;
			$id_field    = explode( '__', $table );
			$id_field    = $id_field[1];

			$pos = strrpos( $id_field, "_" );
			// Les taules booking__extra_to_property tenen id: extra_property_id
			// les taules inmo__property_video, inmo__property_image, inmo__property_file .. tenen video_id, file_id ...
			// La resta si no segueix la norma, s'ha d'especificar expressament q->id_field = "field_id";
			if ( strrpos( $id_field, "_to_" ) ) {
				$id_field = str_replace( '_to_', '_', $id_field );
			}
			elseif ( $pos !== false ) {
				$last_part = substr( $id_field, $pos + 1 );
				if ( $last_part == 'video' || $last_part == 'image' || $last_part == 'file' ) {
					$id_field = $last_part;
				}
			}

			$this->id_field = $id_field . '_id';
		}
		$this->related_table = $related_table;

		$this->set_id_fields( $id_fields );
		$this->set_other_fields( $other_fields );
		$this->messages = isset( $GLOBALS['gl_messages'] ) ? $GLOBALS['gl_messages'] : ''; // CLI no te gl_messages
	}

	public function set_order_by( $order_by ) {
		$this->order_by = $order_by;
		return $this;
	}

	public function set_limit( $limit ) {
		$this->limit = $limit;
		return $this;
	}

	public function set_id_field( $id_field ) {
		$this->id_field = $id_field;
		return $this;
	}

	public function set_id_fields( $id_fields ) {

		if ( $id_fields ) {
			$this->id_fields = explode( ',', $id_fields );
			if ( count( $this->id_fields ) != 2 ) {
				Debug::backtrace();
				die( 'Nomes funciona amb 2 id_fields' );
			}

			$this->id_fields_fixed    = $this->id_fields[0];
			$this->id_fields_variable = $this->id_fields[1];
		}

		$this->set_fields();

	}

	public function set_other_fields( $other_fields ) {
		if ( $other_fields ) {
			$this->other_fields = explode( ',', $other_fields );
		}
		$this->set_fields();
	}

	public function set_fixed_value( $val ) {
		$this->id_fields_fixed_value = $val;
	}

	private function set_fields() {
		$this->fields = array_merge( $this->id_fields, $this->other_fields );
	}

	/*
	 * Es pot pasar config del modul
	 * O array('text','date','currency')
	 */
	public function set_config( $config ) {

		// si es array multidimensional, es la del mòdul
		if ( is_array( $config ) && is_array( current( $config ) ) ) {
			$this->is_module_config = true;
			$this->config           = $config;
		}
		else {
			$this->config = func_get_args();
		}
	}


	public function get_imploded( $field, $condition, $glue = ',', ...$condition_vars ) {

		if ( $condition_vars ) {
			foreach ( $condition_vars as &$condition_var ) {
				$condition_var = Db::qstr( $condition_var );
			}
			$condition = sprintf( $condition, ...$condition_vars );
		}

		$results = $this->get( $field, $condition );
		return implode_field( $results, $field, $glue );
	}

	/**
	 * @param string $fields Per defecte tots, sino camps separats per comes
	 * @param bool $condition
	 * <p>Per retornar tot sense limit posar a false</p>
	 *
	 * @param array $condition_vars
	 * <p>Variables per escapar i posar a condition</p>
	 * get ('*',  camp1=% AND camp2=% )
	 *
	 * @return array
	 */
	public function get( $fields = '*', $condition = false, ...$condition_vars ) {

		if ( $condition === '' ) return [];

		$query = "SELECT " . $fields . " FROM " . $this->table;

		// ids separats per comes

		if ( $condition !== false ) $condition = trim( $condition );

		// TODO S'ha de detectar si es un array numeric ?? No es fa enlloc ara
		// Serien els arguments passats així: '1,4,23,45'
		if ( strpos( $condition, ',' ) !== false && is_numeric( str_replace( ',', '', $condition ) ) ) {
			$query .= " WHERE " . $this->id_field . " IN (" . $condition . ")";
		}
		// numeric busco per id
		elseif ( is_numeric( $condition ) ) {
			$query .= " WHERE " . $this->id_field . " = " . $condition . "";
		}
		// busco tot
		elseif ( $condition === false ) {
		}
		// qualsevol altra
		else {

			if ( $condition_vars ) {
				foreach ( $condition_vars as &$condition_var ) {
					$condition_var = Db::qstr( $condition_var );
				}
				$condition = sprintf( $condition, ...$condition_vars );
			}

			$query .= " WHERE " . $condition . "";

		}

		if ( $this->order_by ) {
			$query .= " ORDER BY " . $this->order_by . "";
		}
		if ( $this->limit ) {
			$query .= " LIMIT " . $this->limit . "";
		}

		//Debug::p($query, $condition);
		$this->query = $query;

		return Db::get_rows( $query );

	}

	/** Alias de {@link Query::get}
	 *
	 * @param string $fields
	 * @param bool $condition
	 *
	 * @return array la primera fila de resultat
	 * <p>Molt util per exemple en una consulta tipus: SELECT preu, nom, altres FROM preus WHERE id=5,
	 * retorna nomes la primera fila ja que sabem que només hi haurà un resultat</p>
	 */

	public function get_rows( $fields = '*', $condition = false, ...$condition_vars ) {
		return $this->get( $fields, $condition, ...$condition_vars );
	}


	public function get_rows_c( ...$condition_vars ) {
		if (!$this->get_fields || !$this->get_condition) return false;
		return $this->get( $this->get_fields, $this->get_condition, ...$condition_vars );
	}
	public function get_c( ...$condition_vars ) {
		if (!$this->get_fields || !$this->get_condition) return false;
		return $this->get( $this->get_fields, $this->get_condition, ...$condition_vars );
	}

	/** Alias de {@link Query::get} però retorna només el valor del primer camp del primer rs
	 *
	 * @param string $fields
	 * @param bool $condition
	 * @param array $condition_vars
	 * <p>Variables per escapar i posar a condition</p>
	 * get ('*',  camp1=% AND camp2=% )
	 *
	 * @return string | false Primer camp o false si no hi ha registre
	 * <p>Molt util per exemple en una consulta tipus: SELECT preu FROM preus WHERE id=5,
	 * retorna el preu, no fa falta ni les files, ni els camps</p>
	 */
	public function get_first( $fields = '*', $condition = false, ...$condition_vars ) {
		$rows = $this->get( $fields, $condition, ...$condition_vars );
		return $rows ? current ( $rows[0]) : false;
	}


	public function get_first_c( ...$condition_vars ) {
		if (!$this->get_fields || !$this->get_condition) return false;
		return $this->get_first( $this->get_fields, $this->get_condition, ...$condition_vars );
	}

	/** Alias de {@link Query::get} però retorna només el primer rs
	 *
	 * @param string $fields
	 * @param bool $condition
	 *
	 * @return array
	 */
	public function get_row( $fields = '*', $condition = false, ...$condition_vars ) {

		$rows = $this->get( $fields, $condition, ...$condition_vars );
		return $rows ? $rows[0] : $rows;

	}

	public function get_row_c( ...$condition_vars ) {
		if (!$this->get_fields || !$this->get_condition) return false;
		return $this->get_row( $this->get_fields, $this->get_condition, ...$condition_vars );
	}

	/**
	 * Estableix uns camps i una condició per poder utilitzar-se en els querys sense haver de posar-la cada cop	 *
	 * <br>
	 * Es poden establir els camps i la condició i reutilitzar-ho cada cop amb valors nous	 *
	 * <br>
	 * Cada funció de consulta té la seva funció emparellada per utilitzar amb cache_get:
	 * <ul>
	 * <li>get => get_c</li>
	 * <li>get_rows => get_rows_c</li>
	 * <li>get_row => get_row_c</li>
	 * <li>get_first => get_first_c</li>
	 * </ul>
	 * <br><br>
	 * La diferència amb prepare és que prepare ja deixa la consulta apunt per executar-la, així es pot veure i decidir abans d'haver-la executat ( per exemple a la consola client )
	 *
	 *
	 * <code>
	 *
	 * $q->cache_get("camp1, camp2", "camp = %s AND camp2 = %s");
	 * ...
	 * $q->get_first_c($param_1, param 2);
	 * ...
	 * $q->get_c($altre_param_1, altre_param 2);
	 * ...
	 * $q->get_rows_c($altre_param_1, altre_param 2);
	 *
	 * </code>
	 * @param string $fields
	 * @param string $condition
	 *
	 * @return $this
	 */
	public function cache_get( $fields = '*', $condition ){
		$this->get_fields    = $fields;
		$this->get_condition = $condition;
		return $this;
	}
		/**
	 * @param $row
	 * <p>Array de valors per insertar</p>
	 * <p>Si row es multidimensional amb el mateix format que get_rows, inserta tots els registres</p>
	 *
	 * @return $this|string
	 */
	function insert( $row ) {

		if (empty($row)) {
			trigger_error( 'No es pot fer insert sense valors', E_USER_ERROR );
		}

		$rows         = empty( $row[0] ) ? [ $row ] : $row;
		$whole_values = '';
		foreach ( $rows as $row ) {
			$fields = '`';
			$values = '';
			foreach ( $row as $key => $r ) {
				if ( $this->include_id_field || $key != $this->id_field ) {
					$fields .= "$key`,`";

					if ( $r != 'now()' ) {
						$r = DB::qstr( $r );
					}
					$values .= "$r,";
				}
			}
			$fields       = substr( $fields, 0, - 2 );
			$values       = substr( $values, 0, - 1 );
			$whole_values .= "($values),";

		}
		$whole_values = substr( $whole_values, 0, - 1 );

		$query = "INSERT INTO $this->table
					($fields) 
					VALUES $whole_values";

		$this->query = $query;

		if ($this->is_prepare) {
			$this->is_prepare = false;
			return $query;
		}
		else {
			$this->execute( $query );
			$this->insert_id = Db::insert_id();
			return $this;
		}

	}
	public function prepare_insert( $row ){
		$this->is_prepare = true;
		return $this->insert( $row );
	}
	public function execute_insert( $query ){
		$this->execute( $query );
		$this->insert_id = Db::insert_id();
		return $this;
	}
	public function include_id_field(){
		$this->include_id_field = true;
		return $this;
	}
	public function exclude_id_field(){
		$this->include_id_field = false;
		return $this;
	}


	/**
	 * @param array|string $row Array -> Pars nom-valor per actualitzar al registre. Exclou automàticament el id_field i el posa com a condició al update, per tant es pot passar a l'array o posar-ho al paràmetre $condition ( posar-ho sempre quan hi ha més d'una condició )
	 * <br><br>String -> es posa tal qual, així es pot posar una funció o camp ej. value = value+ 1
	 * @param string|int $condition String -> Si hi ha condició no agafa automàticament la condició del id_field
	 * <br><br>Int -> Utilitza el número directament com a id a actualitzar
	 *
	 * @param array $condition_vars
	 * <p>Variables per escapar i posar a condition</p>
	 * get ('*',  camp1=% AND camp2=% )
	 *
	 * @return $this|string
	 */
	function update( $row, $condition = '', ...$condition_vars ) {

		$values = '';

		if ( $condition ) {

			if ( $condition_vars ) {
				foreach ( $condition_vars as &$condition_var ) {
					$condition_var = Db::qstr( $condition_var );
				}
				$condition = sprintf( $condition, ...$condition_vars );
			}
			else if ( is_numeric( $condition ) ) {
				$condition = "$this->id_field = '$condition'";
			}
		}

		if ( is_string( $row ) ) {
			$values = $row;
		}
		else {
			foreach ( $row as $key => $r ) {
				if ( $key == $this->id_field ) {
					if ( ! $condition ) $condition = "$key = '$r'";
				}
				else {
					if ( $r != 'now()' ) {
						$r = DB::qstr( $r );
					}
					$values .= "$key=$r,";
				}
			}
			$values = substr( $values, 0, - 1 );
		}

		if ( ! $condition ) {
			trigger_error( 'No es pot fer update sense condició', E_USER_ERROR );
		}

		$query = "UPDATE $this->table 
			SET $values
			WHERE $condition";

		if ( $this->is_prepare ) {
			$this->is_prepare = false;
			return $query;
		}
		else {
			$this->query = $query;

			$this->execute( $query );

			return $this;
		}

	}
	public function prepare_update( array $row, $condition = '', ...$condition_vars ){
		$this->is_prepare = true;
		return $this->update( $row, $condition, ...$condition_vars );
	}
	public function execute_update( $query ){
		$this->execute( $query );
		return $this;
	}


	/**
	 * @param string|int $condition String -> Si hi ha condició no agafa automàticament la condició del id_field
	 * <br><br>Int -> Utilitza el número directament com a id a esborrar
	 *
	 * @param array $condition_vars
	 * <p>Variables per escapar i posar a condition</p>
	 * get ('*',  camp1=% AND camp2=% )
	 *
	 * @return $this|string
	 */
	function delete( $condition = '', ...$condition_vars ) {

		$values = '';

		if ( $condition ) {

			if ( $condition_vars ) {
				foreach ( $condition_vars as &$condition_var ) {
					$condition_var = Db::qstr( $condition_var );
				}
				$condition = sprintf( $condition, ...$condition_vars );
			}
			else if ( is_numeric( $condition ) ) {
				$condition = "$this->id_field = '$condition'";
			}
		}

		// Per seguretat, només esborrarem registres si hi ha condició
		if ( ! $condition ) {
			trigger_error( 'No es pot esborrar sense condició', E_USER_ERROR );
		}

		$query = "DELETE FROM $this->table 
			WHERE $condition";

		if ( $this->is_prepare ) {
			$this->is_prepare = false;
			return $query;
		}
		else {
			$this->query = $query;

			$this->execute( $query );

			return $this;
		}

	}
	public function prepare_delete( $condition = '', ...$condition_vars ){
		$this->is_prepare = true;
		return $this->delete( $condition, ...$condition_vars );
	}
	public function execute_delete( $query ){
		$this->execute( $query );
		return $this;
	}



	public function get_related( $id, $given_field, $return_field = false, $fields = false ) {

		$query = "SELECT  ";

		if ( $return_field ) {
			$query .= $return_field;
		}
		elseif ( $fields ) {
			$query .= $fields;
		}
		elseif ( $this->id_fields ) {
			$query .= implode( ',', $this->id_fields );
		}
		else {
			Debug::p( "No s'han definit camps", 'Error' );
			return;
		}

		$query .= " FROM  " . $this->related_table . " WHERE " . $given_field . "='" . $id . "'";

		if ( $return_field ) {
			//Debug::p($query, 'get_related');
			$results = Db::get_rows( $query );
			return implode_field( $results, $return_field );
		}
		else
			return Db::get_row( $query );
	}

	/*
	 * 
	 *  guardo relacionats des de un list_records, amb un checkbox per triar si es relaciona o no ( selected )
	 * 		
	 *  nomes funciona amb 2 id fields
	 *  esborra només els que s'han desmarcat en el llistat, `per tant pot funcionar amb paginació
	 * 
	 * El camp id que es fixe el detecta automaticament, 
	 * ej. tots els extres d'una propietat, propietat_id es sempre igual, guarda els extres d'una propietat
	 * 
	 */
	public function save_related_post( $selected, $delete_others = true ) {

		$this->show_querys = false;

		$selected = R::post( $selected );

		if ( ! $selected ) return;

		$values_arrays = array();
		foreach ( $this->fields as $field ) {
			$values_arrays[ $field ] = R::post( $field );

			// agafo el fixed del get automatic	
			if ( ! $values_arrays[ $field ] ) {
				$values_arrays[ $field ] = R::get( $field );
			}
		}
		// puc definir el valor fixe quan no es passa per get
		if ( $this->id_fields_fixed_value ) $values_arrays[ $this->id_fields_fixed ] = $this->id_fields_fixed_value;

		Debug::p( $this->id_fields_fixed_value, '$this->id_fields_fixed_value' );
		Debug::p( $values_arrays, 'text' );
		foreach ( $selected as $id ) {

			$values = array();
			$conta  = 0;
			foreach ( $this->fields as $field ) {
				$value = is_array( $values_arrays[ $field ] ) ? $values_arrays[ $field ][ $id ] : $values_arrays[ $field ];

				if ( $this->config ) {

					$type = $this->is_module_config ? $this->config[ $field ]['type'] : $this->config[ $conta ];

					$function = 'unformat_' . $type . '_list';
					if ( function_exists( $function ) ) $value = $function( $value );
					else {
						$function = 'unformat_' . $type;
						if ( function_exists( $function ) ) $value = $function( $value );
					}
				}
				Debug::p( $value, 'Value' );
				$values [] = $value;
				$conta ++;
			}

			$this->save_related( $values );
		}

		// esborro la resta	
		// nomes funciona amb 2 id fields
		if ( $delete_others ) {

			$delete_ids = '';
			Debug::p( $this->id_fields_variable, 'text' );
			foreach ( $values_arrays[ $this->id_fields_variable ] as $key => $val ) {

				if ( ! isset( $selected[ $key ] ) ) $delete_ids .= $key . ',';

			}
			$delete_ids = substr( $delete_ids, 0, - 1 );
			if ( $delete_ids ) {
				$query = "DELETE 
								FROM " . $this->related_table . " 
								WHERE " . $this->id_fields_variable . " 
								IN (" . $delete_ids . ") 
								AND	" . $this->id_fields_fixed . "=" . $values_arrays[ $this->id_fields_fixed ];

				$this->execute( $query );
			}

		}

		Debug::add( 'Query querys', $this->querys, 2 );
		$GLOBALS['gl_message'] = $this->messages['list_saved'];
	}

	/*
	 * 
	 * Pot agafar els valors en un array o un a un com a parametres
	 * 
	 * 
	 */
	public function save_related( $values ) {


		$count = func_num_args();

		if ( $count == 1 && is_array( $values ) ) {
			$count = count( $values );
		}
		elseif ( $count > count( $this->fields ) ) {
			die ( 'Estan malament els paràmetres' );
		}
		else {
			$values = func_get_args();
		}
		$this->id_fields_fixed_value = $values[0];

		$count_id_fields    = count( $this->id_fields );
		$count_other_fields = count( $this->other_fields );

		$condition     = " WHERE  ";
		$insert_fields = "";
		$insert        = "";
		$update        = "";

		for ( $i = 0; $i < $count_id_fields; $i ++ ) {

			$condition .= $this->id_fields[ $i ] . "='" . $values[ $i ] . "'";

			if ( $i != $count_id_fields - 1 ) {
				$condition .= ' AND ';
			}
			// l'ultim és el segon
			else {
				$this->id_fields_variable_values .= $values[ $i ] . ',';
			}

		}

		for ( $i = 0; $i < $count; $i ++ ) {

			$insert_fields .= $this->fields[ $i ];
			$insert        .= Db::qstr( $values[ $i ] );

			if ( $i != $count - 1 ) {
				$insert_fields .= ',';
				$insert        .= ',';
			}
		}

		for ( $i = 0; $i < $count_other_fields; $i ++ ) {

			$update .= $this->other_fields[ $i ] . "=" . Db::qstr( $values[ $i + $count_id_fields ] ) . "";

			if ( $i != $count_other_fields - 1 ) {
				$update .= ' ,';
			}
		}

		$this->querys .= "
			SELECT count(*) FROM " . $this->related_table . $condition;

		if (
		Db::get_first( "
			SELECT count(*) FROM " . $this->related_table . $condition
		)
		) {
			// si hi ha més camps per fer update, si nomes hi ha els de id no cal fer res
			if ( $update ) {
				$query = "
					UPDATE " . $this->related_table . "
					SET 
					" . $update . $condition;

				$this->querys .= "\n" . $query;
				$this->execute( $query );
			}

		}
		else {

			$query = "
				INSERT INTO " . $this->related_table . " (" . $insert_fields . ") 
				VALUES (" . $insert . ");";

			$this->querys .= "\n" . $query;

			$this->execute( $query );
		}

		if ( $this->show_querys ) {
			Debug::add( 'Query querys', $this->querys, 2 );
			$GLOBALS['gl_message'] = $this->messages['list_saved'];
		}

	}

	/*
	 * 
	 * Esborra tots els registres que no s'han actualitzat o insertat
	 * 
	 * El primer camp es fixe i el segon variable
	 * 
	 * 
	 */
	public function delete_others() {

		Debug::p( $this->id_fields_variable_values, 'id_fields_variable_values' );
		if ( ! $this->id_fields_variable_values ) return;
		// si no s'ha actualitzat cap, es borren tots
		if ( $this->id_fields_variable_values ) {
			$this->id_fields_variable_values = substr( $this->id_fields_variable_values, 0, - 1 );
		}
		else {
			$this->id_fields_variable_values = 0;
		}

		$query = "DELETE 
						FROM " . $this->related_table . " 
						WHERE " . $this->id_fields_variable . " 
						NOT IN (" . $this->id_fields_variable_values . ") 
						AND	" . $this->id_fields_fixed . "=" . $this->id_fields_fixed_value;

		if ( $this->delete_condition ) {
			$query .= " AND " . $this->delete_condition;
		}

		$this->execute( $query );
		Debug::p( $query, 'text' );
		Debug::add( 'Query querys', $this->querys, 2 );
	}

	private function execute( $query ) {
		Db::execute( $query );
		$this->querys .= '<br>' . $query;
	}
}
