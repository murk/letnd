<?
	// Aquesta funció es crida cada cop que es carrega un mòdul a INVOICE
	function client_on_load (&$module){

		Main::load_class( 'invoice', 'common', 'admin' );

		InvoiceCommon::init();
		InvoiceCommon::add_page_files();

	}
?>