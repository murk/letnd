

var booking = {
	start: true,
	date_in: false,
	date_out: false,
	painted: {},
	b_original_class: false,
	inicialized: false,
	month_width:0,
	month_loaded:{},
	month_loaded_count:0,
	first_month_viewing:'',
	paint_class:'selected',
	unpaint_class:'booked',
	caller: '',
	attr:'',	
	is_season_calendar:false,
	property_id: 0,
	is_set_booking: false,
	init: function (){
	
		if (this.inicialized) return;
		
		// fixo la mida per quan es carreguin mes mesos
		this.month_width = $('.mes-holder').width();
		
		$('#book-calendar').width($('#book-calendar').width());
		$('.mes-holder').width(this.month_width);
		
		$('.mes-holder').each(function( index ) {
			if (!booking.first_month_viewing) booking.first_month_viewing = this.id;
			booking.month_loaded[this.id] = true;
			booking.month_loaded_count++;	
		});
		
		this.inicialized = true;
	},
	set_booking: function(book_id,date_in,date_out,season_id,old_season_id){
		
		this.is_set_booking = true;
		this.caller = 'free';
		
		if (typeof(season_id)=='undefined') season_id = false;
		if (typeof(old_season_id)=='undefined') old_season_id = false;
		
		// hem editat un registre, hem de despintar tots els que tenen data-book-id
		if (!this.date_in && !this.date_out){
			$('[data-book-id='+book_id+']').each(
				function(){
					// no te en compte first-free i last-free, ara no cal
					var new_class = $(this).attr('class');
					new_class = new_class.replace('middle-booked','free');
					new_class = new_class.replace('first-booked','free');
					new_class = new_class.replace('last-booked','free');
					
					if (season_id)
						new_class = new_class.replace('season'+old_season_id,'');
					
					$(this).attr('class',new_class);
				}
			);
		}		
		else{
			this.paint_days(false);
			
			this.date_in = false;
			this.date_out = false;
		}
		this.attr = book_id;
		
		if(season_id) {
			this.paint_class = 'booked season' + season_id;
		}
		else {
			this.paint_class = 'booked';
		}
		this.unpaint_class = 'selected';
		
		
		var date = date_in.split('/');
		var dia = date[2];
		var mes = date[1];
		var any = date[0];
				
		var id = '#'+dia+'-'+mes+'-'+any;
		this.set_day(dia,mes,any,'b');
				
		var date = date_out.split('/');
		var dia = date[2];
		var mes = date[1];
		var any = date[0];
				
		var id = '#'+dia+'-'+mes+'-'+any;
		this.set_day(dia,mes,any,'a');
		
		// resetejo per poder tornar a reservar de nou
		this.paint_class = 'selected';
		this.unpaint_class = 'booked';
		
		this.date_in = false;
		this.date_out = false;		
		
		this.is_set_booking = false;
		
	},
	cancel_date_out: function (){
		
		var dia = this.date_in.getDate();
		var mes = this.date_in.getMonth()+1;
		var any = this.date_in.getFullYear();
		
		this.do_action(dia,mes,any,'b');
		
		
	},
	show_form_edit: function(book_id, id){
		
	},
	show_form_new: function(formated_date_in,formated_date_out){
		
		if (this.is_set_booking) return;
		
		if (this.is_season_calendar){
			
		}
		else{		
			id = bookingform.id;
			$('#selected-dates').html(formated_date_in+' - '+formated_date_out);
			$('#date_in_'+id).val(formated_date_in);
			$('#date_out_'+id).val(formated_date_out);
			
			bookingform.get_price();
		}
	},
	do_action: function (dia, mes, any, half, book_id){
		
		var id = '#'+dia+'-'+mes+'-'+any;
		if (typeof(book_id)=='undefined') book_id = false;
		
		if (
			$(id+'-'+half).hasClass('disabled')
		){
			return false;
		}
		else if  (
			$(id+'-a,' +id+'-b').hasClass('middle-booked') ||
			$(id+'-'+half).hasClass('first-booked') ||
			$(id+'-'+half).hasClass('last-booked')
		){	
			this.caller = 'booked';
			if (!this.date_in) this.show_form_edit(book_id, id+'-'+half);
		}
		else if (
			$(id+'-a,' +id+'-b').hasClass('middle-selected') ||
			$(id+'-'+half).hasClass('first-selected') ||
			$(id+'-'+half).hasClass('last-selected')		
		){
			this.caller = 'selected';	
			this.set_day (dia, mes, any, half, id)	
		}
		else{
			this.caller = 'free';
			this.set_day (dia, mes, any, half, id)
		}
		dp('Caller', this.caller);
		
	},
	set_day: function (dia, mes, any, half, id){
		
		
		var other_half = half=='a'?'b':'a';
		
		if (this.date_in && this.date_out) this.paint_days(false);
		
		//book_id = document.theForm.book_id_javascript.value;
		
		var date_entered = new Date(any,mes-1,dia);	
		
		
		if (!this.date_in){
		
			
			// marco el primer click
			//if (!this.date_out){		
			
			if (this.caller != 'booked'){	
				
				this.date_in = date_entered;
				
				this.painted['a'] = {'id':id+'-a','old_class':$(id+'-a').attr('class')};
				this.painted['b'] = {'id':id+'-b','old_class':$(id+'-b').attr('class')};
											
				// el mig que he clicat el marco perque ja se que no es booked				
				$(id+'-'+half).addClass('middle-' + this.paint_class);
				$(id+'-'+half).removeClass('middle-' + this.unpaint_class);
				$(id+'-'+half).removeClass((half=='a'?'first':'last')+'-free');	
				$(id+'-'+half).removeClass('free');
						
				// l'altre mig el marco si no es booked	
				if (
					!$(id+'-'+other_half).hasClass('first-booked') &&
					!$(id+'-'+other_half).hasClass('last-booked')
					)
				{
					
					$(id+'-'+other_half).addClass('middle-' + this.paint_class);
					$(id+'-'+other_half).removeClass('middle-' + this.unpaint_class);
					$(id+'-'+other_half).removeClass((other_half=='a'?'first':'last')+'-free');	
					$(id+'-'+other_half).removeClass('free');
				}
			}
		}
		else {
			// quan clico sobre un dia marcat el desmarco i resetejo tot
			if  (date_entered.getTime() === this.date_in.getTime()){	
				$(this.painted.a.id).attr('class',this.painted.a.old_class);
				$(this.painted.b.id).attr('class',this.painted.b.old_class); 
				this.date_in = false;		
				this.cancel_paint();
				
			}
			else if  (date_entered < this.date_in){	
				this.date_out = this.date_in;		
				this.date_in = date_entered;		
			}
			else {	
				this.date_out = date_entered;			
			}
			
		}
		
		if (this.date_in && this.date_out) {
			
			
			if (this.paint_days('test')) {
				
				// restauro classes del primer dia clicat
				$(this.painted.a.id).attr('class',this.painted.a.old_class);
				$(this.painted.b.id).attr('class',this.painted.b.old_class);
				
				this.paint_days(this.paint_class);				
				
			}
			else {	
			
				if (this.date_in == date_entered) this.date_in = this.date_out;
				this.cancel_paint();
			}
			
		}
	},
	paint_days: function (paint){
	
		var conta = 1;
		var formated_date_in = '';
		for (var d = new Date(this.date_in.valueOf()); d <= this.date_out; d.setDate(d.getDate() + 1)) {
		
			var dia = d.getDate();
			var mes = d.getMonth()+1;
			var any = d.getFullYear();
			
			var id = '#'+dia+'-'+mes+'-'+any+'-a';
			var id2 = '#'+dia+'-'+mes+'-'+any+'-b';
			
			// defineixo classes per afegir i treure
			
			var middle_class = 'middle-' + this.paint_class;
			var first_class = 'first-' + this.paint_class;
			var last_class = 'last-' + this.paint_class;
			
			var unmiddle_class = 'middle-' + this.unpaint_class;
			var unfirst_class = 'first-' + this.unpaint_class;
			var unlast_class = 'last-' + this.unpaint_class;
			
			
			// canvio la classes del primer quadre del primer dia
			if (conta==1){
				$(id).removeClass('free');
				if (this.can_be_free(id))
					$(id).addClass('first-free');	
				id=false;
				formated_date_in = dia+'/'+mes+'/'+any;
			}			
			
			// comprobo si es pot reservar
			if (paint=='test'){				
				return this.can_be_free(id);
			}
			// pinto els dies
			else if (paint){			
				
				
				if (id){
					this.painted[id] = {'id':id,'old_class':$(id).attr('class')};
					$(id).addClass( middle_class );
					$(id).removeClass(unmiddle_class);
					$(id).removeClass('free');
					
					if (this.attr) $(id).attr('data-book-id',this.attr);
				}
				if (id2){
					this.painted[id2] = {'id':id2,'old_class':$(id2).attr('class')};
					
					$(id2).removeClass('free');
					if (this.attr) $(id2).attr('data-book-id',this.attr);
					
					if (!id){
						$(id2).addClass( first_class );
						$(id2).removeClass( unfirst_class );
					}
					else{
						$(id2).addClass( middle_class );
						$(id2).removeClass( unmiddle_class );
					}
				}
			}
			// desselecciono els dies
			else {
				if (id){
					$(id).attr('class',this.painted[id].old_class);
				}
				if (id2){
					$(id2).attr('class',this.painted[id2].old_class);
				}
			}				
			conta++;
		}
		
				
		if (paint==='test'){
		
		}
		//
		// He seleccionat una franja
		//
		else if (paint){	
			// canvio la classes de l'ultim dia	
			$(id).removeClass( middle_class );			
			$(id).addClass( last_class );
			$(id).removeClass( unlast_class );
			
			$(id2).removeClass( middle_class );	
			if (this.can_be_free(id2))
				$(id2).addClass('last-free');
			
			if (this.attr) $(id2).attr('data-book-id','');
			
			formated_date_out = dia+'/'+mes+'/'+any;
			
			this.show_form_new(formated_date_in,formated_date_out);
		}
		else{
			this.date_in = this.date_out = false;
		}
		
		return true;
		
	},
	cancel_paint: function (){
		this.date_out = false;
		//this.date_in = this.date_out = this.a_original_class = this.b_original_class = false;		
	},
	can_be_free: function(id){
		return !( $(id).hasClass('first-booked') || $(id).hasClass('last-booked') || $(id).hasClass('middle-booked')) ;
	},
	show_month: function(direction,property_id){
	
		var viewing = this.first_month_viewing.split('-');
		
		var viewing_month = viewing[0];
		var viewing_year = viewing[1];
		var month=0;
		var year=0
		
		if (direction == 'next'){
			month = Number(viewing_month)+1;
			year = viewing_year;
			if (month>12) {
				month = 1;
				year++;
			}			
			this.first_month_viewing = month + '-' + year;
			
			month = Number(viewing_month)+3;
			year = viewing_year;
			if (month>12) {
				month = month-12;
				year++;
			}
		}
		if (direction == 'prev'){
			month = viewing_month-1;
			year = viewing_year;
			if (month<1) {				
				month = 12;
				year--;
			}
			this.first_month_viewing = month + '-' + year;
		}
		//dp(month + '-' + year);
		if (typeof(this.month_loaded[month + '-' + year])=='undefined'){
		
			var link = this.is_season_calendar?'season':'book';
			link = '/booking/book/get_month/?process='+link+'&month='+month+'&year='+year+'&property_id='+property_id+'&language='+gl_language;
			
			$.ajax({
				url: link
			})
				.done(function(data) {
				
					if (direction=='next'){
						$('#book-calendar-tr').append(data);
					}
					else{
						$('#book-calendar-tr').prepend(data);
						$('#book-calendar-table').css('margin-left',-booking.month_width);
					}
					$('.mes-holder').width(booking.month_width);
					
					booking.month_loaded[month + '-' + year] = true;
					booking.month_loaded_count++;
					
					$('#book-calendar-table').width(booking.month_width*booking.month_loaded_count);
					
					booking.animate_month(direction);	
					
				});
			
		}
		else{
			booking.animate_month(direction);
		}
		
	},
	animate_month: function(direction){
	
		var dir = direction=='next'?-1:1
		var obj = {};
		obj['marginLeft'] =  dir*this.month_width + parseInt($('#book-calendar-table').css('margin-left'));
				
		$('#book-calendar-table').stop().animate(obj, 500);
	}
}



var bookingform = {

	id: 0,
	price_book: 0,
	price_extras: 0,
	price_total: 0,
	promcode_discount: 0,
	old_date_in: false,
	old_date_out: false,
	season_prices:{}, 
	date_message_error: '',
	too_many_person: '',
	old_adult: 0,
	old_child: 0,
	old_baby: 0,
	person: 0,
	js_string: '',
	init: function(){
		
		
		this.old_adult = $('#adult_' + this.id).val();
		this.old_child = $('#child_' + this.id).val();
		this.old_baby = $('#baby_' + this.id).val();
		
		$('#adult_' + this.id + ',#child_' + this.id + ',#baby_' + this.id).change(
			function(){
				bookingform.check_person();
				bookingform.get_price();
			}
		);
		
		$('[name^="extra_book_selected"],[name^="quantity"]').click(
			function(){
				bookingform.get_price_extras();
			}
		);
		$('[name^="quantity"]').change(
			function(){
				bookingform.get_price_extras();
			}
		);
		$( "#login_mail,#login_password" ).keypress(function(event) {
			if ( event.which == 13 ) {
				event.preventDefault();
			}
		});
		$('#promcode').blur(
			function(){
				bookingform.get_price();
			}
		);
		$('#promcode-button').click(
			function(){
				bookingform.get_price();
			}
		);
		
	},
	get_price: function(){
		
		var property_id = $('#property_id_' + this.id).val();
		var date_in = $('#date_in_' + this.id).val();
		var date_out = $('#date_out_' + this.id).val();
		var adult = $('#adult_' + this.id).length?$('#adult_' + this.id).val():'1';
		var child = $('#child' + this.id).length?$('#child_' + this.id).val():'';
		var baby = $('#baby' + this.id).length?$('#baby_' + this.id).val():'';
		var promcode = $('#promcode').length?$('#promcode').val():'';
		
		$.ajax({
				url: '/' + gl_language + '/booking/book/get_price_ajax/'
						+'?date_in=' + date_in 
						+'&date_out='+date_out
						+'&book_id='+this.id
						+'&adult='+adult
						+'&child='+child
						+'&baby='+baby
						+'&property_id='+property_id
						+'&promcode='+promcode,
				dataType: 'json'
			})
			.done(function(data) {
			
				var errors = '';
			
				
				if (data.promcode_error) {					
					errors += '\n' + data.promcode_error;
					$('#promcode_error').html(data.promcode_error);
				}
				else if (data.promcode_discount){
					$('#promcode_form').html('');
					$('#promcode_tr').show();
					bookingform.promcode_discount = data.promcode_discount;
					$('#promcode_discount').html('-' + format_currency(Math.round(data.promcode_discount*100)) + ' €');
					bookingform.set_price_total();
				}
				
				if (data.error) {
					if (data.new_date_in){
						$('#date_in_' + bookingform.id).val(data.new_date_in);
						$('#date_out_' + bookingform.id).val(data.new_date_out);
					}
					else{
						$('#date_in_' + bookingform.id).val(bookingform.old_date_in);
						$('#date_out_' + bookingform.id).val(bookingform.old_date_out);					
					}
					booking.cancel_date_out();
					errors += '\n' + data.error;
				}
				else if (data.price_book !== false){
				
					bookingform.price_book = data.price_book;
					$('#price_book').html(format_currency(Math.round(data.price_book*100)) + ' €');
					
					bookingform.price_tt = data.price_tt;
					$('#price_tt').html(format_currency(Math.round(data.price_tt*100)) + ' €');

					// Ho agafa dels extres de la pàgina i no per ajax
					// bookingform.price_extras = data.price_extras;
					// $('#price_extras').html(format_currency(data.price_extras * 100) + ' €');

					bookingform.set_price_total();
					
					bookingform.old_date_in = date_in;
					bookingform.old_date_out = date_out;
				}
				
				if (errors) alert(errors);
			});
	},
	get_price_extras: function(){
		var price = 0;
		$('[name^="extra_book_selected"]').each(
			function(){
				if (this.type =='hidden' || $(this).is(":checked")) {
					
					if ($('[name="quantity\['+this.value+'\]"]').length){
						quantity = $('[name="quantity\['+this.value+'\]"]').val();
					}
					else {
						quantity = 1;
					}

					price += Math.round(Number($(this).attr('data-price'))*100*quantity);
				}				
			}
		);
		this.price_extras = price/100;
		$('#price_extras').html(format_currency(price) + ' €');
		bookingform.set_price_total();
	},
	set_price_total: function(){
		var price = Math.round(this.price_book*100) + Math.round(this.price_extras*100) + Math.round(this.price_tt*100);
		price -= Math.round(this.promcode_discount*100);
		$('#price_total').html(format_currency(price) + ' €');
	},
	check_person: function (){
		
		var adult = $('#adult_' + this.id).val();
		var child = $('#child_' + this.id).val();
		var baby = $('#baby_' + this.id).val();
		
		if ( (Number(adult)+Number(child)) > this.person ){
		
			alert(this.too_many_person_error);			
			
			$('#adult_' + this.id).val(this.old_adult);
			$('#child_' + this.id).val(this.old_child);
			$('#baby_' + this.id).val(this.old_baby);
			
			return false;
		}
		else{
			this.old_adult = adult;
			this.old_child = child;
			this.old_baby = baby;
			return true;
		}
	},
	login: function(){
	
		$.ajax({
				url: '/' + gl_language + '/booking/book/login_ajax/',
				type: 'POST',
				dataType: 'json',
				data: { 
					'login_mail': $('#login_mail').val(), 
					'login_password': $('#login_password').val()
				}
			})
			.done(function(data) {
				if (data.is_logged){
					$('#book-custumer').html(data.custumer_form);

					$('#theForm').attr('onsubmit',"if ( bookingform.submit() ) return validar(this" + data.custumer_form_js_string + bookingform.js_string + "); else return false;");

					bookingform.get_price();
				}
				else{
					if (data.message) alert(data.message);
				}
			});
	},
	submit: function(){
		id  = bookingform.id;
		
		date_in = $('#date_in_'+id).val();
		date_out = $('#date_out_'+id).val();
		
		if (!date_in || !date_out){
			alert(bookingform.date_message_error);
			return false;
		}

			return true;

	},
	edit_custumer: function(ordre){	
		var link = $('#selected_custumer_id_' + ordre).val();
		
		if (link!='0'){
			link = '/' + gl_language + '/custumer/custumer/show-form-edit/custumer_id/' + link + '/?return-url=' + encodeURIComponent(window.location.href);
			document.location.href = link;
		}
	}
	
}

var custumer = {
	is_login_down: false,
	open_login: function (){	
			
		if (this.is_cart_down) this.open_cart();
		
		if (this.is_login_down){
			$('#login-submenu').slideUp(200);
			$('#login-submenu-mobile').slideUp(200);
			$('#login-link').removeClass('down');
		}
		else{
			$('#login-submenu').slideDown(200);
			$('#login-submenu-mobile').slideDown(200);
			$('#login-link').addClass('down');		
		}	
		this.is_login_down = !this.is_login_down;
	},
	show_password: function (sufix){
		if (!sufix) sufix = ''; 
		$('#password_form' + sufix).toggle(200);
		$('#mail_password' + sufix).focus();
		$("#login_form" + sufix).toggle(200);
	},
	logout: function(){
	
		link = '/'+gl_language+'/custumer/custumer/logout/';
		
		$.ajax({
			url: link
		})
			.done(function(data) {
			
				location.reload();	
				
			});
	}
}


function format_currency(currency){
	
	currency = Math.round(currency)
	ret_curr = currency.toString();
	
	if (currency>99999){
		coma_index = ret_curr.indexOf(',');
		if (coma_index==-1) coma_index = ret_curr.length;
		punt_index = coma_index-5;
		ret_curr = ret_curr.substr(0,punt_index) + '.' + ret_curr.substr(punt_index)
	}
	if (currency>99){
		punt_index = ret_curr.length-2;
		ret_curr = ret_curr.substr(0,punt_index) + ',' + ret_curr.substr(punt_index);
	}
	else if (currency>9){
		ret_curr = '0,' + ret_curr;
	}
	else{
		ret_curr = '0,0' + ret_curr;
	}
	
	return ret_curr;
}