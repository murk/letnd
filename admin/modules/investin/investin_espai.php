<?
/**
 * InvestinEspai
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2011
 * @version $Id$
 * @access public
 */
/*
 *
 * INFO AL guardar, es crea nova zona a API o es canvia el nom de la zona a API
 *
 * INFO Les zones s'eliminen a API i INVEST si no tenen immobles
 * INFO A espais, quan es en blanc no surt a públic, però quan es zero si
 *
 *
 * */


class InvestinEspai extends Module {
	var $form_tabs;
	function __construct() {
		parent::__construct();
	}

	function on_load(){
		$this->form_tabs = array('0' => array('tab_action'=>'show_form_map','tab_caption'=>$this->caption['c_edit_map']));

		parent::on_load();
	}

	function list_records() {
		$GLOBALS['gl_content'] = $this->get_records();
	}

	function get_records() {
		$listing = new ListRecords( $this );

		$listing->add_button ('edit_map', 'action=show_form_map','boto1','after');
		$listing->order_by = 'zone ASC';

		return $listing->list_records();
	}

	function show_form() {
		$GLOBALS['gl_content'] = $this->get_form();
	}

	function get_form() {
		$show = new ShowForm( $this );

		$show->form_tabs = $this->form_tabs;

		$show->set_form_level1_titles( 'de_figueres', 'xarxa_aigua', 'superficies', 'parcela_minima', 'associacio_empresaris', 'altres', 'a_ap7', 'a_figueres_tren', 'a_aeroport_barcelona', 'a_port_palamos', 'transport_public' );

		$show->set_form_level2_titles( 'a_figueres_tren', 'a_girona_tren', 'altres_transport_ferrocarril' );

		$show->call( 'records_walk', '', true );

		return $show->show_form();
	}

	function records_walk() {
		$ret         = array();
		$ret['zone'] = 'Zona';

		return $ret;
	}

	function save_rows() {

		if ( $this->action == 'save_rows_delete_selected' ) {
			$query_ids = array();
			foreach ( $_POST["selected"] as $key ) {
				$query_ids[ $key ] = R::id( $key, false, false );
			}
			$notify_ids = $this->get_delete_ids( $query_ids );
		}

		$save_rows = new SaveRows( $this );
		$save_rows->save();

		if ( ! empty( $notify_ids ) ) {
			$save_rows->notify( $notify_ids );
		}
	}

	function write_record() {
		if ( $this->action == 'delete_record' ) {
			$query_ids        = array();
			$id               = R::id( $this->id_field, false, '_POST' );
			$query_ids[ $id ] = $id;
			$notify_ids       = $this->get_delete_ids( $query_ids );
		}

		$writerec = new SaveRows( $this );

		$zone_id = $this->write_zone_api( $writerec );
		$writerec->set_value( 'zone_id', $zone_id );


		if ( empty ( $notify_ids ) ) {
			$writerec->save();
		} else {
			$writerec->notify( $notify_ids );
		}
	}

	function manage_images() {
		$image_manager = new ImageManager( $this );
		$image_manager->form_tabs = $this->form_tabs;
		$image_manager->execute();
	}

	/**
	 * @param $writerec
	 *
	 * @return int
	 */
	private function write_zone_api( $writerec ) {

		include_once( DOCUMENT_ROOT . 'clients/investin/functions.php' );
		investin_connect_inmo();

		$zone        = Db::qstr( $writerec->get_value( 'zone', 'cat' ) );
		$zone_id     = $writerec->get_value( 'zone_id' );
		$municipi_id = $writerec->get_value( 'municipi_id' );

		if ( $this->action == 'add_record' ) {
			$query = "
				INSERT
				INTO inmo__zone (municipi_id, zone)
				VALUES ('$municipi_id',$zone);";
			Db::execute( $query );
			$zone_id = Db::insert_id();
		} elseif ( $this->action == 'save_record' ) {
			$query = "
				UPDATE inmo__zone
				SET municipi_id = '$municipi_id',
				zone = $zone
				WHERE zone_id = '$zone_id'";
			Db::execute( $query );
		}

		Db::reconnect();

		return $zone_id;
	}

	/**
	 * @param $query_ids
	 *
	 * @return array
	 */
	private function get_delete_ids( $query_ids ) {

		include_once( DOCUMENT_ROOT . 'clients/investin/functions.php' );

		$zone_ids = $notify_ids = array();
		foreach ( $query_ids as $id ) {
			$zone_id = Db::get_first(
				"SELECT zone_id FROM investin__espai WHERE espai_id = '$id'"
			);

			$zone_ids [ $zone_id ] = $id;
		}

		investin_connect_inmo();

		// Comprobo les zones i espais que es poden borrar
		$query_related = 'SELECT DISTINCT zone_id
							FROM inmo__property
							WHERE zone_id IN (' . implode( ',', array_keys( $zone_ids ) ) . ')';
		$results       = Db::get_rows( $query_related );

		// Desmarco les zones que no es poden borrar
		foreach ( $results as $rs ) {
			$notify_ids[] = $zone_ids[ $rs['zone_id'] ];
			$this->unset_post_selected( $zone_ids[ $rs['zone_id'] ] );
			unset ( $zone_ids[ $rs['zone_id'] ] );
		}

		// Borro les zones que es poden borrar a API
		foreach ( $zone_ids as $zone_id => $val ) {
			$query = "DELETE FROM inmo__zone WHERE zone_id = $zone_id";
			Db::execute( $query );
		}
		Db::reconnect();

		return $notify_ids;
	}

	private function unset_post_selected( $espai_id ) {
		foreach ( $_POST["selected"] as $key => $val ) {
			if ( $val == $espai_id ) {
				unset( $_POST["selected"][ $key ] );

				return;
			}
		}
	}





	function show_form_map (){ //esenyo el napa
		global $gl_news;

		$rs = Db::get_row('SELECT * FROM investin__espai WHERE espai_id = ' . $_GET['espai_id']);
		$rs['title'] = Db::get_first('SELECT zone FROM investin__espai_language  WHERE espai_id = ' . $_GET['espai_id'] . ' AND language = LANGUAGE');
		$rs['address'] = Db::get_first('SELECT municipi FROM inmo__municipi  WHERE municipi_id = ' . $rs['municipi_id']);
		$rs['is_default_point'] = ($rs['latitude']!=0)?0:1;
		$rs['latitude1']= substr($rs['latitude'],0,strpos($rs['latitude'],';'));
		$rs['longitude1']=substr($rs['longitude'],0,strpos($rs['longitude'],';'));
		$rs['center_latitude']=$rs['center_latitude']!=0?$rs['center_latitude']:GOOGLE_CENTER_LATITUDE;
		$rs['center_longitude']=$rs['center_longitude']!=0?$rs['center_longitude']:GOOGLE_CENTER_LONGITUDE;
		$rs['zoom']=$rs['zoom']?$rs['zoom']:GOOGLE_ZOOM;
		$rs['menu_id'] =  $_GET['menu_id'];
		$rs['id'] =  $rs[$this->id_field];
		$rs['id_field'] =  $this->id_field;
		$this->set_vars($rs);//passo els resultats del array
		$this->set_file('investin/espai_map.tpl'); //crido el arxiu
		$this->set_vars($this->caption); //passo tots els captions
		$gl_news = $this->caption['c_click_map'];
		$content = $this->process();//processo
		$GLOBALS['gl_content'] = $content;
	}
	function save_record_map() {
		global $gl_page;
		extract( $_POST );
		//Debug::p( $_POST );

		if ( $latitude && $longitude ) {

			$query = "UPDATE `investin__espai` SET
				`latitude` = '$latitude',
				`longitude` = '$longitude',
				`center_latitude` = '$center_latitude',
				`center_longitude` = '$center_longitude',
				`zoom` = '$zoom'
				WHERE `espai_id` =$espai_id
				LIMIT 1";
			//Debug::p( $query );
			Db::execute ($query) ;
			$ret ['message'] = $this->messages['point_saved'];
		}
		else {
			$ret ['message'] = $this->messages['point_not_saved'];
		}

		Debug::p_all();
		die (json_encode($ret));

	}
	function save_rows_map_points(){
		global $gl_page;
		$espai_id = $_GET['espai_id'];
		$points = trim($_POST['points']);
		$points = explode(';',$points);

		$latitudes = array();
		$longitudes = array();
		$error = false;

		foreach ($points as $point){
			if ($point){
				$p = explode(',',trim($point));

				$la =trim($p[0]);
				$lo = trim($p[1]);

				if (is_numeric($la) && is_numeric($lo)
				    && $la!='' && $lo!=''
				    && count($p)==2) {
					$latitudes[] =$la;
					$longitudes[] = $lo;
				}
				else{
					$error=true;
					break;
				}
			}
		}
		if ($error){
			$this->map_points_error();
		}

		$center_latitude = $latitudes[0]; // centro en el primer punt
		$center_longitude = $longitudes[0]; // centro en el primer punt

		$latitudes = implode (';',$latitudes);
		$longitudes = implode (';',$longitudes);

		debug::p($latitudes);
		debug::p($longitudes);

		if ($latitudes && $longitudes){
			Db::execute ("UPDATE `investin__espai` SET
				`latitude` = '$latitudes',
				`longitude` = '$longitudes',
				`center_latitude` = '$center_latitude',
				`center_longitude` = '$center_longitude'
				WHERE `espai_id` =$espai_id
				LIMIT 1") ;

			//$gl_page->show_message($this->messages['point_saved']);
			$_SESSION['message'] = $this->messages['point_saved'];
			Debug::p_all();
			print_javascript('parent.window.location.replace(parent.window.location);');
			die();
		}
		else{
			$this->map_points_error();
		}
	}
	function map_points_error(){
		global $gl_page;
		$gl_page->show_message('Hi ha un error en l\'entrada de coordenades, si us plau comproveu que estigui ben formada');
		Debug::p_all();
		die();
	}

}

?>