var map = null;
var polyLine;
var g = google.maps;	
var gl_latlng;
var one_point_marker;

var polyOptions = {
		strokeColor: "#f33111",
		strokeOpacity: 0.8,
		strokeWeight: 4
};

var initMap = function(mapHolder) {

	var mapOptions = {
		zoom: gl_zoom,
		center: gl_latlng, 
		mapTypeId: g.MapTypeId.HYBRID,
		draggableCursor: 'auto',
		draggingCursor: 'move',
		disableDoubleClickZoom: true,
		mapTypeControlOptions: {style: google.maps.MapTypeControlStyle.DROPDOWN_MENU}
	};
	map = new g.Map(document.getElementById(mapHolder), mapOptions);
};

var initPolyline = function() {	
	polyLine = new g.Polyline(polyOptions);
	polyLine.setMap(map);
};

var set_one_point_marker = function(point){
	if (markers.length != 1) {
		delete_one_point_marker();
	}
	else{
		create_one_point_marker(markers[0].getPosition());
	}
}
var create_one_point_marker = function(point){
	one_point_marker = new g.Marker({
		position: point,
		map: map
	});
}

var set_polyline_points = function(){

	var latitudes = gl_latitude.split(';');
	var longitudes = gl_longitude.split(';');
	
	if (latitudes.length==1) {
		var latlng = new google.maps.LatLng(latitudes[0] , longitudes[0]);
		create_one_point_marker(latlng);
	}
	else{
		for (var m = 0; m < latitudes.length; m++) {
			if (latitudes[m]) {
				var latlng = new google.maps.LatLng(latitudes[m] , longitudes[m]);				
				var path = polyLine.getPath();
				path.push(latlng);
			}
		}
	}
	m = null;
};

window.onload = function() {
	if (typeof gl_center_latitude === 'undefined') return;
	gl_latlng = new g.LatLng(gl_center_latitude , gl_center_longitude);
	initMap('google_map');
	initPolyline();
	set_polyline_points(); 
};
