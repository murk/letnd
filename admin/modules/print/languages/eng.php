<?php
// menus eina
define('PRINT_MENU_KIND','Tipus de contracte');
define('PRINT_MENU_KIND_NEW','Entrar nou tipus');
define('PRINT_MENU_KIND_LIST','Llistar tipus');
define('PRINT_MENU_CONTRACT','Contractes');
define('PRINT_MENU_CONTRACT_NEW','Entrar nou contracte');
define('PRINT_MENU_CONTRACT_LIST','Llistar contractes');
define('PRINT_MENU_CONTRACT_BIN','Paperera de reciclatge');
define('PRINT_MENU_SHOWWINDOW', 'Aparadors');
define('PRINT_MENU_SHOWWINDOW_LIST', 'Llistar finques');


// captions seccio
$gl_caption_contract['c_property_id'] = 'Immoble';
$gl_caption_contract['c_ref'] = 'Ref. immoble';
$gl_caption_contract['c_property_private'] = 'Nom privat';
$gl_caption_contract['c_category_id'] = 'Tipus immoble';
$gl_caption_contract['c_signed'] = 'Signat';
$gl_caption_contract['c_registered'] = 'Data de compraventa';
$gl_caption_contract['c_registered2'] = 'Compraventa';
$gl_caption_contract['c_rescinded'] = 'Rescindit';
$gl_caption_contract['c_duration'] = 'Duració';
$gl_caption_contract['c_months'] = 'mesos';
$gl_caption_contract['c_days'] = 'dies';
$gl_caption_contract['c_years'] = 'anys';
$gl_caption_contract['c_kind_id'] = 'Tipus';
$gl_caption_contract['c_language_id'] = 'Idioma';
$gl_caption_contract['c_state'] = 'Estat';
$gl_caption_contract['c_current'] = 'Vigent';
$gl_caption_contract['c_canceled'] = 'Rescindit';
$gl_caption_contract['c_ipc'] = 'Ipc';
$gl_caption_contract['c_ipc2'] = 'dies per aplicar el pròxim ipc';
$gl_caption_contract['c_ipc3'] = 'Ipc aplicat';
$gl_caption_contract['c_ipc4'] = 'Ipc en';
$gl_caption_contract['c_expires'] = 'Expira';
$gl_caption_contract['c_expires2'] = 'dies per caducar el contracte';
$gl_caption_contract['c_expires3'] = 'Expira en';
$gl_caption_contract['c_contract_id'] = 'Ref.';
$gl_caption_contract['c_select_property_link'] = 'Seleccionar immoble...';
$gl_caption_contract['c_add_property_link'] = 'Entrar nou immoble...';
$gl_caption_contract['c_select_buyers_link'] = 'Seleccionar comprador - llogater...';
$gl_caption_contract['c_add_buyers_link'] = 'Entrar nou comprador - llogater...';
$gl_caption_contract['c_buyers'] = 'Compradors - Llogaters';
$gl_caption_contract['c_owner_title'] = 'Propietaris';
$gl_caption_contract['c_select_owners_link'] = 'Seleccionar propietari...';
$gl_caption_contract['c_add_owners_link'] = 'Entrar nou propietari...';
$gl_caption_contract['c_contract_title'] = 'Dades del contracte';
$gl_caption_contract['c_contract_title_list'] = 'Contracte';
$gl_caption_contract['c_filter_kind_id'] = 'Tots els tipus';
$gl_caption_contract['c_contract_observations'] = 'Observacions';
$gl_caption_contract['c_print_contract_button']='Imprimir contracte';
$gl_caption_contract['c_place']='Lloc de signatura';
$gl_caption_contract['c_diposit']='Dipòsit (€)';
$gl_caption['c_error_contract']='Error! Imposible imprimir contracte, falten dades';

$gl_caption_contract ['c_custumer_id']='Nom client';
$gl_caption_contract ['c_phone'] ='Telèfon';

$gl_caption_kind['c_kind'] = 'Tipus';


$gl_caption_showwindow['c_land'] = 'Plot';
$gl_caption_showwindow['c_municipi_id'] = 'Situation';
$gl_caption_showwindow['c_bathroom'] = 'Bathrooms';
$gl_caption_showwindow['c_wc'] = 'Toilets';
$gl_caption_showwindow['c_garage_area'] = '%s m<sup>2</sup>';
$gl_caption_showwindow['c_storage_area'] = '%s m<sup>2</sup>';
$gl_caption_showwindow['c_terrace_area'] = '%s m<sup>2</sup>';
$gl_caption_showwindow['c_garden_area'] = '%s m<sup>2</sup>';
$gl_caption_showwindow['c_swimmingpool_area'] = '%s m<sup>2</sup>';
$gl_caption_showwindow['c_2garage_area'] = '%s m2';
$gl_caption_showwindow['c_2storage_area'] = '%s m2';
$gl_caption_showwindow['c_2terrace_area'] = '%s m2';
$gl_caption_showwindow['c_2garden_area'] = '%s m2';
$gl_caption_showwindow['c_2swimmingpool_area'] = '%s m2';
$gl_caption_showwindow['c_bathroom_singular'] = ' bathroom';
$gl_caption_showwindow['c_bathroom_plural'] = ' bathrooms';
$gl_caption_showwindow['c_wc_singular'] = ' toilet';
$gl_caption_showwindow['c_wc_plural'] = ' toilets';

?>