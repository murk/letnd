
<div style="visibility:hidden;height:0px;"><iframe id="contacte"  src="/?tool=contact&amp;tool_section=contact&amp;action=show_form_new&amp;language=<?=LANGUAGE?>&amp;template=clean"></iframe></div>
<div class="pageResults">
  <div class="left">
    <?=$split_results_count?>
  </div>
  <div class="right">
    <?=$split_results?>
  </div>
</div>
<? foreach($loop as $l): extract ($l)?>
  <div class="list_item">
    <div class="left">
      <div class="ref"><a href="<?=$show_record_link?>" class="ref">
        <?=$c_ref?>
        <?=$ref?>
        </a> </div>
      <? if ($property):?>
        <div class="property">
          <?=$property?>
        </div>
        <? endif //property?>
      <? if ($property_title):?>
        <div class="property_title">
          <?=$property_title?>
        </div>
        <? endif //property_title?>
      <? if ($description):?>
        <div class="description">
          <?=$description?>
        </div>
        <? endif //description?>
      <? if ($price):?>
        <div class="price">
          <?=$c_price?>
          : <span>
          <?=$price?>
          <?=CURRENCY_NAME?>
          </span> </div>
        <? endif //price?>
      <? if ($price_consult):?>
        <div class="price">
          <?=$price_consult?>
        </div>
        <? endif //price_consult?>
      <div class="icons"><img src="/public/themes/plantilla4/images/mail.gif" alt="email" width="76" height="57" onclick="open_contact('<?=$id?>','<?=$ref?>')" /><a href="<?=$show_record_link?>"><img src="/public/themes/plantilla4/images/info.gif" alt="info" width="51" height="57" /></a></div>
    </div>
<? if($images||$status||$offer){?>
    <div class="images_list"<? if(!$images){ echo'style="border: 20px #FFFFFF solid;"';}//endif //images?>><? if ($status):?><div class="sold"><img alt="<?=$status?>" src="<?=DIR_TEMPLATES?>images/sold_<?=LANGUAGE?>.png" width="162" height="73" /></div><? endif //status?><? if (!$status):?><? if ($offer):?><div class="sold"><img alt="<?=$offer?>" src="<?=DIR_TEMPLATES?>images/offer_<?=LANGUAGE?>.png" width="162" height="73" /></div><? endif //offer?><? endif //status?><? foreach($images as $image): extract ($image)?><a href="<?=$show_record_link?>"><img src="/<?=CLIENT_DIR?>/inmo/property/images/<?=$image_name_thumb?>" width="230" alt="<?=htmlspecialchars($property)?>" /></a><? break; //images?><? endforeach //images?></div><div class="cls"></div>
<? }//endif //images?>
  </div>
   <div id="contacte_<?=$id?>" class="contacte"></div>
  <? endforeach //$loop?>
<div class="pageResults">
  <div class="left">
    <?=$split_results_count?>
  </div>
  <div class="right">
    <?=$split_results?>
  </div>
</div>
