function show(option, field)
{
	document.getElementById(field+'text').style.display='none';
	document.getElementById(field+'textarea').style.display='none';
	document.getElementById(field+'select').style.display='none';
	document.getElementById(field+'checkbox').style.display='none';
	document.getElementById(field+'config_select').style.display='none';

	switch (option)
	{
        case 'text':
        case 'password':
        case 'email':
        case 'int':
        case 'decimal':
        case 'account':
        case 'nif':
		case 'names':
			document.getElementById(field+'text').style.display='block';
			break;
		case 'textarea':
			document.getElementById(field+'textarea').style.display='block';
			break;
		case 'select':
		case 'radios':
		case 'radios2':
			document.getElementById(field+'select').style.display='block';
			document.getElementById(field+'config_select').style.display='block';
			break;
		case 'checkbox':
			document.getElementById(field+'checkbox').style.display='block';
			break;
        case 'data':
        case 'time':
        case 'hidden':
        case 'html':
			break;
	}
}

function disable_inputs(formu, checkbox, field)
{

	for (x=0;x<formu.elements.length; x++)
	{
	 if (formu.elements[x].name.indexOf(field) != -1)
	 {
	 	formu.elements[x].disabled = !checkbox.checked;
	 }
	}
}

function crear_configuracio(tool, tool_section)
{
	
	if (tool=='qualsevulla' && tool_section=='taulaambpotes'){	
		value = document.theForm.elements[tool + '_' + tool_section].value;	
		tools = value.split('__');
		if (tools.length==1){
		alert('La taula no es correcta, ha d\'haver 2 guions __');
		return;
		}
		tool = tools[0];
		tool_section = tools[1];
		window.location='/admin/?action=show_form_edit&tool2='+tool+'&tool_section2=' + tool_section + '&menu_id=1006&action_previous_list=list_records&table=' + value;
	}
	else{
	window.location='/admin/?action=show_form_edit&tool2='+tool+'&tool_section2=' + tool_section + '&menu_id=1006&action_previous_list=list_records&table=' + tool + '__' + tool_section;		
	}
	
}
function clic_vars(){
	fForm = document.getElementById('theForm');
	vF = fForm.file.value;
	vC = fForm.client.value;
	vL1 = fForm.lang1.value;
	vL2 = fForm.lang2.value;
	window.location = '?menu_id=1033&action=list_records_vars&file=' + vF + '&client=' + vC + '&lang1=' + vL1 + '&lang2=' + vL2;
	return false;
}