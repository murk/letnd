<?header('Content-type: text/html; charset=utf-8');
//error_reporting(0);
define("DOCUMENT_ROOT",$_SERVER["DOCUMENT_ROOT"]?$_SERVER["DOCUMENT_ROOT"]:$_ENV["DOCUMENT_ROOT"]);
$images_title = $_GET['images_title'];
$images_dir = $_GET['images_dir'];
$images = $_GET['images'];
$start = $_GET['start'];
for ($i=0; $i < $start; $i++) {
    array_shift ($images);
}
echo('<?xml version="1.0" encoding="UTF-8"?>');
?>
<images title="" frameColor="0x000000" frameWidth="0" borderColor="0xeae9e9" borderWidth="0" stagePadding="50" imagePadding="120" thumbSelectedWidth="2" enableRightClickOpen="true">
<? foreach($images as $image):
$image = $images_dir . $image;
?>
<image>
   <path>/<?=$image?></path>
   <caption><?=$images_title?></caption>
   <? list($width, $height) = getimagesize(DOCUMENT_ROOT . '/' . $image);?>
   <width><?=$width?></width>
   <height><?=$height?></height>
</image>
<? endforeach //$loop?>
</images>