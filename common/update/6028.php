<?

/* 5-2-15

Actualització utility
Formularis de càlculs de venda i compra imobles

*/

if ( is_table( 'all__menu' ) ) {

	$update_sql .= "
			DELETE FROM `all__menu` WHERE  `menu_id`=10002;
			DELETE FROM `all__menu` WHERE  `menu_id`=10004;
			DELETE FROM `all__menu` WHERE  `menu_id`=10005;

		";

	if ( ! Db::get_first( 'SELECT menu_id FROM all__menu WHERE menu_id = 10006' ) ) {


		// li dono els mateixos permisos traduccions de inmo
		$read_write = Db::get_row( "SELECT read1,write1 FROM all__menu WHERE menu_id = 10001" );
		$read       = $read_write['read1'];
		$write      = $read_write['write1'];

		$update_sql .= "
			INSERT INTO `all__menu` (`menu_id`, `menu_group`, `tool`, `tool_section`, `action`, `parent_id`, `toolmode_id`, `variable`, `ordre`, `link`, `process`, `bin`, `read1`, `write1`) VALUES (10006, 'utility', 'utility', 'buy', 'show_form_new', 10001, 1, 'UTILITY_MENU_BUY', 1, '', '', 0, '$read', '$write');
			INSERT INTO `all__menu` (`menu_id`, `menu_group`, `tool`, `tool_section`, `action`, `parent_id`, `toolmode_id`, `variable`, `ordre`, `link`, `process`, `bin`, `read1`, `write1`) VALUES (10007, 'utility', 'utility', 'sell', 'show_form_new', 10001, 1, 'UTILITY_MENU_SELL', 1, '', '', 0, '$read', '$write');
			UPDATE `all__menu` SET `link`='10006' WHERE `menu_id`=10000;
		";
	}
}

if ( ! is_table( 'utility__configadmin' ) ) {
	$update_sql .= "
		CREATE TABLE `utility__configadmin` (
			`configadmin_id` INT(11) NOT NULL AUTO_INCREMENT,
			`name` VARCHAR(50) NOT NULL DEFAULT '' COLLATE 'utf8_spanish2_ci',
			`value` VARCHAR(255) NOT NULL DEFAULT '' COLLATE 'utf8_spanish2_ci',
			PRIMARY KEY (`name`, `configadmin_id`)
		)
		COLLATE='utf8_spanish2_ci'
		ENGINE=MyISAM;
	";
	$update_sql .= "
		INSERT INTO `utility__configadmin` (`configadmin_id`, `name`, `value`) VALUES (1, 'register_percent', '0.5');
		INSERT INTO `utility__configadmin` (`configadmin_id`, `name`, `value`) VALUES (2, 'notary_percent', '1');
		INSERT INTO `utility__configadmin` (`configadmin_id`, `name`, `value`) VALUES (3, 'mortgage_valuation', '250');
		INSERT INTO `utility__configadmin` (`configadmin_id`, `name`, `value`) VALUES (4, 'mortgage_register_percent', '0.5');
		INSERT INTO `utility__configadmin` (`configadmin_id`, `name`, `value`) VALUES (5, 'mortgage_percent', '80');
		INSERT INTO `utility__configadmin` (`configadmin_id`, `name`, `value`) VALUES (6, 'mortgage_open_percent', '0.5');
		INSERT INTO `utility__configadmin` (`configadmin_id`, `name`, `value`) VALUES (7, 'mortgage_notary_percent', '1');
		INSERT INTO `utility__configadmin` (`configadmin_id`, `name`, `value`) VALUES (8, 'mortgage_length', '25');
		INSERT INTO `utility__configadmin` (`configadmin_id`, `name`, `value`) VALUES (9, 'mortgage_initial_interest', '2.7');
		INSERT INTO `utility__configadmin` (`configadmin_id`, `name`, `value`) VALUES (11, 'mortgage_cancel_percent', '0.5');
		INSERT INTO `utility__configadmin` (`configadmin_id`, `name`, `value`) VALUES (12, 'habitability_card', '200');
		INSERT INTO `utility__configadmin` (`configadmin_id`, `name`, `value`) VALUES (13, 'estate_commission_percent', '3');
		INSERT INTO `utility__configadmin` (`configadmin_id`, `name`, `value`) VALUES (14, 'efficiency', '150');
		INSERT INTO `utility__configadmin` (`configadmin_id`, `name`, `value`) VALUES (15, 'agency_percent', '0');
	";
} else {
	trigger_error( 'Ja existeixen la taula utility__configadmin' );
}
?>