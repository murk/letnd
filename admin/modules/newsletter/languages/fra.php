<?
// menus eina
if (!defined('NEWSLETTER_MENU_CUSTUMER')){
define('NEWSLETTER_MENU_CUSTUMER','Adresses');
define('NEWSLETTER_MENU_CUSTUMER_LIST','Listes des adresses');
define('NEWSLETTER_MENU_CUSTUMER_REMOVED','Adresses terminés');
define('NEWSLETTER_MENU_CUSTUMER_NEW','Insérez la adresse');
define('NEWSLETTER_MENU_CUSTUMER_BIN','Corbeille');

define('NEWSLETTER_MENU_GROUP','Groupe');
define('NEWSLETTER_MENU_GROUP_NEW','Insérez groupe');
define('NEWSLETTER_MENU_GROUP_LIST','Énumérer groupes');


define('NEWSLETTER_MENU_MESSAGE','Mail');
define('NEWSLETTER_MENU_MESSAGE_SEND','Envvoyer mail');
define('NEWSLETTER_MENU_MESSAGE_SEND_SAVED','Brouillons');
define('NEWSLETTER_MENU_MESSAGE_SEND_FROM_HISTORY','Messages postés');
define('NEWSLETTER_MENU_MESSAGE_NOT_SENT','Messages non envoyés');
define('NEWSLETTER_MENU_MESSAGE_BIN','Corbeille');

define('NEWSLETTER_MENU_TEMPLATE','Modèles');
define('NEWSLETTER_MENU_TEMPLATE_NEW_TEXT','Insérez modèle de texte');
define('NEWSLETTER_MENU_TEMPLATE_LIST_TEXT','Énumérez modèles de texte');
define('NEWSLETTER_MENU_CONFIGADMIN','Configuration');



define('NEWSLETTER_CONFIGADMIN_MUST_CONFIGURE','Avant de envoyer le courrier doit configuerer la application');
define('NEWSLETTER_GROUP_MUST_INSERT','Vous devez créer au moins un groupe à insérer des adresses e-mail');

define('NEWSLETTER_MAIL_SEND_ONLY_TESTING','Exécution en mode de test, il enverra pas de message');
define('NEWSLETTER_MAIL_SEND_SENDING','Début du processus de soumission');
define('GETTING_ADDRESSES','On obtient les adresses e-mail...');
define('GOT_ADDRESSES','Adresses obtenues!<br>');
define('GETTING_MESSAGE','Générant messages...');
define('GETTING_MESSAGE_ONE','Générant un message...');
define('GOT_MESSAGE','Messages générés!<br>');
define('GOT_MESSAGE_ONE','Message généré!');
define('SENDING_MESSAGE','Envoi de messages ...<br>');
define('SENDING_MESSAGE_ONE','Envoi du message à:');
define('MESSAGE_SENT','Messages envoyés!');
define('MESSAGE_NOT_SENT','Messages non envoyés!');
define('NO_MESSAGE_SENT','Nessun message envoyé!');
define('MESSAGE_SENT_ONE_A','<b>Message </b>');
define('MESSAGE_SENT_ONE_B','<b>envoyé à :</b>');
define('MESSAGE_NOT_SENT_ONE_A','<b>Le message </b>');
define('MESSAGE_NOT_SENT_ONE_B','<b> n\'a pas pu être envoyé: </b>');
define('MESSAGE_SAVED_NOT_SENT','<b>Le message a été enregistré dans \"' . NEWSLETTER_MENU_MESSAGE_NOT_SENT . '\"</b><br>');
define('MESSAGE_SAVED_NOT_SENT_ONE','<b>Le message a été enregistré dans \"' . NEWSLETTER_MENU_MESSAGE_NOT_SENT . '\"</b><br>');
define('MESSAGE_SAVED_NOT_SENT_TEST','<b>Le message a été enregistré dans \"' . NEWSLETTER_MENU_MESSAGE_SEND_SAVED . '\"</b><br>');
define('MESSAGE_QUEUE_STOPPED',"Cola de envio detenida, esperando per poder enviar ... ");
}

// captions seccio
$gl_caption_custumer['c_mail'] = 'Adresse Mail';
$gl_caption_custumer['c_name'] = 'Prénom';
$gl_caption_custumer['c_surname1'] = 'Première Nom';
$gl_caption_custumer['c_surname2'] = 'Duxième Nom';
$gl_caption['c_group_id'] = 'Group';
$gl_caption_custumer['c_vat'] = 'DNI/NIF/NIE';
$gl_caption_custumer['c_phone'] = 'Téléphone';
$gl_caption_custumer['c_phone1'] = 'Tel. 1';
$gl_caption_custumer['c_phone2'] = 'Tel. 2';
$gl_caption_custumer['c_phone3'] = 'Fax';
$gl_caption_custumer['c_adress'] = 'Rue';
$gl_caption_custumer['c_numstreet'] = 'Nombre';
$gl_caption_custumer['c_block'] = 'Bloc';
$gl_caption_custumer['c_flat'] = 'Appartement';
$gl_caption_custumer['c_door'] = 'Porte';
$gl_caption_custumer['c_town'] = 'Ville';
$gl_caption_custumer['c_province'] = 'Province';
$gl_caption_custumer['c_country'] = 'Pays';
$gl_caption_custumer['c_bankdata'] = 'Références bancaires';
$gl_caption_custumer['c_bank'] = 'Caisse / Banque';
$gl_caption_custumer['c_comta'] = 'IBAN';
$gl_caption_custumer['c_account_holder'] = 'Titulaire';
$gl_caption_custumer['c_bic'] = 'BIC';
$gl_caption_custumer['c_mail'] = 'Email';
$gl_caption_custumer['c_zip'] = 'Code';
$gl_caption_custumer['c_observations1'] = 'Commentaires';
$gl_caption_custumer['c_surnames'] = 'Noms de famille';
$gl_caption_custumer['c_address'] = 'Adresse';

$gl_caption_custumer['c_action'] = 'Action';
$gl_caption_custumer['c_edit'] = 'Modifier';
$gl_caption_custumer['c_delete_selected'] = 'Annuler la sélection';
$gl_caption_custumer['c_restore_selected'] = 'Récupérer la sélection';
$gl_caption_custumer['c_confirm_deleted'] = 'Les éléments sélectionnés seront supprimés définitivement de la base de données \\n\\nVoulez-vous continuer?';
$gl_caption_custumer['c_select_addresses'] = 'Cliquez sur une adresse de la liste pour le sélectionner
';

$gl_caption_custumer['c_search'] = 'Chercher une adresse';
$gl_caption_custumer['c_groups'] = 'Filtrer par groupe';
$gl_caption_custumer['c_selected'] = 'Adresse sélectionnée:';
$gl_caption_custumer['c_move_to_group_id'] = 'Moure al grup';
$gl_caption_custumer['c_enteredc']= 'Crée';
$gl_caption_custumer['c_group_caption']= '[Aucun groupe]';
$gl_caption_custumer['c_select_all_groups'] = 'Tous les groupes';
$gl_caption_custumer['c_prefered_language'] = 'Langue';

$gl_caption_custumer['c_user_id'] = 'Agent';
$gl_caption_custumer['c_filter_user_id'] = 'Tous les agents';
$gl_caption ['c_user_id_format']='%s %s';
$gl_caption_custumer['c_category_id'] = 'Demande';
$gl_caption_custumer['c_filter_category_id'] = 'Toutes les demandes';

$gl_caption_custumer['c_filter_prefered_language'] = 'Toutes les langues';


$gl_messages_custumer['select_search_no_records']='Aucune adresse contenant: ';
$gl_messages_custumer['select_address']='Cliquez sur une adresse de la liste pour le sélectionner';


$gl_caption_group['c_group_name'] = 'Groupe';
$gl_caption_group['c_description'] = 'Description';
$gl_caption_group['c_button_addresses'] = 'Voir les adresses';


$gl_caption_message['c_message'] = 'Message';
$gl_caption_message['c_message_body'] = 'Corps du message';
$gl_caption_message['c_language_id'] = 'Langue';
$gl_caption_message['c_send_to_same_language_only'] = "Enviar sólo a clientes con este idioma";
$gl_caption_message['c_send_to_no_language_too'] = "Enviar también a clientes sin idioma establecido";
$gl_caption_message['c_templatehtml_id'] = 'Modèle';
$gl_caption_message['c_templatetext_id'] = 'Modèle';
$gl_caption_message['c_from_address'] = 'E-mail';
$gl_caption_message['c_from_address_repeat'] = 'Répetéz e-mail';
$gl_caption_message['c_from_name'] = 'Prénom';
$gl_caption_message['c_reply_to'] = 'E-mail';
$gl_caption_message['c_reply_to_repeat'] = 'Répetez e-mail';
$gl_caption_message['c_reply_to_name'] = 'Prénom';
$gl_caption_message['c_subject'] = 'Sujet';
$gl_caption_message['c_format'] = 'Format';
$gl_caption_message['c_send_method'] = 'Méthode d\'expédition';
$gl_caption_message['c_CC'] = 'Copiez chaque direction (CC)';
$gl_caption_message['c_BCC'] = 'opiez chaquer chaque adresse(BCC)';
$gl_caption_message['c_one_by_one'] = 'Un message différent pour chaque adresse';
$gl_caption_message['c_addresses_options'] = 'Options';
$gl_caption_message['c_from_title'] = 'De';
$gl_caption_message['c_reply_to_title'] = 'Réponse';
$gl_caption_message['c_send'] = 'Envoyer message';
$gl_caption_message['c_save'] = 'Enregistrer des brouillons';
$gl_caption_message['c_text'] = 'text';
$gl_caption_message['c_html'] = 'html';
$gl_caption_message['c_picture'] = 'Images';
$gl_caption_message['c_send_caption'] = 'Target';
$gl_caption_message['c_send_to_addresses'] = 'Envoyer à sélectionnée';
$gl_caption_message['c_select_addresses'] = 'Sélectionnez adresses...';

$gl_caption_message['c_select_property'] = 'Envoyer propriétés';
$gl_caption_message['c_select_inmo_property_link'] = 'Sélectionnez les propriétés...';
$gl_caption_message['c_select_inmo_property_list_records'] = 'Liste';
$gl_caption_message['c_select_inmo_property_show_record'] = 'Dossier';

$gl_caption_message['c_select_product'] = 'Envoyer des produitss';
$gl_caption_message['c_select_product_product_link'] = 'Choisir des produits ...';
$gl_caption_message['c_select_product_product_list_records'] = 'Liste';
$gl_caption_message['c_select_product_product_show_record'] = 'Dossiers';
$gl_caption_message['c_select_inmo_property_show_record'] = 'Dossiers';

$gl_caption_message['c_select_new'] = 'Envoyez des nouvelles';
$gl_caption_message['c_select_news_new_link'] = 'Sélectionnez nouvelles ...';
$gl_caption_message['c_select_news_new_list_records'] = 'Liste';
$gl_caption_message['c_select_news_new_show_record'] = 'Dossiers';

$gl_caption_message['c_group_id'] = 'Envoyer au groupe';
$gl_caption_message['c_preview'] = 'Avant-première';
$gl_caption_message['c_text_options'] = 'Les options de configuration suivantes sont prises automatiquement, mais vous pouvez changer ce message';
$gl_caption_message['c_file'] = 'Attacher un fichier';
$gl_caption_message['c_content_extra'] = 'Contenu inclus';
$gl_caption_message['c_entered'] = 'Crée';
$gl_caption_message['c_modified'] = 'Modifié';
$gl_caption_message['c_template_caption']='[pas de modèle]';
$gl_caption_message['c_messages_sent'] = 'Expédié';
$gl_caption['c_sent'] = 'Expédié';
$gl_caption['c_sendstart'] = $gl_caption_message['c_sent'] = 'Expédié';
$gl_caption_message['c_messages_sent_message'] = 'de';
$gl_caption_message['c_edit_button'] = 'Éditer et envoyer';
$gl_caption_message['c_not_send_button'] = 'Envoyer autres';
$gl_caption_message['c_re_send_button'] = 'Modifier et renvoyer';


$gl_caption_configadmin = $gl_caption_message;
$gl_caption_configadmin['c_templatehtml_id'] = 'Modèle en html';
$gl_caption_configadmin['c_templatetext_id'] = 'Modèle de text';
$gl_caption_configadmin['c_group_1'] = 'De';
$gl_caption_configadmin['c_group_2'] = 'Répondre';
$gl_caption_configadmin['c_group_3'] = 'Données SMTP';
$gl_caption_configadmin['c_m_username'] = 'Nom d\'utilisateur';
$gl_caption_configadmin['c_m_password'] = 'Mot de passe';
$gl_caption_configadmin['c_m_password_repeat'] = 'Répéter mot de passe';
$gl_caption_configadmin['c_m_host'] = 'Serveur';
$gl_caption_configadmin['c_only_test'] = 'Mode test';
$gl_caption_configadmin['c_only_test_description'] = 'Si vous cochez cette case, n\'envoyez pas de courrier, il ne simule présentationt';
$gl_caption_configadmin['c_custumer_table'] = 'Table d\'adresses';
$gl_caption_configadmin['c_newsletter__custumer'] = 'newsletter__custumer';
$gl_caption_configadmin['c_custumer__custumer'] = 'custumer__custumer';
$gl_caption_configadmin['c_product__customer'] = 'product__customer';


$gl_caption_templatetext['name'] = 'Nom';
$gl_caption_templatetext['header'] = 'Head';
$gl_caption_templatetext['footer'] = 'Pied';
$gl_caption_templatetext['send'] = 'Envoyer';

// en el cos del mail
$gl_caption_message['c_remove']['cat'] = 'Si no desitges rebre més missatges d\'aquesta llista de correu pots donar-te de baixa <a href="%s">aquí</a>';
$gl_caption_message['c_remove']['spa'] = 'Si no deseas recibir más mensajes de esta lista de correo puedes darte de baja <a href="%s">aquí</a>';
$gl_caption_message['c_remove']['fra'] = 'Si vous ne désirez pas recevoir d\'autres messages de cette liste de diffusion, vous pouvez vous désabonner <a href="%s">ici</a>';
$gl_caption_message['c_remove']['eng'] = 'If you do not wish to continue receiving mails from our newsletter, you may unsubscribe <a href="%s">here</a>';

$gl_caption_customer = $gl_caption_custumer;

$gl_caption_message['c_select_agenda'] = 'Enviar activitats';
$gl_caption_message['c_select_llemena_agenda_link'] = 'Seleccionar activitats ...';
$gl_caption_message['c_select_llemena_agenda_list_records'] = 'llistat';
$gl_caption_message['c_select_llemena_agenda_show_record'] = 'fitxes';

$gl_caption_message['c_select_activitat'] = 'Enviar activitats';
$gl_caption_message['c_select_lassdive_activitat_link'] = 'Seleccionar activitats ...';
$gl_caption_message['c_select_lassdive_activitat_list_records'] = 'llistat';
$gl_caption_message['c_select_lassdive_activitat_show_record'] = 'fitxes';

$gl_caption_custumer['c_delete_button'] = 'Supprimer';
$gl_caption_custumer['c_group_sent'] = 'Envoyée';
$gl_caption_custumer['c_group_not_sent'] = 'No envoyée';
$gl_messages['deleted_not_sent'] = 'La direction a été supprimé de la liste de destinataires';



$gl_caption['c_import_preview'] = 'Previsualizar importación';
$gl_caption['c_import'] = 'Importar';
$gl_caption['c_import_title'] = 'Importar direcciones';
$gl_caption['c_select_file'] = 'Seleccionar archivo';
$gl_caption['c_group'] = 'Grupo';

$gl_caption['c_max_sends_reached'] = "Se ha alcanzado el máximo de emails enviados, enviar más podría ser motivo de SPAM";
$gl_caption['c_max_sends_reached_2'] = "Se aconseja enviar el resto en un mínimo de 2 horas";
$gl_caption['c_max_sends_reached_button'] = "Enviar igualmente el resto ahora";