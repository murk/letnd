<?php
global $gl_caption_demand_property, $gl_caption_demand;
// menus eina
if (!defined('CUSTUMER_MENU_CUSTUMER')) {
	define('CUSTUMER_MENU_CUSTUMER','Gestió de clients');
	define('CUSTUMER_MENU_NEW','Entrar nou client');
	define('CUSTUMER_MENU_LIST','Llistar tots els clients');
	define('CUSTUMER_MENU_LIST_INACTIVE','Llistar clients desactivats');
	define('CUSTUMER_MENU_LIST_OWNERS','Propietaris venda');
	define('CUSTUMER_MENU_LIST_RENTERS','Sol·licitants lloguer');
	define('CUSTUMER_MENU_LIST_BUYERS','Sol·licitants compra');
	define('CUSTUMER_MENU_LIST_TENANTS','Propietaris lloguer');
	define('CUSTUMER_MENU_LIST_BOOKERS','Clients turístic');
	define('CUSTUMER_MENU_LIST_OTHERS','Altres');
	define('CUSTUMER_MENU_BIN','Papelera de reciclatge');
	define('CUSTUMER_MENU_','Sol·licituds de clients');
	define('CUSTUMER_MENU_DEMAND','Llistar sol·licituds');
	define('CUSTUMER_MENU_DEMAND_LIST','Llistar sol·licituds');
	define('CUSTUMER_MENU_DEMAND_CHECK','Cerques exactes');
	define('CUSTUMER_MENU_DEMAND_PROX','Cerques aproximatives');
	define('CUSTUMER_SELECT_WINDOW_TILE','Sel·lecionar propietats');
	define('CUSTUMER_MENU_CUSTUMERLIST','Llistar clients');
	define('CUSTUMER_MENU_HISTORY','Historial');
	define('CUSTUMER_MENU_HISTORY_LIST','Llistar historial');
	define('CUSTUMER_MENU_HISTORY_BIN','Papelera de recilatge');
	define('CUSTUMER_MENU_HISTORY_NEW','Nova entrada');

	define('CUSTUMER_MENU_GROUP','Grup');
	define('CUSTUMER_MENU_GROUP_NEW','Insertar grup');
	define('CUSTUMER_MENU_GROUP_LIST','Llistar grups');

	define('MLS_MENU_COMPANIES','MLS');
	define('MLS_MENU_COMPANIES_LIST','Llistar empreses');
	define('MLS_MENU_COMPANIES_YES','Empreses compartides');
}

$gl_caption_custumer['c_activated'] = 'Actiu';
$gl_caption_custumer['c_deactivate'] = 'Desactivar seleccionats';
$gl_caption_custumer['c_activate'] = 'Activar seleccionats';

$gl_caption_custumer['c_file'] = 'Documents';
//captions custumer_buy.tpl
$gl_caption_custumer['c_insert'] ='Tipus de propietat';
$gl_caption_custumer['c_tipus'] ='Tipus de propietat';
$gl_caption ['c_maxprice'] ='Preu màxim';
//captions tpl what.tpl
$gl_caption_custumer['c_add'] = 'Alta propietat';
$gl_caption_custumer['c_add_expand'] = '( El client posa en venta una propietat.) ';
$gl_caption_custumer['c_assign'] = 'Assignar propietat';
$gl_caption_custumer['c_assign_expand'] = '( S\'assigna una propietat existent a aquest client.) ';
$gl_caption_custumer['c_buy'] = 'Sol·licitud de compra';
$gl_caption_custumer['c_buy_expand'] = ' Assignar una sol·licitud de compra. Letnd cercarà periòdicament coincidenciens amb les propietats actuals amb les sol·licituds de compra incluint les propietats MLS ) ';
$gl_caption_custumer['c_custumer'] = 'Client';
$gl_caption_custumer['c_insert']= 'Insertar';
//captions tpl search_custumer.tpl
$gl_caption_custumer['c_search_title'] = 'Cercar client';
$gl_caption_custumer['c_by_nif']= 'Per nif ';
$gl_caption_custumer['c_by_words']= 'Per paraules' ;
//custumer_selected_frame.tpl
$gl_caption_custumer['c_by_ref'] = '(o referències separades per espais, ej: "456 245 45")';
$gl_caption_custumer['c_by_words'] = 'Per paraules';
$gl_caption_custumer['c_selected']='Propietats sel·leccionades';
$gl_caption_custumer['c_selected_customers']='Propietaris sel·leccionats';
$gl_caption_custumer['c_status'] = 'Estat';
$gl_caption_custumer['c_property_title'] = 'Descripció curta';
$gl_caption_custumer['c_description'] = 'Descripció';
$gl_caption_custumer['c_category_id'] = 'Tipus';
$gl_caption_custumer['c_prepare'] = 'En preparació';
$gl_caption_custumer['c_review'] = 'Revisar';
$gl_caption_custumer['c_onsale'] = 'Per vendre';
$gl_caption_custumer['c_sold'] = 'Venut';
$gl_caption_custumer['c_archived'] = 'Arxivat';
$gl_caption['c_property'] = 'Immoble';
$gl_caption_custumer['c_property_private']='Nom privat';
$gl_caption_custumer['c_surnames'] = 'Cognoms';
$gl_caption_custumer['c_treatment'] = 'Tracte';
$gl_caption['c_treatment_sra'] = "Sra.";
$gl_caption['c_treatment_sr'] = "Sr.";
$gl_caption_custumer['c_birthdate'] = 'Data de naixement';
$gl_caption_custumer['c_company'] = 'Empresa';
$gl_caption_custumer['c_job'] = 'Feina';
//captions tpl demands_list.tpl
$gl_caption_custumer_demand['c_result_search']= 'Resultat de la cerca. ';
$gl_caption_custumer['c_demandnumber']= 'Sol·licitud Nº';
//$gl_caption_custumer['c_municipi']= 'Sol·licitud Nº';
$gl_caption_custumer['c_enteredc']= 'Creat';
$gl_caption_custumer['c_entered']= 'Creada';
$gl_caption_custumer['c_historycustumer']='Historial del client';
$gl_caption_custumer['c_add_history']='Afegir entrada';
$gl_caption['c_show_not_show']='Ensenyar / Amagar';
$gl_caption_history['c_filter_tipus']='Totes les accions';
$gl_caption_history['c_various']='Varis';
$gl_caption_history['c_to_call']='A trucar';
$gl_caption_history['c_waiting_call']='Esperant trucada';
$gl_caption_history['c_assembly']='A reunir-se';
$gl_caption_history['c_show_property']='Ensenyar propietat';
$gl_caption['c_like_property']='Interessat en l\'immoble';
$gl_caption['c_send_information']='Informació enviada';
$gl_caption_history['c_office']='Atendre a l\'oficina';
$gl_caption_history['c_offer']='Passar oferta';
$gl_caption_history['c_tipus']='Acciò';
$gl_caption_history['c_name']='Nom';
$gl_caption_history['c_custumer_id']='Client';
$gl_caption_history['c_status']='Estat';
$gl_caption_history['c_filter_status']='Tots els estats';
$gl_caption_history['c_sort_out']='Pendent';
$gl_caption_history['c_finished']='Finalitzat';
$gl_caption_history['c_canceled']='Cancelat';
$gl_caption_history['c_dates']='Dates';
$gl_caption_history['c_date']='Data alta';
$gl_caption_history['c_finished_date']='Data finalització';
$gl_caption['c_no_history']='No hi han entrades a l\'historial del client';
$gl_caption_custumer['c_edit_history']='Historial';
$gl_caption_custumer['c_form_history_title'] = 'Historial del client:';
$gl_caption_custumer['c_edit_history_button']='Editar entrada';

$gl_caption['c_price_history_title'] = 'Historial de preus';
$gl_caption['c_no_price_history'] = "No hi han entrades a l'historial de preus";

// editar dades clients -> llistar demandes
/*$gl_caption_custumer_demand ['c_custumer_id']='Client';
$gl_caption_custumer_demand['c_tipus2'] = 'Tipus d\'obra';
$gl_caption_custumer_demand['c_new'] = 'Obra nova';
$gl_caption_custumer_demand['c_second'] = '2º ma';
$gl_caption_custumer_demand['c_tipus'] = 'Operació';
$gl_caption_custumer_demand['c_sell'] = 'Venda';
$gl_caption_custumer_demand['c_rent'] = 'Lloguer';
$gl_caption_custumer_demand['c_municipi_id'] = 'Municipi';
$gl_caption_custumer_demand['c_garage'] = 'Garatge';
$gl_caption_custumer_demand['c_storage'] = 'Traster';
$gl_caption_custumer_demand['c_terrace'] = 'Terrassa';
$gl_caption_custumer_demand['c_garden'] = 'Jardí';*/

// captions comuns
$gl_caption ['c_custumer_id']='Codi';
$gl_caption_demand ['c_custumer_id']='Nom client';
$gl_caption_demand ['c_demand_id'] ='Sol·licitut';
$gl_caption_demand ['c_phone'] ='Telèfon';

// captions seccio
$gl_caption_custumer['c_prefered_language'] = 'Idioma';
$gl_caption_custumer['c_demand_id']='Sol·licitud';
$gl_caption['c_name'] = 'Nom';
$gl_caption['c_surname1'] = 'Primer Cognom';
$gl_caption['c_surname2'] = 'Segon Cognom';
$gl_caption_custumer ['c_custumer_id']='Codi';
$gl_caption_custumer['c_vat'] = 'Document d\'identitat';
$gl_caption_custumer['c_phone'] = 'Telèfons';
$gl_caption['c_phone1'] = 'Principal';
$gl_caption['c_phone1b'] = 'Principal';
$gl_caption['c_phone2'] = 'Feina';
$gl_caption['c_phone4'] = 'Privat';
$gl_caption['c_phone3'] = 'Fax';
$gl_caption_custumer['c_adress'] = 'Carrer';
$gl_caption_custumer['c_numstreet'] = 'Número';
$gl_caption_custumer['c_block'] = 'Bloc';
$gl_caption_custumer['c_flat'] = 'Pis';
$gl_caption_custumer['c_door'] = 'Porta';
$gl_caption_custumer['c_town'] = 'Població';
$gl_caption_custumer['c_province'] = 'Provincia';
$gl_caption_custumer['c_country'] = 'País';
$gl_caption_custumer['c_bankdata'] = 'Dades bancàries';
$gl_caption_custumer['c_bank'] = 'Caixa / Banc';
$gl_caption_custumer['c_comta'] = 'IBAN';
$gl_caption_custumer['c_account_holder'] = 'Titular';
$gl_caption_custumer['c_billing'] = 'Facturació';
$gl_caption_custumer['c_billing_company'] = 'Empresa';
$gl_caption_custumer['c_billing_citizen'] = 'Particular';
$gl_caption_custumer['c_bic'] = 'BIC';
$gl_caption_custumer['c_vat_type'] = 'Tipus de document';
$gl_caption_custumer['c_vat_type_D'] = 'DNI';
$gl_caption_custumer['c_vat_type_P'] = 'Passaport';
$gl_caption_custumer['c_vat_type_C'] = 'Carnet conduir (només ciutadans espanyols)';
$gl_caption_custumer['c_vat_type_I'] = 'Carta o document identitat';
$gl_caption_custumer['c_vat_type_N'] = 'Permís de residència espanyol';
$gl_caption_custumer['c_vat_type_X'] = 'Permís de residència d\'un altre Estat Membre de la Unió Europea';
$gl_caption_custumer['c_vat_date'] = 'Data expedició';
$gl_caption_custumer['c_vat_num'] = 'Número';
$gl_caption_custumer['c_nationality'] = 'Nacionalitat';
$gl_caption_custumer['c_mail'] = 'Correu electrònic';
$gl_caption_custumer['c_website'] = 'Web';
$gl_caption_custumer['c_zip'] = 'Codi postal';
$gl_caption_custumer['c_observations1'] = 'Observacions';
$gl_caption_custumer['c_observations2'] = 'Observacions (2)';
$gl_caption_custumer ['c_search1']='Cerca per codi';
$gl_caption_custumer ['c_owner']='El client és propietari? ';
$gl_caption_custumer ['c_buyer']='El client és comprador? ';
$gl_caption_custumer ['c_category'] ='Tipus';
$gl_caption_custumer['c_address'] = 'Adreça';

$gl_caption_custumer['c_add_button'] = 'Alta propietat';
//$gl_caption_custumer['c_assign_button'] = 'Assignar propietat';
$gl_caption_custumer['c_demand_button'] = 'Sol·licitud';
$gl_caption_custumer['c_add_demand_link']='Crear nova sol·licitud';
$gl_caption['c_edit_demand_button']='Editar sol·licitud'; // aquest ha de ser gl_caption
$gl_caption_custumer['c_propertyscustumer'] = 'Propietats del client';
$gl_caption_custumer['c_demandscustumer'] = 'Sol·licituds del client';
$gl_caption_custumer['c_bookingcustumer'] = 'Reserves del client';
$gl_caption_custumer['c_bookingcustumer_list'] = 'Llistar reserves';
$gl_caption_custumer['c_ref']='Ref';
$gl_caption['c_edit_property_button']='Editar immoble';
$gl_caption['c_select_property_link']='Assignar immoble';
$gl_caption['c_add_property_link']='Crear nou immoble';
$gl_caption['c_assign_button']='Assignar propietat';
//captions propietari
$gl_caption_custumer ['c_by_code']='Cerca per codi: ';
$gl_caption_custumer_owner ['c_search']='Búscar: ';

$gl_caption ['c_books_button'] = 'Veure reserves';
$gl_caption ['c_searchexact_button'] = 'Cercar exactes';
$gl_caption ['c_searchprox_button'] = 'Cercar aprox.';
$gl_caption_demand ['c_entered'] = 'Creada';
$gl_caption_demand ['c_data_demand'] = 'Dades de la sol·licitud';
$gl_caption_demand ['c_garage_area'] = 'superfície';
//$gl_caption_property ['c_direccio'] = 'Direcció';

$gl_caption_demand ['c_comercialname'] = 'Empresa';
//$gl_caption_demand ['c_custumer_id_format']='<strong>Nom: </strong> %s %s <strong>Tel: </strong> %s';
$gl_caption_demand ['c_0']='-';
$gl_caption_demand ['c_1']='si';
$gl_caption_demand ['c_room_0']='-';
$gl_caption_demand ['c_room_1']='1';

$gl_caption_demand ['c_bathroom_0'] = $gl_caption_demand ['c_wc_0'] = $gl_caption_demand ['c_room_0'] = $gl_caption_demand ['c_living_0'] = $gl_caption_demand ['c_dinning_0'] = $gl_caption_demand ['c_level_count_0'] = $gl_caption_demand ['c_room_0'];
$gl_caption_demand ['c_bathroom_1'] = $gl_caption_demand ['c_wc_1'] = $gl_caption_demand ['c_living_1'] = $gl_caption_demand ['c_dinning_1'] = $gl_caption_demand ['c_level_count_1'] = $gl_caption_demand ['c_room_1'];

$gl_caption_demand_property['c_send_edit'] = 'Assignar sel·leccionats';
$gl_caption_demand_property ['c_0']='-';
$gl_caption_demand_property ['c_1']='si';
$gl_caption_demand_property ['c_room_0']='-';
$gl_caption_demand_property ['c_room_1']='1';

$gl_caption ['c_nodefinit']='no o no definit';
$gl_caption_demand ['c_nodefinit']='no o no definit';
$gl_caption_demand ['c_maxprice'] ='Preu màxim';
$gl_caption_demand['c_resultsmls']= 'Resultats cerca MLS';
$gl_caption_demand['c_observations']= 'Observacions';

$gl_caption_demand['c_phone1']='Telèfon';
$gl_caption_demand['c_client_id']='';
$gl_caption_demand['c_client_dir']='';
$gl_caption_demand['c_domain']='';


$gl_caption_demand['c_search_level_filter'] = 'Nivell d\'aproximació en la cerca';
$gl_caption_demand['c_search_level_1'] = 'Restrictiu';
$gl_caption_demand['c_search_level_2'] = 'Normal';
$gl_caption_demand['c_search_level_3'] = 'Poc restrictiu';

$gl_caption_demand['c_show_all_results'] = 'Mostrar tots els resultats';

$gl_messages['no_records_demands'] = 'No s\'ha trobat cap coïncidència per cap sol·licitud';
$gl_messages['no_records_demand'] = 'No s\'ha trobat cap coïncidència per la sol·licitud';
$gl_messages['no_records_custumer'] = 'No s\'ha trobat cap coïncidència pel client';

$gl_caption ['c_date']='data';

$gl_caption['c_history_title']='Títol';
$gl_caption['c_history']='Història';
$gl_caption_history['c_message_id']='Newsletter';
$gl_caption_history['c_message_id_view']='Reenviar missatge';
$gl_caption_history['c_property_id']='Immoble';

$gl_caption['property_id_caption']='Cap';

$gl_caption_custumer['c_user_id'] = 'Agent';
$gl_caption_custumer['c_filter_user_id'] = 'Tots els agents';
$gl_caption ['c_user_id_format']='%s %s';

$gl_caption_custumer['c_category_id'] = 'Sol·licitud';
$gl_caption_custumer['c_filter_category_id'] = 'Totes les sol·licituds';

$gl_caption_custumer['c_filter_prefered_language'] = 'Totes els idiomes';
$gl_caption_custumer['c_group_id'] = 'Grup';

$gl_caption['c_demand_refused'] = 'Propietat descartada';
$gl_caption['c_demand_reseted'] = 'Propietat readmesa';
$gl_caption['c_price_changed'] = 'Canvi de preu';

$gl_caption_demand['c_refuse_property'] = 'Descartar';
$gl_caption_demand['c_reset_property'] = 'Restablir';
$gl_caption_demand['c_interest_property'] = 'Interessa';
$gl_caption_demand['c_view_property'] = 'Veure immoble';

$gl_caption_demand['c_refuse_property_done'] = "L'inmoble ha estat descartat";
$gl_caption_demand['c_reset_property_done'] = "S'ha restablert l'immoble";
$gl_caption_demand['c_like_property_done'] = "L'inmoble interessa al client";


$gl_caption_custumer['c_marital_status'] = 'Estat civil';
$gl_caption_custumer['c_marital_status_'] = '';
$gl_caption_custumer['c_marital_status_married'] = 'Casat';
$gl_caption_custumer['c_marital_status_divorced'] = 'Divorciat';
$gl_caption_custumer['c_marital_status_separated'] = 'Separat';
$gl_caption_custumer['c_marital_status_single'] = 'Solter';
$gl_caption_custumer['c_marital_status_widower'] = 'Vidu';
$gl_caption_custumer['c_marital_status_commonlaw'] = 'Parella de fet';

$gl_caption_custumer['c_invoice_information']='Dades de facturació';
$gl_caption_custumer['c_is_invoice']='Es facturable';
$gl_caption_custumer['c_comercial_name']='Nom comercial';
$gl_caption_custumer['c_billing_mail']='E-mail de facturació';
$gl_caption_custumer['c_billing_type']='Forma de pagament';
$gl_caption_custumer['c_bank_receipt']='Rebut bancari';
$gl_caption_custumer['c_wire_transfer']='Transferència bancària';
$gl_caption_custumer['c_bank_draft']='Xec bancari';
$gl_caption_custumer['c_cash']='Efectiu';
$gl_caption_custumer['c_invoice_informations']='Observacions de facturació';
$gl_caption_custumer['c_cif']='C.I.F';
?>