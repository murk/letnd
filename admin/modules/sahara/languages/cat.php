<?php
// menus eina
if (!defined('SAHARA_MENU_CIRCUIT')){
	define('SAHARA_MENU_CIRCUIT','Circuits');
	define('SAHARA_MENU_CIRCUIT_NEW','Nou circuit');
	define('SAHARA_MENU_CIRCUIT_LIST','Llistar circuits');
	define('SAHARA_MENU_CIRCUIT_BIN','Paperera de reciclatge');
	define('SAHARA_MENU_PLACE','Llocs');
	define('SAHARA_MENU_PLACE_NEW','Nou lloc');
	define('SAHARA_MENU_PLACE_LIST','Llistar llocs');
	define('SAHARA_MENU_ELEMENT','Inclosos / no inclosos');
	define('SAHARA_MENU_ELEMENT_NEW','Nou ítem');
	define('SAHARA_MENU_ELEMENT_LIST','Llistar ítems');
}
$gl_messages_circuit['no_place']='Per poder insertar circuits hi ha d\'haver almenys una categoria';

// captions seccio

$gl_caption_circuit['c_prepare'] = 'En preparació';
$gl_caption_circuit['c_review'] = 'Per revisar';
$gl_caption_circuit['c_public'] = 'Públic';
$gl_caption_circuit['c_archived'] = 'Arxivat';
$gl_caption_circuit['c_circuit'] = 'Circuit';
$gl_caption_circuit['c_content'] = 'Descripció complerta';
$gl_caption_circuit['c_entered'] = 'Data creació';
$gl_caption_circuit['c_modified'] = 'Data modificació';
$gl_caption_circuit['c_status'] = 'Estat';
$gl_caption_circuit['c_place_id'] = 'Lloc';
$gl_caption_circuit['c_filter_category_id'] = 'Totes les categories';
$gl_caption_circuit['c_filter_status'] = 'Tots els estats';
$gl_caption_circuit['c_filter_in_home'] = 'Totes, home o no';
$gl_caption_circuit['c_ordre'] = 'Ordre';
$gl_caption_circuit['c_url1_name']='Nom URL(1)';
$gl_caption_circuit['c_url1']='URL(1)';
$gl_caption_circuit['c_url2_name']='Nom URL(2)';
$gl_caption_circuit['c_url2']='URL(2)';
$gl_caption_circuit['c_url3_name']='Nom URL(3)';
$gl_caption_circuit['c_url3']='URL(3)';
$gl_caption_circuit['c_videoframe']='Video (youtube, metacafe...)';
$gl_caption_circuit['c_file']='Arxius';
$gl_caption_circuit['c_in_home']='Surt a la Home';
$gl_caption_circuit['c_in_home_0']='No surt a la Home';
$gl_caption_circuit['c_in_home_1']='Surt a la Home';
$gl_caption_circuit['c_content_list']='Descripció curta al llistat';
$gl_caption_circuit['c_price']='Preu';
$gl_caption_circuit['c_price_text']='Preu';

$gl_caption_circuit['c_form_level1_title_page_title']='Eines SEO';

$gl_caption_circuit['c_form_level1_title_circuit']='Descripcions';
$gl_caption_circuit['c_form_level1_title_url1_name']='Enllaços i arxius adjunts';
$gl_caption_circuit['c_place'] = 'Lloc';
$gl_caption_circuit['c_includes'] = 'Inclou';
$gl_caption_circuit['c_excludes'] = 'Exclou';
$gl_caption_circuit['c_edit_dies'] = 'Dies';


$gl_caption_place['c_place'] = 'Llocs';
$gl_caption_place['c_place_description'] = 'Descripció';
$gl_caption_place['c_place_description_list'] = 'Descripció curta al llistat';
$gl_caption_place['c_circuit_button'] = 'Veure circuits';

$gl_caption_element['c_element'] = 'Ítem';

$gl_caption['c_ordre'] = 'Ordre';
$gl_caption['c_circuit_file_name'] = $GLOBALS['gl_caption']['c_file_name'];
$gl_caption['c_circuit_old_file_name'] = $GLOBALS['gl_caption']['c_old_file_name'];



// mapes
$gl_caption['c_remove_polyline']='Esborrar traçat';
$gl_caption['c_search_adress']='Buscar adreça';
$gl_caption['c_latitude']='latitud';
$gl_caption['c_longitude']='longitud';
$gl_caption['c_found_direction']='adreça trobada';
$gl_caption['c_edit_map'] = 'Mapa';
$gl_caption['c_save_map'] = 'Guardar ubicació';
$gl_caption['c_click_map'] = 'Fes un click en un punt o més del mapa per guardar la ubicació';
$gl_caption['c_form_map_title'] = 'Ubicació';
$gl_messages['point_saved'] = 'Nova ubicació guardada correctament';

// dies

$gl_caption_circuit['c_new_dia'] = 'Afegir dia';
$gl_caption['c_edit_dia_button'] = 'Editar dia';
$gl_caption_circuit['c_form_dia_title'] = 'Dies del circuit:';
$gl_caption_circuit['no_assigned'] = $gl_messages_dia['no_records'] = 'No s\'ha entrat cap dia per aquest circuit';
$gl_caption_circuit ['c_delete_button']='Eliminar';
$gl_caption['c_edit_dia'] = 'Dies';

$gl_caption_dia['c_dia_num'] = 'Dia';
$gl_caption_dia['c_dia'] = 'Títol';
$gl_caption_dia['c_dia_content'] = 'Descripció';

$gl_caption_place['c_in_home']='Surt a la Home';
$gl_caption_place['c_in_home_0']='No surt a la Home';
$gl_caption_place['c_in_home_1']='Surt a la Home';

$gl_caption['c_0'] = '';
$gl_caption['c_1'] = '1';
$gl_caption['c_2'] = '2';
$gl_caption['c_3'] = '3';
$gl_caption['c_4'] = '4';
$gl_caption['c_5'] = '5';
$gl_caption['c_6'] = '6';
$gl_caption['c_7'] = '7';
$gl_caption['c_8'] = '8';
$gl_caption['c_9'] = '9';
$gl_caption['c_10'] = '10';
$gl_caption['c_11'] = '11';
$gl_caption['c_12'] = '12';
$gl_caption['c_13'] = '13';
$gl_caption['c_14'] = '14';
$gl_caption['c_15'] = '15';
$gl_caption['c_16'] = '16';
$gl_caption['c_17'] = '17';
$gl_caption['c_18'] = '18';
$gl_caption['c_19'] = '19';
$gl_caption['c_20'] = '20';
?>