<?

/**
 * LassdiveTimetableCommon
 *
 * Funcions comunes per lassdive_timetable i lassdive_reserva_timetable
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2019
 * @version $Id$
 * @access public
 */
class LassdiveTimetableCommon {
	public $positions = [], $start_date, $end_date;

	public function get_hours() {
		static $hours = [];

		if ( $hours ) return $hours;

		$begin_hour    = 7; // hora
		$end_hour      = 22; // hora
		$step          = 15; // minuts
		$begin_quarter = 45;

		$current_hour = $begin_hour;
		while ( $current_hour < $end_hour ) {
			$begin_quarter += $step;
			if ( $begin_quarter == '60' ) {
				$begin_quarter = 0;
				$current_hour ++;
			}
			$begin_quarter_str = $begin_quarter == 0 ? '00' : (string) $begin_quarter;
			$hours []          = "$current_hour:$begin_quarter_str";

		};

		return $hours;
	}

	public function get_busy_hours( $hours, $centres, $product_id, $reservaitem_id, $reservaitem_date ) {

		$ret = [];
		if ( $this->start_date ) {
			$centre_hour_vars['day_timetables'] = [];

			$start_date = new DateTime( $this->start_date );
			$end_date   = new DateTime( $this->end_date );

			$this->create_temp_table( $product_id, $start_date->format( "Y-m-d" ), $end_date->format( "Y-m-d" ) );
		}
		else {
			$this->create_temp_table( $product_id, $reservaitem_date, false );
			$start_date = $end_date = false;

		}

		$no_time_reservas = $this->get_no_time_reservas( $centres, $reservaitem_date, $start_date, $end_date );

		foreach ( $hours as $hour ) {

			$minutes       = substr( $hour, - 2 );
			$is_o_clock    = $minutes == '00';
			$ret [ $hour ] = [
				'hour'       => $is_o_clock ? $hour : $minutes,
				'hour_class' => $is_o_clock ? 'o-clock' : ''
			];
			foreach ( $centres as $centre ) {

				$centre_id           = $centre['centre_id'];
				$centre_max_quantity = $centre['centre_max_quantity'];


				// Setmanes - Només hi haurà setmanes o mesos a vista general
				if ( $this->start_date ) {
					$centre_hour_vars['day_timetables'] = [];

					$interval = DateInterval::createFromDateString( '1 day' );
					$period   = new DatePeriod( $start_date, $interval, $end_date );
					foreach ( $period as $dt ) {
						$reserva_date = $dt->format( "Y-m-d" );

						$centre_hour_vars['day_timetables'][ $reserva_date ] = $this->get_centre_hour( $product_id, $reservaitem_id, $reserva_date, $centre_id, $centre_max_quantity, $hour );

					}
					unset( $period );

				}
				// dies - Només per una reserva d'un producte concret
				else {
					$centre_hour_vars = $this->get_centre_hour( $product_id, $reservaitem_id, $reservaitem_date, $centre_id, $centre_max_quantity, $hour );
				}


				$ret [ $hour ]['center_timetables'] [ $centre_id ] = $centre_hour_vars;

			}
		}

		$this->drop_temp_table();

		return [
			'timetables'       => $ret,
			'no_time_reservas' => $no_time_reservas
		];
	}

	// Nomes agafa reserves sense horari, les que no tenen ni data de reserva no surten enlloc
	public function get_no_time_reservas ( $centres, $reservaitem_date, $start_date, $end_date ) {

		$r = [];
		$at_least_one = false;
		foreach ( $centres as $centre ) {

			$centre_id = $centre['centre_id'];

			if ( $this->start_date ) {
				$r[ $centre_id ]['days'] = [];

				$interval = DateInterval::createFromDateString( '1 day' );
				$period   = new DatePeriod( $start_date, $interval, $end_date );
				foreach ( $period as $dt ) {
					$reserva_date = $dt->format( "Y-m-d" );

					$reserva_items = $this->get_day_reserva_items( $reserva_date, true );
					if (!$at_least_one && $reserva_items) $at_least_one = true;

					$r [ $centre_id ]['days'][ $reserva_date ] =
						[ 'reserva_items' => $reserva_items ];

				}
				unset( $period );
			}
			else {

				$reserva_items = $this->get_day_reserva_items( $reservaitem_date, true );
				if (!$at_least_one && $reserva_items) $at_least_one = true;

				$r [ $centre_id ] =
					[ 'reserva_items' => $reserva_items ];
			}
		}
		return $at_least_one ? $r : [];
	}

	public function get_busy_days( $product_id, $reservaitem_date, $centre_id ) {

		$start_date = new DateTime( $this->start_date );
		$end_date   = new DateTime( $this->end_date );
		$today_date   = new DateTime();
		$today_date_formatted   = $today_date->format( "Y-m-d" );
		$reservaitem_date   = new DateTime( $reservaitem_date );
		$current_month =  $reservaitem_date->format( "n" );

		$this->create_temp_table( $product_id, $start_date->format( "Y-m-d" ), $end_date->format( "Y-m-d" ) );

		$ret = [];

		$interval = DateInterval::createFromDateString( '1 day' );
		$period   = new DatePeriod( $start_date, $interval, $end_date );
		$row      = 0;
		$conta    = 0;
		foreach ( $period as $dt ) {
			$reserva_date = $dt->format( "Y-m-d" );
			$day          = $dt->format( "d" );
			$month = $dt->format( "n" );
			if ( $conta == 7 ) {
				$conta = 0;
				$row ++;
			}

			$day_class = $month == $current_month ? '' : 'other-month';
			$day_class = $today_date_formatted == $reserva_date ? 'today' : $day_class;

			$ret [ $row ]['days'][ $reserva_date ] = [
				'day'           => $day,
				'day_class'           => $day_class,
				'reserva_items' => $this->get_day_reserva_items( $reserva_date, false, $centre_id ),
			];
			$conta ++;

		}
		unset( $period );

		$this->drop_temp_table();

		return $ret;
	}

	private function get_day_reserva_items( $reservaitem_date, $only_no_time = false, $centre_id = 0 ) {

		$ret = [];

		// Aquesta consulta ha d'estar igual al crear la taula temporal o almenys que estigui inclosa
		$no_time_query = $only_no_time ? "AND start_time = '00:00:00' AND end_time = '00:00:00'" : '';
		$centre_id_query = $centre_id ? "AND centre_id = $centre_id" : '';

		$query = "
				SELECT reservaitem_id, start_time, end_time, 1 as is_stored, product_id, order_id, is_done, order_status, product_title, product_variations
				FROM lassdive_reserva_temp 
				WHERE reservaitem_date = '$reservaitem_date'
				$no_time_query
				$centre_id_query
				ORDER BY start_time ASC, end_time ASC, product_title
				";

		$reserva_items_results = Db::get( $query );


		foreach ( $reserva_items_results as $rs ) {
			$reservaitem_id = $rs['reservaitem_id'];
			$start_time     = format_time( $rs['start_time'] );
			$end_time       = format_time( $rs['end_time'] );
			$order_id       = $rs['order_id'];
			$is_stored      = $rs['is_stored'];
			$is_done        = $rs['is_done'];
			$order_status   = $rs['order_status'];
			$product_title  = $rs['product_title'];
			$product_variations  = $rs['product_variations'];
			$users          = $this->get_orderitem_users( $reservaitem_id );


			$item_other_classes = $is_stored ? ' stored' : '';
			$item_other_classes .= $is_done ? ' is-done' : '';

			$ret [ $reservaitem_id ] = [
				'order_id'           => $order_id,
				'start_time'         => $start_time,
				'end_time'           => $end_time,
				'item_other_classes' => $item_other_classes,
				'order_status'       => in_array( $order_status, [
					'completed',
					'shipped',
					'delivered'
				] ) ? 'payed' : 'pending',
				'users'              => $users,
				'product_title'              => $product_title,
				'product_variations'              => $product_variations,
			];
		}

		return $ret;
	}

	// Obté les dades de la reserva per una hora concreta, així es pot utilitzar tant per obtindre per dia o per setmana
	private function get_centre_hour( $product_id, $reservaitem_id, $reservaitem_date, $centre_id, $centre_max_quantity, $hour ) {

		$query = "
				SELECT reservaitem_id, start_time, end_time, 1 as is_stored, product_id, order_id, is_done, order_status
				FROM lassdive_reserva_temp 
				WHERE reservaitem_date = '$reservaitem_date'
				AND start_time <= '$hour' 
				AND end_time > '$hour'
				AND centre_id = $centre_id
				";
		if ( $reservaitem_id != 0 ) $query .= "AND reservaitem_id <> $reservaitem_id";

		$reserva_items_results = Db::get_rows( $query );
		$count                 = count( $reserva_items_results );

		$this->count_busy_hours_new( $count, $reservaitem_id, $reservaitem_date, $hour, $centre_id, $product_id, $reserva_items_results );

		$reserva_items = $this->get_reserva_items( $reserva_items_results, $centre_max_quantity, $hour, $centre_id, $reservaitem_date );

		// $busy_percent = $count * 100 / $centre_max_quantity;
		// $free_percent = number_format( 100 - $busy_percent, 2 );

		$time       = explode( ':', $hour );
		$id_hour    = (int) $time[0];
		$id_minutes = (int) $time[1];

		$centre_hour_vars = [
			'timetable_id'        => "${centre_id}_${id_hour}_${id_minutes}",
			'centre_id'           => $centre_id,
			'hour'                => $hour,
			// 'busy_percent'      => $busy_percent,
			// 'free_percent'      => $free_percent,
			'show_free_percent'   => $count != 0,
			'centre_max_quantity' => $centre_max_quantity,
			'free'                => $centre_max_quantity - $count,
			'reserva_items'       => $reserva_items,
			'is_busy'             => $centre_max_quantity == $count
		];

		return $centre_hour_vars;
	}

	private function create_temp_table( $product_id, $reservaitem_date, $reservaitem_date2 ) {

		if ( $reservaitem_date2 )
			$date_sql = "(reservaitem_date BETWEEN '$reservaitem_date' AND '$reservaitem_date2')";
		else
			$date_sql = "(reservaitem_date = '$reservaitem_date')";

		$product_id_query = $product_id ?
			"AND product_id = $product_id" :
			"";
		if ( substr( $product_id, 0, 7 ) == 'family-' ) {
			$family_id = substr( $product_id, 7 );
			$product_id_query = "AND product_id IN ( SELECT product_id FROM product__product WHERE family_id = '$family_id' OR subfamily_id = '$family_id')";
		}


		$create_sql = "
			DROP TABLE IF EXISTS `lassdive_reserva_temp`;
			CREATE TEMPORARY TABLE lassdive_reserva_temp
			(
				INDEX `reservaitem_id` (`reservaitem_id`),
				INDEX `reservaitem_date` (`reservaitem_date`),
				INDEX `start_time` (`start_time`),
				INDEX `end_time` (`end_time`),
				INDEX `centre_id` (`centre_id`)
			)
			SELECT reservaitem_id, reservaitem_date, start_time, end_time, centre_id, order_id, is_done,
			        product_id, product_title, product_variations, (			
						SELECT order_status 
						FROM product__order 
						WHERE product__order.order_id = product__orderitem.order_id
					)   AS order_status
				FROM lassdive__orderitem 
				INNER JOIN product__orderitem USING (orderitem_id)
				WHERE (
				    $date_sql 
				    # Per si vull inclure els que no tenen data OR (start_time = '00:00:00' AND end_time = '00:00:00')
				    )
				$product_id_query				
				AND ( SELECT bin FROM product__order WHERE product__order.order_id = product__orderitem.order_id ) = 0";

		Db::multi_query( $create_sql );
	}

	private function drop_temp_table() {
		Db::execute( "DROP TEMPORARY TABLE lassdive_reserva_temp;" );
	}

	static public function get_days( $start_date, $end_date ) {
		$days = [];

		$start_date = new DateTime( $start_date );
		$end_date   = new DateTime( $end_date );

		$interval = DateInterval::createFromDateString( '1 day' );
		$period   = new DatePeriod( $start_date, $interval, $end_date );
		$months = [];
		foreach ( $period as $dt ) {
			$week_day = utf8_encode( strftime( "%a", strtotime( $dt->format( "Y-m-d" ) ) ) );
			$month = ucfirst(utf8_encode( strftime( "%b", strtotime( $dt->format( "Y-m-d" ) ) ) ));

			if (!in_array($month, $months)) $months [] = $month;

			$days []  = [
				'date'     => $dt->format( "Y-m-d" ),
				'day'      => $dt->format( "d" ),
				'week_day' => $week_day,
				'year'     => $dt->format( "d" ),
			];
		}
		unset( $period );

		return [
			'days' => $days,
			'month_name' => implode( ' - ', $months )
		];
	}

	static public function count_busy_hours_new( &$count, $reservaitem_id, $reservaitem_date, $hour, $centre_id, $product_id, &$reserva_items_results ) {

		$is_new = R::id( 'is_new' );
		if ( ! $is_new ) return;

		// descomptar les de sessió, ja afegides al cart
		$extra_vars = isset( $_SESSION['product']['cart_vars'][ $product_id ]['extra_vars'] ) ? $_SESSION['product']['cart_vars'][ $product_id ]['extra_vars'] : [];
		// descomptar les seleccionades, encara no afegides al cart,
		// s'estant seleccionant en aquest mateix moment
		$selected = R::post( 'selected_reserva_items', [] );

		$vars = array_merge( $extra_vars, $selected );
		// No calia, el merge ja ho fa be, poder en algún cas que no he vist
		// if (isset($vars[0])) unset($vars[0]); // orderitems comencen per l'index 1

		$hour = (int) str_replace( ':', '', $hour );

		foreach ( $vars as $rs ) {
			$saved_reservaitem_id   = $rs['reservaitem_id'];
			$start_time             = (int) str_replace( ':', '', $rs['start_time'] );
			$end_time               = (int) str_replace( ':', '', $rs['end_time'] );
			$saved_reservaitem_date = $rs['reservaitem_date'];
			$saved_centre_id        = $rs['centre_id'];

			if (
				$saved_reservaitem_date == $reservaitem_date
				&& $saved_reservaitem_id != $reservaitem_id
				&& $start_time <= $hour
				&& $end_time > $hour
				&& $saved_centre_id == $centre_id
			) {
				$count ++;
				// Ha de ser igual que el query a la BBDD - Aquestes variables s'utilitzen get_reserva_items
				$reserva_items_results [] = [
					'reservaitem_id' => "0-$saved_reservaitem_id",
					'start_time'     => $rs['start_time'] . ':00',
					'end_time'       => $rs['end_time'] . ':00',
					'order_id'       => '',
					'is_done'        => 0,
					'order_status'   => '',
					'is_stored'      => 0,
					'product_id'     => $product_id,
				];
			}
		}

	}

	private function get_reserva_items( $reserva_items_results, $centre_max_quantity, $hour, $centre_id, $reservaitem_date ) {

		$ret = [];
		if ( ! $reserva_items_results ) return $ret;

		Debug::p( $reserva_items_results, 'reserva_items_results' );

		if ( ! isset( $this->positions[ $centre_id ][ $reservaitem_date ] ) ) $this->positions[ $centre_id ][ $reservaitem_date ] = [];
		$pos = &$this->positions[ $centre_id ][ $reservaitem_date ];

		$item_width = round( 100 / $centre_max_quantity, 2 );

		$hour .= ':00';


		foreach ( $reserva_items_results as $rs ) {
			$reservaitem_id = $rs['reservaitem_id'];
			$start_time     = $rs['start_time'];
			$end_time       = $rs['end_time'];
			$order_id       = $rs['order_id'];
			$is_stored      = $rs['is_stored'];
			$is_done        = $rs['is_done'];
			$order_status   = $rs['order_status'];
			$users          = $this->get_orderitem_users( $reservaitem_id );
			$pos_index      = $reservaitem_id;

			if ( empty( $pos[ $pos_index ] ) ) {

				// Si l'item encara no està posicionat, buscar la primera posició lliure
				$position = 1;
				while ( in_array( $position, $pos ) ) {
					$position ++;
				}
			}
			else {
				$position = $pos[ $pos_index ];
			}

			$item_class = '';

			$is_start = $this->is_time_equal( $hour, $start_time );
			$is_end   = $this->is_end_time( $hour, $end_time );

			if ( $is_start ) {
				$item_class        = 'start';
				$pos[ $pos_index ] = $position;
			}
			if ( $is_end ) {
				$item_class .= ' end';
			}

			if ( ! $is_start && ! $is_end ) {
				$item_class = 'middle';
			}

			$item_other_classes = $is_stored ? ' stored' : '';
			$item_other_classes .= $is_done ? ' is-done' : '';

			$ret [ $reservaitem_id ] = [
				'position'           => $position,
				'order_id'           => $order_id,
				'item_width'         => $item_width,
				'item_right'         => $item_width * ( $position - 1 ),
				'item_class'         => $item_class,
				'item_other_classes' => $item_other_classes,
				'order_status'       => in_array( $order_status, [
					'completed',
					'shipped',
					'delivered'
				] ) ? 'payed' : 'pending',
				'users'              => $users
			];
		}

		// Haig de borrar la posició del item al final del blucle, sino queda lliure per una altre abans d'hora
		foreach ( $ret as $key => $val ) {
			if ( strpos( $val['item_class'], 'end' ) !== false )
				unset( $pos[ $key ] );
		}


		return $ret;
	}

	public function is_time_equal( $time1, $time2 ) {
		$time1 = (int) str_replace( ':', '', $time1 );
		$time2 = (int) str_replace( ':', '', $time2 );

		return $time1 == $time2;
	}

	public function is_end_time( $time1, $time2 ) {

		$datetime = DateTime::createFromFormat( 'G:i:s', $time1 );

		if ( ! $datetime ) return false;

		$datetime->modify( '+15 minutes' );
		$time1 = $datetime->format( 'G:i:s' );

		$time1 = (int) str_replace( ':', '', $time1 );
		$time2 = (int) str_replace( ':', '', $time2 );

		return $time1 == $time2;
	}

	public function get_orderitem_users( $reservaitem_id ) {

		$query   = "
			SELECT user_id 
			FROM lassdive__reservaitem_to_user
			WHERE reservaitem_id = $reservaitem_id";
		$results = Db::get_rows( $query );

		$users = [];

		$sep = ', ';

		foreach ( $results as $rs ) {
			$user_id  = $rs['user_id'];
			$query    = "
				SELECT CONCAT (name, ' ', surname) AS item
					FROM user__user 
					WHERE user_id = $user_id";
			$user     = Db::get_first( $query );
			$users [] = $user;
		}
		$users = implode( $sep, $users );

		return $users;
	}

}