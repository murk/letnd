<?Header("content-type: application/x-javascript");
extract($_GET);
$c_send_edit = addslashes($c_send_edit);

?>

// html_editors = <?=$html_editors?>;

// tooltip = "<?=$c_send_edit?>";
if (html_editors["htmlbasic"]){	
	tinyMCE.init({
		// General options
		mode : "exact",
		elements : html_editors["htmlbasic"].join(','),
		language : "<?=$language_code?>",
        <?if($theme_advanced_fonts) echo 'theme_advanced_fonts: "'.$theme_advanced_fonts.'",'?>
        <?if($theme_advanced_font_sizes) echo 'theme_advanced_font_sizes: "'.$theme_advanced_font_sizes.'",'?>
		theme : "advanced",
		width : "635",
		plugins : "paste",
		
		// paste as plain text
		paste_text_sticky : true,
		paste_auto_cleanup_on_paste : true,
		paste_remove_styles: true,
		paste_remove_styles_if_webkit: true,
		paste_strip_class_attributes: true,
		setup : function(ed) {
            // na zacatku zapnout fci 'vlozit jako plain text'
            ed.onInit.add(function(ed) {
                ed.pasteAsPlainText = true;
            });
        },
		
		// form atributes
		extended_valid_elements: 'form[accept|accept-charset|action|class|dir<ltr?rtl|enctype|id|lang|method<get?post|name|onreset|onsubmit|style|title|target],div[id|class|title],div[*]',

		// Theme options
		theme_advanced_buttons1 : "bold,italic,underline,strikethrough<?if($show_html) echo ",|,code"?>",
		theme_advanced_buttons2 : "",
		theme_advanced_buttons3 : "",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_resizing : true,
		theme_advanced_statusbar_location : "bottom",		

		// CSS
		content_css : "/<?=$client_dir?>/templates/styles/editor.css",
		
		// Configadmin
		indentation : '<?=$tinymce_indent?>px',
		
		// filtre classes
		class_filter : function(cls, rule) {

			if (/^body .*/.test(rule))	return false;
			return cls;
		}

	});

}

if (html_editors["htmlnewsletter"]){	
	tinyMCE.init({
		// General options
		mode : "exact",
		elements : html_editors["htmlnewsletter"].join(','),
		language : "<?=$language_code?>",
        <?if($theme_advanced_fonts) echo 'theme_advanced_fonts: "'.$theme_advanced_fonts.'",'?>
        <?if($theme_advanced_font_sizes) echo 'theme_advanced_font_sizes: "'.$theme_advanced_font_sizes.'",'?>
		theme : "advanced",
		width : "635",
		plugins : "advlist,images,spellchecker,save,advhr,advimage,advlink,inlinepopups,contextmenu,paste,fullscreen,noneditable",
		spellchecker_languages : "+Català=ca,Español=es,English=en,Français=fr,Deutsch=de",
		relative_urls: false,
		remove_script_host: true,
		
		// paste as plain text
		paste_text_sticky : true,
		paste_auto_cleanup_on_paste : true,
		paste_remove_styles: true,
		paste_remove_styles_if_webkit: true,
		paste_strip_class_attributes: true,
		setup : function(ed) {
            // na zacatku zapnout fci 'vlozit jako plain text'
            ed.onInit.add(function(ed) {
                ed.pasteAsPlainText = true;
            });
        },
		
		// form atributes
		extended_valid_elements: 'form[accept|accept-charset|action|class|dir<ltr?rtl|enctype|id|lang|method<get?post|name|onreset|onsubmit|style|title|target],div[id|class|title],div[*]',

		// Theme options
		theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,formatselect,fontselect,fontsizeselect,|,link,unlink,|,cleanup,removeformat,images,|,undo,redo,|,spellchecker,fullscreen,save",
		theme_advanced_buttons2 : "bullist,numlist,|,indent,outdent,|,sub,sup,|,cut,copy,paste,pastetext,pasteword,selectall<?if($show_html) echo ",|,code"?>",
		theme_advanced_buttons3 : "",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_statusbar_location : "bottom",
		theme_advanced_resizing : true,

		// CSS
		content_css : "/<?=$client_dir?>/templates/styles/editor.css",

		// Drop lists for link/image/media/template dialogs
		//template_external_list_url : "js/template_list.js",
		//external_link_list_url : "js/link_list.js",
		external_image_list_url : "/common/jscripts/image_list.js",
		//media_external_list_url : "js/media_list.js",
		
		// Configadmin
		indentation : '<?=$tinymce_indent?>px',
		
		// filtre classes
		class_filter : function(cls, rule) {

			if (/^body .*/.test(rule))	return false;
			return cls;
		}
	});
}

if (html_editors["html"]){	
	tinyMCE.init({
		// General options
		mode : "exact",
		elements : html_editors["html"].join(','),
		language : "<?=$language_code?>",
        <?if($theme_advanced_fonts) echo 'theme_advanced_fonts: "'.$theme_advanced_fonts.'",'?>
        <?if($theme_advanced_font_sizes) echo 'theme_advanced_font_sizes: "'.$theme_advanced_font_sizes.'",'?>
		theme : "advanced",
		width : "635",
		plugins : "advlist,images,spellchecker,save,advhr,advimage,advlink,inlinepopups,contextmenu,paste,fullscreen,noneditable",
		spellchecker_languages : "+Català=ca,Español=es,English=en,Français=fr,Deutsch=de",
		relative_urls: false,
		remove_script_host: true,
		
		// paste as plain text
		paste_text_sticky : true,
		paste_auto_cleanup_on_paste : true,
		paste_remove_styles: true,
		paste_remove_styles_if_webkit: true,
		paste_strip_class_attributes: true,
		setup : function(ed) {
            // na zacatku zapnout fci 'vlozit jako plain text'
            ed.onInit.add(function(ed) {
                ed.pasteAsPlainText = true;
            });
        },
		
		// form atributes
		extended_valid_elements: 'form[accept|accept-charset|action|class|dir<ltr?rtl|enctype|id|lang|method<get?post|name|onreset|onsubmit|style|title|target],div[id|class|title],div[*]',

		// Theme options
		theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,styleselect,|,link,unlink,|,cleanup,removeformat,images,|,undo,redo,|,spellchecker,fullscreen,save",
		theme_advanced_buttons2 : "bullist,numlist,|,indent,outdent,|,sub,sup,|,cut,copy,paste,pastetext,pasteword,selectall<?if($show_html) echo ",|,code"?>",
		theme_advanced_buttons3 : "",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_statusbar_location : "bottom",
		theme_advanced_resizing : true,

		// CSS
		content_css : "/<?=$client_dir?>/templates/styles/editor.css",

		// Drop lists for link/image/media/template dialogs
		//template_external_list_url : "js/template_list.js",
		//external_link_list_url : "js/link_list.js",
		external_image_list_url : "/common/jscripts/image_list.js",
		//media_external_list_url : "js/media_list.js",
		
		// Configadmin
		indentation : '<?=$tinymce_indent?>px',
		
		// filtre classes
		class_filter : function(cls, rule) {

			if (/^body .*/.test(rule))	return false;
			return cls;
		}
	});
}

if (html_editors["htmlfull"]){	
	tinyMCE.init({
		// General options
		mode : "exact",
		elements : html_editors["htmlfull"].join(','),
		language : "<?=$language_code?>",
        <?if($theme_advanced_fonts) echo 'theme_advanced_fonts: "'.$theme_advanced_fonts.'",'?>
        <?if($theme_advanced_font_sizes) echo 'theme_advanced_font_sizes: "'.$theme_advanced_font_sizes.'",'?>
		theme : "advanced",
		width : "635",
        plugins : "advlist,images,autolink,lists,spellchecker,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",
		spellchecker_languages : "+Català=ca,Español=es,English=en,Français=fr,Deutsch=de",
		relative_urls: false,
		remove_script_host: true,
		
		// paste as plain text
		paste_text_sticky : true,
		paste_auto_cleanup_on_paste : true,
		paste_remove_styles: true,
		paste_remove_styles_if_webkit: true,
		paste_strip_class_attributes: true,
		setup : function(ed) {
            // na zacatku zapnout fci 'vlozit jako plain text'
            ed.onInit.add(function(ed) {
                ed.pasteAsPlainText = true;
				//alert(ed.settings.valid_elements)				
            });
        },
		/*valid_elements : "@[id|class|style|title|dir<ltr?rtl|lang|xml::lang|onclick|ondblclick|"
							+ "onmousedown|onmouseup|onmouseover|onmousemove|onmouseout|onkeypress|"
							+ "onkeydown|onkeyup],a[rel|rev|charset|hreflang|tabindex|accesskey|type|"
							+ "name|href|target|title|class|onfocus|onblur],strong/b,em/i,strike,u,"
							+ "#p,-ol[type|compact],-ul[type|compact],-li,br,img[longdesc|usemap|"
							+ "src|border|alt=|title|hspace|vspace|width|height|align],-sub,-sup,"
							+ "-blockquote,-table[border=0|cellspacing|cellpadding|width|frame|rules|"
							+ "height|align|summary|bgcolor|background|bordercolor],-tr[rowspan|width|"
							+ "height|align|valign|bgcolor|background|bordercolor],tbody,thead,tfoot,"
							+ "#td[colspan|rowspan|width|height|align|valign|bgcolor|background|bordercolor"
							+ "|scope],#th[colspan|rowspan|width|height|align|valign|scope],caption,-div,"
							+ "-span,-code,-pre,address,-h1,-h2,-h3,-h4,-h5,-h6,hr[size|noshade],-font[face"
							+ "|size|color],dd,dl,dt,cite,abbr,acronym,del[datetime|cite],ins[datetime|cite],"
							+ "object[classid|width|height|codebase|*],param[name|value|_value],embed[type|width"
							+ "|height|src|*],script[src|type],map[name],area[shape|coords|href|alt|target],bdo,"
							+ "button,col[align|char|charoff|span|valign|width],colgroup[align|char|charoff|span|"
							+ "valign|width],dfn,fieldset,form[action|accept|accept-charset|enctype|method],"
							+ "input[accept|alt|checked|disabled|maxlength|name|readonly|size|src|type|value],"
							+ "kbd,label[for],legend,noscript,optgroup[label|disabled],option[disabled|label|selected|value],"
							+ "q[cite],samp,select[disabled|multiple|name|size],small,"
							+ "textarea[cols|rows|disabled|name|readonly],tt,var,big",*/
		// form atributes
		extended_valid_elements: 'form[accept|accept-charset|action|class|dir<ltr?rtl|enctype|id|lang|method<get?post|name|onreset|onsubmit|style|title|target],p[class|dir<ltr?rtl|id|lang|onclick|ondblclick|onkeydown|onkeypress|onkeyup|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|style|title],div[*]',

		// Theme options
		theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect,fontsizeselect,|,undo,redo,|,spellchecker,fullscreen,save,newdocument",
        theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,link,unlink,anchor,images,cleanup,|,insertdate,inserttime,|,forecolor,backcolor",
        theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,advhr,|,print",
        theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking<?if($show_html) echo ",|,code"?>",        
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_statusbar_location : "bottom",
		theme_advanced_resizing : true,

		// CSS
		content_css : "/<?=$client_dir?>/templates/styles/editor.css",

		// Drop lists for link/image/media/template dialogs
		//template_external_list_url : "js/template_list.js",
		external_link_list_url : "/common/jscripts/link_list.js",
		external_image_list_url : "/common/jscripts/image_list.js",
		media_external_list_url : "/common/jscripts/media_list.js",
		
		// Configadmin
		indentation : '<?=$tinymce_indent?>px',
		
		// filtre classes
		class_filter : function(cls, rule) {

			if (/^body .*/.test(rule))	return false;
			return cls;
		}
	});
}


<? /* TOTES LES  OPCIONS
        // Theme options
        theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect,fontsizeselect",
        theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
        theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
        theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,spellchecker,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,blockquote,pagebreak,|,insertfile,insertimage",
        theme_advanced_toolbar_location : "top",
        theme_advanced_toolbar_align : "left",
        theme_advanced_statusbar_location : "bottom",
        theme_advanced_resizing : true,
	*/