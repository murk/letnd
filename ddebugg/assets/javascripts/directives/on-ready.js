Clockwork.directive('onReady', function ($rootScope, $http, $timeout) {
        return {
            restrict: 'A',
            link: function ($scope) {
                console.log('Waiting.');
                var to;

                function resetTimer() {
                    //Reset counter for every digest cycle
                    $timeout.cancel(to);

                    to = $timeout(function () {

                        if($http.pendingRequests > 0) {
                            console.log('Wait..');
                            resetTimer();
                        }

                        console.log('Application ready');
                        listener(); //Stop watching
                        $rootScope.$broadcast('ready');
                    }, 2000);

                }

                var listener = $scope.$watch(function () {
                    console.log('digest');
                    resetTimer();
                });
            }
        }
});

