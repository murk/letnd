<?
// firefox:

// Executable del php5 - open in editor c:\server\php5\php.exe
// -f D:\Letnd\common\open_php.php -- %url %line
// 
// Ej. C:\server\php5\php.exe -f "D:\Letnd\common\open_php.php" -- http://w.de/clients/webletnd/templates/styles/general.css 8
if ( php_sapi_name() == 'cli' ) {
	define( "HOST_URL", "http://w.letnd/" );
} else {
	define( "HOST_URL", $_SERVER["HTTP_HOST"] ? "http://" . $_SERVER["HTTP_HOST"] . "/" : "http://" . $_ENV["HTTP_HOST"] . "/" );
}


$is_linux = ! is_dir( 'c:\\' );

// nomes per quan estem en local, si no fariem executar el notepad al servidor
if ( strstr( HOST_URL, 'http://w.' ) || strstr( HOST_URL, 'http://w2.' ) || strstr( HOST_URL, 'http://w3.' ) || strstr( HOST_URL, 'http://localhost' ) ) {
	if ( php_sapi_name() == 'cli' ) {
	    $argv = $_SERVER['argv'];
		$_GET['is_firebug'] = true;
		$_GET['file']       = $argv[1];
		$_GET['line']       = $argv[2];
	}

	if ( ! $is_linux ) {
		$_GET['file'] = str_replace( '/letnd', 'd:\letnd', $_GET['file'] );
	}

	if ( isset( $_GET['is_firebug'] ) ) {
		$_GET['file'] = substr( $_GET['file'], 7 );
		$pos1         = strpos( $_GET['file'], '/' );
		$_GET['file'] = $_SERVER['DOCUMENT_ROOT'] . substr( $_GET['file'], $pos1 );

		$pos2 = strpos( $_GET['file'], '?' );
		if ( $pos2 ) {
			$_GET['file'] = substr( $_GET['file'], 0, $pos2 );
		}

		if ( $is_linux ) {
			$_GET['file'] = '/media/webs/letnd' . str_replace( '/', '\\', $_GET['file'] );
		} else {
			$_GET['file'] = 'D:\letnd' . str_replace( '/', '\\', $_GET['file'] );

		}
	}

	// linux
	if ( $is_linux ) {

		if ( ( substr( $_GET['file'], - 3 ) == 'php' ) ) {
			$project = '/Letnd/';
		} elseif ( ( strpos( $_GET['file'], 'admin' ) ) ) {
			$project = '/Admin/';
		} else {
			$project = '/Clients/';
		}

		error_reporting( E_ALL );
		ini_set( 'display_errors', true );
		ini_set( 'display_startup_errors', true );
		//$path = 'C:\Program Files (x86)\JetBrains\PhpStorm 9.0.2\bin\PhpStorm64.exe D:/' . $project;

		// linux per cli
		if ( php_sapi_name() == 'cli' ) {
			$project = 'Phpstorm-linux' . $project;
			$path    = '/opt/PhpStorm9/bin/phpstorm.sh /media/webs/' . $project;
			$path .= " --line " . $_GET['line'] . ' ' . $_GET['file'];

			my_exec( $path );
		}
		//
		// linux per vagrant
		// CURL per executar script al apache del Windows
		//
		else {

			$service_url = 'http://w2.de/common/open_php.php?file='.$_GET['file'].'&line=' . $_GET['line'];


			$curl        = curl_init( $service_url );
			curl_setopt( $curl, CURLOPT_RETURNTRANSFER, false );
			curl_exec( $curl );
			curl_close( $curl );
		}

		return;
	}


	$WshShell = new COM( "WScript.Shell" );

	$php_storm_path = file_exists( 'C:\Program Files (x86)\JetBrains\PhpStorm 9.0.2\bin\PhpStorm64.exe' )?'C:\Program Files (x86)\JetBrains\PhpStorm 9.0.2\bin\PhpStorm64.exe':false;

	$php_storm_path = file_exists( 'C:\Program Files (x86)\JetBrains\PhpStorm 2016.1\bin\PhpStorm64.exe' )?'C:\Program Files (x86)\JetBrains\PhpStorm 2016.1\bin\PhpStorm64.exe':$php_storm_path;

	$php_storm_path = file_exists( 'C:\Program Files\JetBrains\PhpStorm 2019.1.1\bin\phpstorm64.exe' )?'C:\Program Files\JetBrains\PhpStorm 2019.1.1\bin\phpstorm64.exe':$php_storm_path;



	if ( $php_storm_path ) {

		if ( ( substr( $_GET['file'], - 3 ) == 'php' ) ) {
			$project = 'Phpstorm\Letnd\\';
		} elseif ( ( strpos( $_GET['file'], 'admin' ) ) ) {
			$project = 'Phpstorm\Admin\\';
		} else {
			$project = 'Phpstorm\Clients\\';
		}


		$path = '"' . $php_storm_path . '" D:\\' . $project;
		$path .= " --line " . $_GET['line'] . ' ' . $_GET['file'];
	} else {
		$path = file_exists( 'C:\Program Files\Notepad++\Notepad++.exe' ) ?
			'"C:\Program Files\Notepad++\Notepad++.exe "' :
			'"C:\Program Files (x86)\Notepad++\Notepad++.exe "';

		$path = $path . ' -n' . $_GET['line'] . ' ' . $_GET['file'];

		if ( substr( $_GET['file'], - 3 ) == 'php' ) {

			$path = file_exists( 'C:\Program Files (x86)\NetBeans 8.0\bin\netbeans.exe' ) ?
				'"C:\Program Files (x86)\NetBeans 8.0\bin\netbeans.exe" "' . $_GET['file'] . ':' . $_GET['line'] . '" --console suppress' : $path;
		}

	}

	$oExec = $WshShell->Run( $path, 3, false );
	$oExec = null;

	//file_put_contents("d:\info.txt", $path);
}

function my_exec( $cmd, $input = '' ) {
	$proc = proc_open( $cmd, array(
		0 => array( 'pipe', 'r' ),
		1 => array( 'pipe', 'w' ),
		2 => array( 'pipe', 'w' )
	), $pipes );
	fwrite( $pipes[0], $input );
	fclose( $pipes[0] );
	$stdout = stream_get_contents( $pipes[1] );
	fclose( $pipes[1] );
	$stderr = stream_get_contents( $pipes[2] );
	fclose( $pipes[2] );
	$rtn = proc_close( $proc );

	return array(
		'stdout' => $stdout,
		'stderr' => $stderr,
		'return' => $rtn
	);
}
