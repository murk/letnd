<?php

if (!defined('TITLE_COMARCA')){
	define('TITLE_COMARCA', 'Biens à : ');
	define('TITLE_PRICE', 'Biens jusqu\'à %s '.CURRENCY_NAME);
	define('TITLE_PRICE_2', 'Biens entre %s et %s '.CURRENCY_NAME);
	define('TITLE_PRICE_3', 'Biens de plus de %s '.CURRENCY_NAME);

	define('TITLE_FLOORSPACE', 'Biens jusqu\'à %s m<sup>2</sup>');
	define('TITLE_FLOORSPACE_2', 'Biens entre %s et %s m<sup>2</sup>');
	define('TITLE_FLOORSPACE_3', 'Biens de plus de %s m<sup>2</sup>');

	define('TITLE_PERSON', 'Jusqu\'à %s personnes');
	define('TITLE_PERSON_2', 'Entre %s et %s personnes');
	define('TITLE_PERSON_3', 'De plus de %s personnes');
}

$gl_caption['c_filter_price_min'] = 'Prix min.';
$gl_caption['c_filter_price_max'] = 'Prix max.';
$gl_caption['c_filter_price_min_temp'] = 'Prix min. / nuit';
$gl_caption['c_filter_price_max_temp'] = 'Prix max. / nuit';

$gl_caption['c_recent'] = 'Nouveau';

// NO TRADUIT
$gl_caption['c_property_list__view_all'] = 'Voir tous les immeubles visitées';
$gl_caption['c_characteristics'] = 'Caractéristiques';
// FI NO TRADUIT

$gl_caption['c_booking_close_results_message'] = 'There are no properties available on the given dates, the following properties can be booked on similar dates';
?>