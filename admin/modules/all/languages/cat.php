<?php
// menus eina
if (!defined('ALL_MENU_PAGE')){
	
	define('ALL_MENU_PAGE','Edició de pàgines');
	define('ALL_MENU_PAGE_NEW','Nova pàgina');
	define('ALL_MENU_PAGE_LIST','Llistar pàgina');
	define('ALL_MENU_PAGE_LIST_BIN','Paperera');
}

// TODO-i Falta fra
$gl_caption_page['c_ordre'] = 'Ordre';
$gl_caption_page['c_template'] = 'Plantilla';
$gl_caption_page['c_link'] = 'Link';
$gl_caption_page_list['c_page_title'] = 'Títol cercadors';
$gl_caption_page_list['c_page_file_name'] = 'Nom pàgina';
$gl_caption_page['c_page_file_name'] = $GLOBALS['gl_caption']['c_file_name'];
$gl_caption_page['c_page_old_file_name'] = $GLOBALS['gl_caption']['c_old_file_name'];
$gl_caption_page['c_page'] = 'Nom menú o títol cos';
$gl_caption_page['c_title'] = 'Títol al cos';
$gl_caption_page['c_body_subtitle'] = 'Subtítol';
$gl_caption_page['c_body_description'] = 'Descripció curta';
$gl_caption_page['c_content'] = 'Contingut';
$gl_caption_page['c_prepare'] = 'En preparació';
$gl_caption_page['c_review'] = 'Per revisar';
$gl_caption_page['c_public'] = 'Públic';
$gl_caption_page['c_archived'] = 'Arxivat';
$gl_caption_page['c_status'] = 'Estat';
$gl_caption_page['c_page_id'] = 'Id';
$gl_caption_page['c_parent_id'] = 'Parent';
$gl_caption_page['c_level'] = 'Nivell';
$gl_caption_page['c_menu'] = 'Menú';
$gl_caption_page['c_blocked'] = 'Block.';
$gl_caption_page['c_filter_parent_id'] = 'Parent';
$gl_caption_page['c_filter_level'] = 'Level';
$gl_caption_page['c_has_content'] = 'Content';
$gl_caption_page['c_has_content_0'] = 'no';
$gl_caption_page['c_has_content_1'] = 'si';
$gl_caption_page['c_edit_content_button'] = 'Editar pàgina';
$gl_caption_page['c_edit_page_button'] = 'Editar dades';
$gl_caption_page['c_filter_menu2'] = 'Menú de navegació';

$gl_caption_page['c_data'] = 'Dades de la pàgina';
$gl_caption_page['c_seo'] = 'Optimització cercadors';

?>