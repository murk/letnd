<?php
// menus eina
if (!defined('RACING_MENU_RALLY')){
	define('RACING_MENU_RALLY', 'Rallis');
	define('RACING_MENU_RALLY_NEW', 'Entrar nou ralli');
	define('RACING_MENU_RALLY_LIST', 'Llistar i editar rallis');
	define('RACING_MENU_RALLY_BIN', 'Paperera de reciclatge');

	define('RACING_MENU_CATEGORY', 'Categories');
	define('RACING_MENU_CATEGORY_NEW', 'Entrar nova categoria');
	define('RACING_MENU_CATEGORY_LIST', 'Llistar categories');

	define('RACING_MENU_TEMA', 'Temes');
	define('RACING_MENU_TEMA_NEW', 'Entrar nou tema');
	define('RACING_MENU_TEMA_LIST', 'Llistar temes');

	define('RACING_MENU_SUPORT', 'Suport');
	define('RACING_MENU_SUPORT_NEW', 'Entrar nou suport');
	define('RACING_MENU_SUPORT_LIST', 'Llistar i editar suports');
	define('RACING_MENU_SUPORT_BIN', 'Paperera de reciclatge');

	define('RACING_MENU_VIDEO', 'Vídeos');
	define('RACING_MENU_VIDEO_NEW', 'Entrar nou vídeo');
	define('RACING_MENU_VIDEO_LIST', 'Llistar i editar vídeos');
	define('RACING_MENU_VIDEO_BIN', 'Paperera de reciclatge');
}

$gl_messages['no_category']='Per poder insertar rall hi ha d\'haver almenys una categoria';

// captions seccio
$gl_caption['c_prepare'] = 'En preparació';
$gl_caption['c_review'] = 'Per revisar';
$gl_caption['c_public'] = 'Públic';
$gl_caption['c_archived'] = 'Arxivat';
$gl_caption['c_rally'] = 'Ralli';
$gl_caption['c_suport'] = 'Nom';
$gl_caption['c_video'] = 'Nom';
$gl_caption['c_title'] = 'Títol';
$gl_caption['c_subtitle'] = 'Subtítol';
$gl_caption['c_tema'] = $gl_caption['c_tema_id'] = 'Tema';
$gl_caption['c_content'] = 'Contingut';
$gl_caption['c_entered'] = 'Data publicació';
$gl_caption_rally['c_entered'] = 'Data del Rally';
$gl_caption['c_expired'] = 'Data caducitat';
$gl_caption['c_modified'] = 'Data modificació';
$gl_caption['c_status'] = 'Estat';
$gl_caption['c_category_id'] = 'Categoria';
$gl_caption['c_filter_category_id'] = 'Totes les categories';
$gl_caption['c_filter_status'] = 'Tots els estats';
$gl_caption['c_filter_in_home'] = 'Totes, home o no';
$gl_caption['c_filter_destacat'] = 'Totes, destacades o no';
$gl_caption['c_ordre'] = 'Ordre';
$gl_caption['c_filter_tema_id'] = 'Tots els temes';
$gl_caption['c_tema_select_caption'] = 'Cap tema';
$gl_caption['c_manual_id'] = 'Manual relacionat';
$gl_caption['c_manual_select_caption'] = 'Cap manual';

$gl_caption_category['c_category'] = 'Categoria';
$gl_caption_category['c_racing_button'] = 'Veure suports';
$gl_caption['c_tema_button'] = 'Veure FAQs';
$gl_caption_category['c_ordre'] = 'Ordre';
$gl_caption_image['c_name'] = 'Nom';
$gl_caption['c_url1_name']='Nom URL(1)';
$gl_caption['c_url1']='URL(1)';
$gl_caption['c_url2_name']='Nom URL(2)';
$gl_caption['c_url2']='URL(2)';
$gl_caption['c_url3_name']='Nom URL(3)';
$gl_caption['c_url3']='URL(3)';
$gl_caption['c_videoframe']='Video (youtube, metacafe...)';
$gl_caption['c_file']='Arxius';
$gl_caption['c_in_home']='Surt a la Home';
$gl_caption['c_in_home_0']='No surt a la Home';
$gl_caption['c_in_home_1']='Surt a la Home';
$gl_caption['c_content_list']='Descripció curta al llistat';
$gl_caption['c_destacat']='Notícia destacada';
$gl_caption['c_destacat_0']='No destacada';
$gl_caption['c_destacat_1']='Destacada';

$gl_caption['c_rally_file_name'] = $gl_caption['c_suport_file_name'] = $gl_caption['c_video_file_name'] = $GLOBALS['gl_caption']['c_file_name'];
$gl_caption['c_rally_old_file_name'] = $gl_caption['c_suport_old_file_name'] = $gl_caption['c_video_old_file_name'] = $GLOBALS['gl_caption']['c_old_file_name'];
$gl_caption['c_form_level1_title_page_title'] = $GLOBALS['gl_caption']['c_seo'];

$gl_caption['c_selected']='Notícies seleccionades';

// mapes
$gl_caption['c_search_adress']='Buscar adreça';
$gl_caption['c_latitude']='latitud';
$gl_caption['c_longitude']='longitud';
$gl_caption['c_found_direction']='adreça trobada';
$gl_caption['c_edit_map'] = 'Mapa';
$gl_caption['c_save_map'] = 'Guardar ubicació';
$gl_caption['c_click_map'] = 'Fes un click en un punt o més del mapa per guardar la ubicació.';
$gl_caption['c_form_map_title'] = 'Ubicació';
$gl_messages['point_saved'] = 'Nova ubicació guardada correctament';
$gl_messages['point_not_saved'] = 'No s\'ha pogut guardar l\'ubicació';
$gl_caption['c_remove_polyline']='Esborrar traçat';
?>