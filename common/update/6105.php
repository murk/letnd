<?

/* 14-11-16

	Productes relacionats


 */

if ( is_table( 'product__product' ) ) {

	if ( ! is_table( 'product__product_related' ) ) {

		$update_sql [] = "
			CREATE TABLE `product__product_related` (
				`product_related_id` INT(11) NOT NULL AUTO_INCREMENT,
				`product_id` INT(11) NOT NULL,
				`product2_id` INT(11) NOT NULL,
				`ordre` INT(11) NOT NULL,
				PRIMARY KEY (`product_related_id`),
				INDEX `product_id` (`product_id`),
				INDEX `product2_id` (`product2_id`)
			)
			COLLATE='utf8_spanish2_ci'
			ENGINE=MyISAM;
";

	} else {
		trigger_error( 'Ja existeixen la taula product__product_related' );
	}

}


?>