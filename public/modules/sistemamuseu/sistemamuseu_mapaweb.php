<?
/**
 * SistemamuseuMapaweb
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2012
 * @version $Id$
 * @access public
 */
class SistemamuseuMapaweb extends Module{

	function __construct(){
		parent::__construct();
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function get_records()
	{	
		return get_block(7);
	}
}
?>