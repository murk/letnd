<?php
global $gl_caption_demand_property, $gl_caption_demand;
// menus eina
if (!defined('CUSTUMER_MENU_CUSTUMER')) {
	define('CUSTUMER_MENU_CUSTUMER','Gestion des clients');
	define('CUSTUMER_MENU_NEW','Entrez un nouveau client');
	define('CUSTUMER_MENU_LIST','Énumérer tous les clients');
	define('CUSTUMER_MENU_LIST_INACTIVE','Énumérer clients desactivées');
	define('CUSTUMER_MENU_LIST_OWNERS','Propriétaires de vente');
	define('CUSTUMER_MENU_LIST_RENTERS','Requérants à louer');
	define('CUSTUMER_MENU_LIST_BUYERS','Requérants à achat');
	define('CUSTUMER_MENU_LIST_TENANTS','Propriétaires louent');
	define('CUSTUMER_MENU_LIST_BOOKERS','Clients turistique');
	define('CUSTUMER_MENU_LIST_OTHERS','Autres');
	define('CUSTUMER_MENU_BIN','Corbeille');
	define('CUSTUMER_MENU_','Demandes de clients');
	define('CUSTUMER_MENU_DEMAND','Énumérer les demandes');
	define('CUSTUMER_MENU_DEMAND_LIST','Énumérer les demandes');
	define('CUSTUMER_MENU_DEMAND_CHECK','Recherches exactes');
	define('CUSTUMER_MENU_DEMAND_PROX','Recherches aproximatives');
	define('CUSTUMER_SELECT_WINDOW_TILE','Sélectionné propriétés');
	define('CUSTUMER_MENU_CUSTUMERLIST','Énumérer clients');
	define('CUSTUMER_MENU_HISTORY','Historique');
	define('CUSTUMER_MENU_HISTORY_LIST','Énumérer historique');
	define('CUSTUMER_MENU_HISTORY_BIN','Corbeille');
	define('CUSTUMER_MENU_HISTORY_NEW','Nouvelle entrée');

	define('CUSTUMER_MENU_GROUP','Groupe');
	define('CUSTUMER_MENU_GROUP_NEW','Insérez groupe');
	define('CUSTUMER_MENU_GROUP_LIST','Liste des groupes');

	define('MLS_MENU_COMPANIES','MLS');
	define('MLS_MENU_COMPANIES_LIST','Énumérer entreprises');
	define('MLS_MENU_COMPANIES_YES','Entreprises communes');
}

$gl_caption_custumer['c_activated'] = 'Active';
$gl_caption_custumer['c_deactivate'] = 'Désactiver sélectionnées';
$gl_caption_custumer['c_activate'] = 'Activer sélectionnées';

$gl_caption_custumer['c_file'] = 'Documents';
//captions custumer_buy.tpl
$gl_caption_custumer['c_insert'] ='Type de propriété';
$gl_caption_custumer['c_tipus'] ='Type de propriété';
$gl_caption ['c_maxprice'] ='Prix maximum';
//captions tpl what.tpl
$gl_caption_custumer['c_add'] = 'Haute propriété';
$gl_caption_custumer['c_add_expand'] = '(Le client dispose d\'un bien immobilier à vendre.)';
$gl_caption_custumer['c_assign'] = 'Attribuer la propriété ';
$gl_caption_custumer['c_assign_expand'] = '(Attribué une propriété existante pour le client.)';
$gl_caption_custumer['c_buy'] = 'Demande d\'Achat ';
$gl_caption_custumer['c_buy_expand'] = 'Affecter une application à acheter. Letnd regarder régulièrement coincidenciens les propriétés disponibles avec les demandes d\'achat, y compris plusieurs propriétés MLS';
$gl_caption_custumer['c_custumer'] = 'Client';
$gl_caption_custumer['c_insert']= 'Insérer';
//captions tpl search_custumer.tpl
$gl_caption_custumer['c_search_title'] = 'Chercher client';
$gl_caption_custumer['c_by_nif']= 'Par NIF ';
$gl_caption_custumer['c_by_words']= 'Par mots' ;
//custumer_selected_frame.tpl
$gl_caption_custumer['c_by_ref'] = '(Références séparés par des espaces, par exemple: "456 245 45")';
$gl_caption_custumer['c_by_words'] = 'Par mots';
$gl_caption_custumer['c_selected']='Propriétés sélectionnés';
$gl_caption_custumer['c_selected_customers']='Propiétaires sélectionnés';
$gl_caption_custumer['c_status'] = 'État';
$gl_caption_custumer['c_property_title'] = 'Description courte';
$gl_caption_custumer['c_description'] = 'Description';
$gl_caption_custumer['c_category_id'] = 'Type';
$gl_caption_custumer['c_prepare'] = 'En préparation';
$gl_caption_custumer['c_review'] = 'Pour revue';
$gl_caption_custumer['c_onsale'] = 'Par vendre';
$gl_caption_custumer['c_sold'] = 'Vendue';
$gl_caption_custumer['c_archived'] = 'Déposée';
$gl_caption['c_property'] = 'Propriété';
$gl_caption_custumer['c_property_private']='Nom privée';
$gl_caption_custumer['c_surnames'] = 'Noms de famille';
$gl_caption_custumer['c_treatment'] = 'Infliger';
$gl_caption['c_treatment_sra'] = "Mme.";
$gl_caption['c_treatment_sr'] = "M.";
$gl_caption_custumer['c_birthdate'] = 'Date de naissance';
$gl_caption_custumer['c_company'] = 'Société';
$gl_caption_custumer['c_job'] = 'Travail';
//captions tpl demands_list.tpl
$gl_caption_custumer_demand['c_result_search']= 'Résultats de la recherche.';
$gl_caption_custumer['c_demandnumber']= 'Application Nº';
//$gl_caption_custumer['c_municipi']= 'Sol·licitud Nº';
$gl_caption_custumer['c_enteredc']= 'Crée';
$gl_caption_custumer['c_entered']= 'Crée';
$gl_caption_custumer['c_historycustumer']='Histoire du client';
$gl_caption_custumer['c_add_history']='Nouvelle entrée';
$gl_caption['c_show_not_show']='Enseigner / cacher';
$gl_caption_history['c_filter_tipus']='Toutes les actions';
$gl_caption_history['c_various']='Divers';
$gl_caption_history['c_to_call']='Pour appelez';
$gl_caption_history['c_waiting_call']='En attente de l\appel';
$gl_caption_history['c_assembly']='Pour se rencontrer';
$gl_caption_history['c_show_property']='Enseigner la propriété ';
$gl_caption['c_like_property']='Intéressé par l\'inmeuble';
$gl_caption['c_send_information']='Information envoyée';
$gl_caption_history['c_office']='Servir au bureau';
$gl_caption_history['c_offer']='Faire une offre';
$gl_caption_history['c_tipus']='Action';
$gl_caption_history['c_name']='Prénom';
$gl_caption_history['c_custumer_id']='Client';
$gl_caption_history['c_status']='État';
$gl_caption_history['c_filter_status']='Tous les états';
$gl_caption_history['c_sort_out']='En attendant';
$gl_caption_history['c_finished']='Terminé';
$gl_caption_history['c_canceled']='Annulé';
$gl_caption_history['c_dates']='Dates';
$gl_caption_history['c_date']='Date d\'ajout';
$gl_caption_history['c_finished_date']='Date d\'achèvement';
$gl_caption['c_no_history']='Il n\'y a pas d\'entrées dans l\'historique du client';
$gl_caption_custumer['c_edit_history']='Historique';
$gl_caption_custumer['c_form_history_title'] = 'Historique du client:';
$gl_caption_custumer['c_edit_history_button']='Modifier entré';

$gl_caption['c_price_history_title'] = 'Historique de prix';
$gl_caption['c_no_price_history'] = "Il n\'y a pas d\'entrées dans l\'historique de prix";

// editar dades clients -> Parcourir demandes
/*$gl_caption_custumer_demand ['c_custumer_id']='Client';
$gl_caption_custumer_demand['c_tipus2'] = 'Type d'oeuvre';
$gl_caption_custumer_demand['c_new'] = 'Oeuvre nouveau';
$gl_caption_custumer_demand['c_second'] = '2e main';
$gl_caption_custumer_demand['c_tipus'] = 'Opération';
$gl_caption_custumer_demand['c_sell'] = 'Vente';
$gl_caption_custumer_demand['c_rent'] = 'Louer';
$gl_caption_custumer_demand['c_municipi_id'] = 'Ville';
$gl_caption_custumer_demand['c_garage'] = 'Garage';
$gl_caption_custumer_demand['c_storage'] = 'Jonque';
$gl_caption_custumer_demand['c_terrace'] = 'Terrasse';
$gl_caption_custumer_demand['c_garden'] = 'Jardin';*/

// captions comuns
$gl_caption ['c_custumer_id']='Code';
$gl_caption_demand ['c_custumer_id']='Nom du client';
$gl_caption_demand ['c_demand_id'] ='Demande';
$gl_caption_demand ['c_phone'] ='Téléphone';

// captions seccio
$gl_caption_custumer['c_prefered_language'] = 'Langue';
$gl_caption_custumer['c_demand_id']='Demande';
$gl_caption['c_name'] = 'Prénom';
$gl_caption['c_surname1'] = '1er nom de famille';
$gl_caption['c_surname2'] = '2ème nom du famille';
$gl_caption_custumer ['c_custumer_id']='Code';
$gl_caption_custumer['c_vat'] = 'Document d\'identité';
$gl_caption_custumer['c_phone'] = 'Téléphone';
$gl_caption['c_phone1'] = 'Principal';
$gl_caption['c_phone1b'] = 'Principaux';
$gl_caption['c_phone2'] = 'Travail';
$gl_caption['c_phone4'] = 'Privée';
$gl_caption['c_phone3'] = 'Fax';
$gl_caption_custumer['c_adress'] = 'Rue';
$gl_caption_custumer['c_numstreet'] = 'Numéro';
$gl_caption_custumer['c_block'] = 'Bloc';
$gl_caption_custumer['c_flat'] = 'Étage';
$gl_caption_custumer['c_door'] = 'Porte';
$gl_caption_custumer['c_town'] = 'Ville';
$gl_caption_custumer['c_province'] = 'Province';
$gl_caption_custumer['c_country'] = 'Pays';
$gl_caption_custumer['c_bankdata'] = 'Coordonnées bancaires';
$gl_caption_custumer['c_bank'] = 'Casse / Banque';
$gl_caption_custumer['c_comta'] = 'IBAN';
$gl_caption_custumer['c_account_holder'] = 'Titulaire';
$gl_caption_custumer['c_billing'] = 'Facturation';
$gl_caption_custumer['c_billing_company'] = 'Entreprise';
$gl_caption_custumer['c_billing_citizen'] = 'Particulier';
$gl_caption_custumer['c_bic'] = 'BIC';
$gl_caption_custumer['c_vat_type'] = 'Type de document';
$gl_caption_custumer['c_vat_type_D'] = 'DNI (espagnol)';
$gl_caption_custumer['c_vat_type_P'] = 'Passeport';
$gl_caption_custumer['c_vat_type_C'] = 'Permis de conduire (seulement pour les citoyes espagnols)';
$gl_caption_custumer['c_vat_type_I'] = 'Carte ou document d\'identité';
$gl_caption_custumer['c_vat_type_N'] = 'Permis de résidence espagnol';
$gl_caption_custumer['c_vat_type_X'] = 'Permis de résidence d\'un autre état membre de l\'UE';
$gl_caption_custumer['c_vat_date'] = 'Date d\'émission';
$gl_caption_custumer['c_vat_num'] = 'Numéro';
$gl_caption_custumer['c_nationality'] = 'Nationalité';
$gl_caption_custumer['c_mail'] = 'Courrier électronique';
$gl_caption_custumer['c_website'] = 'Web';
$gl_caption_custumer['c_zip'] = 'Code postal';
$gl_caption_custumer['c_observations1'] = 'Observations';
$gl_caption_custumer['c_observations2'] = 'Observations (2)';
$gl_caption_custumer ['c_search1']='Recherche par code';
$gl_caption_custumer ['c_owner']='Le client est le propriétaire?';
$gl_caption_custumer ['c_buyer']='Le client est l\'acheteur?';
$gl_caption_custumer ['c_category'] ='Types';
$gl_caption_custumer['c_address'] = 'Adresse';

$gl_caption_custumer['c_add_button'] = 'Nouveau propriété';
//$gl_caption_custumer['c_assign_button'] = 'Attribuer la propriété';
$gl_caption_custumer['c_demand_button'] = 'Demande';
$gl_caption_custumer['c_add_demand_link']='Créer un nouveau demande';
$gl_caption['c_edit_demand_button']='Modifier demande'; // aquest ha de ser gl_caption
$gl_caption_custumer['c_propertyscustumer'] = 'Propriétés du client';
$gl_caption_custumer['c_demandscustumer'] = 'Demandes du client';
$gl_caption_custumer['c_bookingcustumer'] = 'Réservations du client';
$gl_caption_custumer['c_bookingcustumer_list'] = 'Lister réservations';
$gl_caption_custumer['c_ref']='Ref';
$gl_caption['c_edit_property_button']='Modifier la propriété';
$gl_caption['c_select_property_link']='Assignation du propriété';
$gl_caption['c_add_property_link']='Créer une nouvelle propriété';
$gl_caption['c_assign_button']='Attribuer propriété';
//captions propietari
$gl_caption_custumer ['c_by_code']='Recherche par code: ';
$gl_caption_custumer_owner ['c_search']='Cherchez: ';

$gl_caption ['c_books_button'] = 'Voir réservations';
$gl_caption ['c_searchexact_button'] = 'Chercher exact.';
$gl_caption ['c_searchprox_button'] = 'Chercher approchée';
$gl_caption_demand ['c_entered'] = 'Crée';
$gl_caption_demand ['c_data_demand'] = 'Données du demande';
$gl_caption_demand ['c_garage_area'] = 'surface';
//$gl_caption_property ['c_direccio'] = 'adresse';

$gl_caption_demand ['c_comercialname'] = 'Company';
//$gl_caption_demand ['c_custumer_id_format']='<strong>Nom: </strong> %s %s <strong>Tel: </strong> %s';
$gl_caption_demand ['c_0']='-';
$gl_caption_demand ['c_1']='si';
$gl_caption_demand ['c_room_0']='-';
$gl_caption_demand ['c_room_1']='1';

$gl_caption_demand ['c_bathroom_0'] = $gl_caption_demand ['c_wc_0'] = $gl_caption_demand ['c_room_0'];
$gl_caption_demand ['c_bathroom_1'] = $gl_caption_demand ['c_wc_1'] = $gl_caption_demand ['c_room_1'];

$gl_caption_demand_property['c_send_edit'] = 'Attribuez les sélectionnés';
$gl_caption_demand_property ['c_0']='-';
$gl_caption_demand_property ['c_1']='si';
$gl_caption_demand_property ['c_room_0']='-';
$gl_caption_demand_property ['c_room_1']='1';

$gl_caption_demand ['c_bathroom_0'] = $gl_caption_demand ['c_wc_0'] = $gl_caption_demand ['c_room_0'] = $gl_caption_demand ['c_living_0'] = $gl_caption_demand ['c_dinning_0'] = $gl_caption_demand ['c_level_count_0'] = $gl_caption_demand ['c_room_0'];
$gl_caption_demand ['c_bathroom_1'] = $gl_caption_demand ['c_wc_1'] = $gl_caption_demand ['c_living_1'] = $gl_caption_demand ['c_dinning_1'] = $gl_caption_demand ['c_level_count_1'] = $gl_caption_demand ['c_room_1'];

$gl_caption ['c_nodefinit']='non défini ou non ';
$gl_caption_demand ['c_nodefinit']='non défini ou non ';
$gl_caption_demand ['c_maxprice'] ='Prix maximum';
$gl_caption_demand['c_resultsmls']= 'Résultats de la recherche pour MLS';
$gl_caption_demand['c_observations']= 'Observations';

$gl_caption_demand['c_phone1']='Téléphone';
$gl_caption_demand['c_client_id']='';
$gl_caption_demand['c_client_dir']='';
$gl_caption_demand['c_domain']='';


$gl_caption_demand['c_search_level_filter'] = 'Niveau d\'approche dans la recherche';
$gl_caption_demand['c_search_level_1'] = 'Restrictif';
$gl_caption_demand['c_search_level_2'] = 'Normal';
$gl_caption_demand['c_search_level_3'] = 'Peu restrictif';

$gl_caption_demand['c_show_all_results'] = 'Afficher tous les résultats';

$gl_messages['no_records_demands'] = 'Aucun résultat trouvé pour n\'importe quelle application ';
$gl_messages['no_records_demand'] = 'Aucun résultat trouvé pour votre demande ';
$gl_messages['no_records_custumer'] = 'Aucun résultat trouvé pour le client';

$gl_caption ['c_date']='date';

$gl_caption['c_history_title']='Titre';
$gl_caption['c_history']='Histoire';
$gl_caption_history['c_message_id']='Newsletter';
$gl_caption_history['c_message_id_view']='Transférer le message';
$gl_caption_history['c_property_id']='Propriété';

$gl_caption['property_id_caption']='Aucune';

$gl_caption_custumer['c_user_id'] = 'Agent';
$gl_caption_custumer['c_filter_user_id'] = 'Tous les agents';
$gl_caption ['c_user_id_format']='%s %s';

$gl_caption_custumer['c_category_id'] = 'Demande';
$gl_caption_custumer['c_filter_category_id'] = 'Tous les demandes';

$gl_caption_custumer['c_filter_prefered_language'] = 'Tous les langues';
$gl_caption_custumer['c_group_id'] = 'Group';

$gl_caption['c_demand_refused'] = 'Propietat descartada';
$gl_caption['c_demand_reseted'] = 'Propietat readmesa';
$gl_caption['c_price_changed'] = 'Change de prix';

$gl_caption_demand['c_refuse_property'] = 'Rejeter';
$gl_caption_demand['c_reset_property'] = 'Rétablir';
$gl_caption_demand['c_interest_property'] = 'Intéresse';
$gl_caption_demand['c_view_property'] = 'Voir inmeuble';

$gl_caption_demand['c_refuse_property_done'] = "L'inmoble ha estat descartat";
$gl_caption_demand['c_reset_property_done'] = "L'inmeuble à eté rétabli";
$gl_caption_demand['c_like_property_done'] = "L'inmoble interessa al client";


$gl_caption_custumer['c_marital_status'] = 'État civil';
$gl_caption_custumer['c_marital_status_'] = '';
$gl_caption_custumer['c_marital_status_married'] = 'Marié';
$gl_caption_custumer['c_marital_status_divorced'] = 'Divorcé';
$gl_caption_custumer['c_marital_status_separated'] = 'Séparé';
$gl_caption_custumer['c_marital_status_single'] = 'Célibataire';
$gl_caption_custumer['c_marital_status_widower'] = 'Veuf';
$gl_caption_custumer['c_marital_status_commonlaw'] = 'Couple enregistré';

$gl_caption_custumer['c_invoice_information']='Datos de facturación';
$gl_caption_custumer['c_is_invoice']='Es facturable';
$gl_caption_custumer['c_comercial_name']='Nombre comercial';
$gl_caption_custumer['c_billing_mail']='E-mail de facturación';
$gl_caption_custumer['c_billing_type']='Forma de pago';
$gl_caption_custumer['c_bank_receipt']='Recibo bancario';
$gl_caption_custumer['c_wire_transfer']='Transferencia bancaria';
$gl_caption_custumer['c_bank_draft']='Cheque bancario';
$gl_caption_custumer['c_cash']='Efectivo';
$gl_caption_custumer['c_invoice_informations']='Observaciones de facturación';
$gl_caption_custumer['c_cif']='C.I.F';
?>