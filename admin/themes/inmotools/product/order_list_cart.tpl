<table border="0" cellspacing="0" cellpadding="0" width="100%" class="subListRecord">
	<tr>
		<td class="listCaption"></td>
		<td class="listCaption">
			<?= strtoupper( $c_ref ) ?>
		</td>
		<td class="listCaption">
			<?= $c_product_title ?>
		</td>
		<td class="listCaption">
			<?= $c_product_variations ?>
		</td>

		<? if ( $has_order_date ): ?>

			<td class="listCaption" align="right">
				<?= $c_product_date ?>
			</td>

		<? endif // $has_order_date ?>

		<td class="listCaption" align="right">
			<?= $c_quantity ?>
		</td>
		<td class="listCaption" align="right">
			<?= $c_product_base ?>
		</td>
		<td class="listCaption" align="right">
			<?= $c_product_total_base ?>
		</td>
	</tr>
	<? foreach ( $loop as $l ): extract( $l ) ?>
		<tr class="listItem<?= $odd_even ?>" onmousedown="change_bg_color(this, 'Down', '<?= $id ?>')" id="list_row_<?= $id ?>">
			<td valign="top" class="listImage" width="<?= round( $im_admin_thumb_w / 2 ) ?>">
				<? if ( $image_name ): ?>
					<div>
						<a href="/admin/?action=show_form_edit&menu_id=20004&product_id=<?= $product_id ?>"><img src="<?= $image_src_admin_thumb ?>" width="<?= round( $im_admin_thumb_w / 2 ) ?>"></a>
					</div>
				<? else: // image_name?>
					<div class="noimage" style="min-height:<?= round( $im_admin_thumb_h / 2 ) ?>px;"></div>
				<? endif // image_name?>
			</td>
			<td valign="top">
				<?= $ref ?>
				&nbsp;
			</td>
			<td valign="top"><? if ( $product_title ): ?>
					<a href="/admin/?action=show_form_edit&menu_id=20004&product_id=<?= $product_id ?>">
						<?= $product_title ?>
					</a>
				<? endif //product_title?>
				&nbsp;
			</td>
			<td valign="top">
			    <? foreach ( $product_variation_loop as $l ): extract( $l ) ?>
				    <p><strong><?= $product_variation_category ?></strong>: <?= $product_variation ?></p>
			    <? endforeach //$product_variations_loop?>
				&nbsp;
			</td>
			<? if ( $has_order_date ): ?>

				<td align="right" valign="top">
					<?= $product_date ?>
					&nbsp;
				</td>

			<? endif // $has_order_date ?>
			<td align="right" valign="top">
				<?= $quantity ?>
				&nbsp;
			</td>

			<td align="right" valign="top">
				<?= $product_basetax // TODO-i Mostrar amb iVA o sense segon congig? ?>
				<?= CURRENCY_NAME ?>
				&nbsp;
			</td>
			<td align="right" valign="top">
				<?= $product_total_basetax ?>
				<?= CURRENCY_NAME ?>
				&nbsp;
			</td>
		</tr>
	<? endforeach //$loop?>
</table>