<?php
	// Aquest arxiu es genera automaticament
	// Qualsevol canvi que es faci es podrÃ  sobrescriure involuntariament desde l'eina de gestiÃ³
	// Achtung!
global $gl_language;
$gl_file_protected = true;
	$gl_db_classes_fields_included =  array (
  'portfoli_id' => '1',
  'url1' => '1',
  'url2' => '1',  
  'status' => '1',  
  'entered' => '1',
  'title' => '1',
  'subtitle' => '1',
  'description' => '1',
);
	$gl_db_classes_language_fields =  array (
  0 => 'title',
  1 => 'subtitle',
  2 => 'description',
  3 => 'page_title',
  4 => 'page_description',
  5 => 'page_keywords',
  6 => 'portfoli_file_name', 
  7 => 'portfoli_old_file_name', 
);
	$gl_db_classes_fields =  array (
  'portfoli_id' =>
   array (
    'type' => 'hidden',
    'enabled' => '1',
    'form_admin' => 'text',
    'form_public' => 'text',
    'list_admin' => 'text',
    'list_public' => 'text',
    'order' => '0',
    'default_value' => '',
    'override_save_value' => '',
    'class' => '',
    'javascript' => '',
    'text_size' => '',
    'text_maxlength' => '',
    'textarea_cols' => '',
    'textarea_rows' => '',
    'select_caption' => '',
    'select_size' => '',
    'select_table' => '',
    'select_fields' => '',
    'select_condition' => '',
  ), 
  'ordre' =>
   array (
    'type' => 'int',
    'enabled' => '1',
    'form_admin' => 'input',
    'form_public' => 'no',
    'list_admin' => 'input',
    'list_public' => 'no',
    'order' => '0',
    'default_value' => '',
    'override_save_value' => '',
    'class' => '',
    'javascript' => '',
    'text_size' => '',
    'text_maxlength' => '11',
    'textarea_cols' => '',
    'textarea_rows' => '',
    'select_caption' => '',
    'select_size' => '',
    'select_table' => '',
    'select_fields' => '',
    'select_condition' => '',
  ), 
  'status' =>
   array (
    'type' => 'select',
    'enabled' => '1',
    'form_admin' => 'input',
    'form_public' => 'text',
    'list_admin' => 'input',
    'list_public' => 'text',
    'order' => '0',
    'default_value' => '',
    'override_save_value' => '',
    'class' => '',
    'javascript' => '',
    'text_size' => '',
    'text_maxlength' => '',
    'textarea_cols' => '',
    'textarea_rows' => '',
    'select_caption' => '',
    'select_size' => '',
    'select_table' => '',
    'select_fields' => 'prepare,review,public,archived',
    'select_condition' => '',
  ),
  'category_id' =>
  array (
    'type' => 'checkboxes',
    'enabled' => '1',
    'form_admin' => 'input',
    'form_public' => 'text',
    'list_admin' => 'input',
    'list_public' => 'text',
    'order' => '5',
    'default_value' => '',
    'override_save_value' => '',
    'class' => '',
    'javascript' => '',
    'text_size' => '',
    'text_maxlength' => '',
    'textarea_cols' => '',
    'textarea_rows' => '',
    'select_caption' => '',
    'select_size' => '', 
    'select_table' => 'portfoli__category,portfoli__category_language',
    'select_fields' => 'portfoli__category.category_id,category',
    'select_condition' => '(portfoli__category.category_id = portfoli__category_language.category_id) AND (language = \''.LANGUAGE.'\') ORDER BY ordre ASC, category ASC',
    'checked_table' => 'portfoli__portfoli_to_category',
    'checked_fields' => 'category_id',
    'checked_condition' => '',
  ),
 
  'title' =>
   array (
    'type' => 'text',
    'enabled' => '1',
    'form_admin' => 'input',
    'form_public' => 'text',
    'list_admin' => 'text',
    'list_public' => 'text',
    'order' => '2',
    'required' => '',
    'default_value' => '',
    'override_save_value' => '',
    'class' => '',
    'javascript' => '',
    'text_size' => '90',
    'text_maxlength' => '',
    'textarea_cols' => '',
    'textarea_rows' => '',
    'select_caption' => '',
    'select_size' => '',
    'select_table' => '',
    'select_fields' => '',
    'select_condition' => '',
  ),
  'subtitle' =>
   array (
    'type' => 'text',
    'enabled' => '1',
    'form_admin' => 'input',
    'form_public' => 'text',
    'list_admin' => 'text',
    'list_public' => 'text',
    'order' => '3',
    'default_value' => '',
    'override_save_value' => '',
    'class' => '',
    'javascript' => '',
    'text_size' => '90',
    'text_maxlength' => '',
    'textarea_cols' => '',
    'textarea_rows' => '',
    'select_caption' => '',
    'select_size' => '',
    'select_table' => '',
    'select_fields' => '',
    'select_condition' => '',
  ),
  'description' =>
   array (
    'type' => 'html',
    'enabled' => '1',
    'form_admin' => 'input',
    'form_public' => 'text',
    'list_admin' => 'text',
    'list_public' => 'text',
    'order' => '4',
    'default_value' => '',
    'override_save_value' => '',
    'class' => '',
    'javascript' => '',
    'text_size' => '',
    'text_maxlength' => '',
    'textarea_cols' => '',
    'textarea_rows' => '',
    'select_caption' => '',
    'select_size' => '',
    'select_table' => '',
    'select_fields' => '',
    'select_condition' => '',
  ),

  'url1' =>
   array (
    'type' => 'text',
    'enabled' => '1',
    'form_admin' => 'input',
    'form_public' => 'text',
    'list_admin' => '0',
    'list_public' => 'text',
    'order' => '2',
    'required' => '',
    'default_value' => '',
    'override_save_value' => '',
    'class' => '',
    'javascript' => '',
    'text_size' => '90',
    'text_maxlength' => '250',
    'textarea_cols' => '',
    'textarea_rows' => '',
    'select_caption' => '',
    'select_size' => '',
    'select_table' => '',
    'select_fields' => '',
    'select_condition' => '',
  ),
 
    'url2' =>
   array (
    'type' => 'text',
    'enabled' => '1',
    'form_admin' => 'input',
    'form_public' => 'text',
    'list_admin' => 'text',
    'list_public' => 'text',
    'order' => '2',
    'required' => '',
    'default_value' => '',
    'override_save_value' => '',
    'class' => '',
    'javascript' => '',
    'text_size' => '90',
    'text_maxlength' => '250',
    'textarea_cols' => '',
    'textarea_rows' => '',
    'select_caption' => '',
    'select_size' => '',
    'select_table' => '',
    'select_fields' => '',
    'select_condition' => '',
  ),
  'entered' =>
   array (
    'type' => 'datetime',
    'enabled' => '1',
    'form_admin' => 'input',
    'form_public' => 'text',
    'list_admin' => 'text',
    'list_public' => 'text',
    'order' => '7',
    'default_value' => '',
    'override_save_value' => '',
    'class' => '',
    'javascript' => '',
    'text_size' => '',
    'text_maxlength' => '-1',
    'textarea_cols' => '',
    'textarea_rows' => '',
    'select_caption' => '',
    'select_size' => '',
    'select_table' => '',
    'select_fields' => '',
    'select_condition' => '',
  ),
  'modified' =>
   array (
    'type' => 'datetime',
    'enabled' => '1',
    'form_admin' => 'text',
    'form_public' => 'text',
    'list_admin' => 'no',
    'list_public' => 'text',
    'order' => '7',
    'default_value' => '',
    'override_save_value' => '',
    'class' => '',
    'javascript' => '',
    'text_size' => '',
    'text_maxlength' => '-1',
    'textarea_cols' => '',
    'textarea_rows' => '',
    'select_caption' => '',
    'select_size' => '',
    'select_table' => '',
    'select_fields' => '',
    'select_condition' => '',
  ),
  'destacat' =>
   array (
    'type' => 'checkbox',
    'enabled' => '1',
    'form_admin' => 'input',
    'form_public' => '0',
    'list_admin' => '0',
    'list_public' => '0',
    'order' => '9',
    'default_value' => '0' ,
    'override_save_value' => '',
    'class' => '',
    'javascript' => '',
    'text_size' => '',
    'text_maxlength' => '1',
    'textarea_cols' => '',
    'textarea_rows' => '',
    'select_caption' => '',
    'select_size' => '',
    'select_table' => '',
    'select_fields' => '1,0',
    'select_condition' => '',
  ),  
  'page_id' =>
   array (
    'type' => 'checkboxes',
    'enabled' => '1',
    'form_admin' => 'input',
    'form_public' => 'text',
    'list_admin' => 'input',
    'list_public' => 'text',
    'order' => '0',
    'default_value' => '',
    'override_save_value' => '',
    'class' => '',
    'javascript' => '',
    'text_size' => '',
    'text_maxlength' => '11',
    'textarea_cols' => '2',
    'textarea_rows' => '',
    'select_caption' => '',
    'select_size' => '',
    'select_table' => 'all__page,all__page_language',
    'select_fields' => 'all__page.page_id,page',
    'select_condition' => '(all__page.page_id = all__page_language.page_id) AND (link="/" OR menu>8) AND (language = \''.LANGUAGE.'\') ORDER BY menu ASC, ordre ASC, page ASC',
    'checked_table' => 'portfoli__portfoli_to_page',
    'checked_fields' => 'page_id',
    'checked_condition' => '',
    'not_sortable' => '1',
  ),
   'videoframe' =>
  array (
    'type' => 'videoframe',
    'enabled' => '1',
    'form_admin' => 'input',
    'form_public' => 'text',
    'list_admin' => 'no',
    'list_public' => 'text',
    'order' => '2',
    'required' => '',
    'default_value' => '',
    'override_save_value' => '',
    'class' => 'wide',
    'javascript' => '',
    'text_size' => '',
    'text_maxlength' => '',
    'textarea_cols' => '',
    'textarea_rows' => '6',
    'select_caption' => '',
    'select_size' => '',
    'select_table' => '',
    'select_fields' => '',
    'select_condition' => '',
  ),
  'file' =>
  array (
    'type' => 'file',
    'enabled' => '1',
    'form_admin' => 'input',
    'form_public' => '0',
    'list_admin' => '0',
    'list_public' => '0',
    'order' => '0',
    'default_value' => '',
    'override_save_value' => '',
    'class' => '',
    'javascript' => '',
    'text_size' => '',
    'text_maxlength' => '5',
    'textarea_cols' => '',
    'textarea_rows' => '',
    'select_caption' => '',
    'select_size' => '',
    'select_table' => 'portfoli__portfoli_file',
    'select_fields' => 'file_id,name',
    'select_condition' => '',
    'accept' => 'pdf|doc|docx'
  ),
   'page_title' =>
   array (
    'type' => 'text',
    'enabled' => '1',
    'form_admin' => 'input',
    'form_public' => 'text',
    'list_admin' => '0',
    'list_public' => '0',
    'order' => '1',
    'default_value' => '',
    'override_save_value' => '',
    'class' => 'wide',
    'javascript' => '',
    'text_size' => '',
    'text_maxlength' => '69',
    'textarea_cols' => '',
    'textarea_rows' => '',
    'select_caption' => '',
    'select_size' => '',
    'select_table' => '',
    'select_fields' => '',
    'select_condition' => '',
  ),
    'page_description' =>
   array (
    'type' => 'text',
    'enabled' => '1',
    'form_admin' => 'input',
    'form_public' => 'text',
    'list_admin' => '0',
    'list_public' => '0',
    'order' => '1',
    'default_value' => '',
    'override_save_value' => '',
    'class' => 'wide',
    'javascript' => '',
    'text_size' => '',
    'text_maxlength' => '158',
    'textarea_cols' => '',
    'textarea_rows' => '',
    'select_caption' => '',
    'select_size' => '',
    'select_table' => '',
    'select_fields' => '',
    'select_condition' => '',
  ),
    'page_keywords' =>
   array (
    'type' => 'text',
    'enabled' => '1',
    'form_admin' => 'input',
    'form_public' => 'text',
    'list_admin' => '0',
    'list_public' => '0',
    'order' => '1',
    'default_value' => '',
    'override_save_value' => '',
    'class' => 'wide',
    'javascript' => '',
    'text_size' => '',
    'text_maxlength' => '200',
    'textarea_cols' => '',
    'textarea_rows' => '',
    'select_caption' => '',
    'select_size' => '',
    'select_table' => '',
    'select_fields' => '',
    'select_condition' => '',
  ),
    'portfoli_file_name' =>
   array (
    'type' => 'filename',
    'enabled' => '1',
    'form_admin' => 'input',
    'form_public' => 'text',
    'list_admin' => '0',
    'list_public' => 'text',
    'order' => '1',
    'default_value' => '',
    'override_save_value' => '',
    'class' => 'wide',
    'javascript' => '',
    'text_size' => '',
    'text_maxlength' => '50',
    'textarea_cols' => '',
    'textarea_rows' => '',
    'select_caption' => '',
    'select_size' => '',
    'select_table' => '',
    'select_fields' => '',
    'select_condition' => '',
  ),
      'portfoli_old_file_name' =>
   array (
    'type' => 'filename',
    'enabled' => '1',
    'form_admin' => 'input',
    'form_public' => '0',
    'list_admin' => '0',
    'list_public' => '0',
    'order' => '1',
    'default_value' => '',
    'override_save_value' => '',
    'class' => 'wide',
    'javascript' => '',
    'text_size' => '',
    'text_maxlength' => '50',
    'textarea_cols' => '',
    'textarea_rows' => '',
    'select_caption' => '',
    'select_size' => '',
    'select_table' => '',
    'select_fields' => '',
    'select_condition' => '',
  ),  
);
	$gl_db_classes_list =  array (
  'cols' => '1',
);
	$gl_db_classes_form =  array (
  'cols' => '1',
  'images_title' => 'title',
);
	$gl_db_classes_related_tables = array (
  '0' =>
  array (
    'table' => 'portfoli__portfoli_to_category',
    'action' => 'delete',
  ),
  '1' =>
  array (
    'table' => 'portfoli__portfoli_to_page',
    'action' => 'delete',
  ),
);
$gl_db_classes_uploads = array (
	'file' => '',
);
	?>