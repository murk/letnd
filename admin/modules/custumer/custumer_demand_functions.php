<?php
global $gl_resultats, $gl_mls, $gl_config, $gl_property_module;
// sempre posar prefix """"""$gl_"""""""" davant les variables globals, així qualsevol que ho miri sabrà que es global
$gl_resultats = array (); //inicialitzo el array de resultats
$gl_mls = $gl_config['mls_search_always'];


$gl_property_module = Module::load('inmo','property', '', false, 'custumer/custumer_demand_property_config.php'); // així no necessito el product_orderitem.php
$gl_property_module->action = 'get_records';
$gl_property_module->set_var('is_listing', !isset($_GET['demand_id']));

function list_demands ($type)
{
    global $gl_resultats, $tpl;
	$tpl->set_var('search_level_filter', get_filter_search_level($type));
	
    // Debug::add('Resultats de la cerca', $gl_resultats);

    $listing = new ListRecords;
    $listing->name = 'demands';
	$listing->join = "INNER JOIN custumer__custumer USING (custumer_id)";
    $listing->has_bin = false;
	if (isset($_GET['demand_id'])) { // si nomes mostrem una solicitud concreta
		$listing->set_options(1, 0, 0, 0, 0, 1, 1);
    }
    else{
		$listing->set_options(1, 1, 0, 1, 0, 1, 1);
		$listing->add_filter ('tipus');
		$listing->add_filter ('category_id');
	}
    $listing->template = 'custumer/demand_search_list';
    $listing->condition = 'demand_id IN (' . implode (',',array_keys($gl_resultats)) . ')';
                            // Aquí no cal, ja ho fa check_demands: 'AND (custumer__custumer.bin = 0 AND custumer__custumer.activated = 1)';
    $listing->call('list_records_walk', 'demand_id,land,land_units', true); //cada cop que passa per un registre crida la funcio list_records_wak
    	
	$listing->order_by = 'ENTERED DESC';
    $listing->list_records();
}
function get_filter_search_level($type){
	if ($type == 'exact') return false;
	global $gl_caption, $gl_config;
	$html = '';
	$level = R::escape('search_level',$gl_config['search_level']);

	$levels = array('1','2','3');
	foreach ($levels as $l)
	{
                $selected = $l == $level?' selected':'';
		        $html .= '<option value="' . $l . '"' . $selected . '>' . $gl_caption['c_search_level_' . $l];
                $html .= '</option>';
	}
	$html = '<select name="search_level" onChange="javascript:window.location=\'index.php' . get_all_get_params(array('search_level', 'page'),'?','&',true) . 'search_level=\'+this.options[this.selectedIndex].value"'.' class="filter">' . $html . '</select>';
	return $html;
}
// aquesta funcio extreu les coincidencies per cada client
function list_records_walk (&$listing,$demand_id,$land,$land_units)
{
    global $gl_content, $gl_caption, $gl_caption_demand_property, $gl_mls, $gl_resultats, $tpl, $gl_has_images, $gl_property_module, $gl_action;
    $caption_old = $gl_caption;
    $gl_has_images_old = $gl_has_images;
	$old_tpl = $tpl;
	$content = '';
	$gl_has_images = true;

    //change_tool('inmo', 'property', 'list_records');
    //set_config('custumer/custumer_demand_property_config.php');
    //$gl_caption = array_merge($gl_caption, $gl_caption_demand_property);

	// nomes envio mails a les que no estan marcades de cap manera
	$email_property_ids  = '';

    if (isset($gl_resultats[$demand_id]['no_mls'])) {
	    	// extrec els resultats segons el custumer_i la demanda
	    $property_ids = implode(',', $gl_resultats[$demand_id]['no_mls']);
	    // llisto les propietats coincidencies
		$show_more_link = '?menu_id=5009&action=' . $gl_action . '&demand_id=' . $demand_id; // important abans listing
        $gl_property_module->set_config('custumer/custumer_demand_property_config.php');
	    $listing = new ListRecords($gl_property_module);
		$listing->caption = array_merge($listing->caption, $gl_caption_demand_property);		
	    $listing->name = 'no_mls'.$demand_id;
	    $listing->call('get_ref', 'ref,category_id,land,land_units,price,price_private,property_id');
		$listing->set_options (0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
	    $listing->template = 'custumer/demand_property_list';
		$listing->set_var('show_more_results', $gl_resultats[$demand_id]['show_more_results']);
		$listing->set_var('show_more_link', $show_more_link);
		$listing->set_var('custumer_id', $gl_resultats[$demand_id]['custumer_id']);
		$listing->set_var('demand_id', $demand_id);
	    $listing->condition = 'inmo__property.property_id IN(' . $property_ids . ')';
		
		$listing->list_records(false);
		
		get_property_state($listing->results, $demand_id);

	    $content = $listing->parse_template();

	    foreach( $gl_resultats[$demand_id]['no_mls'] as $email_property_id){
		    if (!Db::get_first(
			    "SELECT property_id
					FROM custumer__demand_to_property
					WHERE demand_id = " . $demand_id . "
					AND property_id = " . $email_property_id . "
					AND (mailed=1 OR liked=1 OR refused=1 OR message_id<>0)"
		    )) {
			    $email_property_ids .= $email_property_id . ',';
		    }
	    }
    }

	$ret['email_property_ids'] = substr($email_property_ids, 0, -1);
	
	
    $ret['custumer_id'] = $gl_resultats[$demand_id]['custumer_id'];
    $ret['properties'] = $content;

    $ret['propertiesmls'] = list_records_walk_mls($demand_id);
    $ret +=demand_records_walk($listing,$land,$land_units,$demand_id);
    $gl_caption = $caption_old;
    $gl_has_images = $gl_has_images_old;
	$tpl = $old_tpl;
    return $ret;
}
function list_records_walk_mls ($demand_id)
{
    global $gl_content, $gl_resultats, $gl_mls, $tpl, $gl_message, $gl_property_module, $gl_action;
    $old_tpl = $tpl;
	$gl_content = '';
    if ($gl_mls && isset($gl_resultats[$demand_id]['mls'])) {

	    // extrec els resultats segons el custumer_i la demanda
	    $propertys_result = implode(',', $gl_resultats[$demand_id]['mls']);
	    // llisto les propietats coincidencies
	    $show_more_link = '?menu_id=5009&action=' . $gl_action . '&demand_id=' . $demand_id; // important abans listing
        Db::connect_mother();
        $gl_property_module->set_config('custumer/custumer_demand_property_mls_config.php');
        $listing = new ListRecords($gl_property_module);
	    $listing->name = 'mls'.$demand_id;
	    $listing->call('get_ref', 'ref,category_id,land,land_units,price,price_private,property_id');
		$listing->set_options (0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
        $listing->template =  'custumer/demand_property_mls_list';
		$listing->set_var('show_more_results', $gl_resultats[$demand_id]['show_more_results']);
	    $listing->set_var('show_more_link', $show_more_link);
	    $listing->set_var('custumer_id', $gl_resultats[$demand_id]['custumer_id']);
		$listing->set_var('demand_id', $demand_id);
		//$listing->set_var('action', $gl_action);
        $listing->condition = 'inmo__property.property_id IN(' . $propertys_result . ')';
        $listing->join = 'INNER JOIN letnd.client__client USING (client_id)';
        //$listing->before_table = 'letnd.client__client,';
		
		$listing->list_records(false);
		
        Db::reconnect();
		get_property_state($listing->results, $demand_id, true);
        Db::connect_mother();
	    $content = $listing->parse_template();

        Db::reconnect();
        $tpl = $old_tpl;
        $listing->has_results?$gl_message='':false;
        return $content;
    }
    else
    {
		return '';
	}
}
function get_property_state(&$results, $demand_id, $is_mls = false) {
	foreach ($results as &$rs){
		$client_id = $is_mls?$rs['client_id']:0;
		$property_id = $is_mls?$rs['property_id_client']:$rs['property_id'];
		
		$new_rs =Db::get_row("
			SELECT liked, mailed, refused 
			FROM custumer__demand_to_property
			WHERE demand_id = " . $demand_id . "
				AND property_id = " . $property_id . "
				AND client_id = " . $client_id);

		if ($new_rs) $rs = array_merge($rs,$new_rs);
		else $rs = array_merge($rs,array('liked'=>'0','mailed'=>'0','refused'=>'0'));
	}
}

function check_demands($type, $property_id=false, $discard = false)
{
    global $tpl, $gl_content, $gl_caption, $gl_page, $gl_caption, $gl_resultats, $template, $gl_mls;
	
	$property_id_get = R::number('property_id');
	if (!$property_id && $property_id_get) $property_id = $property_id_get; 
	
	$is_listing = true;
    // primer busco les demandes que hi han:
    $query = "SELECT * FROM custumer__demand INNER JOIN custumer__custumer USING (custumer_id) WHERE ";

    // miro si tinc de buscar per un client concret
    if (isset($_GET['custumer_id'])) {
    	$query .= "custumer_id = " . R::id('custumer_id');
    }
    // si no es un client concret, descarto tots els clients de la paperera i els desactivats
    else {
    	$query .= "(custumer__custumer.bin = 0 AND custumer__custumer.activated = 1)";
    }

    // miro si tinc de buscar una demanda concreta si no les busco totes
    if (isset($_GET['demand_id'])) {
		$query .=" AND demand_id = '" . R::id('demand_id') . "'";
		$is_listing = false;
	}

    // Només busco en demandes de menys de 2 anys - TODO-i Ronisol en tenia 5000, limito a 500 però realment potser que els resultat es trobin de la 100 a la 1100 per exemple, per tant donaria 0 resustats i és erroni .- Tot això s'ha de fer amb un CRON
	if ( $property_id ) {
		$query .= " AND entered >= CURDATE() - INTERVAL 2 YEAR ORDER BY entered DESC LIMIT 500";
	}
    $results = Db::get_rows($query);
    // comprovo les demandes una a una segons les peticions
    foreach ($results as $rs)
    {
	    $rs['category_id'] = get_query_category($rs['demand_id']);
	    $rs['municipi_id'] = get_query_municipi($rs['demand_id']);
	    $query = call_user_func_array('get_query_' . $type, array(&$rs, false, '', $is_listing, $property_id, $discard));
        $results_demand = Db::get_rows($query);

	    push_resultats($results_demand, $rs, 'no_mls',$is_listing);
	    // Debug::add('Consulta no mls',$query);
	    // Debug::add('Demands',$rs['demand_id'] );

	    /*if ($results_demand)
		    Debug::add( 'Resultats no mls', $results_demand );*/
    }
    // un cop he mirat localment, busco si es necessari per el MLS
    if ($gl_mls)
    {
	
        // - 1er necessito saber a quines empreses m'han donat permis per checkejar
        // les seves propietats MLS o les que donen permis a tothom
        $empreses = companies_with_permission();
        if ($empreses) // Si hi han empreses MLS a Buscar
        {
			// miro la categoria abans de conectar a la mare
	        foreach ($results as $key=>&$rs)
	        {
		        $rs['category_id'] = get_query_category($rs['demand_id']);
		        $rs['municipi_id'] = get_query_municipi($rs['demand_id']);
	        }

	        Db::connect_mother();
            // 2on miro segons les demandes
            foreach ($results as $rs)
            {
                $query = call_user_func_array('get_query_' . $type, array(&$rs, true, $empreses, $is_listing, $property_id,$discard));
                $results_demand = Db::get_rows($query);

                // Gravo resultats de la busqueda MLS, si en te
			    push_resultats($results_demand, $rs, 'mls',$is_listing);
			    Debug::add('Consulta mls',$query);
	            if ($results_demand)
		            Debug::add( 'Resultats', $results_demand );
            }
	        Db::reconnect();
        }
    }
}

function get_query_exact(&$rs, $is_mls, $empreses='', $is_listing, $property_id=false, $discard=false){
	
	$query = "SELECT property_id
			FROM inmo__property
			WHERE tipus = '" . $rs['tipus'] . "'
			AND price <= '" . $rs['maxprice']. "'" .
			$rs['category_id'] .
			$rs['municipi_id'] . "
			AND floor_space >= '" . $rs['floor_space'] . "'
			AND land >= '" . $rs['land'] . "'";

	// tipus 2 és opcional
	add_to_query($query, $rs, 'tipus2');


	$fields = $GLOBALS ['gl_demand_details'];

	foreach ( $fields as $field => $sign ) {
		add_to_query($query, $rs, $field, $sign);
	}


	$query .= $is_mls?" AND client_id IN (" . $empreses . ")":" AND bin = 0 AND status = 'onsale'";
	if ($property_id) $query .=" AND property_id = '" . $property_id . "'";
	
	if ($discard && !$is_mls) {
		// A l'dismiss miro per les propietats que queden descartades
		// ej. mailed = 1 , em descarta totes a les que s'han enviat emails
		$query_status = "SELECT property_id 
								FROM custumer__demand_to_property 
								WHERE demand_id = " . $rs['demand_id'] . "
								AND (" . $discard . ")";		
		$query .=" AND property_id NOT IN (" . $query_status . ")";
	}
	
	if ($is_listing) $query .=" LIMIT 4";
	return $query;
}
// -----------------------------------------------------------------------------------------------------------------
// Busquedes proximatives
/*

Tots ->
	Passa de si es 1er o 2ona ma
	Igual que -> Tipus
	Igual que opcional -> category_id, municipi, floor_space, land
	Aplica % de desnivell
Nivell 1 ->
	Major que -> room, bathroom, wc, living, dining
	Igual que -> resta de camps
Nivell 2 ->
	Major que -> resta segons quantitat -> room ( mínim 1 ), bathroom + wc ( mínim 1 ), living + dining
	Igual que -> resta de camps
Nivell 3 ->
	Major que -> resta segons quantitat -> room ( mínim 1 ), bathroom + wc ( mínim 0 )
	Ignora -> living, dining i resta de camps

*/
function add_to_query(&$query, &$rs, $what, $sign = '='){
	if ($rs[$what] && $rs[$what]!='0.00') $query .= " AND ". $what . " " . $sign . " '" . $rs[$what] . "'";
}
function get_query_prox(&$rs, $is_mls, $empreses='', $is_listing, $property_id=false, $discard = false){
    global $gl_config;
	// Agafo el % i el nivel.
    // com que esta definit a la taula ALL_configadmin ho puc agafar de la variale gl_config
    $percent = $gl_config['search_percent'];
	$level = R::escape('search_level',$gl_config['search_level']);

	$query = "SELECT property_id FROM inmo__property
				WHERE tipus = '" . $rs['tipus'] . "'" .
	            $rs['category_id'] .
	            $rs['municipi_id'];

	$maxprice =  addpercent ($rs['maxprice'], $percent);
	$floor_space =  substractpercent ($rs['floor_space'], $percent);
	$land =  substractpercent ($rs['land'], $percent);

    if ($maxprice) $query .= " AND price <= '" .  $maxprice . "'";
    if ($floor_space) $query .= " AND floor_space >= '" . $floor_space . "'";
    if ($land) $query .= " AND land >= '" . $land . "'";

	$query .= $is_mls?" AND client_id IN (" . $empreses . ")":" AND bin = 0 AND status = 'onsale'";

	// Debug::add('Nivell de busqueda',$level);
	$fields = $GLOBALS ['gl_demand_details'];

	switch ($level)
    {
        case 1:
	        foreach ( $fields as $field => $sign ) {
		        add_to_query($query, $rs, $field, $sign);
	        }
		    break;
		case 2:

			$rs['room'] = round_less($rs['room'], 1);

			join_two_fields('bathroom', 'wc', $rs, $fields, 1);
			join_two_fields('living', 'dinning', $rs, $fields, 0);

			foreach ( $fields as $field => $sign ) {
				add_to_query($query, $rs, $field, $sign);
			}

		    break;
		case 3:

			$fields_level_3 = array();
			$fields_level_3 ['room'] = $fields['room'];
			$fields_level_3 ['bathroom'] = $fields['bathroom'];
			$fields_level_3 ['wc'] = $fields['wc'];

			$rs ['room'] = round_less($rs['room'], 1);

			join_two_fields('bathroom', 'wc', $rs, $fields_level_3, 0);

			$fields = $fields_level_3;
			foreach ( $fields as $field => $sign ) {
				add_to_query($query, $rs, $field, $sign);
			}

		    break;
	}
	if ($property_id) $query .=" AND property_id = '" . $property_id . "'";
	
	if ($discard && !$is_mls) {
		// A l'dismiss miro per les propietats que queden descartades
		// ej. mailed = 1 , em descarta totes a les que s'han enviat emails
		$query_status = "SELECT property_id 
								FROM custumer__demand_to_property 
								WHERE demand_id = " . $rs['demand_id'] . "
								AND (" . $discard . ")";		
		$query .=" AND property_id NOT IN (" . $query_status . ")";
	}
	
	if ($is_listing) $query .=" LIMIT 4";
	return $query;
}
function get_query_category($demand_id){
	$query = "
		SELECT category_id
		FROM custumer__demand_to_category
		WHERE demand_id = " . $demand_id;
	$results = Db::get_rows_array($query);
	
	if ($results){
		return " AND category_id IN (" . implode_field($results) . ")";
	}
	else{
		return '';
	}
}
function get_query_municipi($demand_id){

	$ret = '';
	// Query per la comarca
	$query = "
			SELECT comarca_id
			FROM custumer__demand_to_municipi
			WHERE municipi_id = 0
				AND comarca_id != 0
				AND demand_id = " . $demand_id;
	$results = Db::get_rows_array($query);

	if ($results){
		$ret = " comarca_id IN (" . implode_field($results) . ")";
	}

	$query = "
		SELECT municipi_id
		FROM custumer__demand_to_municipi
		WHERE municipi_id != 0
			AND demand_id = " . $demand_id;
	$results = Db::get_rows_array($query);

	if ($results){
		if ($ret)
			$ret .= ") OR (";
		$ret = "municipi_id IN (" . implode_field($results) . ")";
	}

	if ($ret)
		$ret =  ' AND (' . $ret . ')';

	return $ret;
}

function companies_with_permission ()
{
    Db::connect_mother ();
    // Miro les que em donen permis a mi o les que donen permis a tothom
    //$query = "SELECT client_id1 FROM mls__client_to_client WHERE permission = 1 AND client_id2 = '" . $_SESSION['client_id'] . "' OR permission = 3 ";
    $query = "SELECT client_id1 FROM mls__client_to_client WHERE permission = 'accepted' AND client_id2 = '" . $_SESSION['client_id'] . "'";
	$companiesmls = Db::get_rows ($query);
    Db::reconnect();
    return implode_field($companiesmls, 'client_id1');
}

function push_resultats(&$results_demand, &$rs, $is_mls, $is_listing){
	/* si hi ha coincidencies les guardo a un array multidimensional asociatiu:
	 $gl_resultats[demand_id][no_mls][0] = property_id
	 $gl_resultats[demand_id][mls][0] = property_id */
	global $gl_resultats;
	
	if (!$results_demand) return;
	
	if (count($results_demand)==4 && $is_listing){
		$gl_resultats[$rs['demand_id']]['show_more_results'] = true;
		array_pop ($results_demand);
	}
	else{	
		$gl_resultats[$rs['demand_id']]['show_more_results'] = false;
	}
	
	$gl_resultats[$rs['demand_id']]['custumer_id'] = $rs['custumer_id'];
	
	foreach($results_demand as $property){
		$gl_resultats[$rs['demand_id']][$is_mls][] = $property['property_id'];
	}
}

function addpercent ($value, $percent) // to add up -->sumar
{
    return $value + (($value * $percent) / 100);
}
function substractpercent ($value, $percent) // to substract->restar
{
    return $value - (($value * $percent) / 100);
}
function round_less ($value, $minimum)
{
    if ($value > $minimum)
    {
        switch ($value)
        {
            case $value <= 4:
                $value = $value -1;
                break;
            case ($value >= 5 AND $value <= 8):
                $value = $value -2;
                break;
            case ($value >= 9 AND $value <= 13):
                $value = $value -3;
                break;
            case $value >= 14:
                $value = $value -4;
                break;
        } // switch
    }
    return $value;
}

function join_two_fields($field1, $field2, &$rs, &$fields, $minimum) {

			$baths = $rs[$field1] + $rs[$field2];
			$fields["($field1 + $field2)"] = '>=';
			$rs["($field1 + $field2)"] = round_less($baths, $minimum);
			unset( $fields[$field1] );
			unset( $fields[$field2] );

}
?>