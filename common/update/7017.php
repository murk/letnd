<?

/* 12-6-17
  Afegir pagament a booking

 */
if ( is_table( 'booking__configadmin' ) && ! Db::get_first( "SELECT * FROM booking__configadmin WHERE name='has_payment'" ) ) {
	$update_sql[] = "
		INSERT INTO booking__configadmin (`name`, `value`) VALUES ('has_payment', '0');";

} else {
	if ( is_table( 'booking__configadmin' ) ) {
		trigger_error( 'Ja existeixen el camp has_payment' );
	}
}
?>