<?if (!$results):?>
	<?if ($options_bar):?>
	<div id='opcions_holder'><table width="100%" border="0" cellspacing="0" cellpadding="0" id="opcions">
		<tr>
		  <td class="opcions"><div id="opcions_fixed_bar"><div class="more"></div></div><table class="filters">
			<tr valign="middle">
			<?foreach($options_filters as $options_filter):?><form>
			  <td><?=$options_filter?></td>
			  </form>
			  <?endforeach //$options_filters?>
			  </tr></table><?if ($options_filters):?><div class="sep"></div><?endif //options_filters?>
			  <table border="0" cellpadding="2" cellspacing="0"><tr valign="middle">
			  <?if ($options_buttons):?>
			  <td class="buttons">
			  <?foreach($options_buttons as $options_button):?><?=$options_button?><?endforeach //$options_button?>
			  </td><?endif //options_buttons?>
			</tr>
		  </table></td>
		</tr>
	</table>
	</div>
	<?endif //$options_bar?>
<?endif //!$results?>
<?if ($results):?><?if ($split_results_count):?>
<div id='split_holder'><table width="100%" border="0" cellpadding="0" cellspacing="0" id="split">
  <tr valign="middle"> 
    <td width="20%" align="left" nowrap class="pageResults"><?=$split_results_count?></td>
    <td align="right" nowrap class="pageResults"><?=$split_results?></td>
  </tr>
</table>
</div>
<?endif //$split_count?>
<?if ($options_bar):?>
<div id='opcions_holder'><table width="100%" border="0" cellspacing="0" cellpadding="0" id="opcions">
    <tr>
      <td class="opcionsTitol"><strong><?=$c_options_bar?></strong></td>
    </tr>
    <tr>
      <td class="opcions"><div id="opcions_fixed_bar"><div class="more"></div></div><table class="filters">
        <tr valign="middle">
		<?foreach($options_move_tos as $options_move_to):?>
          <td><?=$options_move_to?></td>
		  <?endforeach //$options_move_tos?>
		<?foreach($options_filters as $options_filter):?><form>
          <td><?=$options_filter?></td>
          </form>
		  <?endforeach //$options_filters?>
          </tr></table><?if ($options_filters):?><div class="sep"></div><?endif //options_filters?>
          <table border="0" cellpadding="2" cellspacing="0"><tr valign="middle">
		  <?if ($options_buttons):?>
		  <td class="buttons">
		  <?foreach($options_buttons as $options_button):?><?=$options_button?><?endforeach //$options_button?>
		  </td><?endif //options_buttons?>
		<?if ($save_button):?>
          <td><strong><a class="botoOpcionsGuardar" href="javascript:submit_form_save('save_rows_save_records<?=$action_add?>')">
            <?=$c_send_edit?>
          </a></strong></td>
		  <?endif //save_button?><?if ($select_button):?>
          <td><?=$c_select?>
            &nbsp;<strong><a class="opcions" href="javascript:select_all(true, document.list)">
              <?=$c_all?>
              </a>&nbsp;-&nbsp;<a class="opcions" href="javascript:select_all(false, document.list)">
                <?=$c_nothing?>
              </a></strong></td>
		  <?endif //select_button?><?if ($delete_button):?>
          <?if ($has_bin):?>
          <?if ($is_bin):?>
          <td><strong><a class="botoOpcionsBorrar" href="javascript:submit_list('save_rows_delete_selected<?=$action_add?>', '<?=$c_confirm_deleted?>', true)">
            <?=$c_delete_selected?>
          </a></strong></td>
          <td><strong><a class="botoOpcionsGuardar" href="javascript:submit_list('save_rows_restore_selected<?=$action_add?>')">
            <?=$c_restore_selected?>
          </a></strong></td>
          <?else:?>
          <td><strong><a class="botoOpcionsBorrar" href="javascript:submit_list('save_rows_bin_selected<?=$action_add?>')">
            <?=$c_delete_selected?>
          </a></strong></td>
          <?endif // is_bin?>
          <?else:?>
          <td><strong><a class="botoOpcionsBorrar" href="javascript:submit_list('save_rows_delete_selected<?=$action_add?>', '<?=$c_confirm_deleted?>', true)">
            <?=$c_delete_selected?>
          </a></strong></td>
          <?/*<td>
              <td>&nbsp;</td><strong><a class="botoOpcions" href="/admin?menu_id=<?=$menu_id?>&sort_by=0&sort_order=<?=$sort_order0?>">
                <?=$c_move_to?>
               
            <input type="submit" name="bin_selected" value="<?=$c_delete_selected?>" class="boto1">
              </a></strong></td>*/?>
          <?endif // has_bin?><?endif //delete_button?>
		  <?if ($print_button):?>
          <td><strong><a class="botoOpcions" href="<?=$print_link?>">
            <?=$c_print_all?>
          </a></strong></td>
		  <?endif //delete_button?>
        </tr>
      </table></td>
    </tr>
</table></div>
<div class="sep"></div>
	<?if ($order_bar): // ho poso dins de opcions bar per que no els surti als agents ?>
	<div class="sep"></div>
	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="listRecord">
	<tr><td class="listCaption">
	<table border="0" cellspacing="0" cellpadding="0" class="noPadding" >
	  <tr>
		<td valign="top" nowrap="nowrap"><strong>
		  &nbsp;<?=$c_order_by?>&nbsp;&nbsp;
		  </strong></td>
		  <td valign="top">
		<?foreach($fields as $l): extract ($l)?><strong><a class="opcions" href="<?=$order_link?>">
			<?=$field?>
			</a></strong>
			<?if ($order_image):?>
			  <img src="/admin/themes/inmotools/images/<?=$order_image?>.gif" width="9" height="9" border="0" align="middle">
			  <?endif //order_image?>
			&nbsp;
		  <?endforeach //$field?></td>
	  </tr>
	</table>
	</td>
	</tr>
	</table>
	<?else: //$order_bar?>
	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="listRecord">
	  <tr>
		<td style="padding:0px"></td>
	  </tr>
	</table>
	<?endif //$order_bar?>
<?endif //$options_bar?>
<form method="post" action="<?=$form_link?>" name="list" onsubmit="return check_selected(this)" target="save_frame">
<input name="action" type="hidden" value="save_rows"><?=$out_loop_hidden?>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="listRecord" style="margin-top:0px">
   
    <?foreach($loop as $l): extract ($l)?>
      <tr class="listItem<?=$odd_even?>" onmousedown="change_bg_color(this, 'Down', '<?=$id?>')" id="list_row_<?=$id?>">
        <td valign="top" class="listImage" width="<?=$im_admin_thumb_w?>">	  
	  <?if ($image_name):?><div><img width="<?=$im_admin_thumb_w?>" src="<?=$image_src_admin_thumb?>"></div>
	  <?else: // image_name?><div class="noimage" style="min-height:<?=$im_admin_thumb_h?>px;"></div>
	  <?endif // image_name?>
	  </td>
        <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0" class="noPadding">
            <tr>
              <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
                  <tr><?if ($options_checkboxes):?> 
                    <td width="10"><input name="selected[<?=$conta?>]" type="checkbox" value="<?=$id?>" onclick="return false;"<?=$selected?>/></td><?endif // options_checkboxes?>
                    <td class="listTitleProperty">
						<input name="agent_id[<?=$agent_id?>]" type="hidden" value="<?=$agent_id?>" />
						<?=$nom?> <?=$cognoms?><?if($telefon):?> - <?=$telefon?><?endif //telefon?>
                      </td>
                    </tr>
                </table></td>
              </tr>
            <tr>
              <td><table width="99%" border="0" cellpadding="0" cellspacing="0">
                  <tr>
                    <td valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                      <tr>
                        <td height="30" valign="top" class="listTitleProperty2"><?=$agencia?></td>
                        <td height="30" align="right" valign="top" class="listResaltat">API: <span class="listTitleProperty2"><?=$api_id?></span> - AICAT: <span class="listTitleProperty2"><?=$aicat_id?></span></td>
                      </tr>
                    </table>
                      <table width="100%" border="0" cellpadding="0" cellspacing="0" class="listDetails2">
                        <tr>
                          <td valign="top"><strong class="listItem">
                            <?=$c_adresa?>
                            :</strong>&nbsp;</td>
							<?if($speciality_id):?>
                          <td valign="top"><strong class="listItem">
                            <?=$c_speciality_id?>
                            :</strong>&nbsp;</td>
							<?endif //speciality?>
	                        <? if( $files && !$user_is_agent ): ?>
                          <td valign="top"><strong class="listItem">
                            <?=$c_file?>
                            :</strong>&nbsp;</td>
				            <? endif // $files ?>
                        </tr>
                        <tr>
                          <td valign="top">
							<?=$adresa?>
								<br />
								<?=$cp?>
								<?=$municipi_id?>
								<br />
								<?=$comarca_id?> - 
								<?=strtoupper($provincia_id)?>
								<br />
							<br />
								Tel. <?=$telefon?><?if ($telefon2):?> / <?=$telefon2?><? endif //$telefon2?>
								<? if ($fax):?>
									<br />
								Fax. <?=$fax?><? endif //$fax?>
								<? if ($url1):?>
									<br />
									<a href="http://<?=$url1?>" target="_blank"><?=$url1?></a>
								<? endif //$mail?>
								<? if ($correu):?>
									<br />
									<a href="mailto:<?=$correu?>" target="_blank"><?=$correu?></a>
								<? endif //$correu?>
							</td>
							<?if($speciality_id):?>
                          <td valign="top">
							<?=$speciality_id?>
							</td>
							<?endif //speciality?><? if( $files && !$user_is_agent ): ?>
                            <td valign="top">
				                  <? foreach( $files as $l ): extract( $l ) ?>
					                  <a class="files" target="_blank" href="<?= $file_src ?>"><?= $file_title ?></a>
					                  <br>
				                  <? endforeach //files?>
				              </td>
				            <? endif // $files ?>
                        </tr>
                      </table>
					  </td>
                    <td width="300" valign="top" class="listDescription"><div style="height:30px;"></div><div style="padding-bottom:8px;"><span class="formsHor">
                      <?=$agent_description?>
                    </span></div></td>
                  </tr>
                </table>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td align="right"><div class="right"><?foreach($buttons as $button):?><?=$button?><?endforeach //$buttons?></div></td>
                  </tr>
                  <tr>
                    <td><img src="/admin/themes/inmotools/images/spacer.gif" width="1" height="10"></td>
                  </tr>
                </table></td>
              </tr>
          </table></td>
        </tr>
      <?endforeach //$loop?>  
</table> 
  </form>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td height="10"></td>
  </tr>
</table> <?if ($split_results_count):?>
  <table width="100%" border="0" cellpadding="0" cellspacing="0">
    <tr valign="middle">
      <td width="20%" align="left" nowrap class="pageResults"><?=$split_results_count?></td>
      <td align="right" nowrap class="pageResults"><?=$split_results?></td>
    </tr>
  </table><?endif //$split_count?>
<?endif //results?>