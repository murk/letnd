<?

/**
 * AdmintotalQuery
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2016
 * @version $Id$
 * @access public
 */
class AdmintotalQuery extends Module {

	function __construct() {
		parent::__construct();
	}

	function list_records() {
		$GLOBALS['gl_content'] = $this->get_records();
	}

	function get_records() {
		global $gl_is_local;
		$template = 'admintotal/query.tpl';
		$tpl = &$this->tpl;
		$tpl->set_file( $template );
		$loop            = array();
		$loop_results    = array();
		$loop_no_results = array();
		$loop_errors     = array();

		$class = '';

		$q = R::get('q_query');

		if ( $q ) {
			Db::connect_mother();


			if ( $gl_is_local ) {
				$query           = 'SHOW DATABASES';
				$results_clients = db::get_rows( $query );

				foreach ( $results_clients as &$rs ) {
					$rs['host']       = 'localhost';
					$rs['dbusername'] = 'root';
					$rs['dbpwd']      = '1';
					$rs['dbname']     = $rs['Database'];
					$rs['domain']     = 'w2.' . $rs['Database'];
				}
			} else {

				$query           = 'SELECT  `client_id`,  `fiscalname`,  `solution`,  `domain`,  `2domain`,  `3domain`,  `host`,  `dbname`,  `dbusername`,  `dbpwd`,  `client_dir` FROM `letnd`.`client__client` ORDER BY domain';
				$results_clients = db::get_rows( $query );

			}
			foreach ( $results_clients as $client ) {
				extract( $client );

				if ( $host && $dbusername && $dbpwd && $dbname && $host == 'localhost' ) {


					$host = '127.0.0.1';
					if ( $gl_is_local ) {
						$dbpwd = '';
					}

					// BUCLE DE CONSULTES PER BBDDs
					$cn = mysqli_connect( $host, $dbusername, $dbpwd );
					if ( $cn ) {
						mysqli_select_db( $cn, $dbname );

						$query_type = substr( $q, 0, 5 ) == 'SHOW ' ? '' : 'SELECT ';
						$results    = mysqli_query( $cn, $query_type . $q );

						if ( $results!== false && mysqli_num_rows( $results ) != 0 ) {
							$consulta = "<table>";
							while ( $rs = mysqli_fetch_assoc( $results ) ) {
								$consulta .= '<tr>';
								foreach ( $rs as $f ) {
									$consulta .= '<td>' . $f . '</td>';
								}
								$consulta .= '</tr>';
							}
							$consulta .= "</table>";
							$loop_results[] = array( 'consulta' => $consulta, 'domain' => $domain, 'class' => $class );
						} elseif ( $results === false ) {
							$consulta      = 'Error: ' . mysqli_error($cn);
							$loop_errors[] = array( 'consulta' => $consulta, 'domain' => $domain, 'class' => $class );
						} else {
							$consulta          = 'Cap resultat';
							$loop_no_results[] = array(
								'consulta' => $consulta,
								'domain'   => $domain,
								'class'    => $class
							);
						}
						$loop[] = array( 'consulta' => $consulta, 'domain' => $domain, 'class' => $class );
					} else {
						$consulta      = 'Error: ' . mysqli_error($cn);
						$loop_errors[] = array( 'consulta' => $consulta, 'domain' => $domain, 'class' => $class );
					}

				}
			}

			Db::reconnect();
		}


		$tpl->set_var( 'q', htmlspecialchars( $q ) );
		$tpl->set_var( 'loop', $loop );
		$tpl->set_var( 'loop_results', $loop_results );
		$tpl->set_var( 'loop_no_results', $loop_no_results );
		$tpl->set_var( 'loop_errors', $loop_errors );
		return $tpl->process();
	}
}

?>