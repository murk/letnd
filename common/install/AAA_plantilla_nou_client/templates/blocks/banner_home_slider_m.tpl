<section id="banner-home">
	<? /*

	TODO-i Per posar la imatge com a background del banner -- CANVIAR el segon loop per loop2

	<?$loop2 = $loop?>
	<style scoped>
		<?foreach($loop as $l): extract ($l)?>
		<?if($banner_src):?>
		#slider-home .banner<?=$banner_id?> {
			background-image:url(<?=$banner_src?>);
		}
		<?endif ?>
		<?endforeach //$loop?>
	</style>

	*/ ?>

	<div id="slider-home">
		<? foreach ( $loop as $l ): extract( $l ) ?>
		<? // foreach ( $loop2 as $l ): extract( $l ) ?>

			<div <? if ($url1) echo 'onclick="window.location.href=\'' . addslashes($url1) . '\'"' ?> class="banner<?=$banner_id?> item<?= $conta > 0 ? ' hidden' : '' ?> ">

				<h3>
					<? if ($url1): ?><a target="<?= $url1_target ?>" href="<?= $url1 ?>" title="<?= htmlspecialchars( $url1_name ) ?>">
						<? endif //url1?>

						<?= $url1_name ?>

						<? if ($url1): ?></a><? endif //url1?>
				</h3>

				<div class="text"><?= $banner_text ?></div>


	            <?if($banner_src):?>
				<img src="<?=$banner_src?>" height="<?=$banner_height?>" width="<?=$banner_width?>" alt="<?=htmlspecialchars($url1_name)?>" />
				<?endif //banner_src?>

			</div>

		<? endforeach //$loop?>
	</div>
</section>