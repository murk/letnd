<?

/* 9-11-16

	Canvis courier


 */

if ( is_table( 'product__courier' ) && ! is_field( 'product__courier', 'default_courier' ) ) {

	$update_sql[] = "ALTER TABLE `product__courier`
	ADD COLUMN `default_courier` TINYINT(1) NOT NULL DEFAULT '0' AFTER `courier_url`;";

} else {
	if ( is_table( 'product__courier' ) ) {
		trigger_error( 'Ja existeixen el camp default_courier' );
	}
}


?>