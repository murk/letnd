<?

class ProductProductHome extends Module {
    function __construct()
    {
        parent::__construct();
    }
	function on_load(){
		// si es privada no deixo fer res excepte entrar login i password
		if ($this->is_index && $this->config['is_private_shop'] && !$this->is_logged()) {
			redirect('/?tool=product&tool_section=customer&action=login&language='.LANGUAGE);
			die();
		}
	}
    function list_records()
    {
        $GLOBALS['gl_content'] = $this->get_records();
    }
    function get_records()
    {
        $vars = array(
        	'destacat'=>'',
        	'loop_familys'=>array()
        );


	    // es llisten totes directament, sense home
	    if (PRODUCT_HOME_LIST_ALL){
		    $module = Module::load('product', 'product');
		    $ret = $module->do_action('get_records');
		    $this->caption = $module->caption; // pels other
		    return $ret;
	    }
		// ------------------------------
        // Llistat automatic de la primera familia
        // ------------------------------
    	elseif (
			$GLOBALS['gl_is_home'] && $this->config['home_show_first_family']
				||
			!$GLOBALS['gl_is_home'] && $GLOBALS['gl_is_tool_home'] && $this->config['home_tool_show_first_family']			
				||
			$GLOBALS['gl_is_home'] && $this->config['home_show_subfamilys']
				||
			!$GLOBALS['gl_is_home'] && $GLOBALS['gl_is_tool_home'] && $this->config['home_tool_show_subfamilys']				
				) {
			
			$config_prefix = $GLOBALS['gl_is_tool_home']?'home_tool_':'home_';
			
			if($this->config[$config_prefix . 'show_first_family']){
				
				$_GET['family_id'] = $this->get_first_family();
				
			}

			$module = Module::load('product', 'product'); //crido el modul
			$module->is_home = true;
		    $ret = $module->do_action('get_records');
		    $this->caption = $module->caption; // pels other
			return $ret;
			
		}
		// ------------------------------
        // Llistat de productes destacats
        // ------------------------------
    	elseif ($this->config['home_show_marked']) {
			//$GLOBALS['gl_db_max_results'] = $this->config['home_max_results'];
			$module = Module::load('product', 'product'); //crido el modul
			$module->parent = 'ProductProductHome';
			$module->condition = "AND destacat = 1"; //faig una condició per els destacats
    		$module->limit = '0, ' . $this->config['home_max_results'];
			$listing = $module->do_action('get_records'); //crido l'accio del product__product que em retorna el listing per poder utilitzar-lo aquí

		    $this->caption = $module->caption; // pels other

			// si tinc un arxiu "product_home_list" el faig servir de plantilla per poder fer un llistat diferent a la home
			if (file_exists(CLIENT_DIR.'/templates/product/product_home_list.tpl')) {
				$listing->template = 'product/product_home_list';
			}	
			
			// si no hi han destacats ni llistat de famílies llisto la primera família amb resultats per emplenar la home
			if (!$listing->has_results && !$this->config['home_show_familys'])
			{
				$family_id = $this->get_first_family();
				if ($family_id){
					$listing->condition = "(status = 'onsale' OR status = 'nostock' OR status = 'nocatalog') AND family_id = " . $family_id;
					$module->give_me_information_family($family_id, $listing, true);
					// $GLOBALS['gl_page']->title = $family_information[0];
					$vars['destacat'] = $listing->list_records();
				}
				// else    ------> s'haurà de fer algo si no hi ha cap familia amb productes
			}
    		// si no hi ha resultats de destacats no vull que surti el missatge de "En estos momentos no hay ningún producto para esta categoria"
			elseif ($listing->has_results){				
				$vars['destacat'] = $listing->list_records();
			}
    	}
		if (PRODUCT_HOME_TITLE) $GLOBALS['gl_page']->title = PRODUCT_HOME_TITLE;

		// ----------------------------------------------------------------------
        // Llistat de famílies, subfamílies i productes destacats de cada subfamilia
        // ----------------------------------------------------------------------

		if ($this->config['home_show_familys']) {
			// si no tinc un $listing, en cas que no mostri destacats, el faig de nou aquí	
			if (!isset($listing)){
				$module = Module::load('product', 'product');
				$module->parent = 'ProductProductHome';
				$listing = $module->do_action('get_records'); //crido l'accio del product__product que em retorna el listing per poder utilitzar-lo aquí
			}
			
	    	// de moment agafarem exactament les mateixes families del block
			$home_max_familys = $this->config['home_max_familys']; // passo el limit de famílies a el block
	    	include(DOCUMENT_ROOT.'public/blocks/product_family.php');

    		$listing->limit = '0, ' . $this->config['home_max_family_products'];
			
	        foreach ($loop as $key=>$value) {
						
			$image = UploadFiles::get_record_images($value['id'], false, 'product', 'family');
				foreach ($image as $k=>$v){
					$loop[$key]['family_'.$k] = $v;
				}
	        	//$listing = new ListRecords($this);
	        	$listing->condition = "(status = 'onsale' OR status = 'nostock') AND family_id = " . $value['id'];
				//Debug::p($listing->condition, 'text');
				$listing->order_by = 'destacat DESC, ordre ASC, product_title ASC';
	        	$listing->set_records(); // es com un list_records però no obté la variable $content, així puc posar les variables del llistat en un altre template: $listing->loops['loop'] i $listing->vars
	            $loop[$key]['loop2'] = $listing->loops['loop'];		 
			 
	        }

	    	//Debug::p($loop);
	        //$vars += $listing->vars;
	        $vars['loop_familys'] = $loop;
		}


        // Assigno a la plantilla de la home
        // ----------------------------
        $this->set_file('product/product_home.tpl');
        $this->set_vars($vars);
        $this->set_vars($this->caption);
        return $this->process();
    }
	function get_first_family(){
		$query = "SELECT product__family.family_id AS family_id
			  	FROM product__family, product__family_language
				INNER JOIN product__product
				USING (family_id)
				WHERE product__product.bin = 0
				AND (status = 'onsale' OR status = 'nostock' OR status = 'nocatalog')
				AND product__family_language.family_id = product__family.family_id
				AND language = '" . LANGUAGE . "'
				AND product__family.parent_id= 0
				AND product__family.bin = 0
				ORDER BY product__family.ordre ASC, product__family_language.family
				LIMIT 1 ";
		$family_id = Db::get_first($query);
		return $family_id;
	}

	/**
	 * ProductCustomer::is_logged()
	 * Saber si està loguejat
	 * @return
	 */
	function is_logged(){
		// miro si hi ha les 2 sessions per més seguretat de que esta logged
		return (isset($_SESSION['product']['customer_id']) && isset($_SESSION['product']['mail']));
	}
}

?>