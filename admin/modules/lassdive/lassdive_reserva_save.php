<?

/**
 * LassdiveReservaSave
 *
 * Funcions comuns per Lassdive Reserves
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2018
 * @version 1
 * @access public
 */
class LassdiveReservaSave {

	var $is_admin;

	function save_rows() {
		$is_order     = R::post( 'order_id' );
		$is_orderitem = R::post( 'orderitem_id' );

		if ( $is_order ) {
			$tool         = 'product';
			$tool_section = 'order';
			$config_file  = 'lassdive/lassdive_order_config.php';
		}
		else if ( $is_orderitem ) {
			$tool         = 'lassdive';
			$tool_section = 'orderitem';
			$config_file  = 'lassdive/lassdive_orderitem_config.php';
		}
		else {
			return;
		}


		$module    = Module::load( $tool, $tool_section, '', false, $config_file );
		$save_rows = new SaveRows( $module );
		$save_rows->save();

		$this->save_order_items();
		$GLOBALS['gl_message'] = $save_rows->message_list_saved;

	}

	public function save_order_items() {

		$inputs_prefix = 'product__orderitem_saved';

		if ( isset( $_POST[ $inputs_prefix ] ) ) {

			// Guardo els is_dones
			$post     = $_POST[ $inputs_prefix ];
			$is_dones = R::post( 'is_dones' );
			$is_dones = R::post( 'is_dones' );
			$query    = '';
			$conta    = 0;
			if ( $is_dones ) {
				foreach ( $is_dones as $reservaitem_id => $val ) {
					$is_done = ( isset( $post['is_done'][ $reservaitem_id ] ) ) ? '1' : '0';
					$query   .= "
					UPDATE lassdive__orderitem 
					SET is_done = $is_done
					WHERE reservaitem_id = $reservaitem_id;
				";
				}
			}
			Db::multi_query( $query );

		}
	}


	public function write_record() {

		$module   = Module::load( 'product', 'order', '', false, 'lassdive/lassdive_order_config.php' );
		$writerec = new SaveRows( $module );
		$writerec->save();

		$this->save_order_items();

		$order_id = $writerec->id;

		if ($writerec->action != 'save_record') return $order_id;

		$this->delete_order_items( $order_id );

		return $order_id;

	}

	private function delete_order_items( $order_id ) {

		$inputs_prefix = 'product__orderitem_saved';

		$delete_ids = $_POST[ $inputs_prefix ]['delete_record_ids'];
		if ( ! $delete_ids ) return;
		$GLOBALS ['gl_reload'] = true;

		$delete_ids = explode( ',', $delete_ids );
		R::escape_array( $delete_ids );

		foreach ( $delete_ids as $delete_id ) {
			$query        = "SELECT orderitem_id FROM lassdive__orderitem WHERE reservaitem_id = '$delete_id'";
			$orderitem_id = Db::get_first( $query );

			if ( ! $orderitem_id ) continue;

			$m_query = "
				DELETE FROM lassdive__orderitem WHERE reservaitem_id = $delete_id;
				
			
				UPDATE product__orderitem 
				SET quantity = quantity-1 
				WHERE orderitem_id = $orderitem_id;
				
				DELETE FROM product__orderitem WHERE quantity = 0 AND orderitem_id = $orderitem_id;
				
				# recalculo orderitem
				UPDATE product__orderitem 
				SET product_total_base = product_base * quantity,
				product_total_tax = product_tax * quantity,
				product_total_tax = product_tax * quantity,
				product_total_basetax = product_basetax * quantity,
				product_total_equivalencia = product_equivalencia * quantity
				WHERE orderitem_id = $orderitem_id;";
			Db::multi_query( $m_query );

		}

		// recalculo order_id
		$query = "
			SELECT 
				sum(product_total_base)  AS product_total_base,
				sum(product_total_tax)  AS product_total_tax,
				sum(product_total_equivalencia)  AS product_total_equivalencia,
				sum(product_total_basetax)  AS product_total_basetax
			FROM product__orderitem
			WHERE order_id = $order_id";
		$rs    = Db::get_row( $query );

		$total_base         = $rs['product_total_base'];
		$total_tax          = $rs['product_total_tax'];
		$total_equivalencia = $rs['product_total_equivalencia'];
		$total_basetax      = $rs['product_total_basetax'];


		$query = "
				UPDATE product__order 
				SET total_base = '$total_base',
				total_tax = '$total_tax',
				total_equivalencia = '$total_equivalencia',
				total_basetax = '$total_basetax',
				all_base = $total_base + rate_base,
				all_tax = $total_tax + rate_tax,
				all_equivalencia = $total_equivalencia,
				all_basetax = $total_basetax + rate_basetax - promcode_discount
				WHERE order_id = $order_id";
		Db::execute( $query );
	}

}

?>