<?php
// menus eina

define('NEWS_MENU_NEW', 'Edició de notícies');
define('NEWS_MENU_NEW_FORM', 'Entrar nova notícia');
define('NEWS_MENU_NEW_LIST', 'Llistar i Editar notícies');
define('NEWS_MENU_NEW_BIN', 'Paperera de reciclatge');

// nous globals
$gl_caption['c_prepare'] = 'En preparació';
$gl_caption['c_review'] = 'Per revisar';
$gl_caption['c_public'] = 'Públic';
$gl_caption['c_archived'] = 'Arxivat';
$gl_caption['c_new'] = 'notícia';
$gl_caption['c_title'] = 'Títol';
$gl_caption['c_subtitle'] = 'Subtítol';
$gl_caption['c_content'] = 'Contingut';
$gl_caption['c_ordre'] = 'Ordre';
$gl_caption['c_entered'] = 'Data de creació';
$gl_caption['c_status'] = 'Estat';

$gl_caption_image['c_name'] = 'Nom';

?>
