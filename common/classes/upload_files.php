<?php
/**
 * * pujar arxius del client i insertar-los a la BBDD
 *                                             -puja els arxius, els graba a la BBDD amb el id, nom i tamany
 *                                             -actualitza els arxius, borra els descartats i puja els nous
 *                                             - borra tots els arxius i registres de la BBDD relacionats amb un registre
 */

class UploadFiles
{
    var $dir, $table, $id_relation_col, $id_col, $deleted_files, $type, $files = array(), $new_files = array();
	var $action, $fields, $language_fields, $has_languages, $id_field, $messages, $tool, $tool_section, $config;
    // nom de la taula, nom de la columna amb id de la taula relacionada, nom de la columna id principal
    /**
     * Constructor
     *
     * @access protected
     */
    function __construct(&$module, $table, $id_relation_col, $dir, $type)
    {
        $this->table = $table;
        $this->id_relation_col = $id_relation_col;
        $this->dir = $dir;
        $this->type = $type;
        $this->id_col = $this->type . '_id';

		// aquí es podria fer servir el module_base si    if !($module && $module->is_v3) $module = false
		if ($module && $module->is_v3) {
			$this->action = &$module->action;
			$this->fields = &$module->fields;
			$this->language_fields = &$module->language_fields;
			$this->id_field = &$module->id_field;
			$this->messages = &$module->messages;
			$this->tool = &$module->tool;
			$this->tool_section = &$module->tool_section;
			$this->has_images = &$module->has_images;
			$this->config = &$module->config;
    	}
		else{
			// globals per eliminar a v3
			global $gl_db_classes_id_field, $gl_action, $gl_messages, $gl_db_classes_fields, $gl_db_classes_language_fields, $gl_tool, $gl_tool_section, $gl_has_images;
			$this->action = $gl_action;
			$this->fields =  $gl_db_classes_fields;
			$this->id_field = $gl_db_classes_id_field;
			$this->language_fields = $gl_db_classes_language_fields;
			$this->messages = $gl_messages;
			$this->tool = $gl_tool;
			$this->tool_section = $gl_tool_section;
			$this->has_images = &$gl_has_images;
			$this->config = &$GLOBALS['gl_config'];
		}
		$this->has_languages = in_array($this->type, $this->language_fields);
    }

    /**
     * * Guarda les imatges al directori i a la BBDD des d'un POST
     *
     * @access public
     * @return void
     */
    function get_files()
    {
        //
		// HE CANVIAT TOTS ELS THIS->ID_FIELD  PER THIS->id_relation_col
		//
		global $gl_insert_id, $gl_is_admin;

        if ($gl_is_admin){
            //set_time_limit( 30 * 60 ); // 30 minuts
        }

        // borrem els que hi ha araz
        $id_field = $gl_insert_id?$gl_insert_id:$_POST[$this->id_relation_col];
        // REVISAR per que $_POST[$gl_db_classes_id_field] es array i crec que si bens del manage_images no???
        if (is_array($id_field)) $id_field = current($id_field);
		
		// comprovo que sigui numeric
		Debug::p($_POST, $this->id_relation_col);
		Debug::p($id_field, 'id_field');
		Debug::p($_FILES, 'Files');
		$id_field = R::id($id_field,false,false);

        $form_id_field = $gl_insert_id?0:$id_field; // si es formulari nou es igual a 0
		
        // borrar imatges excloses al editar el registre
        // image_manager te l'acció de save_rows_get_images , per tant no borra ni copia imatges per aquest sistema
        if (strpos($this->action, 'copy_record') === 0)
        {
            $this->_copy_previous_files();
        } elseif (strpos($this->action, 'save_record') === 0)
        {
            $this->_delete_previous_files();
            $this->_save_previous_files_data();
        }
        Debug::add ('_FILES', $_FILES);

        $ordre = $this->_get_next_ordre($id_field);

        foreach ($_FILES as $key => $upload_file)
        {
            Debug::add ('Key', $key);
            Debug::add ('Nom', $upload_file['name']);
            Debug::add ('Tmp', $upload_file['tmp_name']);
            Debug::add ('dir', $this->dir);
            Debug::add ('error', $upload_file['error']);

            // input file multiple
            if (is_array ($upload_file['name'])) {


                Debug::p( [
                            [ 'form_id_field', $form_id_field ],
                            [ 'upload_file[\'name\']', $upload_file['name'] ],
                        ], 'uploadFiles');

                // si te idiomes faig el bocle pels idiomes, sino faig un array fake
                if ($this->has_languages){
                    foreach ( $upload_file['name'][ $form_id_field ] as $lang => $u ) {
                        Debug::p( $lang, 'Idioma'  );
                        foreach ( $upload_file['name'][ $form_id_field ][$lang] as $k => $v ) {

                            $upload_file2             = array();
                            $upload_file2['name']     = $upload_file['name'][ $form_id_field ][ $lang ][ $k ];
                            $upload_file2['type']     = $upload_file['type'][ $form_id_field ][ $lang ][ $k ];
                            $upload_file2['tmp_name'] = $upload_file['tmp_name'][ $form_id_field ][ $lang ][ $k ];
                            $upload_file2['error']    = $upload_file['error'][ $form_id_field ][ $lang ][ $k ];
                            $upload_file2['size']     = $upload_file['size'][ $form_id_field ][ $lang ][ $k ];

                            Debug::p( $upload_file2, 'upload_file2: ' . $lang );

                            $this->_get_file( $key . '_' . $lang . $k, $upload_file2, $id_field, $ordre );
                        }
                    }

                }
                else {
                    foreach ( $upload_file['name'][ $form_id_field ] as $k => $v ) {

                        $upload_file2             = array();
                        $upload_file2['name']     = $upload_file['name'][ $form_id_field ][ $k ];
                        $upload_file2['type']     = $upload_file['type'][ $form_id_field ][ $k ];
                        $upload_file2['tmp_name'] = $upload_file['tmp_name'][ $form_id_field ][ $k ];
                        $upload_file2['error']    = $upload_file['error'][ $form_id_field ][ $k ];
                        $upload_file2['size']     = $upload_file['size'][ $form_id_field ][ $k ];

                        $this->_get_file( $key . $k, $upload_file2, $id_field, $ordre );
                    }
                }
            }
            else {
                $this->_get_file( $key, $upload_file, $id_field, $ordre );
            }
        }
        Debug::add ('Array imatges guardades', $this->files);
    }

    /**
     * @param $key
     * @param $upload_file
     * @param $id_field
     * @param $ordre
     * @param bool $language El faig servir desde multiupload, ja que hem posa el _FILES diferent
     */
    private function _get_file( $key, $upload_file, $id_field, &$ordre ) {

        global $gl_reload, $gl_message, $gl_errors;

        static $store_name = '';
        static $id_file = '';
        $language = '';

        if ( $this->has_languages ) {
            //$language = substr($key, strlen($this->type)+1,3);
            $language = substr( $key, - 4, 3 );
            // tinc en compte el format: file_id_cat1, file_id_cat2 ...

        }
        Debug::add( 'language', $language );

        if ( strpos( $key, 'replace_' . $this->type ) === 0) return $this->_get_replace_file( $key, $upload_file, $id_field );


		Debug::p( [
					[ 'No replace key', $key ],
					[ 'No replace upload_file', $upload_file ],
					[ 'No replace id_field', $id_field ],
					[ 'No replace type', $this->type ],
				], 'uploadFiles');

        // agafa nomes els que l'input comença igual que el tipus
        // ex: image1, image2, image3 -> agafarà aquests files si es tipus igual a image
        // ex: file1, file2, file3 -> agafarà aquests files si es tipus igual a file
        // quan comencen per Thumbnail els agafa tots ja que provenen de ImageUploader
        if ( ( strpos( $key, $this->type ) === 0 && $this->_can_acept( $upload_file['name'] ) ) || ( strpos( $key, 'Thumbnail' ) === 0 ) ) {
            if ( $upload_file['error'] == 1 ) {
                if ( ! $gl_errors['post_file_max_size'] ) {
                    $gl_message                      = ' ' . $this->messages['post_file_max_size'] . (int) ini_get( 'upload_max_filesize' ) . ' Mb';
                    $gl_errors['post_file_max_size'] = true;
                    $gl_reload                       = true;
                }
            } else {
                // per el java jumploader que canvia el nom de la imatge original
                if ( isset( $_POST['fileName'] ) ) {
                    $upload_file['name'] = $_POST['fileName'];
                }

                $moved = move_uploaded_file( $upload_file['tmp_name'], $this->dir . basename( $upload_file['tmp_name'] ) );
                Debug::add( 'Imatge temporal Move', $this->dir . basename( $upload_file['tmp_name'] ) );
                Debug::add( 'Imatge temporal Move KEy', $key );
                // quan estem a les imatges, el segon thubnail no l'ha de posar a la BBDD, ja que la BBDD nomès guardem un registre per imatge diferent, no es guarden diferents mides d'una mateixa imatge
                if ( ( $moved ) && ! ( strstr( $key, 'Thumbnail2' ) ) ) {
                    if ( ! ( strpos( $key, 'Thumbnail' ) === 0 ) )
                        $gl_reload = true; // a image_manager no faig reload
                    $id_file    = $this->_insert_file( $id_field, $upload_file['name'], $upload_file['size'], $ordre, $language );
                    $store_name = UploadFiles::_get_store_name( $id_file, $upload_file['name'] );
                    rename( $this->dir . basename( $upload_file['tmp_name'] ), $this->dir . $store_name );
                    chmod( $this->dir . $store_name, 0776 );
                    $this->files[ $id_file ]     = array(
                        'id'         => $id_file,
                        'name'       => $upload_file['name'],
                        'store_name' => $store_name
                    );
                    $this->new_files[ $id_file ] = $this->files[ $id_file ];
                    $ordre ++;
                }
                if ( $moved && strstr( $key, 'Thumbnail2' ) ) {
                    // per el segon thumbnail, com que primer hem fet el pirmer thumbnail, $id_file i store_name ja ens serveix, es el mateix
                    $image_ext = strrchr( substr( $store_name, - 5, 5 ), '.' );
                    rename( $this->dir . basename( $upload_file['tmp_name'] ), $this->dir . $id_file . "_medium" . $image_ext );
	                Debug::add( 'Imatge thumbnail2 renombrada',  $this->dir . $id_file . "_medium" . $image_ext );
                    chmod( $this->dir . $store_name, 0776 );
                }
            }
        }
    }

	private function _get_replace_file( $key, $upload_file, $id_field ) {

		global $gl_reload, $gl_message, $gl_errors;

		if ( ! $this->_can_acept( $upload_file['name'] ) ) {
			return false;
		}

		$id_to_replace = str_replace( 'replace_' . $this->type, '', $key );


		if ( $upload_file['error'] == 1 ) {
			if ( ! $gl_errors['post_file_max_size'] ) {
				$gl_message                      = ' ' . $this->messages['post_file_max_size'] . (int) ini_get( 'upload_max_filesize' ) . ' Mb';
				$gl_errors['post_file_max_size'] = true;
				$gl_reload                       = true;
			}
		} else {

			$moved      = move_uploaded_file( $upload_file['tmp_name'], $this->dir . basename( $upload_file['tmp_name'] ) );

			if ($moved) {

				$gl_reload = true;

				$old_name = Db::get_first("
					SELECT name
						FROM " . $this->table . "
						WHERE " . $this->id_col . " = '$id_to_replace'"
				);

				$old_store_name = UploadFiles::_get_store_name( $id_to_replace, $old_name );
				unlink($this->dir . $old_store_name);
				$old_file_ext = strrchr(substr($old_name, -5, 5), '.');

				$store_name = UploadFiles::_get_store_name( $id_to_replace, $upload_file['name'] );
                rename( $this->dir . basename( $upload_file['tmp_name'] ), $this->dir . $store_name );
				$file_ext = strrchr(substr($upload_file['name'], -5, 5), '.');

				$query = '';
				// Si les extensions són diferent, actualitzo a la bbdd
				if ( $file_ext != $old_file_ext ) {
					$new_name = Db::qstr(substr( $old_name, 0, - strlen( $old_file_ext ) ) . $file_ext);
					$query = "
						UPDATE " . $this->table . " 
							SET `name`=$new_name
							WHERE " . $this->id_col . " = '$id_to_replace'";
					Db::execute( $query	);
	                Debug::p( [
	                            [ 'Replace query', $query ],
	                        ], 'uploadFiles');
				}


				$name_original = Db::qstr( $upload_file['name'] );
				$update_sql = "`name_original`=$name_original";

				// Actualitzo name
				if ($this->config['files_override_file_name']) {
					$update_sql .= ", `name`=$name_original";
				}

				// Actualitzo nom original arxiu
				$query = "
						UPDATE " . $this->table . " 
							SET $update_sql
							WHERE " . $this->id_col . " = '$id_to_replace'";
				Db::execute( $query	);




                
                Debug::p( [
                			[ 'Replace tmp', $this->dir . basename( $upload_file['tmp_name'] ) ],
                			[ 'Replace arxiu vell storage', $this->dir . $old_store_name ],
                			[ 'Replace arxiu vell bbdd', $old_name ],
                			[ 'Replace arxiu nou storage', $this->dir . $store_name ],
                			[ 'Replace arxiu nou bbdd', $upload_file['name'] ],
                			[ 'Replace extensió vell', $old_file_ext ],
                			[ 'Replace extensió nou', $file_ext ],
                		], 'uploadFiles');
                

			} else {
				return false;
			}

		}

		Debug::p( [
			[ 'Replace key', $key ],
			[ 'Replace id', $id_to_replace ],
			[ 'Replace upload_file', $upload_file ],
			[ 'Replace id_field', $id_field ],
			[ 'Replace type', $this->type ],
		], 'uploadFiles' );



		return true;

	}

    /**
     * * comprobar si el tipus d'arxiu es permès
     *
     * @access protected
     * @return boolean
     */
    function _can_acept($file)
    {
        global $gl_reload;
        // si no està especificat les extencions que accepta, llavors si val qualsevol tipus d'arxiu menys els especificats
        $accept = !isset($this->fields[$this->type]['accept'])?false:$this->fields[$this->type]['accept'];
        $invert = false;
        if (!$accept)
        {
            $accept = 'exe|com|pif|bat|php|reg|scr|js|vbs|cgi|dll';
            $invert = true;
        }
        if (preg_match("/\.(" . $accept . ")$/", strtolower($file)))
        {
            return $invert?false:true;
        }
        else
        {
            return $invert?true:false;
        }
    }

    /**
     * * si es un registre copiant anterior, nomès em de copiar els arxius marcats
     *
     * @access protected
     * @return void
     */
    function _copy_previous_files()
    {
        global $gl_insert_id;
        // agafar nom d'arxius de la BBDD
        $previous = $this->_get_previous_files($_POST[$this->id_field . '_copy']);
        $post = isset($_POST[$this->type])?$_POST[$this->type]:array();
        // REVISAR array
        if (is_array(current($post))) $post = current($post);
        $ordre = 0;
        foreach ($previous as $p)
        {
            // si està marcat el checkbox copiem
            if (isset($post[$p[$this->id_col]]))
            {
                $store_name = UploadFiles::_get_store_name($p[$this->id_col], $p['name']);
                // insertar registres a la BBDD dels copiats
                $id_file = $this->_insert_file($gl_insert_id, $p['name'], $p['size'], $ordre ++);
                $new_store_name = UploadFiles::_get_store_name($id_file, $p['name']);
                // copiem l'arxiu per al nou missatge
                copy($this->dir . $store_name, $this->dir . $new_store_name);
                // guardar els marcats en l'array de attachments
                $this->files[$id_file] = array ('id' => $id_file , 'name' => $p['name'],
                    'store_name' => $new_store_name);
            }
        }
    }

    /**
     * * La grida get_files
     * * cas; guardar registre, simplement borrar els desmarcats i posar els marcats en l'array d'imatges total
     *
     * @access protected
     * @return void
     */
    function _delete_previous_files()
    {
        // agafar nom d'arxius de la BBDD
        global $gl_reload, $gl_languages;

        if (!isset($_FILES[$this->type])) return;

        $delete_ids = array();
        $previous = $this->_get_previous_files();
        if (!$previous) return;
        Debug::add('IMATGES PREVIS:', $previous);
		
		// si no hi ha post poso un array biuda, així borra tots els arxius
        $post = isset($_POST[$this->type])?$_POST[$this->type]:array(); 
		$post= current($post);
		// busco ids de les imatges marcades ( les que es guarden )
		$save_ids = array();		
		foreach ($post as $po)
        {
			if (($this->has_languages)){
				foreach ($po as $p){
					if (!is_array($p)) $save_ids[]= $p;
				}
			}
			else{
				$save_ids[]= $po;
			}
		}
		Debug::add('SAVE_IDS:', $save_ids);
		
		// de totes les imatges de la BBDD, les que es guarden no les borro i la resta si
		foreach ($previous as $p)
        {
			$id_file = $p[$this->id_col];
            $store_name = UploadFiles::_get_store_name($id_file, $p['name']);
			
			if (in_array($id_file,$save_ids)){			
				$this->files[$id_file] = array ('id' => $id_file ,'name' => $p['name'],
				'store_name' => $store_name);	
			}
			else{			
				$delete_ids[] = $id_file;
                // borrar els desmarcats
                unlink($this->dir . $store_name);
                $gl_reload = true;		
			}
		}
		
		debug::add('IMATGES A GUARDAR $this->files', $this->files);
        // borrar registres a la BBDD dels borrats
        if ($delete_ids) $this->_delete_files($delete_ids);
    }

    function _save_previous_files_data () {

        global $gl_languages;

	    $is_admin = $GLOBALS['gl_is_admin'];

	    // Els camps extres només són per admin, desde públic només poden pujar l'arxiu
	    if (!$is_admin) return;

        $has_title = is_field($this->table, 'title');
        $has_ordre = is_field($this->table, 'ordre');
        $has_public = is_field($this->table, 'public');
    	$has_show_home = !empty ( $this->fields[ $this->type ]['has_show_home'] ) &&  $this->fields[ $this->type ]['has_show_home']=='1';
	    $has_filecat_id = is_table( $this->table . 'cat' );


        /*
        Pot tindre nomes l'edicio del nom
        if ( !$has_title && !$has_ordre && !$public && !$has_show_home  )
            return;
        */

        if ( empty( $this->files ) )
            return;
        if ( ! isset( $_POST[ $this->type ] ) )
            return;

		$post= current($_POST[$this->type]);
	    $post_data = array();

        foreach ($this->files as $id => $val ){

	        if ( $this->has_languages ) {

		        foreach ( $gl_languages['public'] as $language ) {
			        if (isset($post[$language][$id])){
			        	$post_data = $post[$language];
				        break;
			        }
		        }

	        }
	        else {
	        	$post_data = $post;
	        }


	        if ( isset( $post_data['file_name_short'][ $id ] ) && isset( $post_data['file_ext'][ $id ] ) ) {
		        $value = Db::qstr( $post_data['file_name_short'][ $id ] . $post_data['file_ext'][ $id ] );
		        $query = "UPDATE " . $this->table . " SET `name`=$value WHERE   " . $this->id_col . "='" . $id . "'";
		        Debug::p( 'Id file a guardar' . $id, $query );
		        Db::execute( $query );
	        }
	        if ( $has_filecat_id && isset( $post_data['filecat_id'][ $id ] ) ) {
		        $value = Db::qstr( $post_data['filecat_id'][ $id ] );
		        $query = "UPDATE " . $this->table . " SET filecat_id=$value WHERE   " . $this->id_col . "='" . $id . "'";
		        Debug::p( 'Id file a guardar' . $id, $query );
		        Db::execute( $query );
	        }
	        if ( $has_title && isset( $post_data['title'][ $id ] ) ) {
		        $value = Db::qstr( $post_data['title'][ $id ] );
		        $query = "UPDATE " . $this->table . " SET title=$value WHERE   " . $this->id_col . "='" . $id . "'";
		        Debug::p( 'Id file a guardar' . $id, $query );
		        Db::execute( $query );
	        }
	        if ( $has_ordre && isset( $post_data['ordre'][ $id ] ) ) {
		        $value = Db::qstr( $post_data['ordre'][ $id ] );
		        $query = "UPDATE " . $this->table . " SET ordre=$value WHERE   " . $this->id_col . "='" . $id . "'";
		        Debug::p( 'Id file a guardar' . $id, $query );
		        Db::execute( $query );
	        }
	        if ( $has_public ) { // es un checkbox,  quan es zero no està definit
		        $value = ! empty( $post_data['public'][ $id ] ) ? 1 : 0;
		        $query = "UPDATE " . $this->table . " SET public=$value WHERE   " . $this->id_col . "='" . $id . "'";
		        Debug::p( 'Id file a guardar' . $id, $query );
		        Db::execute( $query );
	        }
	        if ( $has_show_home ) {
		        $value = ! empty( $post_data['show_home'][ $id ] ) ? 1 : 0;
		        $query = "UPDATE " . $this->table . " SET show_home=$value WHERE   " . $this->id_col . "='" . $id . "'";
		        Debug::p( 'Id file a guardar' . $id, $query );
		        Db::execute( $query );
	        }
        }
    }

    /**
     * * La grida get_files ->  _delete_previous_files
     * * Borra de la BBDD els registres desmarcats en els checkboxes
     *
     * @access protected
     * @return void
     */

    function _delete_files($delete_ids)
    {
        $query = "DELETE FROM " . $this->table . " WHERE ";
        foreach ($delete_ids as $key)
        {
            $query .= $this->id_col . " = " . Db::qstr($key) . " OR ";
        }
        $query = substr($query, 0, -4);
        Debug::add("Consulta Borra Images", $query);
        Db::execute($query);
    }

    /**
     * * La grida get_files
     * * Inserta a la BBDD
     *
     * @access protected
     * @return id inserció
     */
    function _insert_file($id_field, $name, $size, $ordre, $language)
    {

    	$has_title = is_field($this->table, 'title');

        $title_name = $title_value = '';
		if ($has_title){
			$title_name = ", title";
			$title_value = ", " . Db::qstr($name) . "";
		}


        $lang_name = $lang_value = '';
		if ($language){		
			$lang_name = ", language";
			$lang_value = ", " . Db::qstr($language) . "";
		}
		$query = "
				INSERT INTO " . $this->table . " (
	          " . $this->id_relation_col . ",
	            name,
	            name_original, 
	            size, 
	            ordre 
	            $lang_name 
	            $title_name
	          ) values (
                '" . R::id($id_field,false,false) . "', " .
	            Db::qstr($name) . ", " .
	            Db::qstr($name) . ", " .
	            Db::qstr($size) . ", " .
	            Db::qstr($ordre) .
	            "$lang_value $title_value)";
        Debug::add("Consulta Inserta arxiu", $query);		
		
        Db::execute($query);
		
		$insert_id = Db::insert_id();
		
		// a imatges, inserto registres camps buits pels captions en idiomes
		if ($this->type=='image') $this->_add_images_record_languages($insert_id);
		
        return $insert_id;
    }

    /**
	 *  La crida _insert_file
     *	a imatges, inserto registres camps buits pels captions en idiomes
     * @access public
     * @return void
     */
    function _add_images_record_languages($insert_id)
    {
        global $gl_languages;
		
        $query = '';
		
        foreach ($gl_languages['public'] as $language)
        {
            $query .= "('" . $insert_id . "','','','" . $language . "'),";
        }
		
        $query = "INSERT INTO " . $this->table . "_language 
					(image_id, image_alt, image_title, language) 
					values " . substr($query, 0, -1);
		
        Debug::add("Consulta Inserta imatge idiomes", $query);
        Db::execute($query);		
    }

    /**
     * * La grida get_files
     * * Obté el número més alt d'ordre per posar les noves imatges a continuació
     *
     * @access protected
     * @return ordre
     */
    function _get_next_ordre($id_field)
    {
        $query = "SELECT ordre FROM " . $this->table . " WHERE
	          " . $this->id_relation_col . "=" . $id_field . "
			  ORDER BY ordre DESC limit 1";
        Debug::add("Consulta obté ordre", $query);
        $results = Db::get_rows($query);
        if ($results)
        {
            return $results[0]['ordre'] + 1;
        }
        else
        {
            return 0;
        }
    }

    /**
     * * La grida _delete_previous_files i , _copy_previous_files
     * * Obtindre el llistat d'imatges ja guardades a la BBDD, per poder borrar les desmarcades
     *
     * @access protected
     * @return llistat d'imatges
     */
    function _get_previous_files($id = false)
    {
        if (!$id) $id = $_POST[$this->id_field];
        if (is_array($id)) $id = current($id);
		
		R::id($id,false,false);
		
        $query = "SELECT " . $this->id_col . ", name, size
					FROM " . $this->table . "
					WHERE " . $this->id_relation_col . " = '" . $id . "'";

        Debug::add('Consulta arxius previs:', $query);

        return Db::get_rows($query);
    }

    /**
     * *Obtenim el nom de la imatge en el servidor
     *
     * @access public
     * @return void
     */
    static public function _get_store_name($id_file, $name)
    {
        $file_ext = strrchr(substr($name, -5, 5), '.');
        return $id_file . $file_ext;
    }

    /**
     * * Borrem tots els arxius d'un registre,
     * es crida desprès d'eliminar un registre per eliminar els arxius relacionats
     *
     * @access public
     * @return void
     */
    function delete_all_files($id_field = '')
    {
        $query = 'SELECT ' . $this->id_col . ',name FROM ' . $this->table . ' WHERE ';
        $query_delete = 'DELETE FROM ' . $this->table . " WHERE ";
        if ($id_field)
        {
            // per un sol registre
            $query .= $this->id_relation_col . " = " . R::id($id,false,false);
            $query_delete .= $this->id_relation_col . " = " . R::id($id,false,false);
        }
        else
        {
            // per un llistat
            foreach ($_POST["selected"] as $key)
            {
                $id = R::id($key,false,false);
				$query .= $this->id_relation_col . " = " . $id . " OR ";
                $query_delete .= $this->id_relation_col . " = " . $id . " OR ";
            }
            $query = substr($query, 0, -4);
            $query_delete = substr($query_delete, 0, -4);
        }
        Debug::add("Consulta Borrar Files", $query);
        $rs = Db::get_rows($query);
        // borrem tots els arxius relacionats
        foreach ($rs as $result)
        {
            $store_name = UploadFiles::_get_store_name($result[$this->id_col], $result['name']);
            Debug::add("Files dir Borrar", $this->dir . $store_name);
            unlink ($this->dir . $store_name);
            $this->deleted_files[$result[$this->id_col]] = array ('name' => $result['name'],
                'store_name' => $store_name);
        }
        // borrem tots els registres relacionats
        Debug::add("Consulta borra arxius relacionats", $query_delete);
        Db::execute($query_delete);
    }
    static public function get_file_by_id (&$module, $type, $id) {

	    $id_col =$type.'_id';

	    $tool =  $module->tool;
        $tool_section =  $module->tool_section;
	    $table = $tool . "__" . $tool_section . "_" . $type;

        $has_title = is_field($table, 'title');
        $has_ordre = is_field($table, 'ordre');
        $has_public = is_field($table, 'public');

	    $other_fields = '';
	    if ($has_title)
	    	$other_fields .= $other_fields .= ',title';
	    if ($has_ordre)
	    	$other_fields .= $other_fields .= ',ordre';

	    $query = "SELECT ${type}_id, name $other_fields FROM $table
                    WHERE ${type}_id = '$id'";

	    if ( $has_public && ! $GLOBALS['gl_is_admin'] ) {
		    $query .= " AND public = 1";
	    }


        $rs   = Db::get_row( $query );
	    $title = $has_title ? $rs['title'] : $rs['name'];
	    $ordre = $has_ordre ? $rs['ordre'] : '0';

        $file_ext = strrchr(substr($rs['name'], -5, 5), '.');
        $file_name_short = str_replace($file_ext, '', $rs['name']);
        $file = array(
            $type . "_id" => $rs[$id_col],
            $type . "_title" => $title,
            $type . "_ordre" => $ordre,
            $type . "_name_original" => $rs['name'],
            $type . "_name_short" => $file_name_short,
            $type . "_name" => $rs[$id_col] . $file_ext,
            $type . "_src_real" => '/' . CLIENT_DIR . '/' . $tool . '/' . $tool_section . '/' . $type . 's/' . $rs[$id_col] . $file_ext,
            $type . "_src" =>  UploadFiles::get_download_link($tool, $tool_section , $type, $rs[$id_col],$file_ext),
            $type . "_ext" => substr($file_ext,1));

        return $file;
    }
	
	// per cridar desde instancia
    public function get_record_files_i($id)
    {
		return UploadFiles::get_record_files($id, $this->type, $this->tool, $this->tool_section, $has_languages = false, $this);
	}
    /**
     * * Retorna els noms dels arxius en un array llest per isertar en un loop en el template
     *
     * @access public
     * @return array() Retorna els noms dels arxius en un array
	// Quan cridem d'una altra eina: UploadFiles::get_record_files($id, $id_col, $tool, $tool_section)
     */
    static public function get_record_files($id, $type, $tool, $tool_section, $has_languages = false, $module = false, $condition = '' )
    {				
		$table = $module?$module->table:($tool . '__' . $tool_section . '_' . $type);		
		$id_col = $module?$module->id_col:$type.'_id';		
		$id_relation_col = $module?$module->id_relation_col:($tool_section . '_id');
        if ($condition) $condition = ' AND ' . $condition;

		$file_names = array();

        $has_title = is_field($table, 'title');
        $has_ordre = is_field($table, 'ordre');
        $has_public = is_field($table, 'public');
        $has_filecat_id = is_table( $table . 'cat' );
        $has_category_zero = !empty ($module->config['filecat_has_category_zero']);

	    $other_fields = '';
	    $order_clause = '';
	    $id_get = R::id( $id, false, false );

	    if ( $has_title ) {
		    $other_fields .= $other_fields .= ',title';
	    }
	    if ( $has_ordre ) {
		    $other_fields .= $other_fields .= ',ordre';
	    }
	    if ( $has_public ) {
		    $other_fields .= $other_fields .= ',public';
	    }
	    if ( $has_filecat_id ) {
		    $other_fields .= ',filecat_id';
	    }

	    if ( $has_filecat_id ) {
		    // inicialitzo arrays de categoryes per que no doni undefined
		    $categorys = Db::get_rows( "SELECT ${type}cat_id, filecat FROM {$table}cat WHERE language = '" . LANGUAGE . "' ORDER BY filecat ASC" );
		    foreach ( $categorys as $rs ) {
			    $cat_name                  = "${type}s_cat_" . $rs[ $type . 'cat_id' ];
			    $file_names[ $cat_name ] = array();
		    }

		    if ( $has_category_zero ) {
			    $cat_name                  = "${type}s_cat_0";
			    $file_names[ $cat_name ] = array();
			    $categorys = array_merge( [ 0 => ["${type}cat_id" => '0', 'filecat' => ''] ], $categorys );
		    }

		    $file_names[$type . "s_categorys"] = $categorys;
		    $file_names[$type . "s"] = array();
	    }
		
		$has_languages = $module?$module->has_languages:$has_languages; // agafo type així puc deixar el paràmetre has_languages opcional

		$conta = 0;

	    if ($has_public && !$GLOBALS['gl_is_admin'])
	    	 $condition .= " AND public = 1";
	    if ($has_ordre) {

			if (empty ($module->config['file_order_desc'])){
				$order_clause = "ordre, ";
			}
			else {
				$order_clause = "ordre DESC, ";
			}
	    }

	    $order_clause .= "name, $id_col";
		
		// si tinc idioma, primer busco l'arxiu de l'idioma concret
		if ($has_languages){		
			$query = "SELECT $id_col, name, language $other_fields
                        FROM $table
                        WHERE $id_relation_col  = $id_get
                        $condition
                        AND language = '" . $GLOBALS['gl_language'] . "'
                        ORDER BY $order_clause";
		}else
		{
            $query = "SELECT  $id_col , name $other_fields
					FROM  $table
                    WHERE $id_relation_col = $id_get
                    $condition
                    ORDER BY $order_clause";
		}

		$results = Db::get_rows($query);
		
		Debug::add("Consulta get names", $query);
        if ($results)
        {
            foreach ($results as $rs)
            {
                $title = $has_title ? $rs['title'] : $rs['name'];
	            $ordre = $has_ordre ? $rs['ordre'] : '0';
	            $public = $has_public ? $rs['public'] : '0';
                $file_ext = strrchr(substr($rs['name'], -5, 5), '.');
                $file_name_short = str_replace($file_ext, '', $rs['name']);

				$file_rs = array(
					$type . "_id" => $rs[$id_col],
                    $type . "_title" => $title,
                    $type . "_ordre" => $ordre,
					$type . "_name_original" => $rs['name'],
					$type . "_name_short" => $file_name_short,
                	$type . "_name" => $rs[$id_col] . $file_ext,
					$type . "_src_real" => '/' . CLIENT_DIR . '/' . $tool . '/' . $tool_section . '/' . $type . 's/' . $rs[$id_col] . $file_ext,
					$type . "_src" =>  UploadFiles::get_download_link($tool, $tool_section , $type, $rs[$id_col],$file_ext),
					$type . "_ext" => substr($file_ext,1),
					$type . "_public" => $public,
					$type . "_conta" => $conta,
					$type . "_loop_count" => count($results));

	            // faig array especific per categoria
	            if ( $has_filecat_id ) {

					$file_names[$type . "s"][] = $file_rs;

		            $cat_name                  = "${type}s_cat_" . $rs[ $type . 'cat_id' ];
		            $file_names[ $cat_name ][] = $file_rs;
		            $count_cat                 = count( $file_names[ $cat_name ] ) - 1;

		            $file_names[ $cat_name ][ $count_cat ][ $type . "_conta" ] = $count_cat;
	            }
	            // es retorna un array amb totes les imatges
	            else {
					$file_names[] = $file_rs;
	            }

	            $conta ++;
            }
        }
        return $file_names;
    }
	
	// per cridar desde instancia
    public function get_record_images_i($id)
    {
		return UploadFiles::get_record_images($id, true, $this->tool, $this->tool_section, '', $this);
	}
    // NOMES SI ES TRACTA D'IMATGES
    // Retorna els noms i scr de la imatge principal per un element d'un llistat
    // Retorna tots els noms i totes les src de la imatge tant per un llistat com per un form o record
    // Si $get_all = false nomes busca l'imatge principal
	// Definint tool i tool_section, puc cridar la funció desde un altre modul ( ej. a product__order necessito mostrar la imatge del producte, però product__order no te imatges, les imatges son de product__product
	// Quan cridem d'una altra eina: UploadFiles::get_record_images($id, true, $tool, $tool_section)
    static public function get_record_images($id, $get_all, $tool, $tool_section, $prefix='', $module = false )
    {
        $image = '';
        $image_medium = '';
        $image_details = '';
        $image_thumb = '';
        $image_admin_thumb = '';
        $images_array = array();
        $image_rs = array();
        $conta = 0;
		$table = $module?$module->table:($tool . '__' . $tool_section . '_image');
		$id_relation_col = $module?$module->id_relation_col:($tool_section . '_id');
		$has_categories = false;
		
        
		// si no tinc $module no cal comprobar si module->has_images ja que es crida UploadFiles::get_record_images
		if (!$module || $module->has_images) 
        {
			$imagecat = '';
			if (is_table($table.'cat')){
				$imagecat = ", imagecat_id";
				$has_categories = true;
				
				// inicialitzo totes les arrays de categories per que no doni undefines
				$categories = Db::get_rows("SELECT imagecat_id FROM " . $table . "cat");
				foreach ($categories as $rs){
					$cat_name = $prefix.'images_cat_' . $rs['imagecat_id'];
					$images_array[$cat_name] = array();
				}
			}
            $query = "SELECT " . $table . ".image_id as image_id,  name, main_image, image_alt, image_title" . $imagecat . "
				FROM " . $table . ", " . $table . "_language				
				WHERE " . $table . ".image_id = " . $table . "_language.image_id
				AND language = '" . LANGUAGE . "'
				AND " . $id_relation_col . "=" . R::id($id,false,false);

            if (!$get_all)
            {
                $query .= "
				AND main_image='1'";
            }
			
			
			

            $query .= " ORDER BY ordre";

            // Debug::add("Consulta get images ", $query);

            $results = Db::get_rows($query);
            if ($results)
            {
                foreach ($results as $rs)
                {
                    $image_id = $rs['image_id'];					
                    $name = strrchr(substr($rs['name'], -5, 5), '.');					

                    $image_original = $rs['name'];
                    $image = $image_id . $name;
                    $image_medium = $image_id . '_medium' . $name;
                    $image_details = $image_id . '_details' . $name;
                    $image_thumb = $image_id . '_thumb' . $name;
                    $image_admin_thumb = $image_id . '_admin_thumb' . $name;

					$path_base = '/' . CLIENT_DIR . '/' .  $tool . '/' . $tool_section . '/images/';

                    $image_rs = array(
						$prefix."image_id" => $image_id,
						$prefix."image_name" => $image,
                        $prefix."image_src" => $path_base . $image,
                        $prefix."image_name_original" => $image_original,
                        $prefix."image_src_original" => $path_base . 'original/' . $image_original,
                        $prefix."image_name_medium" => $image_medium,
                        $prefix."image_src_medium" => $path_base . $image_medium,
                        $prefix."image_name_details" => $image_details,
                        $prefix."image_src_details" => $path_base . $image_details,
                        $prefix."image_name_thumb" => $image_thumb,
                        $prefix."image_src_thumb" => $path_base . $image_thumb,
                        $prefix."image_name_admin_thumb" => $image_admin_thumb,
                        $prefix."image_src_admin_thumb" => $path_base . $image_admin_thumb,
                        $prefix."image_conta" => $conta,
                        $prefix."image_is_main" => $rs['main_image'],
                        $prefix."image_alt" => $rs['image_alt'],
                        $prefix."image_title" => $rs['image_title'],
                        $prefix."image_alt_value" => $rs['image_alt'],
                        $prefix."image_title_value" => $rs['image_title'],
                        $prefix."image_imagecat_id" => isset($rs['imagecat_id'])?$rs['imagecat_id']:false
                        );

                    if ($rs['main_image'] == '1')
                    {
                        // poso la imatge principal directament com a variables, fora del loop d'imatges
						$images_array += $image_rs;
                    }
                    // retorna images->0->image_name, images->0->image_src etc.... per poder fer un looop i mostrar totes les imatges d'un registre
                    if ($get_all) {
						$images_array[$prefix.'images'][] = $image_rs;
						// faig array especific per categoria
						if ($has_categories) {
							$cat_name = $prefix.'images_cat_' . $rs['imagecat_id'];
							$images_array[$cat_name][] = $image_rs;
							$count_cat = count($images_array[$cat_name])-1;
							$images_array[$cat_name][$count_cat][$prefix."image_conta"] = $count_cat;
						}
					}
	                $conta++;
                }
            }
            else
            {
                $images_array += array(
					$prefix."image_name" => '',
                    $prefix."image_src" => '',
                    $prefix."image_name_original" => '',
                    $prefix."image_src_original" => '',
                    $prefix."image_name_medium" => '',
                    $prefix."image_src_medium" => '',
                    $prefix."image_name_details" => '',
                    $prefix."image_src_details" => '',
                    $prefix."image_name_thumb" => '',
                    $prefix."image_src_thumb" => '',
                    $prefix."image_name_admin_thumb" => '',
                    $prefix."image_src_admin_thumb" => '',
                    $prefix."image_conta" => $conta
                    );
                $images_array[$prefix.'images'] = array();
            }

			$images_array[$prefix.'images_dir'] = CLIENT_DIR . '/' . $tool . '/' . $tool_section . '/images/';
			
            return $images_array;
        }
    }
    // Funcio per inicialitzar tots els objectes $gl_upload
    // es crida UploadFiles::initialize($module)
    static public function initialize(&$module)
    {

    	if ($module && $module->is_v3) {
    		$tool = $module->tool;
			$tool_section = $module->tool_section;
    		$table = $module->table;
			$uploads =  &$module->uploads;
    		$id_field = $module->id_field;
    		$upload_settings = $module->upload_settings;
    		$has_images = &$module->has_images;
    	}
		else {
			global $gl_upload, $gl_tool, $gl_tool_section, $gl_db_classes_id_field, $gl_db_classes_uploads, $gl_has_images;
			$tool = $gl_tool;
			$tool_section = $gl_tool_section;
    		$table = $tool . '__' . $tool_section;
			$uploads = &$gl_upload;
			$id_field = $gl_db_classes_id_field;
			$upload_settings = $gl_db_classes_uploads;
			$has_images = &$gl_has_images;
		}
        $uploads = array();
        // /
        // per imatges es detecta automàticament
        // /
        if (is_table($table . '_image'))
		{
			// si està definit el get descarrego arxiu
			// i aqui s'acaba tot
			if (!empty($_GET['image'])){
				UploadFiles::download_file ($module, 'image');
			}
				
            // v3 cambiar aquesta constant per una variable
			if (!defined('IMAGES_DIR')) define('IMAGES_DIR', CLIENT_PATH . $tool . '/' . $tool_section . '/images/');
            $has_images = true;
            $uploads['image'] = new UploadFiles($module, $table . '_image', $id_field, IMAGES_DIR, 'image');
        }
        // /
        // altres uploads, es defineix al config
        // /
		debug::add('Uploads',$upload_settings);
        if ($upload_settings)
        {
            foreach($upload_settings as $key => $value)
            {
				// si està definit el get descarrego arxiu
				// i aquí s'acaba tot ( 'file' "file" )
				if (!empty($_GET[$key])){
					UploadFiles::download_file ($module, $key);
				}
				
				
                // comento isset per que de moment no fa falta que els noms de BD no segueixin les normes
                // isset($value['id_field'])?$value['relation_col']:$gl_db_classes_id_field;
                $table = $tool . '__' . $tool_section . '_' . $key;
                // isset($value['table'])?$value['table']:$gl_tool . '__' . $gl_tool_section . '_' . $key;
                $dir = CLIENT_PATH . $tool . '/' . $tool_section . '/' . $key . 's/';
                if (!defined(strtoupper($key) . 'S_DIR')) define(strtoupper($key) . 'S_DIR', $dir);
                $uploads[$key] = new UploadFiles($module, $table, $id_field, $dir, $key);
				
            }
        }
		debug::add('Uploads',array_keys($uploads));
    }
	// construeixi enllaç a l'arxiu
	// ej: http://w.blunik/spa/news/new/file/30/
	// L'últim paràmetre a quedat obsolet, ja no fa falta
	//
	static public function get_download_link ($tool, $tool_section, $key, $id, $file_ext = '') {
		

		
		//////////////////////////////////////
		//
		//
		//
		if ($GLOBALS['gl_is_admin']){


		    // igual a admin, així descarreguem amb el nom d'arxiu real
            // el problema a admin es saber quin idioma està disponible a public
            return '/admin/?' .
                   'tool=' . $tool . '&amp;' .
                   'tool_section=' . $tool_section . '&amp;' .
                   'menu_id=' . $GLOBALS['gl_menu_id'] . '&amp;' .
                   $key . '=' . $id . '';

            // a admin retorno el src_real
			//return '/' . CLIENT_DIR . '/' . $tool . '/' . $tool_section . '/' . $key . 's/' . $id . $file_ext;
		}
		else
			{
			return '/' . LANGUAGE . '/' . $tool . "/" . $tool_section . '/' . $key . '/' . $id . '/';
		}
	}
	static public function download_file (&$module, $key) {

        $force = R::number( 'force' );

        if ( $key ) {
            $id = R::id( $key );
        } else {
            $id = R::id( 'file_id' );
        }
        $rs   = Db::get_row( "SELECT " . $key . "_id, name FROM " . $module->tool . "__" . $module->tool_section . "_" . $key . "
						WHERE " . $key . "_id = '" . $id . "'" );
        $name = $rs['name'];
        $file = CLIENT_PATH . '/' . $module->tool . '/' . $module->tool_section . '/' . $key . 's/' . UploadFiles::_get_store_name( $rs[ $key . '_id' ], $rs['name'] );

        UploadFiles::serve_download( $file, $name );
	}
	static public function serve_download ($file,$name, $force = false, $file_content=false, $type = false) {	
				
		if (R::number('force')) $force = R::number('force');
		
		if (file_exists($file) && !is_dir($file) || $file_content) {
			
			include_once (DOCUMENT_ROOT . 'common/includes/browser/browser.php');
			$browser = new Browser();
			$b_name = $browser->getBrowser();
			$b_version = $browser->getVersion();
			$ext = substr( $file, - 4 );


			if ($b_name == Browser::BROWSER_IE && $b_version <= 8){
				$filename = 'filename=' . urlencode(str_replace(' ', '_' ,$name));
			}
			elseif ($b_name == Browser::BROWSER_SAFARI){
				$filename = 'filename=' . str_replace(' ', '_' ,$name);	
			}
			else{
				$filename = 'filename=' . str_replace(' ', '_' ,$name);	
				//$filename = 'filename*=UTF-8\'\'' . urlencode(str_replace(' ', '_' ,$name));			
			}
			
			
			// Download PDF as file
			if (ob_get_contents()) {
				Debug::p('Ja s\'han enviat les capceleres, no es pot baixar l\'arxiu');
			}	
			
			if (!$type){

				// els arxius .docx en realitat són tipus zip, però assumeixo que tots seràn un arxiu word
				// no crec que cap client utilitzi .docx per un arxiu zip

				if ($ext == 'docx') {
					$type = 'application/vnd.openxmlformats-officedocument.wordprocessingml.document';
				}
				else
				{
					$finfo = finfo_open( FILEINFO_MIME_TYPE );
					$type  = finfo_file( $finfo, $file );
					finfo_close( $finfo );
				}
			}
			$length = $file_content?strlen($file_content):filesize($file);
		
			header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');

			// Explorer dona error al obrir document inline
			if ($force == '1' || ($b_name == Browser::BROWSER_IE && $ext != '.pdf' )) {
				header('Content-Description: File Transfer');
				header('Content-Type: application/force-download');
				header('Content-Disposition: attachment; '. $filename);
			}
			else{				
				header('Content-Disposition: inline; '. $filename);
			}
			header('Content-Type: ' . $type);
			header('Content-Transfer-Encoding: binary');
			header('Expires: 0');
			header('Cache-Control: must-revalidate');
			header('Pragma: public');
			header('Content-Length: '.$length);			
			
			ob_clean();
			flush();
			if ($file_content) echo ($file_content);
			else readfile($file);
			die();
		}
	}
    // Funcio per obtindre records de tots els objectes $gl_upload d'un registre
    // es crida UploadFiles::gl_upload_get_files()
    static public function gl_upload_get_record_files(&$module, $id)
    {
		if ($module) {
			$upload =  $module->uploads;
			$upload_settings = $module->upload_settings;
		}
		else {
			global $gl_upload, $gl_db_classes_uploads;
			$upload = $gl_upload;
			$upload_settings = $gl_db_classes_uploads;
		}



        $r = array();
        // les imatges les agafa ->get_record_images
        // bucle per $gl_db_classes_uploads (no agafa imatges) si fòs per $gl_upload agafaria també les imatges
        if ($upload_settings)
        {
            foreach($upload_settings as $key => $value)
            {
                $ret = $upload[$key]->get_record_files_i($id);

	            if ( isset ($ret [ $key . 's' ] ) ) {
		            $r += $ret;
	            }
	            else {
		            $r[ $key . 's' ] = $ret;
	            }
            }
        }
        return $r;
    }
    // Funcio per obtindre arxius de tots els objectes $gl_upload
    // es crida UploadFiles::gl_upload_get_files()
    static public function gl_upload_get_files(&$module=false)
    {
		if ($module) {
			$upload =  $module->uploads;
			$upload_settings = $module->upload_settings;
		}
		else {
			global $gl_upload, $gl_db_classes_uploads;
			$upload = $gl_upload;
			$upload_settings = isset($gl_db_classes_uploads)?$gl_db_classes_uploads:'';
		}
        // les imatges les recull ja la clase manage_images de manera diferent ja que no estan en el mateix formulari
        // bucle per $gl_db_classes_uploads (no agafa imatges) si fòs per $gl_upload agafaria també les imatges
        if ($upload_settings)
        {
            foreach($upload_settings as $key => $value)
            {
                $upload[$key]->get_files();
            }
        }
    }
    // Funcio per borrar tots els arxius de tots els objectes $gl_upload
    // es crida UploadFiles::gl_upload_delete_all_files()
    static public function gl_upload_delete_all_files(&$module=false)
    {
		if ($module) {
			$upload =  $module->uploads;
			$upload_settings = $module->upload_settings;
		}
		else {
			global $gl_upload, $gl_db_classes_uploads;
			$upload = $gl_upload;
			$upload_settings = $gl_db_classes_uploads;
		}
        // també excloc les imatges, queda millor a save_rows per llegir el codi ja que també s'han de borrar tota la serie de imatges i thumbnails d'una mateixa imatge
        // bucle per $gl_db_classes_uploads o per $gl_upload (aquest agafa també les imatges)
        if (!empty($upload_settings))
        {
            foreach($upload_settings as $key => $value)
            {
                $upload[$key]->delete_all_files();
            }
        }
    }
}