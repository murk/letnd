<?php
include ('inmo_search_functions.php');
function save_rows()
{	
	$save_rows = new SaveRows;
	inmo_check_user_permission(implode($save_rows->id,','));
    $save_rows->save();

    export_to_portal($save_rows->id, $save_rows->action);
}

function list_records()
{
	set_custom_config();
	global $gl_action, $gl_caption;
	if ($gl_action == 'list_records_municipis') get_municipis_jscript();
	show_private_button();
	$gl_caption['c_bathroom_0']=$gl_caption['c_wc_0']='-';
    $listing = new ListRecords;
	
	if ($_SESSION['edit_only_descriptions']) {
		$listing->set_field('status','list_admin','text');
		$listing->set_options(1, 0, 0, 0, 0, 1, 1, 0); // set_options ($options_bar = 1, $options_checkboxes = 1, $save_button = 1, $select_button = 1, $delete_button = 1, $print_button = 1, $edit_buttons = 1, $image_buttons = 1)
	}
	else{
		if ($gl_action== 'list_records_book_search'){
			$listing->add_button ('book_property_button', 'menu_id=152&action=show_form_new&process=booking', 'boto2', 'before');
		}
		elseif ($GLOBALS['gl_config']['is_custumer_on']) {
			$listing->add_button ('edit_custumer_button', 'action=list_records_custumer', 'boto1_private', 'after');
		}
	}
	list_records_property($listing);
}


function show_form()
{
	set_custom_config();
	
    global $gl_action, $tpl, $gl_news, $gl_caption, $gl_config;	
	
    if ($gl_action == 'show_form_map'){
    	global $gl_content, $gl_caption;
		
		$property_id = R::id('property_id');
		// sino puc estar editant aquesta propietat, em fa fora directe
		inmo_check_user_permission($property_id, 'inmo__property', 'property_id');
		
    	$tpl->set_file('inmo/property_map.tpl');
    	$tpl->set_vars($gl_caption);

    	$rs = Db::get_row('
			SELECT ref, provincia_id, municipi_id, adress, numstreet, block, flat, door, property_private, property_id, latitude, longitude, center_latitude, center_longitude, zoom, tipus, show_map, show_map_point 
			FROM inmo__property 
			WHERE property_id = %s', $property_id);

    	$provincia = Db::get_first('SELECT provincia FROM inmo__provincia WHERE provincia_id = %s', $rs['provincia_id']);
    	$municipi = Db::get_first('SELECT municipi FROM inmo__municipi WHERE municipi_id = %s', $rs['municipi_id']);

   		$rs['show_tipus_temp'] = $rs['tipus'] == 'temp'?'block':'none';
    	$rs['is_default_point'] = ($rs['latitude']!=0)?0:1;
    	$rs['latitude']=$rs['latitude']!=0?$rs['latitude']:INMO_GOOGLE_CENTER_LATITUDE;
    	$rs['longitude']=$rs['longitude']!=0?$rs['longitude']:INMO_GOOGLE_CENTER_LONGITUDE;
    	$rs['center_latitude']=$rs['center_latitude']!=0?$rs['center_latitude']:INMO_GOOGLE_CENTER_LATITUDE;
    	$rs['center_longitude']=$rs['center_longitude']!=0?$rs['center_longitude']:INMO_GOOGLE_CENTER_LONGITUDE;
    	$rs['zoom']=$rs['zoom']?$rs['zoom']:INMO_GOOGLE_ZOOM;
    	$rs['menu_id'] =  $_GET['menu_id'];		
		$rs['tool'] =  'inmo';		
		$rs['tool_section'] =  'property';	
		$rs['id'] =  $rs['property_id'];		
		$rs['id_field'] =  'property_id';
    	$rs['municipi'] =  addSlashes($municipi . ' (' . strtoupper($provincia) . ')');
    	$rs['show_map_point'] =  '<input type="checkbox" id="show_map_point" name="show_map_point" value="1" '.($rs['show_map_point']?' checked':'') . ($rs['show_map']?'':' disabled').' />';
    	$rs['show_map'] =  '<input type="checkbox" id="show_map" name="show_map" value="1" '.($rs['show_map']?' checked':'').' />';

   	 	$adress = '';
		if ($rs['adress']) {
			$adress .= $rs['adress'];
			if ($rs['numstreet']) $adress .= ', ' . $rs['numstreet'];
			/*
			if ($rs['block']) $adress .= ' ' . $rs['block'];
			if ($rs['flat']) {
				$adress .= ' ' . $rs['flat'];
				if ($rs['door']) $adress .= '-' . $rs['door'];
			}
			*/
		}
    	$rs['adress'] =  addSlashes($adress);

    	$tpl->set_vars($rs);
    	$tpl->set_vars($gl_config);
    	$gl_content = $tpl->process();
    	$gl_news = $gl_caption['c_click_map'];
    }
    else{
    	show_form_property();
    }
    if ($gl_action != 'show_form_new' && (isset($_GET['show_search'])?$_GET['show_search']:true))
    {
        show_search_form();
    }
}

function write_record()
{
	set_custom_config();
    global $gl_action, $gl_message, $gl_messages;
	if ($gl_action=='save_record_map'){
    	extract($_GET);
		
		
    	if ($property_id){
	    	Db::execute ("UPDATE `inmo__property` SET
				`latitude` = '$latitude',
				`longitude` = '$longitude',
				`center_latitude` = '$center_latitude',
				`center_longitude` = '$center_longitude',
				`zoom` = '$zoom',
				`show_map` = '$show_map',
				`show_map_point` = '$show_map_point'
				WHERE `property_id` =$property_id
				LIMIT 1") ;
			$gl_message = $gl_messages['point_saved'];

            export_to_portal($property_id, $gl_action);
		}
    }
    else
    {
		write_record_property();
	}
}

function manage_images()
{
    global $gl_action, $tpl;
	show_private_button();
	// mostrar tabs per apartat contract
    $tpl->set_var('show_tabs', isset($_GET['show_tabs'])?$_GET['show_tabs']:true);

	$tipus = Db::get_first('SELECT tipus FROM inmo__property WHERE property_id = ' . $_GET['property_id']);
	$show_tipus_temp = $tipus == 'temp'?'block':'none';
    $tpl->set_var('show_tipus_temp',$show_tipus_temp);

	if (strstr($gl_action, "list_records_images")) set_showwindow();
	$image_manager = new ImageManager;
	
	
	inmo_check_user_permission($_GET['property_id']);
	
	$image_manager->set_options (1, 1, 1, 0);
    $image_manager->execute();

    if ((strstr($gl_action, "list_records_images")) && (isset($_GET['show_search'])?$_GET['show_search']:true))
    {
        show_search_form();
    }
}

?>