<?
/**
 * HABITACLIA
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) Juny 2019
 * @version 1
 * @access public
 */

/*
TODO-i Canviar floor_Space_build per floor_space_util a inmo i a les exportacions, aquí ja ho deixo bé

INFO Exportació portals

letnd.inmo__municipi -> es guarden les relacions de municipis

INFO FEATURES

	Obligatori preu i codi postal

PREGUNTAR A CEIGRUP

	propietario cal posar-ho, o ja surt l'agencia per defecte?

CANVIS EN BBDD


LETND NO TE

	Tipus transfer
	<m2salon>	Metros cuadrados de salón


HABITACLIA NO TE

INFO - Consultes Sql


Veure tipus:
	SELECT export__habitaclia_category_to_subtipos.subtipo_id AS subtipo_id, category, category_id,
	(
		SELECT DES_TIP
		FROM export__habitaclia_tipos
		WHERE export__habitaclia_tipos.TIP_INM = export__habitaclia_subtipos.TIP_INM
	) as DES_TIP1,
	DES_TIP, TIP_INM, TIP_INM2
	FROM export__habitaclia_category_to_subtipos
	LEFT OUTER JOIN export__habitaclia_subtipos USING (subtipo_id);

Veure tipus de habitaclia:
	SELECT
		(
			SELECT DES_TIP
			FROM export__habitaclia_tipos
			WHERE export__habitaclia_tipos.TIP_INM = export__habitaclia_subtipos.TIP_INM
		) as DES_TIP1,
		DES_TIP, TIP_INM, TIP_INM2
	FROM export__habitaclia_subtipos
	ORDER BY DES_TIP1 ASC, DES_TIP ASC;


*/

class AdmintotalHabitaclia extends Module {

	public $exporter = 'habitaclia';
	public $section = 'property';
	public $is_global_export = false;

	public $config_rs = [];
	public $export_rs = [];
	public $property_rs = [];
	public $agencia_rs = [];
	public $is_cli = false;

	public $response_errors = [];

	// Per poder exportar nomes una agencia i un inmoble, en temps real
	public $agencia_id = 0;
	public $property_ids = [];
	public $portal_vars = [];
	public $client_images_url;

	var $habitaclia_tipus = [
		'sell'       => 'venta_01',
		'rent'       => 'alquiler_01',
		'temp'       => 'alquiler_temporada_01',
		'moblat'     => 'alquiler_01',
		'selloption' => 'alquiler_opcion_venta_01',
		'transfer'         => 'traspaso_01', // No hi es a letnd
	];

	var $ceigrup_anunci_languages = [
		'spa' => 'es',
		'cat' => 'ca',
		'eng' => 'en',
		'fra' => 'fr',
		'dut' => 'nl',
	];


	function __construct() {

		include_once( DOCUMENT_ROOT . 'admin/modules/admintotal/export_common.php' );
		parent::__construct();
	}

	public function start() {
		$this->_start();
		html_end();
	}

	public function _start( $is_global_export = true ) {

		$agencia_id = R::get_opt( 'agencia_id' );
		if ( $agencia_id ) {
			$this->agencia_id = $agencia_id;
		}

		$this->is_global_export = $is_global_export;

		ExportCommon::p( "INICI: " . date( "d-m-Y H:i" ), 1 );
		set_time_limit( 0 );        //ini_set('implicit_flush', true);		//error_reporting(E_WARNING);
		echo '<link href="/admin/themes/inmotools/styles/export.css" rel="stylesheet" type="text/css">';

		Db::connect_mother();

		$agencias = ExportCommon::get_agencias( 'inmo_habitaclia', $this->agencia_id );

		if ( ! $agencias ) {
			ExportCommon::end( $this );

			return;
		}

		$this->export_properties( $agencias );

		if ( $is_global_export ) {
			ExportCommon::show_end();
			ExportCommon::end( $this );
		}

	}

	private function get_agencia_config() {

		ExportCommon::get_agencia_config( $this->agencia_rs, 'habitaclia' );

		$agencia = &$this->agencia_rs;

		$agencia['url'] = "http://" . $this->agencia_rs['domain'];

	}

	function get_config_results() {


		$query = "SELECT habitaclia_field, letnd_table, letnd_field, `function`, `condition`
							FROM export__habitaclia
							WHERE habitaclia_table <>  ''";

		return Db::get_rows( $query );

	}


	function export_properties( $agencias ) {

		foreach ( $agencias as $rs ) {

			$xml = $this->get_XMLWRITER( $rs['inmo_habitaclia_user'], 'producto' );

			if (!$xml) continue;

			$this->agencia_rs =  &$rs;

			$this->get_agencia_config();
			$condition = $this->get_agencia_condition();

			$this->client_images_url = $rs['url'] . '/' . $rs['client_dir'] . '/inmo/property/images/';

			$this->build_agencia_xml( $xml, $condition );

			$xml->endElement(); // producto
			$xml->flush();
			unset( $xml );

		}
	}

	private function get_agencia_condition() {


		// TODO-i Les condicions s'han de revisar aqui i a portals
		// category_id, certificat obligatori
		$condition = "(status = 'onsale' || status = 'reserved')
				AND bin = 0
				AND country_id = '1'
				AND ref <> ''
				AND category_id <> 0";

		if ( ! $this->agencia_rs['habitaclia_export_all'] ) {
			$condition .= "
				AND property_id IN (
					SELECT property_id
					FROM `" . $this->agencia_rs['dbname'] . "`.inmo__property_to_portal
					WHERE habitaclia = 1
				)
			";
		}

		ExportCommon::p( "Consulta immobles: " . $this->agencia_rs['comercialname'], 1 );

		return $condition;

	}

	private function build_agencia_xml( &$xml, $condition ) {

		ExportCommon::p( "Extracció d'inmobles: " . $this->agencia_rs['comercialname'], 1 );
		ExportCommon::p( "Exportar tots: " . ( $this->agencia_rs['habitaclia_export_all'] ? 'Sí' : 'No' ), 3 );

		$query   = "
			SELECT * FROM `" . $this->agencia_rs['dbname'] . "`.inmo__property
			WHERE " . $condition;
		$results = Db::get_rows( $query );

		ExportCommon::p( "Total immobles a exportar: " . count( $results ), 3 );
		$total_property_exported = 0;


		$config_results = $this->get_config_results();

		foreach ( $results as $key => $rs ) {

			$this->property_rs = &$rs;
			$this->export_rs   = [];

			$has_text = $this->get_anunci();

			if ( ! $has_text ) continue;
			$total_property_exported ++;

			$this->get_images();


			// Altres Valors
			$this->get_contact_details();

			foreach ( $config_results as $config_rs ) {

				$this->config_rs = &$config_rs;
				$letnd_val   = $rs[ $config_rs['letnd_field'] ];

				// Si val es 0 tampoc exporta res
				if ( $config_rs['function'] ) {
					$val = $this->{$config_rs['function']}( $letnd_val );
					if ( $val ) {
						$this->export_rs[ $config_rs['habitaclia_field'] ] = $val;
					}
				}
				else {
					if ( $letnd_val ) {
						$this->export_rs[ $config_rs['habitaclia_field'] ] = $letnd_val;
					}
				}
			}
			$this->config_rs = [];

			$this->write_xml( $xml );

			ExportCommon::p( $rs['ref'] . " , id: " . $rs['property_id'], 2 );
		}


		ExportCommon::p( "Total immobles exportats: " . $total_property_exported, 3 );

		$this->property_rs = [];
		$this->export_rs   = [];

	}


	function write_xml( &$xml ) {

		$xml->startElement( 'inmueble' );

		foreach ( $this->export_rs as $k => $v ) {

			if ( $k == 'photos' ) {
				$xml->startElement( 'photos' );
				foreach ( $v as $im ) {
					$xml->startElement( 'photo' );
					foreach ( $im as $k => $v ) {
						$xml->writeElement( $k, $v );
					}
					$xml->endElement();
				}
				$xml->endElement();
			}
			// Pel map
			elseif (is_array($v)){
				$xml->startElement( $k );
				foreach ( $v as $k2 => $v2 ) {
					$xml->writeElement( $k2, $v2 );
				}
				$xml->endElement();
			}
			else {
				// Write element fa htmlentities i cambia salts de linea per &#13;
				// text també fa hmlspecialchars ???
				// $xml->writeElement( $k, $v ? $v : null );
				/*$xml->startElement( $k );
				if ($v)
					$xml->text( $v );
				$xml->endElement();*/

				$xml->startElement( $k );
				if ( $v ) {
					// $v = htmlspecialchars( $v, ENT_COMPAT);

					// Anuncio ja te el cdata
					if ( $k != 'destacado' && $k != 'descripcion' ) {
						$v = str_replace( '"', '‘', $v );
						$v = str_replace( "'", '‘', $v );
						$v = str_replace( '&', '&amp;', $v );
					}

					$xml->writeRaw( $v );
				}
				$xml->endElement();

			}
		}

		$xml->endElement();
		if ( ExportCommon::$conta % 1000 == 0 && ExportCommon::$conta != 0 ) {
			$xml->flush();
			ExportCommon::p( 'FLUSH', 1 );
		}
	}

	function get_XMLWriter( $file, $element ) {

		if ( ! $file ) {
			ExportCommon::p( 'El client no té nom d\'usuari a client__client', 'error' );
			return false;
		}
		// TODO-i Canvia carpeta destí, no se si podré posar un sol ftp per tots els clients
		global $gl_is_local;
		$xml = new XMLWriter();
		$xml->openUri( ( $gl_is_local ? DOCUMENT_ROOT : "/var/www/letnd.com/datos/web/" ) . 'clients/habitaclia/' . $file . '.xml' );
		$xml->startDocument( '1.0', 'ISO-8859-1' );
		//if ( $GLOBALS['gl_is_local'] ) { // És més fàcil per depurar
		$xml->setIndent( true );
		$xml->setIndentString( "\t" );
		//}
		$xml->startElement( $element );
		$xml->writeAttribute( 'xmlns:xsi', "http://www.w3.org/2001/XMLSchema-instance" );
		$xml->writeAttribute( 'xmlns:xsd', "http://www.w3.org/2001/XMLSchema" );

		return $xml;

	}

	private function get_contact_details() {

		// $this->export_rs['propietario']   = $this->agencia_rs['company_name'];
		$this->export_rs['email_sucursal'] = $this->agencia_rs['default_mail'];

	}

	// Funcions per camps
	function get_category( $val ) {
		$query = "
			SELECT TIP_INM as tip_inm, TIP_INM2 as tip_inm2, DES_TIP as subtipo,
		       (
					SELECT DES_TIP
					FROM export__habitaclia_tipos
					WHERE export__habitaclia_tipos.TIP_INM = export__habitaclia_subtipos.TIP_INM
				) as tipo
			FROM export__habitaclia_category_to_subtipos
	        LEFT OUTER JOIN export__habitaclia_subtipos USING (subtipo_id)
			WHERE category_id = $val";

		$rs = Db::get_row( $query );

		foreach ( $rs as $k => $v ) {
			$this->export_rs [$k]   = $v;
		}

		return false;
	}

	function get_anunci() {

		$id    = $this->property_rs['property_id'];
		$query = "SELECT
				language, description, property, page_title, page_description, page_keywords FROM `{$this->agencia_rs['dbname']}`.inmo__property_language
			 WHERE property_id = '$id' AND language = 'spa'";

		$rs = Db::get_row( $query );

		$rs['property'] = add_dots( '', $rs['property'], 46 );

		$this->export_rs ['destacado']   = '<![CDATA[' . $rs['property'] . ']]>';
		$this->export_rs ['descripcion'] = '<![CDATA[' . $rs['description'] . ']]>';

		return true;


		/*
		$results = Db::get_rows( $query );

		if ( $results ) {

			$this->export_rs ['Textos'] = [];
			foreach ( $results as $rs ) {
				$ceigrup_language = isset( $this->ceigrup_anunci_languages[ $rs['language'] ] ) ? $this->ceigrup_anunci_languages[ $rs['language'] ] : false;
				if ( $ceigrup_language !== false && $rs ['description'] ) {
					$ex_rs                        = [];
					$ex_rs ['Idioma']             = $ceigrup_language;
					$ex_rs ['Descripcion']        = '<![CDATA[' . $rs['description'] . ']]>';
					$ex_rs ['Titulo']             = '<![CDATA[' . $rs['property'] . ']]>';
					$ex_rs ['TituloMeta']         = '<![CDATA[' . $rs['page_title'] . ']]>';
					$ex_rs ['DescripcionMeta']    = '<![CDATA[' . $rs['page_description'] . ']]>';
					$ex_rs ['PalabrasClave']      = '<![CDATA[' . $rs['page_keywords'] . ']]>';
					$this->export_rs ['Textos'][] = $ex_rs;
				}
			}
		}

		if ( ! $this->export_rs ['Textos'] ) {
			unset ( $this->export_rs ['Textos'] );

			return false;
		}

		return true;*/
	}

	function get_images() {

		$id        = $this->property_rs['property_id'];
		$data_base = $this->agencia_rs['dbname'];

		$query  = "SELECT
				`$data_base`.inmo__property_image.image_id as image_id, name, image_title
				FROM `$data_base`.inmo__property_image, `$data_base`.inmo__property_image_language
				WHERE property_id = " . $id . "
				AND `$data_base`.inmo__property_image.image_id = `$data_base`.inmo__property_image_language.image_id
				AND language = 'spa'
				ORDER BY main_image DESC, ordre";
		$images = Db::get_rows( $query );

		if ( $images ) {
			$this->export_rs ['photos'] = [];

			$ordre = 0;

			foreach ( $images as $image ) {

				$image_id  = $image['image_id'];
				$image_url = $this->client_images_url . $image_id . strrchr( substr( $image['name'], - 5, 5 ), '.' );
				$ordre ++;

				$this->export_rs ['photos'][] = [
					'url'        => $image_url,
					'numimagen'  => $ordre,
					'descimagen' => $image['image_title'],
				];
			}

		}
	}

	function get_latitude_longitud($val) {

		$rs          = &$this->property_rs;
		$latitude = $rs['latitude'];
		$longitude = $rs['longitude'];
		$zoom = $rs['zoom'];
		$show_map_point = $rs['show_map_point'];

		if ( $latitude && $latitude != '0.0000000' ) {
			$this->export_rs ['mapa'] = [
				'latitud' => $latitude,
				'longitud' => $longitude,
				'zoom' => $zoom,
				'puntero' => $show_map_point,
			];
		}

		// retorna nivel_detalle_ubicacion
		return $val;

	}

	function get_referencia( $ref ) {
		return ExportCommon::get_ref( $ref, $this->property_rs, $this->agencia_rs );
	}

	function get_tipus( $val ) {

		$tipus = $this->habitaclia_tipus [ $val ];
		foreach ( $this->habitaclia_tipus as $habitaclia_tipus ) {
			$this->export_rs[ $habitaclia_tipus ] = $tipus == $habitaclia_tipus ? 1 : 0;
		}

		$tipus_preu = substr( $tipus, 0, - 3 );
		if ( ! $this->property_rs['price_consult'] )
			$this->export_rs["precio_$tipus_preu"] = $this->property_rs['price'];

		return false;
	}

	function get_provincia( $val ) {
		$val = Db::get_first( "SELECT provincia FROM `{$this->agencia_rs['dbname']}`.inmo__provincia WHERE provincia_id = %s", $val );

		return $val;
	}

	function get_comarca( $val ) {
		$val = Db::get_first( "SELECT comarca FROM `{$this->agencia_rs['dbname']}`.inmo__comarca WHERE comarca_id = %s", $val );

		return $val;
	}

	function get_municipi( $val ) {
		$val = Db::get_first( "SELECT municipi FROM `{$this->agencia_rs['dbname']}`.inmo__municipi WHERE municipi_id = %s", $val );

		return $val;
	}



	function get_full_address( $val ) {

		$full_adress = '';
		$rs          = &$this->property_rs;

		if ( $rs['adress'] ) {
			$full_adress .= $rs['adress'];
		}
		if ( $rs['numstreet'] ) {
			$full_adress .= ', ' . $rs['numstreet'];
		}

		if ( $rs['block'] ) {
			$full_adress .= ', ' . $rs['block'];
		}
		if ( $rs['flat'] != '' || $rs['door'] ) {
			$full_adress .= ', ' . $rs['flat'] . '-' . $rs['door'];
		}

		if (!$full_adress) $full_adress = $this->get_municipi($rs['municipi_id']) . ', ' . $this->get_comarca($rs['comarca_id']). ', ' . $this->get_provincia($rs['provincia_id']);

		return $full_adress ? $full_adress : false;
	}


	function get_efficiency( $val ) {
		if ( $val == 'progress' ) {
			$val = 'ZZ';
		}
		elseif ( $val == 'exempt' ) {
			$val = 'EX';
		}
		elseif ( $val == 'notdefined' ) {
			$val = false;
		}


		return $val;
	}

	function get_date( $val ) {
		if ( $val == '0000-00-00') return false;
		return date( "d/m/Y", strtotime( $val ) );
	}

	function get_year( $val ) {
		if ( $val == '0000-00-00') return false;
		return date( "Y", strtotime( $val ) );
	}

	// SI es un terreny, la superfície pot ser "land"
	function get_floor_space( $val ) {

		if ( ! $val && $this->property_rs['category_id'] == '5' ) {
			$val = $this->property_rs['land'];
		}

		return (int) $val;

	}

	/* NO FA FALTA, ho guarda tot amb metres
	function get_land( $val ) {

		if ( $this->property_rs['land_units'] == 'ha' ) {
			$val = $val / 10000;
		}

		return (int)$val;
	}*/

	function get_parking( $val ) {
		$garage = $this->property_rs['garage'];

		return $val || $garage;
	}

	function get_tipus2( $val ) {
		$val = ( $val == 'new' ) ? '1' : '0';

		return $val;
	}

	// Genériques

	function get_int( $val ) {
		return (int) $val;
	}


	/*
	 *
	 * UTILITATS BBDD
	 *
	 *
	 *
	 */

	/**
	 * Posa id del municipi de ceigrup a la taula inmo__municipi
	 * /admin/?tool=admintotal&tool_section=ceigrup&action=update_municipis
	 *
	 * Comprovant la mateixa provincia, ( agafo el que te id més baix ):
	 *  Montagut 2 resutats
	 *  Gallifa 3
	 *  Viladecans 2
	 *  Canyelles 2
	 *
	 *
	 */
	function update_municipis() {

		Db::connect_mother();
		$query = "SELECT * FROM inmo__municipi ORDER BY municipi";

		$results_municipis = Db::get_rows( $query );
		$conta             = 0;

		foreach ( $results_municipis as $rs ) {
			$municipi_id = $rs['municipi_id'];
			$municipi    = Db::qstr( $rs['municipi'] );
			$comarca_id  = $rs['comarca_id'];

			$query = "SELECT Poblacion, IdPoblacion FROM export__ceigrup_poblacion WHERE Poblacion = $municipi AND IdProvincia = (SELECT provincia_id FROM inmo__comarca WHERE comarca_id = $comarca_id) ORDER BY IdPoblacion ASC LIMIT 1";


			$results = Db::get_rows( $query );
			$found   = false;
			if ( $results ) {
				/*
				 * Per comprobar repetits, treure el LIMIT de la consulta
				if ( count( $results ) > 1 ) {
					$poblacions = implode_field( $results, 'Poblacion' );
					ExportCommon::p( $poblacions, 1 );
				}
				*/
				$found = true;
				$this->update_municipi_id( $results, $municipi_id );

			}
			if ( ! $found ) {
				$municipi_array = explode( ',', $rs['municipi'] );

				// Busco els compostos
				if ( isset ( $municipi_array[1] ) ) {

					$sep = strpos( $municipi_array[1], "'" ) === false ? ' ' : '';

					$municipi2 = trim( $municipi_array[1] ) . $sep . trim( $municipi_array[0] );
					$municipi2 = Db::qstr( $municipi2 );


					$query = "SELECT Poblacion, IdPoblacion FROM export__ceigrup_poblacion WHERE Poblacion = $municipi2 AND IdProvincia = (SELECT provincia_id FROM inmo__comarca WHERE comarca_id = $comarca_id) ORDER BY IdPoblacion ASC LIMIT 1";

					$results = Db::get_rows( $query );
					if ( $results ) {
						$found = true;
						$this->update_municipi_id( $results, $municipi_id );
					}
					else {
						ExportCommon::p( "$conta - $municipi2 - No s'ha trobat", 1 );
					}

				}
				else {
					// ExportCommon::p( "$conta - $municipi - No s'ha trobat compost", 1 );
				}
			}
			if ( ! $found ) {
				$conta ++;
				ExportCommon::p( "$conta - $municipi - No s'ha trobat", 1 );
			}

		}
		die();
	}

	private function update_municipi_id( &$results, $municipi_id ) {

		$IdPoblacion = $results[0]['IdPoblacion'];
		$query       = "UPDATE inmo__municipi SET ceigrup_id = $IdPoblacion WHERE municipi_id = $municipi_id";
		Db::execute( $query );

	}


}