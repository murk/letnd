<?
/**
 * PvRequisit
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2011
 * @version $Id$
 * @access public
 */
class PvRequisit extends Module{
	var $q, $condition, $is_search;

	function __construct(){
		parent::__construct();
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_search_form() . $this->get_records();
	}
	function get_records()
	{
		$this->set_field('num_requisit','text_size','2');
		
	    $listing = new ListRecords($this);
		
		$listing->set_field('top_id','type','select');
		
		
		if ($this->is_search)
			$listing->condition = $this->condition;
		else
			$listing->add_filter('top_id');
			
		$listing->group_fields = array('top_id');
		
		$listing->order_by =  'top_id ASC, num_requisit';
		
		$listing->add_dots_filter();
		$listing->add_swap_edit();
		
	    return $listing->list_records();
	}
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{	
		
	    $show = new ShowForm($this);
		$show->get_values();
		
		if ($this->action=='show_form_new'){
			$norma_id = R::id('norma_id');
		}
		else{
			$norma_id = $show->rs['norma_id'];
		}
		
		$top_id = Db::get_first('
				SELECT parent_id 
				FROM pv__norma
				WHERE norma_id = ' . $norma_id );
			
		// si parent es 0 vol dir que estem al top_id, l'id de la norma
		if (!$top_id) $top_id = $norma_id;
		
		if ($this->action=='show_form_new'){
		
			$show->set_field('norma_id','form_admin','input');
			$show->set_field('norma_id','default_value',$norma_id);
			
			$show->set_field('top_id','form_admin','input');
			$show->set_field('top_id','default_value',$top_id);
				
			$num_requisit = Db::get_first('
				SELECT num_requisit 
				FROM pv__requisit
				WHERE top_id = ' . $top_id . '
				ORDER BY num_requisit DESC LIMIT 1');
				
			if (!$num_requisit)  $num_requisit = 0;
			$num_requisit ++;
			
			$show->set_field('num_requisit','default_value',$num_requisit);
			$show->set_field('num_requisit','form_admin','input');
		}
		
		$rs = Db::get_row ('SELECT norma,codi_lloc, 
			(SELECT codi_categoria 
				FROM pv__categoria 
				WHERE pv__categoria.categoria_id = pv__norma.categoria_id ) 
				as codi_categoria,
				numero
			FROM pv__norma 
			WHERE norma_id = ' . $top_id);
			
		$parent_norma = $rs['norma'] . ' - <strong>' . strtoupper($rs['codi_lloc']) . ' ' . $rs['codi_categoria'] . ' ' . $rs['numero'] . '</strong>';
		
		if($top_id!=$norma_id) {
			$evolucio = Db::get_first ('SELECT evolucio
			FROM pv__norma 
			WHERE norma_id = ' . $norma_id);
			$parent_norma .= ' - ev. ' . $evolucio;
		}
			
		$show->add_field('parent_norma');
		$show->set_field('parent_norma','type','none');
		$show->set_field('parent_norma','form_admin','out');
		$show->send_field_top('parent_norma');
		
		$show->rs['parent_norma'] = $parent_norma;
		
	    return $show->show_form();
	}

	function save_rows()
	{
	    $save_rows = new SaveRows($this);
	    $save_rows->save();
		
		if ($this->action=='save_rows_delete_selected'){
			Db::execute('DELETE FROM pv__seguimentitem WHERE requisit_id IN (' . implode(',', $save_rows->id) . ')');
		}
	}

	function write_record()
	{
	    $writerec = new SaveRows($this);
	    $writerec->save();
		
		if ($this->action=='add_record'){
			
			$id = $GLOBALS['gl_insert_id'];
			
			// afegeixo el registres a cada client
			$results = Db::get_rows ('SELECT seguiment_id FROM pv__seguiment');
			foreach ($results as $rs){
				$seguiment_id = $rs['seguiment_id'];
				
				$query = "
					INSERT INTO pv__seguimentitem
					(seguiment_id, requisit_id) VALUES
					(".$seguiment_id.", ".$id.");";
				Db::execute($query);
			}
			
			$GLOBALS['gl_do_next'] = false;
			print_javascript('
				if (parent.document.theForm) 
					parent.document.theForm.reset();
					
				top.$("#num_requisit_0").val('. ($writerec->get_value('num_requisit')+1) .');
				
				if (typeof parent.form_set_serialized == "function")
					parent.form_set_serialized();'
				);
			
		}
		
		if ($this->action=='delete_record'){
			Db::execute('DELETE FROM pv__seguimentitem WHERE requisit_id = ' . $writerec->id);
		}
	}

	function manage_images()
	{
	    $image_manager = new ImageManager($this);
	    $image_manager->execute();
	}
	
	
	//
	// FUNCIONS BUSCADOR
	//
	function search(){

		$q = $this->q = R::escape('q');
		
		$search_condition = '';
		
		$this->is_search = true;

		if ($q)
		{	
			$search_condition = " (
				requisit like '%" . $q . "%'
				OR descripcio_requisit like '%" . $q . "%'
				" ;
			
			$search_condition .= ") " ;			
			

			$GLOBALS['gl_page']->title = TITLE_SEARCH . '<strong>&nbsp;&nbsp;"' . $q . '"</strong>';
		}
		// per fer un reset de una cerca, busco cadena buida
		else{
			Main::redirect('/admin/?menu_id=' . $GLOBALS['gl_menu_id']);
		
		}
		$this->condition = $search_condition;
		Debug::add('Search condition', $this->condition);
		$this->do_action('list_records');
	}

	function get_search_form(){
		$tpl = new phemplate(PATH_TEMPLATES);
		$tpl->set_vars($this->caption);
		$tpl->set_file('search.tpl');
		$tpl->set_var('menu_id',$GLOBALS['gl_menu_id']);
		$tpl->set_var('q',  htmlspecialchars(R::get('q')));
		$ret = $tpl->process();		
		
		if ($this->q) {
			$GLOBALS['gl_page']->javascript .= "
				$('table.listRecord').highlight('".addslashes(R::get('q'))."');			
			";
		}		
		
		return $ret;
	}
}
?>