<?

/**
 * UtilityBuy
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2015
 * @version 1
 * @access public
 */
class UtilityBuy extends Module {

	public function __construct() {
		parent::__construct();
	}
	public function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}

	/**
	 * @param $val
	 *
	 * @return mixed
	 */
	public function format_decimal  ($val){
		return str_replace( '.', ',', $val );
	}
	public function get_form()
	{
		$show = new ShowForm($this);

		$price = R::get('price');
		if ( $price ) {
			$this->set_field('price','default_value',$this->format_decimal($price));
		}

		$this->set_field('notary_percent','default_value',$this->format_decimal(UTILITY_NOTARY_PERCENT));
		$this->set_field('register_percent','default_value',$this->format_decimal(UTILITY_REGISTER_PERCENT));
		$this->set_field('agency_percent','default_value',$this->format_decimal(UTILITY_AGENCY_PERCENT));
		$this->set_field('mortgage_percent','default_value',$this->format_decimal(UTILITY_MORTGAGE_PERCENT));
		$this->set_field('mortgage_open_percent','default_value',$this->format_decimal(UTILITY_MORTGAGE_OPEN_PERCENT));
		$this->set_field('mortgage_initial_interest','default_value',$this->format_decimal(UTILITY_MORTGAGE_INITIAL_INTEREST));
		$this->set_field('mortgage_length','default_value',format_int(UTILITY_MORTGAGE_LENGTH));
		$this->set_field('mortgage_register_percent','default_value',format_decimal(UTILITY_MORTGAGE_REGISTER_PERCENT));
		$this->set_field('mortgage_notary_percent','default_value',format_decimal(UTILITY_MORTGAGE_NOTARY_PERCENT));
		$this->set_field('mortgage_valuation','default_value',format_int(UTILITY_MORTGAGE_VALUATION));

		return $show->show_form();
	}

	function write_record()
	{
		// Per guardar s'ha de canviar el valor del select des taxtype_id utility_buy_config.php, bookmark camviar per poder fer...
		/*$writerec = new SaveRows($this);
		$writerec->save();*/
	}
}

?>