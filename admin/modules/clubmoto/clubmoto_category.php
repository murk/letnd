<?
/**
 * ClubmotoCategory
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2012
 * @version $Id$
 * @access public
 */
class ClubmotoCategory extends Module{

	function __construct(){
		parent::__construct();
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function get_records()
	{
		$listing = new ListRecords($this);
		$listing->has_bin = false;
		$listing->order_by = 'ordre asc, category_id';
		$listing->set_options (1, 1, 1, 1, 1, 1, 0, 0, 1); // $options_bar = 1, $options_checkboxes = 1, $save_button = 1, $select_button = 1, $delete_button = 1, $print_button = 1, $edit_buttons = 1, $image_buttons = 1, $split_count = 1
		$listing->show_langs = true;	
		$listing->call('list_records_walk','category_id,is_clubone');
		return $listing->list_records();
	}
	
	function list_records_walk($category_id, $is_clubone){		
		
		$ret = array();
		$checked = $is_clubone?' checked':'';
		$ret['is_clubone']= '<input id="is_clubone_'.$category_id.'" type="checkbox" value="1" name="is_clubone['.$category_id.']"'.$checked.'>
<input type="hidden" value="" name="old_is_clubone['.$category_id.']">';
		$ret['is_clubone'] .= '<span style="vertical-align:baseline"><img src="/admin/modules/clubmoto/clubone.gif" width="62" height="16"></span>';
		
		return $ret;
	}
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{
	    $show = new ShowForm($this);
		$show->has_bin = false;
	    return $show->show_form();
	}

	function save_rows()
	{
	    $save_rows = new SaveRows($this);
	    $save_rows->save();
	}

	function write_record()
	{
	    $writerec = new SaveRows($this);
	    $writerec->save();
	}

	function manage_images()
	{
	    $image_manager = new ImageManager($this);
	    $image_manager->execute();
	}
}
?>