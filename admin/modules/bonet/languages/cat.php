<?php
if (!defined('BONET_MENU_AGENCIA')){
	// menus eina	
	define('BONET_MENU_AGENCIA','Agències');
	define('BONET_MENU_AGENCIA_NEW','Nova agència');
	define('BONET_MENU_AGENCIA_LIST','Llistar agències');
	define('BONET_MENU_AGENCIA_BIN','Paperera de reciclatge');
	
	define('BONET_MENU_ONEAGENCIA','Agència');
	define('BONET_MENU_ONEAGENCIA_EDIT','Editar agència');
	define('BONET_MENU_ONEAGENCIA_LIST','Editar agents');
	define('BONET_MENU_ONEAGENCIA_LIST_FILES','La meva carpeta');
	define('BONET_COMMENT_ONEAGENCIA','Editar agència pròpia i agents pel grup "agents"');
	
	define('BONET_MENU_AGENTENQUESTA','Enquestes');
	define('BONET_MENU_AGENTENQUESTA_NEW','Enquesta de satisfacció');
	define('BONET_COMMENT_AGENTENQUESTA','Enquesta pel grup "agents"');
	
	define('BONET_MENU_AGENT','Agents');
	define('BONET_MENU_AGENT_NEW','Nou agent');
	define('BONET_MENU_AGENT_LIST','Llistar agents');
	define('BONET_MENU_AGENT_BIN','Paperera de reciclatge');

	define('BONET_MENU_AGENT_FILECAT_NEW','Nova categoria arxius');
	define('BONET_MENU_AGENT_FILECAT_LIST','Llistar categories arxius');

	define('BONET_MENU_SPECIALITY','Especialitats');
	define('BONET_MENU_SPECIALITY_NEW','Nova especialitat');
	define('BONET_MENU_SPECIALITY_LIST','Llistar especialitats');
	define('BONET_MENU_SPECIALITY_BIN','Paperera de reciclatge');

	define('BONET_MENU_PAGINA','Edició de pàgines'); 
	define('BONET_MENU_PAGINA_NEW','Entrar nova pàgina');
	define('BONET_MENU_PAGINA_NEW_SECOND','Entrar nova pàgina secundària');
	define('BONET_MENU_PAGINA_LIST','Llistar pàgines'); 
	define('BONET_MENU_PAGINA_LIST_SECOND','Llistar pàgines secundàries'); 
	define('BONET_MENU_PAGINA_BIN','Paperera de reciclatge'); 

	define('BONET_MENU_CURS','Cursos');
	define('BONET_MENU_CURS_NEW','Nou curs');
	define('BONET_MENU_CURS_LIST','Llistar cursos');
	define('BONET_MENU_CURS_BIN','Paperera de reciclatge');
}

/*--------------- nomès castellà i català, no traduit a la resta -------------------*/

/*agencies*/
$gl_caption_agencia['c_agencia'] = 'Agència';
$gl_caption_agencia['c_agents'] = 'Agents';
$gl_caption_agencia['c_user_ids'] = 'Usuaris';
$gl_caption_agencia['c_add_new_agent'] = 'Afegir agent';
$gl_caption_agencia['c_add_new_user'] = 'Afegir usuari';
$gl_caption_agencia['c_view_users'] = 'Veure usuaris';
$gl_caption_agencia['c_enquesta'] = 'Enquesta';

$gl_caption_agent['c_agencia'] = 'Agència';
$gl_caption_agent['c_agent'] = 'Agent';
$gl_caption_agent['c_agent_description'] = 'Descripció';
$gl_caption_agent['c_nom'] = 'Nom';
$gl_caption_agent['c_cognoms'] = 'Cognoms';
$gl_caption_agent['c_adresa'] = 'Adreça';
$gl_caption_agent['c_adress'] = 'Carrer';
$gl_caption_agent['c_numstreet'] = 'Número';
$gl_caption_agent['c_block'] = 'Bloc';
$gl_caption_agent['c_flat'] = 'Pis';
$gl_caption_agent['c_door'] = 'Porta';
$gl_caption_agent['c_provincia_id'] = 'Província';
$gl_caption_agent['c_comarca_id'] = 'Comarca';
$gl_caption_agent['c_municipi_id'] = 'Municipi';
$gl_caption_agent['c_cp'] = 'Codi postal';
$gl_caption_agent['c_telefon'] = 'Telèfon';
$gl_caption_agent['c_telefon2'] = 'Telèfon 2';
$gl_caption_agent['c_fax'] = 'Fax';
$gl_caption_agent['c_correu'] = 'E-mail';
$gl_caption_agent['c_correu2'] = 'E-mail 2';
$gl_caption_agent['c_mail'] = 'E-mail newsletter';
$gl_caption_agent['c_url1'] = 'Web';
$gl_caption_agent['c_url1_name'] = 'Nom de la web';
$gl_caption_agent['c_codi_habitat'] = 'Codi habitatsoft';
$gl_caption_agent['c_api_id'] = 'Núm. API';
$gl_caption_agent['c_aicat_id'] = 'Núm. AICAT';
$gl_caption_agent['c_nums'] = 'Identificadors';

$gl_caption_agent['c_speciality_id'] = 'Especialitats';
$gl_caption_agent['c_group_id'] = 'Grup newsletter';

$gl_caption['c_filecat_id'] = $gl_caption['c_filecat'] = "Categoria";
$gl_caption['c_ordre'] = "Ordre";
$gl_caption['c_form_files_title'] = "Documents de l'agent: ";
$gl_caption['c_edit_files'] = "Documents";
$gl_caption['c_file'] = "Documents";

$gl_caption_speciality['c_speciality'] = 'Especialitat';

$gl_caption['c_filter_actiu'] = 'Tots els agents';
$gl_caption['c_actiu'] = 'Excerceix';
$gl_caption['c_actiu_1'] = 'Excerceix';
$gl_caption['c_actiu_0'] = 'No exerceix';

$gl_caption['c_filter_comarca_id'] = 'Totes les comarques';
$gl_caption['c_filter_municipi_id'] = 'Tots els municipis';

$gl_caption['c_desactivar'] = 'Desactivar';
$gl_caption['c_activar'] = 'Activar';

// mapes
$gl_caption['c_edit_map'] = 'Ubicació';
$gl_caption['c_search_adress']='Buscar adreça';
$gl_caption['c_latitude']='latitud';
$gl_caption['c_longitude']='longitud';
$gl_caption['c_found_direction']='adreça trobada';
$gl_caption['c_save_map'] = 'Guardar ubicació';
$gl_caption['c_click_map'] = 'Fes un click en un punt del mapa per definir la ubicació';
$gl_caption['c_form_map_title'] = 'Ubicació';
$gl_messages['point_saved'] = 'Nova ubicació guardada correctament';

/*pagines*/
$gl_caption_pagina['c_edit_title'] = 'Dades de la pàgina';
$gl_caption_pagina['c_page_title_title'] = 'Dades de la pàgina';
$gl_caption_pagina['c_column_title1'] = 'Primer pàrraf';
$gl_caption_pagina['c_column_title2'] = 'Segon pàrraf';
$gl_caption_pagina['c_column_title3'] = 'Tercer pàrraf';
$gl_caption_pagina['c_column_title4'] = 'Vídeo';
$gl_caption_pagina['c_column_title5'] = 'Google';
$gl_caption_pagina['c_column_title6'] = 'Més informació';

$gl_caption_pagina['c_breadcrumb_edit_level1'] = '"editar pàgina"';
$gl_caption_pagina['c_breadcrumb_edit_level2'] = '"editar pàgina"';
$gl_caption_pagina['c_breadcrumb_edit_level3'] = '"editar pàgina"';
$gl_caption_pagina['c_breadcrumb_edit_list'] = '"llistat de subapartats"';

$gl_caption_pagina['c_subtitle_level2'] = 'Llistar pàgines';
$gl_caption_pagina['c_subtitle_level3'] = 'Llistar pàgines';

$gl_caption_pagina['c_filter_status'] = 'Tots els estats';
$gl_caption_pagina['c_filter_paginatpl_id'] = 'Totes les plantilles';

$gl_caption_pagina['c_edit_content_button'] = 'Editar pàgina';
$gl_caption_pagina['c_edit_pagina_button'] = 'Editar dades';



$gl_caption_pagina['c_ordre'] = 'Ordre';
$gl_caption_pagina['c_entered'] = 'Creat';
$gl_caption_pagina['c_modified'] = 'Última Modificació';
$gl_caption_pagina['c_paginatpl_id'] = 'Plantilla fitxa';
$gl_caption_pagina['c_listtpl_id'] = 'Plantilla llistat';
$gl_caption_pagina['c_link'] = 'Link';
$gl_caption_pagina['c_pagina_file_name'] = $GLOBALS['gl_caption']['c_file_name'];
$gl_caption_pagina['c_body_supertitle'] = 'Supertítol';
$gl_caption_pagina['c_body_supertitle2'] = 'Supertítol';
$gl_caption_pagina['c_body_supertitle3'] = 'Supertítol';
$gl_caption_pagina['c_title'] = 'Títol';
$gl_caption_pagina['c_title2'] = 'Títol';
$gl_caption_pagina['c_title3'] = 'Títol';
$gl_caption_pagina['c_body_subtitle'] = 'Subtítol';
$gl_caption_pagina['c_body_subtitle2'] = 'Subtítol 2';
$gl_caption_pagina['c_paragraph_title'] = 'Títol';
$gl_caption_pagina['c_paragraph_title2'] = 'Títol';
$gl_caption_pagina['c_paragraph_title3'] = 'Títol';
$gl_caption_pagina['c_pagina'] = 'Nom de la pàgina en el menú';
$gl_caption_pagina['c_subpagina'] = 'Subapartats';
$gl_caption_pagina_list['c_pagina'] = 'Nom';
$gl_caption_pagina['c_body_content'] = 'Contingut';
$gl_caption_pagina['c_body_content2'] = 'Contingut';
$gl_caption_pagina['c_body_content3'] = 'Contingut';
$gl_caption_pagina['c_body_content_llistat'] = 'Explicació llistat';
$gl_caption_pagina['c_prepare'] = 'En preparació';
$gl_caption_pagina['c_review'] = 'Per revisar';
$gl_caption_pagina['c_public'] = 'Públic';
$gl_caption_pagina['c_archived'] = 'Arxivat';
$gl_caption['c_status'] = 'Estat';
$gl_caption_pagina['c_pagina_id'] = 'Id';
$gl_caption_pagina['c_parent_id'] = 'Pertany a';
$gl_caption_pagina['c_custom_id'] = 'Galeria de personalització';
$gl_caption_pagina['c_level'] = '';
$gl_caption_pagina['c_blocked'] = 'Block.';
$gl_caption_pagina['c_is_full_pagina'] = '';

$gl_caption_pagina['c_add_pagina_button'] = 'Entrar nova pàgina';
$gl_caption_pagina['c_add_pagina_button_to'] = 'Entrar subapartat a: ';
$gl_caption_pagina['c_list_subpagines'] = 'Llistar tots';
$gl_caption_pagina['c_add_subpagina'] = 'Afegir';
$gl_caption_pagina['c_add_subpagina2'] = 'Afegir subapartat';
$gl_caption_pagina['c_url1_name']='Nom URL';
$gl_caption_pagina['c_url1']='URL';
$gl_caption_pagina['c_url2_name']='Nom URL(2)';
$gl_caption_pagina['c_url2']='URL(2)';
$gl_caption_pagina['c_url3_name']='Nom URL(3)';
$gl_caption_pagina['c_url3']='URL(3)';
$gl_caption_pagina['c_videoframe']='Vídeo (youtube, metacafe...)<br />( 340 x 255 )';
$gl_caption_pagina['c_file']='Arxius';

$gl_caption['c_last_listing']='Llistat anterior';


$gl_caption_category['c_ordre']='Ordre';
$gl_caption_category['c_category']='Categoria';
$gl_caption_category['c_is_clubone'] = 'Menú socios';

$gl_messages['c_restore_items'] = "Els elements marcats no s'han podut recuperar, ja que primer s'ha de recuperar la pàgina superior a la que pertanyen";



/*banners*/
$gl_caption_banner['c_banner_id']='';
$gl_caption_banner['c_ordre']='Ordre';
$gl_caption_banner['c_url1']='Enllaç';
$gl_caption_banner['c_url1_name']='Text a l\'enllaç';
$gl_caption_banner['c_url1_target']='Destí';
$gl_caption_banner['c_url1_target__blank']='Una nova finestra';
$gl_caption_banner['c_url1_target__self']='La mateixa finestra';
$gl_caption_banner['c_file']='Imatge banner <br>(960x92 - 248x122)';
$gl_caption_banner['c_banner']='Banner';


$gl_caption_agentenquesta['c_send_new']='Enviar enquesta';
$gl_caption_agentenquesta['c_agencia_id']='Agència de la Propietat Immobiliària';
$gl_caption_agentenquesta['c_nom_persona']='Nom de la persona que contesta l’enquesta';
$gl_caption_agentenquesta['c_entered']='Data';
$gl_messages['enquesta_sent']='S\'ha enviat l\'enquesta correctament, gràcies per la seva participació';
$gl_messages['enquesta_not_sent']="No s'ha pogut enviar l'enquesta, si us plau torna a probar-ho";


// Cursos

$gl_caption_curs['c_prepare'] = 'En preparació';
$gl_caption_curs['c_archived'] = 'Arxivat';
$gl_caption_curs['c_content'] = 'Descripció';
$gl_caption_curs['c_entered'] = 'Creat';
$gl_caption_curs['c_modified'] = 'Modificat';
$gl_caption_curs['c_status'] = 'Estat';
$gl_caption_curs['c_subtitle'] = 'Subtítol';
$gl_caption_curs['c_videoframe'] = 'Vídeo';

$gl_caption['c_curs_file_name'] = $GLOBALS['gl_caption']['c_file_name'];
$gl_caption['c_curs_old_file_name'] = $GLOBALS['gl_caption']['c_old_file_name'];

$gl_caption_curs['c_filter_status'] = 'Tots els estats';

$gl_caption_curs['c_form_level1_title_curs']='Títols pàgina';
$gl_caption_curs['c_form_level1_title_timetable']='Característiques';
$gl_caption_curs['c_form_level1_title_file']='Arxius adjunts';
$gl_caption_curs['c_form_level1_title_page_title']='SEO';

// Públic
$gl_caption_curs['c_curs'] = $gl_caption['c_curs_id'] = 'Curs';
$gl_caption_curs['c_timetable'] = 'Horaris';
$gl_caption_curs['c_open'] = 'Obert';
$gl_caption_curs['c_full'] = 'Complet';
$gl_caption_curs['c_finished'] = 'Finalitzat';


$gl_caption_curs['c_speciality_id'] = 'Especialitat';

$gl_caption_curs['c_start_date'] = 'Data inici';
$gl_caption_curs['c_end_date'] = 'Data final';
$gl_caption_curs['c_maximum_person'] = 'Màxim persones';
$gl_caption_curs['c_weekday_id'] = 'Dies';

$gl_caption_curs['c_durada'] = "Durada";
$gl_caption_curs['c_modalitat'] = "Modalitat";
$gl_caption_curs['c_lloc'] = "Lloc";
$gl_caption_curs['c_adressa'] = "Adreça";
$gl_caption_curs['c_tipologia'] = "Tipologia";
$gl_caption_curs['c_convalidacio'] = "Convalidacions";
$gl_caption_curs['c_credit'] = "Crèdits";
$gl_caption_curs['c_temari'] = "Temari";
$gl_caption_curs['c_ponent'] = "Ponents";
$gl_caption_curs['c_avaluacio'] = "Sistema d'avaluació";
$gl_caption_curs['c_preu'] = "Preu";
$gl_caption_curs['c_descompte'] = "Descompte";
$gl_caption_curs['c_titulacio'] = "Titulació";
$gl_caption_curs['c_inscripcio'] = "Inscripció";

$gl_caption_curs['c_modalitat_none'] = "";
$gl_caption_curs['c_modalitat_presencial'] = "Presencial";
$gl_caption_curs['c_modalitat_semi-presencial'] = "Semi presencial";
$gl_caption_curs['c_modalitat_taller-practic'] = "Taller pràctic";
$gl_caption_curs['c_modalitat_online'] = "Online";