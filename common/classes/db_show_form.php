<?php
/**
 * Clase ShowForm
 *
 * @author Joan Sanahuja <sanahuja@gmail.com>
 * @version 1.0
 */

/**
 * Classe per crear formularis
 *
 * @author Joan Sanahuja <sanahuja@gmail.com>
 * @version 1.0
 * @uses FormObject
 */
class ShowForm extends BaseListShow {
	var $id, $title_copy, $form_tabs = array(), $form_subtitle = '', $add_dots = false;

	function __construct( &$module = false, $id = false ) {
		BaseListShow::__construct( $module );
		$this->id = $id ? $id : $this->get_id();
		Debug::p( [
			[ 'Form ID', $this->id ],
		], 'general' );

		$this->list_show_type = 'form';
		// per defecte mostrem tots els idiomes en el formulari a admin, i a public nomès un
		$this->show_langs   = ! $this->is_site_public;
		$this->title_modify = $this->messages['title_edit'];
		$this->title_copy   = $this->messages['title_copy'];

		$this->set_options(); // per defecte tenim totes les opcions al menu d'opcions
	}

	function get_id() {

		$post_id = isset( $_POST[ $this->id_field ] ) ? current( $_POST[ $this->id_field ] ) : 0;
		$id      = isset( $_GET['id'] ) ? $_GET['id'] : $post_id;
		$id      = isset( $_GET[ $this->id_field ] ) ? $_GET[ $this->id_field ] : $id;

		// en el cas de parent_tool_section i friendly url  ej: http://w.tecnomix/spa/tecnomix/projecte/4/proyectos.html , tenim projecte_id enlloc de new_id
		if ( $id == '' && isset( $_GET[ $this->parent_tool_section . '_id' ] ) ) {
			$id = $_GET[ $this->parent_tool_section . '_id' ];
		}

		$check_id = $id;

		if ( isset( $this->fields[ $this->tool_section . '_file_name' ] ) ) {
			// si hi ha file_name agafo l'id del registre
			// això no funciona per blocks i tot això, ja que nomes es al show_form, s'ha de posar en un altre lloc
			$file_name = R::file_name( 'file_name' );

			if ( $file_name ) {
				$id = Db::get_first( "SELECT " . $this->id_field . " 
					FROM " . $this->table . "_language 
					WHERE language= '" . LANGUAGE . "'
					AND " . $this->tool_section . "_file_name = '" . $file_name . "'" );
				if ( ! $id ) {
					$id       = $file_name;
					$check_id = true;
				} else {
					$check_id = false;
				}
			}

			$id = R::id( $id, false, false );

			// si hi ha id comprobo si aquest id te un file_name per redireccionar
			if ( $this->is_site_public && $check_id ) {
				$file_name = Db::get_first( "SELECT " . $this->tool_section . "_file_name 
					FROM " . $this->table . "_language 
					WHERE language = '" . LANGUAGE . "'
					AND " . $this->id_field . " = '" . $id . "'" );

				if ( $file_name && $this->is_index ) {

					$category_file_name = ''; // falta fer
					$params_arr         = array(
						'action',
						$this->id_field,
						'id',
						'page' . $this->name,
						'tool',
						'tool_section',
						'language',
						'page_file_name'
					);
					$url                = Page::get_link( $this->parent_tool ? $this->parent_tool : $this->tool, $this->parent_tool_section ? $this->parent_tool_section : $this->tool_section, false, $id, $this->friendly_params, $params_arr, $file_name, $category_file_name, $GLOBALS['gl_page']->page_file_name );

					// No faig redirect si el crawler es el facebook, així pot indexar la pa`gina desde social.php
					if (
						strpos( $_SERVER["HTTP_USER_AGENT"], "facebookexternalhit/" ) === false && strpos( $_SERVER["HTTP_USER_AGENT"], "Facebot" ) === false
					) {
						Main::redirect( $url );
					}


				}
			}
		}
		if ( $this->is_index ) {
			$GLOBALS['gl_current_form_id'] = $id;
		}

		return $id;
	}

	/**
	 * Executa la clase un cop s'han definit els paràmetres
	 *
	 * @access public
	 * @return string/void
	 */
	function show_form( $parse_template = true ) {
		$this->list_show();

		$this->set_vars_common();
		$this->set_vars_form();
		if ( $parse_template ) {
			return $this->parse_template();
		}
	}

	function set_record() {
		$this->show_form( false );
		$this->parse_template( false );
	}

	function parse_template( $process = true ) {

		// o poso abans del bucle per cada camp, per així poder sobreescriure resultats en cas que sigui necessari
		// crido funció general amb els parametres marcats
		$this->get_call_rs( $this->rs );
		// BUCLE PER CADA CAMP DEL RESUTAT

		foreach ( $this->fields as $key => $val ) {
			if ( ( $val[ 'form_' . $this->site_part ] == 'input' ) || ( $val[ 'form_' . $this->site_part ] == 'text' ) || ( $key == $this->id_field ) || ( $val[ 'form_' . $this->site_part ] == 'out' ) ) // (($val['form_' . $this->site_part] != '0') || ($key == $this->id_field)) ho he cambiat, faltava el $this->site_part] != 'no', ara està igual que al list
			{
				$this->process_field( $val, $key, $this->rs );
			}
		}
		// li enchufo els hidden en el primer key que trobo, així ho fa automatic i no haig de posar-ho al template
		// sino ho poso en la variable hidden, per fer servir en tots els forms no automatics
		if ( isset( $this->rs['fields'][0]['key'] ) ) {
			$this->rs['fields'][0]['key'] .= $this->hidden;
		} else {
			$this->set_var( 'hidden', $this->hidden );
		}

		// $this->js_string = ; vaig anular per poder agafar la cadena sempre quew vulgui
		$this->tpl->set_var( 'js_string', "return validar(this" . $this->js_string . ');' );

		if ( strstr( $this->action, "show_form_copy" ) ) {
			$this->rs['id'] = $this->rs[ $this->id_field ] = 0;
		}
		$this->tpl->set_vars( $this->rs );

		$this->tpl->set_file( $this->template );
		if ( ! $process ) {
			// acces directe als resultats del llistat dins el tpl
			//$this->loop = &$this->tpl->loops['loop'];
			return;
		}

		$content = $this->tpl->process();
		// nomès modifiquem el gl_content per comptabilitat v2, treure a v3
		if ( ! $this->is_v3 ) {
			$GLOBALS['gl_content'] = $content;
		}

		return $content;
	}

	/**
	 * Consulta la BBDD per obtenir els valors dels camps<br>
	 * Segons el valor de $action:
	 * - Si action='show_record': mostrem un registre a la part publica <br>
	 * Llavors hem de cridar aquesta funció manualment per saber el valor de "public", ja que si està public cridem {@link show_form} comsempre, però sino está public fem qualsevol altra acció, per exemple mostrar tot el llistat o anar a una pàgina de "error no trobat":
	 * - Si action='show_form': editem un registre a la part privada, en aquest cas aquesta funció no cal cridar-la, ja la cridem al executar {@link set_vars_form}
	 * - Si action='show_form_new': creem un nou registre, en aquest cas aquesta funció no cal cridar-la, ja que crem un registre nou
	 *
	 * @access public
	 * @return void
	 */
	function get_values( $return_one = false ) {
		$this->automatic_condition = '(' . $this->table . '.' . $this->id_field . " = '" . $this->id . "')";
		$query                     = $this->get_query();

		$results           = Db::get_rows( $query['q'] );
		$this->has_results = ! empty( $results );
		if ( $results ) {
			$this->rs += $results[0];
		}
		if ( $return_one ) {
			return $results[0];
		}
	}

	function GetOne() {
		return $this->get_values( true );
	}

	function get_post_values() {
		foreach ( $_POST as $key => $value ) {
			$this->rs += array( $key => $value[0] );
		}
	}

	/**
	 * Variables automàtiques, sempre iguals a tots els formularis
	 *
	 * @access private
	 * @return void
	 */
	function set_vars_form() {
		global $gl_menu_id, $gl_tool_top_variable, $gl_page, $gl_languages, $gl_print_link, $gl_saved;

		$this->tpl->set_var( 'form_new', false );
		$this->tpl->set_var( 'record', false );
		$this->tpl->set_var( 'form_edit', false );
		$this->tpl->set_var( 'print_link', $gl_print_link );
		$this->tpl->set_var( 'has_bin', $this->has_bin );
		$this->tpl->set_var( 'form_subtitle', $this->form_subtitle );
		$this->tpl->set_var( 'back_button', $this->last_listing['link'] );

		switch ( true ) {
			case strstr( $this->action, "show_form_new" ) || strstr( $this->action, "get_form_new" ): // Si estem creant un registre nou
				// $this->get_post_values();
				// afegim la mateixa terminacio que el form al final de save_record
				$action_add = explode( "show_form_new", $this->action );
				$action_add = isset( $action_add[1] ) ? $action_add[1] : '';
				$this->rs += array(
					'menu_id' => $gl_menu_id,
					'action'  => "add_record" . $action_add
				);
				$this->tpl->set_var( 'form_new', true ); // per mostrar troç html nomès a form_new;
				$this->rs['id'] = $this->rs['id_copy'] = '0';
				// $this->rs += $_POST[0];
				$this->rs['link_action_delete'] = '';
				$this->rs[ $this->id_field ]    = '0';
				$this->rs['image_name']         = '';
				break;
			// Si estem mostrant un registre (part pública)
			// es pot cridar get_values manualment abans de show_form o no
			case strstr( $this->action, "show_record" ) || $this->action == "get_record":// a V3 no existirà el strstr ja que no hi ha action_add
				! isset( $this->rs[ $this->id_field ] ) ? $this->get_values() : false;
				$this->tpl->set_var( 'record', true );
				$this->rs['id']     = $this->rs['id_copy'] = $this->id;
				$this->rs['action'] = '';
				$action_add         = '';
				if ( $this->has_images ) {
					$this->rs += $this->uploads['image']->get_record_images_i( $this->rs['id'] );
				}
				$this->rs += UploadFiles::gl_upload_get_record_files( $this, $this->rs['id'] ); // nomès a show_record, a show_form ja ho fa la clase form_object
				break;
			case strstr( $this->action, "show_form_edit" ) || strstr( $this->action, "get_form_edit" ):
				$gl_saved ? $this->get_values() : $this->get_post_values();
				// si editem a la paperera trec totes les opcions i poso com a show_record
				if ( isset( $this->rs['bin'] ) && $this->rs['bin'] == 1 ) {
					$this->action = 'show_record';
					Debug::add( 'Atenció!!!!!!!!!!!!!!, canvi automàtic d\'action a ', 'show_record, pot petar si estem a get_record' );
					$this->tpl->set_var( 'record', true );
					$this->rs['id']     = $this->rs['id_copy'] = $this->id;
					$this->rs['action'] = '';
					$action_add         = '';
				} else {
					// afegim la mateixa terminacio que el form al final de save_record
					$action_add = explode( "show_form_edit", $this->action );
					$action_add = isset( $action_add[1] ) ? $action_add[1] : '';
					if ( $GLOBALS['gl_is_admin'] ) {
						$gl_page->title = $this->title_modify;
					}
					$this->rs += array(
						'menu_id' => $gl_menu_id,
						'action'  => "save_record" . $action_add
					);
					$this->rs['id'] = $this->rs['id_copy'] = $this->id;

					$action_delete                  = ( $this->has_bin ? 'bin_record' : 'delete_record' ) . $action_add; // no es pot editar desde la paperera, per tant sempre es bin_record
					$this->rs['link_action_delete'] = '?action=' . $action_delete . get_all_get_params( array(
							'action',
							$this->id_field
						), '&' );
					$this->tpl->set_var( 'form_edit', true );
					$this->tpl->set_var( 'form_tabs', $this->form_tabs );
					// si hi ha hagut un error al guardar agafem els valors del post per no tornar a entrar les mateixes dades
				}
				if ( $this->has_images ) {
					$this->rs += $this->uploads['image']->get_record_images_i( $this->rs['id'] );
				}
				break;

			// REVISAR està en fase beta - s'haurà de testejar segins els casos, de moment funcionarà anewsletter
			case strstr( $this->action, "show_form_copy" ):
				// afegim la mateixa terminacio que el form al final de save_record
				$this->get_values();
				$action_add     = explode( "show_form_copy", $this->action );
				$action_add     = $action_add[1] ? $action_add[1] : '';
				$gl_page->title = $this->title_copy;
				$this->rs += array(
					'menu_id' => $gl_menu_id,
					'action'  => "copy_record" . $action_add
				);
				$this->rs['id']                 = $this->rs['id_copy'] = $this->id;
				$this->rs['link_action_delete'] = '';
				$this->tpl->set_var( 'delete_button', false ); // quan fem una copia d'un registre no es pot borrar
				$this->tpl->set_var( 'form_edit', true );
				if ( $this->has_images ) {
					$this->rs += $this->uploads['image']->get_record_images_i( $this->rs['id'] );
				}
		} // switch
		// si es public afegim action i tool_action
		if ( $this->is_site_public ) {
			$this->rs['link_action'] = "?action=" . $this->rs['action'] . "&amp;" . $this->base_link .
			                           get_all_get_params( array(
				                           'action',
				                           $this->id_field,
				                           'tool',
				                           'tool_section'
			                           ), '&' );
		} else {
			$this->rs['link_action'] = "?action=" . $this->rs['action'] . "&amp;menu_id=" . $gl_menu_id .
			                           get_all_get_params( array( 'action', $this->id_field, 'menu_id' ), '&' );
		}

		$this->rs['data_link']  = '/admin/?action=show_form_edit' . get_all_get_params( array( 'action' ), '&' );
		$this->rs['image_link'] = "?action=list_records_images" . $action_add . "&amp;" . $this->id_field . "=" . $this->rs[ $this->id_field ] . get_all_get_params( array(
				'action',
				$this->id_field
			), '&' );

		$language = current( $gl_languages['public'] );

		if (USE_FRIENDLY_URL){
			$this->rs['preview_link'] = "/$language/$this->tool/$this->tool_section/$this->id.htm?preview";
		}
		else {
			$this->rs['preview_link'] = "/index.php?$this->base_link&amp;action=show_record&amp;$this->id_field=$this->id&amp;language=$language&preview";
		}



		$this->tpl->set_var( 'show_record_link', $this->get_show_record_link( $this->rs ) );
		$this->set_prev_next_links();

		$this->rs['id_field'] = $this->id_field;
		Debug::add( 'Rs show_form', $this->rs );
	}


	/**
	 * Per poder aprofitar la mateixa funció call_after_set_records del llistat
	 * i la mateixa funcionalitat
	 */
	public function call_after_set_record($function_name, $set = 'loop') {

		$function_name = array( &$this->module, $function_name ); // nomes v3


			$parameters = array();
			$parameters[] = $this->rs;
			$parameters[] = &$this;

			$ret = call_user_func_array( $function_name, $parameters );
			if ( is_array( $ret ) ) {

				// sobrescric per llistats amb plantilla
				foreach ($ret as $k => $v){
					if (isset($this->tpl->vars[$k])) {
						$this->tpl->vars[$k] = $v;
						$this->_set_field_value_after_set_records( $this->tpl->vars, $k, $v );
					}
				}
			}

	}

	function set_editable($editable){
		if ($editable){
			$this->is_editable = true;
		}
		else{
			$this->is_editable = false;
			$this->set_options (); // resetejo el set_options amb les opcions forçades quan no es editable
		}
	}

	// Configuració del menu d'opcions d'editar
	function set_options( $save_button = 1, $delete_button = 1, $preview_button = 0, $print_button = 0 ) {

		// quan no es editable mai mostro aquests botons
		if (!$this->is_editable) {
			$save_button        = 0;
			$delete_button      = 0;
		}

		$this->tpl->set_var( 'save_button', $save_button );
		$this->tpl->set_var( 'delete_button', $delete_button );
		$this->tpl->set_var( 'preview_button', $preview_button );
		$this->tpl->set_var( 'print_button', $print_button );

		if ( ! $save_button ) {
			$this->set_fields_editable( false );
		}
	}

	function set_form_level1_titles() {
		$this->form_level1_titles = func_get_args();
	}

	function set_form_level2_titles() {
		$this->form_level2_titles = func_get_args();
	}

	function set_prev_next_links() {
		Main::load_class( 'db_utils' );
		Utils::set_prev_next_links( $this );
	}
}