<?foreach($loop as $l): extract ($l)?>
	<? if ($item):?>
	<h6 class="active<?=$block_selected?> <?=$class?>"><?=$item?></h6>
	<?endif //item?>
	<ul>
		<?foreach($loop as $l): extract ($l)?>
		<li>
			<a href="<?=$link?>" class="active<?=$selected?> <?=$class?>" title="<?=htmlspecialchars($item)?>"><?=$item?></a>
		</li>
		<?endforeach //$loop?>
	</ul>
<?endforeach //$loop?>