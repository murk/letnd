<?
/**
 * LlemenaPagina
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2010
 * @version $Id$
 * @access public
 
 He desactiva t que s'entrin imatges just desprès d'entrar un registre, els registres s'han d'editar dede el seu llistat així agafa el process
 
 
 */
class LlemenaPagina extends Module{
	var $pagina_ids = array(), $parent_ids = array(), $levels = array(), $paginas = array(), $menus = 1, $levels_count = array(), $dir, $delete_button=0, $override_form_parent_id = false;
	var $form_tabs;
	
	// desplegable formulari
	var $pagina_id_select = '', $pagina_id='';
	function __construct(){
		parent::__construct();
	}
	function on_load(){
		$this->form_tabs = array('0' => array('tab_action'=>'show_form_map','tab_caption'=>$this->caption['c_edit_map']));
	
		$this->menus = $this->process?$this->process:'1';
		if ($this->process!='2') {
			$this->set_field('parent_id','list_admin','0');
			$this->set_field('parent_id','form_admin','0');
		}	
		if ($this->action=='show_form_new') {
			// entrar una nova subpagina a menu esquerra
			if (isset($_GET['subpage']) && isset($_GET['pagina_id']) && $this->process=='2'){
				$this->override_form_parent_id = true;
				$this->set_field('menu','type','hidden');
				$this->set_field('menu','default_value','2');
				$this->set_field('menu','form_admin','input');
			}
			else{
				$this->set_field('menu','form_admin','input');
			}
		}		
		if ($this->action=='list_records_bin') {
			$this->set_field('parent_id','list_admin','0');
			$this->set_field('parent_id','form_admin','0');
			$this->menus = '1,2,3,4';
		}	
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function get_records()
	{
	    // poso el filtre en cookie per repetir sempre l'últim
		if ($this->menus == '2'){
			if (isset($_GET['top_parent'])) {
				 if ($_GET['top_parent']=='null') {
					setcookie ("llemena_top_parent", "", time() - 3600);
				}
				else{
					setcookie("llemena_top_parent",$_GET['top_parent']);	
				}
			}
			elseif (isset($_COOKIE["llemena_top_parent"])){
				$_GET['top_parent'] = $_COOKIE["llemena_top_parent"];
			}
		}
		
		$listing = new ListRecords($this);
		$listing->swap_fields('paginatpl_id','ordre');
		$listing->swap_fields('paginatpl_id','page_title');
		if ($this->action!='list_records_bin'){
		$listing->paginate = false;
		$listing->set_options(1, 1, 1, 1, 1, 1, 0, 1, 1, 1);
		$listing->set_field('pagina','type','none');
				
		$this->get_pagina_ids(0,1);
		$pagina_ids = $this->pagina_ids?implode($this->pagina_ids,','):'0';
		
		$listing->condition = 'llemena__pagina.pagina_id IN ('.$pagina_ids.')';
		$listing->extra_fields = 'llemena__pagina.pagina_id as pagina_id2'; // per comptabilitat mysql 4
        $listing->condition .= " ORDER BY FIELD(pagina_id2, " . $pagina_ids . ")";	
		
		if ($this->menus == '2'){
		$listing->add_field('top_parent');
		$listing->set_field('top_parent','list_admin','no');
		$listing->set_field('top_parent','select_table','llemena__pagina,llemena__pagina_language');
		$listing->set_field('top_parent','select_fields','llemena__pagina.pagina_id,pagina');
		$listing->set_field('top_parent','select_condition',"llemena__pagina.pagina_id=llemena__pagina_language.pagina_id AND level=1 AND language='" . LANGUAGE . "' AND bin=0 AND menu IN (" . $this->menus . ") order by ordre");
		$listing->add_filter ('top_parent','','','select',false);	
		}
		
		$listing->add_button ('edit_content_button', 'action=show_form_edit', 'boto1');
		$listing->add_button ('edit_pagina_button', 'action=show_form_edit', 'boto2');
		$listing->add_button ('add_subpagina_button', 'action=show_form_new&subpage=true', 'boto1');
		$listing->call('list_records_walk','pagina_id,pagina,level,parent_id,has_content,template,body_content,body_content_list,link',true);
		}
		else{		
			$listing->set_options(1, 1, 1, 1, 1, 1, 0, 0, 0, 0);
		}
	    return $listing->list_records();
	}
	function list_records_walk(&$listing,$pagina_id,$pagina,$level,$parent_id,$has_content,$template,$body_content, $body_content_list, $link){
		
		$style = '';
		if ($level == 1) $style = "font-weight:bold;";
		if ($level > 2) $style = "color:#5e6901;";
		
		$l_key = $parent_id . '_' . $level;
		if (!isset($this->levels_count[$l_key])) $this->levels_count[$l_key] = 0;
		$this->levels_count[$l_key]++;
		
		$ret['pagina'] = '<div style="margin-left:'.(30*($level-1)).'px;'.$style.'">'.$this->levels_count[$l_key].' - ' .$pagina.'</div>';
		
		// trec el boto d'imatges quan hi ha link ( o sigui modul ), excepte en la home, que servirà per galeria de la home
		
		// també poden tindre pàgines, per poder fer un llistat amb imatges com a restaurants
		//$listing->buttons['edit_images_button']['show'] = (!$link || $link=='/');
		//$listing->buttons['add_images_button']['show'] = (!$link || $link=='/');
		
		
		$listing->buttons['edit_content_button']['show'] = $link=='';($has_content==1);
		$listing->buttons['edit_pagina_button']['show'] = $link!='';
		$ret['body_content']=$body_content_list?$body_content_list:add_dots('body_content', $body_content);
		
		return $ret;
	}
	function get_pagina_ids($parent_id, $level){
	

		$loop = array();
		$query = "SELECT llemena__pagina.pagina_id as pagina_id, level, pagina, parent_id
						FROM llemena__pagina
						JOIN llemena__pagina_language
						USING (pagina_id)";
		$query .= "		WHERE parent_id = " . $parent_id . "
						AND bin <> 1
						AND menu IN (" . $this->menus . ")
						AND language = '" . LANGUAGE . "'";
		if ($level ==1 && isset($_GET['top_parent']) && $_GET['top_parent']!='null') $query .= " AND pagina_id = " . $_GET['top_parent'];
		
		$query .= "		ORDER BY menu ASC, ordre ASC, pagina_id";
		// si no hi ha top_parent, mostrem del primer, nomes al menu 2
		if ($this->menus == '2' && $level ==1 && (!isset($_GET['top_parent']) || $_GET['top_parent']=='null') ) $query .= " LIMIT 1";
		
		Debug::add('Query get_pagina_recursive',$query);	
		$results = Db::get_rows($query);

		if ($results)
		{
			foreach($results as $rs)
			{
				$this->pagina_ids[]=$rs['pagina_id'];
				$this->parent_ids[]=$rs['parent_id'];
				$this->levels[]=$rs['level'];
				$this->paginas[]=$rs['pagina'];
				
				$this->get_pagina_ids($rs['pagina_id'], $level+1);
			}
		}
	}
	
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{
	    $show = new ShowForm($this);
		$show->form_tabs = $this->form_tabs;
		$show->call('show_records_walk','pagina_id,has_content,template,parent_id',true);
		
		// comprovo que el process coincideix amb el menu, per si es treu el menu_id manualment de dalt o qualsevol altra cosa
		$show->get_values();
		if ($show->id && $show->rs['menu']!=$this->process) Main::redirect ('/admin/');
		
		// són pagines amb modul o amb link a una altra pàgina interna
		// nomès s'edita el títol
		if ($show->id && $show->rs['link']!='') {
			$this->unset_field('paginatpl_id');
			$this->unset_field('title');
			$this->unset_field('body_subtitle');
			$this->unset_field('body_content_list');
			$this->unset_field('body_content');
			//$this->unset_field('municipi');
			$this->unset_field('category_id');
			$this->unset_field('address');
			$this->unset_field('visit');
			$this->unset_field('url1');
			$this->unset_field('url1_name');
			$this->unset_field('url2');
			$this->unset_field('url2_name');
			$this->unset_field('url3');
			$this->unset_field('url3_name');
			$this->unset_field('videoframe');
			$this->unset_field('file');
			// també poden tindre pàgines, per poder fer un llistat amb imatges com a restaurants -- if ($show->rs['link']!='/') $show->has_images=false; // la home si te imatges
			$show->form_tabs = array();
		}
		
	    return $show->show_form();
	}
	function show_records_walk(&$show,$pagina_id,$has_content,$template,$parent_id){
		if ($this->override_form_parent_id) $parent_id = $_GET['pagina_id'];
		// obtinc desplegable parent_id
		$this->get_pagina_ids(0,1);
		$self_found = false;
		foreach ($this->pagina_ids as $key=>$loop_id){
			$space = '';
			$level = $this->levels[$key];		
			$l_key = $parent_id . '_' . $level;
			if (!isset($this->levels_count[$l_key])) $this->levels_count[$l_key] = 0;
			$this->levels_count[$l_key]++;
		
			
			for ($i = 1; $i <= $level; $i++) {
				$space.='&nbsp;&nbsp;&nbsp;&nbsp;';
			}
			
			if ($level == 1) $style = "color:#000000;";
			if ($level == 2) $style = "color:#666666;";
			if ($level > 2) $style = "color:#5e6901;";
		
			$selected = ($parent_id == $loop_id)?' selected':'';
			if ($this->parent_ids[$key] == '0') $self_found = false;
			
			if (($loop_id != $pagina_id) && (!$self_found)){								
				$self_found = false; 
				$item = $space . $this->levels_count[$l_key] . ' - ' . $this->paginas[$key];
				$this->pagina_id_select.='<option value="'.$loop_id.'"'.$selected. ' style="' . $style.'">' . $item.'</option>';
			}
			// quan troba la pagina actual no mostra ni aquesta ni cap fill
			else{				
				$self_found = true; // seguirà fent això fins que trobem el primer parent_id = 0;
			}
		}
		
		$this->pagina_id_select = 
			'<select class="wide" name="parent_id['.$pagina_id.']">
				<option value="0">Nivell superior</option>' . 
				$this->pagina_id_select . '
			</select>
			<input name="old_parent_id['.$pagina_id.']" type="hidden" value="'.$parent_id.'">';		
		return array('parent_id' => $this->pagina_id_select);
	}

	function save_rows()
	{
	    $save_rows = new SaveRows($this);
	    $save_rows->save();
	}

	function write_record()
	{	    
		$writerec = new SaveRows($this);
		
		// nomès hi ha submenus en el menu 2
		if ($this->process=='2') {
			// obtinc el nivell actual
			$parent_id = $writerec->get_value('parent_id');
			$query = 'SELECT level FROM llemena__pagina WHERE pagina_id = ' . $parent_id;
			$level = Db::get_first ($query);
			$level = (int)$level +1;
			$writerec->set_field('level','override_save_value',$level);
		}
		else{
			$level = 1;
		}
		
		// si guardo un registres, el registre pot tindre fills, per tant haure de canviar els seus levels
		// buscar totes les pagines inferiors i canviar el nivell
		if ($this->action=='save_record'){
		
			$pagina_id = $writerec->id;
			$query = 'SELECT level FROM llemena__pagina WHERE pagina_id = ' . $pagina_id;
			$old_level = Db::get_first ($query);
			
			$diff_level = $old_level-$level;
			
			if ($diff_level){
				$this->get_pagina_ids($pagina_id, $old_level+1);			
				foreach ($this->pagina_ids as $id){
					$query = 'UPDATE llemena__pagina SET level=level-('.$diff_level.') WHERE pagina_id=' . $id;
					Db::execute ($query);
					//$child_level = 
				}
				
				debug::p($this->pagina_ids);
				debug::p($this->levels);
			}
		}		
				
	    $writerec->save();
		
		
		// buscar totes les pagines inferiors i canviar el nivell
		
	}

	function manage_images()
	{
	    $image_manager = new ImageManager($this);
		$image_manager->form_tabs = $this->form_tabs;
	    $image_manager->execute();
	}
	
	
	function show_form_map (){ //esenyo el napa
		global $gl_news;
							
		$rs = Db::get_row('SELECT * FROM llemena__pagina WHERE pagina_id = ' . $_GET['pagina_id']);
		$rs['municipi']=$this->caption['c_municipi_'.$rs['municipi']];
		$rs['title'] = Db::get_first('SELECT pagina FROM llemena__pagina_language  WHERE pagina_id = ' . $_GET['pagina_id'] . ' AND language = LANGUAGE');
		$rs['address'] = Db::get_first('SELECT address FROM llemena__pagina_language  WHERE pagina_id = ' . $_GET['pagina_id'] . ' AND language = LANGUAGE');
		$rs['is_default_point'] = ($rs['latitude']!=0)?0:1;
		$rs['latitude1']= substr($rs['latitude'],0,strpos($rs['latitude'],';'));
		$rs['longitude1']=substr($rs['longitude'],0,strpos($rs['longitude'],';'));
		$rs['center_latitude']=$rs['center_latitude']!=0?$rs['center_latitude']:GOOGLE_CENTER_LATITUDE;
		$rs['center_longitude']=$rs['center_longitude']!=0?$rs['center_longitude']:GOOGLE_CENTER_LONGITUDE;
		$rs['zoom']=$rs['zoom']?$rs['zoom']:GOOGLE_ZOOM;
		$rs['menu_id'] =  $_GET['menu_id'];		
		$rs['id'] =  $rs[$this->id_field];		
		$rs['id_field'] =  $this->id_field;		
		$this->set_vars($rs);//passo els resultats del array
		$this->set_file('llemena/pagina_map.tpl'); //crido el arxiu
		$this->set_vars($this->caption); //passo tots els captions
		$gl_news = $this->caption['c_click_map'];
		$content = $this->process();//processo
		$GLOBALS['gl_content'] = $content;		
	}
	function save_record_map(){
		global $gl_page;

		R::escape_array($_POST);
		extract( $_POST );

		if ( $latitude && $longitude ) {

			Db::execute ("UPDATE `llemena__pagina` SET
			`latitude` = '$latitude',
			`longitude` = '$longitude',
			`center_latitude` = '$center_latitude',
			`center_longitude` = '$center_longitude',
			`zoom` = '$zoom'
			WHERE `pagina_id` =$pagina_id
			LIMIT 1") ;

			$ret ['message'] = $this->messages['point_saved'];
		}
		else {
			$ret ['message'] = $this->messages['point_not_saved'];
		}

		Debug::p_all();
		die (json_encode($ret));
	}
	function save_rows_map_points(){
		global $gl_page;
		$pagina_id = $_GET['pagina_id'];
		$points = trim($_POST['points']);
		$points = explode(';',$points);
		
		$latitudes = array();
		$longitudes = array();
		$error = false;
		
		foreach ($points as $point){
			if ($point){
				$p = explode(',',trim($point));
				
				$la =trim($p[0]);
				$lo = trim($p[1]);
				
				if (is_numeric($la) && is_numeric($lo) 
					&& $la!='' && $lo!=''
					&& count($p)==2) {
				$latitudes[] =$la;
				$longitudes[] = $lo;
				}
				else{
					$error=true;
					break;
				}
			}
		}
		if ($error){
			$this->map_points_error();		
		}
		
		$center_latitude = $latitudes[0]; // centro en el primer punt
		$center_longitude = $longitudes[0]; // centro en el primer punt
		
		$latitudes = implode (';',$latitudes);
		$longitudes = implode (';',$longitudes);
		
		debug::p($latitudes);
		debug::p($longitudes);		
		
		if ($latitudes && $longitudes){	
			Db::execute ("UPDATE `llemena__pagina` SET
				`latitude` = '$latitudes',
				`longitude` = '$longitudes',
				`center_latitude` = '$center_latitude',
				`center_longitude` = '$center_longitude'
				WHERE `pagina_id` =$pagina_id
				LIMIT 1") ;
				
			//$gl_page->show_message($this->messages['point_saved']);
			$_SESSION['message'] = $this->messages['point_saved'];
			Debug::p_all();
			print_javascript('parent.window.location.replace(parent.window.location);');
			die();	
		}
		else{
			$this->map_points_error();
		}
	}
	function map_points_error(){	
		global $gl_page;
		$gl_page->show_message('Hi ha un error en l\'entrada de coordenades, si us plau comproveu que estigui ben formada');
		Debug::p_all();
		die();
	}
}
?>