<div class="listTitle">
	<?=$c_portal_config_title?>
</div>
<div class="portal-checkboxes">
	<div class="portal-options-text"><?= $c_apicat_export_all ?></div>
	<label>
		<img src="/admin/themes/inmotools/images/fotocasa.png" width="61" height="14" alt="Fotocasa"/>
		<div class="portal-checkbox"><?= $fotocasa_export_all ?></div>
	</label>
	<label>
		<img src="/admin/themes/inmotools/images/habitaclia.png" width="70" height="22" alt="Habitaclia"/>
		<div class="portal-checkbox"><?= $habitaclia_export_all ?></div>
	</label>
	<label>
		<img src="/admin/themes/inmotools/images/apicat.png" width="50" height="18" alt="Apicat"/>
		<div class="portal-checkbox"><?= $apicat_export_all ?></div>
	</label>
	<span><?= $c_apicat_export_all_text ?></span>
</div>
<? /* Ja no cal amb els errors posats a cada casa <p><?=$c_apicat_export_all_text_2?></p> */ ?>
