<?

/**
 * NewsNew
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2010
 * @version $Id$
 * @access public
 */
class PortfoliPortfoli extends Module {

	function __construct() {
		parent::__construct();
	}

	function on_load() {
		// social
		Main::load_class( 'social' );
		$this->form_tabs =
			array(
				'0' =>
					array(
						'tab_action'  => 'show_form_social&amp;menu_id=' . $GLOBALS['gl_menu_id'],
						'tab_caption' => $this->caption['c_edit_social']
					)
			);
		if ( ! $this->config['show_page_id'] ) {
			$this->unset_field( 'page_id' );
			$this->caption['c_destacat'] = $this->caption['c_destacat_not_page_id'];
		}
		if ( ! $this->config['has_files'] ) {
			$this->unset_field( 'file' );
		}

	   	Main::load_class('other');
		Other::set_fields($this);

		parent::on_load();
	}

	function list_records() {
		$GLOBALS['gl_content'] = $this->get_records();
	}

	function get_records() {
		$listing = new ListRecords( $this );
		$listing->call( 'records_walk', '', true );
		Social::init_listing( $listing );

		$listing->add_swap_edit();
		$listing->order_by = 'ordre asc, portfoli_id desc';

		$listing->add_filter( 'category_id' );
		$listing->add_filter( 'destacat' );
		if ( isset( $_GET['destacat'] ) && $_GET['destacat'] != 'null' ) {
			$listing->set_field( 'destacat', 'list_admin', 'input' );
		}
		if ( isset( $_GET["category_id"] ) && $_GET["category_id"] && $_GET["category_id"] != 'null' ) {
			$listing->join = "INNER JOIN portfoli__portfoli_to_category USING (portfoli_id)";
			//$listing->condition .= "AND group_id = " . $_GET["group_id"];
		}

		return $listing->list_records();
	}

	function records_walk( &$listing ) {

		$ret = array();
		// social
		$this->set_buttons_social( $listing );

		return $ret;
	}

	function show_form() {
		$GLOBALS['gl_content'] = $this->get_form();
	}

	function get_form() {
		$this->set_field( 'entered', 'default_value', now( true ) );
		$this->set_field( 'modified', 'default_value', now( true ) );

		$show            = new ShowForm( $this );
		$show->form_tabs = $this->form_tabs;
		$show->set_form_level1_titles( 'destacat', 'page_title' );

		return $show->show_form();
	}

	function save_rows() {
		$save_rows = new SaveRows( $this );
		$save_rows->save();
	}

	function write_record() {
		$writerec = new SaveRows( $this );
		$writerec->set_field( 'modified', 'override_save_value', 'now()' );
		$writerec->save();

		if ( $this->action == 'save_record' || $this->action = 'add_record' ) {
			$descriptions     = $writerec->get_value( 'description' );
			$id               = $writerec->id;


			foreach ( $descriptions as $language => $description ) {
				
				$description_text = addslashes( rip_tags( $description ) );
				
				$query = "
					UPDATE portfoli__portfoli_language
						SET description_text='" . $description_text . "'
						WHERE portfoli_id= " . $id . "
						AND language='" . $language . "' LIMIT 1;";
				// Debug::p( $query );
				Db::execute( $query );
			}
		}
	}

	function manage_images() {
		$image_manager            = new ImageManager( $this );
		$image_manager->form_tabs = $this->form_tabs;
		$image_manager->execute();
	}

	/////////////
	// SOCIAL
	///////////////
	function show_form_social() {
		$id = R::id( $this->id_field );
		$language = Social::get_language();

		$query = "
			SELECT 
				title, 
				subtitle, 
				description,
				portfoli_file_name AS file_name
			FROM " . $this->table . "_language 
			WHERE " . $this->id_field . "='" . $id . "'
			AND language = '" . $language . "'";
		$rs    = Db::get_row( $query );

		if ( ! $rs )
			return;
		$host_url = substr( HOST_URL, 0, - 1 );
		$link     = HOST_URL . $language . '/portfoli/portfoli/' . $id . '.htm';

		$facebook_params = array(
			'message'     => '',
			'name'        => htmlspecialchars( $rs['title'] ),
			'caption'     => htmlspecialchars( $rs['subtitle'] ),
			'description' => htmlspecialchars( p2nl( $rs['description'] ) )
		);
		$twitter_params  = array(
			'status' => htmlspecialchars( $rs['title'] )
		);

		$social                = new Social( $this, $id, $facebook_params, $twitter_params, $link );
		$GLOBALS['gl_content'] = $social->get_form();
	}

	function write_record_social() {
		$social = new Social( $this );
		$social->publish();
	}

	function set_buttons_social( &$listing ) {

		Social::set_buttons_listing( $listing );

	}
}

?>