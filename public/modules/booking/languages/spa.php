<?
$gl_caption['c_subject_booked'] = 'Reserva inmueble - #';
$gl_caption['c_subject_booked_owner'] = 'Reserva inmueble - #';
$gl_caption['c_no_date_selected'] = "No se ha seleccionado ninguna fecha";
$gl_caption['c_booked_message_1'] = "La reserva se ha efectuado con éxito.";
$gl_caption['c_booked_message_2'] = "A continuación entre el resto de la información";
$gl_caption['c_form_book_title'] = "Datos de la reserva";
$gl_caption['c_form_book_custumer_title'] = "Dades personals";
$gl_caption['c_book_finish_button'] = "Finalizar reserva";
$gl_caption['c_book_form_title'] = "Reserva #%s";

$gl_caption['c_edit_book'] = "Editar reserva";

$gl_caption['c_mail_book_details'] = "Detalles de la reserva";
$gl_caption['c_mail_custumer_details'] = "Datos personales";
$gl_caption['c_mail_extras'] = "Extras";
$gl_caption ['c_ref']='Ref.';
$gl_caption['c_promcode_button'] = 'Aplicar';


$gl_caption_payment['c_payment_title'] = 'Efectuando la compra';
$gl_caption_payment['c_method_title1']="Escoja el metodo de pago que desea utilizar.";
$gl_caption_payment['c_method_title2']="El importe total es de <strong>%s&nbsp;%s</strong> I.V.A incluido.";
$gl_caption_payment['c_method_title_initial']="El importe del pago inicial es de <strong>%s&nbsp;%s</strong> IVA incluido. Se tendrá que efectuar el resto del pago, %s&nbsp;%s, antes de la fecha de entrada";
$gl_caption_payment['c_method_title_rest']="El importe del %s pago es de <strong>%s&nbsp;%s</strong> IVA incluido.";
$gl_caption_payment['c_method_title_number_2']="segundo";
$gl_caption_payment['c_method_title_number_3']="tercero";
$gl_caption_payment['c_method_title_number_4']="cuarto";

$gl_caption_payment['c_method_paypal']="Pago con PayPal";
$gl_caption_payment['c_method_account']="Pago por transferencia bancaria";
$gl_caption_payment['c_method_lacaixa']="Tarjeta de crédito";

$gl_caption_payment['c_show_paypal']="Se está efectuando el pago a Paypal, si no empieza el proceso automáticamente clique aqui:";
$gl_caption_payment['c_show_lacaixa']="Se está efectuando el pago por tarjeta, si no empieza el proceso automáticamente clique aqui:";

$gl_caption['c_account_title']= 'Datos de la transferencia' ;
$gl_caption['c_account_amount']= 'Importe' ;
$gl_caption['c_account_concept']= 'Concepto' ;

$gl_caption_book['c_payment_whole']="Pago";
$gl_caption_book['c_payment_1']="Primer pago";
$gl_caption_book['c_payment_2']="Segundo pago";
$gl_caption_book['c_payment_3']="Tercer pago";
$gl_caption_book['c_payment_4']="Cuarto pago";
$gl_caption_book['c_payment_paid']="Pagado";
$gl_caption_book['c_payment_pending']="Pendiente";
$gl_caption_book['c_payment_pay']="Pagar";
?>