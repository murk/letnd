<?

/**
 * AdmintotalFilesearch
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2011
 * @version $Id$
 * @access public
 */
class AdmintotalFilesearch extends Module {
	var $server_backup_dir, $local_backup_dir, $is_server, $is_local, $is_local_windows, $is_local_linux;

	function __construct() {
		parent::__construct();
	}

	function on_load() {

		$this->is_local = $GLOBALS['gl_is_local'];
		$this->is_server = ! $GLOBALS['gl_is_local'];

		if ($this->is_server) $GLOBALS['gl_news'] = 'Nomes va amb el domini on s\'entra';

		$this->is_local_linux   = ! $this->is_server && $GLOBALS['gl_is_linux'];
		$this->is_local_windows = ! $this->is_server && ! $GLOBALS['gl_is_linux'];

		$this->server_backup_dir = '/letnd.com/datos/motor/data/backups/';
		$this->local_backup_dir  = $this->is_server?'/var/www'.$this->server_backup_dir:DOCUMENT_ROOT . 'data/backups/';
	}

	function list_records() {

		$GLOBALS['gl_content'] = $this->get_records();
		// $GLOBALS['gl_content'] = $this->is_server ? 'Nomes en local' : $this->get_records();
	}

	function get_records() {
		// TODO-i Per que funcioni en el servidor, s'ha de posar /var/www/ open_basedir

		$template = 'admintotal/filesearch.tpl';
		$tpl      = &$this->tpl;
		$tpl->set_file( $template );
		$tpl->set_var( 'is_server', $this->is_server );
		$tpl->set_var( 'is_local', $this->is_local );
		$loop            = array();
		$loop_no_results = array();
		$loop_errors     = array();

		$class = '';

		$q             = R::get( 'q_filesearch' );
		$solution_get  = R::escape( 'solution' );
		$client_get    = R::escape( 'client' );
		$client_id_get = R::escape( 'client_id' );


		if ( $q ) {
			//Db::connect_mother();
			// Conecto online per veure els clients que hi ha
			$this->connect_mother();

			$query = 'SELECT  `client_id`,  `fiscalname`,  `solution`,  `domain`,  `2domain` AS domain2,  `3domain` AS domain3,  `solution`,  `client_dir` FROM `letnd`.`client__client` WHERE bin=0 ';

			$and = "AND ";
			if ( $solution_get ) {
				$query .= $and . "solution like '$solution_get'";
				$and = "AND ";
			}
			if ( $client_id_get ) {
				$query .= $and . "client_id = '$client_id_get'";
				$and = "AND ";
			}

			$query .= " ORDER BY domain";


			$results_clients = db::get_rows( $query );

			/*}*/

			foreach ( $results_clients as $client ) {
				extract( $client );

				// TODO-i En local busca al directori local, per tant si no tenim la web sencera descarregada potser que no trobi alguns arxius
				// S'hauria de busca sempre online, i si no hi  ha l'arxiu, descarregar-lo igualment. Per fer aixó s'ha de comporbar que xexisteixi tota la ruta, anar creant les carpetes que faltin
				if ( $this->is_local ) {
					if ( $this->is_local_windows ) {
						$base_dir = "d:\\Letnd\\$client_dir";
					} else {
						$base_dir = "/letnd/$client_dir";

					}
				}
				else {
					$base_dir = "/var/www/$domain/datos/web/$client_dir";
					if ( !is_dir( $base_dir ) ) {
						$base_dir = "/var/www/$domain2/datos/web/$client_dir";
					}
					if ( !is_dir( $base_dir ) ) {
						$base_dir = "/var/www/$domain3/datos/web/$client_dir";
					}

					// die (file_get_contents( $base_dir . '/templates/general.tpl' ));
				}

				if ( is_dir( $base_dir ) ) {

					$filenames = glob( $base_dir . '/' . $q );

					if ( ! $filenames ) {
						$loop_no_results[] = array(
							'domain'     => $domain,
							'client_dir' => $client_dir
						);
					}

					foreach ( $filenames as $filename ) {

						$file = str_replace( $base_dir, '', $filename );

/*						if ($this->is_local) {
							$file = '\\' . substr( $file, 1 );
						}*/

						// Només arxius, quan tinguin un punt,  sinó son directoris
						if (strpos($file, '.')) {
							$loop [] = array(
								'domain'     => $domain,
								'solution'   => $solution,
								'client_id'  => $client_id,
								'client_dir' => $client_dir,
								// 'server_dir' => $server_dir,
								'filename'   => $filename,
								'file'       => $file
							);
						}

					}
				} else {
					$loop_errors[] = array(
						'domain'     => $domain,
						'solution'   => $solution,
						'client_dir' => $client_dir
					);
				}


			}

			Db::reconnect();
		}


		$tpl->set_var( 'q', htmlspecialchars( $q ) );
		$tpl->set_var( 'solution_get', htmlspecialchars( $solution_get ) );
		$tpl->set_var( 'client_selected', $client_get );
		$tpl->set_var( 'client_id_selected', $client_id_get );
		$tpl->set_var( 'loop', $loop );
		$tpl->set_var( 'loop_count', count( $loop ) );
		$tpl->set_var( 'loop_no_results', $loop_no_results );
		$tpl->set_var( 'loop_errors', $loop_errors );
		$tpl->set_var( 'clients', json_encode( $this->get_clients() ) );

		return $tpl->process();
	}

	public function get_file() {
		$domain             = R::post( 'domain' );
		$local_file         = R::post( 'local_file' );
		$file_relative_path = R::post( 'file_relative_path' );
		$server_dir = '';

		if ( $this->is_local ) {
			$ret = $this->check_server_dir();
			if ( $ret['error'] ) {
				die( json_encode( $ret ) );
			}
			$server_dir = $ret['server_dir'];
		}

		$ret['html'] = false;

		$file_name   = pathinfo( $local_file, PATHINFO_BASENAME );
		$server_file = $server_dir . $file_relative_path;

		if ( $this->is_server ) {
			$this->get_file_server( $local_file, $ret );
		}
		else {
			$this->get_file_local( $domain, $local_file, $file_name, $server_file, $ret );
		}

	}

	public function get_mime_type ($file) {

		$ext = substr( $file, strrpos( $file, "." ) + 1 );

		$mime = '';

		if ( $ext == 'js' ) {
			$mime = 'text/javascript';
		}
		elseif ( $ext == 'json' ) {
			$mime = 'application/json';
		}
		elseif ( $ext == 'php' ) {
			$mime = 'application/x-httpd-php';
		}
		elseif ( $ext == 'tpl' ) {
			$mime = 'application/x-httpd-php';
		}
		elseif ( $ext == 'css' ) {
			$mime = 'text/css';
		}

		return $mime;

	}

	public function get_file_server( $local_file, $ret ) {

		$ret['message']         = '';
		$ret['server_dir']         = $local_file;
		$ret['server_dir_message'] = $this->get_message( 'folder-o', 'Directori al servidor', '', true, 'span' );

		$ret['html'] = file_get_contents( $local_file );
		$ret['mime'] = $this->get_mime_type ( $local_file );
		die ( json_encode( $ret ) );

	}

	public function get_file_local( $domain, $local_file, $file_name, $server_file, $ret ) {

		$this->_back_up_file( $local_file, $file_name, $domain, 'local' );
		$downloaded = $this->_download_file( $server_file, $local_file );

		$ret['message'] = $this->get_message( 'clone', 'Arxiu local copiat a "d:\letnd\data\backups"' );

		$ret['message'] .= $this->get_message(
			'long-arrow-left',
			'Arxiu descarregat del servidor',
			'No s\'ha descarregat l\'arxiu del servidor i no es pot carregar',
			$downloaded );

		if ( $downloaded ) {
			$ret['html'] = file_get_contents( $local_file );
		}

		$ret['mime'] = $this->get_mime_type ( $local_file );

		die ( json_encode( $ret ) );
	}

	private function get_message( $icon, $text_success, $text_error = '', $is_ok = true, $tag = 'div' ) {
		if ( $is_ok ) {
			return "<$tag><i class=\"fa fa-$icon fa-fw\"></i>&nbsp; $text_success</$tag>";
		} else {
			return "<$tag class='error'><i class=\"fa fa-exclamation fa-fw\"></i>&nbsp; $text_error</$tag>";
		}
	}

	private function _save_file( $file, $content ) {
		return file_put_contents( $file, $content );
	}

	public function save_file() {

		$file_content       = R::post( 'file_content' );
		$domain             = R::post( 'domain' );
		$local_file         = R::post( 'local_file' );
		$server_dir         = R::post( 'server_dir' );
		$file_relative_path = R::post( 'file_relative_path' );
		$ret['message']     = '';

		$file_info   = pathinfo( $local_file );
		$file_name   = $file_info['basename'];
		$file_extension   = $file_info['extension'];



		$server_file = $server_dir . $file_relative_path;

		if ( $this->is_server ) {
			$this->save_file_server( $local_file, $file_content, $ret, $server_file, $file_name, $domain, $server_dir, $file_extension );
		}
		else {
			$this->save_file_local( $local_file, $file_content, $ret, $server_file, $file_name, $domain, $server_dir, $file_extension );
		}

	}

	private function save_file_local( $local_file, $file_content, $ret, $server_file, $file_name, $domain, $server_dir, $file_extension ) {

		// GUARDO
		$saved = $this->_save_file( $local_file, $file_content );

		$ret['message'] .= $this->get_message(
			'save',
			'Arxiu local guardat',
			'No s\'ha pogut guardar l\'arxiu local',
			$saved );

		if ( ! $saved ) {
			die( json_encode( $ret ) );
		}

		// COPIO ARXIU DEL SERVIDOR
		$saved = $this->_back_up_file( $server_file, $file_name, $domain, 'server' );

		$ret['message'] .= $this->get_message(
			'clone',
			'Arxiu remot copiat a "/letnd.com/datos/motor/data/backups"',
			'No s\'ha pogut fer còpia de l\'arxiu del servidor',
			$saved );

		if ( ! $saved ) {
			die( json_encode( $ret ) );
		}

		// PUJO ARXIU
		$uploaded = $this->_upload_file( $server_file, $local_file );


		$ret['message'] .= $this->get_message(
			'long-arrow-right',
			'Arxiu pujat al servidor',
			'No s\'ha pujar l\'arxiu del servidor',
			$uploaded );

		if ( ! $uploaded ) {
			die( json_encode( $ret ) );
		}

		// BORRO styles.css I jscripts.js
		$ret['message'] .= $this->_delete_cached_files( $server_dir, $file_extension );

		die( json_encode( $ret ) );
	}

	private function save_file_server( $local_file, $file_content, $ret, $server_file, $file_name, $domain, $server_dir, $file_extension ) {


		// COPIO ARXIU DEL SERVIDOR
		// Per poder fer la copia s'han de donar permisos 777
		$saved = $this->_back_up_file( $local_file, $file_name, $domain, 'local' ); // estem al servidor però guardem en local ( no FTP )

		$ret['message'] .= $this->get_message(
			'clone',
			'Arxiu remot copiat a "/letnd.com/datos/motor/data/backups"',
			'No s\'ha pogut fer còpia de l\'arxiu del servidor',
			$saved );

		if ( ! $saved ) {
			die( json_encode( $ret ) );
		}

		// GUARDO - Poder per guardar s'haura de fer per ftp també amb un arxiu temporal i despres borant-lo. Podent fallar els permisos, per fer copia a backups he hagut de canviar.los, en canvi altres amb 664 ja es pot modificar. Suposo que depen de l'usuari que es propietari.
/*
 *
 *    Metode directe, amb el nou ftp no funcionava
 *
 * 		$saved = $this->_save_file( $local_file, $file_content );

		$ret['message'] .= $this->get_message(
			'save',
			'Arxiu local guardat',
			'No s\'ha pogut guardar l\'arxiu local',
			$saved );

		if ( ! $saved ) {
			die( json_encode( $ret ) );
		}*/


		// PUJO ARXIU ( igual que en local, ho faig per ftp
		$uploaded = $this->_upload_file( $server_file, $local_file );


		$ret['message'] .= $this->get_message(
			'long-arrow-right',
			'Arxiu pujat al servidor',
			'No s\'ha pujar l\'arxiu del servidor',
			$uploaded );

		if ( ! $uploaded ) {
			die( json_encode( $ret ) );
		}

		// BORRO styles.css I jscripts.js
		$ret['message'] .= $this->_delete_cached_files( $server_dir, $file_extension );

		die( json_encode( $ret ) );
	}

	private function _back_up_file( $file, $file_name, $domain, $action ) {
		if ( $action == 'local' ) {
			$path = $this->local_backup_dir;
		} else {
			$path = $this->server_backup_dir;
		}
		$sufix      = date( "d-m-Y_G.i.s" );
		$final_file = $path . $domain . '_' . $sufix . '_' . $file_name;

		if ( $action == 'local' ) {
			// die("'$file' '$final_file' ");
			return copy( $file, $final_file );
		} else {
			$conn_id = get_ftp_letnd_global();

			return $this->ftp_copy( $conn_id, $file, $final_file, $file_name );
		}
	}

	private function _upload_file( $server_file, $local_file ) {
		// Conecto ftp
		$conn_id = get_ftp_letnd_global();

		if ( ftp_put( $conn_id, $server_file, $local_file, FTP_BINARY ) ) {
			$ret = true;
		} else {
			$ret = false;
		}
		ftp_close( $conn_id );

		return $ret;
	}

	private function _download_file( $server_file, $local_file ) {
		// Conecto ftp
		$conn_id = get_ftp_letnd_global();

		if ( ftp_get( $conn_id, $local_file, $server_file, FTP_BINARY ) ) {
			$ret = true;
		} else {
			$ret = false;
		}
		ftp_close( $conn_id );

		return $ret;
	}

	private function _delete_cached_files( $server_dir, $file_extension ) {

		if ($file_extension != 'css' && $file_extension != 'js')
			return;

		if ( $file_extension == 'css' ) {
			$file = $server_dir . '/web/styles.css';
		} else {
			$file = $server_dir . '/web/jscripts.js';
		}



		// Conecto ftp
		$conn_id = get_ftp_letnd_global();

		if ( ftp_delete( $conn_id, $file ) ) {
			$ret = true;
		} else {
			$ret = false;
		}

		ftp_close( $conn_id );



		$message = $this->get_message(
			'smile-o',
			'Borrat: ' . $file,
			"No s'han borrar: $file, pot ser aquest client no genera els estils",
			$ret );


		return $message;

	}

	public function check_server_dir() {

		$client_id = R::id( 'client_id', false, '_POST' );

		$this->connect_mother();

		$query = 'SELECT  `domain` AS domain1,  `2domain` AS domain2,  `3domain` AS domain3, `client_dir` FROM `letnd`.`client__client` WHERE client_id=' . $client_id;
		$rs    = Db::get_row( $query );
		Db::reconnect();

		$ret['server_dir'] = false;
		$ret['error']      = false;
		$server_dirs       = '';

		// Entro per ftp
		$conn_id = get_ftp_letnd_global();

		for ( $i = 1; $i < 4; $i ++ ) {
			$domain     = $rs[ 'domain' . $i ];
			$client_dir = $rs['client_dir'];
			$server_dir = "/$domain/datos/web/$client_dir";
			if ( $domain )
				$server_dirs .= $server_dir . ', ';


			if ( $domain && ftp_chdir( $conn_id, $server_dir ) ) {
				$ret['server_dir']         = $server_dir;
				$ret['server_dir_message'] = $this->get_message( 'folder-o', 'Directori al servidor', '', true, 'span' );
				break;
			}
		}

		ftp_close( $conn_id );

		if ( ! $ret['server_dir'] ) {
			$ret['server_dir'] = substr( $server_dirs, 0, - 2 );
			$ret['error']      = $this->get_message( '', '', 'No es troba el directori al servidor', false, 'span' );
		}

		// die ( json_encode( $ret ) );
		return $ret;
	}

	private function get_clients() {
		$this->connect_mother();
		$query = "SELECT  `client_id`,  `fiscalname`, `domain` FROM `letnd`.`client__client`
					 WHERE bin=0 ORDER BY fiscalname ASC, domain ASC";

		$results = Db::get_rows( $query );

		$new_results = array();
		foreach ( $results as $rs ) {
			$client = $rs['fiscalname'];
			if ( $client && $rs['domain'] ) {
				$client .= ' - ';
			}
			$client .= $rs['domain'];
			$new_results [] = array( 'data' => $rs['client_id'], 'value' => $client );
		}
		Db::reconnect();

		return $new_results;
	}

	function ftp_copy( $conn_id, $file, $final_file, $file_name ) {
		// on recupere l'image puis on la repose dans le nouveau folder
		if ($this->is_local) {
			$temp_file = DOCUMENT_ROOT . 'data/temp/' . $file_name;
		}
		else {
			$temp_file = '/letnd.com/datos/motor/data/temp/' . $file_name;
		}
		if ( ftp_get( $conn_id, $temp_file, $file, FTP_BINARY ) ) {
			if ( ftp_put( $conn_id, $final_file, $temp_file, FTP_BINARY ) ) {
				unlink( $temp_file );
			} else {
				return false;
			}

		} else {
			return false;
		}

		return true;
	}

	/*public function get_select_long_values() {
		{
			$this->connect_mother();

			//	$field = R::get( 'field' );
			//	$id = R::get( 'id' );
			$limit = 100;
			$q = R::escape( 'query' );

			$ret                = array();
			$ret['suggestions'] = array();

			$r = &$ret['suggestions'];

			//	if ($q)
			//	{
			// 1 - Comarca, que comenci
			$query = "SELECT  `client_id`,  `fiscalname`, `domain` FROM `letnd`.`client__client`
					WHERE fiscalname LIKE '" . $q . "%'
					OR domain  LIKE '" . $q . "%'
					ORDER BY fiscalname ASC, domain ASC
					LIMIT " . $limit;

			$results = Db::get_rows( $query );

			$rest = $limit - count( $results );

			// 2 - Comarca, qualsevol posició
			if ( $rest > 0 ) {

			$query = "SELECT  `client_id`,  `fiscalname`, `domain` FROM `letnd`.`client__client`
					WHERE (
						fiscalname LIKE '%" . $q . "%'
						AND fiscalname NOT LIKE '" . $q . "%'					
					)
					OR (
						domain  LIKE '%" . $q . "%'
						AND domain NOT LIKE '" . $q . "%'	
					)
					ORDER BY fiscalname ASC, domain ASC
					LIMIT " . $rest;

				$results2 = Db::get_rows( $query );

				$results = array_merge( $results, $results2 );

			}

			// Presento resultats
			foreach ( $results as $rs ) {
				$value = $rs['fiscalname'] ?
					$rs['fiscalname'] . " - " . $rs['domain'] :
					$rs['domain'];
				$r[]   = array(
					'value' => $value,
					'data'  => $rs['client_id']
				);
			}
			//	}


			Db::reconnect();
			die ( json_encode( $ret ) );


		}

	}*/


	/**
	 * Conecta a la BBDD mare al SERVIDOR DE LETND
	 *
	 * @access public
	 */
	function connect_mother() {
		// Get database parameters
		global $configuration;
		$dbhost  = '81.25.126.213';//$configuration['server']['db_host_mother'];
		$dbname  = $configuration['server']['db_name_mother'];
		$dbuname = $configuration['server']['db_user_name_mother'];
		$dbpass  = $configuration['server']['db_password_mother'];
		// Start connection
		Db::connect( $dbhost, $dbuname, $dbpass, $dbname );
	}
}

?>