<?php
// no es poden posar ; entre mig de la consulta ja que parteix les consultes per el ; a no ser que fem servir $update_sql en array()
			
if (is_table("product__customer") && !is_field("product__customer",'has_equivalencia')){		
	$update_sql[]="ALTER TABLE `product__customer`
	ADD COLUMN `has_equivalencia` TINYINT(1) NOT NULL DEFAULT '0' AFTER `is_wholesaler`;";
	$update_sql[]="ALTER TABLE `product__customer`
	ADD COLUMN `wholesaler_rate` ENUM('1','2','3','4','5') NOT NULL DEFAULT '1' AFTER `is_wholesaler`;";	
	
	$update_sql[]="INSERT INTO `product__configadmin` (`name`, `value`) VALUES ('has_equivalencia_default', '0');";
	
	$update_sql[]="ALTER TABLE `product__product`
	ADD COLUMN `pvd5` DECIMAL(11,2) NOT NULL DEFAULT '0.00' AFTER `pvd`;";
	$update_sql[]="ALTER TABLE `product__product`
	ADD COLUMN `pvd4` DECIMAL(11,2) NOT NULL DEFAULT '0.00' AFTER `pvd`;";
	$update_sql[]="ALTER TABLE `product__product`
	ADD COLUMN `pvd3` DECIMAL(11,2) NOT NULL DEFAULT '0.00' AFTER `pvd`;";
	$update_sql[]="ALTER TABLE `product__product`
	ADD COLUMN `pvd2` DECIMAL(11,2) NOT NULL DEFAULT '0.00' AFTER `pvd`;";
	
	$update_sql[]="ALTER TABLE `product__product_to_variation`
	ADD COLUMN `variation_pvd2` DECIMAL(11,2) NOT NULL DEFAULT '0.00' AFTER `variation_pvd`,
	ADD COLUMN `variation_pvd3` DECIMAL(11,2) NOT NULL DEFAULT '0.00' AFTER `variation_pvd2`,
	ADD COLUMN `variation_pvd4` DECIMAL(11,2) NOT NULL DEFAULT '0.00' AFTER `variation_pvd3`,
	ADD COLUMN `variation_pvd5` DECIMAL(11,2) NOT NULL DEFAULT '0.00' AFTER `variation_pvd4`;";
	
	$update_sql[]="ALTER TABLE `product__variation`
	ADD COLUMN `variation_pvd2` DECIMAL(11,2) NOT NULL DEFAULT '0.00' AFTER `variation_pvd`,
	ADD COLUMN `variation_pvd3` DECIMAL(11,2) NOT NULL DEFAULT '0.00' AFTER `variation_pvd2`,
	ADD COLUMN `variation_pvd4` DECIMAL(11,2) NOT NULL DEFAULT '0.00' AFTER `variation_pvd3`,
	ADD COLUMN `variation_pvd5` DECIMAL(11,2) NOT NULL DEFAULT '0.00' AFTER `variation_pvd4`;";
	
	$update_sql[]="ALTER TABLE `product__tax`
	ADD COLUMN `equivalencia` DECIMAL(2,1) NOT NULL DEFAULT '0' AFTER `tax_value`;";
	
	$update_sql[]="
	UPDATE `product__tax` SET `equivalencia`=4 WHERE  `tax_value`=18 LIMIT 1;";
	
	$update_sql[]="
	UPDATE `product__tax` SET `equivalencia`=1 WHERE  `tax_value`=8 LIMIT 1;";
	
	$update_sql[]="
	UPDATE `product__tax` SET `equivalencia`=0.5 WHERE  `tax_value`=4 LIMIT 1;";
	
	$update_sql[]="
	DELETE FROM `product__configadmin` WHERE  `name`='equivalencia' LIMIT 1;";
}	

	
?>