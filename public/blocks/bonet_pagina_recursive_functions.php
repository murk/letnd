<?
function _block_bonet_pagina_get_children($parent_id, $level, $selected_ids, $menu, $last_level){
	$loop = array();
	if ($last_level == $level) return $loop;
	$query = "SELECT bonet__pagina.pagina_id as pagina_id, pagina, title, body_subtitle, pagina_file_name, parent_id, has_content, link, pagina_text_id
					FROM bonet__pagina_language, bonet__pagina
					WHERE bonet__pagina_language.pagina_id = bonet__pagina.pagina_id
					".$GLOBALS['gl_bonet_status']."
					AND parent_id = " . $parent_id . "
					AND bin <> 1
					AND menu = '" . $menu . "'
					AND language = '" . LANGUAGE . "'
					ORDER BY ordre ASC, pagina, pagina_id";
	Debug::add($query,'Query bonet_pagina_recursive');
	$results = Db::get_rows($query);

	if ($results)
	{
		$conta = 0;
		$loop_count = count($results);
		foreach($results as $rs)
		{
			
			$selected = (isset($selected_ids[$level]) && ($selected_ids[$level] == $rs["pagina_id"]))?1:0;
			
			// els 3 links diferents que hi ha
			// de moment lo facil
			/*if ($level==1 && $rs['link']){
				$id = $rs['pagina_id'];
				$category_id=R::id('category_id');
				$tool = $GLOBALS['gl_tool'];
				$tool_section = $GLOBALS['gl_tool_section'];
				
				if ($tool=='bonet' && $tool_section=='agent' && $id=='15') $selected = 1;
				elseif ($tool=='news' && $tool_section=='new' && $category_id=='2' && $id=='17') $selected = 1;
				elseif ($tool=='contact' && $tool_section=='contact' && $id=='18') $selected = 1;
			}
			*/
			if ($rs['link']){
				$l['link'] = '/' . LANGUAGE . $rs['link'] . '/';// . $rs['pagina_file_name'] . '.htm';	
			}
			else{
				$l['link'] = 
			Page::get_link('bonet','pagina','show_record',$rs['pagina_id'],array(),false,$rs['pagina_file_name'],'');
			
			}			
			
			$l['selected'] = $selected;
			$l['item'] = $rs['pagina'];
			$l['title'] = $rs['title']; // si no hi ha title, el titol es el pagina ( com era al principi ) -> _block_bonet_pagina_set_top_vars
			$l['body_subtitle'] = $rs['body_subtitle'];
			$l['has_content'] = $rs['has_content'];
			$l['id'] = $rs['pagina_id'];
			$l['pagina_text_id'] = $rs['pagina_text_id'];
			$l['conta'] = $conta;
			$l['class'] = '';
			$l['loop_count'] = $loop_count;
			$l['loop'] = _block_bonet_pagina_get_children($rs['pagina_id'], $level+1, $selected_ids, $menu, $last_level);
			$conta++;
			$loop[] = $l;
		}
		
		$loop[0]['class'] = 'first';	
		$loop[$loop_count-1]['class'] = 'last';
		//$loop[] = array ('loop' => $loop2,
		//	'item' => '');
	}
	return $loop;
}
function _block_bonet_pagina_set_top_vars(&$loop, $selected_id){
	static $is_set_title = false;
	if ($is_set_title) return;
	$is_set_title = true;
	
	if (!$loop) return;
	foreach ($loop as $l){
		if ($l['id']==$selected_id) {
			$GLOBALS['gl_page']->top_title = $l['title']?$l['title']:$l['item'];// si no hi ha title, el titol es el page ( com era al principi )
			$GLOBALS['gl_page']->top_page_id = $l['id'];
			break;
		}
	}
	
}