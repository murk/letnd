<?

/* 4-10-16

	DIRECTORY

	UP-DATE
	- Afegir configuració

 */

if ( is_table( 'directory__configadmin' ) && ! Db::get_first( "SELECT * FROM directory__configadmin WHERE name='add_file_types'" ) ) {
	$update_sql[]= "
		INSERT INTO directory__configadmin (`name`, `value`) VALUES ('add_file_types', '');";

} else {
	if ( is_table( 'directory__configadmin' ) ) {
		trigger_error( 'Ja existeixen el camp add_file_types' );
	}
}

?>