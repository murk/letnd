<?

/* 14-10-14 
  ACtualitzacions inmo mapes

 */


if (is_table('inmo__property') && !is_field('inmo__property','center_latitude')) {
	$update_sql []="
		ALTER TABLE `inmo__property`
			ADD COLUMN `center_latitude` DECIMAL(9,7) NOT NULL AFTER `longitude`,
			ADD COLUMN `center_longitude` DECIMAL(9,7) NOT NULL AFTER `center_latitude`;";
	$update_sql []="
		UPDATE inmo__property SET `center_latitude`=latitude;";
	$update_sql []="
		UPDATE inmo__property SET `center_longitude`=longitude;";

}
elseif (is_table('inmo__property')){
	trigger_error('Ja existeixen el camp center_latitude');
}
?>