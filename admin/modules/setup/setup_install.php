<?

/**
 * SetupInstall
 *
 * @property string solution
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2014
 * @version $Id$
 * @access public
 */
class SetupInstall extends Module {
	public static $solution;

	function __construct() {
		parent::__construct();
	}

	public static function init() {
		if ( DEBUG ) {
			error_reporting( E_ALL );
			Db::$debug = true;
		}
		self::$solution = R::post( 'action' );

		if ( self::$solution ) {

			self::init_p();
			self::install();
			self::clean_tpls();
			self::clean_tables();

			if ( ! DEBUG ) Main::redirect( '/' );

			echo "<p></p>";
			self::p( 'INSTALAT!!!', 'success' );

		}
		else {
			$tpl       = New phemplate( DOCUMENT_ROOT );
			$tpl->file = 'admin/themes/inmotools/setup/install_form.tpl';
			echo $tpl->process();
		}
		html_end();
	}

	private static function delete_blocks( $solution ) {

		// borro blocks
		$results = Db::get_rows( "SELECT block_id FROM all__block WHERE solution='" . $solution . "'" );

		foreach ( $results as $rs ) {
			Db::execute( "DELETE FROM all__block WHERE block_id = '" . $rs['block_id'] . "'" );
			Db::execute( "DELETE FROM all__block_language WHERE block_id = '" . $rs['block_id'] . "'" );
		}
	}

	private static function delete_pages( $solution ) {

		// borro pages
		$results = Db::get_rows( "SELECT page_id FROM all__page WHERE solution='" . $solution . "'" );

		foreach ( $results as $rs ) {
			Db::execute( "DELETE FROM all__page WHERE page_id = '" . $rs['page_id'] . "'" );
			Db::execute( "DELETE FROM all__page_language WHERE page_id = '" . $rs['page_id'] . "'" );
		}
	}

	private static function clean_tpls() {

		$solution = self::$solution;
		self::p( "Netejant tpls", true );

		$tpls       = [
			'clean.tpl',
			'general.tpl',
			'jscripts/functions.js',
			'scss/styles.scss',
			'scss/_general.scss',
			'scss/_header.scss',
			'scss/_home.scss',
		];
		$m          = new Mustache_Engine;
		$client_dir = DOCUMENT_ROOT . $_SESSION['client_dir'] . '/templates/';

		$data = array(
			'is_product' => $solution == 'product',
			'is_inmo'    => $solution == 'inmo',
			'is_web'     => $solution == 'web',
			'solution'   => $solution
		);
		foreach ( $tpls as $tpl ) {
			$file = $client_dir . $tpl;
			$text = "{{=//{{ }}=}}" . file_get_contents( $file ); // canvio el delimitador dels templates
			self::p( $file, 0, 1 );
			$text = $m->render( $text, $data );
			file_put_contents( $file, $text );
		}
	}

	private static function clean_tables() {

		$solution = self::$solution;
		// obtinc taules i borro les que sobren
		$tables = Db::get_rows_array( 'SHOW tables' );

		self::p( "Esborrant taules d'altres eines", 2 );
		switch ( $solution ) {
			//
			// IMMO
			//
			case 'inmo':
				self::delete_blocks( 'product' );
				self::delete_pages( 'product' );
				foreach ( $tables as $rs ) {
					$table = $rs[0];
					if (
						strpos( $table, 'product__' ) === 0
					) {
						self::p( $table, 0, 1 );
						Db::execute( 'DROP TABLE ' . $table );
					}
				}

				// borro de all__menu
				Db::execute( "DELETE FROM all__menu WHERE tool = 'product'" );
				Db::execute( "DELETE FROM all__menu WHERE VARIABLE = 'WEB_MENU_CONFIGPRODUCT_ADMIN_LIST'" );
				Db::execute( "DELETE FROM all__menu WHERE VARIABLE = 'WEB_MENU_CONFIGPRODUCT_PUBLIC_LIST'" );

				// menu per defecte
				Db::execute( "UPDATE all__configadmin 
								SET `value`='103' 
								WHERE  `name`='default_menu_id';" );

				// taula de newsletter
				Db::execute( "UPDATE newsletter__configadmin
									SET `value`='custumer__custumer' 
									WHERE  `name`='custumer_table';" );

				break;

			//
			// PRODUCT
			//
			case 'product':
				self::delete_blocks( 'inmo' );
				self::delete_pages( 'inmo' );
				foreach ( $tables as $rs ) {
					$table = $rs[0];
					if (
						strpos( $table, 'inmo__' ) === 0
						|| strpos( $table, 'custumer__' ) === 0
						|| strpos( $table, 'print__' ) === 0
						|| strpos( $table, 'booking__' ) === 0
					) {
						self::p( $table, 0, 1 );
						Db::execute( 'DROP TABLE ' . $table );
					}
				}

				// borro de all__menu
				Db::execute( "DELETE FROM all__menu WHERE tool = 'custumer'" );
				Db::execute( "DELETE FROM all__menu WHERE tool = 'mls'" );
				Db::execute( "DELETE FROM all__menu WHERE tool = 'inmo'" );
				Db::execute( "DELETE FROM all__menu WHERE tool = 'booking'" );
				Db::execute( "DELETE FROM all__menu WHERE tool = 'print'" );
				Db::execute( "DELETE FROM all__menu WHERE tool = 'shopwindow'" );
				Db::execute( "DELETE FROM all__menu WHERE tool = 'utility'" );
				Db::execute( "DELETE FROM all__menu WHERE VARIABLE = 'WEB_MENU_CONFIGINMOADMIN_LIST'" );
				Db::execute( "DELETE FROM all__menu WHERE VARIABLE = 'WEB_MENU_CONFIGINMOPUBLIC_LIST'" );
				Db::execute( "DELETE FROM all__menu WHERE VARIABLE = 'WEB_MENU_CONFIGBOOKING_ADMIN_LIST'" );
				Db::execute( "DELETE FROM all__menu WHERE VARIABLE = 'WEB_MENU_CONFIGBOOKING_PUBLIC_LIST'" );
				Db::execute( "DELETE FROM all__menu WHERE VARIABLE = 'WEB_MENU_CONFIGCUSTUMER_LIST'" );

				// menu per defecte
				Db::execute( "UPDATE all__configadmin 
								SET `value`='20004' 
								WHERE  `name`='default_menu_id';" );

				// taula de newsletter
				Db::execute( "UPDATE newsletter__configadmin
									SET `value`='product__customer' 
									WHERE  `name`='custumer_table';" );

				break;

			//
			// WEB CORPORATIVA
			//
			case 'web':
				self::delete_blocks( 'product' );
				self::delete_blocks( 'inmo' );
				self::delete_pages( 'product' );
				self::delete_pages( 'inmo' );
				foreach ( $tables as $rs ) {
					$table = $rs[0];
					if (
						strpos( $table, 'product__' ) === 0
						|| strpos( $table, 'inmo__' ) === 0
						|| strpos( $table, 'custumer__' ) === 0
						|| strpos( $table, 'print__' ) === 0
						|| strpos( $table, 'booking__' ) === 0
					) {
						self::p( $table, 0, 1 );
						Db::execute( 'DROP TABLE ' . $table );
					}
				}

				// borro de all__menu
				Db::execute( "DELETE FROM all__menu WHERE tool = 'custumer'" );
				Db::execute( "DELETE FROM all__menu WHERE tool = 'mls'" );
				Db::execute( "DELETE FROM all__menu WHERE tool = 'inmo'" );
				Db::execute( "DELETE FROM all__menu WHERE tool = 'booking'" );
				Db::execute( "DELETE FROM all__menu WHERE tool = 'print'" );
				Db::execute( "DELETE FROM all__menu WHERE tool = 'product'" );
				Db::execute( "DELETE FROM all__menu WHERE tool = 'shopwindow'" );
				Db::execute( "DELETE FROM all__menu WHERE tool = 'utility'" );
				Db::execute( "DELETE FROM all__menu WHERE VARIABLE = 'WEB_MENU_CONFIGPRODUCT_ADMIN_LIST'" );
				Db::execute( "DELETE FROM all__menu WHERE VARIABLE = 'WEB_MENU_CONFIGPRODUCT_PUBLIC_LIST'" );
				Db::execute( "DELETE FROM all__menu WHERE VARIABLE = 'WEB_MENU_CONFIGINMOPUBLIC_LIST'" );
				Db::execute( "DELETE FROM all__menu WHERE VARIABLE = 'WEB_MENU_CONFIGINMOADMIN_LIST'" );
				Db::execute( "DELETE FROM all__menu WHERE VARIABLE = 'WEB_MENU_CONFIGBOOKING_ADMIN_LIST'" );
				Db::execute( "DELETE FROM all__menu WHERE VARIABLE = 'WEB_MENU_CONFIGBOOKING_PUBLIC_LIST'" );

				// menu per defecte
				Db::execute( "UPDATE all__configadmin 
								SET `value`='3003' 
								WHERE  `name`='default_menu_id';" );

				// taula de newsletter
				Db::execute( "UPDATE newsletter__configadmin
									SET `value`='newsletter__custumer' 
									WHERE  `name`='custumer_table';" );


				// desconecto newsletter
				Db::execute( "UPDATE all__menu
								SET `read1`=',1,',
								`write1`=',1,'
								WHERE  `menu_group`='newsletter';" );


				break;

			default:
				break;
		}

		// Comuns
		Db::execute( "TRUNCATE `missatge__missatge`;" );
	}

	private static function can_copy_file( $name ) {
		$exclude_files = [
			'inmo' => "product",
			'product' => "inmo|custumer|print",
			'web' => "product|inmo|custumer|print|newsletter",
		];
		$exclude = $exclude_files[ self::$solution ];
		$exclude = "($exclude|exclude)";

		return !preg_match( $exclude, $name );
	}

	public static function install() {
		$install_dir = DOCUMENT_ROOT . 'common/install';
		//////////////////////////////////
		// Primer copiem directori
		//////////////////////////////////

		if ( ! is_file( DOCUMENT_ROOT . 'index.php' ) ) {
			if ( copy( $install_dir . '/index.php', DOCUMENT_ROOT . 'index.php' ) ) {
				self::p( "CREAT INDEX.PHP", 1, 2 );
			}
			else
				self::p( "<font color='red'>Index.php no s'ha pogut copiar!</font><br>", 'error', 2 );

		}
		if ( ! is_dir( DOCUMENT_ROOT . 'clients' ) ) {
			if ( mkdir( DOCUMENT_ROOT . 'clients', 0777, true ) )
				self::p( '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CREAT DIRECTORI CLIENTS', 1 );
			else
				self::p( "<font color='red'>No s'ha pogut crear la carpeta de CLIENTS!</font><br>", 'error', 4 );
		}
		self::p( 'COPIANT DIRECTORIS', true );
		$copied_dirs = self::dircopy( $install_dir . '/AAA_plantilla_nou_client', DOCUMENT_ROOT . $_SESSION['client_dir'] );

		//////////////////////////////////
		// Despres copiem base de dades
		//////////////////////////////////
		$cn = new mysqli( "localhost", "root", $_SESSION['db_password'] );

		// creo DBs
		if ( ! $cn->select_db( $_SESSION['db_name'] ) ) {

			$query = "CREATE DATABASE `" . $_SESSION['db_name'] . "` CHARACTER SET utf8 COLLATE 'utf8_spanish2_ci'";
			$cn->query( $query ) or Db::error( $query );
		}
		// afegeixo contingut
		self::p( 'INSTALANT BASE DE DADES DE LETND', true );
		$cn->select_db( $_SESSION['db_name'] );
		self::execute_sql_file( $install_dir . '/nou_client.sql', $cn );

		$cn->close();
	}

	public static function execute_sql_file( $filename, $cn ) {

		$query = file_get_contents( $filename );


		if ( $cn->multi_query( $query ) ) {
			do {
				if ( $result = $cn->store_result() ) $result->free();
				if ( ! $cn->more_results() ) break;
			} while ( $cn->next_result() );
		}
	}

	private static function init_p() {
		echo "	

			<style>
			body {
				width:1000px;
				margin: 70px auto 0;
			}
			div {
				font-size:13px;
			}
			.debug_title_1{
				margin:30px 0 15px 0;
				padding:7px;
				font-size:18px;
				background-color:#ededeb;
			}
			.debug_title_2{
			    margin: 30px 0 10px 0;
			    font-size: 18px;
			    padding-left: 7px;
			}
			.debug_title_2 .red, .debug_title_1 .red{
				color:black;
			}
			.debug_title_error {
				color: red;
			}
			.debug_indent_1{
				padding-left: 20px;
			}
			.debug_indent_2{
				padding-left: 40px;
			}
			.debug_indent_3{
				padding-left: 60px;
			}
			.debug_indent_4{
				padding-left: 80px;
			}
			a.small{
				font-size:12px;
			}
			.debug_title_installing,
			.debug_title_success {
				position: absolute;
				top:0;
				left: 0;
				right: 0;
				background-color: #A7BF1A;
				color: white;
				padding:15px;
				font-size:22px;
				text-align: center;
				z-index:10;
			}
			.debug_title_installing {
				z-index:1;
				background-color: red;			
			}
			</style>";

		$solution_caption = [
			'inmo' => 'inmobiliaria',
			'product' => 'botiga',
			'web' => 'web corporativa'
		];
		$solution = self::$solution;

		self::p( 'Iniciant instal·lació ' . $solution_caption [$solution], 'installing' );

	}

	private static function p( $message, $is_title = false, $indent = 0 ) {
		$indent = $indent ? "debug_indent_$indent" : '';
		if ( $is_title ) {
			$message = "<div class=\"debug_title_$is_title $indent\">$message</div>";
		}
		else {
			$message = "<div class=\"$indent\">$message</div>";
		}
		echo $message;
	}


	/* Copies a dir to another. Optionally caching the dir/file structure, used to synchronize similar destination dir (web farm).
	*
	* @param $src_dir str Source directory to copy.
	* @param $dst_dir str Destination directory to copy to.
	* @param $verbose bool Show or hide file copied messages
	* @return Number of files copied/updated.
	* @example
	*     To copy a dir:
	*         dircopy("c:\max\pics", "d:\backups\max\pics");
	*
	*     To sync to web farms (webfarm 2 to 4 must have same dir/file structure (run once with cache off to make sure if necessary)):
	*        dircopy("//webfarm1/wwwroot", "//webfarm2/wwwroot", false, true);
	*        dircopy("//webfarm1/wwwroot", "//webfarm3/wwwroot", false, true);
	*        dircopy("//webfarm1/wwwroot", "//webfarm4/wwwroot", false, true);
	*/
	public static function dircopy( $src_dir, $dst_dir, $verbose = true ) {

		$num = 0;

		if ( ( $slash = substr( $src_dir, - 1 ) ) == "\\" || $slash == "/" )
			$src_dir = substr( $src_dir, 0, strlen( $src_dir ) - 1 );
		if ( ( $slash = substr( $dst_dir, - 1 ) ) == "\\" || $slash == "/" )
			$dst_dir = substr( $dst_dir, 0, strlen( $dst_dir ) - 1 );

		$src_tree       = self::get_dir_tree( $src_dir );
		$dst_tree = self::get_dir_tree( $dst_dir );

		if ( ! is_dir( $dst_dir ) )
			mkdir( $dst_dir, 0777 );
		if ( $verbose )
			self::p( "<b>directori: </b>$dst_dir", 0, 3 );

		foreach ( $src_tree as $file => $src_mtime ) {
			if (self::can_copy_file($file)) {
				if ( ! isset( $dst_tree[ $file ] ) && $src_mtime === false ) // dir

				{
					if ( $verbose )
						self::p( "<b>directori: </b>$dst_dir/$file", 0, 3 );
					mkdir( "$dst_dir/$file" );
				}
				elseif ( ! isset( $dst_tree[ $file ] ) && $src_mtime || isset( $dst_tree[ $file ] ) && $src_mtime > $dst_tree[ $file ] ) // file

				{
					if ( copy( "$src_dir/$file", "$dst_dir/$file" ) ) {
						if ( $verbose )
							self::p( "<b>arxiu: </b>$dst_dir/$file", 0, 4 );
						touch( "$dst_dir/$file", $src_mtime );
						$num ++;
					}
					else
						self::p( "<font color='red'>File '$src_dir/$file' no s'ha pogut copiar!</font><br>", 0, 4 );
				}
			}
		}

		return $num;
	}

	/* Creates a directory / file tree of a given root directory
	*
	* @param $dir str Directory or file without ending slash
	* @param $root bool Must be set to true on initial call to create new tree.
	* @return Directory & file in an associative array with file modified time as value.
	*/
	public static function get_dir_tree( $dir, $root = true ) {
		static $tree;
		static $base_dir_length;

		if ( $root ) {
			$tree            = array();
			$base_dir_length = strlen( $dir ) + 1;
		}

		if ( is_file( $dir ) ) {
			//if (substr($dir, -8) != "/CVS/Tag" && substr($dir, -9) != "/CVS/Root"  && substr($dir, -12) != "/CVS/Entries")
			$tree[ substr( $dir, $base_dir_length ) ] = filemtime( $dir );
		}
		elseif ( is_dir( $dir ) && $di = dir( $dir ) ) // add after is_dir condition to ignore CVS folders: && substr($dir, -4) != "/CVS"

		{
			if ( ! $root )
				$tree[ substr( $dir, $base_dir_length ) ] = false;
			while ( ( $file = $di->read() ) !== false ) {
				if ( $file != "." && $file != ".." )
					self::get_dir_tree( "$dir/$file", false );
			}
			$di->close();
		}

		if ( $root )
			return $tree;
	}
}