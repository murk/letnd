<?

/**
 * LassdiveCentre
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2011
 * @version $Id$
 * @access public
 */
class LassdiveCentre extends Module {

	private $query_users;
	private $query_admin;

	public function __construct() {
		parent::__construct();
	}

	public function on_load() {

		$this->query_users = LassdiveCommon::get_query_centre_users();

		$this->query_admin = LassdiveCommon::get_query_centre_admin();

		$this->set_field( 'admin_id', 'select_condition', $this->query_admin );
		$this->set_field( 'user_id', 'select_condition', $this->query_users );
		parent::on_load();

	}

	public function list_records() {
		$GLOBALS['gl_content'] = $this->get_records();
	}

	public function get_records() {

		// Hack per mostrar checkboxes_long al llistat
		$this->set_field( 'admin_id', 'type', 'checkboxes' );
		$this->set_field( 'user_id', 'type', 'checkboxes' );

		$listing           = new ListRecords( $this );
		$listing->has_bin  = false;
		$listing->order_by = 'centre ASC';

		return $listing->list_records();
	}

	public function show_form() {
		$GLOBALS['gl_content'] = $this->get_form();
	}

	public function get_form() {
		$show = new ShowForm( $this );

		$show->set_form_level1_titles('admin_id', 'centreproduct');

		return $show->show_form();
	}

	public function save_rows() {
		$save_rows = new SaveRows( $this );
		$save_rows->save();
	}

	public function write_record() {
		$writerec = new SaveRows( $this );
		$writerec->save();
	}

	public function manage_images() {
		$image_manager = new ImageManager( $this );
		$image_manager->execute();
	}

	// Llistat al carregar el formulari - utilitza la taula product__product_related
	public function get_sublist_html($id, $field, $is_input){

		$listing = NewRow::get_sublist_html($this, $id, $field, $is_input );

		$listing->group_fields = [ 'family_id' ];
		$listing->order_by     = 'family_id, subfamily_id, status asc, product_title ASC';

		$listing->list_records(false);

		/*foreach ( $listing->results as &$rs ) {
		}*/


		return $listing-> parse_template();

		// return $listing->list_records(); // si no necessitem fer el foreach
	}

	// Llistat al seleccionar un item per ajax - utilitza la taula lassdive__centreproduct
	public function get_sublist_item(){

		$listing = new NewRow($this);

		$product_id = $listing->selected_id;


		// altres camps relacionats
		$rs = array (

			'family_id' => Db::get_first("SELECT product__family_language.family FROM product__family_language, product__product WHERE product__family_language.family_id = product__product.family_id AND language =  '" . LANGUAGE . "' AND product__product.product_id = $product_id"),

			'subfamily_id' => Db::get_first("SELECT product__family_language.family FROM product__family_language, product__product WHERE product__family_language.family_id = product__product.subfamily_id AND language =  '" . LANGUAGE . "' AND product__product.product_id = $product_id"),

			'product_title' => Db::get_first("SELECT product__product_language.product_title FROM product__product_language WHERE language =  '" . LANGUAGE . "' AND product__product_language.product_id = $product_id"),

			'product_subtitle' => Db::get_first("SELECT product__product_language.product_subtitle FROM product__product_language WHERE language =  '" . LANGUAGE . "' AND product__product_language.product_id = $product_id"),

			'ref' => Db::get_first("SELECT product__product.ref FROM product__product WHERE product__product.product_id = $product_id"),

			'status' => Db::get_first("SELECT product__product.status FROM product__product WHERE product__product.product_id = $product_id"),

			'centre_max_quantity' => Db::get_first("SELECT product__product.max_quantity FROM product__product WHERE product__product.product_id = $product_id"),

			'quantityconcept_id' => Db::get_first("SELECT product__product.quantityconcept_id FROM product__product WHERE product__product.product_id = $product_id"),
		);

		$listing->add_row ($rs);
		$listing->new_row();
	}

	public function get_text_autocomplete_values() {
		{
			$limit = 100;

			$q = R::escape( 'query' );

			$ret                = array();
			$ret['suggestions'] = array();

			$r = &$ret['suggestions'];

			// 1 - Població, que comenci
			$query = "SELECT DISTINCT poblacio
					FROM lassdive__centre
					WHERE poblacio LIKE '$q%'
					ORDER BY poblacio
					LIMIT " . $limit;

			$results = Db::get_rows( $query );

			$rest = $limit - count( $results );

			// 2 - Població, qualsevol posició
			if ( $rest > 0 ) {

				$query = "SELECT DISTINCT poblacio
						FROM lassdive__centre
						WHERE poblacio LIKE '%$q%'
			            AND poblacio NOT LIKE '$q%'
						ORDER BY poblacio
						LIMIT " . $rest;

				$results2 = Db::get_rows( $query );

				$results = array_merge( $results, $results2 );

			}


			// Presento resultats
			foreach ( $results as $rs ) {
				$r[] = array(
					'value' => $rs['poblacio'],
					'data'  => ''
				);
			}


			$rest               = $limit - count( $results );
			$ret['is_complete'] = $rest > 0;

			Debug::p_all();
			die ( json_encode( $ret ) );


		}

	}
	public function get_checkboxes_long_values() {
		{

			$field = R::text_id( 'field' );

			if ( $field == 'centreproduct' ) {
				$this->get_checkboxes_results_products();
				return;
			}
			elseif ( $field == 'user_id' ) {
				$condition = $this->query_users;
				$user_type = 'user';
				$this->get_checkboxes_results_users( $condition, $user_type );
			}
			else {
				$condition = $this->query_admin;
				$user_type = 'admin';
				$this->get_checkboxes_results_users( $condition, $user_type );
			}

		}

	}

	private function get_checkboxes_results_products() {

		Main::load_class( 'lassdive', 'reserva_ajax' );
		LassdiveReservaAjax::get_checkboxes_results_products();

	}

	private function get_checkboxes_results_users( $condition, $user_type ) {

		Main::load_class( 'lassdive', 'reserva_ajax' );
		LassdiveReservaAjax::get_checkboxes_results_users( $condition, $user_type );

	}
}