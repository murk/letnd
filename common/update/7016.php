<?

/* 12-6-17
  Afegir pagament a booking

 */
if ( is_table( 'booking__book' ) && ! is_field( 'booking__book', 'error_code' ) ) {

	$update_sql[] = "ALTER TABLE `booking__book`
	CHANGE COLUMN `status` `status` ENUM('paying','failed','pending','denied','voided','refunded','completed','pre_booked','booked','confirmed_email','confirmed_post','contract_signed','cancelled') NOT NULL DEFAULT 'booked' COLLATE 'utf8_spanish2_ci' AFTER `contract_id`,
	ADD COLUMN `payment_method` ENUM('paypal','lacaixa') NOT NULL AFTER `status`,
	ADD COLUMN `error_code` VARCHAR(255) NOT NULL AFTER `payment_method`,
	ADD COLUMN `error_description` VARCHAR(255) NOT NULL AFTER `error_code`,
	ADD COLUMN `transaction_id` CHAR(17) NOT NULL AFTER `error_description`,
	ADD COLUMN `approval_code` CHAR(17) NOT NULL AFTER `transaction_id`;
";

} else {
	if ( is_table( 'booking__book' ) ) {
		trigger_error( 'Ja existeixen el camp error_code' );
	}
}
?>