<? if( GOOGLE_MAPS_KEY ): ?>
	<script type="text/javascript">
		 var gl_google_latitude = '<?=$google_latitude;?>';
		 var gl_google_longitude = '<?=$google_longitude;?>';
		 var gl_google_center_latitude = <?=$google_center_latitude;?>;
		 var gl_google_center_longitude = <?=$google_center_longitude;?>;
		 var gl_zoom = <?=$google_zoom;?>;
	</script>
<? endif // GOOGLE_MAPS_KEY ?>


<div id="form_sent">
	<?= $c_form_sent ?>
</div>
<div id="form_not_sent">
	<?= $c_form_not_sent ?>
</div>


<form class="form_area" action="<?= $link_action ?>" method="post" name="theForm" id="theForm" onsubmit="<?= $js_string ?>">
	<div>
		<?= $c_contacte_text_1 ?>
		<a href="mailto:<?= encode_email( $default_mail ) ?>"><?= encode_email( $default_mail ) ?></a>
		<?= $c_contacte_text_2 ?>
		<?= PAGE_PHONE ?>
	</div>

	<? /* AMB LABELS PER SIMULAR EL TEXT DINS L'INPUT */ ?>
	<label><strong><?= $c_name ?> *</strong><span><?= $name ?></span></label>
	<label><strong><?= $c_surnames ?> *</strong><span><?= $surnames ?></span></label>
	<label><strong><?= $c_mail ?> *</strong><span><?= $mail ?></span></label>
	<label><strong><?= $c_phone ?> *</strong><span><?= $phone ?></span></label>
	<label><strong><?= $c_company ?></strong><span><?= $company ?></span></label>
	<label><strong><?= $c_matter ?></strong><span><?= $matter ?></span></label>
	<label class="nofloat"><strong><?= $c_comment ?></strong><span><?= $comment ?></span></label>

	<?= get_google_recaptcha() ?>

	<div class="accepta">
		<div>
			<input class="accept-condicions" id="accepto" type="checkbox" name="accepto" value="1"/>
			<label for="accepto">
				<?= $c_politica_privacitat_text_1 ?>
				<a href="<?= get_page_link( 'politica' ) ?>?template=clean" data-iframe="true"><?= $c_politica_privacitat_text_2 ?></a>
			</label>
		</div>
		<div>
			<?= $accept_info ?>
			<label for="accept_info"><?= $c_accepto_informacio ?> <?= COMPANY_NAME ?></label>
		</div>
	</div>
	<? /* FI AMB LABELS PER SIMULAR EL TEXT DINS L'INPUT */ ?>


	<? /* AMB LABELS */ ?>
	<label class="left"><?= $c_name ?> *<br/> <?= $name ?></label>
	<label class="right"><?= $c_surnames ?> *<br/> <?= $surnames ?></label>
	<label class="left"><?= $c_phone ?> *<br/> <?= $phone ?></label>
	<label class="right"><?= $c_mail ?> *<br/> <?= $mail ?></label>
	<label class="left"><?= $c_matter ?><br/> <?= $matter ?></label>
	<label class="right"><?= $c_comment ?><br/> <?= $comment ?></label>

	<?= get_google_recaptcha() ?>

	<div class="accepta">
		<div>
			<input class="accept-condicions" id="accepto" type="checkbox" name="accepto" value="1"/>
			<label for="accepto">
				<?= $c_politica_privacitat_text_1 ?>
				<a href="<?= get_page_link( 'politica' ) ?>?template=clean" data-iframe="true"><?= $c_politica_privacitat_text_2 ?></a>
			</label>
		</div>
		<div>
			<?= $accept_info ?>
			<label for="accept_info"><?= $c_accepto_informacio ?> <?= COMPANY_NAME ?></label>
		</div>
	</div>
	<? /* FI AMB LABELS */ ?>

	<? /* O AMB TAULES */ ?>
	<table class="form_area">
		<tr>
			<th><label for="name_0"><?= $c_name ?> *:</label></th>
			<td><?= $name ?></td>
			<th><label for="matter_0"><?= $c_matter ?>:</label></th>
			<td class="no-margin"><?= $matter ?></td>
		</tr>
		<tr>
			<th><label for="surnames_0"><?= $c_surnames ?> *:</label></th>
			<td class="first"><?= $surnames ?></td>
			<td colspan="2" rowspan="3">
				<label class="no-margin" for="comment_0"><?= $c_comment ?></label>
				<?= $comment ?>
			</td>
		</tr>
		<tr>
			<th><label for="phone_0"><?= $c_phone ?> *:</label></th>
			<td><?= $phone ?></td>
		</tr>
		<tr>
			<th><label for="mail_0"><?= $c_mail ?> *:</label></th>
			<td class="first"><?= $mail ?></td>
		</tr>

		<? if ( has_google_recaptcha() ): ?>
		<tr>
			<td colspan="4">
				<?= get_google_recaptcha() ?>
			</td>
		</tr>
		<? endif // RECAPTCHA_KEY ?>
		<tr>
			<td colspan="4" class="accepta">
				<div>
					<input class="accept-condicions" id="accepto" type="checkbox" name="accepto" value="1"/>
					<label for="accepto">
						<?= $c_politica_privacitat_text_1 ?>
						<a href="<?= get_page_link( 'politica' ) ?>?template=clean" data-iframe="true"><?= $c_politica_privacitat_text_2 ?></a>
					</label>
				</div>
				<div>
					<?= $accept_info ?>
					<label for="accept_info"><?= $c_accepto_informacio ?> <?= COMPANY_NAME ?></label>
				</div>
			</td>
		</tr>
	</table>
	<? /* FI O AMB TAULES */ ?>

	<input name="contact-button" type="submit" id="contact-button" value="<?= $c_send ?>"/>

</form>

<address>

	<strong class="fn org"><?= COMPANY_NAME ?></strong>

	<p class="adr">
			<span class="street-address"><?= PAGE_ADDRESS ?> <?= PAGE_NUMSTREET ?><? if( PAGE_FLAT ): ?>,
					<?= PAGE_FLAT ?><? endif //  ?><? if( PAGE_DOOR && PAGE_FLAT ): ?>-<?= PAGE_DOOR ?><? endif //  ?></span>
		<br>
		<span class="postal-code"><?= PAGE_POSTCODE ?></span>
		<span class="locality"><?= PAGE_MUNICIPI ?></span> <br>
		<span class="region"><?= PAGE_PROVINCIA ?></span>,
		<span class="country-name"><?= PAGE_COUNTRY ?></span>
	</p>
	<p class="tel">
		<span class="type">Tel</span>. <span class="value"><?= PAGE_PHONE ?></span><? if( PAGE_MOBILE ): ?>,
			<span class="type">Mob</span>.
			<span class="value"><?= PAGE_MOBILE ?></span><? endif //PAGE_MOBILE?><? if( PAGE_FAX ): ?>,
			<span class="type">Fax</span>. <span class="value"><?= PAGE_FAX ?></span><? endif //FAX?>
	</p>

	<p>
		<a class="email" href="mailto:<?= encode_email( $default_mail ) ?>"><?= encode_email( $default_mail ) ?></a>
	</p>

</address>

<div id="google-map">
	<? if( !GOOGLE_MAPS_KEY ): ?>
		<? /*
            TODO-i Primer mirar de trobar el place_id  es troba aqui: https://developers.google.com/maps/documentation/javascript/examples/places-placeid-finder#try-it-yourself
                UPDATE `all__configpublic` SET `value`='place_id o coordenades si no hi ha place_id' WHERE  `name`='google_place_id';
        */ ?>
		<?= get_google_iframe() ?>
	<? endif // GOOGLE_MAPS_KEY ?>
</div>