# Dedal.io - letnd software base
Gestor multisite para servidores<br> 
Crea backend+backoffice dejando libre implementación pera front.

## Dependéncias:
  - Node v10.23.0 (npm v6.14.8)
  - Gulp Cli v2.3 / Local v3.9.1
  - Mysql 5.5 
  - Apache 2.4
  - PHP v5 (v7 migrando)
  
## Uso general:
  
#### 1. Editar hosts con linias: 
  - 127.0.0.1 &nbsp; &nbsp; &nbsp; &nbsp; w.nuevosite &nbsp; &nbsp; &nbsp; *debug*
  - 127.0.0.1 &nbsp; &nbsp; &nbsp; &nbsp; w2.nuevosite &nbsp;&nbsp;&nbsp; *client view*
      
#### 2. Al navegador -> w/w2.nuevosite
  - Tipo de web a escoger entre:
    - Web Corporativa
    - Tienda Online
    - Immobiliaria (nombrada _inmo_)

#### 3. letnd/clientes/nuevosite
  - Se habrá creado el árbol de directorio en _letnd/clients/*_ y db en local
  - Gulp para agilizar el desarrollo: 
    - A **_letnd/clients_** -> **_gulp --nuevosite** (_gulp css --nuevosite_ para aplicar CSS, **por defecto SASS**)
  - Todos los cambios de maquetación y diseño se hacen bajo el siguiente árbol de directorio:
    - _D:\\letnd/clients/nuevosite/templates/**_

#### 4. Próximos cambios / Esperados
  - MySQL v8
  - PHP v7.4
  - ~~Gulp~~ > Webpack
  - Node v14.15 (npm v6.14.8)
  - Vagrant -> **Sobre VMWare**
<br>
<br>
<br>
INFO:
<br>
marcdecline@gmail.com con sujeto: LETND - [seguido del sujeto]
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
