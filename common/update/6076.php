<?

/* 25-5-16

	Envio missatge a tots els clients
 */
if ( is_table( 'inmo__configadmin' ) && ! Db::get_first( "SELECT * FROM inmo__configadmin WHERE name='apicat_export_all'" ) ) {
	$update_sql[] = "
		INSERT INTO inmo__configadmin (`name`, `value`) VALUES ('apicat_export_all', '0');";

} else {
	if ( is_table( 'inmo__configadmin' ) ) {
		trigger_error( 'Ja existeixen el camp apicat_export_all' );
	}
}

?>