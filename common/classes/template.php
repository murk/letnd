<?php

class phemplate
{
    var $vars = array(); /// Holds all the template variables
    /**
     * loops container
     */
    var $loops = array();
    var $path; /// path to templates
    var $file; /// template
    var $module; /// enllaça al modul

	/**
	 * Constructor
	 *
	 * @param string $path the path to the templates
	 * @param string $file Plantilla, es pot definir despre´s amb {@link set_file()}
	 */
    function __construct($path = '',$file = '')
    {
        $this->path = $path;
		$this->file = $file;
        $this->vars = array();

        if ($file) Debug::add('Plantilla', $this->get_debug_vars($file));
    }

    /**
     * Set the template file
     *
     * @param string $file template file
     * @return void
     */
    function set_file($file, $clear = false)
    {
		if ($clear)	$this->vars = array();
		$this->file = $file;

    	Debug::add('Plantilla', $this->get_debug_vars($file));
    }

    private function get_debug_vars( $message ) {

		if (!DEBUG) return '';

        $path = $this->path;
        $file = $this->file;

        return Debug::get_clock_link($path . $file, 1, $message);

    }

    /**
     * Set a template variable.
     *
     * @param string $name name of the variable to set
     * @param mixed $value the value of the variable
     * @return void
     */
    function set_var($name, $value)
    {
        $this->vars[$name] = $value;
    }
    function get_var($name)
    {
        return $this->vars[$name];
    }

    /**
     * Set a bunch of variables at once using an associative array.
     *
     * @param array $vars array of vars to set
     * @param bool $clear whether to completely overwrite the existing vars
     * @return void
     */
    function set_vars($vars, $clear = false)
    {
        if ($clear)
        {
            $this->vars = $vars;
        }
        else
        {
            if (is_array($vars)) $this->vars = array_merge($this->vars, $vars);
        }
    }

    /**
	*	assign array to loop handle
	*	@param string $loop_name - name for a loop
	*	@param array $loop - loop data
     * @return void
     */
    function set_loop($loop_name, $loop)
    {
            $this->loops[$loop_name] = $loop;
    }

    /**
     * Open, parse, and return the template file.
     *
     * @param string $ string the template file name
     * @return string
     */
    function process()
    {
        extract($this->vars); // Extract the vars to local namespace
        extract($this->loops); // Extract the vars to local namespace
		//Debug::p($this->module, 'module tpl');
        ob_start(); // Start output buffering
        include($this->path . '/' . $this->file); // Include the file
        $contents = ob_get_contents(); // Get the contents of the buffer
        ob_end_clean(); // End buffering and discard
        return $contents; // Return the contents
    }
}
?>