current_contact_id = '';
function open_contact(contact_id, ref){

	if (current_contact_id == contact_id){
		$(oContact).slideToggle();
		return;
	}

	oContact = document.getElementById('contacte_'+contact_id);
	if (current_contact_id){
		fName = document.theForm['name[0]'].value;
		fSurnames = document.theForm['surnames[0]'].value;
		fPhone = document.theForm['phone[0]'].value;
		fMail = document.theForm['mail[0]'].value;
		oCurrent_contact = document.getElementById('contacte_'+current_contact_id);
		$(oCurrent_contact).height($(oCurrent_contact).height());
		oCurrent_contact.innerHTML = '';
		$(oCurrent_contact).slideUp(
			function(){}
			);
			
	}
	contacte_html = document.getElementById('contacte').contentWindow.document.getElementById('contacte').innerHTML;
	oContact.innerHTML =   '<div class="contacte_list">' + contacte_html + '</div><div class="sep_middle"></div>';
	document.theForm['matter[0]'].value='Ref. ' + ref;
	$('#name_0').focus();
	if (current_contact_id){
		document.theForm['name[0]'].value = fName;
		document.theForm['surnames[0]'].value = fSurnames;
		document.theForm['phone[0]'].value = fPhone;
		document.theForm['mail[0]'].value = fMail;
	}
	current_contact_id = contact_id;
	$(oContact).slideToggle();
	
	
	$("a[data-gal^='prettyPhoto']").prettyPhoto({
		theme: 'pp_default',
		social_tools:'',
		deeplinking: false,
		hook: 'data-gal',
		hideflash: true
		}
	);
	
}
function open_pretty (){
	$(".images_list_form a:first-child").click();
}

$(function() {

	initialize();
	
	$("a[data-gal^='prettyPhoto']").prettyPhoto({
		theme: 'pp_default',
		social_tools:'',
		deeplinking: false,
		hook: 'data-gal',
		hideflash: true
		}
	);


	var dur = 1800;
	var pDur = 3500;

	$('#header_middle_left div').carouFredSel({
		circular    : false,
		infinite    : true,
		auto        : true,
		swipe: true,
		items: {
			visible: 1,
			width: 389,
			height: 195
		},
		scroll: {
			fx: 'fade',
			easing: 'linear',
			duration: dur,
			timeoutDuration: pDur,
			onBefore: function( data ) {
				animate( data.items.visible );
			},
			onAfter: function( data ) {
				data.items.old.stop().css({
					marginTop: 0,
					marginLeft: 0
				});
			}
		},
		onCreate: function( data ) {
			animate( data.items );
		}
	});
	
	function animate( item, dur ) {
		var obj = {
			marginTop: -35,
			marginLeft: -11
		};
		setTimeout(
			function (){
				item.animate(obj, 2000, 'linear' );
			}
			, 800);		
	}

});



function initialize() {

	if (typeof gl_google_latitude == 'undefined') return;

    var myLatlng = new google.maps.LatLng(gl_google_latitude,gl_google_longitude);
    var myLatlng_center = new google.maps.LatLng(gl_google_center_latitude,gl_google_center_longitude);
    var myOptions = {
      zoom: gl_zoom,
      center: myLatlng_center,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      mapTypeControlOptions: {style: google.maps.MapTypeControlStyle.DROPDOWN_MENU}
    }

    var map = new google.maps.Map(document.getElementById("google-map"), myOptions);

	var image = new google.maps.MarkerImage(gl_dir_templates + 'images/marker.png',
			new google.maps.Size(64, 50),
			new google.maps.Point(0,0),
			new google.maps.Point(15, 47)
    );

    var marker = new google.maps.Marker({
        position: myLatlng,
        map: map,
		icon: image
        //title: gl_google_info_html
    });	

}