<?php
// menus eina
define( 'MISSATGE_MENU_NEW', 'Crear Missatge' );
define( 'MISSATGE_MENU_MISSATGE', 'Missatges' );
define( 'MISSATGE_MENU_LIST', 'Llistar Missatges' );
define( 'MISSATGE_MENU_BIN', 'Papelera Reciclatge' );

// captions seccio
$gl_caption_missatge['c_status']          = 'Estat';
$gl_caption_missatge['c_subject']         = 'Assumpte';
$gl_caption_missatge['c_missatge']        = 'Missatge';
$gl_caption_missatge['c_blocked']         = '';
$gl_caption_missatge['c_bin']             = '';
$gl_caption_missatge['c_mls_from_id']     = 'De';
$gl_caption_missatge['c_mls_to_id']       = 'Per';
$gl_caption_missatge['c_creator_user_id'] = 'De';

$gl_caption_missatge['c_status_prepare'] = 'En preparació';
$gl_caption_missatge['c_status_waiting'] = 'Per llegir';
$gl_caption_missatge['c_status_readed']  = 'Llegit';
$gl_caption_missatge['c_status_send']    = 'Enviat';

$gl_caption_missatge ['c_kind']         = 'Tipus';
$gl_caption_missatge ['c_kind_mls']     = 'Mls';
$gl_caption_missatge ['c_kind_general'] = 'General';

$gl_caption_missatge ['c_entered']  = 'Data creació';
$gl_caption_missatge ['c_modified'] = 'Data modificació';
$gl_caption_missatge ['c_sent']     = 'Enviat';

$gl_caption['c_user_id']       = "O usuari";
$gl_caption_list['c_user_id']  = "Usuaris";
$gl_caption['c_group_id']      = "Grup";
$gl_caption_list['c_group_id'] = "Grup";

$gl_caption['c_form_level1_title_group_id'] = "Enviar a:";
$gl_caption['c_form_level1_title_subject']  = "Missatge";

$gl_caption['c_reply_button'] = $gl_caption['c_reply'] ;
?>
                  

