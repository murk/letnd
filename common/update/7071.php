<?php
/* 19-2-19
Afegeixo cercar paraules als filtres de productes


*/
if ( is_table( 'product__configpublic' ) && ! Db::get_first( "SELECT * FROM product__configpublic WHERE name='search_fields'" ) ) {
	$update_sql[] = "
		INSERT INTO product__configpublic (`name`, `value`) VALUES ('search_fields', 'product_title,product_subtitle,others1,others2,others3,others4,others5,others6,others7,others8,product_description');";

}
else {
	if ( is_table( 'product__configpublic' ) ) {
		trigger_error( 'Ja existeixen el camp search_fields' );
	}
}

