<?php

if (!defined('TITLE_COMARCA')){
	define('TITLE_COMARCA', 'Propiedades en: ');
	define('TITLE_PRICE', 'Propiedades hasta %s '.CURRENCY_NAME);
	define('TITLE_PRICE_2', 'Propiedades de %s a %s '.CURRENCY_NAME);
	define('TITLE_PRICE_3', 'Propiedades de más de %s '.CURRENCY_NAME);

	define('TITLE_FLOORSPACE', 'Propiedades hasta %s m<sup>2</sup>');
	define('TITLE_FLOORSPACE_2', 'Propiedades de %s a %s m<sup>2</sup>');
	define('TITLE_FLOORSPACE_3', 'Propiedades de más de %s m<sup>2</sup>');

	define('TITLE_PERSON', 'Hasta %s personas');
	define('TITLE_PERSON_2', 'De %s a %s personas');
	define('TITLE_PERSON_3', 'Más de %s personas');
}

$gl_caption['c_filter_price_min'] = 'Precio min.';
$gl_caption['c_filter_price_max'] = 'Precio max.';
$gl_caption['c_filter_price_min_temp'] = 'Precio min. / noche';
$gl_caption['c_filter_price_max_temp'] = 'Precio max. / noche';

$gl_caption['c_recent'] = 'Nuevo';

// NO TRADUIT
$gl_caption['c_property_list__view_all'] = 'Ver todos los inmuebles visitados';
$gl_caption['c_characteristics'] = 'Características';
// FI NO TRADUIT

$gl_caption['c_booking_close_results_message'] = 'No se ha encontrado ningún inmueble para las fechas seleccionadas, los siguientes inmuebles se pueden reservar en fechas similares';
?>