<?
/**
 * LassdiveFamily
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2011
 * @version $Id$
 * @access public
 */
class LassdiveFamily extends Module{

	function __construct(){
		parent::__construct();
	}
	function show_calendar()
	{
		$this->_execute_module($this->action);
	}
	function save_has_calendar()
	{
		$this->_execute_module($this->action);
	}
	function _execute_module($action=''){
		// $this->name = 'instalacio'; // si vull canviar el nom del parent
		$module = Module::load('product','family', $this);
		$action = $action?$action:$this->action;
		return $module->do_action($action);	
	}
}
?>