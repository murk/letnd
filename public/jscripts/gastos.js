function Validacion(valor,idioma)
{
	
	if(idioma == null){
		idioma = 'e';
	}
	
	if(idioma=='c'){
		var me1 = "És obligatori omplir el camp 'Interés'";
		var me2 = "És obligatorio omplir el camp 'Anys'";
		var me3 = "És obligatorio omplir el camp 'Import'";
		var me4 = "resultats";
		var me5 = "Si us plau, informi algun dels imports";
	}
	
	if(idioma=='i'){
		var me1 = "Insert value for 'Interest'";
		var me2 = "Insert value for 'Years'";
		var me3 = "Insert value for 'Import'";
		var me4 = "results";
		var me5 = "Please insert someone of the amounts";
	}
	
	if(idioma=='f'){
		var me1 = "Introduire une valeur dans 'Intérêt'";
		var me2 = "Introduire une valeur dans 'Années'";
		var me3 = "Introduire une valeur dans 'Montant'";
		var me4 = "résultats";
		var me5 = "S'il vous plaît remplissez un des montants";
	}
	
	if(idioma=='a'){
		var me1 = "Insert value for 'Interest'";
		var me2 = "Insert value for 'Years'";
		var me3 = "Insert value for 'Import'";
		var me4 = "results";
		var me5 = "Please insert someone of the amounts";
	}
	
	if(idioma=='t'){
		var me1 = "Insert value for 'Interest'";
		var me2 = "Insert value for 'Years'";
		var me3 = "Insert value for 'Import'";
		var me4 = "results";
		var me5 = "Please insert someone of the amounts";
	}
	
	if(idioma=='e'){
		var me1 = "Es obligatorio rellenar el campo 'Interés'";
		var me2 = "Es obligatorio rellenar el campo 'Años'";
		var me3 = "Es obligatorio rellenar el campo 'Importe'";
		var me4 = "resultados";
		var me5 = "Por favor rellene alguno de los importes";
	}
	
	if (document.hipoteca.i1.value=="")
	{
		window.alert(me1); //me1
	}
	else if(document.hipoteca.anos.value=="")
	{
		window.alert(me2); //me2
	}
	else if(document.formu.importeh.value=="")
	{
		window.alert(me3); //me3
	}
	else

	{
	 if (valor=="x")
	 {
		if (document.hipoteca.MinApert.value=="")
		{
			document.hipoteca.MinApert.value=0
		}
		if (document.hipoteca.Comision.value=="")
		{
			document.hipoteca.Comision.value=0
		}
		document.hipoteca.result.value = me4; //me4
	 }
	 document.hipoteca.submit();
	}
}

function CleanThis(Tipo)
{
	if (Tipo=="dinero")
		document.formu.importeh.value="";
	else if(Tipo=="Apertura")
		document.hipoteca.MinApert.value="";
}


function euro(idioma) {
	
	if(idioma == null){
	idioma = 'e';
	}
	
	if(idioma =='c'){
		var me14 = "Reajust del valor en euros!";
	}
	
	if(idioma =='i'){
		var me14 = "Readjustment of the value in euros!";
	}
	
	if(idioma =='f'){
		var me14 = "¡Un rajustement de la valeur dans des euros!";
	}
	
	if(idioma =='a'){
		var me14 = "Readjustment of the value in euros!";
	}
	
	if(idioma =='t'){
		var me14 = "Readjustment of the value in euros!";
	}
	
	if(idioma =='e'){
		var me14 = "¡Reajuste del valor en euros!";
	}
	
	if (!(document.formu.importeh.value == "" && document.formu.valorh.value == "" )){
	var valor_actual;
	var res;
	var valor1;
	var valor2;
	var flag_aviso;

		valor1=Delete_miles(document.formu.importeh.value);
		valor2=Delete_miles(document.formu.valorh.value);

    if (!(document.formu.importeh.value == ""))
    {
  	  valor_actual = valor1 / 166.386;
	  document.formu.importeh.value = Math.round(valor_actual);
	  document.formu.importeh.value = Formato_numero(document.formu.importeh.value);
	  valor_actual = 0;
	  calcular_onclick(idioma);
	  if(!(flag_aviso==1)){
	  	alert (me14); //me14
	  };
	  flag_aviso = 1;
	}

    if (!(document.formu.valorh.value == ""))
    {
	  valor_actual = valor2 / 166.386;
	  document.formu.valorh.value = Math.round(valor_actual);
	  document.formu.valorh.value = Formato_numero(document.formu.valorh.value);
	  calcular_onclick(idioma);
	  if(!(flag_aviso==1)){
	  	alert (me14); //me14
	  };
	  flag_aviso = 1;
	}
	}
}

function pts(idioma) {
	
	if(idioma == null){
	idioma = 'e';
	}
	
	if(idioma =='c'){
		var me14 = "Reajust del valor en pessetes!";
	}
	
	if(idioma =='i'){
		var me14 = "Readjustment of the value in pesetas!";
	}
	
	if(idioma =='f'){
		var me14 = "¡Un rajustement de la valeur dans des pesetas!";
	}
	
	if(idioma =='a'){
		var me14 = "Readjustment of the value in pesetas!";
	}
	
	if(idioma =='t'){
		var me14 = "Readjustment of the value in pesetas!";
	}
	
	if(idioma =='e'){
		var me14 = "¡Reajuste del valor en pesetas!";
	}
	
	if (!(document.formu.importeh.value == "" && document.formu.valorh.value == "" )){
	var valor_actual;
	var res;
	var valor1;
	var valor2;
	var flag_aviso;

		valor1=Delete_miles(document.formu.importeh.value);
		valor2=Delete_miles(document.formu.valorh.value);


    if (!(document.formu.importeh.value == ""))
    {
	  valor_actual = valor1 * 166.386;
	  document.formu.importeh.value = Math.round(valor_actual);
	  document.formu.importeh.value = Formato_numero(document.formu.importeh.value);
	  valor_actual = 0;
	  calcular_onclick(idioma);
	  if(!(flag_aviso==1)){
	  	alert (me14); //me14
	  };
	  flag_aviso = 1;
	}

    if (!(document.formu.valorh.value == ""))
    {
  	  valor_actual = valor2 * 166.386;
	  document.formu.valorh.value = Math.round(valor_actual);
	  document.formu.valorh.value = Formato_numero(document.formu.valorh.value);
	  calcular_onclick(idioma);
	  if(!(flag_aviso==1)){
	  	alert (me14); //me14
	  };
	  flag_aviso = 1;
	}
	}
}

// ********************************************************
// ********************************************************
// ***   calcular_onclick()
// ********************************************************
// ********************************************************

function calcular_onclick(idioma) {
	
		if(idioma == null){
		idioma = 'e';
	}
	
	if(idioma=='c'){
		var me1 = "És obligatori omplir el camp 'Interés'";
		var me2 = "És obligatorio omplir el camp 'Anys'";
		var me3 = "És obligatorio omplir el camp 'Import'";
		var me4 = "resultats";
		var me5 = "Si us plau, informi algun dels imports";
	}
	
	if(idioma=='i'){
		var me1 = "Insert value for 'Interest'";
		var me2 = "Insert value for 'Years'";
		var me3 = "Insert value for 'Import'";
		var me4 = "results";
		var me5 = "Please insert someone of the amounts";
	}
	
	if(idioma=='f'){
		var me1 = "Introduire une valeur dans 'Intérêt'";
		var me2 = "Introduire une valeur dans 'Années'";
		var me3 = "Introduire une valeur dans 'Montant'";
		var me4 = "résultats";
		var me5 = "S'il vous plaît remplissez un des montants";
	}
	
	if(idioma=='a'){
		var me1 = "Insert value for 'Interest'";
		var me2 = "Insert value for 'Years'";
		var me3 = "Insert value for 'Import'";
		var me4 = "results";
		var me5 = "Please insert someone of the amounts";
	}
	
	if(idioma=='t'){
		var me1 = "Insert value for 'Interest'";
		var me2 = "Insert value for 'Years'";
		var me3 = "Insert value for 'Import'";
		var me4 = "results";
		var me5 = "Please insert someone of the amounts";
	}
	
	if(idioma=='e'){
		var me1 = "Es obligatorio rellenar el campo 'Interés'";
		var me2 = "Es obligatorio rellenar el campo 'Años'";
		var me3 = "Es obligatorio rellenar el campo 'Importe'";
		var me4 = "resultados";
		var me5 = "Por favor rellene alguno de los importes";
	}
	
	var valor1;
	var valor2;
	var provincia;
	
	valor1=Delete_miles(document.formu.importeh.value);
	valor2=Delete_miles(document.formu.valorh.value);
	provincia = document.formu.provinciah.value;
	
	// ********************************************************
	// *** SI ESTAN LOS DOS CAMPOS RELLENOS
	if (!(document.formu.importeh.value == "") && !(document.formu.valorh.value == "" )){		
		var suma;

        buscarvalor(valor1, valor2);
        
        if (document.formu.radioviv[0].checked)
        {// SI ES OBRA NUEVA
  		  //document.f_result.ajd.value = valor2 * 0.005;
		  document.f_result.ivaviv.value = valor2 * 0.07;
		  document.f_result.itp.value = "";	
		  
			if (document.formu.provinciah.options[document.formu.provinciah.selectedIndex].text.toLowerCase() == 'extremadura') // arreglo para extremadura solo. 22/01/2003
			{
				document.f_result.impuesto.value = valor1 * 0.01;
				document.f_result.impuesto2.value = valor2 * 0.01;
			}else{
				  
			  document.f_result.impuesto.value = valor1 * 0.005;
			  document.f_result.impuesto2.value = valor2 * 0.005;	
			  
			}	  
		
		}else{// SI ES SEGUNDA MANO
		  
			if (document.formu.provinciah.options[document.formu.provinciah.selectedIndex].text.toLowerCase() == 'extremadura'){ // arreglo para extremadura solo. 22/01/2003
				document.f_result.impuesto.value = valor1 * 0.01;
			}else{
				document.f_result.impuesto.value = valor1 * 0.008;
			}
				//document.f_result.itp.value = valor2 * 0.07;
				document.f_result.itp.value = valor2 * provincia;
				//document.f_result.ajd.value = "";
				document.f_result.ivaviv.value = "";
				document.f_result.impuesto2.value = "";
		}

		document.f_result.comision.value = valor1 * 0.01; // Comisión
		//document.f_result.impuesto.value = valor1 * 0.0106;		

		// SI SON PESETAS
		if (document.formu.radio[0].checked){
			document.f_result.tasacion.value = 30000;
			document.f_result.seguro.value = 50000;
            if (document.formu.radioviv[0].checked)
            {// SI ES OBRA NUEVA
  			  suma = parseInt(document.f_result.impuesto2.value)+parseInt(document.f_result.seguro.value) + parseInt(document.f_result.comision.value) + parseInt(document.f_result.iva.value) + parseInt(document.f_result.tasacion.value) + parseInt(document.f_result.impuesto.value) + parseInt(document.f_result.notario.value) + parseInt(document.f_result.gestion.value) + parseInt(document.f_result.registro.value) + parseInt(document.f_result.notariocv.value) + parseInt(document.f_result.registrocv.value) + parseInt(document.f_result.gestioncv.value) + parseInt(document.f_result.ivacv.value) + parseInt(document.f_result.ivaviv.value);
  			  suma_hipo = parseInt(document.f_result.seguro.value) + parseInt(document.f_result.comision.value) + parseInt(document.f_result.iva.value) + parseInt(document.f_result.tasacion.value) + parseInt(document.f_result.impuesto.value) + parseInt(document.f_result.notario.value) + parseInt(document.f_result.gestion.value) + parseInt(document.f_result.registro.value);
  			  suma_compra = parseInt(document.f_result.notariocv.value) + parseInt(document.f_result.impuesto2.value)+ parseInt(document.f_result.registrocv.value) + parseInt(document.f_result.gestioncv.value) + parseInt(document.f_result.ivacv.value) + parseInt(document.f_result.ivaviv.value);
            }
            else
            {// SI ES SEGUNDA MANO
  			  suma = parseInt(document.f_result.seguro.value) + parseInt(document.f_result.comision.value) + parseInt(document.f_result.iva.value) + parseInt(document.f_result.tasacion.value) + parseInt(document.f_result.impuesto.value) + parseInt(document.f_result.notario.value) + parseInt(document.f_result.gestion.value) + parseInt(document.f_result.registro.value) + parseInt(document.f_result.notariocv.value) + parseInt(document.f_result.registrocv.value) + parseInt(document.f_result.gestioncv.value) + parseInt(document.f_result.ivacv.value) + parseInt(document.f_result.itp.value);
  			  suma_hipo = parseInt(document.f_result.seguro.value) + parseInt(document.f_result.comision.value) + parseInt(document.f_result.iva.value) + parseInt(document.f_result.tasacion.value) + parseInt(document.f_result.impuesto.value) + parseInt(document.f_result.notario.value) + parseInt(document.f_result.gestion.value) + parseInt(document.f_result.registro.value);
  			  suma_compra = parseInt(document.f_result.notariocv.value) + parseInt(document.f_result.registrocv.value) + parseInt(document.f_result.gestioncv.value) + parseInt(document.f_result.ivacv.value) + parseInt(document.f_result.itp.value);
            }
			document.f_result.seguro.value = Formato_numero(document.f_result.seguro.value);
			document.f_result.tasacion.value = Formato_numero(document.f_result.tasacion.value);
			document.f_result.total.value = Formato_numero(suma);
			document.f_result.comision.value = Formato_numero(Math.round(document.f_result.comision.value));
			document.f_result.iva.value = Formato_numero(document.f_result.iva.value);
			document.f_result.tasacion.value = Formato_numero(document.f_result.tasacion.value);
			document.f_result.impuesto.value = Formato_numero(Math.round(document.f_result.impuesto.value));
			document.f_result.notario.value = Formato_numero(document.f_result.notario.value);
			document.f_result.gestion.value = Formato_numero(document.f_result.gestion.value);
			document.f_result.registro.value = Formato_numero(document.f_result.registro.value);
			document.f_result.notariocv.value = Formato_numero(document.f_result.notariocv.value);
			document.f_result.registrocv.value = Formato_numero(document.f_result.registrocv.value);
			document.f_result.ivacv.value = Formato_numero(document.f_result.ivacv.value);
			document.f_result.gestioncv.value = Formato_numero(document.f_result.gestioncv.value);
			document.f_result.total_compra.value = Formato_numero(suma_compra);
			document.f_result.total_hipo.value = Formato_numero(suma_hipo);
            if (document.formu.radioviv[0].checked)
            {// SI ES OBRA NUEVA
         		  //document.f_result.ajd.value = Formato_numero(Math.round(document.f_result.ajd.value));
	            document.f_result.ivaviv.value = Formato_numero(Math.round(document.f_result.ivaviv.value));
	            document.f_result.impuesto2.value = Formato_numero(Math.round(document.f_result.impuesto2.value));
	        }
	        else
	        {// SI ES SEGUNDA MANO
     	        document.f_result.itp.value = Formato_numero(Math.round(document.f_result.itp.value));
	        }
		}
		else
		{// SI SON EUROS			
			//document.f_result.comision.value = document.f_result.comision.value / 166.386;
			document.f_result.seguro.value = 300;
			document.f_result.iva.value = document.f_result.iva.value / 166.386;
			//document.f_result.tasacion.value = document.f_result.tasacion.value / 166.386;
			//document.f_result.tasacion.value = 180; 24-04-2007
			document.f_result.tasacion.value = 250;
			//document.f_result.impuesto.value = document.f_result.impuesto.value / 166.386;
			document.f_result.notario.value = document.f_result.notario.value / 166.386;
			document.f_result.gestion.value = document.f_result.gestion.value / 166.386;
			document.f_result.registro.value = document.f_result.registro.value / 166.386;
			document.f_result.notariocv.value = document.f_result.notariocv.value / 166.386;
			document.f_result.registrocv.value = document.f_result.registrocv.value / 166.386;
			document.f_result.ivacv.value = document.f_result.ivacv.value / 166.386;
			document.f_result.gestioncv.value = document.f_result.gestioncv.value / 166.386;			
            if (document.formu.radioviv[0].checked)
            {// SI ES OBRA NUEVA
   		      suma = parseInt(document.f_result.impuesto2.value) + parseInt(document.f_result.seguro.value) + parseInt(document.f_result.comision.value) + parseInt(document.f_result.iva.value) + parseInt(document.f_result.tasacion.value) + parseInt(document.f_result.impuesto.value) + parseInt(document.f_result.notario.value) + parseInt(document.f_result.gestion.value) + parseInt(document.f_result.registro.value) + parseInt(document.f_result.notariocv.value) + parseInt(document.f_result.registrocv.value) + parseInt(document.f_result.gestioncv.value) + parseInt(document.f_result.ivacv.value) + parseInt(document.f_result.ivaviv.value);
  			  suma_hipo = parseInt(document.f_result.seguro.value) + parseInt(document.f_result.comision.value) + parseInt(document.f_result.iva.value) + parseInt(document.f_result.tasacion.value) + parseInt(document.f_result.impuesto.value) + parseInt(document.f_result.notario.value) + parseInt(document.f_result.gestion.value) + parseInt(document.f_result.registro.value);
  			  suma_compra = parseInt(document.f_result.notariocv.value) + parseInt(document.f_result.impuesto2.value) + parseInt(document.f_result.registrocv.value) + parseInt(document.f_result.gestioncv.value) + parseInt(document.f_result.ivacv.value) + parseInt(document.f_result.ivaviv.value);
            }
            else
            {// SI ES SEGUNDA MANO
              suma = parseInt(document.f_result.seguro.value) + parseInt(document.f_result.comision.value) + parseInt(document.f_result.iva.value) + parseInt(document.f_result.tasacion.value) + parseInt(document.f_result.impuesto.value) + parseInt(document.f_result.notario.value) + parseInt(document.f_result.gestion.value) + parseInt(document.f_result.registro.value) + parseInt(document.f_result.notariocv.value) + parseInt(document.f_result.registrocv.value) + parseInt(document.f_result.gestioncv.value) + parseInt(document.f_result.ivacv.value) + parseInt(document.f_result.itp.value);
  			  suma_hipo = parseInt(document.f_result.seguro.value) + parseInt(document.f_result.comision.value) + parseInt(document.f_result.iva.value) + parseInt(document.f_result.tasacion.value) + parseInt(document.f_result.impuesto.value) + parseInt(document.f_result.notario.value) + parseInt(document.f_result.gestion.value) + parseInt(document.f_result.registro.value);
  			  suma_compra = parseInt(document.f_result.notariocv.value) + parseInt(document.f_result.registrocv.value) + parseInt(document.f_result.gestioncv.value) + parseInt(document.f_result.ivacv.value) + parseInt(document.f_result.itp.value);
            }

		    // fin de calculos para euros
		    //comienzo de formateo
			document.f_result.total.value = Formato_numero(Math.round(suma));
			document.f_result.comision.value = Formato_numero(Math.round(document.f_result.comision.value));
			document.f_result.iva.value = Formato_numero(Math.round(document.f_result.iva.value));
			document.f_result.tasacion.value = Formato_numero(Math.round(document.f_result.tasacion.value));
			document.f_result.impuesto.value = Formato_numero(Math.round(document.f_result.impuesto.value));
			document.f_result.notario.value = Formato_numero(Math.round(document.f_result.notario.value));
			document.f_result.gestion.value = Formato_numero(Math.round(document.f_result.gestion.value));
			document.f_result.registro.value = Formato_numero(Math.round(document.f_result.registro.value));
			document.f_result.notariocv.value = Formato_numero(Math.round(document.f_result.notariocv.value));
			document.f_result.registrocv.value = Formato_numero(Math.round(document.f_result.registrocv.value));
			document.f_result.ivacv.value = Formato_numero(Math.round(document.f_result.ivacv.value));
			document.f_result.gestioncv.value = Formato_numero(Math.round(document.f_result.gestioncv.value));
			document.f_result.total_compra.value = Formato_numero(Math.round(suma_compra));
			document.f_result.total_hipo.value = Formato_numero(Math.round(suma_hipo));
            if (document.formu.radioviv[0].checked)
            {// SI ES OBRA NUEVA
        	//	document.f_result.ajd.value = Formato_numero(Math.round(document.f_result.ajd.value));
    	    	document.f_result.ivaviv.value = Formato_numero(Math.round(document.f_result.ivaviv.value));
    	    	document.f_result.impuesto2.value = Formato_numero(Math.round(document.f_result.impuesto2.value));
    	    }
    	    else
    	    {// SI ES SEGUNDA MANO
     	    	document.f_result.itp.value = Formato_numero(Math.round(document.f_result.itp.value));

    	    }
		}
	}//SI ESTAN LOS DOS CAMPOS RELLENOS

	// ********************************************************
	// *** SI ESTA RELLENO SOLO EL PRIMERO (IMPORTE SOLICITADO)	
	if (!(document.formu.importeh.value == "") && (document.formu.valorh.value == "")){
		var suma;
		valor2 = 0;
        buscarvalor(valor1, valor2);

		document.f_result.comision.value = valor1 * 0.01;
		// document.f_result.impuesto.value = valor1 * 0.0106;
		document.f_result.impuesto.value = valor1 * 0.005;

		// SI SON PESETAS
		if (document.formu.radio[0].checked){
			document.f_result.tasacion.value = 30000;
			document.f_result.seguro.value = 50000;

  			    suma_hipo = parseInt(document.f_result.seguro.value) + parseInt(document.f_result.comision.value) + parseInt(document.f_result.iva.value) + parseInt(document.f_result.tasacion.value) + parseInt(document.f_result.impuesto.value) + parseInt(document.f_result.notario.value) + parseInt(document.f_result.gestion.value) + parseInt(document.f_result.registro.value);
  			    suma_compra = "";
  			    suma = suma_hipo;
			
			document.f_result.seguro.value = Formato_numero(document.f_result.seguro.value);
			document.f_result.tasacion.value = Formato_numero(document.f_result.tasacion.value);
			document.f_result.total.value = Formato_numero(suma);
			document.f_result.comision.value = Formato_numero(Math.round(document.f_result.comision.value));
			document.f_result.iva.value = Formato_numero(document.f_result.iva.value);
			document.f_result.tasacion.value = Formato_numero(document.f_result.tasacion.value);
			document.f_result.impuesto.value = Formato_numero(Math.round(document.f_result.impuesto.value));			
			document.f_result.notario.value = Formato_numero(document.f_result.notario.value);			
			document.f_result.gestion.value = Formato_numero(document.f_result.gestion.value);
			document.f_result.registro.value = Formato_numero(document.f_result.registro.value);
			document.f_result.notariocv.value = "";
			document.f_result.registrocv.value = "";
			document.f_result.ivacv.value = "";			
			document.f_result.gestioncv.value = "";
			document.f_result.total_compra.value = "";
			document.f_result.total_hipo.value = Formato_numero(suma_hipo);
			//document.f_result.total_hipo.value = Formato_numero(document.f_result.total_hipo.value);
			}
		else
			{// SI SON EUROS			
			//document.f_result.comision.value = document.f_result.comision.value / 166.386;
			document.f_result.seguro.value = 300;
			document.f_result.iva.value = document.f_result.iva.value / 166.386;
			document.f_result.tasacion.value = document.f_result.tasacion.value / 166.386;
			//document.f_result.tasacion.value = 180; 24-04-2007
			document.f_result.tasacion.value = 250;
			//document.f_result.impuesto.value = document.f_result.impuesto.value / 166.386;
			document.f_result.notario.value = document.f_result.notario.value / 166.386;
			document.f_result.gestion.value = document.f_result.gestion.value / 166.386;
			document.f_result.registro.value = document.f_result.registro.value / 166.386;
						
            var suma_hipo;
			var suma_compra;
			
			suma_hipo = parseInt(document.f_result.seguro.value) + parseInt(document.f_result.comision.value) + parseInt(document.f_result.iva.value) + parseInt(document.f_result.tasacion.value) + parseInt(document.f_result.impuesto.value) + parseInt(document.f_result.notario.value) + parseInt(document.f_result.gestion.value) + parseInt(document.f_result.registro.value);
  			suma_compra = "";
  			suma = suma_hipo;

		    // fin de calculos para euros
		    //comienzo de formateo
			document.f_result.total.value = Formato_numero(Math.round(suma));
			document.f_result.comision.value = Formato_numero(Math.round(document.f_result.comision.value));
			document.f_result.iva.value = Formato_numero(Math.round(document.f_result.iva.value));
			document.f_result.tasacion.value = Formato_numero(Math.round(document.f_result.tasacion.value));
			document.f_result.impuesto.value = Formato_numero(Math.round(document.f_result.impuesto.value));
			document.f_result.notario.value = Formato_numero(Math.round(document.f_result.notario.value));
			document.f_result.gestion.value = Formato_numero(Math.round(document.f_result.gestion.value));
			document.f_result.registro.value = Formato_numero(Math.round(document.f_result.registro.value));
			document.f_result.total_hipo.value = Formato_numero(Math.round(suma_hipo));

			}
	document.f_result.notariocv.value = "";
	document.f_result.registrocv.value = "";
	document.f_result.ivacv.value = "";
	document.f_result.gestioncv.value = "";
	document.f_result.total_compra.value = "";	
	//document.f_result.ajd.value = "";
	document.f_result.ivaviv.value = "";
	document.f_result.itp.value = "";
	}// SI ESTA RELLENO SOLO EL PRIMERO (IMPORTE SOLICITADO)
	
	// ********************************************************
	// *** SI ESTA RELLENO SOLO EL SEGUNDO( IMPORTE DE LA VIVIENDA)
	if ((document.formu.importeh.value == "") && !(document.formu.valorh.value == "" )){		
		var suma;
		valor1 = 0;
		document.f_result.seguro.value = "";
        buscarvalor(valor1, valor2);

        if (document.formu.radioviv[0].checked)
        { // SI ES OBRA NUEVA
  		  //document.f_result.ajd.value = valor2 * 0.05;
		  document.f_result.ivaviv.value = valor2 * 0.07;
		  document.f_result.itp.value = "";
		  document.f_result.impuesto2.value = valor2 * 0.005;
		}
		else
		{ // SI ES SEGUNDA MANO
		  document.f_result.itp.value = valor2 * 0.07;
  		  //document.f_result.ajd.value = "";
		  document.f_result.ivaviv.value = "";
		  document.f_result.impuesto2.value = "";
		}

		// SI PESETAS
		if (document.formu.radio[0].checked){
              if (document.formu.radioviv[0].checked)
              { // SI ES OBRA NUEVA
  			    suma_hipo = "";
  			    suma_compra = parseInt(document.f_result.impuesto2.value) + parseInt(document.f_result.notariocv.value) + parseInt(document.f_result.registrocv.value) + parseInt(document.f_result.gestioncv.value) + parseInt(document.f_result.ivacv.value) + parseInt(document.f_result.ivaviv.value);
  			    suma = suma_compra;
              }
              else
              {// SI ES SEGUNDA MANO
  			    suma_hipo = "";
  			    suma_compra = parseInt(document.f_result.notariocv.value) + parseInt(document.f_result.registrocv.value) + parseInt(document.f_result.gestioncv.value) + parseInt(document.f_result.ivacv.value) + parseInt(document.f_result.itp.value);
  			    suma = suma_compra;
              }
			document.f_result.notariocv.value = Formato_numero(document.f_result.notariocv.value);
			document.f_result.registrocv.value = Formato_numero(document.f_result.registrocv.value);
			document.f_result.ivacv.value = Formato_numero(document.f_result.ivacv.value);
			document.f_result.gestioncv.value = Formato_numero(document.f_result.gestioncv.value);
			document.f_result.total_compra.value = Formato_numero(suma_compra);
			document.f_result.total_hipo.value = "";
              if (document.formu.radioviv[0].checked)
              {// SI ES OBRA NUEVA
//         	  	  document.f_result.ajd.value = Formato_numero(Math.round(document.f_result.ajd.value));
	        	  document.f_result.ivaviv.value = Formato_numero(Math.round(document.f_result.ivaviv.value));
	        	  document.f_result.impuesto2.value = Formato_numero(Math.round(document.f_result.impuesto2.value));
	          }
	          else
	          {// SI ES SEGUNA MANO
     	     	  document.f_result.itp.value = Formato_numero(Math.round(document.f_result.itp.value));
	          }

			}
		else
			{ // SI EUROS			
			//document.f_result.comision.value = document.f_result.comision.value / 166.386;
			document.f_result.notariocv.value = document.f_result.notariocv.value / 166.386;
			document.f_result.registrocv.value = document.f_result.registrocv.value / 166.386;
			document.f_result.ivacv.value = document.f_result.ivacv.value / 166.386;
			document.f_result.gestioncv.value = document.f_result.gestioncv.value / 166.386;
             if (document.formu.radioviv[0].checked)
              {// SI ES OBRA NUEVA
  			    suma_hipo = "";
  			    suma_compra = parseInt(document.f_result.impuesto2.value) + parseInt(document.f_result.notariocv.value) + parseInt(document.f_result.registrocv.value) + parseInt(document.f_result.gestioncv.value) + parseInt(document.f_result.ivacv.value) + parseInt(document.f_result.ivaviv.value);
  			    suma = suma_compra;
              }
              else
              {// SI ES SEGUNDA MANO
  			    suma_hipo = "";
  			    suma_compra = parseInt(document.f_result.notariocv.value) + parseInt(document.f_result.registrocv.value) + parseInt(document.f_result.gestioncv.value) + parseInt(document.f_result.ivacv.value) + parseInt(document.f_result.itp.value);
  			    suma = suma_compra;
              }

		    // fin de calculos para euros
		    //comienzo de formateo
			document.f_result.total.value = Formato_numero(Math.round(suma));
			document.f_result.notariocv.value = Formato_numero(Math.round(document.f_result.notariocv.value));			
			document.f_result.registrocv.value = Formato_numero(Math.round(document.f_result.registrocv.value));
			document.f_result.ivacv.value = Formato_numero(Math.round(document.f_result.ivacv.value));
			document.f_result.gestioncv.value = Formato_numero(Math.round(document.f_result.gestioncv.value));
			document.f_result.total_compra.value = Formato_numero(Math.round(suma_compra));
			document.f_result.total_hipo.value = "";
            if (document.formu.radioviv[0].checked)
            {// SI ES OBRA NUEVA
//        		document.f_result.ajd.value = Formato_numero(Math.round(document.f_result.ajd.value));
    	    	document.f_result.ivaviv.value = Formato_numero(Math.round(document.f_result.ivaviv.value));
    	    	document.f_result.impuesto2.value = Formato_numero(Math.round(document.f_result.impuesto2.value));
    	    }
    	    else
    	    {// SI ES SEGUNDA MANO
     	    	document.f_result.itp.value = Formato_numero(Math.round(document.f_result.itp.value));
    	    }
		}
		document.f_result.comision.value = "";
		document.f_result.iva.value = "";		
		document.f_result.tasacion.value = "";
		document.f_result.impuesto.value = "";
		document.f_result.notario.value = "";
		document.f_result.gestion.value = "";
		document.f_result.registro.value = "";
		document.f_result.total.value = document.f_result.total_compra.value;
	} //SI ESTA RELLENO SOLO EL SEGUNDO( IMPORTE DE LA VIVIENDA)
	
	
	// ********************************************************
	// *** SI LOS DOS ESTAN VACIOS	
	if ((document.formu.importeh.value == "") && (document.formu.valorh.value == "" )){		
			document.f_result.seguro.value = "";
			document.f_result.tasacion.value = "";
			document.f_result.total.value = "";
			document.f_result.comision.value = "";
			document.f_result.iva.value = "";
			document.f_result.tasacion.value = "";
			document.f_result.impuesto.value = "";
			document.f_result.impuesto2.value = "";
			document.f_result.notario.value = "";
			document.f_result.gestion.value = "";
			document.f_result.registro.value = "";
			document.f_result.notariocv.value = "";
			document.f_result.registrocv.value = "";
			document.f_result.ivacv.value = "";
			document.f_result.gestioncv.value = "";
			document.f_result.total_compra.value = "";
			document.f_result.total_hipo.value = "";			
//			document.f_result.ajd.value = "";
    	    document.f_result.ivaviv.value = "";
    	    alert (me5); //me5
    	   
	}//SI LOS DOS ESTAN VACIOS
}// function calcular_onclick()


//Función específica para Cálculo de Gastos.
function buscarvalor(v1, v2) {

if (document.formu.radio[1].checked){
	v2 = v2 * 166.386;
	v1 = v1 * 166.386;
	//alert (v1 + "--" + v2);
}

if (v1 > "95000000") {
		document.f_result.notario.value = 137000;
		document.f_result.registro.value = 87000;
		document.f_result.gestion.value = 64500;
		document.f_result.iva.value = 10320;
}
if (v1 > "90000000" && v1 <= "95000000"){
		document.f_result.notario.value = 134500;
		document.f_result.registro.value = 84500;
		document.f_result.gestion.value = 64500;
		document.f_result.iva.value = 10320;
}
if (v1 > "85000000" && v1 <= "90000000"){
		document.f_result.notario.value = 132000;
		document.f_result.registro.value = 82000;
		document.f_result.gestion.value = 64500;
		document.f_result.iva.value = 10320;
}
if (v1 > "80000000" && v1 <= "85000000"){
		document.f_result.notario.value = 129500;
		document.f_result.registro.value = 79500;
		document.f_result.gestion.value = 64500;
		document.f_result.iva.value = 10320;
}
if (v1 > "75000000" && v1 <= "80000000"){
		document.f_result.notario.value = 127000;
		document.f_result.registro.value = 77000;
		document.f_result.gestion.value = 64500;
		document.f_result.iva.value = 10320;
}
if (v1 > "70000000" && v1 <= "75000000"){
		document.f_result.notario.value = 124500;
		document.f_result.registro.value = 74500;
		document.f_result.gestion.value = 64500;
		document.f_result.iva.value = 10320;
}
if (v1 > "65000000" && v1 <= "70000000"){
		document.f_result.notario.value = 122000;
		document.f_result.registro.value = 72000;
		document.f_result.gestion.value = 64500;
		document.f_result.iva.value = 10320;
}
if (v1 > "60000000" && v1 <= "65000000"){
		document.f_result.notario.value = 119500;
		document.f_result.registro.value = 69500;
		document.f_result.gestion.value = 64500;
		document.f_result.iva.value = 10320;
}
if (v1 > "55000000" && v1 <= "60000000"){
		document.f_result.notario.value = 117000;
		document.f_result.registro.value = 67000;
		document.f_result.gestion.value = 64500;
		document.f_result.iva.value = 10320;
}
if (v1 > "50000000" && v1 <= "55000000"){
		document.f_result.notario.value = 114500;
		document.f_result.registro.value = 64500;
		document.f_result.gestion.value = 64500;
		document.f_result.iva.value = 10320;
}
if (v1 > "45000000" && v1 <= "50000000"){
		document.f_result.notario.value = 112000;
		document.f_result.registro.value = 62000;
		document.f_result.gestion.value = 49500;
		document.f_result.iva.value = 7920;
}
if (v1 > "40000000" && v1 <= "45000000"){
		document.f_result.notario.value = 109500;
		document.f_result.registro.value = 59500;
		document.f_result.gestion.value = 49500;
		document.f_result.iva.value = 7920;
}
if (v1 > "35000000" && v1 <= "40000000"){
		document.f_result.notario.value = 107000;
		document.f_result.registro.value = 57000;
		document.f_result.gestion.value = 49500;
		document.f_result.iva.value = 7920;
}
if (v1 > "30000000" && v1 <= "35000000"){
		document.f_result.notario.value = 104500;
		document.f_result.registro.value = 54500;
		document.f_result.gestion.value = 49500;
		document.f_result.iva.value = 7920;
}
if (v1 > "29000000" && v1 <= "30000000"){
		document.f_result.notario.value = 102000;
		document.f_result.registro.value = 52000;
		document.f_result.gestion.value = 49500;
		document.f_result.iva.value = 7920;
}
if (v1 > "28000000" && v1 <= "29000000"){
		document.f_result.notario.value = 100500;
		document.f_result.registro.value = 51000;
		document.f_result.gestion.value = 49500;
		document.f_result.iva.value = 7920;

}if (v1 > "27000000" && v1 <= "28000000"){
		document.f_result.notario.value = 99000;
		document.f_result.registro.value = 50000;
		document.f_result.gestion.value = 49500;
		document.f_result.iva.value = 7920;
}
if (v1 > "26000000" && v1 <= "27000000"){
		document.f_result.notario.value = 97500;
		document.f_result.registro.value = 49000;
		document.f_result.gestion.value = 49500;
		document.f_result.iva.value = 7920;
}
if (v1 > "25000000" && v1 <= "26000000"){
		document.f_result.notario.value = 96000;
		document.f_result.registro.value = 48000;
		document.f_result.gestion.value = 49500;
		document.f_result.iva.value = 7920;
}
if (v1 > "24000000" && v1 <= "25000000"){
		document.f_result.notario.value = 94500;
		document.f_result.registro.value = 47000;
		document.f_result.gestion.value = 49500;
		document.f_result.iva.value = 7920;
}
if (v1 > "23000000" && v1 <= "24000000"){
		document.f_result.notario.value = 93000;
		document.f_result.registro.value = 46000;
		document.f_result.gestion.value = 49500;
		document.f_result.iva.value = 7920;
}
if (v1 > "22000000" && v1 <= "23000000"){
		document.f_result.notario.value = 91500;
		document.f_result.registro.value = 45000;
		document.f_result.gestion.value = 49500;
		document.f_result.iva.value = 7920;
}
if (v1 > "21000000" && v1 <= "22000000"){
		document.f_result.notario.value = 90000;
		document.f_result.registro.value = 44000;
		document.f_result.gestion.value = 49500;
		document.f_result.iva.value = 7920;
}
if (v1 > "20000000" && v1 <= "21000000"){
		document.f_result.notario.value = 88500;
		document.f_result.registro.value = 43000;
		document.f_result.gestion.value = 49500;
		document.f_result.iva.value = 7920;
}
if (v1 > "19000000" && v1 <= "20000000"){
		document.f_result.notario.value = 87000;
		document.f_result.registro.value = 42500;
		document.f_result.gestion.value = 39500;
		document.f_result.iva.value = 6320;
}
if (v1 > "18000000" && v1 <= "19000000"){
		document.f_result.notario.value = 85500;
		document.f_result.registro.value = 41000;
		document.f_result.gestion.value = 39500;
		document.f_result.iva.value = 6320;
}
if (v1 > "17000000" && v1 <= "18000000"){
		document.f_result.notario.value = 84000;
		document.f_result.registro.value = 39500;
		document.f_result.gestion.value = 39500;
		document.f_result.iva.value = 6320;
}
if (v1 > "16000000" && v1 <= "17000000"){
		document.f_result.notario.value = 82500;
		document.f_result.registro.value = 38000;
		document.f_result.gestion.value = 39500;
		document.f_result.iva.value = 6320;
}
if (v1 > "15000000" && v1 <= "16000000"){
		document.f_result.notario.value = 81000;
		document.f_result.registro.value = 36500;
		document.f_result.gestion.value = 39500;
		document.f_result.iva.value = 6320;
}
if (v1 > "14000000" && v1 <= "15000000"){
		document.f_result.notario.value = 79500;
		document.f_result.registro.value = 35000;
		document.f_result.gestion.value = 34500;
		document.f_result.iva.value = 5520;
}
if (v1 > "13000000" && v1 <= "14000000"){
		document.f_result.notario.value = 78000;
		document.f_result.registro.value = 34500;
		document.f_result.gestion.value = 34500;
		document.f_result.iva.value = 5520;
}
if (v1 > "12000000" && v1 <= "13000000"){
		document.f_result.notario.value = 76500;
		document.f_result.registro.value = 33000;
		document.f_result.gestion.value = 34500;
		document.f_result.iva.value = 5520;
}
if (v1 > "11000000" && v1 <= "12000000"){
		document.f_result.notario.value = 75000;
		document.f_result.registro.value = 31500;
		document.f_result.gestion.value = 34500;
		document.f_result.iva.value = 5520;
}
if (v1 > "10000000" && v1 <= "11000000"){
		document.f_result.notario.value = 73500;
		document.f_result.registro.value = 30000;
		document.f_result.gestion.value = 34500;
		document.f_result.iva.value = 5520;
}
if (v1 > "9000000" && v1 <= "10000000"){
		document.f_result.notario.value = 72000;
		document.f_result.registro.value = 28500;
		document.f_result.gestion.value = 27000;
		document.f_result.iva.value = 4320;
}
if (v1 > "8000000" && v1 <= "9000000"){
		document.f_result.notario.value = 71000;
		document.f_result.registro.value = 27000;
		document.f_result.gestion.value = 27000;
		document.f_result.iva.value = 4320;
}
if (v1 > "7000000" && v1 <= "8000000"){
		document.f_result.notario.value = 67000;
		document.f_result.registro.value = 25500;
		document.f_result.gestion.value = 27000;
		document.f_result.iva.value = 4320;
}
if (v1 > "6000000" && v1 <= "7000000"){
		document.f_result.notario.value = 63000;
		document.f_result.registro.value = 24000;
		document.f_result.gestion.value = 27000;
		document.f_result.iva.value = 4320;
}
if (v1 > "5000000" && v1 <= "6000000"){
		document.f_result.notario.value = 59000;
		document.f_result.registro.value = 22500;
		document.f_result.gestion.value = 27000;
		document.f_result.iva.value = 4320;
}
if (v1 > "4000000" && v1 <= "5000000"){
		document.f_result.notario.value = 55000;
		document.f_result.registro.value = 21000;
		document.f_result.gestion.value = 22000;
		document.f_result.iva.value = 3520;
}
if (v1 > "3000000" && v1 <= "4000000"){
		document.f_result.notario.value = 51000;
		document.f_result.registro.value = 19500;
		document.f_result.gestion.value = 22000;
		document.f_result.iva.value = 3520;
}
if (v1 > "2000000" && v1 <= "3000000"){
		document.f_result.notario.value = 47000;
		document.f_result.registro.value = 18000;
		document.f_result.gestion.value = 22000;
		document.f_result.iva.value = 3520;
}
if (v1 > "1000000" && v1 <= "2000000"){
		document.f_result.notario.value = 43000;
		document.f_result.registro.value = 16500;
		document.f_result.gestion.value = 22000;
		document.f_result.iva.value = 3520;
}
if (v1 <= "1000000"){
		document.f_result.notario.value = 35000;
		document.f_result.registro.value = 15000;
		document.f_result.gestion.value = 22000;
		document.f_result.iva.value = 3520;
}
// fin calculo de gastos para hipoteca

if (v2 > "95000000") {
		document.f_result.notariocv.value = 150000;
		document.f_result.registrocv.value = 84000;
		document.f_result.gestioncv.value = 50000;
		document.f_result.ivacv.value = 8000;
}
if (v2 > "90000000" && v2 <= "95000000"){
		document.f_result.notariocv.value = 145000;
		document.f_result.registrocv.value = 82000;
		document.f_result.gestioncv.value = 50000;
		document.f_result.ivacv.value = 8000;
}
if (v2 > "85000000" && v2 <= "90000000"){
		document.f_result.notariocv.value = 140000;
		document.f_result.registrocv.value = 80000;
		document.f_result.gestioncv.value = 50000;
		document.f_result.ivacv.value = 8000;
}
if (v2 > "80000000" && v2 <= "85000000"){
		document.f_result.notariocv.value = 135000;
		document.f_result.registrocv.value = 78000;
		document.f_result.gestioncv.value = 50000;
		document.f_result.ivacv.value = 8000;
}
if (v2 > "75000000" && v2 <= "80000000"){
		document.f_result.notariocv.value = 130000;
		document.f_result.registrocv.value = 76000;
		document.f_result.gestioncv.value = 50000;
		document.f_result.ivacv.value = 8000;
}
if (v2 > "70000000" && v2 <= "75000000"){
		document.f_result.notariocv.value = 125000;
		document.f_result.registrocv.value = 74000;
		document.f_result.gestioncv.value = 50000;
		document.f_result.ivacv.value = 8000;
}
if (v2 > "65000000" && v2 <= "70000000"){
		document.f_result.notariocv.value = 120000;
		document.f_result.registrocv.value = 72000;
		document.f_result.gestioncv.value = 50000;
		document.f_result.ivacv.value = 8000;
}
if (v2 > "60000000" && v2 <= "65000000"){
		document.f_result.notariocv.value = 115000;
		document.f_result.registrocv.value = 70000;
		document.f_result.gestioncv.value = 45000;
		document.f_result.ivacv.value = 7200;
}
if (v2 > "55000000" && v2 <= "60000000"){
		document.f_result.notariocv.value = 110000;
		document.f_result.registrocv.value = 68000;
		document.f_result.gestioncv.value = 45000;
		document.f_result.ivacv.value = 7200;
}
if (v2 > "50000000" && v2 <= "55000000"){
		document.f_result.notariocv.value = 105000;
		document.f_result.registrocv.value = 66000;
		document.f_result.gestioncv.value = 45000;
		document.f_result.ivacv.value = 7200;
}
if (v2 > "45000000" && v2 <= "50000000"){
		document.f_result.notariocv.value = 100000;
		document.f_result.registrocv.value = 64000;
		document.f_result.gestioncv.value = 40000;
		document.f_result.ivacv.value = 6400;
}
if (v2 > "40000000" && v2 <= "45000000"){
		document.f_result.notariocv.value = 95000;
		document.f_result.registrocv.value = 62000;
		document.f_result.gestioncv.value = 40000;
		document.f_result.ivacv.value = 6400;
}
if (v2 > "35000000" && v2 <= "40000000"){
		document.f_result.notariocv.value = 92500;
		document.f_result.registrocv.value = 60000;
		document.f_result.gestioncv.value = 40000;
		document.f_result.ivacv.value = 6400;
}
if (v2 > "30000000" && v2 <= "35000000"){
		document.f_result.notariocv.value = 90000;
		document.f_result.registrocv.value = 58000;
		document.f_result.gestioncv.value = 40000;
		document.f_result.ivacv.value = 6400;
}
if (v2 > "29000000" && v2 <= "30000000"){
		document.f_result.notariocv.value = 86000;
		document.f_result.registrocv.value = 54000;
		document.f_result.gestioncv.value = 40000;
		document.f_result.ivacv.value = 6400;
}
if (v2 > "28000000" && v2 <= "29000000"){
		document.f_result.notariocv.value = 85000;
		document.f_result.registrocv.value = 52500;
		document.f_result.gestioncv.value = 40000;
		document.f_result.ivacv.value = 6400;

}if (v2 > "27000000" && v2 <= "28000000"){
		document.f_result.notariocv.value = 84000;
		document.f_result.registrocv.value = 51000;
		document.f_result.gestioncv.value = 40000;
		document.f_result.ivacv.value = 6400;
}
if (v2 > "26000000" && v2 <= "27000000"){
		document.f_result.notariocv.value = 83000;
		document.f_result.registrocv.value = 49500;
		document.f_result.gestioncv.value = 40000;
		document.f_result.ivacv.value = 6400;
}
if (v2 > "25000000" && v2 <= "26000000"){
		document.f_result.notariocv.value = 82000;
		document.f_result.registrocv.value = 48000;
		document.f_result.gestioncv.value = 40000;
		document.f_result.ivacv.value = 6400;
}
if (v2 > "24000000" && v2 <= "25000000"){
		document.f_result.notariocv.value = 81000;
		document.f_result.registrocv.value = 46500;
		document.f_result.gestioncv.value = 40000;
		document.f_result.ivacv.value = 6400;
}
if (v2 > "23000000" && v2 <= "24000000"){
		document.f_result.notariocv.value = 80000;
		document.f_result.registrocv.value = 45000;
		document.f_result.gestioncv.value = 40000;
		document.f_result.ivacv.value = 6400;
}
if (v2 > "22000000" && v2 <= "23000000"){
		document.f_result.notariocv.value = 79000;
		document.f_result.registrocv.value = 43500;
		document.f_result.gestioncv.value = 40000;
		document.f_result.ivacv.value = 6400;
}
if (v2 > "21000000" && v2 <= "22000000"){
		document.f_result.notariocv.value = 78000;
		document.f_result.registrocv.value = 42000;
		document.f_result.gestioncv.value = 40000;
		document.f_result.ivacv.value = 6400;
}
if (v2 > "20000000" && v2 <= "21000000"){
		document.f_result.notariocv.value = 77000;
		document.f_result.registrocv.value = 40500;
		document.f_result.gestioncv.value = 40000;
		document.f_result.ivacv.value = 6400;
}
if (v2 > "19000000" && v2 <= "20000000"){
		document.f_result.notariocv.value = 76000;
		document.f_result.registrocv.value = 39000;
		document.f_result.gestioncv.value = 35000;
		document.f_result.ivacv.value = 5600;
}
if (v2 > "18000000" && v2 <= "19000000"){
		document.f_result.notariocv.value = 75000;
		document.f_result.registrocv.value = 37500;
		document.f_result.gestioncv.value = 35000;
		document.f_result.ivacv.value = 5600;
}
if (v2 > "17000000" && v2 <= "18000000"){
		document.f_result.notariocv.value = 74000;
		document.f_result.registrocv.value = 36000;
		document.f_result.gestioncv.value = 35000;
		document.f_result.ivacv.value = 5600;
}
if (v2 > "16000000" && v2 <= "17000000"){
		document.f_result.notariocv.value = 73000;
		document.f_result.registrocv.value = 34500;
		document.f_result.gestioncv.value = 35000;
		document.f_result.ivacv.value = 5600;
}
if (v2 > "15000000" && v2 <= "16000000"){
		document.f_result.notariocv.value = 72000;
		document.f_result.registrocv.value = 33000;
		document.f_result.gestioncv.value = 35000;
		document.f_result.ivacv.value = 5600;
}
if (v2 > "14000000" && v2 <= "15000000"){
		document.f_result.notariocv.value = 71000;
		document.f_result.registrocv.value = 31500;
		document.f_result.gestioncv.value = 30000;
		document.f_result.ivacv.value = 4800;
}
if (v2 > "13000000" && v2 <= "14000000"){
		document.f_result.notariocv.value = 70000;
		document.f_result.registrocv.value = 30000;
		document.f_result.gestioncv.value = 30000;
		document.f_result.ivacv.value = 4800;
}
if (v2 > "12000000" && v2 <= "13000000"){
		document.f_result.notariocv.value = 69000;
		document.f_result.registrocv.value = 28500;
		document.f_result.gestioncv.value = 30000;
		document.f_result.ivacv.value = 4800;
}
if (v2 > "11000000" && v2 <= "12000000"){
		document.f_result.notariocv.value = 68000;
		document.f_result.registrocv.value = 27000;
		document.f_result.gestioncv.value = 30000;
		document.f_result.ivacv.value = 4800;
}
if (v2 > "10000000" && v2 <= "11000000"){
		document.f_result.notariocv.value = 67000;
		document.f_result.registrocv.value = 25500;
		document.f_result.gestioncv.value = 30000;
		document.f_result.ivacv.value = 4800;
}
if (v2 > "9000000" && v2 <= "10000000"){
		document.f_result.notariocv.value = 66000;
		document.f_result.registrocv.value = 24000;
		document.f_result.gestioncv.value = 22500;
		document.f_result.ivacv.value = 3600;
}
if (v2 > "8000000" && v2 <= "9000000"){
		document.f_result.notariocv.value = 64500;
		document.f_result.registrocv.value = 25500;
		document.f_result.gestioncv.value = 22500;
		document.f_result.ivacv.value = 3600;
}
if (v2 > "7000000" && v2 <= "8000000"){
		document.f_result.notariocv.value = 63000;
		document.f_result.registrocv.value = 21000;
		document.f_result.gestioncv.value = 22500;
		document.f_result.ivacv.value = 3600;
}
if (v2 > "6000000" && v2 <= "7000000"){
		document.f_result.notariocv.value = 61500;
		document.f_result.registrocv.value = 19500;
		document.f_result.gestioncv.value = 22500;
		document.f_result.ivacv.value = 3600;
}
if (v2 > "5000000" && v2 <= "6000000"){
		document.f_result.notariocv.value = 60000;
		document.f_result.registrocv.value = 18000;
		document.f_result.gestioncv.value = 22500;
		document.f_result.ivacv.value = 3600;
}
if (v2 > "4000000" && v2 <= "5000000"){
		document.f_result.notariocv.value = 56000;
		document.f_result.registrocv.value = 16500;
		document.f_result.gestioncv.value = 17500;
		document.f_result.ivacv.value = 2800;
}
if (v2 > "3000000" && v2 <= "4000000"){
		document.f_result.notariocv.value = 48000;
		document.f_result.registrocv.value = 15000;
		document.f_result.gestioncv.value = 17500;
		document.f_result.ivacv.value = 2800;
}
if (v2 > "2000000" && v2 <= "3000000"){
		document.f_result.notariocv.value = 43000;
		document.f_result.registrocv.value = 13500;
		document.f_result.gestioncv.value = 17500;
		document.f_result.ivacv.value = 2800;
}
if (v2 > "1000000" && v2 <= "2000000"){
		document.f_result.notariocv.value = 38500;
		document.f_result.registrocv.value = 12000;
		document.f_result.gestioncv.value = 17500;
		document.f_result.ivacv.value = 2800;
}
if (v2 <= "1000000"){
		document.f_result.notariocv.value = 34000;
		document.f_result.registrocv.value = 10500;
		document.f_result.gestioncv.value = 17500;
		document.f_result.ivacv.value = 2800;
}



}