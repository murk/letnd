<?
// menus eina
if (!defined('SISTEMAMUSEU_MENU_MUSEU')){
	define('SISTEMAMUSEU_MENU_MUSEU','Museus');
	define('SISTEMAMUSEU_MENU_MUSEU_NEW','Nou museu');
	define('SISTEMAMUSEU_MENU_MUSEU_EDIT','Fitxa museu');
	define('SISTEMAMUSEU_MENU_MUSEU_LIST','LListar museus');
	define('SISTEMAMUSEU_MENU_MUSEU_LIST_BIN','Paperera');
	define('SISTEMAMUSEU_MENU_EXHIBITION','Exposicions');
	define('SISTEMAMUSEU_MENU_EXHIBITION_NEW','Nova exposició');
	define('SISTEMAMUSEU_MENU_EXHIBITION_LIST','Llistar exposicions');
	define('SISTEMAMUSEU_MENU_EXHIBITION_LIST_BIN','Paperera');
	define('SISTEMAMUSEU_MENU_TOPIC','Temes exposicions');
	define('SISTEMAMUSEU_MENU_TOPIC_NEW','Nou tema');
	define('SISTEMAMUSEU_MENU_TOPIC_LIST','Llistar temes');
	define('SISTEMAMUSEU_MENU_TOPIC_LIST_BIN','Paperera');
	define('SISTEMAMUSEU_MENU_PUBLICATION','Publicacions');
	define('SISTEMAMUSEU_MENU_PUBLICATION_NEW','Nova publicació');
	define('SISTEMAMUSEU_MENU_PUBLICATION_LIST','Llistar publicacions');
	define('SISTEMAMUSEU_MENU_PUBLICATION_LIST_BIN','Paperera');
	define('SISTEMAMUSEU_MENU_PUBLICATIONKIND','Temes publicacions');
	define('SISTEMAMUSEU_MENU_PUBLICATIONKIND_NEW','Nou tema');
	define('SISTEMAMUSEU_MENU_PUBLICATIONKIND_LIST','Llistar temes');
	define('SISTEMAMUSEU_MENU_PUBLICATIONKIND_LIST_BIN','Paperera');
	
	define('SISTEMAMUSEU_MENU_SENTENCE','Frases Home');
	define('SISTEMAMUSEU_MENU_SENTENCE_NEW','Nova frase');
	define('SISTEMAMUSEU_MENU_SENTENCE_LIST','Llistar frases');
	define('SISTEMAMUSEU_MENU_SENTENCE_LIST_BIN','Paperera');
	
	define('SISTEMAMUSEU_MENU_NEW','Notícies Home');
	define('SISTEMAMUSEU_MENU_NEW_NEW','Nova notícia');
	define('SISTEMAMUSEU_MENU_NEW_LIST','Llistar notícies');
	define('SISTEMAMUSEU_MENU_NEW_LIST_BIN','Paperera');
	
	define('SISTEMAMUSEU_MENU_LLEURE','Lleure');
	define('SISTEMAMUSEU_MENU_LLEURE_NEW','Nou lleure');
	define('SISTEMAMUSEU_MENU_LLEURE_LIST','Llistar lleures');
	define('SISTEMAMUSEU_MENU_LLEURE_LIST_BIN','Paperera');

	define('SISTEMAMUSEU_MENU_EXPERT','Racó de l\'expert');
	define('SISTEMAMUSEU_MENU_EXPERT_NEW','Nou article');
	define('SISTEMAMUSEU_MENU_EXPERT_LIST','Llistar articles');
	define('SISTEMAMUSEU_MENU_EXPERT_LIST_BIN','Paperera');

	define('SISTEMAMUSEU_MENU_PAGE','Editor pàgines');
	define('SISTEMAMUSEU_MENU_PAGE_LIST','Llistar pàgines');
	
	define('SISTEMAMUSEU_MENU_BANNER','Banners');
	define('SISTEMAMUSEU_MENU_BANNER_NEW','Nou banner');
	define('SISTEMAMUSEU_MENU_BANNER_LIST','Llistar banners');
	define('SISTEMAMUSEU_MENU_BANNER_LIST_BIN','Paperera');
}

// captions seccio
$gl_caption['c_edit_images'] = 'Galeria d\'imatges';
$gl_caption['c_status'] = 'Estat';
$gl_caption['c_status_prepare'] = 'En preparació';
$gl_caption['c_status_review'] = 'Per revisar';
$gl_caption['c_status_public'] = 'Públic';
$gl_caption['c_status_archived'] = 'Arxivat';

$gl_caption_museu['c_ref']='Ref';
$gl_caption_museu['c_museu_title']='Nom del museu';
$gl_caption_museu['c_museu_subtitle']='Frase del museu';
$gl_caption_museu['c_tipus']='Tipus';
$gl_caption_museu['c_tipus_1']='Museus seu';
$gl_caption_museu['c_tipus_2']='Museus secció';
$gl_caption_museu['c_tipus_3']='Museus col·laboradors';
$gl_caption_museu['c_home']='Destacat';
$gl_caption_museu['c_phone'] = 'Telèfon';
$gl_caption_museu['c_phone2'] = 'Telèfon (2)';
$gl_caption_museu['c_phone3'] = 'Telèfon (3)';
$gl_caption_museu['c_fax'] = 'Fax';
$gl_caption_museu['c_mail'] = 'E-mail';
$gl_caption_museu['c_mail2'] = 'E-mail (2)';
$gl_caption_museu['c_url1_name']='Nom URL';
$gl_caption_museu['c_url1']='URL';
$gl_caption_museu['c_url2_name']='Nom URL(2)';
$gl_caption_museu['c_url2']='URL oferta educativa';
$gl_caption_museu['c_visita']='Visita virtual';
$gl_caption_museu['c_schedule']='Horari';
$gl_caption_museu['c_adress'] = 'Adreça';
$gl_caption_museu['c_cp']='Codi postal';
$gl_caption_museu['c_museu_description'] = 'Descripció del museu';
$gl_caption_museu['c_data']='Dades del museu';
$gl_caption_museu['c_general_data']='Dades generals';
$gl_caption_museu['c_descriptions']='Descripcions';
$gl_caption_museu['c_description']='Descripció';
$gl_caption['c_file']='Arxius';
$gl_caption_museu['c_videoframe']='Video (Ample 326px)';
$gl_caption_museu['c_edit_map'] = 'Mapa';
$gl_caption_museu['c_edit_data'] = 'Editar Museu';
$gl_caption_museu['c_save_map'] = 'Guardar ubicació';
$gl_caption_museu['c_click_map'] = 'Fes un click en un punt del mapa per guardar la ubicació del museu';
$gl_caption_museu['c_form_map_title'] = 'Ubicació del museu';
$gl_caption_museu['c_country_id']='País';
$gl_caption_museu['c_municipi_id']='Municipi';
$gl_caption_museu['c_provincia_id']='Provincia';
$gl_caption_museu['c_comarca_id']='Comarca';
$gl_caption_museu['c_ordre']='Ordre';
$gl_caption_museu['c_adress_plural']='Adreces';
$gl_caption_museu['c_schedule_plural']='Horaris del museu';

$gl_messages['point_saved'] = 'Nova ubicació guardada correctament';

$gl_caption_museu['c_data_museu']='Dades del museu';
$gl_caption_museu['c_search_adress']='Buscar adreça';
$gl_caption_museu['c_latitude']='latitud';
$gl_caption_museu['c_longitude']='longitud';
$gl_caption_museu['c_found_direction']='adreça trobada';

$gl_caption['c_filter_tipus']='Tots els tipus';
$gl_caption_museu['c_filter_provincia_id'] = 'Totes les provincies';

/*Exposicions*/
$gl_caption_exhibition['c_museu_id']='Museu';
$gl_caption_exhibition['c_kind']='Tipus exposició';
$gl_caption_exhibition['c_expired']='Data caducitat';
$gl_caption_exhibition['c_url']='Url';
$gl_caption_exhibition['c_mail']='E-mail';
$gl_caption_exhibition['c_videoframe']='Video (Ample 500px)';
$gl_caption_exhibition['c_owner']='Propietari';
$gl_caption_exhibition['c_contact_person']='Persona de contacte';
$gl_caption_exhibition['c_technical_data']='Dades tècniques';
$gl_caption_exhibition['c_material']='Tipus de material';
$gl_caption['c_destacat']='Destacat';
$gl_caption_exhibition['c_exhibition_title']='Titol exposició';
$gl_caption_exhibition['c_exhibition_subtitle']='Subtitol exposició';
$gl_caption_exhibition['c_exhibition_description']='Descripció';
$gl_caption_exhibition['c_itinerant']='Itinerant';
$gl_caption_exhibition['c_temporal']='Temporal';
$gl_caption_exhibition['c_permanent']='Permanent';
$gl_caption_exhibition['c_filter_kind'] = 'Tots els tipus';
$gl_caption['c_filter_museu_id'] = 'Tots els museus';
$gl_caption_exhibition['c_ordre'] = $gl_caption_publication['c_ordre']  = $gl_caption_lleure['c_ordre']  = $gl_caption_expert['c_ordre']  = 'Ordre destacats';

/*temes*/
$gl_caption_topic['c_name']='Tema';
$gl_caption['c_topic_id']=$gl_caption_topic['c_name'];


/*publicacions*/
$gl_caption['c_publicationkind_id']=$gl_caption['c_publicationkind']='Tema de publicació';
$gl_caption_publication['c_title']='Titol de la publicació';
$gl_caption_publication['c_subtitle']='Subtitol de la publicació';
$gl_caption_publication['c_description']='Descripció de la publicació';
$gl_caption_publication['c_autor']='Autor';
$gl_caption_publication['c_editorial']='Editorial';
$gl_caption_publication['c_anyo']='Any';

/*expert*/
$gl_caption_expert['c_publicationkind_id']=$gl_caption_expert['c_publicationkind']='Tema de l\'article';
$gl_caption_expert['c_new'] = 'Lleure';
$gl_caption_expert['c_title']='Titol de l\'article';
$gl_caption_expert['c_subtitle']='Subtitol de l\'article';
$gl_caption_expert['c_description']='Descripció de l\'article';

/*frases*/
$gl_caption_sentence['c_sentence_id']='';
$gl_caption_sentence['c_sentence']='Frase';
$gl_caption_sentence['c_sentence_url']='URL més informació';

/* noticies - lleure */

$gl_messages_new['no_category']='Per poder insertar notícies hi ha d\'haver almenys una categoria';

$gl_caption_new['c_prepare'] = 'En preparació';
$gl_caption_new['c_review'] = 'Per revisar';
$gl_caption_new['c_public'] = 'Públic';
$gl_caption_new['c_archived'] = 'Arxivat';
$gl_caption_new['c_new'] = 'Notícia';
$gl_caption_lleure['c_new'] = 'Lleure';
$gl_caption_new['c_title'] = 'Títol';
$gl_caption_new['c_subtitle'] = 'Subtítol';
$gl_caption_new['c_content'] = 'Contingut';
$gl_caption_new['c_entered'] = 'Data publicació';
$gl_caption_new['c_expired'] = 'Data caducitat';
$gl_caption_new['c_status'] = 'Estat';
$gl_caption_new['c_category_id'] = 'Categoria';
$gl_caption_new['c_filter_category_id'] = 'Totes les categories';
$gl_caption_new['c_filter_status'] = 'Tots els estats';
$gl_caption_new['c_ordre'] = 'Ordre';


$gl_caption_image['c_name'] = 'Nom';
$gl_caption_new['c_url1_name']='Nom URL(1)';
$gl_caption_new['c_url1']='URL(1)';
$gl_caption_new['c_url2_name']='Nom URL(2)';
$gl_caption_new['c_url2']='URL(2)';
$gl_caption_new['c_videoframe']='Video (Ample 500px)';
$gl_caption_new['c_file']='Arxius';
$gl_caption_new['c_in_home']='Destacat';

$gl_caption_new['c_tipus']='Tipus';
$gl_caption_new['c_tipus_joc']='Jocs';
$gl_caption_new['c_conte']='Contes';
$gl_caption_new['c_ruta']='Rutes i activitats';

/*banners*/
$gl_caption_banner['c_banner_id']='';
$gl_caption_banner['c_ordre']='Ordre';
$gl_caption_banner['c_url1']='Enllaç';
$gl_caption_banner['c_url1_name']='Text a l\'enllaç';
$gl_caption_banner['c_url1_target']='Destí';
$gl_caption_banner['c_url1_target__blank']='Una nova finestra';
$gl_caption_banner['c_url1_target__self']='La mateixa finestra';
$gl_caption_banner['c_file']='Banner (200 x 70px)';
$gl_caption_banner['c_banner']='Banner';


$gl_caption['c_museu_id']='Museu';
$gl_caption['c_museu_id_caption']='Cap museu';
$gl_caption['c_filter_status'] = 'Tots els estats';
$gl_caption['c_filter_publicationkind_id'] = 'Tots els temes';
$gl_caption['c_filter_topic_id'] = 'Tots els temes';
$gl_caption['c_parent_section'] = '';

?>