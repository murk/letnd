<?php
/*
 *
TODO-i Posar preu a consultar a tots els clients botiga
TODO-i Demandes ha d'anar per xhr http://w.belle-property/admin/?action=show_form_edit&menu_id=103&property_id=1157
TODO-i El debug
TODO-i Treure class excel vella
TODO-i Actualitzar classes, si es possible per composer
TODO-i Borrar classes Export -> pasa a ser Excel
         comprobar a booking_book:1791, i a booking_book:63


TODO-i Acabar classe query, afagar model de laravel
TODO-i Posar route control de laravel
 */

/*
 * if (!empty($_GET['debug'] ) && $_GET['debug'] == 'pass') {
	error_reporting( E_ALL );
	ini_set( 'display_errors', true );
	ini_set( 'display_startup_errors', true );
	$GLOBALS['force_debug'] = 1;
}*/
/*
 * PHP 5.6

TODO-i Posar tots el que hi ha a apicat i falta a letnd letnd
	Categories d'inmobles
	Orientació -> 4 Vientos
	Característiques
TODO-i Posar totes les variables gl_caption i gl_message en BBDD

TODO-i Pasar tots els bin a tinint per millorar l'indexat
TODO-i Module::load() Tercer paràmetre hauria de ser by reference

 * Testejar: *
	Tecnomix
	Atot
	Easybrava comprovar els type-cast
	Comprobar bookingPrice, posar crida al parent

* Product
 TODO-i Afegir quan no can_buy a tots els client
			<? else: // ?>
					<input type="hidden" id="pvp" value="<?= $pvp ?>"><?= $pvp ?>



 * Altres

TODO-i Falta casa unifamiliar a la bbdd de inmoble al frs, deu, eng
TODO-i buscar a inmos document.password i canviar per <?= $c_mail ?>
TODO-i Posar els missatges de easybrava a les botigues per defecte
TODO-i Desconectar clients - mls al crear web nova COMPROVA TOTS ELS PERMISOS , escritors i lectors
TODO-i Treure traduccions de tots els lectors
TODO-i Afegir a botigues ( la resta no ho sé ):


	INSERT INTO `all__provincia` (`provincia_id`, `admin2_id`, `comunitat_id`, `country_id`, `name`, `alternatenames`) VALUES (6362988, NULL, '2', 'ES', 'Melilla', NULL);
	INSERT INTO `all__provincia` (`provincia_id`, `admin2_id`, `comunitat_id`, `country_id`, `name`, `alternatenames`) VALUES (2519582, NULL, '1', 'ES', 'Ceuta', NULL);

TODO-i Posar el family product_family_v2 a les botigues
TODO-i Arreglar les families a les botigues
TODO-i Posar be estils de gl_news quan no hi ha una taula
 */



include_once ('common/classes/main.php');
Main::set_vars(); // defineixo les 4 variables necessaries abans de fer res
Debug::start(); // començo el debug si s'escau

include ('common/main.php'); // carrego arxius que tenen variables globals, a la llarga ha d'anar a Main::load_files();
Main::load_files(); // carrego includes i classes

Main::check_redirect(); // faig redirects 301 si convé

$gl_page = new Page; // començo la pàgina

if ($gl_tool && $gl_tool_section)
{
	Module::get_config_values();// variables de configuracio de la taula modul__config
	Module::load(); // carrego arxiu del modul
	$gl_module = Module::init(); // inicio modul general o modul v3 creats amb classes
	$gl_module->set_tpl();// defineixo tpl
	$gl_module->set_language(); // carrego arxiu variables idioma
	$gl_module->set_config(); // carrego arxiu config del modul
	$gl_module->set_functions(); // carrego arxiu de funcions per el modul
	$gl_module->on_load(); // crido la funció on_load quan he carregat tot el modul

    UploadFiles::initialize($gl_module); // iniciar uploads si n'hi ha

	CallFunction::call(); // crido una funcio desde get, just abans de do_action per poder anular o no el do_action

    $gl_module->do_action(); // segons l'action crido el metode amb el mateix nom
}
// si no hi ha cap eina necessitem idiomes per el page
else
{
	if (is_file(CLIENT_PATH . 'languages/' . $gl_language . '.php')) include (CLIENT_PATH . 'languages/' . $gl_language . '.php');
	Module::set_client_language($gl_caption, true);

	CallFunction::call(); // crido una funcio desde get, just abans de do_action per poder anular o no el do_action
}
Debug::add('Es frame',$gl_page->is_frame);
Debug::add('Es has_save_frame',$gl_config['has_save_frame']);
$gl_page->is_frame && $gl_config['has_save_frame']? // has_save_frame -> si guarda al frame o no
	$gl_page->show_message($gl_message): // mostro missatge al top frame
	$gl_page->show(); // mostro pagina

Debug::p_all(); // mostro les variables del debug si toca
?>