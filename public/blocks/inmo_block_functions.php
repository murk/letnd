<?
// nomes tinc en compte tipus si ho poso a la configuracio del block
// auto -> agafa automaticament el tipus depenent d'on som, ej homeselect a lloguer sempre agafo els tipus rent-temp-moblat-selloption del rent
function inmo_block_get_tipus(&$tipus) {
		
	if (!defined('INMO_DEFAULT_TIPUS'))
		Module::get_config_values('inmo');
	
	if (!$tipus) return;
	
	if ($tipus == 'auto'){			
		$tipus = R::text_id('tipus');		
	}	
	
	if(INMO_DEFAULT_TIPUS && !$tipus) $tipus = INMO_DEFAULT_TIPUS;
	
	$GLOBALS['gl_page']->tpl->set_var('inmo_block_tipus',$tipus);
	$GLOBALS['gl_inmo_block_tipus'] = $tipus;
	
	if ($tipus){
		$tipus_sql = explode('-',$tipus);

		if (is_array($tipus_sql)) $tipus_sql = "tipus = '" . implode("' OR tipus = '" , $tipus_sql) . "'"; 
		else $tipus = " tipus='" . $tipus_sql . "'";

		return " AND (" . $tipus_sql . ")";
	}
	else{
		return '';
	}
	
}

?>