<?

/* 14-10-14 
  ACtualitzacions inmo booking

 */

/* Config per canviar colors temporades */

if (is_table('booking__book') && !is_table('booking__configadmin')) {
	$update_sql []="
		CREATE TABLE IF NOT EXISTS `booking__configadmin` (
		  `name` varchar(50) collate utf8_spanish2_ci NOT NULL default '',
		  `value` varchar(255) collate utf8_spanish2_ci NOT NULL default '',
		  PRIMARY KEY  (`name`)
		) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci ROW_FORMAT=DYNAMIC;";
	$update_sql []="
		INSERT INTO `booking__configadmin` (`name`, `value`) VALUES
			('season_colors', '');";

}
elseif (is_table('booking__book')){	
	trigger_error('Ja existeixen la taula a booking__configadmin');
}

if (is_table('inmo__property') && !is_field('inmo__property', 'show_map')) {
	$update_sql []="
		ALTER TABLE `inmo__property`
			ADD COLUMN `show_map` TINYINT(1) NOT NULL DEFAULT 0 AFTER `zoom`,
			ADD COLUMN `show_map_point` TINYINT(1) NOT NULL DEFAULT 1 AFTER `show_map`;";
}
elseif (is_table('inmo__property')){		
		trigger_error('Ja existeixen el camp show_map');
}

if (is_table('inmo__property') && !is_field('inmo__property', 'videoframe')) {
	$update_sql []="
		ALTER TABLE `inmo__property`
			ADD COLUMN `videoframe` TEXT NOT NULL DEFAULT '' AFTER `facing`;";
}
else{
	if (is_table('inmo__property')) {		
		trigger_error('Ja existeixen el camp videoframe');
	}
}


if (is_table('inmo__configadmin') && !Db::get_first("SELECT * FROM inmo__configadmin WHERE name='videoframe_info_size'" )) {	
	
	$update_sql []= "
		INSERT INTO inmo__configadmin (`name`) VALUES ('videoframe_info_size');
		";	
}
else{
	if (is_table('inmo__property')) {		
		trigger_error('Ja existeixen el name videoframe_info_size');
	}
}

?>