'use strict';

// Source: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/round
function decimalAdjust(type, value, exp) {
	// If the exp is undefined or zero...
	if (typeof exp === 'undefined' || +exp === 0) {
		return Math[type](value);
	}
	value = +value;
	exp = +exp;
	// If the value is not a number or the exp is not an integer...
	if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0)) {
		return NaN;
	}
	// Shift
	value = value.toString().split('e');
	value = Math[type](+(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp)));
	// Shift back
	value = value.toString().split('e');
	return +(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp));
}

function Mortgage(amount, interest, years) {
  this.amount = amount;
  this.interest = interest/100/12; // / 12;
  this.months = years * 12;
}

Mortgage.prototype.round = function (number) {
	return decimalAdjust('round', number, -2);
};

Mortgage.prototype.fixedMonthlyPayment = function () {
  var a = this.amount;
  var i = this.interest;
  var n = this.months;
  var payment = a * ((i * Math.pow(1 + i, n)) / (Math.pow(1 + i, n) - 1));
	dp(payment);
  return payment.toFixed(2);
};

Mortgage.prototype.remainingLoanBalance = function (afterMonths) {
  var p = afterMonths;
  var a = this.amount;
  var i = this.interest;
  var n = this.months;
  var balance = a * ((Math.pow(1 + i, n) - Math.pow(1 + i, p)) / (Math.pow(1 + i, n) - 1));

  return balance.toFixed(2);
};

// Returns a table of the following: payment, principal paid, interest paid, total interest, balance.
Mortgage.prototype.amortizationTable = function () {
  var balance = this.amount;
  var interest = this.interest;
  var payment = this.fixedMonthlyPayment();
  var totalInterestPaid = 0;
  var totalPrincipalPaid = 0;
  var table = [];

  for (var i = 0; i < this.months; i++) {

    // TODO: decimal rounding
    var interestPaid = this.round(balance * interest);
    var principalPaid = this.round(payment - interestPaid);

    totalInterestPaid = this.round(totalInterestPaid + interestPaid);
    totalPrincipalPaid = this.round(totalPrincipalPaid + principalPaid);
    balance = this.round(balance - principalPaid);

    table.push([
      payment,
      principalPaid,
      interestPaid,
      totalInterestPaid,
      balance
    ]);

  }

  return table;
};