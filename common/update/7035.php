<?

/* 7-9-17
 Afegir camp entered i modified a product


 */

if ( is_table( 'product__product' ) && ! is_field( 'product__product', 'modified' ) ) {

	$update_sql [] = "ALTER TABLE `product__product`
	ADD COLUMN `entered` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `others20`;";

	$update_sql [] = "UPDATE `product__product` SET `entered`=now();";


	$update_sql [] = "ALTER TABLE `product__product`
	ADD COLUMN `modified` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00' AFTER `entered`;";

	$update_sql [] = "UPDATE `product__product` SET `modified`=now();";

} else {
	if ( is_table( 'product__product' ) ) {
		trigger_error( 'Ja existeixen el camp ' );
	}
}


?>