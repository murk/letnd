<? /* TODO-i  si tot clicable: onclick="window.location.href='<?=$show_record_link?>'" */ ?>
<article class="item <?= $hidden_class ?>">

	<? /* TODO-i  SI NO son columnes posar la a A i FIGURE a dins l'IF */ ?>
	<a href="<?= $show_record_link ?>" title="<?= htmlspecialchars( $product_title ) ?>">
		<figure>
			<? if( $image_name ): ?>
				<img <?= get_image_attrs( $image_src_thumb, $image_alt ? $image_alt : $product_title, 500, 330, false ) ?> />
			<? endif //images?>
		</figure>
	</a>

	<? /* REF */ ?>
	<div class="ref"><?= $c_ref ?>. <?= $ref ?></div>

	<? if( $product_title ): ?>
		<a href="<?= $show_record_link ?>" title="<?= htmlspecialchars( $product_title ) ?>"><h3>
				<?= $product_title ?>
			</h3></a>
	<? endif ?>

	<? if( $product_subtitle ): ?>
		<a href="<?= $show_record_link ?>" title="<?= htmlspecialchars( $product_subtitle ) ?>">
			<h4><?= $product_subtitle ?></h4>
		</a>
	<? endif ?>

	<? if( $product_description ): ?>
		<div class="description" onclick="window.location.href='<?= $show_record_link ?>'"><?= $product_description ?></div>
	<? endif ?>

	<? if( $can_buy ): ?>
		<a href="<?= $show_record_link ?>" title="<?= htmlspecialchars( $product_title ) ?>">
			<div class="price">
				<? if( $old_price ): ?>

					<span class="old-price"><?= $old_price ?> <?= CURRENCY_NAME ?></span>

					<? if( $discount && !$discount_fixed ): ?>
						<span class="discount">&nbsp;-&nbsp;<?= $discount ?>%</span>
					<? endif ?>

				<? endif //old_price?>

				<?= $pvp ?> <?= CURRENCY_NAME ?>

			</div>
		</a>

		<a onclick="cart_add_item(<?= $id ?>,1,1,event);" class="cart-button" title="<?= htmlspecialchars( $c_add_to_cart ) ?>"></a>

	<? else: //?>
		<? /* TODO-i per 1 columna borrar - Pot servir per anivellar l'alçada */ ?>
		<div class="discount"></div>
		<div class="price"></div>

		<div class="can-buy-message">
			<?= $can_buy_message ?>
		</div>

	<? endif ?>


	<? /* BANDERES */ ?>

	<? /* stock */ ?>
	<? if( $can_buy_message && $can_buy ): ?>
		<div class="flag nostock"><?= $can_buy_message ?></div>

		<? /* ultimes unitats */ ?>
	<? elseif( $show_last_units ): ?>
		<div class="flag ultimas"><?= $c_last_units ?></div>

		<? /* promocio */ ?>
	<? elseif( $prominent ): ?>
		<div class="flag promo"><?= $c_prominent ?></div>

		<? /* descompte ( surt repetit també al costat del nou preu, borrar la que no es vulgui ) */ ?>
	<? elseif( $discount ): ?>
		<div class="flag discount"><?= $c_discount ?><? if( !$discount_fixed ): ?>
				<?= $discount ?>%<? endif ?></div>
	<? endif //discount?>

</article>