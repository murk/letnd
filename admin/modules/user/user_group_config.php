<?php
	// Aquest arxiu es genera automaticament
	// Qualsevol canvi que es faci es podrà sobrescriure involuntariament desde l'eina de gestió
	// Achtung!
	$gl_file_protected = true;
	$gl_db_classes_fields_included = array (
  'name' => '1',
  'group_id' => '1',
);
	$gl_db_classes_language_fields = array (
  0 => '',
);
	$gl_db_classes_fields = array (
  'group_id' => 
  array (
    'type' => 'hidden',
    'enabled' => '1',
    'form_admin' => 'input',
    'form_public' => 'text',
    'list_admin' => 'text',
    'list_public' => 'text',
    'order' => '0',
    'default_value' => '',
    'override_save_value' => '',
    'class' => '',
    'javascript' => '',
    'text_size' => '70',
    'textarea_cols' => '',
    'textarea_rows' => '',
    'select_caption' => '',
    'select_size' => '',
    'text_maxlength' => '',
    'select_table' => '',
    'select_fields' => '',
    'select_condition' => '',
  ),
  'name' => 
  array (
    'type' => 'text',
    'enabled' => '1',
    'form_admin' => 'input',
    'form_public' => 'text',
    'list_admin' => 'input',
    'list_public' => 'text',
    'required' => '1',
    'order' => '0',
    'default_value' => '',
    'override_save_value' => '',
    'class' => '',
    'javascript' => '',
    'text_size' => '70',
    'textarea_cols' => '',
    'textarea_rows' => '',
    'select_caption' => '',
    'select_size' => '',
    'text_maxlength' => '',
    'select_table' => '',
    'select_fields' => '',
    'select_condition' => '',
  ),
  'show_seo' =>
  array (
    'type' => 'checkbox',
    'enabled' => '1',
    'form_admin' => 'input',
    'form_public' => 'no',
    'list_admin' => 'input',
    'list_public' => 'no',
    'required' => '',
    'order' => '5',
    'default_value' => '',
    'override_save_value' => '',
    'class' => '',
    'javascript' => '',
    'text_size' => '60',
    'text_maxlength' => '100',
    'textarea_cols' => '',
    'textarea_rows' => '',
    'select_caption' => '',
    'select_size' => '',
    'select_table' => '',
    'select_fields' => '',
    'select_condition' => '',
  ),
  'show_html_editor' =>
  array (
    'type' => 'checkbox',
    'enabled' => '1',
    'form_admin' => 'input',
    'form_public' => 'no',
    'list_admin' => 'input',
    'list_public' => 'no',
    'required' => '',
    'order' => '5',
    'default_value' => '',
    'override_save_value' => '',
    'class' => '',
    'javascript' => '',
    'text_size' => '60',
    'text_maxlength' => '100',
    'textarea_cols' => '',
    'textarea_rows' => '',
    'select_caption' => '',
    'select_size' => '',
    'select_table' => '',
    'select_fields' => '',
    'select_condition' => '',
  ),
  'is_editable' =>
  array (
    'type' => 'none',
    'enabled' => '1',
    'form_admin' => '0',
    'form_public' => 'no',
    'list_admin' => '0',
    'list_public' => 'no',
    'required' => '',
    'order' => '5',
    'default_value' => '',
    'override_save_value' => '',
    'class' => '',
    'javascript' => '',
    'text_size' => '60',
    'text_maxlength' => '100',
    'textarea_cols' => '',
    'textarea_rows' => '',
    'select_caption' => '',
    'select_size' => '',
    'select_table' => '',
    'select_fields' => '',
    'select_condition' => '',
  ),
);
	$gl_db_classes_list = array (
  'cols' => '1',
);
	$gl_db_classes_form = array (
  'cols' => '1',
  'images_title' => '',
);
	$gl_db_classes_related_tables = array (
  '0' =>
  array (
    'table' => 'user__user',
    'action' => 'notify',
  ),
);
	?>