<?php
// menus eina
if (!defined('PRODUCT_MENU_PRODUCT')){
define('PRODUCT_MENU_PRODUCT','Productes');
define('PRODUCT_MENU_NEW','Crear producte');
define('PRODUCT_MENU_LIST','Llistar productes');
define('PRODUCT_MENU_LIST_BIN','Paperera');
define('PRODUCT_MENU_FAMILY','Famílies');
define('PRODUCT_MENU_FAMILY_NEW','Crear família');
define('PRODUCT_MENU_FAMILY_LIST','Llistar famílies');
define('PRODUCT_MENU_FAMILY_LIST_BIN','Paperera');
define('PRODUCT_MENU_BRAND','Marques');
define('PRODUCT_MENU_BRAND_NEW','Crear marca');
define('PRODUCT_MENU_BRAND_LIST','Llistar marques');
define('PRODUCT_MENU_BRAND_LIST_BIN','Paperera');
define('PRODUCT_MENU_TAX','Impostos');
define('PRODUCT_MENU_TAX_NEW','Crear impost');
define('PRODUCT_MENU_TAX_LIST','Llistar impost');
define('PRODUCT_MENU_TAX_LIST_BIN','Paperera');
//--------------- a traduir -----------------
define('PRODUCT_MENU_COLOR','Colors');
define('PRODUCT_MENU_COLOR_NEW','Crear color');
define('PRODUCT_MENU_COLOR_LIST','Llistar colors');
define('PRODUCT_MENU_COLOR_LIST_BIN','Paperera');
define('PRODUCT_MENU_MATERIAL','Materials');
define('PRODUCT_MENU_MATERIAL_NEW','Crear material');
define('PRODUCT_MENU_MATERIAL_LIST','Llistar materials');
define('PRODUCT_MENU_MATERIAL_LIST_BIN','Paperera');
define('PRODUCT_MENU_VARIATION','Variacions de preu');
define('PRODUCT_MENU_VARIATION_NEW','Crear variació');
define('PRODUCT_MENU_VARIATION_LIST','Llistar variacions');
define('PRODUCT_MENU_VARIATION_DEFAULT','Variacions per defecte');
define('PRODUCT_MENU_VARIATION_CATEGORY','Categories variations');
define('PRODUCT_MENU_VARIATION_CATEGORY_NEW','Crear Categoria');
define('PRODUCT_MENU_VARIATION_CATEGORY_LIST','Llistar Categories');
define('PRODUCT_MENU_VARIATION_LIST_BIN','Paperera');

define('PRODUCT_MENU_PROMCODE','Codi promocional');
define('PRODUCT_MENU_PROMCODE_NEW','Crear codi');
define('PRODUCT_MENU_PROMCODE_LIST','Llistar codi');
define('PRODUCT_MENU_PROMCODE_LIST_BIN','Paperera');

define('PRODUCT_MENU_SECTOR','Sectors');
define('PRODUCT_MENU_SECTOR_NEW','Crear sector');
define('PRODUCT_MENU_SECTOR_LIST','Llistar sectors');
define('PRODUCT_MENU_SECTOR_LIST_BIN','Paperera');

define('SELLPOINT_MENU_SELLPOINT','Punts de venda');
define('SELLPOINT_MENU_SELLPOINT_NEW','Nou punt de venda');
define('SELLPOINT_MENU_SELLPOINT_LIST','Llistar punt de venda');
define('SELLPOINT_MENU_SELLPOINT_BIN','Paperera de reciclatge');

define('PRODUCT_MENU_SPEC','Especificacions');
define('PRODUCT_MENU_SPEC_NEW','Crear especificació');
define('PRODUCT_MENU_SPEC_LIST','Llistar especificacions');
define('PRODUCT_MENU_SPEC_CATEGORY','Categories especificacions');
define('PRODUCT_MENU_SPEC_CATEGORY_NEW','Crear categoria');
define('PRODUCT_MENU_SPEC_CATEGORY_LIST','Llistar categories');
define('PRODUCT_MENU_SPEC_LIST_BIN','Paperera de reciclatge');

define('PRODUCT_MENU_QUANTITYCONCEPT','Conceptes quantitat');
define('PRODUCT_MENU_QUANTITYCONCEPT_NEW','Crear concepte');
define('PRODUCT_MENU_QUANTITYCONCEPT_LIST','Llistar conceptes');
}



$gl_caption['c_images_variation_title']='Variació';
$gl_caption_color['c_color_name']='Nom del color';
$gl_caption['c_color_id']='Color';
$gl_caption['c_material_id']='Material';
$gl_caption['c_choose']='Escull';
$gl_caption_product['c_temperature_unit']='';
$gl_caption_product['c_util_life_unit']='';
$gl_caption_product['c_others1']='';
$gl_caption_product['c_others2']='';
$gl_caption_product['c_others3']='';
$gl_caption_product['c_others4']='';
$gl_caption_product['c_auto_ref']='Automàtica';

$gl_caption_product['c_format']='Format';
$gl_caption_product['c_weight']='Pes';
$gl_caption_product['c_gram']='grams';
$gl_caption_product['c_kilo']='quilo/s';
$gl_caption_product['c_weight_unit']='';
$gl_caption_product['c_height_unit']='';
$gl_caption_product['c_height']='Alçada';
$gl_caption_product['c_mm']='mm';
$gl_caption_product['c_cm']='cm';
$gl_caption_product['c_mt']='mt';
$gl_caption_product['c_km']='km';
$gl_caption_product['c_feet']='peus';
$gl_caption_product['c_temperature']='Temperatura';
$gl_caption_product['c_celcius']='celcius';
$gl_caption_product['c_fahrenheit']='fahrenheit';
$gl_caption_product['c_util_life']='Vida útil';
$gl_caption_product['c_hours']='hores';
$gl_caption_product['c_days']='dies';
$gl_caption_product['c_years']='anys';
$gl_caption_product['c_meters']='metres';
$gl_caption_product['c_km']='kilometres';
$gl_caption_product['c_months']='mesos';
$gl_caption_product['c_caracteristics']='Característiques';
$gl_caption_product['c_other_caracteristics']='Altres Característiques <a href="/admin/?menu_id=4110">( configuració )</a>';
$gl_caption_product['c_power']='Potència';
$gl_caption_product['c_alimentacion']='Alimentació';
$gl_caption_product['c_certification']='Certificat/s';
$gl_caption_material['c_material']='Nom del material';


//-----------------------------------
$gl_caption['c_ref'] = 'Ref';
$gl_caption_product['c_home']='Destacat';
$gl_caption_product['c_ref_number'] = 'Ref. automàtica';
$gl_caption_product['c_ref_supplier'] = 'Ref Proveïdor';
$gl_caption_product['c_product_private'] = 'Nom privat';
$gl_caption['c_family_id'] = 'Família';
$gl_caption_product['c_subfamily_id'] = 'Subfamília';
$gl_caption['c_family_content_list'] = 'Descripció curta al llistat';
$gl_caption['c_family_description']='Descripció de la família';
$gl_caption_product['c_subfamily_id_format']='#%s#%s';
$gl_caption_product['c_subsubfamily_id'] = 'Subfamília';
$gl_caption_product['c_subsubfamily_id_format']='#%s#%s';
$gl_caption_product['c_pvp'] = 'PVP';
$gl_caption_product['c_price_cost'] = 'Preu de cost';
$gl_caption['c_price'] = 'Preu';
$gl_caption_product['c_destacat']='Destacat';
$gl_caption_product['c_pvd'] = 'PVD';
$gl_caption_product['c_pvd1'] = 'Tarifa 1';
$gl_caption_product['c_pvd2'] = 'Tarifa 2';
$gl_caption_product['c_pvd3'] = 'Tarifa 3';
$gl_caption_product['c_pvd4'] = 'Tarifa 4';
$gl_caption_product['c_pvd5'] = 'Tarifa 5';
$gl_caption_product['c_price_consult'] = 'a consultar';
$gl_caption_product['c_price_consult_1'] = 'Preu a consultar';
$gl_caption_product['c_brand_id'] = $gl_caption_product['c_brand'] = 'Marca';
$gl_caption_product['c_sell'] = 'Venta web?';
$gl_caption['c_guarantee']='Garantia';
$gl_caption_product['c_guarantee_period']='Periode garantia';
$gl_caption_product['c_da']='dies';
$gl_caption_product['c_mo']='mesos';
$gl_caption_product['c_yr']='anys';
$gl_caption['c_status'] = 'Estat';
$gl_caption_product['c_free_send'] = 'Permetre enviament gratuït';
$gl_caption_product['c_ordre'] = 'Ordre';
$gl_caption['c_product_title'] = 'Nom del Producte';
$gl_caption['c_product_subtitle'] = 'Subtítol';
$gl_caption['c_product_description'] = 'Descripció del Producte';
$gl_caption['c_prepare'] = 'En preparació';
$gl_caption['c_review'] = 'Per revisar';
$gl_caption['c_onsale'] = 'Per vendre';
$gl_caption['c_nostock'] = 'Fora de stock';
$gl_caption['c_nocatalog'] = 'Descatalogat';
$gl_caption['c_archived'] = 'Arxivat';
$gl_caption['c_tax_id'] = 'I.V.A (%)';
$gl_caption['c_tax_included'] = 'IVA inclòs';
$gl_caption['c_tax_not_included'] = 'IVA no inclòs';
$gl_caption_product['c_offer'] = 'Oferta';
$gl_caption_product['c_discount'] = 'Descompte (%)';
$gl_caption_product['c_discount_wholesaler'] = 'Descompte distribuïdor(%)';
$gl_caption_product['c_margin'] = 'Marge (%)';
$gl_caption_product['c_ean'] = 'EAN';
$gl_caption_product['c_price_unit'] = 'Preu per';
$gl_caption_product['c_unit'] = 'Unitat';
$gl_caption_product['c_ten'] = 'Decena';
$gl_caption_product['c_hundred'] = 'Centenar';
$gl_caption_product['c_thousand'] = 'Miler';
$gl_caption_product['c_couple'] = 'Parella';
$gl_caption_product['c_dozen'] = 'Dotzena';
$gl_caption_product['c_m2'] = 'm2';
$gl_caption_product['c_data']='Dades del producte';
$gl_caption_product['c_general_data']='Dades generals';
$gl_caption_product['c_descriptions']='Descripcions';
$gl_caption_product['c_description']='Descripció';
$gl_caption_product['c_data_product']='Dades del Producte';
$gl_caption_product['c_data_web']='Venta Web';
$gl_caption_product['c_stock']='Stock';
$gl_caption_product['c_sells']='Unitats venudes';
$gl_caption_product['c_file']='Arxius públics';
$gl_caption_product['c_discount_fixed_price']='Descompte preu fixe';
$gl_caption_product['c_discount_fixed_price_wholesaler']='Descompte preu fixe distribuïdor';
$gl_caption_product['c_selected']='Productes sel·leccionades';
$gl_caption_product['c_videoframe']='Video (youtube, metacafe...)';


$gl_caption['c_observations'] = 'Observacions';
$gl_caption['c_check_parent_id']='- cap família -';
$gl_caption['c_check_brand_id']='Tria marca';




$gl_caption['c_brand_name']='Nom de la marca';
$gl_caption_family['c_parent_id']='Família a la que pertany';
$gl_caption_family['c_family']='Nom de la família';
$gl_caption_family['c_family_alt']='Família "title/alt"';
$gl_caption_family['c_family_subtitle'] = 'Subtítol';
$gl_caption_family['c_view_subfamilys']='veure subfamílies';
$gl_caption_family['c_add_subfamily_button']=$gl_caption_family['c_add_subfamily']='Afegir subfamília';
$gl_caption_family['c_filter_parent_id'] = 'Pertany a qualsevol família';
$gl_caption_family['c_search_title'] = 'Buscar famílies';




$gl_caption_tax['c_tax_name']='Nom del impost';
$gl_caption_tax['c_tax_value']='Valor (%)';
$gl_caption_tax['c_equivalencia']='Equivalència (%)';




$gl_caption_product['c_search_title']= 'Busqueda de productes';
$gl_caption_product['c_by_nif']= 'Per Ref.';
$gl_caption_product['c_by_words']= 'Per paraules.';


$gl_caption['c_observations'] = 'Observations';
$gl_caption['c_ordre']='Ordre';
$gl_caption['c_withinstall']='Pvp amb instal·laciò';

$gl_caption ['c_noproduct']= $gl_messages_product ['no_records']= 'En aquests moments no hi ha cap producte per aquesta categoria.';
$gl_caption_product['c_model']='Model';
$gl_caption['c_year']='Any';
$gl_caption['c_ishandmade']='Fet a mà';



// No traduit
if (!defined('CUSTOMER_MENU_CUSTOMER')) {
	define( 'CUSTOMER_MENU_CUSTOMER', 'Clients' );
	define( 'PRODUCT_MENU_CUSTOMER', 'Clients' );
	define( 'CUSTOMER_MENU_CUSTOMER_NEW', 'Insertar client' );
	define( 'CUSTOMER_MENU_CUSTOMER_LIST', 'Llistar clients' );
	define( 'CUSTOMER_MENU_CUSTOMER_BIN', 'Paperera de reciclatge' );

	define( 'CUSTOMER_MENU_ORDER', 'Comandes' );
	define( 'PRODUCT_MENU_ORDER', 'Comandes' );
	define( 'CUSTOMER_MENU_ORDER_NEW', 'Nova comanda' );
	define( 'CUSTOMER_MENU_ORDER_LIST', 'Llistar comandes' );
	define( 'CUSTOMER_MENU_ORDER_LIST_BIN', 'Paperera de reciclatge' );

	define( 'CUSTOMER_MENU_WHOLESALER', 'Distribuïdors' );
	define( 'PRODUCT_MENU_WHOLESALER', 'Distribuïdors' );
	define( 'CUSTOMER_MENU_WHOLESALER_NEW', 'Insertar distribuïdor' );
	define( 'CUSTOMER_MENU_WHOLESALER_LIST', 'Llistar distribuïdor' );
	define( 'CUSTOMER_MENU_WHOLESALER_BIN', 'Paperera de reciclatge' );

	define( 'CUSTOMER_MENU_RATE', 'Tarifes enviament' );
	define( 'PRODUCT_MENU_RATE', 'Tarifes enviament' );
	define( 'CUSTOMER_MENU_RATE_NEW', 'Afegir país' );
	define( 'CUSTOMER_MENU_RATE_PROVINCIA_NEW', 'Afegir provincia' );
	define( 'CUSTOMER_MENU_RATE_LIST', 'Llistar tarifes' );
	define( 'PRODUCT_MENU_COUNTRY', 'Selecció països' );
	define( 'CUSTOMER_MENU_COUNTRY_LIST', 'Selecció països' );

	define( 'CUSTOMER_MENU_COURIER', 'Missatgers' );
	define( 'PRODUCT_MENU_COURIER', 'Missatgers' );
	define( 'CUSTOMER_MENU_COURIER_NEW', 'Crear missatger' );
	define( 'CUSTOMER_MENU_COURIER_LIST', 'Llistar missatgers' );
	define( 'CUSTOMER_MENU_COURIER_LIST_BIN', 'Paperera' );
}

$gl_caption ['c_quantity']='Quantitat';
$gl_caption ['c_product_title']='Producte';
$gl_caption ['c_product_tax']='I.V.A'; // això ha d'anar a la taula de productes
$gl_caption ['c_product_basetax']='Preu/unitat';
$gl_caption ['c_product_base']='Preu/unitat';
$gl_caption ['c_product_total_base']='Total';
$gl_caption ['c_product_total_tax']='I.V.A'; // això ha d'anar a la taula de productes
$gl_caption ['c_product_total_basetax']='Total';
$gl_caption ['c_total']='Total';
$gl_caption ['c_total_base']='Subtotal';
$gl_caption ['c_total_tax']='I.V.A'; // això ha d'anar a la taula de productes
$gl_caption ['c_total_equivalencia']='Recàrrec equivalència';
$gl_caption ['c_total_basetax']='Total';
$gl_caption ['c_rate_basetax']='Despeses enviament';
$gl_caption ['c_rate_base']='';
$gl_caption ['c_rate_tax']='';
$gl_caption ['c_deposit']='Dipòsit';
$gl_caption ['c_deposit_rest']='Import restant';
$gl_caption ['c_all_basetax']='Total';
$gl_caption ['c_all_base'] = 'Base imponible';
$gl_caption ['c_all_tax']='I.V.A';
$gl_caption ['c_all_equivalencia']='Recàrrec equivalència';
$gl_caption ['c_promcode_basetax']='Descompte codi promocional';
$gl_caption['c_product_old_base'] = "";
$gl_caption['c_product_old_basetax'] = "";
$gl_caption['c_product_discount'] = "Descompte";
$gl_caption['c_product_discount_fixed'] = "";

$gl_caption_customer['c_prefered_language'] = 'Idioma';
$gl_caption_customer['c_form_title'] = 'Dades del client';
$gl_caption_customer['c_customer_id'] = 'Núm.';
$gl_caption_customer['c_customer_id_title'] = 'Número';
$gl_caption_customer['c_treatment'] = 'Tracte';
$gl_caption_customer['c_name'] = 'Nom';
$gl_caption_customer['c_company']='Empresa';
$gl_caption_customer['c_surname'] = 'Cognoms';
$gl_caption_customer['c_birthdate'] = 'Data de naixement';
$gl_caption_customer['c_mail'] = "Adreça d'e-mail";
$gl_caption_customer['c_password'] = 'Contrasenya';
$gl_caption_customer['c_password_repeat'] = 'Repetir contrasenya';
$gl_caption['c_telephone'] = 'Telèfon';
$gl_caption_customer['c_telephone_mobile'] = 'Telèfon Mòbil';
$gl_caption['c_has_equivalencia'] = 'Recàrrec equivalència';
$gl_caption['c_has_no_vat'] = 'No afegir IVA';
$gl_caption_customer['c_wholesaler_rate'] = 'Tarifa';
$gl_caption_customer['c_wholesaler_rate_1'] = 'Tarifa 1';
$gl_caption_customer['c_wholesaler_rate_2'] = 'Tarifa 2';
$gl_caption_customer['c_wholesaler_rate_3'] = 'Tarifa 3';
$gl_caption_customer['c_wholesaler_rate_4'] = 'Tarifa 4';
$gl_caption_customer['c_wholesaler_rate_5'] = 'Tarifa 5';
$gl_caption['c_dni'] = 'CIF / NIF';
$gl_caption_customer['c_comment'] = 'Observacions';
$gl_caption_customer['c_bin'] = '';
$gl_caption_customer['c_enteredc'] = "Data d'entrada";
$gl_caption['c_treatment_sra'] = "Sra.";
$gl_caption['c_treatment_sr'] = "Sr.";
$gl_caption ['c_same_address']='La mateixa adreça de facturació';
$gl_caption_customer['c_list_orders'] = 'Veure comandes';
$gl_caption_customer ['c_form_title2']='Dades d\'accès';
$gl_caption_customer ['c_form_title3']='Dades de facturació';
$gl_caption_customer ['c_form_title3_list']='Dades de facturació';
$gl_caption_customer ['c_form_title_wholesaler']='Distribuïdor';
$gl_caption_customer ['c_form_title4']='Adreça d\'entrega';
$gl_caption_customer['c_add_button'] = 'Afegir adreça';

$gl_caption['c_order_id'] = $gl_caption['c_order'] = 'Comanda';
$gl_caption_order['c_customer_id'] = 'Client';
$gl_caption_order['c_total_base'] = 'Subtotal';
$gl_caption_order['c_total_tax'] = 'I.V.A.';
$gl_caption_order['c_total_basetax'] = 'Total';
$gl_caption_order['c_entered_order'] = 'Data de la comanda';
$gl_caption_order['c_entered_order_short'] = 'Data';
$gl_caption_order['c_customer_id'] = 'Client';
$gl_caption_order['c_product_id'] = 'Producte';
$gl_caption_order['c_shipped'] = 'Data d\'enviament';
$gl_caption_order['c_delivered'] = 'Data d\'entrega';
$gl_caption_order['c_order_status'] = 'Estat';
$gl_caption_order['c_deposit_payment_method'] = 'Mètode de pagament dipòsit';
$gl_caption_order_list['c_deposit_payment_method'] = 'M. pagament dipòsit';
$gl_caption_order['c_payment_method'] = 'Mètode de pagament';
$gl_caption_order_list['c_payment_method'] = 'M. pagament';
$gl_caption_order['c_comment'] = 'Observacions privades';
$gl_caption['c_customer_comment'] = 'Observacions client';
$gl_caption['c_is_gift_card'] = 'Xec regal';
$gl_caption_order['c_address_invoice_id'] = '';
$gl_caption_order['c_filter_order_status'] = 'Tots els estats';
$gl_caption_order['c_filter_payment_method'] = 'Tots els mètodes';
$gl_caption_order['c_customer_id_format'] = '%s %s';
$gl_caption_order['c_file'] = 'Arxius públics';
$gl_caption_order['c_button-edit-admin'] = '';


$gl_caption_order['c_order_status_paying'] = 'Pendent pagament';
$gl_caption_order['c_order_status_failed'] = 'Pagament fallit';
$gl_caption_order['c_deposit_payed'] = 'Dipòsit pagat';
$gl_caption_order['c_order_status_pending'] = 'Pagament pendent d\'estar acceptat';
$gl_caption_order['c_order_status_denied'] = 'Pagament rebutjat';
$gl_caption_order['c_order_status_voided'] = 'Pagament anulat';
$gl_caption_order['c_order_status_refunded'] = 'Pagament retornat';
$gl_caption_order['c_order_status_completed'] = 'Pagat';
$gl_caption_order['c_order_status_preparing'] = 'En preparació';
$gl_caption_order['c_order_status_shipped'] = 'Enviat';
$gl_caption_order['c_order_status_delivered'] = 'Entregat';
$gl_caption_order['c_order_status_collect'] = 'Pendent de recollir';

$gl_caption_order['c_deposit_status'] = 'Estat dipòsit';

$gl_caption_order['c_deposit_status_paying'] = 'Pendent pagament';
$gl_caption_order['c_deposit_status_failed'] = 'Pagament fallit';
$gl_caption_order['c_deposit_status_deposit'] = 'Dipòsit pagat';
$gl_caption_order['c_deposit_status_pending'] = 'Pagament pendent d\'estar acceptat';
$gl_caption_order['c_deposit_status_denied'] = 'Pagament rebutjat';
$gl_caption_order['c_deposit_status_voided'] = 'Pagament anulat';
$gl_caption_order['c_deposit_status_refunded'] = 'Pagament retornat';
$gl_caption_order['c_deposit_status_completed'] = 'Pagat';

$gl_caption_order['c_deposit_rest'] = 'Resta';

// Enviament
$gl_caption['c_send_date'] = "Enviament";
$gl_caption['c_send_date_shipped'] = $gl_caption_order['c_shipped'];
$gl_caption['c_send_date_delivered'] = $gl_caption_order['c_delivered'];
$gl_caption['c_send_date_no_date_shipped'] = 'Sense data enviament';
$gl_caption['c_send_date_no_date_delivered'] = 'Sense data entrega';

$gl_caption['c_payment_method_none'] = 'Cap';
$gl_caption['c_payment_method_paypal'] = 'Paypal';
$gl_caption['c_payment_method_account'] = 'Transferència bancària';
$gl_caption['c_payment_method_ondelivery'] = 'Contra reembors';
$gl_caption['c_payment_method_directdebit'] = 'Rebut bancari';
$gl_caption['c_payment_method_4b'] = 'Passarel·la 4B';
$gl_caption['c_payment_method_lacaixa'] = 'Targeta de crèdit';
$gl_caption['c_payment_method_cash'] = 'Efectiu';
$gl_caption['c_payment_method_physicaltpv'] = 'TPV físic';

$gl_caption['c_deposit_payment_method_none'] = 'Cap';
$gl_caption['c_deposit_payment_method_paypal'] = $gl_caption['c_payment_method_paypal'];
$gl_caption['c_deposit_payment_method_account'] = $gl_caption['c_payment_method_account'];
$gl_caption['c_deposit_payment_method_ondelivery'] = $gl_caption['c_payment_method_ondelivery'];
$gl_caption['c_deposit_payment_method_directdebit'] = $gl_caption['c_payment_method_directdebit'];
$gl_caption['c_deposit_payment_method_4b'] = $gl_caption['c_payment_method_4b'];
$gl_caption['c_deposit_payment_method_lacaixa'] = $gl_caption['c_payment_method_lacaixa'];
$gl_caption['c_deposit_payment_method_cash'] = $gl_caption['c_payment_method_cash'];
$gl_caption['c_deposit_payment_method_physicaltpv'] = $gl_caption['c_payment_method_physicaltpv'];


$gl_caption['c_collect'] = 'Recollida a la botiga física';
$gl_caption_list['c_collect'] = 'Recollida';
$gl_caption_order['c_edit_button'] = 'Editar comanda';
$gl_caption_order['c_show_record'] = 'Veure comanda';
$gl_caption_order['c_show_invoice'] = 'Veure factura';
$gl_caption_order['c_show_invoice_refund'] = 'Veure factura devolució';
$gl_caption_order['c_pay_now'] = 'Efectuar pagament';
$gl_caption_order['c_no_shipped'] = 'No enviat';
$gl_caption_order['c_no_delivered'] = 'No entregat';


$gl_caption_order ['c_order_title_admin']='Dades de la comanda';
$gl_caption_order ['c_order_title']='Informació de la comanda';
$gl_caption_order ['c_order_title2']='Productes';
$gl_caption_order ['c_order_title3']='Dades de facturació';
$gl_caption_order ['c_order_title4']='Adreça d\'entrega';
$gl_caption_order ['c_order_title5']='Forma de pagament';
$gl_caption_order ['c_order_title_invoice_customer']='Dades del client';
$gl_caption_order ['c_order_num']='num. ';
$gl_caption ['c_product_variation_ids']='';
$gl_caption ['c_product_variations']='Variacions';
$gl_caption ['c_product_variation_categorys']='';

$gl_caption_order['c_invoice_id'] = 'Factura';
$gl_caption_order['c_invoice_refund_id'] = 'Factura devolució';
$gl_caption_order['c_invoice_date'] = 'Data';
$gl_caption_order['c_invoice_refund_date'] = 'Data';
$gl_caption_order['c_view_invoice'] = 'Veure factura';
$gl_caption_order['c_view_invoice_refund'] = 'Veure factura devolució';

$gl_caption_orderitem = $gl_caption_order;
$gl_caption_orderitem_list = $gl_caption_order_list;

$gl_caption ['c_address_id']='';
$gl_caption ['c_delivery_id']='';
$gl_caption_address ['c_customer_id']='';
$gl_caption_address['c_a_name'] = 'Nom';
$gl_caption_address['c_a_surname'] = 'Cognom';
$gl_caption_address['c_address'] = 'Adreça';
$gl_caption_address['c_zip'] = 'Codi postal';
$gl_caption_address['c_city'] = 'Població';
$gl_caption_address['c_country_id'] = 'País';
$gl_caption_address['c_invoice'] = '';
$gl_caption_address['c_edit_address'] = 'Editar adreça';
$gl_caption_address['c_new_address'] = 'Afegir adreça';

$gl_caption_variation['c_variation'] = 'Variació';
$gl_caption_variation['c_variation_subtitle'] = 'Subtítol';
$gl_caption_variation['c_variation_description'] = 'Descripció';
$gl_caption_variation['c_list_pick_default'] = 'Predet.';
$gl_caption_variation['c_variation_category_id'] = 'Categoria';
$gl_caption_variation['c_variation_status'] = 'Estat';
$gl_caption['c_disabled'] = 'Deshabilitada';
$gl_caption_variation['c_variation_pvp'] = 'Increment PVP';
$gl_caption_variation['c_variation_pvd'] = 'Increment PVD 1';
$gl_caption_variation['c_variation_pvd2'] = 'Increment PVD 2';
$gl_caption_variation['c_variation_pvd3'] = 'Increment PVD 3';
$gl_caption_variation['c_variation_pvd4'] = 'Increment PVD 4';
$gl_caption_variation['c_variation_pvd5'] = 'Increment PVD 5';
$gl_caption_variation['c_variation_others1']='';
$gl_caption_variation['c_variation_others2']='';
$gl_caption_variation['c_variation_others3']='';
$gl_caption_variation['c_variation_others4']='';
$gl_caption_variation_category['c_variation_category'] = 'Categoria';
$gl_caption_variation_category['c_variation_description'] = 'Descripció';
$gl_caption_product['c_edit_variations'] = $gl_caption_product['c_edit_variations_button'] = 'Variacions';
$gl_caption_product['c_edit_specs'] = $gl_caption_product['c_edit_specs_button'] = 'Especificacions';
$gl_caption_product['c_edit_relateds'] = $gl_caption_product['c_edit_relateds_button'] = 'Relacionats';
$gl_caption['c_no_assigned'] = 'No hi ha cap variació definida';
$gl_caption_product['c_data_variations'] = 'Variacions del producte';
$gl_caption_product['c_price_base'] = 'Preu base del producte';
$gl_caption['c_data_digital'] = 'Producte digital';
$gl_caption_product['c_download'] = 'Arxius descarregables';
$gl_caption_product['c_filter_family_id'] = 'Totes les famílies';
$gl_caption_product['c_filter_subfamily_id'] = 'Totes les subfamílies';
$gl_caption_product['c_filter_subsubfamily_id'] = 'Totes les subfamílies';
$gl_caption_product['c_filter_status'] = 'Tots els estats';
$gl_caption_product['c_filter_destacat'] = 'Destacats / No destacts';
$gl_caption_product['c_destacat_0'] = 'No destacats';
$gl_caption_product['c_destacat_1'] = 'Destacats';
$gl_caption_product['c_filter_prominent'] = 'En Promoció / No promoció';
$gl_caption_product['c_prominent_0'] = 'No promoció';
$gl_caption_product['c_prominent_1'] = 'En promoció';
$gl_caption_product['c_stockcontrol']='Control d\'stock';
$gl_caption_product['c_last_stock_units_public']='Avís últimes unitats públic';
$gl_caption_product['c_last_stock_units_admin']='Avís últimes unitats admin';
$gl_caption_brand['c_brand_description']='Descripció';
$gl_caption['c_prominent']='En promoció';
$gl_caption_list['c_prominent']='Prom.';
$gl_caption_product['c_is_new_product']='Producte nou';
$gl_caption_product_list['c_is_new_product']='Nou';
$gl_caption_variation['c_variation_stock']='Stock';
$gl_caption_variation['c_variation_last_stock_units_public']='Avís públic';
$gl_caption_variation['c_variation_last_stock_units_admin']='Avís admin';
$gl_caption_variation['c_variation_ref']='Ref';
$gl_caption_variation['c_variation_ref_number']='Ref. Automàtica';
$gl_caption_variation['c_variation_ref_supplier']='Ref Proveïdor';

$gl_caption['c_product_file_name'] = $GLOBALS['gl_caption']['c_file_name'];
$gl_caption['c_product_old_file_name'] = $GLOBALS['gl_caption']['c_old_file_name'];

$gl_caption_variation['c_variation_sells']='Unitats venudes';
$gl_caption['c_add_variations']='Afegir variacions';
$gl_caption_variation['c_selected']='Variacions per afegir';
$gl_caption['c_new_variations']='Variacions afegides';
$gl_caption['c_variation_category_select']='Totes les categories';
$gl_caption_variation['c_filter_variation_category_id']='Totes les categories';

$gl_caption['c_send_password'] = 'Enviar contrasenya per email';
$gl_caption['c_password_sent'] = 'S\ha enviat la contrasenya al distribuïdor';
$gl_caption['c_password_mail_subject'] = 'La seva contrasenya a: ';
$gl_caption['c_password_mail_text_1'] = 'La seva contrasenya per accedir a la nostra botiga és';
$gl_caption['c_password_mail_text_2'] = 'Pot canviar la contrasenya en l\'apartat "El seu compte" de la nostra pàgina web';
$gl_messages_customer['password_sent'] = 'S\'ha enviat la contrasenya al client';
$gl_messages_customer['password_not_sent'] = 'No s\'ha pogut enviar la contrasenya, si us plau cliqui a "enviar contrasenya per mail"';

$gl_caption_customer['c_activate_button'] = 'Activar client';
$gl_caption_customer['c_deactivate_button'] = 'Desactivar client';
$gl_caption_customer['c_set_wholesaler_button'] = 'Fer distribuïdor';
$gl_caption_customer['c_unset_wholesaler_button'] = 'Treure distribuïdor';
$gl_caption_customer['c_activated_mail_message'] = 'El seu compte a %s ha estat activat, ja pot accedir a la nostra botiga.';
$gl_caption_customer['c_activated_mail_subject'] = 'El seu compte ha estat activat';
$gl_caption_customer['c_filter_activated'] = 'Activats / Desactivats';
$gl_caption_customer['c_activated_0'] = 'Desactivats';
$gl_caption_customer['c_activated_1'] = 'Activats';

$gl_messages_customer['activated'] = 'El client ha estat activat';
$gl_messages_customer['deactivated'] = 'El client ha estat desactivat';
$gl_messages_customer['not_activated'] = 'No s\'ha pogut activar el client, si us plau torna a probar-ho';
$gl_messages_customer['set_wholesaler'] = 'El client ara és distribuïdor';
$gl_messages_customer['unset_wholesaler'] = 'El client ja no és distribuïdor';

$gl_caption_family['c_destacat']='Destacada';

$gl_caption['c_promcode'] = 'Codi promocional';
$gl_caption_promcode['c_name'] = 'Nom';
$gl_caption_promcode['c_discount_pvp'] = 'Descompte PVP';
$gl_caption_promcode['c_discount_pvd'] = 'Descompte tarifa 1';
$gl_caption_promcode['c_discount_pvd2'] = 'Descompte tarifa 2';
$gl_caption_promcode['c_discount_pvd3'] = 'Descompte tarifa 3';
$gl_caption_promcode['c_discount_pvd4'] = 'Descompte tarifa 4';
$gl_caption_promcode['c_discount_pvd5'] = 'Descompte tarifa 5';
$gl_caption_promcode['c_discount_type'] = 'Tipus de descompte';
$gl_caption_promcode['c_discount_type_fixed'] = 'Import fixe';
$gl_caption_promcode['c_discount_type_percent'] = 'Percentatge';
$gl_caption_promcode['c_minimum_amount'] = 'Despesa mínima';
$gl_caption_promcode['c_start_date'] = 'Data inicial';
$gl_caption_promcode['c_end_date'] = 'Data final';
$gl_caption['c_promcode_discount'] = 'Dte. codi promocional';

$gl_caption['c_sector']='Sector';
$gl_caption['c_sector_id']='Sector';
$gl_caption_sector['c_sector_description']='Descripció';


$gl_caption_brand['c_send_rate_spain']='Espanya';
$gl_caption_brand['c_send_rate_france']='França';
$gl_caption_brand['c_send_rate_andorra']='Andorra';
$gl_caption_brand['c_send_rate_world']='Resta del món';
$gl_caption['c_minimum_buy_free_send']='Compra mínima';


/*  SELLPOINT  */
/*--------------- nomès castellà i català, no traduit a la resta -------------------*/


$gl_caption_sellpoint['c_sellpoint'] = 'Títol';
$gl_caption_sellpoint['c_sellpoint_description'] = 'Descripció';
$gl_caption_sellpoint['c_empresa'] = 'Empresa';
$gl_caption_sellpoint['c_adress'] = 'Adreça';
$gl_caption_sellpoint['c_comunitat_id'] = 'Comunitat';
$gl_caption_sellpoint['c_provincia_id'] = 'Província';
$gl_caption_sellpoint['c_provincia_id_caption'] = 'Cap provincia';
$gl_caption_sellpoint['c_poblacio'] = 'Població';
$gl_caption_sellpoint['c_cp'] = 'Codi postal';
$gl_caption_sellpoint['c_telefon'] = 'Telèfon';
$gl_caption_sellpoint['c_telefon2'] = 'Telèfon 2';
$gl_caption_sellpoint['c_fax'] = 'Fax';
$gl_caption_sellpoint['c_correu'] = 'E-mail';
$gl_caption_sellpoint['c_correu2'] = 'E-mail 2';
$gl_caption_sellpoint['c_url1'] = 'Web';
$gl_caption_sellpoint['c_url1_name'] = 'Nom de la web';



$gl_caption_sellpoint['c_filter_comarca_id'] = 'Totes les comarques';
$gl_caption_sellpoint['c_filter_municipi_id'] = 'Tots els municipis';
$gl_caption_sellpoint['c_filter_seller_kind'] = 'Tots els tipus';


// mapes
$gl_caption_sellpoint['c_edit_map'] = 'Ubicació';
$gl_caption_sellpoint['c_search_adress']='Buscar adreça';
$gl_caption_sellpoint['c_latitude']='latitud';
$gl_caption_sellpoint['c_longitude']='longitud';
$gl_caption_sellpoint['c_found_direction']='adreça trobada';
$gl_caption_sellpoint['c_save_map'] = 'Guardar ubicació';
$gl_caption_sellpoint['c_click_map'] = 'Fes un click en un punt del mapa per definir la ubicació';
$gl_caption_sellpoint['c_form_map_title'] = 'Ubicació';
$gl_messages['point_saved'] = 'Nova ubicació guardada correctament';

$gl_caption_sellpoint['c_prepare'] = 'En preparació';
$gl_caption_sellpoint['c_review'] = 'Per revisar';
$gl_caption_sellpoint['c_public'] = 'Públic';
$gl_caption_sellpoint['c_archived'] = 'Arxivat';
$gl_caption_sellpoint['c_status'] = 'Estat';

$gl_caption_sellpoint['c_international'] = 'Internacional';
$gl_caption_sellpoint['c_country_id'] = 'País';
$gl_caption_sellpoint['c_seller_kind'] = 'Tipus';
$gl_caption_sellpoint['c_seller_kind_shop'] = 'Botiga';
$gl_caption_sellpoint['c_seller_kind_shops'] = 'Botigues';
$gl_caption_sellpoint['c_seller_kind_dealer'] = 'Distribuïdor';
$gl_caption_sellpoint['c_seller_kind_dealers'] = 'Distribuïdors';

$gl_caption_sellpoint['c_sellpoint_file_name'] = $GLOBALS['gl_caption']['c_file_name'];
$gl_caption_sellpoint['c_sellpoint_old_file_name'] = $GLOBALS['gl_caption']['c_old_file_name'];

$gl_caption_sellpoint['c_filter_provincia_id'] = 'Totes les provincies';

// tarifes
$gl_caption['c_country_id'] = $gl_caption['c_country'] = 'País';
$gl_caption['c_provincia_id'] = $gl_caption['c_provincia'] = 'Provincia';
$gl_caption_rate['c_rate'] = 'Tarifa enviament';
$gl_caption_rate['c_send_rate_world']='Resta de països';
$gl_caption_rate['c_rate_title']='Tarifes';
$gl_caption_rate['c_tax_included'] = 'IVA inclòs';
$gl_caption_rate['c_tax_not_included'] = 'IVA no inclòs';
$gl_caption_rate['c_rate_encrease_ondelivery'] = $gl_caption_rate['c_send_rate_encrease_ondelivery_world'] = 'Increment contra reembors';

$gl_caption['c_can_deliver'] = "Permetre enviament";

// Especificacions
$gl_caption['c_spec_category_id'] = $gl_caption['c_spec_category'] =  'Categoria';
$gl_caption['c_spec_type'] = 'Tipus';
$gl_caption['c_ordre'] = 'Ordre';
$gl_caption['c_spec'] = 'Especificació';
$gl_caption['c_spec_unit'] = 'Unitats';
$gl_caption['c_filter_spec_category_id'] = 'Totes les categories';


$gl_caption['c_spec_type_checkbox']     = 'Casella verificació';
$gl_caption['c_spec_type_int']          = 'Nombre';
$gl_caption['c_spec_type_text']         = 'Texte en idiomes';
$gl_caption['c_spec_type_text_no_lang'] = 'Texte';
$gl_caption['c_spec_type_textarea']     = 'Texte varies línies';
$gl_caption['c_spec_type_select']       = 'Desplegable';

$gl_caption['c_data_specs']       = 'Especificacions del producte';

// Couriers
$gl_caption['c_courier'] = $gl_caption['c_courier_id'] = "Missatger";
$gl_caption['c_courier_url'] = "Web";
$gl_caption['c_courier_follow_code'] = "Codi de seguiment";
$gl_caption['c_default_courier'] = "Establir com a predeterminat";
$gl_caption['c_default_courier_0'] = "";
$gl_caption['c_default_courier_1'] = "Predeterminat";

// Enviar emails segons/  estat
// admin
$gl_caption['c_prepared_emailed'] =
$gl_caption['c_delivered_emailed'] =
$gl_caption['c_shipped_emailed'] = '';
$gl_caption['c_send_mail_prepared'] = "Enviar email comanda preparada";
$gl_caption['c_send_mail_shipped'] = "Enviar email comanda enviada";
$gl_caption['c_send_mail_delivered'] = "Enviar email comanda entregada";
$gl_caption['c_resend_mail'] = "Reenviar email";
$gl_caption['c_mail_sent'] = "Email enviat el";
$gl_caption['c_mail_send_error'] = "No s'ha pogut enviar el missatge, si us plau torna a provar-ho";
$gl_caption['c_sending_email'] = "Enviant email...";
$gl_caption['c_email_sended_correctly'] = "Email enviat correctament";

// públic
$gl_caption['c_mail_subject_buyer_status_prepared'] = 'La teva comanda a %1$s està preparada';
$gl_caption['c_mail_subject_buyer_status_shipped'] = 'La teva comanda a %1$s està enviada';
$gl_caption['c_mail_subject_buyer_status_delivered'] = 'La teva comanda a %1$s ja s\'ha entregada';
$gl_caption['c_mail_subject_seller_status_prepared'] = 'Còpia de comanda preparada';
$gl_caption['c_mail_subject_seller_status_shipped'] = 'Còpia de comanda enviada';
$gl_caption['c_mail_subject_seller_status_delivered'] = 'Còpia de comanda entregada';

$gl_caption['c_mail_body_prepared'] = '
	<p>Hola %1$s,</p>
	<p>Volem agrair la teva confiança en nosaltres a l\'hora de realitzar les teves
		compres.</p>
	<p>Estem preparant la teva comanda, amb la referència: <strong>%2$s</strong>. Aviat realitzarem l\'enviament
		corresponent. Et demanem que revisis la següent informació i et posis en contacte amb nosaltres en cas d\'error.</p>
	<p>En qualsevol moment, pots consultar la teva comanda accedint al <a href="%3$s">teu compte</a> a la nostra pàgina web. També et pots
		posar en contacte amb nosaltres a través del correu:</p>
	<p><a href="mailto:%4$s">%4$s</a></p>
	<p>Gràcies per comprar a %5$s!</p>';

$gl_caption['c_mail_body_shipped'] = '
	<p>Hola %1$s,</p>
	<p>T\'informem que la teva comanda <strong>%2$s</strong> ha estat enviada</p>';
$gl_caption['c_mail_body_shipped_2'] = '
	<p>La companyia de
		transports responsable de l\'enviament de la teva comanda és <a href="%3$s"><strong>%4$s</strong></a> i el nombre de seguiment
		<strong>%5$s</strong>.</p >';
$gl_caption['c_mail_body_shipped_3'] = '
	<p>Pots revisar la comanda a: "comandes" dins de "<a href="%6$s">el meu compte</a>".</p >
	<p>Gràcies per comprar a %7$s!</p >';

$gl_caption['c_mail_body_delivered'] = '
	<p>Hola %1$s,</p>
	<p>La teva comanda <strong>%2$s</strong> ja ha estat lliurada a l\'adreça que ens vas indicar.</p>
	<p>Esperem que estiguis completament satisfet amb la teva compra. Si per alguna raó aquest no és el cas,
	agrairíem que ens donis l\'oportunitat de resoldre les teves inquietuds responent directament aquest correu
	electrònic:</p>
	<p><a href="mailto:%3$s">%3$s</a></p>
	<p>Gràcies per comprar a %4$s!</p>';


// Relacionats
$gl_caption['c_data_related']       = 'Productes relacionats';
$gl_caption['c_product_relateds'] = "Productes relacionats";
$gl_caption['c_select_related_products'] = "Busca i selecciona productes relacionats";
$gl_caption_product_related['c_image'] = "";

// Quantitat màxima
$gl_caption['c_max_quantity'] = "Quantitat màxima per comanda";
$gl_caption_list['c_max_quantity'] = "Max";
$gl_caption['c_max_quantity_abbr'] = "Max.";
$gl_caption['c_min_quantity'] = "Quantitat mínima per comanda";
$gl_caption_list['c_min_quantity'] = "Min";
$gl_caption['c_min_quantity_abbr'] = "Min.";
$gl_caption['c_quantityconcept_id'] = "Concepte";
$gl_caption['c_quantityconcept'] = "Concepte";

// Calendari
$gl_caption['c_edit_calendar'] = "Calendari";
$gl_caption['c_has_calendar'] = "Activar calendari";
$gl_caption['c_is_calendar_public'] = "Públic";
$gl_caption['c_familybusy_id'] = "";
$gl_caption['c_date_busy'] = "Dia";
$gl_caption['c_familybusy_entered'] = "Data creació";
$gl_caption['c_form_calendar_title'] = "Dates ocupades";
$gl_caption['c_calendar_not_saved_error'] = "No s'ha pogut guardar la data, si us plau torna a probar-ho";
$gl_caption['c_calendar_saved_occupied_message'] = "S'ha ocupat el dia";
$gl_caption['c_calendar_saved_freed_message'] = "S'ha desocupat el dia";

$gl_caption['c_choose_date'] = "S'ha de triar una data";

// Famílies privades
$gl_caption['c_is_family_private'] = "Privada";
$gl_caption['c_pass_code'] = "Codi d'accés";

$gl_caption['c_pass_code_error'] = "El codi entrat no és vàlid";
$gl_caption['c_pass_code_success'] = "El codi és correcte";


// Cart
$gl_caption['c_product_date'] = "Data";

// Cercador comandes
$gl_caption['c_dates_from'] = 'Del';
$gl_caption['c_dates_to'] = 'al';
$gl_caption['c_caption_filter_order_status'] = 'Estat';
$gl_caption['c_order_status'] = 'Estat';
$gl_caption['c_order_status_group_payed'] = 'Pagat';
$gl_caption['c_order_status_group_not_payed'] = 'Per pagar';
$gl_caption['c_order_status_group_payed_deposit'] = 'Dipòsit pagat';
$gl_caption['c_order_status_group_not_payed_deposit'] = 'Dipòsit per pagar';
$gl_caption['c_view_format'] = "Mostrar";
$gl_caption['c_view_format_all'] = "Tot";

$gl_caption['c_view_format_order'] = "Comandes";
$gl_caption['c_view_format_orderitems'] = "Productes";

$gl_caption['c_view_format_title'] = "Tipus de llistat";