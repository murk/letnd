<nav id="breadcrumb">
	<?foreach($loop as $l): extract ($l)?>
	
		<?if ($link):?>
			<a class="active<?=$selected?>" title="<?=htmlspecialchars($item)?>" href="<?=$link?>"><?=$item?></a>
		<?else: //link?>
			<strong><?=$item?></strong>
		<?endif //link?>

		<?if (!$selected):?>
			<span>&gt;</span>
		<?endif //selected?>

	<?endforeach //loop?>
</nav>