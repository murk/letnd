<?php
// menus eina
if (!defined('LASSDIVE_MENU_ACTIVITAT')){
	define('LASSDIVE_MENU_ACTIVITAT', 'Activitats');
	define('LASSDIVE_MENU_ACTIVITAT_NEW', 'Entrar nova activitat');
	define('LASSDIVE_MENU_ACTIVITAT_LIST', 'Llistar i editar activitats');
	define('LASSDIVE_MENU_ACTIVITAT_LIST_PACKS', 'Llistar especials');
	define('LASSDIVE_MENU_ACTIVITAT_BIN', 'Paperera de reciclatge');
	define('LASSDIVE_MENU_IDIOMA', 'Idioma');
	define('LASSDIVE_MENU_IDIOMA_NEW', 'Entrar nou idioma');
	define('LASSDIVE_MENU_IDIOMA_LIST', 'Llistar idiomes');

	define('LASSDIVE_MENU_LEVEL', 'Nivells');
	define('LASSDIVE_MENU_LEVEL_NEW', 'Entrar nou nivell');
	define('LASSDIVE_MENU_LEVEL_LIST', 'Llistar nivells');

	define('LASSDIVE_MENU_MODALITY', 'Modalitats');
	define('LASSDIVE_MENU_MODALITY_NEW', 'Entrar nova modalitat');
	define('LASSDIVE_MENU_MODALITY_LIST', 'Llistar modalitats');

	define('LASSDIVE_MENU_ELEMENT','Inclòs / no inclòs');
	define('LASSDIVE_MENU_ELEMENT_NEW','Nou ítem');
	define('LASSDIVE_MENU_ELEMENT_LIST','Llistar ítems');
	
	define('LASSDIVE_MENU_PRICE', 'Preus');
	define('LASSDIVE_MENU_PRICE_NEW', 'Entrar nou preu');
	define('LASSDIVE_MENU_PRICE_LIST', 'Llistar preus');
	define('LASSDIVE_MENU_PRICE_BIN', 'Paperera de reciclatge');

	define('LASSDIVE_MENU_CATEGORY', 'Categories');
	define('LASSDIVE_MENU_CATEGORY_NEW', 'Entrar nova categoria');
	define('LASSDIVE_MENU_CATEGORY_LIST', 'Llistar categories');
}

$gl_messages_activitat['no_category']='Per poder insertar notícies hi ha d\'haver almenys una categoria';

// captions seccio
$gl_caption_activitat['c_see_activities'] = 'Llistar activitats';
$gl_caption_activitat['c_edit_calendar'] = 'Editar calendari';
$gl_caption_activitat['c_see_products'] = 'Llistar productes';
$gl_caption_activitat['c_activitat_title'] = 'Títol';
$gl_caption_activitat['c_parent_id'] = 'Pertany a';
$gl_caption_activitat['c_prepare'] = 'En preparació';
$gl_caption_activitat['c_review'] = 'Per revisar';
$gl_caption_activitat['c_public'] = 'Públic';
$gl_caption_activitat['c_archived'] = 'Arxivat';
$gl_caption_activitat['c_title'] = 'Títol';
$gl_caption_activitat['c_subtitle'] = 'Subtítol';
$gl_caption_activitat['c_content'] = 'Descripció';
$gl_caption_activitat['c_entered'] = 'Creada';
$gl_caption_activitat['c_modified'] = 'Modificada';
$gl_caption_activitat['c_start_date'] = 'Data inici';
$gl_caption_activitat['c_end_date'] = 'Data final';
$gl_caption_activitat['c_minimum_age'] = 'Edat mínima';
$gl_caption_activitat['c_maximum_person'] = 'Màxim persones';
$gl_caption_activitat['c_status'] = 'Estat';
$gl_caption_activitat['c_category_id'] = 'Categoria';
$gl_caption_activitat['c_filter_category_id'] = 'Totes les categories';
$gl_caption_activitat['c_filter_status'] = 'Tots els estats';
$gl_caption_activitat['c_filter_in_home'] = 'Totes, home o no';
$gl_caption_activitat['c_filter_parent_id'] = 'Totes les categories';
$gl_caption_activitat['c_ordre'] = 'Ordre';
$gl_caption_activitat['c_weekday_id'] = 'Dies';
$gl_caption_activitat['c_price'] = 'Preu';
$gl_caption_activitat['c_price2'] = 'Preu 2';
$gl_caption_activitat['c_price3'] = 'Preu 3';
$gl_caption_activitat['c_price4'] = 'Preu 4';
$gl_caption_activitat['c_price5'] = 'Preu 5';
$gl_caption_activitat['c_price6'] = 'Preu 6';
$gl_caption_activitat['c_price7'] = 'Preu 7';
$gl_caption_activitat['c_price8'] = 'Preu 8';
$gl_caption_activitat['c_price9'] = 'Preu 9';
$gl_caption_activitat['c_price10'] = 'Preu 10';
$gl_caption_activitat['c_price_text'] = 'Preu text';
$gl_caption_activitat['c_price_text2'] = 'Preu text 2';
$gl_caption_activitat['c_price_text3'] = 'Preu text 3';
$gl_caption_activitat['c_price_text4'] = 'Preu text 4';
$gl_caption_activitat['c_price_text5'] = 'Preu text 5';
$gl_caption_activitat['c_price_text6'] = 'Preu text 6';
$gl_caption_activitat['c_price_text7'] = 'Preu text 7';
$gl_caption_activitat['c_price_text8'] = 'Preu text 8';
$gl_caption_activitat['c_price_text9'] = 'Preu text 9';
$gl_caption_activitat['c_price_text10'] = 'Preu text 10';
$gl_caption_activitat['c_and'] = 'und';
$gl_caption_activitat['c_or'] = 'oder';

$gl_caption_category['c_category'] = 'Categoria';
$gl_caption_category['c_activitat_button'] = 'Veure activitats';
$gl_caption_category['c_ordre'] = 'Ordre';
$gl_caption_image['c_name'] = 'Nom';
$gl_caption['c_idioma'] = $gl_caption['c_idioma_id'] = 'Idioma';
$gl_caption_activitat['c_url1_name']='Nom URL(1)';
$gl_caption_activitat['c_url1']='URL(1)';
$gl_caption_activitat['c_url2_name']='Nom URL(2)';
$gl_caption_activitat['c_url2']='URL(2)';
$gl_caption_activitat['c_url3_name']='Nom URL(3)';
$gl_caption_activitat['c_url3']='URL(3)';
$gl_caption_activitat['c_videoframe']='Video (youtube, metacafe...)';
$gl_caption_activitat['c_file']='Arxius';
$gl_caption_activitat['c_in_home']='Surt a la Home';
$gl_caption_activitat['c_in_home_0']='No surt a la Home';
$gl_caption_activitat['c_in_home_1']='Surt a la Home';
$gl_caption_activitat['c_destacat']='Notícia destacada';
$gl_caption_activitat['c_destacat_0']='No destacada';
$gl_caption_activitat['c_destacat_1']='Destacada';
$gl_caption_activitat['c_timetable']='Horari';
global $week_names;
$gl_caption_activitat['c_timetable1']=$week_names[0];
$gl_caption_activitat['c_timetable2']=$week_names[1];
$gl_caption_activitat['c_timetable3']=$week_names[2];
$gl_caption_activitat['c_timetable4']=$week_names[3];
$gl_caption_activitat['c_timetable5']=$week_names[4];
$gl_caption_activitat['c_timetable6']=$week_names[5];
$gl_caption_activitat['c_timetable7']=$week_names[6];
$gl_caption['c_level_id'] = $gl_caption['c_level'] = 'Nivell';
$gl_caption['c_modality_id'] = $gl_caption['c_modality'] = 'Modalitat';
$gl_caption['c_needed_material'] = 'Material necessari';
$gl_caption['c_includes'] = 'Inclou';
$gl_caption['c_excludes'] = 'No inclou';
$gl_caption_element['c_element'] = 'Ítem';

$gl_caption_activitat['c_form_level1_title_minimum_age']='Característiques';
$gl_caption_activitat['c_form_level1_title_activitat']='Descripcions';
$gl_caption_activitat['c_form_level1_title_timetable']='Horaris';
$gl_caption_activitat['c_form_level1_title_price']='Preus';
$gl_caption_activitat['c_form_level1_title_needed_material']='Material';
$gl_caption_activitat['c_form_level1_title_file']='Arxius adjunts';
$gl_caption_activitat['c_form_level1_title_page_title']='SEO';
$gl_caption_activitat['c_form_level2_title_entered']='Dates';


$gl_caption['c_activitat_file_name'] = $GLOBALS['gl_caption']['c_file_name'];
$gl_caption['c_activitat_old_file_name'] = $GLOBALS['gl_caption']['c_old_file_name'];

$gl_caption_activitat['c_selected']='Activitats seleccionades';

$gl_caption['c_ordre']='Ordre';

// public
$gl_caption['c_activitat'] = $gl_caption['c_activitat_id'] = 'Activitat';
$gl_caption['c_activitat'] = 'Activitat ( Menú i llistat )';
$gl_caption_list['c_activitat'] = 'Activitat';
$gl_caption['c_price_id']='Preus';
$gl_caption['c_price_month']='Preu mensual';
$gl_caption['c_price_quarter']='Preu trimestral';