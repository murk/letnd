<?

namespace Console\Command;

// use Nette\Security\Passwords;
use Console;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Style\SymfonyStyle;

// use Symfony\Component\Console\Question\Question;


final class Create extends Command {

	private $output;
	private $input;

	protected function configure() {
		$this->setName( 'create' )
		     ->setDescription( 'Crear nous elements - arxius update, classes php ...' )
		     ->addArgument( 'action', InputArgument::REQUIRED, "Acció a realitzar" )
		     ->addArgument( 'arg1', InputArgument::OPTIONAL, "" )
		     ->setAliases( [ 'new' ] );
	}

	protected function execute( InputInterface $input, OutputInterface $output ) {
		$this->input  = $input;
		$this->output = $output;
		$io           = new SymfonyStyle( $input, $output );

		$actions = [ 'update-file' ];
		$action  = $input->getArgument( 'action' );

		$arg1 = $input->getArgument( 'arg1' );

		if ( ! in_array( $action, $actions ) ) {


			$helper   = $this->getHelper( 'question' );
			$question = new ChoiceQuestion(
				'Acció a realitzar?',
				$actions );

			$question->setErrorMessage( 'L\'acció %s no es valid.' );

			$action = $helper->ask( $input, $output, $question );

		}

		$action = str_replace( '-', '_', $action );
		$this->$action( $action, $io, ...[ $arg1 ] );

		return 0;
	}

	private function update_file( $action, SymfonyStyle $io ) {

		$update_dir = DOCUMENT_ROOT . 'common/update';

		foreach ( array_reverse( glob( "$update_dir/[0-9][0-9][0-9][0-9].php" ) ) as $nombre_fichero ) {
			$path_info = pathinfo( $nombre_fichero );
			$name      = $path_info['filename'];

			$name ++;
			$date = date( "d-m-Y" );
			$file = "$update_dir/$name.php";

			file_put_contents( $file,
				"<?
/*
$date

*/

"
			);

			$curl        = curl_init( "http://localhost:63342/api/file?file=$file&line=7" );
			curl_setopt( $curl, CURLOPT_RETURNTRANSFER, false );
			curl_exec( $curl );
			curl_close( $curl );

			break;
		}


	}
}