<?php
// menus eina
if (!defined('PRODUCT_MENU_PRODUCT')){
define('PRODUCT_MENU_PRODUCT','Productos');
define('PRODUCT_MENU_NEW','Nuevo producto');
define('PRODUCT_MENU_LIST','Listar productos');
define('PRODUCT_MENU_LIST_BIN','Papelera');
define('PRODUCT_MENU_FAMILY','Famílias');
define('PRODUCT_MENU_FAMILY_NEW','Crear família');
define('PRODUCT_MENU_FAMILY_LIST','Listar famílas');
define('PRODUCT_MENU_FAMILY_LIST_BIN','Papelera');
define('PRODUCT_MENU_BRAND','Marcas');
define('PRODUCT_MENU_BRAND_NEW','Crear marca');
define('PRODUCT_MENU_BRAND_LIST','Listar marcas');
define('PRODUCT_MENU_BRAND_LIST_BIN','Papelera');
define('PRODUCT_MENU_TAX','Impuestos');
define('PRODUCT_MENU_TAX_NEW','Crear impuesto');
define('PRODUCT_MENU_TAX_LIST','Listar impuesto');
define('PRODUCT_MENU_TAX_LIST_BIN','Papelera');
//--------------- a traduir -----------------
define('PRODUCT_MENU_COLOR','Colores');
define('PRODUCT_MENU_COLOR_NEW','Crear colores');
define('PRODUCT_MENU_COLOR_LIST','Listar colores');
define('PRODUCT_MENU_COLOR_LIST_BIN','Papelera');
define('PRODUCT_MENU_MATERIAL','Materiales');
define('PRODUCT_MENU_MATERIAL_NEW','Crear material');
define('PRODUCT_MENU_MATERIAL_LIST','Listar materiales');
define('PRODUCT_MENU_MATERIAL_LIST_BIN','Papelera');
define('PRODUCT_MENU_VARIATION','Variaciones de precio');
define('PRODUCT_MENU_VARIATION_NEW','Crear variación');
define('PRODUCT_MENU_VARIATION_LIST','Listar variaciones');
define('PRODUCT_MENU_VARIATION_DEFAULT','Variaciones por defecto');
define('PRODUCT_MENU_VARIATION_CATEGORY','Categorias variationes');
define('PRODUCT_MENU_VARIATION_CATEGORY_NEW','Crear Categoría');
define('PRODUCT_MENU_VARIATION_CATEGORY_LIST','Listar Categorias');
define('PRODUCT_MENU_VARIATION_LIST_BIN','Papelera');

define('PRODUCT_MENU_PROMCODE','Código promocional');
define('PRODUCT_MENU_PROMCODE_NEW','Crear código');
define('PRODUCT_MENU_PROMCODE_LIST','Listar código');
define('PRODUCT_MENU_PROMCODE_LIST_BIN','Papelera');

define('PRODUCT_MENU_SECTOR','Sectores');
define('PRODUCT_MENU_SECTOR_NEW','Crear sector');
define('PRODUCT_MENU_SECTOR_LIST','Listar sectors');
define('PRODUCT_MENU_SECTOR_LIST_BIN','Papelera');

define('SELLPOINT_MENU_SELLPOINT','Puntos de venta');
define('SELLPOINT_MENU_SELLPOINT_NEW','Nuevo punto');
define('SELLPOINT_MENU_SELLPOINT_LIST','Listar puntos');
define('SELLPOINT_MENU_SELLPOINT_BIN','Papelera de reciclage');

define('PRODUCT_MENU_SPEC','Especificaciones');
define('PRODUCT_MENU_SPEC_NEW','Crear especificación');
define('PRODUCT_MENU_SPEC_LIST','Listar especificaciones');
define('PRODUCT_MENU_SPEC_CATEGORY','Categorías especificaciones');
define('PRODUCT_MENU_SPEC_CATEGORY_NEW','Crear categoría');
define('PRODUCT_MENU_SPEC_CATEGORY_LIST','Llistar categorías');
define('PRODUCT_MENU_SPEC_LIST_BIN','Papelera de reciclatge');

define('PRODUCT_MENU_QUANTITYCONCEPT','Conceptos cantidad');
define('PRODUCT_MENU_QUANTITYCONCEPT_NEW','Crear concepto');
define('PRODUCT_MENU_QUANTITYCONCEPT_LIST','Listar conceptos');
}



$gl_caption['c_images_variation_title']='Variación';
$gl_caption_color['c_color_name']='Nombre del color';
$gl_caption['c_color_id']='Color';
$gl_caption['c_material_id']='Material';
$gl_caption['c_choose']='Escoge';
$gl_caption_product['c_temperature_unit']='';
$gl_caption_product['c_util_life_unit']='';
$gl_caption_product['c_others1']='';
$gl_caption_product['c_others2']='';
$gl_caption_product['c_others3']='';
$gl_caption_product['c_others4']='';
$gl_caption_product['c_auto_ref']='Automática';

$gl_caption_product['c_format']='Formato';
$gl_caption_product['c_weight']='Peso';
$gl_caption_product['c_gram']='gramos';
$gl_caption_product['c_kilo']='quilo/s';
$gl_caption_product['c_weight_unit']='';
$gl_caption_product['c_height_unit']='';
$gl_caption_product['c_height']='Altura';
$gl_caption_product['c_mm']='mm';
$gl_caption_product['c_cm']='cm';
$gl_caption_product['c_mt']='mt';
$gl_caption_product['c_km']='km';
$gl_caption_product['c_feet']='pies';
$gl_caption_product['c_temperature']='Temperatura';
$gl_caption_product['c_celcius']='celcius';
$gl_caption_product['c_fahrenheit']='fahrenheit';
$gl_caption_product['c_util_life']='Vida útil';
$gl_caption_product['c_hours']='horas';
$gl_caption_product['c_days']='dias';
$gl_caption_product['c_years']='años';
$gl_caption_product['c_meters']='metros';
$gl_caption_product['c_km']='kilometros';
$gl_caption_product['c_months']='meses';
$gl_caption_product['c_caracteristics']='Características';
$gl_caption_product['c_other_caracteristics']='Otras Características <a href="/admin/?menu_id=4110">( configuración )</a>';
$gl_caption_product['c_power']='Potencia';
$gl_caption_product['c_alimentacion']='Alimentación';
$gl_caption_product['c_certification']='Certificado/s';
$gl_caption_material['c_material']='Nombre del material';


//-----------------------------------
$gl_caption['c_ref'] = 'Ref';
$gl_caption_product['c_home']='Destacado';
$gl_caption_product['c_ref_number'] = 'Ref. automática';
$gl_caption_product['c_ref_supplier'] = 'Ref Proveedor';
$gl_caption_product['c_product_private'] = 'Nombre privado';
$gl_caption['c_family_id'] = 'Familia';
$gl_caption_product['c_subfamily_id'] = 'Subfamilia';
$gl_caption['c_family_content_list'] = 'Descripció breve en el listado';
$gl_caption['c_family_description']='Descripción de la familia';
$gl_caption_product['c_subfamily_id_format']='#%s#%s';
$gl_caption_product['c_subsubfamily_id'] = 'Subfamilia';
$gl_caption_product['c_subsubfamily_id_format']='#%s#%s';
$gl_caption_product['c_pvp'] = 'PVP';
$gl_caption_product['c_price_cost'] = 'Precio de coste';
$gl_caption['c_price'] = 'Precio';
$gl_caption_product['c_destacat']='Destacado';
$gl_caption_product['c_pvd'] = 'PVD';
$gl_caption_product['c_pvd1'] = 'Tarifa 1';
$gl_caption_product['c_pvd2'] = 'Tarifa 2';
$gl_caption_product['c_pvd3'] = 'Tarifa 3';
$gl_caption_product['c_pvd4'] = 'Tarifa 4';
$gl_caption_product['c_pvd5'] = 'Tarifa 5';
$gl_caption_product['c_price_consult'] = 'a consultar';
$gl_caption_product['c_price_consult_1'] = 'Precio a consultar';
$gl_caption_product['c_brand_id'] = $gl_caption_product['c_brand'] = 'Marca';
$gl_caption_product['c_sell'] = 'Venda web?';
$gl_caption['c_guarantee']='Garantía';
$gl_caption_product['c_guarantee_period']='Periodo garantía';
$gl_caption_product['c_da']='dias';
$gl_caption_product['c_mo']='meses';
$gl_caption_product['c_yr']='años';
$gl_caption['c_status'] = 'Estado';
$gl_caption_product['c_free_send'] = 'Permitir envio gratuito';
$gl_caption_product['c_ordre'] = 'Orden';
$gl_caption['c_product_title'] = 'Nombre del producto';
$gl_caption['c_product_subtitle'] = 'Subtítulo';
$gl_caption_product['c_product_description'] = 'Descripción del producto';
$gl_caption['c_prepare'] = 'En preparación';
$gl_caption['c_review'] = 'Para revisar';
$gl_caption['c_onsale'] = 'Para vender';
$gl_caption['c_nostock'] = 'Fuera de stock';
$gl_caption['c_nocatalog'] = 'Descatalogado';
$gl_caption['c_archived'] = 'Arxivado';
$gl_caption['c_tax_id'] = 'I.V.A (%)';
$gl_caption['c_tax_included'] = 'IVA incluído';
$gl_caption['c_tax_not_included'] = 'IVA no incluído';
$gl_caption_product['c_offer'] = 'Oferta';
$gl_caption_product['c_discount'] = 'Descuento (%)';
$gl_caption_product['c_discount_wholesaler'] = 'Descuento distribuidor(%)';
$gl_caption_product['c_margin'] = 'Margen (%)';
$gl_caption_product['c_ean'] = 'EAN';
$gl_caption_product['c_price_unit'] = 'Precio por';
$gl_caption_product['c_unit'] = 'Unidad';
$gl_caption_product['c_ten'] = 'Decena';
$gl_caption_product['c_hundred'] = 'Centenar';
$gl_caption_product['c_thousand'] = 'Mil';
$gl_caption_product['c_couple'] = 'Pareja';
$gl_caption_product['c_dozen'] = 'Dozena';
$gl_caption_product['c_m2'] = 'm2';
$gl_caption_product['c_data']='Datos del producto';
$gl_caption_product['c_general_data']='Datos generales';
$gl_caption_product['c_descriptions']='Descripciones';
$gl_caption_product['c_description']='Descripción';
$gl_caption_product['c_data_product']='Datos del producto';
$gl_caption_product['c_data_web']='Venda Web';
$gl_caption_product['c_stock']='Stock';
$gl_caption_product['c_sells']='Unidades vendidas';
$gl_caption_product['c_file']='Archivos públicos';
$gl_caption_product['c_discount_fixed_price']='Descuento precio fijo';
$gl_caption_product['c_discount_fixed_price_wholesaler']='Descuento precio fijo distribuidor';
$gl_caption_product['c_selected']='Productos seleccionados';
$gl_caption_product['c_videoframe']='Video (youtube, metacafe...)';


$gl_caption['c_observations'] = 'Observaciones';
$gl_caption['c_check_parent_id']='- ninguna familia -';
$gl_caption['c_check_brand_id']='Escoge marca';




$gl_caption['c_brand_name']='Nombre de la marca';
$gl_caption_family['c_parent_id']='Familia a la que pertenece';
$gl_caption_family['c_family']='Nombre de la familia';
$gl_caption_family['c_family_alt']='Familia "title/alt"';
$gl_caption_family['c_family_subtitle'] = 'Subtítulo';
$gl_caption_family['c_view_subfamilys']='ver familias';
$gl_caption_family['c_add_subfamily_button']=$gl_caption_family['c_add_subfamily']='Añadir subfamilia';
$gl_caption_family['c_filter_parent_id'] = 'Pertenece a cualquier familia';
$gl_caption_family['c_search_title'] = 'Buscar familias';




$gl_caption_tax['c_tax_name']='Nombre del impuesto';
$gl_caption_tax['c_tax_value']='Valor (%)';
$gl_caption_tax['c_equivalencia']='Equivalencia (%)';




$gl_caption_product['c_search_title']= 'Busqueda de productos';
$gl_caption_product['c_by_nif']= 'Por Ref.';
$gl_caption_product['c_by_words']= 'Por palabras.';


$gl_caption['c_observations'] = 'Observaciones';
$gl_caption['c_ordre']='Orden';
$gl_caption['c_withinstall']='Pvp con instalación';

$gl_caption ['c_noproduct']= $gl_messages_product ['no_records']= 'En estos momentos no hay ningún producto para esta categoría.';
$gl_caption_product['c_model']='Modelo';
$gl_caption['c_year']='Año';
$gl_caption['c_ishandmade']='Hecho a mano';



// No traduit
if(!defined( 'CUSTOMER_MENU_CUSTOMER' )){
	define( 'CUSTOMER_MENU_CUSTOMER', 'Clientes' );
	define( 'PRODUCT_MENU_CUSTOMER', 'Clientes' );
	define( 'CUSTOMER_MENU_CUSTOMER_NEW', 'Insertar cliente' );
	define( 'CUSTOMER_MENU_CUSTOMER_LIST', 'Listar clientes' );
	define( 'CUSTOMER_MENU_CUSTOMER_BIN', 'Papelera de reciclaje' );

	define( 'CUSTOMER_MENU_ORDER', 'Pedidos' );
	define( 'PRODUCT_MENU_ORDER', 'Pedidos' );
	define( 'CUSTOMER_MENU_ORDER_NEW', 'Nuevo pedido' );
	define( 'CUSTOMER_MENU_ORDER_LIST', 'Listar pedidos' );
	define( 'CUSTOMER_MENU_ORDER_LIST_BIN', 'Papelera de reciclaje' );

	define( 'CUSTOMER_MENU_WHOLESALER', 'Distribuidores' );
	define( 'PRODUCT_MENU_WHOLESALER', 'Distribuidores' );
	define( 'CUSTOMER_MENU_WHOLESALER_NEW', 'Insertar distribuidor' );
	define( 'CUSTOMER_MENU_WHOLESALER_LIST', 'Listar distribuidor' );
	define( 'CUSTOMER_MENU_WHOLESALER_BIN', 'Papelera de reciclaje' );

	define( 'CUSTOMER_MENU_RATE', 'Tarifas envio' );
	define( 'PRODUCT_MENU_RATE', 'Tarifas envio' );
	define( 'CUSTOMER_MENU_RATE_NEW', 'Añadir país' );
	define( 'CUSTOMER_MENU_RATE_PROVINCIA_NEW', 'Añadir provincia' );
	define( 'CUSTOMER_MENU_RATE_LIST', 'Listar tarifas' );
	define( 'PRODUCT_MENU_COUNTRY', 'Selección países' );
	define( 'CUSTOMER_MENU_COUNTRY_LIST', 'Selección países' );

	define( 'CUSTOMER_MENU_COURIER', 'Mensajeros' );
	define( 'PRODUCT_MENU_COURIER', 'Mensajeros' );
	define( 'CUSTOMER_MENU_COURIER_NEW', 'Crear mensajero' );
	define( 'CUSTOMER_MENU_COURIER_LIST', 'Llistar mensajeros' );
	define( 'CUSTOMER_MENU_COURIER_LIST_BIN', 'Paperera' );
}

$gl_caption ['c_quantity']='Cantidad';
$gl_caption ['c_product_title']='Producto';
$gl_caption ['c_product_tax']='I.V.A'; // això ha d'anar a la taula de productes
$gl_caption ['c_product_basetax']='Precio/unidad';
$gl_caption ['c_product_base']='Precio/unidad';
$gl_caption ['c_product_total_base']='Total';
$gl_caption ['c_product_total_tax']='I.V.A'; // això ha d'anar a la taula de productes
$gl_caption ['c_product_total_basetax']='Total';
$gl_caption ['c_total']='Total';
$gl_caption ['c_total_base']='Subtotal';
$gl_caption ['c_total_tax']='I.V.A'; // això ha d'anar a la taula de productes
$gl_caption ['c_total_equivalencia']='Recargo equivalencia';
$gl_caption ['c_total_basetax']='Total';
$gl_caption ['c_rate_basetax']='Gastos de envio';
$gl_caption ['c_rate_base']='';
$gl_caption ['c_rate_tax']='';
$gl_caption ['c_deposit']='Depósito';
$gl_caption ['c_deposit_rest']='Importe restante';
$gl_caption ['c_all_basetax']='Total';
$gl_caption ['c_all_base'] = 'Base imponible';
$gl_caption ['c_all_tax']='I.V.A';
$gl_caption ['c_all_equivalencia']='Recargo equivalencia';
$gl_caption ['c_promcode_basetax']='Descuento código promocional';
$gl_caption['c_product_old_base'] = "";
$gl_caption['c_product_old_basetax'] = "";
$gl_caption['c_product_discount'] = "Descuento";
$gl_caption['c_product_discount_fixed'] = "";

$gl_caption_customer['c_prefered_language'] = 'Idioma';
$gl_caption_customer['c_form_title'] = 'Datos del cliente';
$gl_caption_customer['c_customer_id'] = 'Núm.';
$gl_caption_customer['c_customer_id_title'] = 'Número';
$gl_caption_customer['c_treatment'] = 'Trato';
$gl_caption_customer['c_name'] = 'Nombre';
$gl_caption_customer['c_company']='Empresa';
$gl_caption_customer['c_surname'] = 'Apellidos';
$gl_caption_customer['c_birthdate'] = 'Fecha de nacimiento';
$gl_caption_customer['c_mail'] = "Dirección e-mail";
$gl_caption_customer['c_password'] = 'Contraseña';
$gl_caption_customer['c_password_repeat'] = 'Repetir contraseña';
$gl_caption['c_telephone'] = 'Teléfono';
$gl_caption_customer['c_telephone_mobile'] = 'Teléfono Móvil';
$gl_caption['c_has_equivalencia'] = 'Recargo equivalencia';
$gl_caption['c_has_no_vat'] = 'No añadir IVA';
$gl_caption_customer['c_wholesaler_rate'] = 'Tarifa';
$gl_caption_customer['c_wholesaler_rate_1'] = 'Tarifa 1';
$gl_caption_customer['c_wholesaler_rate_2'] = 'Tarifa 2';
$gl_caption_customer['c_wholesaler_rate_3'] = 'Tarifa 3';
$gl_caption_customer['c_wholesaler_rate_4'] = 'Tarifa 4';
$gl_caption_customer['c_wholesaler_rate_5'] = 'Tarifa 5';
$gl_caption['c_dni'] = 'CIF / NIF';
$gl_caption_customer['c_comment'] = 'Observaciones';
$gl_caption_customer['c_bin'] = '';
$gl_caption_customer['c_enteredc'] = "Fecha de entrada";
$gl_caption['c_treatment_sra'] = "Sra.";
$gl_caption['c_treatment_sr'] = "Sr.";
$gl_caption ['c_same_address']='La misma dirección de facturación';
$gl_caption_customer['c_list_orders'] = 'Ver pedidos';
$gl_caption_customer ['c_form_title2']='Datos de acceso';
$gl_caption_customer ['c_form_title3']='Datos de facturación';
$gl_caption_customer ['c_form_title3_list']='Datos de facturación';
$gl_caption_customer ['c_form_title_wholesaler']='Distribuidor';
$gl_caption_customer ['c_form_title4']='Dirección de entrega';
$gl_caption_customer['c_add_button'] = 'Añadir dirección';

$gl_caption['c_order_id'] = $gl_caption['c_order'] = 'Pedido';
$gl_caption_order['c_customer_id'] = 'Cliente';
$gl_caption_order['c_total_base'] = 'Base imponible';
$gl_caption_order['c_total_tax'] = 'I.V.A.';
$gl_caption_order['c_total_basetax'] = 'Total';
$gl_caption_order['c_entered_order'] = 'Fecha del pedido';
$gl_caption_order['c_entered_order_short'] = 'Fecha';
$gl_caption_order['c_customer_id'] = 'Cliente';
$gl_caption_order['c_product_id'] = 'Producto';
$gl_caption_order['c_shipped'] = 'Fecha de envio';
$gl_caption_order['c_delivered'] = 'Fecha de entrega';
$gl_caption_order['c_order_status'] = 'Estado';
$gl_caption_order['c_deposit_payment_method'] = 'Método de pago depósito';
$gl_caption_order_list['c_deposit_payment_method'] = 'M. pago depósito';
$gl_caption_order['c_payment_method'] = 'Método de pago';
$gl_caption_order_list['c_payment_method'] = 'M. pago';
$gl_caption_order['c_comment'] = 'Observaciones privadas';
$gl_caption['c_customer_comment'] = 'Observaciones cliente';
$gl_caption['c_is_gift_card'] = 'Cheque regalo';
$gl_caption_order['c_address_invoice_id'] = '';
$gl_caption_order['c_filter_order_status'] = 'Todos los estados';
$gl_caption_order['c_filter_payment_method'] = 'Todos los métodos';
$gl_caption_order['c_customer_id_format'] = '%s %s';
$gl_caption_order['c_file'] = 'Archivos públicos';
$gl_caption_order['c_button-edit-admin'] = '';


$gl_caption_order['c_order_status_paying'] = 'Pendiente de pago';
$gl_caption_order['c_order_status_failed'] = 'Pago fallido';
$gl_caption_order['c_deposit_payed'] = 'Deposito pagado';
$gl_caption_order['c_order_status_pending'] = 'Pago pendiente de aceptación';
$gl_caption_order['c_order_status_denied'] = 'Pago rechazado';
$gl_caption_order['c_order_status_voided'] = 'Pago anulado';
$gl_caption_order['c_order_status_refunded'] = 'Pago devuelto';
$gl_caption_order['c_order_status_completed'] = 'Pagado';
$gl_caption_order['c_order_status_preparing'] = 'En preparación';
$gl_caption_order['c_order_status_shipped'] = 'Enviado';
$gl_caption_order['c_order_status_delivered'] = 'Entregado';
$gl_caption_order['c_order_status_collect'] = 'Pendiente de recoger';

$gl_caption_order['c_deposit_status'] = 'Estado depósito';

$gl_caption_order['c_deposit_status_paying'] = 'Pendiente de pago';
$gl_caption_order['c_deposit_status_failed'] = 'Pago fallido';
$gl_caption_order['c_deposit_status_deposit'] = 'Deposito pagado';
$gl_caption_order['c_deposit_status_pending'] = 'Pago pendiente de aceptación';
$gl_caption_order['c_deposit_status_denied'] = 'Pago rechazado';
$gl_caption_order['c_deposit_status_voided'] = 'Pago anulado';
$gl_caption_order['c_deposit_status_refunded'] = 'Pago devuelto';
$gl_caption_order['c_deposit_status_completed'] = 'Pagado';

$gl_caption_order['c_deposit_rest'] = 'Resto';

// Enviament
$gl_caption['c_send_date'] = "Envio";
$gl_caption['c_send_date_shipped'] = $gl_caption_order['c_shipped'];
$gl_caption['c_send_date_delivered'] = $gl_caption_order['c_delivered'];
$gl_caption['c_send_date_no_date_shipped'] = 'Sin fecha envio';
$gl_caption['c_send_date_no_date_delivered'] = 'Sin fecha entrega';

$gl_caption['c_payment_method_none'] = 'Ninguno';
$gl_caption['c_payment_method_paypal'] = 'Paypal';
$gl_caption['c_payment_method_account'] = 'Transferencia bancaria';
$gl_caption['c_payment_method_ondelivery'] = 'Contrareembolso';
$gl_caption['c_payment_method_directdebit'] = 'Recibo bancario';
$gl_caption['c_payment_method_4b'] = 'Pasarela 4B';
$gl_caption['c_payment_method_lacaixa'] = 'Tarjeta de crédito';
$gl_caption['c_payment_method_cash'] = 'Efectivo';
$gl_caption['c_payment_method_physicaltpv'] = 'TPV físico';

$gl_caption['c_deposit_payment_method_none'] = 'Ninguno';
$gl_caption['c_deposit_payment_method_paypal'] = $gl_caption['c_payment_method_paypal'];
$gl_caption['c_deposit_payment_method_account'] = $gl_caption['c_payment_method_account'];
$gl_caption['c_deposit_payment_method_ondelivery'] = $gl_caption['c_payment_method_ondelivery'];
$gl_caption['c_deposit_payment_method_directdebit'] = $gl_caption['c_payment_method_directdebit'];
$gl_caption['c_deposit_payment_method_4b'] = $gl_caption['c_payment_method_4b'];
$gl_caption['c_deposit_payment_method_lacaixa'] = $gl_caption['c_payment_method_lacaixa'];
$gl_caption['c_deposit_payment_method_cash'] = $gl_caption['c_payment_method_cash'];
$gl_caption['c_deposit_payment_method_physicaltpv'] = $gl_caption['c_payment_method_physicaltpv'];


$gl_caption['c_collect'] = 'Recogida en tienda física';
$gl_caption_list['c_collect'] = 'Recogida';
$gl_caption_order['c_edit_button'] = 'Editar pedido';
$gl_caption_order['c_show_record'] = 'Ver pedido';
$gl_caption_order['c_show_invoice'] = 'Ver factura';
$gl_caption_order['c_show_invoice_refund'] = 'Ver factura devolución';
$gl_caption_order['c_pay_now'] = 'Efectuar pago';
$gl_caption_order['c_no_shipped'] = 'No enviado';
$gl_caption_order['c_no_delivered'] = 'No entregado';


$gl_caption_order ['c_order_title_admin']='Datos del pedido';
$gl_caption_order ['c_order_title']='Información del pedido';
$gl_caption_order ['c_order_title2']='Productos';
$gl_caption_order ['c_order_title3']='Datos de facturación';
$gl_caption_order ['c_order_title4']='Dirección de entrega';
$gl_caption_order ['c_order_title5']='Forma de pago';
$gl_caption_order ['c_order_title_invoice_customer']='Information cliente';
$gl_caption_order ['c_order_num']='num. ';
$gl_caption ['c_product_variation_ids']='';
$gl_caption ['c_product_variations']='Variaciones';
$gl_caption ['c_product_variation_categorys']='';

$gl_caption_order['c_invoice_id'] = 'Factura';
$gl_caption_order['c_invoice_refund_id'] = 'Factura devolución';
$gl_caption_order['c_invoice_date'] = 'Fecha';
$gl_caption_order['c_invoice_refund_date'] = 'Fecha';
$gl_caption_order['c_view_invoice'] = 'Ver factura';
$gl_caption_order['c_view_invoice_refund'] = 'Ver factura devolución';
$gl_caption_order ['c_order_title_invoice_customer']='Datos del cliente';

$gl_caption_orderitem = $gl_caption_order;
$gl_caption_orderitem_list = $gl_caption_order_list;

$gl_caption ['c_address_id']='';
$gl_caption ['c_delivery_id']='';
$gl_caption_address ['c_customer_id']='';
$gl_caption_address['c_a_name'] = 'Nombre';
$gl_caption_address['c_a_surname'] = 'Apellido';
$gl_caption_address['c_address'] = 'Dirección';
$gl_caption_address['c_zip'] = 'Código postal';
$gl_caption_address['c_city'] = 'Población';
$gl_caption_address['c_country_id'] = 'País';
$gl_caption_address['c_invoice'] = '';
$gl_caption_address['c_edit_address'] = 'Editar dirección';
$gl_caption_address['c_new_address'] = 'Añadir dirección';

$gl_caption_variation['c_variation'] = 'Variación';
$gl_caption_variation['c_variation_subtitle'] = 'Subtítulo';
$gl_caption_variation['c_variation_description'] = 'Descripción';
$gl_caption_variation['c_list_pick_default'] = 'Predet.';
$gl_caption_variation['c_variation_category_id'] = 'Categoria';
$gl_caption_variation['c_variation_status'] = 'Estado';
$gl_caption['c_disabled'] = 'Deshabilitada';
$gl_caption_variation['c_variation_pvp'] = 'Incremento PVP';
$gl_caption_variation['c_variation_pvd'] = 'Incremento PVD 1';
$gl_caption_variation['c_variation_pvd2'] = 'Incremento PVD 2';
$gl_caption_variation['c_variation_pvd3'] = 'Incremento PVD 3';
$gl_caption_variation['c_variation_pvd4'] = 'Incremento PVD 4';
$gl_caption_variation['c_variation_pvd5'] = 'Incremento PVD 5';
$gl_caption_variation_category['c_variation_category'] = 'Categoría';
$gl_caption_variation_category['c_variation_description'] = 'Descripción';
$gl_caption_product['c_edit_variations'] = $gl_caption_product['c_edit_variations_button'] = 'Variaciones';
$gl_caption_product['c_edit_specs'] = $gl_caption_product['c_edit_specs_button'] = 'Especificaciones';
$gl_caption_product['c_edit_relateds'] = $gl_caption_product['c_edit_relateds_button'] = 'Relacionados';
$gl_caption['c_no_assigned'] = 'No hay ninguna variación definida';
$gl_caption_product['c_data_variations'] = 'Variaciones del producto';
$gl_caption_product['c_price_base'] = 'Precio base del producto';
$gl_caption['c_data_digital'] = 'Producto digital';
$gl_caption_product['c_download'] = 'Arxivos descargables';
$gl_caption_product['c_filter_family_id'] = 'Todas las familias';
$gl_caption_product['c_filter_subfamily_id'] = 'Todas las subfamilias';
$gl_caption_product['c_filter_subsubfamily_id'] = 'Todas las subfamilias';
$gl_caption_product['c_filter_status'] = 'Todos los estados';
$gl_caption_product['c_filter_destacat'] = 'Destacados / No destacados';
$gl_caption_product['c_destacat_0'] = 'No destacats';
$gl_caption_product['c_destacat_1'] = 'Destacats';
$gl_caption_product['c_filter_prominent'] = 'En Promoción / No promoción';
$gl_caption_product['c_prominent_0'] = 'No promoción';
$gl_caption_product['c_prominent_1'] = 'En promoción';
$gl_caption_product['c_stockcontrol']='Control de stock';
$gl_caption_product['c_last_stock_units_public']='Aviso últimas unidades público';
$gl_caption_product['c_last_stock_units_admin']='Aviso últimas unidades admin';
$gl_caption_brand['c_brand_description']='Descripción';
$gl_caption['c_prominent']='En promoción';
$gl_caption_list['c_prominent']='Prom.';
$gl_caption_product['c_is_new_product']='Producto nuevo';
$gl_caption_product_list['c_is_new_product']='Nuevo';
$gl_caption_variation['c_variation_stock']='Stock';
$gl_caption_variation['c_variation_last_stock_units_public']='Aviso público';
$gl_caption_variation['c_variation_last_stock_units_admin']='Aviso admin';
$gl_caption_variation['c_ref']='Ref';
$gl_caption_variation['c_ref_supplier']='Ref Proveedor';
$gl_caption_variation['c_variation_ref']='Ref';
$gl_caption_variation['c_variation_ref_number']='Ref. Automática';
$gl_caption_variation['c_variation_ref_supplier']='Ref Proveedor';

$gl_caption['c_product_file_name'] = $GLOBALS['gl_caption']['c_file_name'];
$gl_caption['c_product_old_file_name'] = $GLOBALS['gl_caption']['c_old_file_name'];

$gl_caption_variation['c_variation_sells']='Unidades vendidas';
$gl_caption['c_add_variations']='Añadir variaciones';
$gl_caption_variation['c_selected']='Variaciones para añadir';
$gl_caption['c_new_variations']='Variaciones añadidas';
$gl_caption['c_variation_category_select']='Todas las categorias';
$gl_caption_variation['c_filter_variation_category_id']='Todas las categorias';


$gl_caption['c_send_password'] = 'Enviar contraseña por email';
$gl_caption['c_password_sent'] = 'Se ha enviado una nueva contraseña a su dirección de email';
$gl_caption['c_password_mail_subject'] = 'Su nueva contraseña';
$gl_caption['c_password_mail_text_1'] = 'Su nueva contraseña para acceder a nuestra tienda es';
$gl_caption['c_password_mail_text_2'] = 'Puede cambiar la contraseña en el apartado "Su cuenta" de nuestra página web';
$gl_messages_customer['password_sent'] = 'Se ha enviado la contraseña al cliente';
$gl_messages_customer['password_not_sent'] = 'No se ha podido enviar contrasenña, por favor clique a "enviar contraseña por email"';

$gl_caption_customer['c_activate_button'] = 'Activar cliente';
$gl_caption_customer['c_deactivate_button'] = 'Desactivar cliente';
$gl_caption_customer['c_set_wholesaler_button'] = 'Hacer distribuïdor';
$gl_caption_customer['c_unset_wholesaler_button'] = 'Quitar distribuïdor';
$gl_caption_customer['c_activated_mail_message'] = 'Su cuenta en %s ha sido activada, ya puede acceder a nuestra tienda.';
$gl_caption_customer['c_activated_mail_subject'] = 'Su cuenta ha sido activada';
$gl_caption_customer['c_filter_activated'] = 'Activados / Desactivados';
$gl_caption_customer['c_activated_0'] = 'Desactivados';
$gl_caption_customer['c_activated_1'] = 'Activados';

$gl_messages_customer['activated'] = 'El cliente ha sido activado';
$gl_messages_customer['deactivated'] = 'El cliente ha sido desactivado';
$gl_messages_customer['not_activated'] = 'No se ha podido activar el cliente, por favor vuelve a intentarlo';
$gl_messages_customer['set_wholesaler'] = 'El cliente ahora es distribuïdor';
$gl_messages_customer['unset_wholesaler'] = 'El cliente ya no es distribuïdor';

$gl_caption_family['c_destacat']='Destacada';

$gl_caption['c_promcode'] = 'Código promocional';
$gl_caption_promcode['c_discount_pvp'] = 'Descuento PVP';
$gl_caption_promcode['c_discount_pvd'] = 'Descuento tarifa 1';
$gl_caption_promcode['c_discount_pvd2'] = 'Descuento tarifa 2';
$gl_caption_promcode['c_discount_pvd3'] = 'Descuento tarifa 3';
$gl_caption_promcode['c_discount_pvd4'] = 'Descuento tarifa 4';
$gl_caption_promcode['c_discount_pvd5'] = 'Descuento tarifa 5';
$gl_caption_promcode['c_discount_type'] = 'Tipo de descuento';
$gl_caption_promcode['c_discount_type_fixed'] = 'Importe fijo';
$gl_caption_promcode['c_discount_type_percent'] = 'Porcentaje';
$gl_caption_promcode['c_minimum_amount'] = 'Gasto mínimo';
$gl_caption_promcode['c_start_date'] = 'Fecha inicial';
$gl_caption_promcode['c_end_date'] = 'Fecha final';
$gl_caption['c_promcode_discount'] = 'Dto. código promocional';

$gl_caption['c_sector']='Sector';
$gl_caption['c_sector_id']='Sector';
$gl_caption_sector['c_sector_description']='Descripción';


$gl_caption_brand['c_send_rate_spain']='España';
$gl_caption_brand['c_send_rate_france']='Francia';
$gl_caption_brand['c_send_rate_andorra']='Andorra';
$gl_caption_brand['c_send_rate_world']='Resto del mundo';
$gl_caption['c_minimum_buy_free_send']='Compra mínima';

/*  SELLPOINT  */
/*--------------- nomès castellà i català, no traduit a la resta -------------------*/


$gl_caption_sellpoint['c_sellpoint'] = 'Título';
$gl_caption_sellpoint['c_sellpoint_description'] = 'Descripción';
$gl_caption_sellpoint['c_empresa'] = 'Empresa';
$gl_caption_sellpoint['c_adress'] = 'Dirección';
$gl_caption_sellpoint['c_comunitat_id'] = 'Comunidad';
$gl_caption_sellpoint['c_provincia_id'] = 'Provincia';
$gl_caption_sellpoint['c_provincia_id_caption'] = 'Ninguna provincia';
$gl_caption_sellpoint['c_poblacio'] = 'Població';
$gl_caption_sellpoint['c_cp'] = 'Codigo postal';
$gl_caption_sellpoint['c_telefon'] = 'Teléfono';
$gl_caption_sellpoint['c_telefon2'] = 'Teléfono 2';
$gl_caption_sellpoint['c_fax'] = 'Fax';
$gl_caption_sellpoint['c_correu'] = 'E-mail';
$gl_caption_sellpoint['c_correu2'] = 'E-mail 2';
$gl_caption_sellpoint['c_url1'] = 'Web';
$gl_caption_sellpoint['c_url1_name'] = 'Nombre de la web';



$gl_caption_sellpoint['c_filter_comarca_id'] = 'Todas las comarcas';
$gl_caption_sellpoint['c_filter_municipi_id'] = 'Todos los municipios';
$gl_caption_sellpoint['c_filter_seller_kind'] = 'Todos los tipos';


// mapes
$gl_caption_sellpoint['c_edit_map'] = 'Ubicación';
$gl_caption_sellpoint['c_search_adress']='Buscar dirección';
$gl_caption_sellpoint['c_latitude']='latitud';
$gl_caption_sellpoint['c_longitude']='longitud';
$gl_caption_sellpoint['c_found_direction']='dirección encontrada';
$gl_caption_sellpoint['c_save_map'] = 'Guardar ubicación';
$gl_caption_sellpoint['c_click_map'] = 'Haz un click en un punto del mapa para definir la ubicación';
$gl_caption_sellpoint['c_form_map_title'] = 'Ubicación';
$gl_messages['point_saved'] = 'Nueva ubicación guardada correctamente';

$gl_caption_sellpoint['c_prepare'] = 'En preparación';
$gl_caption_sellpoint['c_review'] = 'Para revisar';
$gl_caption_sellpoint['c_public'] = 'Público';
$gl_caption_sellpoint['c_archived'] = 'Archivado';
$gl_caption_sellpoint['c_status'] = 'Estado';

$gl_caption_sellpoint['c_international'] = 'Internacional';
$gl_caption_sellpoint['c_country_id'] = 'País';
$gl_caption_sellpoint['c_seller_kind'] = 'Tipo';
$gl_caption_sellpoint['c_seller_kind_shop'] = 'Tienda';
$gl_caption_sellpoint['c_seller_kind_shops'] = 'Tiendas';
$gl_caption_sellpoint['c_seller_kind_dealer'] = 'Distribuidor';
$gl_caption_sellpoint['c_seller_kind_dealers'] = 'Distribuidores';

$gl_caption_sellpoint['c_sellpoint_file_name'] = $GLOBALS['gl_caption']['c_file_name'];
$gl_caption_sellpoint['c_sellpoint_old_file_name'] = $GLOBALS['gl_caption']['c_old_file_name'];

$gl_caption_sellpoint['c_filter_provincia_id'] = 'Todas las provincias';

// tarifes
$gl_caption['c_country_id'] = $gl_caption['c_country'] = 'País';
$gl_caption['c_provincia_id'] = $gl_caption['c_provincia'] = 'Provincia';
$gl_caption_rate['c_rate'] = 'Tarifa envio';
$gl_caption_rate['c_send_rate_world']='Resto de países';
$gl_caption_rate['c_rate_title']='Tarifas';
$gl_caption_rate['c_tax_included'] = 'IVA incluido';
$gl_caption_rate['c_tax_not_included'] = 'IVA no incluido';
$gl_caption_rate['c_rate_encrease_ondelivery'] = $gl_caption_rate['c_send_rate_encrease_ondelivery_world'] = 'Incremento contrareembolso';

// Especificacions
$gl_caption['c_spec_category_id'] = $gl_caption['c_spec_category'] = 'Categoría';
$gl_caption['c_spec_type'] = 'Tipo';
$gl_caption['c_ordre'] = 'Orden';
$gl_caption['c_spec'] = 'Especificación';
$gl_caption['c_spec_unit'] = 'Unidades';
$gl_caption['c_filter_spec_category_id'] = 'Todas las categorías';


$gl_caption['c_spec_type_checkbox']     = 'Casilla verificación';
$gl_caption['c_spec_type_int']          = 'Número';
$gl_caption['c_spec_type_text']         = 'Texto en idiomas';
$gl_caption['c_spec_type_text_no_lang'] = 'Texto';
$gl_caption['c_spec_type_textarea']     = 'Texto varias líneas';
$gl_caption['c_spec_type_select']       = 'Desplegable';

$gl_caption['c_data_specs']       = 'Especificaciones del producto';

// Couriers
$gl_caption['c_courier'] = $gl_caption['c_courier_id'] = "Mensajero";
$gl_caption['c_courier_url'] = "Web";
$gl_caption['c_courier_follow_code'] = "Código de seguimiento";
$gl_caption['c_default_courier'] = "Establecer com a predeterminado";
$gl_caption['c_default_courier_0'] = "";
$gl_caption['c_default_courier_1'] = "Predeterminado";

// Enviar emails segons/  estat
// admin
$gl_caption['c_prepared_emailed'] =
$gl_caption['c_delivered_emailed'] =
$gl_caption['c_shipped_emailed'] = '';
$gl_caption['c_send_mail_prepared'] = "Enviar email pedido preparado";
$gl_caption['c_send_mail_shipped'] = "Enviar email pedido enviado";
$gl_caption['c_send_mail_delivered'] = "Enviar email pedido entregado";
$gl_caption['c_resend_mail'] = "Reenviar email";
$gl_caption['c_mail_sent'] = "Email enviado el";
$gl_caption['c_mail_send_error'] = "No se ha podido enviar el mensaje, por favor vuelva a probarlo";
$gl_caption['c_sending_email'] = "Enviando email...";
$gl_caption['c_email_sended_correctly'] = "Email enviado correctamente";

// públic
$gl_caption['c_mail_subject_buyer_status_prepared'] = 'Tu pedido a %1$s está preparada';
$gl_caption['c_mail_subject_buyer_status_shipped'] = 'Tu pedido a %1$s está enviado';
$gl_caption['c_mail_subject_buyer_status_delivered'] = 'Tu pedido a %1$s ya está entregado';
$gl_caption['c_mail_subject_seller_status_prepared'] = 'Còpia de pedido preparado';
$gl_caption['c_mail_subject_seller_status_shipped'] = 'Còpia de pedido enviado';
$gl_caption['c_mail_subject_seller_status_delivered'] = 'Còpia de pedido entregado';

$gl_caption['c_mail_body_prepared'] = '
	<p>Hola %1$s,</p>
	<p>Queremos agradecer tu confianza en nosotros para realizar tus compras.</p>
	<p>Estamos preparando tu pedido con la referencia: <strong>%2$s</strong> y pronto realizaremos el envío correspondiente. te rogamos que revises la siguiente información y te pongas en contacto con nosotros en caso de observar algún error.</p>
	<p>En cualquier momento, puedes consultar <a href="%3$s">tu pedido</a>accediendo a tu cuenta en nuestra página web. También te puedes poner en contacto con nosotros a través del correo:</p>
	<p><a href="mailto:%4$s">%4$s</a></p>
	<p>¡Gracias por comprar en %5$s!</p>';

$gl_caption['c_mail_body_shipped'] = '
	<p>Hola %1$s,</p>
	<p>Te informamos que tu pedido <strong>%2$s</strong> ya ha sido enviado</p>';

$gl_caption['c_mail_body_shipped_2'] = '
	<p>La compañía de transportes responsable del envío de tu pedido es <a href="%3$s"><strong>%4$s</strong></a> y el número de seguimiento
		<strong>%5$s</strong>.</p >';

$gl_caption['c_mail_body_shipped_3'] = '
	<p>Puedes revisar tu pedido en: "pedidos" dentro de "<a href="%6$s">mi cuenta</a>".</p >
	<p>¡Gracias por comprar en %7$s!</p >';

$gl_caption['c_mail_body_delivered'] = '
	<p>Hola %1$s,</p>
	<p>Tu pedido <strong>%2$s</strong> ya ha sido entregado en la dirección que nos indicaste.</p>
	<p>Esperamos que estés completamente satisfecho con tu compra. Si por alguna razón ése no es el caso, agradeceríamos que nos dieras la oportunidad de resolver tus inquietudes respondiendo directamente este correo electrónico:</p>
	<p><a href="mailto:%3$s">%3$s</a></p>
	<p>¡Gracias por comprar en %4$s!</p>';


// Relacionats
$gl_caption['c_data_related']       = 'Productos relacionados';
$gl_caption['c_product_relateds'] = "Productos relacionados";
$gl_caption['c_select_related_products'] = "Busca y selecciona productos relacionados";
$gl_caption_product_related['c_image'] = "";

// Quantitat màxima
$gl_caption['c_max_quantity'] = "Cantidad máxima por pedido";
$gl_caption_list['c_max_quantity'] = "Max";
$gl_caption['c_max_quantity_abbr'] = "Max.";
$gl_caption['c_min_quantity'] = "Cantidad mínima por pedido";
$gl_caption_list['c_min_quantity'] = "Min";
$gl_caption['c_min_quantity_abbr'] = "Min.";
$gl_caption['c_quantityconcept_id'] = "Concepto";
$gl_caption['c_quantityconcept'] = "Concepto";

// Calendari
$gl_caption['c_edit_calendar'] = "Calendario";
$gl_caption['c_has_calendar'] = "Activar calendario";
$gl_caption['c_is_calendar_public'] = "Públic";
$gl_caption['c_familybusy_id'] = "";
$gl_caption['c_date_busy'] = "Día";
$gl_caption['c_familybusy_entered'] = "Fecha creación";
$gl_caption['c_form_calendar_title'] = "Fechas ocupadas";
$gl_caption['c_calendar_not_saved_error'] = "No se ha podido guardar la fecha, por favor vuelve a provar";
$gl_caption['c_calendar_saved_occupied_message'] = "Se ha ocupado el día";
$gl_caption['c_calendar_saved_freed_message'] = "Se ha desocupado el día";

$gl_caption['c_choose_date'] = "Se tiene que escoger una fecha";

// Famílies privades
$gl_caption['c_is_family_private'] = "Privada";
$gl_caption['c_pass_code'] = "Código de acceso";

$gl_caption['c_pass_code_error'] = "El código entrado no es válido";
$gl_caption['c_pass_code_success'] = "El código es correcto";

// Cart
$gl_caption['c_product_date'] = "Fecha";

// Cercador comandes
$gl_caption['c_dates_from'] = 'Del';
$gl_caption['c_dates_to'] = 'al';
$gl_caption['c_caption_filter_order_status'] = 'Estado';
$gl_caption['c_order_status'] = 'Estado';
$gl_caption['c_order_status_group_payed'] = 'Pagado';
$gl_caption['c_order_status_group_not_payed'] = 'Por pagar';
$gl_caption['c_order_status_group_payed_deposit'] = 'Depósito pagado';
$gl_caption['c_order_status_group_not_payed_deposit'] = 'Depósito por pagar';
$gl_caption['c_view_format'] = "Mostrar";
$gl_caption['c_view_format_all'] = "Todo";

$gl_caption['c_view_format_order'] = "Pedidos";
$gl_caption['c_view_format_orderitems'] = "Productos";

$gl_caption['c_view_format_title'] = "Tipo de listado";