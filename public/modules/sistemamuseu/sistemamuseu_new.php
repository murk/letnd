<?
/**
 * SistemamuseuNew
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2010
 * @version $Id$
 * @access public
 */
class SistemamuseuNew extends Module{

	function __construct(){
		parent::__construct();
		$this->parent = 'new';
		
		if (isset($_GET['category_id'])) {
			$this->set_friendly_params(array('category_id'=>R::id('category_id')));
		}
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function get_records()
	{	
		$listing = new ListRecords($this);
		
		$listing->condition = "status = 'public' ";
		$listing->condition .= "AND parent_section = '" . $this->parent . "'";
		
		if ($this->parent == 'new'){
			$listing->condition .= " 
				AND (entered=0 OR entered <= now())
				AND (expired=0 OR expired >= (curdate()))";
		}
		
		$category_id = R::id('category_id');
		if ($category_id) {
			$listing->condition .= " AND category_id = '" . $category_id . "'";
		}
		
		$listing->call('records_walk','url1,url1_name,url2,url2_name');		
		$listing->order_by = 'ordre ASC, entered DESC';
		
		if ($this->process == 'destacat'){	
			//$listing->condition .= "AND in_home=1 AND status='public'";				
			$listing->order_by = 'in_home DESC, ordre ASC, entered DESC';
			$listing->limit = 4;
			$listing->template='sistemamuseu/destaquem.tpl';			
		}
		$listing->list_records(false);
		
		if ($this->process == 'destacat'){
			foreach ($listing->results as $key=>$val){
				$rs = &$listing->results[$key];
				$rs['exhibition_title'] = &$rs['new'];
				$rs['exhibition_subtitle'] = &$rs['subtitle'];
				$rs['exhibition_description'] = &$rs['content'];
				$rs['kind'] = &$rs['category_id'];
				//$rs ['destacat_link'] = '?tool=sistemamuseu&amp;tool_section=lleure&amp;action=show_record&amp;new_id='.$rs['new_id'].'&amp;language='.LANGUAGE.'&amp;page_id=5&amp;category_id='. $rs['category_id'];	
				
				$rs ['destacat_link'] = '/' . LANGUAGE . '/sistemamuseu/lleure/'.$rs['new_id'].'/category_id/'. $rs['category_id'] . '/' . get_page_file_name(5);	
			}
		}
		else{
			foreach ($listing->results as $key=>$val){
				$rs = &$listing->results[$key];
				$rs['museu_link'] = sistemamuseu_get_museu_link($rs['museu_id']);					
			}		
		}
		if ($this->parent == 'new'){
			$listing->limit = 4;
			$listing->parse_template(false);
			return $listing->loop;
		}
		else{
			$listing->call_function['content'] = 'add_dots';			
		}
		return $listing->parse_template();
	}
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{
		$show = new ShowForm($this);
		
		// si no hi ha id, agafo per defecte la primera noticia de la categoria, i si no hi ha categoria, de la primera categoria
		if (!$show->id){
			// si no hi ha categoria agafo la primera ordenat per nom i id
			if (isset($_GET['category_id'])){
				$category_id = $_GET['category_id'];
			}
			else{
				$query = "SELECT sistemamuseu__category.category_id as category_id
					FROM sistemamuseu__category_language, sistemamuseu__category
					WHERE sistemamuseu__category_language.category_id = sistemamuseu__category.category_id
					AND language = '" . LANGUAGE . "'
					ORDER BY category, category_id
					LIMIT 0,1";
				$category_id = Db::get_first($query);
			}
			$query = "SELECT sistemamuseu__new.new_id as new_id
				FROM sistemamuseu__new_language, sistemamuseu__new
				WHERE sistemamuseu__new_language.new_id = sistemamuseu__new.new_id
				AND status = 'public'
				AND bin <> 1
				AND category_id = '" . $category_id . "'
				AND language = '" . LANGUAGE . "'
				ORDER BY entered DESC, new_id
				LIMIT 0,1";
			$show->id = Db::get_first($query);
		}
		$show->condition = "status = 'public'";
		$show->call('records_walk','url1,url1_name,url2,url2_name');
		$show->get_values();		
		if ($show->has_results)
		{
			$GLOBALS['gl_page']->page_title = $show->rs['new'];
			$GLOBALS['gl_page']->page_description = add_dots_meta_description($show->rs['content']);
			$show->set_var('addthis_description', htmlspecialchars ($show->rs['content']) );
			$show->set_var('museu_link', sistemamuseu_get_museu_link($show->rs['museu_id']) );
			return $show->show_form();
		}
		else
		{
			$this->do_action('list_records');
		}
	}
	function records_walk($url1,$url1_name,$url2,$url2_name){		
		// Això ho poso aquí així no cal posar-ho al tpl, simplement posar: <a href="<=$url1>" target="_blank"><=$url1_name></a>
		
		
		$ret['url1_name'] = $url1_name?$url1_name:$url1;
		$ret['url2_name'] = $url2_name?$url2_name:$url2;
		
		return $ret;
	}
}
?>