<?

/* 19-10-16

	FRASES


 */
if ( is_table( 'client__configpublic' ) && ! is_field( 'client__configpublic', 'ordre' ) ) {

	$update_sql[] = "ALTER TABLE `client__configpublic`
	ADD COLUMN `ordre` INT(11) NOT NULL DEFAULT '0' AFTER `type`;";

} else {
	if ( is_table( 'client__configpublic' ) ) {
		trigger_error( 'Ja existeixen el camp ordre' );
	}
}

?>