<section id="inmo-home">

	<h6><?= $title ?></h6>

	<div class="property-list">
		<? foreach( $loop as $l ): extract( $l ) ?>

			<? $hidden_class=$conta >= 4?'hidden':'' // TODO-i Si es un slider i van amagats a partir d'un X items ?>

			<? include( PATH_TEMPLATES . 'inmo/property_list_item.tpl' ) ?>

		<? endforeach //$loop?>
	</div>

	<a href="<?= get_page_link( 'property' ) ?>"><?= $c_veure_mes_propietats ?></a>

</section>