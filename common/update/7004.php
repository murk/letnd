<?php
/* 4-4-17
Màxim persones i ítems

*/

if ( is_table( 'product__product' ) && ! is_field( 'product__product', 'quantityconcept_id' ) ) {

	$update_sql[] = "ALTER TABLE `product__product`
	ADD COLUMN `quantityconcept_id` INT(11) NOT NULL AFTER `certification`;";

} else {
	if ( is_table( 'product__product' ) ) {
		trigger_error( 'Ja existeixen el camp quantityconcept_id' );
	}
}
if ( is_table( 'product__product' ) && ! is_field( 'product__product', 'max_quantity' ) ) {

	$update_sql[] = "ALTER TABLE `product__product`
	ADD COLUMN `max_quantity` INT(11) NOT NULL AFTER `certification`;";

} else {
	if ( is_table( 'product__product' ) ) {
		trigger_error( 'Ja existeixen el camp max_quantity' );
	}
}

if ( is_table( 'product__configadmin' ) && ! Db::get_first( "SELECT * FROM product__configadmin WHERE name='hide_fields'" ) ) {
	$update_sql[] = "
		INSERT INTO product__configadmin (`name`, `value`) VALUES ('hide_fields', '');";

} else {
	if ( is_table( 'product__configadmin' ) ) {
		trigger_error( 'Ja existeixen el camp hide_fields' );
	}
}

if ( is_table( 'product__product' ) ) {

	if ( ! is_table( 'product__quantityconcept' ) ) {

		$update_sql [] = "CREATE TABLE `product__quantityconcept` (
			`quantityconcept_id` INT(11) NOT NULL AUTO_INCREMENT,
			PRIMARY KEY (`quantityconcept_id`)
		)
		COLLATE='utf8_spanish2_ci'
		ENGINE=MyISAM;
		";
		$update_sql [] = "CREATE TABLE `product__quantityconcept_language` (
			`quantityconcept_id` INT(11) NOT NULL DEFAULT '0',
			`language` CHAR(3) NOT NULL DEFAULT '' COLLATE 'utf8_spanish2_ci',
			`quantityconcept` VARCHAR(150) NOT NULL COLLATE 'utf8_spanish2_ci'
		)
		COLLATE='utf8_spanish2_ci'
		ENGINE=MyISAM
		;";

	} else {
		trigger_error( 'Ja existeixen la taula product__quantityconcept' );
	}

}

if ( is_table( 'product__product' ) && (!Db::get_first('SELECT count(*) FROM all__menu WHERE `menu_id` =20200')) ) {

	$update_sql[] = "
		INSERT INTO `all__menu` (`menu_id`, `menu_group`, `tool`, `tool_section`, `action`, `parent_id`, `toolmode_id`, `variable`, `ordre`, `link`, `process`, `bin`, `read1`, `write1`) VALUES (20200, 'product', 'product', 'quantityconcept', '', 20000, 1, 'PRODUCT_MENU_QUANTITYCONCEPT', 3, '', '', 0, ',1,', ',1,');";
	$update_sql[] = "
		INSERT INTO `all__menu` (`menu_id`, `menu_group`, `tool`, `tool_section`, `action`, `parent_id`, `toolmode_id`, `variable`, `ordre`, `link`, `process`, `bin`, `read1`, `write1`) VALUES (20201, 'product', 'product', 'quantityconcept', 'show_form_new', 20200, 1, 'PRODUCT_MENU_QUANTITYCONCEPT_NEW', 1, '', '', 0, ',1,', ',1,');";
	$update_sql[] = "
		INSERT INTO `all__menu` (`menu_id`, `menu_group`, `tool`, `tool_section`, `action`, `parent_id`, `toolmode_id`, `variable`, `ordre`, `link`, `process`, `bin`, `read1`, `write1`) VALUES (20202, 'product', 'product', 'quantityconcept', 'list_records', 20200, 1, 'PRODUCT_MENU_QUANTITYCONCEPT_LIST', 2, '', '', 0, ',1,', ',1,');";

}

?>