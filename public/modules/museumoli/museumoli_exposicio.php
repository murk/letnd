<?
/**
 * MuseumoliExposicio
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2011
 * @version $Id$
 * @access public
 */
class MuseumoliExposicio extends Module{

	function __construct(){
		parent::__construct();
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function get_records()
	{	
		return $this->_execute_module(str_replace('list_','get_',$this->action));
	}
	function arxiu()
	{	
		$this->_execute_module('list_records');
	}
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{
		return $this->_execute_module(str_replace('show_','get_',$this->action));
	}
	function _execute_module($action=''){
		$module = Module::load('museumoli','visita', $this);
		$action = $action?$action:$this->action;
		return $module->do_action($action);	
	}
}
?>