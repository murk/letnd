<?

$vars['block_selected'] = 0;


$query = "SELECT shipyard, nautica__shipyard.shipyard_id
				FROM nautica__shipyard
				ORDER BY ordre ASC, shipyard";

$results = Db::get_rows($query);
Debug::add('Query nautica shipyard', $query);


//$shipyard_id = R::id('shipyard_id');
//$vars['block_selected'] = $shipyard_id?1:0;
$vars['current_shipyard'] = '';
	
$conta = 0;
$loop_count = count($results);
foreach ($results as $rs) {

	$l = array();

	// CONTA VAIXELLS, es mes rapid que posant la consulta com a subquery
	$embarcacion_count = Db::get_first("SELECT count(*) 
						FROM nautica__embarcacion
						WHERE nautica__embarcacion.shipyard_id = " . $rs['shipyard_id'] . "
						AND (status = 'onsale' OR status = 'reserved' OR status = 'sold') 
						AND nautica__embarcacion.bin = 0");

	if ($embarcacion_count) {
		
		$l['id'] = $rs['shipyard_id'];
		$l['link'] = Page::get_link('nautica', 'embarcacion', false, false, array(), false) . '?shipyard_ids['.$l['id'].']=' . $l['id'];
		$l['item'] = $rs['shipyard'];
		$l['embarcacion_count'] = $embarcacion_count;
		$l['class'] = '';
		$l['conta'] = $conta++;
		$l['loop_count'] = $loop_count;
		$checked = isset($_GET['shipyard_ids'][$l['id']])?' checked':'';
		$l['checkbox'] = '<input name="shipyard_ids['.$l['id'].']" value="'.$l['id'].'" type="checkbox" '.$checked.' />';

		//$l['selected'] = $l['id']==$shipyard_id?1:0;
		$loop[] = $l;
		
		if (!$vars['current_shipyard'] && $checked) $vars['current_shipyard'] = $l['item'];
	}
}
if ($loop) {
	$loop[0]['class'] = 'first';
	$loop[count($loop) - 1]['class'] = 'last';
	// poso be el loop_count, pot ser que alguna familia no tingui productes i llavors falla
	if (count($loop) != $loop_count) {
		foreach ($loop as &$val) {
			$val['loop_count'] = count($loop);
		}
	}
	
} else {
	/* si no hi ha cap resultat no mostro el block */
	$show_block = false;
	return;
}
?>