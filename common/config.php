<?php
/**
 * Per saber si som a la part pública o la privada
 *
 * @global string $gl_site_part
 */
$gl_site_part = strstr($_SERVER["PHP_SELF"], 'admin')?'admin':'public'; //

/**
 * Conté tots els parametres de configuració de l'eina actual o parametres per defecte
 * (l'eina actual sobreescriu els parametres per defecte)
 *
 * @global array $gl_config
 */
$gl_config = array(); //
$gl_languages = array();
$gl_default_language =  get_default_language();
$gl_language = get_language();
$gl_language_code = $gl_languages['codes2'][$gl_language];
define("LANGUAGE", $gl_language);
define("LANGUAGE_CODE", $gl_language_code);

// just abans del config per poder agafar configuracions expresses pel mobil
// si s'autodetecta -> agafa plantilles especifiques, agafa config especial mobil
$gl_auto_detect_mobile = false;
$gl_is_mobile = detect_mobile(); 
$mobile_ext = $gl_is_mobile && $gl_auto_detect_mobile?'_m':'';
// transforma tots els valors de configuració  a NAME = value
get_config_values();
// actualitzo la base de dades just desprès de obtindre el config
$gl_version = isset($gl_config['version'])?(int)($gl_config['version']):2000;
// Em serveix per saber quan estic fent una accio en ajax i per fer debug compatible
$gl_is_ajax = R::get('gl_is_ajax');
include (DOCUMENT_ROOT . '/admin/modules/setup/setup_update.php');

// ---------------------
// Variables globals
// ---------------------
// .....................
// variables de page.php
// .....................
if ($gl_site_part == 'admin') {
    $gl_menu_id = R::escape('menu_id',current(explode(',',DEFAULT_MENU_ID)),'_POST'); // poso escape ja que al guardar usuaris petava per post['menu_id'] que es un array
    $gl_menu_id = R::id('menu_id',$gl_menu_id,'_GET'); // te preferencia el get sino no puc fer la secció per administrar menus
	
    $gl_tool = R::text_id('tool','','_GET');
    $gl_tool = R::text_id('tool',$gl_tool,'_POST'); // te preferencia el post
	
    $gl_tool_section = R::text_id('tool_section','','_GET');
    $gl_tool_section = R::text_id('tool_section',$gl_tool_section,'_POST'); // te preferencia el post
	
    $gl_is_admin = true;
} else {
	$gl_is_home = (get_all_get_params(array('language','page')))?false:true;// si nomès hi ha el get d'idioma o cap, som a la homepage o està definit get[is_home]	
	
	$gl_tool = $gl_tool_section = '';
	// nomes afago default quan no es pagina, sino totes les pagines em surten amb un tool per defecte
	if (!isset($_GET['page_id']) && !isset($_GET['page_file_name'])) {
		$gl_tool = DEFAULT_TOOL;
		$gl_tool_section = DEFAULT_TOOL_SECTION; // es constant perque sobreescric a clubmoto
	}	
	
    $gl_tool = R::text_id('tool',($gl_is_home?$gl_config['default_home_tool']:$gl_tool),'_GET');
    $gl_tool = R::text_id('tool',$gl_tool,'_POST'); // te preferencia el post
	
    $gl_tool_section = R::text_id('tool_section',($gl_is_home?$gl_config['default_home_tool_section']:$gl_tool_section),'_GET');
    $gl_tool_section = R::text_id('tool_section',$gl_tool_section,'_POST'); // te preferencia el post
	
	$gl_is_tool_home = (get_all_get_params(array('language','tool','tool_section','page_id','page_file_name','page')))?false:true;// si nomès hi ha el get d'idioma tool i tool_section i page o cap, el nom de la pagina, o paginació, som a la homepage de l'eina 
	
    $gl_is_admin = false;
}

$gl_current_form_id = false;
$gl_tool_top_id = '';
$gl_tool_top_variable = '';

$gl_action = R::text_id('action','','_GET'); 
$gl_action = R::text_id('action',$gl_action,'_POST'); // te preferncia el post
$gl_action = str_replace('-', '_', $gl_action);

$gl_next_form_id = ''; // aquesta variable nomès té un valor quan el primer dels formularis de la llista es un show_form_new, serveix per fer un Wizard per formularis quan s'entra un nou registre, ej. a inmo s'entra la propietat, desprès les imatges i per últim el propietari
$gl_write = false;
$gl_read = false;
$gl_saved = true;

/**
 * Acció del llistat on erem abans d'editar un registre <br><br>
 * Quan mostrem els resultats del llistat es defineix aquesta variable, així sabem en quina acció estavem: busqueda, filtre, mostrar tot, ordre, pàgina, etc...<br>
 * Quan editem el registre seguim passant totes les variables del llistat<br>
 * I quan guardem el registre, un cop guardat, tornem a mostrar el llistat ja que s'han anat pasant totes les variables
 *      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! això no funciona be, el 'page=3' a imatges petava
 *
 * @var string
 */
$gl_action_previous_list = R::text_id('action_previous_list','','_GET'); 
$gl_action_menu = '';

$gl_process = R::text_id('process','','_GET'); 
$gl_process = R::text_id('process',$gl_process,'_POST'); // te preferncia el post

$gl_parent_id = '';
$gl_variable = '';

// a public ho faig al crear page
if (isset($_SESSION['message'])) {
    $gl_message = $_SESSION['message'];
    unset($_SESSION['message']);
} else {
    $gl_message = '';
}
$gl_news = '';
$gl_content = '';
$gl_insert_id = '';
$gl_print_link = '';
// .....................
// variables de page_public_inmo.php
// .....................
if (!isset($gl_section)) {
	$gl_section = R::text_id('section','inmo_home','_GET'); 
}
// ............................
// variables de db_classes.php
// ............................
/**
 * Taula utilitzada per la conexió a la BBDD
 *
 * @global string $gl_db_classes_table
 */
$gl_db_classes_table = '';

/**
 * Quin id field es posa quan fem el next_form_id o quan es mostra un llistat despres de borrar un registre
 * s'assigna automaticament segons el module->parent_module
 *
 * @global string $gl_do_next_id_field
 */
$gl_do_next_id_field = '';

/**
 * Clau principal de la taula (serà millor agafar-la directament del nom de la taula, ej. $gl_db_classes_table.'_id'. Per fer aixó s'hauran de canviar tots els noms de la clau principal a la BBDD)
 *
 * @global string $gl_db_classes_id_field
 */
$gl_db_classes_id_field = '';

/**
 * Array amb tots els camps i les seves configuracions
 *
 * @global array $gl_db_classes_field
 */
$gl_db_classes_fields = [];

/**
 * Array dels camps de la BBDD que van traduits a diferent idiomes
 *
 * @global array $gl_db_classes_language_fields
 */
$gl_db_classes_language_fields = [];

/**
 * Resultats per pàgina en la pàginació dels llistats
 *
 * @global int $gl_db_max_results
 */
$gl_db_max_results = MAX_RESULTS;

/**
 * Màxim nombre de links directes a una pàgina en la pàginació Ej. 1-2-3-4-5   , en aquest cas el nombre de links es 5
 *
 * @global int $gl_db_max_page_links
 */
$gl_db_max_page_links = MAX_PAGE_LINKS;

/**
 * Protexeix d'escritura un arxiu generable desde admintotal
 *
 * @global boolean $gl_file_protected
 */
$gl_file_protected = false;

/**
 * Conté tots els noms de camps, botons, etc. en els arxius d'idiomes
 * (l'eina actual sobreescriu els noms per defecte)
 *
 * @global array $gl_file_protected
 */
$gl_caption = array();
$gl_caption_list = array();
$gl_caption_images = array();

/**
 * Conté tots els missatges de les classes en el arxius d'idiomes
 * (l'eina actual sobreescriu els missatges per defecte)
 *
 * @global array $gl_file_protected
 */
$gl_messages = array();
$gl_messages_images = array();

/**
 * Si l'apartat té imatges relacionades o no
 *
 * @global boolean $gl_has_images
 */
$gl_has_images = false;

/**
 * Carrega els arxius necessaris per quan hi ha un camp de date
 *
 * @global boolean $gl_calendar
 */
$gl_calendar = '';

/**
 * Per recarregar la pàgina o el parent frame un cop acabat de guardar
 *
 * @global boolean $gl_reload
 */
$gl_reload = false;

/**
 * Per guardar errors globals
 *
 * @global boolean $gl_reload
 */
$gl_errors = array();
$gl_errors['post_file_max_size'] = false;

/**
 * per saber si hem sobreescrit la taula, l'idioma, i la configuració
 *
 * @global boolean $gl_calendar
 */
$gl_table_overrided = false;
$gl_language_vars_overrided = false;
$gl_config_overrided = false;

/**
 * Per saber si treballem en local (ej, w.letnd)
 *
 * @global boolean $gl_is_local
 */
// $gl_is_local = false; ////////////// ESTA DEFINIDA A CONFIG_DB QUE eS CARREGA PRIMER

/**
 * Per saber si superadmin esta logejat
 *
 * @global boolean $gl_is_admin_logged
 */
$gl_is_admin_logged = false;
/**
 * Guarda el nom del record actual que s'ha de mostrar en el breadcrumb com a últim item, com que no es un page no sortiria en el breadcrumb 
 *
 * @global boolean $gl_reload
 */
$gl_breadcrumb = '';
/**
 * Posar a  false si no volem que un formulari faci una acció següent per defecte
 *
 * @global boolean $gl_reload
 */
$gl_do_next = true;

$gl_is_windows = strpos( getenv( 'OS' ), 'Windows' )!==false;

$gl_current_mailer = false;


// rutes
define('CLIENT_DIR', $_SESSION['client_dir']);
define('CLIENT_DIR_NAME', substr(CLIENT_DIR, strpos(CLIENT_DIR, "/") + 1));
define('CLIENT_PATH', DOCUMENT_ROOT . CLIENT_DIR . "/");

define('DIR_TEMPLATES', 
		($gl_site_part == 'public')?
		((THEME)?
			('/public/themes/' . THEME . $mobile_ext . '/'):
			('/' . CLIENT_DIR . '/templates' . $mobile_ext . '/')):
		('/admin/themes/' . THEME . $mobile_ext . '/')
	);

define('PATH_TEMPLATES', DOCUMENT_ROOT . substr(DIR_TEMPLATES,1) );

define('PATH_TEMPLATES_PAGE', 
		($gl_site_part == 'public')?
		(CLIENT_PATH . 'templates' . $mobile_ext . '/'):
		(DOCUMENT_ROOT . 'admin/themes/' . THEME . $mobile_ext . '/'));


define('DIR_TEMPLATES_ADMIN', '/admin/themes/' . THEME . $mobile_ext . '/');

// desde admin també necessito saber el path per la part publica, ja que els albums, aparadors, etc han d'anar en aquest directori
// Mobil no fa falta aquí
// de moment, s'hauria d'ajuntar admin i public dels configs per no haber de duplicar'''
define('THEME_PUBLIC', Db::get_first("SELECT value FROM all__configpublic WHERE name = 'theme'"));
define('DIR_THEMES_PUBLIC', '/public/themes/' . THEME_PUBLIC . '/');
define('PATH_THEMES_PUBLIC', DOCUMENT_ROOT . DIR_THEMES_PUBLIC);

define('DIR_TEMPLATES_PUBLIC', '/' . CLIENT_DIR . '/templates/');
define('PATH_TEMPLATES_PUBLIC', DOCUMENT_ROOT . DIR_TEMPLATES_PUBLIC);


define('PATH_BLOCKS', PATH_TEMPLATES . 'blocks/');

$page_url = explode('?',$_SERVER['REQUEST_URI']);
define("PAGE_URL",HOST_URL . substr($page_url[0], 1));

define ('LOGS_PATH', $GLOBALS['gl_is_local'] ?
	( $GLOBALS['gl_is_linux']?'/letnd/data/logs/':'d:\\letnd\\data\\logs\\' )
	:'/var/www/letnd.com/datos/motor/data/logs/');

define ('GLOBAL_TEMP_PATH', $GLOBALS['gl_is_local'] ?
	( $GLOBALS['gl_is_linux']?'/letnd/data/temp/':'d:\\letnd\\data\\temp\\' )
	:'/var/www/letnd.com/datos/motor/data/temp/');



Debug::p( [
	[ 'VERSION - Versió de php', VERSION ],
	[ 'HOST_URL', HOST_URL ],
	[ 'Host', $configuration['dbhost'] ],
	[ 'BD', $configuration['dbname'] ],
	[ 'Usuari', $configuration['dbuname'] ]
], 'general' );


// ............................
// variables en altres arxius
// ............................
// DEBUG    -  debug.php
// DOCUMENT_ROOT      -  main.php
// HOST_URL           -  main.php
// IMAGES_DIR   -  index.php