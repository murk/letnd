<?php
/* 7-9-18
Afegeixo place_id al google
*/

if ( is_table( 'all__configpublic' ) && ! Db::get_first( "SELECT * FROM all__configpublic WHERE name='google_maps_key'" ) ) {
	$update_sql[] = "
		INSERT INTO all__configpublic (`name`, `value`) VALUES ('google_maps_key', '');";

}
else {
	if ( is_table( 'all__configpublic' ) ) {
		trigger_error( 'Ja existeixen el camp google_maps_key' );
	}
}