<?php
/* 7-5-18
Productes, configuració dels index de la BBDD, hi havia consultes de +20 segons

*/

if ( is_table( 'product__product' ) ) {

	$update_sql .= "
ALTER TABLE `product__brand`
	ADD INDEX `bin` (`bin`);
ALTER TABLE `product__brand_image`
	ADD INDEX `brand_id` (`brand_id`),
	ADD INDEX `main_image` (`main_image`);
ALTER TABLE `product__brand_image_language`
	ADD INDEX `image_id` (`image_id`),
	ADD INDEX `language` (`language`);
ALTER TABLE `product__brand_language`
	ADD INDEX `brand_id` (`brand_id`),
	ADD INDEX `language` (`language`);
ALTER TABLE `product__color`
	ADD INDEX `bin` (`bin`);
ALTER TABLE `product__color_language`
	ADD INDEX `color_id` (`color_id`),
	ADD INDEX `language` (`language`);
ALTER TABLE `product__courier`
	ADD INDEX `bin` (`bin`);
ALTER TABLE `product__customer`
	ADD INDEX `bin` (`bin`),
	ADD INDEX `activated` (`activated`);
ALTER TABLE `product__customer_to_promcode`
	ADD INDEX `customer_id` (`customer_id`),
	ADD INDEX `promcode_id` (`promcode_id`);
ALTER TABLE `product__family`
	ADD INDEX `parent_id` (`parent_id`),
	ADD INDEX `bin` (`bin`);
ALTER TABLE `product__family_image`
	ADD INDEX `family_id` (`family_id`);
ALTER TABLE `product__family_image_language`
	ADD INDEX `language` (`language`);
ALTER TABLE `product__family_language`
	ADD INDEX `family_id` (`family_id`),
	ADD INDEX `language` (`language`);
ALTER TABLE `product__material`
	ADD INDEX `bin` (`bin`);
ALTER TABLE `product__material_language`
	ADD INDEX `material_id` (`material_id`),
	ADD INDEX `language` (`language`);
ALTER TABLE `product__orderitem`
	ADD INDEX `product_id` (`product_id`);
ALTER TABLE `product__product_download`
	ADD INDEX `product_id` (`product_id`);
ALTER TABLE `product__order`
	ADD INDEX `bin` (`bin`);
ALTER TABLE `product__product_file`
	ADD INDEX `product_id` (`product_id`);
ALTER TABLE `product__product_image`
	ADD INDEX `product_id` (`product_id`),
	ADD INDEX `main_image` (`main_image`);
ALTER TABLE `product__product_image_language`
	ADD INDEX `image_id` (`image_id`),
	ADD INDEX `language` (`language`);
ALTER TABLE `product__product_to_sector`
	ADD INDEX `product_id` (`product_id`),
	ADD INDEX `sector_id` (`sector_id`);
ALTER TABLE `product__product_to_spec_language`
	ADD INDEX `product_id` (`product_id`);
ALTER TABLE `product__product`
	ADD INDEX `family_id` (`family_id`),
	ADD INDEX `subfamily_id` (`subfamily_id`),
	ADD INDEX `subsubfamily_id` (`subsubfamily_id`),
	ADD INDEX `brand_id` (`brand_id`),
	ADD INDEX `bin` (`bin`);
ALTER TABLE `product__product_language`
	ADD INDEX `product_id` (`product_id`),
	ADD INDEX `language` (`language`);
ALTER TABLE `product__product_to_variation`
	ADD INDEX `product_id` (`product_id`),
	ADD INDEX `variation_id` (`variation_id`);
ALTER TABLE `product__product_to_variation_language`
	ADD INDEX `product_variation_id` (`product_variation_id`),
	ADD INDEX `product_id` (`product_id`),
	ADD INDEX `language` (`language`);
ALTER TABLE `product__promcode_language`
	ADD INDEX `promcode_id` (`promcode_id`),
	ADD INDEX `language` (`language`);
ALTER TABLE `product__promcode`
	ADD INDEX `bin` (`bin`);
ALTER TABLE `product__quantityconcept_language`
	ADD INDEX `quantityconcept_id` (`quantityconcept_id`),
	ADD INDEX `language` (`language`);
ALTER TABLE `product__rate`
	ADD INDEX `country_id` (`country_id`),
	ADD INDEX `provincia_id` (`provincia_id`);
ALTER TABLE `product__sector`
	ADD INDEX `bin` (`bin`);
ALTER TABLE `product__sector_image`
	ADD INDEX `sector_id` (`sector_id`),
	ADD INDEX `main_image` (`main_image`);
ALTER TABLE `product__sector_image_language`
	ADD INDEX `image_id` (`image_id`),
	ADD INDEX `language` (`language`);
ALTER TABLE `product__sector_language`
	ADD INDEX `sector_id` (`sector_id`),
	ADD INDEX `language` (`language`);
ALTER TABLE `product__sellpoint`
	ADD INDEX `country_id` (`country_id`),
	ADD INDEX `municipi_id` (`municipi_id`),
	ADD INDEX `comarca_id` (`comarca_id`),
	ADD INDEX `provincia_id` (`provincia_id`),
	ADD INDEX `comunitat_id` (`comunitat_id`),
	ADD INDEX `bin` (`bin`);
ALTER TABLE `product__sellpoint_image`
	ADD INDEX `sellpoint_id` (`sellpoint_id`),
	ADD INDEX `main_image` (`main_image`);
ALTER TABLE `product__sellpoint_image_language`
	ADD INDEX `image_id` (`image_id`),
	ADD INDEX `language` (`language`);
ALTER TABLE `product__sellpoint_language`
	ADD INDEX `sellpoint_id` (`sellpoint_id`),
	ADD INDEX `language` (`language`);
ALTER TABLE `product__spec`
	ADD INDEX `spec_category_id` (`spec_category_id`),
	ADD INDEX `bin` (`bin`);
ALTER TABLE `product__spec_category_language`
	ADD INDEX `spec_category_id` (`spec_category_id`),
	ADD INDEX `language` (`language`);
ALTER TABLE `product__spec_language`
	ADD INDEX `spec_id` (`spec_id`),
	ADD INDEX `language` (`language`);
ALTER TABLE `product__tax`
	ADD INDEX `bin` (`bin`);
ALTER TABLE `product__variation`
	ADD INDEX `variation_category_id` (`variation_category_id`),
	ADD INDEX `variation_checked` (`variation_checked`),
	ADD INDEX `variation_default` (`variation_default`),
	ADD INDEX `bin` (`bin`);
ALTER TABLE `product__variation_category_language`
	ADD INDEX `variation_category_id` (`variation_category_id`),
	ADD INDEX `language` (`language`);
ALTER TABLE `product__variation_image`
	ADD INDEX `variation_id` (`variation_id`),
	ADD INDEX `main_image` (`main_image`);
ALTER TABLE `product__variation_image_language`
	ADD INDEX `image_id` (`image_id`),
	ADD INDEX `language` (`language`);
ALTER TABLE `product__variation_language`
	ADD INDEX `variation_id` (`variation_id`),
	ADD INDEX `language` (`language`);";

}