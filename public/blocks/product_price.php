<?php
// config - Nomes hi ha fet Range
$block_type = isset( $vars['block_type'] ) ? $vars['block_type'] : 'range'; // links - select - range


$page_id   = isset( $vars['page_id'] ) ? $vars['page_id'] : false;
$condition = '';


$vars['var_name'] = 'prices';

$caption = &$GLOBALS['gl_caption'];
if ( ! isset( $caption['c_filter_price_min'] ) ) {
	include( DOCUMENT_ROOT . 'public/modules/inmo/languages/' . LANGUAGE . '.php' );
	$caption = $gl_caption;
}


$c_filter_price_min = $caption['c_filter_price_min'];
$c_filter_price_max = $caption['c_filter_price_max'];


$prices_request   = R::text_id( 'prices' );
$selected_price_1 = '';
$selected_price_2 = '';
if ( $prices_request ) {
	$prices_request   = explode( '-', $prices_request );
	$selected_price_1 = $prices_request[0];
	$selected_price_2 = $prices_request[1]; // nomes per select
}

$vars['block_selected'] = $selected_price_1!='' ? 1 : 0;


if ( $block_type == 'range' ) {

	$vars['min_price'] = 0;
	$condition         = "
				SELECT max(pvp) 
				FROM product__product 
				WHERE bin=0 
					AND (status = 'onsale' OR status = 'nostock' OR status = 'nocatalog')
					AND price_consult = 0";

	$vars['max_price'] = Db::get_first( $condition );


	$vars['selected_price_1'] = $selected_price_1 && is_numeric($selected_price_1) ? $selected_price_1 : '0';
	$vars['selected_price_2'] = $selected_price_2 && is_numeric($selected_price_2) ? $selected_price_2 : $vars['max_price'];

	// Quan té els valors per defecte ho poso a 0 així no ho inclou en la cerca ( sino sempre busca preus diferents de 0 i llavors deixa de funcionar per si busquem només referencia
	if ( $vars['selected_price_1'] == $vars['min_price'] && $vars['selected_price_2'] == $vars['max_price'] ) {
		$vars['selected_price_hidden'] = '';
	} else {
		$vars['selected_price_hidden'] = $vars['selected_price_1'] . '-' . $vars['selected_price_2'];

	}

	$price_dif = $vars['max_price'] - $vars['min_price'];

	$vars['price_step'] = 5;
	if ( $price_dif > 200 ) {
		$vars['price_step'] = 10;
	}
	if ( $price_dif > 300 ) {
		$vars['price_step'] = 15;
	}
	if ( $price_dif > 400 ) {
		$vars['price_step'] = 20;
	}
	if ( $price_dif > 500 ) {
		$vars['price_step'] = 30;
	}
}


?>