<?
/**
 * __Tool__Section
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2010
 * @version $Id$
 * @access public
 */
class SistemamuseuMuseuHome extends Module{

	function __construct(){
		parent::__construct();
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function get_records()
	{
	    $this->action = 'show_record';
				
		// Array de ids per poder fer el random		
		$query = "SELECT `museu_id` FROM `sistemamuseu__museu` WHERE sistemamuseu__museu.bin = 0 AND status = 'public' ORDER BY tipus, ordre";
		$results =  Db::get_rows($query);
		$ref = array();
		foreach ($results as $rs){
			$ref[]=$rs['museu_id'];
		}
		
		$rand_id = rand(0,count($ref)-1);
		
		$show = new ShowForm($this);
		$show->id = $ref[$rand_id];	
		$show->template = '/sistemamuseu/museu_home.tpl';
		
		$museus_map = array();
		$museus = $this->_get_museus($museus_map, $show->id);//debug::p($museus_map);
				
	    $show->set_loop('museus',$museus);
		$show->get_values();
		$show->set_var('f_ref',$show->rs['ref']);
		
		$GLOBALS['gl_page']->set_var('ref',$show->rs['ref']);
	    $GLOBALS['gl_page']->set_var('news',$this->_get_news());
	    $GLOBALS['gl_page']->set_var('museus_map',$museus_map);
		
		return $show->show_form();
	}
	function _get_museus(&$museus_map, $id)
	{	
		$page = get_page_file_name(16);
		
		$query = "
			SELECT sistemamuseu__museu.museu_id AS museu_id, ref , tipus, museu_title, museu_subtitle, latitude, longitude, visita, url1
			FROM sistemamuseu__museu 
			LEFT OUTER JOIN sistemamuseu__museu_language using (museu_id) 
			WHERE sistemamuseu__museu.bin = 0
			AND sistemamuseu__museu_language.language = '".LANGUAGE."'
			AND status = 'public'
			ORDER BY tipus, ordre";
		$results = $museus_map = Db::get_rows($query);
		$conta=0;
		$ret = array();
		foreach ($results as $key=>$rs){
			$museus_map[$key]['selected'] = $rs['selected'] = ($id==$rs['museu_id'])?'1':'0';
			$rs['display'] = $rs['selected']?'block':'none';
						
			if (!isset($ret[$rs['tipus']])) {
				$ret[$rs['tipus']]['tipus'] = $this->caption['c_tipus_'.$rs['tipus']];
				$ret[$rs['tipus']]['museus2'] = array();
			}
			$rs['class'] = '';
			$rs['show_record_link'] = '/'.LANGUAGE. '/' . $this->tool . '/' . $this->tool_section . '/' . $rs['museu_id'] . '/' . $page;
			$ret[$rs['tipus']]['museus2'][] = $rs;
			$museus_map[$key]['conta']=$conta++;
		}
		// Marco l'ultims de cada tipus
		foreach ($ret as $key=>$value){
		 $ret[$key]['museus2'][count($ret[$key]['museus2'])-1]['class'] = 'last';
		}
		return $ret;
	}
	function _get_news(){
		$module = Module::load('sistemamuseu','new');
		$module->parent = 'new';
		$ret = $module->do_action('get_records');
		if (!is_array($ret)) $ret = array();
		return $ret;
	}
	function show_form()
	{
	}
	function get_form()
	{	    
	}

	function save_rows()
	{
	}

	function write_record()
	{
	}

	function manage_images()
	{
	}
}
?>