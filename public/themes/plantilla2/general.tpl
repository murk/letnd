<!DOCTYPE html	PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?=LANGUAGE_CODE?>" lang="<?=LANGUAGE_CODE?>">
<head>
<title>
<?=$page_title?>
</title>
<meta name="Description"
content="<?=$page_description?>" />
<!-- <?=$page_description?> -->
<meta name="Keywords"
content="<?=$page_keywords?>" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="<?=DIR_TEMPLATES?>styles/general.css" rel="stylesheet" type="text/css">
<script type="text/javascript">
<!--
var a_vars = Array();var pagename='<?=$title?>';var phpmyvisitesSite = 1;var phpmyvisitesURL = "<?=HOST_URL?>common/includes/phpmv2/phpmyvisites.php";
//-->
</script>
<script language="javascript" src="/common/includes/phpmv2/phpmyvisites.js" type="text/javascript"></script>
<script language="JavaScript" src="/public/jscripts/hide_m.js"></script>
<script type="text/javascript" src="/common/jscripts/mootools.js"></script>
<script type="text/javascript" src="/common/jscripts/modalbox.js"></script>
</head>
<body>
<div id="container">
  <div id="header_top">
    <? if ($blocks_ht):?>
      <? foreach($blocks_ht as $b): extract ($b)?>
        <?=$block?>
        <? endforeach //$blocks?>
      <? endif //$blocks_ht?>
  </div>
  <div id="header">
    <div id="header_left"><img src="<?=CLIENT_DIR?>/images/logo_web.png" /></div>
    <div id="header_right">
      <h2><?=PAGE_PHONE?></h2>
      <h1>
        <?=$page_slogan?>
      </h1>
    </div>
  </div>
  <div class="cls"></div>
  <div id="header_image"><img src="<?=CLIENT_DIR?>/images/header_web.jpg" width="777" height="207" /></div>
  <div id="header_bottom">
    <div id="header_bottom_menu">
      <? if ($blocks_hb):?>
        <? foreach($blocks_hb as $b): extract ($b)?>
          <?=$block?>
          <? endforeach //$blocks?>
        <? endif //$blocks_hb?>
    </div>
    <div id="header_bottom_search">
      <form action="/index.php?tool=inmo&tool_section=property" method="get" name="search" id="search">
        <input name="tool" type="hidden" value="inmo" />
        <input name="tool_section" type="hidden" value="property" />
        <input name="action" type="hidden" value="list_records_search_search" />
        <input class="search" name="q" type="text" size="27" />
        <input type="image" name="search_button" id="search_button" src="/public/themes/plantilla2/images/lupa.gif" style="border:0" />
      </form>
    </div>
  </div>
  <!-- Tenca header -->
  <div id="content">
    <!--inclou el left, mt, mb, right i content_center -->
    <div id="left">
      <? if ($blocks_l):  ?>
        <?foreach($blocks_l as $b): extract ($b)?>
          <?=$block?>
          <?endforeach //$blocks  ?>
        <? endif //$blocks_l?>
    </div>
    <!-- tenca left -->
    <div id="middle">
      <h2>
        <?=$title;?>
      </h2>
      <? if ($message):?>
        <br />
        <br />
        <?=$message?>
        <br />
        <br />
        <br />
        <? endif //message?>
      <?=$content;?>
    <? if ($blocks_mb):?>
    <div id="middle_bottom">
        <? foreach($blocks_mb as $b): extract ($b)?>
          <?=$block?>
          <? endforeach //$blocks?>
    </div>
    <? endif //$blocks_mb?>
    <!-- tenca middle_bottom -->
    </div>
    <!-- tenca middle -->
    <div class="cls"></div>
  </div>
  <!-- Tenca content -->
  <div id="footer">
    <?=$page_address?>
    <br /><a href="mailto:<?=$default_mail?>" class="links_generals">
  <?=$default_mail?>
  </a>
    <br />
    <a href="http://www.letnd.com" target="_blank"><img src="/common/images/creatper_blanc_<?=LANGUAGE?>.png" width="181" height="25" border="0" /></a></div>
</div>
<!-- tenca container-->
</body>
</html>
