<?

/* 3-5-13 
  Actualitzacio insertar remove_key a newsletter



 */
$table = 'product__customer';
$table_id = 'customer_id';
if (is_table($table)){
	$q0="
			ALTER TABLE ". $table . "
			ADD COLUMN `remove_key` CHAR(50) NOT NULL DEFAULT '0' AFTER ". $table_id . ",
			ADD COLUMN `removed` TINYINT(1) NOT NULL DEFAULT '0' AFTER remove_key;";
	gl_update_execute_query($q0);
	
	$q1 = "SELECT ". $table_id . " AS id FROM ". $table;
	$q2 = "SELECT count(*) FROM ". $table . " WHERE BINARY remove_key = ";
	$q3 = "UPDATE ". $table . " SET remove_key='%s' WHERE  ". $table_id . " = '%s'";
	
	insert_remove_keys_update_5016($q1, $q2, $q3);
}
$table = 'custumer__custumer';
$table_id = 'custumer_id';
if (is_table($table)){
	$q0="
			ALTER TABLE ". $table . "
			ADD COLUMN `remove_key` CHAR(50) NOT NULL DEFAULT '0' AFTER ". $table_id . ",
			ADD COLUMN `removed` TINYINT(1) NOT NULL DEFAULT '0' AFTER remove_key;";
	gl_update_execute_query($q0);
	
	$q1 = "SELECT ". $table_id . " AS id FROM ". $table;
	$q2 = "SELECT count(*) FROM ". $table . " WHERE BINARY remove_key = ";
	$q3 = "UPDATE ". $table . " SET remove_key='%s' WHERE  ". $table_id . " = '%s'";
	
	insert_remove_keys_update_5016($q1, $q2, $q3);
}
$table = 'newsletter__custumer';
$table_id = 'custumer_id';
if (is_table($table)){
	$q0="
			ALTER TABLE ". $table . "
			ADD COLUMN `remove_key` CHAR(50) NOT NULL DEFAULT '0' AFTER ". $table_id . ",
			ADD COLUMN `removed` TINYINT(1) NOT NULL DEFAULT '0' AFTER remove_key;";
	gl_update_execute_query($q0);
	
	$q1 = "SELECT ". $table_id . " AS id FROM ". $table;
	$q2 = "SELECT count(*) FROM ". $table . " WHERE BINARY remove_key = ";
	$q3 = "UPDATE ". $table . " SET remove_key='%s' WHERE  ". $table_id . " = '%s'";
	
	insert_remove_keys_update_5016($q1, $q2, $q3);
}

if (Db::get_first("SELECT count(*) FROM all__menu WHERE menu_id = 3")) {
	$update_sql[]= "UPDATE `all__menu` SET `ordre`=3 WHERE  `menu_id`=14 LIMIT 1;";
	
	$read_write = Db::get_row("SELECT read1,write1 FROM all__menu WHERE menu_id = 3");
	
	$update_sql[]= "
		INSERT INTO `all__menu`		
			(`menu_id`, `menu_group`, `tool`, `tool_section`, `action`, `parent_id`, `toolmode_id`, `variable`, `ordre`, `link`, `process`, `read1`, `write1`) 
		VALUES
			('17', 'newsletter', 'newsletter', 'custumer', 'list_records', '3', '1', 'NEWSLETTER_MENU_CUSTUMER_REMOVED', '2', '', 'removed', '".$read_write['read1']."', '".$read_write['write1']."')";
}

function insert_remove_keys_update_5016($q1,$q2,$q3) {
	$results = Db::get_rows($q1);
	
	Debug::p("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" . $q1 . "<br>", '', false, true, '100');
	
	foreach ($results as $rs) {
		$updated = false; 
		while (!$updated){
			$pass = get_password(50);
			$q_exists = $q2 . "'".$pass."'";	
			
			// si troba regenero pass
			if(Db::get_first($q_exists)){
				$pass = get_password(50);
				
				Debug::p("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>REPE:</b> " . $q_exists . "<br>", '', false, true, '100');				
			}
			// poso el valor al registre
			else{				
				$q_update = sprintf($q3, $pass, $rs['id']);
				
				Db::execute($q_update);
				
				Debug::p("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" . $q_update . "<br>", '', false, true, '100');
				break;
			}
		}
	}
}
?>