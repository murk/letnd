<?
Header( "content-type: application/x-javascript" );
extract( $_GET );
$c_send_edit = addslashes( $c_send_edit );

//  No va al 5 - if ( $theme_advanced_fonts ) echo 'theme_advanced_fonts: "' . $theme_advanced_fonts . '",'
// if ( $theme_advanced_font_sizes ) echo 'theme_advanced_font_sizes: "' . $theme_advanced_font_sizes . '",'

// Els templates es carreguen del directori del client directament tinymce/template.js

// tooltip = "<?=$c_send_edit>"; // Per si poso botó de guardar
?>
<? if ( 1 == 2 ): ?>
	<script>
		<? endif ?>

		  $(function () {
			  tinymces.init();
		  });

		  var tinymces = {
			  init: function () {
				  var template_plugin = '';
				  if (typeof templates == 'undefined') {
					  templates = [];
				  } else {
					  template_plugin = ' template '
				  }

				  var default_options = {
					  // General options
					  language: "<?=$language_code?>",


					  theme: "silver",
					  templates: templates,

					  plugins: 'importcss print preview searchreplace autolink directionality visualblocks visualchars fullscreen image link media ' + template_plugin + ' codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount imagetools textpattern help filemanager responsivefilemanager spellchecker code paste',
					  toolbar: 'styleselect | bold italic underline forecolor backcolor | responsivefilemanager link image media pageembed ' + template_plugin + ' | cut copy paste pastetext removeformat | alignleft aligncenter alignright alignjustify | numlist bullist outdent indent | spellchecker <?if ( $show_html ) echo " | code"?>',

					  // imatges
					  // image_advtab: true, tab per tab avançat per les imatges
					  image_caption: true,

					  // CSS
					  content_css: "/<?=$client_dir?>/templates/styles/editor.css",
					  importcss_append: true,
					  importcss_selector_filter: function (selector) {
						  return selector.indexOf('mceContentBody') === -1;
					  },

					  // spell
					  spellchecker_dialog: true,
					  spellchecker_rpc_url: 'spellchecker.php',
					  spellchecker_languages: "+Català=ca,Español=es,English=en,Français=fr,Deutsch=de",

					  // paste as plain text,
					  paste_word_valid_elements: "b,strong,i,em,h1,h2,h3,h4,h5,p,table,tr,th,td,ul,ol,li",
					  paste_as_text: false,

					  // urls
					  relative_urls: false,
					  remove_script_host: true,

					  // form atributes
					  extended_valid_elements:
						  'form[accept|accept-charset|action|class|dir<ltr?rtl|enctype|id|lang|method<get?post|name|onreset|onsubmit|style|title|target],div[id|class|title],div[*]',

					  // Filemanager
					  external_filemanager_path: "/common/includes/filemanager/",
					  filemanager_title: "<?= $c_file_manager ?>",
					  external_plugins: {
						  "responsivefilemanager": "/common/jscripts/tinymce.5/plugins/responsivefilemanager/plugin.min.js",
						  "filemanager": "/common/includes/filemanager/plugin.min.js"
					  },

					  // Configadmin
					  indentation: '<?=$tinymce_indent?>px'
				  };


			   <? // Per els llistats ?>

				  if (gl_tool_section != 'config' && (gl_action == 'list_records' || gl_action == 'get_records')) {
					  var options_inline = {
						  menubar: false
						  // inline: true - Ha de ser div
					  };
					  $.extend(default_options, options_inline);
				  }

				  if (html_editors["htmlbasic"]) {

					  options = {
						  menubar: false,
						  toolbar: 'bold italic underline | removeformat | formatselect | spellchecker<?if ( $show_html ) echo " | code"?>'
					  };

					  var settings = $.extend({}, default_options, options);

					  tinymces.init_group(settings, html_editors["htmlbasic"]);

				  }
			   <? //  Plugins nomes canvia template_plugin - Toolbar canvia formatselect fontselect fontsizeselect ?>
				  if (html_editors["htmlnewsletter"]) {

					  options = {
						  plugins: 'importcss print preview searchreplace autolink directionality visualblocks visualchars fullscreen image link media codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount imagetools textpattern help responsivefilemanager spellchecker code paste',
						  toolbar: 'formatselect fontselect fontsizeselect | bold italic underline forecolor backcolor | responsivefilemanager link image media pageembed | cut copy paste pastetext removeformat | alignleft aligncenter alignright alignjustify | numlist bullist outdent indent | spellchecker <?if ( $show_html ) echo " | code"?>',
					  };
					  var settings = $.extend({}, default_options, options);
					  delete settings['content_css'];
					  delete settings['importcss_append'];
					  delete settings['importcss_selector_filter'];

					  tinymces.init_group(settings, html_editors["htmlnewsletter"]);
				  }

			   <? //  A partir de tinymce5 queden iguals ?>
				  if (html_editors["html"]) {

					  options = {};
					  var settings = $.extend({}, default_options, options);

					  tinymces.init_group(settings, html_editors["html"]);
				  }
				  if (html_editors["htmlfull"]) {

					  options = {};
					  var settings = $.extend({}, default_options, options);

					  tinymces.init_group(settings, html_editors["htmlfull"]);
				  }
			  },
			  init_group: function (settings, editors) {

				  for (var i in editors) {
					  var editor = editors[i];
					  var spell_language = $(editor).data('language');
					  settings['selector'] = editor;
					  settings['spellchecker_language'] = spell_language ? spell_language : settings['language'];
					  tinyMCE.init(settings);
				  }
			  }
		  };

		<? if( 1 == 2 ): ?>
	</script>
<? endif ?>