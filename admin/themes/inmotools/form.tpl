<script language="JavaScript" src="/admin/jscripts/validacio.js?v=<?= $version ?>"></script>
<script language="JavaScript" src="/admin/jscripts/euro_calc.js?v=<?= $version ?>"></script>
<!--<script language="JavaScript" src="/common/jscripts/multifile.js?v=--><?//= $version ?><!--"></script>-->
<? if($has_images): ?><? if($image_name): ?>
	<script type="text/javascript">

		var gl_gallery_images = <?= ImageManager::get_gallery_images($images) ?>;

		// gl_default_imatge = '<?=$images[0]['image_name_medium']?>';
	</script>
<? endif // image_name?><? endif // has_images?>
<form name="form_delete" method="post" action="<?= $link_action_delete ?>" target="save_frame">
	<input name="<?= $id_field ?>" type="hidden" value="<?= $id ?>">
</form>
<form name="theForm" method="post" action="<?= $link_action ?>" onsubmit="<?= $js_string ?>"
      encType="multipart/form-data" target="save_frame">
	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="form">
		<tr>
			<td><? if($form_new && !$is_print_page): ?><? if($save_button): ?>
					<div id="form_buttons_holder">
						<table id="form_buttons">
							<tr>
								<td class="buttons_top">
									<div class="right"><input type="submit" name="Submit" value="<?= $c_send_new ?>"
									                          class="btn_form1"/></div>
								</td>
							</tr>
						</table>
					</div>
				<? endif //save_button?>
				<? endif //form_new?>
				<? if($form_edit && !$is_print_page): ?>
					<div id="form_buttons_holder">
						<table id="form_buttons">
							<tr>
								<td valign="bottom">
									<div class="menu">
										<div><?= $c_edit_data ?></div>
										<? if($has_images): ?><a href="<?= $image_link ?>"><?= $c_edit_images ?></a><? endif //has_images?>
										
						                <? foreach($form_tabs as $r): extract( $r ) ?>
							                <? if ( $tab_action ): ?>
								                <a href="?action=<?= $tab_action ?>&<?= $id_field ?>=<?= $id ?>&menu_id=<?= $menu_id ?>"><?= $tab_caption ?></a>
						                    <? else: // ?>
												<div><?= $tab_caption ?></div>
							                <? endif // $tab_action ?>
						                <? endforeach //$form_tabs?>
									</div>
								</td>
								<td class="buttons_top">
									<div class="right">
										<? if($show_previous_link): ?><a class="boto2 btn_show_previous_link"
										                          href="<?= $show_previous_link ?>"></a><? endif //show_previous_link?>
										<? if($show_next_link): ?><a class="boto2 btn_show_next_link"
										                          href="<?= $show_next_link ?>"></a><? endif //show_next_link?>
										<? if($back_button): ?><a class="btn_form_back"
										                          href="<?= $back_button ?>"></a><? endif //back_button?>
										<? if($save_button): ?><input type="submit" name="Submit"
										                              value="<?= $c_send_edit ?>"
										                              class="btn_form1"><? endif //save_button?><? if($delete_button): ?>
											<a class="btn_form_delete"
											   href="javascript:submit_form('<? if($has_bin): ?><?= $c_confirm_form_bin ?><? else: ?><?= $c_confirm_form_deleted ?><? endif //has_bin?>', true);">
											<?= $c_delete ?>
											</a><? endif //delete_button?><? if($preview_button): ?><a class="btn_form1"
										                                                               href="<?= $preview_link ?>"
										                                                               target="_blank">
								<?= $c_preview ?>
											</a><? endif //preview_button?><? if($print_button): ?><a class="btn_form1 btn_print"
										                                                              href="<?= $print_link ?>">
											<?= $c_print ?>
											</a> <? endif //print_button?></div>
								</td>
							</tr>
						</table>
					</div>
				<? endif //form_edit?>
				<table width="100%" border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td><? if($c_form_title): ?>
								<table width="100%" cellpadding="4" cellspacing="0">
									<tr>
										<td class="formCaptionTitle"><strong>
												<?= $is_print_page?$c_print_title:$c_form_title ?> <? if($form_subtitle): ?>
													<span><?= $form_subtitle ?></span><? endif //form_subtitle?>
											</strong></td>
									</tr>
								</table>
							<? endif //c_form_title?><? if($has_images): ?><? if($image_name): ?>
								<table width="100%" border="0" cellpadding="0" cellspacing="0" class="images">
									<tr>
										<? foreach($images as $image): extract( $image ) ?>
											<td align="center"><? if($image_name_thumb): ?>
													<img onClick="open_viewer(this, '<?= $image_conta ?>')" src="<?= $image_src_admin_thumb ?>" vspace="10" border="0"/>
												<? endif //$image_name_details?></td>
											<? if($image_conta == 2) break; ?>
										<? endforeach //$images?>
									</tr>
								</table>
							<? endif // image_name?><? endif // has_images?>
							<table width="100%" cellpadding="4" cellspacing="0" class="formstbl">
								<? foreach($fields as $l): extract( $l ) ?>
									<? if($form_level1_title): ?>
										<tr>
											<td colspan="2" class="formCaptionTitle"><?= $form_level1_title ?></td>
										</tr>
									<? endif //form_level1_title?>

									<? if($form_level2_title): ?>
										<tr>
											<td colspan="2" class="formsCaptionHor"><?= $form_level2_title ?></td>
										</tr>
									<? endif //form_level2_title?>
									<tr id="<?= $field_name . "_holder" ?>">
										<td width="150" valign="top" class="formsCaption"><? if($key): ?><strong>
												<?= $key ?>:</strong><? endif //key?></td>
										<td valign="top" class="forms">
											<table width="100%" border="0" cellpadding="0" cellspacing="0">
												<? foreach($val as $l): extract( $l ) ?>
													<? if ( $lang ): ?>
														<tr>
															<td class="langCaption"><?= $lang ?></td>
														</tr>
													<? endif //lang?>
													<tr>
														<td><?= $val ?></td>
													</tr>
												<? endforeach //$val?>
											</table>
										</td>
									</tr>
								<? endforeach //$field?>
							</table>
							<table width="100%" cellpadding="4" cellspacing="2" class="formsButtonstbl">
								<tr>
									<td align="center" class="formsButtons">
										<div class="centerFloat1">
											<div class="centerFloat2"><? if($form_new): ?><? if($save_button): ?>
													<input type="submit" name="Submit2" value="<?= $c_send_new ?>"
													       class="btn_form1"/>
												<? endif //save_button?><? endif //form_new?>
												<? if($form_edit): ?>
													<? if($save_button): ?><input type="submit" name="Submit"
													                              value="<?= $c_send_edit ?>"
													                              class="btn_form1"><? endif //save_button?><? if($delete_button): ?>
														<a class="btn_form_delete"
														   href="javascript:submit_form('<? if($has_bin): ?><?= $c_confirm_form_bin ?><? else: ?><?= $c_confirm_form_deleted ?><? endif //has_bin?>', true);">
														<?= $c_delete ?>
														</a><? endif //delete_button?><? if($preview_button): ?><a
														class="btn_form1" href="<?= $preview_link ?>" target="_blank">
														<?= $c_preview ?>
														</a><? endif //preview_button?><? if($print_button): ?><a
														class="btn_form1" href="<?= $print_link ?>">
														<?= $c_print ?>
														</a> <? endif //print_button?>
												<? endif //form_edit?></div>
										</div>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</form>
<script type="text/javascript">
	$(function () {
		set_focus(document.theForm);
	});
</script>
