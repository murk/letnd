<?php
$gl_caption ['c_send_mail']='Enviar Correo';
$gl_messages_cart ['no_records']=$gl_caption_cart ['c_no_records']=$gl_caption_cart ['c_empty_cart']='La cesta está vacía';
$gl_messages_order ['no_records'] = 'Aún no has realizado ningún pedido';

$gl_caption['c_productes_destacats']='Productos destacados';
$gl_caption['c_products']='Productos';
$gl_caption['c_product']='Producto';
$gl_caption['c_files']='Archivos';
$gl_caption['c_discount'] = 'Descuento';

// NO TRADUIT

$gl_caption ['c_next_payment_step']='Siguiente';
$gl_caption ['c_continue_shopping']='Seguir comprando';
$gl_caption ['c_text_register']='Vamos a proceder a la creación de su cuenta';
$gl_caption ['c_register']='Registrarse';
$gl_caption_customer ['c_form_title']=$gl_caption ['c_summary_title']='Datos personales';
$gl_caption_customer ['c_title_register']='Registrarse';
$gl_caption_customer ['c_title_login']='Identificación';
$gl_caption_customer ['c_is_customer']='Ya es cliente';
$gl_caption_customer ['c_new_customer']='Nuevo cliente';
$gl_caption_customer ['c_mail_log']='Introduzca su e-mail';
$gl_caption_customer ['c_password_log']='Introduzca su contraseña';
$gl_caption_customer['c_customer_menu'] = 'Mi cuenta';
$gl_caption_customer['c_custumer_menu_personal_data'] = 'Mis datos personales';
$gl_caption_customer['c_custumer_menu_orders'] = 'Mis pedidos';
$gl_caption['c_custumer_menu_logout'] = 'Desconectar';

$gl_caption['c_title_cart'] = 'Cesta de la compra';
$gl_caption['c_my_cart'] = 'Mi cesta';
$gl_caption['c_cart'] = 'Cesta';
$gl_caption['c_add_to_cart'] = 'Añadir a la cesta';
$gl_caption['c_see_cart'] = 'Ver cesta';
$gl_caption['c_buy'] = 'Comprar';

$gl_caption_orderitem['c_title_cart'] = 'Compra';
$gl_caption_order['c_list_title'] = 'Pedidos';

$gl_caption_payment['c_payment_title'] = 'Efectuando la compra';
$gl_caption_payment['c_payment_login'] = 'Identificación';
$gl_caption_payment['c_payment_summary'] = 'Resumen';
$gl_caption_payment['c_payment_payment'] = 'Pago';
$gl_caption_payment['c_payment_end'] = 'Compra finalizada';
$gl_caption_payment ['c_cart_is_empty']='No se puede realizar ningún pedido, la cesta esta vacia';

$gl_caption_payment['c_method_title1']="Escoja el metodo de pago que desea utilizar.";
$gl_caption_payment['c_method_title2']="El importe total es de <strong>%s&nbsp;%s</strong> I.V.A incluido.";
$gl_caption_payment['c_method_paypal']="Pago con PayPal";
$gl_caption_payment['c_method_account']="Pago por transferencia bancaria";
$gl_caption_payment['c_method_ondelivery']="Pago contrareembolso";
$gl_caption_payment['c_method_directdebit']="Recibo bancario";
$gl_caption_payment['c_method_4b']="Tarjeta de crédito (4b)";
$gl_caption_payment['c_method_lacaixa']="Tarjeta de crédito";

$gl_caption_payment['c_show_paypal']="Se está efectuando el pago a Paypal, si no empieza el proceso automáticamente clike aqui:";
$gl_caption_payment['c_show_4b']="Se está efectuando el pago a Sistema 4b, si no empieza el proceso automáticamente clike aqui:";
$gl_caption_payment['c_show_lacaixa']="Se está efectuando el pago por tarjeta, si no empieza el proceso automáticamente clike aqui:";
$gl_caption_payment['c_show_account']="Haga un ingreso al número de cuenta: ";
$gl_caption_payment['c_show_account_mail']="Por favor, haga un ingreso al número de cuenta: %s, indicando su número de pedido %s";
$gl_caption_payment['c_show_ondelivery']="";
$gl_caption_payment['c_show_directdebit']="";
$gl_caption_payment['c_show_end']="";
$gl_caption_payment['c_mail_order_saved']="<strong>Se ha realizado un pedido nuevo en su Tienda on-line!</strong><br><br>En caso que este pedido se confirme recibirá una nueva notificación.";

$gl_caption_payment['c_mail_order_subject_seller']='Pedido pendiente nº %s / Cliente nº %s';
$gl_caption_payment['c_mail_subject_seller']='Resumen del pedido nº %s / Cliente nº %s';
$gl_caption_payment['c_mail_subject_buyer']='Resumen del pedido en %2$s nº %1$s';
$gl_caption_payment['c_text_buyer']='%s %s,';
$gl_caption_payment['c_text_buyer2']= 'Muchas gracias por haber comprado en %s, su número de pedido es: <strong>%s</strong> y su número de cliente: <strong>%s</strong><br><br>Recuerde que puede comprobar el estado de su pedido en el apartado de <a href="%s">Mi cuenta</a>';
$gl_caption_payment['c_text_buyer_no_session']= 'Muchas gracias por haver comprado en %s';
$gl_caption_payment['c_text_buyer_no_order']= 'No se ha realizado ningún pedido, si desea ayuda por favor contacte con nosotros';
$gl_caption_payment['c_text_seller']='El cliente: %s, ha realizado un pedido en %s.';
$gl_caption_payment['c_text_seller_pay_now']='El cliente: %s, ha efectuado el pago pendiente en %s.';

$gl_caption_payment ['c_paypal_pending_text_buyer']='El pedido queda pendiente de pago en espera de la confirmación de pago por Paypal';
$gl_caption_payment ['c_paypal_pending_text_seller']='El pedido queda pendiente de pago en espera de la confirmación de pago por Paypal';
$gl_caption_payment ['c_paypal_completed_text_buyer']='Pago con Paypal confirmado';
$gl_caption_payment ['c_paypal_completed_text_seller']='Pago con Paypal confirmado';

$gl_caption_payment ['c_4b_pending_text_buyer']='';
$gl_caption_payment ['c_4b_pending_text_seller']='';
$gl_caption_payment ['c_4b_completed_text_buyer']='Pago con tarjeta confirmado';
$gl_caption_payment ['c_4b_completed_text_seller']='Pago con tarjeta confirmado';

$gl_caption_payment ['c_lacaixa_pending_text_buyer']='';
$gl_caption_payment ['c_lacaixa_pending_text_seller']='';
$gl_caption_payment ['c_lacaixa_completed_text_buyer']='Pago con tarjeta confirmado';
$gl_caption_payment ['c_lacaixa_completed_text_seller']='Pago con tarjeta confirmado';

$gl_caption_payment ['c_account_text_buyer']='A continuación le detallamos los datos para poder realizar la transferencia bancaria';
$gl_caption_payment ['c_account_text_seller']='El pedido queda pendiente de pago en espera de la transferéncia bancaria';
$gl_caption_payment ['c_ondelivery_text_buyer']='';
$gl_caption_payment ['c_ondelivery_text_seller']='';
$gl_caption_payment ['c_paypal_text_buyer']='';
$gl_caption_payment ['c_paypal_text_seller']='';
$gl_caption_payment ['c_directdebit_text_buyer']='El pedido se cargará a su número de cuenta';
$gl_caption_payment ['c_directdebit_text_seller']='';

$gl_caption['c_account_title']= 'Datos de la transferencia' ;
$gl_caption['c_account_amount']= 'Importe' ;
$gl_caption['c_account_concept']= 'Concepto' ;

$gl_messages_cart ['no_records']='La cesta está vacia';


// traduir al català
$gl_caption['c_password_forgotten'] = '¿Ha olvidado su contraseña?';
$gl_caption['c_password_insert_mail'] = 'Por favor, introduzca su dirección de email y le enviaremos una contraseña nueva a su cuenta de correo.';
$gl_caption['c_password_mail_error'] = 'La dirección de email proporcionada no existe en nuestra base de datos, por favor, escriba la dirección correctamente';
$gl_caption['c_password_sent'] = 'Se ha enviado una nueva contraseña a su dirección de email';
$gl_caption['c_password_mail_subject'] = 'Su nueva contraseña';
$gl_caption['c_password_mail_text_1'] = 'Su nueva contraseña es';
$gl_caption['c_password_mail_text_2'] = 'Puede cambiar la contraseña en el apartado "Su cuenta" de nuestra página web';

$gl_messages_customer['registered_succesful']="El registro se ha efectuado correctamente.<br><br>En breve le notificaremos por email la activación de su cuenta";
$gl_caption_customer['c_subject'] = 'Nuevo cliente registrado';
$gl_caption_customer['c_body_1'] = 'Se ha registrado un nuevo cliente en la tienda on-line';
$gl_caption_customer['c_body_2'] = 'Este cliente està desactivado, se puede activar en el %sárea de gestión de clientes%s';

$gl_caption['c_product_list_view_all'] = 'Ver todos';

$gl_caption_cart['c_promcode_err_not_valid'] = 'El código promocional no es válido';
$gl_caption_cart['c_promcode_err_minimum'] = 'Para aplicar el descuento la compra tiene que ser superior a %s';
$gl_caption_cart['c_promcode_err_expired'] = 'El código promocional ha expirado';
$gl_caption_cart['c_promcode_err_used'] = 'Este código ya se ha utilizado una vez';
$gl_caption_cart['c_promcode_button'] = 'Aplicar';

$gl_messages_cart['c_no_stock_message'] = 'Este producto está fuera de stock';
$gl_messages_cart['c_max_stock_reached_message'] = 'Se ha llegado a la cantidad máxima disponible para este producto';

$gl_caption['c_last_units'] = 'Últimas unidades';
$gl_caption_product['c_price_consult'] = 'Precio a consultar';

$gl_caption['c_customer_comment'] = 'Observaciones';
?>