<?

/* 25-4-16

	PRODUCT
	- Mostra descripció familia com a subtitol de la web
 */
if ( is_table( 'product__configpublic' ) && ! Db::get_first( "SELECT * FROM product__configpublic WHERE name='show_listing_as_subtitle'" ) ) {
	$update_sql[] = "
		INSERT INTO product__configpublic (`name`, `value`) VALUES ('show_listing_as_subtitle', '');";

} else {
	if ( is_table( 'product__configpublic' ) ) {
		trigger_error( 'Ja existeixen el camp show_listing_as_subtitle' );
	}
}

?>