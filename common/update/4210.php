<?php
// no es poden posar ; entre mig de la consulta ja que parteix les consultes per el ; a no ser que fem servir $update_sql en array()
if (is_table("news__new_language")){
		
	$update_sql[]="ALTER TABLE `news__new_language`	ADD COLUMN `url1` VARCHAR(255) NOT NULL AFTER `url2_name`,	ADD COLUMN `url2` VARCHAR(255) NOT NULL AFTER `url1`;";
	
	$update_sql[]="UPDATE `news__new_language` SET `url1`= (SELECT url1 FROM news__new WHERE news__new.new_id = news__new_language.new_id);";
	$update_sql[]="UPDATE `news__new_language` SET `url2`= (SELECT url2 FROM news__new WHERE news__new.new_id = news__new_language.new_id);";
	
	$update_sql[]="ALTER TABLE `news__new` DROP COLUMN `url1`, DROP COLUMN `url2`;";
}	
?>