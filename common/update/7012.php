<?php
/* 26-4-17

Afegir agents de INMO

*/

if (!is_table("user__configadmin")){

	$update_sql[]="CREATE TABLE IF NOT EXISTS `user__configadmin` (
  `configadmin_id` int(11) NOT NULL auto_increment,
  `name` varchar(50) collate utf8_spanish2_ci NOT NULL,
  `value` varchar(255) collate utf8_spanish2_ci NOT NULL,
  PRIMARY KEY  (`configadmin_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;";

	$update_sql[]="
INSERT INTO `user__configadmin` (`configadmin_id`, `name`, `value`) VALUES
	(1, 'im_admin_thumb_w', '200'),
	(2, 'im_admin_thumb_h', '133'),
	(3, 'im_admin_thumb_q', '80'),
	(4, 'im_thumb_w', '200'),
	(5, 'im_thumb_h', '150'),
	(6, 'im_thumb_q', '80'),
	(7, 'im_details_w', '540'),
	(8, 'im_details_h', '360'),
	(9, 'im_details_q', '80'),
	(10, 'im_medium_w', '900'),
	(11, 'im_medium_h', '675'),
	(12, 'im_medium_q', '70'),
	(13, 'im_big_w', '1200'),
	(14, 'im_big_h', '900'),
	(15, 'im_big_q', '70'),
	(16, 'im_list_cols', '2'),
	(17, 'im_custom_ratios', '');
";
}


?>