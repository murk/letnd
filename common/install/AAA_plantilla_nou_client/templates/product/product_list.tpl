<?
$hidden_class = '';
?>

<? if ( $family_description ): ?>
	<div class="family-description row"><?= $family_description ?></div>
<? endif ?>

<?= get_block( 'product_order' ) ?>

	<div class="product-list">
		<? foreach ( $loop as $l ): extract( $l ) ?>

			<? include( PATH_TEMPLATES . 'product/product_list_item.tpl' ) ?>

		<? endforeach //$loop?>
	</div>

<? include( PATH_TEMPLATES . 'results.tpl' ) ?>