<table class="letnd-list" border="0" cellspacing="0" cellpadding="0">
	<?foreach($loop as $l): extract ($l)?>
	<tr>
		<td class="letnd-list-image <?=$first_last?>" valign="top">
			<?if ($images):?>
				<a title="<?=htmlspecialchars($property)?>" href="<?= HTTP ?><?= SHORT_HOST_URL ?><?=$show_record_link?>"><img src="<?=$image_src_thumb?>" width="180" border="0" alt="<?=htmlspecialchars($property)?>" /></a>
			<?endif //image?>
		</td>	
		<td valign="top" class="letnd-list-details <?=$first_last?>">
			<?if ($property):?>
				<h2>
				<?=$property?>
				</h2>
			<?endif //property?>
			<?if ($property_title):?>
				<h3>
				<?=$property_title?>         
				</h3>
			<?endif //property_title?> 
			<div class="inmo-list-ref"><?=$c_ref?> <?=$ref?></div>
			<p><?=$description?></p>			
			
			<p class="letnd-info"><a title="<?=htmlspecialchars($property)?>" href="<?= HTTP ?><?= SHORT_HOST_URL ?><?=$show_record_link?>"><?=$c_more_info?></a></p>
			
			<?if ($price || $price_consult):?>
			<table border="0" cellspacing="0" cellpadding="0" class="inmo-list-price">
				<tr>
					<td class="inmo-list-price-sep">&nbsp;
					</td>
				</tr>
				<tr>
					<td class="inmo-list-price-td">
						<?if ($price):?><strong><?=$c_price?>: </strong>
							<?=$price?>
							<?=CURRENCY_NAME?>&nbsp;
						<?endif //price?>
						<?if ($price_consult):?>
							<?=$price_consult?>
						<?endif //price_consult?>
					</td>
				</tr>
			</table>	
			<?endif //price?>
			
			
		</td>
	</tr>
	<?endforeach //$loop?>
</table>