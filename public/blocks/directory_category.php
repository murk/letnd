<?php

/* 15-9-16
 * 
 * Menu o bloc de categories
 * Nomes va amb USE_FRIENLY_URL
 *  
 */
$block_max_results = isset($vars['block_max_results']) ? $vars['block_max_results'] : false; // si no està definit els llista tots tipus menu

$order = " ORDER BY ordre ASC, category ";
$limit = $block_max_results?"LIMIT " . $block_max_results:'';

$query = "SELECT category, directory__category_language.category_id as category_id
				FROM directory__category_language, directory__category
				WHERE directory__category_language.category_id =  directory__category.category_id
				AND language = '" . LANGUAGE . "'" . $order .
				$limit;

$results = Db::get_rows($query);
$selected_found = false;

// $vars['block_selected'] = (isset($_GET['category']))?1:0;

if ($results) {
    $conta = 0;
    $loop_count = count($results);
    $category_id = R::get('category');
    
    foreach ($results as $rs) {

		$l = array();

		// CONTA, es mes rapid que posant la consulta com a subquery
		$document_count = Db::get_first("SELECT count(*) 
							FROM directory__document
							WHERE category_id = " . $rs['category_id'] . "
							AND (status = 'public') 
							AND bin = 0
							LIMIT 1");
        
        
		if ($document_count) {
			
			$l['link'] = 
				get_link( array(
						'tool' => 'directory',
						'tool_section' => 'document',
						'category' => 'category',
						'category_id' => $rs['category_id'],
						'detect_page' => true
				));
			
			
			$l['category'] = $l['item'] = $rs['category'];
			$l['category_id'] = $l['id'] = $rs['category_id'];
			$l['class'] = '';
			$l['loop_count'] = $loop_count;

			if ($category_id == $rs["category_id"]){
				$selected_found = true;
				$selected = 1;
			}
			else {
				$selected = 0;
			}

			$l['selected'] = $selected;
			//$l['selected'] = $category_id == $rs["category"] && $vars['block_selected'] ? 1 : 0;

			/*
			if ($show_images)
				$l += UploadFiles::get_record_images($rs['category_id'], true, 'directory', 'category');
			 * 
			 */

			$l['conta'] = $conta++;

			$loop[] = $l;
		}
        
    }

    if ($loop) {
        $loop[0]['class'] = 'first';
        $loop[count($loop) - 1]['class'] = 'last';

	    if (!$selected_found && DIRECTORY_LIST_FIRST_CATEGORY) {
	    	$loop[0]['selected'] = 1;
	    }
    }
} else {
    $show_block = false;
    return;
}
?>