<?
/**
 * TestTest
 *
 * Per fer probes
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2017
 * @version $Id$
 * @access public
 */
class TestTest extends Module {

	public function __construct() {
		parent::__construct();
	}

	public function list_records() {


		html_end();
	}


	/**
	 * Test::trace()
	 * Crea un arxiu de log
	 *
	 * @param $text
	 * @param string $name
	 * @param bool $title
	 * @param $file
	 */
	static public function trace($text, $name = '', $title = false, $file = 'test'){

		$fp = fopen (CLIENT_PATH . '/temp/'.$file.'.log', "a+");

		$log = '';
		if ($title){
			$log .="\n\n\n================================================\n" . $title . ' - ' .date("Y-m-d H:i:s"). " - " . $_SERVER['REMOTE_ADDR'] . "\n================================================\n";
		}
		if ($name){
			$name .= ": ";
		}
		$log .= "\n" . $name . $text;

		fwrite($fp,$log);
		fclose($fp);
	}
}