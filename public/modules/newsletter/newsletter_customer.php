<?
/**
 * NewsletterCustomer
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2012
 * @version $Id$
 * @access public
 */
class NewsletterCustomer extends Module{
	var $is_block = false, $never_mind_logged=false; // per sobreescriure el session_id quan no n'hi ha ( distribuidors )
	function __construct(){
		parent::__construct();
	}
	function on_load(){
		$this->table = $this->config['custumer_table'];
		$customer_id = explode('__',$this->table);
		$this->id_field = $customer_id[1].'_id'; // dos vegades per que al fer set_config es reescriu, i aquí el necessito per els idiomes
			
		$this->set_config($customer_id[0] . '/' . $customer_id[0] . '_' . $customer_id[1] . '_config.php');
		
		$this->id_field = $customer_id[1].'_id';
		
		// no vull que sobreescrigui el misatges del mail de l'eina product, nomès el formulari
		if ($this->table == 'product__customer' && $this->action!='send_password'){
			include (DOCUMENT_ROOT . 'admin/modules/product/languages/' . LANGUAGE . '.php');
			$this->caption = array_merge($this->caption, $gl_caption);
			$this->caption = array_merge($this->caption, $gl_caption_customer);
		}
	}
	// obtinc html del form per al block
	function get_show_block()
	{
		$this->is_block = true;
		return $this->do_action('get_form_new');
	}
	function show_form()
	{		
		$GLOBALS['gl_content'] = $this->get_form();
		
		// Nomes poso el menu de customer quan aquest modul es index ( o sigui no el crido desde cap altre modul)
		if ($this->is_logged() && $this->is_index) {
			$GLOBALS['gl_page']->title = $this->caption['c_title_edit'];
		}
		elseif ($this->is_index){
			$GLOBALS['gl_page']->title = $this->caption['c_title_register'];
		}
	}
	function get_form()
	{
		
		// ho trec perque ha clubmoto funciona amb els usuaris product, però amb newsletter, a admin està així, ja veurem que faig
		/*if ($this->table=='product__customer') {
			// no ho he testejat mai !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
			$module = Module::load('product','customer');
			$module->parent = 'NewsletterCustumer';
			$ret = $module->do_action();
			
		}
		else{*/
			// aquest si l'he testejat
			$show = new ShowForm($this);
			$show->set_var('is_block', $this->is_block);
			$show->set_var('is_logged', $this->is_logged());
			$show->set_field('group_id','form_public','input');
			
			if ($this->table=='newsletter__custumer'){
				$show->unset_field('user_id');			
				$show->unset_field('category_id');
			}
			$show->get_values();
			$ret =  $show->show_form();
			
		/*}*/
		
			
		$link_register = Page::get_link_short($this, 'show_form_new');
		$link_send_password = Page::get_link_short($this, 'send_password');

		$this->set_vars(array(
			'link_register'=>$link_register,
			'link_send_password'=>$link_send_password
			));
		
		
	    return $ret;
	}

	function write_record()
	{
	   
		$this->is_block = R::id('is_block',false,'_POST');
		$writerec = new SaveRows($this);

		$mail = $writerec->get_value('mail');

		if (!$mail) die(''); // això es que s'ha fet un submit sense jscript

		$writerec->field_unique = "mail";
		
		// si es nou registre i block redirigeixo al menu de custormer
		if ($this->action=='add_record') {
		
			if ($this->table == 'product__customer')
				$writerec->set_field('activated','override_save_value','1');		
			
		}
		
	    $writerec->save();

		if (!$writerec->saved) return; // si no ha grabat (ej. add_exists) no faig res més
		
		
		// poso el remove key automaticament
		if ($this->action=='add_record' && $GLOBALS['gl_insert_id'])
			$this->newsletter_add_remove_key($this->table, $GLOBALS['gl_insert_id']);
		
		
		
		// guardo en sessio si s'ha registrat correctament
		if ($this->action=='add_record') {
			$customer_id = $GLOBALS['gl_insert_id'];
			
			// creo un registre vuit per l'adresa de facturacio si som a product__customer
			// es sempre obligatori tindre una adresa, sino peta el form	
			if ($this->table == 'product__customer') {	
				$_POST['address_id'] = 0; // nomes per evitar undefined
				$customer_id = $GLOBALS['gl_insert_id'];
				$module = Module::load('product','address');
				$module->parent = 'ProductCustomer';
				$module->customer_id = $customer_id;
				$module->set_field('invoice','override_save_value','1'); // aquesta adreça es la de facturació
				$module->do_action($this->action);
			}
			
			// si hi ha group per defecte relaciono el grup i no hi ha un post de group_id (te preferencia)
			
			if ($this->config['default_group_public'] && $writerec->get_value('group_id')===false){
				$group_ids = explode(',', $this->config['default_group_public']);
				foreach ($group_ids as $group_id){
					Db::execute('
						INSERT into newsletter__custumer_to_group 
						(custumer_id, group_id) 
						VALUES ('.$customer_id.', '.$group_id . ');');			
				}
			}
			
			$_SESSION['newsletter']['customer_id'] = $customer_id;
			$_SESSION['newsletter']['mail'] = current($_POST['mail']);
			$_SESSION['newsletter']['name'] = current($_POST['name']);
			$_SESSION['newsletter']['surname'] = current($_POST['surname']);			
				
			//$this->send_mail_registered($customer_id);	
			
			// si es nou registre i no es block redirigeixo al form de custormer
			// al redirigir miro si hi ha la funcio javascript custom per el tool
			if (!$this->is_block && !isset($_POST['redirect'])){
			
				$message = $GLOBALS['gl_message'];
				$js = $GLOBALS['gl_page']->get_javascript_show_message($message,'saved');
				$js .= '
					else{
						top.window.location.href = "'.Page::get_link_short($this, 'show_form_edit').'";
						if (typeof top.hidePopWin == "function") top.window.setTimeout("hidePopWin(false);", 1000);
						if (typeof top.showPopWin == "function") top.showPopWin("", 300, 100, null, false, false, "'.addslashes($message).'");
					}';
				print_javascript($js);
				Debug::p_all();
				die();
			}
			elseif ($this->is_block && !isset($_POST['redirect'])){
			
				$message = $GLOBALS['gl_message'];
				$js = $GLOBALS['gl_page']->get_javascript_show_message($message,'saved');
				$js .= '
					else{
						top.window.$("#newsletter_block_form_sent").show();
						top.window.$("#newsletter_block_form_not_sent").hide();
						top.window.$("#newsletter_block_form_area").hide();
						top.window.$(".newsletter_block_form_area").hide();
						
						if (typeof top.hidePopWin == "function") top.window.setTimeout("hidePopWin(false);", 1000);
						if (typeof top.showPopWin == "function") top.showPopWin("", 300, 100, null, false, false, "'.addslashes($message).'");
					}';
				print_javascript($js);				
				Debug::p_all();
				die();
			}
			
		}
		
		// al formulari puc posar un input de nom redirect per redireccionar un cop guardat		
		if (isset($_POST['redirect'])) {
			$redirect = $_POST['redirect'];
			print_javascript('top.window.location.href = "'.addslashes($redirect).'";');
			Debug::p_all();
			die();
		}


	}
	/* no se si fer-ho, de moment no
	function send_mail_registered($customer_id){
		$this->never_mind_logged = true;
		$this->action = 'get_record';
		
		// Agafo formulari del customer, però amb el form.tpl per defecte
		$show = new ShowForm($this);
		$show->id = $customer_id;
		$show->set_field('password','form_public','no');
		$show->use_default_template = true;
		$customer = $show->show_form();
		
		$page = $GLOBALS['gl_page'];
		$page->template = 'mail.tpl';
		$page->set_var('preview','');
		$page->content = $this->caption['c_body_1'] . ': <br>' . $customer;
		$body = $page->get();
		
		// envio mail
		$mailer = get_mailer($this->config);
		if (is_file(CLIENT_PATH . 'images/logo_newsletter.jpg'))
			$mailer->AddEmbeddedImage(CLIENT_PATH . 'images/logo_newsletter.jpg', 'logo', 'logo.jpg');
		$mailer->Body = $body;
		$mailer->Subject = $this->caption['c_subject'];
		$mailer->IsHTML(true);
		$mailer->AddAddress ($this->config['default_mail']);
		send_mail($mailer, false);
	}
	*/
	function show_message(){
		// res, nomes es per mostrar el missatge i ja es fa automaticament a "message"
	}

	function manage_images()
	{
	    $image_manager = new ImageManager($this);
	    $image_manager->execute();
	}
	function show_login(){
		if ($this->is_logged()) {
			$this->do_action('show_form_edit');
		}
		else{
			$content = $this->get_login();
			$GLOBALS['gl_page']->title = $this->caption['c_title_login'];
			$GLOBALS['gl_content'] = $content;
		}
	}
	function get_login(){
		$this->set_file('newsletter/customer_login.tpl');
		$this->set_vars($this->caption);	
		
		$link_action = Page::get_link_short($this, 'login');		
		$link_register = Page::get_link_short($this, 'show_form_new');		
		$link_send_password = Page::get_link_short($this, 'send_password');

		$this->set_var('js_string',"return validar(document.theForm,'mail','".addslashes($this->caption['c_mail'])."','email1','password','".addslashes($this->caption['c_password'])."','text1');");
		$this->set_vars(array(
			'link_action'=>$link_action,
			'link_register'=>$link_register,
			'link_send_password'=>$link_send_password
			));
		return $this->process();
	}
	function login()
	{
		global $gl_message, $gl_page;
		debug::add('Post',$_POST);
		if (isset($_POST['mail']) && $_POST['mail']!='' && $_POST['password']!='')
		{	// nomès pot haber un login igual
			$query = "SELECT ".$this->id_field." as customer_id, mail, password, name, surname
						FROM ".$this->table."
						WHERE mail = " . Db::qstr($_POST['mail']) . "
						AND  password = " . Db::qstr($_POST['password']) . "
						AND bin=0";
			
			if ($this->table=='product__customer')
				$query .= "	AND activated=1";
			
			$rs = Db::get_row($query);
			Debug::add('Query client',$query);
			Debug::add('Results client',$rs);
			// més segur amb 2 comprovacions
			if (($rs['mail'] == $_POST['mail'])
				&& ($rs['password'] == $_POST['password']))
			{
				$_SESSION['newsletter']['customer_id'] = $rs['customer_id'];
				$_SESSION['newsletter']['mail'] = $rs['mail'];
				$_SESSION['newsletter']['name'] = $rs['name'];
				$_SESSION['newsletter']['surname'] = $rs['surname'];

				
				// així no DONA ERROR AMB LOGOUT, (recarregava i feia logout i mai conseguiem fer login)
				if (strpos($_SERVER['HTTP_REFERER'],'action=logout')) {
					print_javascript('top.window.location.href = "'.Page::get_link_short($this, 'show_form_new').'";');
				}
				else{
					if (isset($_POST['redirect'])) {
						$redirect = $_POST['redirect'];
						print_javascript('top.window.location.href = "'.addslashes($redirect).'";');
					}
					else {
						$js = $gl_page->get_javascript_show_message(USER_LOGIN_VALID,'logged');
						$js .= '
							else{
								top.window.location.href=top.window.location.href;
							}';
						print_javascript($js);
					}
				}
			}
			// mostra missatge d'error
			else
			{
				$gl_page->show_message(USER_LOGIN_INVALID,'not_logged');
			}
			Debug::p_all();
			die();
		}
		// mostra formulari de login, si no s'ha entrat les dades bé o si s'entra per primer cop
		else
		{
			// guardo post en sessió per poder guardar al entrar login i password correctes
			if ($_POST) {
				$_SESSION['post'] = $_POST;
			}
			$this->show_login();
		}
	}
	function logout()
	{
		global $gl_message;

		unset($_SESSION['newsletter']);
		
		if (isset($_GET['reload'])) {
			print_javascript('if (top.window.location.href != window.location.href) top.window.location.href=top.window.location.href');
		}
		else{
			$this->show_login();
		}
		// anar a pagina inici
	}
	function send_password(){
		$new_password = get_password();
		$mail = $_POST['mail'];
		$sql = "UPDATE ".$this->table." SET password = '".$new_password."' WHERE mail = '" . $mail . "'";
		Debug::add('Consulta nou password', $sql);
		Db::Execute($sql);
		// si o hi ha l'adreça mostro missatge d'error
		if (mysqli_affected_rows(Db::cn())==0) {
			print_javascript('top.alert("' . stripslashes($this->caption['c_password_mail_error']) . '");');
		}
		// si l'adreça es bona envio mail amb la nova contrasenya
		else{
			
			print_javascript('
				if (typeof top.hidePopWin == "function") top.hidePopWin(false);
				top.alert("' . stripslashes($this->caption['c_password_sent']) . '");
				top.customer_show_login();');
			
			// preparo mail
			$page = $GLOBALS['gl_page'];
			$page->template = 'mail.tpl';
			$page->set_var('preview','');
			$page->set_var('remove_text','');
			$this->set_file('/newsletter/mail_password.tpl');
			$rs = $this->_get_customer_name($mail);
			$this->set_vars($rs);
			$this->set_var('password', $new_password);
			$this->set_vars($this->caption);

			$page->content = $this->process();
			$body = $page->get();
			$subject = $this->caption['c_password_mail_subject'];

			// envio mail
			$mailer = get_mailer($this->config);
			
			if (is_file(CLIENT_PATH . 'images/logo_newsletter.jpg'))
				$mailer->AddEmbeddedImage(CLIENT_PATH . 'images/logo_newsletter.jpg', 'logo', 'logo.jpg');
			elseif(is_file(CLIENT_PATH . 'images/logo_newsletter.gif')) 
				$mailer->AddEmbeddedImage(CLIENT_PATH . 'images/logo_newsletter.gif', 'logo', 'logo.gif');
			
			$mailer->Body = $body;
			$mailer->Subject = $subject;
			$mailer->IsHTML(true);
			$mailer->AddAddress ($mail);
			Debug::add('mailer', $mailer);
			send_mail($mailer, false);
		}
		Debug::p_all();
		die();
	}

	function _get_customer_name($mail){
		if ($this->table == 'product__customer'){
			$sql = "SELECT CONCAT(name,' ', surname) as name FROM product__customer WHERE mail = '" . $mail . "'";
		}
		else {
			$sql = "SELECT CONCAT(name,' ', surname1,' ', surname2) as name, treatment FROM ".$this->table." WHERE mail = '" . $mail . "'";		
		}
		return Db::get_row($sql);
		
	}

	/**
	 * NewsletterCustomer::is_logged()
	 * Saber si està loguejat
	 * @return
	 */
	function is_logged(){
		// miro si hi ha les 2 sessions per més seguretat de que esta logged
		if ($this->never_mind_logged) return true; // per poder cridar desde dins la clase alguna acció encara que no estigui logejat, com ara mail a admin  quan s'haregistrat nou usuari
		return (isset($_SESSION['newsletter']['customer_id']) && isset($_SESSION['newsletter']['mail']));
	}
	/**
	 * NewsletterCustomer::do_action()
	 * Sobreescric la funció heredada
	 * Qualsevol acció d'aquesta clase l'haig de cridar per aquí,
	 * ja que és on controlo que l'usuari està loguejat
	 * @param string $action
	 * @return
	 */
	function do_action($action=''){

		// si estic logejat es que ja he emplenat un formulari,
		// per tant no en puc crear cap més, nomès editar el meu
		if ($this->is_logged()){
			if ($this->action == 'show_form_new') $this->action = 'show_form_edit';
			if ($action == 'show_form_new') $action = 'show_form_edit';
			if ($this->action == 'get_form_new') $this->action = 'get_form_edit';
			if ($action == 'get_form_new') $action = 'get_form_edit';
		}


		$ac = $action?$action:$this->action;
		// es important no barrejar action amb this->action, per això faig servir una tercera variable $ac

		// aquests action no necessiten estar loguejats
		if (
				   $ac=='show_form_new'
				|| $ac=='get_form_new'
				|| $ac=='login'
				|| $ac=='get_login'
				|| $ac=='show_login'
				|| $ac=='show_message'
				|| $ac=='send_password'
				|| $ac=='get_show_block'
			) {
			return parent::do_action($action);
		}
		else
		{
			// miro si hi ha les 2 sessions per més seguretat de que esta logged
			if ($this->is_logged()) {
				// m'asseguro que només s'editin les dades del customer_id de la sessió
				$id = $_SESSION['newsletter']['customer_id'];
				$_GET['customer_id'] = $_GET['custumer_id'] = $id;
				if(isset($_POST['customer_id']) &&(
					!isset($_POST['customer_id'][$id]) ||
					$_POST['customer_id'][$id]!=$id)){
					Main::error_404('algu intenta entrar amb un form propi o passa algo raro'); // algu intenta entrar amb un form propi o passa algo raro
				}
				if ($ac == 'add_record') die(); // si està loggejat, no pot afegir un nou customer, un client nomès es pot fer el seu formulari i prou
				return parent::do_action($action);
			}
			elseif ($ac == 'add_record')
			{
				// add record no deixo fer si està logejat i si, si no ho està ( per això no està posat al grup de dalt )
				parent::do_action('add_record');
			}
			else
			{
				parent::do_action('show_login');
			}
		}
	}
	
	
	


	function newsletter_add_remove_key($table, $id) {	

			$table_id = strpos($table, "customer")!==false?'customer_id':'custumer_id';

			$q2 = "SELECT count(*) FROM ". $table . " WHERE BINARY remove_key = ";
			$q3 = "UPDATE ". $table . " SET remove_key='%s' WHERE  ". $table_id . " = '%s'";

			$updated = false; 
			while (!$updated){
				$pass = get_password(50);
				$q_exists = $q2 . "'".$pass."'";	

				// si troba regenero pass
				if(Db::get_first($q_exists)){
					$pass = get_password(50);							
				}
				// poso el valor al registre
				else{				
					$q_update = sprintf($q3, $pass, $id);

					Db::execute($q_update);
					break;
				}
			}
	}
}
?>