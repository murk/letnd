//onsubmit="return validar('document.theForm','Nom','Nom','','Lloc','Lloc','','DataInici','Data inici','di','DataFi','Data fi','df','Filtros','f')"
VALIDAR_CUSTOM_ERROR = new Object();
function validar(){
	
	$('.form_error').remove();
	var args=validar.arguments,
		numargs=args.length, 
		envia, missatge, 
		tipus, incorrecte='', 
		formu = args[0];
		
	for (i=1; i<(numargs-1); i+=3)
	{
		if (numargs==3)
		{
			obj=args[0];
			envia=false;
			missatge=args[1]
			tipus=args[2]
		}
		else
		{
			obj = formu.elements[args[i]];
			envia=true;
			missatge='"' + args[i+1] + '"'
			tipus=args[i+2];
		}
		if (obj){
		// obligatori
        if ((tipus.indexOf('1')!=-1) || (tipus=='int1') || (tipus=='email1') || (tipus=='checkbox1') || (tipus=='radios1') || (tipus=='rate1') || (tipus=='yesno1') || (tipus=='select1')){
				if (obj.value=="" ||
					(tipus=='checkbox1' && !$("[name='"+args[i]+"']").is(':checked')) ||
					(tipus=='radios1' && !$("[name='"+args[i]+"']").is(':checked')) ||
					(tipus=='rate1' && !$("[name='"+args[i]+"']").is(':checked')) ||
					(tipus=='yesno1' && !$("[name='"+args[i]+"']").is(':checked')) ||
					(tipus=='select1' && obj.selectedIndex == 0 && obj.options[obj.selectedIndex].value=="null")
				)
				{
					if (!incorrecte && tipus!='radios1' && tipus!='rate1' && tipus!='yesno1'){obj.focus()};
					if (typeof VALIDAR_CUSTOM_ERROR[args[i]]=='undefined'){
					incorrecte+= set_incorrecte(missatge, VALIDAR_TEXT1);
					}
					else{
					incorrecte+= set_incorrecte(VALIDAR_CUSTOM_ERROR[args[i]]['missatge'], VALIDAR_CUSTOM_ERROR[args[i]]['text']);					
					}


				}
				else
				{
					$("[name='"+args[i]+"']").removeClass('input_error');
					if (tipus.indexOf('1')!=-1){tipus=tipus.substr(0,tipus.length-1) };
				}
		}

        // nom, cognoms...

		if ((tipus=='names')  && (obj.value!='')){
		
			var regexp = /[\\\/$&^!@#%*~\?\[\]=\_\(\)]/;
			if(regexp.test(obj.value))
			{
				if (!incorrecte){obj.focus()};
				incorrecte+= set_incorrecte(missatge, VALIDAR_TEXT2);
			}
			else
			{
				$("input[name='"+args[i]+"']").removeClass('input_error');
			}
			//------------- si no te un mínim de 3 caracters
			if (obj.value.length < 2 )
			{
					if (!incorrecte){obj.focus()};
					incorrecte+= set_incorrecte(missatge, VALIDAR_TEXT25);
			}
			else
			{
				$("input[name='"+args[i]+"']").removeClass('input_error');
			}
		}

        // nom arxiu friendly urls

		if ((tipus=='filename')){
			//valido caràcters
			var checkOK = "abcdefghijklmnopqrstuvwxyz0123456789_-";
			var checkStr = obj.value;
			var allValid = true;
			for (x = 0; x < checkStr.length; x++) {
				ch = checkStr.charAt(x);
				for (j = 0; j < checkOK.length; j++){
					if (ch == checkOK.charAt(j)){
					break;
					}
					if (j == checkOK.length-1) {
					allValid = false;
					break;
					}
				}
			}
			if (allValid == false ){
				if (!incorrecte){obj.focus()};
				incorrecte+= set_incorrecte(missatge, VALIDAR_TEXT27);
			}
			else
			{
				$("input[name='"+args[i]+"']").removeClass('input_error');
			}
		}



		// mail
        if ((tipus=='email')  && (obj.value!='')){
		var caracters = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ.-@_+";
		var caractersInvalids='';
		var val=obj.value.trim();
		for (x=0;x<val.length;x++)
		{
			if (caracters.indexOf(val.charAt(x))==-1)
			{
			caractersInvalids = caractersInvalids + ' ' + val.charAt(x)
			}
		}
		arrob=val.indexOf('@')
		punt=val.indexOf('.',arrob+2)
		ultim=val.length-1
			if ((arrob==-1) || (punt==-1) || (arrob==0) || (punt==ultim) || (caractersInvalids))
			{
				if (caractersInvalids)
				{
					caractersInvalids = '. ' + VALIDAR_TEXT2 + ':' + caractersInvalids;
				}
				if (!incorrecte){obj.focus()};
				incorrecte+= set_incorrecte(missatge, VALIDAR_TEXT3+caractersInvalids);
			}
			else
			{	
				// si existeix comprovo la repeticio				
				arr = args[i].split('[');
				name2 = arr[1]?"input[name='"+arr[0] + "_repeat[" + arr[1]+"']":"input[name='"+arr[0] + "_repeat']";
				if ($(name2).length){
					val2 = arr[1]?$(name2).val():$(name2).val();
					if ((val!='')  || (val2!='')){
						if ((val!=val2) && (val!=''))
						{
							if (!incorrecte){obj.focus()};
							incorrecte+= set_incorrecte(missatge, VALIDAR_TEXT26);
						}
						else
						{
							$("input[name='"+args[i]+"']").removeClass('input_error');
						}
					}
				}
				else{
					$("input[name='"+args[i]+"']").removeClass('input_error');
				}
			}
		}

		// número
        if ((tipus=='int')  && (obj.value!='')){
			val = obj.value;
			while (val.indexOf('.')!=-1){val = val.replace('.','');}
			if ((val!=Number(val)) && (obj.value!=''))
			{
				if (!incorrecte){obj.focus()};
				incorrecte+= set_incorrecte(missatge, VALIDAR_TEXT5);
			}
			else
			{
				$("input[name='"+args[i]+"']").removeClass('input_error');
			}
			if (obj.value==''){obj.value='0'}
		}

		// decimal
        if ((tipus=='decimal')  && (obj.value!='')){
			val = obj.value;
			while (val.indexOf('.')!=-1){val = val.replace('.','');}
			val = val.replace(',','.');
			if ((val!=Number(val)) && (obj.value!=''))
			{
				if (!incorrecte){obj.focus()};
				incorrecte+= set_incorrecte(missatge, VALIDAR_TEXT24);
			}
			else
			{
				$("input[name='"+args[i]+"']").removeClass('input_error');
			}
			if (obj.value==''){obj.value='0'}
		}
		// tarjeta
        if ((tipus=='creditcard') && (obj.value!='')){
			if (!ValidarTarjeta(obj.value))
			{
				if (!incorrecte){obj.focus()};
				incorrecte+= set_incorrecte(missatge, VALIDAR_TEXT20);
			}
			else
			{
				$("input[name='"+args[i]+"']").removeClass('input_error');
			}
		}

		// tarjeta caducitat
        if((tipus=='creditdate') && (obj.value!='')){
			if (!comprobaCaducitat(obj.value))
			{
				if (!incorrecte){obj.focus()};
				incorrecte+= set_incorrecte(missatge, VALIDAR_TEXT21);
			}
			else
			{
				$("input[name='"+args[i]+"']").removeClass('input_error');
			}
		}

		// password
        if (tipus=='password'){
			val = obj.value;
			var arr = args[i].split('[');
			var $repeat = $(formu).find("input[name='"+arr[0] + "_repeat[" + arr[1]+"']");

	       if ($repeat.length) {
		       var val2 = $repeat.val();


		       if ((val != '') || (val2 != '')) {
			       if ((val != val2) && (obj.value != '')) {
				       if (!incorrecte) {
					       obj.focus()
				       }
				       ;
				       incorrecte += set_incorrecte(missatge, VALIDAR_TEXT25);
			       }
			       else {
				       $("input[name='" + args[i] + "']").removeClass('input_error');
			       }
		       }
	       }
		}

		// cp
		if ((tipus=='cp') && (obj.value!=''))
		{
			if ((obj.value!=Number(obj.value)) && (obj.value!='')  || (obj.value<10000))
			{
				if (!incorrecte){obj.focus()};
				incorrecte+= set_incorrecte(missatge, VALIDAR_TEXT6);
			}
			else
			{
				$("input[name='"+args[i]+"']").removeClass('input_error');
			}
		}

		// llargada maxima
        if(tipus>1){
			if (obj.value.length > tipus)
			{
				if (!incorrecte){obj.focus()};
				incorrecte+= set_incorrecte(missatge, VALIDAR_TEXT7 + obj.value.length + VALIDAR_TEXT8 + tipus);
			}
			else
			{
				$("input[name='"+args[i]+"']").removeClass('input_error');
			}
		}

		// data
		if (tipus=='di')
		{
			var obj2=eval(args[0]);
			var Any=obj2.ano.value;
			var Mes=obj2.mes.options[obj2.mes.selectedIndex].value;
			var Dia=obj2.dia.options[obj2.dia.selectedIndex].value;
			if (Dia!=0)
			{
				if ((Any == "") || (Any!=Number(Any)))
				{
					incorrecte+= set_incorrecte(missatge, VALIDAR_TEXT9);
				}
				else
				{
					if ((Any<1850) || (Any>2150))
					{
						incorrecte+= set_incorrecte(missatge, VALIDAR_TEXT10);
					}
				}
			    if ((Mes==2) && (Dia>28))
				{
					if (((Any/4)==parseInt(Any/4)) && (Dia<30))
					{
					}
					else
					{
						incorrecte+= set_incorrecte(missatge, VALIDAR_TEXT11);
					}
				}
				if ((Mes==4) || (Mes==6) || (Mes==9) || (Mes==11))
				{
					if (Dia>30)
					{
						incorrecte+= set_incorrecte(missatge, VALIDAR_TEXT11);
					}
				}
			}
		}

		// account
        if ((tipus=='account') && (obj.value!='')){
			if ((obj.value!=Number(obj.value)) && (obj.value!=''))
			{
				if (!incorrecte){obj.focus()};
				incorrecte+= set_incorrecte(missatge, VALIDAR_TEXT22);
			}
			else
			{
				$("input[name='"+args[i]+"']").removeClass('input_error');
			}
			if (obj.value==''){obj.value='0'}
		}



		if (tipus=='fe')
		{
			var obj2=eval(args[0]);
			var Any=obj2.ano.value;
			Any="20"+Any;
			var Mes=obj2.mes.options[obj2.mes.selectedIndex].value;
			var Dia=obj2.dia.options[obj2.dia.selectedIndex].value;
			if (Dia!=0)
			{
				if ((Any == "") || (Any!=Number(Any)))
				{
					incorrecte+= set_incorrecte(missatge, VALIDAR_TEXT9);
				}
				else
				{
					if ((Any<999) || (Any>9999))
					{
						incorrecte+= set_incorrecte(missatge, VALIDAR_TEXT10);
					}
				}
			    if ((Mes==2) && (Dia>28))
				{
					if (((Any/4)==parseInt(Any/4)) && (Dia<30))
					{
					}
					else
					{
						incorrecte+= set_incorrecte(missatge, VALIDAR_TEXT11);
					}
				}
				if ((Mes==4) || (Mes==6) || (Mes==9) || (Mes==11))
				{
					if (Dia>30)
					{
						incorrecte+= set_incorrecte(missatge, VALIDAR_TEXT11);
					}
				}
			}
		}
		// NIF

		// opcio -> contiene el valor:	 1 para NIF 	2 para NIE
		//opcio = document.form.nif_nie.options[document.form.nif_nie.selectedIndex].value;

		if ((tipus == 'nif') && (obj.value!=''))
		{
			var NIF=obj.value;
			var temp = valida_nif_cif_nie(NIF);
			if (temp > 0)
			{
				$("input[name='"+args[i]+"']").removeClass('input_error');;
			}
			else
			{
				incorrecte+= set_incorrecte(missatge, VALIDAR_TEXT23);
			}

		}

		// valor del select
		if(tipus == 'sel')
		{
				if (obj.selectedIndex==0)
				{
					if (!incorrecte){obj.focus()};
					incorrecte+= set_incorrecte(missatge, VALIDAR_TEXT16);
				}
				else
				{
					$("input[name='"+args[i]+"']").removeClass('input_error');
				}
			}

		// checkbox obligatori en un array
		if(tipus == 'check')
		{
		  objChecked = false;
		  formu = eval(args[0]);
		  for (x=0;x<formu.elements.length; x++)
			{
			 if ((formu.elements[x].type == 'checkbox') && (formu.elements[x].name.indexOf(obj)!=-1))
			 {
				if (formu.elements[x].checked == true) objChecked = true;
			 }
			}
		  if (!objChecked)
				{
					incorrecte+= set_incorrecte(missatge, VALIDAR_TEXT18, false);
				}
				else
				{

				}
		}

		// checkbox obligatori en un array
		if(tipus == 'checkmail')
		{
			objChecked = false;
			formu = eval(args[0]);
			for (x=0;x<formu.elements.length; x++)
			{
				if ((formu.elements[x].type == 'checkbox') && (formu.elements[x].name.indexOf(obj)!=-1))
				{
					if (formu.elements[x].checked == true) objChecked = true;
				}
			}
			if (!objChecked && formu.selected_addresses_ids.value=='')
			{
				incorrecte+= set_incorrecte(missatge, VALIDAR_TEXT19, false);
			}
			else
			{

			}
		}
	}// fi obj	
	}// fi for
	
	// validar condicions
	if ($('.accept-condicions',formu).length && !$('.accept-condicions',formu).is(':checked')) {
		incorrecte+= set_incorrecte('\n' + VALIDAR_TEXT28 , '', false);
	}
	
	if (incorrecte){
	    incorrecte= '\n' + VALIDAR_TEXT17 + '\n\n' + incorrecte;
		//if (showPopWin){
		//	window.setTimeout("hidePopWin(false);", 1000);
		//	while (incorrecte.indexOf('\n')!=-1){incorrecte = incorrecte.replace('\n','<br />');}
		//	showPopWin("", 300, 100, null, true, false, '<div class="modalErrors">' + incorrecte + '</div>');
			//$('#errors').html(incorrecte);
		//}
		//else{
			alert (incorrecte);
		//}
	}
	if (envia){
		// guardar amb frame
		if ((document.getElementById('save_frame'))
			&& (typeof showPopWin == 'function')
			&& (incorrecte=='' && (!formu.target) )
			){
				var do_next = true;
				formu.target = 'save_frame';
				if (typeof client != "undefined" && typeof client.on_validar == "function"){	
					do_next = client.on_validar(MODALBOX_SAVE,formu);
				}
				if (do_next){				
					showPopWin('', 300, 100, null, false, false, MODALBOX_SAVE +'<img src="/common/images/wait.png" style="margin-left:10px;vertical-align: middle;" width="32" height="32" />');
					if (document.getElementById('message')) document.getElementById('message').innerHTML = '';
				}
			}
		return (incorrecte=='');
	}


	function set_incorrecte(missatge, textemiss, changecolor){
		if (typeof(changecolor) == "undefined") changecolor=true;
		// utilitzo a radios per posar en una altra cela l'error 
		if ($("[class='"+args[i]+"_error']").length){
			$("[class='"+args[i]+"_error']").html('<span class="form_error">' + textemiss + '</span>');		
		}
		else
		{
			$("[name='"+args[i]+"']").last().after('<span class="form_error">' + textemiss + '</span>');
		}
		if (changecolor){
			$("[name='"+args[i]+"']").last().addClass('input_error');
		}
		return missatge + textemiss + '\n';
	}
}

// FUNCIÓN para validar el NIF
//Retorna: 1 = NIF ok, 2 = CIF ok, 3 = NIE ok, -1 = NIF error, -2 = CIF error, -3 = NIE error, 0 = ??? error
function valida_nif_cif_nie(a)
{
	var temp=a.toUpperCase();
	var cadenadni="TRWAGMYFPDXBNJZSQVHLCKE";

	if (temp!==''){
		//si no tiene un formato valido devuelve error
		if ((!/^[A-Z]{1}[0-9]{7}[A-Z0-9]{1}$/.test(temp) && !/^[T]{1}[A-Z0-9]{8}$/.test(temp)) && !/^[0-9]{8}[A-Z]{1}$/.test(temp))
		{
			return 0;
		}

		//comprobacion de NIFs estandar
		if (/^[0-9]{8}[A-Z]{1}$/.test(temp))
		{
			posicion = a.substring(8,0) % 23;
			letra = cadenadni.charAt(posicion);
			var letradni=temp.charAt(8);
			if (letra == letradni)
			{
			   	return 1;
			}
			else
			{
				return -1;
			}
		}

		//algoritmo para comprobacion de codigos tipo CIF
		suma = parseInt(a.charAt(2))+parseInt(a.charAt(4))+parseInt(a.charAt(6));
		for (i = 1; i < 8; i += 2)
		{
			temp1 = 2 * parseInt(a.charAt(i));
			temp1 += '';
			temp1 = temp1.substring(0,1);
			temp2 = 2 * parseInt(a.charAt(i));
			temp2 += '';
			temp2 = temp2.substring(1,2);
			if (temp2 == '')
			{
				temp2 = '0';
			}

			suma += (parseInt(temp1) + parseInt(temp2));
		}
		suma += '';
		n = 10 - parseInt(suma.substring(suma.length-1, suma.length));

		//comprobacion de NIFs especiales (se calculan como CIFs)
		if (/^[KLM]{1}/.test(temp))
		{
			if (a.charAt(8) == String.fromCharCode(64 + n))
			{
				return 1;
			}
			else
			{
				return -1;
			}
		}

		//comprobacion de CIFs
		if (/^[ABCDEFGHJNPQRSUVW]{1}/.test(temp))
		{
			temp = n + '';
			if (a.charAt(8) == String.fromCharCode(64 + n) || a.charAt(8) == parseInt(temp.substring(temp.length-1, temp.length)))
			{
				return 2;
			}
			else
			{
				return -2;
			}
		}

		//comprobacion de NIEs
		//T
		if (/^[T]{1}/.test(temp))
		{
			if (a.charAt(8) == /^[T]{1}[A-Z0-9]{8}$/.test(temp))
			{
				return 3;
			}
			else
			{
				return -3;
			}
		}

		//XYZ
		if (/^[XYZ]{1}/.test(temp))
		{
			pos = str_replace(['X', 'Y', 'Z'], ['0','1','2'], temp).substring(0, 8) % 23;
			if (a.charAt(8) == cadenadni.substring(pos, pos + 1))
			{
				return 3;
			}
			else
			{
				return -3;
			}
		}
	}

	return 0;
}

function str_replace(search, replace, subject) {
    // http://kevin.vanzonneveld.net
    // +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +   improved by: Gabriel Paderni
    // +   improved by: Philip Peterson
    // +   improved by: Simon Willison (http://simonwillison.net)
    // +    revised by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)
    // +   bugfixed by: Anton Ongson
    // +      input by: Onno Marsman
    // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +    tweaked by: Onno Marsman
    // *     example 1: str_replace(' ', '.', 'Kevin van Zonneveld');
    // *     returns 1: 'Kevin.van.Zonneveld'
    // *     example 2: str_replace(['{name}', 'l'], ['hello', 'm'], '{name}, lars');
    // *     returns 2: 'hemmo, mars'

    var f = search, r = replace, s = subject;
    var ra = r instanceof Array, sa = s instanceof Array, f = [].concat(f), r = [].concat(r), i = (s = [].concat(s)).length;

    while (j = 0, i--) {
        if (s[i]) {
            while (s[i] = s[i].split(f[j]).join(ra ? r[j] || "" : r[0]), ++j in f){};
        }
    };

    return sa ? s : s[0];
}



function ValidarTarjeta(numero_tarjeta) {
//var numero_tarjeta=numa+""+numb+""+numc+""+numd;

	// Comprobamos que solo hemos introducido numeros
	if (!isInt(numero_tarjeta)){
		return false;
	}

	// Paso 1: Tomamos las cifras en posiciones impares y las multiplicamos por 2 y
	// sumamos el resultado
	var cadena = numero_tarjeta.toString();
	var longitud = cadena.length;
	var cifra = null;
	var cifra_cad=null;
	var suma=0;
	for (var i=0; i < longitud; i+=2){
		cifra = parseInt(cadena.charAt(i))*2;
		// Si la cifra resultante es mayor que 9 sumamos las cifras
		if (cifra > 9){
			cifra_cad = cifra.toString();
			cifra = parseInt(cifra_cad.charAt(0))+parseInt(cifra_cad.charAt(1));
		}
		suma+=cifra;
	}
	// Paso 2: Tomamos las cifras en posiciones pares y las sumamos
	for (var i=1; i < longitud; i+=2){
		suma += parseInt(cadena.charAt(i));
	}

	// Paso 3: Comprobamos que el resultado es múltiplo de 10
	if ((suma % 10) == 0){
		return true;
	} else {
		return false;
	}
}
function comprobaCaducitat(dataExpiracio) {
	var ArrayExpiracio=dataExpiracio.split("/");
	if ((ArrayExpiracio.length!=2) || (ArrayExpiracio[0]=="") || (isNaN(ArrayExpiracio[0])) || (ArrayExpiracio[1]=="") || (isNaN(ArrayExpiracio[1])) || (ArrayExpiracio[0]<1) || (ArrayExpiracio[0]>12)) {return false}
	return true;
	}

function isInt(valor){
    var cadena = valor.toString();
	var longitud = cadena.length;
	if (longitud == 0){return false;}
	var ascii = null;
    for (var i=0; i<longitud; i++) {
		ascii = cadena.charCodeAt(i);
        if (ascii < 48 || ascii > 57){return false;}
    }
	return true;
}
function textarea_maxlength(obj, mlen)
{
	var val = obj.value;
	if (val.length > mlen) {
		obj.value = val.slice(0, mlen);
	}
	var id = obj.id;
	if ($('#' + id + '_counter').length) $('#' + id + '_counter').text(obj.value.length);
}