
<script type="text/javascript">
ref = '<?=$c_ref?> <?=$ref?>';
</script><br /><br />
  <table width="90%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td align="left" valign="top"><div align="justify" class="details">
          <?if ($property):?>
        <b class="titolFitxaProp">
          <?=$property?>
        </b><br />
          <?endif //property?>
        <?if ($property_title):?>
        <span class="titolFitxaProp2">
          <?=$property_title?>
          <br />
          <br />
        </span>
          <?endif //property_title?> 
		  <?if ($description):?>
          <table border="0" cellspacing="2" cellpadding="0">
                <tr>
                  <td class="details"><?=$description?></td>
                </tr>
                <tr>
                  <td><img src="/public/themes/plantilla2/images/spacer.gif" width="10" height="3" /></td>
                </tr>
        </table>
        <?endif //description?>
              <table border="0" cellspacing="0" cellpadding="2">
        <?if ($floor_space):?>
                <tr>
                  <td valign="top"><strong class="detailsdes"><?=$c_floor_space?>:</strong></td>
                  <td class="details"><?=$floor_space?> m2</td>
                </tr>
				<?endif //floor_space?>
              <?if ($land):?> 
                <tr>
                  <td valign="top"><strong class="detailsdes"><?=$c_land?>:</strong></td>
                  <td class="details"><?=$land?> <?=$land_units?></td>
                </tr>
              <?endif //land?> 
				<?if ($category_id):?>
                <tr>
                  <td valign="top"><strong class="detailsdes"><?=$c_category_id?>:</strong></td>
                  <td class="details"><?=$category_id?></td>
                </tr>
              <?endif //category_id?>
			  <?if ($comarca_id):?>
                <tr>
                  <td valign="top"><strong class="detailsdes"><?=$c_zone?>:</strong>&nbsp;</td>
                  <td class="details"><?=$comarca_id?><br>
                    <?=$provincia_id?> (<?=$country_id?>)</td>
                </tr>
              <?endif //comarca_id?>
        </table>
          <?if ($price):?>
          <br />
        <span class="price">&nbsp;<strong><?=$c_price?>: </strong>
            <?=$price?>
          <?=CURRENCY_NAME?>&nbsp;</span>
        <?endif //price?>
          <?if ($price_consult):?>
          <br />
        <span class="price">&nbsp;<strong> <?=$price_consult?></strong>
          &nbsp;</span>
        <?endif //price_consult?>
      </div></td>
    </tr>
    <tr>
      <td><img src="/public/themes/plantilla2/images/spacer.gif" width="10" height="10" /></td>
    </tr>
  </table>
  <?if ($details):?>
        <table width="90%" border="0" cellpadding="0" cellspacing="0" class="tabledetails">
          <tr>
            <td width="1" ><img src="/public/themes/plantilla2/images/spacer.gif" width="1" height="1" /></td>
            <td><table width="100%" cellpadding="4" cellspacing="0">
              <?$con = 0?><?foreach($details as $d):?><?if ($$d):?>
               <?if ($con==0):?><tr><?endif //$conn?>
                  <td align="left" nowrap="nowrap" class="details"><strong><?=${'c_'.$d}?>:</strong></td>
                 <td width="50%" align="left" class="details"><?=$$d?></td>
               <?if ($con==0):$con=1?><td  width="1"><img src="/public/themes/plantilla2/images/spacer.gif" width="1" height="1" /></td><?else:$con=0?></tr><?endif //$con?><?endif //$$d?><?endforeach //details?>
            </table></td>
            <td width="1" ><img src="/public/themes/plantilla2/images/spacer.gif" width="1" height="1" /></td>
          </tr>
        </table>
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td width="2"></td>
            <td><img src="/public/themes/plantilla2/images/spacer.gif" width="1" height="2" /></td>
            <td width="2"></td>
          </tr>
        </table></td>
    </tr>
  </table>
  <?endif //$details?>
</div>