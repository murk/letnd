
<script language="JavaScript" src="/admin/jscripts/validacio.js?v=<?=$version?>"></script>
<form name="theForm" method="post" action="<?=$link_action?>" onsubmit="<?=$js_string?>" encType="multipart/form-data" target="save_frame">
	<input name="product_id[<?=$product_id?>]" type="hidden" value="<?=$product_id?>" />
	<input name="product_id_javascript" type="hidden" value="<?=$product_id?>" />
	<div class="form">
		<div id="form_buttons_holder">
			<table id="form_buttons">
				<tr>
					<td valign="bottom">
						<div class="menu">
							<a href="<?= $data_link ?>"><?= $c_edit_data ?></a>
							<a href="<?= $image_link ?>"><?= $c_edit_images ?></a>
							<? foreach ( $form_tabs as $r ): extract( $r ) ?>
								<? if ( $tab_action ): ?>
									<a href="?action=<?= $tab_action ?>&<?= $id_field ?>=<?= $id ?>&menu_id=<?= $menu_id ?>"><?= $tab_caption ?></a>
								<? else: // ?>
									<div><?= $tab_caption ?></div>
								<? endif // $tab_action ?>
							<? endforeach //$form_tabs?>
						</div>
					</td>
					<td class="buttons_top">
						<div class="right">
										<? if($show_previous_link): ?><a class="boto2 btn_show_previous_link"
										                          href="<?= $show_previous_link ?>"></a><? endif //show_previous_link?>
										<? if($show_next_link): ?><a class="boto2 btn_show_next_link"
										                          href="<?= $show_next_link ?>"></a><? endif //show_next_link?>
										<? if($back_button): ?><a class="btn_form_back"
										                          href="<?= $back_button ?>"></a><? endif //back_button?><input type="submit" name="Submit" value="<?=$c_send_edit?>" class="btn_form1" /></div>
					</td>
				</tr>
			</table>
		</div>
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td>
								<table width="100%" cellpadding="4" cellspacing="0">
									<tr>
										<td class="formCaptionTitle"><strong>
												<?= $c_data_related ?>&nbsp;&nbsp;</strong><?= $product_form_title ?></td>
									</tr>
								</table>
								<table width="100%" cellpadding="4" cellspacing="0" class="formstbl">
									<tr>
										<td align="center" valign="top" class="forms">
											<table width="99%" border="0" cellspacing="0" cellpadding="0">
												<tr>
													<td align="left">
														<? /* <div class="listTitle"><strong>
 							                              <?=$c_product?>&nbsp;&nbsp;</strong>
 															<?= $product_title ?> <?= $product_subtitle ?> <?= $ref ?>
 							                              </div> */ ?>
														<div class="listTitle padding-top-3 padding-bottom-1"><?= $c_select_related_products ?></div>

														<?= $product_relateds ?>
														
													</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<table width="100%" cellpadding="4" cellspacing="2" class="formsButtonstbl">
			<tr>
				<td align="center" class="formsButtons">
					<div class="centerFloat1">
						<div class="centerFloat2">
							<input type="submit" name="Submit" value="<?=$c_send_edit?>" class="btn_form1">
						</div>
					</div>
				</td>
			</tr>
		</table>
	</div></form>