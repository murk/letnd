<script type="text/javascript" src="//maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript" src="/admin/jscripts/map.js?v=<?=$version?>"></script>
<script type="text/javascript">
var gl_is_default_point = <?=$is_default_point;?>;
var gl_latitude = <?=$latitude;?>;
var gl_longitude = <?=$longitude;?>;
var gl_zoom = <?=$zoom;?>;
var gl_menu_id =<?=$menu_id;?>;
var gl_tool ='<?=$tool;?>';
var gl_tool_section ='<?=$tool_section;?>';
var gl_id_field = '<?=$id_field;?>';
var gl_id =<?=$id;?>;
MAP_FRAME_TEXT_1 = '<?=addslashes($c_map_frame_1)?>';
MAP_FRAME_TEXT_2 = '<?=addslashes($c_map_frame_2)?>';
MAP_FRAME_TEXT_3 = '<?=addslashes($c_map_frame_3)?>';
MAP_FRAME_TEXT_4 = '<?=addslashes($c_map_frame_4)?>';
</script>
<div  class="form">
<form name="theForm" method="post" action="" onsubmit="">
  <div id="form_buttons_holder"><table id ="form_buttons">
  <tr>
    <td valign="bottom"><div class="menu">
      <a href="?action=show_form_edit&amp;menu_id=<?=$menu_id?>&amp;museu_id=<?=$museu_id?>"><?=$c_edit_data?></a>
      <a href="?action=list_records_images&amp;menu_id=<?=$menu_id?>&amp;museu_id=<?=$museu_id?>">
        <?=$c_edit_images?>
        </a>
      <div>
        <?=$c_edit_map?>
      </div></div></td>
    <td class="buttons_top">
            <div class="right" style="margin-bottom:3px;"><a href="javascript:save_map()" class="btn_form_save">
    <?=$c_save_map?>
    </a></div></td>
  </tr>  
</table></div></form>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td>
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td>
                <table width="100%" cellpadding="4" cellspacing="0">
                  <tr>
                    <td class="formCaptionTitle"><strong>
                      <?=$c_form_map_title?>:</strong>&nbsp;&nbsp <?=$museu_title?> 
                    </td>
                  </tr>
              </table>
              <table width="100%" cellpadding="4" cellspacing="0" class="formstbl">
               
                  <tr>
                    <td align="center" valign="top" class="forms"><table width="99%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td id="custumers"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td align="center">
							<form onsubmit="return search_point()" class="google_map">
								<table cellpadding="0" cellspacing="0" border="0"><tr>
									<td><strong class="formsCaption">
									<?=$c_search_adress?>: </strong><input id="map_address" name="map_address" type="text" value="<? if ($adress):?><?=$adress?> , <? endif //$adress?><?=$municipi?>" size="90" class="search" /></td>
									<td><input type="submit" value="ok" class="boto-search" /></td>
								</tr></table>
							</form>
							<div><span class="formsCaption"><?=$c_latitude?>:&nbsp;</span><span id="latitut"><?=$latitude?></span>&nbsp;&nbsp;&nbsp;<span class="formsCaption"><?=$c_longitude?>:&nbsp;</span><span id="longitut"><?=$longitude?></span></div>
							<div id="google_map"></div>
						</td>
						</tr>
                      </table></td>
                      </tr>
                    </table>
                     </td>
                  </tr>
              </table></td>
          </tr>
        </table></td>
    </tr>
</table>
<table width="100%" cellpadding="4" cellspacing="2" class="formsButtonstbl">
  <tr>
    <td align="center" class="formsButtons"><div class="centerFloat1">
                        <div class="centerFloat2"><a href="javascript:save_map()" class="btn_form_save">
    <?=$c_save_map?>
    </a></div></div></td>
  </tr>
</table>
</div>