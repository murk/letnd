<?php
function get_vars_product_product($content_ids, &$ids, &$names)
{
    $query = "SELECT product__product.product_id AS product_id, ref, product_title
				FROM product__product, product__product_language
				WHERE product__product.product_id IN (" . implode(',', $content_ids) . ")
				AND product__product.product_id = product__product_language.product_id
				AND language = '".LANGUAGE."'
				ORDER BY ref DESC" ;
    $results = Db::get_rows($query);
    foreach($results as $rs)
    {
        $nam = $rs['ref'] . ' - ' . $rs['product_title'];
        $names[] = htmlspecialchars(str_replace(',', ' ', $nam), ENT_QUOTES);
        $ids[] = $rs['product_id'];
    }
}
function get_content_extra_product_product($ids, $action, $language)
{
	if ($action == 'show_record')
    {
	    $ids = explode(',', $ids);
	    $content = '';
        foreach ($ids as $id)
        {
            $content .= get_url_html(HOST_URL . '/?template=&tool=product&tool_section=product&action=show_record&loginnewsletter=letndmarc&passwordnewsletter=5356aopfqkiios5cnoqws90se&product_id='.$id.'&language=' . $language); 
        }
    }
	else if ($action == 'list_records') {

        $content = get_url_html(HOST_URL . '?template=&tool=product&tool_section=product&action=list_records&loginnewsletter=letndmarc&passwordnewsletter=5356aopfqkiios5cnoqws90se&product_ids='.$ids.'&language=' . $language);
	}
    return $content;
}

?>