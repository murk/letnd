<?php
$gl_caption ['c_send_mail']='Send e-mail';
$gl_messages_cart ['no_records']=$gl_caption_cart ['c_no_records']=$gl_caption_cart ['c_empty_cart']='Shopping cart is empty';
$gl_messages_order ['no_records'] = 'You did not place any order yet';

$gl_caption['c_productes_destacats']='Featured Products';
$gl_caption['c_products']='Products';
$gl_caption['c_product']='Product';
$gl_caption['c_files']='Files';
$gl_caption['c_discount'] = 'Discount';

// NO TRADUIT

$gl_caption ['c_next_payment_step']='Next';
$gl_caption ['c_continue_shopping']='Continue shopping';
$gl_caption ['c_text_register']='We will proceed to the creation of your account';
$gl_caption ['c_register']='Register';
$gl_caption_customer ['c_form_title']=$gl_caption ['c_summary_title']='Personal information';
$gl_caption_customer ['c_title_register']='Register';
$gl_caption_customer ['c_title_login']='ID';
$gl_caption_customer ['c_is_customer']='Already a customer';
$gl_caption_customer ['c_new_customer']='New customer';
$gl_caption_customer ['c_mail_log']='Insert your e-mail';
$gl_caption_customer ['c_password_log']='Insert your password';
$gl_caption_customer['c_customer_menu'] = 'Mi account';
$gl_caption_customer['c_custumer_menu_personal_data'] = 'My personal information';
$gl_caption_customer['c_custumer_menu_orders'] = 'My orders';
$gl_caption['c_custumer_menu_logout'] = 'Log Out';

$gl_caption['c_title_cart'] = 'Shopping cart';
$gl_caption['c_my_cart'] = 'My cart';
$gl_caption['c_cart'] = 'Cart';
$gl_caption['c_add_to_cart'] = 'Add to cart';
$gl_caption['c_see_cart'] = 'See cart';
$gl_caption['c_buy'] = 'Buy';

$gl_caption_orderitem['c_title_cart'] = 'Purchases';
$gl_caption_order['c_list_title'] = 'Orders';

$gl_caption_payment['c_payment_title'] = 'Making the purchase';
$gl_caption_payment['c_payment_login'] = 'Identification';
$gl_caption_payment['c_payment_summary'] = 'Summary';
$gl_caption_payment['c_payment_payment'] = 'Payment';
$gl_caption_payment['c_payment_end'] = 'Payment ended';
$gl_caption_payment ['c_cart_is_empty']='You can not make any orders, the cart is empty';

$gl_caption_payment['c_method_title1']="Choose your payment method.";
$gl_caption_payment['c_method_title2']="The amount is <strong>%s&nbsp;%s</strong> vat included.";
$gl_caption_payment['c_method_paypal']="PayPal payment";
$gl_caption_payment['c_method_account']="Bank transfer payment";
$gl_caption_payment['c_method_ondelivery']="On delivery payment";
$gl_caption_payment['c_method_directdebit']="Direct Debit";
$gl_caption_payment['c_method_4b']="Credit card (4b)";
$gl_caption_payment['c_method_lacaixa']="Credit card";

$gl_caption_payment['c_show_paypal']="Paypal payment it's by paying, if the process doesn't begin automatically please clik here:";
$gl_caption_payment['c_show_4b']="Sistema 4b payment it's by paying, if the process doesn't begin automatically please clik here:";
$gl_caption_payment['c_show_lacaixa']="Card payment it's by paying, if the process doesn't begin automatically please clik here:";
$gl_caption_payment['c_show_account']="Make a bank transfer at this account: ";
$gl_caption_payment['c_show_account_mail']="Please, make a bank transfer at this account: %s, indicating your order number %s";
$gl_caption_payment['c_show_ondelivery']="";
$gl_caption_payment['c_show_directdebit']="";
$gl_caption_payment['c_show_end']="";
$gl_caption_payment['c_mail_order_saved']="<strong>A new order has been created in your online shop.</strong><br><br>In case the order is confirmed, you will receive a new notification";

$gl_caption_payment['c_mail_order_subject_seller']='Pre-order nº %s / Client nº %s';
$gl_caption_payment['c_mail_subject_seller']='Order summary # %s / Customer nº %s';
$gl_caption_payment['c_mail_subject_buyer']='Order summary from %2$s # %1$s';
$gl_caption_payment['c_text_buyer']='%s %s,';
$gl_caption_payment['c_text_buyer2']= 'Thank you very much for buying at %s,  your order number is: <strong>%s</strong> and your customer number is: <strong>%s</strong><br><br>Remember that you can verify your order status at: <a href="%s">My account</a>' ;
$gl_caption_payment['c_text_buyer_no_session']= 'Thank you very much for you purchase in %s' ;
$gl_caption_payment['c_text_buyer_no_order']= 'Has not made any purchase, if you need help please contact with us' ;
$gl_caption_payment['c_text_seller']='The customer: %s, has made a purchase in %s.';
$gl_caption_payment['c_text_seller_pay_now']='The customer: %s, has made the pending payment in %s.';

$gl_caption_payment ['c_paypal_pending_text_buyer']='The order remains outstanding pending the confirmation of Paypal payment';
$gl_caption_payment ['c_paypal_pending_text_seller']='The order remains outstanding pending the confirmation of Paypal payment';
$gl_caption_payment ['c_paypal_completed_text_buyer']='Paypal payment confirmed';
$gl_caption_payment ['c_paypal_completed_text_seller']='Paypal payment confirmed';

$gl_caption_payment ['c_4b_pending_text_buyer']='';
$gl_caption_payment ['c_4b_pending_text_seller']='';
$gl_caption_payment ['c_4b_completed_text_buyer']='Credit card payment confirmed';
$gl_caption_payment ['c_4b_completed_text_seller']='Credit card payment confirmed';

$gl_caption_payment ['c_lacaixa_pending_text_buyer']='';
$gl_caption_payment ['c_lacaixa_pending_text_seller']='';
$gl_caption_payment ['c_lacaixa_completed_text_buyer']='Credit card payment confirmed';
$gl_caption_payment ['c_lacaixa_completed_text_seller']='Credit card payment confirmed';

$gl_caption_payment ['c_account_text_buyer']='Below you will find our bank details for the transfer';
$gl_caption_payment ['c_account_text_seller']='The order remains outstanding pending the bank transfer';
$gl_caption_payment ['c_ondelivery_text_buyer']='';
$gl_caption_payment ['c_ondelivery_text_seller']='';
$gl_caption_payment ['c_paypal_text_buyer']='';
$gl_caption_payment ['c_paypal_text_seller']='';
$gl_caption_payment ['c_directdebit_text_buyer']='The order will be charged at your account number';
$gl_caption_payment ['c_directdebit_text_seller']='';

$gl_caption['c_account_title']= 'Bank trasfer details' ;
$gl_caption['c_account_amount']= 'Amount' ;
$gl_caption['c_account_concept']= 'Concept' ;

$gl_messages_cart ['no_records']='Shopping cart is empty';


// traduir al català
$gl_caption['c_password_forgotten'] = '¿have you forgotten the password?';
$gl_caption['c_password_insert_mail'] = 'Please enter your email address and send you a new password to your email account.';
$gl_caption['c_password_mail_error'] = 'The email address provided does not exist in our database, please write the address correctly';
$gl_caption['c_password_sent'] = 'It has sent a new password to your email address';
$gl_caption['c_password_mail_subject'] = 'Your new password';
$gl_caption['c_password_mail_text_1'] = 'Your new password is';
$gl_caption['c_password_mail_text_2'] = 'You can change your password throught "My account" from our website';
$gl_messages_customer['registered_succesful']="The registration has been succesful.<br><br>In short we'll notify you the activation of your account";
$gl_caption_customer['c_subject'] = 'New customer registered';
$gl_caption_customer['c_body_1'] = 'A new customer has been registered at the on-line shop';
$gl_caption_customer['c_body_2'] = 'The customer is deactivated, you can activate him at the %scustomer\'s management area%s';

$gl_caption['c_product_list_view_all'] = 'View all';

$gl_caption_cart['c_promcode_err_not_valid'] = 'The promotional code is not valid';
$gl_caption_cart['c_promcode_err_minimum'] = 'To apply the discount the amount must be bigger than %s';
$gl_caption_cart['c_promcode_err_expired'] = 'The promotional code has expired';
$gl_caption_cart['c_promcode_err_used'] = 'This code has already been used once';
$gl_caption_cart['c_promcode_button'] = 'Apply';

$gl_messages_cart['c_no_stock_message'] = 'This product is out of stock';
$gl_messages_cart['c_max_stock_reached_message'] = ' The maximum amount available for such product has been reached.';

$gl_caption['c_last_units'] = 'Last units';
$gl_caption_product['c_price_consult'] = 'Price on application';

$gl_caption['c_customer_comment'] = 'Comments';
?>