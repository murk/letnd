<?
/*

CRON - cada hora
0 * * * * php -f "/var/www/letnd.com/datos/motor/admin/modules/admintotal/admintotal_mls_cli.php" > /var/www/letnd.com/datos/motor/data/logs/mls.htm

SERVIDOR
php -f "/var/www/letnd.com/datos/motor/admin/modules/admintotal/admintotal_mls_cli.php" > /var/www/letnd.com/datos/motor/data/logs/mls.htm

VAGRANT
php -f "/letnd/admin/modules/admintotal/admintotal_mls_cli.php" > /letnd/data/logs/mls_linux.htm

DOS
C:\server\php5\php.exe -f "D:\Letnd\admin\modules\admintotal\admintotal_mls_cli.php" > d:\letnd\data\logs\mls_windows.htm

DEBUG CLI EX
php -f "/letnd/admin/modules/admintotal/admintotal_mls_cli.php" debug > /letnd/data/logs/mls_linux.htm
*/

$dir = str_replace ( '/admin/modules/admintotal','/common/', __DIR__ );
$dir = str_replace ( '\admin\modules\admintotal','\\common\\', $dir );

include ($dir . 'cli_init.php');


include (DOCUMENT_ROOT . 'admin/modules/admintotal/admintotal_mls.php');


$mls = New AdmintotalMls;
$mls->is_cli = true;
$mls->_start();
