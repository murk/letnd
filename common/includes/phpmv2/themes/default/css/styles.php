<?php
header('Content-type: text/css');
$theme_path = "";
$direction = $_GET['dir'];

if ($direction == "rtl")
{
	$rightouleft = "right";
	$leftouright = "left";
}
else
{
	$rightouleft = "left";
	$leftouright = "right";
}

print "
/* styles par defaut pour tous les elements */
* {
font-family: verdana, tahoma, Trebuchet MS,arial, sans-serif;
}

/* Fons de la p�gina */
BODY {
background: #FFFFFF;
margin: 0;
}
/* titols principals */
H1 {
direction: $direction;
font-size: 14px;
text-align: center;
color: #000000;
}
/* Titols grans vermells */
H2 {
direction: $direction;
font-size: 12px;
text-align: center;
line-height:1.5em;
color: #ff3333;
}
/* Titols blaus ex. grafico de resumen estad�stico de largo plazo */
H3 {
direction: $direction;
text-align: $rightouleft;
padding-left:20px;
font-size: 13px;
background-position: 0%;
color: #7b6b39;
margin-top: 2em;
margin-left: 2em;
margin-bottom: 2em;
}
/* green big messages */
H4 {
direction: $direction;
font-size: large;
text-align: center;
line-height:1.5em;
color: #1bbd1b;
}
/* small titles */
H5 {
direction: $direction;
text-align: $rightouleft;
font-size: 95%;
color: #000000;
margin-top: 1.5em;
margin-left: 2em;
margin-bottom: 0.3em;
}
/* Quadre central */
#main {
text-align: center;
background: #ffffff;
}

/* cadre dans lequel se trouve les donnees */

#contenu {
font-size: 90%;
line-height: 1.4em;
width: 100%;
border: 0px solid #00008B;
text-align: $rightouleft;
margin: auto;
padding: 0.5em;
}
#login, #loginTable{ 
margin:auto;
font-size: 90%;
}
#login{
margin-top: 100px;
margin-bottom: 100px;
}
#interest{
margin-top:10px;
margin-left:70px;
font-size:100%;
border-bottom: 2px dotted #F0F0F0;
color:#696a6c;
}
#interestcontent {
margin-top:20px;
}
A.help_pagename {

	margin-left:70px;
	font-size:80%;
	color:#B9BBBE;
	font-decoration:italic;
	border-bottom: 1px dotted #E3E3E3;
}
#includepopulationtop, A.includepopulation  {
	margin-left:470px;
	margin-top:30px;
	font-size:100%;
	color:#696a6c;
}
#includepopulation, A.includepopulation {
	border-bottom: 2px dotted #F0F0F0;
}

/* menu haut */
#menu 
}
#menu LI {
display: inline;
margin: 0;
padding: 0;
margin-top:8px;
float: left;
text-align: center;
voice-family: \"\\\"}\\\"\";
voice-family:inherit;
width: 108px;
}
html>body #menu LI {
width: 122px;
}
#menu LI UL { /* listes de deuxieme niveau */
padding: 0;
position: absolute;
background: #ffffff;
border: 1px solid #00008B;
left: -999em; /* on met left plutot que display pour cacher les menus parce que display: none n'est pas lu par les lecteurs d'ecran */
}
#menu LI UL LI { /* sous-listes */
float: none;
width: auto;
}
#menu LI UL LI A { /* sous-listes */
border-width: 0;
border-top: 1px solid #ffffff;
border-bottom: 1px solid #ffffff;
font-variant: normal;
font-weight: normal;
text-align: left;
}
#menu LI UL LI A:hover { /* sous-listes */
border-top: 1px solid #00008B;
border-bottom: 1px solid #00008B;
}
#menu LI:hover UL { /* listes imbriquees sous les items de listes survoles */
left: auto;
}
#menu LI.sfhover UL { /* listes imbriquees sous les items de listes survoles */
left: auto;
margin-left: -54px;
}
#menu A {
}
#menu A:hover {
}


/* Comportament dels links per defecte */
A {
color: #2f2f90; 
text-decoration: none;
}

/* TOP Link */
A.movetop {
display: block;
margin-top: 2em;
}

/* Comportement par defaut de la balise <SMALL> */
SMALL {
font-size:0.8em;
}
/* Comportement par defaut de la balise <FORM> */
FORM {
margin: 0;
padding: 0;
}
/* Comportement par defaut des listes deroulantes */
SELECT {
font-size: 12px;
direction: $direction;
}
SELECT > OPTION {
background-color: #F8FBFF;
}
/* DIV generique permettant de remettre les position a zero (notament apres des float) */
DIV.both {
clear: both;
}
IMG.generic {
display: block;
margin-left: auto;
margin-right: auto;
 }
/* DIV generique permettant de faire un centrage de contenu */
DIV.centrer {
text-align: center;
}

/* Paragraphe generique avec texte centre */
P.centrer {
text-align: center;
}
/* Affichage de code (javascript par ex) */
CODE {
direction: $direction;
text-align: left;
display: block;
font-size: 90%;
border-style: dashed dashed dashed solid;
border-color: rgb(0, 0, 139);
border-width: 1px 1px 1px 5px;
margin: 2px;
margin-bottom: 20px;
padding: 4px;
background-color: rgb(240, 247, 255);
}
CODE STRONG {
font-size: 100%;
font-weight: bold;
}
/* style par defaut de toutes les images */
IMG {
border: 0;
}
/* champs de formulaire */
input, textarea {
background: #FBFBFF;
border: #b3b3b3 1px solid;
color: #0c183a;
}

/* DIV contenant le logo + le choix des sites + le calendrier */
#logo {
width: 100%;
}
/* Choix des sites */
#sites {
bottom:0;
float: right;
padding-right: 15px;
height: 130px;
}
#sites SELECT {
border: #000000 1px solid;
margin: auto;
margin-top: 110px;
}
#sites P {
margin: 0;
}
#sites OPTGROUP {
padding-left: 3px;
}
/* Calendrier */
#calendrier {
float: left;
text-align: $rightouleft;
}
#calendrier P {
margin: 0;
}

#calendrier TABLE { /* rcuadre general del calendari */
direction: $direction;
border: 1px solid #a4a1a1;
border-bottom: 1px solid #a4a1a1;
border-left: 1px solid #a4a1a1;
border-collapse: collapse;
line-height: 100%;
margin-top:6px;
}
#calendrier A.selection {
color: red;
}

#calendrier TH {
border-top: 1px solid rgb(80, 100, 133);
border-right: 1px solid rgb(80, 100, 133);
color: #ffffff;
text-align: center;
background-color: #769b28; /* Color fons dies semana calendari */
width: 12px;
padding: 0.1em;
margin: 0;
font-size: x-small;
}
#calendrier TD {
border-top: 1px solid #7b7777;
border-right: 1px solid #7b7777;
text-align: center;
color: #c0c0c0; /* Color calendaris dies que encara no han passat */
padding: 0.2em;
margin: 0;
font-size: x-small;
}
#calendrier A.selection { /* dia seleccionat delcalendari */
color: red;
}
#calendrier SELECT {
border: #00ff00 1px solid;
}
/*---------------------------------- Boto llengua ----------------*/ 
/* Choix de la langue */
#choixlangue {
float: $leftouright;
margin: auto;
text-align: center;
}
#choixlangue FIELDSET { 
width: 130px;
padding: 5px;
}
#choixlangue LEGEND {
color: rgb(0, 0, 139);
font-weight: bold;
font-size:90%;
}
#choixlangue P {
margin: 0;
}

/*---------------------------------------------------------------*/
/* styles pour les infos techniques */
P.gristrans {
color: #cdcaca;
}
P.archivefait {
font-size: 10pt;
color: #ffffff;
}
P.gristrans A {
color: #cdcaca;
}
P.gristrans EM A{
font-style: italic;
}
P.archive {
font-size: 70%;
color: #bcbcbc;
margin: 0;
text-align: center;
}
P.archive EM {
font-size: 100%;
font-style: italic;
}
P.gristrans STRONG {
font-weight: bold;
}
P.archive STRONG {
font-weight: bold;
}
SPAN.oktrans {
color: #9ec2a7;
}
SPAN.errortrans {
color: red;
}
/*----------------------------------------------------------------------------*/
/* Information de la periode visualisee */
P.periodevisu {
margin: 0;
font-size:13px;
}
/* Separacions dia, semana, mes, any */
P.choixperiode { 
margin: 0;
margin-right: 2em;
color: #818181;
}
/* Color dia, semana, mes, any
P.choixperiode A {
font-style: italic;
color: #00ff00;
font-size:11px;
}

/*--------------------------------------------------------------------*/
#error{
color:red;
font-size:100%;
font-weight:bold;
}
TABLE TD.error {
font-size:150%;
color:red;
}
/*------------------------------------------------------------------*/
/* Tous les tableaux de resultats */
TABLE.resultats, TABLE.interest  {
direction: $direction;
margin: auto;
empty-cells: hide;
font-size: 90%;
border:0
}
TABLE.interest {
width:75%;
empty-cells: hide;
}
/*  tota les taules amb resultats d'alguna cosa. Primera fila. Ex. Per al per�ode estudiat */
TABLE.resultats TH, TABLE.interest TH{
font-size:90%;
border-top: 1px solid #d4d0c8;
border-right: 1px solid #d4d0c8;
border-left: 1px solid #d4d0c8;
text-align: center;
background-color: #e8e7d9;
color: #000000;
font-size:12px;
empty-cells: hide;
}
/* Informaci� de dintre les taurles */
TABLE.resultats TD, TABLE.interest TD {
font-size:10px;
border: 1px solid #d4d0c8;
text-align: center;
color: #7b6b39;
padding-top:3px;
padding-bottom:3px;
}

TABLE.resultats TD.damier, TABLE.interest TD.damier  {
background-color: #f0f0ed;
}

TABLE.resultats TD.damierlight  {
background-color: #f6f6f6;
}
/* taules amb resultats col(2) */
TABLE.resultats TD.damieralign, TABLE.interest TD.damieralign {
background-color: #f0f0ed;
text-align: $rightouleft;
padding:6;
}
TABLE.resultats TD.align, TABLE.interest TD.align {
text-align: $rightouleft;
padding:6;
}
/* celdes buides */
TABLE.resultats TD.vide, TABLE.resultats TH.vide, TABLE.interest TD.vide, TABLE.interest TH.vide {
border-width: 0;
background-color: #ffffff;
}
TABLE.resultats TD.sansbordure, TABLE.interest TD.sansbordure {
border-width: 0;
text-align: $rightouleft;
}
/* color resultats de dins les taules */
TABLE.resultats TD STRONG, TABLE.interest TD STRONG {
font-weight: bold;
}
/*-------------------------------------------------------------*/

TABLE TD.details {
padding:0;
margin:0;
}

/* Tous les tableaux de sous-resultats */
TABLE.sresultats {
margin: auto;
empty-cells: hide;
}
TABLE.sresultats TH {
border-top: 1px solid rgb(80, 100, 133);
border-right: 1px solid rgb(80, 100, 133);
font-weight: bold;
text-align: center;
background-color: rgb(96, 115, 165);
color: #ffffff;
font-size: 100%;
}

/* Affichage des resultats positifs (vert en general) */
SPAN.positif {
direction: $direction;
color: #2f2f90; /* resultats dintre les taules en lletres ex. resum del periode + 200% * positiu */
}
/* resultats negatiu */
SPAN.negatif {
direction: $direction; 
color: red; 
}


/* link Next / Previous */
#next {
margin-bottom:10px;
font-size:85%;
margin-top:5px;
}



/*---------------------------- Informacio dins les taules. On no hi ha gr�fics Ex. pagines vistes */
{
border:0;
font-size:100%;
border-top: 1px solid #d4d0c8;
text-align: left;
color: #2f2f90; /*color lletra */
padding-top:5px;
padding-bottom:5px;
}

/* contains a subtable */
TABLE TD.tc{
padding:0;
}

/* without top border */
TABLE TD.wtb{
border-top:0;
padding-top:0;
}
/* particions talues on no hi han gr�fics */
/* with right border */
TABLE TD.wrb{
border-right: 1px solid #d4d0c8;
}

/*--------------------------------------------------------------*/

TABLE TD.acenter{
text-align: center;
color: #00008B;
}

TABLE.sub0 TD.data{
padding-left:5px;
}
TABLE.sub1 TD.data{
padding-left:45px;
}
TABLE.sub2 TD.data{
padding-left:75px;
}
TABLE.sub3 TD.data{
padding-left:110px;
}
TABLE.sub4 TD.data{
padding-left:140px;
}
TABLE.sub5 TD.data{
padding-left:150px;
}
TABLE.sub6 TD.data{
padding-left:160px;
}

TABLE TR.cate, TABLE TR.rouge{
height:28px;
font-weight: bold;
}


/* BUTTONS */
.boutonsAction {
text-align: center;
}
.boutonsAction INPUT {
	border: 3px double #999;
	border-left-color: #ccc;
	border-top-color: #ccc;
	color: #333;
	padding: 0.25em;
	font-size: 1.2em;
	text-align: center;
}

.boutonsAction INPUT:active {
	background: #f4f4f4;
	border: 3px double #ccc;
	border-left-color: #999;
	border-top-color: #999;
}

input.bouton {
background: #FFFFFF;
border: #FFFFFF 1px solid;
}


}
";
 ?>
