<?if (!$results):?>
<?if ($options_filters):?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td class="opcionsTitol"><strong><?=$c_options_bar?></strong></td>
    </tr>
    <tr>
      <td class="opcions"><table class="filters">
        <tr valign="middle">
		<?foreach($options_filters as $options_filter):?>
          <td><?=$options_filter?></td>
		  <?endforeach //$options_filters?>
        </tr>
      </table></td>
    </tr>
</table>
<?endif //options_filters?>
<?endif //!$results?>
<?if ($results):?><?=$out_loop_hidden?>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="listRecord">
<?if ($order_bar):?><tr><?if ($has_images):?>
      <td valign="middle" class="listCaption" width="<?=$im_admin_thumb_w?>">&nbsp;</td><?endif // has_images?>
       <?if ($options_checkboxes):?>
      <td width="30" valign="top" class="listCaption">&nbsp;</td><?endif // options_checkboxes?>
      <td width="30" valign="top" class="listCaption"><?=$c_list_pick_default?>&nbsp; </td>
      <?foreach($fields as $l): extract ($l)?>
      <td align="left" valign="top" nowrap="nowrap" class="listCaption"><?=$field?>&nbsp;
       </td> 
      <?endforeach //$field?>   
  </tr><?endif //order_bar?>
  <?foreach($loop as $l): extract ($l)?>
  <?if ($group_fields):?><tr><td colspan="<?=count($fields)+1?>" class="listGroup"><?foreach($group_fields as $f):?><?=$f?><?endforeach //$field?></td></tr><?endif // group_fields?>
    <tr onmousedown="list_pick[<?=$all_variation_category_id?>].on_click(event, <?=$id?>,'<?=$conta?>')" class="listItem<?=$odd_even?>" id="list_row_<?=$id?>"><?if ($has_images):?>
      <td valign="top" class="listImage" width="<?=$im_admin_thumb_w?>">	  
	  <?if ($image_name):?><div><img src="<?=$image_src_admin_thumb?>"></div>
	  <?else: // image_name?><div class="noimage" style="min-height:<?=$im_admin_thumb_h?>px;"></div>
	  <?endif // image_name?>	  
	  </td>
      <?endif // has_images?>
       <?if ($options_checkboxes):?> 
      <td width="30" valign="top"><input id="selected_<?=$id?>" name="selected[<?=$id?>]" type="checkbox" value="<?=$id?>"<?=$selected?> /></td><?endif // options_checkboxes?>
      <td width="30" valign="top"><input name="default[<?=$all_variation_category_id?>]" id="default_<?=$all_variation_category_id?>" type="radio" value="<?=$id?>"<?=$default_selected?> /></td>
  
<?foreach($fields as $l): extract ($l)?>
      <td align="left" valign="top">           
                  <table width="100%" border="0" cellpadding="0" cellspacing="0">
						<?foreach($val as $l): extract ($l)?>
                  <?if ($lang):?><tr>
                            <td colspan="2"><span class="formsCaption">
                            <?=$lang?>
                            </span></td>
						</tr>
                            <?endif //lang?><tr>
                        <td>&nbsp;</td><td width="100%"><?=$val?></td>
                      </tr>
						<?endforeach //$val?>
            </table></td><?endforeach //$field?></tr>  
<?endforeach //$loop?>
</table> 
<div class="sep"></div>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td align="left" class="opcions"><table border="0" cellpadding="2" cellspacing="0">
            <tr>
              <?if ($select_button):?>
              <td align="left">&nbsp;
                <?=$c_select?>
                &nbsp;<strong><a class="opcions" href="javascript:select_all_variations(true, '<?=$all_variation_category_id?>')">
                  <?=$c_all?>
                  </a>&nbsp;-&nbsp;<a class="opcions" href="javascript:select_all_variations(false, '<?=$all_variation_category_id?>')">
                    <?=$c_nothing?>
                  </a></strong></td>
              <?endif //select_button?>
            </tr>
        </table></td>
      </tr>
    </table>
<div class="separador"></div>
<?endif //results?>
