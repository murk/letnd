<?

/* 15-9-16

	DIRECTORI

	UP-DATE
	- Afegir possibilitat de no donar permisos, tot públic

 */

if ( is_table( 'directory__configadmin' ) && ! Db::get_first( "SELECT * FROM directory__configadmin WHERE name='is_all_public'" ) ) {
	$update_sql[] = "
		INSERT INTO directory__configadmin (`name`, `value`) VALUES ('is_all_public', '0');";

} else {
	if ( is_table( 'directory__configadmin' ) ) {
		trigger_error( 'Ja existeixen el camp is_all_public' );
	}
}


?>