<?

/*5-5-14
  Afegir subtitle product
	
 
 */

if (is_table('product__product_language') && !is_field("product__product_language",'product_subtitle')) {	
	
	$update_sql[] = "
		ALTER TABLE `product__product_language`
	ADD COLUMN `product_subtitle` VARCHAR(255) NOT NULL DEFAULT '' AFTER `product_title`;";
	
}



if (is_table('product__product_to_variation')){
	
	$update_sql[] = "					
					ALTER TABLE `product__variation_language`
						ADD COLUMN `variation_subtitle` VARCHAR(255) NOT NULL DEFAULT '' AFTER `variation`,
						ADD COLUMN `variation_description` TEXT NOT NULL DEFAULT '' AFTER `variation_subtitle`;
					";	
	
	$update_sql[] = "
					ALTER TABLE `product__product_to_variation`
							ADD COLUMN `variation_others1` VARCHAR(255) NOT NULL DEFAULT '' AFTER `variation_ref_supplier`,
							ADD COLUMN `variation_others2` VARCHAR(255) NOT NULL DEFAULT '' AFTER `variation_others1`,
							ADD COLUMN `variation_others3` VARCHAR(255) NOT NULL DEFAULT '' AFTER `variation_others2`,
							ADD COLUMN `variation_others4` VARCHAR(255) NOT NULL DEFAULT '' AFTER `variation_others3`;
					";	

	$update_sql[] = "
					CREATE TABLE `product__product_to_variation_language` (
						`product_variation_id` INT(11) NOT NULL DEFAULT '0',
						`product_id` INT(11) NOT NULL DEFAULT '0',
						`language` CHAR(3) NOT NULL DEFAULT '' COLLATE 'utf8_spanish2_ci',
						`variation_others1` TEXT NOT NULL COLLATE 'utf8_spanish2_ci',
						`variation_others2` TEXT NOT NULL COLLATE 'utf8_spanish2_ci',
						`variation_others3` TEXT NOT NULL COLLATE 'utf8_spanish2_ci',
						`variation_others4` TEXT NOT NULL COLLATE 'utf8_spanish2_ci'
					)
					COLLATE='utf8_spanish2_ci'
					ENGINE=MyISAM
					ROW_FORMAT=DYNAMIC;
					";	

	$update_sql[]="
			INSERT INTO `product__configadmin` (`name`) 
			VALUES ('variation_others1');";
	$update_sql[]="
			INSERT INTO `product__configadmin` (`name`,value) 
			VALUES ('variation_others1_type','text_no_lang');";

	$update_sql[]="
			INSERT INTO `product__configadmin` (`name`) 
			VALUES ('variation_others2');";
	$update_sql[]="
			INSERT INTO `product__configadmin` (`name`, value) 
			VALUES ('variation_others2_type','text_no_lang');";

	$update_sql[]="
			INSERT INTO `product__configadmin` (`name`) 
			VALUES ('variation_others3');";
	$update_sql[]="
			INSERT INTO `product__configadmin` (`name`,value) 
			VALUES ('variation_others3_type','text_no_lang');";

	$update_sql[]="
			INSERT INTO `product__configadmin` (`name`) 
			VALUES ('variation_others4');";
	$update_sql[]="
			INSERT INTO `product__configadmin` (`name`,value) 
			VALUES ('variation_others4_type','text_no_lang');";
	
	
	
	
	
	global $gl_languages;	
	
	foreach($gl_languages['names'] as $lang=>$val){
		$update_sql[]="
			INSERT INTO `product__configadmin_language` (`name`, `language`) 
			VALUES ('variation_others1', '".$lang."');";
		$update_sql[]="
			INSERT INTO `product__configadmin_language` (`name`, `language`) 
			VALUES ('variation_others2', '".$lang."');";
		$update_sql[]="
			INSERT INTO `product__configadmin_language` (`name`, `language`) 
			VALUES ('variation_others3', '".$lang."');";
		$update_sql[]="
			INSERT INTO `product__configadmin_language` (`name`, `language`) 
			VALUES ('variation_others4', '".$lang."');";
	}
	
}


/*

ALTER TABLE `product__variation_language`
	ADD COLUMN `variation_subtitle` VARCHAR(255) NOT NULL DEFAULT '' AFTER `variation`,
	ADD COLUMN `variation_description` TEXT NOT NULL DEFAULT '' AFTER `variation_subtitle`;
 

ALTER TABLE `product__product_to_variation`
	ADD COLUMN `variation_others1` VARCHAR(255) NOT NULL DEFAULT '' AFTER `variation_ref_supplier`,
	ADD COLUMN `variation_others2` VARCHAR(255) NOT NULL DEFAULT '' AFTER `variation_others1`,
	ADD COLUMN `variation_others3` VARCHAR(255) NOT NULL DEFAULT '' AFTER `variation_others2`,
	ADD COLUMN `variation_others4` VARCHAR(255) NOT NULL DEFAULT '' AFTER `variation_others3`;


CREATE TABLE `product__product_to_variation_language` (
	`product_variation_id` INT(11) NOT NULL DEFAULT '0',
	`product_id` INT(11) NOT NULL DEFAULT '0',
	`language` CHAR(3) NOT NULL DEFAULT '' COLLATE 'utf8_spanish2_ci',
	`other4` TEXT NOT NULL COLLATE 'utf8_spanish2_ci'
)
COLLATE='utf8_spanish2_ci'
ENGINE=MyISAM
ROW_FORMAT=DYNAMIC;

 */


?>