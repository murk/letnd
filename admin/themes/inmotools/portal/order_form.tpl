<script language="JavaScript" src="/admin/jscripts/validacio.js?v=<?=$version?>"></script>
<form name="form_delete" method="post" action="<?=$link_action_delete?>">
    <input name="<?=$id_field?>" type="hidden" value="<?=$id?>">
  </form><form name="theForm" method="post" action="<?=$link_action?>" onsubmit="<?=$js_string?>">
<input name="order_id[<?=$id?>]" type="hidden" value="<?=$id?>" />
<input name="order_id_javascript" type="hidden" value="<?=$id?>" />
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><table border="0" cellspacing="2" cellpadding="2">
      <tr>
        <td class="formCaptionTitle"><strong>
          <?=$c_edit_data?>
          &nbsp;</strong></td>
        </tr>
    </table></td>
  </tr>
</table>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td><table width="100%" cellpadding="4" cellspacing="0">
      <tr>
        <td class="formCaptionTitle"><strong>
          <?=$c_order_title_admin?></strong>&nbsp;
          <?=$c_order_num?><?=$id?>
          &nbsp;</td>
      </tr>
    </table>
      <table width="100%" border="0" cellpadding="0" cellspacing="0" class="formstbl">
        <tr>
          <td><table width="100%" cellpadding="4" cellspacing="0">
              <tr>
                <td width="150" class="formsCaption" valign="top"><strong>
                  <?=$c_entered_order?>
                  :</strong></td>
                <td valign="top" class="forms"><?=$entered_order?></td>
              </tr>
              <tr>
                <td width="150" class="formsCaption" valign="top"><strong>
                  <?=$c_shipped?>
                  :</strong></td>
                <td valign="top" class="forms"><?=$shipped?></td>
              </tr>
              <tr>
                <td width="150" class="formsCaption" valign="top"><strong>
                  <?=$c_delivered?>
                  :</strong></td>
                <td valign="top" class="forms"><?=$delivered?></td>
              </tr>
              <tr>
                <td width="150" class="formsCaption" valign="top"><strong>
                  <?=$c_order_status?>
                  :</strong></td>
                <td valign="top" class="forms"><?=$order_status?></td>
              </tr>
              <tr>
                <td valign="top" class="formsCaption"><strong>
                  <?=$c_comment?>
                  : </strong></td>
                <td class="forms"><?=$comment?></td>
              </tr>
            </table></td>
        </tr>
      </table>
      <? if($show_account):?>
      <table width="100%" cellpadding="4" cellspacing="0">
        <tr>
          <td class="formCaptionTitle"><div class="title">
            <strong>
            <?=$c_account_title?>
          </strong></div></td>
        </tr>
      </table>
      <table  class="formstbl">
        <tr>
          <td width="150" valign="top"><strong>
            <?=$banc_name?>
            :</strong></td>
          <td valign="top"><?=$account_number?></td>
        </tr>
        <tr>
          <td width="150" valign="top"><strong>
            <?=$c_account_amount?>
            :</strong></td>
          <td valign="top"><?=$total?>
            <?=CURRENCY_NAME?></td>
        </tr>
        <tr>
          <td width="150" valign="top"><strong>
            <?=$c_account_concept?>
            :</strong></td>
          <td valign="top"><?=$concept?></td>
        </tr>
      </table>
	  <? endif // show_account?>
      <table width="100%" cellpadding="4" cellspacing="0">
        <tr>
          <td class="formCaptionTitle"><strong>
            <?=$c_order_title2?>
          </strong></td>
        </tr>
      </table>
      <table width="100%" border="0" cellpadding="8" cellspacing="0" class="formstbl">
          <tr>
            <td style="padding-top:18px;padding-bottom:18px;"><?=$orderitems?></td>
          </tr>
        </table>
      <table width="100%" cellpadding="4" cellspacing="0">
        <tr>
          <td class="formCaptionTitle"><strong>
            <?=$c_order_title3?>
          </strong></td>
        </tr>
      </table>
      <table width="100%" border="0" cellpadding="8" cellspacing="0" class="formstbl">
          <tr>
            <td style="padding-top:18px;padding-bottom:18px;"><?=$address_invoice?></td>
          </tr>
        </table>
      <table width="100%" cellpadding="4" cellspacing="0">
        <tr>
          <td class="formCaptionTitle"><strong>
            <?=$c_order_title4?>
          </strong></td>
        </tr>
      </table>
      <table width="100%" border="0" cellpadding="8" cellspacing="0" class="formstbl">
          <tr>
            <td style="padding-top:18px;padding-bottom:18px;"><?=$address_delivery?></td>
          </tr>
        </table>
      <table width="100%" cellpadding="4" cellspacing="0">
        <tr>
          <td class="formCaptionTitle"><strong>
            <?=$c_order_title5?>
          </strong></td>
        </tr>
      </table>
      <table width="100%" border="0" cellpadding="8" cellspacing="0" class="formstbl">
          <tr>
            <td style="padding-top:18px;padding-bottom:18px;"><?=$payment_method?></td>
          </tr>
        </table>
      <?if ($form_new):?>
      <table width="100%" cellpadding="4" cellspacing="2" class="formsButtonstbl">
        <tr>
          <td align="center" class="formsButtons"><input type="submit" name="Submit" value="<?=$c_send_new?>" class="btn_form1" /></td>
        </tr>
      </table>
      <?endif //form_new?>
      <?if ($form_edit):?>
      <table width="100%" cellpadding="4" cellspacing="2" class="formsButtonstbl">
        <tr>
          <td align="center" class="formsButtons"><input type="submit" name="Submit" value="<?=$c_send_edit?>" class="btn_form1" />
            &nbsp;&nbsp;<a class="btn_form1" href="javascript:submit_form('<?=$c_confirm_form_bin?>', true);">
              <?=$c_delete?>
              </a>&nbsp;<a class="btn_form1" href="<?=$preview_link?>" target="_blank">
                <?=$c_preview?>
              </a>&nbsp;</td>
        </tr>
      </table>
      <?endif //form_edit?></td>
  </tr>
</table></form>
