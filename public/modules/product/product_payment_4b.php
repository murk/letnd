<?
/**
 * ProductOrder
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 7/2012
 * @version $Id$
 * @access public
Config a https://tpv.4b.es/config/
1 - http://www.clubmotoshop.com/spa/product/payment/v/action/get_order/process/4b/
2 - http://www.clubmotoshop.com/spa/product/payment/v/action/set_notice/process/4b/
3 - http://www.clubmotoshop.com/spa/product/payment/v/action/set_notice/process/4b/
4 - http://www.clubmotoshop.com/spa/product/payment/v/action/show_end/process/end/
 
1 - Conectar a passsat: Apartat 3.1.1
	$this->get_content
	--------------------------------------------
	https://tpv2.4b.es/simulador/teargral.exe 
	https://tpv.4b.es/tpvv/teargral.exe
	
	Parametres ref com, clau comerce, idioma ( es, ca, en, fr, de )

2-  Retorna a botiga: Apartat 3.1.2	
	$this->get_order()
	--------------------------------------------
	order = ref compra
	store: calu botiga
	
3 - Generar sortida	
	$this->get_order()
	test: /spa/product/payment/v/action/get_order/process/4b/?&order=76&store=PI00020630
	http://www.clubmotoshop.com/spa/product/payment/v/action/get_order/process/4b/
	--------------------------------------------
	Import total compra M97810000   -> 978 -> codi euos, 10000 -> import multiplicat per 100 ( per treure els dos decinals )
	Num registres de la cistella
	Registres cistella compra -> referencia
								descripcio
								unitats
								preu    -> 10000  -> import multiplicat per 100 ( per treure els dos decinals )
	
	Cada un d'aquests parmetres van amb un retorn de carro sense espais blancs ni html
	
	ej: 12 euros, 2 productes
				M9781200
				2
				A1345
				Novelas clásicas Vol.1
				1
				500
				A1348
				VHS  El fin de los dias
				1
				700

4 - Comunicació online del resultat. Apartat 3.2.3
	--------------------------------------------
	
		Adreça resposta banc			
		/spa/product/payment/v/action/set_notice/process/4b/
		
		Autoritzat
		?result=0&pszPurchorderNum=75&pszTxnDate=10/02/2010&tipotrans=SSL&store=PI00020630&pszApprovalCode=123456&pszTxnID=123456789012
		
		Denegat
		?result=2&pszPurchorderNum=75&pszTxnDate=10/02/2010&tipotrans=SSL&store=PI00020630&coderror=180&deserror=Operacion%20Denegada

		Verificar IP del banc
		
				result 	0->Transacción autorizada
						1->Transacción cancelada
						2->Transacción fallida
				pszPurchorderNum  -> referencia compra
				pszTxnDate         -> data transaccio
				tipotrans		  -> SSL/CES
				store             -> clau botiga
			
			Aperacio correcta
				pszApprovalCode   -> codi aprovacio
				pszTxnID		->   Id transacció
			Operacio fallida
				coderror		-> codi error
				deserror		-> descripcio error
			Firma de dade ( opcional )
				MAC				-> Resumen hash de los parámetros enviados.
				
	A les URLS del mòdul deconfiguració: 
		URL que graba el resultado en la BD (TRANSACCIÓN AUTORIZADA)
		URL que graba el resultado en la BD (TRANSACCIÓN DENEGADA)

5 - Pàgina de notificació de la compra
	--------------------------------------------
	
	Adreça:
	/spa/product/payment/v/action/show_end/process/end/

	ELs mateixos parametres que el 4
		
	
 */
class ProductPayment4b extends ModuleBase {
    var $vars = array();
    var $config;
	var $url;
	var $order_id;

	var $order_status = '';
	var $transaction_id;
	var $approval_code = '';
	var $error_code = '';
	var $error_description = '';
    function __construct(&$module, $order_vars='')
    {
        $this->config = &$module->config;
		$this->url = $this->config['4b_simulador']?'tpv2.4b.es/simulador/teargral.exe':'tpv.4b.es/tpvv/teargral.exe';

        if ($order_vars) {
			// variables del paypal
			$this->vars = array(
				'order' => $order_vars['order_id'],
				'id_comercio' => $this->config['4b_id_comercio']
				//,
				//'idioma' => LANGUAGE_CODE
			);
			//Debug::p($this->vars);
        }

    }
    // obté formulari html
    function get_content()
    {
        $ret = '<form action="https://'.$this->url.'" method="post" id="4b_form">';
        foreach ($this->vars as $key => $val) {
            $ret .= '<input type="hidden" name="' . $key . '" value="' . $val . '" />
			';
        }
        $ret .= '<input type="submit">';
        $ret .= '</form>';
        if (!$this->config['4b_simulador']) $ret .= get_javascript('$("#4b_form").submit();');
        return $ret;
    }
	// test: /?tool=product&tool_section=payment&language=cat&action=get_order&process=4b&order=76&store=PI00020630
	/*	Array
	(
		[order] => 76
		[store] => PI00020630
	)
	*/
    function get_order(){
	
		//guardamos las variables que pasa el sistema passat
		$order_id = R::escape('order');
		$store = R::escape('store');

		//comprobacions de seguridad e integridad: s'hauria de comprobar IP

		//comprobem el codi del comerç
		if ($store != $this->config['4b_id_comercio'] || !$order_id || !$store) {
			
			$this->trace($ret . "Variables get:\n---------------\n" . print_r($_GET,true) . "\n\n", 'get_order NO HAFET RES');
			
			Debug::p_all();
			die();
		
		}

		//el preu iso es M (money) + el código del euro (978) + el precio normalizado
		// agafo preu
		$all_basetax = Db::get_first("SELECT all_basetax FROM product__order WHERE order_id = '".$order_id."'");

		$all_basetax = $all_basetax*100;
		$all_quantity = 0;
		
		$ret = '';
		
		
		// items
		$module = Module::load('product','orderitem', '', false); // així no necessito el product_orderitem.php
		$module->action = 'get_records';		
		$listing = new ListRecords($module);	
		$listing->paginate = false;
		$listing->has_bin = false;
		$listing->condition = 'order_id = ' . $order_id;
		$listing->list_records(false);
		
		foreach ($listing->results as $key=>$val){
			$rs = &$listing->results[$key];
			$product = $rs['product_title'];
			if ($rs['product_variations']) {
				$rs['product_variations'] = explode("\r",$rs['product_variations']);
				$rs['product_variation_categorys'] = explode("\r",$rs['product_variation_categorys']);
				array_pop ($rs['product_variations']);
				array_pop ($rs['product_variation_categorys']);
				foreach($rs['product_variations'] as $key=>$val){
					$rs['product_variations'][$key] = 
						$rs['product_variation_categorys'][$key] . ': ' . $rs['product_variations'][$key];
				}
				$rs['product_variations'] = implode(", ",$rs['product_variations']);
				$product .= ' - ' . $rs['product_variations'];
				
			}
			$ret .= $rs['ref'] . "\n";
			$ret .= $product . "\n";
			$ret .= $rs['quantity'] . "\n";
			$ret .= ($rs['product_total_basetax']*100) . "\n";
			$all_quantity += (int)$rs['quantity'];			
		}
		$ret = "M978" . $all_basetax . "\n" . $all_quantity . "\n" . $ret . "\n";
		/*
		$ret .= "M978$all_basetax\n";
		$ret .= "1\n";    
		$ret .= "1\n";        
		$ret .= "Compra en tienda online\n";        
		$ret .= "1\n";            
		$ret .= "$all_basetax\n";        
		$ret .= "\n"; 
		*/
	
		
		$this->trace($ret . "Variables get:\n---------------\n" . print_r($_GET,true) . "\n\n", 'get_order');
		echo $ret;
		
		Debug::p_all();
		die();
	}
	// test: /spa/product/payment/v/action/set_notice/process/4b/?result=0&pszPurchorderNum=75&pszTxnDate=10/02/2010&tipotrans=SSL&store=PI00020630&pszApprovalCode=123456&pszTxnID=123456789012
    function set_notice()
    {
		Debug::add('4b set_notice','Si');
		        
        $emailtext = "";
		// send_mail_admintotal('Intent de pagament invàlid per HTTP ERROR amb paypal a ' . HOST_URL,$emailtext);
		
        $order_id = $_REQUEST['pszPurchorderNum'];
		$store = $_REQUEST['store'];
		$pszApprovalCode=isset($_REQUEST['pszApprovalCode'])?$_REQUEST['pszApprovalCode']:'';
		$transaction_id=isset($_REQUEST['pszTxnID'])?$_REQUEST['pszTxnID']:'';
		$error_code=isset($_REQUEST['coderror'])?$_REQUEST['coderror']:'';
		$error_description=isset($_REQUEST['deserror'])?$_REQUEST['deserror']:'';
        
		// ids
		$this->transaction_id = $transaction_id;
		$this->approval_code = $pszApprovalCode;
		$this->order_id = $order_id;
		$this->error_code = $error_code;
		$this->error_description = $error_description;
		Debug::add('4b pszApprovalCode',$this->transaction_id);
		
		switch($_REQUEST['result']){
			case '0':
				$this->order_status = 'completed';
				break;
			case '1':
				$this->order_status = 'voided';
				break;
			case '2':
				$this->order_status = 'failed';
				break;
		
		}
		
		$this->trace("Variables get:\n---------------\n" . print_r($_GET,true) . "\n\n", 'set_notice');
		
    	return true;
    }
	
	function trace($log, $method){
		$fp = fopen (CLIENT_PATH . '/temp/4b.log', "a+");
		fwrite($fp,"================================================\n" . $method . ' - ' .date("Y-m-d H:i:s"). " - " . $_SERVER['REMOTE_ADDR'] . "\n================================================\n\n".$log);
		fclose($fp);
	}
}
?>