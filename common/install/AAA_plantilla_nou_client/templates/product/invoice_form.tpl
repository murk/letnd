<div class="botoprint noprint" onclick="window.print();"><?=$c_print?></div>


<div class="invoice">

	<div class="logo-invoice"><img src="/<?=CLIENT_DIR?>/images/logo_print.gif" width="150" border="0" alt=""/></div>

	<div class="row">

		<div class="left">
				<h4><strong><?= $c_invoice_id ?>:</strong> <?= $invoice_id ?></h4>

				<h3 class="company"><?= COMPANY_NAME ?></h3>

				<span class="street-address"><?= PAGE_ADDRESS ?> <?= PAGE_NUMSTREET ?></span>,
				<span class="postal-code"><?= PAGE_POSTCODE ?></span>
				<span class="locality"><?= PAGE_MUNICIPI ?></span> <br>
				<span class="region"><?= PAGE_PROVINCIA ?></span> <br>
				<span class="country-name"><?= PAGE_COUNTRY ?></span><? if ( PAGE_PHONE ): ?>,
			T. <span class="tel"><?= PAGE_PHONE ?></span>
		<? endif // PAGE_PHONE ?><? if ( PAGE_FAX ): ?>, <span class="type">Fax</span>.
					<span class="value"><?= PAGE_FAX ?></span><? endif //FAX?> <br>
				<?= HOST_URL ?> - <?= encode_email( $default_mail ) ?> <br>
				<strong>CIF:</strong> <?= COMPANY_CIF ?>
		</div>

		<div class="right">
			<h4><strong><?= $c_invoice_date ?>:</strong> <?= $invoice_date ?></h4>
			<h3>
				<?= $c_order_title_invoice_customer ?>
			</h3>
			<?= $address_invoice ?>
		</div>
	</div>

	<h3>
		<?=$c_order_title2?>
	</h3>
	
	<?=$orderitems?>

</div>
