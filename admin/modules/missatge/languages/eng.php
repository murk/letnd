<?php
// menus eina
define( 'MISSATGE_MENU_NEW', 'Create Message' );
define( 'MISSATGE_MENU_MISSATGE', 'Messages' );
define( 'MISSATGE_MENU_LIST', 'List Messages' );
define( 'MISSATGE_MENU_BIN', 'Recycling Bin' );

// captions seccio
$gl_caption_missatge['c_status']          = 'Status';
$gl_caption_missatge['c_subject']         = 'Subject';
$gl_caption_missatge['c_missatge']        = 'Message';
$gl_caption_missatge['c_blocked']         = '';
$gl_caption_missatge['c_bin']             = '';
$gl_caption_missatge['c_mls_from_id']     = 'From';
$gl_caption_missatge['c_mls_to_id']       = 'To';
$gl_caption_missatge['c_creator_user_id'] = 'From';

$gl_caption_missatge['c_status_prepare'] = 'In preparation';
$gl_caption_missatge['c_status_waiting'] = 'To read';
$gl_caption_missatge['c_status_readed']  = 'Read';
$gl_caption_missatge['c_status_send']    = 'Sent';

$gl_caption_missatge ['c_kind']         = 'Rype';
$gl_caption_missatge ['c_kind_mls']     = 'Mls';
$gl_caption_missatge ['c_kind_general'] = 'General';

$gl_caption_missatge ['c_entered']  = 'Creation date';
$gl_caption_missatge ['c_modified'] = 'Modification date';
$gl_caption_missatge ['c_sent']     = 'Sent';

$gl_caption['c_user_id']       = "Or user";
$gl_caption_list['c_user_id']  = "Users";
$gl_caption['c_group_id']      = "Group";
$gl_caption_list['c_group_id'] = "Group";

$gl_caption['c_form_level1_title_group_id'] = "Send to:";
$gl_caption['c_form_level1_title_subject']  = "Message";

$gl_caption['c_reply_button'] = $gl_caption['c_reply'] ;
?>
                  

