<?

/**
 * ProductCommonOrder
 *
 * Funcions comuns per Ordres tant a public com admin
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) Novembre 2016
 * @version $Id$
 * @access public
 */
class ProductCommonOrder {

	function __construct() {

	}


	static public function send_email_status_change( $caption, $config ) {

		$page        = ProductCommonOrder::email_get_page( $config, false );
		$action      = R::text_id( 'mail_status', false, '_POST' );
		$id          = R::id( 'id', false, '_POST' );
		$status_date = R::escape( 'status_date', false, '_POST' );
		$status_time = R::escape( 'status_time', false, '_POST' );
		$courier_id = R::escape( 'courier_id', false, '_POST' );
		$courier_follow_code = R::escape( 'courier_follow_code', false, '_POST' );

		if ( ! $id || ! $action ) {
			$ret['error'] = $caption['c_mail_send_error'];
			json_encode( $ret );
		}

		// primer guardo alguns camps
		$query = "UPDATE product__order SET courier_id= '$courier_id', courier_follow_code= '$courier_follow_code'";
		// guardo la data també, així no haig de guardar el form
		if ( $status_date ) {
			$query .= ", ${action} = '" . unformat_date( $status_date ) . " " . unformat_time( $status_time ) . "'";
		}
		$query .= " WHERE order_id=$id";
		Db::execute( $query );


		$query    = "SELECT CONCAT(name,' ', surname) AS name, mail, treatment, customer_id, courier_follow_code, courier_id FROM product__customer JOIN product__order USING(customer_id) WHERE order_id = '" . $id . "'";
		$customer = Db::get_row( $query );

		$query = "SELECT courier, courier_url FROM product__courier WHERE courier_id = " . $customer['courier_id'];
		$courier = Db::get_row( $query );

		$t = array();

		if ( USE_FRIENDLY_URL ) {
			// TODO-i SSL - Canviar segons el que tingui el client, a l'email no m'en fio del truc "//url"
			$customer_url = 'http://' . $_SERVER['HTTP_HOST'] . '/' . LANGUAGE . '/product/customer/';
		} else {
			$customer_url = 'http://' . $_SERVER['HTTP_HOST'] . '/?tool=product&tool_section=customer&language=' . LANGUAGE;
		}

		switch ( $action ) {
			case 'prepared':
				$t['c_text_status_change'] = sprintf(
					$caption[ 'c_mail_body_' . $action ],

					$customer['name'],
					$id,
					$customer_url,
					DEFAULT_MAIL,
					SHORT_HOST_URL
				);
				break;
			case 'shipped':

				if(!$courier || !$customer['courier_follow_code'])
					$caption['c_mail_body_shipped_2'] = '';

				$caption[ 'c_mail_body_shipped'] .= $caption[ 'c_mail_body_shipped_2'] . $caption[ 'c_mail_body_shipped_3'];

				$t['c_text_status_change'] = sprintf(
					$caption[ 'c_mail_body_' . $action ],

					$customer['name'],
					$id,
					$courier['courier_url'],
					$courier['courier'],
					$customer['courier_follow_code'],
					$customer_url,
					SHORT_HOST_URL
				);
				break;
			case 'delivered':
				$t['c_text_status_change'] = sprintf(
					$caption[ 'c_mail_body_' . $action ],

					$customer['name'],
					$id,
					DEFAULT_MAIL,
					SHORT_HOST_URL
				);
				break;
		}

		$module        = ProductCommonOrder::email_get_order_module( $id, $action, $t );

		$module->set_vars( array(
			'is_buyer'  => false,
			'is_seller' => false,
			'is_status_change' => true,
			'status_change' => $action
		) );

		$body_buyer    = ProductCommonOrder::email_get_order_body( $module, $page );
		$subject_buyer = sprintf( $caption[ 'c_mail_subject_buyer_status_' . $action ], substr( HOST_URL, 7, - 1 ), $id );

		$body_seller    = ProductCommonOrder::email_get_order_body( $module, $page );
		$subject_seller = sprintf( $caption[ 'c_mail_subject_seller_status_' . $action ], substr( HOST_URL, 7, - 1 ), $id );

		// dades mail venedor i comprador
		$ds = array(
			'0' => array(
				'is_seller' => false,
				'to'        => $customer['mail'],
				'body'      => $body_buyer,
				'subject'   => $subject_buyer
			),
			'1' => array(
				'is_seller' => true,
				'to'        => $config['from_address'],
				'body'      => $body_seller,
				'subject'   => $subject_seller
			),
		);

		$sent_customer = false;
		// envio tots els mails
		foreach ( $ds as $d ) {
			$sent = ProductCommonOrder::send_email( $d, false, $config );
			if ( ! $d['is_seller'] && $sent )
				$sent_customer = true;
		}

		// header('Content-Type: application/json');
		// si s'ha enviar, guardo la data d'enviament
		if ( $sent_customer ) {
			$date = now();

			$query = "UPDATE product__order SET ${action}_emailed = '" . unformat_date( $date ) . "' WHERE order_id=$id";

			Db::execute( $query );

			$ret['error']        = false;
			$ret['date_emailed'] = $date;
		} else {
			$ret['error'] = $caption['c_mail_send_error'];
		}

		Debug::p_all();
		header( 'Content-Type: application/json' );
		die( json_encode( $ret ) );

	}

	static public function email_get_page( &$config, $is_test = false ) {

		$GLOBALS['gl_current_mailer'] = get_mailer( $config );

		include_once( DOCUMENT_ROOT . 'admin/modules/newsletter/newsletter_message_functions.php' );

		$page = clone $GLOBALS['gl_page'];

		if ( $GLOBALS['gl_is_admin'] ) {

			$page->path = PATH_TEMPLATES_PUBLIC;
		}

		$page->template = 'mail.tpl';

		$page->set_vars( array(
			'preview'        => $is_test ? true : false,
			'url'            => HOST_URL,
			'default_mail'   => DEFAULT_MAIL,
			'page_slogan'    => PAGE_SLOGAN,
			'remove_text'    => '',
			'short_host_url' => substr( HOST_URL, 7, - 1 )
		) );

		return $page;
	}

	// Lassdive
	static public function shop_app_update_sells( $id ) {
		$module            = Module::load( 'product', 'order' );
		$module->parent    = 'ProductPayment';
		$module->is_logged = true;
		$module->id        = $id;
		$module->is_shop_app = true;

		// TODO Aquí recarrega el cart_list i no hauria de fer-ho, poder el millor es simplement agafar valors deirecte de la bbdd
		return $module->do_action( 'get_record' );

	}

	static public function email_get_order_module( $id, $action, $texts, $vars = false ) {
		$module            = Module::load( 'product', 'order' );
		$module->parent    = 'ProductPayment';
		$module->is_logged = true;
		$module->id        = $id;
		if ( $vars )
			$module->set_vars( $vars );
		$module->set_vars( array(
			'is_buyer'        => false,
			'is_seller'       => false,
			'show_' . $action => $action
		) );

		foreach ( $texts as $key => $text ) {
			$module->caption[ $key ] = $text;
		}

		return $module;
	}

	static public function email_get_body( &$page, $body ) {

		$page->content = $body;
		$body          = $page->get();

		return $body;
	}

	static public function email_get_order_body( &$module, &$page ) {

		$body          = $module->do_action( 'get_record' );
		$page->content = $body;
		$body          = $page->get();

		return $body;
	}

	static public function send_email( $vars, $is_test, $config ) {

		extract( $vars );
		$mail = $GLOBALS['gl_current_mailer'];

		if ( is_file( CLIENT_PATH . 'images/logo_newsletter.jpg' ) ) {
			$mail->AddEmbeddedImage( CLIENT_PATH . 'images/logo_newsletter.jpg', 'logo', 'logo.jpg' );
		} elseif ( is_file( CLIENT_PATH . 'images/logo_newsletter.gif' ) ) {
			$mail->AddEmbeddedImage( CLIENT_PATH . 'images/logo_newsletter.gif', 'logo', 'logo.gif' );
		}

		// Debug::p( $config, '$this->config' );
		if ( $config ['mail_auto_inline_css'] ) {

			$instyler = new \Pelago\Emogrifier( $body );
			$body     = $instyler->emogrify();
		}

		// resetejo el recipients abans d'enviar
		$mail->clearAllRecipients();

		$mail->Body    = $body;
		$mail->Subject = $subject;
		$mail->IsHTML( true );
		$mail->AddAddress( $to );
		//$mail->AddAddress ('marc@letnd.com');
		// per veure com queda el mail quan estem maquetant
		if ( $is_test ) {
			Debug::p_all();
			if ( ! isset( $_GET['is_seller'] ) || $is_seller )
				die( $body );

			return true;
		} elseif ( DEBUG ) {
			// Debug::p( $body, 'body' );
			$sent = send_mail( $mail, false );
			return true;
		} else {
			$sent = send_mail( $mail, false );
			send_mail_admintotal( $subject, $body ); // per veure com s'envien les comandes
			return $sent;
		}
	}

	static public function set_refund_negative_vals($is_refund, &$show, &$listing){

		if ( $is_refund ) {

			$negative_vars = [
				'total_base',
				'total_tax',
				'total_price',
				'total_basetax',
				'rate_base',
				'rate_tax',
				'rate_price',
				'rate_basetax',
				'all_base',
				'all_tax',
				'all_equivalencia',
				'all_price',
				'all_basetax',
				'promcode_discount',
			];

			// TODO-i Que s'ha de fer amb el recàrrec d'equivalencia?
			foreach ( $negative_vars as $negative_var ) {
				if (floatval( $show->rs[$negative_var]))
					$listing->set_var($negative_var, '-' . $listing->get_var($negative_var));
			}

		}
	}

	static public function get_order_item_downloads($order_id, $order_status, &$module, &$listing) {

		if (
			( $order_status == 'paying') ||  ///////// AQUESTS ESTATS SON QUAN EL PRODUCTE NO ESTÀ PAGAT
			( $order_status == 'failed') ||
			( $order_status == 'pending') ||
			( $order_status == 'denied') ||
			( $order_status == 'voided') ||
			( $order_status == 'refunded')
			// ( $order_status == 'completed' ) ||  ///////// AQUESTS ESTATS SON QUAN EL PRODUCTE JA ESTÀ PAGAT
			// ( $order_status == 'preparing' ) ||
			// ( $order_status == 'shipped' ) ||
			// ( $order_status == 'delivered' )||
			// ( $order_status == 'collect' )
		) {
			$listing->set_var('is_downloadable',false);
			return;
		}

		// Si es podes descarregar arxius, comprovo si ni ha, sino no cal mostrar la columna de descarregues
		$has_downloads = false;
		foreach ( $listing->results as $key => $val ) {
			$rs = &$listing->results[ $key ];

			$rs['product_downloads'] = $module->_get_downloads( $rs['product_id'], $order_id );
			if ( $rs['product_downloads'] ) $has_downloads = true;
		}

		$listing->set_var('is_downloadable',$has_downloads);
	}

	static public function get_order_item_variations( &$listing, $price_sufix, $is_refund ) {
		foreach ( $listing->results as $key => $val ) {
			$rs                                = &$listing->results[ $key ];
			$product_variation_ids          = explode( "_", $rs['product_variation_ids'] ); // aquesta no sobreescric el valor del rs ( ho vaig posar per poder buscaqr la imatge corresponent )
			$rs['product_variations']          = explode( "\r", $rs['product_variations'] );
			$rs['product_variation_categorys'] = explode( "\r", $rs['product_variation_categorys'] );
			$rs['product_variation_loop']      = array();

			array_pop( $rs['product_variations'] );
			array_pop( $rs['product_variation_categorys'] );

			$product_variation_categorys = '';
			foreach ( $rs['product_variations'] as $k => $v ) {
				$product_variation_categorys .=
					"<strong>" . $rs['product_variation_categorys'][ $k ] . ":</strong> " . $rs['product_variations'][ $k ] . ', ';
				$rs['product_variation_loop'][] = array(
					'product_variation_id' => $product_variation_ids[$k],
					'product_variation_category' => $rs['product_variation_categorys'][ $k ],
					'product_variation'          => $rs['product_variations'][ $k ]
				);
			}

			$rs['product_variation_categorys'] = $product_variation_categorys;
			$rs['product_variations']          = implode( ",", $rs['product_variations'] );

			$rs['product_price']       = format_currency( $rs[ 'product' . $price_sufix ] );
			$rs['product_total_price'] = format_currency( $rs[ 'product_total' . $price_sufix ] );


			if ( $is_refund ) {

				$negative_vars             = [
					'product_price',
					'product_total_price',
					'product_base',
					'product_total_base',
				];

				foreach ( $negative_vars as $negative_var ) {
					$rs[ $negative_var ] = "-" . $rs[ $negative_var ];
				}
			}

			// la foto l'agafo de product
			$rs += UploadFiles::get_record_images( $rs['product_id'], true, 'product', 'product' );
		}
	}

	// TODO-i Nomes funciona amb una sola categoria de variacions amb imatges
	static public function cart_get_variation_image( $rs ) {

		$product_id = $rs['product_id'];

		$product_variation_loop = $rs['product_variation_loop'];
		$images                = $rs['images'];
		$image_id              = isset($rs['image_id'])?$rs['image_id']:'';

		if (!$image_id) return [];

		foreach ( $product_variation_loop as $variation ) {

			$selected_product_variation_id = $variation['product_variation_id'];
			// primer comprobo si es la principal
			$query                = "
				SELECT product_variation_id 
				FROM product__product_image
				WHERE image_id = $image_id";
			$product_variation_id = Db::get_first( $query );

			if ( $product_variation_id == $selected_product_variation_id ) {
				return [];
			}

			foreach ( $images as $image ) {

				$image_id             = $image['image_id'];
				$query                = "
				SELECT product_variation_id 
				FROM product__product_image
				WHERE image_id = $image_id
				ORDER BY ordre";
				$product_variation_id = Db::get_first( $query );

				if ( $product_variation_id == $selected_product_variation_id ) {
					return $image;
				}

			}
		}

		return [];
	}
}

?>