<?php
$gl_caption ['c_send_mail']='Enviar Correu';
$gl_messages_cart ['no_records']=$gl_caption_cart ['c_no_records']=$gl_caption_cart ['c_empty_cart']='La cistella està buida';
$gl_messages_order ['no_records'] = 'Encara no has realitzat cap comanda';

$gl_caption['c_productes_destacats']='Productes destacats';
$gl_caption['c_products']='Productes';
$gl_caption['c_product']='Producte';
$gl_caption['c_files']='Arxius';
$gl_caption['c_discount'] = 'Descompte';

// NO TRADUIT

$gl_caption ['c_next_payment_step']='Següent';
$gl_caption ['c_continue_shopping']='Seguir comprant';
$gl_caption ['c_text_register']='Anem  a procedir a la creació del seu compte';
$gl_caption ['c_register']='Registrar-se';
$gl_caption_customer ['c_form_title']=$gl_caption ['c_summary_title']='Dades personals';
$gl_caption_customer ['c_title_register']='Registrar-se';
$gl_caption_customer ['c_title_login']='Identificació';
$gl_caption_customer ['c_is_customer']='Ja és client nostre';
$gl_caption_customer ['c_new_customer']='És un nou client';
$gl_caption_customer ['c_mail_log']='Introduexi el seu e-mail';
$gl_caption_customer ['c_password_log']='Introduexi la seva contrasenya';
$gl_caption_customer['c_customer_menu'] = 'El meu compte';
$gl_caption_customer['c_custumer_menu_personal_data'] = 'Les meves dades personals';
$gl_caption_customer['c_custumer_menu_orders'] = 'Les meves comandes';
$gl_caption['c_custumer_menu_logout'] = 'Desconnectar';

$gl_caption['c_title_cart'] = 'Cistella de compra';
$gl_caption['c_my_cart'] = 'Cistella';
$gl_caption['c_cart'] = 'Cistella';
$gl_caption['c_add_to_cart'] = 'Afegir a la cistella';
$gl_caption['c_see_cart'] = 'Veure cistella';
$gl_caption['c_buy'] = 'Comprar';

$gl_caption_orderitem['c_title_cart'] = 'Compra';
$gl_caption_order['c_list_title'] = 'Comandes';

$gl_caption_payment['c_payment_title'] = 'Efectuant la compra';
$gl_caption_payment['c_payment_login'] = 'Identificació';
$gl_caption_payment['c_payment_summary'] = 'Resum';
$gl_caption_payment['c_payment_payment'] = 'Pagament';
$gl_caption_payment['c_payment_end'] = 'Compra finalitzada';
$gl_caption_payment ['c_cart_is_empty']='No es pot fer cap comanda, la cistella és buida';

$gl_caption_payment['c_method_title1']="Esculli el metode de pagament que desitja utilitzar.";
$gl_caption_payment['c_method_title2']="L'import total és de <strong>%s&nbsp;%s</strong> IVA inclòs.";
$gl_caption_payment['c_method_paypal']="Pagament amb PayPal";
$gl_caption_payment['c_method_account']="Pagament per transferència bancària";
$gl_caption_payment['c_method_ondelivery']="Pagament contrareembols";
$gl_caption_payment['c_method_directdebit']="Rebut bancari";
$gl_caption_payment['c_method_4b']="Targeta de crèdit (4b)";
$gl_caption_payment['c_method_lacaixa']="Targeta de crèdit";

$gl_caption_payment['c_show_paypal']="S'està efectuant el pagament a Paypal, si no comença el process automàticament faci clic aquí: ";
$gl_caption_payment['c_show_4b']="S'està efectuant el pagament a Sistema 4b, si no comença el process automàticament faci clic aquí: ";
$gl_caption_payment['c_show_lacaixa']="S'està efectuant el pagament per targeta, si no comença el process automàticament faci clic aquí: ";
$gl_caption_payment['c_show_account']="Faci un ingrès al número de compte: ";
$gl_caption_payment['c_show_account_mail']="Si us plau, faci un ingrès al número de compte: %s, indicant el número de comanda %s";
$gl_caption_payment['c_show_ondelivery']="";
$gl_caption_payment['c_show_directdebit']="";
$gl_caption_payment['c_show_end']="";
$gl_caption_payment['c_mail_order_saved']="<strong>S\'ha realitzat una comanda nova a la seva botiga on-line!</strong><br><br>En cas que aquesta comanda es confirmi rebrà una nova notificació.";

$gl_caption_payment['c_mail_order_subject_seller']='Comanda pendent nº %s / Client nº %s';
$gl_caption_payment['c_mail_subject_seller']='Resum de la comanda nº %s / Client nº %s';
$gl_caption_payment['c_mail_subject_buyer']='Resum de la comanda a %2$s nº %1$s';
$gl_caption_payment['c_text_buyer']='%s %s,';
$gl_caption_payment['c_text_buyer2']= 'Moltes gràcies per haver comprat a %s, el seu número de comanda és: <strong>%s</strong> y el seu número de client: <strong>%s</strong><br><br>Recordi que pot comprovar l\'estat de la seva comanda en l\'apartat de <a href="%s">El meu compte</a>';
$gl_caption_payment['c_text_buyer_no_session']= 'Moltes gràcies per haver comprat a %s' ;
$gl_caption_payment['c_text_buyer_no_order']= 'No s\'ha pogut realitzar cap comanda, si desitja ajuda si us plau contacti amb nosaltres' ;
$gl_caption_payment['c_text_seller']='El client: %s, acaba de realitzar una compra a %s.';
$gl_caption_payment['c_text_seller_pay_now']='El client: %s, ha efectuat el pagament pendent a %s.';

$gl_caption_payment ['c_paypal_pending_text_buyer']='La comanda queda marcada com a pendent de pagament fins que no es confirmi el pagament desde PayPal';
$gl_caption_payment ['c_paypal_pending_text_seller']='La comanda queda marcada com a pendent de pagament fins que no es confirmi el pagament desde PayPal';
$gl_caption_payment ['c_paypal_completed_text_buyer']='El pagament de Paypal ha estat confirmat';
$gl_caption_payment ['c_paypal_completed_text_seller']='El pagament de Paypal ha estat confirmat';

$gl_caption_payment ['c_4b_pending_text_buyer']='';
$gl_caption_payment ['c_4b_pending_text_seller']='';
$gl_caption_payment ['c_4b_completed_text_buyer']='El pagament amb targeta ha estat confirmat';
$gl_caption_payment ['c_4b_completed_text_seller']='El pagament amb targeta ha estat confirmat';

$gl_caption_payment ['c_lacaixa_pending_text_buyer']='';
$gl_caption_payment ['c_lacaixa_pending_text_seller']='';
$gl_caption_payment ['c_lacaixa_completed_text_buyer']='El pagament amb targeta ha estat confirmat';
$gl_caption_payment ['c_lacaixa_completed_text_seller']='El pagament amb targeta ha estat confirmat';

$gl_caption_payment ['c_account_text_buyer']='A continuació li detallem les dades per poder fer la transferència bancària';
$gl_caption_payment ['c_account_text_seller']='La comanda queda marcada com a pendent de pagament fins que no es faci l\'ingrès al número de compte';
$gl_caption_payment ['c_ondelivery_text_buyer']='';
$gl_caption_payment ['c_ondelivery_text_seller']='';
$gl_caption_payment ['c_paypal_text_buyer']='';
$gl_caption_payment ['c_paypal_text_seller']='';
$gl_caption_payment ['c_directdebit_text_buyer']='La comanda es carregarà al seu número de compte';
$gl_caption_payment ['c_directdebit_text_seller']='';

$gl_caption['c_productes_destacats']='Productes destacats';
$gl_caption['c_account_title']= 'Dades de la transferència' ;
$gl_caption['c_account_amount']= 'Import' ;
$gl_caption['c_account_concept']= 'Concepte' ;

$gl_messages_cart ['no_records']='La cistella és buida';


// traduir al català
$gl_caption['c_password_forgotten'] = 'Ha oblidat la seva contrasenya?';
$gl_caption['c_password_insert_mail'] = 'Si us plau, introdueixi la seva adreça d\'email i li enviarem una contrasenya nova al seu compte de correu.';
$gl_caption['c_password_mail_error'] = 'L\'adreça d\'email proporcionada no existeix en la nostra base de dades, si us plau, escrigui l\'adreça correctament';
$gl_caption['c_password_sent'] = 'S\ha enviat una nova contrasenya a la seva adreça d\'email';
$gl_caption['c_password_mail_subject'] = 'La seva nova contrasenya';
$gl_caption['c_password_mail_text_1'] = 'La seva nova contrasenya és';
$gl_caption['c_password_mail_text_2'] = 'Pot canviar la contrasenya en l\'apartat "El seu compte" de la nostra pàgina web';

$gl_messages_customer['registered_succesful']="El registre s'ha efectuat correctament.<br><br>En breu li notificarem per email l'activació del seu compte";
$gl_caption_customer['c_subject'] = 'Nou client registrat';
$gl_caption_customer['c_body_1'] = 'S\'ha registrat un nou client a la botiga on-line';
$gl_caption_customer['c_body_2'] = 'Aquest client està desactivat, es pot activar a l\'%sàrea de gestió de clients%s';

$gl_caption['c_product_list_view_all'] = 'Veure tots';

$gl_caption_cart['c_promcode_err_not_valid'] = 'El codi promocional no és vàlid';
$gl_caption_cart['c_promcode_err_minimum'] = 'Per aplicar el descompte la compra ha de ser superior a %s';
$gl_caption_cart['c_promcode_err_expired'] = 'El codi promocional ha expirat';
$gl_caption_cart['c_promcode_err_used'] = 'Aquest codi ja ha estat utilitzat un cop';
$gl_caption_cart['c_promcode_button'] = 'Aplicar';

$gl_messages_cart['c_no_stock_message'] = 'Aquest producte està fora d\'estoc';
$gl_messages_cart['c_max_stock_reached_message'] = 'S\'ha arribat a la quantitat màxima disponible per aquest producte';

$gl_caption['c_last_units'] = 'Últimes unitats';
$gl_caption_product['c_price_consult'] = 'Preu a consultar';

$gl_caption['c_customer_comment'] = 'Observacions';
?>