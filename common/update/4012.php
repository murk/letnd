<?php
// no es poden posar ; entre mig de la consulta ja que parteix les consultes per el ; a no ser que fem servir $update_sql en array()
if (is_table("product__configadmin")){
	
	$update_sql[]="INSERT INTO `product__configadmin` (`name`, `value`) VALUES ('is_tax_included', '1');";	
	$update_sql[]="INSERT INTO `product__configadmin` (`name`, `value`) VALUES ('is_wholesaler_tax_included', '0');";
	
	$update_sql[]="DELETE FROM `product__configadmin` WHERE name='pvd_tax_default';";
	$update_sql[]="DELETE FROM `product__configadmin` WHERE name='pvp_tax_default';";
	$update_sql[]="DELETE FROM `product__configpublic` WHERE name='is_tax_included';";
}	
?>