<?php
// menus eina
if (!defined('CLUBMOTO_MENU_PAGINA')){
	define('CLUBMOTO_MENU_PAGINA','Edició de revistes'); 
	define('CLUBMOTO_MENU_PAGINA_NEW','Entrar nova revista');
	define('CLUBMOTO_MENU_PAGINA_LIST','Llistar revistes'); 
	define('CLUBMOTO_MENU_CATEGORY','Categories de reportatges'); 
	define('CLUBMOTO_MENU_CATEGORY_NEW','Nova categoria'); 
	define('CLUBMOTO_MENU_CATEGORY_LIST','Llistar categories');
	define('CLUBMOTO_MENU_PAGINA_BIN','Paperera de reciclatge'); 

	
	define('CLUBMOTO_MENU_BANNER','Banners');
	define('CLUBMOTO_MENU_BANNER_NEW','Nou banner');
	define('CLUBMOTO_MENU_BANNER_LIST','Llistar banners');
	define('CLUBMOTO_MENU_BANNER_LIST_BIN','Paperera');
}

/*--------------- nomès castellà i català, no traduit a la resta -------------------*/


$gl_caption_pagina['c_edit_revista'] = 'Dades de la revista';
$gl_caption_pagina['c_edit_apartat'] = 'Dades del reportatge';
$gl_caption_pagina['c_edit_pagina'] = 'Dades de la pàgina';
$gl_caption_pagina['c_page_title_title'] = 'Dades de la pàgina';
$gl_caption_pagina['c_column_title1'] = 'Primera columna';
$gl_caption_pagina['c_column_title2'] = 'Segona columna';
$gl_caption_pagina['c_column_title3'] = 'Tercera columna';
$gl_caption_pagina['c_column_title4'] = 'Vídeo';

$gl_caption_pagina['c_breadcrumb_edit_revista'] = 'editar revista';
$gl_caption_pagina['c_breadcrumb_edit_apartat'] = 'editar reportatge';
$gl_caption_pagina['c_breadcrumb_edit_pagina'] = 'editar pàgina';

$gl_caption_pagina['c_subtitle_apartat'] = 'Llistar reportatges';
$gl_caption_pagina['c_subtitle_pagina'] = 'Llistar pàgines';

$gl_caption_pagina['c_is_clubone'] = 'Privat';

$gl_caption_pagina['c_is_advertisement'] = 'Publicitat';

$gl_caption_pagina['c_filter_status'] = 'Tots els estats';
$gl_caption_pagina['c_filter_category_id'] = 'Totes les categories';
$gl_caption_pagina['c_filter_paginatpl_id'] = 'Totes les plantilles';

$gl_caption_pagina['c_filter_is_clubone'] = 'Privats i ClubOne';
$gl_caption_pagina['c_is_clubone_0'] = 'Públic';
$gl_caption_pagina['c_is_clubone_1'] = 'Clubone';

$gl_caption_pagina['c_filter_is_advertisement'] = 'Tots (publicitat o no)';
$gl_caption_pagina['c_is_advertisement_0'] = 'No publicitat';
$gl_caption_pagina['c_is_advertisement_1'] = 'Publicitat';



$gl_caption_pagina['c_ordre'] = 'Ordre';
$gl_caption_pagina['c_entered'] = 'Creat';
$gl_caption_pagina['c_modified'] = 'Última Modificació';
$gl_caption_pagina['c_template'] = 'Plantilla';
$gl_caption_pagina['c_paginatpl_id'] = 'Plantilla';
$gl_caption_pagina['c_link'] = 'Link';
$gl_caption_pagina['c_pagina_file_name'] = $GLOBALS['gl_caption']['c_file_name'];
$gl_caption_pagina['c_body_supertitle'] = 'Supertítol';
$gl_caption_pagina['c_body_supertitle2'] = 'Supertítol';
$gl_caption_pagina['c_body_supertitle3'] = 'Supertítol';
$gl_caption_pagina['c_title'] = 'Títol';
$gl_caption_pagina['c_title2'] = 'Títol';
$gl_caption_pagina['c_title3'] = 'Títol';
$gl_caption_pagina['c_body_subtitle'] = 'Subtítol';
$gl_caption_pagina['c_body_subtitle2'] = 'Subtítol';
$gl_caption_pagina['c_body_subtitle3'] = 'Subtítol';
$gl_caption_pagina['c_pagina'] = 'Nom del reportatge en el menú';
$gl_caption_pagina_list['c_pagina'] = 'Nom';
$gl_caption_pagina['c_body_content'] = 'Contingut';
$gl_caption_pagina['c_body_content2'] = 'Contingut';
$gl_caption_pagina['c_body_content3'] = 'Contingut';
$gl_caption_pagina['c_prepare'] = 'En preparació';
$gl_caption_pagina['c_review'] = 'Per revisar';
$gl_caption_pagina['c_public'] = 'Públic';
$gl_caption_pagina['c_archived'] = 'Arxivat';
$gl_caption['c_status'] = 'Estat';
$gl_caption_pagina['c_pagina_id'] = 'Id';
$gl_caption_pagina['c_parent_id'] = 'Pertany a';
$gl_caption_pagina['c_level'] = '';
$gl_caption_pagina['c_blocked'] = 'Block.';

$gl_caption_pagina['c_add_revista_button'] = 'Insertar nova revista';
$gl_caption_pagina['c_add_apartat_button'] = 'Insertar nou reportatge';
$gl_caption_pagina['c_add_pagina_button'] = 'Insertar nova pàgina';
$gl_caption_pagina['c_edit_revista_button'] = 'Editar revista';
$gl_caption_pagina['c_edit_apartat_button'] = 'Editar reportatge';
$gl_caption_pagina['c_edit_pagina_button'] = 'Editar pàgina';
$gl_caption_pagina['c_view_apartat_button'] = 'Llistar reportatges';
$gl_caption_pagina['c_view_pagina_button'] = 'Llistar pàgines';
$gl_caption_pagina['c_url1_name']='Nom URL';
$gl_caption_pagina['c_url1']='URL';
$gl_caption_pagina['c_url2_name']='Nom URL(2)';
$gl_caption_pagina['c_url2']='URL(2)';
$gl_caption_pagina['c_url3_name']='Nom URL(3)';
$gl_caption_pagina['c_url3']='URL(3)';
$gl_caption_pagina['c_videoframe']='Vídeo (youtube, metacafe...)';
$gl_caption_pagina['c_file']='Arxius';
$gl_caption_pagina['c_category_id']='Categoria';
$gl_caption_image['c_main_image'] = 'Fons pàgina';
$gl_caption_image['c_main_image2'] = 'Imatge llistat';

$gl_caption['c_last_listing']='Llistat anterior';


$gl_caption_category['c_ordre']='Ordre';
$gl_caption_category['c_category']='Categoria';
$gl_caption_category['c_is_clubone'] = 'Menú clubone';

$gl_messages['c_restore_items'] = "Els elements marcats no s'han podut recuperar ja que s'han de recuperar el reportatge o la revista a la que pertanyen";



/*banners*/
$gl_caption_banner['c_banner_id']='';
$gl_caption_banner['c_ordre']='Ordre';
$gl_caption_banner['c_url1']='Enllaç';
$gl_caption_banner['c_url1_name']='Text a l\'enllaç';
$gl_caption_banner['c_url1_target']='Destí';
$gl_caption_banner['c_url1_target__blank']='Una nova finestra';
$gl_caption_banner['c_url1_target__self']='La mateixa finestra';
$gl_caption_banner['c_file']='Imatge banner <br>(960x92 - 248x122)';
$gl_caption_banner['c_banner']='Banner';

?>