<?php

/* 30-7-13 
  Actualitzacio tarifes enviament
  
  Configuracio per les imatges a variations



 */
	
if (is_table("product__configadmin")){
	
	$france_rate = Db::get_first("SELECT value FROM product__configadmin WHERE name = 'send_rate_france'");
	
	$update_sql[]="INSERT INTO `product__configadmin` (`name`, `value`) VALUES ('is_brand_rate_on', '0');";	
	
	$update_sql[]="INSERT INTO `product__configadmin` (`name`, `value`) VALUES ('send_rate_andorra', '".$france_rate."');";	
	
	$update_sql[]="ALTER TABLE `product__brand`
	ADD COLUMN `send_rate_spain` DECIMAL(7,2) NOT NULL DEFAULT '0' AFTER `ordre`,
	ADD COLUMN `send_rate_france` DECIMAL(7,2) NOT NULL DEFAULT '0' AFTER `send_rate_spain`,
	ADD COLUMN `send_rate_andorra` DECIMAL(7,2) NOT NULL DEFAULT '0' AFTER `send_rate_france`,
	ADD COLUMN `send_rate_world` DECIMAL(7,2) NOT NULL DEFAULT '0' AFTER `send_rate_andorra`,
	ADD COLUMN `minimum_buy_free_send` DECIMAL(9,2) NOT NULL DEFAULT '0' AFTER `send_rate_world`;";	
}	

?>