<?
/**
 * MuseumoliVisita
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2011
 * @version $Id$
 * @access public
 */
class MuseumoliVisita extends Module{
	var $category_menu_id=0, $order_by = '';
	function __construct(){
		parent::__construct();
		$this->parent = 'MuseumoliVisita';
	}
	/* funció que es crida per fer una acció del modul automatica o desde un altre modul ( list_records, show_form ) etc...*/
	function do_action($action = ''){
		switch ($this->parent){		
			case 'MuseumoliVisita':		
			case 'MuseumoliVisitataller':	
				$this->unset_field('inscripcio');
				$this->unset_field('tipus_curs');
				$this->unset_field('tipus_colaborador');
				$this->unset_field('year');
				$this->order_by = 'ordre ASC, entered DESC';
			break;
			case 'MuseumoliCurs':
				$this->unset_field('visitant_id');
				$this->unset_field('content_list');	
				$this->unset_field('reserva');
				$this->unset_field('tipus_colaborador');
				$this->unset_field('year');
				$this->order_by = 'ordre ASC, entered DESC';
			break;	
			case 'MuseumoliExposicio':	
				$this->swap_fields('content_list','duracio');
				$this->unset_field('visitant_id');
				$this->unset_field('content_list');
				$this->unset_field('mes_info');
				$this->unset_field('reserva');
				$this->unset_field('inscripcio');
				$this->unset_field('tipus_curs');
				$this->unset_field('tipus_colaborador');
				$this->unset_field('year');
				$this->order_by = 'ordre ASC, entered DESC';
			break;		
			case 'MuseumoliColaborador':	
				$this->swap_fields('content_list','duracio');
				$this->unset_field('visitant_id');
				$this->unset_field('content_list');
				$this->unset_field('duracio');
				$this->unset_field('datatext');
				$this->unset_field('nivell_educatiu');
				$this->unset_field('reserva');
				$this->unset_field('inscripcio');
				$this->unset_field('tipus_curs');
				$this->unset_field('preu_alumne');
				$this->unset_field('preu');
				$this->unset_field('ordre');
				$this->order_by = 'visita ASC';
			break;	
		}
		// el camp parent_module nomès el tinc quan creo un registre nou
		// un cop creat el registre no es modifica mai aquest camp, el registre pertany sempre a aquella secció
	    if ($this->action=='add_record') {
			$this->add_field('parent_module'); 
			$this->set_field ('parent_module','override_save_value',"'" . $this->parent . "'"); // sempre sobreescric el parent_module
		}
		return parent::do_action($action);
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function get_records()
	{
	    $listing = new ListRecords($this);
		$listing->call('records_walk','content,content_list');
		$listing->condition = "parent_module = '" . $this->parent . "'";
		$listing->add_filter('status');
		$listing->order_by = $this->order_by;	
	    return $listing->list_records();
	}
	function records_walk($content=false,$content_list=false){	
			$ret['content']=$content_list?$content_list:add_dots('content', $content);
		return $ret;
	}
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{
	    // si no hi ha cap categoria faig un redirect a entrar categoria nova
		/*if (!Db::get_first("SELECT count(*) FROM tecnomix__category WHERE parent_module = '".$this->parent."category' LIMIT 0,1")){
			$_SESSION['message'] = $this->messages['no_category'];
			redirect('/admin/?menu_id=' . $this->category_menu_id);
		}*/
		$this->set_field('entered','default_value',now(true));
		$show = new ShowForm($this);
	    return $show->show_form();
	}

	function save_rows()
	{
	    $save_rows = new SaveRows($this);
		if (($this->action=='save_rows_bin_selected' || $this->action=='save_rows_delete_selected')
			&&
			(in_array ('12', $save_rows->id) || in_array ('19', $save_rows->id))) {
			$GLOBALS['gl_message'] = $this->messages['no_delete_taller'];
			return;
		}
	    $save_rows->save();
	}

	function write_record()
	{
	    $writerec = new SaveRows($this);
		if (($this->action=='bin_record' || $this->action=='delete_record')
			&& 
			($writerec->id == '12' || $writerec->id == '19')) {
			$GLOBALS['gl_message'] = $this->messages['no_delete_taller'];
			return;
		}
		$writerec->save();
	}

	function manage_images()
	{
	    $image_manager = new ImageManager($this);
	    $image_manager->execute();
	}
}
?>