<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<base href="<?= $url ?>">
	<title><?= $page_title ?></title>

	<?include('newsletter/message_templates/styles.tpl')?>

	<style type="text/css">

		/* PRODUCT CISTELLA */

		.mobile {
			display: none;
		}
		.table-products {
			width: 100%;
		}

		.table-products p {
			margin: 0 !important;
			padding: 0 !important;
		}

		.table-products td, .table-products th {
		}

		.table-products tr.last {
			border-bottom: 0;
		}

		.table-products tr {
		}

		.table-products th {
			padding: 8px 6px;
			text-align: right;
			color: <?= $text_color ?>;
			font-weight: 400;
			background-color: <?= $highlight_background ?>;
		}

		.table-products td {
			padding: 20px 10px;
			border-bottom: 1px dotted <?= $line_color ?>;
		}

		.table-products .td-image {
			padding: 20px 0;
			line-height: 0;
			font-size: 0;
			vertical-align: top;
			text-align: center;
		}

		.table-products .td-title a.title {
			padding: 0 0 6px 0;
			display: block;
			font-weight: bold;
			text-decoration: none;
			font-size: 15px;
			color: <?= $highlight_color ?>;
		}

		.table-products .td-title a.title:hover {
			text-decoration: underline;
		}

		.table-products th.th-left {
			text-align: left;
		}

		.table-products td.td-price {
			text-align: right;
			white-space: nowrap;
		}
		.table-products .td-downloads {

		}
		.table-products .downloads a {
			text-decoration: none;
			font-size: 15px;
			color: <?= $highlight_color ?>;
		}
		.table-products .downloads a:hover {
			text-decoration: underline;
		}

		.table-products .old-price {
			text-decoration: line-through;
			margin-right: 10px;
			color: <?= $highlight_color ?>;
		}

		.table-products td.td-price strong {
			color: <?= $highlight_color ?>;
			font-weight: normal;
		}

		.table-products td.td-cart {
			text-align: right;
		}

		.table-products.totals {
			color: <?= $text_color ?>;
			margin-top: 30px;
		}

		.table-products.totals tr {
			border-bottom: none;
			background-color: <?= $highlight_background ?>;
		}

		.table-products.totals th {
			text-align: left;
			font-weight: 400;
			background-color: <?= $highlight_background ?>;
		}

		.table-products.totals td {
			font-weight: 300;
			text-align: right;
			padding-top: 10px;
			padding-bottom: 10px;
			color: <?= $text_color ?>;
			border-bottom: 0;
		}

		.table-products.totals tr.tr-total-price td,
		.table-products.totals tr.tr-total-price th {
			padding-top: 10px;
			font-weight: 700;
		}

		.table-products.totals tr.tr-promcode th,
		.table-products.totals tr.tr-promcode td {
			padding-top: 10px;
			font-weight: 400;
			color: <?= $highlight_color ?>;
		}

		.table-products.totals tr.tr-promcode td strong {
			font-weight: 700;
			color: <?= $highlight_color ?>;
		}

		.table-products.totals tr.all th,
		.table-products.totals tr.all td {
			font-weight: 700;
			font-size: 15px;
		}

		.promcode_form {
			color: <?= $highlight_color ?>;
			font-weight: bold;
			font-size: 14px;
			line-height: 20px;
			text-align: right;
			padding-top: 20px;
			margin-bottom: 20px;
			width: 100%;
			overflow: hidden;
		}
         .customer-comment {
	        margin-top: 50px;
			margin-bottom: 20px;
         }

         .customer-comment > div {
	         margin-bottom: 20px;
	         font-weight: bold;
         }

		/* FI PRODUCT CISTELLA */

		/*

		El margin left no fuciona als mails

		*/

		.table-products.totals {
			width: 100%;
			margin-left: 0;
		}

		/*

		Copiar un a un a partir d'aquí

		*/
		/* ORDER */

		div.order {
			margin-bottom: 38px;
		}

		/* FI ORDER */

		/* CUSTOMER - CONTACT FORM  */
		.form {
			border-spacing: 0;
			padding: 0;
			width: 100%;
		}

		.form td {
			padding: 6px 8px 6px 0;
		}

		.form th {
			padding: 6px 8px 6px 0;
			vertical-align: top;
			text-align: left;
			width: 240px;
			font-size: 15px;
			line-height: 21px;
			font-weight: normal;
		}
		.form .collect th {
			font-size: 18px;
			font-weight: bold;
		}
		.form .collect td {
			font-size: 18px;
		}

		h3 {
			border-bottom: 1px dotted <?= $line_color ?>;
			color: <?= $highlight_color ?>;
			font-size: 20px;
			font-weight: 700;
			margin-bottom: 35px;
			margin-top: 50px;
			padding-bottom: 10px;
		}

		/* PASSWORD  */
		.notice {
			padding: 30px 0 20px;
		}

		/* RESPOSTA AUTOMATICA  */
		.autoreply {
			font-size: 18px;
			padding: 0 0 20px;
			border-bottom: 1px dotted <?= $line_color ?>;
		}
		.autoreply p {
			font-size: 17px;
			margin: 0;
		}

	</style>
</head>
<body>

	<table id="letnd-table-content">
		<tr>
			<td id="letnd-td-content" align="center">

				<? include( CLIENT_PATH . '/templates/newsletter/message_templates/header.tpl' ) ?>

				<table id="letnd-content" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td valign="top" class="letnd-content">
							<?= $content ?>
						</td>
					</tr>
				</table>

				<? include( CLIENT_PATH . '/templates/newsletter/message_templates/footer.tpl' ) ?>

			</td>
		</tr>
	</table>

</body>
</html>