<?

/* 25-02-13 
  Afegir taules idiomes clients
 * 
 * 

 */

if (!is_table('client__configpublic')) {	
	
	$update_sql[]= "
		CREATE TABLE `client__configpublic` (
	`name` VARCHAR(50) NOT NULL DEFAULT '' COLLATE 'utf8_spanish2_ci',
	`value` VARCHAR(100) NOT NULL COLLATE 'utf8_spanish2_ci',
	`page_text_id` VARCHAR(100) NOT NULL DEFAULT '' COLLATE 'utf8_spanish2_ci',
	`type` ENUM('text','htmlbasic','html','htmlfull') NOT NULL DEFAULT 'text' COLLATE 'utf8_spanish2_ci',
	PRIMARY KEY (`name`)
	)
	COLLATE='utf8_spanish2_ci'
	ENGINE=MyISAM
	ROW_FORMAT=DYNAMIC;";
	
	$update_sql[]= "
		CREATE TABLE `client__configpublic_language` (
	`name` VARCHAR(50) NOT NULL DEFAULT '' COLLATE 'utf8_spanish2_ci',
	`value` TEXT NOT NULL COLLATE 'utf8_spanish2_ci',
	`language` CHAR(3) NOT NULL DEFAULT '' COLLATE 'utf8_spanish2_ci'
	)
	COLLATE='utf8_spanish2_ci'
	ENGINE=MyISAM
	ROW_FORMAT=DYNAMIC;";
	
}


?>