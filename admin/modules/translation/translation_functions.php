<?php
function erase_language( $string ) {
	$string = substr( $string, 0, - 9 );

	return $string;
}

function state_name( $state ) {
	switch ( $state ) {
		case "1":
			$estat = "Pendent";
			break;
		case "2":
			$estat = "En traducció";
			break;
		case "3":
			$estat = "Finalitzat";
			break;
		case "4":
			$estat = "Error";
			break;
	} // switch
	return $estat;
}

function get_translation_text( $translation_text ) {
	// transformo el text a traduir, elimino les comes i retorno els resultats a un array.
	$translation_text = explode( ",", $translation_text );

	return $translation_text;
}

function get_number_of_languages( $languages ) {
	// aquesta funcio el que fa es retornar el numero total del array dels idiomes a traduir
	$numlanguages = 0;
	for ( $i = 0; $i < count( $languages ); $i ++ ) {
		$numlanguages = $numlanguages + 1;
	}

	return $numlanguages;
}

function send_confirmation_mail_ok( $comercialname, $mailclient ) {
	$mail = get_mailer();
	$mail->AddAddress( "info@letnd.com" );
	$mail->Subject = "9 Sistema - Traduccións finalitzades -";//asunto
	$mail->Body    = "<DIV><FONT face=Arial size=2>" . $comercialname . "</FONT></DIV>
<DIV><FONT face=Arial size=2></FONT>&nbsp;</DIV>
<DIV><FONT face=Arial size=2>Informem que li han arribat traduccions
<STRONG><FONT color=#808000 size=3>correctament</FONT></STRONG>.</FONT></DIV>
<DIV><FONT face=Arial size=2></FONT>&nbsp;</DIV>
<DIV><FONT face=Arial size=2>Att.&nbsp;9 Sistema.</FONT></DIV>
<DIV><FONT face=Arial size=2></FONT>&nbsp;</DIV>
<DIV><FONT face=Arial size=1>9 Sistema Serveis Informàtics 2005
S.L.</FONT></DIV>
<DIV><FONT face=Arial size=1>C/ Juli Garreta 25C 2on a(17002) Girona</FONT></DIV>
<DIV><FONT face=Arial size=1>Telf. 777 888 999 / 665 731 656 / 661 333
289</FONT></DIV>";
	$mail->AltBody = "Informar que té traduccions finalitzades.";
	send_mail( $mail, false );
}

function send_error_mail_translaters( $mailcompany, $translation_id ) {
	$mail = get_mailer();
	$mail->AddAddress( "info@letnd.com" );
	$mail->Subject = "9 Sistema - Traducció erronea -";//asunto
	$mail->Body    = "<DIV><FONT face=Arial size=2>Informem que han intentat finatlizar una traducció
que no s'havia completat. Aquesta s'ha posat amb un estat: <FONT color=#800000
size=3><STRONG>Error</STRONG></FONT></FONT></DIV>
<DIV><FONT face=Arial size=2></FONT>&nbsp;</DIV>
<DIV><FONT face=Arial size=2>Aquest traducció és la nº: <STRONG><FONT
size=3>" . $translation_id . "</FONT></STRONG></FONT></DIV>
<DIV><STRONG><FONT face=Arial></FONT></STRONG>&nbsp;</DIV>
<DIV><FONT face=Arial size=2>Preguem sol·locionin aquesta traducció o es possin
en contacte amb l'equip de 9 Sistema.</FONT></DIV>
<DIV><FONT face=Arial size=2></FONT>&nbsp;</DIV>
<DIV><FONT face=Arial size=2>Att.&nbsp;9 Sistema.</FONT></DIV>
<DIV><FONT face=Arial size=2></FONT>&nbsp;</DIV>
<DIV><FONT face=Arial size=1>9 Sistema Serveis Informàtics 2005
S.L.</FONT></DIV>
<DIV><FONT face=Arial size=1>C/ Juli Garreta 25C 2on a(17002) Girona</FONT></DIV>
<DIV><FONT face=Arial size=1>Telf. 777 888 999 / 665 731 656 / 661 333
289</FONT></DIV>";
	//Definimos AltBody por si el destinatario del correo no admite email con formato html
	$mail->AltBody = "Informem que la traducció: " . $translation_id . " ha generat un error.";
	send_mail( $mail, false );
}

function connect_client( $dbhost, $dbname, $dbuname, $dbpass ) {
	$dbtype = "mysql";
	// Start connection
	Db::connect( $dbhost, $dbuname, $dbpass, $dbname );
}

function check_correct_translate( $languages, $totallanguages ) {
	// Aquesta funcio el que fa es assegurarce que hi hagi algo escrit a tots els idiomes a traduir
	$error = 0;
	for ( $i = 0; $i < $totallanguages; $i ++ ) {
		if ( $_POST[ $languages[ $i ] ] == "" ) {
			$error = 1;
		}
	}

	return $error;
}

/*
function tell_language ($id_language)
{
    global $languageoriginal;
    switch ($id_language) {
        case "1": $languageoriginal = "cat";
            break;
        case "2": $languageoriginal = "spa";
            break;
        case "3": $languageoriginal = "eng";
            break;
        case "4": $languageoriginal = "ale";
            break;
        case "5": $languageoriginal = "fra";
            break;
    } // switch
    return $languageoriginal;
}

function nameoflanguages ($numtotallanguages, $vlanguages)
{
    // aquesta funcio em diu quin es el nom dels idiomes a traduir, extraient-les del array es a dir:
    // vlangues[1]=cat i em retorna la variable $alanguage amb un array amb el nom dels idiomes, per els traductors.
    $numtotallanguages = $numtotallanguages;
    $vlanguages = $vlanguages;
    for ($i = 0;$i <= $numtotallanguages-1;$i++) {
        $namelanguage[$i] = tell_language ($vlanguages[$i]);
    }
    return $namelanguage;
}
*/

?>