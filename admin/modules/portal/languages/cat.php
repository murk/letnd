<?php
// menus eina
if (!defined('PORTAL_MENU_PORTAL')){
define('PORTAL_MENU_CUSTOMER','Clients');
define('PORTAL_MENU_NEW','Nou Client');
define('PORTAL_MENU_LIST','Llistar clients');
define('PORTAL_MENU_LIST_BIN','Paperera');
define('PORTAL_MENU_FAMILY','Famílias');
define('PORTAL_MENU_FAMILY_NEW','Crear família');
define('PORTAL_MENU_FAMILY_LIST','Listar famílas');
define('PORTAL_MENU_FAMILY_LIST_BIN','Papelera');
define('PORTAL_MENU_CATEGORY','Categories');
define('PORTAL_MENU_CATEGORY_NEW','Nova categoria');
define('PORTAL_MENU_CATEGORY_LIST','Llistar categories');
define('PORTAL_MENU_CATEGORY_LIST_BIN','Paperera');
}

$gl_caption_customer['c_customer_private']='Nom privat';
$gl_caption_customer['c_home']='Destacat';
$gl_caption_customer['c_family_id'] = 'Família';
$gl_caption_customer['c_subfamily_id'] = 'Subfamília';
$gl_caption_customer['c_telephone'] = 'Teléfon';
$gl_caption_customer['c_telephone_mobile'] = 'Teléfon Mòbil';
$gl_caption_customer['c_fax'] = 'Fax';
$gl_caption_customer['c_mail'] = 'E-mail';
$gl_caption_customer['c_url'] = 'Url';
$gl_caption_customer['c_adress'] = 'Direcció';
$gl_caption_customer['c_price_id'] = 'Rango de preu';
$gl_caption_customer['c_customer_title'] = 'Nom del client';
$gl_caption_customer['c_customer_description'] = 'Descripció';
$gl_caption_customer['c_data']='Dades del client';
$gl_caption_customer['c_general_data']='Dades generals';
$gl_caption_customer['c_descriptions']='Descripcions';
$gl_caption_customer['c_description']='Descripció';
$gl_caption_customer['c_status'] = 'Estat';
$gl_caption_customer['c_prepare'] = 'En preparació';
$gl_caption_customer['c_review'] = 'Per revisar';
$gl_caption_customer['c_onsale'] = 'Per vendre';
$gl_caption_customer['c_archived'] = 'Arxivt';
$gl_caption_customer['c_owner_name'] = 'Nom del gerent';
$gl_caption_customer['c_owner_surnames'] = 'Cognoms del gerent';
$gl_caption_customer['c_owner_phone'] = 'Teléfon del gerent';
$gl_caption_customer['c_file']='Arxius';
$gl_caption_customer['c_edit_map'] = 'Mapa';
$gl_caption_customer['c_edit_data'] = 'Editar Client';
$gl_caption_customer['c_save_map'] = 'Guardar ubicació';
$gl_caption_customer['c_click_map'] = 'Fes click a un punt del mapa per establir la situació';
$gl_caption_customer['c_form_map_title'] = 'Ubicació del client';
$gl_caption_customer['c_country_id']='País';
$gl_caption_customer['c_municipi_id']='Municipi';
$gl_caption_customer['c_provincia_id']='Provincia';
$gl_caption_customer['c_comarca_id']='Comarca';
$gl_caption_customer['c_schedule']='Horaris';


$gl_caption_customer['c_data_customer']='Dades publiques del client';
$gl_caption_customer['c_search_adress']='Buscar direcció';
$gl_caption_customer['c_latitude']='latitud';
$gl_caption_customer['c_longitude']='longitud';
$gl_caption_customer['c_found_direction']='direcció trobada';

$gl_messages['point_saved'] = 'Nova ubicació guardada correctament';
//Categories
$gl_caption_customer['c_category_id']=$gl_caption_category['c_category_id']='Categoria';
$gl_caption_category['c_category_name']='Nom de la categoria';


//captions comuns
$gl_caption['c_observations'] = 'Observaciones';
$gl_caption['c_family_description']='Descripción de la família';
$gl_caption['c_ordre'] = 'Orden';
$gl_caption['c_check_parent_id']='Escoge família';
$gl_caption['c_subfamily_id_format']='#%s#%s';

//apartat families
$gl_caption_family['c_parent_id']='Familia a la que pertenece';
$gl_caption_family['c_family']='Nombre de la familia';

?>