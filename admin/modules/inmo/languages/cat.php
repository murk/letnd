<?php
global $gl_caption_tv, $gl_messages_tv;
// menus eina
if (!defined('INMO_MENU_PROPERTY')){
	define('INMO_MENU_PROPERTY', 'Immobles');
	define('INMO_MENU_PROPERTY_NEW', 'Entrar nou immoble');
	define('INMO_MENU_PROPERTY_LIST', 'Llistar immobles');
	define('INMO_MENU_SEARCH_FORM', 'Buscar i Editar immobles');
	define('INMO_MENU_PROPERTY_BIN', 'Paperera de reciclatge');
	define('INMO_MENU_CATEGORY', 'Tipus d\'immobles');
	define('INMO_MENU_CATEGORY_NEW', 'Entrar nou tipus');
	define('INMO_MENU_CATEGORY_LIST', 'Llistar tipus');
	define('INMO_MENU_TRANSLATION', 'Traduccions');
	define('INMO_MENU_PROPERTY_TRANSLATION_LIST', 'Llistar immobles per traduir');
	define('INMO_MENU_CATEGORY_TRANSLATION_LIST', 'Llistar categories per traduir');
	define('INMO_MENU_CONFIG', 'Configuració Immobles');
	define('INMO_MENU_CONFIGADMIN_LIST', 'Configuració part privada');
	define('INMO_MENU_CONFIGPUBLIC_LIST', 'Configuració part pública');
	define('INMO_MENU_CONFIGADMIN', 'Configuració part privada');
	define('INMO_MENU_CONFIGPUBLIC', 'Configuració part pública');
	define('INMO_MENU_ZONE', 'Zones');
	define('INMO_MENU_ZONE_NEW', 'Entrar nova zona');
	define('INMO_MENU_ZONE_LIST', 'Llistar zones');
	define('INMO_MENU_ZONE_BIN', 'Paperera de reciclatge');
	define('INMO_MENU_TV', 'Aparadors virtuals');
	define('INMO_MENU_TV_LIST', 'Sel·leccionar immobles');
	define('INMO_MENU_TV_LIST_SELECTED', 'Immobles a l\'aparador');
	define('INMO_MENU_TV_LIST_DOWNLOAD', 'Descarregar aparador');
	define('INMO_MENU_PROPERTY_MAP_LIST', 'Buscar per mapa');		
	define('INMO_MENU_ADJECTIVE_NEW', 'Entrar nou adjectiu');
	define('INMO_MENU_ADJECTIVE_LIST', 'Listar adjectius');
	
	define('BOOKING_MENU_BOOK','Reserves');
	define('BOOKING_MENU_BOOK_NEW','Entrar nova reserva');
	define('BOOKING_MENU_BOOK_LIST','Llistar reserves');
	define('BOOKING_MENU_BOOK_LIST_PAST','Reserves finalitzades');
	define('BOOKING_MENU_BOOK_SEARCH','Disponibilitat');
	
	define('BOOKING_MENU_EXTRA','Serveis');
	define('BOOKING_MENU_EXTRA_NEW','Entrar nou servei');
	define('BOOKING_MENU_EXTRA_LIST','Llistar serveis');
	define('BOOKING_MENU_EXTRA_LIST_BIN','Paperera de reciclatge');
	
	define('BOOKING_MENU_SEASON','Temporades');
	define('BOOKING_MENU_SEASON_NEW','Entrar nova temporada');
	define('BOOKING_MENU_SEASON_LIST','Llistar temporades');

	define ('TITLE_COMARCA_ADMIN','Per zona: ');
	define('PROPERTY_AUTO_REF', 'Automàtica');

	define('BOOKING_MENU_PROMCODE','Codi promocional');
	define('BOOKING_MENU_PROMCODE_NEW','Crear codi');
	define('BOOKING_MENU_PROMCODE_LIST','Llistar codi');
	define('BOOKING_MENU_PROMCODE_LIST_BIN','Paperera');

	define('BOOKING_MENU_POLICE','Registre Mossos');
	define('BOOKING_MENU_POLICE_LIST','Llistar reserves');

	define('INMO_MENU_PORTAL', 'Portals');
	define('INMO_MENU_PORTAL_LIST', 'Seleccionar immobles');
	define('INMO_MENU_PORTAL_LIST_SELECTED', 'Immobles seleccionats');
}
// sobreescrits missatges demand custumer
if (isset($gl_messages)) {
	$gl_messages_demand = $gl_messages;
	$gl_messages_custumer = $gl_messages;
}
// sobreescrits end

// sobreescrits missatges property
$gl_messages_property['added']='L\'immoble s\'ha afegit correctament. A continuació pots insertar les imatges corresponents a la nova finca';
$gl_messages_property['saved']='L\'immoble ha estat modificat correctament';
$gl_messages_property['no_records']='No hi ha cap immoble en aquest llistat';
$gl_messages_property['no_records_bin']='No hi ha cap immoble a la paperera';
$gl_messages_property['list_saved']='Els canvis en els immobles s\'han efectuat correctament';
$gl_messages_property['list_not_saved']='No s\'ha modificat cap immoble, no s\'ha detectat cap canvi';
$gl_messages_property['list_deleted']='Els immobles sel·leccionats han estat esborrats';
$gl_messages_property['list_bined']='Els immobles s\'han enviat a la paperera de reciclatge';
$gl_messages_property['list_restored']='Els immobles s\'han recuperat correctament';
// sobreescrits end

$gl_messages_category['related_not_activated'] = 'Aquest tipus no es pot desactivar, conté immobles relacionats';
$gl_messages_category['relateds_not_activated'] = 'Els tipus d\'immobles marcats no es poden desactivar, contenen immobles relacionats';

// captions seccio
$gl_caption_demand['c_form_title'] = 'Dades de la demanda del client: ';

$gl_caption['c_property_button'] = 'Veure immobles';
$gl_caption['c_view_property'] = 'Veure immoble';

$gl_caption_property['c_move_to_category_id'] = '[tipus d\'immoble]';
$gl_caption_property['c_move_to_category_id2'] = 'Canvia immobles seleccionats a:';
$gl_caption_property['c_move_to_zone_id'] = '[zona]';
$gl_caption_property['c_move_to_zone_id2'] = 'Canvia immobles seleccionats a:';
$gl_caption_property['c_move_to_zone_id3'] = '[cap zona]';

$gl_caption_category['c_ordre'] = 'Ordre';
$gl_caption_category['c_ref_code'] = 'Codi referència immobles';
$gl_caption_property['c_room'] = 'Habitacions';
$gl_caption_property['c_bathroom'] = 'Banys';
$gl_caption_property['c_wc'] = 'Lavabos';
$gl_caption_property['c_laundry'] = 'Rentador';
$gl_caption_property['c_parking'] = 'Pàrquing';
$gl_caption_property['c_garage'] = 'Garatge';
$gl_caption_property['c_garage_area'] = 'superfície';
$gl_caption_property['c_storage'] = 'Traster';
$gl_caption_property['c_storage_area'] = 'superfície';
$gl_caption_property['c_terrace'] = 'Terrassa';
$gl_caption_property['c_terrace_area'] = 'superfície';
$gl_caption_property['c_garden'] = 'Jardí';
$gl_caption_property['c_garden_area'] = 'superfície';
$gl_caption_property['c_swimmingpool'] = 'Piscina';
$gl_caption_property['c_swimmingpool_area'] = 'superfície';
$gl_caption_property['c_citygas'] = 'Gas ciutat';
$gl_caption_property['c_centralheating'] = 'Calefacció';
$gl_caption_property['c_airconditioned'] = 'Aire condicionat';
$gl_caption_property['c_ref'] = 'Ref.';
$gl_caption_property['c_ref_collaborator'] = 'Ref. colaborador';
$gl_caption_property['c_ref_cadastre']='Ref. Cadastral';
$gl_caption_property['c_gaining']='Captació';
$gl_caption_property['c_place_id']='Oficina';
$gl_caption_property['c_cedula']='Cèdula Habitabilitat';
$gl_caption_property['c_registerpropietat']='Registre de la propietat';
$gl_caption_property['c_numregister']='Número';
$gl_caption_property['c_locationregister']='de';
$gl_caption_property['c_tomregister']='tom';
$gl_caption_property['c_bookregister']='llibre';
$gl_caption_property['c_pageregister']='foli';
$gl_caption_property['c_numberpropertyregister']='finca núm';
$gl_caption_property['c_inscriptionregister']='inscripció';
$gl_caption_property['c_property_private'] = 'Nom privat';
$gl_caption['c_category_id'] = 'Tipus';
$gl_caption_property['c_floor_space'] = 'Superfície';
$gl_caption_property['c_land'] = 'Terreny';
$gl_caption_property['c_land_units'] = 'Unitats';
$gl_caption_property['c_m2'] = 'm2';
$gl_caption_property['c_ha'] = 'hectàrees';
$gl_caption_property['c_country_id'] = 'País';
$gl_caption_property['c_price_private'] = 'Preu propietari';
$gl_caption_property['c_price'] = 'Preu';
$gl_caption_property['c_price_m2'] = 'Preu/m2';
$gl_caption_property['c_zoom'] = 'Zoom';
$gl_caption_property['c_price_consult'] = 'preu a consultar';
$gl_caption_property['c_price_consult_1'] = 'Preu a consultar';
$gl_caption_property['c_price_history_link'] = 'Historial de preus';
$gl_caption_property['c_owner_id'] = '';
$gl_caption_property['c_status'] = 'Estat';
$gl_caption_property['c_prepare'] = 'En preparació';
$gl_caption_property['c_review'] = 'Revisar';
$gl_caption_property['c_onsale'] = 'Públic';
$gl_caption_property['c_sold'] = 'Venut';
$gl_caption_property['c_archived'] = 'Arxivat';
$gl_caption_property['c_reserved'] = 'Reservat';
$gl_caption_property['c_zone'] = 'Zona'; // per la part publica
$gl_caption_property['c_zone_id'] = 'Zona';
$gl_caption_property['c_zone_id_caption'] = '[cap zona]';
$gl_caption_property['c_provincia_id'] = 'Província';
$gl_caption_property['c_comarca_id'] = 'Comarca';
$gl_caption_property['c_municipi_id'] = $gl_caption_property['c_municipi'] = 'Municipi';
$gl_caption_property['c_property'] = 'Nom de l\'immoble';
$gl_caption_property['c_property_title'] = 'Descripció curta';
$gl_caption_property['c_entered'] = 'Creada';
$gl_caption_property['c_ref_number'] = '';
$gl_caption_property['c_home'] = 'Destacat';
$gl_caption_property['c_offer'] = 'Oferta';
$gl_caption_property['c_data'] = 'Dades de l\'immoble';
$gl_caption_property['c_descriptions'] = 'Descripcions';
$gl_caption_property['c_details'] = 'Detalls';
$gl_caption_property['c_description'] = 'Descripció';
$gl_caption_property['c_details_temp'] = 'Lloguer turístic';
$gl_caption_property['c_0'] = '';
$gl_caption_property['c_1'] = '1';
$gl_caption_property['c_2'] = '2';
$gl_caption_property['c_3'] = '3';
$gl_caption_property['c_4'] = '4';
$gl_caption_property['c_5'] = '5';
$gl_caption_property['c_6'] = '6';
$gl_caption_property['c_7'] = '7';
$gl_caption_property['c_8'] = '8';
$gl_caption_property['c_9'] = '9';
$gl_caption_property['c_10'] = '10';
$gl_caption_property['c_11'] = '11';
$gl_caption_property['c_12'] = '12';
$gl_caption_property['c_13'] = '13';
$gl_caption_property['c_14'] = '14';
$gl_caption_property['c_15'] = '15';
$gl_caption_property['c_16'] = '16';
$gl_caption_property['c_17'] = '17';
$gl_caption_property['c_18'] = '18';
$gl_caption_property['c_19'] = '19';
$gl_caption_property['c_20'] = '20';
$gl_caption_property['c_tipus'] = 'Operació';
$gl_caption_property['c_tipus_sell'] = 'Venda';
$gl_caption_property['c_tipus_rent'] = 'Lloguer';
$gl_caption_property['c_filter_status'] = 'Tots els estats';
$gl_caption_property['c_filter_category_id'] = 'Tots els tipus';
$gl_caption_property['c_filter_tipus'] = 'Totes les operacions';
$gl_caption_property['c_address'] = 'Adreça';
$gl_caption_property['c_adress'] = 'Carrer';
$gl_caption_property['c_numstreet'] = 'Número';
$gl_caption_property['c_block'] = 'Bloc';
$gl_caption_property['c_flat'] = 'Pis';
$gl_caption_property['c_door'] = 'Porta';
$gl_caption_property['c_observations'] = 'Observacions';
$gl_caption_property['c_data_private'] = 'Dades privades';
$gl_caption_property['c_data_public'] = 'Dades públiques';
$gl_caption_property['c_tipus2'] = 'Tipus d\'obra';
$gl_caption_property['c_elevator'] = 'Ascensor';
$gl_caption_property['c_balcony'] = 'Balcó';
$gl_caption_property['c_new'] = 'Obra nova';
$gl_caption_property['c_second'] = '2º mà';
$gl_caption_property['c_furniture'] = 'Moblat';
$gl_caption_property['c_file'] = 'Documents';
$gl_caption_property['c_video'] = $gl_caption_property['c_videoframe'] = 'Video';
$gl_caption_property['c_edit_custumer'] = 'Propietaris';
$gl_caption_property['c_edit_custumer_button'] = 'Editar propietaris';

$gl_caption_property['c_mls']='MLS';
$gl_caption_property['c_share_mls']='Compartir per MLS';
$gl_caption_property['c_comission'] = 'Comissió MLS';

$gl_caption_property['c_filter_activated'] = 'Tots els clients';
$gl_caption_property['c_activated_0'] = 'Desactivats';
$gl_caption_property['c_activated_1'] = 'Activats';

$gl_caption_property['c_efficiency_letter'] = 'Lletra';
$gl_caption_property['c_efficiency_number'] = 'Energia';
$gl_caption_property['c_efficiency_number2'] = 'Emissió';
$gl_caption_property['c_sea_distance'] = 'Distància al mar';

$gl_caption_property['c_expenses'] = 'Despeses';
$gl_caption_property['c_community_expenses'] = 'Despeses de comunitat';
$gl_caption_property['c_ibi'] = 'IBI';
$gl_caption_property['c_municipal_tax'] = 'Taxes municipals';


$gl_caption_custumer['c_form_custumer_title'] = 'Propietaris de l\'immoble:';
$gl_caption_custumer['c_new_custumer'] = 'Entrar nou propietari';
$gl_caption_custumer['c_add_custumer'] = 'Assignar propietari';
$gl_caption_custumer['no_assigned'] = 'No hi ha cap propietari assignat a aquest immoble';

$gl_caption_zone['c_zone'] ='Nom del barri i/o zona';
$gl_caption_zone['c_municipi_id'] ='municipi';

// sobreescrits images
$gl_caption_image['c_edit'] = 'Imatges de la finca';
// sobreescrits end

//zones
$gl_caption_zone['c_municipi_id'] ='Municipi';
$gl_caption_zone['c_s_municipi_id'] ='Buscar municipi';
$gl_caption_zone['c_zone'] ='Nom del barri i/o zona';

// nous globals
$gl_caption['c_by_ref'] = '(o referències separades per espais, ej: "456 245 45")';
$gl_caption['c_by_words'] = 'Per paraules';

$gl_caption['c_list_title'] = 'Veure llistats';
$gl_caption['c_search_title'] = 'Buscar finca';
$gl_caption['c_filter_title'] = 'Filtrar finques';
$gl_caption['c_filter_title2'] = 'Gestió immobles';
$gl_caption['c_all_ordre'] = 'ordenat per:';
$gl_caption['c_category_title'] = 'Per Categoria';
$gl_caption['c_comarca_title'] = 'Per Comarca';
$gl_caption['c_municipi_title'] = 'Per Municipi';
$gl_caption['c_zone_title'] = 'Per Zona';
$gl_caption['c_status_title'] = 'Segons Estat';
$gl_caption['c_description_title'] = 'Segons Descripcions per fer';
$gl_caption['c_price_title'] = 'Per Preu';
$gl_caption['c_images_title'] = 'Per Imatges';
$gl_caption['c_has_no_images'] = 'No tenen imatges';
$gl_caption['c_has_images'] = 'Tenen imatges';
$gl_caption['c_images_indiferent'] = 'Indiferent';
$gl_caption['c_ref'] = 'Referència';
$gl_caption['c_property_private'] = 'Nom privat';
$gl_caption['c_entered'] = 'Data';
$gl_caption['c_active'] = 'Activat';
$gl_caption['c_category_original'] = 'Tipus original';
$gl_caption['c_category'] = 'Tipus';
$gl_caption['c_status'] = 'Estat';
$gl_caption['c_show_results'] = 'Mostrar resultats';
$gl_caption['c_floor_space_title'] = 'Superfície';
$gl_caption['c_land_title'] = 'Terreny';
$gl_caption['c_ordered_title'] = 'Ordenades per';
$gl_caption['c_user_title'] = 'Agents';
$gl_caption['c_custom_title'] = 'Característiques configurables';
$gl_caption['c_m2'] = 'm2';
$gl_caption['c_ha'] = 'ha';
$gl_caption['c_showwindow'] = 'Imprimir aparador';
$gl_caption['c_album'] = 'Imprimir album';


// NO TRADUIT

$gl_caption['c_images'] = 'Imatges';
$gl_caption['c_no_available'] = 'Aquest immoble ja no està disponible!';
$gl_caption['c_private'] = 'Amagar privat';
$gl_caption_tv['c_send_edit'] = 'Afegir al aparador';
$gl_caption_tv['c_download'] = 'Arxius';
$gl_caption_tv['c_main_files'] = 'Arxius_principals';
$gl_caption_tv['c_property_file'] = 'Immoble';

$gl_caption['c_type_title'] = 'Per tipus';


$gl_messages_tv['list_saved']='L\'aparador ha estat modificat correctament';


$gl_caption_property['c_living'] = 'Sales';
$gl_caption_property['c_dinning'] = 'Menjadors';
$gl_caption_property['c_service_room'] = 'Habitació servei';
$gl_caption_property['c_service_bath'] = 'Bany servei';
$gl_caption_property['c_study'] = 'Estudi';
$gl_caption_property['c_courtyard'] = 'Pati';
$gl_caption_property['c_courtyard_area'] = 'superfície';
$gl_caption_property['c_floor'] = 'Terra';
$gl_caption_property['c_parket'] = 'Parquet';
$gl_caption_property['c_ceramic'] = 'Mosaic';
$gl_caption_property['c_gres'] = 'Gres';
$gl_caption_property['c_laminated'] = 'Laminat';
$gl_caption_property['c_estrato'] = 'Estrato';
$gl_caption_property['c_estrato_e1'] = '1';
$gl_caption_property['c_estrato_e2'] = '2';
$gl_caption_property['c_estrato_e3'] = '3';
$gl_caption_property['c_estrato_e4'] = '4';
$gl_caption_property['c_estrato_e5'] = '5';
$gl_caption_property['c_estrato_e6'] = '6';
$gl_caption_property['c_edit_map'] = 'Mapa';
$gl_caption_property['c_edit_booking'] = 'Reserves';
$gl_caption_property['c_edit_booking_extra'] = 'Extres';
$gl_caption_property['c_edit_booking_season'] = 'Temporades';
$gl_caption_property['c_form_map_title'] = 'Ubicació de l\'immoble';
$gl_caption_property['c_form_booking_title'] = 'Reserves de l\'immoble';
$gl_caption_property['c_form_booking_extra_title'] = 'Extres de l\'immoble';
$gl_caption_property['c_form_booking_season_title'] = 'Temporades de l\'immoble';
$gl_caption_property['c_save_map'] = 'Guardar ubicació';
$gl_caption_property['c_latitude'] = 'Latitud';
$gl_caption_property['c_longitude'] = 'Longitud';
$gl_caption_property ['c_center_longitude'] = 'Longitud centre mapa';
$gl_caption_property ['c_center_latitude'] = 'Latitud centre mapa';
$gl_caption_property ['c_center_coords'] = 'Centre';
$gl_caption_property['c_click_map'] = 'Fés clic en un punt del mapa per establir la situació';
$gl_caption_property['c_show_map'] = 'Mostrar mapa part pública';
$gl_caption_property['c_show_map_point'] = 'Mostrar punt';
$gl_messages['point_saved'] = 'Nova ubicació guardada correctament';

$gl_caption_property['c_tipus_temp'] = 'Lloguer turístic';
$gl_caption_property['c_tipus_moblat'] = 'Lloguer amb mobles';
$gl_caption_property['c_tipus_selloption'] = 'Lloguer amb opció de compra';

$gl_caption_property['c_efficiency'] = $gl_caption_property['c_efficiency2'] = 'Eficiència energètica';
$gl_caption_property['c_efficiency_'] = '';
$gl_caption_property['c_efficiency_progress'] = 'En tràmit';
$gl_caption_property['c_efficiency_exempt'] = 'Exempt';
$gl_caption_property['c_efficiency_a'] = 'A';
$gl_caption_property['c_efficiency_b'] = 'B';
$gl_caption_property['c_efficiency_c'] = 'C';
$gl_caption_property['c_efficiency_d'] = 'D';
$gl_caption_property['c_efficiency_e'] = 'E';
$gl_caption_property['c_efficiency_f'] = 'F';
$gl_caption_property['c_efficiency_g'] = 'G';

$gl_caption_property['c_efficiency2_a'] = 'A';
$gl_caption_property['c_efficiency2_b'] = 'B';
$gl_caption_property['c_efficiency2_c'] = 'C';
$gl_caption_property['c_efficiency2_d'] = 'D';
$gl_caption_property['c_efficiency2_e'] = 'E';
$gl_caption_property['c_efficiency2_f'] = 'F';
$gl_caption_property['c_efficiency2_g'] = 'G';
$gl_caption_property['c_efficiency2_notdefined'] = '';
$gl_caption_property['c_efficiency_date']='Data';


$gl_caption_property['c_filter_room'] = 'Habitacions';
$gl_caption_property['c_filter_room_0'] = 'Cap';

$gl_caption_property['c_a4'] = 'A4';
$gl_caption_property['c_a3'] = 'A3';
$gl_caption_property['c_caption_showwindow_top_fields'] = 'Sense límit';
$gl_caption_property['c_caption_showwindow_pages'] = $gl_caption_property['c_caption_album_pages'] = 'Totes';
$gl_caption_property['c_submenu_0'] = 'Idioma';
$gl_caption_property['c_submenu_1'] = 'Tamany paper';
$gl_caption_property['c_submenu_2'] = 'Nombre de pàgines';
$gl_caption_property['c_submenu_3'] = 'Límit de camps';
$gl_caption_property['c_submenu_4'] = 'Mostrar';
$gl_caption_property['c_submenu_5'] = 'guardar aquesta configuració';
$gl_caption_property['c_submenu_6'] = 'Plantilla';

$gl_caption_property['c_views'] = 'Vistes';
$gl_caption_property['c_sea_view'] = 'Vista al mar';
$gl_caption_property['c_clear_view'] = 'Vista àmplia';
// FI NO TRADUIT

// nomes admin
$gl_caption_property['c_custom1'] = $gl_caption_property['c_custom2'] = $gl_caption_property['c_custom3'] = $gl_caption_property['c_custom4'] = $gl_caption_property['c_custom5'] = $gl_caption_property['c_custom6'] = $gl_caption_property['c_custom7'] = $gl_caption_property['c_custom8'] = '';

$gl_caption_property['c_filter_offer']='Segons Oferta';
$gl_caption_property['c_offer_0']='Sense Oferta';
$gl_caption_property['c_offer_1']='Oferta';
$gl_caption_property['c_filter_home']='Segons destacat';
$gl_caption_property['c_home_0']='No destacat';
$gl_caption_property['c_home_1']='Destacat';


$gl_caption_property['c_user_id'] = 'Agent';
$gl_caption_property['c_filter_user_id'] = 'Tots els agents';

$gl_caption_property['c_person'] = 'Persones';
$gl_caption_property['c_person_s'] = 'Persona';
$gl_caption_property['c_person_p'] = 'Persones';


$gl_caption['c_edit_history'] = 'Historial';

$gl_caption_history['c_new_history'] = 'Afegir entrada';
$gl_caption_history['c_form_history_title'] = 'Historial de l\'immoble:';
$gl_caption_history['no_assigned'] = $gl_messages_history['no_records'] = 'No hi ha historial en aquest immoble';
$gl_caption_history ['c_custumer_id_format']='%s %s %s';
$gl_caption_history ['c_delete_button']='Eliminar';

$gl_messages_history['added']='L\'entrada s\'ha afegit correctament';
$gl_messages_history['saved']='L\'entrada ha estat modificada correctament';


$gl_caption_property['c_exact_result'] = 'S\'ha trobat  <strong>1</strong> sol·licitud que coincideix amb aquest immoble' ;
$gl_caption_property['c_exact_results'] = 'S\'han trobat <strong>%s</strong> sol·licituds que coincideixen amb aquest immoble' ;
$gl_caption_property['c_exact_results_link'] = 'Veure resultats';
$gl_caption_property['c_aprox_result'] = 'S\'ha trobat <strong>1</strong> sol·licitud aproximativa per aquest immoble';
$gl_caption_property['c_aprox_results'] = 'S\'han trobat <strong>%s</strong> sol·licituds aproximatives per aquest immoble';
$gl_caption_property['c_aprox_results_link'] = 'Veure resultats';
$gl_caption ['c_user_id_format']='%s %s';

$gl_caption['c_property_file_name'] = empty($GLOBALS['gl_caption']['c_file_name'])?'':$GLOBALS['gl_caption']['c_file_name']; // Per quan es crida des de CLI
$gl_caption['c_property_old_file_name'] =  empty($GLOBALS['gl_caption']['c_old_file_name'])?'':$GLOBALS['gl_caption']['c_old_file_name'];

$gl_caption['c_send_mail'] = 'Enviar email';		

$gl_caption_property['c_expired'] = 'Data caducitat';
$gl_caption_property['c_construction_date'] = 'Data contrucció';
$gl_caption_property['c_facing'] = 'Orientació';
$gl_caption_property['c_facing_'] = '';
$gl_caption_property['c_facing_n'] = 'Nord';
$gl_caption_property['c_facing_ne'] = 'Norest';
$gl_caption_property['c_facing_e'] = 'Est';
$gl_caption_property['c_facing_se'] = 'Sudest';
$gl_caption_property['c_facing_s'] = 'Sud';
$gl_caption_property['c_facing_so'] = 'Sudoest';
$gl_caption_property['c_facing_o'] = 'Oest';
$gl_caption_property['c_facing_no'] = 'Noroest';
$gl_caption_property['c_exclusive'] = 'Exclusiva';
$gl_caption_property['c_zip'] = 'Codi postal';
$gl_caption_property['c_descriptionregister'] = 'Descripció registre';
$gl_caption['c_list_mls'] = 'Llistar immobles mls';
$gl_caption['c_phone1'] = 'Telèfon';
$gl_caption['c_comercialname'] = 'Empresa';
$gl_caption['c_client_id']='';
$gl_caption['c_client_dir']='';
$gl_caption['c_domain']='';

$gl_caption['c_star']='Estrelles';
$gl_caption['c_star_0']='Cap';
$gl_caption['c_star_1']='1';
$gl_caption['c_star_2']='2';
$gl_caption['c_star_3']='3';
$gl_caption['c_star_4']='4';
$gl_caption['c_star_5']='5';

// Booking
$gl_caption_property['c_book_search_title'] = 'Buscar disponibilitat';
$gl_caption['c_dates_from'] = 'Del';
$gl_caption['c_dates_to'] = 'al';
$gl_caption_property['c_date_in'] = 'Data d\'entrada';
$gl_caption_property['c_date_out'] = 'Data de sortida';
$gl_caption_property['c_adult'] = 'Adults';
$gl_caption_property['c_child'] = 'Nens';
$gl_caption_property['c_baby'] = 'Bebès';
$gl_caption_property['c_book_property_button'] = 'Reservar';
$gl_caption_property['c_time_in'] = 'Hora d\'entrada';
$gl_caption_property['c_time_out'] = 'Hora de sortida';

$gl_caption_property['c_marble'] = 'Mármol';
$gl_caption_property['c_hut'] = 'HUT';
$gl_caption_property['c_level_count'] = 'Número de plantas';

$gl_caption_property['c_managed_by_owner'] = 'Gestionat pel propietari';

// Traduccions
$gl_caption_adjective['c_gender'] = 'Génere';
$gl_caption_adjective['c_adjective_before'] = 'Abans';
$gl_caption_adjective['c_adjective_after'] = 'Després';

// Portal
$gl_caption['c_portal_config_title'] = "Opcions de publicació";
$gl_caption['c_apicat_export_all'] = "Exportar tots els immobles";
$gl_caption['c_apicat_export_all_text'] = "Si marca la casella s'exportaran tots els immobles públics sense necessitat de seleccionar-ne cap.";
$gl_caption['c_apicat_export_all_text_2'] = "Només s'exportaran els immobles que tinguin preu, codi postal o ubicació exacta. Api.cat no admet lloguer turístic. Fotocasa ha de tindre superfície";
$gl_caption['c_excluded_'] = "Exportables - No exportables";
$gl_caption['c_excluded_apicat'] = "Exclosos Apicat";
$gl_caption['c_excluded_fotocasa'] = "Exclosos Fotocasa";
$gl_caption['c_excluded_habitaclia'] = "Exclosos Habitaclia";

$gl_caption['c_no_subscrit'] = "Informació publicació portals ";
$gl_caption['c_no_subscrit_apicat'] = "No està donat d'alta per publicar a Api.cat";
$gl_caption['c_no_subscrit_fotocasa'] = "No està donat d'alta per publicar a Fotocasa";
$gl_caption['c_no_subscrit_habitaclia'] = "No està donat d'alta per publicar a Habitaclia";

$gl_caption['c_portal_check_title'] = "Falten les següents dades per poder exportar";
$gl_caption['c_fotocasa_check_price'] = "Preu";
$gl_caption['c_fotocasa_check_latitude'] = "Ubicació en el mapa";
$gl_caption['c_fotocasa_check_floor_space'] = "Superfície";

$gl_caption['c_habitaclia_check_ref'] = "Referència";
$gl_caption['c_habitaclia_check_adress'] = "Adreça i ubicació en el mapa amb \"Mostrar punt\" marcat";

$gl_caption['c_apicat_check_tipus'] = "No pot ser lloguer turístic";
$gl_caption['c_apicat_check_consult'] = "Desmarcar \"Preu a consultar\"";
$gl_caption['c_apicat_check_zip'] = "Codi postal o ubicació en el mapa amb \"Mostrar punt\" marcat";