<?

/**
 * InvoicePdf
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2016
 * @version $Id$
 * @access public
 *
 * En el fichero de presentación de adeudos, ¿es necesario indicar en algún campo del fichero si el deudor es persona física o jurídica?  Ocultar información
 *
 * SÍ, debe indicarse en las etiquetas deudor.
 * ¿Qué formato de mandato se admite en SEPA?  Ocultar información
 *
 * El documento de mandato debe contener los datos obligatorios que se exigen para identificar la orden de domiciliación en SEPA, a saber: la referencia única del mandato, el nombre y domicilio del deudor, el IBAN, y en su caso el BIC, de la cuenta del deudor, el nombre del acreedor, el identificador del acreedor, el tipo de pago, la fecha de firma y la firma del deudor, e indicar claramente que se trata de un mandato de adeudo directo SEPA. El diseño de los mandatos es indiferente.
 *
 * En España se ha actualizado el folleto 50 de la serie de normas y procedimientos bancarios para diseñar una nueva orden de domiciliación adaptada a los requerimientos de los adeudos directos SEPA, ya sea en la versión básica como en la B2B, que recoge un formato abreviado de mandato con los elementos mínimos necesarios para emitir adeudos directos SEPA en cada una de las modalidades.
 * Caduca en 36 mesos
 *
 * ¿Qué debe consignarse en la secuencia de un adeudo?  Ocultar información
 *
 * Según el adeudo responda a una operación de pago único o recurrente, el cliente deberá indicar, en cada caso, el código que corresponda:
 *
 * FRST - Primer adeudo de una serie de adeudos recurrentes.
 * RCUR - Adeudo de una serie de adeudos recurrentes acogidos a un mismo mandato, cuando no se trate ni del primero ni del último.
 * FNAL - Último adeudo de una serie de adeudos recurrentes
 * OOFF - Adeudo correspondiente a una autorización con un único pago
 *
 * El incumplimiento de esta secuencia puede provocar el rechazo de las operaciones.
 *
 *
 */

require_once( DOCUMENT_ROOT . 'common/includes/shepa/Sephpa.php' );

class InvoiceSepa {

	var $rs, $errors = [], $file_name;

	function __construct( &$module, $batch_id = false ) {

		header( 'Content-type: text/html; charset=utf-8' ); // necessari pel flush
		@ini_set( 'zlib.output_compression', 0 );
		@ini_set( 'implicit_flush', 1 );
		@ob_end_clean();
		ob_implicit_flush( 1 );

		$this->module    = &$module;
		$this->file_name = "remesa-$batch_id";

		$directDebitFile = new SephpaDirectDebit( 'letnd Serveis Informàtics 2005 S.L.',
			"remesa-$batch_id",
			SephpaDirectDebit::SEPA_PAIN_008_003_02 );

		$this->sepa = $directDebitFile->addCollection( array(
			// needed information about the payer
			'pmtInfId'     => "remesa-$batch_id",
			'lclInstrm'    => SepaUtilities::LOCAL_INSTRUMENT_CORE_DIRECT_DEBIT,
			'seqTp'        => SepaUtilities::SEQUENCE_TYPE_RECURRING,
			'cdtr'         => 'Marc Sanahuja Campanyà',
			'iban'         => 'ES9300810004300006506660',
			'bic'          => 'BSABESBB',
			'ci'           => '47184738P',
			// optional
			'btchBookg'    => 'true',
		) );

	}

	private function get_mandate( &$rs ) {

		$custumer_id = $rs['custumer_id'];
		$invoice_id  = $rs['invoice_id'];
		$ret         = [];

		// Busco el que te invoice_id, la segona consulta se'l pot passar per això en faig 2
		$is_first = Db::get_row( "SELECT * FROM invoice__mandate WHERE custumer_id = $custumer_id AND invoice_id = $invoice_id" );
		// Comprobo si el client te un mandate, ordeno per id així obtinc l'últim del client
		$mandate = Db::get_row( "SELECT * FROM invoice__mandate WHERE custumer_id = $custumer_id ORDER BY mandate_id LIMIT 1" );

		if ( $is_first ) {

			$ret['type']        = 'FRST';
			$ret['mandate_ref'] = $is_first['mandate_ref'];

		} elseif ( $mandate ) {

			$mandate_id = $mandate['mandate_id'];
			// Si invoice_id, vol dir que no es el de la factura actual, posem recurrent
			if ( $mandate['invoice_id'] ) {
				$ret['type'] = 'RCUR';

			} // Si te mandate però no invoice_id, vol dir que no s'ha utilitzat mail
			else {
				$ret['type'] = 'FRST';

				Db::execute( "
					UPDATE invoice__mandate SET invoice_id = '$invoice_id' 
					WHERE mandate_id = $mandate_id;" );
			}
			$ret['mandate_ref'] = $mandate['mandate_ref'];

		} // sino en te el creo automàticament
		else {

			$mandate_ref        = "LETND-$custumer_id-1";
			$ret['type']        = 'FRST';
			$ret['mandate_ref'] = $mandate_ref;

			Db::execute( "
				INSERT INTO invoice__mandate (custumer_id, mandate_ref, invoice_id) 
				VALUES ($custumer_id, '$mandate_ref', $invoice_id)" );

		}

		return $ret;

	}

	private function add_error( $error ) {
		$this->errors [] = $error;
	}

	private function check_payment_error( $custumer_id, $company, &$rs, $mandate ) {
		$link  = "<a href=\"/?menu_id=5003&action=show_form_edit&custumer_id=$custumer_id\">";
		$link2 = "</a>";
		if ( ! $company ) {
			$this->add_error( "$link$custumer_id$link2: no te nom" );
		}
		if ( ! $rs['custumer_comta'] ) {
			$this->add_error( "$link$company$link2: no te <strong>número de compte</strong>" );
		}
		// per si a cas es fes desde invoice, a batch ja no agafa el registre, així es poden fer factures a 0 però que no passi rebut
		if ( $rs['total_price'] == '0.00' ) {
			$this->add_error( "$link$company$link2: no te <strong>preu</strong>" );
		}
		if ( $rs['bill_due'] == '0000-00-00' ) {
			$this->add_error( "$link$company$link2: no te data <strong>factura</strong>" );
		}
		if ( $mandate['mandate_ref'] == '' ) {
			$this->add_error( "$link$company$link2: no te <strong>referencia mandat</strong>" );
		}
		if ( $rs['bill_date'] == '0000-00-00' ) {
			$this->add_error( "$link$company$link2: no te data de <strong>venciment</strong>" );
		}
	}

	public function add_payment( $invoice_id ) {

		$this->rs    = $rs = Db::get_row( "SELECT * FROM invoice__invoice WHERE invoice_id = $invoice_id" );
		$custumer_id = $rs['custumer_id'];

		$mandate = $this->get_mandate( $rs );

		if ( $rs['custumer_billing'] == 'citizen' ) {
			$company = $rs['custumer_name'] . ' ' . $rs['custumer_surname1'] . ' ' . $rs['custumer_surname2'];
		} else {
			$company = $rs['custumer_company'];
		}

		$this->check_payment_error( $custumer_id, $company, $rs, $mandate );

		$payment = array(
			// needed information about the
			    'pmtId'         => '"LETND-$invoice_id"',
			    'instdAmt'      => $rs['total_price'],
			    'mndtId'        => $mandate['mandate_ref'],
			    'dtOfSgntr'     => '2010-04-12',            // Date of signature
			    // 'bic'           => 'BELADEBEXXX',
			    'dbtr'          => $company,        // (max 70 characters)
			    'iban'          =>  $rs['custumer_comta'],// IBAN of the Debtor
			);


		$payment = array(
			// "name"            => $company,
			// "IBAN"            => $rs['custumer_comta'],
			// "amount"          => $rs['total_price'] * 100,
			"type"            => $mandate['type'],
			"collection_date" => $rs['bill_due'],
			// "mandate_id"      => $mandate['mandate_ref'],
			"mandate_date"    => $rs['bill_date'],
			"description"     => "FACTURA LETND " . $rs['invoice_serial'] . $rs['invoice_number'],
			//'end_to_end_id'   => "LETND-$invoice_id"
		);

		$this->sepa->addPayment( $payment );

	}

	public function output() {

		if ( $this->errors ) {
			print_javascript( 'top.$("#message_container").show();top.$("#message").html("");' );
			$this->output_js( "Hi ha errors en les factures<br><br>", 'resaltat' );

			foreach ( $this->errors as $error ) {
				$this->output_js( "$error<br><br>", 'vermell' );
			}


			return;
		}

		$rs   = &$this->rs;
		$path = CLIENT_PATH . 'invoice/sepas/';


		$file = '';
		try {
			$file = $this->sepa->save();
		} catch ( Exception $e ) {
			echo $e->getMessage();
		}

		if ( $file ) {

		} else {
			print_javascript( 'top.alert("No no no")' );
		}

		file_put_contents( $path . $this->file_name, $file );

		UploadFiles::serve_download( $path . $this->file_name, $this->file_name, true );


		die();
	}

	public function output_js( $message, $class, $encode = false ) {

		$message = json_encode( "<span class=\"$class\">$message</span>" );


		echo( '<SCRIPT LANGUAGE="JavaScript" TYPE="text/javascript">
			parent.Invoice.write_report(' . $message . ');
			</SCRIPT>' );

		// this is to make the buffer achieve the minimum size in order to flush data
		echo str_repeat( ' ', 1024 * 64 );
		ob_flush();
		flush();

	}


}

