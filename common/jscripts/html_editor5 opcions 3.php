<?Header("content-type: application/x-javascript");
extract($_GET);
$c_send_edit = addslashes($c_send_edit);
?>
<? if ( 1 == 2 ): ?>
	<script>
<? endif // 1 == 2 ?>

if (typeof templates == 'undefined')  templates = [];

var default_options = {
		// General options
		mode : "exact",
		language : "<?=$language_code?>",
        <?if($theme_advanced_fonts) echo 'theme_advanced_fonts: "'.$theme_advanced_fonts.'",'?>
        <?if($theme_advanced_font_sizes) echo 'theme_advanced_font_sizes: "'.$theme_advanced_font_sizes.'",'?>
		theme : "silver",
		templates: templates,

        forced_root_block : false,

		// paste as plain text,
        paste_retain_style_properties: "all",
        paste_word_valid_elements: "b,strong,i,em,h1,h2",
	    paste_as_text: true,

		spellchecker_languages : "+Català=ca,Español=es,English=en,Français=fr,Deutsch=de",
		relative_urls: false,
		remove_script_host: true,
        // browser_spellcheck: true, Per posar aquest s'ha de desactivar el context menú

		// form atributes
		extended_valid_elements: 'form[accept|accept-charset|action|class|dir<ltr?rtl|enctype|id|lang|method<get?post|name|onreset|onsubmit|style|title|target],div[id|class|title],div[*]',

		// File manager
		external_filemanager_path:"/common/jscripts/filemanager/",
		filemanager_title:"<?= $c_file_manager ?>" ,
		external_plugins: { "filemanager" : "/common/jscripts/filemanager/plugin.min.js"},

		// CSS
		content_css : "/<?=$client_dir?>/templates/styles/editor.css",

		// Configadmin
		indentation : '<?=$tinymce_indent?>px',

        external_link_list_url : "/common/jscripts/link_list.js",
		external_image_list_url : "/common/jscripts/image_list.js",
		media_external_list_url : "/common/jscripts/media_list.js",

		// filtre classes
		class_filter : function(cls, rule) {

			if (/^body .*/.test(rule))	return false;
			return cls;
		},

	};
var option = {};

if ( gl_tool_section != 'config' && (gl_action == 'list_records' || gl_action == 'get_records')){
	var options_inline = {
		 menubar: false
		 // inline: true - Ha de ser div
	 };
	 $.extend(default_options, options_inline);
}

// tooltip = "<?=$c_send_edit?>";
if (html_editors["htmlbasic"]){

	options = {
		elements : html_editors["htmlbasic"].join(','),
	    menubar: false,
        // plugins : "paste",
		plugins: [
			'advlist autolink lists link image charmap print preview anchor textcolor',
			'searchreplace visualblocks code fullscreen',
			'insertdatetime media table paste code help wordcount spellchecker responsivefilemanager'
		],
	   toolbar: 'undo redo | formatselect | bold italic backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | spellchecker<?if($show_html) echo " | code"?>',


		// Theme options
		// theme_advanced_buttons1 : "bold,italic,underline,strikethrough,formatselect<?if($show_html) echo ",|,code"?>"
	};

	var settings = $.extend({}, default_options, options);
	delete settings['templates'];
	tinyMCE.init( settings );

}

if (html_editors["htmlnewsletter"]){

	options = {
		elements : html_editors["htmlnewsletter"].join(','),
		plugins : "advlist image spellchecker save contextmenu paste fullscreen noneditable template responsivefilemanager",
	   // Theme options
		toolbar1 : "bold italic underline strikethrough | formatselect fontselect fontsizeselect | link unlink | cleanup removeformat images | undo redo | spellchecker fullscreen save",
		toolbar2 : "bullist numlist | indent outdent | sub sup | cut copy paste pastetext pasteword selectall<?if($show_html) echo " | code"?>"
	};
	var settings = $.extend({}, default_options, options);

	tinyMCE.init( settings );
}

if (html_editors["html"]){

	options = {
		elements : html_editors["html"].join(','),
		plugins : "advlist image spellchecker save contextmenu paste fullscreen noneditable template responsivefilemanager",
		// Theme options
		toolbar1 : "bold italic underline strikethrough | styleselect formatselect | link unlink | cleanup removeformat images | undo redo | spellchecker fullscreen save",
		toolbar2 : "bullist numlist | indent outdent | sub sup | cut copy paste pastetext pasteword selectall template<?if($show_html) echo " | code"?>"
	};
	var settings = $.extend({}, default_options, options);

	tinyMCE.init( settings );
}

if (html_editors["htmlfull"]){

	options = {
		elements : html_editors["htmlfull"].join(','),
        plugins : "advlist image autolink lists spellchecker pagebreak table save emoticons insertdatetime preview media searchreplace print contextmenu paste directionality fullscreen noneditable visualchars nonbreaking template responsivefilemanager",

	   /*valid_elements : "@[id|class|style|title|dir<ltr?rtl|lang|xml::lang|onclick|ondblclick|"
							+ "onmousedown|onmouseup|onmouseover|onmousemove|onmouseout|onkeypress|"
							+ "onkeydown|onkeyup],a[rel|rev|charset|hreflang|tabindex|accesskey|type|"
							+ "name|href|target|title|class|onfocus|onblur],strong/b,em/i,strike,u,"
							+ "#p,-ol[type|compact],-ul[type|compact],-li,br,img[longdesc|usemap|"
							+ "src|border|alt=|title|hspace|vspace|width|height|align],-sub,-sup,"
							+ "-blockquote,-table[border=0|cellspacing|cellpadding|width|frame|rules|"
							+ "height|align|summary|bgcolor|background|bordercolor],-tr[rowspan|width|"
							+ "height|align|valign|bgcolor|background|bordercolor],tbody,thead,tfoot,"
							+ "#td[colspan|rowspan|width|height|align|valign|bgcolor|background|bordercolor"
							+ "|scope],#th[colspan|rowspan|width|height|align|valign|scope],caption,-div,"
							+ "-span,-code,-pre,address,-h1,-h2,-h3,-h4,-h5,-h6,hr[size|noshade],-font[face"
							+ "|size|color],dd,dl,dt,cite,abbr,acronym,del[datetime|cite],ins[datetime|cite],"
							+ "object[classid|width|height|codebase|*],param[name|value|_value],embed[type|width"
							+ "|height|src|*],script[src|type],map[name],area[shape|coords|href|alt|target],bdo,"
							+ "button,col[align|char|charoff|span|valign|width],colgroup[align|char|charoff|span|"
							+ "valign|width],dfn,fieldset,form[action|accept|accept-charset|enctype|method],"
							+ "input[accept|alt|checked|disabled|maxlength|name|readonly|size|src|type|value],"
							+ "kbd,label[for],legend,noscript,optgroup[label|disabled],option[disabled|label|selected|value],"
							+ "q[cite],samp,select[disabled|multiple|name|size],small,"
							+ "textarea[cols|rows|disabled|name|readonly],tt,var,big",*/
		// form atributes
		extended_valid_elements: 'form[accept|accept-charset|action|class|dir<ltr?rtl|enctype|id|lang|method<get?post|name|onreset|onsubmit|style|title|target],p[class|dir<ltr?rtl|id|lang|onclick|ondblclick|onkeydown|onkeypress|onkeyup|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|style|title],div[*]',
		// Theme options
		toolbar1 : "bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | styleselect formatselect fontselect fontsizeselect | undo redo | spellchecker fullscreen save newdocument",
        toolbar2 : "cut copy paste pastetext pasteword | search replace | bullist numlist | outdent indent blockquote | link unlink anchor images cleanup | insertdate inserttime | forecolor backcolor",
        toolbar3 : "tablecontrols | hr removeformat visualaid | sub sup | charmap emotions iespell | print",
        toolbar4 : "insertlayer moveforward movebackward absolute | styleprops | cite abbr acronym del ins attribs | visualchars nonbreaking<?if($show_html) echo " | code"?>"
	};
	var settings = $.extend({}, default_options, options);

	tinyMCE.init( settings );
}


<? if ( 1 == 2 ): ?>
	</script>
<? endif // 1 == 2 ?>

<? /* TOTES LES  OPCIONS
        // Theme options
        theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,alignleft,aligncenter,alignright,alignjustify,|,styleselect,formatselect,fontselect,fontsizeselect",
        theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
        theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,|,print,|,ltr,rtl,|,fullscreen",
        theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,spellchecker,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,blockquote,pagebreak,|,insertfile,insertimage",
        theme_advanced_toolbar_location : "top",
        theme_advanced_toolbar_align : "left",
        theme_advanced_statusbar_location : "bottom",
        theme_advanced_resizing : true,
	*/