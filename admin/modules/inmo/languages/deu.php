<?php
global $gl_caption_tv, $gl_messages_tv;
// menus eina
if (!defined('INMO_MENU_PROPERTY')){
	define('INMO_MENU_PROPERTY', 'Immobilien');
	define('INMO_MENU_PROPERTY_NEW', 'Neue Immobilie eingeben');
	define('INMO_MENU_PROPERTY_LIST', 'Immobilien auflisten');
	define('INMO_MENU_SEARCH_FORM', 'Immobilien suchen und bearbeiten');
	define('INMO_MENU_PROPERTY_BIN', 'Papierkorb');
	define('INMO_MENU_CATEGORY', 'Immobilientypen');
	define('INMO_MENU_CATEGORY_NEW', 'Neuen Typeingeben');
	define('INMO_MENU_CATEGORY_LIST', 'Typen auflisten');
	define('INMO_MENU_TRANSLATION', 'Übersetzungen');
	define('INMO_MENU_PROPERTY_TRANSLATION_LIST', 'Zu übersetzende Immobilien auflisten');
	define('INMO_MENU_CATEGORY_TRANSLATION_LIST', 'Zu übersetzende Kategorien auflisten');
	define('INMO_MENU_CONFIG', 'Immobilien-Einstellung');
	define('INMO_MENU_CONFIGADMIN_LIST', 'Einstellung des internen Bereichs');
	define('INMO_MENU_CONFIGPUBLIC_LIST', 'Einstellung des öffentlichen Bereichs');
	define('INMO_MENU_CONFIGADMIN', 'Einstellung des internen Bereichs');
	define('INMO_MENU_CONFIGPUBLIC', 'Einstellung des öffentlichen Bereichs');
	define('INMO_MENU_ZONE', 'Gebiete');
	define('INMO_MENU_ZONE_NEW', 'Neues Gebiet eingeben');
	define('INMO_MENU_ZONE_LIST', 'Gebiete auflisten');
	define('INMO_MENU_ZONE_BIN', 'Papierkorb');
	define('INMO_MENU_TV', 'Virtuelle Schaufenster');
	define('INMO_MENU_TV_LIST', 'Schaufenster erzeugen');
	define('INMO_MENU_TV_LIST_SELECTED', 'Eigenschaften im Fenster');
	define('INMO_MENU_TV_LIST_DOWNLOAD', 'Download Eigenschaften');
	define('INMO_MENU_PROPERTY_MAP_LIST', 'Auf der Karte suchen');

	define ('TITLE_COMARCA_ADMIN','Nach Gebiet: ');
	define('PROPERTY_AUTO_REF', 'Automatisch');

	define('BOOKING_MENU_PROMCODE','Codi promocional');
	define('BOOKING_MENU_PROMCODE_NEW','Crear codi');
	define('BOOKING_MENU_PROMCODE_LIST','Llistar codi');
	define('BOOKING_MENU_PROMCODE_LIST_BIN','Paperera');

	define('BOOKING_MENU_POLICE','Registro Mossos');
	define('BOOKING_MENU_POLICE_LIST','Listar reservas');
}






















// sobreescrits missatges demand custumer
if (isset($gl_messages)) {
	$gl_messages_demand = $gl_messages;
	$gl_messages_custumer = $gl_messages;
}
// sobreescrits end

// sobreescrits missatges property
$gl_messages_property['added']='Die Immobilie wurde ordnungsgemäß angehängt. Nachfolgend können Sie die entsprechenden Bilder der neuen Immobilie anhängen.';
$gl_messages_property['saved']='Die Immobilie wurde ordnungsgemäß geändert.';
$gl_messages_property['no_records']='In dieser Liste ist keine Immobilie vorhanden.';
$gl_messages_property['no_records_bin']='Im Papierkorb ist keine Immobilie vorhanden.';
$gl_messages_property['list_saved']='Die Änderungen der Immobilien wurden ordnungsgemäß durchgeführt.';
$gl_messages_property['list_not_saved']='Es wurde keine Immobilie geändert, es wurden keine Änderungen entdeckt.';
$gl_messages_property['list_deleted']='Die ausgewählten Immobilien wurden gelöscht.';
$gl_messages_property['list_bined']='Die Immobilien wurden in den Papierkorb verschoben.';
$gl_messages_property['list_restored']='Die Immobilien wurden ordnungsgemäß wiederhergestellt.';
// sobreescrits end

$gl_messages_category['related_not_activated'] = 'Dieser Typ kann nicht deaktiviert werden, da verbundene Immobilien vorhanden sind.';
$gl_messages_category['relateds_not_activated'] = 'Die markierten Immobilientypen können nicht deaktiviert werden, da verbundene Immobilien vorhanden sind.';

// captions seccio
$gl_caption_demand['c_form_title'] = 'Daten der Kundenanfrage:';

$gl_caption['c_property_button'] = 'Immobilien anzeigen';
$gl_caption['c_view_property'] = 'Immobilien anzeigen';

$gl_caption_property['c_move_to_category_id'] = '[Immobilientyp]';
$gl_caption_property['c_move_to_category_id2'] = 'Ausgewählte Immobilien verschieben nach:';
$gl_caption_property['c_move_to_zone_id'] = '[Gebiet]';
$gl_caption_property['c_move_to_zone_id2'] = 'Ausgewählte Immobilien verschieben nach:';
$gl_caption_property['c_move_to_zone_id3'] = '[kein Gebiet]';

$gl_caption_category['c_ordre'] = 'Reihenfolge';
$gl_caption_category['c_ref_code'] = 'Referenzcode der Immobilien';
$gl_caption_property['c_room'] = 'Schlafzimmer';
$gl_caption_property['c_bathroom'] = 'Badezimmer';
$gl_caption_property['c_wc'] = 'Toiletten';
$gl_caption_property['c_laundry'] = 'Waschbereich';
$gl_caption_property['c_parking'] = 'Parking';
$gl_caption_property['c_garage'] = 'Garage';
$gl_caption_property['c_garage_area'] = 'Fläche';
$gl_caption_property['c_storage'] = 'Abstellraum';
$gl_caption_property['c_storage_area'] = 'Fläche';
$gl_caption_property['c_terrace'] = 'Terrasse';
$gl_caption_property['c_terrace_area'] = 'Fläche';
$gl_caption_property['c_garden'] = 'Garten';
$gl_caption_property['c_garden_area'] = 'Fläche';
$gl_caption_property['c_swimmingpool'] = 'Schwimmbad';
$gl_caption_property['c_swimmingpool_area'] = 'Fläche';
$gl_caption_property['c_citygas'] = 'Stadtgas';
$gl_caption_property['c_centralheating'] = 'Heizung';
$gl_caption_property['c_airconditioned'] = 'Klimaanlage';
$gl_caption_property['c_ref'] = 'Ref.';










$gl_caption_property['c_property_private'] = 'Interner Name';
$gl_caption['c_category_id'] = 'Typ';
$gl_caption_property['c_floor_space'] = 'Fläche';
$gl_caption_property['c_land'] = 'Grundstück';
$gl_caption_property['c_land_units'] = 'Maßeinheiten';
$gl_caption_property['c_m2'] = 'm2';
$gl_caption_property['c_ha'] = 'Hektar';
$gl_caption_property['c_country_id'] = 'Land';
$gl_caption_property['c_price_private'] = 'Interner Preis';
$gl_caption_property['c_price'] = 'Preis';
$gl_caption_property['c_price_m2'] = 'Preis/m2';
$gl_caption_property['c_zoom'] = 'Zoom';
$gl_caption_property['c_price_consult'] = 'Preis nach Absprache';
$gl_caption_property['c_price_consult_1'] = 'Preis nach Absprache';
$gl_caption_property['c_price_history_link'] = 'Historial de precios';
$gl_caption_property['c_owner_id'] = '';
$gl_caption_property['c_status'] = 'Status';
$gl_caption_property['c_prepare'] = 'In Vorbereitung';
$gl_caption_property['c_review'] = 'Überprüfen';
$gl_caption_property['c_onsale'] = 'Zu verkaufen';
$gl_caption_property['c_sold'] = 'Verkauft';
$gl_caption_property['c_archived'] = 'Abgelegt';
$gl_caption_property['c_reserved'] = 'Reserviert';
$gl_caption_property['c_zone'] = 'Gebiet'; // per la part publica
$gl_caption_property['c_zone_id'] = 'Gebiet';
$gl_caption_property['c_zone_id_caption'] = '[kein Gebiet]';
$gl_caption_property['c_provincia_id'] = 'Region';
$gl_caption_property['c_comarca_id'] = 'Landkreis';
$gl_caption_property['c_municipi_id'] = $gl_caption_property['c_municipi'] = 'Ort';
$gl_caption_property['c_property'] = 'Immobilienname';
$gl_caption_property['c_property_title'] = 'Kurzbeschreibung';
$gl_caption_property['c_entered'] = 'Erstellt';
$gl_caption_property['c_ref_number'] = '';
$gl_caption_property['c_home'] = 'Top';
$gl_caption_property['c_offer'] = 'Angebot';
$gl_caption_property['c_data'] = 'Immobiliendaten';
$gl_caption_property['c_descriptions'] = 'Beschreibungen';
$gl_caption_property['c_details'] = 'Einzelheiten';
$gl_caption_property['c_description'] = 'Beschreibung';
$gl_caption_property['c_details_temp'] = 'Saisonmiete';
$gl_caption_property['c_0'] = '';
$gl_caption_property['c_1'] = '1';
$gl_caption_property['c_2'] = '2';
$gl_caption_property['c_3'] = '3';
$gl_caption_property['c_4'] = '4';
$gl_caption_property['c_5'] = '5';
$gl_caption_property['c_6'] = '6';
$gl_caption_property['c_7'] = '7';
$gl_caption_property['c_8'] = '8';
$gl_caption_property['c_9'] = '9';
$gl_caption_property['c_10'] = '10';
$gl_caption_property['c_11'] = '11';
$gl_caption_property['c_12'] = '12';
$gl_caption_property['c_13'] = '13';
$gl_caption_property['c_14'] = '14';
$gl_caption_property['c_15'] = '15';
$gl_caption_property['c_16'] = '16';
$gl_caption_property['c_17'] = '17';
$gl_caption_property['c_18'] = '18';
$gl_caption_property['c_19'] = '19';
$gl_caption_property['c_20'] = '20';
$gl_caption_property['c_tipus'] = 'Transaktion';
$gl_caption_property['c_tipus_sell'] = 'Verkauf';
$gl_caption_property['c_tipus_rent'] = 'Vermietung';
$gl_caption_property['c_filter_status'] = 'Alle Status';
$gl_caption_property['c_filter_category_id'] = 'Alle Typen';
$gl_caption_property['c_filter_tipus'] = 'Alle Transaktionen';
$gl_caption_property['c_address'] = 'Adresse';
$gl_caption_property['c_adress'] = 'Straße';
$gl_caption_property['c_numstreet'] = 'Hausnummer';
$gl_caption_property['c_block'] = 'Block';
$gl_caption_property['c_flat'] = 'Etage';
$gl_caption_property['c_door'] = 'Wohnungsnummer';
$gl_caption_property['c_observations'] = 'Anmerkungen';
$gl_caption_property['c_data_private'] = 'Interne Daten';
$gl_caption_property['c_data_public'] = 'Öffentliche Daten';
$gl_caption_property['c_tipus2'] = 'Bauart';
$gl_caption_property['c_elevator'] = 'Aufzug';
$gl_caption_property['c_balcony'] = 'Balkon';
$gl_caption_property['c_new'] = 'Neubau';
$gl_caption_property['c_second'] = 'Gebraucht';
$gl_caption_property['c_furniture'] = 'Möbliert';
$gl_caption_property['c_file'] = 'Dokumente';
$gl_caption_property['c_video'] = $gl_caption_property['c_videoframe'] = 'Video';
$gl_caption_property['c_edit_custumer'] = 'Eigentümer';
$gl_caption_property['c_edit_custumer_button'] = 'Eigentümer bearbeiten';

$gl_caption_property['c_mls']='MLS';
$gl_caption_property['c_share_mls']='Gemeinsame Nutzung per MLS';
$gl_caption_property['c_comission'] = 'Provision MLS';

$gl_caption_property['c_filter_activated'] = 'All customers';
$gl_caption_property['c_activated_0'] = 'Deactivated';
$gl_caption_property['c_activated_1'] = 'Activated';

$gl_caption_property['c_efficiency_letter'] = 'Letter';
$gl_caption_property['c_efficiency_number'] = 'Energy';
$gl_caption_property['c_efficiency_number2'] = 'Emission';
$gl_caption_property['c_sea_distance'] = 'Distance to the sea';

$gl_caption_property['c_expenses'] = 'Expenses';
$gl_caption_property['c_community_expenses'] = 'Community expenses';
$gl_caption_property['c_ibi'] = 'Property tax';
$gl_caption_property['c_municipal_tax'] = 'Local taxes';


$gl_caption_custumer['c_form_custumer_title'] = 'Immobilieneigentümer:';
$gl_caption_custumer['c_new_custumer'] = 'Neuen Eigentümer eingeben';
$gl_caption_custumer['c_add_custumer'] = 'Eigentümer zuordnen';
$gl_caption_custumer['no_assigned'] = 'Dieser Immobilie ist kein Eigentümer zugeordnet.';

$gl_caption_zone['c_zone'] ='Name des Viertels und/oder Gebiets';
$gl_caption_zone['c_municipi_id'] ='Gemeinde';

// sobreescrits images
$gl_caption_image['c_edit'] = 'Bilder der Immobilie';
// sobreescrits end

//zones
$gl_caption_zone['c_municipi_id'] ='Gemeinde';
$gl_caption_zone['c_s_municipi_id'] ='Gemeinde suchen';
$gl_caption_zone['c_zone'] ='Name des Viertels und/oder Gebiets';

// nous globals
$gl_caption['c_by_ref'] = '(oder Referenzcodes getrennt durch Leerzeichen, z.B.: "456 245 45")';
$gl_caption['c_by_words'] = 'Nach Wörte';

$gl_caption['c_list_title'] = 'Liste anzeigen';
$gl_caption['c_search_title'] = 'Anwesen suchen';
$gl_caption['c_filter_title'] = 'Anwesen filtern';
$gl_caption['c_filter_title2'] = 'Immobilienverwaltung';
$gl_caption['c_all_ordre'] = 'ordnen nach:';
$gl_caption['c_category_title'] = 'Nach Kategorien';
$gl_caption['c_comarca_title'] = 'Nach Comarca';
$gl_caption['c_municipi_title'] = 'Nach Stadt';
$gl_caption['c_zone_title'] = 'Nach Gebieten';
$gl_caption['c_status_title'] = 'Nach Status';
$gl_caption['c_description_title'] = 'Nach Beschreibungen';
$gl_caption['c_price_title'] = 'Nach Preisen';
$gl_caption['c_images_title'] = 'Nach Bildern';
$gl_caption['c_has_no_images'] = 'Keine Bilder vorhanden';
$gl_caption['c_has_images'] = 'Bilder vorhanden';
$gl_caption['c_images_indiferent'] = 'Egal';
$gl_caption['c_ref'] = 'Referenzcode';
$gl_caption['c_property_private'] = 'Interner Name';
$gl_caption['c_entered'] = 'Daten';
$gl_caption['c_active'] = 'Aktivität';
$gl_caption['c_category_original'] = 'Ursprünglicher Typ';
$gl_caption['c_category'] = 'Typ';
$gl_caption['c_status'] = 'Status';
$gl_caption['c_show_results'] = 'Ergebnisse anzeigen';
$gl_caption['c_floor_space_title'] = 'Fläche';
$gl_caption['c_land_title'] = 'Grundstück';
$gl_caption['c_ordered_title'] = 'Geordnet nach';
$gl_caption['c_user_title'] = 'Agents';
$gl_caption['c_custom_title'] = 'Konfigurationseinstellungen';
$gl_caption['c_m2'] = 'm2';
$gl_caption['c_ha'] = 'ha';
$gl_caption['c_showwindow'] = 'Schaufenster drucken';
$gl_caption['c_album'] = 'Album drucken';


// NO TRADUIT

$gl_caption['c_images'] = 'Bilder';
$gl_caption['c_no_available'] = 'Diese Immobilie ist nicht mehr verfügbar!';
$gl_caption['c_private'] = 'Private ausblenden';
$gl_caption_tv['c_send_edit'] = 'In den Fenster hinzufügen';
$gl_caption_tv['c_download'] = 'Files';
$gl_caption_tv['c_main_files'] = 'Main files';
$gl_caption_tv['c_property_file'] = 'Immobilien';

$gl_caption['c_type_title'] = 'von Typ';


$gl_messages_tv['list_saved']='Das Schaufenster wurde erfolgreich geändert wurde';


$gl_caption_property['c_living'] = 'Wohnzimmer';
$gl_caption_property['c_dinning'] = 'Speisezimmer';
$gl_caption_property['c_service_room'] = 'Service';
$gl_caption_property['c_service_bath'] = 'Badezimmer';
$gl_caption_property['c_study'] = 'Studie';
$gl_caption_property['c_courtyard'] = 'Garten';
$gl_caption_property['c_courtyard_area'] = 'Oberfläche';
$gl_caption_property['c_floor'] = 'Floor';
$gl_caption_property['c_parket'] = 'Parket';
$gl_caption_property['c_ceramic'] = 'Ceramic';
$gl_caption_property['c_gres'] = 'Gres';
$gl_caption_property['c_laminated'] = 'Laminated';
$gl_caption_property['c_estrato'] = 'Estrato';
$gl_caption_property['c_estrato_e1'] = '1';
$gl_caption_property['c_estrato_e2'] = '2';
$gl_caption_property['c_estrato_e3'] = '3';
$gl_caption_property['c_estrato_e4'] = '4';
$gl_caption_property['c_estrato_e5'] = '5';
$gl_caption_property['c_estrato_e6'] = '6';
$gl_caption_property['c_edit_map'] = 'Karte';
$gl_caption_property['c_edit_booking'] = 'Buchung';
$gl_caption_property['c_edit_booking_extra'] = 'Extras';
$gl_caption_property['c_edit_booking_season'] = 'Jahreszeit';
$gl_caption_property['c_form_map_title'] = 'Lage der Immobilie';
$gl_caption_property['c_form_booking_title'] = 'Reservierungen Immobilien';
$gl_caption_property['c_form_booking_extra_title'] = 'Zusatz Dienstleistungen vom Haus';
$gl_caption_property['c_form_booking_season_title'] = 'Verschiedene Saison Preise ';
$gl_caption_property['c_save_map'] = 'Standort speichern ';
$gl_caption_property['c_latitude'] = 'Breite';
$gl_caption_property['c_longitude'] = 'Länge';
$gl_caption_property ['c_center_longitude'] = 'Map center latitude';
$gl_caption_property ['c_center_latitude'] = 'Map center longitude';
$gl_caption_property ['c_center_coords'] = 'Center';
$gl_caption_property['c_click_map'] = 'Klicken Sie auf einen Punkt auf der Karte, um den Standort festgelegt';
$gl_caption_property['c_show_map'] = 'Mostrar mapa part pública';
$gl_caption_property['c_show_map_point'] = 'Mostrar punt';
$gl_messages['point_saved'] = 'Neuer Standort richtig gelagert';

$gl_caption_property['c_tipus_temp'] = 'Touristen mieten';
$gl_caption_property['c_tipus_moblat'] = 'Eine möblierte Wohnung mieten';
$gl_caption_property['c_tipus_selloption'] = 'Miete mit Kaufoption';

$gl_caption_property['c_efficiency'] = 'Energiezertifikat';
$gl_caption_property['c_efficiency_'] = '';
$gl_caption_property['c_efficiency_progress'] = 'In Bearbeitung';
$gl_caption_property['c_efficiency_exempt'] = 'Befreien';
$gl_caption_property['c_efficiency_a'] = 'A';
$gl_caption_property['c_efficiency_b'] = 'B';
$gl_caption_property['c_efficiency_c'] = 'C';
$gl_caption_property['c_efficiency_d'] = 'D';
$gl_caption_property['c_efficiency_e'] = 'E';
$gl_caption_property['c_efficiency_f'] = 'F';
$gl_caption_property['c_efficiency_g'] = 'G';

$gl_caption_property['c_efficiency2_a'] = 'A';
$gl_caption_property['c_efficiency2_b'] = 'B';
$gl_caption_property['c_efficiency2_c'] = 'C';
$gl_caption_property['c_efficiency2_d'] = 'D';
$gl_caption_property['c_efficiency2_e'] = 'E';
$gl_caption_property['c_efficiency2_f'] = 'F';
$gl_caption_property['c_efficiency2_g'] = 'G';
$gl_caption_property['c_efficiency2_notdefined'] = '';
$gl_caption_property['c_efficiency_date']='Datum';


$gl_caption_property['c_filter_room'] = 'Zimmer';
$gl_caption_property['c_filter_room_0'] = 'Keine';

$gl_caption_property['c_a4'] = 'A4';
$gl_caption_property['c_a3'] = 'A3';
$gl_caption_property['c_caption_showwindow_top_fields'] = 'Unbegrenzt';
$gl_caption_property['c_caption_showwindow_pages'] = $gl_caption_property['c_caption_album_pages'] = 'Alle';
$gl_caption_property['c_submenu_0'] = 'Sprache';
$gl_caption_property['c_submenu_1'] = 'Papierformat';
$gl_caption_property['c_submenu_2'] = 'Anzahl der Seiten';
$gl_caption_property['c_submenu_3'] = 'Limit-Felder';
$gl_caption_property['c_submenu_4'] = 'Zeigen';
$gl_caption_property['c_submenu_5'] = 'Speichern Sie diese Einstellung';
$gl_caption_property['c_submenu_6'] = 'Schlabone';

$gl_caption_property['c_views'] = 'Views';
$gl_caption_property['c_sea_view'] = 'Meerblick';
$gl_caption_property['c_clear_view'] = 'Clear view';
// FI NO TRADUIT

// nomes admin
$gl_caption_property['c_custom1'] = $gl_caption_property['c_custom2'] = $gl_caption_property['c_custom3'] = $gl_caption_property['c_custom4'] = $gl_caption_property['c_custom5'] = $gl_caption_property['c_custom6'] = $gl_caption_property['c_custom7'] = $gl_caption_property['c_custom8'] = '';









$gl_caption_property['c_user_id'] = 'Agent';
$gl_caption_property['c_filter_user_id'] = 'Alle Agenten';

$gl_caption_property['c_person'] = 'Persons';
$gl_caption_property['c_person_s'] = 'Person';
$gl_caption_property['c_person_p'] = 'Persons';


$gl_caption['c_edit_history'] = 'Geschichte';

$gl_caption_history['c_new_history'] = 'Neue Eintrag';
$gl_caption_history['c_form_history_title'] = 'Geschichte der Immobilie:';
$gl_caption_history['no_assigned'] = $gl_messages_history['no_records'] = 'Keine Geschichte auf diesem Grundstück ';
$gl_caption_history ['c_custumer_id_format']='%s %s %s';
$gl_caption_history ['c_delete_button']='Abnehmen';

$gl_messages_history['added']='L\'entrada s\'ha afegit correctament';
$gl_messages_history['saved']='L\'entrada ha estat modificada correctament';


$gl_caption_property['c_exact_result'] = 'Gefunden<strong>1</strong> Antrag die diese Eigenschaft übereinstimmt';
$gl_caption_property['c_exact_results'] = 'Gefunden <strong>%s</strong> Antrag die diese Eigenschaft übereinstimmen';
$gl_caption_property['c_exact_results_link'] = 'Ergebnisse anzeigen';
$gl_caption_property['c_aprox_result'] = 'Gefunden <strong>1</strong> annähern Antrag die diese Eigenschaft übereinstimmen';
$gl_caption_property['c_aprox_results'] = 'Gefunden <strong>%s</strong> annähern Antragdie diese Eigenschaft übereinstimmen';
$gl_caption_property['c_aprox_results_link'] = 'Ergebnisse anzeigen';
$gl_caption ['c_user_id_format']='%s %s';

$gl_caption['c_property_file_name'] = $GLOBALS['gl_caption']['c_file_name'];
$gl_caption['c_property_old_file_name'] = $GLOBALS['gl_caption']['c_old_file_name'];

$gl_caption['c_send_mail'] = 'Enviar email';

$gl_caption_property['c_expired'] = 'Ablaufsdatum';
$gl_caption_property['c_construction_date'] = 'Baujahr';
$gl_caption_property['c_facing'] = 'Orientierung';
$gl_caption_property['c_facing_'] = '';
$gl_caption_property['c_facing_n'] = 'Nord';
$gl_caption_property['c_facing_ne'] = 'Nordosten';
$gl_caption_property['c_facing_e'] = 'Osten';
$gl_caption_property['c_facing_se'] = 'Südosten';
$gl_caption_property['c_facing_s'] = 'Süd';
$gl_caption_property['c_facing_so'] = 'Südwesten';
$gl_caption_property['c_facing_o'] = 'Westen';
$gl_caption_property['c_facing_no'] = 'Nordwesten';
$gl_caption_property['c_exclusive'] = 'Exklusivität';
$gl_caption_property['c_zip'] = 'Codi postal';
$gl_caption_property['c_descriptionregister'] = 'Descripció registre';
$gl_caption['c_list_mls'] = 'List mls properties';
$gl_caption['c_phone1'] = 'Telefon';
$gl_caption['c_comercialname'] = 'Company';
$gl_caption['c_client_id']='';
$gl_caption['c_client_dir']='';
$gl_caption['c_domain']='';

$gl_caption['c_star']='Stars';
$gl_caption['c_star_0']='None';
$gl_caption['c_star_1']='1';
$gl_caption['c_star_2']='2';
$gl_caption['c_star_3']='3';
$gl_caption['c_star_4']='4';
$gl_caption['c_star_5']='5';

// Booking
$gl_caption_property['c_book_search_title'] = 'Verfügbarkeiten suchen';
$gl_caption['c_dates_from'] = 'Von';
$gl_caption['c_dates_to'] = 'bis';
$gl_caption_property['c_date_in'] = 'Anreisedatum';
$gl_caption_property['c_date_out'] = 'Abreisedatum';
$gl_caption_property['c_adult'] = 'Erwachsene';
$gl_caption_property['c_child'] = 'Kinder';
$gl_caption_property['c_baby'] = 'Babys';
$gl_caption_property['c_book_property_button'] = 'Buchen';
$gl_caption_property['c_time_in'] = 'Anreisezeit';
$gl_caption_property['c_time_out'] = 'Abreisezeit';

$gl_caption_property['c_marble'] = 'Marmor';
$gl_caption_property['c_hut'] = 'HUT';
$gl_caption_property['c_level_count'] = 'Anzahl Stockwerke';

$gl_caption_property['c_managed_by_owner'] = 'Managed by owner';

// Traduccions
$gl_caption_adjective['c_gender'] = 'Génere';
$gl_caption_adjective['c_adjective_before'] = 'Antes';
$gl_caption_adjective['c_adjective_after'] = 'Después';

// Portal
$gl_caption['c_portal_config_title'] = "Publishing options";
$gl_caption['c_apicat_export_all'] = "Export all properties";
$gl_caption['c_apicat_export_all_text'] = "If you check, all public properties will be exported without the need of selecting any of them";
$gl_caption['c_apicat_export_all_text_2'] = "Only properties with price and ZIP Code or exact location will be exported, Api.cat doesn't allow tourist rental. Fotocasa needs floor space";
$gl_caption['c_excluded_'] = "Exportable - No exportable";
$gl_caption['c_excluded_apicat'] = "Excluded Apicat";
$gl_caption['c_excluded_fotocasa'] = "Excluded Fotocasa";
$gl_caption['c_excluded_habitaclia'] = "Excluded Habitaclia";

$gl_caption['c_no_subscrit'] = "Information on portals publication";
$gl_caption['c_no_subscrit_apicat'] = "You are not registered to publish on Api.cat";
$gl_caption['c_no_subscrit_fotocasa'] = "You are not registered to publish on Fotocasa";
$gl_caption['c_no_subscrit_habitaclia'] = "You are not registered to publish on Habitaclia";

$gl_caption['c_portal_check_title'] = "The following data is needed to export";
$gl_caption['c_fotocasa_check_price'] = "Price";
$gl_caption['c_fotocasa_check_latitude'] = "Map location";
$gl_caption['c_fotocasa_check_floor_space'] = "Floor space";

$gl_caption['c_habitaclia_check_ref'] = "Reference";
$gl_caption['c_habitaclia_check_adress'] = "Address and map location with \"Show point\" checked";

$gl_caption['c_apicat_check_tipus'] = "Turístic rent not allowed";
$gl_caption['c_apicat_check_consult'] = "Uncheck \"Price on application\"";
$gl_caption['c_apicat_check_zip'] = "Postal code or map location with \"Show point\" checked";