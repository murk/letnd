<?

/* 20-3-13 
  Afegir caracteristiques a inmo



 */
if (is_table("inmo__property")) {

	$update_sql[] = "
					ALTER TABLE `custumer__history`
						ADD COLUMN `property_id` INT(11) NOT NULL DEFAULT '0' AFTER `history`;
					";	
	$update_sql[] = "
					ALTER TABLE `custumer__custumer`
						ADD COLUMN `user_id` INT(11) NOT NULL DEFAULT '0' AFTER `enteredc`;
					";	
	$update_sql[] = "
					ALTER TABLE `inmo__property`
						ADD COLUMN `custom1` VARCHAR(50) NOT NULL AFTER `zoom`,
						ADD COLUMN `custom2` VARCHAR(50) NOT NULL AFTER `custom1`,
						ADD COLUMN `custom3` VARCHAR(50) NOT NULL AFTER `custom2`,
						ADD COLUMN `custom4` VARCHAR(50) NOT NULL AFTER `custom3`,
						ADD COLUMN `user_id` INT(11) NOT NULL DEFAULT '0' AFTER `custom4`;
					";	

	$update_sql[] = "
					ALTER TABLE `inmo__property_language`
						ADD COLUMN `custom1` VARCHAR(255) NOT NULL DEFAULT '' AFTER `property_title`,
						ADD COLUMN `custom2` VARCHAR(255) NOT NULL DEFAULT '' AFTER `custom1`,
						ADD COLUMN `custom3` VARCHAR(255) NOT NULL DEFAULT '' AFTER `custom2`,
						ADD COLUMN `custom4` VARCHAR(255) NOT NULL DEFAULT '' AFTER `custom3`;
					";	
	$update_sql[] = "
					CREATE TABLE `inmo__configadmin_language` (
						`configadmin_id` INT(11) NOT NULL DEFAULT '0',
						`name` VARCHAR(50) NOT NULL DEFAULT '' COLLATE 'utf8_spanish2_ci',
						`value` VARCHAR(255) NOT NULL DEFAULT '' COLLATE 'utf8_spanish2_ci',
						`language` CHAR(3) NOT NULL DEFAULT '' COLLATE 'utf8_spanish2_ci'
					)
					COLLATE='utf8_spanish2_ci'
					ENGINE=MyISAM;
					";
	
	
	$update_sql[]="
			INSERT INTO `inmo__configadmin` (`name`) 
			VALUES ('custom1_name');";
	$update_sql[]="
			INSERT INTO `inmo__configadmin` (`name`) 
			VALUES ('custom1_type');";
	$update_sql[]="
			INSERT INTO `inmo__configadmin` (`name`) 
			VALUES ('custom1_filter');";
	
	
	$update_sql[]="
			INSERT INTO `inmo__configadmin` (`name`) 
			VALUES ('custom2_name');";
	$update_sql[]="
			INSERT INTO `inmo__configadmin` (`name`) 
			VALUES ('custom2_type');";
	$update_sql[]="
			INSERT INTO `inmo__configadmin` (`name`) 
			VALUES ('custom2_filter');";
	
	
	$update_sql[]="
			INSERT INTO `inmo__configadmin` (`name`) 
			VALUES ('custom3_name');";
	$update_sql[]="
			INSERT INTO `inmo__configadmin` (`name`) 
			VALUES ('custom3_type');";
	$update_sql[]="
			INSERT INTO `inmo__configadmin` (`name`) 
			VALUES ('custom3_filter');";
	
	
	$update_sql[]="
			INSERT INTO `inmo__configadmin` (`name`) 
			VALUES ('custom4_name');";
	$update_sql[]="
			INSERT INTO `inmo__configadmin` (`name`) 
			VALUES ('custom4_type');";
	$update_sql[]="
			INSERT INTO `inmo__configadmin` (`name`) 
			VALUES ('custom4_filter');";
	
	
	global $gl_languages;
	foreach($gl_languages['names'] as $lang=>$val){
		$update_sql[]="
			INSERT INTO `inmo__configadmin_language` (`name`, `language`) 
			VALUES ('custom1_name', '".$lang."');";
		$update_sql[]="
			INSERT INTO `inmo__configadmin_language` (`name`, `language`) 
			VALUES ('custom2_name', '".$lang."');";
		$update_sql[]="
			INSERT INTO `inmo__configadmin_language` (`name`, `language`) 
			VALUES ('custom3_name', '".$lang."');";
		$update_sql[]="
			INSERT INTO `inmo__configadmin_language` (`name`, `language`) 
			VALUES ('custom4_name', '".$lang."');";
	}
}
?>