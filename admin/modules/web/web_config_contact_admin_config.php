<?php

	$gl_db_classes_language_fields = array (
  0 => '',
);
	$gl_db_classes_fields = array (
  'configadmin_id' =>
  array (
    'type' => 'hidden',
    'enabled' => '1',
    'form_admin' => '0',
    'order' => '0',
    'default_value' => '',
    'override_save_value' => '',
    'group' => '',
    'class' => '',
    'javascript' => '',
    'text_size' => '',
    'text_maxlength' => '11',
    'textarea_cols' => '',
    'textarea_rows' => '',
    'select_caption' => '',
    'select_size' => '',
    'select_table' => '',
    'select_fields' => '',
    'select_condition' => '',
  ),
 'custumer_table' =>
  array (
    'type' => 'select',
    'enabled' => '1',
    'form_admin' => 'admintotal',
    'order' => '2',
    'default_value' => '',
    'override_save_value' => '',
    'class' => '',
    'javascript' => '',
    'text_size' => '80',
    'text_maxlength' => '255',
    'textarea_cols' => '',
    'textarea_rows' => '',
    'select_caption' => '',
    'select_size' => '',
    'select_table' => '',
    'select_fields' => 'contact__contact,newsletter__custumer,custumer__custumer',
    'select_condition' => '',
    'group' => '',
  )
);
	$gl_db_classes_list = array (
  'cols' => '1',
);
	$gl_db_classes_form = array (
  'cols' => '1',
  'images_title' => '',
);
	?>