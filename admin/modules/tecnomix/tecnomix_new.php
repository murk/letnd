<?
/**
 * TecnomixNew
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2011
 * @version $Id$
 * @access public
 */
class TecnomixNew extends Module{
	var $category_menu_id=0;
	function __construct(){
		parent::__construct();
		$this->parent = 'TecnomixNew';
	}
	/* funció que es crida per fer una acció del modul automatica o desde un altre modul ( list_records, show_form ) etc...*/
	function do_action($action = ''){
		switch ($this->parent){		
			case 'TecnomixNew':
				$this->set_field ('category_id','type','hidden');
				$this->set_field('category_id','default_value','1');	
			break;
			case 'TecnomixProjecte':			
				$this->unset_field ('in_home');				
				$this->set_field ('category_id','type','hidden');
				$this->set_field('category_id','default_value','2');
			break;	
			case 'TecnomixEquip':			
				$this->unset_field ('in_home');				
				//$this->category_menu_id = 3706;				
				$this->set_field ('category_id','type','hidden');	
				$this->set_field('category_id','default_value','3');
				$this->config['im_thumb_w'] = '214';	
				$this->config['im_thumb_h'] = '161';
				$this->config['im_details_w'] = '214';	
				$this->config['im_details_h'] = '';
			break;		
			case 'TecnomixInstalacio':			
				$this->unset_field ('in_home');				
				//$this->category_menu_id = 3606;				
				$this->set_field ('category_id','type','hidden');	
				$this->set_field('category_id','default_value','4');
				$this->config['im_thumb_w'] = '214';	
				$this->config['im_thumb_h'] = '161';
				$this->config['im_details_w'] = '214';	
				$this->config['im_details_h'] = '';
			break;
		}
		// el camp parent_module nomès el tinc quan creo un registre nou
		// un cop creat el registre no es modifica mai aquest camp, el registre pertany sempre a aquella secció
	    if ($this->action=='add_record') {
			$this->add_field('parent_module'); 
			$this->set_field ('parent_module','override_save_value',"'" . $this->parent . "'"); // sempre sobreescric el parent_module
		}
		return parent::do_action($action);
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function get_records()
	{
	    $listing = new ListRecords($this);
		$listing->call_function['content'] = 'add_dots';
		$listing->condition = "parent_module = '" . $this->parent . "'";
	
		if (isset($_GET['category_id']))
		{
			$listing->condition .= ' AND category_id = ' . $_GET['category_id'];
		}
		else
		{
			switch ($this->parent){	
				case 'TecnomixInstalacio':	
				case 'TecnomixEquip':
					//$listing->add_filter('category_id');
				break;	
			}
		}
		$listing->add_filter('status');
		$listing->order_by = 'ordre ASC, entered DESC';		
	    return $listing->list_records();
	}
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{
	    // si no hi ha cap categoria faig un redirect a entrar categoria nova
		/*if (!Db::get_first("SELECT count(*) FROM tecnomix__category WHERE parent_module = '".$this->parent."category' LIMIT 0,1")){
			$_SESSION['message'] = $this->messages['no_category'];
			redirect('/admin/?menu_id=' . $this->category_menu_id);
		}*/
		$this->set_field('entered','default_value',now(true));
		$show = new ShowForm($this);
	    return $show->show_form();
	}

	function save_rows()
	{
	    $save_rows = new SaveRows($this);
	    $save_rows->save();
	}

	function write_record()
	{
	    $writerec = new SaveRows($this);		
		$writerec->field_unique = "new_file_name";
		$writerec->save();
	}

	function manage_images()
	{
	    $image_manager = new ImageManager($this);
	    $image_manager->execute();
	}
}
?>