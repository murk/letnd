<?
define("MENU_HOME_PUBLIC","Website");
define("MENU_HOME","Startseite");
define("MENU_LOGOUT","Verbindung trennen");


// configurable
define("USER_TEXT","benutzer");
define('USER_LOGIN_INVALID','The user name or password are not valid');
define('USER_LOGIN_VALID','Wellcome to ' . HOST_URL);

// la linea de sota no es pot tocar, que te dit que no la toquis!!!!!!!
// menus
define('EXPERIENCIA_MENU_ROOT','Erfahrung');
define('NEWSLETTER_MENU_ROOT','Newsletter');
define('TRANSLATION_MENU_ROOT','Übersetzungen');
define('FACTURACIO_MENU_ROOT','Berechnung');
define('STATISTICS_MENU_ROOT','Statistiken');
define('USER_MENU_ROOT','Benutzermanagement');
define('ADMINTOTAL_MENU_ROOT','Gesamtmanagement');
define('WEB_MENU_ROOT','Letnd-Management');
define('INMO_MENU_ROOT','Immobilien');
define('OWNER_MENU_ROOT','Eigentümer');
define('REPORT_MENU_ROOT','Dokumente');
define('CONFIG_MENU_ROOT','Einstellung');
define('NEWS_MENU_ROOT','Nachrichten');
define('GALERY_MENU_ROOT','Bildergalerie');
define('RENOVATS_MENU_ROOT','Gratispostkarten');
define('SHOPWINDOW_MENU_ROOT','Schaufenster');
define('UTILITY_MENU_ROOT','Hilfsprogramme');
define('MLS_MENU_ROOT','MLS');
define('CUSTUMER_MENU_ROOT','Kunden');
define('WORKS_MENU_ROOT','Stellenangebote');
define('PRODUCT_MENU_ROOT','Produkte');
define('RATE_MENU_ROOT','Tarife');
define('CARS_MENU_ROOT','Cotxes');
define('NEW_MENU_ROOT','Noticies');
define('PORTFOLI_MENU_ROOT','Portfoli');
define('CONTACT_MENU_ROOT','Contacte');
define('PRINT_MENU_ROOT','Drucken');
define('BOOKING_MENU_ROOT','Bookings');
define('MISSATGE_MENU_ROOT','Messages');
define('CUSTOMER_MENU_ROOT','Clients and orders');
define('DIRECTORY_MENU_ROOT','Files');
define('SELLPOINT_MENU_ROOT','Sell points');



define('DB_SPLIT_TITLE_FIRST_PAGE', 'Start');
define('DB_SPLIT_TITLE_PREVIOUS_PAGE', 'Vorherige Seite');
define('DB_SPLIT_TITLE_NEXT_PAGE', 'Folgende Seite');
define('DB_SPLIT_TITLE_LAST_PAGE', 'Ende');
define('DB_SPLIT_TITLE_PAGE_NO', 'Seite %d');
define('DB_SPLIT_TITLE_PREV_SET_OF_NO_PAGE', 'Vorherige %d Seiten');
define('DB_SPLIT_TITLE_NEXT_SET_OF_NO_PAGE', 'Folgende %d Seiten');
define('DB_SPLIT_BUTTON_FIRST', '&lt;&lt;START');
define('DB_SPLIT_BUTTON_PREV', '&lt;&lt;&nbsp;Vorherige');
define('DB_SPLIT_BUTTON_NEXT', 'Folgende&nbsp;&gt;&gt;');
define('DB_SPLIT_BUTTON_LAST', 'ENDE&gt;&gt;');
define('DB_SPLIT_DISPLAY_NUMBER_OF_RESULTS', 'Anzeige von <b>%d</b> bis <b>%d</b> (von <b>%d</b>)');
define ('TITLE_SEARCH','Suchergebnisse');

define('DB_SPLIT_PAGE_TITLE', 'Seite');
define('DB_SPLIT_PAGE_DESCRIPTION', 'Anzeige von %d bis %d von %d');

$month_names = array("Januar", "Februar", "März", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Dezember");
$week_names = array("Sonntag", "Montag", "Dienstag", "Mittwoch", "Donnerstag", "Freitag", "Samstag");


// Caption default
$gl_caption['c_form_title'] = 'Daten bearbeiten';
$gl_caption['c_print_title'] = 'Daten';
$gl_caption['c_config_title'] = 'Einstellung bearbeiten';
$gl_caption['c_edit'] = 'Bearbeiten';


$gl_caption['c_edit_button'] = 'Daten bearbeiten';
$gl_caption['c_view_button'] = 'Daten anzeigen';
$gl_caption['c_edit_images_button'] = 'Bilder bearbeiten';
$gl_caption['c_add_images_button'] = 'Bilder anhängen';



$gl_caption['c_edit_data'] = 'Daten bearbeiten';
$gl_caption['c_edit_images'] = 'Bilder';
$gl_caption['c_add_images'] = 'Bilder anhängen';
$gl_caption['c_search_form'] = 'Suchen';
$gl_caption['c_send_new'] = 'Speichern';
$gl_caption['c_send_edit'] = 'Änderungen speichern';
$gl_caption['c_delete'] = 'Löschen';
$gl_caption['c_action'] = 'Aktion';
$gl_caption['c_delete_selected'] = 'Auswahl löschen';
$gl_caption['c_restore_selected'] = 'Auswahl wiederherstellen';
$gl_caption['c_confirm_deleted'] = 'Die ausgewählten Elemente werden endgültig aus der Datenbank gelöscht. \\n\\nMöchten Sie fortfahren?';
$gl_caption['c_confirm_images_deleted'] = 'Die ausgewählten Bilder werden endgültig gelöscht. \\n\\nMöchten Sie fortfahren?';
$gl_caption['c_confirm_form_bin'] = 'Das Verzeichnis wird in den Papierkorb verschoben. \\n\\nMöchten Sie fortfahren?';
$gl_caption['c_confirm_form_deleted'] = 'Das Verzeichnis wird endgültig aus der Datenbank gelöscht. \\n\\nMöchten Sie fortfahren?';
$gl_caption['c_move_to'] = 'Verschiebung nach:';
$gl_caption['c_print_all'] = 'Alles drucken';
$gl_caption['c_print'] = 'Drucken';
$gl_caption['c_order_by'] = 'Ordnen nach:';
$gl_caption['c_select'] = 'Auswählen';
$gl_caption['c_multislect_selectable'] = 'Fields';
$gl_caption['c_multislect_selected'] = 'Selected fields';
$gl_caption['c_all'] = 'alle';
$gl_caption['c_nothing'] = 'keine';
$gl_caption['c_preview'] = 'Voranzeige';
$gl_caption['c_yes'] = 'Ja';
$gl_caption['c_no'] = 'Nein';
$gl_caption['c_options_bar'] = 'Optionen';
$gl_caption['c_file_maximum'] = 'jeweils maximal %s Dateien';
$gl_caption['c_file_maximum_1'] = 'jeweils maximal 1 Datei';

// buscadors
$gl_caption['c_search_title'] = 'Suchen';
$gl_caption['c_by_ref'] = '(oder Referenzcodes getrennt durch Leerzeichen, z.B.: "456 245 45")';
$gl_caption['c_by_words'] = 'Per paraules';

// sobreescrits caption images
$gl_caption_image['c_edit'] = 'Bilder von';
$gl_caption_image['c_insert'] = 'Bilder aus Ihrem Computer anhängen';
$gl_caption_image['c_insert_advanced'] = 'Advanced (java)';
$gl_caption_image['c_delete_selected'] = 'Ausgewählte Bilder löschen';
$gl_caption_image['c_confirm_deleted'] = 'Die Bilder werden endgültig gelöscht.\\n\\n\\nMöchten Sie fortfahren?';
$gl_caption_image['c_select_all'] = 'Alle auswählen';
$gl_caption_image['c_deselect_all'] = 'Gesamte Auswahl aufheben';

// nous images
$gl_caption_image['c_stop'] = 'Anhalten';
$gl_caption_image['c_refresh'] = 'Aktualisieren';
$gl_caption_image['c_add'] = 'Bilder anhängen';
$gl_caption_image['c_thumbnails'] = 'Bilder';
$gl_caption_image['c_icons'] = 'Symbole';
$gl_caption_image['c_list'] = 'Liste';
$gl_caption_image['c_details'] = 'Einzelheiten';
$gl_caption_image['c_progress_dialog_title_text'] = 'Bilder werden angehängt';

// camps images
$gl_caption_image['c_ordre'] = 'Reihenfolge';
$gl_caption_image['c_main_image'] = 'Hauptbild';
$gl_caption_image['c_name'] = 'Name';
$gl_caption_image['c_imagecat_id'] = 'Category';
$gl_caption_image['c_image_alt'] = 'Alt';
$gl_caption_image['c_image_title'] = 'Title';
$gl_caption_image['c_edit_titles_images'] = 'Editar títols imatges';
$gl_caption_image['c_hide_titles_images'] = 'Amagar títols imatges';

// sobreescrits caption traduccions i traduccions_news.tpl
$gl_caption_translation['c_send_edit'] = 'Zum Übersetzen verschicken';
$gl_caption_translation['c_page_words_title'] = 'Gesamtzahl der zu übersetzenden Wörter auf dieser Seite';
$gl_caption_translation['c_all_page_words_title'] = 'Gesamtzahl der zu übersetzenden Wörter auf allen Seiten';
$gl_caption_translation['c_total'] = 'Gesamt';

// sobreescrits end


// Message default
$gl_messages['title_edit']='Bearbeiten';
$gl_messages['title_copy']='Daten kopieren';
$gl_messages['added']='Das Verzeichnis wurde ordnungsgemäß angehängt.';
$gl_messages['add_exists']='Es existiert bereits ein Verzeichnis mit dieser/-m %1$s: %2$s';
$gl_messages['saved']='Das Verzeichnis wurde ordnungsgemäß geändert.';
$gl_messages['not_saved']='Das Verzeichnis wurde nicht geändert.';
$gl_messages['no_records']='In dieser Liste ist kein Verzeichnis vorhanden.';
$gl_messages['no_records_bin']='Im Papierkorb ist kein Verzeichnis vorhanden.';
$gl_messages['list_saved']='Die Änderungen der Verzeichnisse wurden ordnungsgemäß durchgeführt.';
$gl_messages['list_not_saved']='Es wurde kein Verzeichnis geändert, es wurden keine Änderungen entdeckt.';
$gl_messages['list_deleted']='Die Verzeichnisse wurden gelöscht.';
$gl_messages['list_bined']='Die Verzeichnisse wurden in den Papierkorb verschoben.';
$gl_messages['list_restored']='Die Verzeichnisse wurden ordnungsgemäß wiederhergestellt.';
$gl_messages['concession_no_acces']='Sie sind nicht für den Zugriff auf diesen Bereich autorisiert.';
$gl_messages['concession_no_write']='Sie sind nicht zum Überschreiben in diesem Bereich autorisiert.';
$gl_messages['select_item']='Klicken Sie ein Verzeichnis der Liste an, um es auszuwählen.';
$gl_messages['related_not_deleted']='Die markierten Verzeichnisse können nicht gelöscht werden, da verbundene Verzeichnisse vorhanden sind.';



// sobreescrits message image
$gl_messages_image['list_saved']='Die Änderungen an den Bildern wurden ordnungsgemäß durchgeführt.';
$gl_messages_image['list_not_saved']='Es wurde kein Verzeichnis geändert, es wurden keine Änderungen entdeckt.';
$gl_messages_image['list_deleted']='Die Bilder wurden gelöscht.';
// sobreescrits end


// NO TRADUIT
$gl_messages['deleted']='El registre ha estat esborrat';
$gl_messages['bined']='El registre  s\'ha enviat a la paperera de reciclatge';
$gl_messages['restored']='El registre s\'ha recuperat correctament';
$gl_caption['c_ok'] = 'Acceptar';
$gl_caption['c_cancel'] = 'Cancelar';
$gl_caption ['c_close']='Zumachen';
$gl_messages['post_max_size']='El pes del formulari (contingut + arxius adjunts) excedeix el límit de ';
$gl_messages['post_file_max_size']='El pes dels arxius adjunts excedeix el límit de ';
$gl_messages['assigned']='Registres sel·leccionats';
$gl_caption['c_black']='Negre';
$gl_caption['c_white']='Blanc';
$gl_caption['c_silver']='Plata';
$gl_caption['c_grey']='Verd';
$gl_caption['c_blue']='Blau';
$gl_caption['c_red']='Vermell';
$gl_caption['c_orange']='Taronja';
$gl_caption['c_green']='Verd';
$gl_caption['c_purple']='Lila';
$gl_caption['c_pink']='Rosa';
$gl_caption['c_golden']='Daurat';
$gl_caption['c_yellow']='Groc';
$gl_caption['c_maroon']='Grana';
$gl_caption['c_brown']='Marró';
//creo un array global per els mesos
$gl_caption_months = array("Gener", "Febrer", "Març", "Abril", "Maig", "Juny", "Juliol", "Agost", "Setembre", "Octubre", "Novembre", "Desembre");

$gl_caption ['c_2']='dos';
$gl_caption ['c_3']='tres';
$gl_caption ['c_4']='quatre';
$gl_caption ['c_5']='cinc';
$gl_caption ['c_6']='sis';
$gl_caption ['c_7']='set';
$gl_caption ['c_8']='vuit';
$gl_caption ['c_9']='nou';
$gl_caption['c_10']='deu';
$gl_caption['c_11']='onze';
$gl_caption['c_12']='dotze';
$gl_caption['c_13']='tretze';
$gl_caption['c_14']='catorze';
$gl_caption['c_15']='quinze';
$gl_caption['c_16']='setze';
$gl_caption['c_17']='diset';
$gl_caption['c_18']='divuit';
$gl_caption['c_19']='dinou';
$gl_caption['c_20']='vint';
$gl_caption['c_30']='trenta';
$gl_caption['c_40']='quaranta';
$gl_caption['c_50']='cinquanta';
$gl_caption['c_60']='seixanta';
$gl_caption['c_70']='setanta';
$gl_caption['c_80']='vuitanta';
$gl_caption['c_90']='noaranta';
$gl_caption['c_1000']='mil';


$gl_caption['c_add_button'] = 'Afegir registre';

$gl_caption['c_login'] = 'Login';
$gl_caption['c_password'] = 'Password';
$gl_caption['c_mail'] = 'E-mail';
$gl_caption['c_mail_repeat'] = 'Repeat E-mail';
$gl_caption['c_password_repeat'] = 'Repeat password';
$gl_caption['c_noscript'] = '<p><strong>El seu navegador no soporta o té desactivat Javascript. </strong></p><p>Ha d\'<a href="//www.google.com/support/websearch/bin/answer.py?answer=23852">habilitar Javascript</a> per poder utilitzar Letnd</p>';
$gl_caption['c_cookie'] = '<p><strong>El seu navegador no soporta o té desactivades les Cookies. </strong></p><p>Ha d\'<a href="//www.google.com/support/websearch/bin/answer.py?answer=35851">habilitar les Cookies</a> per poder utilitzar Letnd</p>';

$gl_messages_image['images_uploaded']='Imatges guardades';
$gl_messages['form_sent']='El formulari ha estat enviat correctament';
$gl_messages['form_not_sent']='El formulari no s\'ha pogut enviar';


$gl_caption['c_search_adress'] = 'Buscar adreça';
$gl_caption['c_map_frame_1'] = 'S\'ha trobat coincidència per';
$gl_caption['c_map_frame_2'] = 'Si l\'ubicació és correcta';
$gl_caption['c_map_frame_3'] = 'clica aquí per guardar';
$gl_caption['c_map_frame_4'] = 'No s\'ha trobat cap coincidència';

$gl_caption['c_list_view_1'] = 'Normal';
$gl_caption['c_list_view_2'] = 'Big icons';
$gl_caption['c_list_view_3'] = 'List';

$gl_caption['c_edit_list_button_1'] = 'No editar llistat';
$gl_caption['c_edit_list_button_'] = 'Editar llistat';
$gl_caption['c_add_dots'] = 'Retallar text';

// SEO
$gl_caption['c_seo']='Eines SEO ( Posicionament web )';
$gl_caption['c_page_title']='Títol de la pàgina. max. 66-69 caràcters. Òptim < 65 car:';
$gl_caption['c_page_description']='Descripció de la pàgina. max. 156-158 caràcters:';
$gl_caption['c_page_keywords']='Paràules clau:';
$gl_caption['c_file_name']='Nom de la pàgina en URL. Ex. www.lamevaweb.com/<b>nom-de-la-pagina</b>.htm max. 90 caràcters:';
$gl_caption['c_file_name_excel']='Nom pàgina';
$gl_caption['c_old_file_name']='Nom vell de la pàgina en URL.';



// SOCIAL
$gl_caption['c_edit_social'] = 'Xarxes socials';
$gl_caption['c_edit_social_title'] = 'Publicar';
$gl_caption['c_social_language'] = 'Idioma';
$gl_caption['c_facebook_message'] = 'Missatge';
$gl_caption['c_facebook_picture'] = 'Imatge';
$gl_caption['c_facebook_link'] = 'Enllaç';
$gl_caption['c_facebook_link_explanation'] = 'Agafa els texts directament de la pàgina';
$gl_caption['c_facebook_name'] = 'Descripció 1<sup>a</sup> línia';
$gl_caption['c_facebook_caption'] = 'Descripció 2<sup>a</sup> línia';
$gl_caption['c_facebook_image_title'] = 'Imatge';
$gl_caption['c_facebook_description'] = 'Descripció';
$gl_caption['c_facebook_save'] = 'Publicar a facebook';
$gl_caption['c_facebook_publish_as'] = 'Publicar com a';
$gl_caption['c_facebook_publish_as_image'] = 'Imatge';
$gl_caption['c_facebook_publish_as_link'] = 'Enllaç';
$gl_caption['c_facebook_publish_save_default']='Predeterminar aquesta opció';
$gl_caption['c_facebook_publish_user']='Mur propi';
$gl_caption['c_facebook_publish_pagina']='Pàgina';
$gl_caption['c_facebook_publish_id']='Publicar a';
$gl_caption['c_facebook_login']='Iniciar sessió a Facebook';
$gl_caption['c_facebook_delete_text']='<p>Els missatges publicats a Facebook només es poden modificar des de Facebook.</p><p>Si vols tornar a publicar des de Letnd primer s\'ha d\'esborrar la publicació anterior</p>';
$gl_caption['c_facebook_delete']='Esborrar publicació a Facebook';
$gl_caption['c_facebook_logout']='Logout';
$gl_caption['c_facebook_cannot_delete']='No es pot esborrar la publicació ja que es va crear amb un altre usuari de Facebook corresponent a';
$gl_caption['c_facebook_activate']='El preu d\'activació d\'aquest servei es de 60€ para poder donar d\'alta a Facebook';

$gl_caption['c_twitter_message'] = 'Missatge';
$gl_caption['c_twitter_save'] = 'Publicar a twitter';
$gl_caption['c_twitter_login']='Iniciar sessió a Twitter';
$gl_caption['c_twitter_delete_text']='<p>Els missatges publicats a Twitter només es poden modificar des de Twitter.</p><p>Si vols tornar a publicar des de Letnd primer s\'ha d\'esborrar la publicació anterior</p>';
$gl_caption['c_twitter_delete']='Esborrar publicació a Twitter';
$gl_caption['c_twitter_cannot_delete']='No es pot esborrar la publicació ja que es va crear amb un altre usuari de Twitter corresponent a';

$gl_caption['c_button_form_twitter_published']=
$gl_caption['c_button_form_facebook_published']=
$gl_caption['c_button_form_twitter']=
$gl_caption['c_button_form_facebook']=	'';
$gl_caption['c_chars']=	'Caràcters';

$gl_messages['social_facebook_no_user']='No estàs loguejat, si us plau torna a probar-ho';
$gl_messages['social_facebook_no_page']='No hi ha cap pàgina amb aquest id';
$gl_messages['social_facebook_published']='El missatge s\'ha publicat correctament a Facebook';
$gl_messages['social_facebook_not_init']='No s\'ha pogut connectar a Facebook';
$gl_messages['social_facebook_error']='Hi ha hagut un error a Facebook: ';
$gl_messages['social_facebook_deleted']='El missatge s\'ha esborrat a Facebook';
$gl_messages['social_facebook_not_deleted']='No hi ha cap missatge per borrar';

$gl_messages['social_twitter_empty']='El missatge està buit, no es pot publicar a Twitter';
$gl_messages['social_twitter_no_user']='No estàs loguejat, si us plau torna a probar-ho';
$gl_messages['social_twitter_published']='El missatge s\'ha publicat correctament a Twitter';
$gl_messages['social_twitter_not_init']='No s\'ha pogut conectar a Twitter';
$gl_messages['social_twitter_error']='Hi ha hagut un error a Twitter: ';
$gl_messages['social_twitter_deleted']='El missatge s\'ha esborrat a Twitter';
$gl_messages['social_twitter_not_deleted']='No hi ha cap missatge per borrar';

// FI NO TRADUIT


$gl_caption['c_file_manager']=	'File manager';
$gl_messages['no_records_search']='No match found for this search';
$gl_caption['c_back'] = 'Back';

$gl_caption['c_asc'] = 'aufst.';
$gl_caption['c_desc'] = 'abst.';

$gl_caption['c_reply'] = 'Respondre';

$gl_caption['c_file_file']='File';
$gl_caption['c_file_title']='Title';
$gl_caption['c_file_show_home']='In list';
$gl_caption['c_file_ordre']='Order';
$gl_caption['c_file_public']='Public';
$gl_caption['c_file_filecat_id']='Cat.';

$gl_caption['c_export_button']= 'Export';

$gl_caption['c_entered'] = 'Created';
$gl_caption['c_modified'] = 'Modified';