<script src="/common/jscripts/codemirror/lib/codemirror.js"></script>
<script src="/common/jscripts/codemirror/emmet/emmet.js"></script>


<script src="/common/jscripts/codemirror/mode/xml/xml.js" type="text/javascript"></script>
<script src="/common/jscripts/codemirror/mode/clike/clike.js" type="text/javascript"></script>
<script src="/common/jscripts/codemirror/mode/javascript/javascript.js" type="text/javascript"></script>
<script src="/common/jscripts/codemirror/mode/css/css.js" type="text/javascript"></script>
<script src="/common/jscripts/codemirror/mode/php/php.js" type="text/javascript"></script>
<script src="/common/jscripts/codemirror/mode/htmlmixed/htmlmixed.js" type="text/javascript"></script>
<script src="/common/jscripts/codemirror/mode/xml/xml.js" type="text/javascript"></script>
<!-- CodeMirror Addons -->
<script src="/common/jscripts/codemirror/keymap/sublime.js"></script>
<script src="/common/jscripts/codemirror/addon/selection/active-line.js"></script>
<script src="/common/jscripts/codemirror/addon/lint/lint.js"></script>
<script src="/common/jscripts/codemirror/addon/fold/foldcode.js"></script>
<script src="/common/jscripts/codemirror/addon/fold/foldgutter.js"></script>
<script src="/common/jscripts/codemirror/addon/fold/brace-fold.js"></script>
<script src="/common/jscripts/codemirror/addon/fold/xml-fold.js"></script>
<script src="/common/jscripts/codemirror/addon/fold/indent-fold.js"></script>
<script src="/common/jscripts/codemirror/addon/fold/markdown-fold.js"></script>
<script src="/common/jscripts/codemirror/addon/fold/comment-fold.js"></script>
<script src="/common/jscripts/codemirror/addon/edit/matchtags.js"></script>
<script src="/admin/modules/admintotal/jscripts/filesearch.js"></script>

<link href="/common/jscripts/codemirror/addon/lint/lint.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href="/common/jscripts/codemirror/lib/codemirror.css">
<link rel="stylesheet" href="/common/jscripts/codemirror/addon/fold/foldgutter.css">
<link rel="stylesheet" href="/common/jscripts/codemirror/theme/solarized.css">
<link rel="stylesheet" href="/common/includes/font-awesome/css/font-awesome.min.css">

<style>

	.left {
		float: left;
		width: 500px;
		padding: 30px;
	}
	.titol-local-servidor {
		padding-top: 30px;
		padding-right: 30px;
		padding-left: 30px;
		font-size: 30px;
	}
	#file_holder {
		display: none;
		overflow: hidden;
	}
	textarea {
		height: auto;
	}
	.CodeMirror {
		border: none;
		height: auto;
		line-height: 1.6;
		font-size: 15px;
	}
	.file_name {
		cursor: pointer;
		font-size: 15px !important;
	}
	.file_name.active1 {
		font-weight: bold;
	}
	#server_dir {
		color: #2cade6;
		line-height: 1.5;
	}
	.error {
		color: #CB2903;;
	}
	.formCaptionTitle div div {
		font-weight: normal;
		font-size: 16px;
	}
	.formCaptionTitle div div strong {
		font-weight: 700;
	}
	td.vert {
		padding-left: 20px;
	}
	table.files td {
		padding-bottom: 5px;
		vertical-align: top;
	}
	#help {
		font-size: 18px;
		font-weight: 400;
		margin-top: 50px;
		line-height: 2;
		margin-left: 560px;
	}
		#help h6 {
			font-size: 22px;
			margin: 0;
		}
</style>
<script>
	$(function () {

		filesearch.auto_complete_results =
		<?= $clients ?>
		;
		filesearch.init();

	});
</script>


<div class="search">
	<h3>Buscar arxius en totes les webs letnd</h3>
	<form name="search" method="get" action="/admin/">
		<table border="0" cellspacing="0" cellpadding="0">
			<tr valign="middle">
				<td>**/nom_arxiu.tpl</td>
				<td>
					<input type="hidden" value="1035" name="menu_id"/>
					<input name="action" type="hidden" value="list_records">
					<input name="q_filesearch" type="text" class="wide" value="<?= $q ?>"></td>
				<td>
				<td class="padding-left-3">%inmo%</td>
				<td>
					<input name="solution" type="text" size="10" value="<?= $solution_get ?>"></td>
				<td class="padding-left-3">Client</td>
				<td>
					<input name="client_id" id="client_id" type="hidden" value="<?= $client_id_selected ?>"><input name="client" id="client" type="hidden" value="<?= $client_selected ?>"><input id="client_add" type="text" size="40" value="<?= $client_selected ?>">
				</td>
				<td>
					<input type="submit" name="Submit" value="ok" class="boto-search"></td>
			</tr>
		</table>
	</form>
</div>

<div class="titol-local-servidor"><? if ( $is_server ): ?>
		Edició Servidor
	<? else: // ?>
		Edició Local
	<? endif //$is_server ?>
</div>

<? if ( $loop || $loop_no_results || $loop_errors ): ?>
	<div class="left">

		<? if ( $loop ): ?>
			<h2><?= $loop_count ?> arxius</h2>
			<table class="files">
				<? foreach ( $loop as $l ): extract( $l ) ?>
					<tr>
						<td><a href="http://<?= $domain ?>" target="_blank"><?= $domain ?></a><br>
							<strong><?= $solution ?></strong>
							<? if ( $is_local ): ?>
								<a target="save_frame" href="/common/open_php.php?file=<?= $filename ?>&line=1" title="Editar en PhpStorm"><i class="fa fa-edit fa-fw vermell"></i></a>
							<? endif // $is_local ?>
						</td>
						<td class="file_name vert" onclick="filesearch.load_file('<?= addslashes( $filename ) ?>', '<?= $domain ?>', '<?= $file ?>', '<?= $client_id ?>',  this)"><?= $file ?></td>
					</tr>
				<? endforeach //loop?>
			</table>

		<? endif ?>
		<? if ( $loop_no_results ): ?>
			<h2 class="padding-top-4">No s'ha trobat arxius</h2>
			<table class="files">
				<? foreach ( $loop_no_results as $l ): extract( $l ) ?>
					<tr>
						<td><?= $domain ?></td>
					</tr>
				<? endforeach //loop?>
			</table>

		<? endif ?>
		<? if ( $loop_errors ): ?>
			<h2 class="padding-top-4"">No existeix el directori</h2>
			<table class="files">
				<? foreach ( $loop_errors as $l ): extract( $l ) ?>
					<tr>
						<td><?= $domain ?><br>
							<strong><?= $solution ?></strong></td>
						<td class="file_name vert"><?= $client_dir ?></td>
					</tr>
				<? endforeach //loop?>
			</table>

		<? endif ?>
	</div>
	<div id="help">
		<? if ( $is_local ): ?>

			<h6>Al clicar:</h6>

			<div><i class="fa fa-clone fa-fw"></i>&nbsp; Fa còpia de l'arxiu local a "D:\Letnd\data\backups"</div>
			<div><i class="fa fa-long-arrow-left fa-fw"></i>&nbsp; Descarrega arxiu del servidor</div>

		<? endif // $is_local ?>

		<h6>Ctrl+s</h6>

		<? if ( $is_local ): ?>
			<div><i class="fa fa-save fa-fw"></i>&nbsp; Guarda arxiu en local</div>
			<div><i class="fa fa-clone fa-fw"></i>&nbsp; Còpia arxiu remot a "/letnd.com/datos/motor/data/backups"
			</div>
			<div><i class="fa fa-long-arrow-right fa-fw"></i>&nbsp; Puja arxiu al servidor</div>
		<? endif // $is_local ?>


		<? if ( $is_server ): ?>
			<div><i class="fa fa-clone fa-fw"></i>&nbsp; Còpia arxiu remot a "/letnd.com/datos/motor/data/backups"
			</div>
			<div><i class="fa fa-save fa-fw"></i>&nbsp; Guarda arxiu directament al servidor</div>
		<? endif // $is_server ?>

	</div>
<? endif // $loop || $loop_no_errors || $loop_errors ?>

<div id="file_holder">
	<table width="100%" cellspacing="0" cellpadding="4">
		<tbody>
		<tr>
			<td class="formCaptionTitle">
				<p id="file_path"></p>
				<div id="server_dir">Preparant arxiu <i class="fa fa-spinner fa-pulse fa-fw"></i></div>
			</td>
		</tr>
		</tbody>
	</table>
	<div id="file"></div>
	<div id="file"></div>
</div>