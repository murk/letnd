<?php
// no es poden posar ; entre mig de la consulta ja que parteix les consultes per el ; a no ser que fem servir $update_sql en array()
if (is_table("all__page_language") && !is_field('all__page_language','page_file_name')){	
	$update_sql[]="ALTER TABLE `all__page_language`  ADD COLUMN `page_file_name` VARCHAR(100) NOT NULL DEFAULT '' AFTER `page`;";
	$update_sql[]="UPDATE `all__page`, `all__page_language` SET `page_file_name`=template WHERE all__page.page_id = all__page_language.page_id";
}
if (!Db::get_first("SELECT count(*) FROM all__configpublic WHERE name='use_friendly_url'")){
	$update_sql[]="INSERT INTO `all__configpublic` (`name`, `value`) VALUES ('use_friendly_url', '0');";
}		
?>