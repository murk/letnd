<style>
	.image_select{
		width:684px;
	}
	.image_select div{
		float: left;
		margin: 0 10px 10px 0;
		cursor:pointer;
	}
	.image_select div img{
		border: 3px solid transparent;
		border-radius: 2px;
	}
	.image_select div:hover img{
		border: 3px solid #1381ce;
	}
	.image_select div.selected img, #tpl_custom img{
		border: 3px solid #e1001a;
	}
	.image_select span{
		display: block;
	}
	#tpl_custom{	
		margin: 10px 10px 10px 0;
	}
	#columna1,#columna2,#columna3,#videoframe {
		display:none;
	}
</style>
<script language="JavaScript" src="/admin/jscripts/validacio.js?v=<?=$version?>"></script>
<script language="JavaScript" src="/admin/jscripts/euro_calc.js?v=<?=$version?>"></script>
<script language="JavaScript" src="/common/jscripts/jquery.dropshadow.js?v=<?=$version?>"></script>
<script type="text/javascript">
$(function() {
	set_focus(document.theForm);
	
	<?if($paginatpl_id):?>
	$("#paginatpl_id_image_select div").click(function(){
		var new_selected = $(this).attr("rel");
		$("#paginatpl_id_image_select div").removeClass("selected");	
		$(this).addClass("selected");				
		$("#paginatpl_id_<?=$id?>").val(new_selected);
		show_tpl(new_selected);
	});
	
	show_tpl(<?=$pagina_tpl_selected?>);
	<?endif // paginnatpl_id?>
	<?if($has_custom_galleries):?>
	set_custom_tpl(true);
	<?endif //has_custom_galleries?>
	
});




function show_tpl(tpl_num){
	if (tpl_num==5 || tpl_num==6){
		$('#columna1,#columna2,#columna3').hide();
	}
	else if (tpl_num==9 || tpl_num==10 || tpl_num==11 || tpl_num==12 || tpl_num==13 || tpl_num==14 || tpl_num==15 || tpl_num==16 || tpl_num==50){
		$('#columna2,#columna3').hide();
		$('#columna1').show();	
	}
	else{
		$('#columna1,#columna2,#columna3').show();		
	}
	if (tpl_num==3 || tpl_num==4) $('#videoframe').show();
	else $('#videoframe').hide();
	fixa_menus(true);
}

// el custom es el tpl 50
function set_custom_tpl(load){

	var value = $('#custom_id_<?=$id?>').val();
	
	if (value!=''){
		$('#tpls').hide();
		$('#tpl_custom').show();
		$("#paginatpl_id_<?=$id?>").val(50);
	}
	else{
		$('#tpls').show();	
		$('#tpl_custom').hide();
		if (!load) $("#paginatpl_id_image_select div[rel='1']").click();
	}
}
</script>
<form name="form_delete" method="post" action="<?=$link_action_delete?>" target="save_frame">
  <input name="<?=$id_field?>" type="hidden" value="<?=$id?>">
</form>
<form name="theForm" method="post" action="<?=$link_action?>" onsubmit="<?=$js_string?>" encType="multipart/form-data" target="save_frame">
  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="form">
    <tr>
      <td><?if ($form_new):?><?if ($save_button):?>
          <div id="form_buttons_holder"><table id ="form_buttons">
            <tr>
              <td class="buttons_top"><div class="right"><input type="submit" name="Submit" value="<?=$c_send_new?>" class="btn_form1" /></div></td>
            </tr>
          </table></div>
          <?endif //save_button?>
          <?endif //form_new?>
        <?if ($form_edit):?>               
          <div id="form_buttons_holder"><table id ="form_buttons">
            <tr>
              <td valign="bottom"><div class="menu"><div><?=$c_edit_data?></div><?if ($has_images):?>
            	<a href="<?=$image_link?>"><?=$c_edit_images?></a>
         <?endif //has_images?><? foreach($form_tabs as $r): extract ($r)?><a href="?action=<?=$tab_action?>&<?=$id_field?>=<?=$id?>&menu_id=<?=$menu_id?>"><?=$tab_caption?></a><? endforeach //$loop?></div></td>
              <td class="buttons_top"><div class="right">
										<? if($show_previous_link): ?><a class="boto2 btn_show_previous_link"
										                          href="<?= $show_previous_link ?>"></a><? endif //show_previous_link?>
										<? if($show_next_link): ?><a class="boto2 btn_show_next_link"
										                          href="<?= $show_next_link ?>"></a><? endif //show_next_link?>
               <? if ($back_button):?><a class="btn_form_back" href="<?=$back_button?>"></a><? endif //back_button?>
            <? if ($save_button):?><input type="submit" name="Submit" value="<?=$c_send_edit?>" class="btn_form1"><? endif //save_button?><? if ($delete_button):?><a class="btn_form_delete" href="javascript:submit_form('<?if ($has_bin):?><?=$c_confirm_form_bin?><?else:?><?=$c_confirm_form_deleted?><?endif //has_bin?>', true);">
                <?=$c_delete?>
                </a><? endif //delete_button?><? if ($preview_button):?><a class="btn_form1" href="<?=$preview_link?>" target="_blank">
                <?=$c_preview?>
                </a><? endif //preview_button?><? if ($print_button):?><a class="btn_form1 btn_print" href="<?=$print_link?>">
                <?=$c_print?>
                </a> <? endif //print_button?></div></td>
            </tr>
          </table></div>
          <?endif //form_edit?>
        <table width="100%" cellpadding="0" cellspacing="0" >
          <tr>
            <td class="formCaptionTitle"><strong>
              <?=$c_edit_title?>
              &nbsp;&nbsp;</strong>
              <?if ($form_edit):?><span><?=$c_entered?>:
              <?=$entered?>
              -
              <?=$c_modified?>:
              <?=$modified?></span><?endif //form_edit?></td>
          </tr>
        </table>
        <table width="100%" cellpadding="0" cellspacing="0" class="formstbl">
          <tr>
            <td valign="top" class="formsCaption"><strong>
              <?=$c_pagina?>:</strong>
              <input name="pagina_id[<?=$id?>]" type="hidden" value="<?=$id?>" />
              <input name="pagina_id_javascript" type="hidden" value="<?=$id?>" /><?if ($form_new):?><?=$parent_id?><?=$level?><?endif //form_new?></td>
            <td class="forms"><table cellpadding="4" cellspacing="0">
                <?foreach($pagina as $l): extract ($l)?>
                  <tr>
					<?if($lang):?>
                    <td class="forms"><?=$lang?></td>
					<?endif //lang?>
                    <td><?=$pagina?></td>
                  </tr>
                  <?endforeach //$pagina?>
              </table></td>
          </tr>
          <?if($custom_id):?><tr>
            <td valign="top" class="formsCaption"><strong>
              <?=$c_custom_id?>:</strong></td>
            <td class="forms"><?=$custom_id?>			
			<div id="tpl_custom">
				<img src="/admin/modules/revista/paginatpl_custom.gif" title="">
			</div>
			</td>
          </tr><?endif //custom_id?>
          <?if($paginatpl_id):?><tr id="tpls">
            <td valign="top" class="formsCaption"><strong>
              <?=$c_paginatpl_id?>
              :</strong></td>
            <td class="forms"><?=$paginatpl_id?></td>
          </tr><?endif //paginatpl_id?>
          <tr>
            <td valign="top" class="formsCaption"><strong>
              <?=$c_status?>
              :</strong></td>
            <td class="forms"><?=$status?></td>
          </tr>
          <?if($category_id):?>
            <tr>
              <td valign="top" class="formsCaption"><strong>
                <?=$c_category_id?>
                :</strong></td>
              <td class="forms"><?=$category_id?></td>
            </tr>
            <?endif //category_id?>
          <?if($is_clubone):?>
            <tr>
              <td valign="top" class="formsCaption"><strong>
                <?=$c_is_clubone?>
                :</strong></td>
              <td class="forms"><?=$is_clubone?> <span style="vertical-align:baseline"><img src="/admin/modules/clubmoto/socis.gif" width="47" height="16"></span>
              </td>
            </tr>
            <?endif //is_clubone?>
          <?if($is_advertisement):?>
            <tr>
              <td valign="top" class="formsCaption"><strong>
                <?=$c_is_advertisement?>
                :</strong></td>
              <td class="forms"><?=$is_advertisement?> <span style="vertical-align:baseline"><img src="/admin/modules/clubmoto/advertisement.gif" width="70" height="16"></span>
              </td>
            </tr>
            <?endif //is_advertisement?>
          <?if($pagina_page_title):?>
          <tr>
            <td valign="top" class="formsCaption"><strong>
              <?=$c_page_title?>:</strong></td>
            <td class="forms"><table cellpadding="4" cellspacing="0">
                <?foreach($page_title as $l): extract ($l)?>
                  <tr>
                    <td class="forms" valign="top"><?=$lang?></td>
                    <td><?=$page_title?></td>
                  </tr>
                  <?endforeach //$page_title?>
              </table></td>
          </tr>
          <tr>
            <td valign="top" class="formsCaption"><strong>
              <?=$c_page_description?>:</strong></td>
            <td class="forms"><table cellpadding="4" cellspacing="0">
                <?foreach($page_description as $l): extract ($l)?>
                  <tr>
                    <td class="forms" valign="top"><?=$lang?></td>
                    <td><?=$page_description?></td>
                  </tr>
                  <?endforeach //$c_page_description?>
              </table></td>
          </tr>
          <tr>
            <td valign="top" class="formsCaption"><strong>
              <?=$c_page_keywords?>:</strong></td>
            <td class="forms"><table cellpadding="4" cellspacing="0">
                <?foreach($page_keywords as $l): extract ($l)?>
                  <tr>
                    <td class="forms" valign="top"><?=$lang?></td>
                    <td><?=$page_keywords?></td>
                  </tr>
                  <?endforeach //$page_keywords?>
              </table></td>
          </tr>
            <?endif //page_keywords?>
          <?if($pagina_file_name):?>
          <tr>
            <td valign="top" class="formsCaption"><strong>
              <?=$c_pagina_file_name?>:</strong></td>
            <td class="forms"><table cellpadding="4" cellspacing="0">
                <?foreach($pagina_file_name as $l): extract ($l)?>
                  <tr>
					<?if($lang):?>
                    <td class="forms"><?=$lang?></td>
					<?endif //lang?>
                    <td><?=$pagina_file_name?></td>
                  </tr>
                  <?endforeach //$pagina_file_name?>
              </table></td>
          </tr>
            <?endif //pagina_file_name?>
        </table>
        <?if($is_pagina):?>
		  <div id="columna1"><div class="sep_dark"></div>
        <table width="100%" cellpadding="4" cellspacing="0">
          <tr>
            <td class="formCaptionTitle"><strong>
              <?=$c_column_title1?>
              </strong></td>
          </tr>
        </table>
        <table width="100%" cellpadding="0" cellspacing="0" class="formstbl">
          <tr>
            <td valign="top" class="formsCaption"><strong>
              <?=$c_col_valign?>
              :</strong></td>
            <td class="forms">
				<?=$col_valign?>
			</td>
          </tr>
          <tr>
            <td valign="top" class="formsCaption"><strong>
              <?=$c_body_supertitle?>
              :</strong></td>
            <td class="forms"><table cellpadding="4" cellspacing="0">
                <?foreach($body_supertitle as $l): extract ($l)?>
                  <tr>
					<?if($lang):?>
                    <td class="forms"><?=$lang?></td>
					<?endif //lang?>
                    <td><?=$body_supertitle?></td>
                  </tr>
                  <?endforeach //$title?>
              </table></td>
          </tr>
          <tr>
            <td valign="top" class="formsCaption"><strong>
              <?=$c_title?>
              :</strong></td>
            <td class="forms"><table cellpadding="4" cellspacing="0">
                <?foreach($title as $l): extract ($l)?>
                  <tr>
					<?if($lang):?>
                    <td class="forms"><?=$lang?></td>
					<?endif //lang?>
                    <td><?=$title?></td>
                  </tr>
                  <?endforeach //$title?>
              </table></td>
          </tr>
          <tr>
            <td valign="top" class="formsCaption"><strong>
              <?=$c_body_subtitle?>
              :</strong></td>
            <td class="forms"><table cellpadding="4" cellspacing="0">
                <?foreach($body_subtitle as $l): extract ($l)?>
                  <tr>
					<?if($lang):?>
                    <td class="forms"><?=$lang?></td>
					<?endif //lang?>
                    <td><?=$body_subtitle?></td>
                  </tr>
                  <?endforeach //$body_subtitle?>
              </table></td>
          </tr>
        </table>
        <table width="100%" cellpadding="0" cellspacing="0" class="formstbl">
          <tr>
            <td class="formsCaptionHor2"><?=$c_body_content?></td>
          </tr>
          <?foreach($body_content as $l): extract ($l)?>
            <tr>
              <td class="formsHor">
					<?if($lang):?><?=$lang?>
                <br /><?endif //lang?>
                <?=$body_content?></td>
            </tr>
            <?endforeach //$body_content?>
        </table>
		  </div>
		  <div id="columna2"><div class="sep_dark"></div>
        <table width="100%" cellpadding="4" cellspacing="0">
          <tr>
            <td class="formCaptionTitle"><strong>
              <?=$c_column_title2?>
              </strong></td>
          </tr>
        </table>
        <table width="100%" cellpadding="0" cellspacing="0" class="formstbl">
          <tr>
            <td valign="top" class="formsCaption"><strong>
              <?=$c_col_valign?>
              :</strong></td>
            <td class="forms">
				<?=$col_valign2?>
			</td>
          </tr>
          <tr>
            <td valign="top" class="formsCaption"><strong>
              <?=$c_body_supertitle2?>
              :</strong></td>
            <td class="forms"><table cellpadding="4" cellspacing="0">
                <?foreach($body_supertitle2 as $l): extract ($l)?>
                  <tr>
					<?if($lang):?>
                    <td class="forms"><?=$lang?></td>
					<?endif //lang?>
                    <td><?=$body_supertitle2?></td>
                  </tr>
                  <?endforeach //$title?>
              </table></td>
          </tr>
          <tr>
            <td valign="top" class="formsCaption"><strong>
              <?=$c_title2?>
              :</strong></td>
            <td class="forms"><table cellpadding="4" cellspacing="0">
                <?foreach($title2 as $l): extract ($l)?>
                  <tr>
					<?if($lang):?>
                    <td class="forms"><?=$lang?></td>
					<?endif //lang?>
                    <td><?=$title2?></td>
                  </tr>
                  <?endforeach //$title2?>
              </table></td>
          </tr>
          <tr>
            <td valign="top" class="formsCaption"><strong>
              <?=$c_body_subtitle2?>
              :</strong></td>
            <td class="forms"><table cellpadding="4" cellspacing="0">
                <?foreach($body_subtitle2 as $l): extract ($l)?>
                  <tr>
					<?if($lang):?>
                    <td class="forms"><?=$lang?></td>
					<?endif //lang?>
                    <td><?=$body_subtitle2?></td>
                  </tr>
                  <?endforeach //$body_subtitle2?>
              </table></td>
          </tr>
        </table>
        <table width="100%" cellpadding="0" cellspacing="0" class="formstbl">
          <tr>
            <td class="formsCaptionHor2"><?=$c_body_content2?></td>
          </tr>
          <?foreach($body_content2 as $l): extract ($l)?>
            <tr>
              <td class="formsHor">
					<?if($lang):?><?=$lang?>
                <br /><?endif //lang?>
                <?=$body_content2?></td>
            </tr>
            <?endforeach //$body_content2?>
        </table>
		  </div>
		  <div id="columna3"><div class="sep_dark"></div>
        <table width="100%" cellpadding="4" cellspacing="0">
          <tr>
            <td class="formCaptionTitle"><strong>
              <?=$c_column_title3?>
              </strong></td>
          </tr>
        </table>
        <table width="100%" cellpadding="0" cellspacing="0" class="formstbl">
          <tr>
            <td valign="top" class="formsCaption"><strong>
              <?=$c_col_valign?>
              :</strong></td>
            <td class="forms">
				<?=$col_valign3?>
			</td>
          </tr>
          <tr>
            <td valign="top" class="formsCaption"><strong>
              <?=$c_body_supertitle3?>
              :</strong></td>
            <td class="forms"><table cellpadding="4" cellspacing="0">
                <?foreach($body_supertitle3 as $l): extract ($l)?>
                  <tr>
					<?if($lang):?>
                    <td class="forms"><?=$lang?></td>
					<?endif //lang?>
                    <td><?=$body_supertitle3?></td>
                  </tr>
                  <?endforeach //$title?>
              </table></td>
          </tr>
          <tr>
            <td valign="top" class="formsCaption"><strong>
              <?=$c_title3?>
              :</strong></td>
            <td class="forms"><table cellpadding="4" cellspacing="0">
                <?foreach($title3 as $l): extract ($l)?>
                  <tr>
					<?if($lang):?>
                    <td class="forms"><?=$lang?></td>
					<?endif //lang?>
                    <td><?=$title3?></td>
                  </tr>
                  <?endforeach //$title3?>
              </table></td>
          </tr>
          <tr>
            <td valign="top" class="formsCaption"><strong>
              <?=$c_body_subtitle3?>
              :</strong></td>
            <td class="forms"><table cellpadding="4" cellspacing="0">
                <?foreach($body_subtitle3 as $l): extract ($l)?>
                  <tr>
					<?if($lang):?>
                    <td class="forms"><?=$lang?></td>
					<?endif //lang?>
                    <td><?=$body_subtitle3?></td>
                  </tr>
                  <?endforeach //$body_subtitle3?>
              </table></td>
          </tr>
        </table>
        <table width="100%" cellpadding="0" cellspacing="0" class="formstbl">
          <tr>
            <td class="formsCaptionHor2"><?=$c_body_content3?></td>
          </tr>
          <?foreach($body_content3 as $l): extract ($l)?>
            <tr>
              <td class="formsHor">
					<?if($lang):?><?=$lang?>
                <br /><?endif //lang?>
                <?=$body_content3?></td>
            </tr>
            <?endforeach //$body_content3?>
        </table>
		  </div>
		  <div id="videoframe"><div class="sep_dark"></div>
        <table width="100%" cellpadding="4" cellspacing="0">
          <tr>
            <td class="formCaptionTitle"><strong>
              <?=$c_column_title4?>
              </strong></td>
          </tr>
        </table>
        <table width="100%" cellpadding="0" cellspacing="0" class="formstbl">
          <tr>
            <td valign="top" class="formsCaption"><strong>
              <?=$c_videoframe?>
              :</strong></td>
            <td class="forms"><?=$videoframe?></td>
          </tr>
        </table>
		  </div>
		<?endif //is_pagina?>
        <?if ($form_new):?>
          <table width="100%" cellpadding="4" cellspacing="2" class="formsButtonstbl">
            <tr>
              <td align="center" class="formsButtons"><input type="submit" name="Submit" value="<?=$c_send_new?>" class="btn_form1"></td>
            </tr>
          </table>
          <?endif //form_new?>
        <?if ($form_edit):?>
          <table width="100%" cellpadding="4" cellspacing="2" class="formsButtonstbl">
            <tr>
              <td align="center" class="formsButtons"><div class="centerFloat1">
                  <div class="centerFloat2">
                    <input type="submit" name="Submit" value="<?=$c_send_edit?>" class="btn_form1">
                  </div>
                </div></td>
            </tr>
          </table>
          <?endif //form_edit?></td>
    </tr>
  </table>
</form>
