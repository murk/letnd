<?
/**
 * FOTOCASA
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) Octubre 2017
 * @version 1
 * @access public
 */

/*

Activar client
---------------
Comprobar si hi ha immobles ( introduir api_key )

	Si hi ha immobles:
		Comentar-ho al client abans de continuar, ja que s'esborraràn tots

	Si no hi ha cap immoble:
		Posar les variables a client__client
		Assegurar-se que la url principal sigui la primera: ej. www.easybrava.com i no easybrava.cat ( camp 'domain' )
		S'esborrern tots els importats de d'un altre canal

Fotocasa URLS
--------------
https://api.inmofactory.com/Help
https://api.inmofactory.com/Help/PropertyJsonNavigationSchema

http://login.inmofactory.com/Account/LogOn?ReturnUrl=%
testapiletnd@mail.es
ZO?jQv6

Z-Ray
------------
Posar a IPs: 10.5.5.1 i 127.0.0.1 ( separades, junt no va ) -> així es fa debug i z-ray al tkn-acs




INFO Exportació portals

S'han agefit camps efficiency a apicat i fotocasa

	efficiency_number
	efficiency_number2
	modified

INFO FEATURES

	Obligatori
		Ubicació
		Superfície

	Mostro l'adreça complerta o nomes carrer amb els mateixos criteris del mapa
	Adreça -> mostro zona a "Zone". provincia i municipi no, suposo ho agafa de les coordenades
				StreetTypeId -> NO

	No exporto Referencia Cadastral i registre


PREGUNTAR A FOTOCASA

	Agafa provincia i municipi de les coordenades?
	Perquè s'ha de posar idioma a la característica, per exemple Superfície, si es posa en castellà només es veurà la superfície al castellà?
	HUT, on es pot veure? -> Si se informa la transacción alquiler vacacional es obligatorio indicar un código de turismo válido (entre 8 y 15 caracteres).

	TODO-i  Passar html a text si ho es html_to_text();

CANVIS EN BBDD


LETND NO TE



FOTOCASA NO TE

	Finca exclusiva
	Protecció oficial
	Amarratge
	Garatge




VARS

INFO - Consultes Sql

Veure tipus:
	SELECT *, (SELECT CONCAT (description, ' - ', description_type) FROM export__fotocasa_building_subtype WHERE export__fotocasa_building_subtype.id = export__fotocasa_category_to_subtype.subtype_id) as subtype FROM export__fotocasa_category_to_subtype;

Actualitzar tipus:

	UPDATE export__fotocasa_category_to_subtype SET category = (SELECT category FROM inmo__category_language WHERE export__fotocasa_category_to_subtype.category_id = inmo__category_language.category_id AND language= 'cat');

	UPDATE export__fotocasa_building_subtype SET description_type = (SELECT description FROM export__fotocasa_building_type WHERE export__fotocasa_building_type.id = export__fotocasa_building_subtype.building_type_id);


TODO-i

NOTICE [8] Undefined index: TypeId
Backtrace:

Arxiu: /var/www/letnd.com/datos/motor/common/classes/debug_cli.php, linea: 112
Arxiu: /var/www/letnd.com/datos/motor/admin/modules/admintotal/admintotal_fotocasa.php, linea: 1082
Arxiu: /var/www/letnd.com/datos/motor/admin/modules/admintotal/admintotal_fotocasa.php, linea: 1070
Arxiu: /var/www/letnd.com/datos/motor/admin/modules/admintotal/admintotal_fotocasa.php, linea: 635
Arxiu: /var/www/letnd.com/datos/motor/admin/modules/admintotal/admintotal_fotocasa.php, linea: 540
Arxiu: /var/www/letnd.com/datos/motor/admin/modules/admintotal/admintotal_fotocasa.php, linea: 441
Arxiu: /var/www/letnd.com/datos/motor/admin/modules/admintotal/admintotal_fotocasa_cli.php, linea: 37


NOTICE [8] Undefined property: stdClass::$StatusCode
Backtrace:

Arxiu: /var/www/letnd.com/datos/motor/common/classes/debug_cli.php, linea: 112
Arxiu: /var/www/letnd.com/datos/motor/admin/modules/admintotal/admintotal_fotocasa.php, linea: 737
Arxiu: /var/www/letnd.com/datos/motor/admin/modules/admintotal/admintotal_fotocasa.php, linea: 669
Arxiu: /var/www/letnd.com/datos/motor/admin/modules/admintotal/admintotal_fotocasa.php, linea: 540
Arxiu: /var/www/letnd.com/datos/motor/admin/modules/admintotal/admintotal_fotocasa.php, linea: 441
Arxiu: /var/www/letnd.com/datos/motor/admin/modules/admintotal/admintotal_fotocasa_cli.php, linea: 37

TODO-i Pel tema de que a fotocasa no es pot canviar de tipus, s'ha de comprovar cada cop que es guarda, que no hi hagi cap altre amb external_id que comenci amb letnd_id

	 ej:  [ExternalId] => 6110316-2, [ExternalId] => 6110316-23
	Aquest abans era categoria 2, i s'ha canviat a 23, en canvi nomes s'ha esborrat el [ExternalId] => 6110316 pero el [ExternalId] => 6110316-2 no

Però en canvi quan es fa l'exportació complerta ja s'esborren


*/

class AdmintotalFotocasa extends Module {

	var $exporter = 'fotocasa';
	public $section = 'property';
	var $is_global_export = false;
	var $config_rs = [];
	var $export_rs = [];
	var $property_rs = [];
	var $agencia_rs = [];
	var $client_images_url = '';
	var $foto_casa_url = "https://api.inmofactory.com";
	var $is_cli = false;
	var $headers = [];

	var $response_errors = [];
	var $agencia_exported_ids = [];

	// Per poder exportar nomes una agencia i un inmoble, en temps real
	var $agencia_id = 0;
	var $property_ids = [];
	var $portal_vars = [];

	var $fotocasa_country_id = [
		// '' =>  0,	// Indefinido
		// '' =>  20,	// Andorra
		// '' =>  250,	// Francia
		// '' =>  620,	// Portugal
		'1' => 724,    // España
	];
	var $fotocasa_flat = [
		// '' => 0,  //	Indefinido
		// '' => 1,  //	Sótano
		// '' => 2,  //	Subsótano
		// '' => 3,  //	Bajos
		// '' => 4,  //	Entresuelo
		// '' => 5,  //	Principal
		'1'  => 6,  //	1º
		'2'  => 7,  //	2º
		'3'  => 8,  //	3º
		'4'  => 9,  //	4º
		'5'  => 10,  //	5º
		'6'  => 11,  //	6º
		'7'  => 12,  //	7º
		'8'  => 13,  //	8º
		'9'  => 14,  //	9º
		'10' => 15,  //	10º
		'11' => 16,  //	11º
		'12' => 17,  //	12º
		'13' => 18,  //	13º
		'14' => 19,  //	14º
		'15' => 20,  //	15º
		'16' => 21,  //	A partir del 15º
		// '' => 22,  //	Ático
		// '' => 23,  //	Sobreático
		'0'  => 24,  //	Planta baja
		// '' => 28,  //	Medio
		// '' => 29,  //	Último
		// '' => 30,  //	Terraza
		// '' => 31,  //	Otro
	];

	var $fotocasa_languages = [
		// ''    => 0, //Indefinido
		'por' => 1, //Portugués
		'eng' => 2, //Inglés
		'fra' => 3, //Francés
		'spa' => 4, //Español
		'ita' => 5, //Italiano
		'deu' => 6, //Alemán
		// '' => 7	, //Griego
		'cat' => 8, //Catalán
		// '' => 9	, //Euskera
		// '' => 13, //	Gallego
		'dut' => 14, //	Holandés
		// '' => 15, //	Valenciano -> es conya??
		// '' => 16, //	Búlgaro
	];

	var $fotocasa_tipus = [
		'sell'       => 1, //	Venta
		'rent'       => 3, //	Alquiler
		// '' => 4, //	Traspaso
		// '' => 7, //	A compartir
		'temp'       => 8, //	Alquiler vacacional
		'selloption' => 9, //	Alquiler con opcion a compra
	];

	var $fotocasa_facing = [
		'ne' => 1,    // Noreste
		'o'  => 2,    // Oeste
		'n'  => 3,    // Norte
		'so' => 4,    // Suroeste
		'e'  => 5,    // Este
		'se' => 6,    // Sureste
		'no' => 7,    // Noroeste
		's'  => 8,    // Sur
	];
	/*
	 * Feature 109 i 110, no implementat, per gres i parquet ja hi han els bàsics
	var $fotocasa_floor_type = [
		'' => 1, //suelo baldosa
		'' => 2, //Suelo baldosa rustica
		'' => 3, //Suelo cerámico
		'' => 4, //Suelo corcho
		'' => 5, //Suelo de cemento enlucido
		'gres' => 6, //Suelo de gres
		'parket' => 7, //Suelo de madera
		'' => 8, //Suelo de mármol
		'' => 10, //Suelo de parquet / terrazo
		'' => 11, //Suelo de tarima
		'' => 12, //Suelo linóleo
		'' => 13, //Suelo moqueta
		'' => 14, //Suelo mosaico
		'' => 15, //Suelo porcelanato
		'' => 16, //Suelo terrazo
	];*/

	var $fotocasa_efficiency = [
		'a'        => 1, //	A
		'b'        => 2, //	B
		'c'        => 3, //	C
		'd'        => 4, //	D
		'e'        => 5, //	E
		'f'        => 6, //	F
		'g'        => 7, //	G
		'progress' => 8, //	En trámite
		'exempt'   => 9, //	Exento
	];

	var $fotocasa_publications = [
		1, // Fotocasa, // www.fotocasa.es
		2, // Vibbo, // www.vibbo.com
		9, // Inmogeo, // www.inmogeo.es
		17, // EnAlquiler.com, // http://www.enalquiler.com
		18, // casasconjardin.com, // http://www.casasconjardin.com
		20, // tevagustar.es, // http://www.tevagustar.es
		21, // Trovit, // http://www.trovit.es
		22, // miparcela.com, // http://www.miparcela.com
		24, // inzoco.com, // http://www.inzoco.com
		25, // inmostock.es, // http://www.inmostock.es
		26, // globaliza.com, // http://www.globaliza.com
		27, // multipiso.com, // http://www.multipiso.com
		28, // urbaniza.com, // http://www.urbaniza.com
		29, // capgros.com, // http://www.capgros.com
		30, // tucasa.com, // http://www.tucasa.com
		31, // pisos.com, // http://www.pisos.com
		32, // alquilacompra.es, // http://www.alquilacompra.es
		33, // buscocasa.com, // http://www.buscocasa.com
		34, // facilisimo.com, // http://www.facilisimo.com
		35, // ibuscando.com, // http://www.ibuscando.com
		36, // ganga.es, // http://www.ganga.es
		37, // tenertodo.com, // http://www.tenertodo.com
		38, // eMaresme.com, // http://www.emaresme.com
		39, // RealtorWorldGuide.com, // http://www.realtorworldguide.com
		48, // masprofesional.com, // http://www.masprofesional.com
		49, // ventadepisos.com , // http://www.ventadepisos.com
		1084, // Ivive.com, // http://www.ivive.com
		1983, // spainhouses.net, // http://www.spainhouses.net
		2047, // yaencontre.com, // http://www.yaencontre.com
		2157, // Web Mobile Fotocasa, //
		2346, // hogaria.net, // http://www.hogaria.net
		2725, // anuncioneon, // http://www.anuncioneon.com
		3076, // Buscafincas, // http://www.buscafincas.com
		3078, // Alquilerjoven, // http://www.alquilerjoven.com
		3079, // Instalate, // http://www.instalate.com
		3169, // Web Mobile Vibbo, //
		3387, // Pisocasas.com, // http://www.pisocasas.com
		3439, // Test portal, //
		3489, // Centro Comercial Inmobiliario, // http://www.centrocomercialinmobiliario.com
		3504, // Green Acres, // http://www.green-acres.es/
		3799, // HispaCasas.com, // http://www.hispacasas.com
		4005, // linkbyu.es, // http://www.linkbyu.es
		4006, // Micasa.es, // http://www.micasa.es
		4010, // Divendo.es, // http://www.divendo.es
		4112, // Buscocasaenmurcia, // http://www.buscocasaenmurcia.com
		4114, // elpisoideal, // http://www.elpisoideal.com
		4115, // alnido, // http://www.alnido.es
		4116, // inmoportalix, // http://www.inmoportalix.com
		4508, // plandeprotecciondealquiler.com, // http://www.plandeprotecciondealquiler.com
		4533, // casanuncio.com, // http://www.casanuncio.com
		4534, // pisocasa.es, // http://www.pisocasa.es
		4535, // granmanzana.es, // http://www.granmanzana.es
		4536, // compra-venta.org, // http://www.compra-venta.org
		4537, // vendoo.net, // http://www.vendoo.net
		4538, // mitula.com, // http://www.mitula.com
		4539, // kyero.com, // http://www.kyero.com
		4692, // Heraldo.es, // http://www.heraldo.es
		5131, // misviviendas, // http://www.misviviendas.com
		5189, // venderya, // http://www.venderya.es
		6053, // Portales Internacionales, //
		7598, // Anuncit, // http://anuncit.com
		9127, // Trovimap, // http://trovimap.com
		9282, // WorldImmo, // http://worldimmo.org
		19156, // Fotocasa Microsite Advance, //
		23648, // Habitaclia, // http://www.habitaclia.com
		23958, // Internacional Premium Network, //
		23959, // Listglobally Classic
	];

	function __construct() {

		include_once( DOCUMENT_ROOT . 'admin/modules/admintotal/export_common.php' );

		ExportCommon::init_unirest( $this );

		parent::__construct();
	}

	function start() {
		$this->_start();
		html_end();
	}


	// Funcions per actualitzar automàticament al guardar
	function on_save_portal() {

		$export_all = Db::get_first( "SELECT value FROM inmo__configadmin WHERE name = 'fotocasa_export_all'" );

		$this->set_on_vars();

		// Si està a exportar tot, aquí no faig res, l'unic que pot passar es que si es el primer cop, haurà d'esperar a que faci el cronjob
		if ( $export_all ) {
			// TODO-i Fer alguna acció per que actualitzi tot el primer cop
		}
		else {

			$this->property_ids = array_merge( $this->portal_vars ['fotocasa']['new'], $this->portal_vars ['fotocasa']['equal'] ); // Poso equal també ja que pot ser que per falta de dades tampoc s'hagi exportat

			$deleted_ids = $this->portal_vars['fotocasa']['deleted'];

			$this->_start( false );


			foreach ( $deleted_ids as $property_id ) {
				$this->delete_property( $property_id, false );
			}
		}
		ExportCommon::end( $this );

	}

	function on_save_restore() {
		$this->on_save_update();
	}

	function on_save_update() {

		$this->set_on_vars();
		$this->_start( false );

		// haig de borrar els que no s'han exportat, poder perque ha canviat estat, han tret les coordenades, etc..
		$not_exported = array_diff( $this->property_ids, $this->agencia_exported_ids );

		Debug::p( [
			[ 'exported', $this->agencia_exported_ids ],
			[ 'exported_count', count( $this->agencia_exported_ids ) ],
			[ 'not_exported', $not_exported ],
			[ 'not_exported_count', count( $not_exported ) ],
		], 'Fotocasa' );


		ExportCommon::p( "Eliminació d'inmobles: " . $this->agencia_rs['comercialname'], 1 );


		// funciona perque nomes hi ha una agència, sino hauria d'anar al loop a export_properties
		foreach ( $not_exported as $property_id ) {
			$this->delete_property( $property_id, false );
		}

		ExportCommon::end( $this );

	}

	function on_save_bin() {
		$this->on_save_delete();
	}

	function on_save_delete() {

		$this->set_on_vars();

		Db::connect_mother();

		$agencias = ExportCommon::get_agencias( 'inmo_fotocasa', $this->agencia_id );

		if ( ! $agencias ) {
			ExportCommon::end( $this );

			return;
		}

		// Només hi ha una agència, però així aprofito la funció
		foreach ( $agencias as $rs ) {

			$this->agencia_rs =  &$rs;

			ExportCommon::p( "Eliminació d'inmobles: " . $this->agencia_rs['comercialname'], 1 );
			$this->auth();

			foreach ( $this->property_ids as $property_id ) {
				$this->delete_property( $property_id, false );
				Debug::p( [
					[ 'Deleted', $property_id ],
				], 'Fotocasa' );
			}

		}


		ExportCommon::end( $this );

	}

	function set_on_vars() {
		$this->agencia_id = R::id( $this->agencia_id, false, false );
		R::escape_array( $this->property_ids );
	}

	// Iniciar, pot ser que només s'actualitzi un inmoble o tots amb is_global_export
	function _start( $is_global_export = true ) {

		$agencia_id = R::get_opt( 'agencia_id' );
		if ( $agencia_id ) {
			$this->agencia_id = $agencia_id;
		}

		$this->is_global_export = $is_global_export;

		if ( $is_global_export ) {
			Debug::add( date( "d-m-Y H:i" ), "" );
			ExportCommon::p( "INICI: " . date( "d-m-Y H:i" ), 1 );
			set_time_limit( 0 );        //ini_set('implicit_flush', true);		//error_reporting(E_WARNING);
			echo '<link href="/admin/themes/inmotools/styles/export.css" rel="stylesheet" type="text/css">';
		}

		Db::connect_mother();

		$agencias = ExportCommon::get_agencias( 'inmo_fotocasa', $this->agencia_id );

		if ( ! $agencias ) {
			ExportCommon::end( $this );

			return;
		}

		$this->export_properties( $agencias );

		ExportCommon::show_end();

		if ( $is_global_export ) {
			ExportCommon::end( $this );
		}
	}

	// Obtinc tots els immobles de fotocasa i comprovo si s'ha de borrar algún ( nomes si estem exportant tot, mai al fer l'update o delete d'un sol immoble )
	private function check_published_properties() {

		if ( ! $this->is_global_export ) return;

		$api_url  = $this->foto_casa_url . '/api/publication/property';
		$response = Unirest\Request::get( $api_url, $this->headers );
		$this->log_json( $api_url, 'Check published porperties', $response );

		$propertys           = json_decode( $response->body );
		$fotocasa_properties = [];

		foreach ( $propertys as $property ) {
			$property_id = $property->ExternalId;
			if ( $property_id ) {
				$fotocasa_properties [] = $property_id;
			}
		}
		$exported_ids = $this->agencia_exported_ids;

		$properties_to_delete = array_diff( $fotocasa_properties, $exported_ids );


		if ( ! $properties_to_delete ) return;

		ExportCommon::p( "Eliminació d'inmobles: " . $this->agencia_rs['comercialname'], 1 );

		foreach ( $properties_to_delete as $property_id ) {
			$this->delete_property( $property_id );
		}

	}

	function get_config_results() {


		$query = "SELECT fotocasa_field, fotocasa_subfield, fotocasa_feature_id, fotocasa_feature_value_type, fotocasa_feature_building_types,fotocasa_building_types_excluded, letnd_table, letnd_field, `function`, `condition`
							FROM export__fotocasa
							WHERE fotocasa_table <>  ''";

		Debug::p( [
			[ 'Consulta export__fotocasa', $query ],
		], 'Fotocasa' );

		return Db::get_rows( $query );

	}

	private function get_agencia_config() {

		ExportCommon::get_agencia_config( $this->agencia_rs, 'fotocasa' );

		$agencia = &$this->agencia_rs;

		$agencia['url'] = "http://" . $this->agencia_rs['domain'];

		$publications = $this->get_publications();

		if ( $publications === false ) return false;

		$new_publications = [];

		foreach ( $publications as $publication ) {
			if ( $publication->PublicationId != 25373 ) {
				$new_publications [] = [
					'PublicationId'     => $publication->PublicationId,
					'PublicationTypeId' => $publication->TypeId,
				];
			}
		}
		$agencia['publications'] = $new_publications;

		return true;

	}


	function export_properties( $agencias ) {

		foreach ( $agencias as $rs ) {

			$this->agencia_rs =  &$rs;
			// Només loguejo un cop
			$this->auth();

			$is_active = $this->get_agencia_config();
			$condition = $this->get_agencia_condition();

			Debug::p( [
				[ 'Condition', $condition ],
			], 'Fotocasa' );

			$this->client_images_url = $rs['url'] . '/' . $rs['client_dir'] . '/inmo/property/images/';


			ExportCommon::p( "Extracció d'inmobles: " . $this->agencia_rs['comercialname'], 1 );

			if ( $is_active !== false ) $this->build_agencia_json( $condition );
			else
				ExportCommon::p( "Api key no vàlid: " . $this->agencia_rs['comercialname'], 1 );

		}
	}

	private function get_agencia_condition() {

		$condition = "(status = 'onsale' || status = 'reserved')
				 AND bin = 0
				 AND country_id = '1'
				 AND price > 0
                 AND (
                        latitude != 0.0000000 &&
                        longitude != 0.0000000
                    )
                 AND (
	                    (floor_space <> 0 )
	                    OR
	                    (category_id = 5 AND land <> 0 )
                    )
                    ";

		if ( $this->property_ids ) {
			$condition .= " AND property_id IN (" . implode( ',', $this->property_ids ) . ")";
		}

		if ( ! $this->agencia_rs['fotocasa_export_all'] ) {
			$condition .= "
				AND property_id IN (
					SELECT property_id
					FROM `" . $this->agencia_rs['dbname'] . "`.inmo__property_to_portal
					WHERE fotocasa = 1
				)
			";
		}

		ExportCommon::p( "Consulta immobles: " . $this->agencia_rs['comercialname'], 1 );
		ExportCommon::p( $condition, 3 );

		return $condition;

	}

	private function build_agencia_json( $condition ) {

		ExportCommon::p( "Exportar tots: " . ( $this->agencia_rs['fotocasa_export_all'] ? 'Sí' : 'No' ), 3 );

		$this->agencia_exported_ids = [];

		$query   = "
			SELECT * FROM `" . $this->agencia_rs['dbname'] . "`.inmo__property
			WHERE " . $condition;
		$results = Db::get_rows( $query );

		Debug::p( [
			[ 'Consulta Immobles', $query ],
		], 'Fotocasa' );

		ExportCommon::p( "Total immobles a exportar: " . count( $results ), 3 );
		$total_property_exported = 0;


		$config_results = $this->get_config_results();

		foreach ( $results as $key => $rs ) {

			$this->property_rs     = &$rs;
			$this->export_rs       = [];
			$ex                    = &$this->export_rs;
			$ex['PropertyFeature'] = $this->get_default_features(); // Inicialitzo per que es obligatori i serveix per texts i caraterístiques

			$total_property_exported ++;

			// $this->export_rs['Caracteristicas'] = [];

			// Altres Valors
			$ex['ShowSurface']   = true;
			$ex['ContactTypeId'] = 3; // 1 Agencia, 2 Agent, 3 Especificat // Dono l'especificat cada cop, es el de la pàgina web
			// TODO-i Posar usuari intranet $ex['ContactName']        = $this->agencia_rs['respname'] . ' ' . $this->agencia_rs['surnames'];
			$ex['IsPromotion']          = false; // Promocions es un conjunt de propietats en nova construcció, no en tenim
			$ex['PropertyPublications'] = $this->agencia_rs['publications'];

			foreach ( $config_results as $cg_rs ) {

				$this->config_rs   = &$cg_rs;
				$letnd_val     = $rs[ $cg_rs['letnd_field'] ];
				$letnd_field   = $cg_rs['letnd_field'];
				$fotocasa_field    = $cg_rs['fotocasa_field'];
				$fotocasa_subfield = $cg_rs['fotocasa_subfield'];
				$function          = $cg_rs['function'];
				$cf_condition      = $cg_rs['condition'];

				// Si hi ha funció no agafa del'array automàticament
				if ( $function ) {
					$letnd_val = $val = $this->{$function}( $letnd_val );
				} // si hi ha array 'letnd' => 'fotocasa', així no cal fer funció
				elseif ( isset( $this->{"fotocasa_$letnd_field"} ) ) {
					$letnd_val = $this->{"fotocasa_$letnd_field"}[ $letnd_val ];
				}

				// obté true o false segons condició
				if ( $cf_condition ) {

					// Així també funciona al cridar una funció ( al camp condició si posa $val
					$val = $letnd_val;

					$ex[ $fotocasa_field ] = eval( "return " . $cf_condition . ";" );
				}
				elseif ( $fotocasa_subfield ) {
					if ( $letnd_val ) {
						if ( ! isset( $ex[ $fotocasa_field ] ) ) {
							$ex[ $fotocasa_field ] = [ [] ];
						}
						$ex[ $fotocasa_field ][0][ $fotocasa_subfield ] = $letnd_val;
					}
				}
				else {
					if ( $letnd_val ) {
						$ex[ $fotocasa_field ] = $letnd_val;
					}
				}
			}

			$this->get_anunci();
			$this->get_images();
			$this->get_contact_details();

			$this->config_rs = [];

			ExportCommon::p( $rs['ref'] . " , id: " . $rs['property_id'], 2 );
			$this->export_property();
		}

		// Comprovo les propietats publicades del'agencia
		$this->check_published_properties();


		ExportCommon::p( "Total immobles exportats: " . $total_property_exported, 3 );

		$this->property_rs = [];
		$this->export_rs   = [];

	}


	function export_property() {

		STATIC $export_attempt = 1;

		$property                      = $this->export_rs;
		$property_id                   = $this->property_rs['property_id'];
		$this->agencia_exported_ids [] = $this->export_rs['ExternalId'];


		// $this->foto_casa_url = HOST_URL . "sleep_proba.php";

		// $property = Unirest\Request\Body::json($property); // no cal , no es application/json
		$property_json = json_encode( $property, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES );

		Debug::p( [
			[ 'exported_id', $this->export_rs['ExternalId'] ],
			[ 'exported_json', $property ],
		], 'Fotocasa' );

		// El puto entrecomillat em fa escapar aquests caràcters
		$property = str_replace( '\\', '\\\\', $property_json );
		$property = str_replace( "'", "\'", $property );
		$property = "'" . $property . "'"; // El json debe ir amb el puto entrecomillado

		$api_url = $this->foto_casa_url . '/api/property';
		// Nou immoble
		$response = Unirest\Request::post( $api_url, $this->headers, $property );
		$this->log_json( $api_url, $property_json, $response );

		$code = $response->body->Code;

		// Editar immoble - Si dona aquest error, es que ja existeix l'inmoble i llavors hag d'actualitzar-lo
		if ( $code == 'Error_110' ) {
			$response = Unirest\Request::put( $api_url, $this->headers, $property );
			$this->log_json( $api_url, $property_json, $response );
			$code = $response->body->Code;
			/*$this->response_errors [] = [
				'error'       => 'Error_110',
				'code'        => $code,
				'status_code' => $response->body->StatusCode,
				'message'     => $response->body->Message,
				'property_id' => $property_id,
			];*/
		}

		if ( in_array( $code, [ 'Ok_201', 'Ok_203', 'Ok_204' ] ) ) {
			ExportCommon::p( print_r( $response->body, true ), 2 );
		}
		elseif ( in_array( $code, [ 'Ok_205', 'Ok_206' ] ) ) {
			ExportCommon::p( print_r( $response->body, true ), 'warning' );
			ExportCommon::p( $property_json, 2 );
		}
		elseif ( in_array( $code, [ 'Error_1601', 'Error_1602' ] ) ) {
			// A fotocasa no permeten canviar la categoria, per tant haig de posar el category_id al darrera per poder publicar amb una altra categoria
			ExportCommon::p( print_r( $response->body, true ), 'warning' );
			$this->delete_property( $property_id );
			$this->export_rs['ExternalId'] = $this->property_rs['property_id'] . '-' . $this->property_rs['category_id'];
			$export_attempt ++;
			if ( $export_attempt < 10 ) $this->export_property();

			return;
		}
		else {
			ExportCommon::p( print_r( $response->body, true ), 'error' );
			$this->response_errors [] = [
				'error'       => 'Altres',
				'code'        => $code,
				'status_code' => $response->body->StatusCode,
				'message'     => $response->body->Message,
				'property_id' => $this->property_rs['property_id'],
			];
			ExportCommon::p( $property_json, 2 );
		}

		$export_attempt = 1;

		return;

	}

	function delete_property( $property_id, $has_category_id = true ) {

		// Unirest\Request::curlOpt( CURLOPT_USERAGENT, 'Letnd' );
		$category_id = '';
		if ( ! $has_category_id ) {
			$query       = "SELECT category_id FROM `" . $this->agencia_rs['dbname'] . "`.inmo__property WHERE property_id = '$property_id'";
			$category_id = Db::get_first( $query );
		}

		$api_url  = $this->foto_casa_url . '/api/v2/property/' . base64_encode( $property_id );
		$response = Unirest\Request::delete( $api_url, $this->headers );
		$this->log_json( $api_url, 'Delete', $response );

		$code = $response->body->Code;

		if ( $code != 'Ok_202' ) {

			ExportCommon::p( print_r( $response->body, true ), 'warning' );
			ExportCommon::p( "Delete: $property_id", 2 );

			$this->response_errors [] = [
				'error'       => 'not_deleted',
				'code'        => $code,
				'status_code' => $response->body->StatusCode,
				'message'     => $response->body->Message,
				'property_id' => $property_id,
			];


			if ($category_id) {

				$api_url  = $this->foto_casa_url . '/api/v2/property/' . base64_encode( $property_id . '-' . $category_id );
				$response = Unirest\Request::delete( $api_url, $this->headers );
				$this->log_json( $api_url, 'Delete', $response );

				$code = $response->body->Code;

				if ( $code != 'Ok_202' ) {

					ExportCommon::p( print_r( $response->body, true ), 'error' );
					ExportCommon::p( "Delete: $property_id-$category_id", 2 );

					$this->response_errors [] = [
						'error'       => 'not_deleted',
						'code'        => $code,
						'status_code' => $response->body->StatusCode,
						'message'     => $response->body->Message,
						'property_id' => $property_id,
					];
				}
			}
		}
		else {
			ExportCommon::p( print_r( $response->body, true ), 'warning' );
			ExportCommon::p( "id: " . $property_id, 2 );
		}

	}

	function get_publications() {

		$api_url  = $this->foto_casa_url . '/api/publication';
		$response = Unirest\Request::get( $api_url, $this->headers );
		$this->log_json( $api_url, 'Publications', $response );

		if ( $response->code == '401' ) return false;

		$ret = json_decode( $response->body );

		// ExportCommon::p( $response->body, 'error' );

		foreach ( $ret as $key => $val ) {
			if ( ! in_array( $val->PublicationId, $this->fotocasa_publications ) ) {
				unset( $ret[ $key ] );
			}
		}

		// ExportCommon::p( json_encode($ret), 'error' );

		return $ret;

	}

	private function auth() {

		Unirest\Request::auth( $this->agencia_rs['inmo_fotocasa_user'], $this->agencia_rs['inmo_fotocasa_pass'] );
	}

	private function get_contact_details() {
		/*
		1	Email
		2	Teléfono
		3	Móvil
		4	Fax
		5	Web
		6	AIM/AOL
		7	Yahoo!
		8	Messenger
		9	Windows Live Messenger
		10	ICQ
		11	Jabeer/Google Talk
		12	Skype
		13	Facebook
		14	Twitter
		15	SlideShare
		16	Blog
		17	Otros
		*/

		$this->export_rs['PropertyContactInfo'] = [];
		$ex                                     = &$this->export_rs['PropertyContactInfo'];

		$ex [] = [
			'TypeId'      => 1,
			'Value'       => $this->agencia_rs['default_mail'],
			'ValueTypeId' => 1
		];
		$ex [] = [
			'TypeId'      => 2,
			'Value'       => $this->agencia_rs['page_phone'],
			'ValueTypeId' => 1
		];
		$ex [] = [
			'TypeId'      => 4,
			'Value'       => $this->agencia_rs['page_fax'],
			'ValueTypeId' => 1
		];
		$ex [] = [
			'TypeId'      => 5,
			'Value'       => $this->agencia_rs['url'],
			'ValueTypeId' => 1
		];

		$this->export_rs['PropertyUser'] = [];
		$ex                              = &$this->export_rs['PropertyUser'];
		$ex []                           = [
			'UserId'      => (int) $this->agencia_rs['inmo_fotocasa_id_agente'],
			'IsPrincipal' => true
		];

	}

	// Funcions per camps
	function get_ref( $ref ) {
		return ExportCommon::get_ref( $ref, $this->property_rs, $this->agencia_rs );
	}

	function get_category( $val ) {

		$query = "
			SELECT subtype_id,
				( SELECT building_type_id FROM export__fotocasa_building_subtype WHERE export__fotocasa_building_subtype.id = export__fotocasa_category_to_subtype.subtype_id )  as building_type_id
			FROM export__fotocasa_category_to_subtype 
			WHERE category_id = $val";

		$ret = Db::get_row( $query );

		$this->export_rs['SubtypeId'] = (int) $ret['subtype_id'];

		return (int) $ret['building_type_id'];

	}

	function get_status( $val ) {

		if ( $val == 'reserved' ) {
			return 2;
		}
		else {
			return 1;
		}
	}

	function get_flat( $val ) {

		$rs = &$this->property_rs;

		$this->export_rs['PropertyAddress'][0]['VisibilityModeId'] = [];

		$mode = &$this->export_rs['PropertyAddress'][0]['VisibilityModeId'];

		// Mostro l'adreça complerta o nomes carrer amb els mateixos criteris del mapa
		/*
		0	Indefinido
		1	Mostrar Calle y Número
		2	Mostrar Calle
		3	Mostrar Sólo zona
		*/
		if ( $rs['show_map'] && $rs['show_map_point'] ) {
			$mode = 1;
		}
		elseif ( $rs['show_map'] ) {
			$mode = 2;
		}
		else {
			$mode = 3;
		}

		// Obtinc el pis
		if ( isset( $this->fotocasa_flat[ $val ] ) ) {
			return $this->fotocasa_flat[ $val ];
		}

		$val = strtoupper( $val );

		if ( $val == 'B' || $val == 'BAIXOS' || $val == 'BAJOS' ) {
			return '24';
		}

		return false;
	}

	function get_zone( $val ) {
		$rs = &$this->property_rs;

		if ( ! ( $rs['show_map'] ) || ! ( $rs['show_map_point'] ) ) {
			return false;
		}

		$query = "SELECT
				zone FROM `" . $this->agencia_rs['dbname'] . "`.inmo__zone
			 WHERE zone_id = '$val'";

		$zone = Db::get_first( $query );

		return $zone;

	}

	function get_latitude_longitud( $val ) {
		if ( $val == '0.0000000' ) {
			return false;
		}
		else {
			return $val;
		}
	}

	function get_price( $val ) {

		$floor_space = $this->property_rs['floor_space'];
		if ( ! $floor_space && $this->property_rs['category_id'] == '5' ) {
			$floor_space = $this->property_rs['land'];
		}
		$this->export_rs['PropertyTransaction'][0]['PriceM2']    = (float) ( $val / $floor_space );
		$this->export_rs['PropertyTransaction'][0]['CurrencyId'] = 1;
		$this->export_rs['PropertyTransaction'][0]['ShowPrice']  = $this->property_rs['price'] && ! $this->property_rs['price_consult'];

		return (float) $val;

	}


	function get_anunci() {

		$id    = $this->property_rs['property_id'];
		$query = "SELECT language, description, property 
				FROM `{$this->agencia_rs['dbname']}`.inmo__property_language
			    WHERE property_id = '$id '
			    ORDER BY FIELD (LANGUAGE,'spa') DESC";

		$results = Db::get_rows( $query );


		// 2 	Descripción abreviada 	TextValue
		// 3 	Descripción extendida 	TextValue


		foreach ( $results as $rs ) {
			$fotocasa_language = isset( $this->fotocasa_languages[ $rs['language'] ] ) ? $this->fotocasa_languages[ $rs['language'] ] : false;
			if ( $fotocasa_language !== false ) {

				$property    = $rs['property'];
				$description = $rs['description'];

				if ( $property ) {
					$this->export_rs ['PropertyFeature'][] = [
						'FeatureId'  => 2,
						'LanguageId' => $fotocasa_language,
						'TextValue'  => $rs['property'],
					];
				}
				if ( $description ) {
					$this->export_rs ['PropertyFeature'][] = [
						'FeatureId'  => 3,
						'LanguageId' => $fotocasa_language,
						'TextValue'  => $rs['description'],
					];
				}
			}
		}

		// return true;
	}

	/*
	// Caràcterístiques i funcions de característiques
	*/
	function add_custom_feature( $feature_id, $value_type, $val ) {
		$this->export_rs ['PropertyFeature'][] = [
			'FeatureId'  => $feature_id,
			'LanguageId' => 4,
			$value_type  => $val
		];
	}

	function get_default_features() {
		return [
			[
				'FeatureId'  => 4,
				'LanguageId' => 4,
				'BoolValue'  => false
			],
		];
	}

	// SI es un terreny, la superfície pot ser "land"
	function get_floor_space( $val ) {

		if ( ! $val && $this->property_rs['category_id'] == '5' ) {
			$val = $this->property_rs['land'];
		}

		return $this->get_feature( $val );

	}

	function get_land( $val ) {

		if ( $this->property_rs['category_id'] == '5' ) {
			return false;
		}

		return $this->get_feature( $val );

	}

	function get_efficiency( $val ) {

		if ( $val == 'exempt' ) {
			$this->add_custom_feature( 327, 'DecimalValue', 3 );

			return false;
		}
		if ( $val == 'progress' ) {
			$this->add_custom_feature( 327, 'DecimalValue', 2 );

			return false;
		}

		$energy = $this->fotocasa_efficiency[ $val ];
		$this->add_custom_feature( 323, 'DecimalValue', $energy );

		$energy_number = $this->property_rs['efficiency_number'];
		$this->add_custom_feature( 325, 'DecimalValue', (float) $energy_number );

		$emission = $this->property_rs['efficiency2'];
		if ( $emission != 'notdefined' ) {
			$emission = $this->fotocasa_efficiency[ $emission ];
			$this->add_custom_feature( 324, 'DecimalValue', $emission );


			$emission_number = $this->property_rs['efficiency_number2'];
			$this->add_custom_feature( 326, 'DecimalValue', (float) $emission_number );
		}

		$this->add_custom_feature( 327, 'DecimalValue', 1 );

		return false;
	}

	/*
	 * Feature 109 i 110, no implementat, per gres i parquet ja hi han els bàsics
	function get_floor_type( $val ) {

		if ($this->export_rs['TypeId'] == 1) {
			$this->config_rs['fotocasa_feature_id'] = 110;
		}

		if ($val == '1') {
			$val = $this->fotocasa_floor_type[ $this->config_rs['letnd_field'] ];
			return $this->get_feature( $val );
		}
		else return false;
	}*/
	function get_construction_date( $val ) {

		if ( $val == '0000-00-00' ) {
			return false;
		}

		$year = date( "Y", strtotime( $val ) );

		return $this->get_feature( $year );
	}

	function get_feature( $val ) {

		if ( ! $val ) {
			return false;
		}
		$cf                = &$this->config_rs;
		$value_type        = $cf['fotocasa_feature_value_type'];
		$building_excluded = explode( ',', $cf['fotocasa_building_types_excluded'] );
		$letnd_field   = $cf['letnd_field'];
		$type_id           = isset( $this->export_rs['TypeId'] ) ? $this->export_rs['TypeId'] : false;

		if ( $value_type == 'DecimalValue' ) {
			$val = (float) $val;
		}
		elseif ( $value_type == 'BoolValue' ) {
			$val = (bool) $val;
		}
		elseif ( $value_type == 'List' ) {
			$value_type = 'DecimalValue';
			$val        = $this->{"fotocasa_$letnd_field"}[ $val ];
		}
		elseif ( $value_type == 'Multiple' ) {
			// NO HI HA CAP MULTIPLE IMPLEMENTAT
			$value_type = 'DecimalValue';
			// no pot ser automatic ja que diferents camps agafen el mateix array, s'ha de fer a la funció
			$val = (float) $val;
		}

		// Després de convertir a num o bool pot donar false un altre cop
		// No cal posar al JSON els que tenen valors 0, 0.00 o FALSE
		// Els que estan posats abans a la bbdd, no tenen el type_id ja que encara no s'ha buscat
		if ( ! $val
		     ||
		     ( $type_id && in_array( $type_id, $building_excluded ) ) ) {
			return false;
		}

		$feature = [
			'FeatureId'  => (int) $cf['fotocasa_feature_id'],
			'LanguageId' => 4,
		];

		$feature [ $value_type ] = $val;

		$this->export_rs['PropertyFeature'][] = $feature;

		// Retorno false per que no crei el registre, ja l'he creat aquí
		return false;
	}

	function get_images() {

		if ( R::get_opt( 'exclude_images' ) ) return; // Només per testejar

		$id = $this->property_rs['property_id'];

		$data_base = $this->agencia_rs['dbname'];

		// TODO-i Idioma poso castellà per defecte
		$query  = "SELECT
				`$data_base`.inmo__property_image.image_id as image_id, name, image_title
				FROM `$data_base`.inmo__property_image, `$data_base`.inmo__property_image_language
				WHERE property_id = " . $id . "
				AND `$data_base`.inmo__property_image.image_id = `$data_base`.inmo__property_image_language.image_id
				AND language = 'spa'
				ORDER BY main_image DESC, ordre";
		$images = Db::get_rows( $query );

		if ( $images ) {
			$this->export_rs ['PropertyDocument'] = [];

			$ordre = 0;

			foreach ( $images as $image ) {

				// CodigoPublicador-CodigoProducto-Orden.jpg
				$ext       = strrchr( substr( $image['name'], - 5, 5 ), '.' );
				$image_id  = $image['image_id'];
				$image_url = $this->client_images_url . $image['image_id'] . '_medium' . strrchr( substr( $image['name'], - 5, 5 ), '.' );
				$ordre ++;

				$ext_lower    = strtolower( $ext );
				$file_type_id = 0;
				if ( $ext_lower == '.gif' ) {
					$file_type_id = 1;
				}
				elseif ( $ext_lower == '.jpg' ) {
					$file_type_id = 2;
				}
				elseif ( $ext_lower == '.bmp' ) {
					$file_type_id = 22;
				}
				elseif ( $ext_lower == '.tif' || $ext_lower == '.tiff' ) {
					$file_type_id = 23;
				}
				elseif ( $ext_lower == '.jpeg' ) {
					$file_type_id = 34;
				}
				elseif ( $ext_lower == '.png' ) {
					$file_type_id = 35;
				}

				if ( ! $file_type_id ) {
					continue;
				}

				$this->export_rs ['PropertyDocument'][] = [
					'TypeId'      => 1,
					'Description' => $image['image_title'],
					'Url'         => $image_url,
					'FileTypeId'  => $file_type_id,
					'Visible'     => true,
					'SortingId'   => $ordre,
				];
			}

		}

		if ( $images && ! $this->export_rs ['PropertyDocument'] ) {
			unset( $this->export_rs ['PropertyDocument'] );
		}
	}

	function __get_date( $val ) {
		return date( "d/m/Y", strtotime( $val ) );
	}

	function __get_parking( $val ) {
		$garage = $this->property_rs['garage'];

		return $this->get_caracteristica( $val || $garage );
	}

	function __get_view( $val ) {
		$clear_view = $this->property_rs['clear_view'];

		return $this->get_caracteristica( $val || $clear_view );
	}

	// Genériques

	function get_int( $val ) {
		return (int) $val;
	}

	private function log_json( $api_url, $json_sent, $json_response ) {
		ExportCommon::log_json( $api_url, $json_sent, $json_response, $this->agencia_rs, $this->exporter );
	}


	private function trace( $text, $name = '', $title = false ) {
		ExportCommon::trace( $text, $name, $title, 'fotocasa' );
	}

	// Funcions per gestió desde admintotal


	// Obtinc tots els immobles de fotocasa. Per comprobar el primer cop si se'n esborrarà algún
	// Posar manualment les dades
	public function admintotal_get_published() {

		$this->set_file( 'admintotal/fotocasa.tpl' );

		$api_key = R::text_id( 'api_key', false, '_POST' );
		$this->set_var( 'api_key', $api_key );
		$this->set_var( 'immobles', '' );


		if ( $api_key ) {

			Unirest\Request::auth( 'api_key', $api_key );

			$api_url  = $this->foto_casa_url . '/api/publication/property';
			$response = Unirest\Request::get( $api_url, $this->headers );

			$ret = json_decode( $response->body );

			$this->set_var( 'immobles', "<pre>   <code  class=\"language-php\">" . var_export( $ret, true ) . "</code></pre>" );

		}


		$GLOBALS['gl_content'] = $this->process();

	}


}