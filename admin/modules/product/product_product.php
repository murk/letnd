<?
include_once(DOCUMENT_ROOT . 'common/includes/phpqrcode/qrlib.php');

class ProductProduct extends Module {
	var $q = '';
	var $form_tabs;
	var $variations_listing = [];
	var $order_by, $is_excel = false;
    function __construct() // constructor, ha de ser el mateix nom de la clase per inicialitzar-le
    {
        parent::__construct();
        $this->order_by = 'status asc, product_title ASC,ordre ASC';
    }
	function on_load() {

		$this->form_tabs = array(
			'0' => array(
				'tab_action'=>$this->action == 'show_variations'?false:'show_variations',
				'tab_caption'=>$this->caption['c_edit_variations']
			),
			'1' => array(
				'tab_action'=>$this->action == 'show_specs'?false:'show_specs',
				'tab_caption'=>$this->caption['c_edit_specs']
			),
			'2' => array(
				'tab_action'=>$this->action == 'show_relateds'?false:'show_relateds',
				'tab_caption'=>$this->caption['c_edit_relateds']
			)
		);
		if ($this->config['is_specs_on'] == '0')
			unset ( $this->form_tabs[1] );

		// Camp descripcio en HTML
		if ($this->config['is_description_html'] == '1'){
			$this->set_field('product_description','type','html');
			$this->set_field('product_description','textarea_rows','');
		}

		if ($this->config['hide_fields']){
			$hide_fields = explode( ',', $this->config['hide_fields'] );
			foreach ( $hide_fields as $field ) {

			}
		}

		if ( $this->config['is_sell_on'] == '0' ) {
			$this->set_field( 'sells', 'list_admin', '0' );
			$this->set_field( 'min_quantity', 'list_admin', '0' );
			$this->set_field( 'max_quantity', 'list_admin', '0' );
			$this->set_field( 'discount', 'list_admin', '0' );

			if ( $this->action == 'export' ) {
				$this->unset_field( 'sells' );
				$this->unset_field( 'sell' );
				$this->unset_field( 'free_send' );
				$this->unset_field( 'discount' );
				$this->unset_field( 'discount_fixed_price' );
				$this->unset_field( 'discount_wholesaler' );
				$this->unset_field( 'discount_fixed_price_wholesaler' );
				$this->unset_field( 'stock' );
				$this->unset_field( 'last_stock_units_public' );
				$this->unset_field( 'last_stock_units_admin' );
				$this->unset_field( 'min_quantity' );
				$this->unset_field( 'max_quantity' );
				$this->unset_field( 'quantityconcept_id' );
			}

		}

		if ( $this->config['is_characteristics_on'] == '0' ) {

			if ( $this->action == 'export' ) {
				$this->unset_field( 'brand_id' );
				$this->unset_field( 'guarantee' );
				$this->unset_field( 'guarantee_period' );
				$this->unset_field( 'year' );
				$this->unset_field( 'format' );
				$this->unset_field( 'weight' );
				$this->unset_field( 'weight_unit' );
				$this->unset_field( 'height' );
				$this->unset_field( 'height_unit' );
				$this->unset_field( 'temperature' );
				$this->unset_field( 'temperature_unit' );
				$this->unset_field( 'util_life' );
				$this->unset_field( 'util_life_unit' );
				$this->unset_field( 'color_id' );
				$this->unset_field( 'material_id' );
				$this->unset_field( 'power' );
				$this->unset_field( 'alimentacion' );
				$this->unset_field( 'certification' );
				$this->unset_field( 'ishandmade' );
			}

		}

		if ( $this->config['is_sector_on'] == '0' ) {
			$this->set_field( 'sector_id', 'list_admin', '0' );

			if ( $this->action == 'export' ) {
				$this->unset_field( 'sector_id' );
			}
		}

		if ( $this->config['is_pvp_on'] == '0' ) {
			$this->set_field( 'pvp', 'list_admin', '0' );
			$this->set_field( 'price_cost', 'list_admin', '0' );

			if ( $this->action == 'export' ) {
				$this->unset_field( 'pvp' );
				$this->unset_field( 'price_consult' );
				$this->unset_field( 'price_cost' );
			}
		}

		if ( $this->config['is_pvd_on'] == '0' ) {
			$this->set_field( 'pvd', 'list_admin', '0' );

			if ( $this->action == 'export' ) {
				$this->unset_field( 'pvd' );
				$this->unset_field( 'pvd2' );
				$this->unset_field( 'pvd3' );
				$this->unset_field( 'pvd4' );
				$this->unset_field( 'pvd5' );
			}
		}

		if ( $this->config['is_pvp_on'] == '0' && $this->config['is_pvd_on'] == '0' ) {
			$this->set_field( 'tax_id', 'list_admin', '0' );
			$this->set_field( 'price_unit', 'list_admin', '0' );

			if ( $this->action == 'export' ) {
				$this->unset_field( 'tax_id' );
				$this->unset_field( 'price_unit' );
			}
		}

		if ( $this->config['is_ref_required'] == '0' ) {
			$this->set_field( 'ref', 'required', '0' );
		}


		parent::on_load();
	}
    function list_records()
    {
        $GLOBALS['gl_content'] = $this->get_records();
    }
    function get_records()
    {
		$this->set_other_config();
        unset($this->caption['c_subfamily_id_format']);
		$this->set_field('subfamily_id','select_fields','product__family.family_id,family');

		// mostro desplegable de subfamilies relatives a la familia seleccionada
		if (isset($_GET['family_id']) && $_GET['family_id']!='null'){
			$this->set_field('subfamily_id','select_condition','product__family.family_id=product__family_language.family_id  AND language=\''.LANGUAGE.'\' AND product__family.parent_id = '.$_GET['family_id'].' ORDER BY ordre ASC, family ASC');
		}

		//Abans de fer ell llistat compro que no hi hagin productes fora d'stock o amb avís.


		// global $gl_content, $gl_action; Ara no es necessita cap de les dues. el gl_content passa a ser el que em dongui la gana per ex. $content, es el que retorno
        // i el $gl_action  passa a ser una propietat $this->action
		$listing = new ListRecords($this);

		$listing->add_filter('family_id');
		$listing->add_filter('subfamily_id');
		$listing->add_filter('status');
		$listing->add_filter('destacat');
		$listing->add_filter('prominent');

		$listing->order_by = 'status asc, product_title ASC,ordre ASC';


		if (isset($_GET['destacat']) && $_GET['destacat']!='null') {
			$listing->set_field('destacat','list_admin','input');
		}
		if (isset($_GET['prominent']) && $_GET['prominent']!='null') {
			$listing->set_field('prominent','list_admin','input');
		}

    	$search_content = '';

        switch ($this->process) { // process-> propietat invetada que passo pel post del tpl
            case 'search_ref':
                $listing->condition .= $this->search_search_condition();
            	$search_content = $this->get_search_content();
                break;
            case 'search_words':
				$listing->condition .= $this->search_list_condition_words();
				$search_content = $this->get_search_content();
                break;
            case 'search_ref_select': // poso els 2 casos junts i sense el break
            case 'search_words_select'://expresament per que executi també el 'select_product'
                if ($this->process == 'search_ref_select') {
                    $listing->condition .= $this->search_search_condition();
                }else {
                    $listing->condition .= $this->search_list_condition_words();
                }
            case 'select_product': // llistat dins iframe per seleccionar productes

                break;
        	default:
				$search_content = $this->get_search_content();
        } // switch*/

		$is_newsletter_frame = R::text_id('is_newsletter_frame');

		// llistat dins iframe per seleccionar productes
		if ($is_newsletter_frame) {
			$page = &$GLOBALS['gl_page'];
			$page->template = 'clean.tpl';
			$page->title = '';
			$listing->set_field('sells', 'list_admin', 'no');
			$listing->set_field('ordre', 'list_admin', 'no');
			$listing->call('list_records_walk_select', 'product_id,product_title');
			$listing->set_options(0, 0, 0, 0, 0, 0, 0, 0, 1);
			if ($listing->condition) $listing->condition .= ' AND ';
			$listing->condition .= "(status = 'onsale' OR status = 'nostock' OR status = 'nocatalog')";
			$GLOBALS['gl_message'] = $GLOBALS['gl_messages']['select_item'];
			$search_content = '';
		}

		else  {
			if ($this->config['is_specs_on'] == '1')
				$listing->add_button ('edit_specs_button', 'action=show_specs', 'boto1', 'after');

			if ($this->config['is_sector_on'] == '1' && $this->config['show_sector_at_listing'] == '1'){
				$this->set_field('sector_id', 'list_admin', 'input');
			}

			$listing->add_button ('edit_variations_button', 'action=show_variations', 'boto1', 'after');

			$listing->add_options_button('export_button', 'action=export', 'botoOpcions1 excel', 'top_after');
		}

		// per poder llistar tots els productes d'una família concreta, per exemple desde el llistat de famílies
	    if ( R::number( 'family' ) ) {
		    $familia = R::number( 'family' );
		    if ( $listing->condition ) {
			    $listing->condition .= ' AND ';
		    }
		    $listing->condition .= $this->get_family_or_subfamily( $familia );
	    }


		// imprimir productes
		if ($GLOBALS['gl_page']->template == 'print.tpl'){
			set_time_limit(0);
			// ara esta a 128M per defecte // ini_set("memory_limit","50M");
			// kaskote ho volien així, s'ha de fer amb els camps opcionals
			$listing->set_field('product_description','list_admin','text');
			$listing->unset_field('ordre');
			$listing->unset_field('sells');
			$listing->unset_field('status');
			$listing->unset_field('pvp');
			$listing->unset_field('product_title');
			$listing->paginate = false;
		}

		if ( $this->is_excel ) {
			return $listing;
		}

        return $search_content . $listing->list_records();
    }
	function get_family_or_subfamily($family_id){

		// averiguo si es familia o subfamilia
		$parent_id = Db::get_first("SELECT parent_id FROM product__family WHERE family_id ='".$family_id."'");
		if ($parent_id){
			$parent_parent_id = Db::get_first("SELECT parent_id FROM product__family WHERE family_id ='".$parent_id."'");
			if ( $parent_parent_id ) {
				$ret = "subsubfamily_id = $family_id  AND subsubfamily_id = 0";
			}
			else {
				$ret = 'subfamily_id = ' . $family_id;
			}
		}
		else {
			$ret = "family_id = $family_id AND subfamily_id = 0";
		}
		return $ret;

	}
    function show_form() // mostra
    {
        $GLOBALS['gl_content'] = $this->get_form();
    }
    function get_form() // obtinc
    {
		$this->set_other_config();

		if ($this->config['videoframe_info_size']) $this->caption['c_videoframe'] = $this->caption['c_videoframe'] . '<br /> ( ' . $this->config['videoframe_info_size'] . ' )';

		$this->set_var('product_form_title', $this->get_product_title());
        $show = new ShowForm($this);
		$show->form_tabs = $this->form_tabs;
		//activo o desactivo el seo
		$show->set_var('show_seo',$_SESSION['show_seo']);

		if (!$_SESSION['show_seo']) {
			$show->unset_field('page_title');
			$show->unset_field('page_description');
			$show->unset_field('page_keywords');
			$show->unset_field('product_file_name');
		}

		// referncia automatica
		if ($this->config['auto_ref'])
		{
			$show->set_field('ref','form_admin','text');
			$show->set_field('ref','default_value',$this->caption['c_auto_ref']);
		}

        // missatge de iva inclos o no inclos
		$show->caption['c_is_public_tax_included'] = $this->config['is_public_tax_included']?$this->caption['c_tax_included']:$this->caption['c_tax_not_included'];
		$show->caption['c_is_wholesaler_tax_included'] = $this->config['is_wholesaler_tax_included']?$this->caption['c_tax_included']:$this->caption['c_tax_not_included'];


    	$show->set_var('variation_link', $this->link . '&action=show_variations&menu_id=20004&product_id=' . $show->id);

		// stock - si hi ha variacions, agafo l'stock de variacions
		$stock = $this->get_variations_total_stock($show->id);


		if ($stock!==false){
			$show->set_field('stock','form_admin','text');
			$show->get_values();
			$show->rs['stock'] = $stock;
		}
		$show->set_record();

		$this->get_others_array($show->rs);

        return $this->get_search_content() . $show->process();
    }
	function show_variations(){

		$prices = true; // aprofito els get_product_title que ja fa una consulta del producte per agafar el preu
		$ref = ''; // passo per referencia a get_product_title
		$this->set_var('product_form_title', $this->get_product_title($ref,'',$prices));

	    $this->set_var ('form_tabs' , $this->form_tabs);
		$id = $_GET['product_id'];
		$this->file = 'product/product_variations.tpl';

		$this->set_var('menu_id', $GLOBALS['gl_menu_id']);
		$this->set_vars($prices);
		$this->set_vars($this->caption);
		// obtinc les categories
		$query = "SELECT DISTINCT variation_category_id, variation_category 
					FROM product__variation_category_language 
					INNER JOIN product__variation
					USING (variation_category_id)
					WHERE language = '" . LANGUAGE . "'
					AND product__variation.bin=0
					ORDER BY variation_category_id";
		$results = Db::get_rows($query);
		$loop = array();
		$assign_selected_ids = array();
		foreach ($results as $rs)
		{
			$variations = $this->get_variation($rs['variation_category_id'],$rs['variation_category'],$ref);
			$loop[] = array(
						'variation'=>$variations['variation'],
						'variation_category_id' => $rs['variation_category_id'],
						'variation_category' => $rs['variation_category']);

			$assign_selected_ids = array_merge($assign_selected_ids, $variations['ids']);
		}

		$this->set_vars(array(
			'assign_selected_ids'=>json_encode($assign_selected_ids),
			'id'=>$id,
			'loop'=>$loop,
			'image_link'=>'/admin/?action=list_records_images&menu_id=20004&product_id='.$id,
			'data_link'=>'/admin/?action=show_form_edit&menu_id=20004&product_id='.$id,
			'form_link'=>'?tool=product&tool_section=variation&product_id='.$id,
			'id_field'=>$this->id_field
			));
		$GLOBALS['gl_content'] =  $this->get_search_content() . $this->process();
	}
	function show_new_variations(){

			$variations = $this->get_variation(false);

			echo $variations['variation'];
			Debug::p_all();
			die();
	}
	function get_variation($variation_category_id,$variation_category=0,$ref_product=''){

		$query_ids = '';
		$ids = array();
		$query_category = '';
		$default_found = false;
		$selected_ids = array();
		$selected_item = array();
		$first_selected = -1;
		$is_ajax = $variation_category_id==0;

		if (!$is_ajax){

			$id = R::id('product_id');
			// ids guardats perl producte
			$query = "SELECT 
							product_variation_id,
							product__variation.variation_id as variation_id, 
							product__product_to_variation.variation_status as variation_status, 
							product__product_to_variation.variation_pvp as variation_pvp, 
							product__product_to_variation.variation_pvd as variation_pvd, 
							product__product_to_variation.variation_pvd2 as variation_pvd2, 
							product__product_to_variation.variation_pvd3 as variation_pvd3, 
							product__product_to_variation.variation_pvd4 as variation_pvd4, 
							product__product_to_variation.variation_pvd5 as variation_pvd5,
							product__product_to_variation.variation_others1 as variation_others1,
							product__product_to_variation.variation_others2 as variation_others2,
							product__product_to_variation.variation_others3 as variation_others3,
							product__product_to_variation.variation_others4 as variation_others4,
							product__product_to_variation.variation_stock as variation_stock,
							product__product_to_variation.variation_last_stock_units_public as variation_last_stock_units_public,
							product__product_to_variation.variation_last_stock_units_admin as variation_last_stock_units_admin,
							product__product_to_variation.variation_ref as variation_ref,
							product__product_to_variation.variation_ref_supplier as variation_ref_supplier,
							product__product_to_variation.variation_sells as variation_sells,     
							product__product_to_variation.variation_default as variation_default 
						FROM product__product_to_variation, product__variation 
						WHERE product__product_to_variation.variation_id = product__variation.variation_id
						AND variation_category_id = " . $variation_category_id . "
						AND product_id = '" . $id . "'";
		}
		else{
			// ids nous
			$new_ids = R::ids('ids');

			$query = "SELECT 
							0 as product_variation_id,
							product__variation.variation_id as variation_id, 
                            'onsale' as variation_status,
							variation_pvp,
							variation_pvd,
							variation_pvd2, 
							variation_pvd3, 
							variation_pvd4, 
							variation_pvd5,
							'' as variation_others1,
							'' as variation_others2,
							'' as variation_others3,
							'' as variation_others4,
							0 as variation_stock,
							0 as variation_last_stock_units_public,
							0 as variation_last_stock_units_admin,
							'' as variation_ref,
							'' as variation_ref_supplier,
							0 as variation_sells,     
							variation_default 
						FROM product__variation 
						WHERE variation_id IN (" . $new_ids . ")";
		}
		$results = Db::get_rows($query);

		foreach ($results as $rs)
		{
			$selected_ids[] = $rs['variation_id'];
			$selected_item[$rs['variation_id']] = $rs;
		}

		if ($results) {
			$query_ids = '\''.implode($selected_ids,"','").'\'';
		}
		else{
			return array('variation' => '', 'ids'=>$ids);
		}

		// si hi ha resultats faig un llistat de variacions per cada categoria

		$module = Module::load('product','variation','',false);
		$listing = $this->variations_listing = new ListRecords($module);

		$this->set_variation_other_config($listing);

		$listing->order_by = 'ordre ASC,variation ASC';
		if ($variation_category) $listing->caption['c_variation'] = $variation_category; // poso el nom de la categoria en la columna del llistat
		$listing->paginate = false;
		$listing->set_field('ordre','list_admin','no');
		$listing->set_field('variation_category_id','type','hidden');
		$listing->set_field('variation_pvp','type','none');
		$listing->set_field('variation_pvd','type','none');
		$listing->set_field('variation_pvd2','type','none');
		$listing->set_field('variation_pvd3','type','none');
		$listing->set_field('variation_pvd4','type','none');
		$listing->set_field('variation_pvd5','type','none');
		$listing->set_field('variation_status','type','none');
		$listing->set_field('variation_status','list_admin','out');
		$listing->set_field('variation_stock','type','none');
		$listing->set_field('variation_stock','list_admin','out');
		$listing->set_field('variation_last_stock_units_public','type','none');
		$listing->set_field('variation_last_stock_units_public','list_admin','out');
		$listing->set_field('variation_last_stock_units_admin','type','none');
		$listing->set_field('variation_last_stock_units_admin','list_admin','out');
		$listing->set_field('variation_ref','type','none');
		$listing->set_field('variation_ref','list_admin','out');
		$listing->set_field('variation_ref_supplier','type','none');
		$listing->set_field('variation_ref_supplier','list_admin','out');
		$listing->set_field('variation_sells','type','none');
		$listing->set_field('variation_sells','list_admin','out');

		for ($i = 1; $i <= 4; $i++) {
			if ( !$listing->config["variation_others$i"] ) continue;

			$listing->set_field("variation_others$i",'type','none');
			$listing->set_field("variation_others$i",'list_admin','out');
		}

		if ($is_ajax)
			$listing->set_field('variation','type','none');

		$listing->set_var('all_variation_category_id', $variation_category_id);

		if (!$is_ajax){
			$listing->condition = 'variation_category_id = ' . $variation_category_id . ' AND variation_id IN ('.$query_ids.')';
		}
		else{
			$listing->condition = 'variation_id IN ('.$query_ids.')';
		}

		$listing->default_template = "product/variation_list_pick";
		$listing->list_records(false);


		if ($listing->has_results) {
			foreach ($listing->results as $key=>$val){
				$rs = &$listing->results[$key];
				// tots els ids pel javascript				
				$ids[]= $rs['variation_id'];

				$default_selected = '';

				// els que hem guardat a product__product_to_variation hi posem el preu guardat i quin es per defecte i el checkbox seleccionat

				$rs_selected = &$selected_item[$rs['variation_id']];

				$first_selected==-1?$first_selected = $key:false;
				$product_variation_id = $rs_selected['product_variation_id'];
				$status = $rs_selected['variation_status'];
				$pvp = format_currency($rs_selected['variation_pvp']);
				$pvd = format_currency($rs_selected['variation_pvd']);
				$pvd2 = format_currency($rs_selected['variation_pvd2']);
				$pvd3 = format_currency($rs_selected['variation_pvd3']);
				$pvd4 = format_currency($rs_selected['variation_pvd4']);
				$pvd5 = format_currency($rs_selected['variation_pvd5']);
				$variation_others1 = $rs_selected['variation_others1'];
				$variation_others2 = $rs_selected['variation_others2'];
				$variation_others3 = $rs_selected['variation_others3'];
				$variation_others4 = $rs_selected['variation_others4'];
				$stock = format_int($rs_selected['variation_stock']);
				$last_stock_units_public = format_int($rs_selected['variation_last_stock_units_public']);
				$last_stock_units_admin = format_int($rs_selected['variation_last_stock_units_admin']);
				$sells = format_int($rs_selected['variation_sells']);

				if ($this->config['auto_ref']){
					$ref = $ref_product . '-' . $rs['variation_id'];
				}else{
					$ref = $rs_selected['variation_ref'];
				}
				$ref_supplier = $rs_selected['variation_ref_supplier'];

				if ($rs_selected['variation_default'] && !$default_found){
					$default_selected = '  checked="checked"';
					$default_found = true;
				};

				// mostro el nom de la categoria en el llistat de noves variacions
				if ($is_ajax)
					$rs['variation'] = '<strong>' . $this->get_variation_category($rs['variation_category_id']) . ':</strong> ' . $rs['variation'];

				$rs['variation_status'] = $this->get_select($rs['variation_id'], $status, 'status', ['disabled','nostock','onsale']);
				$rs['variation_pvp'] = $this->get_input($rs['variation_id'], $pvp, 'pvp');
				$rs['variation_pvd'] = $this->get_input($rs['variation_id'], $pvd, 'pvd');
				$rs['variation_pvd2'] = $this->get_input($rs['variation_id'], $pvd2, 'pvd2');
				$rs['variation_pvd3'] = $this->get_input($rs['variation_id'], $pvd3, 'pvd3');
				$rs['variation_pvd4'] = $this->get_input($rs['variation_id'], $pvd4, 'pvd4');
				$rs['variation_pvd5'] = $this->get_input($rs['variation_id'], $pvd5, 'pvd5');
				$rs['variation_stock'] = $this->get_input($rs['variation_id'], $stock, 'stock');
				$rs['variation_last_stock_units_public'] = $this->get_input($rs['variation_id'], $last_stock_units_public, 'last_stock_units_public');
				$rs['variation_last_stock_units_admin'] = $this->get_input($rs['variation_id'], $last_stock_units_admin, 'last_stock_units_admin');
				$rs['variation_ref'] = ($this->config['auto_ref'])?$ref:$this->get_input($rs['variation_id'], $ref, 'ref');
				$rs['variation_ref_supplier'] = $this->get_input($rs['variation_id'], $ref_supplier, 'ref_supplier');
				$rs['variation_sells'] = $sells;
				$rs['default_selected'] = $default_selected;

				$languages = &$GLOBALS['gl_languages'];

				for ($i = 1; $i <= 4; $i++) {

					if ( !$listing->config["variation_others$i"] ) continue;
					// idiomes
					if ($listing->config['variation_others'.$i.'_type']=='text')
					{




						$rs['variation_others' . $i] = '';
						foreach ($languages['public'] as $language)
						{
							$val = Db::get_first("SELECT variation_others" .$i . " FROM product__product_to_variation_language WHERE product_variation_id = " . $product_variation_id . " AND language ='".$language."'");
							$lang_name = $languages['names'][$language];

							$rs['variation_others' . $i] =
								$rs['variation_others' . $i] .
								'<div>' . $lang_name . '</div><div class="langList">' .
								$this->get_input(
											$rs['variation_id'],
											$val,
											'others'.$i,
											$language) .
								'</div>';
						}

					}
					else // no idiomes
					{
						$rs['variation_others' . $i] = $this->get_input($rs['variation_id'], ${'variation_others' . $i},'others'.$i);
					}

				}

			}

			// si no he trobat cap default, poso el primer de la llista seleccionat
			if (!$default_found && $results && !$is_ajax){
				$listing->results[$first_selected]['default_selected'] = '  checked="checked"';
			}
			$listing->set_var('is_ajax',$is_ajax);

			$content = $listing->parse_template();
		}
		else{
			$content = '';
		}
		return array('variation' => $content, 'ids'=>$ids);
	}
	function get_variation_category($id){
		return Db::get_first(
			"SELECT variation_category FROM  product__variation_category_language
				WHERE variation_category_id = " . $id . " AND language='".LANGUAGE."'"
				);
	}

	function get_input($variation_id, $value, $field, $language  = false){

		$variation_field = 'variation_' . $field;
		$max_length = $this->variations_listing->fields[ $variation_field ]['text_maxlength'];
		$text_size  = $this->variations_listing->fields[ $variation_field ]['text_size'];

		$id = $variation_field.'_'.$variation_id;
		$name = $variation_field.'['.$variation_id . ']';

		if ($language){
			$id .= '_'.$language;
			$name .= '['.$language.']';
		}

		return '<input id="'.$id.'" name="'.$name .'" type="text" value="'.$value.'" size="'.$text_size.'" maxlength="'.$max_length.'" /><input name="old_'.$name.'" type="hidden" value="'.$value.'" />';
	}

	function get_select($variation_id, $value, $field, $values){

		$variation_field = 'variation_' . $field;

		$id = $variation_field.'_'.$variation_id;
		$name = $variation_field.'['.$variation_id . ']';

		$html = '';

		foreach ( $values as $v ) {
			$selected = $value == $v? 'selected': '';
			$caption = $this->caption["c_$v"];
			$html .= "<option $selected value='$v'>$caption</option>";
		}


		return "<select id=\"$id\" name=\"$name\">$html</select><input name=\"old_{$name}\" type=\"hidden\" value=\"{$value}\" />";
	}

	function show_specs(){

		include( DOCUMENT_ROOT . 'admin/modules/product/product_common_spec.php' );

		$prices = false; // aprofito els get_product_title que ja fa una consulta del producte per agafar el preu
		$ref = ''; // passo per referencia a get_product_title ( calia a variacions, aquí no )
		$this->set_var('product_form_title', $this->get_product_title($ref,'',$prices));

	    $this->set_var ('form_tabs' , $this->form_tabs);
		$id = $_GET['product_id'];
		$this->file = 'product/product_specs.tpl';

		$this->set_var('menu_id', $GLOBALS['gl_menu_id']);
		$this->set_vars($this->caption);

		$loop = ProductCommonSpec::show_specs($id);

		$this->set_vars(array(
			// 'assign_selected_ids'=>json_encode($assign_selected_ids),
			'id'=>$id,
			'loop'=>$loop,
			'image_link'=>'/admin/?action=list_records_images&menu_id=20004&product_id='.$id,
			'data_link'=>'/admin/?action=show_form_edit&menu_id=20004&product_id='.$id,
			'form_link'=>'?tool=product&tool_section=product&product_id='.$id,
			'id_field'=>$this->id_field
			));
		$GLOBALS['gl_content'] =  $this->get_search_content() . $this->process();
	}

	function save_rows_specs() {
		include( DOCUMENT_ROOT . 'admin/modules/product/product_common_spec.php' );
		ProductCommonSpec::save_rows_specs();
	}

	////////////////////////////////////////////////////////////

    function save_rows()
    {
        $save_rows = new SaveRows($this);
		$save_rows->field_unique = "ref";
        $save_rows->save();
    }

    function write_record()
    {
		$this->set_other_config();

        //$product_id = current($_POST['product_id']);
        $writerec = new SaveRows($this);
		$writerec->set_field( 'modified', 'override_save_value', 'now()' );
		$writerec->field_unique = "ref";
	    $writerec->field_unique = "product_file_name";

	    /*
		if (current($_POST['subfamily_id'])!=0) {	//si pertany a una subfamilia
		$familia = current ($_POST['subfamily_id']) ;
		$_POST['subfmaily_id'][$product_id] = 0;
	    $_POST['family_id'][current($_POST['product_id'])]= $familia;
		$_POST['subfamily_id'][current($_POST['product_id'])]= 0;
		}	*/

		// si any = 0 poso valor '' o '0000', si no em guarda com a any 2000
	    if ( isset( $writerec->post['year'] ) ) {
		    $key = key( $writerec->post['year'] );
		    $val = current( $writerec->post['year'] );
		    if ( $val == '0' ) {
			    $writerec->post['year'][ $key ] = '0000';
		    }
	    }
		// si es referencia automatica, l'obtinc automaticament
		if (($this->config['auto_ref']) && ($this->action == 'add_record'))
		{
			$writerec->fields["ref"]['override_save_value'] = $this->get_ref_number();
		}

        $writerec->save();

		// poso totes les variacions al producte si les tinc "per defecte"
		if ($this->action == 'add_record' && $GLOBALS['gl_insert_id']){
			$query = "
			INSERT INTO product__product_to_variation (product_id ,
							variation_id ,
							variation_pvp ,
							variation_pvd ,
							variation_pvd2 ,
							variation_pvd3 ,
							variation_pvd4 ,
							variation_pvd5 ,
							variation_default)
				SELECT      ".$GLOBALS['gl_insert_id'].",
							variation_id ,
							variation_pvp ,
							variation_pvd ,
							variation_pvd2 ,
							variation_pvd3 ,
							variation_pvd4 ,
							variation_pvd5 ,
							variation_default
						FROM product__variation
						WHERE bin =0
						AND variation_checked = '1'
						ORDER BY variation_category_id, ordre
						";

			Db::Execute($query);
		}

		$this->set_stock_status($writerec->id);
    }

    // COMPLEMENTS
	public function show_relateds() {

		$prices = false; // aprofito els get_product_title que ja fa una consulta del producte per agafar el preu
		$ref = ''; // passo per referencia a get_product_title ( calia a variacions, aquí no )
		$this->set_var('product_form_title', $this->get_product_title($ref,'',$prices));

		// Desconecto tots els camps menys els que em fan falta en aquest formulari
		/*
		foreach ( $this->fields as &$field ) {

			if (
				$field != 'product_id' &&
				$field != 'product_title' &&
				$field != 'product_description' &&
				$field != 'ref'
			) {
				// $field['form_admin'] = 'no';
			}
		}*/

		$this->set_field('product_title', 'form_admin', 'text');
		$this->set_field('product_subtitle', 'form_admin', 'text');
		$this->set_field('ref', 'form_admin', 'text');
		$this->set_field('status', 'form_admin', 'text');

		$this->set_field('product_relateds', 'form_admin', 'input');
		$this->action = 'show_form_edit';

		$show            = new ShowForm( $this );
		$show->show_langs = false;
		$show->template = 'product/product_related.tpl';
		$show->form_tabs = $this->form_tabs;

        $GLOBALS['gl_content'] = $show->show_form();

	}

	// Llistat al carregar el formulari - utilitza la taula product__product_related
	public function get_sublist_html($id, $field, $is_input){

		$listing = NewRow::get_sublist_html($this, $id, $field, $is_input );
		$listing->order_by = 'ordre ASC';

		$listing->list_records(false);

		foreach ( $listing->results as &$rs ) {
			$image = UploadFiles::get_record_images($rs['product2_id'], false, 'product', 'product');
			$rs['image'] = '<div><img src="' . $image['image_src_admin_thumb'] . '"></div>';
		}


		return $listing-> parse_template();

		// return $listing->list_records(); // si no necessitem fer el foreach
	}

	// Llistat al seleccionar un item per ajax - utilitza la taula product__product_related
	public function get_sublist_item(){

		$listing = new NewRow($this);

		$product2_id = $listing->selected_id;

		$image = UploadFiles::get_record_images($product2_id, false, 'product', 'product');
		$image = '<div><img src="' . $image['image_src_admin_thumb'] . '"></div>';

		// altres camps relacionats
		$rs = array (

			'image' => $image,

			'family_id' => Db::get_first("SELECT product__family_language.family FROM product__family_language, product__product WHERE product__family_language.family_id = product__product.family_id AND language =  '" . LANGUAGE . "' AND product__product.product_id = $product2_id"),

			'product_title' => Db::get_first("SELECT product__product_language.product_title FROM product__product_language WHERE language =  '" . LANGUAGE . "' AND product__product_language.product_id = $product2_id"),

			'product_subtitle' => Db::get_first("SELECT product__product_language.product_subtitle FROM product__product_language WHERE language =  '" . LANGUAGE . "' AND product__product_language.product_id = $product2_id"),

			'ref' => Db::get_first("SELECT product__product.ref FROM product__product WHERE product__product.product_id = $product2_id"),

			'status' => Db::get_first("SELECT product__product.status FROM product__product WHERE product__product.product_id = $product2_id"),
		);

		$listing->add_row ($rs);
		$listing->new_row();
	}

	// Resultats per el autocomplete
	public function get_checkboxes_long_values() {

		$order = 'status asc, product_title ASC,ordre ASC';

		$limit = 100;

		$q            = R::escape( 'query' );
		$q            = strtolower( $q );
		$id           = R::id( 'id' );
		$selected_ids = R::get( 'selected_ids' );

		if ( $selected_ids ) {
			R::escape_array( $selected_ids );
			$selected_ids         = implode( ',', $selected_ids );
		}

		$ret                = array();
		$ret['suggestions'] = array();

		$r = &$ret['suggestions'];


		$order    = 'status asc, product_title ASC,ordre ASC';
		$language             = LANGUAGE;

		$exclude_selected_ids = $selected_ids ? " AND ( product__product.product_id NOT IN ($selected_ids))" : '';
		$exclude_saved_ids = " AND ( product__product.product_id NOT IN (SELECT product2_id AS product_id FROM product__product_related WHERE product_id = $id))";

		$family_query = "(
			SELECT product__family_language.family FROM product__family_language 
			WHERE product__family_language.family_id = product__product.family_id 
			AND language =  '$language') AS family";

		$subfamily_query = "(
			SELECT product__family_language.family FROM product__family_language 
			WHERE product__family_language.family_id = product__product.subfamily_id 
			AND language =  '$language') AS subfamily";

		$family_subquery = '
			SELECT product__family_language.family_id FROM product__family_language 
			WHERE family';

		$variations_subquery_tpl = "
			SELECT product__product_to_variation.product_id AS product_id FROM product__product_to_variation WHERE variation_id IN (
				SELECT product__variation_language.variation_id FROM product__variation_language 
				WHERE variation LIKE '%s' %s	
			)";


		$common_query_1 = "
					SELECT product__product.product_id as product_id, $family_query, $subfamily_query, product_title, product_subtitle, ref, family_id, subfamily_id, subsubfamily_id
					FROM product__product, product__product_language";
		$common_query_2 = "product__product.product_id = product__product_language.product_id
				            AND 
				            language = '$language'
				            AND 
				            bin = 0";
		$common_query_3 =
					"
		            $exclude_selected_ids
		            $exclude_saved_ids
					ORDER BY $order";

		// 1 - Que comenci
		$variations_subquery = sprintf($variations_subquery_tpl, "$q%", '');
		$query = "
					$common_query_1
					WHERE
				        (
				            $common_query_2
			            ) 
				        AND
				        (
				            product_title LIKE '$q%'
							OR product_subtitle LIKE '$q%'
							OR ref LIKE '$q%'
							OR family_id IN ($family_subquery LIKE '$q%')
							OR subfamily_id IN ($family_subquery LIKE '$q%')
							OR product__product.product_id IN ($variations_subquery)
						)
					$common_query_3
					LIMIT $limit";

		$results = Db::get_rows( $query );

		$rest = $limit - count( $results );

		// 2 - Qualsevol posició
		if ( $rest > 0 ) {

			$variations_subquery = sprintf($variations_subquery_tpl, "%$q%", "AND variation NOT LIKE '$q%'");
			$query = "
						$common_query_1
						WHERE
					        (
					            $common_query_2
				            )
					        AND	
					        (					 
								( 
									product_title LIKE '%$q%'
				                    AND product_title NOT LIKE '$q%' 
				                )
								OR 
								( 
									product_subtitle LIKE '%$q%'
				                    AND product_subtitle NOT LIKE '$q%' 
				                )
								OR 
								( 
									family_id IN (
									$family_subquery LIKE '%$q%'
									AND family NOT LIKE '$q%' 
									)
				                )
								OR 
								( 
									subfamily_id IN (
									$family_subquery LIKE '%$q%'
									AND family NOT LIKE '$q%' 
									)
				                )
								OR 
								( 
									product__product.product_id IN (
									$variations_subquery
									)
				                )
								OR
								 ( 
								    ref LIKE '%$q%'
				                    AND ref NOT LIKE '$q%' 
				                )
				            )
						$common_query_3
						LIMIT $rest";

			$results2 = Db::get_rows( $query );

			$results = array_merge( $results, $results2 );

		}

		$rest = $limit - count( $results );

		$words = array();
		// 3 - Parteixo paraules
		if ( $rest > 0 ) {

			$words = explode( ' ', $q );
			if ( count( $words ) > 1 ) {

				$words_query_title    = '';
				$words_query_subtitle = '';

				foreach ( $words as $word ) {
					$words_query_title    .= "AND product_title LIKE '%$word%'";
					$words_query_subtitle .= "AND product_subtitle LIKE '%$word%'";
				}

				$query = "
						$common_query_1
						WHERE
					        (
					            $common_query_2
				            )
					        AND	
                            (
								( 
									product_title NOT LIKE '%$q%'
				                    AND product_title NOT LIKE '$q%' 
				                    $words_query_title
				                )
								OR 
								( 
									product_subtitle NOT LIKE '%$q%'
				                    AND product_subtitle NOT LIKE '$q%' 
				                    $words_query_subtitle
				                )
			                )
						$common_query_3
						LIMIT $rest";

				$results2 = Db::get_rows( $query );

				$results = array_merge( $results, $results2 );
			}

		}

		// Presento resultats
		foreach ( $results as $rs ) {


			$image = UploadFiles::get_record_images( $rs['product_id'], false, 'product', 'product' );
			$image = '<img src="' . $image['image_src_admin_thumb'] . '">';

			$ref              = str_replace( $q, "<strong>$q</strong>", $rs['ref'] );
			$product_title_text = $rs['product_title'];
			$product_title    = str_replace( $q, "<strong>$q</strong>", $rs['product_title'] );
			$product_subtitle = str_replace( $q, "<strong>$q</strong>", $rs['product_subtitle'] );
			$family             = str_replace( $q, "<strong>$q</strong>", $rs['family'] );
			$subfamily          = str_replace( $q, "<strong>$q</strong>", $rs['subfamily'] );
			$product_id         = $rs['product_id'];

			if ( count( $words ) > 1 ) {
				foreach ( $words as $word ) {
					$ref              = str_replace( $word, "<strong>$word</strong>", $ref );
					$product_title    = str_replace( $word, "<strong>$word</strong>", $product_title );
					$product_subtitle = str_replace( $word, "<strong>$word</strong>", $product_subtitle );
					$family           = str_replace( $word, "<strong>$word</strong>", $rs['family'] );
					$subfamily        = str_replace( $word, "<strong>$word</strong>", $rs['subfamily'] );
				}
			}

			$familys = '';
			if ( $family ) {
				$familys = $family;
			}
			if ( $subfamily ) {
				$familys .= " > $subfamily";
			}
			if ( $familys ) {
				$familys = "<h5>$familys</h5>";
			}


			$r[] = array(
				'value'  => $product_title_text,
				'custom' => "<div class='custom'>$image <div><i>$ref</i><h6>$product_title</h6>$familys<p>$product_subtitle</p></div></div>",
				'data'   => $rs['product_id']
			);
		}


		$rest               = $limit - count( $results );
		$ret['is_complete'] = false; // Aquest mai es complert, ja que el jscript no pot buscar paraules separades, i llavors perdo la funcionalitat // $rest>0;
		$ret['has_custom']  = true;

		json_end( $ret );

	}






	// ----------------------------------------------------------------------------
	// control stock
	// ----------------------------------------------------------------------------
	// poso per vendre si hi ha stock, o poso no-stock si no n'hi ha
	// nomès canvia d'status si es igual a onsale o nostock 
	// només es fa quan hi ha control de stock i no es permet stock negatiu
	// si permeto negatiu i estat ha de ser per vendre també el poso ( per si a cas estava a nostock ),
	// 		amb permetre stock negatiu no ha d'haver mai cap a 'nostock'
	function set_stock_status($product_id){
		// si no hi ha control return
		debug::p($this->config);
		if (!$this->config['stock_control']) return;
		// nomès canvia d'status si es igual a onsale o nostock 
		$query = "
				SELECT status
				FROM product__product
				WHERE product_id = '" . $product_id . "'
				AND (status = 'onsale' OR status = 'nostock')";
		$status = Db::get_first($query);
		// si status es qualsevol altre return
		if (!$status) return;

		$stock = $this->get_variations_total_stock($product_id);debug::p($stock);

		// si no te variacions
		if ($stock==false){
			$query = "
				SELECT stock
				FROM product__product
				WHERE product_id = '" . $product_id . "'";
			$stock = Db::get_first($query);
		}

		// canvio status

		// si permeto negatiu i estat ha de ser per vendre també el poso ( per si a cas estava a nostock )
		if ($this->config['allow_null_stock']){
			Db::execute("UPDATE product__product set status='onsale' WHERE product_id ='" . $product_id . "'");
			print_javascript('
				top.$("#status_'.$product_id.' option[value=\'onsale\']").prop("selected", true);
			');
		}

		$new_status = (intval($stock)>0)?'onsale':'nostock';
		if ($new_status==$status) return;
		// si no permeto negatiu canvio l'estat
		if (!$this->config['allow_null_stock']){
			Db::execute("UPDATE product__product set status='".$new_status."' WHERE product_id ='" . $product_id . "'");
			print_javascript('
				top.$("#status_'.$product_id.' option[value=\''.$new_status.'\']").prop("selected", true);
			');
		}
	}
	function get_variations_total_stock($product_id){
		$query = "
			SELECT SUM(variation_stock) as stock
			FROM product__product_to_variation
			INNER JOIN product__variation USING(variation_id)
			WHERE product_id = '" . $product_id . "'
			AND BIN = 0
			GROUP BY product_id";
		return Db::get_first($query);
	}

	function get_ref_number()
	{

		$query = "SELECT CAST(ref AS signed) as ref_num
					FROM product__product
					ORDER by ref_num desc
					LIMIT 0,1";
		$ref = Db::get_first($query);
		return ((int)$ref) + 1;
	}

    function manage_images()
    {

	    $product_id = R::get( 'product_id' );

        if ($this->action == 'list_records_images'){
		   	$this->set_var('variation_link', $this->link . '&action=show_variations&menu_id=20004&product_id=' . $product_id);
		   	$this->set_var('product_form_title', $this->get_product_title());
		}
        elseif ( $this->action == 'save_rows_save_records_images' && $product_id ) {
	        $variations = $_POST['product_variation_id'];
	        foreach ( $variations as $image_id => $product_variation_id ) {
		        if (!$product_variation_id) $product_variation_id = 0;
		        $image_id = Db::qstr($image_id);
		        $product_variation_id = Db::qstr($product_variation_id);
		        $query = "UPDATE product__product_image SET product_variation_id = $product_variation_id WHERE image_id = $image_id AND product_id = $product_id";
		        Db::execute( $query );
	        }
        }
		$image_manager = new ImageManager($this);
	    $image_manager->form_tabs = $this->form_tabs;

	    $image_manager->call('images_record_walk');

        $image_manager->execute();
		if ($this->action == 'list_records_images'){
			$GLOBALS['gl_content'] =  $this->get_search_content() . $GLOBALS['gl_content'];
		}
    }

    // Desplegable de variacions per les imatges
	function images_record_walk(&$listing, &$image_manager) {
		
		$image_id = $listing->rs['image_id'];
		$selected_product_variation_id = Db::get_first( "SELECT product_variation_id FROM product__product_image WHERE image_id = $image_id" );
		$product_id = $image_manager->id;
		$language = LANGUAGE;

		$select = "<option value=\"\"></option>";

		$query   = "
			SELECT variation_category, variation_category_id 
				FROM product__variation_category_language 
				WHERE variation_category_id 
				IN (
					SELECT variation_category_id 
					FROM product__variation
					WHERE variation_id IN (
						SELECT variation_id
						FROM product__product_to_variation
						WHERE product_id = $product_id
					)
				)
				AND language = '$language'
				ORDER BY variation_category ASC";
		$results_category = Db::get_rows( $query );

		foreach ( $results_category as $rs_category ) {
			$variation_category = htmlspecialchars($rs_category['variation_category']);
			$variation_category_id = $rs_category['variation_category_id'];

			$select .= " <optgroup label=\"$variation_category\">";

			$query   = "
				SELECT (
					SELECT variation 
					FROM product__variation_language 
					WHERE language = '$language' 
					AND product__variation_language.variation_id = product__product_to_variation.variation_id) AS variation, product_variation_id
				FROM product__product_to_variation
				INNER JOIN product__variation USING (variation_id)
				WHERE product_id = $product_id
				AND variation_category_id = $variation_category_id
				";
			$results = Db::get_rows( $query );

			foreach ( $results as $rs ) {
				$variation = $rs['variation'];
				$product_variation_id = $rs['product_variation_id'];
				$selected = $selected_product_variation_id == $product_variation_id?' selected="selected"':'';
				$select .= "<option value=\"$product_variation_id\"$selected>$variation</option>";
			}


		}

		if ($results_category) {
			$select_name = "product_variation_id[$image_id]";
			$select      = "<select name='$select_name'>$select</select>";
		}
		else {
			$select = '';
		}
		
		$ret['product_variation_id'] = $select;

		return $ret;
	}
    // Búsqueda per paraules
    function get_search_content()
    {
        // global $gl_menu_id;//aquesta variable global es manté, pero a partir d'ara millor que no la cridi hi l'agafo sempre que vull amb $GLOBALS['gl_menu_id']. ho fem per distingir lo vell de lo nou.
        //$tpl = new phemplate(PATH_TEMPLATES); //creem un nou objecte independent de la clase
        //$template = 'product/product_search.tpl';
       // $tpl->set_file($template);
        // POSSO EL menu_id i el section per posar-lo al from, aixi al fer el llistat segueixes a aquest menu
        //$tpl->set_var('menu_id', $GLOBALS['gl_menu_id']);
		//$q = $this->q = R::escape('q');
        // set_var = agafa una sola variable // set_loop = fa un loop
        //$tpl->set_vars($this->caption);
       // return $tpl->process();

		$tpl = new phemplate(PATH_TEMPLATES);
		$this->caption['c_by_ref']='';
		$tpl->set_vars($this->caption);
		$tpl->set_file('product/product_search.tpl');
		$tpl->set_var('menu_id',$GLOBALS['gl_menu_id']);
		$tpl->set_var('q',  htmlspecialchars(R::get('words')));


		if ($this->q) {
			$GLOBALS['gl_page']->javascript .= "
				$('table.listRecord').highlight('".addslashes(R::get('words'))."');			
			";
		}

		return $tpl->process();



    }
    function search_list_condition_words ()
    {
		$q = $this->q = R::escape('words');
        $query = "SELECT product_id FROM product__product where
				   ref like '%" . $q . "%' OR
				   ref_number like '%" . $q . "%' OR
				   ref_supplier like '%" . $q . "%' OR
				   ean like '%" . $q . "%' OR
				   product_private like '%" . $q . "%' OR
				   pvp like '%" . $q . "%' OR
				   pvd like '%" . $q . "%' OR
				   observations like '%" . $q . "%'
				   AND bin = 0
				   " ;
        $results1 = Db::get_rows ($query);
        $_1 = count ($results1);
        $query2 = "SELECT product_id FROM product__product_language where
				   product_title like '%" . $q . "%' OR
				   product_subtitle like '%" . $q . "%' OR
				   others1 like '%" . $q . "%' OR
				   others2 like '%" . $q . "%' OR
				   others3 like '%" . $q . "%' OR
				   others4 like '%" . $q . "%' OR
				   others5 like '%" . $q . "%' OR
				   others6 like '%" . $q . "%' OR
				   others7 like '%" . $q . "%' OR
				   others8 like '%" . $q . "%' OR
				   product_description like '%" . $q . "%'
				   " ;
        $results2 = Db::get_rows ($query2);
        $_2 = count ($results2);
        if ($_1 + $_2 > 0) { // faig trampa per si no hi ha cap resultat
            $results = array_merge($results1, $results2);
            foreach ($results as $key) {
                $ids_arr[] = $key['product_id'];
            }

            $ids = implode(',', $ids_arr);
        } else {
            $ids = "NULL";
        }
        $query = "product_id IN (" . $ids . ")";
        $search_condition = $query;
        $GLOBALS['gl_page']->title = TITLE_SEARCH;
        return $search_condition;
    }
    // Busqueda per nif
    function search_search_condition()
    {
        if ($_GET['nif'] == "") {
            $nif = "NULL";
        } else {
            $nif = R::escape('nif');
        }
        $query = "ref = '" . $nif . "'";
        $search_condition = $query;
        $GLOBALS['gl_page']->title = TITLE_SEARCH;
        return $search_condition;
    }
    // Per poder escollir productes desde una finestra amb frame, de moment desde newsletter
    function select_frames()
    {
        $page = &$GLOBALS['gl_page'];

        $page->menu_tool = false;
        $page->menu_all_tools = false;
        $page->template = '';

        $this->set_file('product/product_select_frames.tpl');
        $this->set_vars($this->caption);
        $this->set_var('menu_id', $GLOBALS['gl_menu_id']);
        // $this->set_var('custumer_id',$_GET['custumer_id']);
        $GLOBALS['gl_content'] = $this->process();
    }
	// per posar el onclick al llistat
	function list_records_walk_select ($product_id, $product_title)
	{
		$product_title= addslashes($product_title); // cambio ' per \'
		$product_title= htmlspecialchars($product_title); // cambio nomès cometes dobles per  &quot;
		$ret['tr_jscript'] = 'onClick="parent.insert_address(' . $product_id . ',\'' . $product_title . '\')"';
		return $ret;
	}
	function get_product_title(&$ref='', $product_title='', &$prices=''){
		if (!isset($_GET['product_id'])) return '';
		$query = $prices?', pvp, pvd, pvd2, pvd3, pvd4, pvd5':'';
		$product_id = $_GET['product_id'];
		$query = "
			SELECT product_title,ref" . $query . "
			FROM product__product LEFT OUTER JOIN product__product_language using (product_id) 
			WHERE product__product.product_id = ".$product_id." 
			AND product__product_language.language = '".LANGUAGE."'";
		$rs = Db::get_row($query);
		extract($rs);
		$r = 'Ref. ' . $ref . ' - ' . $product_title;
		if ($product_title) $r .= ' - ' . $product_title;
		if ($prices) {
			$prices = $rs;
			$prices['pvp'] = format_decimal($prices['pvp']);
			$prices['pvd'] = format_decimal($prices['pvd']);
			$prices['pvd2'] = format_decimal($prices['pvd2']);
			$prices['pvd3'] = format_decimal($prices['pvd3']);
			$prices['pvd4'] = format_decimal($prices['pvd4']);
			$prices['pvd5'] = format_decimal($prices['pvd5']);
		}
		return $r;
	}

	// defineixo els tipus de other que hi han

	function set_other_config() {

		$keys = [ 'others' => 20, 'others_private' => 3 ];

		// defineixo els tipus de other que hi han

		foreach ($keys as $k => $v) {
			for ( $i = 1; $i <= $v; $i ++ ) {
				if ( $this->config[ $k . $i ] ) {

					$type = $this->config[ $k . $i . '_type' ];

					$this->set_field( $k . $i, 'form_admin', 'input' );
					$this->set_field( $k . $i, 'form_public', 'text' );
					$this->set_field( $k . $i, 'change_field_name', 'product__product.' . $k . $i . ' AS ' . $k . $i );
					$this->set_field( $k . $i, 'type', $type );

					$this->caption[ 'c_' . $k . $i ] = $this->config[ $k . $i ];

					if ( $type == 'text' ) {
						$this->language_fields[] = $k . $i;
						$this->set_field( $k . $i, 'change_field_name', 'product__product_language.' . $k . $i . ' AS ' . $k . $i );
						$this->set_field( $k . $i, 'text_maxlength', '2000' );
					}
					elseif ( $type == 'text_no_lang' ) {
						$this->set_field( $k . $i, 'type', 'text' );
						$this->set_field( $k . $i, 'text_maxlength', '2000' );
					}
					elseif ($type == 'int') {
						$this->set_field($k.$i, 'type', 'int');
						$this->set_field($k.$i, 'text_maxlength', '100');
					}
					elseif ( $type == 'textarea' ) {
						$this->language_fields[] = $k . $i;
						$this->set_field( $k . $i, 'change_field_name', 'product__product_language.' . $k . $i . ' AS ' . $k . $i );
						$this->set_field( $k . $i, 'textarea_rows', '4' );
						$this->set_field( $k . $i, 'textarea_cols', '50' );
						$this->set_field( $k . $i, 'text_maxlength', '2000' );
					}
					elseif ( $type == 'checkbox' ) {
						$this->set_field( $k . $i, 'select_fields', '0,1' );
					}
					elseif ( $type == 'select' ) {
						$this->set_field( $k . $i, 'type', 'select' );
					}
				}
			}
		}
	}
	function get_others_array(&$rs) {
		$others = array();
		$other_conta = 0;
		for ($i = 1; $i <= 20; $i++) {
			if ($this->config['others'.$i]) {
				$others[]= array(
					'other_caption' =>$this->caption['c_others'.$i],
					'other_name' =>'others'.$i,
					'other_conta' =>$other_conta++,
					'other' =>$rs['others'.$i]
				);
			}
		}
		$this->set_var('others', $others);


		$others_private = array();
		$other_private_conta = 0;
		for ($i = 1; $i <= 3; $i++) {
			if ($this->config['others_private'.$i]) {
				$others_private[]= array(
					'other_private_caption' =>$this->caption['c_others_private'.$i],
					'other_private_name' =>'others_private'.$i,
					'other_private_conta' =>$other_private_conta++,
					'other_private' =>$rs['others_private'.$i]
				);
			}
		}
		$this->set_var('others_private', $others_private);
	}

	// defineixo els tipus de other que hi han a variacions

	function set_variation_other_config(&$listing) {

		// defineixo els tipus de variation other que hi han
		for ($i = 1; $i <= 4; $i++) {
			if ($listing->config['variation_others'.$i]) {

				$type = $listing->config['variation_others'.$i.'_type'];

				$listing->set_field('variation_others'.$i, 'form_admin', 'input');
				$listing->set_field('variation_others'.$i, 'form_public', 'text');
				$listing->set_field('variation_others'.$i, 'type', $type);

				$listing->caption['c_variation_others'.$i] = $listing->config['variation_others'.$i];

				if ($type == 'text') {
					//$listing->language_fields[] = 'variation_others'.$i;
					$listing->set_field('variation_others'.$i, 'text_maxlength', '255');
				}
				elseif ($type == 'text_no_lang') {
					$listing->set_field('variation_others'.$i, 'type', 'text');
				}
				elseif ($type == 'textarea') {
					$listing->language_fields[] = 'variation_others'.$i;
					$listing->set_field('variation_others'.$i, 'textarea_rows', '4');
					$listing->set_field('variation_others'.$i, 'textarea_cols', '50');
				}
				elseif ($type == 'checkbox') {
					$listing->set_field('variation_others'.$i, 'select_fields', '0,1');
				}
			}
		}
	}
	static function get_qr_image($product_id){

		$temp_path = CLIENT_PATH . 'product/product/qrs/';
		$temp_dir = CLIENT_DIR . '/product/product/qrs/';
		$file = $product_id . '.png';

		if (!is_file($temp_path . $file)){
			$link = HOST_URL . 'admin/qrp/'.$product_id;
			QRcode::png($link,$temp_path . $file,QR_ECLEVEL_L,4,0);
		}

		return  '<img id="qrcode" src="/'.$temp_dir . $file . '" border="0"/>';
	}


	function export() {

    	$this->is_excel = true;

		ini_set('max_execution_time',120);
		ini_set('memory_limit', '1024M');

		Main::load_class( 'excel' );

		unset($this->caption['c_subfamily_id_format']);
		unset($this->caption['c_subsubfamily_id_format']);
		$this->caption['c_product_file_name'] = $this->caption['c_file_name_excel'];

		// TODO-i set_visible_fields no em serveix per que ho necessito amb el list_records, integrar-ho a la classe
		foreach ( $this->fields as &$field ) {
			if ($field['form_admin'] == 'input' || $field['form_admin'] == 'text' || $field['form_admin'] == 'out'){
				$field['list_admin'] = 'text';
			}
		}

		// $this->fields['subfamily_id'] = $this->fields['subsubfamily_id'] = $this->fields['family_id'];

		$listing = $this->get_records();
		$listing->limit = 5000;

		$listing->list_records( false );

		$excel = new Excel( $this );
		$excel
			->set_visible_fields()
			->file_name( 'product' )
			->order_by( $this->order_by )
			->make( $listing->results );
	}
}