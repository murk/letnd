<?php
if (!defined('NEWGRASS_MENU_SELLPOINT')){

	// menus eina		
	define('NEWGRASS_MENU_SELLPOINT','Puntos de venta');
	define('NEWGRASS_MENU_SELLPOINT_NEW','Nuevo punto');
	define('NEWGRASS_MENU_SELLPOINT_LIST','Listar puntos');
	define('NEWGRASS_MENU_SELLPOINT_BIN','Papelera de reciclage');
}

/*--------------- nomès castellà i català, no traduit a la resta -------------------*/


$gl_caption_sellpoint['c_sellpoint'] = 'Título';
$gl_caption_sellpoint['c_sellpoint_description'] = 'Descripción';
$gl_caption_sellpoint['c_empresa'] = 'Empresa';
$gl_caption_sellpoint['c_adress'] = 'Dirección';
$gl_caption_sellpoint['c_provincia_id'] = 'Provincia';
$gl_caption_sellpoint['c_poblacio'] = 'Població';
$gl_caption_sellpoint['c_cp'] = 'Codigo postal';
$gl_caption_sellpoint['c_telefon'] = 'Teléfono';
$gl_caption_sellpoint['c_telefon2'] = 'Teléfono 2';
$gl_caption_sellpoint['c_fax'] = 'Fax';
$gl_caption_sellpoint['c_correu'] = 'E-mail';
$gl_caption_sellpoint['c_correu2'] = 'E-mail 2';
$gl_caption_sellpoint['c_url1'] = 'Web';
$gl_caption_sellpoint['c_url1_name'] = 'Nombre de la web';



$gl_caption['c_filter_comarca_id'] = 'Totes les comarques';
$gl_caption['c_filter_municipi_id'] = 'Tots els municipis';


// mapes
$gl_caption['c_edit_map'] = 'Ubicación';
$gl_caption['c_search_adress']='Buscar dirección';
$gl_caption['c_latitude']='latitud';
$gl_caption['c_longitude']='longitud';
$gl_caption['c_found_direction']='dirección encontrada';
$gl_caption['c_save_map'] = 'Guardar ubicación';
$gl_caption['c_click_map'] = 'Haz un click en un punto del mapa para definir la ubicación';
$gl_caption['c_form_map_title'] = 'Ubicación';
$gl_messages['point_saved'] = 'Nueva ubicación guardada correctamente';

$gl_caption['c_prepare'] = 'En preparación';
$gl_caption['c_review'] = 'Para revisar';
$gl_caption['c_public'] = 'Público';
$gl_caption['c_archived'] = 'Archivado';
$gl_caption['c_status'] = 'Estado';

$gl_caption['c_sellpoint_file_name'] = $GLOBALS['gl_caption']['c_file_name'];
$gl_caption['c_sellpoint_old_file_name'] = $GLOBALS['gl_caption']['c_old_file_name'];

$gl_caption['c_filter_provincia_id'] = 'Todas las provincias';


?>