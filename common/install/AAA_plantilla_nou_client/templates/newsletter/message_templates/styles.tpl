
<?
	/*	Simplement canviar aquests colors pels corporatius	*/

	$width = "650px";
	$text_color = "#4e4e4e";

	$header_background = "#000000";
	$page_background = "#ffffff";
	$content_background = "#ffffff";
	$footer_background = "#000000";

	$header_color = "#f7f7f7";
	$footer_color = "#f7f7f7";

	$line_color = "#e2e2e2";
	$link_color = "#51b94e";
	$highlight_color = "#51b94e"; // Color destacats

	$highlight_background = "#51b94e"; // Color de fons per franges de color destacades, tipus gris claret en un títol, o capcelera d'una taula en la cistella compra
	$highlight_background_color = "#ffffff"; // Color del texte que va dins una capa amb el $highlight_background

?>

<style type="text/css">
	body {
		padding: 0;
		margin: 0;
		background-color: <?= $page_background ?>;
	}

	table {
		border: 0 none;
		border-collapse: collapse;
		border-spacing: 0;
	}

	a {
		color: <?= $link_color ?>;
	}

	img {
		border: 0;
	}

	.letnd-sep {
		height: 20px;
		line-height: 16px;
		font-size: 12px;
	}

	h1 {
		font-size: 22px;
		margin: 0;
		padding-top: 15px;
		padding-bottom: 15px;
		font-family: Arial, Lucida, Tahoma, sans-serif;
	}

	h2 {
		font-size: 20px;
		margin: 0;
		padding-top: 15px;
		padding-bottom: 15px;
		font-family: Arial, Lucida, Tahoma, sans-serif;
	}

	h3 {
		font-size: 18px;
		margin: 0;
		padding-top: 15px;
		padding-bottom: 15px;
		font-family: Arial, Lucida, Tahoma, sans-serif;
	}

	h4 {
		font-size: 16px;
		margin: 0;
		padding-top: 15px;
		padding-bottom: 15px;
		font-family: Arial, Lucida, Tahoma, sans-serif;
	}

	h5 {
		font-size: 15px;
		margin: 0;
		padding-top: 15px;
		padding-bottom: 15px;
		font-family: Arial, Lucida, Tahoma, sans-serif;
	}

	p {
		margin: 0;
		padding-top: 15px;
		padding-bottom: 15px;
		font-size: 15px;
	}

	/* HEADER */
	#letnd-header-holder {
		width: 100%;
		background-color: <?= $header_background ?>;
	}
	.letnd-header-holder {
		padding-left: 20px;
		padding-right: 20px;
	}

	#letnd-header {
		width: <?= $width ?>;
	}

	.letnd-header {
		font-size: 20px;
		padding-top: 12px;
		padding-bottom: 12px;
	}

	.letnd-header a {
		color: <?= $text_color ?>;
		font-size: 20px;
		text-decoration: none;
	}

	.letnd-header-right {
		color: <?= $text_color ?>;
		padding-top: 9px;
		padding-bottom: 20px;
		width: 350px;
	}

	#letnd-slogan {
		width: <?= $width ?>;
	}

	.letnd-slogan {
		color: <?= $header_color ?>;
		height: 40px;
		vertical-align: top;
	}

	.letnd-tel {
		padding-top: 7px;
		padding-bottom: 7px;
	}

	.letnd-tel a {
		color: <?= $header_color ?>;
		font-weight: bold;
		font-size: 16px;
		text-decoration: none;
	}

	/* CONTENT */
	#letnd-table-content {
		padding: 0;
		margin: 0;
		font-family: Arial, Lucida, Tahoma, sans-serif;
		font-size: 15px;
		color: <?= $text_color ?>;
		background-color: <?= $content_background ?>;
		width: 100%;
	}

	#letnd-td-content {
		padding-top: 0;
		padding-bottom: 0;
	}

	#letnd-content {
		width: <?= $width ?>;
		border-bottom: 1px solid <?= $line_color ?>;
		margin-right: 20px;
		margin-left: 20px;
	}

	.letnd-content {
		padding-top: 40px;
		padding-bottom: 40px;
		background-color: <?= $content_background ?>;
	}

	.letnd-content-left {
		width: 330px;
	}

	.letnd-picture {
		padding-top: 40px;
		padding-bottom: 40px;
	}

	/* message2 */
	.letnd-content-m2 {
		padding-bottom: 20px;
	}

	.letnd-picture-m2 {
		padding-top: 20px;
		padding-left: 0;
		padding-right: 0;
	}

	/* fi message2 */

	/* message3 */
	.letnd-pictureleft {
		padding-top: 40px;
	}

	.letnd-pictureright {
		padding-top: 40px;
		padding-left: 40px;
	}

	/* fi message3 */

	/* CONTENT EXTRA */
	#letnd-content_extra {
		width: <?= $width ?>;
	}

	#letnd-content_extra td {
	}

	/* FOOTER */
	#letnd-footer-holder {
		width: 100%;
		background-color: <?= $footer_background ?>;
	}
	.letnd-footer-holder {
		padding-right: 20px;
		padding-left: 20px;
	}

	#letnd-footer {
		width: <?= $width ?>;
	}

	#letnd-footer td {
		padding-left: 10px;
		padding-right: 10px;
	}

	#letnd-footer p {
		font-size: 12px;
		line-height: 18px;
		color: <?= $footer_color ?>;
	}

	#letnd-footer p.address {
	}

	#letnd-footer a {
		font-size: 12px;
		color: <?= $footer_color ?>;
		text-decoration: none;
	}

	#letnd-footer a.url {
	}

	.letnd-picture-vertical-holder {
		padding-top: 40px;
		padding-bottom: 25px;
	}

	.letnd-picture-vertical {
		padding-bottom: 25px;
	}

	.letnd-footer {
		text-align: left;
		font-size: 12px;
		padding-top: 10px;
		padding-bottom: 10px;
	}

	#letnd-footer p.remove {
		font-size: 11px;
	}

	#letnd-footer .remove a {
		color: <?= $text_color ?>;
		font-size: 11px;
	}

	/* LIST */

	.letnd-list {
		width: 100%;
	}

	.letnd-list-details {
		padding-top: 40px;
		padding-bottom: 40px;
		padding-left: 10px;
		padding-right: 0;
		width: 380px;
		text-align: left;
		border-bottom: 1px solid <?= $line_color ?>;
	}

	.letnd-list-image {
		padding-top: 40px;
		padding-bottom: 40px;
		width: 260px;
		padding-left: 0;
		text-align: left;
		border-bottom: 1px solid <?= $line_color ?>;
	}

	.letnd-list h2 {
		font-size: 18px;
		line-height: 24px;
	}

	.letnd-list h2 a {
		color: <?= $text_color ?>;
		text-decoration: none;
	}

	.letnd-list h3 {
		font-size: 15px;
		line-height: 22px;
	}

	.letnd-list p {
		font-size: 15px;
		line-height: 18px;
		margin: 0;
		padding-top: 15px;
	}

	.letnd-list .letnd-info {
		margin: 0;
		padding-top: 15px;
	}

	.letnd-list .letnd-info a {
		text-decoration: underline;
		color: <?= $link_color ?>;
		font-size: 14px;
		text-transform: capitalize;
	}

	/* FI LIST */

	/* FORM */

	.letnd-form {
		width: 100%;
	}

	.letnd-form-details {
		padding-top: 40px;
		padding-bottom: 40px;
		padding-left: 10px;
		padding-right: 0;
		width: 330px;
		text-align: left;
	}

	.letnd-form-image {
		padding-top: 40px;
		padding-bottom: 40px;
		width: 310px;
		padding-left: 0;
		text-align: left;
	}

	.letnd-form h2 {
		font-size: 18px;
		line-height: 24px;
	}

	.letnd-form h2 a {
		color: <?= $text_color ?>;
		text-decoration: none;
	}

	.letnd-form h3 {
		font-size: 15px;
		line-height: 22px;
	}

	.letnd-form p {
		font-size: 15px;
		line-height: 18px;
		margin: 0;
		padding-top: 15px;
	}

	.letnd-form .letnd-info {
		margin: 0;
		padding-top: 15px;
	}

	.letnd-form .letnd-info a {
		text-decoration: underline;
		color: <?= $link_color ?>;
		font-size: 14px;
		text-transform: capitalize;
	}

	.letnd-form-description {
		width: 100%;
	}

	.letnd-form-description-td {
		padding-bottom: 40px;
		padding-left: 0;
		padding-right: 0;
		border-bottom: 1px solid <?= $line_color ?>;
	}

	/* FI FORM */

	/* NEWS FORM */
	.news-form-entered {
		font-size: 12px;
		color: <?= $highlight_color ?>;
	}

	.news-form-details {
		text-align: left;
		margin: 0;
		padding-top: 15px;
		font-size: 14px;
	}

	.news-form-details a {
		color: <?= $link_color ?>;
		text-decoration: none;
		display: block;
	}

	.news-form .urls strong {
		color: <?= $highlight_color ?>;
	}

	/* FI NEWS FORM */

	/* PRODUCT FORM */
	.product-form-ref {
		font-style: normal;
		font-weight: bold;
		font-size: 16px;
		padding-top: 10px;
	}

	.product-form-details {
		text-align: left;
		margin: 0;
		padding-top: 15px;
		font-size: 14px;
	}

	.product-form-details th {
		text-align: left;
		padding: 0;
		padding-right: 5px;
		margin: 0;
		font-size: 14px;
	}

	.product-form-files {
		margin: 0;
		padding-top: 15px;
	}

	.product-form-files a {
		color: <?= $link_color ?>;
		text-decoration: underline;
		display: block;
		font-size: 14px;
	}

	.product-form-files a:hover {
		text-decoration: none;
	}

	/* FI PRODUCT FORM */

	/* INMO FORM */
	.inmo-form-image {
		padding-top: 40px;
		padding-bottom: 40px;
		border-bottom: 1px solid <?= $line_color ?>;
		width: <?= $width ?>;
	}

	.inmo-form-ref {
		font-size: 14px;
		color: <?= $highlight_color ?>;
		font-weight: bold;
	}

	.inmo-form-details-left {
		width: 330px;
		padding-left: 10px;
		padding-right: 10px;
		padding-top: 30px;
		padding-bottom: 15px;
	}

	.inmo-form-details-right {
		width: 278px;
		padding-left: 10px;
		padding-right: 10px;
		padding-top: 30px;
		padding-bottom: 15px;
	}

	.inmo-form-details {
		border-left: 1px solid <?= $line_color ?>;
	}

	.inmo-form-details td {
		font-size: 12px;
		line-height: 16px;
		padding: 5px 5px 5px 20px;
	}

	.inmo-form-price {
		padding-top: 10px;
	}

	.inmo-form-price-td {
		font-size: 18px;
		background-color: <?= $highlight_background ?>;
		color: <?= $highlight_background_color ?>;
		padding: 7px 30px;
	}

	/* FI INMO FORM */

	/* INMO LIST */
	.inmo-list-ref {
		font-size: 14px;
		color: <?= $highlight_color ?>;
		font-weight: bold;
	}

	.inmo-list-price {

	}

	.inmo-list-price-sep {
		font-size: 10px;
		line-height: 15px;
		height: 15px;
	}

	.inmo-list-price-td {
		font-size: 18px;
		background-color: <?= $highlight_background ?>;
		color: <?= $highlight_background_color ?>;
		padding: 7px 30px;
	}

	/* FI INMO LIST */

	.cls {
		clear: both;
		height: 0;
		font-size: 1px;
		line-height: 0px;
	}

	/* RESPONSIVE */
	/* 700 */
		@media only screen and (max-width: 700px) {
			#letnd-header,
			#letnd-slogan,
			#letnd-content,
			#letnd-content_extra,
			#letnd-footer {
				width: 100% !important;
			}
			#letnd-content {
				margin-right: 0 !important;
				margin-left: 0 !important;
			}
			.letnd-content {
				padding-right: 20px;
				padding-left: 20px;
			}
			.letnd-header-right,
			.letnd-content-left,
			.letnd-list-details,
			.letnd-list-image,
			.letnd-form-image {
				width: 50% !important;
			}
			.form th {
				width: auto !important;
			}
		}

		/* 620 */
		@media only screen and (max-width: 620px) {
			.letnd-header img {
				width: 160px;
				height: auto;
			}
		}

		/* 560 */
		@media only screen and (max-width: 560px) {
			.letnd-tel-1 {
				display: none;
			}
			.letnd-tel span {
				display: none;
			}
		}

		/* 380 */
		@media only screen and (max-width: 380px) {
			.letnd-header img {
				width: 130px;
				height: auto;
			}
			.letnd-tel {
				font-size: 14px !important;
			}
		}
</style>