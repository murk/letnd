<?
/**
 * __Tool__Section
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2011
 * @version $Id$
 * @access public
 */
class __Tool__Section extends Module{

	public function __construct(){
		parent::__construct();
	}
	public function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	public function get_records()
	{
	    $listing = new ListRecords($this);
	    return $listing->list_records();
	}
	public function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	public function get_form()
	{
	    $show = new ShowForm($this);
	    return $show->show_form();
	}

	public function save_rows()
	{
	    $save_rows = new SaveRows($this);
	    $save_rows->save();
	}

	public function write_record()
	{
	    $writerec = new SaveRows($this);
	    $writerec->save();
	}

	public function manage_images()
	{
	    $image_manager = new ImageManager($this);
	    $image_manager->execute();
	}
}