<div class="payent-common">
	<? if ($show_method):?>
		<h3><?=$c_method_title1?></h3>
		<h4><?=$c_method_title2?></h4>
		<ul class="payent-options">
			<?foreach($loop as $l): extract ($l)?>
			<li>
			  <a href="<?=$link?>">
				  <? /* TODO-i Comprovar en totes les botigues online */ ?>

				  <img  src="/common/images/<?=$method?>.png" alt="<?=$caption?>">
			    <span><?=$caption?></span>
		        <? if ( $method == 'ondelivery' && $rate_encrease_ondelivery_basetax ): ?>
				    <strong>( +<?= $rate_encrease_ondelivery_basetax ?> <?=CURRENCY_NAME?> )</strong>
				<? endif //$ondelivery ?>
				</a>
			</li>
			<?endforeach //$loop?>
		</ul>
	<? endif //show_method?>

	<? if ($show_paypal):?>
		<h3><?=$c_show_paypal?></h3>
		<p><?=$paypal_form?></p>
	<? endif //show_paypal?>

	<? if ($show_4b):?>
		<h3><?=$c_show_4b?></h3>
		<?=$c4b_form?>
	<? endif //show_4b?>

	<? if ($show_lacaixa):?>
		<h3><?=$c_show_lacaixa?></h3>
		<?=$lacaixa_form?>
	<? endif //show_lacaixa?>

	<? if ($show_account):?>
			
			<p class="payment-message"><?=$c_text_buyer2?></p>
			<p><?=$c_text_buyer3?></p>
			<h3><?=$c_account_title?></h3>
		  <table class="form">
			<tr>
			  <td width="150" valign="top"><strong>
				<?=$banc_name?>:</strong> </td>
			  <td valign="top"><?=$account_number?></td>
			</tr>
			<tr>
			  <td width="150" valign="top"><strong>
				<?=$c_account_amount?>:</strong> </td>
			  <td valign="top"><?=$all_basetax?>&nbsp;<?=CURRENCY_NAME?></td>
			</tr>
			<tr>
			  <td width="150" valign="top"><strong>
				<?=$c_account_concept?>:</strong> </td>
			  <td valign="top"><?=$concept?></td>
			</tr>
		  </table>
	<? endif //show_account?>
	
	<? if ($show_ondelivery):?>
			
			<p class="payment-message"><?=$c_text_buyer2?></p>
			<p><?=$c_text_buyer3?></p>
	<? endif //show_ondelivery?>
	
	<? if ($show_end): // es mostra quan es retorna desde paypal ?>
			
			<p class="payment-message"><?=$c_text_buyer2?></p>
			<p><?=$order?></p>
	<? endif //show_end?>
	
	<?if($show_nocart):?>
			<h4 class="cart-empty"><?=$c_cart_is_empty?></h4>
	<?endif //show_nocart?>
	
	<?=$google_conversion?>
</div>