<?php
/* 21-4-17
Afegir camps facturació a clients de INMO

*/
if ( is_table( 'custumer__custumer' ) && ! is_field( 'custumer__custumer', 'comercial_name' ) ) {

	$update_sql[] = "ALTER TABLE `custumer__custumer`
	ADD COLUMN `comercial_name` VARCHAR(250) NULL DEFAULT NULL AFTER `company`;";
	$update_sql[] = "ALTER TABLE `custumer__custumer`
	ADD COLUMN `is_invoice` TINYINT(1) NOT NULL DEFAULT '0' AFTER `name`;";
	$update_sql[] = "ALTER TABLE `custumer__custumer`
ADD COLUMN `billing_mail` VARCHAR(200) NULL DEFAULT NULL AFTER `account_holder`,
	ADD COLUMN `billing_type` ENUM('bank_receipt','wire_transfer','bank_draft','cash') NULL DEFAULT NULL AFTER `billing_mail`;";
	$update_sql[] = "ALTER TABLE `custumer__custumer`
CHANGE COLUMN `billing` `billing` ENUM('company','citizen') NULL DEFAULT NULL COLLATE 'utf8_spanish2_ci' AFTER `billing_type`,
	ADD COLUMN `invoice_informations` TEXT NULL AFTER `billing`;";
	$update_sql[] = "ALTER TABLE `custumer__custumer`
	ADD COLUMN `nif` VARCHAR(10) NULL DEFAULT NULL AFTER `comercial_name`;";

} else {
	if ( is_table( 'custumer__custumer' ) ) {
		trigger_error( 'Ja existeixen el camp comercial_name' );
	}
}
?>