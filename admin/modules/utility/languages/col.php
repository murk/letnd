<?php
// menus eina

define('UTILITY_MENU_UTILITIES','Utilidades');
define('UTILITY_MENU_UTILITIES_QUOTA','Cálculo de cuotas');
define('UTILITY_MENU_UTILITIES_EXPENSE','Cálculo de gastos');
define('UTILITY_MENU_UTILITIES_SAVE','Ahorro fiscal');
define('UTILITY_MENU_UTILITIES_CALCULATOR','Calculadora');

$gl_caption_utilities['c_titlec']= "Cálculo de cuotas para las hipotecas.";
$gl_caption_utilities['c_price']= "Preu ():";
$gl_caption_utilities['c_deposit']= "Entrada ():";
$gl_caption_utilities['c_interest']= "Interes (%):";
$gl_caption_utilities['c_years']= "Años:";
$gl_caption_utilities['c_cuota']= "Cuota ():";
$gl_caption_utilities['c_notice']="La información obtenida en esta simulación de cálculo és totalmente orientativa y no implica ningún compromiso, ni ningún vinculo jurídico-legal.";
$gl_caption_utilities['c_titles']= "Cálculo del ahorro fiscal.";
$gl_caption_utilities['c_import_prestec']= "Importe del prestamo.";
$gl_caption_utilities['c_period_years']= "Terminio de amortitzación:";
$gl_caption_utilities['c_coin']="Moneda";
$gl_caption_utilities['c_eur']="Euros";
$gl_caption_utilities['c_pts']="Pesetas";
$gl_caption_utilities['c_workout']= "Deducción:";
$gl_caption_utilities['c_single']= "Individual";
$gl_caption_utilities['c_couplegroup']= "Dos titulares. Conjunta:";
$gl_caption_utilities['c_couplesingle']= "Dos titulares. Individuales:";
$gl_caption_utilities['c_saveyear']= "Ahorro primer año:";
$gl_caption_utilities['c_completesave']= "Ahorro total de la operación:";
$gl_caption_utilities['c_info']="La información obtenida en esta simulación de cálculo és totalmente orientativa y no implica ningún compromiso, ni ningún vinculo jurídico-legal.";

$gl_caption_utilities['c_calculator']= "Calculadora";



define ('c_workout','Calcular');

?>
