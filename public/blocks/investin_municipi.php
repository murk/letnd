<?php
$tipus = isset( $vars['tipus'] ) ? $vars['tipus'] : false;
$comarca_id = isset( $vars['comarca_id'] ) ? $vars['comarca_id'] : '20'; // per defecte Girona

include_once( 'inmo_block_functions.php' );
$condition = '';
$condition .= inmo_block_get_tipus( $tipus );

$vars['var_name'] = 'municipi_id';

$municipi_id = R::number( 'municipi_id' );
$zone_id     = R::number( 'zone_id' );

$vars['block_selected'] = $municipi_id ? 1 : 0;

$query = "SELECT DISTINCT municipi, inmo__municipi.municipi_id
				FROM inmo__municipi, inmo__property
				WHERE inmo__municipi.municipi_id = inmo__property.municipi_id
				AND inmo__municipi.comarca_id = '" . $comarca_id . "'
				AND bin = 0 AND (status = 'onsale' OR status = 'sold' OR status = 'reserved') " . $condition . "
				ORDER BY municipi";

investin_connect_inmo();
$results = Db::get_rows( $query );

Db::reconnect();


if ( $results ) {
	foreach ( $results as $rs ) {
		$municipi_id == $rs["municipi_id"] ? $selected = 1 : $selected = 0;

		/*
		if (USE_FRIENDLY_URL) {
			$friendly_params['municipi_id'] = $rs['municipi_id'];
			if ($tipus) $friendly_params['tipus'] = $tipus;
			$l['link'] = Page::get_link('inmo','property',false,false,$friendly_params,false,'',false,false);
		} else {
			$l['link'] = '/?tool=inmo&amp;tool_section=property&amp;municipi_id=' . $rs['municipi_id'] . '&amp;language=' . LANGUAGE;
		}
		 * 
		 */

		$l['link'] = get_link( array(
			'tool'            => 'inmo',
			'tool_section'    => 'property',
			'friendly_params' => array( 'tipus'       => $tipus == INMO_DEFAULT_TIPUS ? '' : $tipus,
			                            'municipi_id' => $rs['municipi_id']
			),
			'link_vars'       => array( 'tipus' => $tipus ),
			'detect_page'     => true
		) );

		$l['selected'] = $selected;
		$l['value']    = $rs['municipi_id'];
		$l['item']     = $rs['municipi'];

		$loop2 = array();

		$query = "
		SELECT espai_id,latitude, longitude, zone, zone_id, municipi_id, superficies, tipus
			FROM investin__espai
			LEFT OUTER JOIN investin__espai_language USING (espai_id)
			WHERE investin__espai_language.language = '" . LANGUAGE . "'
			AND investin__espai.bin = 0
			AND status = 'public'
			AND municipi_id = " . $rs["municipi_id"] . "
			ORDER BY zone ASC";

		$results2 = Db::get_rows( $query );

		investin_connect_inmo();

		foreach ( $results2 as $rs2 ) {

			// comprovo que hi hagin inmobles a la zona
			$query = "SELECT count(*)
				FROM inmo__property
				WHERE zone_id = " . $rs2['zone_id'] . "
				AND bin = 0 AND (status = 'onsale' OR status = 'sold' OR status = 'reserved') " . $condition;

			$has_properties = Db::get_first( $query );

			if ( $has_properties ) {
				$l2 = array();

				if ( $zone_id == $rs2["zone_id"] ) {
					$selected      = 1;
					$l['selected'] = 1;
				} else {
					$selected = 0;
				}

				$l2['selected'] = $selected;
				$l2['value']    = $rs2['espai_id'];
				$l2['item']     = $rs2['zone'];
				$l2['link']     = get_link( array(
					'tool'            => 'inmo',
					'tool_section'    => 'property',
					'friendly_params' => array( 'tipus'   => $tipus == INMO_DEFAULT_TIPUS ? '' : $tipus,
					                            'zone_id' => $rs2['zone_id']
					),
					'link_vars'       => array( 'tipus' => $tipus ),
					'detect_page'     => true
				) );

				$loop2[] = $l2;
			}
		}

		Db::reconnect();

		if ( $loop2 ) {
			$loop2[0]['class']                     = 'first';
			$loop2[ count( $loop2 ) - 1 ]['class'] = 'last';
		}
		$l['loop'] = $loop2;

		$loop[] = $l;
	}

	$loop[0]['class']                    = 'first';
	$loop[ count( $loop ) - 1 ]['class'] = 'last';
}


?>