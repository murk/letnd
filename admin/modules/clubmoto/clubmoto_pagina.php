<?
/**
 * ClubmotoPagina
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2012
 * @version $Id$
 * @access public
 
 He desactiva t que s'entrin imatges just desprès d'entrar un registre, els registres s'han d'editar dede el seu llistat així agafa el process
 
 
 */
class ClubmotoPagina extends Module{
	
	var $level, $last_menu, $last_menu_sub;
	
	function __construct(){
		parent::__construct();
	}
	function on_load(){	
		
		$this->last_menu = &$GLOBALS['gl_page']->last_menu[$GLOBALS['gl_menu_id']][0];
		$this->last_menu_sub = &$GLOBALS['gl_page']->last_menu[$GLOBALS['gl_menu_id']][1];		
	
		$levels = array('revista'=>1,'apartat'=>2,'pagina'=>3);
		if (!$this->process) $this->process='revista';
		$this->level = $levels[$this->process];
		
		// agafo el level directament del registre, per assegurar-me, ja que el formulari canvia segons el level
		// Pot ser revista, apartat o secció
		if ($this->action=='show_form_edit') {			
			$this->level = Db::get_first("SELECT level FROM clubmoto__pagina WHERE pagina_id='" . R::id('pagina_id'). "'");
			if ($this->level===false){
				$this->action = 'list_records';
				$this->level=1;
			}
			else{
				$processes = array('1'=>'revista','2'=>'apartat','3'=>'pagina');
				$this->process = $processes [$this->level];
			}
		}
		
		$this->set_vars(array(
			'is_revista'=>false,
			'is_apartat'=>false,
			'is_pagina'=>false,
			));
		$this->caption['c_edit_title'] = $this->caption['c_edit_'.$this->process];
		
		if ($this->process=='revista'){
			$this->unset_field('paginatpl_id');
			$this->unset_field('category_id');
			$this->unset_field('page_title');
			$this->unset_field('page_description');
			$this->unset_field('page_keywords');
			$this->unset_field('body_supertitle');
			$this->unset_field('title');
			$this->unset_field('pagina_file_name');
			$this->unset_field('is_clubone');
			$this->unset_field('is_advertisement');
			$this->unset_field('body_subtitle');
			$this->unset_field('body_content');
			$this->unset_field('body_supertitle2');
			$this->unset_field('title2');
			$this->unset_field('body_subtitle2');
			$this->unset_field('body_content2');
			$this->unset_field('body_supertitle3');
			$this->unset_field('title3');
			$this->unset_field('body_subtitle3');
			$this->unset_field('body_content3');
			$this->unset_field('videoframe');
			$this->unset_field('file');
			$this->set_vars(array(
				'paginatpl_id'=>false,
				'category_id'=>false,
				'pagina_page_title'=>false, // si no m'agafa el page_title de la intranet
				'page_description'=>false,
				'page_keywords'=>false,
				'pagina_file_name'=>false,
				'body_supertitle'=>false,
				'title'=>false,
				'is_clubone'=>false,
				'is_advertisement'=>false,
				'body_subtitle'=>false,
				'body_content'=>false,
				'body_supertitle2'=>false,
				'title2'=>false,
				'body_subtitle2'=>false,
				'body_content2'=>false,
				'body_supertitle3'=>false,
				'title3'=>false,
				'body_subtitle3'=>false,
				'body_content3'=>false,
				'videoframe'=>false,
				'file'=>false,
				'is_revista'=>true,
				));
		}
		elseif ($this->process=='apartat'){
			$this->unset_field('paginatpl_id');	
			$this->unset_field('body_supertitle');
			$this->unset_field('title');
			$this->unset_field('is_clubone');
			$this->unset_field('body_subtitle');
			$this->unset_field('body_content');
			$this->unset_field('body_supertitle2');
			$this->unset_field('title2');
			$this->unset_field('body_subtitle2');
			$this->unset_field('body_content2');
			$this->unset_field('body_supertitle3');
			$this->unset_field('title3');
			$this->unset_field('body_subtitle3');
			$this->unset_field('body_content3');
			$this->unset_field('videoframe');
			$this->unset_field('file');	
			$this->set_vars(array(
				'paginatpl_id'=>false,
				'pagina_page_title'=>true,
				'body_supertitle'=>false,
				'title'=>false,
				'is_clubone'=>false,
				'body_subtitle'=>false,
				'body_content'=>false,
				'body_supertitle2'=>false,
				'title2'=>false,
				'body_subtitle2'=>false,
				'body_content2'=>false,
				'body_supertitle3'=>false,
				'title3'=>false,
				'body_subtitle3'=>false,
				'body_content3'=>false,
				'videoframe'=>false,
				'file'=>false,
				'is_apartat'=>true,
				));
		}
		elseif ($this->process=='pagina'){
			$this->unset_field('category_id');
			$this->unset_field('page_title');
			$this->unset_field('page_description');
			$this->unset_field('page_keywords');
			$this->unset_field('pagina_file_name');
			$this->unset_field('is_advertisement');
			$this->set_vars(array(
				'category_id'=>false,
				'pagina_page_title'=>false,
				'page_description'=>false,
				'page_keywords'=>false,
				'pagina_file_name'=>false,
				'is_advertisement'=>false,
				'is_pagina'=>true,
				));
		}
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function get_records()
	{
		
		$listing = new ListRecords($this);
		
		if ($this->action!='list_records_bin'){
			$listing->set_options(1, 1, 1, 1, 1, 1, 0, 1, 1, 1);
			
			$listing->condition = 'level = ' . $this->level;
			$listing->always_parse_template = true;
			$listing->add_filter('status');
			
			$pagina_id = R::id('pagina_id');
			if ($pagina_id) $listing->condition .= ' AND parent_id = ' . $pagina_id;
			//debug::p($this->process);
			if ($this->process=='revista'){
				$listing->add_options_button ('add_revista_button', 'menu_id=2202');
				$listing->add_button ('edit_revista_button', 'action=show_form_edit', 'boto1');
				$listing->add_button ('view_apartat_button', 'process=apartat', 'boto2','after');
				
				$listing->order_by = 'ordre ASC, pagina desc';			
			}
			elseif ($this->process=='apartat'){
				$listing->add_options_button ('add_apartat_button', 'menu_id=2203&action=show_form_new&process=apartat&parent_id='.$pagina_id);
				$listing->add_button ('edit_apartat_button', 'action=show_form_edit', 'boto1');
				$listing->add_button ('view_pagina_button', 'process=pagina', 'boto2','after');
			
				$listing->add_filter('is_advertisement');
				$listing->add_filter('category_id');
				
				$listing->group_fields = array('category_id');
				$listing->group_fields_function = 'format_groupfield';
				$listing->extra_fields = "(SELECT clubmoto__category.ordre FROM clubmoto__category WHERE clubmoto__category.category_id = clubmoto__pagina.category_id) AS category, (SELECT clubmoto__category.is_clubone FROM clubmoto__category WHERE clubmoto__category.category_id = clubmoto__pagina.category_id) AS category_is_clubone";
				
				$listing->order_by = 'category ASC, category_id, ordre ASC, pagina_id';
				$listing->set_field('is_advertisement','type','none');
				$listing->call('list_records_walk_apartat','pagina_id,is_advertisement');
				
				$this->set_menus_pagina();		
			}
			elseif ($this->process=='pagina'){
				$listing->add_options_button ('add_pagina_button', 'menu_id=2203&action=show_form_new&process=pagina&parent_id='.$pagina_id);
				$listing->add_button ('edit_pagina_button', 'action=show_form_edit', 'boto1');
				$listing->extra_fields = "(SELECT paginatpl FROM clubmoto__paginatpl_language WHERE clubmoto__paginatpl_language.paginatpl_id = clubmoto__pagina.paginatpl_id AND language='".LANGUAGE."') AS paginatpl";
				
				$listing->add_filter('is_clubone');
				$listing->add_filter('paginatpl_id');
				
				$listing->order_by = 'ordre ASC, pagina_id';
				$listing->set_field('is_clubone','type','none');
				$listing->call('list_records_walk_pagina','pagina_id,is_clubone,paginatpl_id,paginatpl');
				
				$this->set_submenus_pagina();
			
			}
			
		}
		else{		
			$listing->set_options(1, 1, 1, 1, 1, 1, 0, 0, 0, 1);		
			$listing->call('list_records_bin_walk','pagina,parent_id');
		}
	    return $listing->list_records();
	}
	function format_groupfield(&$rs, $group_field, $val){
		if ($rs['category_is_clubone'])
			return '<span style="vertical-align:middle">' . $val . ' </span><span style="vertical-align:middle"><img src="/admin/modules/clubmoto/clubone.gif" width="62" height="16"></span>';
		else
			return $val;
	}
	
	function list_records_walk_apartat($id,$is_advertisement){			
		
		$checked = $is_advertisement?' checked':'';
		$ret['is_advertisement']= '<input id="is_advertisement_'.$id.'" type="checkbox" value="1" name="is_advertisement['.$id.']"'.$checked.'>
<input type="hidden" value="" name="old_is_advertisement['.$id.']">';
		$ret['is_advertisement'] .= '<span style="vertical-align:baseline"><img src="/admin/modules/clubmoto/advertisement.gif" width="70" height="16"></span>';
			
		return $ret;
	}
	
	function list_records_walk_pagina($id,$is_clubone,$paginatpl_id,$paginatpl){	
	
		$checked = $is_clubone?' checked':'';
		$ret['is_clubone']= '<input id="is_clubone_'.$id.'" type="checkbox" value="1" name="is_clubone['.$id.']"'.$checked.'>
<input type="hidden" value="" name="old_is_clubone['.$id.']">';
		$ret['is_clubone'] .= '<span style="vertical-align:baseline"><img src="/admin/modules/clubmoto/socis.gif" width="47" height="16"></span>';
		
		$ret['paginatpl_id'] = '<img title="'.$paginatpl.'" src="/admin/modules/clubmoto/paginatpl_'.$paginatpl_id.'.gif" />';
				
		return $ret;
	}
	
	function list_records_bin_walk($pagina,$parent_id){
		
		while ($parent_id!=0) {
			$rs = Db::get_row("SELECT parent_id,pagina FROM clubmoto__pagina, clubmoto__pagina_language WHERE clubmoto__pagina.pagina_id = clubmoto__pagina_language.pagina_id AND language='".LANGUAGE."' AND clubmoto__pagina.pagina_id = '".$parent_id."'");
			$parent_id = $rs['parent_id'];
			$pagina = $rs['pagina'] . ' > ' . $pagina;
		}
		$ret['pagina'] = $pagina;
		return $ret;
	}
	function set_menus_pagina($parent_id=false, $pagina=false){
	
		$page = &$GLOBALS['gl_page'];

		$pagina_id = $parent_id?$parent_id:R::id('pagina_id');				
		
		if ($parent_id){
		$link = '?process=apartat&menu_id=2203&pagina_id='.$parent_id;
				
		}
		else{
			$link = '';
		}
		$name = $this->get_pagina($pagina_id);
		$page->add_breadcrumb ($name, $link);
		$page->title = $this->caption['c_subtitle_'.$this->process];
		$page->subtitle = $name;
		if ($parent_id) Page::add_breadcrumb ($this->caption['c_breadcrumb_edit_'.$this->process]);
		
		//$this->last_menu = array('name'=>$name,'link'=>$link);
	}
	function set_submenus_pagina($parent_id=false, $pagina=false){
	
		$page = &$GLOBALS['gl_page'];
		
		$pagina_id = $parent_id?$parent_id:R::id('pagina_id');
		
		if ($parent_id){
			$link2 = '?process=pagina&menu_id=2203&pagina_id='.$parent_id;
				
		}
		else{
			$link2 = '';
		}
		
		$subtitles =  $this->get_pagina($pagina_id, true);
		$link = '?process=apartat&menu_id=2203&pagina_id='.$subtitles['parent_id'];
		
		$page->title = $this->caption['c_subtitle_'.$this->process];
		$page->subtitle = $subtitles['subtitle'];
		
		$page->add_breadcrumb ($subtitles['title'], $link);
		$page->add_breadcrumb ($subtitles['subtitle'], $link2);
		
		if ($parent_id) Page::add_breadcrumb ($this->caption['c_breadcrumb_edit_'.$this->process]);
		
		$this->last_menu = array('name'=>$subtitles['title'],'link'=>$link);		
		$this->last_menu_sub = array('name'=>$subtitles['subtitle'],'link'=>$link2);		
	}
	function get_pagina($pagina_id, $get_parent=false){
		$query = "SELECT pagina FROM clubmoto__pagina_language WHERE language='" . LANGUAGE . "' AND pagina_id='" . $pagina_id . "'";
		
		$title = Db::get_first($query);
		if ($get_parent){
			$query = "SELECT  pagina, pagina_id FROM clubmoto__pagina_language WHERE language='" . LANGUAGE . "' AND pagina_id=(SELECT parent_id FROM clubmoto__pagina WHERE pagina_id = '" . $pagina_id . "')";
			$rs = Db::get_row($query);
			$title  = array('title'=>$rs['pagina'], 'subtitle'=>$title, 'parent_id'=>$rs['pagina_id']);
		}
		return $title;
	}
	
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{
	    $show = new ShowForm($this);
		
		$show->get_values();

		// segons el llistat d'on vinc, agafo el nivell
		if ($this->action=='show_form_new') {
			if ((isset($_GET['parent_id']))){
				$this->level = Db::get_first('SELECT level FROM clubmoto__pagina WHERE pagina_id = ' . $_GET['parent_id'])+1;
				$parent_id = $_GET['parent_id'];
			}
			else{
				$this->level = 1;
				$parent_id = 0;
			}
			$show->set_field('level','default_value',$this->level);
			$show->set_field('parent_id','default_value',$parent_id);
		}
		else{
			$parent_id = $show->rs['parent_id'];
		}
		
		if ($this->process=='revista'){		
		}
		elseif ($this->process=='apartat'){	
			$this->set_menus_pagina($parent_id);	
		}
		elseif ($this->process=='pagina'){	
			$show->call('show_records_walk','pagina_id,paginatpl_id',true);		
			$this->set_submenus_pagina($parent_id);
		}
		
	    return $show->show_form();
	}
	function show_records_walk(&$show,$pagina_id,$paginatpl_id){
		// faig select pel tpl
		$query = "SELECT paginatpl, clubmoto__paginatpl.paginatpl_id AS paginatpl_id 
					FROM clubmoto__paginatpl, clubmoto__paginatpl_language 
					WHERE clubmoto__paginatpl.paginatpl_id = clubmoto__paginatpl_language.paginatpl_id 
					AND language = '" . LANGUAGE . "' 
					ORDER BY ordre ASC, paginatpl_id ASC";
		
		$results = Db::get_rows($query);
		
		if (!$paginatpl_id) $paginatpl_id = $results[0]['paginatpl_id'];
		$pagina_select = '';
		
		$show->set_var('pagina_tpl_selected',$paginatpl_id);
		
		foreach ($results as $rs){
			$selected = ($paginatpl_id==$rs['paginatpl_id'])?' class="selected"':'';
			$pagina_select .= '<div rel="'.$rs['paginatpl_id'].'"'.$selected.'><img title="'.$rs['paginatpl'].'" src="/admin/modules/clubmoto/paginatpl_'.$rs['paginatpl_id'].'.gif" /></div>';
		}
		$pagina_select = '
			
			<div class="image_select" id="paginatpl_id_image_select">
			' . $pagina_select . 
				'<input id="paginatpl_id_'.$pagina_id.'" name="paginatpl_id['.$pagina_id.']" type="hidden" value="'.$paginatpl_id.'" />
				<input name="old_paginatpl_id['.$pagina_id.']" type="hidden" value="'.$paginatpl_id.'" />
			</div>';
		
		return array('paginatpl_id'=>$pagina_select);
	}	

	function save_rows()
	{
	    $save_rows = new SaveRows($this);
		
		if ($this->action == 'save_rows_bin_selected'){
			foreach ($save_rows->id as $id){
				$this->delete_children($id, 'bin');
			}
		}
		
		if ($this->action == 'save_rows_delete_selected'){
			foreach ($save_rows->id as $id){
				$this->delete_children($id, 'delete');
			}
		}
		
		if ($this->action == 'save_rows_restore_selected'){
		
			$results = Db::get_rows("SELECT pagina_id, level, parent_id FROM clubmoto__pagina WHERE pagina_id IN (" . implode(',',$save_rows->id) . ") ORDER BY level asc");
			
			
			debug::p($results);
			debug::p($_POST['selected']);
			
			// torno a fer l'array per posar nomès els que puc recuperar
			$_POST['selected'] = array();
			$notify_ids = array();
			
			foreach ($results as $rs){
				// primer nivell espoden recuperar tots
				if ($rs['level']==1){
					$_POST['selected'][] = $rs['pagina_id'];
				}
				// si es nivell 3, nomès s'ha de mirar un nivell amunt, ja que el seu parent de nivell 2 nomès es pot recuperar si el de nivell 1 està correcte
				else if ($rs['level']==2 || $rs['level']==3){
					$is_parent_not_deleted = Db::get_first("SELECT pagina_id 
							FROM clubmoto__pagina 
							WHERE bin = 0 AND pagina_id = " .  $rs['parent_id']);
					// si no està borrat el parent, o ja es a l'array de recuperar doncs podem recuperar
					
					if ($is_parent_not_deleted || in_array($rs['parent_id'],$_POST['selected']))
					{
						$_POST['selected'][] = $rs['pagina_id'];
					}
					else{
						$notify_ids[] = $rs['pagina_id'];
					}
				}
			}
			
			debug::p($_POST['selected'],'recuperar');
			debug::p($notify_ids,'no espot');
			
			$save_rows->notify($notify_ids,$this->messages['c_restore_items']);			
		}
	    $save_rows->save();
	}

	function write_record()
	{	 
		$writerec = new SaveRows($this);
		$writerec->set_field('modified','override_save_value','now()');
		
		// borro els valors amagats al canviar de plantilla
		if ($this->process=='pagina'){
			$paginatpl = $writerec->get_value('paginatpl_id');
			if ($paginatpl=='5' || $paginatpl=='6'){
				$writerec->set_value('title','','',true);
				$writerec->set_value('title2','','',true);
				$writerec->set_value('title3','','',true);
				$writerec->set_value('body_subtitle','','',true);
				$writerec->set_value('body_subtitle2','','',true);
				$writerec->set_value('body_subtitle3','','',true);
				$writerec->set_value('body_content','','',true);
				$writerec->set_value('body_content2','','',true);
				$writerec->set_value('body_content3','','',true);
				$writerec->set_value('videoframe');
			}
			if ($paginatpl=='1'){
				$writerec->set_value('videoframe','');
			}
		}
		elseif ($this->process=='apartat'){ 			
			$writerec->field_unique = "pagina_file_name";
		}
		
		if ($this->action == 'bin_record') $this->delete_children($writerec->id, 'bin');
		
		// Nomes es borra desde la paperera i en llistat
		// NO FUNCIONA AMB UN SOL REGISTRE, ja que no borraria els relacionats
		//if ($this->action == 'delete_record') $this->delete_children($save_rows->id, 'delete');
		
	    $writerec->save();
	}
	
	// al posar a la paperera també poso a la paperera tots els apartats i seccions o secions fills d'un registre
	// a la paperera nomès mostro els principals ( no puc controlar quan recuperen un registre fill d'un de la paperera )
	// al borrar també borro tots els apartats i seccions o secions fills d'un registre
	function delete_children($id, $action){
			$ids = $ids2 = false;
			$ids_array = $ids2_array = array();
			$ids_array = Db::get_rows("SELECT pagina_id FROM clubmoto__pagina WHERE parent_id = '" . $id . "'");
			$ids =  implode_field($ids_array, 'pagina_id');
			
			if ($ids) {
				$ids2_array = Db::get_rows("SELECT pagina_id FROM clubmoto__pagina WHERE parent_id IN (" .  $ids . ")");
				debug::add('Borrar subpagines', "SELECT pagina_id FROM clubmoto__pagina WHERE parent_id IN (" .  $ids . ")");
				$ids2 = implode_field($ids2_array, 'pagina_id');
			}
			debug::add('Borrar subpagines', $ids2);
			
			if ($action=='bin')
			{
				if ($ids){
					$query = "UPDATE clubmoto__pagina 
								SET bin='1' 
								WHERE pagina_id IN (" . $ids . ")";
					Db::execute($query);					
					debug::add('Bin subpagines', $query);
				}
				if ($ids2){
					$query = "UPDATE clubmoto__pagina 
							SET bin='1' 
							WHERE pagina_id IN (" . $ids2 . ")";
					Db::execute($query);					
					debug::add('Bin subsubpagines', $query);
				}
			}
			
			// afegeixo els ids de les pagines a borrar al post, així el motor s'encarrega de borrar els relacionats
			if ($action=='delete')
			{
				if ($ids){
					foreach ($ids_array as $rs){
						$_POST['selected'][]= $rs['pagina_id'];
					}
				}
				if ($ids2){
					foreach ($ids2_array as $rs){
					$_POST['selected'][]= $rs['pagina_id'];
					}
				}
			}
	}

	function manage_images()
	{	    
		if ($this->action=='list_records_images'){
		
			$id = $_GET[$this->tool_section . '_id'];
			$parent_id = Db::get_first("SELECT parent_id FROM clubmoto__pagina WHERE pagina_id=".$id);
			
			if ($this->process=='revista'){		
			}
			elseif ($this->process=='apartat'){	
				$this->set_menus_pagina($parent_id);	
			}
			elseif ($this->process=='pagina'){		
				$this->set_submenus_pagina($parent_id);
			}
		}
		
		$image_manager = new ImageManager($this);
		if ($this->process=='revista'){
			//$image_manager->show_main_image = false;
			$image_manager->caption['c_main_image'] = $image_manager->caption['c_main_image2'];
		}
		else{
			$image_manager->show_categorys = false;		
		}		
		
	    $image_manager->execute();
	}
}
?>