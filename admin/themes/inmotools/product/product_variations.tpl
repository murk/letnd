<SCRIPT LANGUAGE="JavaScript" TYPE="text/javascript">
<!--
var assign_selected_ids = <?=$assign_selected_ids?>;
//-->
</SCRIPT>

<script type="text/javascript" language="JavaScript" src="/admin/modules/product/jscripts/list_pick.js?v=<?=$version?>"></script>
<form method="post" action="<?=$form_link?>" name="list" onsubmit="return check_selected(this)" target="save_frame">
  <input name="action" type="hidden" value="save_rows">
<div class="form">
        <div id="form_buttons_holder"><table id ="form_buttons">
              <tr>
                <td valign="bottom"><div class="menu">
                <a href="<?=$data_link?>"><?=$c_edit_data?></a>
                <a href="<?=$image_link?>"><?=$c_edit_images?></a>
                <? foreach($form_tabs as $r): extract( $r ) ?>
	                <? if ( $tab_action ): ?>
		                <a href="?action=<?= $tab_action ?>&<?= $id_field ?>=<?= $id ?>&menu_id=<?= $menu_id ?>"><?= $tab_caption ?></a>
                    <? else: // ?>
						<div><?= $tab_caption ?></div>
	                <? endif // $tab_action ?>
                <? endforeach //$form_tabs?>
	                </div></td>
                <td class="buttons_top"><div class="right">
					<a class="botoOpcions1" href="javascript:select_addresses(<?=$id?>,<?=$menu_id?>)"><?=$c_add_variations?></a>
					<input onclick="submit_form_save('save_rows_save_records')" type="button" name="Button" value="<?=$c_send_edit?>" class="btn_form_save" /></div></td>
              </tr>
          </table></div>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
                  <tr>
                    <td><table width="100%" cellpadding="4" cellspacing="0">
                      <tr>
                        <td class="formCaptionTitle"><strong>
                  <?=$c_data_variations?>&nbsp;&nbsp;</strong><?=$product_form_title?></td>
                      </tr>
                    </table>
                      <table width="100%" cellpadding="4" cellspacing="0" class="formstbl">
                        <tr>
                          <td align="center" valign="top" class="forms"><table width="99%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                              <td align="left"><div class="separador"></div><div class="listTitle"><strong>
                              <?=$c_price_base?>&nbsp;&nbsp;</strong>
                              <em><?=$c_pvp?></em> : <?=$pvp?>&nbsp;<?=CURRENCY_NAME?>&nbsp;, <em><?=$c_pvd?></em> : <?=$pvd?>&nbsp;<?=CURRENCY_NAME?>
                              ,
                              <?=$pvd2?>&nbsp;<?=CURRENCY_NAME?>
                              , 
                              <?=$pvd3?>&nbsp;<?=CURRENCY_NAME?>
                              ,
                              <?=$pvd4?>&nbsp;<?=CURRENCY_NAME?>
                              ,
                              <?=$pvd5?>&nbsp;<?=CURRENCY_NAME?>
                              </div>
                                <?if ($loop):?>
									<div class="separador"></div>
									<? // variacions noves ?>
									<div id="new_variations" class="hidden">
											<div class="listTitle2" style="margin-bottom:4px;">
											  <strong>
											  <?=$c_new_variations?>
											</strong></div>
											<div id="new_variations_content">												
											</div>
									</div>
									<? // fi variacions noves ?>
									
									<?foreach($loop as $l): extract ($l)?>
									
										<?if($variation):?>
											<div class="listTitle2" style="margin-bottom:4px;">
											  <strong>
											  <?=$variation_category?>
											</strong></div>
											<div id="variation<?=$variation_category_id?>">
												<?=$variation?>
											</div>
										<?endif //variation?>
									  
									<?endforeach //$loop?>
                                <?endif //loop?>
								
                                <?if (!$loop):?>
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr>
                                    <td height="70" align="center"><?=$c_no_assigned?></td>
                                  </tr>
                                </table>
                                <?endif //loop?></td>
                            </tr>
                          </table></td>
                        </tr>
                      </table></td>
                  </tr>
                </table></td>
              </tr>
          </table>
<table width="100%" cellpadding="4" cellspacing="2" class="formsButtonstbl">
  <tr>
    <td align="center" class="formsButtons"><div class="centerFloat1">
                        <div class="centerFloat2"><a class="botoOpcions1" href="javascript:select_addresses(<?=$id?>,<?=$menu_id?>)"><?=$c_add_variations?></a><input onclick="submit_form_save('save_rows_save_records')" type="button" name="Button" value="<?=$c_send_edit?>" class="btn_form_save" /></div></div></td>
  </tr>
</table>
</div>