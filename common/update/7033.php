<?

/* 2-12-17
 Gestió de portals, afegir ceigrup i fotocasa


 */

if ( is_table( 'inmo__property_to_portal' ) && ! is_field( 'inmo__property_to_portal', 'fotocasa' ) ) {

	$update_sql[] = "ALTER TABLE `inmo__property_to_portal`
	ADD COLUMN `fotocasa` TINYINT(1) NOT NULL DEFAULT '0' AFTER `apicat`,
	ADD COLUMN `ceigrup` TINYINT(1) NOT NULL DEFAULT '0' AFTER `fotocasa`;";

} else {
	if ( is_table( 'inmo__property_to_portal' ) ) {
		trigger_error( 'Ja existeixen el camp fotocasa' );
	}
}

if ( is_table( 'inmo__configadmin' ) && ! Db::get_first( "SELECT * FROM inmo__configadmin WHERE name='fotocasa_export_all'" ) ) {
	$update_sql[] = "
		INSERT INTO inmo__configadmin (`name`, `value`) VALUES ('fotocasa_export_all', '0');";

} else {
	if ( is_table( 'inmo__configadmin' ) ) {
		trigger_error( 'Ja existeixen el camp fotocasa_export_all' );
	}
}