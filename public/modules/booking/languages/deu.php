<?
$gl_caption['c_subject_booked'] = 'Buchung Eigentum -#';
$gl_caption['c_subject_booked_owner'] = 'Buchung Eigentum -#';
$gl_caption['c_no_date_selected'] = "Kein Datum ausgewählt";
$gl_caption['c_booked_message_1'] = "Die Buchung wurde erfolgreich gespeichert";
$gl_caption['c_booked_message_2'] = "Weitere Informationen hier eingeben";
$gl_caption['c_form_book_title'] = "Buchungsdaten";
$gl_caption['c_form_book_custumer_title'] = "Personal Daten";
$gl_caption['c_book_finish_button'] = "Buchen";
$gl_caption['c_book_form_title'] = "Buchung #%s";

$gl_caption['c_edit_book'] = "Buchung editieren";

$gl_caption['c_mail_book_details'] = "Details zur Reservation ";
$gl_caption['c_mail_custumer_details'] = "Personal Daten";
$gl_caption['c_mail_extras'] = "Extras";
$gl_caption ['c_ref']='Ref.';
$gl_caption['c_promcode_button'] = 'Anwenden';


$gl_caption_payment['c_payment_title'] = 'Einkaufen';
$gl_caption_payment['c_method_title1']="Wählen Sie Ihre Zahlungsmethode.";
$gl_caption_payment['c_method_title2']="Der Betrag ist <strong>%s&nbsp;%s</strong> MwSt inkl.";
$gl_caption_payment['c_method_title_initial']="Die anfängliche Zahlung ist <strong>%s&nbsp;%s</strong> MwSt inkl. Der Rest der Zahlung, %s&nbsp;%s, muss vor dem Eintrittsdatum erfolgen";
$gl_caption_payment['c_method_title_rest']="Der Betrag der %s Zahlung ist <strong>%s&nbsp;%s</strong> MwSt inkl";
$gl_caption_payment['c_method_title_number_2']="Zweite";
$gl_caption_payment['c_method_title_number_3']="Dritte";
$gl_caption_payment['c_method_title_number_4']="Vierte";

$gl_caption_payment['c_method_paypal']="PayPal Zahlung";
$gl_caption_payment['c_method_account']="Bank transfer payment";
$gl_caption_payment['c_method_lacaixa']="Kreditkarte";

$gl_caption_payment['c_show_paypal']="Paypal Zahlung wird gemacht, wenn der Prozess nicht automatisch beginnen, bitte clik hier:";
$gl_caption_payment['c_show_lacaixa']="Kreditkarte Zahlung wird gemacht, wenn der Prozess nicht automatisch beginnen, bitte clik hier:";

$gl_caption['c_account_title']= 'Datos de la transferencia' ;
$gl_caption['c_account_amount']= 'Importe' ;
$gl_caption['c_account_concept']= 'Concepto' ;

$gl_caption_book['c_payment_whole']="Zahlung";
$gl_caption_book['c_payment_1']="Erste Zahlung";
$gl_caption_book['c_payment_2']="Zweite Zahlung";
$gl_caption_book['c_payment_3']="Dritte Zahlung";
$gl_caption_book['c_payment_4']="Vierte Zahlung";
$gl_caption_book['c_payment_paid']="Bezahlt";
$gl_caption_book['c_payment_pending']="Ausstehende";
$gl_caption_book['c_payment_pay']="Zahlen";
?>