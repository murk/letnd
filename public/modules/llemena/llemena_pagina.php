<?
/**
 * LlemenaPagina
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2011
 * @version $Id$
 * @access public
 */
class LlemenaPagina extends Module{
	var $id, $template;
	function __construct(){
		parent::__construct();
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function get_records()
	{
		$listing = new ListRecords($this);
		$listing->template = $this->template;
		$listing->condition = "status = 'public'";
		$listing->condition .= " AND parent_id = '" . $this->id . "'";
		//$listing->call_function['content'] = 'add_dots';
		$listing->call('records_walk','url1,url1_name,url2,url2_name,url3,url3_name,body_content,body_content_list,link,pagina_id,pagina_file_name,has_content');
		$listing->order_by = 'ordre ASC, entered DESC';
		return $listing->list_records();
	}
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{
		$show = new ShowForm($this);
		
		if ($this->parent=='home') $show->id = Db::get_first("SELECT pagina_id FROM llemena__pagina WHERE link='/'");

		$show->condition = "status = 'public'";
		$show->call('records_walk','url1,url1_name,url2,url2_name,url3,url3_name');
		
		$show->set_field('paginatpl_id','select_table','llemena__paginatpl');
		$show->set_field('paginatpl_id','select_fields','paginatpl_id,file_name');
		$show->set_field('paginatpl_id','select_condition','');
		
		$show->get_values();
		$this->id = $show->id;
		if ($show->has_results)
		{
			// defineixo les variables de la pàgina
			$GLOBALS['gl_page']->title = $show->rs['title']?$show->rs['title']:$show->rs['pagina'];
			$GLOBALS['gl_page']->page_title = $GLOBALS['gl_page']->page_title?$GLOBALS['gl_page']->page_title:$show->rs['page_title'];
			$GLOBALS['gl_page']->page_description = add_dots_meta_description($show->rs['body_content']);
				
			if ($this->parent=='home'){				
				$show->template = 'llemena/pagina_home.tpl';
			}
			else{
				$rs = Db::get_row("SELECT file_name,type FROM llemena__paginatpl WHERE paginatpl_id = " . $show->rs['paginatpl_id']);
				extract($rs);
				$show->template = 'llemena/' . $file_name;
				if ($type=='list'){	
					$this->template = $show->template;	
					$show->set_record();
					$this->set_vars($show->vars);
					return $this->do_action('get_records');
				}
			}
			return $show->show_form();
		}
		else
		{
			Main::redirect('/');
		}
	}
	function records_walk($url1,$url1_name,$url2,$url2_name,$url3,$url3_name,$body_content=false,$body_content_list=false,$link='',$pagina_id=0,$pagina_file_name='',$has_content=0){		
		// Això ho poso aquí així no cal posar-ho al tpl, simplement posar: <a href="<=$url1>" target="_blank"><=$url1_name></a>
		$ret['url1_name'] = $url1_name?$url1_name:$url1;
		$ret['url2_name'] = $url2_name?$url2_name:$url2;
		$ret['url3_name'] = $url3_name?$url3_name:$url3;
		
		// Així es pot posar l'adreça amb http o sense ( a la plantilla o poso sempre sense ja que pot ser https
		if (strpos($url1,'://')===false && $url1) $ret['url1']= 'http://' . $url1;
		if (strpos($url2,'://')===false && $url2) $ret['url2']= 'http://' . $url2;
		if (strpos($url3,'://')===false && $url3) $ret['url3']= 'http://' . $url3;
		
		if ($pagina_id) $ret['link'] = _block_llemena_pagina_get_pagina_link2($pagina_id, $link, $pagina_file_name, $has_content);
		
		// si es llistat passa lavariable content
		if ($body_content!==false){
			$ret['body_content']=$body_content_list?$body_content_list:add_dots('body_content', $body_content);
		}		
		
		return $ret;
	}
}

/////////////////// copia exacta del que hi ha a blocks

function _block_llemena_pagina_get_pagina_link2($pagina_id, $link, $pagina_file_name, $has_content)
{
	// si la pàgina no te contingut, agafo el link i tot del primer fill que es la pàgina que es mostra, així el link es exactamnet el mateix que el fill
	if (!$has_content){
		$rs=_block_llemena_get_first_child2($pagina_id);
		if ($rs){
			$link = $rs['link'];
			$pagina_id = $rs['pagina_id'];
			$pagina_file_name = $rs['pagina_file_name'];
		}
	}
	
	if (strpos($link, 'javascript:') === 0) {
		$ret = $link;
	} elseif ($link)
	{
		if (USE_FRIENDLY_URL){
			$ret = '/' . LANGUAGE;
			if ($link != '/') // la home pagina no porta el pagina_id
			{
				$ret .= $link . '/' . $pagina_file_name . '.html';
			}
		}
		else{
			$sep = strstr($link, '?')?'&amp;':'?';
			$ret = $link . $sep . 'language=' . LANGUAGE;
			if (strpos($link, 'pagina_id=') === false) $ret .= '&amp;pagina_id=' . $pagina_id;
		}
	}
	else
	{
		if (USE_FRIENDLY_URL){
			$ret = '/' . LANGUAGE . '/' . $pagina_file_name . '.html';
		}
		else{
			$ret = '/?tool=llemena&amp;tool_section=pagina&amp;action=show_record&amp;pagina_id=' . $pagina_id . '&amp;language=' . LANGUAGE;
		}
	}
	// EVITAR DUPLICATS
	// si es la home, trec el link del menu d'idiomes
	if ($GLOBALS['gl_default_language']==LANGUAGE && $link == '/') $ret = '/';
	
	return $ret;
	
}
function _block_llemena_get_first_child2($pagina_id){

	global $gl_language;
	
	$query = "SELECT template, link, pagina, page_title, page_description, page_keywords, title, body_subtitle, llemena__pagina.pagina_id as pagina_id, parent_id, level, has_content, pagina_file_name
			FROM llemena__pagina_language, llemena__pagina
			WHERE llemena__pagina_language.pagina_id = llemena__pagina.pagina_id
			AND status = 'public'
			AND parent_id = " . $pagina_id . "				
			AND bin <> 1
			AND language = '" . $gl_language . "'
			ORDER BY ordre ASC, pagina_id
			LIMIT 1";
	$rs = Db::get_row($query);	
	if ($rs) {		
		// si m'he equivocat a la taula, i poso que no te content, però tampoc hi ha cap fill, deixo el rs
		if (!$rs['has_content']) 
			return _block_llemena_get_first_child($pagina_id)?_block_llemena_get_first_child($pagina_id):$rs;
		else
			return $rs;
	}
	else {
		return false;
	}
}
?>