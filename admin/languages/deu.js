var NONE_SELECTED = " Es ist keine Zeile ausgewählt.";
 
var VALIDAR_TEXT1 = ' ist verpflichtend.';
var VALIDAR_TEXT2 = ' zeichen ungültig.';
var VALIDAR_TEXT3 = ' Dies ist keine korrekte E-Mail-Adresse.';
var VALIDAR_TEXT4 = ' Es sind mindestens 3 Zeichen erforderlich';
var VALIDAR_TEXT5 = ' Es ist eine Nummer erforderlich';
var VALIDAR_TEXT6 = ' Es ist eine Nummer mit 5 Stellen erforderlich ';
var VALIDAR_TEXT7 = ' Enthält ';
var VALIDAR_TEXT8 = ' Zeichen, darf nicht länger sein als ';
var VALIDAR_TEXT9 = ' Es fehlt das Jahr oder es ist nicht numerisch ';
var VALIDAR_TEXT10 = ' Das Jahr ist nicht richtig ';
var VALIDAR_TEXT11 = ' Das Datum ist nicht richtig ';
var VALIDAR_TEXT12 = ' Dies ist nicht richtig, geben Sie 8 Stellen an, und der Buchstabe ';
var VALIDAR_TEXT13 = ' ist nicht richtig ';
var VALIDAR_TEXT14 = ' Der Buchstabe ist nicht richtig ';
var VALIDAR_TEXT15 = ' ist nicht richtig ';
var VALIDAR_TEXT16 = 'Es muss eine Auswahl erfolgen ';
var VALIDAR_TEXT17 = 'Das Formular ist nicht richtig ausgefüllt worden ';
var VALIDAR_TEXT18 = ' Es muss mindestens eine Option ausgewählt werden';
var VALIDAR_TEXT19 = ' Es muss mindestens eine Anschrift oder Gruppe ausgewählt werden';
var VALIDAR_TEXT20 = ' Die Kreditkartennummer ist nicht richtig ';
var VALIDAR_TEXT21 = ' Das Gültigkeitsdatum der Kreditkarte ist nicht richtig ';
var VALIDAR_TEXT22 = ' is not correct';
var VALIDAR_TEXT23 = ' is not correct';
var VALIDAR_TEXT24 = ' must be a number';
var VALIDAR_TEXT25 = ' the passwords are not equal';
var VALIDAR_TEXT26 = ' confirmation is not equal';
var VALIDAR_TEXT27 = ' only can contain numbers, lower case characters from "a to z" or "_-" or dash-underscore ("_" o "-")';
var VALIDAR_TEXT28 = 'You must accept the privacy policy';
var MODALBOX_SAVE = ' Sending data ...';
var MODALBOX_CLOSE = 'close';
var MULTIFILE_ACCEPT = ' You only can upload files of type:: ';
var MULTIFILE_NOT_ACCEPT = ' You can not upload files of type: ';
var CONFIRM_TEXT = 'Are you sure you want to do this action?';
var WAIT_TEXT = 'One second plis ...';
 
var MONTH_NAMES = new Array("Januar ", " Februar ", " März ", " April ", " Mai ", "Juni", " Juli ", " August ", " September ", " Oktober ", " November ", " Dezember ");

var FILE_DELETE = "Delete";
// NO TRADUIT
var IMMO_MAP_POINT_SELECTED = "Click in a point of the map to establish the situation";
var PRODUCT_CART_ADDED = "Your product has been added to the cart";

var MESSAGE_CONFIRM_DELETE_NOT_SENT = 'Seguro que deseas borrar esta dirección de la llista de destinatarios?';

var NO_RESULTS = 'No results';