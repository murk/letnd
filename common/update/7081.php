<?
/*
30-05-2019
Guestbook

*/


if ( is_table( 'web__guestbook' ) && ! is_field( 'web__guestbook', 'destacat' ) ) {

	$update_sql[] = "ALTER TABLE `web__guestbook`
	ADD COLUMN `destacat` TINYINT(1) NOT NULL AFTER `entered`;
";

}
else {
	if ( is_table( 'web__guestbook' ) ) {
		trigger_error( 'Ja existeixen el camp destacat' );
	}
}