<?

/* 12-6-17
  Afegir pagament a booking

 */


if ( is_table( 'booking__book' ) ) {

	$update_sql[] = "
	ALTER TABLE `booking__book`
		CHANGE COLUMN `payment_entered_1` `payment_entered_1` DATETIME NOT NULL AFTER `payment_done_1`,
		CHANGE COLUMN `payment_entered_2` `payment_entered_2` DATETIME NOT NULL AFTER `payment_done_2`,
		CHANGE COLUMN `payment_entered_3` `payment_entered_3` DATETIME NOT NULL AFTER `payment_done_3`,
		CHANGE COLUMN `payment_entered_4` `payment_entered_4` DATETIME NOT NULL AFTER `payment_done_4`;";


}

?>