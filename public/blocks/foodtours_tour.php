<?php
/* 4-5-16
 * 
 * Block de tours de la home de foodtours
 * Nomes va amb USE_FRIENLY_URL
 *  
 */
$add_dots_length = 120;


$module = Module::load('foodtours','tour');
$module->parent = 'Block';

$loop = $module->do_action('get_records');


if (!$loop) {
	$show_block = false;
	return;
}

$loop_count = count($loop);
$conta = 0;

$query = "SELECT  page_file_name FROM all__page_language, all__page WHERE all__page_language.page_id = all__page.page_id AND language='".LANGUAGE."' AND page_text_id = 'tours'";
			
$page_file_name = Db::get_first($query);

foreach ($loop as $key=>$val){
	$rs = &$loop[$key];
	$rs['class'] = '';
	$rs['conta'] = $conta++;
	$rs['loop_count'] = $loop_count;

	$rs['content'] = add_dots('content', $rs['content'], $add_dots_length);

	$rs['link'] = Page::get_link('foodtours','tour',false,$rs['tour_id'],false,false,$rs['tour_file_name'],false,$page_file_name);
	
}

$loop[0]['class'] = 'first';	
$loop[count($loop)-1]['class'] = 'last';
?>