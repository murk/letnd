<table id="letnd-footer-holder" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td class="letnd-footer-holder" align="center">
			<table id="letnd-footer" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td class="letnd-footer">
						<p class="address"><strong><?= COMPANY_NAME ?></strong> <br>
							<span class="street-address"><?= PAGE_ADDRESS ?> <?= PAGE_NUMSTREET ?></span>,
							<span class="postal-code"><?= PAGE_POSTCODE ?></span>
							<span class="locality"><?= PAGE_MUNICIPI ?></span>,
							<span class="region"><?= PAGE_PROVINCIA ?></span>,
							<span class="country-name"><?= PAGE_COUNTRY ?></span>,
							T. <a target="_blank" value="<?= str_replace( ' ', '', PAGE_PHONE ); ?>" href="tel:<?= PAGE_PHONE ?>"><?= PAGE_PHONE ?></a>
							<? if ( PAGE_MOBILE ): ?>, <span class="type">Mob</span>.
								<a target="_blank" value="<?= str_replace( ' ', '', PAGE_MOBILE ); ?>" href="tel:<?= PAGE_MOBILE ?>"><?= PAGE_MOBILE ?></a><? endif //MOB?>
							<? if ( PAGE_FAX ): ?>, <span class="type">Fax</span>.
								<a target="_blank" value="<?= str_replace( ' ', '', PAGE_FAX ); ?>" href="tel:<?= PAGE_FAX ?>"><?= PAGE_FAX ?></a><? endif //FAX?><br>
							<a class="url" href="<?= $url ?>"><?= $short_host_url ?></a> -
							<a class="url" href="mailto:<?= encode_email( $default_mail ) ?>"><?= encode_email( $default_mail ) ?></a>
						</p>
						<? if ( $remove_text ): ?>
							<p class="remove"><?= $remove_text ?></p>
						<? endif //$remove_text ?>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>