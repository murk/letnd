<?php
/* 21-8-18
Arxius per comandes

*/


if ( is_table( 'product__order' ) ) {

	if ( ! is_table( 'product__order_file' ) ) {

		$update_sql [] = "CREATE TABLE `product__order_file` (
							`file_id` INT(11) NOT NULL AUTO_INCREMENT,
							`order_id` INT(11) NOT NULL,
							`name` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_spanish2_ci',
							`name_original` VARCHAR(255) NOT NULL COLLATE 'utf8_spanish2_ci',
							`title` VARCHAR(255) NOT NULL COLLATE 'utf8_spanish2_ci',
							`size` INT(11) NOT NULL,
							`ordre` INT(11) NOT NULL,
							`main_file` INT(1) NOT NULL,
							`public` TINYINT(1) NOT NULL DEFAULT '1',
							PRIMARY KEY (`file_id`),
							INDEX `order_id` (`order_id`)
						)
						COLLATE='utf8_spanish2_ci'
						ENGINE=MyISAM;";


		$client_dir = DOCUMENT_ROOT . '/' . $_SESSION['client_dir'];

		$dir = $client_dir . '/product/';
		if ( ! is_dir( $dir ) ) {
			mkdir( $dir, 0776 );
		}

		$dir = $client_dir . '/product/order/';
		if ( ! is_dir( $dir ) ) {
			mkdir( $dir, 0776 );
		}

		$dir = $client_dir . '/product/order/files/';
		if ( ! is_dir( $dir ) ) {
			mkdir( $dir, 0776 );
		}


	} else {
		trigger_error( 'Ja existeixen la taula product__order_file' );
	}

}
