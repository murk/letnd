<?
/**
 * InvoiceExpense
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2011
 * @version $Id$
 * @access public
 */
class InvoiceExpense extends Module{
	var $condition = '';
	var $q = false;

	function __construct(){
		parent::__construct();
	}
	function list_records()
	{
		$content = $this->get_records();
		if ($this->q) {
			$GLOBALS['gl_page']->javascript .= "
				$('table.listRecord').highlight('".addslashes($this->q)."');			
			";
		}
		$GLOBALS['gl_content'] = $this->get_search_form() . $content;
	}
	function get_records()
	{

		$this->set_news();

	    $listing = new ListRecords($this);
		$listing->set_field( 'vat_included', 'type', 'select' );
		$listing->set_field( 'vat_exempted', 'type', 'select' );
		$listing->set_field( 'is_deductible', 'type', 'select' );

		// search
		if ($this->condition) {
			$listing->condition .= $this->condition;
		}

		$listing->order_by = 'expense_id DESC';

		// El registre fake porta bin=0 sempre, així només m'haig de preocupar de posar bin=0 quan faig una consulta, i mirar fake només a la paperera que tampoc surti
		if ( $listing->is_bin ) {
			$and = $listing->condition ? ' AND' : '';
			$listing->condition .= "$and fake <> 1";
		}

	    return $listing->list_records();
	}


	function set_news() {
		global $gl_news;

		$today = strftime( '%e' );

		$month      = strftime( '%m' );
		$year      = strftime( '%Y' );
		$month_name      = ucfirst( strftime( '%B' ) );

		// Mes actual
		$ret = Db::get_row( "SELECT sum(total_price_no_vat) as total_no_vat,  sum(total_price) as total FROM invoice__invoice WHERE MONTH(bill_date) = $month AND YEAR(bill_date) = $year AND bin = 0 AND invoice__invoice.batch_id <> 0;" );

		$total_no_vat = $ret['total_no_vat'];
		$total        = $ret['total'];
		$vat = $ret['total'] - $ret['total_no_vat'];


		$ret_deductible = Db::get_row( "SELECT sum(total_price_no_vat) as total_no_vat,  sum(total_price) as total,  sum(total_vat) as total_vat,  sum(total_irpf) as total_irpf FROM invoice__expense WHERE MONTH(bill_due) = $month AND YEAR(bill_due) = $year AND bin = 0 AND is_deductible = 0;" );


		$ret_no_deductible = Db::get_row( "SELECT sum(total_price_no_vat) as total_no_vat,  sum(total_price) as total,  sum(total_irpf) as total_irpf FROM invoice__expense WHERE MONTH(bill_due) = $month AND YEAR(bill_due) = $year AND bin = 0 AND is_deductible = 1;" );


		$total_expense_no_vat = $ret_deductible['total_no_vat'] + $ret_no_deductible['total'];
		$total_expense        = $ret_deductible['total'] + $ret_no_deductible['total'];
		$total_irpf        = $ret_deductible['total_irpf'] + $ret_no_deductible['total_irpf'];
		$vat_expense = $ret_deductible['total_vat'];

		$earning = $total_no_vat - $total_expense_no_vat;
		$vat_to_pay = $vat - $vat_expense;

		InvoiceCommon::format_currentcys( $total_no_vat, $total_expense, $vat_to_pay, $total_irpf, $earning );


		$gl_news = "
			<div class=\"missatge-titol\"><strong>Totals</strong></div>
			<p>Total per facturar $month_name: <strong>$total_no_vat €</strong> - $total € IVA inclòs</p>
			<p>Total despeses ( data venciment ): <strong>$total_expense €</strong></p>
			<p>IVA a pagar: <strong>$vat_to_pay €</strong></p>
			<p>IRPF a pagar: <strong>$total_irpf €</strong></p>
			<p>Total benefici: <strong>$earning €</strong></p>
		";

	}
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{
	    $show = new ShowForm($this);
	    return $show->show_form();
	}

	function save_rows()
	{
	    $save_rows = new SaveRows($this);
	    $save_rows->save();
	}

	function write_record()
	{
	    $writerec = new SaveRows($this);
	    $writerec->save();
	}


	//
	// FUNCIONS BUSCADOR
	//
	function search(){

		$q = $this->q = R::escape('q');

		$search_condition = '';

		if ($q)
		{
			// Sempre busco a tot arreu, abans si detectava que eren refrencies no hi  buscava, posaré checkbox de "buscar nomès referencies"

			$search_condition = "(
				expense like '%$q%'
				OR expense_invoice_number like '%$q%'
				" ;

			$search_condition .= ")";
			$GLOBALS['gl_page']->title = TITLE_SEARCH . '<strong>&nbsp;&nbsp;"' . $q . '"</strong>';
		}

		$this->condition = $search_condition;
		$this->do_action('list_records');
	}

	function get_search_form(){
		$this->caption['c_by_ref']='';
		$this->set_vars($this->caption);
		$this->set_file('search.tpl');
		$this->set_var('menu_id',$GLOBALS['gl_menu_id']);
		$this->set_var('q',  htmlspecialchars(R::get('q')));
		return $this->process();
	}

	// Clients
	public function get_select_long_values () {

		InvoiceCommon::get_custumer_select_long_values();

	}
}
?>