<?

/**
 * LassdiveReservaSearch
 *
 * @property array search_vars
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2011
 * @version $Id$
 * @access public
 */
class LassdiveReservaSearch {

	var $search_vars = array(), $search_fields, $caption, $q = '', $is_excel, $view_format, $action, $fields, $reservaitem_date_field_query;

	public function __construct( &$module, $action = '' ) {
		global $gl_caption, $gl_caption_order;
		include( DOCUMENT_ROOT . 'admin/modules/product/languages/' . LANGUAGE . '.php' );
		$this->caption = array_merge( $gl_caption, $gl_caption_order, $module->caption );
		$this->action = $action ? $action : $module->action;
		$this->fields  = $module->fields;
	}

	function get_search_form() {

		$tpl = new phemplate( PATH_TEMPLATES );
		$tpl->set_vars( $this->caption );
		$tpl->set_file( 'lassdive/reserva_search.tpl' );
		$tpl->set_var( 'menu_id', isset($GLOBALS['gl_menu_id'])?$GLOBALS['gl_menu_id']:'' );
		$tpl->set_var( 'action', $this->action == 'list_records_bin'?'list_records_bin':'list_records' );
		$tpl->set_var( 'is_bin', $this->action == 'list_records_bin' );
		$tpl->set_var( 'q', htmlspecialchars( R::get( 'q' ) ) );

		$tpl->set_vars( $this->search_vars );

		$ret = $tpl->process();

		if ( $this->q ) {
			$GLOBALS['gl_page']->javascript .= "
				$('table.listRecord').highlight('" . addslashes( R::get( 'q' ) ) . "');
			";
		}

		return $ret;
	}

	function get_timetable_search_form( $timetable_date ) {

		$tpl = new phemplate( PATH_TEMPLATES );
		$tpl->set_vars( $this->caption );
		$tpl->set_file( 'lassdive/timetable_search.tpl' );
		$tpl->set_var( 'menu_id', isset($GLOBALS['gl_menu_id'])?$GLOBALS['gl_menu_id']:'' );
		$tpl->set_var( 'timetable_date', format_date_form($timetable_date) );

		$tpl->set_vars( $this->search_vars );

		$ret = $tpl->process();

		return $ret;
	}


	function init_search_advanced() {

		$this->is_excel    = R::get( 'excel' );
		$this->view_format = R::escape( 'view_format', 'all' );

		$this->set_search_advanced_vars();
	}


	function set_search_advanced_vars() {

		$c = &$this->caption;

		// Variables formulari
		$date_from       = R::escape( 'date_from' );
		$date_to         = R::escape( 'date_to' );
		$order_status    = R::escape( 'order_status', 'null' );
		$date_search     = R::escape( 'date_search', 'orderitems' ); // lassdive
		$is_done         = R::escape( 'is_done', '' ); // lassdive
		$source          = R::escape( 'source' ); // lassdive
		$view_format     = $this->view_format;
		$user_id         = R::escape( 'user_id' ); // lassdive
		$user_id_add     = htmlspecialchars( R::get( 'user_id_add' ) ); // lassdive
		$centre_id       = R::escape( 'centre_id' ); // lassdive
		$centre_id_add   = htmlspecialchars( R::get( 'centre_id_add' ) ); // lassdive
		$product_id      = R::escape( 'product_id' ); // Utilitzo product_id perque no puc fer servir orderitem_id
		$product_id_add  = htmlspecialchars( R::get( 'product_id_add' ) ); // Per l'autocomplete
		$payment_method = R::escape( 'payment_method', 'null' );
		$operator_id = R::escape( 'operator_id', 'null' );
		$has_no_timetable = R::escape( 'has_no_timetable' );
		$advanced_search = R::get( 'advanced' ) || R::get( 'excel' );

		// Si he triat mostrar product_ids, només es pot buscar la data a product_id
		$this->$view_format = $view_format;
		if ( $view_format == 'orderitems' ) {
			$date_search = 'orderitems';
		}
		elseif ( $view_format == 'order' ) {
			$date_search = 'order';
		}

		$date_search_select    = get_radios( 'date_search', 'order,orderitems', $date_search, array(), $c );
		$c_is_done = $c;
		$c_is_done['c_is_done_0'] = $c['c_is_done_search_0'];
		$c_is_done['c_is_done_1'] = $c['c_is_done_search_1'];
		$is_done_select        = get_radios( 'is_done', ',0,1', $is_done, array(), $c_is_done );
		$source_select         = get_radios( 'source', ',web,centre', $source, array(), $c );
		$order_status_select   = $this->get_filter_status( $order_status, false, $c['c_caption_filter_order_status'] );
		$input_selected_fields = R::get( 'fields', array() );
		R::escape_array( $input_selected_fields );

		// Cookie
		if ( ! $input_selected_fields && isset( $_COOKIE['lassdive']['selected_fields'] ) ) {
			$input_selected_fields = unserialize( $_COOKIE['lassdive']['selected_fields'] );
		}
		setcookie( "lassdive[selected_fields]", serialize( $input_selected_fields ), time() + ( 10 * 365 * 24 * 60 * 60 ), '/' ); // 30 dies

		$c['c_caption_payment_method'] = ' ';
		$payment_method_fields         = $this->fields['deposit_payment_method']['select_fields'];
		$payment_method_select         = get_select( 'payment_method', $payment_method_fields, $payment_method, [ 'class' => 'alone' ], $c );

		$operator_id_select = $this->get_filter_operator( $operator_id );

		$has_no_timetable_select     = get_radios( 'has_no_timetable', ',no_time,no_complement', $has_no_timetable, array(), $c );


		$vars = array(
			'date_from'               => $date_from,
			'date_to'                 => $date_to,
			'is_done'                 => $is_done,
			'is_done_select'          => $is_done_select,
			'source'                  => $source,
			'source_select'           => $source_select,
			'date_search'             => $date_search,
			'date_search_select'      => $date_search_select,
			'view_format'             => $view_format,
			'order_status'            => $order_status,
			'order_status_select'     => $order_status_select,
			'payment_method'          => $payment_method,
			'payment_method_select'   => $payment_method_select,
			'operator_id'             => $operator_id,
			'operator_id_select'      => $operator_id_select,
			'has_no_timetable'        => $has_no_timetable,
			'has_no_timetable_select' => $has_no_timetable_select,
			'user_id'                 => $user_id, //lassdive
			'user_id_add'             => $user_id_add, //lassdive
			'centre_id'               => $centre_id, //lassdive
			'centre_id_add'           => $centre_id_add, //lassdive
			'product_id'              => $product_id,
			'product_id_add'          => $product_id_add,
		);

		$this->search_vars = $vars;

	}


	function init_timetable_search() {

		// Variables formulari
		$user_id         = R::escape( 'user_id' );
		$user_id_add     = htmlspecialchars( R::get( 'user_id_add' ) );
		$centre_id       = R::escape( 'centre_id' );
		$centre_id_add   = htmlspecialchars( R::get( 'centre_id_add' ) );
		$product_id      = R::escape( 'product_id' );
		$product_id_add  = htmlspecialchars( R::get( 'product_id_add' ) ); // Per l'autocomplete
		$view   = R::escape( 'view', 'month' );

		$vars = array(
			'user_id'             => $user_id,
			'user_id_add'         => $user_id_add,
			'centre_id'           => $centre_id,
			'centre_id_add'       => $centre_id_add,
			'product_id'          => $product_id,
			'product_id_add'      => $product_id_add,
			'view'      => $this->get_timetable_view('month'),
		);

		$this->search_vars = $vars;

	}

	private function get_timetable_view( $view ) {
		$views = ['month', 'week'];
		$html = '';
		foreach ( $views as $v ) {
			$selected = $v == $view ? ' selected' : '';
			$html .= "<option $selected value=\"$v\">{$this->caption['c_view_'.$v]}</option>";
		}
		return "<select id='view' name='view'>$html</select>";
	}

	private function get_filter_status( $status_value = '', $id = false, $caption = '' ) {

		if ( $id === false ) {
			$status = '
			<select class="alone" name="order_status">';
		}
		else {
			$status = '
			<select id="order_status_' . $id . '" name="order_status[' . $id . ']">';

		}
		if ( $caption ) {
			$status .= '<option value="null">' . $caption . '</option>';
		}

		$status .= '
				<option class="outlined" value="grup-not-payed">' . $this->caption['c_order_status_group_not_payed'] . '</option>
				<option class="outlined" value="grup-payed">' . $this->caption['c_order_status_group_payed'] . '</option>
				<option class="outlined" value="grup-not-payed-deposit">' . $this->caption['c_order_status_group_not_payed_deposit'] . '</option>
				<option class="outlined" value="grup-payed-deposit">' . $this->caption['c_order_status_group_payed_deposit'] . '</option>
				<optgroup label="' . $this->caption['c_order_status_group_not_payed'] . '">
				<option value="paying">' . $this->caption['c_order_status_paying'] . '</option>
				<option value="failed">' . $this->caption['c_order_status_failed'] . '</option>
				<option value="pending">' . $this->caption['c_order_status_pending'] . '</option>
				<option value="voided">' . $this->caption['c_order_status_voided'] . '</option>
				<option value="refunded">' . $this->caption['c_order_status_refunded'] . '</option>
				<optgroup label="' . $this->caption['c_order_status_group_payed'] . '">
				<option value="completed">' . $this->caption['c_order_status_completed'] . '</option>
				<option value="preparing">' . $this->caption['c_order_status_preparing'] . '</option>
				<option value="shipped">' . $this->caption['c_order_status_shipped'] . '</option>
				<option value="delivered">' . $this->caption['c_order_status_delivered'] . '</option>
			</select>';

		if ( $status_value )
			$status = str_replace( 'value="' . $status_value . '"', 'value="' . $status_value . '" selected', $status );

		return $status;
	}

	private function get_filter_operator( $operator_value ) {

		$c = &$this->caption;

		$results = Db::get("SELECT
							  operator_id,
							  operator
							FROM
							  lassdive__operator");

		$options = '';
		foreach ( $results as $rs ) {
			$operator_id = $rs['operator_id'];
			$operator = $rs['operator'];
			$options .= "<option value=\"$operator_id\">$operator</option>";
		}

		$ret = "
			<select class=\"alone\" name=\"operator_id\">
				<option value=\"null\">${c['c_operator_select_caption']}</option>
				<option value=\"operator\">${c['c_operator_select_operator']}</option>
				<option value=\"no-operator\">${c['c_operator_select_no_operator']}</option>
				$options
			</select>
			";

		if ( $operator_value )
			$ret = str_replace( 'value="' . $operator_value . '"', 'value="' . $operator_value . '" selected', $ret );

		return $ret;
	}

	public function get_search_condition() {

		$search_condition             = '';
		$search_condition_order_items = '';
		$and                          = '';

		$search_condition .= $this->get_search_condition_product_id( $and );
		$search_condition .= $this->get_search_condition_user_id( $and );
		$search_condition .= $this->get_search_condition_in_orderitem( $and, 'centre_id' );
		$search_condition .= $this->get_search_condition_order_status( $and );

		$search_condition .= $this->get_search_condition_payment_method( $and );
		$search_condition .= $this->get_search_condition_operator( $and );
		$search_condition .= $this->get_search_condition_has_no_timetable( $and );

		$search_condition .= $this->get_search_condition_dates( $and );
		$search_condition .= $this->get_search_condition_in_orderitem( $and, 'is_done' );
		$search_condition .= $this->get_search_condition_source( $and );

		$search_condition .= $this->get_search_words_condition( $and );

		if ($this->view_format == 'orderitems'){
			$search_condition .= "
                $and (SELECT product__order.bin FROM product__order WHERE product__order.order_id = product__orderitem.order_id) = 0";
		}

		return $search_condition;
	}
/*
	public function get_timetable_search_condition() {

		$search_condition             = '';
		$and                          = '';

		$search_condition .= $this->get_search_condition_product_id( $and );
		$search_condition .= $this->get_search_condition_user_id( $and );
		$search_condition .= $this->get_search_condition_in_orderitem( $and, 'centre_id' );

		return $search_condition;
	}*/

	private function get_search_condition_dates( &$and ) {

		$date_from   = $this->search_vars['date_from'];
		$date_to     = $this->search_vars['date_to'];
		$date_search = $this->search_vars['date_search'];

		if ( ! $date_from && ! $date_to ) return '';

		$search_in_orderitems = false;
		$condition            = '';
		$field                = 'entered_order';

		$old_and = $and;

		if ( $date_search == 'orderitems' ) {
			if ( $this->view_format == 'all' ) $and = '';
			$search_in_orderitems = true;
			$field                = 'reservaitem_date';
			$this->reservaitem_date_field_query = "
				 (
				    SELECT reservaitem_date 
				    FROM product__orderitem, lassdive__orderitem
                    WHERE product__orderitem.order_id = product__order.order_id 
					AND product__orderitem.orderitem_id = lassdive__orderitem.orderitem_id 
					ORDER BY reservaitem_date DESC 
					LIMIT 1
				 ) AS reservaitem_date
			";
		}

		if ( $date_from && $date_to ) {

			$condition .= $and . "
					(
						(
							date($field) >= '" . unformat_date( $date_from ) . "' AND
							date($field) <= '" . unformat_date( $date_to ) . "'
						)
					)";
			$and       = ' AND ';

		}
		elseif ( $date_from ) {

			$condition .= $and . "
				(
						date($field) >= '" . unformat_date( $date_from ) . "'
				 )";
			$and       = ' AND ';
		}

		elseif ( $date_to ) {

			$condition .= $and . "
				(
						date($field) <= '" . unformat_date( $date_to ) . "'
						AND 
						UNIX_TIMESTAMP($field) > 0
				 ) ";
			$and       = ' AND ';
		}

		// A cerca a "RESERVA" no busca mai data per activitat, no s'entendria si ho fes ja que no es llista
		if ( $search_in_orderitems && $this->view_format == 'all' ) {
			$and       = $old_and;
			$condition = "$and order_id IN ( SELECT order_id FROM product__orderitem INNER JOIN lassdive__orderitem USING (orderitem_id) WHERE $condition )";
			$and       = ' AND ';
		}
		else {
			$this->reservaitem_date_field_query = '';
		}

		return $condition;

	}

	private function get_search_condition_order_status( &$and ) {

		$order_status = $this->search_vars['order_status'];
		if ( $order_status == 'null' ) return '';

		$old_and = $and;
		if ( $this->view_format == 'orderitems' ) $and = '';

		$condition = '';

		if ( $order_status == 'grup-payed' ) {
			$condition .= $and . "order_status IN ('completed', 'preparing', 'shipped', 'delivered')";
		}
		elseif ( $order_status == 'grup-not-payed' ) {
			$condition .= $and . "order_status IN ('paying', 'failed', 'pending', 'denied', 'voided', 'refunded')";

		}
		// Només busco el depòsit pagat en els que no s'ha pagat la resta
		elseif ( $order_status == 'grup-payed-deposit' ) {
			$condition .= $and . "(order_status IN ('paying', 'failed', 'deposit', 'pending', 'denied', 'voided', 'refunded') AND deposit_status IN ('completed')) AND deposit <> 0";
		}
		// Només busco el depòsit per pagar en els que depòsit no és 0
		elseif ( $order_status == 'grup-not-payed-deposit' ) {
			$condition .= $and . "deposit_status IN ('paying', 'failed', 'pending', 'denied', 'voided', 'refunded') AND deposit <> 0";

		}
		else {
			$condition .= $and . "order_status = '$order_status'";
		}
		$and = ' AND ';

		if ( $this->view_format == 'orderitems' ) {
			$and       = $old_and;
			$condition = "$and order_id IN ( SELECT order_id FROM product__order WHERE $condition )";
			$and = ' AND ';
		}


		return $condition;

	}

	private function get_search_condition_payment_method( &$and ) {

		$value = $this->search_vars['payment_method'];
		if ( $value == 'null' ) return '';

		$condition = "(payment_method = '$value' OR deposit_payment_method = '$value')";

		if ( $this->view_format == 'orderitems' ) {
			$condition = "order_id IN ( SELECT order_id FROM product__order WHERE $condition )";
		}

		$condition = "$and $condition";

		$and = ' AND ';


		return $condition;

	}

	private function get_search_condition_operator( &$and ) {

		$value = $this->search_vars['operator_id'];
		if ( $value == 'null' ) return '';

		if ( $value == 'operator'){
			$condition = "operator_id > 0";
		}
		elseif ( $value == 'no-operator'){
			$condition = "operator_id = 0";
		}
		else {
			$condition = "operator_id = '$value'";
		}

		if ( $this->view_format == 'orderitems' ) {
			$condition = "order_id IN ( SELECT order_id FROM product__order WHERE $condition )";
		}

		$condition = "$and $condition";

		$and = ' AND ';


		return $condition;

	}

	private function get_search_condition_has_no_timetable( &$and ) {

		$value = $this->search_vars['has_no_timetable'];
		if ( ! $value ) return '';

		$condition = "(reservaitem_date = '0000-00-00' OR start_time = '00:00:00' OR end_time = '00:00:00' OR centre_id= '0')";

		if ( $value == 'no_complement' ) {
			$condition = "( $condition
			 AND 
			 ( product_id IN 
		        ( 
					SELECT product_id FROM product__product WHERE family_id IN 
					( 
						SELECT family_id FROM product__family WHERE has_calendar = 1
					)
				)
		    ))";
		}

		if ( $this->view_format != 'orderitems' ) {
			$condition = "order_id IN ( SELECT order_id FROM product__orderitem INNER JOIN lassdive__orderitem USING (orderitem_id) WHERE $condition )";
		}

		$condition = "$and $condition";

		$and = ' AND ';


		return $condition;

	}

	private function get_search_condition_user_id( &$and ) {

		$user_id = $this->search_vars['user_id'];
		if ( ! $user_id ) return '';


		$condition = "reservaitem_id IN (SELECT reservaitem_id FROM lassdive__reservaitem_to_user WHERE user_id = '$user_id')";

		if ( $this->view_format != 'orderitems' ) {
			$condition = "order_id IN ( 
							SELECT order_id
					        FROM
					          product__orderitem
					        INNER JOIN lassdive__orderitem
					        USING (orderitem_id)
					        WHERE $condition)";
		}

		$condition = "$and $condition";

		$and = ' AND ';


		return $condition;

	}

	private function get_search_condition_product_id( &$and ) {

		$product_id = $this->search_vars['product_id'];
		if ( ! $product_id ) return '';

		$condition = "product_id = '$product_id'";

		if ( substr( $product_id, 0, 7 ) == 'family-' ) {
			$family_id = substr( $product_id, 7 );
			$condition = "product_id IN ( SELECT product_id FROM product__product WHERE family_id = '$family_id' OR subfamily_id = '$family_id')";
		}

		if ( $this->view_format != 'orderitems' ) {
			$condition = "order_id IN ( SELECT order_id FROM product__orderitem WHERE $condition )";
		}

		$condition = "$and $condition";

		$and = ' AND ';

		return $condition;

	}

	private function get_search_condition_source( &$and ) {

		$value = $this->search_vars['source'];
		if ( ! $value ) return '';

		if ( $value == 'centre' ) {
			$condition = "source_id > 0";
		}
		else {
			$condition = "source_id = 0";
		}

		if ( $this->view_format == 'orderitems' ) {
			$condition = "order_id IN ( SELECT order_id FROM product__order WHERE $condition )";
		}

		$condition = "$and $condition";

		$and = ' AND ';


		return $condition;

	}

	private function get_search_condition_in_orderitem( &$and, $field ) {

		$value = $this->search_vars[ $field ];
		if ( $value == '' || $value === false ) return '';


		$condition = "$field = '$value'";

		if ( $this->view_format != 'orderitems' ) {
			$condition = "order_id IN ( SELECT order_id FROM product__orderitem INNER JOIN lassdive__orderitem USING (orderitem_id) WHERE $condition )";
		}

		$condition = "$and $condition";

		$and = ' AND ';


		return $condition;

	}

/*	private function get_search_condition_in_order( &$and, $field ) {

		$value = $this->search_vars[ $field ];
		if ( $value == '' || $value === false ) return '';


		$condition = "$field = '$value'";

		if ( $this->view_format == 'orderitems' ) {
			$condition = "order_id IN ( SELECT order_id FROM product__order WHERE $condition )";
		}

		$condition = "$and $condition";

		$and = ' AND ';


		return $condition;

	}*/


	//
	// BUSCADOR PER PARAULES
	//
	function get_search_words_condition( &$and ) {
		/*

		ORDER
		0 - comment, entered_order, customer_comment
		1 - customer_id
		payment_method ha de ser un desplegable
		2 -source_id
		3- id

		*/
		$is_orderitem = $this->view_format == 'orderitems';

		$q = R::escape( 'q' );
		if ( ! $q ) return '';

		$condition = "
				comment like '%" . $q . "%'
				OR entered_order like '%" . $q . "%'
				OR customer_comment like '%" . $q . "%'
				";



		// busco a clients
		$query = "
					SELECT order_id
						FROM product__customer
						INNER JOIN product__order USING (customer_id)
						WHERE (
							name LIKE '%" . $q . "%'
							OR surname LIKE '%" . $q . "%'
							OR CONCAT(TRIM(name), ' ', TRIM(surname)) LIKE '%" . $q . "%'
							OR mail LIKE '%" . $q . "%'
					)
					AND (
						product__customer.bin = 0
					)
			";

		// Debug::p($query);

		$results = Db::get_rows_array( $query );

		$condition .= $this->get_ids_query( $results, 'order_id' );

		// busco a centres
		$query = "
					SELECT order_id
						FROM lassdive__centre, product__order
						WHERE lassdive__centre.centre_id = product__order.source_id
						AND (
							centre LIKE '%" . $q . "%'
							)
			";

		// Debug::p($query);

		$results = Db::get_rows_array( $query );

		$condition .= $this->get_ids_query( $results, 'order_id' );


		// Per referencia
		$condition = $this->get_ref_query( 'order_id', $q, $condition );

		if ( $is_orderitem ) {
			$condition = "order_id IN (SELECT order_id FROM product__order WHERE $condition)";
		}



		/*

		ORDERITEM
		*/
		$condition .= $this->get_search_words_condition_orderitem( $is_orderitem, $q );



		$condition = "$and ($condition)";
		$and = ' AND ';
		return $condition;

	}
	private function get_search_words_condition_orderitem( $is_orderitem, $q ) {


		$family_subquery = '
			SELECT product__family_language.family_id FROM product__family_language 
			WHERE family';



		$condition = "
			SELECT product__product.product_id as product_id
			FROM product__product, product__product_language
			WHERE
				        (
				            product__product.product_id = product__product_language.product_id
				            AND 
				            bin = 0
			            ) 
				        AND
				        (
				            product_title LIKE '%$q%'
							OR product_subtitle LIKE '%$q%'
							OR ref LIKE '%$q%'
							OR family_id IN ($family_subquery LIKE '%$q%')
							OR subfamily_id IN ($family_subquery LIKE '%$q%')
						)
		";

		$condition = "
			product_id IN ( $condition )
			OR product_variations LIKE '%$q%'
			OR product_variation_categorys LIKE '%$q%'
			";
		if ( !$is_orderitem ) {
			$condition = "order_id IN (SELECT order_id FROM product__orderitem WHERE $condition)";
		}

		$condition = "OR $condition";

		return $condition;
	}

	function get_ids_query( &$results, $field ) {
		if ( ! $results ) return '';
		$new_arr = array();
		foreach ( $results as $rs ) {
			$new_arr [ $rs[0] ] = $rs[0];
		}

		return "OR " . $field . " IN (" . implode( ',', $new_arr ) . ") ";
	}

	function get_ref_query( $field, $q, $condition ) {
		//
		// busqueda automatica de referencies separades per espais o comes
		//
		$ref_array = explode( ",", trim($q) ); // si l'array hem dona 1 probo de fer-ho amb espais, si es una sola referència donarà el mateix resultat
		count( $ref_array ) == 1 ? $ref_array = explode( " ", trim($q) ) : false;

		foreach ( $ref_array as $key => $value ) {
			if ( ! is_numeric( $value ) ) {
				$ref_array = '';
				break;
			}
		}

		// consulta
		if ( $ref_array ) {
			$query_ref = "";
			foreach ( $ref_array as $key ) {
				$query_ref .= $field . " = '" . $key . "' OR ";
			}
			$query_ref = "(" . substr( $query_ref, 0, - 3 ) . ") ";

			return '( ' . $query_ref . ' OR ' . $condition . ')';
		}
		else {
			return $condition;
		}
	}

}