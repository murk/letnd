<?
/**
 * RacingRally - Blunik
 *
 * Ralli te categoria d'imatges per que surt a la home
 * Suport és l'únic que te categories, els altres tenen el camp i prou ( estan desactivades )
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2017
 * @version $Id$
 * @access public
 */
class RacingRally extends Module{

	function __construct(){
		parent::__construct();
	}
	function on_load(){

		$this->form_tabs = array('0' => array('tab_action'=>'show_form_map','tab_caption'=>$this->caption['c_edit_map']));

		parent::on_load();
		// social
		// Main::load_class( 'social' );
		/*
		$this->form_tabs =
			array(
				'0' =>
					array(
						'tab_action'  => 'show_form_social&amp;menu_id=' . $GLOBALS['gl_menu_id'],
						'tab_caption' => $this->caption['c_edit_social']
					)
			);
		*/
/*

		if ($this->config['has_files_show_home'] == '1')
			$this->set_field( 'file', 'has_show_home', '1' );*/

	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function get_records()
	{
	    $listing = new ListRecords($this);

		$listing->add_button ('edit_map', 'action=show_form_map','boto1','after');
		
		
		$listing->call('records_walk','content,content_list', true);
		//$listing->add_button ('edit_content_button', 'action=show_form_copy', 'boto1');


		$listing->add_filter('status');
		
		$listing->add_filter('in_home');
		$listing->add_filter('destacat');

		// Social::init_listing($listing);
		
		if (isset($_GET['in_home']) && $_GET['in_home']!='null') {
			$listing->set_field('in_home','list_admin','input');
		}
		if (isset($_GET['destacat']) && $_GET['destacat']!='null') {
			$listing->set_field('destacat','list_admin','input');
		}
		
		$listing->order_by = 'entered DESC, rally, rally_id';
	    return $listing->list_records();
	}
	function records_walk(&$listing, $content=false,$content_list=false){		

		
		// si es llistat passa la variable content
		if ($content!==false){
			$ret['content']=$content_list?$content_list:add_dots('content', $content);
		}

		// $this->set_buttons_social($listing);
		
		return $ret;
	}
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{

		$this->set_field('entered','default_value',now(true));
		$this->set_field('modified','default_value',now(true));
		
		if ($this->config['videoframe_info_size']) $this->caption['c_videoframe'] = $this->caption['c_videoframe'] . '<br /> ( ' . $this->config['videoframe_info_size'] . ' )';
		$show = new ShowForm($this);
		$show->form_tabs = $this->form_tabs;
		$show->set_form_level1_titles('page_title');

		//activo o desactivo el seo
		if (!$_SESSION['show_seo']) {
			$show->unset_field('page_title');	
			$show->unset_field('page_description');
			$show->unset_field('page_keywords');
			$show->unset_field('new_file_name');		    
			$show->unset_field('new_old_file_name');
		}
	    return $show->show_form();
	}

	function save_rows()
	{
	    $save_rows = new SaveRows($this);
	    $save_rows->save();
	}

	function write_record()
	{
	    $writerec = new SaveRows($this);
		$writerec->set_field('modified','override_save_value','now()');			
		$writerec->field_unique = "new_file_name";
		$writerec->save();
	}

	function manage_images()
	{
	    $image_manager = new ImageManager($this);
		$image_manager->form_tabs = $this->form_tabs;
	    $image_manager->execute();
	}
	
	/////////////
	// SOCIAL
	///////////////
	function show_form_social() {
		$id = R::id($this->id_field);
		$language = Social::get_language();

		$query = "
			SELECT 
				rally, 
				subtitle, 
				content_list,
				content,
				rally_file_name as file_name
			FROM " . $this->table . "_language 
			WHERE " . $this->id_field . "='" . $id . "'
			AND language = '" . $language . "'";
		$rs = Db::get_row($query);
		
		if (!$rs) return;

		$link = HOST_URL . $language . '/blunik/rally/' . $id . '.htm';
		
		$facebook_params = array(
			'message' => '',
			'name' => htmlspecialchars($rs['rally']),
			'caption' => htmlspecialchars($rs['subtitle']),
			'description' => htmlspecialchars( p2nl($rs['content_list']?$rs['content_list']:$rs['content']))
		);
		$twitter_params = array(
			'status' => htmlspecialchars($rs['rally'])
		);
		
		$social = new Social($this, $id, $facebook_params, $twitter_params, $link);
		$GLOBALS['gl_content'] = $social->get_form();
	}
	function write_record_social() {
		$social = new Social($this);		
		$social->publish();
	}
	function set_buttons_social(&$listing) {
		
		Social::set_buttons_listing($listing);
		
	}


	/*
	 * MAPES
	 *
	 */

	function show_form_map (){ //esenyo el napa
		global $gl_news;

		$rs = Db::get_row('SELECT * FROM racing__rally WHERE rally_id = ' . $_GET['rally_id']);
		$rs['title'] = Db::get_first('SELECT rally FROM racing__rally_language  WHERE rally_id = ' . $_GET['rally_id'] . ' AND language = LANGUAGE');
		$rs['is_default_point'] = ($rs['latitude']!=0)?0:1;
		$rs['center_latitude']=$rs['center_latitude']!=0?$rs['center_latitude']:GOOGLE_CENTER_LATITUDE;
		$rs['center_longitude']=$rs['center_longitude']!=0?$rs['center_longitude']:GOOGLE_CENTER_LONGITUDE;
		$rs['zoom']=$rs['zoom']?$rs['zoom']:GOOGLE_ZOOM;
		$rs['menu_id'] =  $_GET['menu_id'];
		$rs['id'] =  $rs[$this->id_field];
		$rs['id_field'] =  $this->id_field;
		$this->set_vars($rs);//passo els resultats del array
		$this->set_file('racing/rally_map.tpl'); //crido el arxiu
		$this->set_vars($this->caption); //passo tots els captions
		// $gl_news = $this->caption['c_click_map'];
		$content = $this->process();//processo
		$GLOBALS['gl_content'] = $content;
	}
	function save_record_map() {
		global $gl_page;
		extract( $_GET );
		Debug::p( $_GET );

		if ( $latitude && $longitude ) {

			$query = "UPDATE `racing__rally` SET
				`latitude` = '$latitude',
				`longitude` = '$longitude',
				`center_latitude` = '$center_latitude',
				`center_longitude` = '$center_longitude',
				`zoom` = '$zoom'
				WHERE `rally_id` =$rally_id
				LIMIT 1";
			//Debug::p( $query );
			Db::execute ($query) ;
			$ret ['message'] = $this->messages['point_saved'];
		}
		else {
			$ret ['message'] = $this->messages['point_not_saved'];
		}

		$gl_page->show_message($this->messages['point_saved']);

	}
	function save_rows_map_points(){
		global $gl_page;
		$rally_id = $_GET['rally_id'];
		$points = trim($_POST['points']);
		$points = explode(';',$points);

		$latitudes = array();
		$longitudes = array();
		$error = false;

		foreach ($points as $point){
			if ($point){
				$p = explode(',',trim($point));

				$la =trim($p[0]);
				$lo = trim($p[1]);

				if (is_numeric($la) && is_numeric($lo)
				    && $la!='' && $lo!=''
				    && count($p)==2) {
					$latitudes[] =$la;
					$longitudes[] = $lo;
				}
				else{
					$error=true;
					break;
				}
			}
		}
		if ($error){
			$this->map_points_error();
		}

		$center_latitude = $latitudes[0]; // centro en el primer punt
		$center_longitude = $longitudes[0]; // centro en el primer punt

		$latitudes = implode (';',$latitudes);
		$longitudes = implode (';',$longitudes);

		debug::p($latitudes);
		debug::p($longitudes);

		if ($latitudes && $longitudes){
			Db::execute ("UPDATE `racing__rally` SET
				`latitude` = '$latitudes',
				`longitude` = '$longitudes',
				`center_latitude` = '$center_latitude',
				`center_longitude` = '$center_longitude'
				WHERE `rally_id` =$rally_id
				LIMIT 1") ;

			//$gl_page->show_message($this->messages['point_saved']);
			$_SESSION['message'] = $this->messages['point_saved'];
			Debug::p_all();
			print_javascript('parent.window.location.replace(parent.window.location);');
			die();
		}
		else{
			$this->map_points_error();
		}
	}
	function map_points_error(){
		global $gl_page;
		$gl_page->show_message('Hi ha un error en l\'entrada de coordenades, si us plau comproveu que estigui ben formada');
		Debug::p_all();
		die();
	}

}
?>