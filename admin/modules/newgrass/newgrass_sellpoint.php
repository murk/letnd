<?
/**
 * NewgrassSellpoint
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2011
 * @version $Id$
 * @access public
 */
class NewgrassSellpoint extends Module{

	var $condition = '';
	var $q = false;
	var $form_tabs;

	function __construct(){
		parent::__construct();
	}
	function on_load(){
		$this->form_tabs = array('0' => array('tab_action'=>'show_form_map&amp;tool=newgrass&amp;tool_section=sellpoint','tab_caption'=>$this->caption['c_edit_map']));	
	}
	function list_records()
	{
		$content = $this->get_records();
		/*
		if ($this->q) {
			$GLOBALS['gl_page']->javascript .= "
				$('table.listRecord').highlight('".addslashes(R::get('q'))."');			
			";
		}
		$GLOBALS['gl_content'] = $this->get_search_form() . $content;*/
		$GLOBALS['gl_content'] = $content;
	}
	function get_records()
	{		
	
		$listing = new ListRecords($this);	
		$listing->add_button ('edit_map', 'action=show_form_map','boto1','after');
		
		// search
		if ($this->condition) {
			$listing->condition = $this->condition;
		}
		
		//$listing->set_options (1, 1, 1);		
		if ($this->action!='list_records_bin') {
			$listing->add_filter('provincia_id');
		}
		
		$listing->order_by = 'empresa ASC';
		
	    return $listing->list_records();
	}
	function show_form()
	{
		$GLOBALS['gl_content'] = 
		$this->get_form();
	}
	function get_form()
	{
	    $show = new ShowForm($this);
		$show->form_tabs = $this->form_tabs;
		
		//$show->call_function['comarca_id'] = "get_select_comarques";
		//$show->call_function['municipi_id'] = "get_select_municipis"; // comentat autocomplete
		//$query_provincies = "provincia_id='" . str_replace(",", "' OR provincia_id='", NEWGRASS_PROVINCIES). "'";
		//$show->fields["provincia_id"]['select_condition'] = $query_provincies;	
		
		
	    return $show->show_form() .
		'<script language="JavaScript" src="/admin/modules/newgrass/jscripts/chained_selects.js?v='.$GLOBALS['gl_version'].'"></script>';
	}

	function save_rows()
	{		
	    $save_rows = new SaveRows($this);
	    $save_rows->save();
	}

	function write_record()
	{
	    $writerec = new SaveRows($this);
		$writerec->save();
	}

	function manage_images()
	{
	    $image_manager = new ImageManager($this);
		$image_manager->form_tabs = $this->form_tabs;
	    $image_manager->execute();
	}
	/*
	//
	// FUNCIONS BUSCADOR
	//
	function search(){

		$q = $this->q = R::escape('q');
		
		$search_condition = '';

		if ($q)
		{
			// Sempre busco a tot arreu, abans si detectava que eren refrencies no hi  buscava, posaré checkbox de "buscar nomès referencies"

			$search_condition = "(
				nom like '%" . $q . "%'
				OR cognoms like '%" . $q . "%'
				OR CONCAT(nom, ' ', cognoms) like '%" . $q . "%'
				OR adresa like '%" . $q . "%'
				OR cp like '%" . $q . "%'
				OR telefon like '%" . $q . "%'
				OR telefon2 like '%" . $q . "%'
				OR fax like '%" . $q . "%'
				OR correu like '%" . $q . "%'
				OR url1 like '%" . $q . "%'
				OR agent_description like '%" . $q . "%'
				" ;

			// busco a agencia
			$query = "SELECT agencia_id FROM newgrass__agencia WHERE (
								 agencia like '%" . $q . "%'
									)" ;
			$results = Db::get_rows_array($query);

			$search_condition .= $this->get_ids_query($results, 'agencia_id');
			$search_condition .= ") " ;
			$search_condition = $this->get_ref_query('sellpoint_id',$q, $search_condition);

			$GLOBALS['gl_page']->title = TITLE_SEARCH . '<strong>&nbsp;&nbsp;"' . $q . '"</strong>';
		}
		$this->condition = $search_condition;
		Debug::add('Search condition', $this->condition);
		$this->do_action('list_records');
	}

	function get_ids_query(&$results, $field)
	{
		if (!$results) return '';
		$new_arr = array();
		foreach ($results as $rs){
			$new_arr [$rs[0]] = $rs[0];
		}
		return "OR " . $field . " IN (" . implode(',', $new_arr) . ") ";
	}
	function get_ref_query($field, $q, $search_condition){
		//
		// busqueda automatica de referencies separades per espais o comes
		//
		$ref_array = explode (",", $q); // si l'array hem dona 1 probo de fer-ho amb espais, si es una sola referncia donarà el mateix resultat
		count($ref_array) == 1?$ref_array = explode (" ", $q):false;

		foreach($ref_array as $key => $value)
		{
			if (!is_numeric($value))
			{
				$ref_array = '';
				break;
			}
		}

		// consulta
		if ($ref_array)
		{
			$query_ref = "";
			foreach ($ref_array as $key)
			{
				$query_ref .= $field . " = '" . $key . "' OR ";
			}
			$query_ref = "(" . substr($query_ref, 0, -3) . ") ";

			return '( ' . $query_ref . ' OR ' . $search_condition . ')';
		}
		else{
			return $search_condition;
		}
	}

	function get_search_form(){
		$this->caption['c_by_ref']='';
		$this->set_vars($this->caption);
		$this->set_file('search.tpl');
		$this->set_var('menu_id',$GLOBALS['gl_menu_id']);
		return $this->process();
	}
	*/
	//
	// FUNCIONS MAPA
	//	
	function show_form_map (){ //esenyo el napa
		global $gl_news;
		
		$sellpoint_id = R::id('sellpoint_id');
		$menu_id = R::id('menu_id');
		
		$rs = Db::get_row('SELECT adress, poblacio, cp, provincia_id, empresa, latitude, longitude, zoom, sellpoint_id FROM newgrass__sellpoint WHERE sellpoint_id = ' . $sellpoint_id);
		
		$rs['provincia']=Db::get_first("SELECT name FROM all__provincia  WHERE provincia_id = " . $rs['provincia_id'] . "");
		
		$rs['title'] = 		
			$rs['empresa'];
			
		$rs['address'] =  $rs['adress'] . ', ' . $rs['poblacio'] . ', ' . $rs['cp'] . ', ' . $rs['provincia'];
		
		
		$rs['is_default_point'] = ($rs['latitude']!=0)?0:1;
		$rs['latitude']=$rs['latitude']!=0?$rs['latitude']:GOOGLE_CENTER_LATITUDE;
		$rs['longitude']=$rs['longitude']!=0?$rs['longitude']:GOOGLE_CENTER_LONGITUDE;
		$rs['zoom']=$rs['zoom']?$rs['zoom']:GOOGLE_ZOOM;
		$rs['menu_id'] =  $menu_id;		
		$rs['tool'] =  $this->tool;		
		$rs['tool_section'] =  $this->tool_section;		
		$rs['menu_id'] =  $menu_id;		
		$rs['id'] =  $rs[$this->id_field];
		$rs['id_field'] =  $this->id_field;
		$this->set_vars($rs);//passo els resultats del array
		$this->set_file('newgrass/sellpoint_map.tpl'); //crido el arxiu
		$this->set_vars($this->caption); //passo tots els captions
		$gl_news = $this->caption['c_click_map'];
		$content = $this->process();//processo
		$GLOBALS['gl_content'] = $content;		
	}
	function save_record_map(){
		global $gl_page;
		extract($_GET);
		
		Db::execute ("UPDATE `newgrass__sellpoint` SET
			`latitude` = '$latitude',
			`longitude` = '$longitude',
			`zoom` = '$zoom'
			WHERE `sellpoint_id` =$sellpoint_id
			LIMIT 1") ;
		$gl_page->show_message($this->messages['point_saved']);
		Debug::p_all();
		die();			
	}
}

// fora la clase
function get_select_comarques($key, $value)
{
	$query_provincies = "provincia_id='" . str_replace(",", "'
									OR provincia_id='", API_PROVINCIES) . "'
									ORDER BY comarca";
	return get_select_sub($value, $key, "inmo__comarca", "comarca, provincia_id, comarca_id", $query_provincies, '');
}
function get_select_municipis($key, $value)
{
	$query_comarques = "(inmo__municipi.comarca_id = inmo__comarca.comarca_id)
									AND (provincia_id='" . str_replace(",", "'
									OR provincia_id='", API_PROVINCIES) . "')
									ORDER BY municipi";
	return get_select_sub($value, $key, "inmo__municipi, inmo__comarca", "municipi, inmo__comarca.comarca_id, municipi_id", $query_comarques, '');
}

function get_select_sub($value, $name, $table, $fields, $condition = '', $caption = '', $onchange = '', $disabled = '')
{
	$html = '';
	$results = get_results_sub($table, $fields, $condition);

	if ($caption)
		$html .= '<option value="0">' . $caption . '</option>';
	if ($results)
	{
		foreach ($results as $rs)
		{
			$selected = '';
			if ($rs[2] == $value)
				$selected = " selected";
			$html .= '<option value="' . $rs[2] . '"' . $selected . '>#' . $rs[1] . "#" . $rs[0] . '</option>';
		}
	}
	if ($onchange)
		$onchange = ' onChange="' . $this->onchange . '"';
	if ($disabled)
		$disabled = ' disabled';
	$html = '<select name="' . $name . '"' . $onchange . $disabled . '>' . $html . '</select>';
	Debug::add("Get select", $html);
	return $html . '<input name="old_'.$name.'" type="hidden" value="0" />';
}

function get_results_sub($table, $fields, $condition)
{

	$query = "SELECT " . $fields . " FROM " . $table;
	if ($condition)
		$query .= " WHERE " . $condition;
	Debug::add("Consulta form objects", $query);
	$results = Db::get_rows_array($query);
	return $results;
}
?>