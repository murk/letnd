<form method="post" action="<?= $form_link ?>" name="list" onsubmit="return check_selected(this)" target="save_frame">
	<input name="action" type="hidden" value="save_rows_specs">
	<div class="form">
		<div id="form_buttons_holder">
			<table id="form_buttons">
				<tr>
					<td valign="bottom">
						<div class="menu">
							<a href="<?= $data_link ?>"><?= $c_edit_data ?></a>
							<a href="<?= $image_link ?>"><?= $c_edit_images ?></a>
							<? foreach ( $form_tabs as $r ): extract( $r ) ?>
								<? if ( $tab_action ): ?>
									<a href="?action=<?= $tab_action ?>&<?= $id_field ?>=<?= $id ?>&menu_id=<?= $menu_id ?>"><?= $tab_caption ?></a>
								<? else: // ?>
									<div><?= $tab_caption ?></div>
								<? endif // $tab_action ?>
							<? endforeach //$form_tabs?>
						</div>
					</td>
					<td class="buttons_top">
						<div class="right">
							<input onclick="submit_form_save('save_rows_specs')" type="button" name="Button" value="<?= $c_send_edit ?>" class="btn_form_save"/>
						</div>
					</td>
				</tr>
			</table>
		</div>
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td>
								<table width="100%" cellpadding="4" cellspacing="0">
									<tr>
										<td class="formCaptionTitle"><strong>
												<?= $c_data_specs ?>&nbsp;&nbsp;</strong><?= $product_form_title ?></td>
									</tr>
								</table>
								<table width="100%" cellpadding="4" cellspacing="0" class="formstbl">
									<tr>
										<td align="center" valign="top" class="forms">
											<table width="99%" border="0" cellspacing="0" cellpadding="0">
												<tr>
													<td align="left">
														<? if ( $loop ): ?>

															<table border="0" cellspacing="0" cellpadding="0" class="listRecord">
																<? foreach ( $loop as $l ): extract( $l ) ?>
																	<tr>
																		<td colspan="6" class="listTitle2" style="padding-bottom:20px;padding-top: 40px;">
																			<strong><?= $spec_category ?></strong></td>
																	</tr>

																	<? if ( $specs ): ?>


																		<? /* <thead>
 																		<tr>
 																			<td valign="top" nowrap="nowrap" class="listCaption">
 																				<strong><?= $c_spec ?></strong>
 																				&nbsp;</td>
 																			<td valign="top" class="listCaption">
 																				&nbsp;</td>
 																		</tr>
 																		</thead> */ ?>

																		<? foreach ( $specs as $l ): extract( $l ) ?>
																			<tr class="listItem<?= $odd_even ?>" onmousedown="change_bg_color(this, 'Down', '<?= $spec_id ?>')" id="list_row_<?= $spec_id ?>">

																				<td valign="top">
																					<table width="100%" border="0" cellpadding="0" cellspacing="0">
																						<tr>
																							<td width="100%"><?= $spec_name ?></td>
																						</tr>
																					</table>
																				</td>
																				<td valign="top">
																					<table width="100%" border="0" cellpadding="0" cellspacing="0">
																						<? if ( $spec ): ?>
																							<tr>
																								<td width="100%" align="right"><?= $spec ?></td>
																							</tr>

																						<? endif // $spec ?>

																						<? if ( $language_specs ): ?>
																							<? foreach ( $language_specs as $l ): extract( $l ) ?>
																								<tr>
																									<th class="langListCaption"><?= $lang ?>:</th>
																									<td style="padding-left:20px;padding-bottom:20px;" width="100%"><?= $language_spec ?></td>
																								</tr>

																							<? endforeach //$language_specs?>
																						<? endif // $language_specs ?>

																					</table>
																				</td>
																				<td valign="top">
																					<table width="100%" border="0" cellpadding="0" cellspacing="0">
																						<tr>
																							<td>&nbsp;</td>
																							<td width="100%"><?= $spec_unit ?></td>
																						</tr>
																					</table>
																				</td>
																			</tr>

																		<? endforeach //specs?>

																	<? endif //specs?>

																<? endforeach //$loop?>
															</table>
														<? endif //loop?></td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<table width="100%" cellpadding="4" cellspacing="2" class="formsButtonstbl">
			<tr>
				<td align="center" class="formsButtons">
					<div class="centerFloat1">
						<div class="centerFloat2">
							<input onclick="submit_form_save('save_rows_specs')" type="button" name="Button" value="<?= $c_send_edit ?>" class="btn_form_save"/>
						</div>
					</div>
				</td>
			</tr>
		</table>
	</div></form>