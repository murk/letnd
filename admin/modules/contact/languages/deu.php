<?php
// menus eina
define('CONTACT_MENU_CONTACT','Kontakt');
define('CONTACT_MENU_LIST','Kontakt-E-Mail-Adressen');

// captions seccio
$gl_caption_contact ['c_company'] = 'Unternehmen';
$gl_caption_contact ['c_name'] = 'Name';
$gl_caption_contact ['c_surnames'] = 'Familienname';
$gl_caption_contact ['c_mail'] = 'E-Mail';
$gl_caption_contact ['c_to_address'] = 'To';
$gl_caption_contact ['c_phone'] = 'Telefon';
$gl_caption_contact ['c_matter'] = 'Betriff';
$gl_caption_contact ['c_comment'] = 'Bemerkungen';
$gl_caption_contact ['c_accept_info'] = 'Allow send information';
$gl_caption_contact_list ['c_accept_info'] = 'Allow info';
$gl_caption ['c_sendmail'] = 'E-Mail verschicken';
$gl_messages ['added'] = 'Die E-Mail wurde ordnungsgemäß verschickt.';
$gl_caption ['c_sendok'] = $gl_messages ['added'];
$gl_messages ['not_saved '] = 'Die E-Mail konnte nicht verschickt werden.';

$gl_caption_contact['c_nif']='ID/MwST Nr.';
$gl_caption_contact['c_company']='Firma';

$gl_caption_contact['c_contact_language']='Contact page language';

$gl_caption_contact['c_export_button']= 'Exportar';
?>