<?php
/* 22-1-19
Afegir camp is_family_private a les families

*/


if ( is_table( 'product__family' ) && ! is_field( 'product__family', 'is_family_private' ) ) {

	$update_sql[] = "ALTER TABLE `product__family`
	ADD COLUMN `pass_code` TEXT(50) NOT NULL DEFAULT '' AFTER `has_calendar`,
	ADD COLUMN `is_family_private` TINYINT(1) NOT NULL DEFAULT '0' AFTER `has_calendar`;
";

} else {
	if ( is_table( 'product__family' ) ) {
		trigger_error( 'Ja existeixen el camp is_family_private' );
	}
}

if ( is_table( 'product__configadmin' ) && ! Db::get_first( "SELECT * FROM product__configadmin WHERE name='has_private_familys'" ) ) {
	$update_sql[] = "
		INSERT INTO product__configadmin (`name`, `value`) VALUES ('has_private_familys', '0');";

}
else {
	if ( is_table( 'product__configadmin' ) ) {
		trigger_error( 'Ja existeixen el camp has_private_familys' );
	}
}