<?php
// no es poden posar ; entre mig de la consulta ja que parteix les consultes per el ; a no ser que fem servir $update_sql en array()
	
if (!Db::get_first('SELECT count(*) FROM all__menu WHERE `menu_id` =4010')){
	$update_sql .="
	INSERT INTO `all__menu` 
	(`menu_id`, `menu_group`, `tool`, `tool_section`, `action`, `parent_id`, `toolmode_id`, `variable`, `ordre`, `link`, `process`, `bin`, `read1`, `write1`) VALUES 
	(4010, 'web', 'web', 'banner', '', 4000, 1, 'WEB_MENU_BANNER', 3, '', '', 0, ',1,', ',1,');
	INSERT INTO `all__menu` 
	(`menu_id`, `menu_group`, `tool`, `tool_section`, `action`, `parent_id`, `toolmode_id`, `variable`, `ordre`, `link`, `process`, `bin`, `read1`, `write1`) VALUES 
	(4011, 'web', 'web', 'banner', 'show_form_new', 4010, 1, 'WEB_MENU_BANNER_NEW', 1, '', '', 0, ',1,', ',1,');
	INSERT INTO `all__menu` 
	(`menu_id`, `menu_group`, `tool`, `tool_section`, `action`, `parent_id`, `toolmode_id`, `variable`, `ordre`, `link`, `process`, `bin`, `read1`, `write1`) VALUES 
	(4012, 'web', 'web', 'banner', 'list_records', 4010, 1, 'WEB_MENU_BANNER_LIST', 2, '', '', 0, ',1,', ',1,');
	INSERT INTO `all__menu` 
	(`menu_id`, `menu_group`, `tool`, `tool_section`, `action`, `parent_id`, `toolmode_id`, `variable`, `ordre`, `link`, `process`, `bin`, `read1`, `write1`) VALUES 
	(4013, 'web', 'web', 'banner', 'list_records_bin', 4010, 1, 'WEB_MENU_BANNER_LIST_BIN', 3, '', '', 0, ',1,', ',1,');
	";
}

if (!is_table('web__banner')){
	$update_sql .="
CREATE TABLE IF NOT EXISTS `web__banner` (
  `banner_id` int(11) NOT NULL auto_increment,
  `url1_target` enum('_blank','_self') collate utf8_spanish2_ci NOT NULL default '_blank',
  `ordre` int(11) NOT NULL default '0',
  `status` enum('prepare','review','public','archived') collate utf8_spanish2_ci NOT NULL default 'prepare',
  `bin` int(1) NOT NULL default '0',
  PRIMARY KEY  (`banner_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

CREATE TABLE IF NOT EXISTS `web__banner_file` (
  `file_id` int(11) NOT NULL auto_increment,
  `banner_id` int(11) NOT NULL default '0',
  `language` varchar(3) collate utf8_spanish2_ci NOT NULL default '',
  `name` varchar(100) collate utf8_spanish2_ci NOT NULL default '',
  `size` int(11) NOT NULL default '0',
  `ordre` int(11) NOT NULL default '0',
  `main_file` int(1) NOT NULL default '0',
  PRIMARY KEY  (`file_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

CREATE TABLE `web__banner_language` (
	`banner_id` INT(11) NOT NULL DEFAULT '0',
	`language` VARCHAR(3) NOT NULL DEFAULT '' COLLATE 'utf8_spanish2_ci',
	`url1` VARCHAR(250) NOT NULL DEFAULT '' COLLATE 'utf8_spanish2_ci',
	`url1_name` VARCHAR(100) NOT NULL DEFAULT '' COLLATE 'utf8_spanish2_ci',
	`banner_text` VARCHAR(250) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;
	
CREATE TABLE IF NOT EXISTS `web__configadmin_banner` (
  `configadmin_id` int(11) NOT NULL auto_increment,
  `name` varchar(50) collate utf8_spanish2_ci NOT NULL default '',
  `value` varchar(255) collate utf8_spanish2_ci NOT NULL default '',
  `language` char(3) collate utf8_spanish2_ci NOT NULL default '',
  PRIMARY KEY  (`configadmin_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci ROW_FORMAT=DYNAMIC;

INSERT INTO `web__configadmin_banner` (`configadmin_id`, `name`, `value`) VALUES
	(1, 'banner_info_size', '');";
}

if (is_table('newsletter__group_language')){
	$update_sql .="
ALTER TABLE `newsletter__group_language`
	ENGINE=MyISAM;";
}

$client_dir = DOCUMENT_ROOT . '/' . $_SESSION['client_dir'];
$dir=$client_dir . '/web/';

if (!is_dir($dir)){	
	
	// creo el directori per guardar les imatges del banner
	if (!is_dir($dir)){
		mkdir ($dir, 0776);
	}
	$dir=$client_dir . '/web/banner/';
	if (!is_dir($dir)){
		mkdir ($dir, 0776);
	}
	$dir=$client_dir . '/web/banner/files/';
	if (!is_dir($dir)){
		mkdir ($dir, 0776);
	}
}

?>