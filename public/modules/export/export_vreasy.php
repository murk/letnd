<?
/**
 * VREASY - Pasar una reserva de vreasy a letnd
 *
 * Respostes des de Vreasy
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) Gener 2018
 * @version $Id$
 * @access public
 */
class ExportVreasy extends Module {

	public $exporter = 'vreasy';
	public $api_url = 'https://api.vreasy.com', $headers, $mail_message, $api_key;
	public $vreasy_fields = [];
	public $letnd_fields = [];
	public $vreasy_status = [];
	public $letnd_status = [];
	private $agencia_id;
	public $agencia_rs = [];
	private $has_conflict;
	private $can_book;
	private $letnd_book_id;

	public function __construct() {

		include_once( DOCUMENT_ROOT . 'admin/modules/admintotal/export_common.php' );
		include_once( DOCUMENT_ROOT . 'admin/modules/admintotal/admintotal_vreasy_book_common.php' );

		ExportCommon::init_unirest( $this );

		if ( $GLOBALS['gl_is_local'] ) {
			Unirest\Request::verifyPeer(false);
		}

		$this->agencia_id = $_SESSION['client_id'];

		Db::connect_mother();
		$agencia_rs = ExportCommon::get_agencias( 'inmo_vreasy', $this->agencia_id );
		Db::reconnect();

		$this->agencia_rs = $agencia_rs[0];
		$this->get_agencia_config();



		$this->vreasy_fields = AdminVreasyBookCommon::$vreasy_fields;
		$this->letnd_fields = AdminVreasyBookCommon::get_letnd_fields();
		$this->vreasy_status = AdminVreasyBookCommon::$vreasy_status;
		$this->letnd_status = AdminVreasyBookCommon::$letnd_status;

		parent::__construct();

	}
	public function on_load() {
		$this->tool  = 'booking';
		$this->tool_section  = 'book';
		$this->set_language();
		parent::on_load();
	}

	public function response() {

		// Obtinc variables

		$json = file_get_contents( 'php://input' );
		$obj  = json_decode( $json );

		$this->trace( HOST_URL . "\n Variables JSON:\n---------------\n" . print_r( $obj, true ) . "\n\n", '', 'set_notice' );

		// Ho passo al servidor local que faci la feina, així puc utilitzar el debug
		if ( HOST_URL == 'https://www.letnd.com/' && isset($_REQUEST['is_local']) ) {

			$this->trace( "Enviant a local ... \n\n" );
			$response = Unirest\Request::post( 'http://w2.letnd.com:8080/cat/export/vreasy/response_local_debug/?XDEBUG_SESSION_START=PHPSTORM', $this->headers, $json );
			$this->trace( print_r( $response, true ) . "\n\n", 'Response des de local' );
			http_response_code( 200 );
			die();
		}
		else {
			$this->process_response( $obj );
		}
	}

	function response_local_debug() {

		error_reporting( E_ALL );
		ini_set( 'display_errors', true );
		ini_set( 'display_startup_errors', true );

		/*
		 S'ACCEDEIX REMOTAMENT PER:

		 Xdebug: w2.letnd.com:8080/cat/export/vreasy/response_local_debug/?XDEBUG_SESSION_START=PHPSTORM

		 Zend: http://w2.letnd.com:8080/cat/export/vreasy/response_local_debug/?start_debug=1&debug_host=10.0.2.2&debug_port=10137 -> zend no m'obre el debug per culpa dels ports 10.0.2.2

		 TAULA LETND
		  client__client ->
		  easybrava 4domain
		  inmo_vreeasy = 1
		  inmo_vreasy_user = '$api_key'
		*/

		$json = file_get_contents( 'php://input' );
		$obj  = json_decode( $json );

		$this->trace( HOST_URL . "\n Variables JSON:\n---------------\n" . $json . "\n\n", '', 'set_notice' );

		$this->process_response( $obj );
	}

	function process_response( $obj ) {

		$api_key = $this->api_key = $this->check_api_key( $obj );
		$this->auth( $api_key );

		$this->log_json($_SERVER["REQUEST_URI"], '', $obj);

		$e          = $obj->event;
		$event      = $e->event;
		$update_url = $e->update_url;
		$full_update_url = strtok( $update_url, '?' );

		$this->check_changed_by_letnd( $e );
		$this->check_is_property_activation( $e );

		// $date_in, $date_out, $date_in2, $date_out2, $book_id, $property_id, $adult, $children, $baby

		$fields = implode( ',', $this->vreasy_fields );
		// id,checkin,checkout,property_id,adults,kids,accommodation_cost,status
		$full_update_url .= "?fields=$fields,payments";


		if ( $event == 'reservation_update' ) {
			$this->save_reservation( $event, $full_update_url, $e );
		}
		else if ( $event == 'reservation_create' ) {
			$this->save_reservation( $event, $full_update_url, $e );
		}


		http_response_code( 200 );
		die();
	}

	function check_api_key( &$obj ) {

		if ( $obj ) {
			$api_key = $obj->api_key;
			$client_id = $_SESSION['client_id'];

			Db::connect_mother();
			$api_key_stored = Db::get_first( "SELECT inmo_vreasy_user FROM client__client WHERE client_id = $client_id;" );
			Db::reconnect();
		}
		else {
			$api_key = 0;
			$api_key_stored = -1;
		}

		if ( $api_key != $api_key_stored ) {
			http_response_code( 401 );
			die();
		}
		else {
			return $api_key;
		}
	}

	// Quan es guarda des de letnd, també genera un event a la subscripció i torna cap a letnd
	private function check_changed_by_letnd( &$e ) {

		if ( is_null( $e->acting_api ) || $e->acting_agent != 'api' ) return;

		$acting_api = $e->acting_api;
		$client_api          = 0;

		$api_url  = $this->api_url . "/keys";
		$response = Unirest\Request::get( $api_url, $this->headers );


		if ( $response->body ) {
			foreach ( $response->body as $item ) {
				if ( $item->key == $this->api_key ) {
					$client_api = $item->id;
				}
			}
		}

		$body = "<pre>" . print_r( $e, true ) . "\n\n" . print_r( $response->body, true ) . "</pre>";

		if ( $client_api == $acting_api ) {
			$this->trace( print_r( $e, true ), 'Retorn de letnd' );
			send_mail_admintotal( 'Vreasy Retorn Letnd', $body, 'marc@letnd.com' );
			http_response_code( 200 );
			die();
		}
		else {
			$this->trace( print_r( $e, true ), 'Retorn de airb' );
			send_mail_admintotal( 'Vreasy Airnb', $body, 'marc@letnd.com' );

		}
	}

	// Quan s'activa un inmoble, torna a passar totes les reserves, comprobo si no ha canviat res, llavors no faig cap update ni envio cap email de conflicte
	private function check_is_property_activation( &$e ) {

		if (!property_exists($e, 'changeset')) return;
		$change_set = $e->changeset;

		$activation_events = ["auto_email_enabled", "auto_sms_enabled", "auto_task_enabled"];

		// si es qualsevol d'aquests i nomes aquests
		if ( count( $change_set ) <= 3 ) {

			// Si hi ha qualsevol de diferent als tres autos, segueixo amb l'importació
			foreach ( $change_set as $v ) {
				if (!in_array($v, $activation_events)) return;
			}

			$this->trace( print_r( $e, true ), 'Reserva propietat reactivada' );
			http_response_code( 200 );
			die();
		}
	}

	private function save_reservation( $event, $update_url, $e ) {

		$response = Unirest\Request::get( $update_url, $this->headers );

		$this->log_json($update_url, '', $response);

		$reservation = $response->body;
		$create_book = false;

		foreach ( $this->vreasy_fields as $k => $v ) {
			${$k} = $reservation->$v;
		}

		$baby = 0; // No existeix a vreasy


		$reservation->checkin = $date_in = AdminVreasyBookCommon::get_letnd_date($date_in);
		$reservation->checkout = $date_out = AdminVreasyBookCommon::get_letnd_date($date_out);

		$date_in2 = format_date_list($date_in);
		$date_out2 = format_date_list($date_out);


		$query   = "SELECT book_id, property_id, book_owner FROM booking__book WHERE vreasy_id = $book_id";
		$book_rs = Db::get_row( $query );


		// update reservation
		if ($book_rs){
			$book_owner = $book_rs['book_owner'];

			$letnd_book_id = $book_rs['book_id'];
			$letnd_property_id = $book_rs['property_id'];

			$this->letnd_book_id = $letnd_book_id;

			if ( $book_owner == 'letnd' ) {
				$this->send_mail_conflict( $letnd_property_id, $response, $book_id, 'owner' );
				return;
			}
			$create_book = false;

		}
		// create_reservation
		else {
			$letnd_book_id = 0;
			$letnd_property_id = $this->get_letnd_property_id( $property_id );
			$create_book = true;
		}

		$this->is_reserved($date_in, $date_out, $date_in2, $date_out2, $letnd_book_id, $letnd_property_id, $adult, $child, $baby);

		// Si ja hi ha una reserva per les dates que ens passen de vreasy, envio mail de no s'ha pogut reservar
		if (!$this->can_book) {
			$this->send_mail_conflict( $letnd_property_id, $response, $book_id, 'can_not_book' );
			return;
		}


		// Poso els corresponents a letnd
		$reservation->property_id = $letnd_property_id;
		$reservation->status = $this->letnd_status[ $reservation->status ];

		// Si no hi ha conflicte, creo reserva si fa falta, o updatejo
		if ( $create_book ) {
			$this->create_reservation( $reservation, $book_id );
			$this->insert_extras( $reservation ); // extres son les obligatòries, només cal fer-ho al crear el registre
		}
		else {
			$this->update_reservation( $reservation, $letnd_book_id );
		}

		$this->save_prices( $reservation );

		// Si hi ha conflicte, envio mail de conflicte
		if ($this->has_conflict && $reservation->status != 'cancelled') {
			$this->send_mail_conflict( $letnd_property_id, $response, $book_id, 'has_conflict' );
		}

		$this->trace( print_r( $response, true ), $event );
	}

	private function create_reservation( $reservation, $vreasy_id ) {

		$keys = '';
		$values = '';

		foreach ( $this->vreasy_fields as $k => $v ) {
			if ($k != 'book_id'){
				$keys .= "`$k`,";
				$values .= Db::qstr( $reservation->$v ) . ',';
			}
		}
		$keys .= "`book_owner`, `vreasy_id`";
		$values .= "'vreasy', $vreasy_id";

		$query = "
			INSERT INTO booking__book ($keys)
			VALUES ($values)";

		$this->trace( print_r( $reservation, true ), $query );
		Db::execute( $query );

		$this->letnd_book_id = Db::insert_id();
	}

	private function update_reservation( $reservation, $book_id ) {

		$values = '';

		foreach ( $this->vreasy_fields as $k => $v ) {
			// important treure book_id sino em posa l'id de vreasy a book_id
			if ($k != 'book_id') {
				$value  = Db::qstr( $reservation->$v );
				$values .= "`$k` = $value,";
			}
		}
		$values = substr( $values, 0, - 1 );

		$query = "
			UPDATE booking__book 
			SET $values
			WHERE book_id = $book_id";

		$this->trace( print_r( $reservation, true ), $query );
		Db::execute( $query );

	}

	private function save_prices($reservation) {

		$date_in = $reservation->checkin;
		$date_out = $reservation->checkout;
		$book_id = $this->letnd_book_id;
		$property_id = $reservation->property_id;
		$adult = $reservation->adults;
		$children = $reservation->kids;


		// Obtinc preu per guardar amb la reserva, abans de save();
		Main::load_class('booking', 'price', 'admin');
		Main::load_class('booking', 'book', 'public');
		$price_module = New BookingPrice($this);
		$price_module->action = 'add_record'; // sempre recalcula preu

		$prices = $price_module->get_price(
			$date_in,
			$date_out,
			$book_id,
			$property_id,
			true,
			true,
			$adult,
			$children
		);

		$comment_private = "Preu Vreasy: $reservation->accommodation_cost";

		Db::execute( "
				UPDATE booking__book
					SET
					price_book= ${prices['price_book_value']}, 
					price_extras = ${prices['price_extras_value']}, 
					price_tt = ${prices['price_tt_value']}, 
					price_total = ${prices['price_total_value']},
					comment_private = '$comment_private'
					WHERE  
					book_id = '$book_id';
				" );
	}

	private function insert_extras($reservation){

		$book_id = $this->letnd_book_id;
		$property_id = $reservation->property_id;

		$query   = "
			SELECT
					extra_id, extra_price
			FROM booking__extra_to_property
				INNER JOIN booking__extra
				USING (extra_id)
			WHERE property_id = $property_id
			AND extra_type = 'required';";

		$results = Db::get_rows( $query );

		foreach ( $results as $rs ) {
			$extra_id = $rs['extra_id'];
			$extra_price = $rs['extra_price'];

			$query = "
				INSERT INTO booking__extra_to_book
					(book_id, extra_id, extra_price, quantity)
					VALUES ($book_id, $extra_id, $extra_price, 1)";
			Db::execute( $query );

		}

	}

	private function get_letnd_property_id( $vreasy_property_id ) {
		$query = "SELECT property_id FROM inmo__property_to_vreasy 
					WHERE vreasy_id = $vreasy_property_id";
		return Db::get_first( $query );
	}

	private function do_owner_conflict_action( $book_rs, $vreasy_book_id ) {


		$letnd_book_id = $this->letnd_book_id;

		$link = HOST_URL . "/admin/?menu_id=152&book_id=$letnd_book_id";
		$link_vreasy = "https://www.vreasy.com/calendar/#reservation/$vreasy_book_id";

		$this->trace( print_r( $book_rs, true ) . "\n\n", "Conflicte, reserva propietat de 'Letnd'\n" );
	}

	private function send_mail_conflict( $property_id, $response, $vreasy_book_id, $error ) {

		$letnd_book_id = $this->letnd_book_id;

		$link = HOST_URL . "/admin/?menu_id=152&book_id=$letnd_book_id";
		$link_vreasy = "https://www.vreasy.com/calendar/#reservation/$vreasy_book_id";


		$property_private =  Db::get_first("SELECT property_private FROM inmo__property WHERE property_id = $property_id");

		if ($error == 'owner'){
			$subject = 'No s\'ha pogut guardar una reserva des de Vreasy';
			$body = "Reserva feta originalment des de Letnd, no es pot modificar des de Vreasy: <strong>$property_private</strong>
					<br><br>Reserva a letnd: <a href='$link'> núm $letnd_book_id</a>
					<br>Reserva a vreasy: <a href='$link_vreasy'> núm $vreasy_book_id</a>";
		}
		else if ($error == 'has_conflict') {
			$subject = 'Conflicte en reserva des de Vreasy';
			$body = "Hi ha hagut un conflicte en la reserva de la casa: <strong>$property_private</strong>
					<br><br>Reserva a letnd: <a href='$link'> núm $letnd_book_id</a>
					<br>Reserva a vreasy: <a href='$link_vreasy'> núm $vreasy_book_id</a>
					<br><br> La reserva s'ha guardat igualment";
		}
		else {
			$subject = 'No s\'ha pogut guardar una reserva des de Vreasy';
			$body = "No s'ha pogut guardar la reserva de la casa: <strong>$property_private</strong>
					<br><br>Reserva a vreasy: <a href='$link_vreasy'> núm $vreasy_book_id</a>";
		}

		if ( $this->mail_message ) {
			$body .= '<br><br>' . $this->mail_message;
		}

		send_mail_admin( $subject, $body );

		$body .= "<br><br>Response:<pre>" . var_export($response, true) . "</pre>";
		send_mail_admintotal( $subject, $body );

	}

	private function is_reserved( $date_in, $date_out, $date_in2, $date_out2, $book_id, $property_id, $adult, $children, $baby ) {
		Main::load_class( 'booking', 'common', 'admin' );
		$bc = New BookingCommon();
		$bc->messages = &$this->messages;

		$is_reserved = $bc->check_is_reserved(
			$date_in, $date_out, $date_in2, $date_out2, $book_id, $property_id, $adult, $children, $baby, "<br>" );

		if ( $is_reserved ) {
			$this->mail_message = $bc->reserved_message;
		}


		$results = $bc->check_dates_are_reserved($date_in, $date_out, $book_id, $property_id);
		$this->can_book = $results?false:true;
		$this->has_conflict = $is_reserved;

	}


	private function get_agencia_config() {

		ExportCommon::get_agencia_config( $this->agencia_rs, 'vreasy' );

	}

	private function log_json( $api_url, $json_sent, $json_response ) {

		Db::connect_mother();
		ExportCommon::log_json( $api_url, $json_sent, $json_response, $this->agencia_rs, $this->exporter );
		Db::reconnect();

	}


	private function trace( $text, $name = '', $title = false ) {
		ExportCommon::trace( $text, $name, $title, 'vreasy' );
	}

	private function auth( $login, $pass = '' ) {
		Unirest\Request::auth( $login, $pass );
	}
}