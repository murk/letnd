<?

/* 01-7-14 
  Modificacions immo

 */

if (is_table('inmo__property')) {	
	
	$update_sql .= "
		ALTER TABLE `inmo__property`
	ADD COLUMN `expired` DATE NOT NULL DEFAULT '0000-00-00' AFTER `status`;
		";
	
	$update_sql .= "
		ALTER TABLE `inmo__property`
	ADD COLUMN `construction_date` DATE NOT NULL DEFAULT '0000-00-00' AFTER `inscriptionregister`;
		";
		
	$update_sql .= "
		ALTER TABLE `inmo__property`
	ADD COLUMN `facing` ENUM('','n','ne','e','se','s','so','o','no') NOT NULL DEFAULT '' AFTER `person`;
		";
	
	$update_sql .= "
		ALTER TABLE `inmo__property`
	ADD COLUMN `exclusive` TINYINT(1) NOT NULL DEFAULT '0' AFTER `observations`;
		";
	
	$update_sql .= "
		ALTER TABLE `inmo__property`
	ADD COLUMN `zip` VARCHAR(20) NOT NULL DEFAULT '' AFTER `door`;
		";
	
	$update_sql .= "
		ALTER TABLE `inmo__property`
	ADD COLUMN `descriptionregister` TEXT NOT NULL DEFAULT '' AFTER `inscriptionregister`;
		";
	
	if (!is_table('custumer__demand_to_category')){
		
		
		$update_sql .= "
			CREATE TABLE `custumer__demand_to_category` (
				`demand_category_id` INT(11) NOT NULL AUTO_INCREMENT,
				`demand_id` INT(11) NULL DEFAULT '0',
				`category_id` INT(11) NULL DEFAULT '0',
				PRIMARY KEY (`demand_category_id`),
				INDEX `demand_id` (`demand_id`),
				INDEX `category_id` (`category_id`)
			)
			COLLATE='utf8_spanish2_ci'
			ENGINE=MyISAM;";
		$update_sql .= "
			INSERT into custumer__demand_to_category (demand_id,category_id) SELECT demand_id,category_id FROM custumer__demand;";
	}
	
}

if (is_table('user__user')) {	
	
	$update_sql .= "
		ALTER TABLE `user__user`
			ADD COLUMN `observations` TEXT(255) NOT NULL DEFAULT '' AFTER `group_id`;
		";
	$update_sql .= "
		ALTER TABLE `user__user`
			ADD COLUMN `phone1` VARCHAR(50) NOT NULL DEFAULT '' AFTER `email`,
			ADD COLUMN `phone2` VARCHAR(50) NOT NULL DEFAULT '' AFTER `phone1`,
			ADD COLUMN `phone3` VARCHAR(50) NOT NULL DEFAULT '' AFTER `phone2`,
			ADD COLUMN `phone4` VARCHAR(50) NOT NULL DEFAULT '' AFTER `phone3`;
		";	
}

if (is_table('custumer__custumer')) {	
	
	$update_sql .= "
		ALTER TABLE `custumer__custumer`
	ADD COLUMN `marital_status` ENUM('','married','divorced','separated','single','widower','commonlaw') NOT NULL DEFAULT '' AFTER `treatment`;
		";
}

if (is_table('custumer__history')) {	
	
	$update_sql .= "
		ALTER TABLE `custumer__history`
	CHANGE COLUMN `status` `status` ENUM('sort_out','finished','canceled') NOT NULL COLLATE 'utf8_spanish2_ci' AFTER `date`;
		";
	
	$update_sql .= "
		ALTER TABLE `custumer__history`
			CHANGE COLUMN `date` `date` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00' AFTER `custumer_id`,
			CHANGE COLUMN `finished_date` `finished_date` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00' AFTER `status`;
		";
	$update_sql .= "
		ALTER TABLE `custumer__history`
	CHANGE COLUMN `tipus` `tipus` ENUM('various','to_call','waiting_call','assembly','show_property','like_property','send_information','demand_refused','demand_reseted','office','offer') NOT NULL COLLATE 'utf8_spanish2_ci' AFTER `finished_date`;
		";
}

if (is_table('inmo__category') && !Db::get_first('SELECT count(*) FROM inmo__category WHERE category_id = 22')){

$update_sql .= "INSERT INTO `inmo__category` (`category_id`, `ref_code`, `ordre`, `blocked`, `active`) VALUES (22, '', 0, 0, 1);";
$update_sql .= "INSERT INTO `inmo__category` (`category_id`, `ref_code`, `ordre`, `blocked`, `active`) VALUES (23, '', 0, 0, 1);";
$update_sql .= "INSERT INTO `inmo__category` (`category_id`, `ref_code`, `ordre`, `blocked`, `active`) VALUES (24, '', 0, 0, 1);";
$update_sql .= "INSERT INTO `inmo__category` (`category_id`, `ref_code`, `ordre`, `blocked`, `active`) VALUES (25, '', 0, 0, 1);";
$update_sql .= "INSERT INTO `inmo__category` (`category_id`, `ref_code`, `ordre`, `blocked`, `active`) VALUES (26, '', 0, 0, 1);";
$update_sql .= "INSERT INTO `inmo__category` (`category_id`, `ref_code`, `ordre`, `blocked`, `active`) VALUES (27, '', 0, 0, 1);";




$update_sql .= "INSERT INTO `inmo__category_language` (`category_id`, `language`, `category_original`, `category`) VALUES (22, 'cat', 'Casa adosada', 'Casa adosada');";
$update_sql .= "INSERT INTO `inmo__category_language` (`category_id`, `language`, `category_original`, `category`) VALUES (22, 'deu', 'Reihenhaus', 'Reihenhaus');";
$update_sql .= "INSERT INTO `inmo__category_language` (`category_id`, `language`, `category_original`, `category`) VALUES (22, 'eng', 'Serial house', 'Serial house');";
$update_sql .= "INSERT INTO `inmo__category_language` (`category_id`, `language`, `category_original`, `category`) VALUES (22, 'fra', 'Maison mitoyenne', 'Maison mitoyenne');";
$update_sql .= "INSERT INTO `inmo__category_language` (`category_id`, `language`, `category_original`, `category`) VALUES (22, 'spa', 'Casa adosada', 'Casa adosada');";
$update_sql .= "INSERT INTO `inmo__category_language` (`category_id`, `language`, `category_original`, `category`) VALUES (23, 'cat', 'Casa aparellada', 'Casa aparellada');";
$update_sql .= "INSERT INTO `inmo__category_language` (`category_id`, `language`, `category_original`, `category`) VALUES (23, 'deu', 'Zweifamilienhaus', 'Zweifamilienhaus');";
$update_sql .= "INSERT INTO `inmo__category_language` (`category_id`, `language`, `category_original`, `category`) VALUES (23, 'eng', 'Single family house', 'Single family house');";
$update_sql .= "INSERT INTO `inmo__category_language` (`category_id`, `language`, `category_original`, `category`) VALUES (23, 'fra', 'Maison jumelée', 'Maison jumelée');";
$update_sql .= "INSERT INTO `inmo__category_language` (`category_id`, `language`, `category_original`, `category`) VALUES (23, 'spa', 'Casa pareada', 'Casa pareada');";
$update_sql .= "INSERT INTO `inmo__category_language` (`category_id`, `language`, `category_original`, `category`) VALUES (24, 'cat', 'Casa unifamiliar', 'Casa unifamiliar');";
$update_sql .= "INSERT INTO `inmo__category_language` (`category_id`, `language`, `category_original`, `category`) VALUES (24, 'deu', '', '');";
$update_sql .= "INSERT INTO `inmo__category_language` (`category_id`, `language`, `category_original`, `category`) VALUES (24, 'eng', '', '');";
$update_sql .= "INSERT INTO `inmo__category_language` (`category_id`, `language`, `category_original`, `category`) VALUES (24, 'fra', '', '');";
$update_sql .= "INSERT INTO `inmo__category_language` (`category_id`, `language`, `category_original`, `category`) VALUES (24, 'spa', 'Casa unifamiliar', 'Casa unifamiliar');";
$update_sql .= "INSERT INTO `inmo__category_language` (`category_id`, `language`, `category_original`, `category`) VALUES (25, 'cat', 'Pis planta baixa', 'Pis planta baixa');";
$update_sql .= "INSERT INTO `inmo__category_language` (`category_id`, `language`, `category_original`, `category`) VALUES (25, 'deu', 'Apartment Erdgeschoss', 'Apartment Erdgeschoss');";
$update_sql .= "INSERT INTO `inmo__category_language` (`category_id`, `language`, `category_original`, `category`) VALUES (25, 'eng', 'Ground floor apartment ', 'Ground floor apartment ');";
$update_sql .= "INSERT INTO `inmo__category_language` (`category_id`, `language`, `category_original`, `category`) VALUES (25, 'fra', 'Appartement rez-de-chaussée', 'Appartement rez-de-chaussée');";
$update_sql .= "INSERT INTO `inmo__category_language` (`category_id`, `language`, `category_original`, `category`) VALUES (25, 'spa', 'Piso planta baja', 'Piso planta baja');";
$update_sql .= "INSERT INTO `inmo__category_language` (`category_id`, `language`, `category_original`, `category`) VALUES (26, 'cat', 'Garatge', 'Garatge');";
$update_sql .= "INSERT INTO `inmo__category_language` (`category_id`, `language`, `category_original`, `category`) VALUES (26, 'deu', 'Garage', 'Garage');";
$update_sql .= "INSERT INTO `inmo__category_language` (`category_id`, `language`, `category_original`, `category`) VALUES (26, 'eng', 'Garage', 'Garage');";
$update_sql .= "INSERT INTO `inmo__category_language` (`category_id`, `language`, `category_original`, `category`) VALUES (26, 'fra', 'Garage', 'Garage');";
$update_sql .= "INSERT INTO `inmo__category_language` (`category_id`, `language`, `category_original`, `category`) VALUES (26, 'spa', 'Garaje', 'Garaje');";
$update_sql .= "INSERT INTO `inmo__category_language` (`category_id`, `language`, `category_original`, `category`) VALUES (27, 'cat', 'Edifici', 'Edifici');";
$update_sql .= "INSERT INTO `inmo__category_language` (`category_id`, `language`, `category_original`, `category`) VALUES (27, 'deu', 'Gebäude', 'Gebäude');";
$update_sql .= "INSERT INTO `inmo__category_language` (`category_id`, `language`, `category_original`, `category`) VALUES (27, 'eng', 'Building', 'Building');";
$update_sql .= "INSERT INTO `inmo__category_language` (`category_id`, `language`, `category_original`, `category`) VALUES (27, 'fra', 'Édifice', 'Édifice');";
$update_sql .= "INSERT INTO `inmo__category_language` (`category_id`, `language`, `category_original`, `category`) VALUES (27, 'spa', 'Edificio', 'Edificio');";


}
?>