<?php

/*
03-06-2019
Contacte

*/

if ( is_table( 'contact__configpublic' ) && ! Db::get_first( "SELECT * FROM contact__configpublic WHERE name='send_mail_copy'" ) ) {
	$update_sql[] = "
		INSERT INTO contact__configpublic (`name`, `value`) VALUES ('send_mail_copy', '0');";
	$update_sql[] = "
		INSERT INTO contact__configpublic (`name`, `value`) VALUES ('use_mail_template', '0');";

}
else {
	if ( is_table( 'contact__configpublic' ) ) {
		trigger_error( 'Ja existeixen el camp send_mail_copy' );
	}
}

if ( is_table( 'client__configpublic' ) && ! Db::get_first( "SELECT * FROM client__configpublic WHERE name='missatge_autoresposta_email'" ) ) {
	$update_sql[] = "
		INSERT INTO client__configpublic (`name`, `value`, `page_text_id`) VALUES ('missatge_autoresposta_email', '', 'contact');";

	global $gl_languages;

	foreach ( $gl_languages['names'] as $lang => $val ) {
		$update_sql[] = "
			INSERT INTO `client__configpublic_language` (`name`, `language`)
			VALUES ('missatge_autoresposta_email','$lang');";
	}

}
else {
	if ( is_table( 'client__configpublic' ) ) {
		trigger_error( 'Ja existeixen el camp missatge_autoresposta_email' );
	}
}

$update_sql[] = "UPDATE `all__menu` SET `ordre`='12' WHERE  `menu_id`=4110;";
$update_sql[] = "UPDATE `all__menu` SET `ordre`='13' WHERE  `menu_id`=4111;";
$update_sql[] = "UPDATE `all__menu` SET `ordre`='20' WHERE  `menu_id`=4015;";