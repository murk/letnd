<section class="product-visited">

	<h5><?=$title?></h5>
	
	<div id="product-visited-slider">

		<?foreach($loop as $l): extract ($l)?>

			<? /* TODO-i Canviar el 3 del conta pel número de items-1 que es mostren a l'slider. Si no hi ha slider borrar-ho */ ?>
			<article class="item<?= $conta > 3 ? ' hidden' : '' ?>" onclick="javascript:window.location.href='<?=$show_record_link?>'">

				<?if($image_name):?>
					<figure>
						<a href="<?= $show_record_link ?>" title="<?= htmlspecialchars( $product_title ) ?>">
							<img <?= get_image_attrs( $image_src_thumb, htmlspecialchars( $product_title ), 70, 70 ) ?> />
						</a>
					</figure>
				<?endif //image_name?>
					
				<div class="text">

					<div class="ref">
						<?= $ref ?>
					</div>
				
					<? if ($product_title):?>
						<h6><a href="<?=$show_record_link?>"><?=$product_title?></a></h6>
					<?endif?>
					
					<?if($can_buy):?>
						<div class="price">
							<?=$pvp?> <?=CURRENCY_NAME?>
						</div> 
					<?endif //can_buy?>

				</div>
				
			</article>

		<?endforeach //$loop?>

	</div>

	<? if($more): ?>
		<div class="center"><a title="<?= htmlspecialchars( $more ) ?>" href="<?= $more_link ?>" class="more-info"><?= $more ?></a>
		</div>
	<? endif //$more?>
	
</section>