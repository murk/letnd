<?php
// menus eina
if (!defined('LASSDIVE_MENU_ACTIVITAT')){
	define('LASSDIVE_MENU_ACTIVITAT', 'Activitats');
	define('LASSDIVE_MENU_ACTIVITAT_NEW', 'Entrar nova activitat');
	define('LASSDIVE_MENU_ACTIVITAT_LIST', 'Llistar i editar activitats');
	define('LASSDIVE_MENU_ACTIVITAT_LIST_PACKS', 'Llistar especials');
	define('LASSDIVE_MENU_ACTIVITAT_BIN', 'Paperera de reciclatge');
	define('LASSDIVE_MENU_IDIOMA', 'Idioma');
	define('LASSDIVE_MENU_IDIOMA_NEW', 'Entrar nou idioma');
	define('LASSDIVE_MENU_IDIOMA_LIST', 'Llistar idiomes');

	define('LASSDIVE_MENU_LEVEL', 'Nivells');
	define('LASSDIVE_MENU_LEVEL_NEW', 'Entrar nou nivell');
	define('LASSDIVE_MENU_LEVEL_LIST', 'Llistar nivells');

	define('LASSDIVE_MENU_MODALITY', 'Modalitats');
	define('LASSDIVE_MENU_MODALITY_NEW', 'Entrar nova modalitat');
	define('LASSDIVE_MENU_MODALITY_LIST', 'Llistar modalitats');

	define('LASSDIVE_MENU_ELEMENT','Inclòs / no inclòs');
	define('LASSDIVE_MENU_ELEMENT_NEW','Nou ítem');
	define('LASSDIVE_MENU_ELEMENT_LIST','Llistar ítems');
	
	define('LASSDIVE_MENU_PRICE', 'Preus');
	define('LASSDIVE_MENU_PRICE_NEW', 'Entrar nou preu');
	define('LASSDIVE_MENU_PRICE_LIST', 'Llistar preus');
	define('LASSDIVE_MENU_PRICE_BIN', 'Paperera de reciclatge');

	define('LASSDIVE_MENU_CATEGORY', 'Categories');
	define('LASSDIVE_MENU_CATEGORY_NEW', 'Entrar nova categoria');
	define('LASSDIVE_MENU_CATEGORY_LIST', 'Llistar categories');

	define('LASSDIVE_MENU_FAMILY', 'Calendari famílies');

	define('LASSDIVE_MENU_RESERVA', 'Reserves');
	define('LASSDIVE_MENU_RESERVA_LIST', 'Llistar reserves');
	define('LASSDIVE_MENU_RESERVA_BIN', 'Paperera de reciclatge');

	define('LASSDIVE_MENU_CENTRE', 'Centres');
	define('LASSDIVE_MENU_CENTRE_NEW', 'Entrar nou centre');
	define('LASSDIVE_MENU_CENTRE_LIST', 'Llistar centres');
	define('LASSDIVE_MENU_ASSIGNCENTRE', 'Assignar productes');

	define('LASSDIVE_MENU_OPERATOR', 'Operador');
	define('LASSDIVE_MENU_OPERATOR_NEW', 'Entrar nou operador');
	define('LASSDIVE_MENU_OPERATOR_LIST', 'Llistar operadors');
}

$gl_messages_activitat['no_category']='Per poder insertar notícies hi ha d\'haver almenys una categoria';

// captions seccio
$gl_caption_activitat['c_see_activities'] = 'Llistar activitats';
$gl_caption_activitat['c_edit_calendar'] = 'Editar calendari';
$gl_caption_activitat['c_see_products'] = 'Llistar productes';
$gl_caption_activitat['c_activitat_title'] = 'Títol';
$gl_caption_activitat['c_parent_id'] = 'Pertany a';
$gl_caption_activitat['c_prepare'] = 'En preparació';
$gl_caption_activitat['c_review'] = 'Per revisar';
$gl_caption_activitat['c_public'] = 'Públic';
$gl_caption_activitat['c_archived'] = 'Arxivat';
$gl_caption_activitat['c_title'] = 'Title';
$gl_caption_activitat['c_subtitle'] = 'Subtitle';
$gl_caption_activitat['c_content'] = 'Description';
$gl_caption_activitat['c_entered'] = 'Created';
$gl_caption_activitat['c_modified'] = 'Modified';
$gl_caption_activitat['c_start_date'] = 'Initial date';
$gl_caption_activitat['c_end_date'] = 'Final date';
$gl_caption_activitat['c_minimum_age'] = 'Minimum age';
$gl_caption_activitat['c_maximum_person'] = 'Maximum people';
$gl_caption_activitat['c_status'] = 'Estat';
$gl_caption_activitat['c_category_id'] = 'Category';
$gl_caption_activitat['c_filter_category_id'] = 'Totes les categories';
$gl_caption_activitat['c_filter_status'] = 'Tots els estats';
$gl_caption_activitat['c_filter_in_home'] = 'Totes, home o no';
$gl_caption_activitat['c_filter_parent_id'] = 'Totes les categories';
$gl_caption_activitat['c_ordre'] = 'Order';
$gl_caption_activitat['c_weekday_id'] = 'Days';
$gl_caption_activitat['c_price'] = 'Price';
$gl_caption_activitat['c_price2'] = 'Price 2';
$gl_caption_activitat['c_price3'] = 'Price 3';
$gl_caption_activitat['c_price4'] = 'Price 4';
$gl_caption_activitat['c_price5'] = 'Price 5';
$gl_caption_activitat['c_price6'] = 'Price 6';
$gl_caption_activitat['c_price7'] = 'Price 7';
$gl_caption_activitat['c_price8'] = 'Price 8';
$gl_caption_activitat['c_price9'] = 'Price 9';
$gl_caption_activitat['c_price10'] = 'Price 10';
$gl_caption_activitat['c_price_text'] = 'Price text';
$gl_caption_activitat['c_price_text2'] = 'Price text 2';
$gl_caption_activitat['c_price_text3'] = 'Price text 3';
$gl_caption_activitat['c_price_text4'] = 'Price text 4';
$gl_caption_activitat['c_price_text5'] = 'Price text 5';
$gl_caption_activitat['c_price_text6'] = 'Price text 6';
$gl_caption_activitat['c_price_text7'] = 'Price text 7';
$gl_caption_activitat['c_price_text8'] = 'Price text 8';
$gl_caption_activitat['c_price_text9'] = 'Price text 9';
$gl_caption_activitat['c_price_text10'] = 'Price text 10';
$gl_caption_activitat['c_and'] = 'and';
$gl_caption_activitat['c_or'] = 'or';

$gl_caption_category['c_category'] = 'Category';
$gl_caption_category['c_activitat_button'] = 'See activities';
$gl_caption_category['c_ordre'] = 'Order';
$gl_caption_image['c_name'] = 'Name';
$gl_caption['c_idioma'] = $gl_caption['c_idioma_id'] = 'Language';
$gl_caption_activitat['c_url1_name']='Name URL(1)';
$gl_caption_activitat['c_url1']='URL(1)';
$gl_caption_activitat['c_url2_name']='Name URL(2)';
$gl_caption_activitat['c_url2']='URL(2)';
$gl_caption_activitat['c_url3_name']='Name URL(3)';
$gl_caption_activitat['c_url3']='URL(3)';
$gl_caption_activitat['c_videoframe']='Video (youtube, metacafe...)';
$gl_caption_activitat['c_file']='Files';
$gl_caption_activitat['c_in_home']='Surt a la Home';
$gl_caption_activitat['c_in_home_0']='No surt a la Home';
$gl_caption_activitat['c_in_home_1']='Surt a la Home';
$gl_caption_activitat_list['c_in_home_0']='';
$gl_caption_activitat_list['c_in_home_1']='Yes';
$gl_caption_activitat['c_destacat']='Notícia destacada';
$gl_caption_activitat['c_destacat_0']='No destacada';
$gl_caption_activitat['c_destacat_1']='Destacada';

$gl_caption_activitat['c_timetable']='Horari';
global $week_names;
$gl_caption_activitat['c_timetable1']=$week_names[0];
$gl_caption_activitat['c_timetable2']=$week_names[1];
$gl_caption_activitat['c_timetable3']=$week_names[2];
$gl_caption_activitat['c_timetable4']=$week_names[3];
$gl_caption_activitat['c_timetable5']=$week_names[4];
$gl_caption_activitat['c_timetable6']=$week_names[5];
$gl_caption_activitat['c_timetable7']=$week_names[6];
$gl_caption['c_level_id'] = $gl_caption['c_level'] = 'Level';
$gl_caption['c_modality_id'] = $gl_caption['c_modality'] = 'Modality';
$gl_caption['c_needed_material'] = 'Material needed';
$gl_caption['c_includes'] = 'Includes';
$gl_caption['c_excludes'] = 'Does not include';
$gl_caption_element['c_element'] = 'Item';

$gl_caption_activitat['c_form_level1_title_minimum_age']='Characteristics';
$gl_caption_activitat['c_form_level1_title_activitat']='Descriptions';
$gl_caption_activitat['c_form_level1_title_timetable']='Timetables';
$gl_caption_activitat['c_form_level1_title_price']='Prices';
$gl_caption_activitat['c_form_level1_title_needed_material']='Material';
$gl_caption_activitat['c_form_level1_title_file']='Arxius adjunts';
$gl_caption_activitat['c_form_level1_title_page_title']='SEO';
$gl_caption_activitat['c_form_level2_title_entered']='Dates';


$gl_caption['c_activitat_file_name'] = $GLOBALS['gl_caption']['c_file_name'];
$gl_caption['c_activitat_old_file_name'] = $GLOBALS['gl_caption']['c_old_file_name'];

$gl_caption_activitat['c_selected']='Activities seleccionades';

$gl_caption['c_ordre']='Order';

// public
$gl_caption['c_activitat'] = $gl_caption['c_activitat_id'] = 'Activity';
$gl_caption['c_activitat'] = 'Activitat ( Menú i llistat )';
$gl_caption_list['c_activitat'] = 'Activitat';
$gl_caption['c_price_id']='Prices';
$gl_caption['c_price_month']='Monthly price';
$gl_caption['c_price_quarter']='Quarterly price';


/* Centres*/
$gl_caption['c_centre']    = 'Center';
$gl_caption_centre['c_status']   = 'State';
$gl_caption_centre['c_prepare']  = 'In preparation';
$gl_caption_centre['c_public']   = 'Public';
$gl_caption_centre['c_archived'] = 'Filed';

$gl_caption['c_address']   = 'Address';
$gl_caption['c_poblacio']   = 'City';
$gl_caption['c_zip']       = 'Zip code';
$gl_caption_centre['c_admin_id']       = 'Administrators';
$gl_caption_centre['c_user_id']       = 'Users';

/* Operadors */
$gl_caption['c_operator'] = $gl_caption['c_operator_id'] = 'Operator';
$gl_caption_list['c_operator_id'] = 'Op.';
$gl_caption['c_telephone'] = 'Phone';
$gl_caption['c_operator_select_caption'] = "All";
$gl_caption['c_operator_select_operator'] = "With operator";
$gl_caption['c_operator_select_no_operator'] = "Without operator";

$gl_caption['c_centreproduct'] = "Products";
$gl_caption['c_image'] = "Image";
$gl_caption['c_family_id'] = "Family";
$gl_caption['c_subfamily_id'] = "Subfamily";
$gl_caption['c_product_title'] = "Product";
$gl_caption['c_product_subtitle'] = "Subtitle";
$gl_caption['c_ref'] = "Reference";
$gl_caption['c_status'] = "State";
$gl_caption['c_prepare'] = 'In preparation';
$gl_caption['c_review'] = 'To be checked';
$gl_caption['c_onsale'] = 'To be sold';
$gl_caption['c_nostock'] = 'Out of stock';
$gl_caption['c_nocatalog'] = 'Discontinued';
$gl_caption['c_archived'] = 'Filed';
$gl_caption['c_max_quantity'] = "Maximum quantity";
$gl_caption['c_quantityconcept_id'] = "Concept";

$gl_caption['c_form_level1_title_admin_id'] = "Users";
$gl_caption['c_form_level1_title_centreproduct'] = "Activities";

$gl_caption_assigncentre['c_centres'] = "Centers";
$gl_caption['c_centre_max_quantity'] = "Max";

/* Reserves */
$gl_caption['c_reservaitem_id'] = "";
$gl_caption['c_lassdive_product_date'] = "Requested day";
$gl_caption['c_reservaitem_date'] = "Assigned day";
$gl_caption['c_view_format_order'] = "Bookings";
$gl_caption['c_view_format_orderitems'] = "Activities";
$gl_caption['c_product_id'] = "Activity";
$gl_caption['c_lassdive_product_basetax'] = "Price";

$gl_caption['c_has_no_timetable'] = "Timetables";
$gl_caption['c_has_no_timetable_'] = "All";
$gl_caption['c_has_no_timetable_no_time'] = "No time";
$gl_caption['c_has_no_timetable_no_complement'] = "No compl.";

$gl_caption['c_date_search_caption'] = "Date search";
$gl_caption['c_date_search'] = "Date";
$gl_caption['c_timetable'] = "Schedule";
$gl_caption['c_date_search_order'] = "Booking";
$gl_caption['c_date_search_orderitems'] = "Activity";

$gl_caption['c_is_done_caption'] = "Done / Pending";
$gl_caption['c_is_done'] = "Done";
$gl_caption['c_is_done_'] = "All";
$gl_caption['c_is_done_search_0'] = "No";
$gl_caption['c_is_done_search_1'] = "Done";
$gl_caption['c_is_done_0'] = "No";
$gl_caption['c_is_done_1'] = "Yes";
$gl_caption['c_orderitems'] = "Activities";

$gl_caption['c_start_time'] = "Starts";
$gl_caption['c_end_time'] = "Ends";
$gl_caption['c_centre_id'] = "Center";
$gl_caption['c_user_id'] = "Instructor";
$gl_caption['c_is_done'] = "Done";

$gl_caption['c_source_id'] = "Origin";
$gl_caption['c_source_'] = "All";
$gl_caption['c_source_caption'] = "Origin";
$gl_caption['c_source_web'] = "Web";
$gl_caption['c_source_centre'] = "Center";

$gl_caption['c_no_center_with_activity'] = "There is no center offering this activity";
$gl_caption['c_day_is_busy'] = "No bookings available for this day";
$gl_caption['c_selected_timetables'] = "selected time";
// $gl_caption['c_all_timetables_already_selected'] = "Ja s'han seleccionat tots els horaris";
$gl_caption['c_select_ok'] = "Done!";
$gl_caption['c_select_canviar'] = "Change time";
$gl_caption['c_error_guardar'] = "An error occurred while saving";
$gl_caption['c_button_time'] = "";
$gl_caption['c_selecciona'] = "choose...";
$gl_caption['c_lassdive_login'] = "User";
$gl_caption['c_lassdive_logon'] = "Log in";

$gl_caption['c_reserva_form_activitats'] = "Activities";
$gl_caption['c_reserva_discount'] = "Discount";
$gl_caption['c_reserva_deposit'] = 'Deposit payment';

$gl_caption['c_total'] = "Total";

$gl_caption['c_product_original_title'] = "Prod. original";

// Calendari
$gl_caption['c_view_week'] = "Week";
$gl_caption['c_view_month'] = "Month";


// LOGS
$gl_caption['c_mysql'] = "Sql";
$gl_caption['c_user_id'] = "Usuari";
$gl_caption['c_entered'] = "Data";
$gl_caption['c_session'] = "Sessió";
$gl_caption['c_cookie'] = "Cookie";
$gl_caption['c_uri'] = "Adreça";
$gl_caption['c_referer'] = "Provinent";
$gl_caption['c_query_string'] = "Get";
$gl_caption['c_backtrace'] = "Backtrace";
$gl_caption['c_vars'] = "Variables";