<?
/**
 * PvgmaoOperari
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2011
 * @version $Id$
 * @access public
 */
class PvgmaoOperari extends Module{

	var $needs_client_id = true;
	var $group_id, $user_is_admin, $user_is_client, $client_id;

	function __construct(){
		parent::__construct();
	}

	function on_load() {
		parent::on_load();

		// Desde la fitxa de la màquin es pot insertar un operari, al obriri la finestra no vull que es pugui obrir una altra per entrar especialitats
		if (R::get('template') == 'window'){
			$this->set_field( 'especialitat_id', 'add_new_link_caption', '');
		}
		else {
			$this->set_field( 'especialitat_id', 'add_new_link', '/admin/?menu_id=2748&client_id=' . $this->client_id );
			$this->set_field( 'especialitat_id', 'add_edit_link', '/admin/?menu_id=2749&client_id=' . $this->client_id );
		}

		$this->set_field( 'especialitat_id', 'select_condition', 'client_id=' . $this->client_id );
	}

	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function get_records()
	{
	    $listing = new ListRecords($this);
		$listing->has_bin = false;
		$listing->condition = "client_id = " . $this->client_id;
	    return $listing->list_records();
	}
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{
	    $show = new ShowForm($this);
		$show->condition = "client_id = " . $this->client_id;
		$show->has_bin = false;
	    return $show->show_form();
	}

	function save_rows()
	{
	    $save_rows = new SaveRows($this);
	    $save_rows->save();
	}

	function write_record()
	{
	    $writerec = new SaveRows($this);
	    $writerec->save();
	}

	function manage_images()
	{
	    $image_manager = new ImageManager($this);
	    $image_manager->execute();
	}
}
?>