<?
/**
 * LlemenaAllotjament
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2011
 * @version $Id$
 * @access public
 */
class LlemenaAllotjament extends Module{
	var $form_tabs;
	function __construct(){
		parent::__construct();
	}
	function on_load(){
		$this->form_tabs = array('0' => array('tab_action'=>'show_form_map','tab_caption'=>$this->caption['c_edit_map']));
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function get_records()
	{
	    $listing = new ListRecords($this);
		$listing->call('list_records_walk','customer_description');
		$listing->add_filter ('allotjamentcategory_id');	
		$listing->add_filter ('status');
	    return $listing->list_records();
	}
	function list_records_walk($customer_description){
		$ret['customer_description']=add_dots('customer_description',$customer_description);
		return $ret;
	}
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{
	    $show = new ShowForm($this);
		$show->form_tabs = $this->form_tabs;
	    return $show->show_form();
	}

	function save_rows()
	{
	    $save_rows = new SaveRows($this);
	    $save_rows->save();
	}

	function write_record()
	{
	    $writerec = new SaveRows($this);
	    $writerec->save();
	}

	function manage_images()
	{
	    $image_manager = new ImageManager($this);
		$image_manager->form_tabs = $this->form_tabs;
	    $image_manager->execute();
	}
	function show_form_map (){ //esenyo el napa
		global $gl_news;
						
		$rs = Db::get_row('SELECT * FROM llemena__allotjament WHERE allotjament_id = ' . $_GET['allotjament_id']);
		$rs['municipi']=$this->caption['c_municipi_'.$rs['municipi']];
		$rs['title'] = Db::get_first('SELECT customer_title FROM llemena__allotjament_language  WHERE allotjament_id = ' . $_GET['allotjament_id'] . ' AND language = LANGUAGE');
		$rs['address'] = Db::get_first('SELECT adress FROM llemena__allotjament  WHERE allotjament_id = ' . $_GET['allotjament_id']);		
		$rs['is_default_point'] = ($rs['latitude']!=0)?0:1;
		$rs['latitude']=$rs['latitude']!=0?$rs['latitude']:GOOGLE_CENTER_LATITUDE;
		$rs['longitude']=$rs['longitude']!=0?$rs['longitude']:GOOGLE_CENTER_LONGITUDE;
		$rs['zoom']=$rs['zoom']?$rs['zoom']:GOOGLE_ZOOM;
		$rs['menu_id'] =  $_GET['menu_id'];		
		$rs['tool'] =  $this->tool;		
		$rs['tool_section'] =  $this->tool_section;		
		$rs['id'] =  $rs[$this->id_field];
		$rs['id_field'] =  $this->id_field;
		$this->set_vars($rs);//passo els resultats del array
		$this->set_file('llemena/allotjament_map.tpl'); //crido el arxiu
		$this->set_vars($this->caption); //passo tots els captions
		$gl_news = $this->caption['c_click_map'];
		$content = $this->process();//processo
		$GLOBALS['gl_content'] = $content;		
	}
	function save_record_map(){
		global $gl_page;
		extract($_GET);    	
		Db::execute ("UPDATE `llemena__allotjament` SET
			`latitude` = '$latitude',
			`longitude` = '$longitude',
			`zoom` = '$zoom'
			WHERE `allotjament_id` =$allotjament_id
			LIMIT 1") ;
		$gl_page->show_message($this->messages['point_saved']);
		Debug::p_all();
		die();			
	}
}
?>