/*
Clockwork.directive('datatables', function ($rootScope, $timeout) {
	return {
		restrict: 'E, A, C',
		link: function ($scope, element, attrs, controller) {
			$scope.$watch("request.databaseQueries", function (value) {//I change here
				var val = value || null;
				if (val) {
					$timeout (function () {
						$scope.create_data_tables()
					}, 0);
				}
			});
		}
	};
});
*/

// Només està vinculada a la última (databaseQueries), i a partir d'aqui es creen totes
Clockwork
	.directive('datatables', function ($rootScope, $timeout) {
		return {
			restrict: 'A',
			scope: {
				someAttr: '='
			},
			link: function (scope, element, attrs) {

				if (scope.$parent.$first) {
						console.log('Començant el repeat de datatables ...');
				}
				if (scope.$parent.$last) {
					$timeout(function () {
						console.log('Acabat el repeat de datatables ...');
						scope.$parent.create_data_tables()
					}, 0);
				}
			}
		};
	});
