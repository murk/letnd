<?
/*
 * TODO-i Poder editar una comanda tancada,
 */
class ProductProduct extends Module{

	var $condition = ''; // faig servir a cart
	var $limit = ''; // faig servir a home i blocks
	var $order_by = ''; // faig servir a blocks
	var $listing = ''; // per poder fer el que vulgui amb el listing desde la home
	var $is_home = false;
	function __construct(){
		parent::__construct();
	}
	function on_load(){

		Main::load_class( 'product', 'variation_images' );

		$this->set_friendly_params(array(
			'family_id'=>R::number('family_id'),
			'family'=>R::number('family'),
			'brand_id'=>R::number('brand_id'),
			'discount'=>R::escape('discount'),			
			'new'=>R::escape('new'),
			'promotion'=>R::escape('promotion'),
			'sector_id'=>R::escape('sector_id'),
			'sector'=>R::escape('sector'),
			'list'=>R::escape('list')
			));


		// Camp descripcio en HTML ( també ho necessito a newsletter, per això va davant de tot
		if ($this->config['is_description_html'] == '1'){
			$this->set_field('product_description','type','html');
		}
		
		// si vinc de newsletter deixo veure producte
		if (isset($_GET['loginnewsletter']) && $_GET['loginnewsletter'] == 'letndmarc' && $_GET['passwordnewsletter'] == '5356aopfqkiios5cnoqws90se')
            return;
		// si es privada no deixo fer res excepte entrar login i password
		if ($this->is_index && $this->config['is_private_shop'] && !$this->is_logged()) {
			redirect('/?tool=product&tool_section=customer&action=login&language='.LANGUAGE);
			die();
		}
		unset($this->caption['c_subfamily_id_format']); // aquest format es per el desplegable de la intra
		$this->set_field('subfamily_id','select_fields','product__family.family_id,family');
		
		// passo a formatejar amb sempre 2 digits, currency
		if ($this->config['format_price_as_currency']){
			
			$this->set_field('pvp','type','currency');
			$this->set_field('pvd','type','currency');
			$this->set_field('pvd2','type','currency');
			$this->set_field('pvd3','type','currency');
			$this->set_field('pvd4','type','currency');
			$this->set_field('pvd5','type','currency');
			$this->set_field('discount_fixed_price','type','currency');
			$this->set_field('discount_fixed_price_wholesaler','type','currency');
		}



		// missatge de iva inclos o no inclos
		if ($this->is_wholesaler())
		{
			$this->caption['c_tax_included_excluded'] = $this->config['is_wholesaler_tax_included']?$this->caption['c_tax_included']:$this->caption['c_tax_not_included'];
		}
		else {
			$this->caption['c_tax_included_excluded'] = $this->config['is_public_tax_included']?$this->caption['c_tax_included']:$this->caption['c_tax_not_included'];
		}

		parent::on_load();
		
	}
	function save_rows()
	{
		$save_rows = new SaveRows($this);
		$save_rows->save();
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function search(){
		$this->do_action('list_records');
	}
	function get_records(){

		$this->set_other_config();
		
		//llisto tots els productes de la familia se.leccionada
		if ($this->is_wholesaler()) $this->caption['c_pvp'] = $this->caption['c_pvd'];
		$familia=false;
		$brand_name='';
		$listing = new ListRecords($this);

		// Inicio variables
		$listing->set_vars( [
			'subfamilys'               => false,
			'family'                   => false,
			'family_id'                => false,
			'listing_family_id'        => false,
			'listing_family'           => false,
			'listing_family_parent_id' => false,
			'listing_family_parent'    => false,
			'family_description'    => false,
			'list_description'    => false
		] );

		// per poder previsualitzar desde admin i enviar desde newsletter
		if (isset($_GET['loginnewsletter']) 
				&& $_GET['loginnewsletter'] == 'letndmarc'
				&& $_GET['passwordnewsletter'] == '5356aopfqkiios5cnoqws90se'
		)
		{
			if (is_file(PATH_TEMPLATES . 'newsletter/product_list.tpl')) 
					$listing->template = 'newsletter/product_list';
			
		}

		//$listing->call_function['product_description'] = 'add_dots';
		$listing->call("list_records_walk", "stock,last_stock_units_public,pvp,pvd".$this->get_wholesaler_rate().",discount,discount_fixed_price,discount_wholesaler,discount_fixed_price_wholesaler,price_consult,product_id,status,sell,brand_id",true);


		if (show_preview()){
			$listing->condition = "(status <> 'archived')";
		}
		else {
			$listing->condition = "(status = 'onsale' OR status = 'nostock' OR status = 'nocatalog') ";
		}

		$listing->condition .= $this->condition;
		if ($this->limit) $listing->limit = $this->limit;
		$listing->order_by = $this->order_by?$this->order_by:'ordre ASC, product_title ASC';
		
		

		// cart
		/*puc tenir els següents casos:
		1er->estic a la home i llisto els destacats
		2-> estic a la home i llisto els destacats però no n'hi ha cap, en aquest cast llisto la primera família amb resultats
		3-> vinc d'una famíla
		4-> vinc d'una subfamília
		5-> vinc d'una família o subfamília però no hi ha cap producte, aquest cas no s'hauria de permetre!!!! arreglar Joan
		*/

		if ($this->parent == 'ProductProductHome') { //lo que estem fent es tornar al product product home pq li donqui la condicio
			//vinc de la home
			$listing->set_var('family', $this->caption['c_productes_destacats']);
			$listing->list_records(false);
			return $listing;			
		}
		elseif ($this->parent == 'BlockListing') {//blocks
			return $listing;
		}
		elseif ($this->parent == 'Block') {//blocks
			$listing->set_records();
			return $listing->loop;
		}
		elseif ($this->parent == 'sitemap') {//Sitemap
			$listing->paginate = false;
			$listing->set_field('modified', 'type', 'none');
			$listing->set_records();
			return $listing->loop;
		}
		else {//no vinc de la home o es home amb subfamilies o mostrar primera familia

			if ($this->parent == 'ProductCart') {// cart
				return $listing;
			}
			elseif (isset($_GET['product_visited_more']) && !empty($_SESSION['visited_products'])) {// vinc del block de visitats i els vull mostrar tots
			
				$products = array_reverse($_SESSION['visited_products']);

				R::escape_array( $products );

				$ids = implode(',', $products);

				$listing->condition .= " AND product__product.product_id IN (" . $ids . ")";
				$listing->order_by = "FIELD(product__product.product_id, " . $ids . ")";
				
				$title = Db::get_first("SELECT title 
							FROM all__block, all__block_language
							WHERE script = 'product_visited'
							AND all__block.block_id = all__block_language.block_id
							AND language ='" . LANGUAGE . "'");
				
				$GLOBALS['gl_page']->title = $title;
				$GLOBALS['gl_page']->page_title = $title;
				
			}
			elseif (isset($_GET['product_related_more']) && !empty($_GET['product_id'])) {// vinc del block de visitats i els vull mostrar tots

				$related_id = R::id('product_id');

				$listing->condition .= " AND product__product.product_id IN (
				
					SELECT product2_id FROM product__product_related WHERE product_id = '$related_id'
					ORDER BY ordre
					
				)";

				$title = Db::get_first("SELECT title 
							FROM all__block, all__block_language
							WHERE script = 'product_related'
							AND all__block.block_id = all__block_language.block_id
							AND language ='" . LANGUAGE . "'");

				$GLOBALS['gl_page']->title = $title;
				$GLOBALS['gl_page']->page_title = $title;

			}
			elseif (isset($_GET['subfamily_id'])) {//cas 3
				$familia = R::id('subfamily_id');
				$listing->condition .= ' AND subfamily_id = ' . $familia;
			}
			elseif (R::number('family_id') || R::number('family')){//cas 4
				$familia = R::number('family_id')?R::number('family_id'):R::number('family');
				// $listing->condition .= ' AND family_id = ' . $familia;
				$listing->condition .=  $this->get_family_or_subfamily($familia);
			}
			elseif (isset($_GET['filecat_file_name'])){//cas 3 i 4				
				// FRIENDLY URL Si tinc filecat_file_name, ho passo a family_id o subfamily_id
				$familia = 0; // passo per ref i m'actualitza el valor
				$listing->condition .=  $this->get_family_from_filecat($familia);
			}

			if (R::number('brand_id')){
				$brand_id = R::number('brand_id');
				$listing->condition .= ' AND brand_id = ' . $brand_id;
				$rs_brand = Db::get_row("
						SELECT brand_name, brand_description, page_title, page_description, page_keywords 
						FROM product__brand, product__brand_language  
						WHERE product__brand.brand_id = '" . $brand_id . "' AND product__brand.brand_id = product__brand_language.brand_id 
						AND language = '" . LANGUAGE . "'");
				
					
				if ($this->is_index) {
					
					Page::add_breadcrumb($rs_brand['brand_name'],'');					
					$GLOBALS['gl_page']->title = $rs_brand['brand_name'];
				
					Page::set_page_title($rs_brand['page_title'],$rs_brand['brand_name']);
					Page::set_page_description($rs_brand['page_description'],$rs_brand['brand_description']);
					Page::set_page_keywords($rs_brand['page_keywords']);					
					
					$description = nl2br(htmlspecialchars($rs_brand['brand_description']));
					
					$listing->set_var('brand_description', $description);
					$listing->set_var('list_description', $description);
					
				}
			}

			if (R::number('sector_id') || R::number('sector')){
				$sector_id = R::number('sector_id')?R::number('sector_id'):R::number('sector');
				$listing->condition .= ' AND product_id IN ( SELECT product__product_to_sector.product_id FROM product__product_to_sector WHERE sector_id = ' . $sector_id .')';
				
				$rs_sector = Db::get_row("
						SELECT sector, sector_description, page_title, page_description, page_keywords 
						FROM product__sector_language  
						WHERE sector_id = '" . $sector_id . "' AND language = '" . LANGUAGE . "'");
				
					
				if ($this->is_index) {
					
					Page::add_breadcrumb($rs_sector['sector'],'');
					$GLOBALS['gl_page']->title = $rs_sector['sector'];
				
					Page::set_page_title($rs_sector['page_title'],$rs_sector['sector']);
					Page::set_page_description($rs_sector['page_description'],$rs_sector['sector_description']);
					Page::set_page_keywords($rs_sector['page_keywords']);					
					
				
					$description = nl2br(htmlspecialchars($rs_sector['sector_description']));
					
					$listing->set_var('sector_description', $description);				
					$listing->set_var('list_description', $description);				
				}
			}


			if ($this->is_home && !isset($_GET['family_id'])){
				// cas 8 - mostrar productes primera familia ( ja s'ha establert un family_id, per tant no fa aquest if )
				// cas 9 - Mostrar families home
				$familia = 0;
			}

			if (isset($_GET['promotion'])){
				$listing->condition .= ' AND prominent = 1';
			}
			elseif (isset($_GET['discount'])){
				$wholesaler = is_product_wholesaler()?'_wholesaler':'';
				$listing->condition .= ' AND (discount'.$wholesaler.'>0 or discount_fixed_price'.$wholesaler.'>0)';
			}
			elseif (isset($_GET['new'])){

				$listing->condition .= ' AND is_new_product = 1';
			}


			$this->check_private_family( $familia );

			// ORDERBY

			// per ordenar per marca
			if (R::escape('orderby') == 'brand-asc' || R::escape('orderby') == 'brand-desc') {
				$listing->add_field('brand',
					array(
						'change_field_name'=> '(SELECT brand_name FROM product__brand WHERE product__product.brand_id = product__brand.brand_id) AS brand',
						'list_public' => '0'
					));
			}
			// per ordenar per material
			if (R::escape('orderby') == 'material-asc' || R::escape('orderby') == 'material-desc') {
				$listing->add_field('material',
					array(
						'change_field_name'=> '(SELECT material FROM product__material_language WHERE product__product.material_id = product__material.material_id AND language = '.LANGUAGE.') AS material',
						'list_public' => '0'
					));
			}


			// BUSCADOR, sempre actiu quan hi ha el parametre q

			$listing->condition .= $this->search_list_condition_words();


			
			// FILTRES
			
			// price filtre
			if ( ! empty( $_GET['prices'] ) ) {
				$prices = R::text_id('prices');
				$prices = explode( '-', $prices );

				if ( ! empty( $prices[1] ) ) {
					$price1 = $prices[0];
					$price2 = $prices[1];

					$pvp_field = $this->is_wholesaler() ? 'pvd' . $this->get_wholesaler_rate() : 'pvp';

					$listing->condition .= " AND ($pvp_field BETWEEN '$price1' AND '$price2') AND price_consult = 0 AND $pvp_field != 0";
				}
			}

			// family_id filtre
			if ( ! empty( $_GET['family_ids'] ) ) {
				R::escape_array($_GET['family_ids']);
				$listing->condition .= " AND (
				family_id IN ( ".implode(',',$_GET['family_ids'])." ) ||
				subfamily_id IN ( ".implode(',',$_GET['family_ids'])." ) ||
				subsubfamily_id IN ( ".implode(',',$_GET['family_ids'])." )
				)
				";
			}

			// spec_id filtre
			if ( ! empty( $_GET['spec_ids'] ) ) {

				$spec_ids = $_GET['spec_ids'];
				R::escape_array($spec_ids);

				$spec_ids_language = array();
				$spec_ids_no_language = array();

				foreach ( $spec_ids as $key => $spec_id ) {
					$query_spec   = "SELECT spec_type FROM product__spec WHERE spec_id = '" . $spec_id . "'";
					$spec_type = Db::get_first( $query_spec );
					if ( $spec_type == 'text' ) {
						$spec_ids_language []= $spec_id;
					}
					else {
						$spec_ids_no_language []= $spec_id;
					}
					R::escape_array($_GET['spec_ids_' . $spec_id]);
				}

				if ($spec_ids_language) {
					$spec_condition = '';

					// cada cas ha de tindre el seu product_id IN, no es pot simplificar la consulta sino em sortiria
					//  WHERE (spec_id = '3' AND spec IN ('8')) AND (spec_id = '2' AND spec IN ('10', '60')) i això és impossible
					foreach ( $spec_ids_language as $key => $spec_id ) {
						$spec_condition = " AND spec_id = '" . $spec_id . "' AND product__product_to_spec_language.spec IN ('" . implode( "','", $_GET['spec_ids_' . $spec_id] ) . "')";
						$listing->condition .= " AND product_id IN ( SELECT product__product_to_spec.product_id FROM product__product_to_spec_language, product__product_to_spec WHERE product__product_to_spec_language.product_spec_id = product__product_to_spec.product_spec_id 
						AND language = '" . LANGUAGE . "' " . $spec_condition . ')';
					}
				}

				if ($spec_ids_no_language) {
					$spec_condition = '';
					foreach ( $spec_ids_no_language as $key => $spec_id ) {
						$spec_condition = "spec_id = '" . $spec_id . "' AND spec IN ('" . implode( "','", $_GET['spec_ids_' . $spec_id] ) . "')";
						$listing->condition .= " AND product_id IN ( SELECT product_id FROM product__product_to_spec WHERE " . $spec_condition . ')';
					}
				}
			}

			// sector_id filtre
			if ( ! empty( $_GET['sector_ids'] ) ) {
				R::escape_array($_GET['sector_ids']);
				$listing->condition .= " AND product_id IN ( SELECT product_id FROM product__product_to_sector WHERE sector_id IN (".implode(',',$_GET['sector_ids'])."))";
			}

			// brand_id filtre
			if ( ! empty( $_GET['brand_ids'] ) ) {
				R::escape_array($_GET['brand_ids']);
				$listing->condition .= " AND brand_id IN (".implode(',',$_GET['brand_ids']).")";
			}

			// Famílies privades
			$listing->condition .= $this->get_family_private_condition();

			if ($familia!==false) $this->give_me_information_family($familia, $listing, true);
			$listing->set_var('brand_name',$brand_name);

			if (!$listing->always_parse_template)
				$listing->always_parse_template = $this->config['always_parse_template'];
			return $listing->list_records();
		}

		/*/ Crec que no fa falta el noproduct.tpl
		if (!$listing->has_results && $this->parent != 'ProductCart') //Si no te resultas passo un tpl, conforme no te resultats
		{
			$this->set_file('product/noproduct.tpl');
			//$gl_content = $this->process();
		}*/
	}
	
	function search_list_condition_words ()
	{
	    $q = R::escape('q');

	    if (!$q) return '';
	    $search_fields = $this->config['search_fields'];

	    if (!$search_fields) $search_fields = "product_title,product_subtitle,others1,others2,others3,others4,others5,others6,others7,others8,product_description";

		$search_fields = explode( ',', $search_fields );

		$query = "SELECT product_id FROM product__product where
				   ref like '%" . $q . "%' OR
				   ref_number like '%" . $q . "%' OR
				   pvp like '%" . $q . "%'
				   " ;
		$results1 = Db::get_rows ($query);
		$_1 = count ($results1);
		$query2 = "SELECT product_id FROM product__product_language where ";

		foreach ( $search_fields as $search_field){
			$query2 .= "$search_field like '%$q%' OR ";
		}

		$query2 = substr($query2,0,-4);

		$results2 = Db::get_rows ($query2);
		$_2= count ($results2);
		if ($_1+$_2 > 0 ) {//faig trampa per si no hi ha cap resultat
		    $results = array_merge($results1,$results2 );
			 foreach ($results as $key)
                {
					$ids_arr[] = $key['product_id'];
                }

                $ids = implode(',', $ids_arr);
			} else {$ids="NULL";}

		$query = " AND product_id IN (" . $ids . ")";
		$search_condition = $query;
	    $GLOBALS['gl_page']->title = TITLE_SEARCH;
	    return $search_condition;
	}
	// FRIENDLY URL Si tinc filecat_file_name, ho passo a family_id o subfamily_id
	function get_family_or_subfamily($family_id){
		
		// averiguo si es familia o subfamilia
		$parent_id = Db::get_first("SELECT parent_id FROM product__family WHERE family_id ='".$family_id."'");
		if ($parent_id){
			$parent_parent_id = Db::get_first("SELECT parent_id FROM product__family WHERE family_id ='".$parent_id."'");
			if ( $parent_parent_id ) {
				$ret = ' AND subsubfamily_id = ' . $family_id;
			}
			else {
				$ret = ' AND subfamily_id = ' . $family_id;
			}
		}
		else {
			$ret = ' AND family_id = ' . $family_id;
			if (!$this->config['show_products_family_and_subfamilys']) $ret .= ' AND subfamily_id = 0';
		}	
		return $ret;
		
	}
	// FRIENDLY URL Si tinc filecat_file_name, ho passo a family_id o subfamily_id
	function get_family_from_filecat(&$family_id){
		$file_name = R::file_name('filecat_file_name');
		// de moment nomès fem servir id, despres hi posarem un filecat_file_name
		$family_id = $file_name;

		// averiguo si es familia o subfamilia
		$parent_id = Db::get_first("SELECT parent_id FROM product__family WHERE family_id ='".$family_id."'");
		if ($parent_id){
			$parent_parent_id = Db::get_first("SELECT parent_id FROM product__family WHERE family_id ='".$parent_id."'");
			if ( $parent_parent_id ) {
				$ret = ' AND subsubfamily_id = ' . $family_id;
			}
			else {
				$ret = ' AND subfamily_id = ' . $family_id;
			}
		}
		else {
			$ret = ' AND family_id = ' . $family_id;
			if (!$this->config['show_products_family_and_subfamilys']) $ret .= ' AND subfamily_id = 0';
		}
		return $ret;

	}
	function get_subfamilys($family, &$listing) {
		if (!$listing->config['show_subfamilys'] && !$listing->config['home_show_subfamilys'] && !$listing->config['home_tool_show_subfamilys']) return;
		
		$show_empty_familys = $this->config['show_empty_familys'];
		
		$query = "SELECT family, family_content_list, family_description, family_alt, product__family_language.family_id
						FROM product__family_language, product__family
						WHERE  product__family_language.family_id = product__family.family_id
						AND language = '" . LANGUAGE . "'
						AND product__family.parent_id= '" . $family . "'
						AND bin = 0
						ORDER BY ordre ASC, family";
		$results = Db::get_rows($query);
		
		$conta = 0;
		
		
		foreach ($results as $key=>&$rs) {

			$add_dots_length = Db::get_first("SELECT value FROM product__configadmin_family WHERE name='add_dots_length'");
			
			// CONTA PRODUCTES, es mes rapid que posant la consulta com a subquery
			
			// CONTA PRODUCTES, es mes rapid que posant la consulta com a subquery
			$sub_product_count = Db::get_first("SELECT count(*) 
					FROM product__product
					WHERE (
						product__product.family_id = " . $rs['family_id'] . "
						|| product__product.subfamily_id = " . $rs['family_id'] . " )
							
					AND (status = 'onsale' OR status = 'nostock' OR status = 'nocatalog') 
					AND product__product.bin = 0");

			if ($show_empty_familys || $sub_product_count) {
				
				$rs['family_is_empty'] = !$sub_product_count;

				if (USE_FRIENDLY_URL) {
					$rs['list_records_link'] = Page::get_link('product', 'product', false, false, [], false, '', $rs['family_id'], '');
				} else {
					$rs['list_records_link'] = '/?tool=product&amp;tool_section=product&amp;subfamily_id=' . $rs['family_id'] . '&amp;language=' . LANGUAGE;
				}
				
				
				$rs['conta'] = $conta++;
				
				$rs['family_description']=$rs['family_content_list']?$rs['family_content_list']:add_dots('content', $rs['family_description'],$add_dots_length);

				$rs += UploadFiles::get_record_images($rs['family_id'], true, 'product', 'family');
			}
			else{
				unset($results[$key]);
			}
		}
		$loop_count = count( $results );
		foreach ($results as &$rs) {
			$rs['loop_count'] = $loop_count;
		}

		
		if ($results) $listing->always_parse_template = true;
		
		return $results;

		
	}
	function give_me_information_family ($fam, &$listing, $is_listing){ //crido el listing per referencia

		// sino es listing, ho utilitzo nomès pel breadcrumb del form
		if ($fam) {
			$query = "SELECT family, family_subtitle, family_description, family_content_list, parent_id, page_title, page_description, page_keywords
						FROM product__family_language, product__family
						WHERE product__family_language.family_id = product__family.family_id
						AND product__family.family_id = " . $fam . "
						AND language = '" . LANGUAGE . "'";
			$rs = DB::get_row ($query);

			if ($is_listing) {
				$listing->set_var('family',  $rs['family']);
				$listing->set_var('family_id',  $fam);
				$listing->set_var('listing_family_id',  $fam);
				$listing->set_var('listing_family',  $rs['family']);

				$description = nl2br(htmlspecialchars($rs['family_description']));
				$family_content_list = nl2br(htmlspecialchars($rs['family_content_list']));
				$family_subtitle = $rs['family_subtitle'];

				$listing->set_var('family_description',  $description);	
				$listing->set_var('family_content_list',  $family_content_list);
				$listing->set_var('family_subtitle',  $family_subtitle);
				$listing->set_var('list_description',  $description);
				
				$image = UploadFiles::get_record_images($fam, false, 'product', 'family');
				foreach ($image as $key=>$val){
					$listing->set_var('family_'.$key,$val);
				}
				
				
				
			}
			
			if (($this->is_index || $this->is_home) && $is_listing) {

				if ($this->config['listing_title'])
					Page::set_title( $rs[$this->config['listing_title']] );
				if ($this->config['listing_body_subtitle'])
					Page::set_body_subtitle( $rs[$this->config['listing_body_subtitle']] );
				if ($this->config['listing_body_description'])
					Page::set_body_description( $rs[$this->config['listing_body_description']] );

				Page::set_page_title($rs['page_title'],$rs['family']);
				Page::set_page_description($rs['page_description'],$rs['family_description']);
				Page::set_page_keywords($rs['page_keywords']);		
				
			}
			
			$link = '';
			
			if ($rs['parent_id']){
				$query = "SELECT family FROM product__family_language
						WHERE family_id = " . $rs['parent_id'] . "
						AND language = '" . LANGUAGE . "'";
				$family = DB::get_first ($query);
				
				if ($this->is_index) {
					if (USE_FRIENDLY_URL){						
						$link = Page::get_link('product','product',false,false,array(),false,'',$rs['parent_id'],'');
					}
					else{				
						$link = $this->link . '&family_id=' . $rs['parent_id'];	
					}				
					Page::add_breadcrumb($family,$link);
				}
				if ($is_listing) {
					$listing->set_var( 'listing_family_parent_id', $rs['parent_id'] );
					$listing->set_var( 'listing_family_parent', $family );
				}
			}
			
			if ($this->is_index) {
				// si es formulari, la familia no es l'ultim element del breadcrumb, es el producte
				$link = $is_listing?'':($this->link . '&subfamily_id=' . $fam);	
				
				if (USE_FRIENDLY_URL){						
					$link = Page::get_link('product','product',false,false,array(),false,'',$fam,'');
				}
				else{				
					$link = $this->link . '&subfamily_id=' . $fam;	
				}
				if ($is_listing) $link = '';
				Page::add_breadcrumb($rs['family'],$link);
			}
			
			
		}	

		// així agafa primera familia
		if ($fam!==false) $listing->set_var('subfamilys',  $this->get_subfamilys($fam, $listing));
	}
	// es crida desde list i form
	function list_records_walk($module,$stock,$last_stock_units_public,$pvp, $pvd, $discount, $discount_fixed_price, $discount_wholesaler, $discount_fixed_price_wholesaler, $price_consult, $product_id, $status, $sell, $brand_id, $is_form=false)
	{
		$rs = &$module->rs;
		$ret = $this->get_price_consult($price_consult);
		$ret += $this->get_discount($pvp, $pvd, $discount, $discount_fixed_price, $discount_wholesaler, $discount_fixed_price_wholesaler, $price_consult);
		if (!$is_form) {

			if ($this->parent == 'ProductCart') {

				if ( $rs['old_variation_increment'] ) {
					$ret['old_price'] = $ret['old_price'] + $rs['old_variation_increment'];
				}
			}
			else {

				// Lassdive - TODO-i Ara només a lassdive, testejar per substituir la funció get_variation_increment
				if ( is_table( 'lassdive__activitat' ) ) {
					$ret ['variations'] = $this->get_variations( $product_id, $ret['pvp'], $ret['old_price'], $rs['ref'], $rs['discount'], $rs['discount_fixed_price'], $rs['discount_wholesaler'], $rs['discount_fixed_price_wholesaler'], $is_form );
				}
				// fi lassdive
				else {
					$ret = array_merge(
						$ret,
						$this->get_variation_increment( $ret['pvp'], $pvd, $ret['old_price'], $product_id, $discount, $discount_fixed_price, $discount_wholesaler, $discount_fixed_price_wholesaler )
					);
				}
			}


			$ret['product_description'] = add_dots('product_description',$rs['product_description'],$this->config['add_dots_length']);
		
		}

		// imatge brand
		$ret += UploadFiles::get_record_images($brand_id, false, 'product', 'brand','brand_');
		
		$this->get_stock($product_id, $stock, $last_stock_units_public); // vars per referencia
		$ret['stock'] = $stock;
		$ret['last_stock_units_public'] = $last_stock_units_public;
		$ret['show_last_units']= ($this->config['stock_control']&& ($stock<=$last_stock_units_public));
		
		$ret['old_price'] = ($this->config['format_price_as_currency'])?
								format_currency($ret['old_price']):
								format_decimal($ret['old_price']);		
		// assigno variables d'estat
		$ret['onsale'] = ($status=='onsale');
		$ret['nostock'] = ($status=='nostock');		
		$ret['nocatalog'] = ($status=='nocatalog');
		// condicions per poder comprar: ha d'estar a la venta, ha d'haver preu, ha d'haver 'sell' marcat, ha d'estar 'price_consult' desmarcat (llavors preu=0)
		$ret['can_buy'] = ($status=='onsale' && (double)$ret['pvp'] && $sell);
		
		// tampoc es pot comprar si no es distribuidor i nomes es pot vendre a distribuidors
		if (!$this->is_wholesaler() && $this->config['sell_only_to_wholesalers']) $ret['can_buy'] = false;
		
		$ret['can_buy_message'] = $ret['nostock']?$this->caption['c_nostock']:'';
		$ret['can_buy_message'] = $ret['nocatalog']?$this->caption['c_nocatalog']:$ret['can_buy_message'];
		$ret['can_buy_message'] = $sell?$ret['can_buy_message']:nl2br(PRODUCT_NO_SELL_TEXT);
		$ret['can_buy_message'] = !(double)$ret['pvp']?'':$ret['can_buy_message']; // te prioritat l'ultim missastge


		// obtinc a quina familia pertany directament
		$ret['real_family_id'] = $rs['family_id'];

		$ret['real_family_id'] = $rs['subfamily_id'] != 0?$rs['subfamily_id']:$ret['real_family_id'];

		$ret['real_family_id'] = $rs['subsubfamily_id'] != 0?$rs['subsubfamily_id']:$ret['real_family_id'];

		$rs_calendar = Db::get_row("SELECT has_calendar, is_calendar_public FROM product__family WHERE family_id = " . $ret['real_family_id']);

		$ret['family_has_calendar'] = $rs_calendar['has_calendar'] == 1 && $rs_calendar['is_calendar_public'] == 1;


		return $ret;
	}
	function get_price_consult($price_consult){
		// Si es preu a consultar amago el preu
		$ret = array();
		if($price_consult){
			$ret['pvp'] = 0;
			$ret['price_consult'] = $this->caption['c_price_consult'];
		}
		else{
			$ret['price_consult'] = 0;
		}
		return $ret;
	}

	function get_variation_discount ($pvp, $discount, $discount_fixed_price, $discount_wholesaler, $discount_fixed_price_wholesaler) {

		// Aquí no faig servir pvd ja que si es distribuidor, la funció get_discount passa el preu de distribuidor a la variable pvp, així la plantilla no es preocupa de si es distribuidor o no

		Debug::p( [
					[ 'pvp', $pvp ],
					[ 'discount', $discount ],
					[ 'discount_fixed_price', (double)$discount_fixed_price ],
					[ 'discount', round($pvp*(1-($discount/100)),2) ],
				], 'productDiscount');

		$pvp = (double)$pvp;
		$is_wholesaler = $this->is_wholesaler();


		if ($is_wholesaler){

			if ($discount_wholesaler && !(double)$discount_fixed_price_wholesaler) {
				return round($pvp*(1-($discount_wholesaler/100)),2);
			}
		}
		// PVP
		else{
			if ($discount && !(double)$discount_fixed_price) {
				return round($pvp*(1-($discount/100)),2);
			}

		}

		// Si no hi ha descompte
		return $pvp;


	}
	function get_discount ($pvp, $pvd, $discount, $discount_fixed_price, $discount_wholesaler, $discount_fixed_price_wholesaler, $price_consult){
		$ret = array();
		
		$ret['old_price'] = ''; // nomès hi ha original price quan tenim un descopmte, així podem mostrar els 2 preus, el vell i el preu amb descompte
		
		if($price_consult) return $ret; // si es preu a consultar no faig res
		
		$is_wholesaler = $this->is_wholesaler();
		// PVD
		// si es distribuidor: el pvp passa a ser el preu de distribuidor, i el old_price, passa a ser el preu de venta al public, o si hi ha descompte, el preu de venta al public amb el descompte aplicat ------> sempre el preu més baix de venta al public
		if ($is_wholesaler){
			//$ret['old_price'] = $ret['pvp'];
			$ret['pvp'] = $pvd;
			$ret['discount'] = $discount_wholesaler;

			// si hi ha descompte distribuidor, l'aplico aquí, 
			// així si no n'hi ha el old_price 'un distribuidor queda com el pvp
			if ($discount_fixed_price_wholesaler!=0) { // hi ha descompte fixat i té preferencia sobre el tant per cent
				$ret['pvp'] = $discount_fixed_price_wholesaler;
				$ret['discount_fixed'] = true;
				$ret['old_price'] = $pvd;
				// calculo el percent descomptat			
				$ret['discount'] = round(($pvp-$discount_fixed_price_wholesaler)*100/$pvp);
			}
			elseif ($discount_wholesaler) {
				$ret['pvp'] = round($pvd*(1-($discount_wholesaler/100)),2);
				$ret['discount_fixed'] = false;
				$ret['old_price'] = $pvd;
			}
		}
		// PVP
		else{
		
			$ret['pvp'] = $pvp; // nomès per que fa falta si crido fora del list_records_walk

			if ($discount_fixed_price!=0) { // hi ha descompte fixat i té preferencia sobre el tant per cent
				$ret['pvp'] = $discount_fixed_price;
				$ret['discount_fixed'] = true;
				$ret['old_price'] = $pvp;
				// calculo el percent descomptat			
				$ret['discount'] = round(($pvp-$discount_fixed_price)*100/$pvp);
			}
			elseif ($discount) {
				$ret['pvp'] = round($pvp*(1-($discount/100)),2);
				$ret['discount_fixed'] = false;
				$ret['old_price'] = $pvp;
			}
		
		}

		$ret['old_price_value'] = $ret ['old_price'];
		
		return $ret;
	}
	// per afegir increment de la variació per defecte al preu del llistat
	// TODO no te en compte que hi poguessin haber variacions sense default marcat, teoricament no en pot haber cap, pero prodria fallar el jscript o qualsevol altra parida
	function get_variation_increment($pvp, $pvd, $old_price, $product_id, $discount, $discount_fixed_price, $discount_wholesaler, $discount_fixed_price_wholesaler){
		$ret = array();
		
		// Si es distribuidor, agafo increment del preu de distribuidor
		$is_wholesaler = $this->is_wholesaler();
		$which_pv = $is_wholesaler?'variation_pvd'.$this->get_wholesaler_rate():'variation_pvp'; 
		
		// busco l'increment en el pvp i pvd
		$query = "
			SELECT product__product_to_variation.".$which_pv." as variation_pvp, variation_ref
			FROM product__product_to_variation
			INNER JOIN product__variation USING(variation_id)
			WHERE BIN = 0
			AND product_id = " . $product_id . "
			AND product__product_to_variation.variation_default=1
			AND variation_status = 'onsale'
			ORDER BY 
				variation_category_id ASC";
		$results = Db::get_rows($query);
		debug::add('Query get_variation_increment',$query);
		debug::add('Results get_variation_increment',$results);
		if (!$results) return $ret;
		
		foreach ($results as $rs){		
			// es suma increment al pvp
			$pvp += $this->get_variation_discount($rs['variation_pvp'], $discount, $discount_fixed_price,  $discount_wholesaler, $discount_fixed_price_wholesaler );


		/*	Debug::p( [
					[ 'pvp variation increment', $pvp ]
				], 'productDiscount');*/

			// si hi ha descompte es suma l'increment al old_price	
			if ($old_price) $old_price += (double)$rs['variation_pvp'];
		}
		$ret = array(
			'pvp' => $pvp,
			'old_price' => $old_price
		);
		// en més d'una variació en un producte, stock, unitats venudes i ref deixa de funcionar
		// si vinc del cart no agafo el ref per defecte sino el ref de lavariacio concreta, ja es calcula a ProductCart->get_variations_rs
		if(count($results)== 1 && $rs['variation_ref'] && $this->parent != 'ProductCart') $ret['ref'] = $rs['variation_ref'];
		return $ret;
	}
	// si el producte té variacions, l'estoc es la suma de tots els estocs de les variacions
	// NO ESTA BEEEEE  -> agafo el minim de last_stock_units_public, no té gaire sentit, no hauria d'estar a variacions, ja que sino hauria de mostrar un avís per cada variació del producte i no val la pena
	function get_stock($product_id, &$stock, &$last_stock_units_public){
		$query = "
			SELECT SUM(variation_stock) as stock, MIN(variation_last_stock_units_public) as last_stock_units_public
			FROM product__product_to_variation
			INNER JOIN product__variation USING(variation_id)
			WHERE product_id = '" . $product_id . "'
			AND BIN = 0
			GROUP BY product_id";
		$rs = Db::get_row($query);
		if ($rs) {
			$stock = $rs['stock'];
			$last_stock_units_public = $rs['last_stock_units_public'];
		}
		//debug::p($rs);
	}
	// defineixo els tipus de other que hi han a variacions
	function set_variation_other_config() {
		$cg = &$this->config;
		for ($i = 1; $i <= 4; $i++) {			
			$this->caption['c_variation_others'.$i] = $cg['variation_others'.$i];
		}		
	}

	function get_variation_other_language(&$rs) {
		
		$cg = &$this->config;
		// defineixo els tipus de variation other que hi han
		for ($i = 1; $i <= 4; $i++) {				

				$type = $cg['variation_others'.$i.'_type'];
				
				if ($type == 'text') {
					$rs['variation_others'.$i] = Db::get_first(
						"SELECT variation_others".$i . " 
							FROM product__product_to_variation_language
							WHERE language = '".LANGUAGE."'
							AND product_variation_id = ".$rs['product_variation_id'].""
					);
				}
		}
	}
	function get_variations( $product_id, &$pvp, &$old_price, &$ref, $discount, $discount_fixed_price, $discount_wholesaler, $discount_fixed_price_wholesaler, $is_form ){
		$base_price = $pvp;
		
		$this->set_variation_other_config();
		
		// Si es distribuidor, agafo increment del preu de distribuidor
		$is_wholesaler = $this->is_wholesaler();
		$which_pv = $is_wholesaler?'variation_pvd'.$this->get_wholesaler_rate():'variation_pvp'; 
		
		$query = "
			SELECT 
				product_variation_id, product__variation.variation_id as variation_id,
				product__product_to_variation.".$which_pv." as variation_pvp, 
				variation, 
				variation_subtitle, 
				variation_description, 
				product__product_to_variation.variation_default as variation_default, 
				variation_category_id, 
				variation_ref,
				variation_status,
				variation_others1, 
				variation_others2, 
				variation_others3,
				variation_others4
			FROM product__product_to_variation
			INNER JOIN product__variation USING (variation_id)
			INNER JOIN product__variation_language USING(variation_id)
			WHERE product__product_to_variation.variation_id = product__variation.variation_id
			AND BIN = 0
			AND variation_status != 'disabled'
			AND language = '" . LANGUAGE . "'
			AND product_id = " . $product_id . "
			ORDER BY 
				variation_category_id ASC, 
				product__variation.ordre ASC, 
				product__variation_language.variation ASC";
		$results = Db::get_rows($query);
		if (!$results) return '';		
		
		$variations = array();
		// comprovo que hi hagi algun default en la categoria
		foreach ($results as $rs){
			$vs = &$variations[$rs['variation_category_id']];
			// el primer trobat a la categoria
			if (!isset($vs['category_has_default'])) {
				$vs['category_has_default'] = false;
				$vs['category_has_onsale'] = false;
			}
			$vs['category_has_default'] = ($vs['category_has_default'] || $rs['variation_default'])?true:false;
			$vs['category_has_onsale'] = ($vs['category_has_onsale'] || $rs['variation_status'] == 'onsale')?true:false;
		}
		// faig els llistats de variacions pel formulari
		debug::add('Variacions Query',$query);
		debug::add('Variacions',$results);
		
		$conta = 0;
		$count = count($results);
		
		foreach ($results as $rs){
			
			$this->get_variation_other_language($rs);
			
			extract($rs);
			$checked  = false;
			$disabled = $variation_status == 'onsale' ? '' : 'disabled';
			$title    = htmlspecialchars( $variation_status == 'nostock' ? $this->caption['c_nostock'] : $variation );

			$vs = &$variations[$variation_category_id];
			
			// el primer trobat a la categoria
			if (!isset($vs['variations'])) {
				$vs['variations']='';
				$vs['variation_ids'] = [];
				$vs['variation_hiddens']='';
				$vs['variation_base_price']=$base_price;
				$vs['variation_category'] = Db::get_first(
				"SELECT variation_category 
				FROM product__variation_category_language 
				WHERE variation_category_id = '" . $variation_category_id . "'
				AND language = '" . LANGUAGE . "'");
				// si la categoria no te cap default poso el primer que es aquest
				if (!$vs['category_has_default']) $variation_default = '1';
			}

			$old_variation_pvp = $variation_pvp;
			$variation_pvp = $this->get_variation_discount($variation_pvp, $discount, $discount_fixed_price,  $discount_wholesaler, $discount_fixed_price_wholesaler );

			// He trobat el default, augmento el preu del prod. base si cal, cambio la ref
			// agafo nomès el primer default amb la variable'found_variation_default' ( en cas que n'hi hagin més d'un )
			if (!isset($vs['found_variation_default']) && $variation_default){
				$vs['found_variation_default'] = $product_variation_id; // enlloc de true ja poso l'id del que ha trobat
				if ($old_price) {
					$old_price = unformat_decimal($old_price,false)+$old_variation_pvp;
					$old_price = ($this->config['format_price_as_currency'])?
										format_currency($old_price):
										format_decimal($old_price);
				}
				$pvp += $variation_pvp;
				$checked = true;			
				// en més d'una variació en un producte, stock, unitats venudes i ref deixa de funcionar
				if(count($variations)== 1 && $rs['variation_ref']) $ref = $rs['variation_ref'];
			}



			$vs['variation_ids'][] = array(
				'variation_id' => $variation_id,
				'product_variation_id' => $product_variation_id,
				'variation' => $variation,
				'variation_subtitle' => $variation_subtitle,
				'variation_description' => $variation_description,
				'variation_price' => $this->config['format_price_as_currency']?format_currency($base_price + $variation_pvp):format_decimal($base_price + $variation_pvp),
				'variation_ref' => $variation_ref,
				'variation_others1' => $variation_others1,
				'variation_others2' => $variation_others2,
				'variation_others3' => $variation_others3,
				'variation_others4' => $variation_others4,
				'variation_conta' => $conta,
				'variation_count' => $count,
				'variation_first_last' => $conta==0?'first':($count==$conta+1?'last':''),
				'variation_checked' => $checked,
				'variation_nostock' => $variation_status == 'nostock',
				'variation_on_click' => $variation_status == 'nostock' ? '': " onclick='product_set_price($base_price,$variation_category_id, $product_variation_id )'",
				'variation_status' => $variation_status )
					+
				UploadFiles::get_record_images($variation_id, true, 'product', 'variation', 'variation_');
			
				
			$vs['variation_hiddens'] .= '<input type="hidden" data-variation-old-price="'.$old_variation_pvp .'" data-variation-ref="'.htmlspecialchars($variation_ref) .'" name="variation_pvp['.$product_variation_id.']" value="'.$variation_pvp .'" />';
			
			$vs['variation_category_id'] = $variation_category_id;
			if ($this->config['variations_type']=='radios')
			{
				if ($checked) $checked = ' checked="checked"';

				$class = $checked?'1':0;
				$on_click = $disabled ? '' : 'onclick="product_set_price('.$base_price.')"';

				$vs['variations'] .= '
					<div ' . $on_click . '>
						<input type="hidden"  data-variation-old-price="'.$old_variation_pvp .'"
			            data-variation-ref="' . htmlspecialchars($variation_ref) .'" 
				        name="variation_pvp['. $product_variation_id.']" 
				        value="' . $variation_pvp .'" />
				        <label title="' . $title .'"
						class="active' . $class . ' ' . $variation_status .'">
					        <input type="radio" 
					        name="variation['. $variation_category_id.']" 
					        id="variation_'. $product_variation_id.'" 
					        value="' . $product_variation_id .'"' .
							$disabled . $checked . ' />' .	$variation . '
				        </label>
			        </div>';
			}
			elseif ($this->config['variations_type']=='divs')
			{
				$class = $checked?'1':0;

				$on_click = $disabled ? '' : 'onclick="product_set_price('.$base_price.','.$variation_category_id.','.$product_variation_id.')"';

				$vs['variations'] .= '
					<div id="product_variation_'.$product_variation_id.'" ' .
					$on_click. ' 
					title="' . $title .'"
					class="active' . $class . ' ' . $variation_status .'">
						<input type="hidden" 
							data-variation-old-price="'.$old_variation_pvp .'"
							data-variation-ref="' . htmlspecialchars($variation_ref) .'" 
							name="variation_pvp['. $product_variation_id.']" 
							value="' . $variation_pvp .'" />
						<span>' .
							$variation .'
						</span>
					</div>';
			}
			else{
				if ($checked) $checked = ' selected';
				$vs['variations'] .= "<option title=\"$title\" class='$variation_status' value=\"{$product_variation_id}\" $checked $disabled />$variation</option>";
			}
			$conta++;
		}
		
		$conta = 0;
		$count = count($variations);
		$list_sufix = $is_form?'':"_$product_id";

		foreach($variations as $key=>$val){

			// Si no te cap onsale, trec la variació, no té sentit un desplegable que no es pugui triar res
			if (!$variations[$key]['category_has_onsale']){
				unset ($variations[$key]);
				continue;
			}


			$category_default_hidden = '<input type="hidden" name="variation['.$val['variation_category_id'].']" id="variation_'.$val['variation_category_id'].$list_sufix.'" value="'.$val['found_variation_default'].'" />';

			if ($this->config['variations_type']=='radios')
			{	
				$variations[$key]['variations'] = '<div class="variations">' . $variations[$key]['variations'] . '</div>';
			}
			elseif ($this->config['variations_type']=='divs')
			{
				$variations[$key]['variations'] = '<div id="variation_category_holder_' . $val['variation_category_id'] . '" class="variations">' . $variations[$key]['variations'] . $category_default_hidden . '</div>';
			}
			else{			
				$variations[$key]['variations'] = '<div class="variations">' . $val['variation_hiddens'] . '
					<select onchange="product_set_price('.$base_price.')" name="variation['.$val['variation_category_id'].']" id="variation_'.$val['variation_category_id'].'">
					' . $val['variations'] . '
					</select></div>';				
			}	

			// Afegeixo hidden de la categoria per utilitzar-los en les variacions 'custom', com a valor el default de la categoria
			// per poder calcula al js product_set_price
			$variations[$key]['variation_hiddens'] .= $category_default_hidden;
			
			$variations[$key]['variation_category_first_last'] = $conta==0?'first':($count==$conta+1?'last':'');
			$variations[$key]['variation_category_conta'] = $conta;
			$variations[$key]['variation_category_count'] = $count;
			
			$conta++;
			
			// no fan falta al tpl
			//unset($variations[$key]['hiddens']);
			//unset($variations[$key]['variation_category_id']);
		}
		return $variations;
	}
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()	
	{
		
		$this->set_other_config();
		
		// els descomptes s'apliques a totes les variacions
		$show = new ShowForm($this);

		if (show_preview()){
			// Al formulari es veuen tots, arxivades també
		}
		else {
			$show->condition = "(status = 'onsale' OR status = 'nostock' OR status = 'nocatalog') ";
		}
		$show->get_values();
		
		
		// per poder previsualitzar desde admin i enviar desde newsletter
		if (isset($_GET['loginnewsletter']) 
				&& $_GET['loginnewsletter'] == 'letndmarc'
				&& $_GET['passwordnewsletter'] == '5356aopfqkiios5cnoqws90se'
		)
		{
			if (is_file(PATH_TEMPLATES . 'newsletter/product_form.tpl')) 
					$show->template = 'newsletter/product_form';
			
		}
		
		
		if ($show->has_results) {
			$rs = &$show->rs;

			$this->get_specs( $show );
			
			$show->set_var('show_family_id',$show->rs['family_id']);	
			
			// pel block de productes visitats
			if (isset($_SESSION['visited_products'][$show->id])) unset($_SESSION['visited_products'][$show->id]);
			$_SESSION['visited_products'][$show->id] = $show->id;
			
			$rs = array_merge(
				$rs,
				$this->list_records_walk($show,$rs['stock'],$rs['last_stock_units_public'],$rs['pvp'], $rs['pvd'.$this->get_wholesaler_rate()], $rs['discount'], $rs['discount_fixed_price'], $rs['discount_wholesaler'], $rs['discount_fixed_price_wholesaler'], $rs['price_consult'], $rs['product_id'], $rs['status'], $rs['sell'], $rs['brand_id'],true)
				);
			
			// obtinc els detalls en un array "details"
			$this->set_details($show);
			
			// El get_variations també modifica el pvp i el old_price segons la variació que hi ha per defecte
			$show->set_var('variations', $this->get_variations($show->id,$rs['pvp'],$rs['old_price'],$rs['ref'], $rs['discount'], $rs['discount_fixed_price'], $rs['discount_wholesaler'], $rs['discount_fixed_price_wholesaler'], true));
			
			$familia = $rs['subfamily_id'] ?: $rs['family_id'];
			$familia = $rs['subsubfamily_id'] ?: $familia;

			$this->check_private_family( $familia );

			$this->give_me_information_family($familia, $show, false); // nomès pel breadcrumb
			
			$ret = $show->show_form();

			// es pot mostrar el camp que digui a la configuració com a títol
			if ($this->is_index) {
				
				if ($this->config['show_as_title']){
					Page::set_title($show->rs[$this->config['show_as_title']]);
				}
				if ($this->config['show_as_subtitle']) {
					Page::set_body_subtitle($show->rs[$this->config['show_as_subtitle']]);
				}
				
				// SEO		
				Page::set_page_title($show->rs['page_title'],$GLOBALS['gl_page']->title);
				Page::set_page_description($show->rs['page_description'],$show->rs['product_description']);
				Page::set_page_keywords($show->rs['page_keywords']);
					
				Page::add_breadcrumb($show->rs['product_title'],'');

				Page::set_page_og($show->rs);
				
			}
			
			return $ret;
		}
		else{
			Main::error_404('El producte no existeix o no està públic');
		}

	}
	
	function set_details(&$show){
		// Obtenir valors i array per elbucle de detalls
		$rs = &$show->rs;
		
		/* TOTS ELS CAMPS DE LA TAULA
		$fields = array(	
			'product_id', 'ref', 'ref_number', 'ref_supplier', 'ean', 'product_private', 'family_id', 'subfamily_id', 'pvp', 'pvd', 'price_consult', 'tax_id', 'destacat', 'discount', 'discount_fixed_price', 'price_unit', 'brand_id', 'sell', 'guarantee', 'guarantee_period', 'year', 'observations', 'status', 'home', 'free_send', 'stock', 'sells', 'ordre', 'videoframe', 'format', 'power', 'alimentacion', 'weight', 'weight_unit', 'height', 'height_unit', 'temperature', 'temperature_unit', 'util_life', 'util_life_unit', 'color_id', 'material_id', 'certification', 'others1', 'others2', 'others3', 'others4', 'bin'
		);
		*/
		$fields = array(	
			'brand_id', 'guarantee', 'year', 'format', 'power', 'alimentacion', 'weight', 'height', 'temperature', 'util_life', 'color_id', 'material_id', 'certification', 'handmade', 'others1', 'others2', 'others3', 'others4', 'others5', 'others6', 'others7', 'others8','others9','others10','others11','others12','others13','others14','others15','others16','others17','others18','others19','others20'
		);
		
		$details = array();
		$is_odd = false;
		foreach($fields as $field)
		{
			if (isset($rs[$field]) && $rs[$field] && $rs[$field]!='0000') // l'any pot ser '0000' quan no s'ha entrat cap
			{
				$is_odd=$is_odd?false:true; // intercanviio el valor de is_odd
				
				if ($this->fields[$field]['type'] == 'checkbox' && $rs[$field]){
					 $rs[$field] = $this->caption['c_yes'];
				}
				
				$new_array = array('c_detail' => $show->caption['c_'.$field],'detail' => &$rs[$field],'units' => '','is_odd' => $is_odd);
				
				// si el camp te unitat li poso enla variable units
				$units = (isset($rs[$field.'_unit']))?$field.'_unit':'';
				$units = (isset($rs[$field.'_period']))?$field.'_period':$units;
				if ($units) $new_array['units'] = &$rs[$units];
				
				$details [$field]= $new_array;
			}
		}
		debug::add('details',$details);
		$rs['details'] = $details;
	}

	function get_specs(&$show) {

		if ($this->config['is_specs_on'] == '0') return;

		include_once( DOCUMENT_ROOT . 'admin/modules/product/product_common_spec.php' );

		$loop = ProductCommonSpec::show_specs($show->id);

		// netejo els que estan buits
		foreach ( $loop as $key => $val ) {
			if (!$val['specs'])
				unset ( $loop[$key]);
		}

		$show->set_loop( 'spec_categorys', $loop );

	}
	function write_record()
	{
		$writerec = new SaveRows($this);
		$writerec->save();
	}

	function manage_images()
	{
		$image_manager = new ImageManager($this);
		$image_manager->execute();
	}
	function is_wholesaler(){
		return (isset($_SESSION['product']['customer_id']) && isset($_SESSION['product']['mail']) && isset($_SESSION['product']['is_wholesaler']) && $_SESSION['product']['is_wholesaler']);
	}
	
	function get_wholesaler_rate(){
		if (isset($_SESSION['product']['customer_id'])){
			$query = "SELECT wholesaler_rate FROM product__customer WHERE customer_id = " . $_SESSION['product']['customer_id'];
			$ret = Db::get_first($query);
			return $ret==1?'':$ret;
		}
		else{
			return false;
		}
	}
	

	/**
	 * ProductCustomer::is_logged()
	 * Saber si està loguejat
	 * @return
	 */
	function is_logged(){
		// miro si hi ha les 2 sessions per més seguretat de que esta logged
		return (isset($_SESSION['product']['customer_id']) && isset($_SESSION['product']['mail']));
	}
	
	
	// defineixo els tipus de other que hi han

	function set_other_config() {

		$keys = [ 'others' => 20, 'others_private' => 3 ];

		// defineixo els tipus de other que hi han

		foreach ($keys as $k => $v) {
			for ($i = 1; $i <= $v; $i++) {
				if (!empty ($this->config[$k.$i])) {

					$type = $this->config[$k.$i.'_type'];

					$this->set_field($k.$i, 'form_admin', 'input');
					$this->set_field($k.$i, 'form_public', 'text');
					$this->set_field($k.$i, 'list_public', 'text');
					$this->set_field($k.$i,'change_field_name',  'product__product.' . $k .$i . ' AS ' . $k.$i);
					$this->set_field($k.$i, 'type', $type);

					$this->caption['c_' . $k . $i] = $this->config[$k.$i];

					if ($type == 'text') {
						$this->language_fields[] = $k.$i;
						$this->set_field($k.$i,'change_field_name',  'product__product_language.' . $k .$i . ' AS ' . $k.$i);
						$this->set_field($k.$i, 'text_maxlength', '2000');
					}
					elseif ($type == 'text_no_lang') {
						$this->set_field($k.$i, 'type', 'text');
						$this->set_field($k.$i, 'text_maxlength', '2000');
					}
					elseif ($type == 'int') {
						$this->set_field($k.$i, 'type', 'int');
						$this->set_field($k.$i, 'text_maxlength', '100');
					}
					elseif ($type == 'textarea') {
						$this->language_fields[] = $k.$i;
						$this->set_field($k.$i,'change_field_name',  'product__product_language.' . $k .$i . ' AS ' . $k.$i);
						$this->set_field($k.$i, 'textarea_rows', '4');
						$this->set_field($k.$i, 'textarea_cols', '50');
						$this->set_field($k.$i, 'text_maxlength', '2000');
					}
					elseif ($type == 'checkbox') {
						$this->set_field($k.$i, 'select_fields', '0,1');
					}
				}
			}
		}
	}



	/**
	 * Funció per poder obtindre varis specs de un registre separats per $sep, per poder-se utilitzar des de el llistat
	 * Ej: ProductProduct::get_listed_specs ( $id, '10,20,30', ' ');
	 *
	 * @param $product_id integer Id del producte
	 * @param $spec_ids string Ids dels specs separats per comes
	 * @param $sep string Separador dels specs
	 *
	 * @return string
	 */
	static public function get_listed_specs( $product_id, $spec_ids, $sep ) {

		include_once( DOCUMENT_ROOT . 'admin/modules/product/product_common_spec.php' );

		return ProductCommonSpec::get_listed_specs( $product_id, $spec_ids, $sep );
	}

	/**
	 * Per obtindre els specs d'una categoria en concret
	 *
	 * @param $spec_categorys
	 * @param $spec_category_id
	 *
	 * @return array
	 */
	static public function get_specs_category( $spec_categorys, $spec_category_id ) {

		include_once( DOCUMENT_ROOT . 'admin/modules/product/product_common_spec.php' );

		return ProductCommonSpec::get_specs_category( $spec_categorys, $spec_category_id );
	}

	private function get_family_private_condition(){

		if (!$this->is_index || !$this->config['has_private_familys']) return '';

		Main::load_class( 'product', 'family' );
		return ProductFamily::get_family_private_condition();

	}

	private function check_private_family( $family_id ){

		if (!$this->is_index || !$this->config['has_private_familys']) return;

		Main::load_class( 'product', 'family' );
		ProductFamily::check_family_private( $family_id );

	}
}