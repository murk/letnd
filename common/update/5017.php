<?

/* 8-5-13 
  Més d'una plantilla per l'aparador i album de immo

 */

if (is_table('inmo__property')) {
	
	$update_sql[] = "
		CREATE TABLE `print__template` (
			`template_id` INT(11) NOT NULL AUTO_INCREMENT,
			`file_name` VARCHAR(100) NOT NULL DEFAULT '' COLLATE 'utf8_spanish2_ci',
			`type` enum('showwindow','album') NOT NULL DEFAULT 'showwindow' COLLATE 'utf8_spanish2_ci',
			PRIMARY KEY (`template_id`)
		)
		COLLATE='utf8_spanish2_ci'
		ENGINE=MyISAM;
		";	
	
	
	$update_sql[] = "
		CREATE TABLE `print__template_language` (
			`template_id` INT(11) NOT NULL DEFAULT '0',
			`language` VARCHAR(3) NOT NULL DEFAULT '' COLLATE 'utf8_spanish2_ci',
			`template` VARCHAR(100) NOT NULL DEFAULT '' COLLATE 'utf8_spanish2_ci',
			`description` VARCHAR(255) NOT NULL DEFAULT '' COLLATE 'utf8_spanish2_ci'
		)
		COLLATE='utf8_spanish2_ci'
		ENGINE=MyISAM;
		";	
	
	
	$update_sql[] = "
		INSERT INTO `inmo__configadmin` (`name`) VALUES ('showwindow_template_id');
		";	
	$update_sql[] = "
		INSERT INTO `inmo__configadmin` (`name`) VALUES ('album_template_id');
		";	
	
}


?>