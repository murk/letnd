<? if (!1):?>
<link href="/public/themes/plantilla2/styles/general.css" rel="stylesheet" type="text/css" />
<? endif //$$?>
<div class="pageResults"><div class="left"><?=$split_results_count?></div><div class="right"><?=$split_results?></div></div>
<? foreach($loop as $l): extract ($l)?>
    <div class="ref">
      <a href="?<?=$base_link?>&action=show_record&property_id=<?=$property_id?>&language=<?=$language?>" class="ref"><?=$c_ref?> <?=$ref?></a>  </div>
  <? if ($property):?>
    <div class="property">      
      <?=$property?>
    </div>
  <? endif //property?>
  <? if ($property_title):?>
    <div class="property_title">
      <?=$property_title?>
    </div>
  <? endif //property_title?>
  <? if ($description):?>
    <div class="description">
      <?=$description?>
    </div>
  <? endif //description?>
  <? if ($price):?>
    <div class="price">
    <?=$c_price?>:
    <span><?=$price?>
    <?=CURRENCY_NAME?></span>
  </div>
  <? endif //price?>
  <? if ($price_consult):?>
    <div class="price">
    <span><?=$price_consult?></span>
    </div>
  <? endif //price_consult?>
  <? if ($status):?>
    <div class="sold">
    <?=$status?>
    </div>
  <? endif //status?>
<? if ($images):?>
  <table border="0" cellspacing="0" cellpadding="0" class="images_list">
    <tr>
      <td valign="top"><img src="/public/themes/plantilla2/images/images_list_bg1.gif" width="15" height="123" /></td>
      <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td background="/public/themes/plantilla2/images/images_list_bg4.gif"><img src="/public/themes/plantilla2/images/images_list_bg3.gif" width="4" height="5" /></td>
          <td align="right" background="/public/themes/plantilla2/images/images_list_bg4.gif"><img src="/public/themes/plantilla2/images/images_list_bg5.gif" width="4" height="5" /></td>
        </tr>
      </table><div class="images_list"><? foreach($images as $image): extract ($image)?><div class="images_list_middle"><a href="?<?=$base_link?>&action=show_record&property_id=<?=$property_id?>&language=<?=$language?>" ><img src="<?=CLIENT_DIR?>/inmo/property/images/<?=$image_name_thumb?>" width="180" height="135" border="0" /></a></div>
               <? if ($image_conta==2) break; //images?><? endforeach //images?><div class="cls"></div></div><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td background="/public/themes/plantilla2/images/images_list_bg7.gif"><img src="/public/themes/plantilla2/images/images_list_bg6.gif" width="4" height="5" /></td>
          <td align="right" background="/public/themes/plantilla2/images/images_list_bg7.gif"><img src="/public/themes/plantilla2/images/images_list_bg8.gif" width="4" height="5" /></td>
        </tr>
      </table></td>
      <td valign="top"><img src="/public/themes/plantilla2/images/images_list_bg2.gif" width="16" height="123" /></td>
    </tr>
  </table>
<? endif //images?>
<? endforeach //$loop?>
<div class="pageResults"><div class="left"><?=$split_results_count?></div><div class="right"><?=$split_results?></div></div>
<div style="padding-bottom:20px"></div>
