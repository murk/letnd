<?
/**
 * YesescalaCurs
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2010
 * @version $Id$
 * @access public
 */
class YesescalaCurs extends Module{
	function __construct(){
		parent::__construct();
	}
	function on_load(){
		
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function get_records()
	{
	    $listing = new ListRecords($this);
		
		
		$listing->call('records_walk','content,content_list', true);

	
		
		$listing->add_filter('category_id');
		
		$listing->add_filter('status');
		
		// 
		
		if (isset($_GET['in_home']) && $_GET['in_home']!='null') {
			$listing->set_field('in_home','list_admin','input');
		}
		
		$listing->order_by = 'ordre ASC, curs ASC';	
	    return $listing->list_records();
	}
	function records_walk(&$listing, $content=false,$content_list=false){		
				
		// si es llistat passa la variable content
		if ($content!==false){
			$ret['content']=$content_list?$content_list:add_dots('content', $content);
		}
		
		return $ret;
	}
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{
	    
		$this->set_field('entered','default_value',now(true));
		$this->set_field('modified','default_value',now(true));
		
		$show = new ShowForm($this);
		
		$show->set_form_level1_titles('curs','url1_name','file','page_title');
		$show->set_form_level2_titles('entered');
		
	    return $show->show_form();
	}

	function save_rows()
	{
	    $save_rows = new SaveRows($this);
	    $save_rows->save();
	}

	function write_record()
	{
	    $writerec = new SaveRows($this);
		$writerec->set_field('modified','override_save_value','now()');			
		$writerec->field_unique = "curs_file_name";
		$writerec->save();
	}

	function manage_images()
	{
	    $image_manager = new ImageManager($this);
	    $image_manager->execute();
	}
	
}
?>