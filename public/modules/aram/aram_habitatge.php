<?
/**
 * AramHabitatge
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2012
 * @version $Id$
 * @access public
 */
class AramHabitatge extends Module{
	var $monthly_count;
	function __construct(){
		parent::__construct();
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function get_records()
	{
	    $this->set_floors();
		$listing = new ListRecords($this);
		$listing->paginate = false;
	    $listing->order_by = ('escala asc, floor_id asc, flat asc');
		$listing->condition = "(status = 'public' OR status = 'sold')";
		$listing->set_field('floor_id','type','none');
		$listing->set_field('m2','type','int');
		$listing->set_field('m2_exterior','type','int');
		$listing->set_field('price','type','int');
		$listing->set_field('reserva','type','int');
		$listing->set_field('contracte','type','int');
		$listing->set_field('claus','type','int');
		$listing->set_field('quota_subsidiada','type','int');
		$listing->set_field('quota_subsidiada2','type','int');
		$listing->list_records(false);
		
		foreach ($listing->results as $key=>$val){
			$rs = &$listing->results[$key];
			
			$rs['visible'] = $rs['habitatge_id']=='1'?' visible':'';
			$floor_num = Db::get_first('SELECT floor_num FROM aram__floor WHERE floor_id='.$rs['floor_id']);
			$rs['floor_id'] = $this->caption['c_floor_' . $floor_num];
			$this->monthly_count = $this->get_months_diff($rs['reserva'],$rs['contracte']);
			$this->round($rs, array('m2','m2_exterior','price','reserva','contracte','claus','quota_subsidiada','quota_subsidiada2'));
			$this->get_hipoteca($rs);
			$this->get_pagament($rs);
		}
		
	    return $listing->parse_template();
	}
	function round(&$rs, $fields){
		foreach ($fields as $f){
			$rs[$f] = round($rs[$f]);
		}
	}
	function set_floors(){
		$query = "SELECT  aram__floor.floor_id as floor_id, floor_num, floor
					FROM aram__floor,aram__floor_language
					WHERE aram__floor.floor_id = aram__floor_language.floor_id
					AND language = '".LANGUAGE."'
					ORDER by ordre ASC, floor_num ASC";
		$results = Db::get_rows($query);
		foreach ($results as $key => $val){
			$rs = &$results[$key];
			$rs['floor_selected'] = '';
			$files = UploadFiles::get_record_files($rs['floor_id'], 'file', 'aram', 'floor',true);
			if ($files) $rs +=$files[0];
			else {
				$rs['file_name'] = false;
			}
			
			// agafo els habitatges de cada planta
			$query = "SELECT habitatge_id, coordenades, coordenades2, coordenades3, status FROM aram__habitatge 
						WHERE aram__habitatge.bin = 0 
						AND (status = 'public' OR status = 'sold')
						AND floor_id = ".$rs['floor_id']."
						AND coordenades != ''
						ORDER BY escala asc, floor_id asc, flat asc";
			$rs['loop_habitatges'] = Db::get_rows($query);
			foreach ($rs['loop_habitatges'] as $key => $val){
				$r = &$rs['loop_habitatges'][$key];
				$r['status'] = ($r['status']=='sold')?'sold'.LANGUAGE:'';
				$coords = explode(',',$r['coordenades']);
				$r['coordenades'] = 
					'left:'.$coords[0].'px;
					top:'.$coords[1].'px;
					width:'.($coords[2]-$coords[0]).'px;
					height:'.($coords[3]-$coords[1]).'px;';
				$r['top_sold'] = (($coords[3]-$coords[1])/2)-10;
				
				if ($r['coordenades2']){
				$coords2 = explode(',',$r['coordenades2']);
				$r['coordenades2'] = 
					'left:'.($coords2[0]-$coords[0]).'px;
					top:'.($coords2[1]-$coords[1]).'px;
					width:'.($coords2[2]-$coords2[0]).'px;
					height:'.($coords2[3]-$coords2[1]).'px;';
				
				}
				
				if ($r['coordenades3']){
				$coords2 = explode(',',$r['coordenades3']);
				$r['coordenades3'] = 
					'left:'.($coords2[0]-$coords[0]).'px;
					top:'.($coords2[1]-$coords[1]).'px;
					width:'.($coords2[2]-$coords2[0]).'px;
					height:'.($coords2[3]-$coords2[1]).'px;';
				
				}
			}		
			
		}
		if ($results[0]) $results[0]['floor_selected']='selected';
		$this->set_var('loop_plantes',$results);
		//debug::p($results);
	}
	function get_hipoteca(&$rs){
		extract($rs);	
		
		if ($rs['status']=='sold'){
			$rs['hipoteca']=false;
			return;
		}	
		$monthly_count = $this->monthly_count;
		$interes = $this->config['interest_hipoteca'];
		
		//$hipoteca = $price - ( $reserva + $contracte + ( $monthly * $monthly_count ) + $claus );
		//$percent = $hipoteca/$price*100;
		
		$hipoteca = $price*0.8;
		$percent = 80;
		
		$quota_hipoteca = $hipoteca * ($interes/12) / (100 * ( 1 - pow( 1 + $interes/12/100,-25*12) ));
		
		$rs['hipoteca'] = format_int(round($hipoteca));
		$rs['percent'] = $percent;
		$rs['quota_hipoteca'] = format_int(round($quota_hipoteca));
		
	}
	function get_pagament(&$rs){
	
		$rs['is_sold'] = false;
		$rs['show_payment'] = $this->config['show_payment'];
		
		// si  al config i diu show_payment=0, no mostro el calendari de forma de pagament
		// si el pis es venut no mostro ni calendari ni hipoteca ni boto
		if (!$this->config['show_payment'] || $rs['status']=='sold'){
			if ($rs['status']=='sold') $rs['is_sold'] = true;
			$rs['loop_payment'] = array();
			return;
		}
		
		// així no s'ha de definir sempre dins el loop
		$rs['p_init_build']=false;
		$rs['p_init_payment']=false;
		$rs['p_end_build']=false;
		$rs['p_end_payment']=false;
		$rs['p_reserva_date']=false;
		$rs['p_claus_date']=false;
		
		$init_build = strtotime($this->config['init_date_build']);
		$init_payment = strtotime($this->config['init_date_payment']);
		$end_build = strtotime($this->config['end_date_build']);
		$end_payment = strtotime($this->config['end_date_payment']);
		$reserva_date = strtotime($this->config['reserva_date']);
		$claus_date = strtotime($this->config['claus_date']);
		
		$ds['init_build'] = getdate($init_build);
		$ds['init_payment'] = getdate($init_payment);
		$ds['end_payment'] = getdate($end_payment);
		$ds['end_build'] = getdate($end_build);
		if ($rs['reserva']) $ds['reserva_date'] = getdate($reserva_date);
		$ds['claus_date'] = getdate($claus_date);
		
		$loop = array();
		// faig l'array amb key any*100+mes = 200012 
		
		// construeixo semestre init_build, init_payment, end_build, end_payment, reserva_date, claus_date
		foreach ($ds as $key=>$d){		
			$rest = $d['mon']%6;
			if ($rest==0) $rest=6;
			$base = $d['mon']-$rest;
			//debug::p($rest,$key);
			//debug::p($base,'base');
			//debug::p($d['mon'],'mes');
			
			for ($i = 1; $i <= 6; $i++) {
				$k = $d['year']*100+($base+$i);
				$loop[$k]['p_quota'] = false;
				
				// poso a false tota la resta de dates
				foreach ($ds as $date_key=>$date_val){
				if (!isset($loop[$k]['p_'.$date_key])) $loop[$k]['p_'.$date_key] = false;}
				
				$loop[$k]['p_'.$key] = false;
				$loop[$k]['p_show_reserva'] = false;
				$loop[$k]['p_show_contracte'] = false;
				$loop[$k]['p_last'] = '';
				if (!isset($loop[$k]['p_empty']))
					$loop[$k]['p_empty'] = 'empty';
			}
			$k = $d['year']*100+$d['mon'];
			$loop[$k]['p_'.$key] = true;
			$loop[$k]['p_empty'] = 'full';
			$loop[$k]['p_show_reserva'] = false;
			$loop[$k]['p_show_contracte'] = false;
			$loop[$k]['p_date'] = $GLOBALS['month_names'][$d['mon']-1] . ' ' . $d['year'];		
		}
		
		/***************************************/
		// construeixo totes les quotes
		$year = $ds['init_payment']['year'];
		$month = $m = $ds['init_payment']['mon'];
		
		$total_months = $this->monthly_count+1; // sumo el mes de contracte
		//if ($rs['reserva']) $total_months ++; // reserva
		
		for ($i = $m; $i < $m+$total_months; $i++) {
			if ($month==13) {
				$year++;
				$month = 1;
			}
			$k = $year*100+$month;	
			
			$loop[$k]['p_quota'] = $rs['monthly'];
			$loop[$k]['p_empty'] = 'full';
			$loop[$k]['p_show_contracte'] = ($i==$m&&$rs['contracte']);			
			$loop[$k]['p_date'] = $GLOBALS['month_names'][$month-1] . ' ' . $year;
			$loop[$k]['p_last'] = '';
			
			// poso a false tota la resta de dates
			foreach ($ds as $date_key=>$date_val){
			if (!isset($loop[$k]['p_'.$date_key])) $loop[$k]['p_'.$date_key] = false;}
			
			if ($loop[$k]['p_show_contracte']) $loop[$k]['p_quota'] = false;
			
			$month++;
		}
		
		// l'ultima ha de ser active i sense quota
		//$loop[$k]['p_last'] = ' active';		
		//$loop[$k]['p_quota'] = false;		
		
		ksort($loop);
		$rs['loop_payment'] = $loop;
		//debug::p($loop);
	}
	// igual que la de admin ( el troç de calcular mesos 
	function get_months_diff($reserva,$contracte){
	
		$init_date = strtotime($this->config['init_date_payment']);//'2010-01-01';
		//$today_date = strtotime('now');//avui;		
		$end_date = strtotime($this->config['end_date_payment']);
		
		$init_date = getdate($init_date);
		$end_date = getdate($end_date);
		
		$year_start = $init_date['year'];
		$month_start = $init_date['mon'];
		$year_end = $end_date['year'];
		$month_end = $end_date['mon'];
		
		$years = $year_end-$year_start;
		$months = $month_end - $month_start;
		
		$months = ($years*12)+$months; // no sumo 1 per no contar el mes inici que es el de contracte
		
		//debug::p($init_date);
		//debug::p($end_date);
		//debug::p($months);
		//$months = $months-2; // resto el mes de contracte i pagament final
		
		return $months;
	}
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{
	}

	function save_rows()
	{
	}

	function write_record()
	{
	}

	function manage_images()
	{
	    $image_manager = new ImageManager($this);
	    $image_manager->execute();
	}
	function downloadfloor(){
		$this->download(true);
	}
	function download($is_floor=false){
		
		$id = R::id('id');
		if ($is_floor){		
		$rs = Db::get_row("SELECT file_id, name FROM aram__floor_file 
						WHERE floor_id = '".$id."' ORDER BY file_id LIMIT 1");
			$name = $rs['name'];
			$file = CLIENT_PATH . '/' . $this->tool . '/floor/files/' .UploadFiles::_get_store_name($rs['file_id'],$rs['name']);
		}
		else{
		$rs = Db::get_row("SELECT file_id, name FROM aram__habitatge_file 
						WHERE habitatge_id = '".$id."' ORDER BY file_id LIMIT 1");
			$name = $rs['name'];
			$file = CLIENT_PATH . '/' . $this->tool . '/' . $this->tool_section . '/files/' .UploadFiles::_get_store_name($rs['file_id'],$rs['name']);
		}
		
		if (file_exists($file) && !is_dir($file)) {
			// Download PDF as file
			//if (ob_get_contents()) {
				//$this->Error('Some data has already been output, can\'t send PDF file');
			//}
			header('Content-Description: File Transfer');
			//if (headers_sent()) {
				//$this->Error('Some data has already been output to browser, can\'t send PDF file');
			//}
			header('Cache-Control: public, must-revalidate, max-age=0'); // HTTP/1.1
			header('Pragma: public');
			header('Expires: Sat, 26 Jul 1997 05:00:00 GMT'); // Date in the past
			header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
			// force download dialog
			header('Content-Type: application/force-download');
			header('Content-Type: application/octet-stream', false);
			header('Content-Type: application/download', false);
			header('Content-Type: application/pdf', false);
			// use the Content-Disposition header to supply a recommended filename
			header('Content-Disposition: attachment; filename="'.$name.'";');
			header('Content-Transfer-Encoding: binary');
			header('Content-Length: '.filesize($file));
			ob_clean();
			flush();
			readfile($file);
			die();
		}
	}
}
?>