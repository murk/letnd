<?
/**
 * NewsletterRemove
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 5/2013
 * @version $Id$
 * @access public
 */
class NewsletterRemove extends Module{
	function __construct(){
		parent::__construct();
	}
	function list_records()
	{
		$content = '';
		unset($_GET['language']);
		unset($_GET['tool']);
		unset($_GET['tool_section']);
		
		if (count($_GET) >1) die('die');
		
		foreach ($_GET as $key=>$val){
			// key es el codi
		}
		
		
		$confirmed = R::escape('confirm',false,'_POST');		
		
		// comprovo que existeix
		$field = ($this->config['custumer_table']=='product__customer')?'surname':'surname1';
		$query = "SELECT mail, name, ".$field." as surname
						FROM " . $this->config['custumer_table'] . " 
						WHERE BINARY remove_key = '" . $key . "'
						AND removed = 0";

		$exists = Db::get_row($query);			
		if (!$exists) Main::redirect ('/');
		
		$this->set_file('newsletter/remove_form.tpl');
		$this->set_vars($this->caption);
		$this->set_var('mail', $exists['mail']);
		
		if ($confirmed){			
			
			$this->set_var('confirmed', true);
			
			// poso a removed
			$query = "UPDATE " . $this->config['custumer_table'] . " 
						SET removed = 1
						WHERE BINARY remove_key = '" . $key . "'";
			Db::execute($query);
			
			// envio mail a admin per notificar s'han borrat de la bbdd
			send_mail_admin(
				$this->caption['c_admin_subject'], 
				sprintf($this->caption['c_admin_body'],$exists['name'], $exists['surname'] ,$exists['mail'],  HOST_URL, HOST_URL)
				);
			
			
		}
		else{
			
			$this->set_var('confirmed', false);	
			if (USE_FRIENDLY_URL) {		
				$this->set_var('action', '/'.LANGUAGE.'/newsletter/remove/?'.$key);	
			}
			else{				
				$this->set_var('action', '?language='.LANGUAGE.'&tool=newsletter&tool_section=remove&'.$key);
			}				
		}
		
		$GLOBALS['gl_content'] = $this->process();
	}
}
?>