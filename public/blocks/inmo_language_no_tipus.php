<?php

// No fa un loop per cada tipus, només un sól loop com tots els altres filtres. Va bé per posar-ho amb checkboxes com a Finques Catalonia

$page_id = isset($vars['page_id']) ? $vars['page_id'] : false;

$vars['var_name'] = 'language_id';

$query = "SELECT Distinct user__user.user_id
				FROM inmo__property, user__user
				WHERE user__user.user_id = inmo__property.user_id
				AND inmo__property.bin = 0 AND  (inmo__property.status = 'onsale' OR inmo__property.status = 'sold' OR inmo__property.status = 'reserved')";

$query = "SELECT Distinct language, language_id
				FROM all__language
				WHERE language_id IN ( SELECT language_id FROM user__user_to_language WHERE user__user_to_language.user_id IN ( $query ) )";

$results = Db::get_rows($query);

if ( ! $results ) {
	$show_blocks = false;
}


$results = Db::get_rows($query);


$language_id = R::number('language_id');
$vars['block_selected'] = $language_id?1:0;

if ($results)
{
    foreach($results as $rs)
    {
        $language_id == $rs["language_id"]?$selected = 1:$selected = 0;


		// pel filtre
		if ( isset( $_GET['language_ids'] ) &&
		     in_array( $rs["language_id"], $_GET['language_ids'] )
		) {
			$selected = 1;
			$vars['block_selected'] = 1;
		}

       		
		$l['link'] = 
				get_link( array(
						'tool' => 'inmo', 
						'tool_section' => 'property',
						'friendly_params' => array('language_id'=>$rs['language_id']),
						'detect_page' => $page_id?false:true,
						'page_id' => $page_id
				));
		
		
        $l['selected'] = $selected;
		$l['value'] = $rs['language_id'];
        $l['item'] = $rs['language'];
        $loop2[] = $l;
    }
    $loop[] = array ('loop'=>$loop2,
	'item' => '');
}
?>