<?php
// si estic al carro no mostro el bloc de visited
global $gl_module;
$m = &$gl_module;
if ($m && $m->tool=='product' && ($m->tool_section=='cart' || $m->tool_section=='payment' || $m->tool_section=='customer')
|| isset($_GET['product_visited_more'])){
	$show_block = false;
	return;
}
// si no n'hi ha cap o estic a la pàgina de veure tots els productes visitats, donc no mostro el block
if (!isset($_SESSION['visited_products']) || isset($_GET['products_visited_more'])) {
	$show_block = false;
	return;
}

if (!defined('PRODUCT_VISITED_MAX_RESULTS')) Module::get_config_values('product');

//include_once(DOCUMENT_ROOT . 'public/modules/product/product_product_functions.php');
$products = array_reverse($_SESSION['visited_products']);

R::escape_array( $products );

$ids = implode(',', $products);


$module = Module::load('product','product');
$module->parent = 'Block';

$module->condition = 
	"AND product__product.product_id IN (" . $ids . ")";
	
$module->limit = '0,' . PRODUCT_VISITED_MAX_RESULTS;
$module->order_by = "FIELD(product__product.product_id, " . $ids . ")";

$loop = $module->do_action('get_records');

$vars['more'] = false;
if (count($_SESSION['visited_products']) > PRODUCT_VISITED_MAX_RESULTS)
{
	$vars['more'] = $gl_caption['c_product_list_view_all'];

	if (USE_FRIENDLY_URL) {

		$vars['more_link'] =
					get_link( array(
								'tool' => 'product',
								'tool_section' => 'product',
								'friendly_params' => array('product_visited_more'=>'true'),
								//'link_vars' => array('tipus'=>$rs['tipus_value']), // poden ser tot barrejats, per tant no pot anar a un tipus concret,
								'detect_page' => true
						));
	}
	else {

		$vars['more_link'] = '/?tool=product&amp;tool_section=product&amp;product_visited_more=true&amp;language=' . LANGUAGE;
	}
}
?>