<?
$page_file_name = Page::get_page(2, false, true);
$page_file_name = substr($page_file_name, 0, strpos($page_file_name,'.'));

$order_way = 'asc';

$orders = array(
	'price' => '',
	'buildyear' => '',
	'length' => '',
	'model' => ''
);
		
$loop = array();
		
foreach ($orders as $key => $val){
	
	$friendly_params_asc = array('orderby' => $key . '-asc');
	$friendly_params_desc = array('orderby' => $key . '-desc');
	$caption = &$GLOBALS['gl_module']->caption;
	
	$l = array();	
	$l['id'] = $key;
	$l['link_asc'] = Page::get_link(
		'nautica', 'embarcacion',false,false,$friendly_params_asc,array(), false, false, $page_file_name);
	$l['link_desc'] = Page::get_link(
		'nautica', 'embarcacion',false,false,$friendly_params_desc,array(), false, false, $page_file_name);
	$l['item'] = $caption['c_'.$key];
	$l['selected_asc'] = (isset($_GET['orderby']) && $_GET['orderby']==$key.'-asc')?1:0;
	$l['selected_desc'] = (isset($_GET['orderby']) && $_GET['orderby']==$key.'-desc')?1:0;
	$loop[] = $l;
	
}
?>