<link href="/admin/themes/inmotools/styles/image_drag.css" rel="stylesheet" type="text/css">
<!--[if IE 6]>

<![endif]-->
<!--[if IE 7]>
<style>
#sort
{
	clear: both;
	width: 100%;
	height: 100%;
}

li.sortableitemPlaceholder
{
	height: 0px;
}
</style>
<![endif]-->
<style>
.sortableitemHandle {
	width:<?=$im_admin_thumb_w?>px;
	height:<?=$im_admin_thumb_h?>px;
}
</style>
<script language="JavaScript" src="/admin/modules/inmo/jscripts/functions.js?v=<?=$version?>"></script>
<script language="JavaScript" src="/common/jscripts/jquery.dropshadow.js?v=<?=$version?>"></script>
<script type="text/javascript">
	var gl_gallery_images = <?= ImageManager::get_gallery_images($images) ?>;
	<?if ($results):?>
	<?else:?>
	image_uploader_link = '<?=$uploader_link?>';
	<?endif//results?>


	im_admin_thumb_w = '<?=$im_admin_thumb_w?>';
	im_admin_thumb_h = '<?=$im_admin_thumb_h?>';
	parent_id = '<?=$parent_id?>';

</script>
<script language="JavaScript" src="/admin/jscripts/image_drag.js?v=<?=$version?>"></script>
<div class="subMenu" id="showwindow" style="width:230px">
	<form action="/admin/?action=show_record_showwindow&property_id=<?=$property_id?>&menu_id=202" target="_blank" method="post">
		<table width="220" border="0" cellpadding="0" cellspacing="0">
			<?if($showwindow_template_id):?>
			<tr>
				<td valign="top"><strong>
					<?=$c_submenu_6?>:</strong></td>
				<td><?=$showwindow_template_id?></td>
			</tr>
			<?endif //showwindow_template_id?>
			<tr>
				<td valign="top"><strong>
					<?=$c_submenu_0?>:</strong></td>
				<td><?=$showwindow_language?></td>
			</tr>
			<!--<tr>
				<td valign="top"><strong>
					<?=$c_submenu_1?>:</strong></td>
				<td><?=$showwindow_size?></td>
			</tr>-->
			<tr>
				<td valign="top"><strong>
					<?=$c_submenu_2?>:</strong></td>
				<td><?=$showwindow_pages?></td>
			</tr>
			<tr>
				<td valign="top"><strong>
					<?=$c_submenu_3?>:</strong></td>
				<td><?=$showwindow_top_fields?></td>
			</tr>
			<tr>
				<td valign="top"><strong>
					<?=$c_submenu_4?>:</strong></td>
				<td>&nbsp;</td>
			</tr>
		</table>
		<table width="200" border="0" align="center" cellpadding="0" cellspacing="0">
			<tr>
				<td><?=$c_zone?></td>
				<td><?=$showwindow_show_zone?></td>
				<td><?=$c_comarca_id?></td>
				<td><?=$showwindow_show_comarca?></td>
			</tr>
			<tr>
				<td><?=$c_municipi_id?></td>
				<td><?=$showwindow_show_municipi?></td>
				<td><?=$c_provincia_id?></td>
				<td><?=$showwindow_show_provincia?></td>
			</tr>
		</table>
		<br />
		<table border="0" align="center" cellpadding="0" cellspacing="0">
			<tr>
				<td><input type="checkbox" name="save_config" id="save_config" /></td>
				<td>&nbsp;
					<?=$c_submenu_5?></td>
			</tr>
		</table>
		<br />
		<div class="centerFloat1">
			<div class="centerFloat2">
				<input type="submit" name="Submit" value="<?=$c_print?>" class="btn_form1" />
			</div>
		</div>
	</form>
</div>
<div class="subMenu" id="album" style="width:230px">
	<form action="/admin/?action=show_record_album&property_id=<?=$property_id?>&menu_id=202" target="_blank" method="post">
		<table width="220" border="0" cellpadding="0" cellspacing="0">
			<?if($album_template_id):?>
			<tr>
				<td valign="top"><strong>
					<?=$c_submenu_6?>:</strong></td>
				<td><?=$album_template_id?></td>
			</tr>
			<?endif //album_template_id?>
			<tr>
				<td valign="top"><strong>
					<?=$c_submenu_0?>:</strong></td>
				<td><?=$album_language?></td>
			</tr>
			<!--<tr>
				<td valign="top"><strong>
					<?=$c_submenu_1?>:</strong></td>
				<td><?=$album_size?></td>
			</tr>-->
			<tr>
				<td valign="top"><strong>
					<?=$c_submenu_2?>:</strong></td>
				<td><?=$album_pages?></td>
			</tr>
			<tr>
				<td valign="top"><strong>
					<?=$c_submenu_4?>:</strong></td>
				<td>&nbsp;</td>
			</tr>
		</table>
		<table width="200" border="0" align="center" cellpadding="0" cellspacing="0">
			<tr>
				<td><?=$c_zone?></td>
				<td><?=$album_show_zone?></td>
				<td><?=$c_comarca_id?></td>
				<td><?=$album_show_comarca?></td>
			</tr>
			<tr>
				<td><?=$c_municipi_id?></td>
				<td><?=$album_show_municipi?></td>
				<td><?=$c_provincia_id?></td>
				<td><?=$album_show_provincia?></td>
			</tr>
		</table>
		<br />
		<table border="0" align="center" cellpadding="0" cellspacing="0">
			<tr>
				<td><input type="checkbox" name="save_config" id="save_config" /></td>
				<td>&nbsp;
					<?=$c_submenu_5?></td>
			</tr>
		</table>
		<br />
		<div class="centerFloat1">
			<div class="centerFloat2">
				<input type="submit" name="Submit" value="<?=$c_print?>" class="btn_form1" />
			</div>
		</div>
	</form>
</div>
	<form method="post" action="<?=$link_action_edit_images?>" name="list" target="save_frame">
		<input name="action" type="hidden" value="save_rows_save_records_images<?=$action_add?>">
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="form">
		<tr>
			<td><?if ($show_tabs):?>
					<div id="form_buttons_holder"><table id ="form_buttons">
						<tr>
							<td valign="bottom" class="buttons">
							<?if ($results):?>
					<div class="right">	
							<a class="botoOpcions1" href="javascript:toggle_image_alt();">
								<span class="veure-titols"><?=$c_edit_titles_images?></span>
								<span class="amagar-titols"><?=$c_hide_titles_images?></span>
							</a>
							<input type="submit" name="Submit" value="<?=$c_send_edit?>" class="btn_form1" /><div class="btn_sep"></div>
						<a class="btn_form_delete" href="javascript:submit_list('save_rows_delete_selected_images<?=$action_add?>', '<?=$c_confirm_images_deleted?>', true)">
						<?=$c_delete_selected?>
						</a><a class="btn_form1" href="<?=$preview_link?>" target="_blank">
						<?=$c_preview?>
						</a><a href="/admin/?action=show_record_showwindow&amp;property_id=<?=$property_id?>&menu_id=202" target="_blank" class="btn_form1b" id="showwindow_button">
						<?=$c_showwindow?>
						</a><a onclick="show_options('#showwindow','#showwindow_down','#showwindow_button','left');" class="btn_form_down" id="showwindow_down"><img src="/admin/themes/inmotools/images/down.gif" width="7" height="5" border="0" /></a><a href="/admin/?action=show_record_album&amp;property_id=<?=$property_id?>&amp;menu_id=202" target="_blank" class="btn_form2">
							<?=$c_album?>
							</a><a href="javascript:show_options('#album','#album_down','#album_down','right');" class="btn_form_down" id="album_down"><img src="/admin/themes/inmotools/images/down.gif" width="7" height="5" border="0" /></a></div>
							<?endif //results?>
							
							<div class="menu">
							
							<a href="<?=$data_link?>"><?=$c_edit_data?></a>
							
							<div><?=$c_edit_images?></div>
						
							<?if($is_custumer_on):?>
							<a class="private" href="?menu_id=<?=$menu_id?>&property_id=<?=$property_id?>&action=list_records_custumer"><?=$c_edit_custumer?></a>
							<?endif //custumer_on?>

							<?if($is_history_on):?>
							<a class="private" href="?menu_id=<?=$menu_id?>&property_id=<?=$property_id?>&action=list_records_history"><?=$c_edit_history?></a>
							<?endif //is_history_on?>
			
							<a class="private" href="?menu_id=<?=$menu_id?>&property_id=<?=$property_id?>&action=show_form_map"><?=$c_edit_map?></a>					

							<?if($is_booking_on):?>
							<span id="tipus_temp_link" style="display:<?=$show_tipus_temp?>;">
								<a style="display:<?=$show_tipus_temp?>;" class="sep" href="?menu_id=<?=$menu_id?>&property_id=<?=$property_id?>&action=show_form_new&process=booking"><?=$c_edit_booking?></a>
								
								<a style="display:<?=$show_tipus_temp?>;" class="private" href="?menu_id=<?=$menu_id?>&property_id=<?=$property_id?>&action=show_form_edit&process=booking_propertyrange"><?=$c_edit_booking_season?></a>
							
								<a style="display:<?=$show_tipus_temp?>;" class="private" href="?menu_id=<?=$menu_id?>&property_id=<?=$property_id?>&action=show_form_edit&process=booking_extra"><?=$c_edit_booking_extra?></a>
							<span>
							<?endif //is_booking_on?>
						
							</div></td>
						</tr>
					</table></div>
					<?endif//show_tabs?>
				<table width="100%" cellpadding="4" cellspacing="0">
					<tr>
						<td class="formCaptionTitle"><strong>
							<?=$c_edit?>
							</strong>&nbsp;&nbsp;
							<?=$images_title?></td>
					</tr>
				</table>
					<table width="100%" border="0" cellspacing="0" cellpadding="0" class="formstbl">
						<tr>
							<td id="ajax_list"><br /><table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td width="20%" align="left" valign="middle" nowrap>&nbsp;
											<?=$split_results_count?></td>
										<td align="center" valign="middle" nowrap ><?=$split_results?></td>
										<td width="20%">&nbsp;</td>
									</tr>
								</table>
				<?if (!$results):?><div class="separador"></div><?endif //results?>
				<ul id="sort" class="sort">
									<? foreach($loop as $r): extract ($r)?>
										<? foreach($loop as $l): extract ($l)?>
											<? if ($image_src_admin_thumb):?>
												<li id="<?=$id?>" class="sortableitem">
							<div id="im_container_<?=$conta?>" class="selectableitem_out">                         
															<div id="im_bg_<?=$conta?>"><div class="sortableitemHandle"><img id="im_<?=$conta?>" src="<?=$image_src_admin_thumb?>"></div></div>
														
															<div id="select_bg_<?=$conta?>"><input name="<?=$id_field?>[<?=$id?>]" type="hidden" value="<?=$id?>"><?=$ordre?><input name="image_id_<?=$conta?>" type="hidden" value="<?=$id?>">
								<div class="small">
								<input name="selected[<?=$conta?>]" type="checkbox" value="<?=$id?>" onclick="checkbox_onclick(this);">      
																<?=$c_delete?>
																</div>
																<div class="small">
																<?=$main_image?>
																<?=$c_main_image?>
																</div>
								<?if ($imagecat_id):?>
                                <div class="small">
                                <?=$imagecat_id?>
                                </div>
								<?endif // imagecat_id?>
								<div class="image-alt">
									<?if ($show_seo):?>
											<div class="titol2">
												<?=$c_image_alt?>
											</div>
										<?foreach($image_alt as $l): extract ($l)?>
										<p>
										<?if($lang):?>
										<span><?=$lang?>:</span>
										<?endif //lang?>
										<?=$image_alt?>
										</p>
										<?endforeach //$image_title?>
									<?endif // show_seo?>
										<div class="titol2">
											<?=$c_image_title?>
										</div>
									<?foreach($image_title as $l): extract ($l)?>
									<p>
									<?if($lang):?>
									<span><?=$lang?>:</span><?endif //lang?>
									<?=$image_title?>
									</p>
									<?endforeach //$image_title?>
								</div>
							</div>
												</li>
												<? endif //image_src_admin_thumb?>
											<? endforeach //$loop?>
										<? endforeach //$loop?>
								<div class="cls"></div></ul>
</td>
						</tr>
					</table>
					<table width="100%" cellpadding="4" cellspacing="2" class="formsButtonstbl">
						<tr>
							<td align="center" class="formsButtons"><table border="0" cellspacing="0" cellpadding="0">
									<tr valign="middle">
										<td>
											<div class="centerFloat1">
												<div class="centerFloat2">
													<a class="botoOpcions1" href="javascript:toggle_image_alt();">
														<span class="veure-titols"><?=$c_edit_titles_images?></span>
														<span class="amagar-titols"><?=$c_hide_titles_images?></span>
													</a>
													<input type="submit" name="Submit" value="<?=$c_send_edit?>" class="btn_form1" /><div class="btn_sep"></div>
													<a class="btn_form_delete" href="javascript:submit_list('save_rows_delete_selected_images<?=$action_add?>', '<?=$c_confirm_images_deleted?>', true)">
													<?=$c_delete_selected?>
													</a><a class="btn_form1" href="<?=$preview_link?>" target="_blank">
													<?=$c_preview?>
													</a>
													<? if ($show_tabs):?>
														<a href="/admin/?action=show_record_showwindow&amp;property_id=<?=$property_id?>&menu_id=202" target="_blank" class="btn_form1b" id="showwindow_button2">
														<?=$c_showwindow?>
														</a><a onclick="show_options('#showwindow','#showwindow_down2','#showwindow_button2','left');" class="btn_form_down" id="showwindow_down2"><img src="/admin/themes/inmotools/images/down.gif" width="7" height="5" border="0" /></a><a href="/admin/?action=show_record_album&amp;property_id=<?=$property_id?>&amp;menu_id=202" target="_blank" class="btn_form2">
														<?=$c_album?>
														</a>
							</a><a href="javascript:show_options('#album','#album_down2','#album_down2','right');" class="btn_form_down" id="album_down2"><img src="/admin/themes/inmotools/images/down.gif" width="7" height="5" border="0" /></a>
														<? endif //show_tabs?>
												</div>
											</div></td>
									</tr>
								</table></td>
						</tr>
					</table></td>
		</tr>
</table>
	</form>
<table width="100%" cellpadding="0" cellspacing="0" class="form">
	<tr>
		<td class="formCaptionTitle formCaptionTitleImageUploader"><?=$c_insert?> </td><td class="formCaptionTitle formCaptionTitleImageUploader formCaptionTitleCheckbox" align="right"><input type="checkbox" id="show_java_uploader"> <?= $c_insert_advanced ?></td>
	</tr>
</table>
<div class="form images"><span class="dropzone" id="dropzone<?= $parent_id ?>"/></span></div>
<iframe src="<?= $uploader_link ?>" scrolling="no" allowtransparency="true" id="imagesFrame" name="imagesFrame"></iframe>