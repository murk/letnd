<?

/**
 * PvgmaoMaquina
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2011
 * @version $Id$
 * @access public
 */
class PvgmaoMaquina extends Module {

	var $needs_client_id = true;
	var $group_id, $user_is_admin, $user_is_client, $client_id;

	function __construct() {
		parent::__construct();
	}

	function on_load() {
		parent::on_load();
		$this->set_field( 'tipus_id', 'add_new_link', '/admin/?menu_id=2752&client_id=' . $this->client_id );
		$this->set_field( 'tipus_id', 'add_edit_link', '/admin/?menu_id=2753&client_id=' . $this->client_id );
		$this->set_field( 'tipus_id', 'select_condition', 'client_id=' . $this->client_id );

		$this->set_field( 'estat_id', 'add_new_link', '/admin/?menu_id=2762&client_id=' . $this->client_id );
		$this->set_field( 'estat_id', 'add_edit_link', '/admin/?menu_id=2763&client_id=' . $this->client_id );
		$this->set_field( 'estat_id', 'select_condition', 'client_id=' . $this->client_id );

		$this->set_field( 'ubicacio_id', 'add_new_link', '/admin/?menu_id=2772&client_id=' . $this->client_id );
		$this->set_field( 'ubicacio_id', 'add_edit_link', '/admin/?menu_id=2773&client_id=' . $this->client_id );
		$this->set_field( 'ubicacio_id', 'select_condition', 'client_id=' . $this->client_id );

		$this->set_field( 'operari_id', 'add_new_link', '/admin/?menu_id=2742&client_id=' . $this->client_id );
		$this->set_field( 'operari_id', 'add_edit_link', '/admin/?menu_id=2743&client_id=' . $this->client_id );
		$this->set_field( 'operari_id', 'select_condition', 'tipus_operari= \'intern\' AND client_id=' . $this->client_id . ' order by nom ASC, cognoms ASC' );

		$this->set_field( 'usuari_id', 'add_new_link', '/admin/?menu_id=2742&client_id=' . $this->client_id );
		$this->set_field( 'usuari_id', 'add_edit_link', '/admin/?menu_id=2743&client_id=' . $this->client_id );
		$this->set_field( 'usuari_id', 'select_condition', 'tipus_operari= \'intern\' AND client_id=' . $this->client_id . ' order by nom ASC, cognoms ASC' );

	}

	function list_records() {
		$GLOBALS['gl_content'] = $this->get_records();
	}

	function get_records() {
		$listing            = new ListRecords( $this );
		$listing->condition = "client_id = " . $this->client_id;

		return $listing->list_records();
	}

	function show_form() {
		$GLOBALS['gl_content'] = $this->get_form();
	}

	function get_form() {
		$show            = new ShowForm( $this );
		$show->condition = "client_id = " . $this->client_id;

		// TODO-i Poder imprimir
		$show->set_options( 1, 1, 0, 1 );

		$show->set_form_level1_titles( 'marca', 'maquinaitems' );

		return $show->show_form();
	}

	function save_rows() {
		$save_rows = new SaveRows( $this );
		$save_rows->save();

		// TODO-i actualitzar historial estats si es fa editable
	}

	function write_record() {
		$writerec = new SaveRows( $this );

		// Quan preu es 0 al crear, no poso entrada a l'historial
		$old_estat_id = '0';
		if ( $this->action != "add_record" ) {
			$id = $writerec->id;
			$old_estat_id = Db::get_first( "SELECT estat_id FROM pvgmao__maquina WHERE maquina_id = '$id'" );
		}
		$estat_id = $writerec->get_value( 'estat_id' );

		$writerec->save();

		$id = $writerec->id; // per el add_record
		$this->update_estat_history( $id, $estat_id, $old_estat_id );

	}

	function update_estat_history( $id, $estat_id, $old_estat_id ) {

		// Si ha canviat estat, inserto nou historic
		if ( $estat_id != $old_estat_id ) {
			Db::execute( "
				INSERT INTO `pvgmao__history` (
				`maquina_id`, `estat_id`, `date`, `tipus`, client_id) VALUES
				($id, '$estat_id', now(), 'estat_change',$this->client_id)" );
		}
	}

	function manage_images() {
		$image_manager = new ImageManager( $this );
		$image_manager->execute();
	}

	// Al guardar sublist del formulari
	public function maquinaitems_on_save($saved_ids, $deleted_ids, $action){


		// Si és un registre nou generar manteniment
		if ($action == 'add_record' ) {


			return;
		}

		// Si s'ha editat un registre
		// 1 - Esborrar els manteniments futurs dels esblrrats, sempre i quan no s'hagi editat
		foreach ( $deleted_ids as $maquinaitem_id ) {
			// busco l'últim manteniment generat
			/*$rs = Db::get_row( "
					SELECT data_previst, data_realitzat
					FROM pvgmao__mantenimentprogramat
					WHERE maquinaitem_id = $maquinaitem_id
					ORDER BY data_previst DESC
					LIMIT 1" );*/
		}

		// 2 - Modificar dates del futur
		foreach ( $saved_ids as $maquinaitem_id ) {
			// busco l'últim manteniment generat
			/*$rs = Db::get_row( "
					SELECT data_previst, data_realitzat
					FROM pvgmao__mantenimentprogramat
					WHERE maquinaitem_id = $maquinaitem_id
					ORDER BY data_previst DESC
					LIMIT 1" );*/
		}
	}


	public function get_sublist_html( $id, $field, $is_input, $is_word = false ) {
		$listing = NewRow::get_sublist_html( $this, $id, $field, $is_input );


		$listing->set_field( 'operari_id', 'select_condition', 'client_id=' . $this->client_id . ' order by nom ASC, cognoms ASC' );

		$listing->order_by = 'manteniment ASC, manteniment_id ASC';


		$listing->list_records( false );

		/*if ( $listing->has_results ) {

			foreach ( $listing->results as $key => $val ) {
				$rs = &$listing->results[ $key ];
				$rs['volum'] = $rs['quantitat'] * $rs['capacitat_envas'];
			}
		}*/

		return $listing->parse_template();

	}

	// ajax
	public function get_sublist_item() {

		$listing = new NewRow( $this );

		$listing->set_field( 'operari_id', 'select_condition', 'client_id=' . $this->client_id . ' order by nom ASC, cognoms ASC' );

		$manteniment_id = $listing->selected_id;

		// Valors per defecte i no modificables
		$rs = array(
			'manteniment' => Db::get_first( "SELECT pvgmao__manteniment.manteniment FROM pvgmao__manteniment WHERE pvgmao__manteniment.manteniment_id = '$manteniment_id'" ),
			'data_inici'  => format_date_list( now() ),
			'cada'        => Db::get_first( "SELECT pvgmao__manteniment.cada FROM pvgmao__manteniment WHERE pvgmao__manteniment.manteniment_id = '$manteniment_id'" ),
			'periode'     => Db::get_first( "SELECT pvgmao__manteniment.periode FROM pvgmao__manteniment WHERE pvgmao__manteniment.manteniment_id = '$manteniment_id'" ),
		);

		$listing->add_row( $rs );
		$listing->new_row();
	}

	// ajax
	public function get_checkboxes_long_values() {
		{
			$limit = 100;

			$q                    = R::escape( 'query' );
			$id                   = R::id( 'id' );
			$selected_ids         = R::get( 'selected_ids' );
			$exclude_selected_ids = '';
			$exclude_saved_ids    = '';
			$order_by             = 'manteniment ASC, manteniment_id ASC';

			if ( $selected_ids ) {
				R::escape_array( $selected_ids );
				$selected_ids         = implode( ',', $selected_ids );
				$exclude_selected_ids = " AND ( manteniment_id NOT IN ($selected_ids))";
			}

			if ( $id ) {
				$exclude_saved_ids = " AND ( manteniment_id NOT IN (SELECT manteniment_id FROM pvgmao__maquinaitem WHERE maquina_id = $id))";
			}

			$ret                = array();
			$ret['suggestions'] = array();

			$r = &$ret['suggestions'];

			//	if ($q)
			//	{
			// 1 - Que comenci
			$query = "SELECT manteniment_id, manteniment
					FROM pvgmao__manteniment
					WHERE (manteniment LIKE '$q%')
					$exclude_selected_ids					
					$exclude_saved_ids
					AND client_id = $this->client_id
					AND bin = 0
					ORDER BY $order_by
					LIMIT " . $limit;

			$results = Db::get_rows( $query );

			$rest = $limit - count( $results );

			// 2 - Qualsevol posició
			if ( $rest > 0 ) {

				$query = "SELECT manteniment_id, manteniment
						FROM pvgmao__manteniment
						WHERE 
							( manteniment LIKE '%$q%'
			                AND manteniment NOT LIKE '$q%' 
			                )
							$exclude_selected_ids
							$exclude_saved_ids
							AND client_id = $this->client_id
							AND bin = 0
						ORDER BY $order_by
						LIMIT " . $rest;

				$results2 = Db::get_rows( $query );

				$results = array_merge( $results, $results2 );

			}


			// Presento resultats
			foreach ( $results as $rs ) {

				$value = '';

				if ( $rs['manteniment'] ) {
					$value = $rs['manteniment'];
				}

				$r[] = array(
					'value' => $value,
					'data'  => $rs['manteniment_id']
				);
			}
			//	}


			$rest               = $limit - count( $results );
			$ret['is_complete'] = $rest > 0;

			die ( json_encode( $ret ) );


		}

	}
}

?>