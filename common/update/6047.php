<?

/* 10-07-15

  Actualitzacio product comunitats

 */

if ( is_table( 'all__provincia' ) ) {

	if ( ! is_table( 'all__comunitat' ) ) {

		$update_sql .= "
			CREATE TABLE `all__comunitat` (
				`geonameid` INT(11) NOT NULL DEFAULT '0',
				`comunitat_id` VARCHAR(20) NULL DEFAULT NULL,
				`country_id` CHAR(10) NULL DEFAULT NULL,
				`name` VARCHAR(200) NULL DEFAULT NULL,
				`alternatenames` VARCHAR(4000) NULL DEFAULT NULL,
				PRIMARY KEY (`geonameid`),
				INDEX `admin1_id` (`comunitat_id`)
			)
			COLLATE='utf8_general_ci'
			ENGINE=MyISAM
			;
		";

		$update_sql .= "
		INSERT INTO `all__comunitat` (`geonameid`, `comunitat_id`, `country_id`, `name`, `alternatenames`) VALUES (2513413, '31', 'ES', 'Murcia', 'Avtonomnaja oblast\' regiona Mursija,Murcia,Murcie,Mursija,Múrcia,Region Murcia,Region de Murcia,Region de Murcie,Region of Murcia,Región de Murcia,Région de Murcie,?????????? ??????? ??????? ??????,??????,????');
		INSERT INTO `all__comunitat` (`geonameid`, `comunitat_id`, `country_id`, `name`, `alternatenames`) VALUES (2519582, '1', 'ES', 'Ciudad de Ceuta', 'Ceuta,Ceuto,Ce?to,Ciudad Autonoma de Ceuta,Ciudad Autónoma de Ceuta,Ciudad de Ceuta,Cueta,Eptadelfos,Lungsod ng Ceuta,Sebta,Septa,Septem Fratres,Seuta,Zeuta,?????,?????,????,?????,?????,???,??,???');
		INSERT INTO `all__comunitat` (`geonameid`, `comunitat_id`, `country_id`, `name`, `alternatenames`) VALUES (2521383, '07', 'ES', 'Illes Balears', 'Avtonomnaja oblast\' Balearskikh Ostrovov,Balear Uharteak,Balearane,Balearen,Balearene,Balearerna,Baleares,Balearic Islands,Balearinsuloj,Balearoj,Balears,Balearskie Ostrova,Baléares,Comunidad Autonoma de Illes Balears,Comunidad Autonoma de las Islas Baleares,Comunidad Autónoma de Illes Balears,Comunidad Autónoma de las Islas Baleares,Comunitat Autonoma de les Illes Balears,Comunitat Autònoma de les Illes Balears,Iles Baleares,Ilhas Baleares,Illas Baleares,Illes Balears,Islas Baleares,Islles Baleares,ses Illes,Îles Baléares,?????????? ??????? ?????????? ????????,?????????? ???????,?????');
		INSERT INTO `all__comunitat` (`geonameid`, `comunitat_id`, `country_id`, `name`, `alternatenames`) VALUES (2593109, '51', 'ES', 'Andalucía', 'Andalousie,Andalucia,Andalucía,Andalusia,Andalusie,Andalusien,Andalusija,Andaluzio,Baetica,Comunidad Autonoma de Andalucia,Comunidad Autónoma de Andalucía,?????????,??????');
		INSERT INTO `all__comunitat` (`geonameid`, `comunitat_id`, `country_id`, `name`, `alternatenames`) VALUES (2593110, '53', 'ES', 'Canarias', 'Avtonomnaja oblast\' Kanarskikh Ostrovov,Canarias,Canaries,Canary Islands,Comunidad Autonoma Canaria,Comunidad Autónoma Canaria,Comunidad Canaria,Iles Canaries,Illes Canaries,Illes Canàries,Islas Afortunadas,Islas Canarias,Kanaria,Kanariaj Insuloj,Kanariansaaret,Kanarieoearna,Kanarieöarna,Kanarioyane,Kanarioyene,Kanarische Inseln,Kanariøyane,Kanariøyene,Kanarskie Ostrova,Îles Canaries,?????????? ??????? ????????? ????????,????????? ???????,?????,??????');
		INSERT INTO `all__comunitat` (`geonameid`, `comunitat_id`, `country_id`, `name`, `alternatenames`) VALUES (2593111, '54', 'ES', 'Castilla-La Mancha', 'Castella i la Manxa,Castilla La Mancha,Castilla-La Mancha,Castille-La Mancha,Castille-La-Manche,Comunidad Autonoma de Castilla-La Mancha,Comunidad Autónoma de Castilla-La Mancha,Kastilien-La Mancha,Kastilija — La-Mancha,Suedkastilien,Südkastilien,???????? — ??-?????,????????????,??????????????');
		INSERT INTO `all__comunitat` (`geonameid`, `comunitat_id`, `country_id`, `name`, `alternatenames`) VALUES (2593112, '57', 'ES', 'Extremadura', 'Comunidad Autonoma de Extremadura,Comunidad Autónoma de Extremadura,Ehstremadura,Estremadura,Estremadure,Estrémadure,Extremadura,???????????,??????????');
		INSERT INTO `all__comunitat` (`geonameid`, `comunitat_id`, `country_id`, `name`, `alternatenames`) VALUES (2593113, '60', 'ES', 'Comunidad Valenciana', 'Communaute Valencienne,Communaute de Valence,Communauté Valencienne,Communauté de Valence,Comunidad Valenciana,Comunitat Valenciana,Landes Valencia,Oblast\' Valensija,Pais Valencia,Pais Valenciano,País Valenciano,País Valencià,Region of Valencia,Reino de Valencia,Valencia,Valencian Community,Valensija,????????,??????? ????????,????????????,??????');
		INSERT INTO `all__comunitat` (`geonameid`, `comunitat_id`, `country_id`, `name`, `alternatenames`) VALUES (3114710, '34', 'ES', 'Asturias', 'Asturias,Asturien,Asturies,Asturija,Asturio,Astúries,Comunidad Autonoma del Principado de Asturias,Comunidad Autónoma del Principado de Asturias,Principado de Asturias,Principality of Asturias,Principaute des Asturies,Principauté des Asturies,???????,???????');
		INSERT INTO `all__comunitat` (`geonameid`, `comunitat_id`, `country_id`, `name`, `alternatenames`) VALUES (3115609, '32', 'ES', 'Navarra', 'Alta Navarra,Autonome Region Navarra,Autonomous Region of Navarre,Communaute Forale de Navarre,Communauté Forale de Navarre,Comunidad Foral de Navarra,Nabarra,Nafarroa,Nafarroako Foru Komunitatea,Naparroa,Navaro,Navarra,Navarre,Reino de Navarra,Reyno de Navarra,el Viejo Reyno,???????,????');
		INSERT INTO `all__comunitat` (`geonameid`, `comunitat_id`, `country_id`, `name`, `alternatenames`) VALUES (3117732, '29', 'ES', 'Madrid', 'Autonome Region Madrid,Autonomous Region of Madrid,Communaute de Madrid,Communauté de Madrid,Comunidad Autonoma de Madrid,Comunidad Autónoma de Madrid,Comunidad de Madrid,Comunitat de Madrid,Madrid,Oblast\' Madrid,Region de Madrid,Región de Madrid,??????,??????? ??????,????????????');
		INSERT INTO `all__comunitat` (`geonameid`, `comunitat_id`, `country_id`, `name`, `alternatenames`) VALUES (3336897, '27', 'ES', 'La Rioja', 'Comunidad Autonoma de La Rioja,Comunidad Autónoma de La Rioja,Errioxa,La Rioja,Logrono,Logroño,Rioja,Riokha,?????,?????');
		INSERT INTO `all__comunitat` (`geonameid`, `comunitat_id`, `country_id`, `name`, `alternatenames`) VALUES (3336898, '39', 'ES', 'Cantabria', 'Avtonomnaja oblast\' Kantabrija,Cantabria,Cantabrie,Cantàbria,Comunidad Autonoma de Cantabria,Comunidad Autónoma de Cantabria,Kantabria,Kantabrien,Kantabrija,Kantabrio,Provincia de Santander,Santander,la Montana,la Montana de Castilla,la Montaña,la Montaña de Castilla,?????????? ??????? ?????????,?????????,??????');
		INSERT INTO `all__comunitat` (`geonameid`, `comunitat_id`, `country_id`, `name`, `alternatenames`) VALUES (3336899, '52', 'ES', 'Aragón', 'Arago,Aragon,Aragonien,Aragó,Aragón,Avtonomnaja oblast\' Aragon,Reino de Aragon,Reino de Aragón,?????????? ??????? ??????,??????,????');
		INSERT INTO `all__comunitat` (`geonameid`, `comunitat_id`, `country_id`, `name`, `alternatenames`) VALUES (3336900, '55', 'ES', 'Castilla y León', 'Castella i Lleo,Castella i Lleó,Castiella y Lleon,Castiella y Lleón,Castilla y Leon,Castilla y León,Castille and Leon,Castille and León,Castille-et-Leon,Castille-et-León,Comunidad Autonoma de Castilla y Leon,Comunidad Autónoma de Castilla y León,Kastilien-Leon,Kastilija i Leon,Nordkastilien,???????? ? ????,?????????????');
		INSERT INTO `all__comunitat` (`geonameid`, `comunitat_id`, `country_id`, `name`, `alternatenames`) VALUES (3336901, '56', 'ES', 'Catalunya', 'Catalogna,Catalogne,Catalonha,Catalonia,Cataluna,Catalunya,Cataluña,Katalonia,Katalonien,Katalonija,Katalunio,La Catalogna,Principat de Catalunya,el Principat,?????????,?????????,??????,?????,?????');
		INSERT INTO `all__comunitat` (`geonameid`, `comunitat_id`, `country_id`, `name`, `alternatenames`) VALUES (3336902, '58', 'ES', 'Galicia', 'A Galiza,Comunidade Autonoma de Galicia,Comunidade Autónoma de Galicia,Galice,Galicia,Galicien,Galicija,Galiza,Galícia,???????,????');
		INSERT INTO `all__comunitat` (`geonameid`, `comunitat_id`, `country_id`, `name`, `alternatenames`) VALUES (3336903, '59', 'ES', 'País Vasco', 'Baskarland,Baskenland,Baskerland,Baskien,Basque Country,Comunidad Autonoma del Pais Vasco,Comunidad Autónoma del País Vasco,Euskadi,Euskal Herria,Euzkadi,Pais Basc,Pais Vasco,Pays Basque,País Basc,País Vasco,Provincias Vascongadas,Strana Baskov,Vascongadas,?????? ??????,???????');
		INSERT INTO `all__comunitat` (`geonameid`, `comunitat_id`, `country_id`, `name`, `alternatenames`) VALUES (6362988, '2', 'ES', 'Melilla', 'Melil\'ja,Melilla,???????');
				";

	} else {
		trigger_error( 'Ja existeixen la taula all__comunitat' );
	}

}


?>