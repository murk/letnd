function select_addresses(related_table_id) {
  gl_related_table_id = related_table_id;
  showPopWin('/admin/?action=list_records_select_frames_property&menu_id=5002&custumer_id=' + related_table_id, 950, 530, null, true, false, '');
}

function show_demand_property(property_id, menu_id, curl, language){
	showPopWin(curl + '/?tool=inmo&tool_section=property&action=show_record&mls=true&property_id='+property_id+'&language='+language, 950, 560, null, true, false, '', true);
}


function show_form_new_demand(custumer_id){
	showPopWin('/admin/?template=window&action=show_form_new_demand&menu_id=5003&custumer_id='+custumer_id, 750, 520, null, true, false, '');
}
function show_form_edit_demand(demand_id){
	showPopWin('/admin/?template=window&action=show_form_edit_demand&menu_id=5003&demand_id='+demand_id, 750, 520, null, true, false, '');
}
function show_form_new_property(custumer_id){
	showPopWin('/admin/?template=window&show_tabs=0&show_search=0&action=show_form_new_property&menu_id=5012&custumer_id='+custumer_id, 750, 520, null, true, false, '');
}
function show_form_edit_property(property_id){
	custumer_id = document.theForm.custumer_id_javascript.value;
	showPopWin('/admin/?template=window&show_tabs=0&show_search=0&action=show_form_edit_property&menu_id=5012&property_id='+property_id + '&custumer_id=' + custumer_id, 750, 520, null, true, false, '');
}
function show_form_edit_images(property_id){
	custumer_id = document.theForm.custumer_id_javascript.value;
	showPopWin('/admin/?template=window&show_tabs=0&action=list_records_images_property&menu_id=5012&property_id='+property_id + '&custumer_id=' + custumer_id, 750, 520, null, true, false, '');
}
function show_demand_property_list(custumer_id, menu_id){
	showPopWin('/admin/?template=window&menu_id=' + menu_id + '&custumer_id=' + custumer_id, 750, 520, null, true, false, '');
}
function show_form_new_history(custumer_id){
	showPopWin('/admin/?template=window&action=show_form_new&process=custumer&menu_id=5026&custumer_id='+custumer_id, 750, 485, null, true, false, '');
}
function show_form_edit_history(history_id){ //edito una historia
	custumer_id = document.theForm.custumer_id_javascript.value;
	showPopWin('/admin/?template=window&action=show_form_edit&process=custumer&menu_id=5026&history_id='+history_id+'&custumer_id='+custumer_id, 850, 560, null, true, false, '');
}
function toggle(capa) {
	var show_hide = ($('#'+capa).css("display")=='none')?'show':'hide';
	if (show_hide=='hide') {
	$('#'+capa).animate({ opacity: show_hide, height: 0 }, "slow");
	}
	else {
	$('#'+capa).animate({ opacity: show_hide, height: '100%' }, "slow");
	}

}
function show(capa){
	if ($('#'+capa).css("display")=='none') toggle(capa);
}
function demand_change_property(demand_id, custumer_id, property_id, client_id, process, obj){
	var action = "?menu_id=5011&action=save_rows&process=" + process + "&demand_id=" + demand_id + "&custumer_id=" + custumer_id + "&property_id=" + property_id + "&client_id=" + client_id;
	
	$('#history-holder').remove();	
	
	html = '<div id="history-holder">' +
				'<form method="post"  action="' + action + '" target="save_frame">' +
					'<span>' + gl_caption['c_history_title'] + '</span>' +
					'<input type="text" value="' + gl_caption['c_'+process+'_done'] + '" name="history_title" id="history_title">' +
					'<span>' + gl_caption['c_history'] + '</span>' +
					'<textarea name="history" id="history" cols="80" rows="10"></textarea>' +
					'<div><input type="submit" value="Ok" class="btn_form1"><a class="btn_form_delete" href="javascript:demand_close_change_property();">x</a>' +
				'</form>' +
			'</div>';
	$('#demand-change-'+property_id + '-' + demand_id + '-' + client_id).append(html);
	$('#history').focus();
}
function demand_close_change_property(){
	$('#history-holder').remove();
}

var booking =
{
	show_form_edit_book: function (book_id) {
		var link = '/admin/?template=window&tool=booking&tool_section=book&action=show_form_edit&book_id=' + book_id;
		showPopWin(link, 990, 80, false, true, false, '', false, false, 'px', '%');
	}
};
