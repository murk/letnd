<article>	
  <h2>Política de protección, privacidad y protección de datos</h2>
  <p>La política de privacidad y protección de datos de <?=COMPANY_NAME?>, es con el deseo de dar el mejor servicio y máxima seguridad a nuestros clientes. Los datos facilitados por nuestros clientes en ningún caso serán utilizados para fines que no sean los de abajo indicados.</p>
  <p><?=COMPANY_NAME?> respeta el cumplimiento de la Ley Orgánica 5/92 del 29 de octubre sobre el tratamiento automático de datos de carácter personal. Por esto, el cliente o usuario de la nuestra web puede ejercitar sus derechos de acceso, rectificación, oposición y cancelación pudiéndose dirigir en <?=COMPANY_NAME?> en la dirección indicada en esta página.</p>
  <p><?=COMPANY_NAME?> se reserva el derecho de poder utilizar la información indicada por el cliente o usuario para poder enviarle promociones comerciales, excepto que el cliente especifique lo contrario.</p>
  <p>Los datos guardados automáticamente están en un fichero seguro y de acceso restringido. En ningún caso la información contenida será cedida a terceros. Ésta base de datos solamente se crea para realizar las funciones informáticas ligadas a la comercialización, promociones y estadísticas de <?=COMPANY_NAME?>.</p>
  <?if(COMPANY_DETAILS):?>
  <p><?=COMPANY_DETAILS?></p>
  <?endif //COMPANY_DETAILS?>
</article>