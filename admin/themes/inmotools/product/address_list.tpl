<script>
	function over(obj) {
		obj.className = 'searchItemOver';
	}
	function out(obj) {
		obj.className = 'searchItemOdd';
	}
	gl_custumer_check = true;
	function check(num) {
		if (!gl_custumer_check) {
			gl_custumer_check = true;
			return;
		}
		$("input[id^='delivery']").attr("checked", false);
		$('#delivery_' + num).attr('checked', true);
		customer_id = $('input[name=customer_id_javascript]').val();
		$("#delivery_id_" + customer_id).val(num);

	}
</script>
<table width="100%" border="0" cellpadding="4" cellspacing="0">
	<tr onClick="check(0);" onMouseOver="over(this)" onMouseOut="out(this)"><? if ( $results ): ?>
			<td width="30" valign="top">
			<input onclick="customer_show_address(this)" name="same_address" type="checkbox" id="delivery_0" <?= $same_address ?> />
			</td><? endif //has_results?>
		<td><?= $c_same_address ?></td>
	</tr>
</table>
<? foreach ( $loop as $l ): extract( $l ) ?>
	<table width="100%" border="0" cellpadding="4" cellspacing="0">
		<tr onClick="check(<?= $address_id ?>);" onMouseOver="over(this)" onMouseOut="out(this)">
			<td width="30" valign="top">
				<input name="same_address" type="checkbox" id="delivery_<?= $address_id ?>" onclick="customer_show_address(this)" <?= $checked ?> />
			</td>
			<td width="200"><strong><?= $a_name ?> <?= $a_surname ?></strong><br>
				<?= $address ?><br>
				<?= $city ?> <?= $zip ?><br>
				<? if ( $provincia_id ): ?>
					<?= $provincia_id ?><br>
				<? endif //$ ?>
				<?= strtoupper( $country_id ) ?></td>
			<td valign="top">
				<span onclick="gl_custumer_check=false;"><? foreach ( $buttons as $button ): ?><?= $button ?><? endforeach //$buttons?></span>
			</td>
		</tr>
	</table>
<? endforeach //$loop?>

