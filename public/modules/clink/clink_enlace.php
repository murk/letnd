<?
/**
 * ClinkEnlace
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2011
 * @version $Id$
 * @access public
 */
class ClinkEnlace extends Module{

	function __construct(){
		parent::__construct();
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function get_records()
	{
	    $listing = new ListRecords($this);
		$listing->condition = "clink__category_language.language = '".LANGUAGE."'";
		$listing->group_fields = array('category_id');
		$listing->paginate = false;
		$listing->join = 'LEFT OUTER JOIN clink__category_language USING (category_id)';
		$listing->order_by = 'category ASC, enlace ASC';		
	    return $listing->list_records();
	}
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{
		$show = new ShowForm($this);
	    return $show->show_form();
	}
}
?>