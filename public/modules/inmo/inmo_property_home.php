<?php

function list_records()
{
	$config = &$GLOBALS['gl_config'];
	
	// es llisten totes directament, sense home
	if ($config['home_list_all']){
		$module = Module::load('inmo', 'property');
		$module->do_action('list_records');
		return;
	}
	// ------------------------------
	// Llistat automatic de la primera categoria
	// ------------------------------
	elseif (
		$GLOBALS['gl_is_home'] && $config['home_show_first_category']
			||
		!$GLOBALS['gl_is_home'] && $GLOBALS['gl_is_tool_home'] && $config['home_tool_show_first_category']				
			) {
		
		$_GET['category_id'] = inmo_get_first_category();
		$_GET['tipus'] = 'sell'; // TODO suposarem que la primera catregoria es de sell ( s'haurà de posar en config també )
		$module = Module::load('inmo', 'property');
		$module->do_action('list_records');
		return;
	}
	
	$listing = new ListRecords;
		
	$max_results = INMO_HOME_MAX_RESULTS?INMO_HOME_MAX_RESULTS:5;
	$home_show_checked = INMO_HOME_SHOW_CHECKED ? " AND home = 1" : '';
	
	if (!$GLOBALS['gl_is_home'] && $GLOBALS['gl_is_tool_home']){
		
		$max_results = INMO_HOME_TOOL_MAX_RESULTS?INMO_HOME_TOOL_MAX_RESULTS:$max_results;
		$home_show_checked = INMO_HOME_TOOL_SHOW_CHECKED ? " AND home = 1" : '';
		
	}
    $listing->limit = '0, ' . $max_results;	
	$listing->condition = "(status = 'onsale' OR status = 'sold' OR status = 'reserved')";
	

	// ------------------------------
	// Llistat de les destacades
	// ------------------------------
	if (Db::get_first("SELECT count(*) FROM inmo__property WHERE home=1 AND (status = 'onsale' OR status = 'sold' OR status = 'reserved') AND bin=0 LIMIT 1")){


		$listing->condition .= $home_show_checked;


		if ($config['default_order']){
			$listing->order_by = 'home desc, ' . $config['default_order'];
		}
		else {
			$listing->order_by = 'home desc, ref desc, entered desc';
		}
	} 
	// ------------------------------
	// si no hi ha cap home agafem els mes recents així sempre surt algo
	// ------------------------------
	else 
	{
		$results = Db::get_rows("SELECT property_id FROM inmo__property WHERE (status = 'onsale' OR status = 'sold' OR status = 'reserved')  AND bin=0 ORDER BY home desc, entered desc LIMIT 0, " . $max_results);
		if ($results){
			$condition = implode_field($results, 'property_id', ',');
			$listing->condition .= ' AND property_id IN (' . $condition . ')';


			if ($config['default_order']){
				$listing->order_by = 'home desc, ' . $config['default_order'];
			}
			else {
				$listing->order_by = 'home desc, ref desc, entered desc';
			}
		}
	}

    $listing->call("list_records_walk", "property_id,ref,category_id,price_consult,price,status,land,land_units,tipus,entered");
	$listing->call_function['description'] = 'add_dots';
	$listing->list_records();
}
?>