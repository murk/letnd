<?

/* 29-07-15

  Actualitzacio product - Opci� per descripci� en html

 */

if ( is_table( 'product__configadmin' ) && ! Db::get_first( "SELECT * FROM product__configadmin WHERE name='is_description_html'" ) ) {
	$update_sql[] = "
		INSERT INTO product__configadmin (`name`, `value`) VALUES ('is_description_html', '0');";

} else {
	if ( is_table( 'product__configadmin' ) ) {
		trigger_error( 'Ja existeixen el camp is_description_html' );
	}
}

?>