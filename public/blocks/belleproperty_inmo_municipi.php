<?php
$query = "SELECT Distinct municipi, inmo__municipi.municipi_id as municipi_id
				FROM inmo__municipi, inmo__property
				WHERE inmo__municipi.municipi_id = inmo__property.municipi_id
				AND (inmo__municipi.municipi_id = 1000 OR inmo__municipi.municipi_id = 102)
				AND bin = 0 AND (status = 'onsale' OR status = 'sold' OR status = 'reserved')";

$results = Db::get_rows($query);

$results[] = array('municipi_id'=>'0','municipi'=>$gl_caption['others']);

if ($results) {
    foreach($results as $rs) {
        isset($_GET["municipi_id"]) && ($_GET["municipi_id"] == $rs["municipi_id"])?$selected = 1:$selected = 0;

        $l['link'] = '/?block='.$this->script.'&amp;tool=inmo&amp;tool_section=property&amp;municipi_id=' . $rs['municipi_id'] . '&amp;municipi=' . $rs['municipi'] . '&amp;language=' . LANGUAGE;        
		
		
		$l['link'] = '/' . LANGUAGE . '/v/municipi_id/' . $rs['municipi_id'] . '/block/'.$this->script.'/';
		
		
		
        $l['selected'] = $selected;
        $l['item'] = $rs['municipi'];
        $loop2[] = $l;
    }
$loop[] = array ('loop' => $loop2,
    'item' => '');
}
?>