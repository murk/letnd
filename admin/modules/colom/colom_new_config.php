<?php
	// Aquest arxiu es genera automaticament
	// Qualsevol canvi que es faci es podrà sobrescriure involuntariament desde l'eina de gestió
	// Achtung!
global $gl_language;
$gl_file_protected = true;
	$gl_db_classes_fields_included = array (
  'new_id' => '1',
  'status' => '1',
  'new' => '1',
  'subtitle' => '1',
);
	$gl_db_classes_language_fields = array (
  0 => 'new',
  1 => 'content',
);
	$gl_db_classes_fields = array (
  'new_id' =>
  array (
    'type' => 'hidden',
    'enabled' => '1',
    'form_admin' => 'text',
    'form_public' => 'text',
    'list_admin' => 'text',
    'list_public' => 'text',
    'order' => '0',
    'default_value' => '',
    'override_save_value' => '',
    'class' => '',
    'javascript' => '',
    'text_size' => '',
    'text_maxlength' => '',
    'textarea_cols' => '',
    'textarea_rows' => '',
    'select_caption' => '',
    'select_size' => '',
    'select_table' => '',
    'select_fields' => '',
    'select_condition' => '',
  ),
  'ordre' =>
  array (
    'type' => 'int',
    'enabled' => '1',
    'form_admin' => 'input',
    'form_public' => '0',
    'list_admin' => 'input',
    'list_public' => '0',
    'order' => '0',
    'default_value' => '',
    'override_save_value' => '',
    'class' => '',
    'javascript' => '',
    'text_size' => '2',
    'text_maxlength' => '11',
    'textarea_cols' => '',
    'textarea_rows' => '',
    'select_caption' => '',
    'select_size' => '',
    'select_table' => '',
    'select_fields' => '',
    'select_condition' => '',
  ),
  'status' =>
  array (
    'type' => 'select',
    'enabled' => '1',
    'form_admin' => 'input',
    'form_public' => 'text',
    'list_admin' => 'input',
    'list_public' => 'text',
    'order' => '0',
    'default_value' => '',
    'override_save_value' => '',
    'class' => '',
    'javascript' => '',
    'text_size' => '',
    'text_maxlength' => '',
    'textarea_cols' => '',
    'textarea_rows' => '',
    'select_caption' => '',
    'select_size' => '',
    'select_table' => '',
    'select_fields' => 'prepare,review,public,archived',
    'select_condition' => '',
  ),
  'new' =>
  array (
    'type' => 'text',
    'enabled' => '1',
    'form_admin' => 'input',
    'form_public' => 'text',
    'list_admin' => 'text',
    'list_public' => 'text',
    'order' => '2',
    'required' => '',
    'default_value' => '',
    'override_save_value' => '',
    'class' => 'wide',
    'javascript' => '',
    'text_size' => '',
    'text_maxlength' => '255',
    'textarea_cols' => '',
    'textarea_rows' => '',
    'select_caption' => '',
    'select_size' => '',
    'select_table' => '',
    'select_fields' => '',
    'select_condition' => '',
  ),
  'content' =>
  array (
    'type' => 'html',
    'enabled' => '1',
    'form_admin' => 'input',
    'form_public' => 'text',
    'list_admin' => 'text',
    'list_public' => 'text',
    'order' => '4',
    'default_value' => '',
    'override_save_value' => '',
    'class' => '',
    'javascript' => '',
    'text_size' => '',
    'text_maxlength' => '',
    'textarea_cols' => '',
    'textarea_rows' => '',
    'select_caption' => '',
    'select_size' => '',
    'select_table' => '',
    'select_fields' => '',
    'select_condition' => '',
  ),
);
	$gl_db_classes_list = array (
  'cols' => '1',
);
	$gl_db_classes_form = array (
  'cols' => '1',
  'images_title' => 'new',
);
$gl_db_classes_uploads = array (
);
	?>