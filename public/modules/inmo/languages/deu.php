<?php

if (!defined('TITLE_COMARCA')){
	define('TITLE_COMARCA', 'Immobilien in: ');
	define('TITLE_PRICE', 'Immobilien für bis zu %s Euro');
	define('TITLE_PRICE_2', 'Immobilien für %s bis %s Euro');
	define('TITLE_PRICE_3', 'Immobilien für über %s Euro');

	define('TITLE_FLOORSPACE', 'Immobilien für bis zu %s m<sup>2</sup>');
	define('TITLE_FLOORSPACE_2', 'Immobilien für %s bis %s m<sup>2</sup>');
	define('TITLE_FLOORSPACE_3', 'Immobilien für über %s m<sup>2</sup>');

	define('TITLE_PERSON', 'Bis zu %s personen');
	define('TITLE_PERSON_2', 'Für %s bis %s personen');
	define('TITLE_PERSON_3', 'Über %s personen');
}

$gl_caption['c_filter_price_min'] = 'Preis min.';
$gl_caption['c_filter_price_max'] = 'Preis max.';
$gl_caption['c_filter_price_min_temp'] = 'Preis min. / nacht';
$gl_caption['c_filter_price_max_temp'] = 'Preis max. / nacht';

$gl_caption['c_recent'] = 'Neu';

// NO TRADUIT
$gl_caption['c_property_list__view_all'] = 'Veure tots els inmobles visitats';
$gl_caption['c_characteristics'] = 'Leistungs';
// FI NO TRADUIT

$gl_caption['c_booking_close_results_message'] = 'No s\'ha trobat cap inmoble per les dates seleccionades, els següents inmobles es poden reservar per dates similars';
?>