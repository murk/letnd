<?
/*

El programa funciona en 2 parts

1- Grabar les dades de depuració

S'inicialitza al iniciar el debug, i es graba al fer el p_all



2 - Obtindre les dades de de l'interfície - Pot ser des de l'extensió, o desde /ddebugg

Utilitza l'arxiu Clockwork_letnd.php per recuperar les dades


I la interfície es a /ddebugg/ , es pot utilitzar directament per web o amb l'extensió per chrome


 */
// TODO-i Que no es pugui obrir el app.html ( amb un htaccess)

session_start();

$gl_is_local = true;
$host = $_SERVER["HTTP_HOST"];
if ((strpos($host, 'w.')!==0) && (strpos($host, 'w2.')!==0) && (strpos($host, '192.168.1')!==0)){
	$gl_is_local = false;
}

if (  !$gl_is_local &&
      (empty( $_SESSION['group_id'] ) || $_SESSION['group_id'] != '1')
) {
	header( "Status: 404 Not Found" );
	die();
}

include( 'app.html' );