<?php
set_tool( 'inmo', 'property', 'inmo__property', 'property_id' );
set_config( 'inmo/inmo_property_config.php' );

include_once( 'inmo_common.php' );

$GLOBALS ['gl_portals'] = [
	'fotocasa'   => 'fotocasa',
	'habitaclia' => 'habitaclia',
	'apicat'     => 'apicat',
	'vreasy'     => 'vreasy'
];

function save_rows() {

	global $gl_message, $gl_messages;

	$portals = $GLOBALS['gl_portals'];

	foreach ( $portals as $portal ) {
		${"{$portal}s"} = R::post( $portal, [] );
		R::escape_array( ${"{$portal}s"} );
	}

	$property_ids = R::post( 'property_id', [] );
	R::escape_array( $property_ids );

	// Borro noms els registres d'aquesta paginació, abans d'insertar els seleccionats

	$portal_vars = [];

	$query = "SELECT * FROM inmo__property_to_portal
                            WHERE property_id
                            IN (" . implode( ',', $property_ids ) . ")";

	$results = Db::get_rows( $query );

	// Tots els que han canviat
	$portal_vars ['all_changed_ids'] = [];

	// tots els que es publiquen o s'han borrat ( canviats + equals )
	$portal_vars ['all_ids'] = [];

	// tots els iguals, aquest no el passo al exportar
	$all_equal_ids = [];

	foreach ( $portals as $portal ) {

		$portal_vars [ $portal ] = [];

		$arr = &$portal_vars [ $portal ];

		$deleted_key = "deleted";
		$new_key     = "new";
		$equal_key   = "equal";
		$var_name    = "${portal}s";

		// Els que s'esborren
		$arr[ $deleted_key ] = [];
		// Els nous
		$arr[ $new_key ] = [];
		// Els que segueixen publicats  (estaven a 1 i segueixen a 1)
		$arr[ $equal_key ] = [];

		foreach ( $results as $rs ) {
			$v  = $rs[ $portal ];
			$id = $rs['property_id'];

			// No està a l'array del portal, però guardat com a 1, vol dir que s'ha borrat
			if ( ! in_array( $id, $$var_name ) && $v == 1 ) {
				$arr[ $deleted_key ] [ $id ]             = $id;
				$portal_vars ['all_changed_ids'] [ $id ] = $id;
			}
			elseif ( $v == 1 ) {
				$arr[ $equal_key ] [ $id ] = $id;
				$all_equal_ids [ $id ]     = $id;
			}
		}

		// Son els que no estan a old_property_ids però si a $property_ids
		$arr[ $new_key ] = array_diff( $$var_name, $arr[ $equal_key ], $arr[ $deleted_key ] );

		// Afegeixo els new a all_changed
		foreach ( $arr[ $new_key ] as $k => $v ) {
			$portal_vars ['all_changed_ids'][ $k ] = $v;
		}

	}
	// Borro tots els registres d'aquesta paginació, abans d'insertar els seleccionats
	// TODO-i No hauria de borrar les eliminades fins que no estigui sincronitzat
	$query_delete = "DELETE FROM inmo__property_to_portal
							WHERE property_id
							IN (" . implode( ',', $property_ids ) . ")";
	Db::execute( $query_delete );

	// inserto seleccionats
	foreach ( $property_ids as $property_id ) {

		$row       = [];
		$any_found = false;
		foreach ( $portals as $portal ) {
			${"{$portal}s"} = R::post( $portal, [] );
			R::escape_array( ${"{$portal}s"} );

			$row [ $portal ] = in_array( $property_id, ${$portal . "s"} ) ? 1 : 0;
			if ( $row [ $portal ] ) $any_found = true;
		}

		if ( $any_found ) {
			$row['property_id'] = $property_id;

			$q = New Query( 'inmo__property_to_portal' );
			$q->insert( $row );
		}
	}

	// afegeixo tots els canviats a all_ids
	$portal_vars ['all_ids'] = $portal_vars ['all_changed_ids'];
	foreach ( $all_equal_ids as $v ) {
		$portal_vars ['all_ids'][ $v ] = $v;
	}

	$gl_message = $gl_messages['list_saved'];

	portal_save_export_all( $portals );

	include_once( 'inmo_property_functions.php' );
	export_to_portal( false, 'save_portal', $portal_vars );

}

function portal_save_export_all( $portals ) {
	unset ( $portals['vreasy'] ); // vreasy no te export_all
	foreach ( $portals as $portal ) {
		$export_all = R::escape( "{$portal}_export_all", false, '_POST' );

		if ( $export_all !== false ) {
			Db::execute( "
					UPDATE inmo__configadmin 
						SET value='" . $export_all . "' 
						WHERE  name='{$portal}_export_all';" );
		}
	}
}

function get_fotocasa_excluded_sql() {
	$condition = " AND
			(
				 property_id NOT IN (							 
					 SELECT property_id 
					 FROM inmo__property WHERE (status = 'onsale' || status = 'reserved')
						 AND bin = 0
						 AND country_id = '1'
						 AND price > 0
			             AND (
			                    latitude != 0.0000000 &&
			                    longitude != 0.0000000
			                )
			             AND (
			                    (floor_space <> 0 )
			                    OR
			                    (category_id = 5 AND land <> 0 )
			                )					 
				 )
			)		
		";

	return $condition;
}

function get_habitaclia_excluded_sql() {
	$condition = " AND
			( 
			    property_id NOT IN (							 
						 SELECT property_id 
						 FROM inmo__property WHERE (status = 'onsale' || status = 'reserved')
							AND bin = 0
							AND country_id = '1'
							AND ref <> ''
							AND category_id <> 0					 
					 )
			)		
		";

	return $condition;
}

function get_apicat_excluded_sql() {
	$condition = " AND
			(
				 property_id NOT IN (							 
					 SELECT property_id 
					 FROM inmo__property WHERE (status = 'onsale' || status = 'reserved')
						 AND bin = 0
						 AND country_id = '1'
						 AND price > 0
						 AND price_consult = 0
		                 AND (
		                        zip != '' 
		                        || 
		                        (
		                            show_map = 1 &&
		                            show_map_point = 1 &&
		                            latitude != 0.0000000 &&
		                            longitude != 0.0000000
		                        )
		                    )
						 AND tipus != 'temp'
					 
					 )
			)		
		";

	return $condition;
}

function portal_check_property( $rs ) {

	$c = &$GLOBALS['gl_caption'];
	$ref = $rs['ref'];
	$price          = (int) $rs['price'];
	$price_consult  = (bool) $rs['price_consult'];
	$latitude       = (double) $rs['latitude'];
	$longitude      = (double) $rs['longitude'];
	$show_map       = (bool) $rs['show_map'];
	$show_map_point = (bool) $rs['show_map_point'];
	$zip            = $rs['zip'];
	$floor_space    = (double) $rs['floor_space'];
	$category_id    = (int) $rs['category_id'];
	$land           = (double) $rs['land'];
	$adress = $rs['adress'];
	$tipus = $rs['tipus'];

	// Fotocasa
	$fotocasa_messages = [];
	if ( ! $price ) {
		$fotocasa_messages [] = $c['c_fotocasa_check_price'];
	}
	if ( !$latitude || !$longitude ) {
		$fotocasa_messages [] = $c['c_fotocasa_check_latitude'];
	}
	if ( ! $floor_space && ( $category_id != 5 || ! $land ) ) {
		$fotocasa_messages [] = $c['c_fotocasa_check_floor_space'];
	}

	// Habitaclia
	$habitaclia_messages = [];
	if ( ! $ref ) {
		$habitaclia_messages [] = $c['c_habitaclia_check_ref'];
	}

	// Apicat
	$apicat_messages = [];
	if ( $tipus == 'temp' ) {
		$apicat_messages [] = $c['c_apicat_check_tipus'];
	}
	if ( ! $price ) {
		$apicat_messages [] = $c['c_fotocasa_check_price'];
	}
	if ( $price_consult ) {
		$apicat_messages [] = $c['c_apicat_check_consult'];
	}
	if (
		! $zip && ( ! $show_map || ! $show_map_point || ! $latitude || ! $longitude )
	) {
		$apicat_messages [] = $c['c_apicat_check_zip'];
	}

	$portals = $GLOBALS['gl_portals'];
	unset ( $portals['vreasy'] ); // vreasy no te comprovacions per exportar

	$rs['portal_messages'] = [];

	foreach ( $portals as $portal ) {
		$portal_message = ${$portal . "_messages"};

		if ( $portal_message ) {
			$portal_message           = portal_fix_messages_array( $portal_message );
			$rs['portal_messages'] [$portal] = [
				'portal'   => ucfirst( $portal ),
				'messages' => $portal_message
			];
		}
	}

	return $rs;
}

function portal_fix_messages_array( $messages ) {
	$r           = [];
	$count       = 0;
	$total_count = count( $messages );
	foreach ( $messages as $message ) {
		$count ++;
		$last_message = $count == $total_count;
		$r []         = [
			'last_message' => $last_message,
			'message'      => $message
		];
	}

	return $r;
}

function list_records() {
	include( DOCUMENT_ROOT . 'admin/modules/inmo/inmo_property_functions.php' );
	include( DOCUMENT_ROOT . 'admin/modules/inmo/inmo_search_functions.php' );

	Page::add_css_file( '/admin/themes/inmotools/inmo/portal.css' );

	set_fields_to_text();

	merge_caption( 'portal' );

	check_portal_subscription();

	$content_top = get_portal_config();

	global $gl_process;
	$excluded = R::get( 'excluded' );
	$listing  = new ListRecords;

	$listing->condition = "(status = 'onsale' || status = 'reserved')";
	if ( $gl_process == 'portal_selected' ) {
		$listing->condition .= " AND property_id IN (SELECT property_id FROM inmo__property_to_portal WHERE apicat = 1 OR fotocasa = 1 OR habitaclia = 1 OR vreasy = 1)";
	}

	if ( $excluded == 'fotocasa' ) {
		$listing->condition .= get_fotocasa_excluded_sql();
	}
	elseif ( $excluded == 'habitaclia' ) {
		$listing->condition .= get_habitaclia_excluded_sql();
	}
	elseif ( $excluded == 'apicat' ) {
		$listing->condition .= get_apicat_excluded_sql();
	}

	$listing->content_top = $content_top;

	// $listing->selected_ids = get_portal_ids();
	$listing->set_options( 1, 0, 1, 1, 0, 0, 0, 0, 1 );

	$listing = list_records_property( $listing, true );
	$listing->set_var( 'is_portal', true );
	$listing->set_var( 'is_mls', false );
	$listing->set_var( 'show_mls_button', false );
	$listing->set_var( 'has_vreasy', $_SESSION['inmo_vreasy'] );

	$listing->add_custom_filter(
		'excluded',
		[ '', 'fotocasa', 'habitaclia', 'apicat' ]
	);

	$listing->add_button( 'edit', 'menu_id=103&action=show_form_edit', 'boto1' );

	$listing->set_field( 'latitude', 'list_admin', '0' );
	$listing->set_field( 'longitude', 'list_admin', '0' );
	$listing->set_field( 'show_map', 'list_admin', '0' );
	$listing->set_field( 'show_map_point', 'list_admin', '0' );

	$listing->list_records( false );

	foreach ( $listing->results as &$rs ) {
		$rs = portal_check_property( $rs );
		$rs = portal_get_checkboxes( $rs );
	}


	$listing->parse_template();

}

function portal_get_checkboxes( $rs_property ) {


	$portals     = $GLOBALS['gl_portals'];
	$property_id = $rs_property['property_id'];

	$query = "SELECT apicat, fotocasa, habitaclia, vreasy FROM inmo__property_to_portal WHERE property_id = $property_id";
	$rs    = Db::get_row( $query );

	if ( ! $_SESSION['inmo_vreasy'] ) unset ( $portals['vreasy'] );

	$portal_checkboxes = [];

	foreach ( $portals as $portal ) {

		if ( ! empty ( $rs_property['portal_messages'][ $portal ] ) ) $disabled = ' disabled';
		else $disabled = '';

		$portal_value    = isset( $rs[ $portal ] ) ? $rs[ $portal ] : 0;
		$portal_checkbox = '<input ' . $disabled . ' type="checkbox" name="' . $portal . '[' . $property_id . ']" value="' . $property_id . '" ' . ( $portal_value == 1 ? 'checked' : '' ) . '>';

		$portal_checkboxes[ $portal ] = [
			'portal'          => $portal,
			'portal_class'    => $disabled ? 'disabled' : '',
			'portal_checkbox' => $portal_checkbox
		];
	}
	$rs_property['portal_checkboxes'] = $portal_checkboxes;

	return $rs_property;
}

function check_portal_subscription() {

	global $gl_news, $gl_caption;
	$portals = $GLOBALS['gl_portals'];

	if ( ! $_SESSION['inmo_apicat'] || ! $_SESSION['inmo_fotocasa'] || ! $_SESSION['inmo_habitaclia'] ) {

		$gl_news .= "
			<div class=\"missatge-titol\"><strong>" . $gl_caption['c_no_subscrit'] . "</strong></div>";
	}
	unset ( $portals['vreasy'] ); // vreasy no
	foreach ( $portals as $portal ) {
		if ( ! $_SESSION["inmo_$portal"] ) {
			$gl_news .= "<p>" . $gl_caption["c_no_subscrit_$portal"] . "</p>";
		}
	}
}


function get_portal_config() {

	$tpl = new phemplate( PATH_TEMPLATES );
	$tpl->set_vars( $GLOBALS['gl_caption'] );
	$tpl->set_file( 'inmo/portal_config.tpl' );

	$portals = $GLOBALS['gl_portals'];

	unset ( $portals['vreasy'] ); // vreasy no te export_all
	foreach ( $portals as $portal ) {

		$export_all = Db::get_first( "SELECT value FROM inmo__configadmin WHERE name = '${portal}_export_all'" );
		$export_all = '<input onclick="set_portal_config_input(this)" type="checkbox" name="' . $portal . '_export_all" value="1" ' . ( $export_all == 1 ? 'checked' : '' ) . '>';

		$tpl->set_var( "${portal}_export_all", $export_all );
	}

	return $tpl->process();
}