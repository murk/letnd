<?
/**
 * WebPage
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2010
 * @version $Id$
 * @access public
 */
class WebPage extends Module{
	var $page_ids = array(), $menus, $levels_count = array(), $dir, $delete_button=0;
	function __construct(){
		parent::__construct();
		$this->table = 'all__page';
		$this->dir = CLIENT_PATH.'/templates/page/';
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function get_records()
	{
		$listing = new ListRecords($this);
		$listing->paginate = false;
		$listing->set_options(1, 0, 0, 0, 0, 0, 0, 1, 1, 1);
		$listing->set_field('page','type','none');
		
		//$this->menus = '1'; // s'ha d'agafar del config
		$this->menus = isset($_GET['menu'])?$_GET['menu']:
			(isset($_SESSION['menu'])?$_SESSION['menu']:'1');
		$_GET['menu'] = $_SESSION['menu'] = $this->menus;
		$listing->add_filter ('menu');
		
		$this->get_page_ids(0,1);
		$page_ids = $this->page_ids?implode($this->page_ids,','):'0';
		
		$listing->condition = 'all__page.page_id IN ('.$page_ids.')';
		$listing->extra_fields = 'all__page.page_id as page_id2'; // per comptabilitat mysql 4
        $listing->condition .= "ORDER BY FIELD(page_id2, " . $page_ids . ")";		
		
		$listing->add_button ('edit_content_button', 'action=show_form_edit', 'boto1');
		$listing->add_button ('edit_page_button', 'action=show_form_edit', 'boto2');
		$listing->call('list_records_walk','page_id,page,level,parent_id,has_content,template',true);
		
	    return $listing->list_records();
	}
	function list_records_walk(&$listing,$page_id,$page,$level,$parent_id,$has_content,$template){
		$style = '';
		if ($level == 1) $style = "font-weight:bold;";
		if ($level > 2) $style = "color:#5e6901;";
		$key = $parent_id . '_' . $level;
		if (!isset($this->levels_count[$key])) {
			$this->levels_count[$key] = 0;
		}
		$this->levels_count[$key]++;
		$ret['page'] = '<div style="margin-left:'.(30*($level-1)).'px;'.$style.'">'.$this->levels_count[$key].' - ' .$page.'</div>';
		
		$listing->buttons['edit_content_button']['show'] = ($has_content==1 && $template);
		$listing->buttons['edit_page_button']['show'] = !($has_content==1 && $template);
		
		return $ret;
	}
	function get_page_ids($parent_id, $level){
		$loop = array();
		$query = "SELECT page_id
						FROM all__page
						WHERE parent_id = " . $parent_id . "
						AND bin <> 1
						AND menu IN (" . $this->menus . ")
						ORDER BY menu ASC, ordre ASC, page_id";
		Debug::add($query,'Query get_page_recursive');	
		$results = Db::get_rows($query);

		if ($results)
		{
			foreach($results as $rs)
			{
				$this->page_ids[]=$rs['page_id'];
				$this->get_page_ids($rs['page_id'], $level+1);
			}
		}
	}

	
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{
	    $show = new ShowForm($this);
		$show->set_options (1, $this->delete_button, 0, 0);
		
		$show->add_field('content');
		$show->set_field('content','type','html');
		$show->set_field('content','form_admin','out');
		$show->set_field('content','textarea_rows','30');
		
		$show->call('show_records_walk','page_id,has_content,template',true);
	    return $show->show_form();
	}
	function show_records_walk(&$show,$page_id,$has_content,$template){
		if (!($has_content==1 && $template)) {
		$show->unset_field('content');
		return array();
		}
		
		$ret['content'] = '';
		// obtinc els camps html tal com ho faig desde les classes ( no ho puc fer automatic per que no es un camp de la bbdd si no un arxiu .tpl )
		foreach ($GLOBALS['gl_languages']['public'] as $lang){
			$cg = $show->fields['content'];
			$cg['name'] = 'content[' . $lang . ']';
			$cg['form_admin'] = 'input';
			$cg['input_name'] = 'content[' . $page_id . '][' . $lang . ']';
			$cg['input_language'] = $lang;
			$content = file_get_contents($this->dir . $template.'_'. $lang .'.tpl');
			$input =  new FormObject($cg, $content, 'form', $page_id, $show);
			$ret['content'] .= $input->get_input(false);
		}
		return $ret;
	}

	function save_rows()
	{
	    $save_rows = new SaveRows($this);
	    $save_rows->save();
	}

	function write_record()
	{
	    $writerec = new SaveRows($this);
	    $writerec->save();
		Main::load_class('file');
		foreach ($GLOBALS['gl_languages']['public'] as $lang){
			if ($writerec->get_value('content',$lang)!==false){
			$this->save_files($writerec->get_value('template').'_'.$lang, $writerec->get_value('content',$lang));
			}
		}
	}
	function save_files($template, $val)
	{
		$dir = $this->dir;
		$nom_arxiu =  $template . '.tpl';
		$f = new File();
		
		$vell = $f->read($dir . $nom_arxiu);
		//debug::p($vell);
		//debug::p($val);
		if ($vell == $val) return; // si no ha canviat res no guardo
		
		// copio arxiu vell en un nou segons la data
		if (VERSION==5) date_default_timezone_set('UTC');
		$arxiu_vell= date("d-m-Y_G-i-s") . '_' . $nom_arxiu;
		$f->file = $dir . 'old/' . $arxiu_vell;
		$f->write($vell);

		$f->file = $dir . $nom_arxiu;
		$f->write($val);

		$GLOBALS['gl_message'] = "L'arxiu s'ha guardat correctament";
		$this->cleanFiles($dir,$nom_arxiu);
	}
	function cleanFiles($dir,$nom_arxiu)
	{
		// Borrar los ficheros temporales
		$t = time();
		$h = opendir($dir);
		$num = 0;
		$older_file = '';
		$previous = '';
		while ($file = readdir($h))
		{
			if (substr($file, - strlen ($nom_arxiu)) == $nom_arxiu)
			{
				$num++;
				$path = $dir . '/' . $file;
				if ($older_file) {
					if (filemtime($path)<filemtime($older_file)) {
						$older_file = $path;
					}
				}
				else{
					$older_file = $path;
				}

			}
		}

		// si hi ha mes de X arxius, borro el mes vell
		if ($num>10) {
			@unlink($older_file);
		}
		closedir($h);
	}

	function manage_images()
	{
	    $image_manager = new ImageManager($this);
	    $image_manager->execute();
	}
}
?>