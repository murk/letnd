<?php
global $gl_selected_addresses_names, $gl_selected_addresses_ids;
set_tool('custumer', 'custumer', 'custumer__custumer', 'custumer_id');
set_language_vars('inmo', 'property', 'custumer');
set_config('custumer/custumer_custumer_config.php');
Module::get_config_values();

$gl_selected_addresses_names = '';
$gl_selected_addresses_ids = '';

// save_rows_selected_custumer
function save_rows()
{
    global $gl_action, $gl_message, $gl_messages;

    $property_id = R::id('property_id', false, '_POST');

    $query = "DELETE FROM custumer__custumer_to_property where property_id = '" . $property_id . "'";
    Db::execute ($query);
	
	
    $query = 'INSERT INTO custumer__custumer_to_property (custumer_id, property_id ) VALUES ';
	
    Debug::add ('Relacio custumers a property',$query);

    if ($_POST['selected_addresses_ids'])
    {
        $custumers = explode(',', $_POST['selected_addresses_ids']);
        foreach ($custumers as $custumer_id)
        {
            $query .= "( " . $custumer_id . " , " . $property_id . "),";
        }
        $query = substr($query, 0, -1) . ';';
        Db::execute($query);
    }
    $_GET['property_id'] = $property_id; // per el list_records
	set_inner_html('custumers',list_records('list_records_custumer'));
	$gl_message = $gl_messages['assigned'];
}

// Es llisten també quan volem seleccionar custumers desde contracte, property i booking
function list_records($javascript_action=false)
{
    global $gl_content, $gl_db_max_results, $gl_page, $gl_action, $tpl, $gl_caption, $gl_menu_id, $gl_messages, $gl_message, $gl_process;

    if ($javascript_action) $gl_action = $javascript_action;

    $property_id = R::number('property_id');
	
    if ($gl_action == 'list_records_select_frames_custumer') // frames
    {
        $gl_page->template = '';
        $tpl->set_file('inmo/custumer_select_frames.tpl');
        $tpl->set_vars($gl_caption);
        $tpl->set_var('property_id', $property_id);
        $tpl->set_var('menu_id', $gl_menu_id);
        // si be de l'apartat contract
        if (isset($_GET['contract_id']))
        {
            $tpl->set_var('submit_link', '/admin/?menu_id=206&process=' . $gl_process . '&property_id=' . $property_id);
            $tpl->set_var('contract_id',  R::text_id('contract_id'));
            $tpl->set_var('book_id', '');
            $tpl->set_var('is_booking', false);
			$tpl->set_var('exclude_custumer_ids', '');
			$tpl->set_var('ordre', '');
        }
        // si be de l'apartat book
        elseif (isset($_GET['book_id']))
        {
            $tpl->set_var('submit_link', '/admin/?tool=booking&tool_section=book&action=get_custumers');
            $tpl->set_var('book_id', R::id('book_id'));
            $tpl->set_var('is_booking', true);
            $tpl->set_var('contract_id', '');
			$tpl->set_var('menu_id', $gl_menu_id);
			$tpl->set_var('main_custumer', R::escape('main_custumer'));
			$tpl->set_var('exclude_custumer_ids', R::escape('exclude_custumer_ids'));
			$tpl->set_var('ordre', R::escape('ordre'));
        }
        else
        {
            $tpl->set_var('submit_link', '/admin/?action=save_rows_selected_custumer&menu_id=' . $gl_menu_id);
            $tpl->set_var('contract_id', '');
            $tpl->set_var('is_booking', false);
            $tpl->set_var('book_id', '');
			$tpl->set_var('exclude_custumer_ids', '');
			$tpl->set_var('ordre', '');
        }
        $gl_content = $tpl->process();
        return;
    }

    if (($gl_action == 'list_records_select_custumer') ||
            ($gl_action == 'list_records_search_words_custumer')) // llistat dins frames
        {
        $gl_page->template = 'clean.tpl';
        $gl_page->title = '';
        set_field('phone3','list_admin','no');
	    $listing = new ListRecords;
        $listing->set_options(0, 0, 0, 0, 0, 0, 0, 0, 1);
        $gl_message = $gl_messages['select_item'];
        $listing->call('list_records_walk1', 'custumer_id,name,surname1,surname2');
        if ($gl_action == 'list_records_search_words_custumer')
        {
            include(DOCUMENT_ROOT . '/admin/modules/custumer/custumer_custumer_functions.php');
            $listing->condition .= search_list_condition_words();
        }
		$book_id = R::get('book_id');
		if ($book_id !== false){
			
			$exclude_custumer_ids = R::get('exclude_custumer_ids');
			$main_custumer = R::get('main_custumer');
			$can_change_main_custumer = R::get('ordre')=='1';

            // Si s'ha seleccionat el primer i no s'està canviant el primer
			if ($main_custumer && !$can_change_main_custumer){
				$and = $listing->condition?' AND ':'';
				$listing->condition .= $and . '(book_group = 0  OR  book_group = '.$main_custumer.')';
			}
            // Si no hi ha cap seleccionat o si només s'ha seleccionat el primer
            // $can_change_main_custumer nomes -> quan nomes hi ha seleccionat el primer custumer ( quan ja tenim seleccionat el segon ja no es pot cambiar el primer ), per tant podem posar el que volguem
			else{
				$and = $listing->condition?' AND ':'';
				// si no s'ha especificat el custumer principal, no mes es podrà escollir el principal d'un grup
				$listing->condition .= $and . '(book_group = 0  OR  (book_group != 0 AND book_main = 1))';				
			}
			
			if ($exclude_custumer_ids){
				$and = $listing->condition?' AND ':'';
				$listing->condition .= $and . 'custumer_id NOT IN ('.$exclude_custumer_ids.')';
			}
			Debug::p($listing->condition, 'text');
		}
        $gl_page->title = '';
    }
    else // llistat principal
        {
        $gl_db_max_results = 200000;
        set_field('adress','list_admin','text');
	    $listing = new ListRecords;
        $listing->template = 'inmo/property_custumer_list';
        $listing->set_options(0, 0, 0, 0, 0, 0, 0, 0, 0);
        $listing->join = 'INNER JOIN custumer__custumer_to_property USING (custumer_id)';
        $listing->add_button ('edit_button', '', 'boto1', 'before', 'edit_custumer');
        $listing->condition = 'property_id = ' . $property_id;
        $listing->call('list_records_walk2', 'custumer_id,name,surname1,surname2');
    }

    $listing->order_by = 'name ASC';
	
	// permisos usuaris
	$listing->condition .= inmo_get_user_permission($listing->condition);
	
    $listing->list_records();
	if ($javascript_action) return $gl_content;
    if ($gl_action == 'list_records_custumer') show_form_custumer($gl_content, $listing->has_results);
}

function list_records_walk2 ($custumer_id, $name , $surname1, $surname2)
{
    global $gl_selected_addresses_names, $gl_selected_addresses_ids;
    $gl_selected_addresses_names .= htmlspecialchars($name . ' ' . $surname1 . ' ' . $surname2,ENT_QUOTES) . '#;#';
    $gl_selected_addresses_ids .= $custumer_id . ',';
    return array();
}

function list_records_walk1 ($custumer_id, $name , $surname1, $surname2)
{
	$name=  $name . ' ' . $surname1 . ' ' . $surname2;
	$name= addslashes($name); // cambio ' per \'
	$name= htmlspecialchars($name); // cambio nomès cometes dobles per  &quot;
	
	if (isset($_GET['book_id'])){
		$ret['tr_jscript'] = 'onClick="parent.parent.bookingform.custumer_on_select(' . $custumer_id . ')"';
	}
	else{		
		$ret['tr_jscript'] = 'onClick="parent.insert_address(' . $custumer_id . ',\'' . trim(htmlspecialchars(addslashes($name))) . '\')"';
	}
    return $ret;
}
function show_form_custumer($content, $has_results)
{
    global $tpl, $gl_content, $gl_caption, $gl_selected_addresses_names, $gl_selected_addresses_ids, $gl_message;
    $property_id = R::id('property_id');
    $query = "SELECT property_id, ref, tipus, property_private FROM inmo__property WHERE property_id = " . $property_id;

    $results = Db::get_rows($query);
    $rs = current($results);
    $rs['show_tipus_temp'] = $rs['tipus'] == 'temp'?'block':'none';
    $show_form = new ShowForm;
	
    $show_form->set_vars_common();
    $show_form->set_vars_form();
    $tpl->set_vars($rs);
    $tpl->set_vars($show_form->rs);
    $tpl->set_var('selected_addresses_names', substr($gl_selected_addresses_names, 0, -3));
    $tpl->set_var('selected_addresses_ids', substr($gl_selected_addresses_ids, 0, -1));
    if (!$has_results)
    {
        $content = '';
        $gl_message = '';
    }
    $tpl->set_var('content', $content);
    $tpl->set_file('inmo/property_custumer.tpl');
    $gl_content = $tpl->process();
    show_search_form();
}

function show_form()
{
    global $gl_action, $gl_page, $tpl;
    $gl_page->template = 'window.tpl';
    $show = new ShowForm;
    $tpl->set_var('is_tool_custumer', false);
    $tpl->set_var('hidden', false);
	
	$tpl->set_var('view_only_itself', inmo_view_only_itself());	
	if (inmo_view_only_itself()) $show->set_field ('user_id', 'type', 'hidden');	
	
    $template = 'custumer/custumer_form';
    $show->template = $template;
    $show->call('show_form_walk', 'custumer_id');
    $show->show_form();
}
function show_form_walk()
{
    $ret['properties'] = '';
    $ret['demands'] = '';
    return $ret;
}

function write_record()
{
    global $gl_action, $gl_insert_id, $gl_saved, $gl_reload, $gl_process, $gl_selected_addresses_names, $gl_selected_addresses_ids;
    $writerec = new SaveRows;
    $writerec->field_unique = "vat";
    $writerec->save();
	
	$action = $GLOBALS['gl_action'];
	
	// poso el remove key automaticament
	if (($action=='add_record_custumer' || $GLOBALS['gl_action']=='add_record') && $GLOBALS['gl_insert_id'])
		newsletter_add_remove_key('custumer__custumer', $GLOBALS['gl_insert_id']);
	
    // relaciono el custumer nou amb la propietat
    if ($gl_saved)
    {
        $custumer_id = $gl_insert_id?$gl_insert_id:current($_POST['custumer_id']);
		
		
		/*
		 * 
		 * 
		 * Custumer desde BOOKING
		 * 
		 * 
		 */
		
        if ($gl_process == "book_custumer")
        {   
            $js = "parent.parent.bookingform.custumer_on_select('" . $custumer_id . "')";
            print_javascript($js);
			$GLOBALS['gl_page']->show_message($GLOBALS['gl_messages']['assigned'], 'parent.parent');
            die();	
        }
		/*
		 * 
		 * 
		 * Custumer desde CONTRACT
		 * 
		 * 
		 */
        elseif (strstr($gl_process, "contract"))
        {
            $custumer_name = current($_POST['name']) . ' ' . current($_POST['surname1']) . ' ' . current($_POST['surname2']);
            $js = "top.get_custumer_jscript('" . $custumer_id . "', '" . $custumer_name . "','" . $gl_process . "')";
            print_javascript($js);
            die();
        }
        else
        {
            $property_id = R::id('property_id');
            if (($gl_action == 'add_record_custumer') && ($property_id))
            {
                $query = "INSERT INTO custumer__custumer_to_property (custumer_id, property_id) VALUES ( " . $custumer_id . " , " . $property_id . ")";
                Db::execute($query);
            }

			set_inner_html('custumers',list_records('list_records_custumer'));
			// així trec el borrat de la llista d'assignats
			$javascript = 'top.theForm.selected_addresses_names.value="' . substr($gl_selected_addresses_names, 0, -3) . '";';
			$javascript .= 'top.theForm.selected_addresses_ids.value="' . substr($gl_selected_addresses_ids, 0, -1) . '";';
			print_javascript($javascript);

        }
    }
}


function newsletter_add_remove_key($table, $id) {	
	
		$table_id = strpos($table, "customer")!==false?'customer_id':'custumer_id';
	
		$q2 = "SELECT count(*) FROM ". $table . " WHERE BINARY remove_key = ";
		$q3 = "UPDATE ". $table . " SET remove_key='%s' WHERE  ". $table_id . " = '%s'";
		
		$updated = false; 
		while (!$updated){
			$pass = get_password(50);
			$q_exists = $q2 . "'".$pass."'";	
			
			// si troba regenero pass
			if(Db::get_first($q_exists)){
				$pass = get_password(50);							
			}
			// poso el valor al registre
			else{				
				$q_update = sprintf($q3, $pass, $id);
				
				Db::execute($q_update);
				break;
			}
		}
}

?>