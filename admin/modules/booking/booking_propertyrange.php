<?
/**
 * BookingPropertyrange
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 6-2014
 * @version $Id$
 * @access public
 */
class BookingPropertyrange extends Module{
	
	var	$property_id = 0;
	
	function __construct(){
		parent::__construct();
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function get_records()
	{
	   	Main::load_class('booking', 'common');
		return BookingCommon::get_propertyrange_records($this, $this->property_id, true);
	}
	function records_walk(&$listing) {
		
		return BookingCommon::propertyrange_records_walk($listing, true);
		
	}
	
	
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{
		$property_id = R::id('property_id');
		$date_start = R::escape('date_start');
		$date_end = R::escape('date_end');
		
		$this->set_field('property_id','default_value',$property_id);
		$this->set_field('date_end','default_value',$date_end);
		$this->set_field('date_start','default_value',$date_start);
		
	    $show = new ShowForm($this);
		$show->has_bin = false;
		
		if (!$property_id){
			$show->get_values();
			$property_id = $show->rs['property_id'];
		}
		
		$show->set_var('season_prices', json_encode($this->get_js_season_prices($property_id,$show->rs)));
		$show->set_var('js_is_form_new', $this->action=='show_form_new'?'true':'false');		
		
		$form = $show->show_form();
		
	    return $form;
	}
	function get_js_season_prices($property_id, $rs){
		
		$results = Db::get_rows("
			SELECT season_id 
			FROM booking__season 
			ORDER BY ordre ASC, season_id ASC");
		//Debug::p($rs, 'rs');
		$ret = array();
		
		foreach ($results as $rs_season){
		
			$rs_range = Db::get_row("
				SELECT season_id, price_night, price_weekend, price_week, price_week_type, minimum_nights 
					FROM booking__propertyrange
					WHERE property_id = ". $property_id . "
					AND season_id = ". $rs_season['season_id'] . "
					ORDER BY date_start DESC LIMIT 1");
			if ($rs_range){
				// al editar registre posem el valor que te guardat
				if ($rs && $rs['season_id']== $rs_range['season_id']) $rs_range = $rs;
				
				$rs_range['price_night'] = format_currency($rs_range['price_night']);
				$rs_range['price_weekend'] = format_currency($rs_range['price_weekend']);
				$rs_range['price_week'] = format_currency($rs_range['price_week']);
				$ret[$rs_season['season_id']] = $rs_range;
			}
		}
		return $ret;
	}

	function save_rows()
	{
	    $save_rows = new SaveRows($this);
	    $save_rows->save();
		
		if ($this->process == 'save_property_rows'){
			
			$this->property_id = R::id('property_id');			
			
			// obtinc calendari
			Main::load_class('booking','common');
			Main::load_class('booking','calendar');
			
			$calendar = New BookingCalendar();
			$calendar->seasons_colors = BookingCommon::get_seasons_colors();
			$cal = $calendar->get_season_calendar();
			
		    set_inner_html('book-calendar-tr', $cal['calendar']);			
			
			$season_list = $this->do_action('get_records');
			set_inner_html('list-records',$season_list);
			
		}
	}

	function write_record()
	{
	    $writerec = new SaveRows($this);		
		
		$id = $writerec->id;
		
		$this->property_id = $property_id = $_POST['property_id'][$id];
		
		$date_start2 = $_POST['date_start'][$id];
		$date_end2 = $_POST['date_end'][$id];		
		
		$date_start = unformat_date($date_start2, true);
		$date_end = unformat_date($date_end2, true);
		
		// giro les dates si la de començament es major que la final
		if (is_date_bigger($date_start, $date_end)){
			
			$t = $date_start2;
			$date_start2 = $date_end2;
			$date_end2 = $t;
			
			$_POST['date_start'][$id] = $date_start2;
			$_POST['date_end'][$id] = $date_end2;
			
			$t = $date_start;
			$date_start = $date_end;
			$date_end = $t;
			
		}
		

		$property = Db::get_first("SELECT property_private FROM inmo__property WHERE property_id = $property_id");

		$is_reserved = Db::get_first("SELECT count(*)
							FROM booking__propertyrange
							WHERE property_id = $property_id
							AND $date_start < date_end
							AND $date_end > date_start
							AND propertyrange_id <> '$id'");
		
		Debug::p($is_reserved, 'text');
		if (!$is_reserved)
		{
			$writerec->save();
			
			
			$season_id = $_POST['season_id'][$id];
			$old_season_id = $_POST['old_season_id'][$id];
			$js = "
					top.booking.set_booking(".$writerec->id.",".$date_start.",".$date_end.",'".$season_id."','".$old_season_id."');
				";

			print_javascript($js);
			
			$message = $GLOBALS['gl_message'];
			
			$season_list = $this->do_action('get_records');
			set_inner_html('list-records',$season_list);
			
			$GLOBALS['gl_message'] = $message;
			
		}
		else{
			
			$gl_message = sprintf($this->messages['full2'], ucfirst($property), $date_start2, $date_end2);
			
			global $gl_page;
			//$gl_reload = false;
			$gl_page = new Page;
			$gl_page->show_message($gl_message, 'parent');

			Debug::p_all();
			die();			
		}
	}
	
	/* CALENDARI
	 * 
	 *  Temporades per una propietat
	 *  
	 *  Es configuren de quines temporades disposa una propietat en concret
	 * 
	 * 
	 */	
	function show_property_seasons()
	{
		$this->property_id = R::id('property_id');		
		
		$this->tpl->set_vars($this->caption); // per comentar quan fagi un list
		$this->tpl->set_vars($this->config); // per comentar quan fagi un list
		$this->tpl->set_var('menu_id',$GLOBALS['gl_menu_id']);
		
		$season_list = $this->do_action('get_records');		
		
		// obtinc calendari
		Main::load_class('booking','common');		
		Main::load_class('booking','calendar');
		
		$calendar = New BookingCalendar();
		$calendar->seasons_colors = BookingCommon::get_seasons_colors();
		$cal = $calendar->get_season_calendar();
		
		$this->tpl->set_vars($cal);
				
		$this->set_vars(array(
			'season_list' => $season_list
			));
		
		$this->tpl->file = 'inmo/property_range.tpl';
		
		$rs = Db::get_row('SELECT ref, property_private, property_id FROM inmo__property WHERE property_id = ' . $_GET['property_id']);
		
		$this->tpl->set_vars($rs);
		
		$GLOBALS['gl_content'] = $this->process();
	}
	
	/*
	function save_property_seasons() {
		
		$query = New Query('', 'booking__extra_to_property', 'property_id,extra_id', 'extra_price');
		$query->messages = &$this->messages;
		$query->set_config ('none','int','currency');		
		$query->save_related_post('extra_book_selected');		
		
	}
	*/
}
?>