<?

/**
 * * Gestiona debug
 *
 *
 *
 */
class Debug {
	/**
	 * Debug::start()
	 * Static function
	 * Defineixo si haig de fer debug o no
	 * @access public
	 * @return void
	 */
	static $clockwork;

	static public function init_clockwork() {
		include( DOCUMENT_ROOT . 'common/includes/Clockwork/Support/Letnd/ClockworkMiddleware.php' );
		self::$clockwork = Clockwork\Support\Letnd\ClockworkMiddleware::get_clock();

	}
	static public function start() {

		global $gl_debug_messages;
		if (
			strstr( HOST_URL, 'http://w.' )
			|| strstr( HOST_URL, 'http://localhost' )
			|| ( isset( $_GET['debug'] ) && $_SESSION['group_id'] == 1 )
			|| ( isset( $_SESSION['debug'] ) && $_SESSION['debug'] )
			|| ( HOST_URL == 'http://test2.letnd.com/' )
			|| isset( $GLOBALS['force_debug'] )
		) {
			$_SESSION['debug'] = true;
			define( "DEBUG", true );
			//error_reporting(E_ALL | E_STRICT);
			error_reporting(E_ALL);
			ini_set('display_errors', TRUE);
			ini_set('display_startup_errors', TRUE);
		} elseif (strstr(HOST_URL, 'http://w2.'))
		{
			define("DEBUG", false);
			error_reporting(0);
		} elseif (strstr(HOST_URL, 'http://w3.'))
		{
			define("DEBUG", false);
			error_reporting(0);
		} else
		{
			define("DEBUG", false);
			error_reporting(0);
		}

		if ( DEBUG ) {
			include( DOCUMENT_ROOT . 'common/includes/kint-master/Kint.class.php' );
			Debug::init_clockwork();
		}

		$gl_debug_messages = array();

		set_error_handler( array( 'Debug', 'on_error' ) );
		register_shutdown_function( array( 'Debug', 'shutdown' ) );
	}



	static public function p_all() {
		if (!DEBUG) {
			return;
		}

		Debug::info('Temps execució', Debug::get_execution_time());
		if ( empty( $_SESSION['disable_clockwork'] ) ) Debug::$clockwork->resolveRequest()->storeRequest();
		return;
	}

	/**
	 * Debug::p()
	 *
	 * @param mixed $output
	 * @param string $message
	 * @param boolean $print_anyway Si es true imprimeix inclus quan DEBUG = false
	 * @param boolean $echo Si es falç no imprimeix, però si que retorna valor
	 *
	 * @param bool $is_title
	 * @param string $link
	 *
	 */
	static public function p( $output, $message = '', $print_anyway = false, $echo = true, $is_title = false, $link = '' ) {

		if ( ! DEBUG && ! $print_anyway ) {
			return;
		}

		self::debugg( $message, $output );
		/* TODO-i Posar en una funció a part el que abans era add_zray Debugg::add([[]], place)
		if ( is_array( $output ) ) {
			array_walk( $output, function ( $v ) {
				self::debugg( $v[0], $v[1] );
			} );
		}
		else {
			self::debugg( $message, $output );
		}
		*/
	}
	static public function add( $message, $output, $is_title = false ) {
		self::p($message, $output);
		return;
	}

	static public function pre( $array, $text = '' ) {
		if ( $text ) {
			echo "<strong>" . $text . "</strong>";
		}
		echo( '<pre>' );
		print_r( $array );
		echo '</pre>';
	}

	// debugg::p pero per ajax
	static public function a( $output, $message = '', $print_anyway = false ) {
		if ( ! DEBUG && ! $print_anyway ) {
			return;
		}
		$out_file = '';

		if ( $message ) {
			$out_file .= $message . ': ';
		}

		if ( is_array( $output ) || is_object( $output ) ) {
			$out_file .= print_r( $output, true );

		} else {
			$out_file .= $output;
		}
		$out_file .= Debug::get_error_backtrace( true, true );

		echo "/*\n\n" . $out_file . "\n\n*/\n\n";
	}

	static public function backtrace() {
		if (DEBUG) Debug::p( Debug::get_error_backtrace(), "Backtrace", false, true, true );
	}

	static public function on_error( $type, $message, $file, $line ) {
		$err_text = '';



		switch ( $type ) {
			case E_USER_ERROR:
				$backtrace =  Debug::get_error_backtrace();
				$err_text .= "<b>ERROR</b> [$type] $message";
				$err_text .= "  Error fatal a la linea $line a l'arxiu $file";
				$err_text .= $backtrace;

				self::error($message);

				if ( DEBUG ) {
					Debug::p_all();
					echo $err_text;
				}

				/*else {
					$err = 'Request uri:<strong>' . $_SERVER["REQUEST_URI"] . '</strong><br>' .
					       'Query string:<strong>' . $_SERVER["QUERY_STRING"] . '</strong><br>' .
					       'Referer:<strong>' . $_SERVER["HTTP_REFERER"] . '</strong><br><br>' .
					       $err_text;


					send_mail_admintotal( 'Error fatal - ' . $_SERVER["HTTP_HOST"], $err );
				}*/

				Debug::log_error(
						'fatal',
						Debug::get_error_type($type),
						$message,
						$line,
						$file,
						$backtrace
					);
				exit( 1 );
				break;

			case E_WARNING:
				$err_text .= "<b>WARNING</b> [$type] $message<br />\n";
				break;

			case E_NOTICE:
				$err_text .= "<b>NOTICE</b> [$type] $message<br />\n";
				break;

			case E_USER_WARNING:
				$err_text .= "<b>USER WARNING</b> [$type] $message<br />\n";
				break;

			default:
				$err_text .= "<b>Error desconegut</b>: [$type] $message<br />\n";
				break;
		}

		if ( DEBUG ) {
			self::error( $err_text );
		}

		/* execute PHP internal error handler */

		return false;
	}

	/**
	 * @param $caller
	 * @param string $type
	 * @param string $message
	 * @param string $line
	 * @param string $file
	 * @param string $backtrace
	 * @param string $script_name
	 * @param string $host
	 * @param string $uri
	 * @param string $referer
	 * @param string $query_string
	 * @param string $user_agent
	 */
	static public function log_error(
		$caller,
		$type = '',
		$message = '',
		$line= '',
		$file = '',
		$backtrace = '',
		$script_name = '',
		$host = '',
		$uri = '',
		$referer = '',
		$query_string = '',
		$user_agent = '' ) {

		if ( ! $script_name )
			$script_name = $_SERVER["SCRIPT_NAME"];
		if ( ! $host )
			$host = $_SERVER["HTTP_HOST"];
		if ( ! $uri )
			$uri = $_SERVER["REQUEST_URI"];
		if ( ! $referer && ! empty( $_SERVER["HTTP_REFERER"] ) )
			$referer = $_SERVER["HTTP_REFERER"];
		if ( ! $query_string )
			$query_string = $_SERVER["QUERY_STRING"];
		if ( ! $user_agent )
			$user_agent = $_SERVER['HTTP_USER_AGENT'];

		if ( !empty ($GLOBALS['gl_is_letnd']) ) {
			Db::connect_mother();
			$query = "INSERT INTO `letnd`.`admintotal__error` (`caller`,
						`type`,
						`host`,
						`script_name`,
						`uri`,
						`referer`,
						`query_string`,
						`message`,
						`line`,
						`file`,
						`user_agent`,
						`backtrace`)
					VALUES (
						" . Db::qstr( $caller ) . ",
						" . Db::qstr( $type ) . ",
						" . Db::qstr( $host ) . ",
						" . Db::qstr( $script_name ) . ",
						" . Db::qstr( $uri ) . ",
						" . Db::qstr( $referer ) . ",
						" . Db::qstr( $query_string ) . ",
						" . Db::qstr( $message ) . ",
						" . Db::qstr( $line ) . ",
						" . Db::qstr( $file ) . ",
						" . Db::qstr( $user_agent ) . ",
						" . Db::qstr( $backtrace ) . "
						);";
			Db::execute( $query );

			// Neteja
			/*$query = "DELETE FROM `admintotal__error`
						WHERE error_id NOT IN (
						  SELECT error_id
						  FROM (
						    SELECT error_id
						    FROM `admintotal__error`
						    ORDER BY error_id DESC
						    LIMIT 10000
						  ) foo
						);";
			Db::execute( $query );*/


			Db::reconnect();
		}
	}

	static public function shutdown() {
		
		$error = error_get_last();

		if ( $error !== null ) {
		// if ( $error !== null && $error['type'] === E_ERROR ) {

			switch ( $error['type'] ) {
				case E_WARNING:
				case E_NOTICE:
				case E_USER_WARNING:
				case E_USER_NOTICE:
				case E_DEPRECATED:
				case E_USER_DEPRECATED:
					 return false;
			}

			Debug::log_error(
					'shutdown',
					Debug::get_error_type($error['type']),
					$error['message'],
					$error['line'],
					$error['file']
				);

			Debug::p_all();
			exit;
		}

		/* execute PHP internal error handler */
		return false;
	}
	static public function get_caller_script($num) {

		$errors     = debug_backtrace(0);
		extract ($errors [$num]);

		$file_name = explode( '\\', $file );
					if ( ! is_array( $file_name ) ) {
						$file_name = explode( '/', $file );
					}
					$file_name = $file_name[ count( $file_name ) - 1 ];

		// TODO-i posar a main.js i a public on hi ha la funció db()
		$javascript = 'javascript: var xhttp = new XMLHttpRequest();xhttp.open("GET", "/common/open_php.php?file='. $file . '&line=' . $line . '", true);xhttp.send();';

		return array('line' => $line, 'file' => $file, 'file_name' => $file_name, 'link' => "<a href='$javascript'>$file_name</a>");
	}

	static public function get_error_backtrace( $short = false, $is_ajax = false) {
		$errs     = debug_backtrace(0);
		$err_text = '';

		if ( strpos( getenv( 'OS' ), 'Windows' ) === false ) {
			unset( $errs[0] );
		}

		foreach ( $errs as $err ) {
			extract( $err );

			if (!isset ($file))
				$file = '';
			if (!isset ($line))
				$line = '';

			// descarto els errors d'aquest mateix arxiu i els de la clase db.php
			if ( strpos( $file, "\debug.php" ) === false && strpos( $file, "\db.php" ) === false ) {
				if ( ! $short ) {
					$err_text .= '<br><b style="color:#d35907">Provinent de</b>: <a target="save_frame" href="/common/open_php.php?file=' . $file . '&line=' . $line . '">' . $file . ' </a><b>linea</b>: ' . $line;
				} else {
					$file_name = explode( '\\', $file );
					if ( ! is_array( $file_name ) ) {
						$file_name = explode( '/', $file );
					}
					$file_name = $file_name[ count( $file_name ) - 1 ];

					// a linux agafa el debug com a primer arxiu
					if ( $file_name != '/letnd/common/classes/debug.php' ) {
						if ( $is_ajax ) {
							$err_text .= "\n\n" . $file . ":" . $line;
						} else {
							$err_text .= '&nbsp;&nbsp;<a class="small" target="save_frame" href="/common/open_php.php?file=' . $file . '&line=' . $line . '">' . $file_name . '</a>';
						}
						// aquí nomès vull el primer
						break;
					}
				}
			}

		}

		return $err_text;
	}

	static public function get_error_backtrace_clockwork() {

		$errs     = debug_backtrace( 0 );
		$err_text = '';

		if ( strpos( getenv( 'OS' ), 'Windows' ) === false ) {
			unset( $errs[0] );
		}

		foreach ( $errs as $err ) {
			extract( $err );

			if ( ! isset ( $file ) )
				$file = '';
			if ( ! isset ( $line ) )
				$line = '';

			// descarto els errors d'aquest mateix arxiu i els de la clase db.php
			if (
				strpos( $file, "\debug.php" ) === false &&
				strpos( $file, "\db.php" ) === false &&
				strpos( $file, "/debug.php" ) === false &&
				strpos( $file, "/db.php" ) === false
			) {

				$file = str_replace( '/letnd', '', $file );

				$file_name = explode( '\\', $file );
				if ( ! is_array( $file_name ) ) {
					$file_name = explode( '/', $file );
				}

				$file_name = $file_name[ count( $file_name ) - 1 ];


				$err_text .= self::get_clock_link($file, $line, $file_name) . '<br>';

			}

		}

		return $err_text;
	}
	static public function get_clock_link($file, $line, $message) {


		// NO VA?!!! return "<span ng-click=\"open_in_editor_letnd('$file', '$line')\">$message</span>";

		return "<a target=\"hidden\" href=\"http://localhost:63342/api/file?file=$file&line=$line\">
											$message:$line
										</a>";

		/*return [
			'message' => $message,
			'm_path' => $rel_path,
			'm_number' => 1
		];*/

	}
	// Retorna un link per obrir en notepad++
	// en principi només utilitzar a admintotal o desde un lloc on sigui nomes en DEBUG
	static public function open_link( $file, $caption = '', $line = '1' ) {
		return '<a target="save_frame" href="/common/open_php.php?file=' . DOCUMENT_ROOT . $file . '&line=' . $line . '">' . ( $caption ? $caption : $file ) . ' </a>';
	}

	static public function errors( $val = false ) {
		static $errors = array();
		if ( $val ) {
			$errors[] = $val;
		} else {
			return $errors;
		}
	}


	static public function get_execution_time() {
		// paro el rellotge abans de volcar tot el debug
		$time_end = microtime();

		$time_start = explode( ' ', DOCUMENT_TIME_START );
		$time_end   = explode( ' ', $time_end );

		return number_format( ( $time_end[1] + $time_end[0] - ( $time_start[1] + $time_start[0] ) ), 3, ',', '.' );
	}

	static function console ($data, $message) {
		  echo '<script>';
		  echo 'console.log('. json_encode( $data ) .', '. json_encode( $data ) . ');';
		  echo '</script>';
	}

	static function get_error_type( $type ) {
		$return = "";
		if ( $type & E_ERROR ) // 1 //
		{
			$return .= '& E_ERROR ';
		}
		if ( $type & E_WARNING ) // 2 //
		{
			$return .= '& E_WARNING ';
		}
		if ( $type & E_PARSE ) // 4 //
		{
			$return .= '& E_PARSE ';
		}
		if ( $type & E_NOTICE ) // 8 //
		{
			$return .= '& E_NOTICE ';
		}
		if ( $type & E_CORE_ERROR ) // 16 //
		{
			$return .= '& E_CORE_ERROR ';
		}
		if ( $type & E_CORE_WARNING ) // 32 //
		{
			$return .= '& E_CORE_WARNING ';
		}
		if ( $type & E_COMPILE_ERROR ) // 64 //
		{
			$return .= '& E_COMPILE_ERROR ';
		}
		if ( $type & E_COMPILE_WARNING ) // 128 //
		{
			$return .= '& E_COMPILE_WARNING ';
		}
		if ( $type & E_USER_ERROR ) // 256 //
		{
			$return .= '& E_USER_ERROR ';
		}
		if ( $type & E_USER_WARNING ) // 512 //
		{
			$return .= '& E_USER_WARNING ';
		}
		if ( $type & E_USER_NOTICE ) // 1024 //
		{
			$return .= '& E_USER_NOTICE ';
		}
		if ( $type & E_STRICT ) // 2048 //
		{
			$return .= '& E_STRICT ';
		}
		if ( $type & E_RECOVERABLE_ERROR ) // 4096 //
		{
			$return .= '& E_RECOVERABLE_ERROR ';
		}
		if ( $type & E_DEPRECATED ) // 8192 //
		{
			$return .= '& E_DEPRECATED ';
		}
		if ( $type & E_USER_DEPRECATED ) // 16384 //
		{
			$return .= '& E_USER_DEPRECATED ';
		}

		return substr( $return, 2 );
	}



	static function debugg($message, $output, $link = false)
	{
		if (!DEBUG) return;

		if (!is_array($output)) {$output = [ $output ];}
		Debug::$clockwork->debug($message, $output);
	}

	static function info($message, $output)
	{
		if (!DEBUG) return;

		if (!is_array($output)) {$output = [ $output ];}
		Debug::$clockwork->info($message, $output);
	}

	static function notice($message, $output)
	{
		if (!DEBUG) return;

		if (!is_array($output)) {$output = [ $output ];}
		Debug::$clockwork->notice($message, $output);
	}

	static function error($message)
	{
		if (!DEBUG) return;

		$errors = Debug::get_error_backtrace_clockwork();
		$output = $errors;

		if (!is_array($output)) {$output = [ $output ];}

		if ( empty( $_SESSION['disable_clockwork'] ) ) {
			Debug::$clockwork->error( $message, $output );
		}
		else {
			echo "$message:";
			print_r( $output );
		}
	}
	static function startQuery($query){

		if (!DEBUG) return;

		Debug::$clockwork->getDataSources()[1]->startQuery($query);
	}
	static function stopQuery(){

		if (!DEBUG) return;

		Debug::$clockwork->getDataSources()[1]->stopQuery();
	}

	// Provocar errors
	static function throw_warning( $message ){
		trigger_error($message, E_USER_WARNING);
	}
	static function throw_error( $message ){
		trigger_error($message, E_USER_ERROR);
	}


	// TODO-i Es poden borrar


	/**
	 * @param $title string Títol
	 * @param $place string Lloc
	 */
	static public function add_zray_title( $title, $place ) {

		$title = ucfirst( $title );
		Debug::p(array("<b>$title</b>", ''), $place);

	}
	static public function print_zray( $messages, $place ) {

		// per posar la syntax abreviada
		if ( ! is_array( current( $messages ) ) ) {
			$new_messages = array($messages);
			$messages = $new_messages; // llegeix la variable el zray
		}
	}
	static public function add_zray( $messages, $place ) {

		// per posar la syntax abreviada
		if ( ! is_array( current( $messages ) ) ) {
			$new_messages = array($messages);
			$messages = $new_messages; // llegeix la variable el zray
		}
	}

}