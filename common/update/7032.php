<?

/* 20-10-17
 Configuració inici sellpoint


 */

if ( is_table( 'product__configadmin_sellpoint' ) && ! Db::get_first( "SELECT * FROM product__configadmin_sellpoint WHERE name='home_default_condition'" ) ) {
	$update_sql[] = "
		INSERT INTO product__configadmin_sellpoint (`name`, `value`) VALUES ('home_default_condition', '');";

} else {
	if ( is_table( 'product__configadmin_sellpoint' ) ) {
		trigger_error( 'Ja existeixen el camp home_default_condition' );
	}
}