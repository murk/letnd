{include file="common/header.tpl"}
<div id="main">
<div id="contenu">
	<div id="logo">
		{include file="common/calendar.tpl"}	
	</div>	
	{if $mod==view}
		{include file="common/period_selection.tpl"}
	{/if}

	<div class="both"></div>
	
	{counter print=false assign=a name=a start=0}
	{counter print=false assign=i name=i start=0}
	
	{include file="$contentpage"}
</div>
</div>