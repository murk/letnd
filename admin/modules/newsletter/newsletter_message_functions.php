<?php
function to_brs_spaces($message)
{
    $message_arr = explode("\n", $message);
    $message = '';
    // nl2br
    foreach ($message_arr as $key)
    {
        // d'aquesta manera se'n posa un \r\n de més
        $pos_end = 0;
        $spaces = '';
        // canvio nomès els primers espais en blanc
        for ($x = 0; $x < strlen($key); $x++)
        {
            if (strpos($key, chr(32), $x) !== $x)
            {
                $pos_end = $x;
                break;
            }
        }
        for ($n = 0; $n < $pos_end; $n++)
        {
            $spaces .= "&nbsp;";
        }
        $message .= substr_replace($key, $spaces, 0, $pos_end) . "<br>\n";
    }
    return $message;
}

function preview_mail()
{
    global $tpl, $gl_page, $gl_content;
    get_old_post();
    $gl_page->template = false;
    $mail = false;

    if (R::get('test')) {
        $message = get_message_test();
        $gl_content = $message;
        return;
    }
	
	$message_id =  $_POST["message_id"];
	if (!$message_id && $_POST["message_id_copy"]) $message_id =  $_POST["message_id_copy"][0];
    $message = get_message(true, $message_id, $mail, false);
	
	
    // $message = str_replace(chr(10), '<br>',$message) ;
    // Debug::add ('_POST', $_POST);
    if ($_POST["format"] == "html")
    {
        $tpl->set_var("message_html", $message);
        $tpl->set_var("message_text", false);
    } elseif ($_POST["format"] == "text")
    {
        //$message = to_brs_spaces($message);
        $tpl->set_var("message_text", $message);
        $tpl->set_var("message_html", false);
    }
    //$tpl->set_var("from", $_POST["from_name"]);
    //$tpl->set_var("date", date("j-n-Y", mktime()));
    //$tpl->set_var("to", "aaaa@aaa.com");
    //$tpl->set_var("subject", $_POST["subject"]);
    //$tpl->set_file('newsletter/message_preview.tpl');
    $output = $tpl->process();	
	
    //$gl_content = $output;
    $gl_content = $message;
}

function get_template($template)
{
    $query = "SELECT file_name FROM newsletter__templatehtml WHERE templatehtml_id = " . $template;
    $rs = Db::get_rows($query);
    // Debug::add("Consulta template file", $query);
    // Debug::add("Resultats template file", $rs);
    return $rs[0]["file_name"];
}

/**
 * TEST
 * /admin/?action=show_preview&menu_id=5&test=true&message_id=1&template_id=1&message=Cos+del+missatge
 *
 * @return string
 */
function get_message_test() {
    global $tpl;

    $message_id  = R::id( 'message_id' );
    $language_id = R::id( 'language_id', 'cat' );
    $template_id = R::id( 'template_id', '1' );
    $message = R::get( 'message', '' );

    $_POST["language_id"]     = $language_id;
    $_POST["template_id"]     = $template_id;
    $_POST["message"] = $message;
    $_POST["templatehtml_id"] =  $template_id;
    $_POST["format"] = "html";

    $message = get_message( true, $message_id, $mail, false, true );

    $tpl->set_var( "message_html", $message );
    $tpl->set_var( "message_text", false );

    return $message;

}

/**
 * * Obté el cos del missatge per el missatge i preview, o sigui en tots els casos
 *
 * @access public
 *
 * @param $preview
 * @param $message_id
 * @param $mail
 * @param $remove_key
 *
 * @param bool $test
 *
 * @return string
 */

function get_message($preview, $message_id, &$mail, $remove_key, $test = false)
{
    global $gl_db_classes_id_field, $gl_upload, $gl_caption;
    $upload_files = $gl_upload['picture'];
    $has_content_extra = isset($_POST["tools"]); // si està definit sabem que hi ha contingut extra
    $output = '';
    $content_extra = '';
	$language = R::text_id("language_id", false, '_POST');
	
	if (USE_FRIENDLY_URL) {		
		$remove_link = HOST_URL. $language . '/newsletter/remove/?'.$remove_key;
	}
	else{	
		$remove_link = HOST_URL. '?language='. $language .'&tool=newsletter&tool_section=remove&'.$remove_key;		
	}
	$remove_text = sprintf($gl_caption['c_remove'][$language], $remove_link);

    if ($_POST["format"] == "html" && $_POST["templatehtml_id"])
    {
        // Debug::add("Template", PATH_TEMPLATES_NEWSLETTER_MAIL . get_template($_POST["templatehtml_id"]));

        $tpl = new phemplate(PATH_TEMPLATES_NEWSLETTER_MAIL);
        $tpl->set_vars(get_config_values_public($language));

        //$message = to_brs_spaces($_POST["message"]) ;
        $message = $_POST["message"];
        // sumar contingut d'un altre apartat
        if ($has_content_extra)
        {
        	$content_extra_array = get_content_extra('html', true);
	        $content_extra=$content_extra_array['content_extra'];
        }
        // mostrar les imatges
        $loop_picture = $upload_files->get_record_files_i($message_id);
        // si es el preview mostrem les imatges des de l'ordinador client, pero al preview dels no enviats no ja que no es poden editar les dade
        if ($preview)
        {
            // preview imatges al ordinador client
            // mostrar les imatges ja guardades al sevidor
            foreach ($_POST as $key => $picture)
            {
                // REVISAR strpos
                if (strpos($key, "picture") === 0 && $picture)
                {
                    // per descartar les imatges que ja estan insertades que es passen també per post
                    // ja les obtinc directament de la BBDD 4 linees mes amunt
                    if (!is_array($picture))
                    {
                        $loop_picture[] = array ('picture_name' => '', 'picture_src' => $picture, 'picture_conta' => count($loop_picture));
                    }
                }
            }
        }

		foreach ($loop_picture as &$val){
			$val['picture_loop_count'] = count($loop_picture);
			// poso com a src el real, per que es vegin les imatges desde public
			$val['picture_src'] = $val['picture_src_real'];
			// Debug::p( $val );
		}
        // Debug::add('loop_picture', $loop_picture);
		
		if (!NEWSLETTER_SEPARATE_CONTENT_EXTRA){			
			$message .= $content_extra;
			$content_extra = '';
		}

        $tpl->set_loop('pictures', $loop_picture);
        $tpl->set_var("url", HOST_URL);
		$tpl->set_var('short_host_url', substr(HOST_URL,7,-1));
        $tpl->set_var("message", $message);
        $tpl->set_var("language_id", $language);
        // $tpl->set_var("content_extra", $content_extra);
        $tpl->set_vars($content_extra_array);
        $tpl->set_var("preview", $preview);
		$tpl->set_var('remove_link', $remove_link);
		$tpl->set_var('remove_text', $remove_text);
		
        $tpl->set_file(get_template($_POST["templatehtml_id"]));
        $output = $tpl->process();
	
		if (NEWSLETTER_MAIL_AUTO_INLINE_CSS && !$test) {
			$instyler = new \Pelago\Emogrifier($output);
			$output = $instyler->emogrify();
			$instyler = null;
		}
		
       
    }
    // *
    // *
    // *
    // *
    // *
    // *
    // de moment tot això està capat
    // nomès es fa amb plantilla i prou
    //
    elseif ($_POST["format"] == "text" && $_POST["templatetext_id"])
    {
        $query = "SELECT header, footer FROM newsletter__templatetext WHERE templatetext_id = " . $_POST["templatetext_id"];
        $result = Db::get_rows($query);

        $message = $_POST["message"];
        // sumar contingut d'un altre apartat
        if ($has_content_extra)
        {
            $content_extra = get_content_extra('text');
        }
        $output = $result[0]["header"] . "\r\n" . $message . "\r\n" . $result[0]["footer"];
    } elseif ($_POST["format"] == "html" && !$_POST["templatehtml_id"])
    {
        //$message = to_brs_spaces($_POST["message"]);
        $message = $_POST["message"];
        // sumar contingut d'un altre apartat
        if ($has_content_extra)
        {
            $content_extra = get_content_extra('html');
        }
        $output = $message;
    } elseif ($_POST["format"] == "text" && !$_POST["templatetext_id"])
    {
        $message = $_POST["message"];
        // sumar contingut d'un altre apartat
        if ($has_content_extra)
        {
            $content_extra = get_content_extra('text');
        }
        $output = $message;
    }
    // phpinfo();
	
    return $output;
}
/**
 * * Crida la funcio per obtenir el contingut pel missatge desde un altra eina
 * 		cada funcio esta situada en el directori de les diferents eines
 *
 * @access public
 * @return array | string
 */
function get_content_extra($format, $get_array = false)
{
    $output = '';
    $tools = $_POST['tools'];
    $sections = $_POST['sections'];
    $language = $_POST['language_id'];
    $ret = [];
	foreach ($tools as $key=>$val)
    {
        $content_ids = $_POST['selected_' . $tools[$key] . '_' . $sections[$key] . '_ids'];
        $action = $_POST['selected_' . $tools[$key] . '_' . $sections[$key] . '_action'];
        if ($content_ids) {

        	$tool = $tools[$key];
            $section = $sections[$key];

            include_once(DOCUMENT_ROOT . 'public/modules/' . $tools[$key] . '/newsletter_content_extra.php');
            $html = call_user_func('get_content_extra_' . $tools[$key] . '_' . $sections[$key], $content_ids, $action, $language);
            $output .= $html;
            $ret["content_extra_${tool}_${section}"] = $html;
        }
    }

    if ($get_array){
		 $ret['content_extra'] = $output;
		 return $ret;
    }
    else{
	    return $output;
    }
}

function get_addresses($sending_erroneous, $message_id)
{

	$send_to_same_language_only = R::post('send_to_same_language_only');
	$send_to_no_language_too = R::post('send_to_no_language_too');
	$language = R::text_id("language_id", false, '_POST');


	$customer_table  = $GLOBALS['gl_config']['custumer_table'];
	$customer_id  = explode('__',$customer_table);
	$customer_id  = $customer_id[1].'_id';
	
    $groups = isset($_POST["group_id"])?implode("," , $_POST["group_id"]):'';
    $addresses_ids = $_POST["selected_addresses_ids"];
    $results = array();

    if ($sending_erroneous)
    {
        if ($customer_table=='product__customer'){
		$query = "SELECT DISTINCT " . $customer_table . ".customer_id as custumer_id, mail, name, surname, remove_key
		  		FROM " . $customer_table . ", newsletter__message_to_custumer
		      WHERE " . $customer_table . ".customer_id = newsletter__message_to_custumer.custumer_id
				AND message_id = '" . $message_id . "'
				AND sent=0
				AND removed=0
				AND bin=0";
		}
		elseif ($customer_table=='api__agent'){
		$query = "SELECT DISTINCT " . $customer_table . ".agent_id AS custumer_id, mail, nom AS name, cognoms AS surname, remove_key
		  		FROM " . $customer_table . ", newsletter__message_to_custumer
		      WHERE " . $customer_table . ".agent_id = newsletter__message_to_custumer.custumer_id
				AND message_id = '" . $message_id . "'
				AND sent=0
				AND removed=0
				AND bin=0";
		}
		else{
		$query = "SELECT DISTINCT " . $customer_table . ".custumer_id, mail, name, surname1, surname2, remove_key
		  		FROM " . $customer_table . ", newsletter__message_to_custumer
		      WHERE " . $customer_table . ".custumer_id = newsletter__message_to_custumer.custumer_id
				AND message_id = '" . $message_id . "'
				AND sent=0
				AND removed=0
				AND bin=0";
		}

        // Debug::add("Consulta adreces errors", $query);
        $results = Db::get_rows($query);
    }
    else
    {
    	$query_same_language = '';

	    if ( $send_to_same_language_only ) {

	    	$query_same_language = "prefered_language = '$language'";

		    if ( $send_to_no_language_too ) {
	    	    $query_same_language .= " || prefered_language = ''";
		    }
		    $query_same_language = " AND ($query_same_language)";
	    }


        if ($groups)
        {
            if ($customer_table=='product__customer'){
			$query = "SELECT DISTINCT " . $customer_table . ".customer_id as custumer_id, mail, name, surname,
		  		(@manually_selected:=0) AS manually_selected, remove_key
		  		FROM " . $customer_table . "
				INNER JOIN newsletter__custumer_to_group 
				ON ".$customer_table.".".$customer_id."=newsletter__custumer_to_group.custumer_id
				WHERE group_id IN (" . $groups . ")
				AND removed=0
				AND bin=0";
			}
			elseif ($customer_table=='api__agent'){
			$query = "SELECT DISTINCT " . $customer_table . ".agent_id as custumer_id, mail, nom as name, cognoms as surname,
		  		(@manually_selected:=0) AS manually_selected, remove_key
		  		FROM " . $customer_table . "
				INNER JOIN newsletter__custumer_to_group 
				ON ".$customer_table.".".$customer_id."=newsletter__custumer_to_group.custumer_id
				WHERE group_id IN (" . $groups . ")
				AND removed=0
				AND bin=0";
			}
			else{
			$query = "SELECT DISTINCT " . $customer_table . ".custumer_id as custumer_id, mail, name, surname1, surname2,
		  		(@manually_selected:=0) AS manually_selected, remove_key
		  		FROM " . $customer_table . "
				INNER JOIN newsletter__custumer_to_group 
				ON ".$customer_table.".".$customer_id."=newsletter__custumer_to_group.custumer_id
				WHERE group_id IN (" . $groups . ")
				AND removed=0
				
				AND bin=0";
				if ($customer_table == 'custumer__custumer') $query .= " AND activated = 1";
			}

            if ($query_same_language) $query .= $query_same_language;

            // Debug::add("Consulta adreces", $query);

            $results = Db::get_rows($query);
        }

        if ($addresses_ids)
        {
            if ($customer_table=='product__customer'){
			$query_addresses = "SELECT customer_id as custumer_id, mail, name, surname,
	  		(@manually_selected:=1) AS manually_selected, remove_key
			FROM $customer_table
			WHERE customer_id IN ($addresses_ids)
			AND removed=0
			AND bin=0";
			}
            elseif ($customer_table=='api__agent'){
			$query_addresses = "SELECT agent_id as custumer_id, mail, nom as name, cognoms as surname,
	  		(@manually_selected:=1) AS manually_selected, remove_key
			FROM $customer_table
			WHERE agent_id IN ($addresses_ids)
			AND removed=0
			AND bin=0";
			}
			else{
			$query_addresses = "SELECT custumer_id, mail, name, surname1, surname2,
	  		(@manually_selected:=1) AS manually_selected, remove_key
			FROM $customer_table
			WHERE custumer_id IN ($addresses_ids)
			AND removed=0
			AND bin=0";
			if ($customer_table == 'custumer__custumer') $query_addresses .= " AND activated = 1";
			}

            if ($groups) $query_addresses .= " AND ".$customer_id." NOT IN (SELECT DISTINCT custumer_id FROM newsletter__custumer_to_group WHERE group_id IN (" . $groups . ")) ";

            if ($query_same_language) $query_addresses .= $query_same_language;

            $results2 = Db::get_rows($query_addresses);
            foreach ($results2 as $key)
            {
                $results[] = $key;
            }
            // Debug::add("Consulta adreces manuals", $query_addresses);
        }
    }
	//Debug::add('Results Adreces', $results);
    return $results;
}

function start_send_mail()
{

	header( 'Content-type: text/html; charset=utf-8' ); // necessari pel flush
	@ini_set('zlib.output_compression',0);
	@ini_set('implicit_flush',1);
	@ob_end_clean();
	ob_implicit_flush(1);


    // carregar plantilla amb un iframe per log d'enviament
    global $tpl, $gl_config;
	// Debug::p('sendingggggg', 'text');
    $only_test = $gl_config['only_test']?true:false;
    $tpl->set_var('start_sending', $only_test?NEWSLETTER_MAIL_SEND_ONLY_TESTING:NEWSLETTER_MAIL_SEND_SENDING);

    $tpl->set_file('newsletter/message_send.tpl');
    $content = $tpl->process();

    $js = "
			parent.hidePopWin();
			parent.document.getElementById('contingut').innerHTML = '" . addslashes($content) . "';
			parent.document.getElementById('message_container').style.display='block';
		    parent.window.scrollTo(0,0);
			var window_h = parent.$(parent.window).height();
			var report_top = parent.$('#report' ).position().top;
			parent.$('#report').height(window_h-report_top-50);
			parent.document.getElementById('message').innerHTML = '" . addslashes(SENDING_MESSAGE) . "';
		  ";

    print_javascript($js);
	// this is to make the buffer achieve the minimum size in order to flush data
    echo str_repeat(' ',1024*64);
	ob_flush();
	flush();
}

function newsletter_send_mail()
{
    global $gl_insert_id, $gl_process, $gl_upload, $gl_config, $gl_message, $gl_caption;
    set_time_limit(0); // REVISAR hauria de posar un limit no?
	
    $status = 'sent';
    $sending_erroneous = ($gl_process == 'send_mail_not_sent');
    $message_id = $gl_insert_id?$gl_insert_id:$_POST["message_id"];
    
    $only_test = $gl_config['only_test']?true:false;
    $max_sends_per_message = $gl_config['max_sends_per_message']? (int) $gl_config['max_sends_per_message']:1500; // Poso igualment sempre un límit de 1500


    $one_by_one = $_POST['send_method'] == 'one_by_one';
    $CC = $_POST['send_method'] == 'CC';
    $BCC = $_POST['send_method'] == 'BCC';
    $from_name = $_POST["from_name"];
    $from = $_POST["from_address"];
    // -----------------------------------
    // comença el process
    // -----------------------------------
    output_js(GETTING_ADDRESSES);
    $addresses = get_addresses($sending_erroneous, $message_id);
    $number_addresses = count($addresses);
	$number_addresses_sent = 0;
    $number_addresses_not_sent = 0;
    output_js($number_addresses . ' ' . GOT_ADDRESSES);
    // ------------------------------------
    // MODE ENVIAMENT
    // ------------------------------------
    if ($number_addresses)
    {
        
        // Primer posem tots els missatges_addresses a no enviats, nomès si es nou(no no enviats) i un a un, sino ja son tots no enviats
        // així si peta abans d'acabar queden tots marcats igualment com a no enviats
        if (($one_by_one) && (!$sending_erroneous))
        {
            foreach ( $addresses as $address )
            {
                if (!$address['manually_selected'])
                {
                    write_messages_addresses ($message_id, $address['custumer_id'], true);
                }
            }
        }
        // BUCLE PER ADRECES
	    $x = 0;
        foreach ( $addresses as $address )
        {

	        if ( $x % 100 == 0 && $x != 0 ) {

	        	gc_collect_cycles();
		        output_js("<br><br>" . MESSAGE_QUEUE_STOPPED . " <i class='fa fa-spinner fa-pulse fa-fw'></i><br><br>");
		        sleep(30);

		        if ( $x % 500 == 0) output_js(" ", 'true');
	        }

	        if ( $max_sends_per_message <= $number_addresses_sent ) {

		        $c_max_sends_reached = $gl_caption['c_max_sends_reached'];
		        $c_max_sends_reached_2 = $gl_caption['c_max_sends_reached_2'];
		        $c_max_sends_reached_button = $gl_caption['c_max_sends_reached_button'];

		        print_javascript( "top.alert(\"$c_max_sends_reached\");" );
                output_js("<strong class='resaltat'>$c_max_sends_reached<br><br></strong><strong>$c_max_sends_reached_2<br><br></strong> <a href='/admin/?menu_id=24&action=write_record&message_id=$message_id' target='save_frame'>$c_max_sends_reached_button</a><br><br>");

		        $status = 'error2';
		        $number_addresses_not_sent = $number_addresses - $number_addresses_sent;
		        break;

	        }

            // només es torna a calcular si es un a un, i en el altres casos nomès el primer cop
            if (($one_by_one) || ($x == 0))
            {
				/*
				 * AL FALLAR UNA SOLA ADRESSA PETEN TOTES LES SEGUENTS
				 * Per això haig de reiniciar cada cop el mailer, abans anava fora el bucle al començament de tot
				 */
	            $mail = null;
	            $GLOBALS['gl_current_mailer'] = null;

				$mail = $GLOBALS['gl_current_mailer'] = get_mailer($gl_config, false);
				/*if ($gl_config['m_host']=="ssl://smtp.gmail.com") {
					$mail->SMTPAuth = true;
					$mail->Port = 465;
				}*/
				$mail->From = $from;
				// Debug::p($from, 'from');
				$mail->FromName = $from_name;
				
				if (is_file(CLIENT_PATH . 'images/logo_newsletter.jpg') && ($_POST["format"] == "html")) 
						$mail->AddEmbeddedImage(CLIENT_PATH . 'images/logo_newsletter.jpg', 'logo', 'logo.jpg');
				elseif(is_file(CLIENT_PATH . 'images/logo_newsletter.gif') && ($_POST["format"] == "html")) 
						$mail->AddEmbeddedImage(CLIENT_PATH . 'images/logo_newsletter.gif', 'logo', 'logo.gif');
						
				$mail->IsHTML($_POST["format"] == "html");
				$attachments = $gl_upload['file']->get_record_files_i($message_id);
				if ($attachments)
				{
					foreach ($attachments as $attachment)
					{
						$mail->AddAttachment (FILES_DIR . $attachment['file_name'], $attachment['file_name_original']);
					}
				}

				if ($_POST["reply_to"]) $mail->AddReplyTo($_POST["reply_to"], $_POST["reply_to_name"]);		
								
            
				/*
				 * AL FALLAR UNA SOLA ADRESSA PETEN TOTES LES SEGUENTS
				 * Per això haig de reiniciar cada cop el mailer
				 */
				
				
				
				
                $one_by_one?output_js(GETTING_MESSAGE_ONE):output_js(GETTING_MESSAGE);
                $mail->Subject = $_POST["subject"];
                $one_by_one?output_js(GOT_MESSAGE_ONE):output_js(GOT_MESSAGE);;
                if ($BCC) $mail->AddAddress($from, $from_name);
                // Debug::add('To en el BCC', $mail->to);
            }
			
			// poso sempre ja que necessito un remove_key diferent per cada un
            $mail->Body = get_message(false, $message_id, $mail, $address["remove_key"]);
			
			if ($gl_config['custumer_table']=='product__customer'){
				$address_name = $address["name"] . ' ' . $address["surname"];
			}
			elseif ($gl_config['custumer_table']=='api__agent'){
				$address_name = $address["name"] . ' ' . $address["surname"];
			}
			else
			{
				$address_name = $address["name"] . ' ' . $address["surname1"] . ' ' . $address["surname2"];
			}
            // copia oculta o vista
            if ($CC || $one_by_one)
            {
                $mail->AddAddress($address["mail"], $address_name);
            } elseif ($BCC)
            {
                $mail->AddBCC($address["mail"],  $address_name);
            }

            // Debug::add('Adreces =  ',  $address_name . ' E-mail = ' . $address["mail"]);
            // METHODE UN PER UN
            if ($one_by_one)
            {
                $output_address =  $address_name . ' &lt;' . $address["mail"] . '&gt;' ;
                output_js(SENDING_MESSAGE_ONE . ' ' . $output_address );

                // TEST, no envia
	            if ( $only_test ) {
		            $mail_sent = true;
	            }
	            else {
		            $mail_sent = $mail->Send();
	            }

                if ($mail_sent)
                {
                    output_js(MESSAGE_SENT_ONE_A . '<strong>' . ($x + 1) . ' </strong>' .
                        MESSAGE_SENT_ONE_B . ' ' . $output_address . '<br>');
                    write_messages_addresses ($message_id, $address['custumer_id']);
					newsletter_on_send($message_id, $address["custumer_id"], $mail->Subject, $_POST["message"]);
					$number_addresses_sent ++;
					
                }
                else
                {
                    // error i no s'ha enviat el mail
                    output_js(MESSAGE_NOT_SENT_ONE_A . '<strong>' . ($x + 1) . '</strong>' . MESSAGE_NOT_SENT_ONE_B . ' ' . $output_address);
                    output_js('<strong>' . $mail->ErrorInfo . ' </strong>');
                    output_js(MESSAGE_SAVED_NOT_SENT_ONE . '<br>');
                    $status = 'error2';
					//Debug::p($mail,'sisisis');
                    $number_addresses_not_sent ++;
					// Debug::add('Mail error', $mail);
                }
                $mail->ClearAddresses();
                // Debug::add("TO", $mail->to);
            }
            $x++;
        } // END BUCLE ADRECES
        // METODE TOTS EN UN
        if (!$one_by_one)
        {
            output_js(SENDING_MESSAGE);

             // TEST, no envia
            if ( $only_test ) {
	            $mail_sent = true;
            }
            else {
	            $mail_sent = $mail->Send();
            }


            if ($mail_sent )
            {
                output_js('<strong>' . $number_addresses . ' ' . MESSAGE_SENT . ' </strong>');
                $gl_message = $number_addresses . ' ' . MESSAGE_SENT;
            }
            else
            {
                // error i no s'ha enviat cap mail
                // output_js('<strong>' . MESSAGE_ERROR . $mail->ErrorInfo . ' </strong><br>');
                output_js('<strong>' . NO_MESSAGE_SENT . ' </strong>');
                $gl_message = NO_MESSAGE_SENT;
                output_js('<br>' . MESSAGE_SAVED_NOT_SENT);
                $status = 'error1';
				// Debug::add('Mail error', $mail);
            }
        }
        else
        {
            if ($status == 'error2')
            {
                if ($number_addresses > $number_addresses_not_sent)
                {
                    $gl_message = $number_addresses - $number_addresses_not_sent . ' ' . MESSAGE_SENT
                     . '<br>' . $number_addresses_not_sent . ' ' . MESSAGE_NOT_SENT;
                }
                else
                {
                    $gl_message = NO_MESSAGE_SENT;
                }
                output_js('<strong class=resaltat>' . $gl_message . ' </strong>');
            }
            else
            {
                $gl_message = $number_addresses . ' ' . MESSAGE_SENT;
                output_js('<strong class=resaltat>' . $gl_message . ' </strong>');
            }
        }
        $query = "UPDATE newsletter__message SET sent=now(), status='" . $status . "' WHERE message_id = " . $message_id;
        // Debug::add("Consulta missatge", $query);
        Db::execute($query);		
    }

    // Debug::add("Adreces", $addresses);
}

function write_return_link () {
    $message_return_url = R::post('message_return_url');
    if ($message_return_url){
        output_js(addslashes('<p><a href="' . $message_return_url . '">' . $GLOBALS['gl_caption']['c_back'] .'</a></p>'));
    }

}
function newsletter_on_send($message_id, $custumer_id, $subject, $message) {


	// TODO-i Quan es descarten inmobles en el mateix email, s'hauria de actualitzar i grabar-ho com a descartat ( email 22 de agosto de 2018 d'easybrava )

	$customer_table  = $GLOBALS['gl_config']['custumer_table'];
	
	// nomes si estem en inmo
	if ($customer_table=='custumer__custumer'){		
		
		$history =  p2nl($message);
		$history_title =  $subject;


		// TODO-i El post només va si s'envia el missatge des del formulari, si s'envia un missatge guardat, es perden les demandes relacionades. S'hauria de guardar el contingut del post en el missatge, i recuperar-lo en  la funció get_record_message() a newsletter_message
		$demand_results = R::post('demand_results');
		$demand_results = unserialize($demand_results);
		$demand_id = 0;
		
		// Si tinc resultats demanda, poso la taula a mailed=1
		
		if ($demand_results){
			
			
			// coincidencies per un immoble, varies demandes i varis custumers
			if (isset($demand_results['demand_ids'])){
							
				$demand_ids = explode(',',$demand_results['demand_ids']);	
				$demand_custumer_ids = explode(',',$demand_results['custumer_ids']);
				
				$demand_property_id = $demand_results['property_ids'];	
				
				$conta = 0;
				foreach ($demand_custumer_ids as $demand_custumer_id){
					
					if ($custumer_id==$demand_custumer_id) {					
						
						$demand_id = $demand_ids[$conta];
						
						write_demand_to_property(
							$demand_id, 
							$demand_property_id,
							$history, 
							$history_title, 
							$message_id,
							0);
						
						write_custumer_history($history,$history_title,$custumer_id,$message_id,$demand_id,$demand_property_id);
					}
					$conta++;
				}
			}

			// coincidencies per una demanda i un sol custumer
			elseif (isset($demand_results['demand_id'])){
				
				$demand_id = $demand_results['demand_id'];	
				$demand_custumer_id = $demand_results['custumer_ids'];
				
				$demand_property_ids = explode(',',$demand_results['property_ids']);
				
				if ($custumer_id==$demand_custumer_id) {
					
					foreach ($demand_property_ids as $demand_property_id){
						
						write_demand_to_property(
							$demand_id, 
							$demand_property_id,
							$history, 
							$history_title, 
							$message_id,
							0);
					}
					
					write_custumer_history($history,$history_title,$custumer_id,$message_id,$demand_id,$demand_property_ids);
				}
			}
			
		}
		// si no hi ha demandes, entro historial relacionat amb propietats o nomes amb custumer
		else{
			write_custumer_history($history,$history_title,$custumer_id,$message_id,0);
			
		}		
		
	}
	
}
function write_custumer_history($history,$history_title,$custumer_id,$message_id,$demand_id,$demand_property_ids = false) {
	
		$properties =  $_POST['selected_inmo_property_ids'];

        // per relacionar amb una propietat sense haver d'adjuntar-la al mail
        if (!$properties) $properties =  $_POST['related_property_ids'];
		
		Main::load_class('custumer','history');
		
		// grabo una historia per cada client
		// grabo una historia per cada propietat per cada client
		if ($properties){
			
			$properties = explode(',', $properties);
			$demand_id_copy = $demand_id;
			
			foreach ($properties as $property_id)
			{
				// si hi ha demanda, nomes marco amb demanda les propietats que estan en l'array de demanda
				// , ja que potser hagin introduit més propietats que les que es passen des de la demanda
				if ($demand_property_ids){
					if ($property_id == $demand_property_ids || in_array($property_id, $demand_property_ids)){
						
						$demand_id = $demand_id_copy;
					}
					else{
						$demand_id = 0;
					}
					
				}
				// escric a l'historial
				CustumerHistory::add_to_history(
						$history, 
						$history_title, 
						'send_information', 
						$custumer_id,
						$property_id, 
						$message_id, 
						$demand_id);	
			}
			// TODO-i ACtualitzar totes les dates de l'historial amb mateix message_id al guardar, ja que pot ser s'ha reenviat restants un altre dia
		
		}
		// no he adjuntat propietats, nomes grabo un missatge
		else{
		
			// escric a l'historial
			CustumerHistory::add_to_history(
				$history, 
				$history_title, 
				'send_information', 
				$custumer_id,
				0, 
				$message_id, 
				$demand_id);
		}	
	
}
function write_demand_to_property($demand_id, $property_id, $history, $history_tile, $message_id, $client_id) {
	
	$condition = 
				" WHERE  `demand_id` = $demand_id
					AND property_id = $property_id
					AND client_id = $client_id";
	$tipus = "send_information";
	
	if(
		Db::get_first("
		SELECT count(*) FROM custumer__demand_to_property" . $condition
		)
	){
		$sql = "
			UPDATE custumer__demand_to_property
			SET 
			mailed=1, message_id = " . $message_id . $condition;
		
		Db::execute($sql);
		
		// Debug::add('UPDATE demand_to_property', $sql);

	}
	else {

		$sql = "
			INSERT INTO custumer__demand_to_property (
			`demand_id`, `property_id`, `client_id`, message_id, mailed) 
			VALUES ($demand_id, $property_id, $client_id, $message_id, 1);";
		
		Db::execute($sql);
		
		// Debug::add('UPDATE demand_to_property', $sql);
	}	
			
}

function write_messages_addresses ($message_id, $custumer_id, $inizialize = false)
{
    if ($inizialize)
    {
        $query = "INSERT INTO newsletter__message_to_custumer (message_id,custumer_id,sent)
              VALUES ('" . $message_id . "','" . $custumer_id . "','0')";
        // Debug::add("Consulta missatge addresses", $query);
    }
    else // si no inicialitzo nomès crido això per posar que s'ha enviat, ja que ja son tots a 0
        {
            $query = "UPDATE newsletter__message_to_custumer SET sent = 1
              WHERE message_id = " . $message_id . " AND custumer_id = " . $custumer_id;
        // Debug::add("Consulta missatge addresses", $query);
    }
    Db::execute($query);
}

function output_js($message, $reset = 'false') {
	echo ('<SCRIPT LANGUAGE="JavaScript" TYPE="text/javascript">
			<!--
			parent.write_report("' . $message . '", ' . $reset . ');
			//-->
			</SCRIPT>');
	// this is to make the buffer achieve the minimum size in order to flush data
    echo str_repeat(' ',1024*64);
	@ob_flush();
	flush();
	// sleep(1); // per testejar
}
?>