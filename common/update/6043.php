<?php

/* 6-06-15
  Canvi news, afegir plantilles per llistats

UP-DATE
Actualització del mòdul de notícies, ara hi ha la possibilitat d'incloure plantilles diferents per un llistat de notícies i per la fitxa de notícies

 */

if (!is_table('news__listtpl')) {

	$update_sql[]="
		CREATE TABLE `news__listtpl` (
			`listtpl_id` INT(11) NOT NULL AUTO_INCREMENT,
			`file_name` VARCHAR(100) NOT NULL DEFAULT '' COLLATE 'utf8_spanish2_ci',
			`ordre` INT(11) NOT NULL DEFAULT '0',
			PRIMARY KEY (`listtpl_id`)
		)
		COLLATE='utf8_spanish2_ci'
		ENGINE=MyISAM;";

	$update_sql[]="
		CREATE TABLE `news__listtpl_language` (
			`listtpl_id` INT(11) NOT NULL DEFAULT '0',
			`language` VARCHAR(3) NOT NULL DEFAULT '' COLLATE 'utf8_spanish2_ci',
			`listtpl` VARCHAR(100) NOT NULL DEFAULT '' COLLATE 'utf8_spanish2_ci',
			`description` VARCHAR(255) NOT NULL DEFAULT '' COLLATE 'utf8_spanish2_ci'
		)
		COLLATE='utf8_spanish2_ci'
		ENGINE=MyISAM;";

	$update_sql[]="
		CREATE TABLE `news__formtpl` (
			`formtpl_id` INT(11) NOT NULL AUTO_INCREMENT,
			`file_name` VARCHAR(100) NOT NULL DEFAULT '' COLLATE 'utf8_spanish2_ci',
			`ordre` INT(11) NOT NULL DEFAULT '0',
			PRIMARY KEY (`formtpl_id`)
		)
		COLLATE='utf8_spanish2_ci'
		ENGINE=MyISAM;";

	$update_sql[]="
		CREATE TABLE `news__formtpl_language` (
			`formtpl_id` INT(11) NOT NULL DEFAULT '0',
			`language` VARCHAR(3) NOT NULL DEFAULT '' COLLATE 'utf8_spanish2_ci',
			`formtpl` VARCHAR(100) NOT NULL DEFAULT '' COLLATE 'utf8_spanish2_ci',
			`description` VARCHAR(255) NOT NULL DEFAULT '' COLLATE 'utf8_spanish2_ci'
		)
		COLLATE='utf8_spanish2_ci'
		ENGINE=MyISAM;";

	$update_sql[]="
		ALTER TABLE `news__category`
			ADD COLUMN `listtpl_id` INT(11) NOT NULL DEFAULT '1'  AFTER `category_id`;";
	$update_sql[]="
		ALTER TABLE `news__new`
			ADD COLUMN `formtpl_id` INT(11) NOT NULL DEFAULT '1'  AFTER `category_id`;";


}
	
?>