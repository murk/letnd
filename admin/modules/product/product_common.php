<?
// funcions comunes de public i admin per productes

// poso el numero de factura al pagar o al retornar
// si ja està posat, doncs no faig res més, nomes s'ha d'entrar un cop i prou
// Si order_status, calculop quan haig d'entrar numero factura
function product_set_invoice_id( $order_id, $order_status = false, $is_refund = false ) {

	Debug::p( $order_id, $order_status );
	// si es passa es order_status
	if ( $order_status ) {
		//'paying','failed','pending','denied','voided','refunded','completed','preparing','shipped','delivered','collect'
		if (
			$order_status != 'completed'
			&& $order_status != 'refunded'
			&& $order_status != 'preparing'
			&& $order_status != 'shipped'
			&& $order_status != 'delivered'
			&& $order_status != 'collect'
		) {
			return;
		}
		if ( $order_status == 'refunded' ) {
			$is_refund = true;

			// TODO-i Nomes s'hauria de fer quan hi ha un canvi d'estat
			$GLOBALS['gl_reload'] = true;
		}

	}

	// totes les marcades amb -1 no faig res, son anteriors a aquesta actualitzacio
	if ( Db::get_first( "SELECT count(*)
				FROM product__order WHERE order_id=" . $order_id . ' AND invoice_id=-1' )
	) {
		return;
	}


	// si ja hi ha numero no toco res
	// si es refund i no hi ha factura tampoc faig res
	// si es refund i hi ha factura, llavors si que haig de crear una factura nova negativa
	if ( $is_refund ) {
		if ( Db::get_first( "SELECT invoice_refund_id
				FROM product__order WHERE order_id=" . $order_id )
		) {
			return;
		}

		if ( ! Db::get_first( "SELECT invoice_id
				FROM product__order WHERE order_id=" . $order_id )
		) {
			return;
		}
	} else {

		if ( Db::get_first( "SELECT invoice_id
				FROM product__order WHERE order_id=" . $order_id )
		) {
			return;
		}
	}


	// Si no te activat facturació segueixo marcant com a -1
	if ( ! PRODUCT_HAS_INVOICE ) {

		$query = "UPDATE product__order SET invoice_id= -1 WHERE  order_id=" . $order_id;

		Db::execute( $query );
		Debug::p( 'Update invoice id', $query );

		return;
	}


	$query      = "SELECT GREATEST(MAX(invoice_id), MAX(invoice_refund_id))
				FROM product__order";
	$invoice_id = Db::get_first( $query ) + 1;

	$field      = $is_refund ? 'invoice_refund_id' : 'invoice_id';
	$field_date = $is_refund ? 'invoice_refund_date' : 'invoice_date';

	$query = "UPDATE product__order SET " .
	         $field . "= " . $invoice_id . ", " .
	         $field_date . "= now() 
				WHERE  order_id=" . $order_id;

	Db::execute( $query );

	Debug::p( 'Update invoice id', $query );

}

/**
 * @param $invoice_id
 * @param $prefix
 * @param $invoice_date
 *
 * @return mixed
 *
 * Funció que serveix per poder tindre números de factures amb l'any actual i l'id en qualsevol lloc del prefix
 * El prefix pot contindre 2 variables:
 *  %yyyy -> any 4 cifres
 *  %yy% -> any 2 cifres
 *  %i% -> invoice_id
 *  %ii% -> invoice_id 2 digits
 *  %iii% -> invoice_id 3 digits
 *  %iiii% -> invoice_id 4 digits
 *
 */
function product_get_invoice_number( $prefix, $invoice_id, $invoice_date ) {

	if ($invoice_id == '-1' || $invoice_id == 0) return $invoice_id;

	$invoice_date = strtotime( $invoice_date );
	$invoice_number = '';
	$id_found      = false;
	// Id
	if ( strpos( $prefix, '%iiii%' ) !== false ) {
		$id_found      = true;
		$id = zerofill($invoice_id, 4);
		$invoice_number = str_replace( '%iiii%', $id, $prefix );
	}
	if ( strpos( $prefix, '%iii%' ) !== false ) {
		$id_found      = true;
		$id = zerofill($invoice_id, 3);
		$invoice_number = str_replace( '%iii%', $id, $prefix );
	}
	if ( strpos( $prefix, '%ii%' ) !== false ) {
		$id_found      = true;
		$id = zerofill($invoice_id, 2);
		$invoice_number = str_replace( '%ii%', $id, $prefix );
	}
	if ( strpos( $prefix, '%i%' ) !== false ) {
		$id_found      = true;
		$id = $invoice_id;
		$invoice_number = str_replace( '%i%', $id, $prefix );
	}

	if ( ! $id_found ) {
		$invoice_number = $prefix . $invoice_id;
	}

	// Any
	if ( strpos( $prefix, '%yyyy%' ) !== false ) {
		$year           = date( 'Y', $invoice_date );
		$invoice_number = str_replace( '%yyyy%', $year, $invoice_number );
	}
	if ( strpos( $prefix, '%yy%' ) !== false ) {
		$year           = date( 'y', $invoice_date );
		$invoice_number = str_replace( '%yy%', $year, $invoice_number );
	}

	return $invoice_number;
}

?>