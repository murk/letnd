<? /*
 Títol per incloure a algua pàgina en concret quan no es fa servir el que hi ha a general.tpl
  */

?>

<? if( R::get ('template') != 'clean' ): ?>

	<header>
		<h1 class="page-title" title="<?= htmlspecialchars( Page::get_title() ) ?>"><?= Page::get_title() ?></h1>
		<? if( Page::get_body_subtitle() ): ?>
			<h2 title="<?= htmlspecialchars( Page::get_body_subtitle() ) ?>"><?= Page::get_body_subtitle() ?></h2>
		<? endif //body_subtitle?>
	</header>

<? endif // $template != 'clean' ?>