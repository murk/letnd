<?
/**
 * LlemenaRestaurant
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2011
 * @version $Id$
 * @access public
 */
class LlemenaRestaurant extends Module{

	function __construct(){
		parent::__construct();
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function get_records()
	{
		$listing = new ListRecords($this);
		$listing->condition = "status = 'public'";
		
		$municipi = R::escape('municipi');
		if ($municipi) $listing->condition .= " AND municipi = '" . $municipi . "'";
		
		$listing->call('records_walk','url,customer_description,is_cuina');
		$listing->order_by = 'ordre ASC, customer_title ASC';
		return $listing->list_records();
	}
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{
		$show = new ShowForm($this);
		$show->condition = "status = 'public'";
		$show->call('records_walk','url');
		$show->get_values();		
		if ($show->has_results)
		{
			$GLOBALS['gl_page']->title = $show->rs['customer_title'];
			$GLOBALS['gl_page']->page_title = $show->rs['customer_title'];
			$GLOBALS['gl_page']->page_description = add_dots_meta_description($show->rs['customer_description']);
			$GLOBALS['gl_breadcrumb'] = $show->rs['customer_title'];
			return $show->show_form();
		}
		else
		{
			Main::redirect('/');
		}
	}
	function records_walk($url, $customer_description=false,$is_cuina=0){			
		// si es llistat passa lavariable content
		$ret = [];
		if ($customer_description!==false){
			$ret['customer_description']=add_dots('customer_description', $customer_description);
			$ret['is_cuina']=$is_cuina?$this->caption['c_yes']:$this->caption['c_no'];
		}
		if (strpos($url,'://')===false && $url) $ret['url']= 'http://' . $url;	
		
		return $ret;
	}
}
?>