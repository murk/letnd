<?php

/**
 * CustumerHistory
 *
 * @package
 * @author Marc
 * @copyright Copyright (c) 2010
 * @version $Id$
 * @access public
 */
class CustumerHistory extends Module {
	var $custumer_id, $property_id, $is_property_price_history = false, $group_messages = false, $bin = 0;

	function __construct() {
		parent::__construct();
		include_once( DOCUMENT_ROOT . '/admin/modules/inmo/inmo_common.php' );
	}

	function list_records() {
		$GLOBALS['gl_content'] .= $this->get_records();
	}

	function get_records() {


		$this->is_property_price_history = R::text_id( 'property_price_history' );

		$this->caption['c_edit_history_button'] = $this->caption['c_edit'];

		$listing           = new ListRecords( $this );
		$listing->order_by = 'date DESC, history_id DESC';
		$bin               = $this->bin = $listing->is_bin ? '1' : '0';

		$common_condition = inmo_history_get_user_permission( true );
		$and              = $common_condition ? ' AND' : '';

		if ( $this->parent == 'custumer__custumer' ) {
			$this->group_messages = 'property_id';
			$listing->set_field( 'custumer_id', 'type', 'hidden' );
			$listing->set_field( 'property_id', 'type', 'none' );
			$listing->name = 'history';
			$listing->add_filter( 'status' );
			$listing->add_filter( 'tipus' );
			$listing->set_options( 1, 1, 1, 1, 1, 0, 0, 0, 1, 1 );
			$listing->add_button( 'edit_history_button', '', 'boto1', 'before', 'show_form_edit_history' );

			$common_condition = "			
				$common_condition
				$and custumer_id = $this->custumer_id
			";

			$listing->condition = "
				$common_condition
				AND (
					message_id = 0
					OR custumer__history.history_id IN (
			            SELECT history_id
						FROM (
							SELECT history_id
							FROM custumer__history
							WHERE $common_condition
							AND custumer__history.bin = $bin
							GROUP BY message_id
							ORDER BY message_id
						) AS T1
					)
				)
			";

			$listing->form_link          = "/admin/?tool=custumer&tool_section=history&process=save_custumer_history&custumer_id=" . $this->custumer_id;
			$listing->no_records_message = $this->caption['c_no_history'];


		}
		elseif ( $this->parent == 'inmo__property' ) {
			$this->group_messages = 'custumer_id';
			$listing->set_field( 'property_id', 'type', 'hidden' );
			$listing->set_field( 'custumer_id', 'type', 'none' );
			$listing->name = 'property';
			$listing->add_filter( 'status' );
			$listing->add_filter( 'tipus' );
			$listing->set_options( 1, 1, 1, 1, 1, 0, 0, 0, 1, 1 );
			$listing->condition = 'property_id = ' . $this->property_id;
			$listing->add_button( 'edit_button', '', 'boto1', 'before', 'edit_history' );

			// El subquery utilitzant-lo dins el FROM es molt més ràpid, es veu que així Mysql prioritza el Subqueyr de dins el FROM
			// De laltra manera tardava 500 segons, i així 0,09 s
			/*
			$listing->condition = "
				property_id = $this->property_id
			      AND (message_id = 0 OR
			           custumer__history.history_id IN (
				           SELECT history_id
				           FROM custumer__history
				           WHERE custumer__history.bin = 0
				           AND property_id = $this->property_id
				           GROUP BY message_id
				           ORDER BY message_id
			           )
			      )
			";
			*/

			$common_condition = "			
				$common_condition
				$and property_id = $this->property_id
			";

			$listing->condition = "				
				$common_condition
		            AND (
		                message_id = 0 
			            OR
			            custumer__history.history_id IN (
				            SELECT history_id
							FROM (
								SELECT history_id
								FROM custumer__history
								WHERE $common_condition
								AND custumer__history.bin = $bin
								GROUP BY message_id
								ORDER BY message_id
							) AS T1				           
			           )
		            )
			";


			$listing->form_link = "/admin/?tool=custumer&tool_section=history&process=save_property_history&custumer_id=" . $this->custumer_id;
		}
		elseif ( $this->is_property_price_history ) {
			$GLOBALS['gl_content']       .= '<script language="JavaScript" src="/admin/modules/inmo/jscripts/history.js?v=' . $GLOBALS['gl_version'] . '"></script>';
			$GLOBALS['gl_page']->title   = $this->caption['c_price_history_title'];
			$listing->no_records_message = $this->caption['c_no_price_history'];
			$tipus                       = R::text_id( 'tipus' );
			$property_id                 = R::text_id( 'property_id' );

			$common_condition   = "			
				$common_condition
				$and property_id = '$property_id' AND tipus = '$tipus'
			";
			$listing->condition = $common_condition;

			$listing->name = 'history';
			$listing->set_options( 0, 0, 0, 0, 0, 0, 0, 0, 1, 0 );
			$listing->add_button( 'edit_button', '', 'boto1', 'before', 'edit_price_history' );
			$listing->set_field( 'history_title', 'type', 'hidden' );
			$listing->unset_field( 'finished_date' );
			$listing->unset_field( 'property_id' );
			$listing->unset_field( 'status' );
		}
		else {

			// TODO-i Agrupar igual que a property i a custumer, però amb les 2 agrupacions
			$this->group_messages = 'all';
			$listing->add_filter( 'status' );
			$listing->add_filter( 'tipus' );
			$listing->set_field( 'property_id', 'type', 'none' );
			$listing->set_field( 'custumer_id', 'type', 'none' );

			$common_condition = "
				$common_condition
			";


			$tipus                       = R::text_id( 'tipus' );
			if (!$tipus || $tipus=='null'){

				$common_condition .= "
					$and tipus <> 'price_changed'
				";
				$and = ' AND';
			}


			$listing->condition = "
				$common_condition
				$and (
					message_id = 0
					OR custumer__history.history_id IN (
			            SELECT history_id
						FROM (
							SELECT history_id
							FROM custumer__history
							WHERE $common_condition 
							$and custumer__history.bin = $bin
							GROUP BY message_id
							ORDER BY message_id
						) AS T1
					)
				)
			";
		}
		$listing->set_field( 'history', 'type', 'none' );
		$listing->call( 'list_records_walk', 'history_title,history,message_id,property_id,custumer_id' );

		return $listing->list_records();
	}

	function show_form() {
		$GLOBALS['gl_content'] = $this->get_form();
	}

	function get_form() {
		$show        = new ShowForm( $this );
		$custumer_id = R::id( 'custumer_id' );
		$property_id = R::id( 'property_id' );

		if ( $property_id ) {
			inmo_check_user_permission( $property_id, 'inmo__property', 'property_id' );

			$query = "SELECT ref, property_private FROM inmo__property WHERE property_id = " . $property_id;
			$rs    = Db::get_row( $query );
			$show->set_vars( $rs );

			$show->set_field( 'property_id', 'type', 'hidden' );
			$show->set_field( 'property_id', 'default_value', $property_id );

			$show->set_field( 'custumer_id', 'form_admin', 'input' );

			// permisos del desplegable
			$customer_id_condition = $show->get_field( 'custumer_id', 'select_condition' );
			$customer_id_condition = inmo_history_get_user_permission( $customer_id_condition );

			$show->set_field( 'custumer_id', 'select_condition', $customer_id_condition );

			$show->set_field( 'tipus', 'default_value', 'like_property' );

			$show->tpl->set_var( 'is_custumer', false );// per diferenciar si estem a custumer o inmo
		}
		else {
			// L'historial en prinicipi és de client, despès es va fer el de les propietats
			$show->tpl->set_var( 'custumer', $custumer_id );//li passo el id del client
			$show->tpl->set_var( 'is_custumer', true ); // per diferenciar si estem a custumer o inmo
		}
		$show->get_values();

		if ( $show->rs['message_id'] ) {
			$show->set_field( 'custumer_id', 'form_admin', 'text' );
			$show->set_field( 'custumer_id', 'type', 'none' );
			$show->set_field( 'property_id', 'form_admin', 'text' );
			$show->set_field( 'property_id', 'type', 'none' );
		}


		$show->call( 'form_records_walk', 'message_id,property_id,custumer_id' );


		return $show->show_form();
	}

	function list_records_walk( $history_title, $history, $message_id, $property_id = false, $custumer_id = false ) {


		$ret['history'] = nl2br( $history );


		if ( $history_title ) {
			if ( $this->is_property_price_history ) {
				$history_title = format_int( $history_title );
			}
			$ret['history'] = '<p><strong>' . $history_title . '</strong></p><p>' . $ret['history'] . '</p>';
		}

		$ret['message_id'] = '';
		if ( $message_id )
			$ret['message_id'] = '<a href="/admin/?menu_id=9&action=show_form_copy&message_id=' . $message_id . '">' . $this->caption['c_message_id_view'] . '</a>';


		// Agrupo les propietats del missatge

		$ret['property_id'] = $this->get_grouped_propertys( $message_id, $property_id, $custumer_id );
		$ret['custumer_id'] = $this->get_grouped_custumers( $message_id, $property_id, $custumer_id );


		return $ret;
	}

	function get_grouped_propertys( $message_id, $property_id, $custumer_id ) {

		$ret = '';

		if ( $this->group_messages == 'property_id' || $this->group_messages == 'all' ) {

			$bin = $this->bin;
			$do_query = true;
			if ( $this->group_messages == 'all' ) {
				$subquery_2 = '';
			}
			else {
				$subquery_2 = "AND custumer_id = $custumer_id"; // a la fitxa de custumer mostro nomes els del custumer
			}

			if ( $message_id ) {
				$subquery = "
				INNER JOIN custumer__history USING( property_id )
				WHERE message_id = $message_id 
				$subquery_2 				
				AND custumer__history.bin = $bin
				";
			}
			else {
				$subquery = "
				WHERE property_id = $property_id
				";

				if ( $property_id == 0 ) $do_query = false;
			}

			if ( $do_query ) {
				$query = "
    				SELECT DISTINCT ref,property_private, property_id
    				FROM inmo__property				
    				$subquery				
    				AND inmo__property.bin = 0
    				ORDER BY ref ASC, 
    				property_private ASC
    				LIMIT 15";

				$results = Db::get_rows( $query );

				$count = 0;
				foreach ( $results as $rs ) {
					$count ++;

					// TODO Posar un js per poder veure tots els clients
					if ( $count == 15 ) {
						$ret .= "...";
						break;
					}

					$property         = '';
					$ref              = $rs['ref'];
					$property_private = $rs['property_private'];

					if ( DEBUG ) {
						$property_id_rs = $rs['property_id'];
						$property       .= "Id:$property_id_rs - ";
					}
					if ( $property_private )
						$property .= "$ref -  $property_private";
					else
						$property .= $ref;


					$ret .= "<div>$property</div>";
				}
			}
			else {
				$ret = '';
			}

		}

		return $ret;

	}

	function get_grouped_custumers( $message_id, $property_id, $custumer_id ) {

		$ret = '';
		if ( $this->group_messages == 'custumer_id' || $this->group_messages == 'all' ) {

			$bin = $this->bin;
			$do_query = true;
			if ( $this->group_messages == 'all' ) {
				$subquery_2 = '';
			}
			else {
				$subquery_2 = "AND property_id = $property_id"; // a la fitxa de property mostro nomes els del property
			}
			if ( $message_id ) {
				$subquery = "				
				INNER JOIN custumer__history USING( custumer_id )
				WHERE message_id = $message_id 
				$subquery_2
				AND custumer__history.bin = $bin 
				";
			}
			else {
				$subquery = "
				WHERE custumer_id = $custumer_id
				";

				if ( $custumer_id == 0 ) $do_query = false;
			}

			if ( $do_query ) {
				$query = "
    				SELECT DISTINCT custumer_id, name, surname1, surname2, mail
    				FROM custumer__custumer
    				
    				$subquery
    				
    				AND custumer__custumer.bin = 0 
    				ORDER BY name ASC, 
    				surname1 ASC,
    				surname2 ASC
    				LIMIT 15";

				$results = Db::get_rows( $query );

				$count = 0;
				foreach ( $results as $rs ) {
					$count ++;

					// TODO Posar un js per poder veure tots els clients
					if ( $count == 15 ) {
						$ret .= "...";
						break;
					}

					$name     = $rs['name'];
					$surname1 = $rs['surname1'];
					$surname2 = $rs['surname2'];
					$mail     = $rs['mail'];

					$custumer = "$name $surname1 $surname2";
					if ( ! trim( $custumer ) ) $custumer = $mail;

					$ret .= "<div>$custumer</div>";
				}
			}
			else {
				$ret = '';
			}

			// Si poso un js $ret = "<div class='custumer ellipsis'>${ret['custumer_id']}</div>";

		}


		return $ret;

	}

	function form_records_walk( $message_id, $property_id, $custumer_id ) {

		$custumer_id = R::id( 'custumer_id', $custumer_id );

		$query           = "SELECT * FROM custumer__custumer WHERE custumer_id = " . $custumer_id;
		$rs              = Db::get_row( $query );
		$ret['name']     = $rs['name'];
		$ret['surname1'] = $rs['surname1'];
		$ret['surname2'] = $rs['surname2'];

		if ( $message_id ) {
			$this->group_messages = 'all';
			$ret['property_id']   = $this->get_grouped_propertys( $message_id, $property_id, $custumer_id );
			$ret['custumer_id']   = $this->get_grouped_custumers( $message_id, $property_id, $custumer_id );
		}


		return $ret;
	}

	function write_record() {

		global $gl_action;
		if ( $this->process == 'property' && $this->action == 'bin_record_history' ) {
			$history_id = R::escape( 'history_id' ); // per eliminar desde el jscript
			if ( $history_id ) $_POST['history_id'] = $history_id;
		}

		$writerec = new SaveRows( $this );

		// miro permisos de la propietat que pertany l'historial, si s'afegeix ppropietat id=0 , sempre es te permis
		$property_id = Db::get_first( "SELECT property_id FROM custumer__history WHERE history_id = " . $writerec->id );
		inmo_check_user_permission( $property_id, 'inmo__property', 'property_id' );

		// Event on_save
		$writerec->on_save(
			function ( $ids, $action, $deleted_results, &$save_rows ) {

				$this->on_save_custumer_history( $ids, $action, $deleted_results, $save_rows );

			}, 'message_id'
		);

		$writerec->save();
		if ( $this->process == 'custumer' ) {
			$this->parent      = 'custumer__custumer';
			$this->custumer_id = R::id( 'custumer_id' );
			$gl_action         = 'list_records_history'; // Per l'error en l'enllaç de la paginació
			$history           = $this->do_action( 'get_records' );
			set_inner_html( 'historys', $history );
			print_javascript( "top.hidePopWin()" );
			html_end();
		}
		if ( $this->process == 'property' ) {
			$this->parent      = 'inmo__property';
			$this->property_id = R::id( 'property_id' );
			$gl_action         = 'list_records_history'; // Per l'error en l'enllaç de la paginació
			$history           = $this->do_action( 'get_records' );
			set_inner_html( 'historys', $history );
			print_javascript( "top.hidePopWin()" );
			html_end();
		}

	}

	function save_rows() {
		$save_rows = new SaveRows( $this );

		// Event on_save
		$save_rows->on_save(
			function ( $ids, $action, $deleted_results, &$save_rows ) {

				$this->on_save_custumer_history( $ids, $action, $deleted_results, $save_rows );

			}, 'message_id'
		);

		$save_rows->save();

	}

	/**
	 * Esborra o guarda canvis en tots els registres de l'historial relacionats per mitjà del missatge i client que s'han generat al enviar les propietats d'una demanda a un client
	 *
	 * @param $ids
	 * @param $action
	 * @param $deleted_results
	 * @param $save_rows
	 */
	function on_save_custumer_history( $ids, $action, $deleted_results, &$save_rows ) {
		if ( $action == 'save_rows_save_records' || $action == 'save_record' ) {
			if ( ! is_array( $ids ) ) $ids = [ $ids ];
			foreach ( $ids as $id ) {
				$query = "
					SELECT message_id, status, tipus, `date`, history, history_title 
					FROM custumer__history WHERE 
					history_id = $id AND message_id > 0";
				$rs    = Db::get_row( $query );

				// Quan s'edita un missatge, es sincronitzen tots els que tenen aquell missatge_id (es machaca el que s'hagi pogut fer des de una propietat o un client)
				if ( $rs ) {
					$message_id    = $rs['message_id'];
					$status        = Db::qstr( $rs['status'] );
					$tipus         = Db::qstr( $rs['tipus'] );
					$date          = Db::qstr( $rs['date'] );
					$history_title = Db::qstr( $rs['history_title'] );
					$history       = Db::qstr( $rs['history'] );

					if ( $message_id ) {
						$query = "
								UPDATE custumer__history 
								SET 
								status = $status, 
								tipus = $tipus, 
								`date` = $date, 
								history_title = $history_title, 
								history = $history 
								WHERE message_id = $message_id";
						Db::execute( $query );
						Debug::p( $query, 'Query sincronització historial' );
					}
				}
			}
		}
		if ( $action == 'save_rows_bin_selected' || $action == 'bin_record' ) {
			// tots els posats al bin
			if ( ! is_array( $ids ) ) $ids = [ $ids ];
			foreach ( $ids as $id ) {

				$message_id = Db::get_first( "SELECT message_id FROM custumer__history WHERE history_id = $id" );

				if ( $message_id ) {
					$query = "
						UPDATE
						  custumer__history
						SET
						  bin = 1
						WHERE
						  message_id = $message_id
					";
					Db::execute( $query );
					Debug::p( $query, 'Query sincronització historial' );
				}
			}
		}
		if ( $action == 'save_rows_delete_selected' ) {
			// tots els posats al bin
			foreach ( $deleted_results as $rs ) {

				$message_id = $rs['message_id'];

				if ( $message_id ) {
					$query = "
						DELETE
						FROM custumer__history
						WHERE
						  message_id = $message_id
					";
					Db::execute( $query );
					Debug::p( $query, 'Query sincronització historial' );
				}
			}
		}
		if ( $action == 'save_rows_restore_selected' ) {

			foreach ( $ids as $id ) {

				$message_id = Db::get_first( "SELECT message_id FROM custumer__history WHERE history_id = $id" );

				if ( $message_id ) {
					$query = "
						UPDATE
						  custumer__history
						SET
						  bin = 0
						WHERE
						  message_id = $message_id
					";
					Db::execute( $query );
					Debug::p( $query, 'Query sincronització historial' );
				}
			}
		}
	}


	function manage_images() {
		$image_manager = new ImageManager( $this );
		$image_manager->execute();
	}

	static public function add_to_history( $history, $history_title, $tipus, $custumer_id, $property_id, $message_id, $demand_id, $client_id = 0 ) {

		$history       = Db::qstr( $history );
		$history_title = Db::qstr( $history_title );

		$query = "
			INSERT INTO custumer__history
			(custumer_id, property_id, message_id, demand_id,
			`date`, `status`, `finished_date`, `tipus`, history, history_title, client_id) 
			VALUES 
			($custumer_id, $property_id, $message_id, $demand_id,
			now(), 'finished', now(), '$tipus', $history, $history_title, $client_id);";

		Db::execute( $query );
		Debug::add( 'History query', $query );

	}

	static public function add_demand_to_history( $history, $history_title, $tipus, $custumer_id, $property_id, $demand_id, $client_id ) {
		CustumerHistory::add_to_history(
			$history, $history_title, $tipus, $custumer_id, $property_id, 0, $demand_id, $client_id
		);
	}

	static public function add_message_to_history( $history, $history_title, $custumer_id, $message_id ) {

		$history = p2nl( $history );

		CustumerHistory::add_to_history(
			$history, $history_title, 'send_information', $custumer_id, 0, $message_id, 0
		);
	}

	// la necessito perque de inmo es fa servir el formulari en v2
	public function list_records_get_select_long_values() {
		$this->get_select_long_values();
	}

	public function get_select_long_values() {

		//	$field = R::get( 'field' );
		//	$id = R::get( 'id' );
		$limit = 100;

		$q = R::escape( 'query' );
		$q = strtolower( $q );

		$ret                = array();
		$ret['suggestions'] = array();

		$r = &$ret['suggestions'];


		// 1 - Custumer, que comenci
		$query = "SELECT name, surname1, surname2, custumer_id
					FROM custumer__custumer
					WHERE CONCAT(name,' ', surname1, ' ', surname2) LIKE '" . $q . "%'
					AND (name <> '' OR surname1 <> '' OR surname2 <> '' )
					AND bin = 0
					AND activated = 1
					ORDER BY surname1 ASC, name ASC, surname2 ASC
					LIMIT " . $limit;

		$results = Db::get_rows( $query );

		$rest = $limit - count( $results );

		// 2 - Custumer, qualsevol posició
		if ( $rest > 0 ) {

			$query = "SELECT name, surname1, surname2, custumer_id
					FROM custumer__custumer
					WHERE CONCAT(name,' ', surname1, ' ', surname2) LIKE '%" . $q . "%'
					AND CONCAT(name,' ', surname1, ' ', surname2) NOT LIKE '" . $q . "%'
					AND (name <> '' OR surname1 <> '' OR surname2 <> '' )
					AND bin = 0
					AND activated = 1
					ORDER BY surname1 ASC, name ASC, surname2 ASC
					LIMIT " . $rest;

			$results2 = Db::get_rows( $query );

			$results = array_merge( $results, $results2 );

		}

		// Presento resultats
		foreach ( $results as $rs ) {
			$value = $rs['name'];
			if ( $rs['surname1'] ) $value .= $value ? ' ' . $rs['surname1'] : $rs['surname1'];
			if ( $rs['surname2'] ) $value .= $value ? ' ' . $rs['surname2'] : $rs['surname2'];
			$r[] = array(
				'value' => $value,
				'data'  => $rs['custumer_id']
			);
		}


		$rest               = $limit - count( $results );
		$ret['is_complete'] = $rest > 0;

		die ( json_encode( $ret ) );


	}
}

?>