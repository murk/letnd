<?

/**
 * LassdivePrint
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2018
 * @version $Id$
 * @access public
 */
class LassdivePrint extends Module {

	public function get_vars() {

		Main::load_class( 'lassdive', 'reserva_list', 'admin' );
		$ret = [];

		$order_id = R::id( 'order_id' );
		$query    = "
			SELECT deposit, total_basetax, all_basetax, order_status, customer_id 
			FROM product__order 
			WHERE order_id = $order_id";
		$rs       = Db::get_row( $query );

		$deposit       = $rs['deposit'];
		$total_basetax = $rs['total_basetax'];
		$all_basetax   = $rs['all_basetax'];
		$order_status  = $rs['order_status'];
		$customer_id   = $rs['customer_id'];

		if (
			$order_status == 'paying' ||
			$order_status == 'failed' ||
			$order_status == 'pending' ||
			$order_status == 'denied' ||
			$order_status == 'voided' ||
			$order_status == 'refunded'
		) {
			$order_status = 'pending';
		}
		else {
			$order_status = 'payed';
		}

		$query    = "SELECT CONCAT(name, ' ' , surname) FROM product__customer WHERE customer_id = $customer_id";
		$customer = Db::get_first( $query );
		if ($customer === false) $customer = '';


		$reserva_list           = new LassdiveReservaList();
		$reserva_list->is_print = true;
		$all_order_items        = $reserva_list->get_orderitems( $order_id );
		$order_items            = [];

		foreach ( $all_order_items as $rs ) {

			$user_id = $reserva_list->get_orderitem_users( $rs );
			if ( $user_id === false ) $user_id = '';

			$centre_id = $rs['centre_id'];
			$centre    = Db::get_first( "SELECT centre FROM lassdive__centre WHERE centre_id = $centre_id" );

			if ( $centre === false ) $centre = '';

			$order_items [] = [
				'product_title'    => $rs['product_title'],
				'reservaitem_date' => format_date_form( $rs['reservaitem_date'] ),
				'start_time'       => format_time( $rs['start_time'] ),
				'end_time'         => format_time( $rs['end_time'] ),
				'centre_id'        => $centre,
				'user_id'          => $user_id,
			];
		}

		$deposit       = format_currency( $deposit );
		$total_pending = format_currency( $all_basetax - $deposit );

		$length = max( strlen( $deposit ), strlen( $total_pending ) );

		$deposit       = str_pad( $deposit, $length, ' ', STR_PAD_LEFT );
		$total_pending = str_pad( $total_pending, $length, ' ', STR_PAD_LEFT );

		$ret['vars'] = [
			'order_id'      => $order_id,
			'today'         => now(),
			'deposit'       => $deposit . '€',
			'total_basetax' => format_currency( $total_basetax ) . '€',
			'all_basetax'   => format_currency( $all_basetax ) . '€',
			'total_pending' => $total_pending . '€',
			'order_status'  => $order_status,
			'customer'      => $customer,
			'order_items'   => $order_items
		];

		json_end( $ret );

	}

}