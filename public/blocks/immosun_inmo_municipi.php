<?php
$vars['var_name'] = 'municipi_id';

$query = "SELECT Distinct municipi, inmo__municipi.municipi_id as municipi_id
				FROM inmo__municipi, inmo__property
				WHERE inmo__municipi.municipi_id = inmo__property.municipi_id
				AND (
				inmo__municipi.municipi_id = 1000 
				OR inmo__municipi.municipi_id = 117
				OR inmo__municipi.municipi_id = 102
				OR inmo__municipi.municipi_id = 94
				OR inmo__municipi.municipi_id = 95
				)
				AND bin = 0 AND (status = 'onsale' OR status = 'sold' OR status = 'reserved')
				ORDER BY municipi ASC";

$results = Db::get_rows($query);

$results[] = array('municipi_id'=>'0','municipi'=>$gl_caption['c_other_municipis']);

if ($results) {
    foreach($results as $rs) {
        isset($_GET["municipi_id"]) && ($_GET["municipi_id"] == $rs["municipi_id"])?$selected = 1:$selected = 0;
		
		if ($rs["municipi_id"]=='0'){
			$l['link'] = '/' . LANGUAGE . '/inmo/property/v/municipi_id/' . $rs['municipi_id'] . '/block/'.$this->script.'/';
		}
		else
			{		
			$friendly_params['municipi_id'] = $rs['municipi_id'];
			$l['link'] = Page::get_link('inmo','property',false,false,$friendly_params,false,'',false,false);
		}
		
		
        $l['selected'] = $selected;
        $l['item'] = $rs['municipi'];
        $l['value'] = $rs['municipi_id'];
        $loop2[] = $l;
    }
$loop[] = array ('loop' => $loop2,
    'item' => '');
}
?>