<link href="../../templates/styles/general.css" rel="stylesheet" type="text/css" />
<table width="95%" border="0" cellpadding="0" cellspacing="0" class="fonsgeneralsclars">
  <tr>
    <td width="10"><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="10" height="1"></td>
    <td><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="1" height="15"></td>
    <td width="10"><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="10" height="1"></td>
  </tr>
  <tr>
    <td width="10">&nbsp;</td>
    <td align="left" valign="top"><img src="/clients/agenciaportselva/templates/images/logoasseguran.jpg" width="180" height="112"></td>
    <td width="10">&nbsp;</td>
  </tr>
  <tr>
    <td width="10">&nbsp;</td>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="30"><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="30" height="1"></td>
        <td><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="1" height="25"></td>
        <td width="30"><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="30" height="1"></td>
      </tr>
      <tr>
        <td width="30">&nbsp;</td>
        <td height="600" align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td align="left"><p class="text12verdanawhitejustify"><strong>Quiénes somos</strong></p>
              <p class="text12verdanawhitejustify">Un equipo de profesionales de la mediación que pone todos los medios a disposición de sus clientes, con los conocimientos y la experiencia necesarios. Trabajamos en el mercado de la mediación de seguros desde 1983, como agente de seguros, y desde 1988 como corredor de seguros.</p></td>
          </tr>
          <tr>
            <td><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="1" height="15"/></td>
          </tr>
          <tr>
            <td align="center"><table width="80%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td  class="text12verdanawhite" align="center"><div align="center"><strong>Le asesoramos en la contratación de sus pólizas de seguros y representamos sus intereses en el momento de recibir el servicio.</strong> </div></td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="1" height="15"/></td>
          </tr>
          <tr>
            <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="17"><img src="/clients/agenciaportselva/templates/images/logopetit.gif" width="18" height="18"/></td>
                    <td width="15"><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="15" height="1"/></td>
                    <td class="text10verdanawhite"><strong>Nuestros objetivos</strong></td>
                  </tr>
                  <tr>
                    <td width="17"></td>
                    <td width="15">&nbsp;</td>
                    <td><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="15" height="15"/></td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>Servicio de calidad a nuestros clientes mediante:</td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="15" height="15"/></td>
                  </tr>
                  <tr>
                    <td width="17">&nbsp;</td>
                    <td width="15">&nbsp;</td>
                    <td>
                      <li>Absoluta independencia frente a las compañías aseguradoras.</li>
                      <li>Defensa de los intereses de nuestros clientes frente a las compañías aseguradoras.</li>
                      <li>Formación continua.</li>
                      <li>Especialización.</li>
                      <li>Diseño y desarrollo de productos adecuados a cada necesidad.</li>
                      <li>Trato y servicio personalizado en el estudio, la contratación y el mantenimiento del seguro, así como en la tramitación y la gestión del siniestro.</li></td>
                  </tr>
                </table></td>
                <td width="15"><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="15" height="1"/></td>
                <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="17"><img src="/clients/agenciaportselva/templates/images/logopetit.gif" width="18" height="18"/></td>
                    <td width="15"><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="15" height="1"/></td>
                    <td class="text10verdanawhite"><strong>Productos con compañías de primer nivel</strong></td>
                  </tr>
                  <tr>
                    <td width="17"></td>
                    <td width="15">&nbsp;</td>
                    <td><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="15" height="15"/></td>
                  </tr>

                  <tr>
                    <td width="17">&nbsp;</td>
                    <td width="15">&nbsp;</td>
                    <td><li>Incendios y complementarios.</li>
                        <li>Robo y complementarios.</li>						
						<li>Multirriesgo (Hogar , Comunidades, Negocios, Ind&uacute;strias...) </li>						  
	                        <li>Vehículos a motor</li>
	                        <li>Protección jurídica</li>
	                        <li>Responsabilidad civil (General, Explotación, Privada, Productos, Caza y pesca) </li>
	                        <li>Transportes (Cascos, Embarcaciones) </li>
	                        <li>Técnicos (Construcción, Decenal de daños, Equipos electrónicos) </li>
	                        <li>Agrarios</li>
	                        <li>Vida (individual, colectivos y de convenio)</li>
	                        <li>Jubilación</li>
	                        <li>Planes de pensiones </li>
	                        <li>Fondo de inversión</li>
	                        <li>Defunciones</li>
	                        <li>Salud</li>
	                        <li>Accidentes (individual, colectivos y de convenio)</li>
	                        <li>Viajes</li>
	                        <li>Dental</li>
	                        <li>Subsidiario diario por baja laboral</li>
	                        <li>De dependencia</li>
	                        <li>Contingencias varias</li></td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                  </tr>
                </table></td>
              </tr>

            </table></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
        </table><p class="text12verdanawhitejustify">&nbsp;</p>          </td>
        <td width="30">&nbsp;</td>
      </tr>
    </table></td>
    <td width="10">&nbsp;</td>
  </tr>
  <tr>
    <td width="10">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td width="10">&nbsp;</td>
  </tr>
  <tr>
    <td width="10">&nbsp;</td>
    <td><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="40" height="15"></td>
    <td width="10">&nbsp;</td>
  </tr>
</table>
