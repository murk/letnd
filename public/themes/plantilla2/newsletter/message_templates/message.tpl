<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>AG&Egrave;NCIA PORT DE LA SELVA - Centre de Serveis</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<base href="<?=$url?>">
<style type="text/css">
<!--
.Estilo1 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
}
-->
</style>
<style type="text/css">
.titolFitxa {
	font-family:'Verdana', Arial, Helvetica, sans-serif;	
	font-size: 12px;
	color: #002F49;
}
td.details, .details{
	font-size: 12px;
	line-height : 16px;
}
.detailsdes{
	font-size: 11px;
	line-height : 18px;
	color: #333333;
	font-weight: bold;
}
td.detailsCaption {
	font-family:'Verdana', Arial, Helvetica, sans-serif;	
	font-size: 12px;
	color: #000000;
	background-color : #A7B7D5;
}
table.foto {
	background-color : #002c4a;
	border:12px solid #002c4a;
	padding:8px;
}
.titolFitxaProp2 {
	font-size: 15px;
	color: #736E6E;
}
.price{	
font-family:'Verdana', Arial, Helvetica, sans-serif;	
	font-size: 12px;
} 
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<div style="height:10px;"></div>
<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td valign="top"><?if ($preview):?>
        <img src="/clients/agenciaportselva/images/logo_newsletter.jpg" width="184" height="125" />
        <?endif // $preview?><?if (!$preview):?><img src="cid:logo" width="184" height="125" /><?endif // $preview?>
      <br/>
      <br/></td>
  </tr>
  <tr>
    <td height="1" bgcolor="#005191"></td>
  </tr>
</table>
<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td valign="top"><br>
      <table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td width="100%" valign="top"><?=$message?></td>
        </tr>
        <?foreach($pictures as $picture): extract ($picture)?>
          <tr>
            <td align="center">&nbsp;</td>
          </tr>
          <tr>
            <td align="center"><img src="<?=$picture_src?>"></td>
          </tr>
          <tr>
            <td align="center">&nbsp;</td>
          </tr>
          <?endforeach //$images?>
      </table></td>
  </tr>
</table>
<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" id="footer">
  <tr>
    <td align="center">&nbsp;</td>
  </tr>
  <tr>
    <td height="1" bgcolor="#005191"></td>
  </tr>
  <tr>
    <td align="center"><br/>
      <span class="Estilo1"><strong>Telf. 972 387 011</strong>&nbsp;
      Fax. 972 126 194 &nbsp;
      C/ illa, 31 &nbsp;
      17489, El port de la Selva &nbsp;<br/>
      <br/>
      Web: <a href="http://www.agenciaportselva.com">http://www.agenciaportselva.com</a>&nbsp;E-mail: <a href="mailto:info@agenciaportselva.com">info@agenciaportselva.com</a></span></td>
  </tr>
</table>
<br>
<br>
</body>
</html>
