<style>
	.image_select{
		width:684px;
	}
	.image_select div{
		float: left;
		margin: 0 10px 10px 0;
		cursor:pointer;
	}
	.image_select div img{
		border: 3px solid transparent;
		border-radius: 2px;
	}
	.image_select div:hover img{
		border: 3px solid #1381ce;
	}
	.image_select div.selected img{
		border: 3px solid #e1001a;
	}
	.image_select span{
		display: block;
	}
</style>

<script language="JavaScript" src="/admin/jscripts/validacio.js?v=<?=$version?>"></script>
<script language="JavaScript" src="/common/jscripts/jquery.dropshadow.js?v=<?=$version?>"></script>
<script type="text/javascript">
$(function() {
	set_focus(document.theForm);
	
	<?if($is_full_pagina):?>
	
	$("#paginatpl_id_image_select div").click(function(){
		var new_selected = $(this).attr("rel");
		$("#paginatpl_id_image_select div").removeClass("selected");	
		$(this).addClass("selected");				
		$("#paginatpl_id_<?=$id?>").val(new_selected);
		show_tpl(new_selected);
	});
	
	$("#listtpl_id_image_select div").click(function(){
		var new_selected = $(this).attr("rel");
		$("#listtpl_id_image_select div").removeClass("selected");	
		$(this).addClass("selected");				
		$("#listtpl_id_<?=$id?>").val(new_selected);
		show_listtpl(new_selected);
	});
	
	<?endif //is_full_pagina?>
	
});

</script>
<form name="form_delete" method="post" action="<?=$link_action_delete?>" target="save_frame">
  <input name="<?=$id_field?>" type="hidden" value="<?=$id?>">
</form>
<form name="theForm" method="post" action="<?=$link_action?>" onsubmit="<?=$js_string?>" encType="multipart/form-data" target="save_frame">
  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="form">
    <tr>
      <td><?if ($form_new):?><?if ($save_button):?>
          <div id="form_buttons_holder"><table id ="form_buttons">
            <tr>
              <td class="buttons_top"><div class="right"><input type="submit" name="Submit" value="<?=$c_send_new?>" class="btn_form1" /></div></td>
            </tr>
          </table></div>
          <?endif //save_button?>
          <?endif //form_new?>
        <?if ($form_edit):?>               
          <div id="form_buttons_holder"><table id ="form_buttons">
            <tr>
              <td valign="bottom"><div class="menu"><div><?=$c_edit_data?></div><?if ($has_images):?>
            	<a href="<?=$image_link?>"><?=$c_edit_images?></a>
         <?endif //has_images?><? foreach($form_tabs as $r): extract ($r)?><a href="?action=<?=$tab_action?>&<?=$id_field?>=<?=$id?>&menu_id=<?=$menu_id?>"><?=$tab_caption?></a><? endforeach //$loop?></div></td>
              <td class="buttons_top"><div class="right">
										<? if($show_previous_link): ?><a class="boto2 btn_show_previous_link"
										                          href="<?= $show_previous_link ?>"></a><? endif //show_previous_link?>
										<? if($show_next_link): ?><a class="boto2 btn_show_next_link"
										                          href="<?= $show_next_link ?>"></a><? endif //show_next_link?>
               <? if ($back_button):?><a class="btn_form_back" href="<?=$back_button?>"></a><? endif //back_button?>
            <? if ($save_button):?><input type="submit" name="Submit" value="<?=$c_send_edit?>" class="btn_form1"><? endif //save_button?><? if ($delete_button):?><a class="btn_form_delete" href="javascript:submit_form('<?if ($has_bin):?><?=$c_confirm_form_bin?><?else:?><?=$c_confirm_form_deleted?><?endif //has_bin?>', true);">
                <?=$c_delete?>
                </a><? endif //delete_button?><? if ($preview_button):?><a class="btn_form1" href="<?=$preview_link?>" target="_blank">
                <?=$c_preview?>
                </a><? endif //preview_button?><? if ($print_button):?><a class="btn_form1 btn_print" href="<?=$print_link?>">
                <?=$c_print?>
                </a> <? endif //print_button?></div></td>
            </tr>
          </table></div>
          <?endif //form_edit?>
        <table width="100%" cellpadding="0" cellspacing="0" >
          <tr>
            <td class="formCaptionTitle"><strong>
              <?=$c_edit_title?>
              &nbsp;&nbsp;</strong>
              <?if ($form_edit):?><span><?=$c_entered?>:
              <?=$entered?>
              -
              <?=$c_modified?>:
              <?=$modified?></span><?endif //form_edit?></td>
          </tr>
        </table>
        <table width="100%" cellpadding="0" cellspacing="0" class="formstbl">
          <tr>
            <td valign="top" class="formsCaption"><strong>
              <?=$c_pagina?>:</strong>
              <input name="pagina_id[<?=$id?>]" type="hidden" value="<?=$id?>" />
              <input name="pagina_id_javascript" type="hidden" value="<?=$id?>" /><?if ($form_new):?><?=$parent_id?><?=$level?><?=$menu?><?endif //form_new?></td>
            <td class="forms"><table cellpadding="4" cellspacing="0">
                <?foreach($pagina as $l): extract ($l)?>
                  <tr>
					<?if($lang):?>
                    <td class="forms"><?=$lang?></td>
					<?endif //lang?>
                    <td><?=$pagina?></td>
                  </tr>
                  <?endforeach //$pagina?>
              </table></td>
          </tr>
          <tr>
            <td valign="top" class="formsCaption"><strong>
              <?=$c_header_1?>
              :</strong></td>
            <td class="forms"><table cellpadding="4" cellspacing="0">
                <?foreach($header_1 as $l): extract ($l)?>
                  <tr>
					<?if($lang):?>
                    <td class="forms"><?=$lang?></td>
					<?endif //lang?>
                    <td><?=$header_1?></td>
                  </tr>
                  <?endforeach //$header_1?>
              </table></td>
          </tr>
          <tr>
            <td valign="top" class="formsCaption"><strong>
              <?=$c_header_2?>
              :</strong></td>
            <td class="forms"><table cellpadding="4" cellspacing="0">
                <?foreach($header_2 as $l): extract ($l)?>
                  <tr>
					<?if($lang):?>
                    <td class="forms" valign="top"><?=$lang?></td>
					<?endif //lang?>
                    <td><?=$header_2?></td>
                  </tr>
                  <?endforeach //$header_2?>
              </table></td>
          </tr>
		  <?if($is_full_pagina):?>
          <tr>
            <td valign="top" class="formsCaption"><strong>
              <?=$c_title?>
              :</strong></td>
            <td class="forms"><table cellpadding="4" cellspacing="0">
                <?foreach($title as $l): extract ($l)?>
                  <tr>
					<?if($lang):?>
                    <td class="forms"><?=$lang?></td>
					<?endif //lang?>
                    <td><?=$title?></td>
                  </tr>
                  <?endforeach //$title?>
              </table></td>
          </tr>
          <tr id="tpls">
            <td valign="top" class="formsCaption"><strong>
              <?=$c_paginatpl_id?>
              :</strong></td>
            <td class="forms"><?=$paginatpl_id?></td>
          </tr>
          <tr id="listtpls">
            <td valign="top" class="formsCaption"><strong>
              <?=$c_listtpl_id?>
              :</strong></td>
            <td class="forms"><?=$listtpl_id?></td>
          </tr>
		  <?endif //is_full_pagina?>
          <tr>
            <td valign="top" class="formsCaption"><strong>
              <?=$c_status?>
              :</strong></td>
            <td class="forms"><?=$status?></td>
          </tr>
        </table>
		
		<?if($is_full_pagina):?>
		
		  <div id="columna1"><div class="sep_dark"></div>
        <table width="100%" cellpadding="4" cellspacing="0">
          <tr>
            <td class="formCaptionTitle"><strong>
              <?=$c_column_title1?>
              </strong></td>
          </tr>
        </table>
        <table width="100%" cellpadding="0" cellspacing="0" class="formstbl">
          <tr>
            <td valign="top" class="formsCaption"><strong>
              <?=$c_body_subtitle?>
              :</strong></td>
            <td class="forms"><table cellpadding="4" cellspacing="0">
                <?foreach($body_subtitle as $l): extract ($l)?>
                  <tr>
					<?if($lang):?>
                    <td class="forms"><?=$lang?></td>
					<?endif //lang?>
                    <td><?=$body_subtitle?></td>
                  </tr>
                  <?endforeach //$body_subtitle?>
              </table></td>
          </tr>
        </table>
        <table width="100%" cellpadding="0" cellspacing="0" class="formstbl">
          <tr>
            <td class="formsCaptionHor2"><?=$c_body_content?></td>
          </tr>
          <?foreach($body_content as $l): extract ($l)?>
            <tr>
              <td class="formsHor">
					<?if($lang):?><?=$lang?>
                <br /><?endif //lang?>
                <?=$body_content?></td>
            </tr>
            <?endforeach //$body_content?>
        </table>
		  </div>
		  <div id="columna2"><div class="sep_dark"></div>
        <table width="100%" cellpadding="4" cellspacing="0">
          <tr>
            <td class="formCaptionTitle"><strong>
              <?=$c_column_title2?>
              </strong></td>
          </tr>
        </table>
        <table width="100%" cellpadding="0" cellspacing="0" class="formstbl">
          <tr>
            <td valign="top" class="formsCaption"><strong>
              <?=$c_body_subtitle2?>
              :</strong></td>
            <td class="forms"><table cellpadding="4" cellspacing="0">
                <?foreach($body_subtitle2 as $l): extract ($l)?>
                  <tr>
					<?if($lang):?>
                    <td class="forms"><?=$lang?></td>
					<?endif //lang?>
                    <td><?=$body_subtitle2?></td>
                  </tr>
                  <?endforeach //$body_subtitle2?>
              </table></td>
          </tr>
        </table>
        <table width="100%" cellpadding="0" cellspacing="0" class="formstbl">
          <tr>
            <td class="formsCaptionHor2"><?=$c_body_content2?></td>
          </tr>
          <?foreach($body_content2 as $l): extract ($l)?>
            <tr>
              <td class="formsHor">
					<?if($lang):?><?=$lang?>
                <br /><?endif //lang?>
                <?=$body_content2?></td>
            </tr>
            <?endforeach //$body_content2?>
        </table>
		  </div>
		<?/*  <div id="columna3"><div class="sep_dark"></div>
        <table width="100%" cellpadding="4" cellspacing="0">
          <tr>
            <td class="formCaptionTitle"><strong>
              <?=$c_column_title3?>
              </strong></td>
          </tr>
        </table>
        <table width="100%" cellpadding="0" cellspacing="0" class="formstbl">
          <tr>
            <td valign="top" class="formsCaption"><strong>
              <?=$c_title3?>
              :</strong></td>
            <td class="forms"><table cellpadding="4" cellspacing="0">
                <?foreach($title3 as $l): extract ($l)?>
                  <tr>
					<?if($lang):?>
                    <td class="forms"><?=$lang?></td>
					<?endif //lang?>
                    <td><?=$title3?></td>
                  </tr>
                  <?endforeach //$title3?>
              </table></td>
          </tr>
          <tr>
            <td valign="top" class="formsCaption"><strong>
              <?=$c_body_subtitle3?>
              :</strong></td>
            <td class="forms"><table cellpadding="4" cellspacing="0">
                <?foreach($body_subtitle3 as $l): extract ($l)?>
                  <tr>
					<?if($lang):?>
                    <td class="forms"><?=$lang?></td>
					<?endif //lang?>
                    <td><?=$body_subtitle3?></td>
                  </tr>
                  <?endforeach //$body_subtitle3?>
              </table></td>
          </tr>
        </table>
        <table width="100%" cellpadding="0" cellspacing="0" class="formstbl">
          <tr>
            <td class="formsCaptionHor2"><?=$c_body_content3?></td>
          </tr>
          <?foreach($body_content3 as $l): extract ($l)?>
            <tr>
              <td class="formsHor">
					<?if($lang):?><?=$lang?>
                <br /><?endif //lang?>
                <?=$body_content3?></td>
            </tr>
            <?endforeach //$body_content3?>
        </table>
		  </div> */?>
		  
		  
		  
		<div class="sep_dark"></div>
        <table width="100%" cellpadding="4" cellspacing="0">
          <tr>
            <td class="formCaptionTitle"><strong>
              <?=$c_column_title6?>
              </strong></td>
          </tr>
        </table>
        <table width="100%" cellpadding="0" cellspacing="0" class="formstbl">
          <tr>
            <td valign="top" class="formsCaption"><strong>
              <?=$c_url1?>:</strong></td>
            <td class="forms"><table cellpadding="4" cellspacing="0">
                <?foreach($url1 as $l): extract ($l)?>
                  <tr>
					<?if($lang):?>
                    <td class="forms"><?=$lang?></td>
					<?endif //lang?>
                    <td><?=$url1?></td>
                  </tr>
                  <?endforeach //$url1?>
              </table></td>
          </tr>
          <tr>
            <td valign="top" class="formsCaption"><strong>
              <?=$c_url1_name?>:</strong></td>
            <td class="forms"><table cellpadding="4" cellspacing="0">
                <?foreach($url1_name as $l): extract ($l)?>
                  <tr>
					<?if($lang):?>
                    <td class="forms"><?=$lang?></td>
					<?endif //lang?>
                    <td><?=$url1_name?></td>
                  </tr>
                  <?endforeach //$url1?>
              </table></td>
          </tr>
          <tr>
            <td valign="top" class="formsCaption"><strong>
              <?=$c_url2?>:</strong></td>
            <td class="forms"><table cellpadding="4" cellspacing="0">
                <?foreach($url2 as $l): extract ($l)?>
                  <tr>
					<?if($lang):?>
                    <td class="forms"><?=$lang?></td>
					<?endif //lang?>
                    <td><?=$url2?></td>
                  </tr>
                  <?endforeach //$url1?>
              </table></td>
          </tr>
          <tr>
            <td valign="top" class="formsCaption"><strong>
              <?=$c_url2_name?>:</strong></td>
            <td class="forms"><table cellpadding="4" cellspacing="0">
                <?foreach($url2_name as $l): extract ($l)?>
                  <tr>
					<?if($lang):?>
                    <td class="forms"><?=$lang?></td>
					<?endif //lang?>
                    <td><?=$url2_name?></td>
                  </tr>
                  <?endforeach //$url1?>
              </table></td>
          </tr>
          <tr>
            <td valign="top" class="formsCaption"><strong>
              <?=$c_url3?>:</strong></td>
            <td class="forms"><table cellpadding="4" cellspacing="0">
                <?foreach($url3 as $l): extract ($l)?>
                  <tr>
					<?if($lang):?>
                    <td class="forms"><?=$lang?></td>
					<?endif //lang?>
                    <td><?=$url3?></td>
                  </tr>
                  <?endforeach //$url1?>
              </table></td>
          </tr>
          <tr>
            <td valign="top" class="formsCaption"><strong>
              <?=$c_url3_name?>:</strong></td>
            <td class="forms"><table cellpadding="4" cellspacing="0">
                <?foreach($url3_name as $l): extract ($l)?>
                  <tr>
					<?if($lang):?>
                    <td class="forms"><?=$lang?></td>
					<?endif //lang?>
                    <td><?=$url3_name?></td>
                  </tr>
                  <?endforeach //$url1?>
              </table></td>
          </tr>
          <tr>
            <td valign="top" class="formsCaption"><strong>
              <?=$c_file?>:</strong></td>
            <td class="forms"><table cellpadding="4" cellspacing="0">
                <?foreach($file as $l): extract ($l)?>
                  <tr>
					<?if($lang):?>
                    <td class="forms" valign="bottom"><?=$lang?></td>
					<?endif //lang?>
                    <td><?=$file?></td>
                  </tr>
                  <?endforeach //$url1?>
              </table></td>
          </tr>
        </table>
		  
		  
		  
		<div class="sep_dark"></div>
        <table width="100%" cellpadding="4" cellspacing="0">
          <tr>
            <td class="formCaptionTitle"><strong>
              <?=$c_column_title4?>
              </strong></td>
          </tr>
        </table>
        <table width="100%" cellpadding="0" cellspacing="0" class="formstbl">
          <tr>
            <td valign="top" class="formsCaption"><strong>
              <?=$c_videoframe?>:</strong></td>
            <td class="forms"><?=$videoframe?></td>
          </tr>
        </table>
		  
		  <?endif //is_full_pagina?>
		  
		  
		  
		<div class="sep_dark"></div>
        <table width="100%" cellpadding="4" cellspacing="0">
          <tr>
            <td class="formCaptionTitle"><strong>
              <?=$c_column_title5?>
              </strong></td>
          </tr>
        </table>
        <table width="100%" cellpadding="0" cellspacing="0" class="formstbl">
          <tr>
            <td valign="top" class="formsCaption"><strong>
              <?=$c_page_title?>:</strong></td>
            <td class="forms"><table cellpadding="4" cellspacing="0">
                <?foreach($page_title as $l): extract ($l)?>
                  <tr>
                    <td class="forms" valign="top"><?=$lang?></td>
                    <td><?=$page_title?></td>
                  </tr>
                  <?endforeach //$page_title?>
              </table></td>
          </tr>
          <tr>
            <td valign="top" class="formsCaption"><strong>
              <?=$c_page_description?>:</strong></td>
            <td class="forms"><table cellpadding="4" cellspacing="0">
                <?foreach($page_description as $l): extract ($l)?>
                  <tr>
                    <td class="forms" valign="top"><?=$lang?></td>
                    <td><?=$page_description?></td>
                  </tr>
                  <?endforeach //$c_page_description?>
              </table></td>
          </tr>
          <tr>
            <td valign="top" class="formsCaption"><strong>
              <?=$c_page_keywords?>:</strong></td>
            <td class="forms"><table cellpadding="4" cellspacing="0">
                <?foreach($page_keywords as $l): extract ($l)?>
                  <tr>
                    <td class="forms" valign="top"><?=$lang?></td>
                    <td><?=$page_keywords?></td>
                  </tr>
                  <?endforeach //$page_keywords?>
              </table></td>
          </tr>
		  
		  <?if($is_full_pagina):?>
		  
          <tr>
            <td valign="top" class="formsCaption"><strong>
              <?=$c_pagina_file_name?>:</strong></td>
            <td class="forms"><table cellpadding="4" cellspacing="0">
                <?foreach($pagina_file_name as $l): extract ($l)?>
                  <tr>
					<?if($lang):?>
                    <td class="forms"><?=$lang?></td>
					<?endif //lang?>
                    <td><?=$pagina_file_name?></td>
                  </tr>
                  <?endforeach //$pagina_file_name?>
              </table></td>
          </tr>
		  
		  <?endif //is_full_pagina?>
		  
        </table>
		
        <?if ($form_new):?>
          <table width="100%" cellpadding="4" cellspacing="2" class="formsButtonstbl">
            <tr>
              <td align="center" class="formsButtons"><input type="submit" name="Submit" value="<?=$c_send_new?>" class="btn_form1"></td>
            </tr>
          </table>
          <?endif //form_new?>
        <?if ($form_edit):?>
          <table width="100%" cellpadding="4" cellspacing="2" class="formsButtonstbl">
            <tr>
              <td align="center" class="formsButtons"><div class="centerFloat1">
                  <div class="centerFloat2">
                    <input type="submit" name="Submit" value="<?=$c_send_edit?>" class="btn_form1">
                  </div>
                </div></td>
            </tr>
          </table>
          <?endif //form_edit?></td>
    </tr>
  </table>
</form>