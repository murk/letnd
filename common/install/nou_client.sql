/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

CREATE TABLE IF NOT EXISTS `all__block` (
  `block_id` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `script` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `vars` varchar(150) COLLATE utf8_spanish2_ci DEFAULT '',
  `template` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `active` smallint(1) NOT NULL DEFAULT '0',
  `place` enum('ht','hb','t','l','mt','m','mb','r','b') COLLATE utf8_spanish2_ci NOT NULL DEFAULT 'ht',
  `ordre` int(11) NOT NULL DEFAULT '0',
  `solution` enum('product','inmo','web','all') COLLATE utf8_spanish2_ci NOT NULL DEFAULT 'web',
  PRIMARY KEY (`block_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `all__block` DISABLE KEYS */;
INSERT INTO `all__block` (`block_id`, `script`, `vars`, `template`, `active`, `place`, `ordre`, `solution`) VALUES
	('main_nav', 'all_page_recursive', 'menu=1&start_level=1&last_level=2&set_top_title=0&parent_id=0', 'main_nav', 0, 'hb', 2, 'all'),
	('footer_nav', 'all_page_recursive', 'menu=1&start_level=1&last_level=2&set_top_title=0&parent_id=0', 'footer_nav', 0, 'b', 4, 'all'),
	('languages', 'all_language', '', 'languages', 0, 'ht', 1, 'all'),
	('news_home', 'news_new', 'in_home=1&show_images=1&show_files=1', 'news_home', 0, 'm', 3, 'all'),
	('inmo_category', 'inmo_category', '', 'inmo_category', 1, 'l', 22, 'inmo'),
	('inmo_price', 'inmo_price', '', 'inmo_price', 1, 'l', 24, 'inmo'),
	('inmo_zone', 'inmo_comarca', '', 'inmo_zone', 1, 'l', 23, 'inmo'),
	('inmo_home', 'inmo_offer', 'random=1&&offer_type=home&show_images=1', 'inmo_home', 0, 'm', 21, 'inmo'),
	('inmo_visited', 'inmo_visited', '', 'inmo_visited', 1, 'mb', 25, 'inmo'),
	('banner_home_slider', 'web_banner', 'place_id=1', 'banner_home_slider', 0, 'm', 10, 'web'),
	('inmo_home_slider', 'inmo_offer', 'random=1&offer_type=home&show_images=1', 'inmo_home_slider', 0, 'm', 20, 'inmo'),
	('footer_nav_second', 'all_page_recursive', 'menu=2&start_level=1&last_level=2&set_top_title=0&parent_id=0', 'footer_nav', 0, 'b', 5, 'all'),
	('breadcrumb', 'all_page_breadcrumb', 'show_home=1&minim_items=2', 'breadcrumb', 0, 't', 6, 'all'),
	('product_family', 'product_family_v2', '', 'product_family', 1, 'l', 100, 'product'),
	('product_cart', 'product_cart', '', 'product_cart', 0, 'ht', 101, 'product'),
	('product_visited', 'product_visited', '', 'product_visited', 0, 'mb', 102, 'product'),
	('product_destacat', 'product_destacat', '', 'product_destacat', 0, 'm', 103, 'product'),
	('product_logged', 'all_page_recursive', 'menu=4&start_level=2&last_level=3&set_top_title=0&parent_id=1', 'product_login_nav', 0, 'hb', 105, 'product'),
	('product_nologged', 'all_page_recursive', 'menu=3&start_level=2&last_level=3&set_top_title=0&parent_id=1', 'product_login_nav', 0, 'hb', 104, 'product'),
	('product_related', 'product_related', '', 'product_related', 0, 'm', 103, 'product'),
	('inmo_logged', 'all_page_recursive', 'menu=4&start_level=2&last_level=3&set_top_title=0&parent_id=1', 'inmo_login_nav', 0, 'ht', 8, 'inmo'),
	('inmo_login', 'all_page_recursive', 'menu=3&start_level=2&last_level=3&set_top_title=0&parent_id=1', 'inmo_login_nav', 0, 'ht', 7, 'inmo'),
	('product_brand', 'product_brand', '', 'product_brand', 0, 'l', 106, 'product'),
	('product_order', 'product_order', '', 'product_order', 0, 'mt', 45, 'product');
/*!40000 ALTER TABLE `all__block` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `all__block_language` (
  `block_id` varchar(50) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '0',
  `language` char(3) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `title` varchar(100) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `description` varchar(255) COLLATE utf8_spanish2_ci NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `all__block_language` DISABLE KEYS */;
INSERT INTO `all__block_language` (`block_id`, `language`, `title`, `description`) VALUES
	('main_nav', 'cat', 'Menú principal', ''),
	('main_nav', 'spa', 'Menú principal', ''),
	('main_nav', 'eng', 'Main menu', ''),
	('main_nav', 'fra', 'Menu principale', ''),
	('footer_nav', 'cat', 'Menú peu', ''),
	('footer_nav', 'spa', 'Menú pie', ''),
	('footer_nav', 'eng', 'Foot menu', ''),
	('footer_nav', 'fra', 'Menu pied', ''),
	('languages', 'cat', 'Idiomes', ''),
	('languages', 'spa', 'Idiomas', ''),
	('languages', 'eng', 'Languages', ''),
	('languages', 'fra', 'Langues', ''),
	('news_home', 'cat', 'Notícies', ''),
	('news_home', 'spa', 'Noticias', ''),
	('news_home', 'eng', 'News', ''),
	('news_home', 'fra', 'Nouvelles', ''),
	('inmo_category', 'cat', 'Tipus', ''),
	('inmo_category', 'spa', 'Tipo', ''),
	('inmo_category', 'eng', 'Type', ''),
	('inmo_category', 'fra', 'Type', ''),
	('inmo_home', 'fra', 'Immeubles remarquables', ''),
	('inmo_home', 'eng', 'Outstanding properties', ''),
	('inmo_home', 'spa', 'Inmuebles destacados', ''),
	('inmo_home', 'cat', 'Immobles destacats', ''),
	('inmo_price', 'cat', 'Preu', ''),
	('inmo_price', 'spa', 'Precio', ''),
	('inmo_price', 'fra', 'Prix', ''),
	('inmo_price', 'eng', 'Price', ''),
	('inmo_visited', 'cat', 'Immobles visitats', ''),
	('inmo_visited', 'spa', 'Inmuebles visitados', ''),
	('inmo_visited', 'eng', 'Visited properties', ''),
	('inmo_visited', 'fra', 'Immeubles visitées', ''),
	('inmo_zone', 'cat', 'Zona', ''),
	('inmo_zone', 'spa', 'Zona', ''),
	('inmo_zone', 'eng', 'Zone', ''),
	('inmo_zone', 'fra', 'Zone', ''),
	('banner_home_slider', 'cat', 'Banners', ''),
	('banner_home_slider', 'spa', 'Banners', ''),
	('banner_home_slider', 'fra', 'Banners', ''),
	('banner_home_slider', 'eng', 'Banners', ''),
	('inmo_home_slider', 'cat', 'Banners', ''),
	('inmo_home_slider', 'fra', 'Banners', ''),
	('inmo_home_slider', 'spa', 'Banners', ''),
	('inmo_home_slider', 'eng', 'Banners', ''),
	('footer_nav_second', 'spa', 'Menú pie secundario', ''),
	('footer_nav_second', 'fra', 'Menu pied secondaire', ''),
	('footer_nav_second', 'eng', 'Foot secondary menu', ''),
	('footer_nav_second', 'cat', 'Menú peu secundari', ''),
	('breadcrumb', 'cat', 'Breadcrumb', ''),
	('breadcrumb', 'spa', 'Breadcrumb', ''),
	('breadcrumb', 'eng', 'Breadcrumb', ''),
	('breadcrumb', 'fra', 'Breadcrumb', ''),
	('product_family', 'cat', 'Productes', ''),
	('product_family', 'spa', 'Productos', ''),
	('product_family', 'eng', 'Products', ''),
	('product_family', 'fra', 'Produits', ''),
	('product_cart', 'cat', 'Cistella', ''),
	('product_cart', 'spa', 'Cesta', ''),
	('product_cart', 'eng', 'Cart', ''),
	('product_cart', 'fra', 'Panier', ''),
	('product_visited', 'cat', 'Productes visitats', ''),
	('product_visited', 'spa', 'Productos visitados', ''),
	('product_visited', 'eng', 'Visited products', ''),
	('product_visited', 'fra', 'Produits visitées', ''),
	('product_destacat', 'cat', 'Productes destacats', ''),
	('product_destacat', 'spa', 'Productos destacados', ''),
	('product_destacat', 'eng', 'Featured products', ''),
	('product_destacat', 'fra', 'Produits  remarquables', ''),
	('product_nologged', 'cat', '', ''),
	('product_nologged', 'spa', '', ''),
	('product_nologged', 'eng', '', ''),
	('product_nologged', 'fra', '', ''),
	('product_logged', 'cat', '', ''),
	('product_logged', 'spa', '', ''),
	('product_logged', 'eng', '', ''),
	('product_logged', 'fra', '', ''),
	('product_related', 'cat', 'Productes relacionats', ''),
	('product_related', 'spa', 'Productos relacionados', ''),
	('product_related', 'eng', 'Related products', ''),
	('product_related', 'fra', 'Productos relacionados', ''),
	('inmo_logged', 'spa', 'Logged', ''),
	('inmo_logged', 'eng', 'Logged', ''),
	('inmo_logged', 'fra', 'Logged', ''),
	('inmo_logged', 'cat', 'Logged', ''),
	('inmo_login', 'spa', 'Login', ''),
	('inmo_login', 'eng', 'Login', ''),
	('inmo_login', 'fra', 'Login', ''),
	('inmo_login', 'cat', 'Login', ''),
	('product_brand', 'spa', '', ''),
	('product_brand', 'eng', '', ''),
	('product_brand', 'fra', '', ''),
	('product_brand', 'cat', '', ''),
	('product_order', 'spa', 'Orden', ''),
	('product_order', 'eng', 'Order', ''),
	('product_order', 'fra', 'Ordre', ''),
	('product_order', 'cat', 'Ordre', '');
/*!40000 ALTER TABLE `all__block_language` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `all__comunitat` (
  `geonameid` int(11) NOT NULL DEFAULT '0',
  `comunitat_id` varchar(20) DEFAULT NULL,
  `country_id` char(10) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  `alternatenames` varchar(4000) DEFAULT NULL,
  PRIMARY KEY (`geonameid`),
  KEY `admin1_id` (`comunitat_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*!40000 ALTER TABLE `all__comunitat` DISABLE KEYS */;
INSERT INTO `all__comunitat` (`geonameid`, `comunitat_id`, `country_id`, `name`, `alternatenames`) VALUES
	(2513413, '31', 'ES', 'Murcia', 'Avtonomnaja oblast\' regiona Mursija,Murcia,Murcie,Mursija,Múrcia,Region Murcia,Region de Murcia,Region de Murcie,Region of Murcia,Región de Murcia,Région de Murcie,?????????? ??????? ??????? ??????,??????,????'),
	(2519582, '1', 'ES', 'Ciudad de Ceuta', 'Ceuta,Ceuto,Ce?to,Ciudad Autonoma de Ceuta,Ciudad Autónoma de Ceuta,Ciudad de Ceuta,Cueta,Eptadelfos,Lungsod ng Ceuta,Sebta,Septa,Septem Fratres,Seuta,Zeuta,?????,?????,????,?????,?????,???,??,???'),
	(2521383, '07', 'ES', 'Illes Balears', 'Avtonomnaja oblast\' Balearskikh Ostrovov,Balear Uharteak,Balearane,Balearen,Balearene,Balearerna,Baleares,Balearic Islands,Balearinsuloj,Balearoj,Balears,Balearskie Ostrova,Baléares,Comunidad Autonoma de Illes Balears,Comunidad Autonoma de las Islas Baleares,Comunidad Autónoma de Illes Balears,Comunidad Autónoma de las Islas Baleares,Comunitat Autonoma de les Illes Balears,Comunitat Autònoma de les Illes Balears,Iles Baleares,Ilhas Baleares,Illas Baleares,Illes Balears,Islas Baleares,Islles Baleares,ses Illes,Îles Baléares,?????????? ??????? ?????????? ????????,?????????? ???????,?????'),
	(2593109, '51', 'ES', 'Andalucía', 'Andalousie,Andalucia,Andalucía,Andalusia,Andalusie,Andalusien,Andalusija,Andaluzio,Baetica,Comunidad Autonoma de Andalucia,Comunidad Autónoma de Andalucía,?????????,??????'),
	(2593110, '53', 'ES', 'Canarias', 'Avtonomnaja oblast\' Kanarskikh Ostrovov,Canarias,Canaries,Canary Islands,Comunidad Autonoma Canaria,Comunidad Autónoma Canaria,Comunidad Canaria,Iles Canaries,Illes Canaries,Illes Canàries,Islas Afortunadas,Islas Canarias,Kanaria,Kanariaj Insuloj,Kanariansaaret,Kanarieoearna,Kanarieöarna,Kanarioyane,Kanarioyene,Kanarische Inseln,Kanariøyane,Kanariøyene,Kanarskie Ostrova,Îles Canaries,?????????? ??????? ????????? ????????,????????? ???????,?????,??????'),
	(2593111, '54', 'ES', 'Castilla-La Mancha', 'Castella i la Manxa,Castilla La Mancha,Castilla-La Mancha,Castille-La Mancha,Castille-La-Manche,Comunidad Autonoma de Castilla-La Mancha,Comunidad Autónoma de Castilla-La Mancha,Kastilien-La Mancha,Kastilija — La-Mancha,Suedkastilien,Südkastilien,???????? — ??-?????,????????????,??????????????'),
	(2593112, '57', 'ES', 'Extremadura', 'Comunidad Autonoma de Extremadura,Comunidad Autónoma de Extremadura,Ehstremadura,Estremadura,Estremadure,Estrémadure,Extremadura,???????????,??????????'),
	(2593113, '60', 'ES', 'Comunidad Valenciana', 'Communaute Valencienne,Communaute de Valence,Communauté Valencienne,Communauté de Valence,Comunidad Valenciana,Comunitat Valenciana,Landes Valencia,Oblast\' Valensija,Pais Valencia,Pais Valenciano,País Valenciano,País Valencià,Region of Valencia,Reino de Valencia,Valencia,Valencian Community,Valensija,????????,??????? ????????,????????????,??????'),
	(3114710, '34', 'ES', 'Asturias', 'Asturias,Asturien,Asturies,Asturija,Asturio,Astúries,Comunidad Autonoma del Principado de Asturias,Comunidad Autónoma del Principado de Asturias,Principado de Asturias,Principality of Asturias,Principaute des Asturies,Principauté des Asturies,???????,???????'),
	(3115609, '32', 'ES', 'Navarra', 'Alta Navarra,Autonome Region Navarra,Autonomous Region of Navarre,Communaute Forale de Navarre,Communauté Forale de Navarre,Comunidad Foral de Navarra,Nabarra,Nafarroa,Nafarroako Foru Komunitatea,Naparroa,Navaro,Navarra,Navarre,Reino de Navarra,Reyno de Navarra,el Viejo Reyno,???????,????'),
	(3117732, '29', 'ES', 'Madrid', 'Autonome Region Madrid,Autonomous Region of Madrid,Communaute de Madrid,Communauté de Madrid,Comunidad Autonoma de Madrid,Comunidad Autónoma de Madrid,Comunidad de Madrid,Comunitat de Madrid,Madrid,Oblast\' Madrid,Region de Madrid,Región de Madrid,??????,??????? ??????,????????????'),
	(3336897, '27', 'ES', 'La Rioja', 'Comunidad Autonoma de La Rioja,Comunidad Autónoma de La Rioja,Errioxa,La Rioja,Logrono,Logroño,Rioja,Riokha,?????,?????'),
	(3336898, '39', 'ES', 'Cantabria', 'Avtonomnaja oblast\' Kantabrija,Cantabria,Cantabrie,Cantàbria,Comunidad Autonoma de Cantabria,Comunidad Autónoma de Cantabria,Kantabria,Kantabrien,Kantabrija,Kantabrio,Provincia de Santander,Santander,la Montana,la Montana de Castilla,la Montaña,la Montaña de Castilla,?????????? ??????? ?????????,?????????,??????'),
	(3336899, '52', 'ES', 'Aragón', 'Arago,Aragon,Aragonien,Aragó,Aragón,Avtonomnaja oblast\' Aragon,Reino de Aragon,Reino de Aragón,?????????? ??????? ??????,??????,????'),
	(3336900, '55', 'ES', 'Castilla y León', 'Castella i Lleo,Castella i Lleó,Castiella y Lleon,Castiella y Lleón,Castilla y Leon,Castilla y León,Castille and Leon,Castille and León,Castille-et-Leon,Castille-et-León,Comunidad Autonoma de Castilla y Leon,Comunidad Autónoma de Castilla y León,Kastilien-Leon,Kastilija i Leon,Nordkastilien,???????? ? ????,?????????????'),
	(3336901, '56', 'ES', 'Catalunya', 'Catalogna,Catalogne,Catalonha,Catalonia,Cataluna,Catalunya,Cataluña,Katalonia,Katalonien,Katalonija,Katalunio,La Catalogna,Principat de Catalunya,el Principat,?????????,?????????,??????,?????,?????'),
	(3336902, '58', 'ES', 'Galicia', 'A Galiza,Comunidade Autonoma de Galicia,Comunidade Autónoma de Galicia,Galice,Galicia,Galicien,Galicija,Galiza,Galícia,???????,????'),
	(3336903, '59', 'ES', 'País Vasco', 'Baskarland,Baskenland,Baskerland,Baskien,Basque Country,Comunidad Autonoma del Pais Vasco,Comunidad Autónoma del País Vasco,Euskadi,Euskal Herria,Euzkadi,Pais Basc,Pais Vasco,Pays Basque,País Basc,País Vasco,Provincias Vascongadas,Strana Baskov,Vascongadas,?????? ??????,???????'),
	(6362988, '2', 'ES', 'Melilla', 'Melil\'ja,Melilla,???????');
/*!40000 ALTER TABLE `all__comunitat` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `all__configadmin` (
  `configadmin_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `value` varchar(255) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`configadmin_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `all__configadmin` DISABLE KEYS */;
INSERT INTO `all__configadmin` (`configadmin_id`, `name`, `value`) VALUES
	(3, 'theme', 'inmotools'),
	(1, 'page_title', 'Letnd Intranet'),
	(2, 'default_menu_id', '103'),
	(4, 'language', 'cat'),
	(5, 'max_results', '25'),
	(6, 'max_page_links', '10'),
	(8, 'date_created', '2014/02/18'),
	(7, 'add_dots_length', '250'),
	(44, 'mls_allow_listing', '0'),
	(10, 'search_percent', '5'),
	(11, 'search_level', '2'),
	(12, 'mls_all', '0'),
	(13, 'mls_comission', '50'),
	(14, 'mls_search_always', '0'),
	(15, 'default_mail', 'marc@letnd.com'),
	(16, 'missatge_on_start', '1'),
	(17, 'version', '7088'),
	(18, 'currency', 'EUR'),
	(19, 'currency_name', '&euro;'),
	(20, 'currency2', 'PTS'),
	(21, 'currency2_name', 'Pts'),
	(22, 'currency_change', '166.386'),
	(23, 'is_colombia', '0'),
	(34, 'use_custom_foot', '0'),
	(33, 'tinymce_indent', '30'),
	(32, 'use_custom_logo', '0'),
	(31, 'company_details', 'Adreça, NIF: Empresa'),
	(35, 'use_custom_logo_footer', '0'),
	(36, 'split_results_add_nbsp', '0'),
	(37, 'theme_advanced_font_sizes', ''),
	(38, 'theme_advanced_fonts', ''),
	(39, 'auto_detect_mobile', '0'),
	(40, 'facebook_default_page_id', ''),
	(41, 'facebook_appid', ''),
	(42, 'facebook_secret', ''),
	(45, 'im_admin_thumb_unsharp', '0.5|0.5|2.5|0.02'),
	(46, 'im_thumb_unsharp', '0.5|0.5|2.5|0.02'),
	(47, 'im_details_unsharp', '1|0.5|2|0.02'),
	(48, 'im_medium_unsharp', ''),
	(49, 'im_big_unsharp', ''),
	(50, 'has_ssl', '0'),
	(51, 'can_edit_pages', '0'),
	(52, 'files_show_download', '1'),
	(53, 'files_override_file_name', '0'),
	(54, 'files_show_original_name', '1'),
	(55, 'has_preview', '1');
/*!40000 ALTER TABLE `all__configadmin` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `all__configadmin_page` (
  `configadmin_page_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `value` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  PRIMARY KEY (`configadmin_page_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `all__configadmin_page` DISABLE KEYS */;
INSERT INTO `all__configadmin_page` (`configadmin_page_id`, `name`, `value`) VALUES
	(1, 'im_admin_thumb_w', '200'),
	(2, 'im_admin_thumb_h', '133'),
	(3, 'im_admin_thumb_q', '80'),
	(4, 'im_thumb_w', '200'),
	(5, 'im_thumb_h', '150'),
	(6, 'im_thumb_q', '80'),
	(7, 'im_details_w', '300'),
	(8, 'im_details_h', '225'),
	(9, 'im_details_q', '80'),
	(10, 'im_medium_w', '900'),
	(11, 'im_medium_h', '675'),
	(12, 'im_medium_q', '70'),
	(13, 'im_big_w', '1200'),
	(14, 'im_big_h', '900'),
	(15, 'im_big_q', '70'),
	(16, 'im_list_cols', '2'),
	(17, 'im_custom_ratios', ''),
	(18, 'show_body_subtitle', '1'),
	(19, 'show_body_description', '1');
/*!40000 ALTER TABLE `all__configadmin_page` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `all__configpublic` (
  `configpublic_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `value` varchar(768) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`configpublic_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `all__configpublic` DISABLE KEYS */;
INSERT INTO `all__configpublic` (`configpublic_id`, `name`, `value`) VALUES
	(1, 'default_home_tool', ''),
	(3, 'language', 'cat'),
	(4, 'max_results', '6'),
	(5, 'max_page_links', '10'),
	(6, 'theme', ''),
	(2, 'default_home_tool_section', ''),
	(7, 'home_max_results', '3'),
	(8, 'page_slogan', ''),
	(9, 'page_title', ''),
	(10, 'page_description', ''),
	(11, 'page_keywords', ''),
	(12, 'page_address', ''),
	(13, 'add_dots_length', '250'),
	(14, 'default_mail', 'marc@letnd.com'),
	(32, 'use_friendly_url', '1'),
	(17, 'currency', 'EUR'),
	(18, 'currency_name', '&euro;'),
	(19, 'currency2', 'PTS'),
	(20, 'currency2_name', 'Pts'),
	(21, 'currency_change', '166.386'),
	(22, 'is_colombia', '0'),
	(24, 'page_phone', '+34 777 888 999'),
	(26, 'google_latitude', '43.51668853502906'),
	(27, 'google_longitude', '4.3505859375'),
	(28, 'google_center_latitude', '43.51668853502906'),
	(29, 'google_center_longitude', '4.3505859375'),
	(30, 'google_zoom', '17'),
	(31, 'has_save_frame', '1'),
	(33, 'auto_detect_language', '1'),
	(34, 'separate_tool_content', '0'),
	(35, 'override_gl_page_title', '1'),
	(36, 'base_url_public', ''),
	(37, 'active_blocks', '1'),
	(38, 'default_tool_section', ''),
	(39, 'default_tool', ''),
	(40, 'google_analytics', ''),
	(41, 'client_version', '1'),
	(42, 'page_numstreet', '24'),
	(43, 'page_flat', ''),
	(44, 'page_door', ''),
	(45, 'page_postcode', '17000'),
	(46, 'page_municipi', 'Poble'),
	(47, 'page_provincia', 'Girona'),
	(48, 'page_country', ''),
	(49, 'page_fax', ''),
	(50, 'from_name', ''),
	(51, 'from_address', 'marc@letnd.com'),
	(52, 'm_host', 'mail.letnd.com'),
	(53, 'm_username', 'marc@letnd.com'),
	(54, 'm_password', 'tanreb'),
	(55, 'page_mobile', ''),
	(56, 'company_name', 'Nom empresa'),
	(57, 'company_cif', ''),
	(59, 'recaptcha_key', ''),
	(60, 'recaptcha_secret', ''),
	(61, 'use_recaptcha', '1'),
	(62, 'google_place_id', ''),
	(63, 'force_css_jscript_generation', '1'),
	(64, 'google_maps_key', '');
/*!40000 ALTER TABLE `all__configpublic` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `all__configpublic_language` (
  `configpublic_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(50) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `value` varchar(255) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `language` char(3) COLLATE utf8_spanish2_ci NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `all__configpublic_language` DISABLE KEYS */;
INSERT INTO `all__configpublic_language` (`configpublic_id`, `name`, `value`, `language`) VALUES
	(8, 'page_slogan', 'Frase corporativa', 'cat'),
	(8, 'page_slogan', 'Frase corporativa', 'spa'),
	(8, 'page_slogan', '', 'eng'),
	(9, 'page_title', 'Titol pàgina per defecte', 'cat'),
	(9, 'page_title', 'Título página por defecto', 'spa'),
	(9, 'page_title', '', 'eng'),
	(10, 'page_description', 'Descripcio per defecte en els cercadors', 'cat'),
	(10, 'page_description', 'Descripcón por defecto en los buscadores', 'spa'),
	(10, 'page_description', '', 'eng'),
	(11, 'page_keywords', '', 'cat'),
	(11, 'page_keywords', '', 'spa'),
	(11, 'page_keywords', '', 'eng'),
	(12, 'page_address', 'Carrer', 'cat'),
	(12, 'page_address', 'Carrer', 'spa'),
	(12, 'page_address', 'Carrer', 'eng'),
	(8, 'page_slogan', '', 'fra'),
	(9, 'page_title', '', 'fra'),
	(10, 'page_description', '', 'fra'),
	(11, 'page_keywords', '', 'fra'),
	(12, 'page_address', 'Carrer', 'fra'),
	(0, 'page_country', 'Espanya', 'cat'),
	(0, 'page_country', 'España', 'spa'),
	(0, 'page_country', 'Spain', 'eng'),
	(0, 'page_country', 'Espagne', 'fra');
/*!40000 ALTER TABLE `all__configpublic_language` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `all__country` (
  `country_id` char(2) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `iso3` char(3) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `iso_numeric` int(11) DEFAULT NULL,
  `fips` varchar(3) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `country` varchar(200) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `european_comunity` tinyint(1) DEFAULT '0',
  `can_deliver` tinyint(1) DEFAULT '1',
  `capital` varchar(200) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `area_in_sq_km` double DEFAULT NULL,
  `population` mediumint(9) DEFAULT NULL,
  `continent` char(2) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `tld` char(10) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `currency_code` char(3) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `currency_name` char(10) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `phone` varchar(10) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `postal_code_format` varchar(100) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `postal_code_regex` varchar(100) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `languages` varchar(200) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `geoname_id` int(11) DEFAULT NULL,
  `neighbours` varchar(50) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `equivalent_fips_code` varchar(3) COLLATE utf8_spanish2_ci DEFAULT NULL,
  PRIMARY KEY (`country_id`),
  KEY `country` (`country`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `all__country` DISABLE KEYS */;
INSERT INTO `all__country` (`country_id`, `iso3`, `iso_numeric`, `fips`, `country`, `european_comunity`, `can_deliver`, `capital`, `area_in_sq_km`, `population`, `continent`, `tld`, `currency_code`, `currency_name`, `phone`, `postal_code_format`, `postal_code_regex`, `languages`, `geoname_id`, `neighbours`, `equivalent_fips_code`) VALUES
	('AD', 'AND', 20, 'AN', 'Andorra', 0, 1, 'Andorra la Vella', 468, 72000, 'EU', '.ad', 'EUR', 'Euro', '376', 'AD###', '^(?:AD)*(d{3})$', 'ca,fr-AD,pt', 3041565, 'ES,FR', ''),
	('AE', 'ARE', 784, 'AE', 'United Arab Emirates', 0, 1, 'Abu Dhabi', 82880, 4621000, 'AS', '.ae', 'AED', 'Dirham', '971', '', '', 'ar-AE,fa,en,hi,ur', 290557, 'SA,OM', ''),
	('AF', 'AFG', 4, 'AF', 'Afghanistan', 0, 1, 'Kabul', 647500, 8388607, 'AS', '.af', 'AFN', 'Afghani', '93', '', '', 'fa-AF,ps,uz-AF,tk', 1149361, 'TM,CN,IR,TJ,PK,UZ', ''),
	('AG', 'ATG', 28, 'AC', 'Antigua and Barbuda', 0, 1, 'St. John\'s', 443, 69000, 'NA', '.ag', 'XCD', 'Dollar', '+1-268', '', '', 'en-AG', 3576396, '', ''),
	('AI', 'AIA', 660, 'AV', 'Anguilla', 0, 1, 'The Valley', 102, 13254, 'NA', '.ai', 'XCD', 'Dollar', '+1-264', '', '', 'en-AI', 3573511, '', ''),
	('AL', 'ALB', 8, 'AL', 'Albania', 0, 1, 'Tirana', 28748, 3619000, 'EU', '.al', 'ALL', 'Lek', '355', '', '', 'sq,el', 783754, 'MK,GR,CS,ME,RS', ''),
	('AM', 'ARM', 51, 'AM', 'Armenia', 0, 1, 'Yerevan', 29800, 2968000, 'AS', '.am', 'AMD', 'Dram', '374', '######', '^(d{6})$', 'hy', 174982, 'GE,IR,AZ,TR', ''),
	('AN', 'ANT', 530, 'NT', 'Netherlands Antilles', 0, 1, 'Willemstad', 960, 136197, 'NA', '.an', 'ANG', 'Guilder', '599', '', '', 'nl-AN,en,es', 3513447, 'GP', ''),
	('AO', 'AGO', 24, 'AO', 'Angola', 0, 1, 'Luanda', 1246700, 8388607, 'AF', '.ao', 'AOA', 'Kwanza', '244', '', '', 'pt-AO', 3351879, 'CD,NA,ZM,CG', ''),
	('AQ', 'ATA', 10, 'AY', 'Antarctica', 0, 1, '', 14000000, 0, 'AN', '.aq', '', '', '', '', '', '', 6697173, '', ''),
	('AR', 'ARG', 32, 'AR', 'Argentina', 0, 1, 'Buenos Aires', 2766890, 8388607, 'SA', '.ar', 'ARS', 'Peso', '54', '@####@@@', '^([A-Z]d{4}[A-Z]{3})$', 'es-AR,en,it,de,fr', 3865483, 'CL,BO,UY,PY,BR', ''),
	('AS', 'ASM', 16, 'AQ', 'American Samoa', 0, 1, 'Pago Pago', 199, 57881, 'OC', '.as', 'USD', 'Dollar', '+1-684', '', '', 'en-AS,sm,to', 5880801, '', ''),
	('AT', 'AUT', 40, 'AU', 'Austria', 1, 1, 'Vienna', 83858, 8205000, 'EU', '.at', 'EUR', 'Euro', '43', '####', '^(d{4})$', 'de-AT,hr,hu,sl', 2782113, 'CH,DE,HU,SK,CZ,IT,SI,LI', ''),
	('AU', 'AUS', 36, 'AS', 'Australia', 0, 1, 'Canberra', 7686850, 8388607, 'OC', '.au', 'AUD', 'Dollar', '61', '####', '^(d{4})$', 'en-AU', 2077456, '', ''),
	('AW', 'ABW', 533, 'AA', 'Aruba', 0, 1, 'Oranjestad', 193, 71566, 'NA', '.aw', 'AWG', 'Guilder', '297', '', '', 'nl-AW,es,en', 3577279, '', ''),
	('AX', 'ALA', 248, '', 'Aland Islands', 0, 1, 'Mariehamn', 0, 26711, 'EU', '.ax', 'EUR', 'Euro', '+358-18', '', '', 'sv-AX', 661882, '', 'FI'),
	('AZ', 'AZE', 31, 'AJ', 'Azerbaijan', 0, 1, 'Baku', 86600, 8177000, 'AS', '.az', 'AZN', 'Manat', '994', 'AZ ####', '^(?:AZ)*(d{4})$', 'az,ru,hy', 587116, 'GE,IR,AM,TR,RU', ''),
	('BA', 'BIH', 70, 'BK', 'Bosnia and Herzegovina', 0, 1, 'Sarajevo', 51129, 4590000, 'EU', '.ba', 'BAM', 'Marka', '387', '#####', '^(d{5})$', 'bs,hr-BA,sr-BA', 3277605, 'CS,HR,ME,RS', ''),
	('BB', 'BRB', 52, 'BB', 'Barbados', 0, 1, 'Bridgetown', 431, 281000, 'NA', '.bb', 'BBD', 'Dollar', '+1-246', 'BB#####', '^(?:BB)*(d{5})$', 'en-BB', 3374084, '', ''),
	('BD', 'BGD', 50, 'BG', 'Bangladesh', 0, 1, 'Dhaka', 144000, 8388607, 'AS', '.bd', 'BDT', 'Taka', '880', '####', '^(d{4})$', 'bn-BD,en', 1210997, 'MM,IN', ''),
	('BE', 'BEL', 56, 'BE', 'Belgium', 1, 1, 'Brussels', 30510, 8388607, 'EU', '.be', 'EUR', 'Euro', '32', '####', '^(d{4})$', 'nl-BE,fr-BE,de-BE', 2802361, 'DE,NL,LU,FR', ''),
	('BF', 'BFA', 854, 'UV', 'Burkina Faso', 0, 1, 'Ouagadougou', 274200, 8388607, 'AF', '.bf', 'XOF', 'Franc', '226', '', '', 'fr-BF', 2361809, 'NE,BJ,GH,CI,TG,ML', ''),
	('BG', 'BGR', 100, 'BU', 'Bulgaria', 1, 1, 'Sofia', 110910, 7262000, 'EU', '.bg', 'BGN', 'Lev', '359', '####', '^(d{4})$', 'bg,tr-BG', 732800, 'MK,GR,RO,CS,TR,RS', ''),
	('BH', 'BHR', 48, 'BA', 'Bahrain', 0, 1, 'Manama', 665, 718000, 'AS', '.bh', 'BHD', 'Dinar', '973', '####|###', '^(d{3}d?)$', 'ar-BH,en,fa,ur', 290291, '', ''),
	('BI', 'BDI', 108, 'BY', 'Burundi', 0, 1, 'Bujumbura', 27830, 8388607, 'AF', '.bi', 'BIF', 'Franc', '257', '', '', 'fr-BI,rn', 433561, 'TZ,CD,RW', ''),
	('BJ', 'BEN', 204, 'BN', 'Benin', 0, 1, 'Porto-Novo', 112620, 8294000, 'AF', '.bj', 'XOF', 'Franc', '229', '', '', 'fr-BJ', 2395170, 'NE,TG,BF,NG', ''),
	('BL', 'BLM', 652, 'TB', 'Saint Barthélemy', 0, 1, 'Gustavia', 21, 8450, 'NA', '.gp', 'EUR', 'Euro', '590', '### ###', '', 'fr', 3578476, '', ''),
	('BM', 'BMU', 60, 'BD', 'Bermuda', 0, 1, 'Hamilton', 53, 65365, 'NA', '.bm', 'BMD', 'Dollar', '+1-441', '@@ ##', '^([A-Z]{2}d{2})$', 'en-BM,pt', 3573345, '', ''),
	('BN', 'BRN', 96, 'BX', 'Brunei', 0, 1, 'Bandar Seri Begawan', 5770, 381000, 'AS', '.bn', 'BND', 'Dollar', '673', '@@####', '^([A-Z]{2}d{4})$', 'ms-BN,en-BN', 1820814, 'MY', ''),
	('BO', 'BOL', 68, 'BL', 'Bolivia', 0, 1, 'La Paz', 1098580, 8388607, 'SA', '.bo', 'BOB', 'Boliviano', '591', '', '', 'es-BO,qu,ay', 3923057, 'PE,CL,PY,BR,AR', ''),
	('BR', 'BRA', 76, 'BR', 'Brazil', 0, 1, 'Brasília', 8511965, 8388607, 'SA', '.br', 'BRL', 'Real', '55', '#####-###', '^(d{8})$', 'pt-BR,es,en,fr', 3469034, 'SR,PE,BO,UY,GY,PY,GF,VE,CO,AR', ''),
	('BS', 'BHS', 44, 'BF', 'Bahamas', 0, 1, 'Nassau', 13940, 301790, 'NA', '.bs', 'BSD', 'Dollar', '+1-242', '', '', 'en-BS', 3572887, '', ''),
	('BT', 'BTN', 64, 'BT', 'Bhutan', 0, 1, 'Thimphu', 47000, 2376000, 'AS', '.bt', 'BTN', 'Ngultrum', '975', '', '', 'dz', 1252634, 'CN,IN', ''),
	('BV', 'BVT', 74, 'BV', 'Bouvet Island', 0, 1, '', 0, 0, 'AN', '.bv', 'NOK', 'Krone', '', '', '', '', 3371123, '', ''),
	('BW', 'BWA', 72, 'BC', 'Botswana', 0, 1, 'Gaborone', 600370, 1842000, 'AF', '.bw', 'BWP', 'Pula', '267', '', '', 'en-BW,tn-BW', 933860, 'ZW,ZA,NA', ''),
	('BY', 'BLR', 112, 'BO', 'Belarus', 0, 1, 'Minsk', 207600, 8388607, 'EU', '.by', 'BYR', 'Ruble', '375', '######', '^(d{6})$', 'be,ru', 630336, 'PL,LT,UA,RU,LV', ''),
	('BZ', 'BLZ', 84, 'BH', 'Belize', 0, 1, 'Belmopan', 22966, 301000, 'NA', '.bz', 'BZD', 'Dollar', '501', '', '', 'en-BZ,es', 3582678, 'GT,MX', ''),
	('CA', 'CAN', 124, 'CA', 'Canada', 0, 1, 'Ottawa', 9984670, 8388607, 'NA', '.ca', 'CAD', 'Dollar', '1', '@#@ #@#', '^([a-zA-Z]d[a-zA-Z]d[a-zA-Z]d)$', 'en-CA,fr-CA', 6251999, 'US', ''),
	('CC', 'CCK', 166, 'CK', 'Cocos Islands', 0, 1, 'West Island', 14, 628, 'AS', '.cc', 'AUD', 'Dollar', '61', '', '', 'ms-CC,en', 1547376, '', ''),
	('CD', 'COD', 180, 'CG', 'Democratic Republic of the Congo', 0, 1, 'Kinshasa', 2345410, 8388607, 'AF', '.cd', 'CDF', 'Franc', '243', '', '', 'fr-CD,ln,kg', 203312, 'TZ,CF,SD,RW,ZM,BI,UG,CG,AO', ''),
	('CF', 'CAF', 140, 'CT', 'Central African Republic', 0, 1, 'Bangui', 622984, 4434000, 'AF', '.cf', 'XAF', 'Franc', '236', '', '', 'fr-CF,ln,kg', 239880, 'TD,SD,CD,CM,CG', ''),
	('CG', 'COG', 178, 'CF', 'Republic of the Congo', 0, 1, 'Brazzaville', 342000, 3039126, 'AF', '.cg', 'XAF', 'Franc', '242', '', '', 'fr-CG,kg,ln-CG', 2260494, 'CF,GA,CD,CM,AO', ''),
	('CI', 'CIV', 384, 'IV', 'Ivory Coast', 0, 1, 'Yamoussoukro', 322460, 8388607, 'AF', '.ci', 'XOF', 'Franc', '225', '', '', 'fr-CI', 2287781, 'LR,GH,GN,BF,ML', ''),
	('CK', 'COK', 184, 'CW', 'Cook Islands', 0, 1, 'Avarua', 240, 21388, 'OC', '.ck', 'NZD', 'Dollar', '682', '', '', 'en-CK,mi', 1899402, '', ''),
	('CL', 'CHL', 152, 'CI', 'Chile', 0, 1, 'Santiago', 756950, 8388607, 'SA', '.cl', 'CLP', 'Peso', '56', '#######', '^(d{7})$', 'es-CL', 3895114, 'PE,BO,AR', ''),
	('CM', 'CMR', 120, 'CM', 'Cameroon', 0, 1, 'Yaoundé', 475440, 8388607, 'AF', '.cm', 'XAF', 'Franc', '237', '', '', 'en-CM,fr-CM', 2233387, 'TD,CF,GA,GQ,CG,NG', ''),
	('CN', 'CHN', 156, 'CH', 'China', 0, 1, 'Beijing', 9596960, 8388607, 'AS', '.cn', 'CNY', 'Yuan Renmi', '86', '######', '^(d{6})$', 'zh-CN,yue,wuu', 1814991, 'LA,BT,TJ,KZ,MN,AF,NP,MM,KG,PK,KP,RU,VN,IN', ''),
	('CO', 'COL', 170, 'CO', 'Colombia', 0, 1, 'Bogotá', 1138910, 8388607, 'SA', '.co', 'COP', 'Peso', '57', '', '', 'es-CO', 3686110, 'EC,PE,PA,BR,VE', ''),
	('CR', 'CRI', 188, 'CS', 'Costa Rica', 0, 1, 'San José', 51100, 4191000, 'NA', '.cr', 'CRC', 'Colon', '506', '####', '^(d{4})$', 'es-CR,en', 3624060, 'PA,NI', ''),
	('CS', 'SCG', 891, 'YI', 'Serbia and Montenegro', 0, 1, 'Belgrade', 102350, 8388607, 'EU', '.cs', 'RSD', 'Dinar', '+381', '#####', '^(d{5})$', 'cu,hu,sq,sr', 863038, 'AL,HU,MK,RO,HR,BA,BG', ''),
	('CU', 'CUB', 192, 'CU', 'Cuba', 0, 1, 'Havana', 110860, 8388607, 'NA', '.cu', 'CUP', 'Peso', '53', 'CP #####', '^(?:CP)*(d{5})$', 'es-CU', 3562981, 'US', ''),
	('CV', 'CPV', 132, 'CV', 'Cape Verde', 0, 1, 'Praia', 4033, 426000, 'AF', '.cv', 'CVE', 'Escudo', '238', '####', '^(d{4})$', 'pt-CV', 3374766, '', ''),
	('CX', 'CXR', 162, 'KT', 'Christmas Island', 0, 1, 'Flying Fish Cove', 135, 361, 'AS', '.cx', 'AUD', 'Dollar', '61', '####', '^(d{4})$', 'en,zh,ms-CC', 2078138, '', ''),
	('CY', 'CYP', 196, 'CY', 'Cyprus', 1, 1, 'Nicosia', 9250, 792000, 'EU', '.cy', 'EUR', 'Euro', '357', '####', '^(d{4})$', 'el-CY,tr-CY,en', 146669, '', ''),
	('CZ', 'CZE', 203, 'EZ', 'Czech Republic', 1, 1, 'Prague', 78866, 8388607, 'EU', '.cz', 'CZK', 'Koruna', '420', '### ##', '^(d{5})$', 'cs,sk', 3077311, 'PL,DE,SK,AT', ''),
	('CH', 'CHE', 756, 'SZ', 'Switzerland', 0, 1, 'Berne', 41290, 7581000, 'EU', '.ch', 'CHF', 'Franc', '41', '####', '^(d{4})$', 'de-CH,fr-CH,it-CH,rm', 2658434, 'DE,IT,LI,FR,AT', ''),
	('DE', 'DEU', 276, 'GM', 'Germany', 1, 1, 'Berlin', 357021, 8388607, 'EU', '.de', 'EUR', 'Euro', '49', '#####', '^(d{5})$', 'de', 2921044, 'CH,PL,NL,DK,BE,CZ,LU,FR,AT', ''),
	('DJ', 'DJI', 262, 'DJ', 'Djibouti', 0, 1, 'Djibouti', 23000, 506000, 'AF', '.dj', 'DJF', 'Franc', '253', '', '', 'fr-DJ,ar,so-DJ,aa', 223816, 'ER,ET,SO', ''),
	('DK', 'DNK', 208, 'DA', 'Denmark', 1, 1, 'Copenhagen', 43094, 5484000, 'EU', '.dk', 'DKK', 'Krone', '45', '####', '^(d{4})$', 'da-DK,en,fo,de-DK', 2623032, 'DE', ''),
	('DM', 'DMA', 212, 'DO', 'Dominica', 0, 1, 'Roseau', 754, 72000, 'NA', '.dm', 'XCD', 'Dollar', '+1-767', '', '', 'en-DM', 3575830, '', ''),
	('DO', 'DOM', 214, 'DR', 'Dominican Republic', 0, 1, 'Santo Domingo', 48730, 8388607, 'NA', '.do', 'DOP', 'Peso', '+1-809 and', '#####', '^(d{5})$', 'es-DO', 3508796, 'HT', ''),
	('DZ', 'DZA', 12, 'AG', 'Algeria', 0, 1, 'Algiers', 2381740, 8388607, 'AF', '.dz', 'DZD', 'Dinar', '213', '#####', '^(d{5})$', 'ar-DZ', 2589581, 'NE,EH,LY,MR,TN,MA,ML', ''),
	('EC', 'ECU', 218, 'EC', 'Ecuador', 0, 1, 'Quito', 283560, 8388607, 'SA', '.ec', 'USD', 'Dollar', '593', '@####@', '^([a-zA-Z]d{4}[a-zA-Z])$', 'es-EC', 3658394, 'PE,CO', ''),
	('EE', 'EST', 233, 'EN', 'Estonia', 1, 1, 'Tallinn', 45226, 1307000, 'EU', '.ee', 'EEK', 'Kroon', '372', '#####', '^(d{5})$', 'et,ru', 453733, 'RU,LV', ''),
	('EG', 'EGY', 818, 'EG', 'Egypt', 0, 1, 'Cairo', 1001450, 8388607, 'AF', '.eg', 'EGP', 'Pound', '20', '#####', '^(d{5})$', 'ar-EG,en,fr', 357994, 'LY,SD,IL', ''),
	('EH', 'ESH', 732, 'WI', 'Western Sahara', 0, 1, 'El-Aaiun', 266000, 273008, 'AF', '.eh', 'MAD', 'Dirham', '212', '', '', 'ar,mey', 2461445, 'DZ,MR,MA', ''),
	('ER', 'ERI', 232, 'ER', 'Eritrea', 0, 1, 'Asmara', 121320, 5028000, 'AF', '.er', 'ERN', 'Nakfa', '291', '', '', 'aa-ER,ar,tig,kun,ti-ER', 338010, 'ET,SD,DJ', ''),
	('ES', 'ESP', 724, 'SP', 'España', 1, 1, 'Madrid', 504782, 8388607, 'EU', '.es', 'EUR', 'Euro', '34', '#####', '^(d{5})$', 'es-ES,ca,gl,eu', 2510769, 'AD,PT,GI,FR,MA', ''),
	('ET', 'ETH', 231, 'ET', 'Ethiopia', 0, 1, 'Addis Ababa', 1127127, 8388607, 'AF', '.et', 'ETB', 'Birr', '251', '####', '^(d{4})$', 'am,en-ET,om-ET,ti-ET,so-ET,sid', 337996, 'ER,KE,SD,SO,DJ', ''),
	('FI', 'FIN', 246, 'FI', 'Finland', 1, 1, 'Helsinki', 337030, 5244000, 'EU', '.fi', 'EUR', 'Euro', '358', 'FI-#####', '^(?:FI)*(d{5})$', 'fi-FI,sv-FI,smn', 660013, 'NO,RU,SE', ''),
	('FJ', 'FJI', 242, 'FJ', 'Fiji', 0, 1, 'Suva', 18270, 931000, 'OC', '.fj', 'FJD', 'Dollar', '679', '', '', 'en-FJ,fj', 2205218, '', ''),
	('FK', 'FLK', 238, 'FK', 'Falkland Islands', 0, 1, 'Stanley', 12173, 2638, 'SA', '.fk', 'FKP', 'Pound', '500', '', '', 'en-FK', 3474414, '', ''),
	('FM', 'FSM', 583, 'FM', 'Micronesia', 0, 1, 'Palikir', 702, 108105, 'OC', '.fm', 'USD', 'Dollar', '691', '#####', '^(d{5})$', 'en-FM,chk,pon,yap,kos,uli,woe,nkr,kpg', 2081918, '', ''),
	('FO', 'FRO', 234, 'FO', 'Faroe Islands', 0, 1, 'Tórshavn', 1399, 48228, 'EU', '.fo', 'DKK', 'Krone', '298', 'FO-###', '^(?:FO)*(d{3})$', 'fo,da-FO', 2622320, '', ''),
	('FR', 'FRA', 250, 'FR', 'France', 1, 1, 'Paris', 547030, 8388607, 'EU', '.fr', 'EUR', 'Euro', '33', '#####', '^(d{5})$', 'fr-FR,frp,br,co,ca,eu', 3017382, 'CH,DE,BE,LU,IT,AD,MC,ES', ''),
	('GA', 'GAB', 266, 'GB', 'Gabon', 0, 1, 'Libreville', 267667, 1484000, 'AF', '.ga', 'XAF', 'Franc', '241', '', '', 'fr-GA', 2400553, 'CM,GQ,CG', ''),
	('GB', 'GBR', 826, 'UK', 'United Kingdom', 1, 1, 'London', 244820, 8388607, 'EU', '.uk', 'GBP', 'Pound', '44', '@# #@@|@## #@@|@@# #@@|@@## #@@|@#@ #@@|@@#@ #@@|GIR0AA', '^(([A-Z]d{2}[A-Z]{2})|([A-Z]d{3}[A-Z]{2})|([A-Z]{2}d{2}[A-Z]{2})|([A-Z]{2}d{3}[A-Z]{2})|([A-Z]d[A-Z]', 'en-GB,cy-GB,gd', 2635167, 'IE', ''),
	('GD', 'GRD', 308, 'GJ', 'Grenada', 0, 1, 'St. George\'s', 344, 90000, 'NA', '.gd', 'XCD', 'Dollar', '+1-473', '', '', 'en-GD', 3580239, '', ''),
	('GE', 'GEO', 268, 'GG', 'Georgia', 0, 1, 'Tbilisi', 69700, 4630000, 'AS', '.ge', 'GEL', 'Lari', '995', '####', '^(d{4})$', 'ka,ru,hy,az', 614540, 'AM,AZ,TR,RU', ''),
	('GF', 'GUF', 254, 'FG', 'French Guiana', 0, 1, 'Cayenne', 91000, 195506, 'SA', '.gf', 'EUR', 'Euro', '594', '#####', '^((97)|(98)3d{2})$', 'fr-GF', 3381670, 'SR,BR', ''),
	('GG', 'GGY', 831, 'GK', 'Guernsey', 0, 1, 'St Peter Port', 78, 65228, 'EU', '.gg', 'GBP', 'Pound', '+44-1481', '@# #@@|@## #@@|@@# #@@|@@## #@@|@#@ #@@|@@#@ #@@|GIR0AA', '^(([A-Z]d{2}[A-Z]{2})|([A-Z]d{3}[A-Z]{2})|([A-Z]{2}d{2}[A-Z]{2})|([A-Z]{2}d{3}[A-Z]{2})|([A-Z]d[A-Z]', 'en,fr', 3042362, '', ''),
	('GH', 'GHA', 288, 'GH', 'Ghana', 0, 1, 'Accra', 239460, 8388607, 'AF', '.gh', 'GHS', 'Cedi', '233', '', '', 'en-GH,ak,ee,tw', 2300660, 'CI,TG,BF', ''),
	('GI', 'GIB', 292, 'GI', 'Gibraltar', 0, 1, 'Gibraltar', 6.5, 27884, 'EU', '.gi', 'GIP', 'Pound', '350', '', '', 'en-GI,es,it,pt', 2411586, 'ES', ''),
	('GL', 'GRL', 304, 'GL', 'Greenland', 0, 1, 'Nuuk', 2166086, 56375, 'NA', '.gl', 'DKK', 'Krone', '299', '####', '^(d{4})$', 'kl,da-GL,en', 3425505, '', ''),
	('GM', 'GMB', 270, 'GA', 'Gambia', 0, 1, 'Banjul', 11300, 1593256, 'AF', '.gm', 'GMD', 'Dalasi', '220', '', '', 'en-GM,mnk,wof,wo,ff', 2413451, 'SN', ''),
	('GN', 'GIN', 324, 'GV', 'Guinea', 0, 1, 'Conakry', 245857, 8388607, 'AF', '.gn', 'GNF', 'Franc', '224', '', '', 'fr-GN', 2420477, 'LR,SN,SL,CI,GW,ML', ''),
	('GP', 'GLP', 312, 'GP', 'Guadeloupe', 0, 1, 'Basse-Terre', 1780, 443000, 'NA', '.gp', 'EUR', 'Euro', '590', '#####', '^((97)|(98)d{3})$', 'fr-GP', 3579143, 'AN', ''),
	('GQ', 'GNQ', 226, 'EK', 'Equatorial Guinea', 0, 1, 'Malabo', 28051, 562000, 'AF', '.gq', 'XAF', 'Franc', '240', '', '', 'es-GQ,fr', 2309096, 'GA,CM', ''),
	('GR', 'GRC', 300, 'GR', 'Greece', 1, 1, 'Athens', 131940, 8388607, 'EU', '.gr', 'EUR', 'Euro', '30', '### ##', '^(d{5})$', 'el-GR,en,fr', 390903, 'AL,MK,TR,BG', ''),
	('GS', 'SGS', 239, 'SX', 'South Georgia and the South Sandwich Islands', 0, 1, 'Grytviken', 3903, 100, 'AN', '.gs', 'GBP', 'Pound', '', '', '', 'en', 3474415, '', ''),
	('GT', 'GTM', 320, 'GT', 'Guatemala', 0, 1, 'Guatemala City', 108890, 8388607, 'NA', '.gt', 'GTQ', 'Quetzal', '502', '#####', '^(d{5})$', 'es-GT', 3595528, 'MX,HN,BZ,SV', ''),
	('GU', 'GUM', 316, 'GQ', 'Guam', 0, 1, 'Hagåtña', 549, 168564, 'OC', '.gu', 'USD', 'Dollar', '+1-671', '969##', '^(969d{2})$', 'en-GU,ch-GU', 4043988, '', ''),
	('GW', 'GNB', 624, 'PU', 'Guinea-Bissau', 0, 1, 'Bissau', 36120, 1503000, 'AF', '.gw', 'XOF', 'Franc', '245', '####', '^(d{4})$', 'pt-GW,pov', 2372248, 'SN,GN', ''),
	('GY', 'GUY', 328, 'GY', 'Guyana', 0, 1, 'Georgetown', 214970, 770000, 'SA', '.gy', 'GYD', 'Dollar', '592', '', '', 'en-GY', 3378535, 'SR,BR,VE', ''),
	('HK', 'HKG', 344, 'HK', 'Hong Kong', 0, 1, 'Hong Kong', 1092, 6898686, 'AS', '.hk', 'HKD', 'Dollar', '852', '', '', 'zh-HK,yue,zh,en', 1819730, '', ''),
	('HM', 'HMD', 334, 'HM', 'Heard Island and McDonald Islands', 0, 1, '', 412, 0, 'AN', '.hm', 'AUD', 'Dollar', ' ', '', '', '', 1547314, '', ''),
	('HN', 'HND', 340, 'HO', 'Honduras', 0, 1, 'Tegucigalpa', 112090, 7639000, 'NA', '.hn', 'HNL', 'Lempira', '504', '@@####', '^([A-Z]{2}d{4})$', 'es-HN', 3608932, 'GT,NI,SV', ''),
	('HR', 'HRV', 191, 'HR', 'Croatia', 1, 1, 'Zagreb', 56542, 4491000, 'EU', '.hr', 'HRK', 'Kuna', '385', 'HR-#####', '^(?:HR)*(d{5})$', 'hr-HR,sr', 3202326, 'HU,SI,CS,BA,ME,RS', ''),
	('HT', 'HTI', 332, 'HA', 'Haiti', 0, 1, 'Port-au-Prince', 27750, 8388607, 'NA', '.ht', 'HTG', 'Gourde', '509', 'HT####', '^(?:HT)*(d{4})$', 'ht,fr-HT', 3723988, 'DO', ''),
	('HU', 'HUN', 348, 'HU', 'Hungary', 1, 1, 'Budapest', 93030, 8388607, 'EU', '.hu', 'HUF', 'Forint', '36', '####', '^(d{4})$', 'hu-HU', 719819, 'SK,SI,RO,UA,CS,HR,AT,RS', ''),
	('ID', 'IDN', 360, 'ID', 'Indonesia', 0, 1, 'Jakarta', 1919440, 8388607, 'AS', '.id', 'IDR', 'Rupiah', '62', '#####', '^(d{5})$', 'id,en,nl,jv', 1643084, 'PG,TL,MY', ''),
	('IE', 'IRL', 372, 'EI', 'Ireland', 1, 1, 'Dublin', 70280, 4156000, 'EU', '.ie', 'EUR', 'Euro', '353', '', '', 'en-IE,ga-IE', 2963597, 'GB', ''),
	('IL', 'ISR', 376, 'IS', 'Israel', 0, 1, 'Jerusalem', 20770, 6500000, 'AS', '.il', 'ILS', 'Shekel', '972', '#####', '^(d{5})$', 'he,ar-IL,en-IL,', 294640, 'SY,JO,LB,EG,PS', ''),
	('IM', 'IMN', 833, 'IM', 'Isle of Man', 0, 1, 'Douglas, Isle of Man', 572, 75049, 'EU', '.im', 'GBP', 'Pound', '+44-1624', '@# #@@|@## #@@|@@# #@@|@@## #@@|@#@ #@@|@@#@ #@@|GIR0AA', '^(([A-Z]d{2}[A-Z]{2})|([A-Z]d{3}[A-Z]{2})|([A-Z]{2}d{2}[A-Z]{2})|([A-Z]{2}d{3}[A-Z]{2})|([A-Z]d[A-Z]', 'en,gv', 3042225, '', ''),
	('IN', 'IND', 356, 'IN', 'India', 0, 1, 'New Delhi', 3287590, 8388607, 'AS', '.in', 'INR', 'Rupee', '91', '######', '^(d{6})$', 'en-IN,hi,bn,te,mr,ta,ur,gu,ml,kn,or,pa,as,ks,sd,sa,ur-IN', 1269750, 'CN,NP,MM,BT,PK,BD', ''),
	('IO', 'IOT', 86, 'IO', 'British Indian Ocean Territory', 0, 1, 'Diego Garcia', 60, 0, 'AS', '.io', 'USD', 'Dollar', '246', '', '', 'en-IO', 1282588, '', ''),
	('IQ', 'IRQ', 368, 'IZ', 'Iraq', 0, 1, 'Baghdad', 437072, 8388607, 'AS', '.iq', 'IQD', 'Dinar', '964', '#####', '^(d{5})$', 'ar-IQ,ku,hy', 99237, 'SY,SA,IR,JO,TR,KW', ''),
	('IR', 'IRN', 364, 'IR', 'Iran', 0, 1, 'Tehran', 1648000, 8388607, 'AS', '.ir', 'IRR', 'Rial', '98', '##########', '^(d{10})$', 'fa-IR,ku', 130758, 'TM,AF,IQ,AM,PK,AZ,TR', ''),
	('IS', 'ISL', 352, 'IC', 'Iceland', 0, 1, 'Reykjavík', 103000, 304000, 'EU', '.is', 'ISK', 'Krona', '354', '###', '^(d{3})$', 'is,en,de,da,sv,no', 2629691, '', ''),
	('IT', 'ITA', 380, 'IT', 'Italy', 1, 1, 'Rome', 301230, 8388607, 'EU', '.it', 'EUR', 'Euro', '39', '####', '^(d{5})$', 'it-IT,de-IT,fr-IT,sl', 3175395, 'CH,VA,SI,SM,FR,AT', ''),
	('JE', 'JEY', 832, 'JE', 'Jersey', 0, 1, 'Saint Helier', 116, 90812, 'EU', '.je', 'GBP', 'Pound', '+44-1534', '@# #@@|@## #@@|@@# #@@|@@## #@@|@#@ #@@|@@#@ #@@|GIR0AA', '^(([A-Z]d{2}[A-Z]{2})|([A-Z]d{3}[A-Z]{2})|([A-Z]{2}d{2}[A-Z]{2})|([A-Z]{2}d{3}[A-Z]{2})|([A-Z]d[A-Z]', 'en,pt', 3042142, '', ''),
	('JM', 'JAM', 388, 'JM', 'Jamaica', 0, 1, 'Kingston', 10991, 2801000, 'NA', '.jm', 'JMD', 'Dollar', '+1-876', '', '', 'en-JM', 3489940, '', ''),
	('JO', 'JOR', 400, 'JO', 'Jordan', 0, 1, 'Amman', 92300, 6198000, 'AS', '.jo', 'JOD', 'Dinar', '962', '#####', '^(d{5})$', 'ar-JO,en', 248816, 'SY,SA,IQ,IL,PS', ''),
	('JP', 'JPN', 392, 'JA', 'Japan', 0, 1, 'Tokyo', 377835, 8388607, 'AS', '.jp', 'JPY', 'Yen', '81', '###-####', '^(d{7})$', 'ja', 1861060, '', ''),
	('KE', 'KEN', 404, 'KE', 'Kenya', 0, 1, 'Nairobi', 582650, 8388607, 'AF', '.ke', 'KES', 'Shilling', '254', '#####', '^(d{5})$', 'en-KE,sw-KE', 192950, 'ET,TZ,SD,SO,UG', ''),
	('KG', 'KGZ', 417, 'KG', 'Kyrgyzstan', 0, 1, 'Bishkek', 198500, 5356000, 'AS', '.kg', 'KGS', 'Som', '996', '######', '^(d{6})$', 'ky,uz,ru', 1527747, 'CN,TJ,UZ,KZ', ''),
	('KH', 'KHM', 116, 'CB', 'Cambodia', 0, 1, 'Phnom Penh', 181040, 8388607, 'AS', '.kh', 'KHR', 'Riels', '855', '#####', '^(d{5})$', 'km,fr,en', 1831722, 'LA,TH,VN', ''),
	('KI', 'KIR', 296, 'KR', 'Kiribati', 0, 1, 'South Tarawa', 811, 110000, 'OC', '.ki', 'AUD', 'Dollar', '686', '', '', 'en-KI,gil', 4030945, '', ''),
	('KM', 'COM', 174, 'CN', 'Comoros', 0, 1, 'Moroni', 2170, 731000, 'AF', '.km', 'KMF', 'Franc', '269', '', '', 'ar,fr-KM', 921929, '', ''),
	('KN', 'KNA', 659, 'SC', 'Saint Kitts and Nevis', 0, 1, 'Basseterre', 261, 39000, 'NA', '.kn', 'XCD', 'Dollar', '+1-869', '', '', 'en-KN', 3575174, '', ''),
	('KP', 'PRK', 408, 'KN', 'North Korea', 0, 1, 'Pyongyang', 120540, 8388607, 'AS', '.kp', 'KPW', 'Won', '850', '###-###', '^(d{6})$', 'ko-KP', 1873107, 'CN,KR,RU', ''),
	('KR', 'KOR', 410, 'KS', 'South Korea', 0, 1, 'Seoul', 98480, 8388607, 'AS', '.kr', 'KRW', 'Won', '82', 'SEOUL ###-###', '^(?:SEOUL)*(d{6})$', 'ko-KR,en', 1835841, 'KP', ''),
	('KW', 'KWT', 414, 'KU', 'Kuwait', 0, 1, 'Kuwait City', 17820, 2596000, 'AS', '.kw', 'KWD', 'Dinar', '965', '#####', '^(d{5})$', 'ar-KW,en', 285570, 'SA,IQ', ''),
	('KY', 'CYM', 136, 'CJ', 'Cayman Islands', 0, 1, 'George Town', 262, 44270, 'NA', '.ky', 'KYD', 'Dollar', '+1-345', '', '', 'en-KY', 3580718, '', ''),
	('KZ', 'KAZ', 398, 'KZ', 'Kazakhstan', 0, 1, 'Astana', 2717300, 8388607, 'AS', '.kz', 'KZT', 'Tenge', '7', '######', '^(d{6})$', 'kk,ru', 1522867, 'TM,CN,KG,UZ,RU', ''),
	('LA', 'LAO', 418, 'LA', 'Laos', 0, 1, 'Vientiane', 236800, 6677000, 'AS', '.la', 'LAK', 'Kip', '856', '#####', '^(d{5})$', 'lo,fr,en', 1655842, 'CN,MM,KH,TH,VN', ''),
	('LB', 'LBN', 422, 'LE', 'Lebanon', 0, 1, 'Beirut', 10400, 3971000, 'AS', '.lb', 'LBP', 'Pound', '961', '#### ####|####', '^(d{4}(d{4})?)$', 'ar-LB,fr-LB,en,hy', 272103, 'SY,IL', ''),
	('LC', 'LCA', 662, 'ST', 'Saint Lucia', 0, 1, 'Castries', 616, 172000, 'NA', '.lc', 'XCD', 'Dollar', '+1-758', '', '', 'en-LC', 3576468, '', ''),
	('LI', 'LIE', 438, 'LS', 'Liechtenstein', 0, 1, 'Vaduz', 160, 34000, 'EU', '.li', 'CHF', 'Franc', '423', '####', '^(d{4})$', 'de-LI', 3042058, 'CH,AT', ''),
	('LK', 'LKA', 144, 'CE', 'Sri Lanka', 0, 1, 'Colombo', 65610, 8388607, 'AS', '.lk', 'LKR', 'Rupee', '94', '#####', '^(d{5})$', 'si,ta,en', 1227603, '', ''),
	('LR', 'LBR', 430, 'LI', 'Liberia', 0, 1, 'Monrovia', 111370, 3334000, 'AF', '.lr', 'LRD', 'Dollar', '231', '####', '^(d{4})$', 'en-LR', 2275384, 'SL,CI,GN', ''),
	('LS', 'LSO', 426, 'LT', 'Lesotho', 0, 1, 'Maseru', 30355, 2128000, 'AF', '.ls', 'LSL', 'Loti', '266', '###', '^(d{3})$', 'en-LS,st,zu,xh', 932692, 'ZA', ''),
	('LT', 'LTU', 440, 'LH', 'Lithuania', 1, 1, 'Vilnius', 65200, 3565000, 'EU', '.lt', 'LTL', 'Litas', '370', 'LT-#####', '^(?:LT)*(d{5})$', 'lt,ru,pl', 597427, 'PL,BY,RU,LV', ''),
	('LU', 'LUX', 442, 'LU', 'Luxembourg', 1, 1, 'Luxembourg', 2586, 486000, 'EU', '.lu', 'EUR', 'Euro', '352', '####', '^(d{4})$', 'lb,de-LU,fr-LU', 2960313, 'DE,BE,FR', ''),
	('LV', 'LVA', 428, 'LG', 'Latvia', 1, 1, 'Riga', 64589, 2245000, 'EU', '.lv', 'LVL', 'Lat', '371', 'LV-####', '^(?:LV)*(d{4})$', 'lv,ru,lt', 458258, 'LT,EE,BY,RU', ''),
	('LY', 'LBY', 434, 'LY', 'Libya', 0, 1, 'Tripolis', 1759540, 6173000, 'AF', '.ly', 'LYD', 'Dinar', '218', '', '', 'ar-LY,it,en', 2215636, 'TD,NE,DZ,SD,TN,EG', ''),
	('MA', 'MAR', 504, 'MO', 'Morocco', 0, 1, 'Rabat', 446550, 8388607, 'AF', '.ma', 'MAD', 'Dirham', '212', '#####', '^(d{5})$', 'ar-MA,fr', 2542007, 'DZ,EH,ES', ''),
	('MC', 'MCO', 492, 'MN', 'Monaco', 0, 1, 'Monaco', 1.95, 32000, 'EU', '.mc', 'EUR', 'Euro', '377', '#####', '^(d{5})$', 'fr-MC,en,it', 2993457, 'FR', ''),
	('MD', 'MDA', 498, 'MD', 'Moldova', 0, 1, 'Chişinău', 33843, 4324000, 'EU', '.md', 'MDL', 'Leu', '373', 'MD-####', '^(?:MD)*(d{4})$', 'ro,ru,gag,tr', 617790, 'RO,UA', ''),
	('ME', 'MNE', 499, 'MJ', 'Montenegro', 0, 1, 'Podgorica', 14026, 678000, 'EU', '.cs', 'EUR', 'Euro', '381', '#####', '^(d{5})$', 'sr,hu,bs,sq,hr,rom', 3194884, 'AL,HR,BA,RS', ''),
	('MF', 'MAF', 663, 'RN', 'Saint Martin', 0, 1, 'Marigot', 53, 33100, 'NA', '.gp', 'EUR', 'Euro', '590', '### ###', '', 'fr', 3578421, '', ''),
	('MG', 'MDG', 450, 'MA', 'Madagascar', 0, 1, 'Antananarivo', 587040, 8388607, 'AF', '.mg', 'MGA', 'Ariary', '261', '###', '^(d{3})$', 'fr-MG,mg', 1062947, '', ''),
	('MH', 'MHL', 584, 'RM', 'Marshall Islands', 0, 1, 'Uliga', 181.3, 11628, 'OC', '.mh', 'USD', 'Dollar', '692', '', '', 'mh,en-MH', 2080185, '', ''),
	('MK', 'MKD', 807, 'MK', 'Macedonia', 0, 1, 'Skopje', 25333, 2061000, 'EU', '.mk', 'MKD', 'Denar', '389', '####', '^(d{4})$', 'mk,sq,tr,rmm,sr', 718075, 'AL,GR,CS,BG,RS', ''),
	('ML', 'MLI', 466, 'ML', 'Mali', 0, 1, 'Bamako', 1240000, 8388607, 'AF', '.ml', 'XOF', 'Franc', '223', '', '', 'fr-ML,bm', 2453866, 'SN,NE,DZ,CI,GN,MR,BF', ''),
	('MM', 'MMR', 104, 'BM', 'Myanmar', 0, 1, 'Yangon', 678500, 8388607, 'AS', '.mm', 'MMK', 'Kyat', '95', '#####', '^(d{5})$', 'my', 1327865, 'CN,LA,TH,BD,IN', ''),
	('MN', 'MNG', 496, 'MG', 'Mongolia', 0, 1, 'Ulan Bator', 1565000, 2996000, 'AS', '.mn', 'MNT', 'Tugrik', '976', '######', '^(d{6})$', 'mn,ru', 2029969, 'CN,RU', ''),
	('MO', 'MAC', 446, 'MC', 'Macao', 0, 1, 'Macao', 254, 449198, 'AS', '.mo', 'MOP', 'Pataca', '853', '', '', 'zh,zh-MO', 1821275, '', ''),
	('MP', 'MNP', 580, 'CQ', 'Northern Mariana Islands', 0, 1, 'Saipan', 477, 80362, 'OC', '.mp', 'USD', 'Dollar', '+1-670', '', '', 'fil,tl,zh,ch-MP,en-MP', 4041467, '', ''),
	('MQ', 'MTQ', 474, 'MB', 'Martinique', 0, 1, 'Fort-de-France', 1100, 432900, 'NA', '.mq', 'EUR', 'Euro', '596', '#####', '^(d{5})$', 'fr-MQ', 3570311, '', ''),
	('MR', 'MRT', 478, 'MR', 'Mauritania', 0, 1, 'Nouakchott', 1030700, 3364000, 'AF', '.mr', 'MRO', 'Ouguiya', '222', '', '', 'ar-MR,fuc,snk,fr,mey,wo', 2378080, 'SN,DZ,EH,ML', ''),
	('MS', 'MSR', 500, 'MH', 'Montserrat', 0, 1, 'Plymouth', 102, 9341, 'NA', '.ms', 'XCD', 'Dollar', '+1-664', '', '', 'en-MS', 3578097, '', ''),
	('MT', 'MLT', 470, 'MT', 'Malta', 1, 1, 'Valletta', 316, 403000, 'EU', '.mt', 'EUR', 'Euro', '356', '@@@ ###|@@@ ##', '^([A-Z]{3}d{2}d?)$', 'mt,en-MT', 2562770, '', ''),
	('MU', 'MUS', 480, 'MP', 'Mauritius', 0, 1, 'Port Louis', 2040, 1260000, 'AF', '.mu', 'MUR', 'Rupee', '230', '', '', 'en-MU,bho,fr', 934292, '', ''),
	('MV', 'MDV', 462, 'MV', 'Maldives', 0, 1, 'Malé', 300, 379000, 'AS', '.mv', 'MVR', 'Rufiyaa', '960', '#####', '^(d{5})$', 'dv,en', 1282028, '', ''),
	('MW', 'MWI', 454, 'MI', 'Malawi', 0, 1, 'Lilongwe', 118480, 8388607, 'AF', '.mw', 'MWK', 'Kwacha', '265', '', '', 'ny,yao,tum,swk', 927384, 'TZ,MZ,ZM', ''),
	('MX', 'MEX', 484, 'MX', 'Mexico', 0, 1, 'Mexico City', 1972550, 8388607, 'NA', '.mx', 'MXN', 'Peso', '52', '#####', '^(d{5})$', 'es-MX', 3996063, 'GT,US,BZ', ''),
	('MY', 'MYS', 458, 'MY', 'Malaysia', 0, 1, 'Kuala Lumpur', 329750, 8388607, 'AS', '.my', 'MYR', 'Ringgit', '60', '#####', '^(d{5})$', 'ms-MY,en,zh,ta,te,ml,pa,th', 1733045, 'BN,TH,ID', ''),
	('MZ', 'MOZ', 508, 'MZ', 'Mozambique', 0, 1, 'Maputo', 801590, 8388607, 'AF', '.mz', 'MZN', 'Meticail', '258', '####', '^(d{4})$', 'pt-MZ,vmw', 1036973, 'ZW,TZ,SZ,ZA,ZM,MW', ''),
	('NA', 'NAM', 516, 'WA', 'Namibia', 0, 1, 'Windhoek', 825418, 2063000, 'AF', '.na', 'NAD', 'Dollar', '264', '', '', 'en-NA,af,de,hz,naq', 3355338, 'ZA,BW,ZM,AO', ''),
	('NC', 'NCL', 540, 'NC', 'New Caledonia', 0, 1, 'Nouméa', 19060, 216494, 'OC', '.nc', 'XPF', 'Franc', '687', '#####', '^(d{5})$', 'fr-NC', 2139685, '', ''),
	('NE', 'NER', 562, 'NG', 'Niger', 0, 1, 'Niamey', 1267000, 8388607, 'AF', '.ne', 'XOF', 'Franc', '227', '####', '^(d{4})$', 'fr-NE,ha,kr,dje', 2440476, 'TD,BJ,DZ,LY,BF,NG,ML', ''),
	('NF', 'NFK', 574, 'NF', 'Norfolk Island', 0, 1, 'Kingston', 34.6, 1828, 'OC', '.nf', 'AUD', 'Dollar', '672', '', '', 'en-NF', 2155115, '', ''),
	('NG', 'NGA', 566, 'NI', 'Nigeria', 0, 1, 'Abuja', 923768, 8388607, 'AF', '.ng', 'NGN', 'Naira', '234', '######', '^(d{6})$', 'en-NG,ha,yo,ig,ff', 2328926, 'TD,NE,BJ,CM', ''),
	('NI', 'NIC', 558, 'NU', 'Nicaragua', 0, 1, 'Managua', 129494, 5780000, 'NA', '.ni', 'NIO', 'Cordoba', '505', '###-###-#', '^(d{7})$', 'es-NI,en', 3617476, 'CR,HN', ''),
	('NL', 'NLD', 528, 'NL', 'Netherlands', 1, 1, 'Amsterdam', 41526, 8388607, 'EU', '.nl', 'EUR', 'Euro', '31', '#### @@', '^(d{4}[A-Z]{2})$', 'nl-NL,fy-NL', 2750405, 'DE,BE', ''),
	('NO', 'NOR', 578, 'NO', 'Norway', 0, 1, 'Oslo', 324220, 4644000, 'EU', '.no', 'NOK', 'Krone', '47', '####', '^(d{4})$', 'no,nb,nn', 3144096, 'FI,RU,SE', ''),
	('NP', 'NPL', 524, 'NP', 'Nepal', 0, 1, 'Kathmandu', 140800, 8388607, 'AS', '.np', 'NPR', 'Rupee', '977', '#####', '^(d{5})$', 'ne,en', 1282988, 'CN,IN', ''),
	('NR', 'NRU', 520, 'NR', 'Nauru', 0, 1, 'Yaren', 21, 13000, 'OC', '.nr', 'AUD', 'Dollar', '674', '', '', 'na,en-NR', 2110425, '', ''),
	('NU', 'NIU', 570, 'NE', 'Niue', 0, 1, 'Alofi', 260, 2166, 'OC', '.nu', 'NZD', 'Dollar', '683', '', '', 'niu,en-NU', 4036232, '', ''),
	('NZ', 'NZL', 554, 'NZ', 'New Zealand', 0, 1, 'Wellington', 268680, 4154000, 'OC', '.nz', 'NZD', 'Dollar', '64', '####', '^(d{4})$', 'en-NZ,mi', 2186224, '', ''),
	('OM', 'OMN', 512, 'MU', 'Oman', 0, 1, 'Muscat', 212460, 3309000, 'AS', '.om', 'OMR', 'Rial', '968', '###', '^(d{3})$', 'ar-OM,en,bal,ur', 286963, 'SA,YE,AE', ''),
	('PA', 'PAN', 591, 'PM', 'Panama', 0, 1, 'Panama City', 78200, 3292000, 'NA', '.pa', 'PAB', 'Balboa', '507', '', '', 'es-PA,en', 3703430, 'CR,CO', ''),
	('PE', 'PER', 604, 'PE', 'Peru', 0, 1, 'Lima', 1285220, 8388607, 'SA', '.pe', 'PEN', 'Sol', '51', '', '', 'es-PE,qu,ay', 3932488, 'EC,CL,BO,BR,CO', ''),
	('PF', 'PYF', 258, 'FP', 'French Polynesia', 0, 1, 'Papeete', 4167, 270485, 'OC', '.pf', 'XPF', 'Franc', '689', '#####', '^((97)|(98)7d{2})$', 'fr-PF,ty', 4020092, '', ''),
	('PG', 'PNG', 598, 'PP', 'Papua New Guinea', 0, 1, 'Port Moresby', 462840, 5921000, 'OC', '.pg', 'PGK', 'Kina', '675', '###', '^(d{3})$', 'en-PG,ho,meu,tpi', 2088628, 'ID', ''),
	('PH', 'PHL', 608, 'RP', 'Philippines', 0, 1, 'Manila', 300000, 8388607, 'AS', '.ph', 'PHP', 'Peso', '63', '####', '^(d{4})$', 'tl,en-PH,fil', 1694008, '', ''),
	('PK', 'PAK', 586, 'PK', 'Pakistan', 0, 1, 'Islamabad', 803940, 8388607, 'AS', '.pk', 'PKR', 'Rupee', '92', '#####', '^(d{5})$', 'ur-PK,en-PK,pa,sd,ps,brh', 1168579, 'CN,AF,IR,IN', ''),
	('PL', 'POL', 616, 'PL', 'Poland', 1, 1, 'Warsaw', 312685, 8388607, 'EU', '.pl', 'PLN', 'Zloty', '48', '##-###', '^(d{5})$', 'pl', 798544, 'DE,LT,SK,CZ,BY,UA,RU', ''),
	('PM', 'SPM', 666, 'SB', 'Saint Pierre and Miquelon', 0, 1, 'Saint-Pierre', 242, 7012, 'NA', '.pm', 'EUR', 'Euro', '508', '', '', 'fr-PM', 3424932, '', ''),
	('PN', 'PCN', 612, 'PC', 'Pitcairn', 0, 1, 'Adamstown', 47, 46, 'OC', '.pn', 'NZD', 'Dollar', '', '', '', 'en-PN', 4030699, '', ''),
	('PR', 'PRI', 630, 'RQ', 'Puerto Rico', 0, 1, 'San Juan', 9104, 3916632, 'NA', '.pr', 'USD', 'Dollar', '+1-787 and', '#####-####', '^(d{9})$', 'en-PR,es-PR', 4566966, '', ''),
	('PS', 'PSE', 275, 'WE', 'Palestinian Territory', 0, 1, 'East Jerusalem', 5970, 3800000, 'AS', '.ps', 'ILS', 'Shekel', '970', '', '', 'ar-PS', 6254930, 'JO,IL', ''),
	('PT', 'PRT', 620, 'PO', 'Portugal', 1, 1, 'Lisbon', 92391, 8388607, 'EU', '.pt', 'EUR', 'Euro', '351', '####-###', '^(d{7})$', 'pt-PT,mwl', 2264397, 'ES', ''),
	('PW', 'PLW', 585, 'PS', 'Palau', 0, 1, 'Koror', 458, 20303, 'OC', '.pw', 'USD', 'Dollar', '680', '96940', '^(96940)$', 'pau,sov,en-PW,tox,ja,fil,zh', 1559582, '', ''),
	('PY', 'PRY', 600, 'PA', 'Paraguay', 0, 1, 'Asunción', 406750, 6831000, 'SA', '.py', 'PYG', 'Guarani', '595', '####', '^(d{4})$', 'es-PY,gn', 3437598, 'BO,BR,AR', ''),
	('QA', 'QAT', 634, 'QA', 'Qatar', 0, 1, 'Doha', 11437, 928000, 'AS', '.qa', 'QAR', 'Rial', '974', '', '', 'ar-QA,es', 289688, 'SA', ''),
	('RE', 'REU', 638, 'RE', 'Reunion', 0, 1, 'Saint-Denis', 2517, 776948, 'AF', '.re', 'EUR', 'Euro', '262', '#####', '^((97)|(98)(4|7|8)d{2})$', 'fr-RE', 935317, '', ''),
	('RO', 'ROU', 642, 'RO', 'Romania', 1, 1, 'Bucharest', 237500, 8388607, 'EU', '.ro', 'RON', 'Leu', '40', '######', '^(d{6})$', 'ro,hu,rom', 798549, 'MD,HU,UA,CS,BG,RS', ''),
	('RS', 'SRB', 688, 'RB', 'Serbia', 0, 1, 'Belgrade', 88361, 8388607, 'EU', '.rs', 'RSD', 'Dinar', '381', '######', '^(d{6})$', 'sr,hu,bs,rom', 6290252, 'AL,HU,MK,RO,HR,BA,BG,ME', ''),
	('RU', 'RUS', 643, 'RS', 'Russia', 0, 1, 'Moscow', 17100000, 8388607, 'EU', '.ru', 'RUB', 'Ruble', '7', '######', '^(d{6})$', 'ru-RU', 2017370, 'GE,CN,BY,UA,KZ,LV,PL,EE,LT,FI,MN,NO,AZ,KP', ''),
	('RW', 'RWA', 646, 'RW', 'Rwanda', 0, 1, 'Kigali', 26338, 8388607, 'AF', '.rw', 'RWF', 'Franc', '250', '', '', 'rw,en-RW,fr-RW,sw', 49518, 'TZ,CD,BI,UG', ''),
	('SA', 'SAU', 682, 'SA', 'Saudi Arabia', 0, 1, 'Riyadh', 1960582, 8388607, 'AS', '.sa', 'SAR', 'Rial', '966', '#####', '^(d{5})$', 'ar-SA', 102358, 'QA,OM,IQ,YE,JO,AE,KW', ''),
	('SB', 'SLB', 90, 'BP', 'Solomon Islands', 0, 1, 'Honiara', 28450, 581000, 'OC', '.sb', 'SBD', 'Dollar', '677', '', '', 'en-SB,tpi', 2103350, '', ''),
	('SC', 'SYC', 690, 'SE', 'Seychelles', 0, 1, 'Victoria', 455, 82000, 'AF', '.sc', 'SCR', 'Rupee', '248', '', '', 'en-SC,fr-SC', 241170, '', ''),
	('SD', 'SDN', 736, 'SU', 'Sudan', 0, 1, 'Khartoum', 2505810, 8388607, 'AF', '.sd', 'SDG', 'Dinar', '249', '#####', '^(d{5})$', 'ar-SD,en,fia', 366755, 'TD,ER,ET,LY,KE,CF,CD,UG,EG', ''),
	('SE', 'SWE', 752, 'SW', 'Sweden', 1, 1, 'Stockholm', 449964, 8388607, 'EU', '.se', 'SEK', 'Krona', '46', 'SE-### ##', '^(?:SE)*(d{5})$', 'sv-SE,se,sma,fi-SE', 2661886, 'NO,FI', ''),
	('SG', 'SGP', 702, 'SN', 'Singapore', 0, 1, 'Singapur', 692.7, 4608000, 'AS', '.sg', 'SGD', 'Dollar', '65', '######', '^(d{6})$', 'cmn,en-SG,ms-SG,ta-SG,zh-SG', 1880251, '', ''),
	('SH', 'SHN', 654, 'SH', 'Saint Helena', 0, 1, 'Jamestown', 410, 7460, 'AF', '.sh', 'SHP', 'Pound', '290', 'STHL 1ZZ', '^(STHL1ZZ)$', 'en-SH', 3370751, '', ''),
	('SI', 'SVN', 705, 'SI', 'Slovenia', 1, 1, 'Ljubljana', 20273, 2007000, 'EU', '.si', 'EUR', 'Euro', '386', 'SI- ####', '^(?:SI)*(d{4})$', 'sl,sh', 3190538, 'HU,IT,HR,AT', ''),
	('SJ', 'SJM', 744, 'SV', 'Svalbard and Jan Mayen', 0, 1, 'Longyearbyen', 62049, 2265, 'EU', '.sj', 'NOK', 'Krone', '47', '', '', 'no,ru', 607072, '', ''),
	('SK', 'SVK', 703, 'LO', 'Slovakia', 1, 1, 'Bratislava', 48845, 5455000, 'EU', '.sk', 'EUR', 'Euro', '421', '###  ##', '^(d{5})$', 'sk,hu', 3057568, 'PL,HU,CZ,UA,AT', ''),
	('SL', 'SLE', 694, 'SL', 'Sierra Leone', 0, 1, 'Freetown', 71740, 6286000, 'AF', '.sl', 'SLL', 'Leone', '232', '', '', 'en-SL,men,tem', 2403846, 'LR,GN', ''),
	('SM', 'SMR', 674, 'SM', 'San Marino', 0, 1, 'San Marino', 61.2, 29000, 'EU', '.sm', 'EUR', 'Euro', '378', '4789#', '^(4789d)$', 'it-SM', 3168068, 'IT', ''),
	('SN', 'SEN', 686, 'SG', 'Senegal', 0, 1, 'Dakar', 196190, 8388607, 'AF', '.sn', 'XOF', 'Franc', '221', '#####', '^(d{5})$', 'fr-SN,wo,fuc,mnk', 2245662, 'GN,MR,GW,GM,ML', ''),
	('SO', 'SOM', 706, 'SO', 'Somalia', 0, 1, 'Mogadishu', 637657, 8388607, 'AF', '.so', 'SOS', 'Shilling', '252', '@@  #####', '^([A-Z]{2}d{5})$', 'so-SO,ar-SO,it,en-SO', 51537, 'ET,KE,DJ', ''),
	('SR', 'SUR', 740, 'NS', 'Suriname', 0, 1, 'Paramaribo', 163270, 475000, 'SA', '.sr', 'SRD', 'Dollar', '597', '', '', 'nl-SR,en,srn,hns,jv', 3382998, 'GY,BR,GF', ''),
	('ST', 'STP', 678, 'TP', 'Sao Tome and Principe', 0, 1, 'São Tomé', 1001, 205000, 'AF', '.st', 'STD', 'Dobra', '239', '', '', 'pt-ST', 2410758, '', ''),
	('SV', 'SLV', 222, 'ES', 'El Salvador', 0, 1, 'San Salvador', 21040, 7066000, 'NA', '.sv', 'USD', 'Dollar', '503', 'CP ####', '^(?:CP)*(d{4})$', 'es-SV', 3585968, 'GT,HN', ''),
	('SY', 'SYR', 760, 'SY', 'Syria', 0, 1, 'Damascus', 185180, 8388607, 'AS', '.sy', 'SYP', 'Pound', '963', '', '', 'ar-SY,ku,hy,arc,fr,en', 163843, 'IQ,JO,IL,TR,LB', ''),
	('SZ', 'SWZ', 748, 'WZ', 'Swaziland', 0, 1, 'Mbabane', 17363, 1128000, 'AF', '.sz', 'SZL', 'Lilangeni', '268', '@###', '^([A-Z]d{3})$', 'en-SZ,ss-SZ', 934841, 'ZA,MZ', ''),
	('TC', 'TCA', 796, 'TK', 'Turks and Caicos Islands', 0, 1, 'Cockburn Town', 430, 20556, 'NA', '.tc', 'USD', 'Dollar', '+1-649', 'TKCA 1ZZ', '^(TKCA 1ZZ)$', 'en-TC', 3576916, '', ''),
	('TD', 'TCD', 148, 'CD', 'Chad', 0, 1, 'N\'Djamena', 1284000, 8388607, 'AF', '.td', 'XAF', 'Franc', '235', '', '', 'fr-TD,ar-TD,sre', 2434508, 'NE,LY,CF,SD,CM,NG', ''),
	('TF', 'ATF', 260, 'FS', 'French Southern Territories', 0, 1, 'Martin-de-Viviès', 7829, 0, 'AN', '.tf', 'EUR', 'Euro', '', '', '', 'fr', 1546748, '', ''),
	('TG', 'TGO', 768, 'TO', 'Togo', 0, 1, 'Lomé', 56785, 5858000, 'AF', '.tg', 'XOF', 'Franc', '228', '', '', 'fr-TG,ee,hna,kbp,dag,ha', 2363686, 'BJ,GH,BF', ''),
	('TH', 'THA', 764, 'TH', 'Thailand', 0, 1, 'Bangkok', 514000, 8388607, 'AS', '.th', 'THB', 'Baht', '66', '#####', '^(d{5})$', 'th,en', 1605651, 'LA,MM,KH,MY', ''),
	('TJ', 'TJK', 762, 'TI', 'Tajikistan', 0, 1, 'Dushanbe', 143100, 7211000, 'AS', '.tj', 'TJS', 'Somoni', '992', '######', '^(d{6})$', 'tg,ru', 1220409, 'CN,AF,KG,UZ', ''),
	('TK', 'TKL', 772, 'TL', 'Tokelau', 0, 1, '', 10, 1405, 'OC', '.tk', 'NZD', 'Dollar', '690', '', '', 'tkl,en-TK', 4031074, '', ''),
	('TL', 'TLS', 626, 'TT', 'East Timor', 0, 1, 'Dili', 15007, 1107000, 'OC', '.tp', 'USD', 'Dollar', '670', '', '', 'tet,pt-TL,id,en', 1966436, 'ID', ''),
	('TM', 'TKM', 795, 'TX', 'Turkmenistan', 0, 1, 'Ashgabat', 488100, 5179000, 'AS', '.tm', 'TMT', 'Manat', '993', '######', '^(d{6})$', 'tk,ru,uz', 1218197, 'AF,IR,UZ,KZ', ''),
	('TN', 'TUN', 788, 'TS', 'Tunisia', 0, 1, 'Tunis', 163610, 8388607, 'AF', '.tn', 'TND', 'Dinar', '216', '####', '^(d{4})$', 'ar-TN,fr', 2464461, 'DZ,LY', ''),
	('TO', 'TON', 776, 'TN', 'Tonga', 0, 1, 'Nuku\'alofa', 748, 118000, 'OC', '.to', 'TOP', 'Pa\'anga', '676', '', '', 'to,en-TO', 4032283, '', ''),
	('TR', 'TUR', 792, 'TU', 'Turkey', 0, 1, 'Ankara', 780580, 8388607, 'AS', '.tr', 'TRY', 'Lira', '90', '#####', '^(d{5})$', 'tr-TR,ku,diq,az,av', 298795, 'SY,GE,IQ,IR,GR,AM,AZ,BG', ''),
	('TT', 'TTO', 780, 'TD', 'Trinidad and Tobago', 0, 1, 'Port of Spain', 5128, 1047000, 'NA', '.tt', 'TTD', 'Dollar', '+1-868', '', '', 'en-TT,hns,fr,es,zh', 3573591, '', ''),
	('TV', 'TUV', 798, 'TV', 'Tuvalu', 0, 1, 'Vaiaku', 26, 12000, 'OC', '.tv', 'AUD', 'Dollar', '688', '', '', 'tvl,en,sm,gil', 2110297, '', ''),
	('TW', 'TWN', 158, 'TW', 'Taiwan', 0, 1, 'Taipei', 35980, 8388607, 'AS', '.tw', 'TWD', 'Dollar', '886', '#####', '^(d{5})$', 'zh-TW,zh,nan,hak', 1668284, '', ''),
	('TZ', 'TZA', 834, 'TZ', 'Tanzania', 0, 1, 'Dar es Salaam', 945087, 8388607, 'AF', '.tz', 'TZS', 'Shilling', '255', '', '', 'sw-TZ,en,ar', 149590, 'MZ,KE,CD,RW,ZM,BI,UG,MW', ''),
	('UA', 'UKR', 804, 'UP', 'Ukraine', 0, 1, 'Kiev', 603700, 8388607, 'EU', '.ua', 'UAH', 'Hryvnia', '380', '#####', '^(d{5})$', 'uk,ru-UA,rom,pl,hu', 690791, 'PL,MD,HU,SK,BY,RO,RU', ''),
	('UG', 'UGA', 800, 'UG', 'Uganda', 0, 1, 'Kampala', 236040, 8388607, 'AF', '.ug', 'UGX', 'Shilling', '256', '', '', 'en-UG,lg,sw,ar', 226074, 'TZ,KE,SD,CD,RW', ''),
	('UM', 'UMI', 581, '', 'United States Minor Outlying Islands', 0, 1, '', 0, 0, 'OC', '.um', 'USD', 'Dollar', '', '', '', 'en-UM', 5854968, '', ''),
	('US', 'USA', 840, 'US', 'United States', 0, 1, 'Washington', 9629091, 8388607, 'NA', '.us', 'USD', 'Dollar', '1', '#####-####', '^(d{9})$', 'en-US,es-US,haw', 6252001, 'CA,MX,CU', ''),
	('UY', 'URY', 858, 'UY', 'Uruguay', 0, 1, 'Montevideo', 176220, 3477000, 'SA', '.uy', 'UYU', 'Peso', '598', '#####', '^(d{5})$', 'es-UY', 3439705, 'BR,AR', ''),
	('UZ', 'UZB', 860, 'UZ', 'Uzbekistan', 0, 1, 'Tashkent', 447400, 8388607, 'AS', '.uz', 'UZS', 'Som', '998', '######', '^(d{6})$', 'uz,ru,tg', 1512440, 'TM,AF,KG,TJ,KZ', ''),
	('VA', 'VAT', 336, 'VT', 'Vatican', 0, 1, 'Vatican City', 0.44, 921, 'EU', '.va', 'EUR', 'Euro', '379', '', '', 'la,it,fr', 3164670, 'IT', ''),
	('VC', 'VCT', 670, 'VC', 'Saint Vincent and the Grenadines', 0, 1, 'Kingstown', 389, 117534, 'NA', '.vc', 'XCD', 'Dollar', '+1-784', '', '', 'en-VC,fr', 3577815, '', ''),
	('VE', 'VEN', 862, 'VE', 'Venezuela', 0, 1, 'Caracas', 912050, 8388607, 'SA', '.ve', 'VEF', 'Bolivar', '58', '####', '^(d{4})$', 'es-VE', 3625428, 'GY,BR,CO', ''),
	('VG', 'VGB', 92, 'VI', 'British Virgin Islands', 0, 1, 'Road Town', 153, 21730, 'NA', '.vg', 'USD', 'Dollar', '+1-284', '', '', 'en-VG', 3577718, '', ''),
	('VI', 'VIR', 850, 'VQ', 'U.S. Virgin Islands', 0, 1, 'Charlotte Amalie', 352, 108708, 'NA', '.vi', 'USD', 'Dollar', '+1-340', '', '', 'en-VI', 4796775, '', ''),
	('VN', 'VNM', 704, 'VM', 'Vietnam', 0, 1, 'Hanoi', 329560, 8388607, 'AS', '.vn', 'VND', 'Dong', '84', '######', '^(d{6})$', 'vi,en,fr,zh,km', 1562822, 'CN,LA,KH', ''),
	('VU', 'VUT', 548, 'NH', 'Vanuatu', 0, 1, 'Port Vila', 12200, 215000, 'OC', '.vu', 'VUV', 'Vatu', '678', '', '', 'bi,en-VU,fr-VU', 2134431, '', ''),
	('WF', 'WLF', 876, 'WF', 'Wallis and Futuna', 0, 1, 'Matâ\'Utu', 274, 16025, 'OC', '.wf', 'XPF', 'Franc', '681', '', '', 'wls,fud,fr-WF', 4034749, '', ''),
	('WS', 'WSM', 882, 'WS', 'Samoa', 0, 1, 'Apia', 2944, 217000, 'OC', '.ws', 'WST', 'Tala', '685', '', '', 'sm,en-WS', 4034894, '', ''),
	('XK', 'XKX', 0, 'KV', 'Kosovo', 0, 1, 'Priština', 0, 1800000, 'EU', '', 'EUR', 'Euro', '', '', '', 'sq,sr', 831053, 'RS,AL,MK,ME', ''),
	('YE', 'YEM', 887, 'YM', 'Yemen', 0, 1, 'San‘a’', 527970, 8388607, 'AS', '.ye', 'YER', 'Rial', '967', '', '', 'ar-YE', 69543, 'SA,OM', ''),
	('YT', 'MYT', 175, 'MF', 'Mayotte', 0, 1, 'Mamoudzou', 374, 159042, 'AF', '.yt', 'EUR', 'Euro', '269', '#####', '^(d{5})$', 'fr-YT', 1024031, '', ''),
	('ZA', 'ZAF', 710, 'SF', 'South Africa', 0, 1, 'Pretoria', 1219912, 8388607, 'AF', '.za', 'ZAR', 'Rand', '27', '####', '^(d{4})$', 'zu,xh,af,nso,en-ZA,tn,st,ts', 953987, 'ZW,SZ,MZ,BW,NA,LS', ''),
	('ZM', 'ZMB', 894, 'ZA', 'Zambia', 0, 1, 'Lusaka', 752614, 8388607, 'AF', '.zm', 'ZMK', 'Kwacha', '260', '#####', '^(d{5})$', 'en-ZM,bem,loz,lun,lue,ny,toi', 895949, 'ZW,TZ,MZ,CD,NA,MW,AO', ''),
	('ZW', 'ZWE', 716, 'ZI', 'Zimbabwe', 0, 1, 'Harare', 390580, 8388607, 'AF', '.zw', 'ZWL', 'Dollar', '263', '', '', 'en-ZW,sn,nr,nd', 878675, 'ZA,MZ,BW,ZM', '');
/*!40000 ALTER TABLE `all__country` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `all__language` (
  `language_id` int(11) NOT NULL AUTO_INCREMENT,
  `language` varchar(32) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `language_description` varchar(32) COLLATE utf8_spanish2_ci NOT NULL,
  `code` char(3) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `code2` char(2) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `admin` int(1) NOT NULL DEFAULT '0',
  `public` int(1) NOT NULL DEFAULT '0',
  `url` varchar(100) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `ordre` int(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `all__language` DISABLE KEYS */;
INSERT INTO `all__language` (`language_id`, `language`, `language_description`, `code`, `code2`, `admin`, `public`, `url`, `ordre`) VALUES
	(1, 'Català', '', 'cat', 'ca', 1, 1, '', 1),
	(2, 'Español', '', 'spa', 'es', 0, 1, '', 2),
	(3, 'English', '', 'eng', 'en', 0, 1, '', 4),
	(4, 'Français', '', 'fra', 'fr', 0, 1, '', 5),
	(5, 'Deutsch', '', 'deu', 'de', 0, 0, '', 6),
	(6, 'Español', 'colombia', 'col', 'es', 0, 0, '', 3);
/*!40000 ALTER TABLE `all__language` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `all__menu` (
  `menu_id` int(11) NOT NULL DEFAULT '0',
  `menu_group` varchar(32) COLLATE utf8_spanish2_ci NOT NULL,
  `tool` varchar(32) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `tool_section` varchar(32) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `action` varchar(32) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `toolmode_id` int(1) NOT NULL DEFAULT '0',
  `variable` varchar(100) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `ordre` int(2) NOT NULL DEFAULT '0',
  `link` varchar(32) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `process` varchar(100) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `bin` int(1) NOT NULL DEFAULT '0',
  `read1` varchar(251) COLLATE utf8_spanish2_ci NOT NULL DEFAULT ',1,2,3,',
  `write1` varchar(251) COLLATE utf8_spanish2_ci NOT NULL DEFAULT ',1,2,',
  PRIMARY KEY (`menu_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `all__menu` DISABLE KEYS */;
INSERT INTO `all__menu` (`menu_id`, `menu_group`, `tool`, `tool_section`, `action`, `parent_id`, `toolmode_id`, `variable`, `ordre`, `link`, `process`, `bin`, `read1`, `write1`) VALUES
	(24, 'newsletter', 'newsletter', 'message', 'list_records', 2, 1, 'NEWSLETTER_MENU_MESSAGE_NOT_SENT', 3, '', 'send_mail_not_sent', 0, ',1,2,', ',1,2,'),
	(20, 'newsletter', 'newsletter', 'configadmin', 'list_records', 25, 1, 'NEWSLETTER_MENU_CONFIGADMIN', 0, '', '', 0, ',1,2,', ',1,2,'),
	(16, 'newsletter', 'newsletter', 'message', 'list_records_bin', 2, 1, 'NEWSLETTER_MENU_MESSAGE_BIN', 4, '', '', 0, ',1,2,', ',1,2,'),
	(14, 'newsletter', 'newsletter', 'custumer', 'list_records_bin', 3, 1, 'NEWSLETTER_MENU_CUSTUMER_BIN', 3, '', '', 0, ',1,2,', ',1,2,'),
	(13, 'newsletter', 'newsletter', 'group', 'list_records', 4, 1, 'NEWSLETTER_MENU_GROUP_LIST', 1, '', '', 0, ',1,2,', ',1,2,'),
	(12, 'newsletter', 'newsletter', 'group', 'show_form_new', 4, 1, 'NEWSLETTER_MENU_GROUP_NEW', 0, '', '', 0, ',1,2,', ',1,2,'),
	(11, 'newsletter', 'newsletter', 'custumer', 'list_records', 3, 1, 'NEWSLETTER_MENU_CUSTUMER_LIST', 1, '', '', 0, ',1,2,', ',1,2,'),
	(10, 'newsletter', 'newsletter', 'custumer', 'show_form_new', 3, 1, 'NEWSLETTER_MENU_CUSTUMER_NEW', 0, '', '', 0, ',1,2,', ',1,2,'),
	(9, 'newsletter', 'newsletter', 'message', 'list_records', 2, 1, 'NEWSLETTER_MENU_MESSAGE_SEND_FROM_HISTORY', 2, '', 'send_mail_history', 0, ',1,2,', ',1,2,'),
	(7, 'newsletter', 'newsletter', 'message', 'list_records_saved', 2, 1, 'NEWSLETTER_MENU_MESSAGE_SEND_SAVED', 1, '', 'send_mail_saved', 0, ',1,2,', ',1,2,'),
	(5, 'newsletter', 'newsletter', 'message', 'show_form_new', 2, 1, 'NEWSLETTER_MENU_MESSAGE_SEND', 0, '', 'send_mail', 0, ',1,2,', ',1,2,'),
	(4, 'newsletter', 'newsletter', 'group', '', 1, 1, 'NEWSLETTER_MENU_GROUP', 2, '', '', 0, ',1,2,', ',1,2,'),
	(3, 'newsletter', 'newsletter', 'custumer', '', 1, 1, 'NEWSLETTER_MENU_CUSTUMER', 1, '', '', 0, ',1,2,', ',1,2,'),
	(1, 'newsletter', 'newsletter', '', '', 0, 1, 'NEWSLETTER_MENU_ROOT', 40, '5', '', 0, ',1,2,', ',1,2,'),
	(2, 'newsletter', 'newsletter', 'message', '', 1, 1, 'NEWSLETTER_MENU_MESSAGE', 0, '', '', 0, ',1,2,', ',1,2,'),
	(100, 'inmo', 'inmo', '', '', 0, 1, 'INMO_MENU_ROOT', 1, '103', '', 0, ',1,2,', ',1,2,'),
	(101, 'inmo', 'inmo', 'property', '', 100, 1, 'INMO_MENU_PROPERTY', 0, '', '', 0, ',1,2,', ',1,2,'),
	(102, 'inmo', 'inmo', 'property', 'show_form_new', 101, 1, 'INMO_MENU_PROPERTY_NEW', 0, '', '', 0, ',1,2,', ',1,2,'),
	(103, 'inmo', 'inmo', 'property', 'list_records', 101, 1, 'INMO_MENU_PROPERTY_LIST', 1, '', '', 0, ',1,2,', ',1,2,'),
	(104, 'inmo', 'inmo', 'property', 'show_search_form', 101, 1, 'INMO_MENU_SEARCH_FORM', 2, '', '', 0, ',1,2,', ',1,2,'),
	(105, 'inmo', 'inmo', 'property', 'list_records_bin', 101, 1, 'INMO_MENU_PROPERTY_BIN', 3, '', '', 0, ',1,2,', ',1,2,'),
	(106, 'inmo', 'inmo', 'category', '', 100, 1, 'INMO_MENU_CATEGORY', 4, '', '', 0, ',1,2,', ',1,2,'),
	(90000, 'contact', 'contact', '', '', 0, 1, 'CONTACT_MENU_ROOT', 80, '90002', '', 0, ',1,2,', ',1,2,'),
	(108, 'inmo', 'inmo', 'category', 'list_records', 106, 1, 'INMO_MENU_CATEGORY_LIST', 1, '', '', 0, ',1,2,', ',1,2,'),
	(110, 'inmo', 'inmo', 'property', 'show_form_new', 102, 1, 'INMO_MENU_PROPERTY_NEW', 0, '', '', 0, ',1,2,', ',1,2,'),
	(111, 'inmo', 'inmo', 'property', 'list_records_images', 102, 1, 'INMO_MENU_PROPERTY_NEW', 1, '', '', 0, ',1,2,', ',1,2,'),
	(112, 'inmo', 'inmo', 'property', 'show_form_new', 103, 1, 'INMO_MENU_PROPERTY_NEW', 0, '', '', 0, ',1,2,', ',1,2,'),
	(113, 'inmo', 'inmo', 'property', 'list_records_images', 103, 1, 'INMO_MENU_PROPERTY_NEW', 1, '', '', 0, ',1,2,', ',1,2,'),
	(114, 'inmo', 'inmo', 'translation', '', 100, 1, 'INMO_MENU_TRANSLATION', 6, '', '', 0, ',1,2,', ',1,2,'),
	(115, 'inmo', 'inmo', 'translation', 'list_records_property', 114, 1, 'INMO_MENU_PROPERTY_TRANSLATION_LIST', 0, '', '', 0, ',1,2,', ',1,2,'),
	(116, 'inmo', 'inmo', 'translation', 'list_records_category', 114, 1, 'INMO_MENU_CATEGORY_TRANSLATION_LIST', 0, '', '', 0, ',1,2,', ',1,2,'),
	(200, 'print', 'print', '', '', 0, 1, 'PRINT_MENU_ROOT', 30, '207', '', 0, ',1,2,', ',1,2,'),
	(1000, 'admintotal', 'admintotal', '', '', 0, 1, 'ADMINTOTAL_MENU_ROOT', 1000, '1006', '', 0, ',1,', ',1,'),
	(1001, 'admintotal', 'admintotal', 'structure', '', 1000, 1, 'ADMINTOTAL_MENU_STRUCTURE', 1, '', '', 0, ',1,', ',1,'),
	(1002, 'admintotal', 'admintotal', 'bbdd', 'list_records', 1001, 1, 'ADMINTOTAL_MENU_STRUCTURE_BBDD_LIST', 7, '', '', 0, ',1,', ',1,'),
	(1003, 'admintotal', 'admintotal', 'dirs', 'list_records', 1001, 1, 'ADMINTOTAL_MENU_STRUCTURE_DIRS_LIST', 8, '', '', 0, ',1,', ',1,'),
	(1004, 'admintotal', 'admintotal', 'tools', '', 1000, 1, 'ADMINTOTAL_MENU_TOOLS', 3, '', '', 1, ',1,', ',1,'),
	(1005, 'admintotal', 'admintotal', 'menu', 'list_records', 1001, 1, 'ADMINTOTAL_MENU_TOOLS_MENUS_LIST', 4, '', '', 0, ',1,', ',1,'),
	(1006, 'admintotal', 'admintotal', 'modules', 'list_records', 1001, 1, 'ADMINTOTAL_MENU_TOOLS_FIELDS_CONFIG', 5, '', '', 0, ',1,', ',1,'),
	(1010, 'admintotal', 'admintotal', 'customer', 'show_form_new', 1009, 1, 'ADMINTOTAL_MENU_CUSTOMER_ADD', 1, '', '', 0, ',1,', ',1,'),
	(1011, 'admintotal', 'admintotal', 'customer', 'list_records', 1009, 1, 'ADMINTOTAL_MENU_CUSTOMER_LIST', 2, '', '', 0, ',1,', ',1,'),
	(2000, 'translation', 'translation', '', '', 0, 1, 'TRANSLATION_MENU_ROOT', 120, '2002', '', 0, ',1,', ',1,'),
	(2001, 'translation', 'translation', 'translation', '', 2000, 1, 'TRANSLATION_MENU_TRANSLATION', 0, '', '', 0, ',1,', ',1,'),
	(2002, 'translation', 'translation', 'translation', 'list_records', 2001, 1, 'TRANSLATION_MENU_LIST', 0, '', '', 0, ',1,', ',1,'),
	(2004, 'translation', 'translation', 'translation', 'list_records_state', 2001, 1, 'TRANSLATION_MENU_LIST_STATE', 0, '', '', 0, ',1,', ',1,'),
	(4000, 'web', 'web', '', '', 0, 1, 'WEB_MENU_ROOT', 100, '4002', '', 0, ',1,2,', ',1,2,'),
	(4001, 'web', 'web', 'config', '', 4000, 1, 'WEB_MENU_CONFIG', 1, '', '', 0, ',1,2,', ',1,2,'),
	(4002, 'web', 'web', 'config', 'list_records', 4001, 1, 'WEB_MENU_CONFIGADMIN_LIST', 1, '', 'admin', 0, ',1,2,', ',1,2,'),
	(4004, 'web', 'web', 'config', 'list_records', 4001, 1, 'WEB_MENU_CONFIGPUBLIC_LIST', 2, '', 'public', 0, ',1,2,', ',1,2,'),
	(4100, 'web', 'web', 'config', 'list_records', 4001, 1, 'WEB_MENU_CONFIGINMOPUBLIC_LIST', 4, '', 'inmo_public', 0, ',1,2,', ',1,2,'),
	(4104, 'web', 'web', 'config', 'list_records_configinmoadmin', 4001, 1, 'WEB_MENU_CONFIGINMOADMIN_LIST', 3, '', 'inmo_admin', 0, ',1,2,', ',1,2,'),
	(4101, 'web', 'web', 'config', 'list_records', 4001, 1, 'WEB_MENU_CONFIGCUSTUMER_LIST', 5, '', 'custumer', 0, ',1,2,', ',1,2,'),
	(4102, 'web', 'web', 'config', 'list_records', 4001, 1, 'WEB_MENU_CONFIGNEWSLETTER_LIST', 6, '', 'newsletter', 0, ',1,2,', ',1,2,'),
	(4103, 'web', 'web', 'config', 'list_records', 4001, 1, 'WEB_MENU_CONFIGUTILITY_LIST', 7, '', 'utility', 0, ',1,2,', ',1,2,'),
	(4006, 'web', 'web', 'save', '', 4000, 1, 'WEB_MENU_SAVE', 5, '', '', 0, ',1,2,', ',1,2,'),
	(4007, 'web', 'web', 'save', 'list_records', 4006, 1, 'WEB_MENU_SAVE_LIST', 1, '', '', 0, ',1,2,', ',1,2,'),
	(4013, 'web', 'web', 'banner', 'list_records_bin', 4010, 1, 'WEB_MENU_BANNER_LIST_BIN', 3, '', '', 0, ',1,2,', ',1,2,'),
	(4012, 'web', 'web', 'banner', 'list_records', 4010, 1, 'WEB_MENU_BANNER_LIST', 2, '', '', 0, ',1,2,', ',1,2,'),
	(4011, 'web', 'web', 'banner', 'show_form_new', 4010, 1, 'WEB_MENU_BANNER_NEW', 1, '', '', 0, ',1,2,', ',1,2,'),
	(4010, 'web', 'web', 'banner', '', 4000, 1, 'WEB_MENU_BANNER', 3, '', '', 0, ',1,2,', ',1,2,'),
	(4040, 'web', 'web', 'translation', '', 4000, 1, 'WEB_MENU_TRANSLATION', 4, '', '', 0, ',1,2,', ',1,2,'),
	(4041, 'web', 'web', 'translation', 'list_records', 4040, 1, 'WEB_MENU_TRANSLATION_LIST', 1, '', '', 0, ',1,2,', ',1,2,'),
	(4042, 'web', 'web', 'translation', 'list_records_page', 4040, 1, 'WEB_MENU_PAGE_TRANSLATION_LIST', 2, '', '', 0, ',1,2,', ',1,2,'),
	(5000, 'custumer', 'custumer', '', '', 0, 1, 'CUSTUMER_MENU_ROOT', 10, '5003', '', 0, ',1,2,', ',1,2,'),
	(5001, 'custumer', 'custumer', 'custumer', '', 5000, 1, 'CUSTUMER_MENU_CUSTUMER', 0, '', '', 0, ',1,2,', ',1,2,'),
	(5002, 'custumer', 'custumer', 'custumer', 'show_form_new', 5001, 1, 'CUSTUMER_MENU_NEW', 1, '', '', 0, ',1,2,', ',1,2,'),
	(5003, 'custumer', 'custumer', 'custumer', 'list_records', 5001, 1, 'CUSTUMER_MENU_LIST', 2, '', '', 0, ',1,2,', ',1,2,'),
	(5005, 'custumer', 'custumer', 'custumer', 'list_records_bin', 5001, 1, 'CUSTUMER_MENU_BIN', 6, '', '', 0, ',1,2,', ',1,2,'),
	(5006, 'custumer', 'custumer', 'custumer', 'show_form_new', 5002, 1, 'CUSTUMER_MENU_NEW', 4, '', '', 0, ',1,2,', ',1,2,'),
	(5007, 'custumer', 'custumer', 'custumer', 'show_form_edit', 5002, 1, 'CUSTUMER_MENU_NEW', 5, '', '', 0, ',1,2,', ',1,2,'),
	(5008, 'custumer', 'custumer', 'demand', '', 5000, 1, 'CUSTUMER_MENU_DEMAND', 5, '', '', 0, ',1,2,', ',1,2,'),
	(5009, 'custumer', 'custumer', 'demand', 'list_records', 5008, 1, 'CUSTUMER_MENU_DEMAND_LIST', 1, '', '', 0, ',1,2,', ',1,2,'),
	(5010, 'custumer', 'custumer', 'demand', 'list_records_exact', 5008, 1, 'CUSTUMER_MENU_DEMAND_CHECK', 2, '', '', 0, ',1,2,', ',1,2,'),
	(5011, 'custumer', 'custumer', 'demand', 'list_records_prox', 5008, 1, 'CUSTUMER_MENU_DEMAND_PROX', 3, '', '', 0, ',1,2,', ',1,2,'),
	(7000, 'user', 'user', '', '', 0, 1, 'USER_MENU_ROOT', 90, '7003', '', 0, ',1,2,', ',1,2,'),
	(7001, 'user', 'user', 'user', '', 7000, 1, 'USER_MENU_USER', 1, '', '', 0, ',1,2,', ',1,2,'),
	(7002, 'user', 'user', 'user', 'show_form_new', 7001, 1, 'USER_MENU_USER_NEW', 1, '', '', 0, ',1,2,', ',1,2,'),
	(7003, 'user', 'user', 'user', 'list_records', 7001, 1, 'USER_MENU_USER_LIST', 2, '', '', 0, ',1,2,', ',1,2,'),
	(7004, 'user', 'user', 'user', 'list_records_bin', 7001, 1, 'USER_MENU_USER_BIN', 3, '', '', 0, ',1,2,', ',1,2,'),
	(7005, 'user', 'user', 'concession', '', 7000, 1, 'USER_MENU_CONCESSION', 2, '', '', 0, ',1,2,', ',1,2,'),
	(7006, 'user', 'user', 'concession', 'list_records', 7005, 1, 'USER_MENU_CONCESSION_LIST', 1, '', '', 0, ',1,2,', ',1,2,'),
	(7007, 'user', 'user', 'group', '', 7000, 1, 'USER_MENU_GROUP', 3, '', '', 0, ',1,2,', ',1,2,'),
	(7008, 'user', 'user', 'group', 'show_form_new', 7007, 1, 'USER_MENU_GROUP_NEW', 1, '', '', 0, ',1,2,', ',1,2,'),
	(7009, 'user', 'user', 'group', 'list_records', 7007, 1, 'USER_MENU_GROUP_LIST', 2, '', '', 0, ',1,2,', ',1,2,'),
	(3000, 'news', 'news', 'new', '', 0, 1, 'NEWS_MENU_ROOT', 50, '3003', '', 0, ',1,2,4,', ',1,2,4,'),
	(8000, 'shopwindow', 'shopwindow', '', '', 0, 1, 'SHOPWINDOW_MENU_ROOT', 60, '8002', '', 0, ',,', ',,'),
	(10000, 'utility', 'utility', '', '', 0, 1, 'UTILITY_MENU_ROOT', 70, '10006', '', 0, ',1,2,', ',1,2,'),
	(10001, 'utility', 'utility', 'utilities', '', 10000, 1, 'UTILITY_MENU_UTILITIES', 0, '', '', 0, ',1,2,', ',1,2,'),
	(5028, 'custumer', 'custumer', 'custumer', 'list_records_bookers', 5024, 1, 'CUSTUMER_MENU_LIST_BOOKERS', 6, '', '', 0, ',1,2,', ',1,2,'),
	(120, 'inmo', 'inmo', 'zone', '', 100, 1, 'INMO_MENU_ZONE', 5, '', '', 0, ',1,2,', ',1,2,'),
	(10007, 'utility', 'utility', 'sell', 'show_form_new', 10001, 1, 'UTILITY_MENU_SELL', 1, '', '', 0, ',1,2,', ',1,2,'),
	(10006, 'utility', 'utility', 'buy', 'show_form_new', 10001, 1, 'UTILITY_MENU_BUY', 0, '', '', 0, ',1,2,', ',1,2,'),
	(121, 'inmo', 'inmo', 'zone', 'show_form_new', 120, 1, 'INMO_MENU_ZONE_NEW', 0, '', '', 0, ',1,2,', ',1,2,'),
	(122, 'inmo', 'inmo', 'zone', 'list_records', 120, 1, 'INMO_MENU_ZONE_LIST', 1, '', '', 0, ',1,2,', ',1,2,'),
	(1009, 'admintotal', 'admintotal', 'customer', '', 1000, 1, 'ADMINTOTAL_MENU_CUSTOMER', 4, '', '', 0, ',1,', ',1,'),
	(130, 'inmo', 'inmo', 'property', 'list_records_custumer', 102, 1, 'INMO_MENU_PROPERTY_NEW', 2, '', '', 0, ',1,2,', ',1,2,'),
	(131, 'inmo', 'inmo', 'property', 'list_records_custumer', 103, 1, 'INMO_MENU_PROPERTY_NEW', 2, '', '', 0, ',1,2,', ',1,2,'),
	(5012, 'custumer', 'custumer', 'custumer', 'show_form_new_property', 5002, 1, 'CUSTUMER_MENU_NEW', 6, '', '', 0, ',1,2,', ',1,2,'),
	(5013, 'custumer', 'custumer', 'custumer', 'list_records_images_property', 5002, 1, 'CUSTUMER_MENU_NEW', 7, '', '', 0, ',1,2,', ',1,2,'),
	(5020, 'custumer', 'custumer', 'custumer', 'list_records_owners', 5024, 1, 'CUSTUMER_MENU_LIST_OWNERS', 3, '', '', 0, ',1,2,', ',1,2,'),
	(5021, 'custumer', 'custumer', 'custumer', 'list_records_buyers', 5024, 1, 'CUSTUMER_MENU_LIST_BUYERS', 5, '', '', 0, ',1,2,', ',1,2,'),
	(7011, 'user', 'user', 'log', '', 7000, 1, 'USER_MENU_LOG', 4, '', '', 0, ',1,2,', ',1,2,'),
	(7012, 'user', 'user', 'log', 'list_records', 7011, 1, 'USER_MENU_LOG_LIST', 1, '', '', 0, ',1,2,', ',1,2,'),
	(9004, 'custumer', 'mls', 'companies', 'list_records_yes', 9001, 1, 'MLS_MENU_COMPANIES_YES', 3, '', '', 0, ',1,2,', ',1,2,'),
	(140, 'inmo', 'inmo', 'tv', '', 100, 1, 'INMO_MENU_TV', 7, '', '', 0, ',1,2,', ',1,2,'),
	(141, 'inmo', 'inmo', 'tv', 'list_records', 140, 1, 'INMO_MENU_TV_LIST', 1, '', '', 0, ',1,2,', ',1,2,'),
	(9002, 'custumer', 'mls', 'companies', 'list_records', 9001, 1, 'MLS_MENU_COMPANIES_LIST', 1, '', '', 0, ',1,2,', ',1,2,'),
	(9001, 'custumer', 'mls', 'companies', '', 5000, 1, 'MLS_MENU_COMPANIES', 6, '', '', 0, ',1,2,', ',1,2,'),
	(1082, 'admintotal', 'admintotal', 'generator', 'export', 1080, 1, 'ADMINTOTAL_MENU_GENERATOR_EXPORT', 2, '', '', 0, ',1,', ',1,'),
	(1080, 'admintotal', 'admintotal', 'generator', '', 1000, 1, 'ADMINTOTAL_MENU_GENERATOR', 2, '', '', 0, ',1,', ',1,'),
	(1030, 'admintotal', 'admintotal', 'category', '', 1000, 1, 'ADMINTOTAL_MENU_CATEGORY', 6, '', '', 0, ',1,', ',1,'),
	(1031, 'admintotal', 'admintotal', 'category', 'show_form_new', 1030, 1, 'ADMINTOTAL_MENU_CATEGORY_NEW', 0, '', '', 0, ',1,', ',1,'),
	(1032, 'admintotal', 'admintotal', 'category', 'list_records', 1030, 1, 'ADMINTOTAL_MENU_CATEGORY_LIST', 1, '', '', 0, ',1,', ',1,'),
	(90001, 'contact', 'contact', 'contact', '', 90000, 1, 'CONTACT_MENU_CONTACT', 0, '', '', 0, ',1,2,', ',1,2,'),
	(90002, 'contact', 'contact', 'contact', 'list_records', 90001, 1, 'CONTACT_MENU_LIST', 1, '', '', 0, ',1,2,', ',1,2,'),
	(1007, 'admintotal', 'admintotal', 'template', 'list_records', 1001, 1, 'ADMINTOTAL_MENU_TEMPLATE_LIST', 6, '', '', 0, ',1,', ',1,'),
	(25, 'newsletter', 'newsletter', 'configadmin', '', 1, 1, 'NEWSLETTER_MENU_CONFIGADMIN', 3, '', '', 0, ',1,2,', ',1,2,'),
	(202, 'print', 'print', 'showwindow', 'list_records', 201, 1, 'PRINT_MENU_SHOWWINDOW_LIST', 0, '', '', 0, ',1,2,', ',1,2,'),
	(205, 'print', 'print', 'contract', '', 200, 1, 'PRINT_MENU_CONTRACT', 0, '', '', 0, ',1,2,', ',1,2,'),
	(206, 'print', 'print', 'contract', 'show_form_new', 205, 1, 'PRINT_MENU_CONTRACT_NEW', 1, '', '', 0, ',1,2,', ',1,2,'),
	(207, 'print', 'print', 'contract', 'list_records', 205, 1, 'PRINT_MENU_CONTRACT_LIST', 2, '', '', 0, ',1,2,', ',1,2,'),
	(208, 'print', 'print', 'kind', 'show_form_new', 200, 1, 'PRINT_MENU_KIND', 1, '', '', 0, ',1,', ',1,'),
	(209, 'print', 'print', 'kind', 'show_form_new', 208, 1, 'PRINT_MENU_KIND_NEW', 1, '', '', 0, ',1,', ',1,'),
	(210, 'print', 'print', 'kind', 'list_records', 208, 1, 'PRINT_MENU_KIND_LIST', 2, '', '', 0, ',1,', ',1,'),
	(211, 'print', 'print', 'contract', 'list_records_bin', 205, 1, 'PRINT_MENU_CONTRACT_BIN', 3, '', '', 0, ',1,2,', ',1,2,'),
	(142, 'inmo', 'inmo', 'tv', 'list_records', 140, 1, 'INMO_MENU_TV_LIST_DOWNLOAD', 3, '', 'tv_download', 0, ',1,2,', ',1,2,'),
	(143, 'inmo', 'inmo', 'tv', 'list_records', 140, 1, 'INMO_MENU_TV_LIST_SELECTED', 2, '', 'tv_selected', 0, ',1,2,', ',1,2,'),
	(5022, 'custumer', 'custumer', 'custumer', 'list_records_tenants', 5024, 1, 'CUSTUMER_MENU_LIST_TENANTS', 4, '', '', 0, ',1,2,', ',1,2,'),
	(80000, 'missatge', 'missatge', '', '', 0, 1, 'MISSATGE_MENU_ROOT', 140, '80003', '', 0, ',1,', ',1,'),
	(80001, 'missatge', 'missatge', 'missatge', '', 80000, 1, 'MISSATGE_MENU_MISSATGE', 0, '', '', 0, ',1,', ',1,'),
	(80002, 'missatge', 'missatge', 'missatge', 'show_form_new', 80001, 1, 'MISSATGE_MENU_NEW', 1, '', '', 0, ',1,', ',1,'),
	(80003, 'missatge', 'missatge', 'missatge', 'list_records', 80001, 1, 'MISSATGE_MENU_LIST', 1, '', '', 0, ',1,', ',1,'),
	(80004, 'missatge', 'missatge', 'missatge', 'list_records_bin', 80001, 1, 'MISSATGE_MENU_BIN', 1, '', '', 0, ',1,', ',1,'),
	(4106, 'web', 'web', 'config', 'list_records', 4001, 1, 'WEB_MENU_CONFIGCONTACT_PUBLIC_LIST', 11, '', 'contact_public', 0, ',1,2,', ',1,2,'),
	(4107, 'web', 'web', 'config', 'list_records', 4001, 1, 'WEB_MENU_CONFIGCONTACT_ADMIN_LIST', 10, '', 'contact_admin', 0, ',1,2,', ',1,2,'),
	(5023, 'custumer', 'custumer', 'custumer', 'list_records_renters', 5024, 1, 'CUSTUMER_MENU_LIST_RENTERS', 5, '', '', 0, ',1,2,', ',1,2,'),
	(5024, 'custumer', 'custumer', 'custumer', '', 5000, 1, 'CUSTUMER_MENU_CUSTUMERLIST', 1, '', '', 0, ',1,2,', ',1,2,'),
	(5025, 'custumer', 'custumer', 'history', '', 5000, 1, 'CUSTUMER_MENU_HISTORY', 4, '', '', 0, ',1,2,', ',1,2,'),
	(5026, 'custumer', 'custumer', 'history', 'list_records', 5025, 1, 'CUSTUMER_MENU_HISTORY', 2, '', '', 0, ',1,2,', ',1,2,'),
	(5027, 'custumer', 'custumer', 'history', 'list_records_bin', 5025, 1, 'CUSTUMER_MENU_HISTORY_BIN', 3, '', '', 0, ',1,2,', ',1,2,'),
	(1033, 'admintotal', 'admintotal', 'util', 'list_records', 1001, 1, 'ADMINTOTAL_MENU_UTIL_LIST', 1, '', '', 0, ',1,', ',1,'),
	(1050, 'admintotal', 'admintotal', 'page', '', 1000, 1, 'ADMINTOTAL_MENU_PAGE', 1, '', '', 0, ',1,', ',1,'),
	(1051, 'admintotal', 'admintotal', 'page', 'show_form_new', 1050, 1, 'ADMINTOTAL_MENU_PAGE_NEW', 1, '', '', 0, ',1,', ',1,'),
	(1052, 'admintotal', 'admintotal', 'page', 'list_records', 1050, 1, 'ADMINTOTAL_MENU_PAGE_LIST', 2, '', '', 0, ',1,', ',1,'),
	(1053, 'admintotal', 'admintotal', 'page', 'list_records_bin', 1050, 1, 'ADMINTOTAL_MENU_PAGE_BIN', 3, '', '', 0, ',1,', ',1,'),
	(1060, 'admintotal', 'admintotal', 'bug', '', 1000, 1, 'ADMINTOTAL_MENU_BUG', 0, '', '', 1, ',1,', ',1,'),
	(1062, 'admintotal', 'admintotal', 'bug', 'list_records', 1060, 1, 'ADMINTOTAL_MENU_BUG_LIST', 2, '', '', 0, ',1,', ',1,'),
	(1063, 'admintotal', 'admintotal', 'bug', 'list_records_bin', 1060, 1, 'ADMINTOTAL_MENU_BUG_BIN', 3, '', '', 0, ',1,', ',1,'),
	(1070, 'admintotal', 'admintotal', 'block', '', 1000, 1, 'ADMINTOTAL_MENU_BLOCK', 3, '', '', 0, ',1,', ',1,'),
	(1071, 'admintotal', 'admintotal', 'block', 'show_form_new', 1070, 1, 'ADMINTOTAL_MENU_BLOCK_NEW', 1, '', '', 0, ',1,', ',1,'),
	(1072, 'admintotal', 'admintotal', 'block', 'list_records', 1070, 1, 'ADMINTOTAL_MENU_BLOCK_LIST', 2, '', '', 0, ',1,', ',1,'),
	(1073, 'admintotal', 'admintotal', 'block', 'list_records_bin', 1070, 1, 'ADMINTOTAL_MENU_BLOCK_BIN', 3, '', '', 0, ',1,', ',1,'),
	(3001, 'news', 'news', 'new', '', 3000, 1, 'NEWS_MENU_NEW', 1, '', '', 0, ',1,2,4,', ',1,2,4,'),
	(3002, 'news', 'news', 'new', 'show_form_new', 3001, 1, 'NEWS_MENU_NEW_NEW', 1, '', '', 0, ',1,2,4,', ',1,2,4,'),
	(3003, 'news', 'news', 'new', 'list_records', 3001, 1, 'NEWS_MENU_NEW_LIST', 2, '', '', 0, ',1,2,4,', ',1,2,4,'),
	(3004, 'news', 'news', 'new', 'list_records_bin', 3001, 1, 'NEWS_MENU_NEW_BIN', 3, '', '', 0, ',1,2,4,', ',1,2,4,'),
	(3005, 'news', 'news', 'category', '', 3000, 1, 'NEWS_MENU_CATEGORY', 2, '', '', 0, ',1,4,', ',1,4,'),
	(3007, 'news', 'news', 'category', 'list_records', 3005, 1, 'NEWS_MENU_CATEGORY_LIST', 2, '', '', 0, ',1,4,', ',1,4,'),
	(3006, 'news', 'news', 'category', 'show_form_new', 3005, 1, 'NEWS_MENU_CATEGORY_NEW', 1, '', '', 0, ',1,4,', ',1,4,'),
	(3008, 'news', 'news', 'new', 'show_form_new', 3002, 1, 'NEWS_MENU_NEW_NEW', 1, '', '', 0, ',1,2,4,', ',1,2,4,'),
	(3009, 'news', 'news', 'new', 'list_records_images', 3002, 1, 'NEWS_MENU_NEW_NEW', 2, '', '', 0, ',1,2,4,', ',1,2,4,'),
	(3010, 'news', 'news', 'new', 'show_form_new', 3003, 0, 'NEWS_MENU_NEW_NEW', 1, '', '', 0, ',1,2,4,', ',1,2,4,'),
	(3011, 'news', 'news', 'new', 'list_records_images', 3003, 1, 'NEWS_MENU_NEW_NEW', 2, '', '', 0, ',1,2,4,', ',1,2,4,'),
	(4031, 'web', 'web', 'page', '', 4000, 1, 'WEB_MENU_PAGE', 2, '', '', 0, ',1,2,', ',1,2,'),
	(4033, 'web', 'web', 'page', 'list_records', 4031, 1, 'WEB_MENU_PAGE_LIST', 1, '', '', 0, ',1,2,', ',1,2,'),
	(17, 'newsletter', 'newsletter', 'custumer', 'list_records', 3, 1, 'NEWSLETTER_MENU_CUSTUMER_REMOVED', 2, '', 'removed', 0, ',1,2,', ',1,2,'),
	(5031, 'custumer', 'custumer', 'group', '', 5000, 1, 'CUSTUMER_MENU_GROUP', 2, '', '', 0, ',1,2,', ',1,2,'),
	(5032, 'custumer', 'custumer', 'group', 'show_form_new', 5031, 1, 'CUSTUMER_MENU_GROUP_NEW', 0, '', '', 0, ',1,2,', ',1,2,'),
	(5033, 'custumer', 'custumer', 'group', 'list_records', 5031, 1, 'CUSTUMER_MENU_GROUP_LIST', 1, '', '', 0, ',1,2,', ',1,2,'),
	(20113, 'customer', 'product', 'order', 'list_records_bin', 20110, 1, 'CUSTOMER_MENU_ORDER_LIST_BIN', 3, '', '', 0, ',1,2,', ',1,2,'),
	(20112, 'customer', 'product', 'order', 'list_records', 20110, 1, 'CUSTOMER_MENU_ORDER_LIST', 2, '', '', 0, ',1,2,', ',1,2,'),
	(20110, 'customer', 'product', 'order', '', 20100, 1, 'CUSTOMER_MENU_ORDER', 2, '', '', 0, ',1,2,', ',1,2,'),
	(20105, 'customer', 'product', 'customer', 'list_records_bin', 20101, 1, 'CUSTOMER_MENU_CUSTOMER_BIN', 3, '', '', 0, ',1,2,', ',1,2,'),
	(20104, 'customer', 'product', 'customer', 'list_records', 20101, 1, 'CUSTOMER_MENU_CUSTOMER_LIST', 2, '', '', 0, ',1,2,', ',1,2,'),
	(20100, 'customer', 'product', '', '', 0, 1, 'CUSTOMER_MENU_ROOT', 2, '20112', '', 0, ',1,2,', ',1,2,'),
	(20101, 'customer', 'product', 'customer', '', 20100, 1, 'CUSTOMER_MENU_CUSTOMER', 0, '', '', 0, ',1,2,', ',1,2,'),
	(20102, 'customer', 'product', 'customer', 'show_form_new', 20101, 1, 'CUSTOMER_MENU_CUSTOMER_NEW', 1, '', '', 0, ',1,2,', ',1,2,'),
	(20121, 'customer', 'product', 'customer', '', 20100, 1, 'CUSTOMER_MENU_WHOLESALER', 5, '', 'wholesaler', 0, ',1,2,', ',1,2,'),
	(20122, 'customer', 'product', 'customer', 'show_form_new', 20121, 1, 'CUSTOMER_MENU_WHOLESALER_NEW', 1, '', 'wholesaler', 0, ',1,2,', ',1,2,'),
	(20124, 'customer', 'product', 'customer', 'list_records', 20121, 1, 'CUSTOMER_MENU_WHOLESALER_LIST', 2, '', 'wholesaler', 0, ',1,2,', ',1,2,'),
	(20125, 'customer', 'product', 'customer', 'list_records_bin', 20121, 1, 'CUSTOMER_MENU_WHOLESALER_BIN', 3, '', 'wholesaler', 0, ',1,2,', ',1,2,'),
	(20000, 'product', 'product', '', '', 0, 1, 'PRODUCT_MENU_ROOT', 1, '20004', '', 0, ',1,2,', ',1,2,'),
	(20001, 'product', 'product', 'product', '', 20000, 1, 'PRODUCT_MENU_PRODUCT', 1, '', '', 0, ',1,2,', ',1,2,'),
	(20002, 'product', 'product', 'product', 'show_form_new', 20001, 1, 'PRODUCT_MENU_NEW', 1, '', '', 0, ',1,2,', ',1,2,'),
	(20004, 'product', 'product', 'product', 'list_records', 20001, 1, 'PRODUCT_MENU_LIST', 2, '', '', 0, ',1,2,', ',1,2,'),
	(20005, 'product', 'product', 'product', 'list_records_bin', 20001, 1, 'PRODUCT_MENU_LIST_BIN', 3, '', '', 0, ',1,2,', ',1,2,'),
	(20006, 'product', 'product', 'product', 'list_records_images', 20002, 1, 'PRODUCT_MENU_NEW', 1, '', '', 0, ',1,2,', ',1,2,'),
	(20010, 'product', 'product', 'family', '', 20000, 1, 'PRODUCT_MENU_FAMILY', 2, '', '', 0, ',1,2,', ',1,2,'),
	(20011, 'product', 'product', 'family', 'show_form_new', 20010, 1, 'PRODUCT_MENU_FAMILY_NEW', 1, '', '', 0, ',1,2,', ',1,2,'),
	(20012, 'product', 'product', 'family', 'list_records', 20010, 1, 'PRODUCT_MENU_FAMILY_LIST', 2, '', '', 0, ',1,2,', ',1,2,'),
	(20013, 'product', 'product', 'family', 'list_records_bin', 20010, 1, 'PRODUCT_MENU_FAMILY_LIST_BIN', 3, '', '', 0, ',1,2,', ',1,2,'),
	(20020, 'product', 'product', 'brand', '', 20000, 1, 'PRODUCT_MENU_BRAND', 7, '', '', 0, ',1,2,', ',1,2,'),
	(20021, 'product', 'product', 'brand', 'show_form_new', 20020, 1, 'PRODUCT_MENU_BRAND_NEW', 1, '', '', 0, ',1,2,', ',1,2,'),
	(20022, 'product', 'product', 'brand', 'list_records', 20020, 1, 'PRODUCT_MENU_BRAND_LIST', 2, '', '', 0, ',1,2,', ',1,2,'),
	(20023, 'product', 'product', 'brand', 'list_records_bin', 20020, 1, 'PRODUCT_MENU_BRAND_LIST_BIN', 3, '', '', 0, ',1,2,', ',1,2,'),
	(20030, 'product', 'product', 'tax', '', 20000, 1, 'PRODUCT_MENU_TAX', 8, '', '', 0, ',1,2,', ',1,2,'),
	(20031, 'product', 'product', 'tax', 'show_form_new', 20030, 1, 'PRODUCT_MENU_TAX_NEW', 1, '', '', 0, ',1,2,', ',1,2,'),
	(20032, 'product', 'product', 'tax', 'list_records', 20030, 1, 'PRODUCT_MENU_TAX_LIST', 2, '', '', 0, ',1,2,', ',1,2,'),
	(20033, 'product', 'product', 'tax', 'list_records_bin', 20030, 1, 'PRODUCT_MENU_TAX_LIST_BIN', 3, '', '', 0, ',1,2,', ',1,2,'),
	(20007, 'product', 'product', 'product', 'show_form_new', 20002, 1, 'PRODUCT_MENU_NEW', 0, '', '', 0, ',1,2,', ',1,2,'),
	(20008, 'product', 'product', 'product', 'list_records_images', 20002, 1, 'PRODUCT_MENU_NEW', 1, '', '', 0, ',1,2,', ',1,2,'),
	(20042, 'product', 'product', 'color', 'list_records', 20040, 1, 'PRODUCT_MENU_COLOR_LIST', 2, '', '', 0, ',1,2,', ',1,2,'),
	(20043, 'product', 'product', 'color', 'list_records_bin', 20040, 1, 'PRODUCT_MENU_COLOR_LIST_BIN', 3, '', '', 0, ',1,2,', ',1,2,'),
	(20041, 'product', 'product', 'color', 'show_form_new', 20040, 1, 'PRODUCT_MENU_COLOR_NEW', 1, '', '', 0, ',1,2,', ',1,2,'),
	(20040, 'product', 'product', 'color', '', 20000, 1, 'PRODUCT_MENU_COLOR', 9, '', '', 0, ',1,2,', ',1,2,'),
	(20050, 'product', 'product', 'material', '', 20000, 1, 'PRODUCT_MENU_MATERIAL', 10, '', '', 0, ',1,2,', ',1,2,'),
	(20051, 'product', 'product', 'material', 'show_form_new', 20050, 1, 'PRODUCT_MENU_MATERIAL_NEW', 1, '', '', 0, ',1,2,', ',1,2,'),
	(20052, 'product', 'product', 'material', 'list_records', 20050, 1, 'PRODUCT_MENU_MATERIAL_LIST', 2, '', '', 0, ',1,2,', ',1,2,'),
	(20053, 'product', 'product', 'material', 'list_records_bin', 20050, 1, 'PRODUCT_MENU_MATERIAL_LIST_BIN', 3, '', '', 0, ',1,2,', ',1,2,'),
	(20060, 'product', 'product', 'variation', '', 20000, 1, 'PRODUCT_MENU_VARIATION', 4, '', '', 0, ',1,2,', ',1,2,'),
	(20061, 'product', 'product', 'variation', 'show_form_new', 20060, 1, 'PRODUCT_MENU_VARIATION_NEW', 1, '', '', 0, ',1,2,', ',1,2,'),
	(20062, 'product', 'product', 'variation', 'list_records', 20060, 1, 'PRODUCT_MENU_VARIATION_LIST', 2, '', '', 0, ',1,2,', ',1,2,'),
	(20063, 'product', 'product', 'variation', 'list_records_bin', 20060, 1, 'PRODUCT_MENU_VARIATION_LIST_BIN', 6, '', '', 0, ',1,2,', ',1,2,'),
	(20064, 'product', 'product', 'variation_category', 'show_form_new', 20060, 1, 'PRODUCT_MENU_VARIATION_CATEGORY_NEW', 4, '', '', 0, ',1,2,', ',1,2,'),
	(20065, 'product', 'product', 'variation_category', 'list_records', 20060, 1, 'PRODUCT_MENU_VARIATION_CATEGORY_LIST', 5, '', '', 0, ',1,2,', ',1,2,'),
	(20066, 'product', 'product', 'variation', 'list_records', 20060, 1, 'PRODUCT_MENU_VARIATION_DEFAULT', 3, '', 'default', 0, ',1,2,', ',1,2,'),
	(20014, 'product', 'product', 'family', 'list_records_images', 20011, 1, 'PRODUCT_MENU_FAMILY_NEW', 1, '', '', 0, ',1,2,', ',1,2,'),
	(20024, 'product', 'product', 'brand', 'show_form_new', 20021, 1, 'PRODUCT_MENU_BRAND_NEW', 1, '', '', 0, ',1,2,', ',1,2,'),
	(20025, 'product', 'product', 'brand', 'list_records_images', 20021, 1, 'PRODUCT_MENU_BRAND_NEW', 2, '', '', 0, ',1,2,', ',1,2,'),
	(20026, 'product', 'product', 'brand', 'show_form_new', 20022, 0, 'PRODUCT_MENU_BRAND_NEW', 1, '', '', 0, ',1,2,', ',1,2,'),
	(20027, 'product', 'product', 'brand', 'list_records_images', 20022, 0, 'PRODUCT_MENU_BRAND_NEW', 2, '', '', 0, ',1,2,', ',1,2,'),
	(20070, 'product', 'product', 'promcode', '', 20000, 1, 'PRODUCT_MENU_PROMCODE', 6, '', '', 0, ',1,2,', ',1,2,'),
	(20071, 'product', 'product', 'promcode', 'show_form_new', 20070, 1, 'PRODUCT_MENU_PROMCODE_NEW', 1, '', '', 0, ',1,2,', ',1,2,'),
	(20072, 'product', 'product', 'promcode', 'list_records', 20070, 1, 'PRODUCT_MENU_PROMCODE_LIST', 2, '', '', 0, ',1,2,', ',1,2,'),
	(20073, 'product', 'product', 'promcode', 'list_records_bin', 20070, 1, 'PRODUCT_MENU_PROMCODE_LIST_BIN', 3, '', '', 0, ',1,2,', ',1,2,'),
	(20080, 'product', 'product', 'sector', '', 20000, 1, 'PRODUCT_MENU_SECTOR', 5, '', '', 0, ',1,2,', ',1,2,'),
	(20081, 'product', 'product', 'sector', 'show_form_new', 20080, 1, 'PRODUCT_MENU_SECTOR_NEW', 1, '', '', 0, ',1,2,', ',1,2,'),
	(20082, 'product', 'product', 'sector', 'list_records', 20080, 1, 'PRODUCT_MENU_SECTOR_LIST', 2, '', '', 0, ',1,2,', ',1,2,'),
	(20083, 'product', 'product', 'sector', 'list_records_bin', 20080, 1, 'PRODUCT_MENU_SECTOR_LIST_BIN', 3, '', '', 0, ',1,2,', ',1,2,'),
	(20400, 'sellpoint', 'product', 'sellpoint', '', 0, 1, 'SELLPOINT_MENU_ROOT', 3, '20403', '', 0, ',1,2,', ',1,2,'),
	(20401, 'sellpoint', 'product', 'sellpoint', '', 20400, 1, 'SELLPOINT_MENU_SELLPOINT', 1, '', '', 0, ',1,2,', ',1,2,'),
	(20402, 'sellpoint', 'product', 'sellpoint', 'show_form_new', 20401, 1, 'SELLPOINT_MENU_SELLPOINT_NEW', 1, '', '', 0, ',1,2,', ',1,2,'),
	(20403, 'sellpoint', 'product', 'sellpoint', 'list_records', 20401, 1, 'SELLPOINT_MENU_SELLPOINT_LIST', 2, '', '', 0, ',1,2,', ',1,2,'),
	(20404, 'sellpoint', 'product', 'sellpoint', 'list_records_bin', 20401, 1, 'SELLPOINT_MENU_SELLPOINT_BIN', 3, '', '', 0, ',1,2,', ',1,2,'),
	(20405, 'sellpoint', 'product', 'sellpoint', 'show_form_new', 20402, 1, 'SELLPOINT_MENU_SELLPOINT_NEW', 1, '', '', 0, ',1,2,', ',1,2,'),
	(20406, 'sellpoint', 'product', 'sellpoint', 'list_records_images', 20402, 1, 'SELLPOINT_MENU_SELLPOINT_NEW', 2, '', '', 0, ',1,2,', ',1,2,'),
	(20407, 'sellpoint', 'product', 'sellpoint', 'show_form_new', 20403, 1, 'SELLPOINT_MENU_SELLPOINT_NEW', 1, '', '', 0, ',1,2,', ',1,2,'),
	(20408, 'sellpoint', 'product', 'sellpoint', 'list_records_images', 20403, 1, 'SELLPOINT_MENU_SELLPOINT_NEW', 2, '', '', 0, ',1,2,', ',1,2,'),
	(4110, 'web', 'web', 'config', 'list_records', 4001, 1, 'WEB_MENU_CONFIGPRODUCT_ADMIN_LIST', 12, '', 'product_admin', 0, ',1,2,', ',1,2,'),
	(4111, 'web', 'web', 'config', 'list_records', 4001, 1, 'WEB_MENU_CONFIGPRODUCT_PUBLIC_LIST', 13, '', 'product_public', 0, ',1,2,', ',1,2,'),
	(4015, 'web', 'web', 'config', 'list_records', 4001, 1, 'WEB_MENU_CONFIGCLIENT_LIST', 20, '', 'client_public', 0, ',1,2,', ',1,2,'),
	(151, 'booking', 'booking', 'book', '', 150, 1, 'BOOKING_MENU_BOOK', 1, '', '', 0, ',1,', ',1,'),
	(152, 'booking', 'booking', 'book', 'list_records', 151, 1, 'BOOKING_MENU_BOOK_LIST', 2, '', '', 0, ',1,', ',1,'),
	(153, 'booking', 'booking', 'book', 'list_records', 151, 1, 'BOOKING_MENU_BOOK_LIST_PAST', 3, '', 'past_bookings', 0, ',1,', ',1,'),
	(154, 'booking', 'inmo', 'property', 'list_records_book_search', 151, 1, 'BOOKING_MENU_BOOK_SEARCH', 1, '', '', 0, ',1,2,', ',1,2,'),
	(160, 'booking', 'booking', 'extra', '', 150, 1, 'BOOKING_MENU_EXTRA', 2, '', '', 0, ',1,', ',1,'),
	(161, 'booking', 'booking', 'extra', 'show_form_new', 160, 1, 'BOOKING_MENU_EXTRA_NEW', 1, '', '', 0, ',1,', ',1,'),
	(162, 'booking', 'booking', 'extra', 'list_records', 160, 1, 'BOOKING_MENU_EXTRA_LIST', 2, '', '', 0, ',1,', ',1,'),
	(163, 'booking', 'booking', 'extra', 'list_records_bin', 160, 1, 'BOOKING_MENU_EXTRA_LIST_BIN', 3, '', '', 0, ',1,', ',1,'),
	(170, 'booking', 'booking', 'season', '', 150, 1, 'BOOKING_MENU_SEASON', 3, '', '', 0, ',1,', ',1,'),
	(171, 'booking', 'booking', 'season', 'show_form_new', 170, 1, 'BOOKING_MENU_SEASON_NEW', 1, '', '', 0, ',1,', ',1,'),
	(172, 'booking', 'booking', 'season', 'list_records', 170, 1, 'BOOKING_MENU_SEASON_LIST', 1, '', '', 0, ',1,', ',1,'),
	(20131, 'customer', 'product', 'rate', '', 20100, 1, 'CUSTOMER_MENU_RATE', 3, '', '', 0, ',1,2,', ',1,2,'),
	(20132, 'customer', 'product', 'rate', 'show_form_new', 20131, 1, 'CUSTOMER_MENU_RATE_NEW', 2, '', 'country', 0, ',1,2,', ',1,2,'),
	(20133, 'customer', 'product', 'rate', 'show_form_new', 20131, 1, 'CUSTOMER_MENU_RATE_PROVINCIA_NEW', 3, '', 'provincia', 0, ',1,2,', ',1,2,'),
	(20134, 'customer', 'product', 'rate', 'list_records', 20131, 1, 'CUSTOMER_MENU_RATE_LIST', 1, '', '', 0, ',1,2,', ',1,2,'),
	(150, 'booking', 'booking', '', '', 0, 1, 'BOOKING_MENU_ROOT', 20, '152', '', 0, ',1,', ',1,'),
	(180, 'booking', 'booking', 'promcode', '', 150, 1, 'BOOKING_MENU_PROMCODE', 8, '', '', 0, ',1,', ',1,'),
	(181, 'booking', 'booking', 'promcode', 'show_form_new', 180, 1, 'BOOKING_MENU_PROMCODE_NEW', 1, '', '', 0, ',1,', ',1,'),
	(182, 'booking', 'booking', 'promcode', 'list_records', 180, 1, 'BOOKING_MENU_PROMCODE_LIST', 2, '', '', 0, ',1,', ',1,'),
	(183, 'booking', 'booking', 'promcode', 'list_records_bin', 180, 1, 'BOOKING_MENU_PROMCODE_LIST_BIN', 3, '', '', 0, ',1,', ',1,'),
	(7021, 'user', 'user', 'oneuser', '', 7000, 1, 'USER_MENU_ONEUSER', 1, '', '', 0, ',1,', ',1,'),
	(7022, 'user', 'user', 'oneuser', 'show_form_edit', 7021, 1, 'USER_MENU_ONEUSER_EDIT', 2, '', '', 0, ',1,', ',1,'),
	(190, 'booking', 'booking', 'police', '', 150, 1, 'BOOKING_MENU_POLICE', 4, '', '', 0, ',1,', ',1,'),
	(192, 'booking', 'booking', 'police', 'list_records', 190, 1, 'BOOKING_MENU_POLICE_LIST', 2, '', '', 0, ',1,', ',1,'),
	(5004, 'custumer', 'custumer', 'custumer', 'list_records_inactive', 5001, 1, 'CUSTUMER_MENU_LIST_INACTIVE', 3, '', '', 0, ',1,2,', ',1,2,'),
	(4120, 'web', 'web', 'place', '', 4000, 1, 'WEB_MENU_PLACE', 0, '', '', 0, ',1,2,', ',1,2,'),
	(4121, 'web', 'web', 'place', 'show_form_new', 4120, 1, 'WEB_MENU_PLACE_NEW', 1, '', '', 0, ',1,2,', ',1,2,'),
	(4122, 'web', 'web', 'place', 'list_records', 4120, 1, 'WEB_MENU_PLACE_LIST', 2, '', '', 0, ',1,2,', ',1,2,'),
	(220, 'inmo', 'inmo', 'portal', '', 100, 1, 'INMO_MENU_PORTAL', 6, '', '', 0, ',1,2,', ',1,2,'),
	(221, 'inmo', 'inmo', 'portal', 'list_records', 220, 1, 'INMO_MENU_PORTAL_LIST', 1, '', '', 0, ',1,2,', ',1,2,'),
	(223, 'inmo', 'inmo', 'portal', 'list_records', 220, 1, 'INMO_MENU_PORTAL_LIST_SELECTED', 2, '', 'portal_selected', 0, ',1,2,', ',1,2,'),
	(1083, 'admintotal', 'admintotal', 'generator', 'list_logs', 1080, 1, 'ADMINTOTAL_MENU_GENERATOR_LOG', 3, '', '', 0, ',1,', ',1,'),
	(4200, 'web', 'web', 'gallery', '', 4000, 1, 'WEB_MENU_GALLERY', 1, '', '', 0, ',1,', ',1,'),
	(4201, 'web', 'web', 'gallery', 'show_form_new', 4200, 1, 'WEB_MENU_GALLERY_NEW', 1, '', '', 0, ',1,', ',1,'),
	(4202, 'web', 'web', 'gallery', 'list_records', 4200, 1, 'WEB_MENU_GALLERY_LIST', 1, '', '', 0, ',1,', ',1,'),
	(4203, 'web', 'web', 'gallery', 'list_records', 4200, 1, 'WEB_MENU_GALLERY_LIST_BIN', 3, '', '', 0, ',1,', ',1,'),
	(20090, 'product', 'product', 'spec', '', 20000, 1, 'PRODUCT_MENU_SPEC', 3, '', '', 0, ',1,', ',1,'),
	(20091, 'product', 'product', 'spec', 'show_form_new', 20090, 1, 'PRODUCT_MENU_SPEC_NEW', 1, '', '', 0, ',1,', ',1,'),
	(20092, 'product', 'product', 'spec', 'list_records', 20090, 1, 'PRODUCT_MENU_SPEC_LIST', 2, '', '', 0, ',1,', ',1,'),
	(20093, 'product', 'product', 'spec', 'list_records_bin', 20090, 1, 'PRODUCT_MENU_SPEC_LIST_BIN', 5, '', '', 0, ',1,', ',1,'),
	(20094, 'product', 'product', 'spec_category', 'show_form_new', 20090, 1, 'PRODUCT_MENU_SPEC_CATEGORY_NEW', 3, '', '', 0, ',1,', ',1,'),
	(20095, 'product', 'product', 'spec_category', 'list_records', 20090, 1, 'PRODUCT_MENU_SPEC_CATEGORY_LIST', 4, '', '', 0, ',1,', ',1,'),
	(20141, 'customer', 'product', 'courier', '', 20100, 1, 'CUSTOMER_MENU_COURIER', 4, '', '', 0, ',1,2,3,', ',1,2,'),
	(20142, 'customer', 'product', 'courier', 'show_form_new', 20141, 1, 'CUSTOMER_MENU_COURIER_NEW', 1, '', '', 0, ',1,2,3,', ',1,2,'),
	(20144, 'customer', 'product', 'courier', 'list_records', 20141, 1, 'CUSTOMER_MENU_COURIER_LIST', 2, '', '', 0, ',1,2,3,', ',1,2,'),
	(1034, 'admintotal', 'admintotal', 'query', 'list_records', 1001, 2, 'ADMINTOTAL_MENU_QUERY_LIST', 1, '', '', 0, ',1,', ',1,'),
	(1035, 'admintotal', 'admintotal', 'filesearch', 'list_records', 1001, 3, 'ADMINTOTAL_MENU_FILESEARCH_LIST', 1, '', '', 0, ',1,', ',1,'),
	(20202, 'product', 'product', 'quantityconcept', 'list_records', 20200, 1, 'PRODUCT_MENU_QUANTITYCONCEPT_LIST', 2, '', '', 0, ',1,', ',1,'),
	(20201, 'product', 'product', 'quantityconcept', 'show_form_new', 20200, 1, 'PRODUCT_MENU_QUANTITYCONCEPT_NEW', 1, '', '', 0, ',1,', ',1,'),
	(20200, 'product', 'product', 'quantityconcept', '', 20000, 1, 'PRODUCT_MENU_QUANTITYCONCEPT', 3, '', '', 0, ',1,', ',1,'),
	(1084, 'admintotal', 'admintotal', 'fotocasa', 'admintotal_get_published', 1080, 1, 'ADMINTOTAL_MENU_PORTAL_FOTOCASA', 1, '', '', 0, ',1,', ',1,'),
	(20135, 'customer', 'product', 'country', 'list_records', 20131, 1, 'CUSTOMER_MENU_COUNTRY_LIST', 4, '', '', 0, ',1,2,', ',1,2,'),
	(1012, 'admintotal', 'admintotal', 'error', 'list_records', 1001, 1, 'ADMINTOTAL_MENU_ERROR_LIST', 9, '', '', 0, ',1,', ',1,'),
	(4112, 'web', 'web', 'config', 'list_records', 4001, 1, 'WEB_MENU_CONFIGBOOKING_ADMIN_LIST', 8, '', 'booking_admin', 0, ',1,2,', ',1,2,'),
	(4113, 'web', 'web', 'config', 'list_records', 4001, 1, 'WEB_MENU_CONFIGBOOKING_PUBLIC_LIST', 9, '', 'booking_public', 0, ',1,2,', ',1,2,');
/*!40000 ALTER TABLE `all__menu` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `all__page` (
  `page_id` int(11) NOT NULL AUTO_INCREMENT,
  `page_text_id` varchar(100) COLLATE utf8_spanish2_ci NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `level` tinyint(1) NOT NULL DEFAULT '1',
  `template` varchar(100) COLLATE utf8_spanish2_ci NOT NULL,
  `link` varchar(255) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `ordre` int(11) NOT NULL DEFAULT '0',
  `has_content` tinyint(1) NOT NULL DEFAULT '1',
  `menu` tinyint(1) NOT NULL DEFAULT '1',
  `blocked` tinyint(1) NOT NULL DEFAULT '0',
  `modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `bin` tinyint(1) NOT NULL DEFAULT '0',
  `status` enum('prepare','review','public','archived') COLLATE utf8_spanish2_ci NOT NULL DEFAULT 'prepare',
  `solution` enum('botiga','product','inmo','web','all') COLLATE utf8_spanish2_ci NOT NULL DEFAULT 'all',
  PRIMARY KEY (`page_id`),
  KEY `text_id` (`page_text_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `all__page` DISABLE KEYS */;
INSERT INTO `all__page` (`page_id`, `page_text_id`, `parent_id`, `level`, `template`, `link`, `ordre`, `has_content`, `menu`, `blocked`, `modified`, `bin`, `status`, `solution`) VALUES
	(1, 'home', 0, 1, '', '/', 1, 1, 1, 0, '0000-00-00 00:00:00', 0, 'public', 'all'),
	(3, 'company', 0, 1, 'company', '', 5, 1, 1, 0, '0000-00-00 00:00:00', 0, 'public', 'all'),
	(4, 'contact', 0, 1, '', '/contact/contact', 6, 1, 1, 0, '0000-00-00 00:00:00', 0, 'public', 'all'),
	(5, 'politica', 0, 1, 'politica', '', 1, 1, 5, 0, '0000-00-00 00:00:00', 0, 'public', 'all'),
	(6, 'legal', 0, 1, 'legal', '', 2, 1, 2, 0, '0000-00-00 00:00:00', 0, 'public', 'all'),
	(7, 'services', 0, 1, 'services', '', 4, 1, 1, 0, '0000-00-00 00:00:00', 0, 'public', 'all'),
	(8, 'new', 0, 1, '', '/news/new', 3, 1, 1, 0, '0000-00-00 00:00:00', 0, 'public', 'all'),
	(9, 'property', 0, 1, '', '/inmo/property', 2, 1, 1, 0, '0000-00-00 00:00:00', 0, 'public', 'inmo'),
	(32, 'register', 1, 2, '', '/product/customer/show-form-new', 1, 1, 3, 2, '0000-00-00 00:00:00', 0, 'public', 'product'),
	(33, 'login', 1, 2, '', 'javascript:product.open_login()', 2, 1, 3, 1, '0000-00-00 00:00:00', 0, 'public', 'product'),
	(34, 'account', 1, 2, '', '/product/customer', 1, 1, 4, 1, '0000-00-00 00:00:00', 0, 'public', 'product'),
	(35, 'logout', 1, 2, '', 'javascript:product.logout()', 2, 1, 4, 2, '0000-00-00 00:00:00', 0, 'public', 'product'),
	(31, 'product', 0, 1, '', '/product/product', 2, 1, 1, 0, '0000-00-00 00:00:00', 0, 'public', 'product'),
	(12, 'register', 1, 2, '', '/custumer/custumer/show-login', 0, 1, 3, 0, '0000-00-00 00:00:00', 0, 'public', 'inmo'),
	(13, 'login', 1, 2, '', 'javascript:custumer.open_login()', 0, 1, 3, 0, '0000-00-00 00:00:00', 0, 'public', 'inmo'),
	(14, 'account', 1, 2, '', '/custumer/custumer', 0, 1, 4, 0, '0000-00-00 00:00:00', 0, 'public', 'inmo'),
	(15, 'logout', 1, 2, '', 'javascript:custumer.logout()', 0, 1, 4, 0, '0000-00-00 00:00:00', 0, 'public', 'inmo'),
	(20, 'cookies', 0, 1, 'cookies', '', 4, 1, 2, 0, '2012-09-13 15:40:06', 0, 'public', 'all');
/*!40000 ALTER TABLE `all__page` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `all__page_image` (
  `image_id` int(11) NOT NULL AUTO_INCREMENT,
  `page_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `name_original` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `size` int(11) NOT NULL DEFAULT '0',
  `ordre` int(11) NOT NULL DEFAULT '0',
  `main_image` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`image_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `all__page_image` DISABLE KEYS */;
/*!40000 ALTER TABLE `all__page_image` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `all__page_image_language` (
  `image_id` int(11) NOT NULL DEFAULT '0',
  `language` char(3) COLLATE utf8_spanish2_ci NOT NULL,
  `image_alt` varchar(150) COLLATE utf8_spanish2_ci NOT NULL,
  `image_title` varchar(255) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `all__page_image_language` DISABLE KEYS */;
/*!40000 ALTER TABLE `all__page_image_language` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `all__page_language` (
  `page_id` int(11) NOT NULL DEFAULT '0',
  `language` char(3) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `page_title` varchar(255) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `page_description` varchar(255) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `page_keywords` varchar(255) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `page` varchar(100) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `title` varchar(255) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `body_subtitle` varchar(255) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `body_description` text COLLATE utf8_spanish2_ci NOT NULL,
  `page_file_name` varchar(100) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `page_old_file_name` varchar(100) COLLATE utf8_spanish2_ci NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `all__page_language` DISABLE KEYS */;
INSERT INTO `all__page_language` (`page_id`, `language`, `page_title`, `page_description`, `page_keywords`, `page`, `title`, `body_subtitle`, `body_description`, `page_file_name`, `page_old_file_name`) VALUES
	(1, 'cat', '', '', '', 'Inici', '', '', '', '', ''),
	(1, 'spa', '', '', '', 'Inicio', '', '', '', '', ''),
	(1, 'eng', '', '', '', 'Home', '', '', '', '', ''),
	(1, 'fra', '', '', '', 'Home', '', '', '', '', ''),
	(3, 'cat', '', '', '', 'Empresa', '', '', '', 'empresa', ''),
	(3, 'spa', '', '', '', 'Empresa', '', '', '', 'empresa', ''),
	(3, 'eng', '', '', '', 'Company', '', '', '', 'company', ''),
	(3, 'fra', '', '', '', 'Entreprise', '', '', '', 'entreprise', ''),
	(4, 'cat', '', '', '', 'Contacte', '', '', '', 'contacte', ''),
	(4, 'spa', '', '', '', 'Contacto', '', '', '', 'contacto', ''),
	(4, 'eng', '', '', '', 'Contact', '', '', '', 'contact', ''),
	(4, 'fra', '', '', '', 'Contact', '', '', '', 'contact', ''),
	(5, 'cat', '', '', '', 'Política de privacitat', '', '', '', 'politica', ''),
	(5, 'spa', '', '', '', 'Política de privacidad', '', '', '', 'politica', ''),
	(5, 'eng', '', '', '', 'Privacy policy', '', '', '', 'privacy', ''),
	(5, 'fra', '', '', '', 'Règles de confidentialité', '', '', '', 'confidentialite', ''),
	(6, 'cat', '', '', '', 'Avís legal', '', '', '', 'avis', ''),
	(6, 'spa', '', '', '', 'Aviso legal', '', '', '', 'aviso', ''),
	(6, 'eng', '', '', '', 'Legal notice', '', '', '', 'notice', ''),
	(6, 'fra', '', '', '', 'Avertissement légal', '', '', '', 'avertissement', ''),
	(7, 'cat', '', '', '', 'Serveis', '', '', '', 'serveis', ''),
	(7, 'spa', '', '', '', 'Servicios', '', '', '', 'servicios', ''),
	(7, 'eng', '', '', '', 'Services', '', '', '', 'services', ''),
	(7, 'fra', '', '', '', 'Services', '', '', '', 'services', ''),
	(8, 'cat', '', '', '', 'Actualitat', '', '', '', 'noticies', ''),
	(8, 'spa', '', '', '', 'Actualidad', '', '', '', 'noticias', ''),
	(8, 'eng', '', '', '', 'News', '', '', '', 'news', ''),
	(8, 'fra', '', '', '', 'Actualité', '', '', '', 'nouvelles', ''),
	(9, 'cat', '', '', '', 'Immobles', '', '', '', 'immobles', ''),
	(9, 'spa', '', '', '', 'Inmuebles', '', '', '', 'inmuebles', ''),
	(9, 'eng', '', '', '', 'Properties', '', '', '', 'properties', ''),
	(9, 'fra', '', '', '', 'Propriétés', '', '', '', 'immeubles', ''),
	(32, 'cat', '', '', '', 'Registrar-se', '', '', '', 'registrarse', ''),
	(32, 'spa', '', '', '', 'Registrarse', '', '', '', 'registrarse', ''),
	(32, 'eng', '', '', '', 'Register', '', '', '', 'register', ''),
	(32, 'fra', '', '', '', 'Enregistrer', '', '', '', 'enregistrer', ''),
	(33, 'cat', '', '', '', 'Accedir', '', '', '', '', ''),
	(33, 'spa', '', '', '', 'Acceder', '', '', '', '', ''),
	(33, 'eng', '', '', '', 'Login', '', '', '', '', ''),
	(33, 'fra', '', '', '', 'Login', '', '', '', '', ''),
	(34, 'cat', '', '', '', 'El meu compte', '', '', '', 'compte', ''),
	(34, 'spa', '', '', '', 'Mi cuenta', '', '', '', 'cuenta', ''),
	(34, 'eng', '', '', '', 'My account', '', '', '', 'account', ''),
	(34, 'fra', '', '', '', 'Ma compte', '', '', '', 'compte', ''),
	(35, 'cat', '', '', '', 'Sortir', '', '', '', '', ''),
	(35, 'spa', '', '', '', 'Salir', '', '', '', '', ''),
	(35, 'eng', '', '', '', 'Logout', '', '', '', '', ''),
	(35, 'fra', '', '', '', 'Sortir', '', '', '', '', ''),
	(31, 'cat', '', '', '', 'Productes', '', '', '', 'productes', ''),
	(31, 'spa', '', '', '', 'Productos', '', '', '', 'productos', ''),
	(31, 'eng', '', '', '', 'Products', '', '', '', 'products', ''),
	(31, 'fra', '', '', '', 'Produits', '', '', '', 'produits', ''),
	(12, 'spa', '', '', '', 'Registrarse', '', '', '', 'registrarse', ''),
	(12, 'eng', '', '', '', 'Register', '', '', '', 'register', ''),
	(12, 'fra', '', '', '', 'Enregistrer', '', '', '', 'enregistrer', ''),
	(12, 'cat', '', '', '', 'Registrar-se', '', '', '', 'registrarse', ''),
	(13, 'spa', '', '', '', 'Acceder', '', '', '', '', ''),
	(13, 'eng', '', '', '', 'Login', '', '', '', '', ''),
	(13, 'fra', '', '', '', 'Login', '', '', '', '', ''),
	(13, 'cat', '', '', '', 'Accedir', '', '', '', '', ''),
	(14, 'spa', '', '', '', 'Mi cuenta', '', '', '', 'cuenta', ''),
	(14, 'eng', '', '', '', 'My account', '', '', '', 'account', ''),
	(14, 'fra', '', '', '', 'Mon compte', '', '', '', 'compte', ''),
	(14, 'cat', '', '', '', 'El meu compte', '', '', '', 'compte', ''),
	(15, 'spa', '', '', '', 'Salir', '', '', '', '', ''),
	(15, 'eng', '', '', '', 'Logout', '', '', '', '', ''),
	(15, 'fra', '', '', '', 'Sortir', '', '', '', '', ''),
	(15, 'cat', '', '', '', 'Sortir', '', '', '', '', ''),
	(20, 'cat', '', '', '', 'Política de cookies', '', '', '', 'cookies', ''),
	(20, 'spa', '', '', '', 'Política de cookies', '', '', '', 'cookies', ''),
	(20, 'eng', '', '', '', 'Cookies policy', '', '', '', 'cookies', ''),
	(20, 'fra', '', '', '', 'Technologie Cookie', '', '', '', 'cookies', '');
/*!40000 ALTER TABLE `all__page_language` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `all__provincia` (
  `provincia_id` int(11) NOT NULL DEFAULT '0',
  `admin2_id` varchar(20) DEFAULT NULL,
  `comunitat_id` varchar(20) DEFAULT NULL,
  `country_id` char(10) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  `alternatenames` varchar(4000) DEFAULT NULL,
  PRIMARY KEY (`provincia_id`),
  KEY `admin2_id` (`admin2_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*!40000 ALTER TABLE `all__provincia` DISABLE KEYS */;
INSERT INTO `all__provincia` (`provincia_id`, `admin2_id`, `comunitat_id`, `country_id`, `name`, `alternatenames`) VALUES
	(2509951, 'V', '60', 'ES', 'Valencia', 'Balenzia,Province de Valence,Provincia de Valencia,Província de València,Valence,Valencia,Valentzia,Valenza,València,Valência,バレンシア,ヴァレンシア'),
	(2510407, 'TO', '54', 'ES', 'Toledo', 'Provincia de Toledo,Tolede,Toledo,Tolède,トレド'),
	(2510910, 'SE', '51', 'ES', 'Sevilla', 'Provincia de Sevilla,Sebilla,Sevilha,Sevilla,Seville,Siviglia,Séville,セビリアセビリヤセヴィーリャセヴィリア,セビーリャ（セビージャ）'),
	(2511173, 'TF', '53', 'ES', 'Santa Cruz de Tenerife', 'Province de Santa Cruz de Tenerife,Province de Santa Cruz de Ténérife,Province of Santa Cruz de Tenerife,Provincia de Santa Cruz de Tenerife,Provinz Santa Cruz de Tenerife,Província de Santa Cruz de Tenerife,Santa Cruz de Tenerife,Tenerife,Tenerifeko Santa Cruz,サンタ・クルス・デ・テネリフェ'),
	(2514254, 'MA', '51', 'ES', 'Málaga', 'Malaga,Màlaga,Málaga,Provincia de Malaga,Provincia de Málaga,マラガ'),
	(2515271, 'GC', '53', 'ES', 'Las Palmas', 'As Palmas,Las Palmas,Les Palmes,Province de Las Palmas,Province of Las Palmas,Provincia de Las Palmas,Provinz Las Palmas,Província de Las Palmas,ラスパルマス,ラス・パルマス'),
	(2516394, 'J', '51', 'ES', 'Jaén', 'Chaen,Chaén,Jaen,Jaén,Provincia de Jaen,Provincia de Jaén,Xaen,Xaén,ハエン'),
	(2516547, 'H', '51', 'ES', 'Huelva', 'Huelva,Provincia de Huelva,Uelba,ウエルバ'),
	(2517115, 'GR', '51', 'ES', 'Granada', 'Granada,Granata,Grenade,Provincia de Granada,グラナダ'),
	(2519034, 'CU', '54', 'ES', 'Cuenca', 'Conca,Cuenca,Provincia de Cuenca,クエンカ'),
	(2519239, 'CO', '51', 'ES', 'Córdoba', 'Cordoba,Cordoue,Cordova,Còrdova,Córdoba,Kordoba,コルドバ'),
	(2519401, 'CR', '54', 'ES', 'Ciudad Real', 'Cidade Real,Ciuda Real,Ciudad Real,Ciudá Real,Ziudat Reyal,シウダッド・レアル'),
	(2520597, 'CA', '51', 'ES', 'Cádiz', 'Cadice,Cadix,Cadiz,Cádiz,Provincia de Cadis,Provincia de Cadiz,Provincia de Cádiz,Província de Cadis,カディス'),
	(2520610, 'CC', '57', 'ES', 'Cáceres', 'Caceres,Cazeres,Càceres,Cáceres,Provincia de Caceres,Provincia de Cáceres,カセレス'),
	(2521419, 'BA', '57', 'ES', 'Badajoz', 'Badajoz,Provincia de Badajoz,バダホス'),
	(2521883, 'AL', '51', 'ES', 'Almería', 'Almeria,Almería,Alméria,Ourense,Provincia de Almeria,Provincia de Almería,アルメリア'),
	(2521976, 'A', '60', 'ES', 'Alicante', 'Alacant,Alacante,Alicant,Alicante,Provincia d\'Alacant,Provincia de Alicante,Província d\'Alacant,アリカンテ'),
	(2522257, 'AB', '54', 'ES', 'Albacete', 'Albacete,Albazet,Provincia de Albacete,アルバセテ'),
	(3104323, 'Z', '52', 'ES', 'Zaragoza', 'Province de Saragosse,Provincia de Saragossa,Provincia de Zaragoza,Província de Saragossa,Prtovince of Saragossa,Saragoca,Saragossa,Saragosse,Saragozza,Saragoça,Zaragoza,サラゴサ'),
	(3104341, 'ZA', '55', 'ES', 'Zamora', 'Provincia de Zamora,Zamora,サモーラ'),
	(3104469, 'BI', '59', 'ES', 'Bizkaia', 'Biscaia,Biscay,Biscaya,Biscaye,Bizcaya,Bizkaia,Provincia de Vizcaya,Senorio de Vizcaya,Señorío de Vizcaya,Vizcaya,ビスカヤ'),
	(3106671, 'VA', '55', 'ES', 'Valladolid', 'Balladolit,Provincia de Valladolid,Valladolid,バリァドリッド（バジャドリッド）'),
	(3108125, 'TE', '52', 'ES', 'Teruel', 'Province deTeruel,Province of Teruel,Provincia de Terol,Provincia de Teruel,Provinz Teruel,Província de Terol,Tergueel,Tergüel,Terol,Teruel,テルエル'),
	(3108287, 'T', '56', 'ES', 'Tarragona', 'Province de Tarragone,Province of Tarragona,Provincia de Tarragona,Provinz Tarragona,Província de Tarragona,Tarragona,タラゴナ'),
	(3108680, 'SO', '55', 'ES', 'Soria', 'Provincia de Soria,Soria,Sòria,Sória,ソリア'),
	(3109254, 'SG', '55', 'ES', 'Segovia', 'Provincia de Segovia,Segobia,Segovia,Segovie,Segòvia,Segóvia,Ségovie,セゴビア'),
	(3109716, 'S', '39', 'ES', 'Cantabria', 'Province de Cantabrie,Province of Cantabria,Provincia de Cantabria,Provinz Cantabria,Província de Cantabria,カンタブリア'),
	(3111107, 'SA', '55', 'ES', 'Salamanca', 'Provincia de Salamanca,Salamanca,Salamanque,サラマンカ'),
	(3113208, 'PO', '58', 'ES', 'Pontevedra', 'Pontebedra,Pontevedra,Provincia de Pontevedra,Puntevedra,ポンテベドラ'),
	(3114530, 'P', '55', 'ES', 'Palencia', 'Palence,Palencia,Palentzia,Palenzia,Palència,Palência,Provincia de Palencia,パレンシア'),
	(3114964, 'OR', '58', 'ES', 'Ourense', 'Orense,Ourense,Provincia de Orense,Provincia de Ourense,オレンセ（オウレンセ）'),
	(3117813, 'LU', '58', 'ES', 'Lugo', 'Lugo,Provincia de Lugo,ルゴ'),
	(3118528, 'LE', '55', 'ES', 'León', 'Leon,León,Lion,Lión,Lleo,Lleó,Llion,Llión,Léon,Provincia de Leon,Provincia de León,レオン'),
	(3119840, 'C', '58', 'ES', 'A Coruña', 'A Coruna,A Coruña,Coruna,Corunha,Corunna,Coruña,La Corogne,La Coruna,La Coruña,Provincia de Coruna,Provincia de Coruña,Provincia de La Coruna,Provincia de La Coruña,ラコルーニャ,ラ・コルーニャ'),
	(3120513, 'HU', '52', 'ES', 'Huesca', 'Huesca,Osca,Province de Huesca,Province of Huesca,Provincia d\'Osca,Provincia de Huesca,Provinz Huesca,Província d\'Osca,Uesca,ウエスカ'),
	(3120935, 'SS', '59', 'ES', 'Gipuzkoa', 'Gipuzkoa,Guipuscoa,Guipuzcoa,Guipuzcua,Guipúscoa,Guipúzcoa,ギプスコア'),
	(3121069, 'GU', '54', 'ES', 'Guadalajara', 'Guadalachara,Guadalajara,Guadalaxara,Provincia de Guadalajara,グアダラハラ'),
	(3125881, 'CS', '60', 'ES', 'Castellón', 'Castello,Castellon,Castelló,Castellón,Provincia de Castello,Provincia de Castellon,Provincia de Castellon de la Plana,Provincia de Castellón,Provincia de Castellón de la Plana,Província de Castelló,カステジョン,カステヨン'),
	(3127460, 'BU', '55', 'ES', 'Burgos', 'Burgos,Provincia de Burgos,ブルゴス'),
	(3128759, 'B', '56', 'ES', 'Barcelona', 'Barcelona,Bartzelona,Barzelona,Province de Barcelone,Province of Barcelona,Provincia de Barcelona,Provinz Barcelona,Província de Barcelona,ばるせろな,バルセロナ'),
	(3129138, 'AV', '55', 'ES', 'Ávila', 'Abila,Avila,Badaxoz,Provincia de Avila,Provincia de Ávila,Àvila,Ávila,アビラ'),
	(3130717, 'VI', '59', 'ES', 'Álava', 'Alaba,Alava,Araba,Provincia de Alava,Provincia de Álava,Àlaba,Álava,アラバ'),
	(6355230, 'GI', '56', 'ES', 'Girona', 'Chirona,Gerona,Gerunda,Girona,Province de Gerone,Province de Gérone,Province of Girona,Provincia de Girona,Provinz Girona,Província de Girona,Xerona,Xirona,ジローナ（ヘローナ）'),
	(6355231, 'L', '56', 'ES', 'Lleida', 'Lerida,Lleida,Lérida,Province de Lleida,Province of Lleida,Provincia de Lleida,Provinz Lleida,Província de Lleida,ジェイダ（レリダ）'),
	(6355232, 'LO', '27', 'ES', 'La Rioja', 'A Rioxa,Errioxa,La Rioxa,Province de La Rioja,Province of La Rioja,Provincia de La Rioja,Provinz La Rioja,Província de La Rioja,ラ・リオハ'),
	(6355233, 'M', '29', 'ES', 'Madrid', 'Madrid,Madril,Province de Madrid,Province of Madrid,Provincia de Madrid,Provinz Madrid,Província de Madrid,マドリッド,マドリード/マドリ−ド/マドリ－ド'),
	(6355234, 'MU', '31', 'ES', 'Murcia', 'Murcia,Murtzia,Murzia,Múrcia,Province de Murcie,Province of Murcia,Provincia de Murcia,Provinz Murcia,Província de Múrcia,ムルシア'),
	(6355235, 'NA', '32', 'ES', 'Navarra', 'Nabarra,Nafarroa,Navarra,Province de Navarre,Province of Navarre,Provincia de Navarra,Provinz Navarra,Província de Navarra,ナバーラ'),
	(6355236, 'O', '34', 'ES', 'Asturias', 'Asturias,Asturie,Asturies,Province d\'Asturies,Province of Asturias,Provincia d\'Asturies,Provincia de Asturias,Provinz Asturien,Província d\'Astúries,アストゥリアス'),
	(6424360, 'PM', '07', 'ES', 'Illes Baleares', 'Balearane,Balearen Inseln,Balearene,Ilhas Baleares,Illes Balears,Islas Baleares,Province des Iles Baleares,Province des Îles Baléares,Province of Balearic Islands,Provincia de Illes Balears,Provincia de les Illes Balears,Província de les Illes Balears,las Baleares,ses Illes,バレアレス'),
	(2519582, NULL, '1', 'ES', 'Ceuta', NULL),
	(6362988, NULL, '2', 'ES', 'Melilla', NULL);
/*!40000 ALTER TABLE `all__provincia` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `all__toolmode` (
  `toolmode_id` int(11) NOT NULL AUTO_INCREMENT,
  `mode` varchar(100) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `description` varchar(100) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`toolmode_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `all__toolmode` DISABLE KEYS */;
INSERT INTO `all__toolmode` (`toolmode_id`, `mode`, `description`) VALUES
	(1, '', 'Inactive'),
	(2, '1', 'Basic'),
	(3, '2', 'Advanced');
/*!40000 ALTER TABLE `all__toolmode` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `booking__book` (
  `book_id` int(11) NOT NULL AUTO_INCREMENT,
  `property_id` int(11) NOT NULL,
  `adult` int(11) NOT NULL,
  `child` int(11) NOT NULL,
  `baby` int(11) NOT NULL,
  `date_in` date NOT NULL,
  `date_out` date NOT NULL,
  `name` varchar(100) COLLATE utf8_spanish2_ci NOT NULL,
  `surnames` varchar(100) COLLATE utf8_spanish2_ci NOT NULL,
  `mail` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `adress` varchar(250) COLLATE utf8_spanish2_ci NOT NULL,
  `comment` text COLLATE utf8_spanish2_ci NOT NULL,
  `comment_private` text COLLATE utf8_spanish2_ci NOT NULL,
  `phone` int(10) NOT NULL,
  `book_entered` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `promcode_discount` decimal(11,2) NOT NULL,
  `payment_1` decimal(11,2) NOT NULL,
  `payment_done_1` decimal(11,2) NOT NULL,
  `payment_entered_1` datetime NOT NULL,
  `payment_2` decimal(11,2) NOT NULL,
  `payment_done_2` decimal(11,2) NOT NULL,
  `payment_entered_2` datetime NOT NULL,
  `payment_3` decimal(11,2) NOT NULL,
  `payment_done_3` decimal(11,2) NOT NULL,
  `payment_entered_3` datetime NOT NULL,
  `payment_4` decimal(11,2) NOT NULL,
  `payment_done_4` decimal(11,2) NOT NULL,
  `payment_entered_4` datetime NOT NULL,
  `price_book` decimal(11,2) NOT NULL,
  `price_extras` decimal(11,2) NOT NULL,
  `price_tt` decimal(11,2) NOT NULL,
  `price_total` decimal(11,2) NOT NULL,
  `is_owner` tinyint(1) NOT NULL DEFAULT '0',
  `contract_id` tinyint(11) NOT NULL DEFAULT '1',
  `status` enum('pre_booked','booked','confirmed_email','confirmed_post','contract_signed','cancelled') COLLATE utf8_spanish2_ci NOT NULL DEFAULT 'booked',
  `order_status_1` enum('paying','failed','pending','denied','voided','refunded','completed') COLLATE utf8_spanish2_ci NOT NULL DEFAULT 'paying',
  `payment_method_1` enum('none','cash','paypal','account','directdebit','lacaixa') COLLATE utf8_spanish2_ci NOT NULL DEFAULT 'none',
  `order_status_2` enum('paying','failed','pending','denied','voided','refunded','completed') COLLATE utf8_spanish2_ci NOT NULL DEFAULT 'paying',
  `payment_method_2` enum('none','cash','paypal','account','directdebit','lacaixa') COLLATE utf8_spanish2_ci NOT NULL DEFAULT 'none',
  `order_status_3` enum('paying','failed','pending','denied','voided','refunded','completed') COLLATE utf8_spanish2_ci NOT NULL DEFAULT 'paying',
  `payment_method_3` enum('none','cash','paypal','account','directdebit','lacaixa') COLLATE utf8_spanish2_ci NOT NULL DEFAULT 'none',
  `order_status_4` enum('paying','failed','pending','denied','voided','refunded','completed') COLLATE utf8_spanish2_ci NOT NULL DEFAULT 'paying',
  `payment_method_4` enum('none','cash','paypal','account','directdebit','lacaixa') COLLATE utf8_spanish2_ci NOT NULL DEFAULT 'none',
  `error_code_1` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `error_description_1` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `transaction_id_1` char(17) COLLATE utf8_spanish2_ci NOT NULL,
  `approval_code_1` char(17) COLLATE utf8_spanish2_ci NOT NULL,
  `error_code_2` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `error_description_2` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `transaction_id_2` char(17) COLLATE utf8_spanish2_ci NOT NULL,
  `approval_code_2` char(17) COLLATE utf8_spanish2_ci NOT NULL,
  `error_code_3` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `error_description_3` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `transaction_id_3` char(17) COLLATE utf8_spanish2_ci NOT NULL,
  `approval_code_3` char(17) COLLATE utf8_spanish2_ci NOT NULL,
  `error_code_4` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `error_description_4` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `transaction_id_4` char(17) COLLATE utf8_spanish2_ci NOT NULL,
  `approval_code_4` char(17) COLLATE utf8_spanish2_ci NOT NULL,
  `vreasy_id` int(11) NOT NULL DEFAULT '0',
  `book_owner` enum('letnd','vreasy') COLLATE utf8_spanish2_ci NOT NULL DEFAULT 'letnd',
  `synced` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`book_id`),
  KEY `property` (`property_id`),
  KEY `date_in` (`date_in`),
  KEY `date_out` (`date_out`),
  KEY `is_owner` (`is_owner`),
  KEY `synced` (`synced`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `booking__book` DISABLE KEYS */;
/*!40000 ALTER TABLE `booking__book` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `booking__book_to_custumer` (
  `book_id` int(11) NOT NULL DEFAULT '0',
  `custumer_id` int(11) NOT NULL DEFAULT '0',
  `ordre` int(11) DEFAULT '1',
  KEY `custumer_id` (`custumer_id`),
  KEY `group_id` (`book_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci ROW_FORMAT=FIXED;

/*!40000 ALTER TABLE `booking__book_to_custumer` DISABLE KEYS */;
/*!40000 ALTER TABLE `booking__book_to_custumer` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `booking__color` (
  `color_id` int(11) NOT NULL AUTO_INCREMENT,
  `color` varchar(15) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`color_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `booking__color` DISABLE KEYS */;
INSERT INTO `booking__color` (`color_id`, `color`) VALUES
	(1, '204,206,1'),
	(2, '175,177,7'),
	(3, '143,145,2'),
	(4, '253,196,21'),
	(5, '244,150,0'),
	(6, '195,120,0'),
	(7, '233,81,14'),
	(8, '255,0,0'),
	(9, '207,0,0'),
	(10, '227,50,140'),
	(11, '187,10,126'),
	(12, '149,27,128'),
	(13, '82,191,211'),
	(14, '1,164,169'),
	(15, '4,136,140'),
	(16, '144,181,255'),
	(17, '3,86,254'),
	(18, '3,59,173');
/*!40000 ALTER TABLE `booking__color` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `booking__configadmin` (
  `configadmin_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `value` varchar(255) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`configadmin_id`),
  KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci ROW_FORMAT=DYNAMIC;

/*!40000 ALTER TABLE `booking__configadmin` DISABLE KEYS */;
INSERT INTO `booking__configadmin` (`configadmin_id`, `name`, `value`) VALUES
	(1, 'has_contracts', '0'),
	(2, 'codi_establiment', ''),
	(3, 'nom_establiment', ''),
	(4, 'contract_default_language', 'spa'),
	(5, 'default_status', 'booked'),
	(6, 'public_default_status', 'booked'),
	(7, 'contract_format', 'pdf'),
	(8, 'ignore_outside_ue', '0'),
	(9, 'has_payment', '0');
/*!40000 ALTER TABLE `booking__configadmin` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `booking__configpublic` (
  `configpublic_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `value` varchar(1024) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`configpublic_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `booking__configpublic` DISABLE KEYS */;
INSERT INTO `booking__configpublic` (`configpublic_id`, `name`, `value`) VALUES
	(1, 'google_analytics_conversion', ''),
	(3, 'lacaixa_clave_sha2', ''),
	(4, 'lacaixa_merchant_code', ''),
	(5, 'lacaixa_simulador', '1'),
	(6, 'lacaixa_terminal', '1'),
	(7, 'payment_methods', 'paypal,lacaixa'),
	(8, 'paypal_mail', ''),
	(9, 'paypal_sandbox', '1'),
	(10, 'banc_name', ''),
	(11, 'account_number', '');
/*!40000 ALTER TABLE `booking__configpublic` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `booking__contract` (
  `contract_id` int(11) NOT NULL AUTO_INCREMENT,
  `initial_percent` int(11) NOT NULL DEFAULT '0',
  `entered` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `whole_import` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`contract_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `booking__contract` DISABLE KEYS */;
INSERT INTO `booking__contract` (`contract_id`, `initial_percent`, `entered`, `whole_import`) VALUES
	(1, 40, '2015-12-09 12:44:16', 0);
/*!40000 ALTER TABLE `booking__contract` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `booking__custumer_to_promcode` (
  `custumer_promcode_id` int(10) NOT NULL AUTO_INCREMENT,
  `custumer_id` int(10) NOT NULL,
  `promcode_id` int(10) NOT NULL,
  PRIMARY KEY (`custumer_promcode_id`),
  KEY `custumer_id` (`custumer_id`),
  KEY `promcode_id` (`promcode_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `booking__custumer_to_promcode` DISABLE KEYS */;
/*!40000 ALTER TABLE `booking__custumer_to_promcode` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `booking__extra` (
  `extra_id` int(11) NOT NULL AUTO_INCREMENT,
  `extra_type` enum('required','optional','included') COLLATE utf8_spanish2_ci DEFAULT NULL,
  `has_quantity` tinyint(1) DEFAULT '0',
  `concept` enum('normal','deposit','cleaning') COLLATE utf8_spanish2_ci DEFAULT 'normal',
  `bin` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`extra_id`),
  KEY `bin` (`bin`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `booking__extra` DISABLE KEYS */;
INSERT INTO `booking__extra` (`extra_id`, `extra_type`, `has_quantity`, `concept`, `bin`) VALUES
	(1, 'required', 0, 'deposit', 0),
	(2, 'required', 0, 'cleaning', 0),
	(3, 'optional', 1, 'normal', 1),
	(4, 'optional', 1, 'normal', 0),
	(5, 'included', 0, 'normal', 0),
	(6, 'included', 0, 'normal', 0),
	(7, 'optional', 1, 'normal', 0),
	(8, 'included', 0, 'normal', 0);
/*!40000 ALTER TABLE `booking__extra` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `booking__extra_language` (
  `extra_id` int(11) DEFAULT NULL,
  `language` char(3) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `extra` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `comment` text COLLATE utf8_spanish2_ci,
  KEY `service_id` (`extra_id`),
  KEY `language` (`language`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `booking__extra_language` DISABLE KEYS */;
INSERT INTO `booking__extra_language` (`extra_id`, `language`, `extra`, `comment`) VALUES
	(1, 'eng', 'Deposit', ''),
	(1, 'spa', 'Fianza', ''),
	(1, 'cat', 'Fianza', ''),
	(1, 'fra', 'Dépôt de garantie', ''),
	(2, 'cat', 'Neteja', ''),
	(2, 'spa', 'Limpieza', ''),
	(2, 'eng', 'Cleaning', ''),
	(2, 'fra', 'Nettoyage', ''),
	(3, 'cat', 'Tovalloles', ''),
	(3, 'spa', 'Toallas', ''),
	(3, 'eng', 'Towels', ''),
	(3, 'fra', 'Serviettes', ''),
	(4, 'cat', 'Llençols (per persona)', ''),
	(4, 'spa', 'Sábanas (por persona)', ''),
	(4, 'eng', 'Linen sets (for person)', ''),
	(4, 'fra', 'Draps (par personne)', ''),
	(5, 'cat', 'Televisió', ''),
	(5, 'spa', 'Televisión', ''),
	(5, 'eng', 'Television', ''),
	(5, 'fra', 'Télévision', ''),
	(6, 'cat', 'Internet-Wi-Fi', ''),
	(6, 'spa', 'Internet-Wi-Fi', ''),
	(6, 'eng', 'Internet-Wi-Fi', ''),
	(6, 'fra', 'Internet-Wi-Fi', ''),
	(7, 'cat', 'Cuna bebe', ''),
	(7, 'spa', 'Cuna bebe', ''),
	(7, 'eng', 'Baby bed', ''),
	(7, 'fra', 'Lit de bébé', ''),
	(8, 'cat', 'Canals satèl·lit', ''),
	(8, 'spa', 'Canales satélite', ''),
	(8, 'eng', 'Satellite channels', ''),
	(8, 'fra', 'Chaînes Satélite', '');
/*!40000 ALTER TABLE `booking__extra_language` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `booking__extra_to_book` (
  `extra_book_id` int(11) NOT NULL AUTO_INCREMENT,
  `book_id` int(11) NOT NULL DEFAULT '0',
  `extra_id` int(11) NOT NULL DEFAULT '0',
  `extra_price` decimal(6,2) NOT NULL DEFAULT '0.00',
  `quantity` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`extra_book_id`),
  KEY `book_id` (`book_id`),
  KEY `extra_id` (`extra_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `booking__extra_to_book` DISABLE KEYS */;
/*!40000 ALTER TABLE `booking__extra_to_book` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `booking__extra_to_property` (
  `extra_property_id` int(11) NOT NULL AUTO_INCREMENT,
  `property_id` int(11) NOT NULL DEFAULT '0',
  `extra_id` int(11) NOT NULL DEFAULT '0',
  `extra_price` decimal(6,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`extra_property_id`),
  KEY `property_id` (`property_id`),
  KEY `extra_id` (`extra_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `booking__extra_to_property` DISABLE KEYS */;
/*!40000 ALTER TABLE `booking__extra_to_property` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `booking__promcode` (
  `promcode_id` int(10) NOT NULL AUTO_INCREMENT,
  `promcode` varchar(100) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `discount_pvp` decimal(11,2) NOT NULL DEFAULT '0.00',
  `discount_type` enum('fixed','percent') COLLATE utf8_spanish2_ci NOT NULL DEFAULT 'fixed',
  `minimum_amount` decimal(11,2) NOT NULL DEFAULT '0.00',
  `one_time_only` tinyint(1) NOT NULL DEFAULT '1',
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `custumer_id` int(11) NOT NULL,
  `bin` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`promcode_id`),
  KEY `bin` (`bin`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `booking__promcode` DISABLE KEYS */;
/*!40000 ALTER TABLE `booking__promcode` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `booking__promcode_language` (
  `promcode_id` int(11) NOT NULL DEFAULT '0',
  `language` char(3) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `name` varchar(100) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  KEY `promcode_id` (`promcode_id`),
  KEY `language` (`language`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `booking__promcode_language` DISABLE KEYS */;
/*!40000 ALTER TABLE `booking__promcode_language` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `booking__propertyrange` (
  `propertyrange_id` int(11) NOT NULL AUTO_INCREMENT,
  `property_id` int(11) NOT NULL DEFAULT '0',
  `season_id` int(11) NOT NULL DEFAULT '0',
  `date_start` date NOT NULL,
  `date_end` date NOT NULL,
  `minimum_nights` int(11) NOT NULL DEFAULT '1',
  `whole_week` tinyint(1) NOT NULL DEFAULT '0',
  `start_day` tinyint(2) NOT NULL DEFAULT '0',
  `price_night` decimal(6,2) NOT NULL DEFAULT '0.00',
  `price_weekend` decimal(6,2) NOT NULL DEFAULT '0.00',
  `price_week` decimal(6,2) NOT NULL DEFAULT '0.00',
  `price_week_type` enum('night','week') COLLATE utf8_spanish2_ci NOT NULL DEFAULT 'week',
  PRIMARY KEY (`propertyrange_id`),
  KEY `property_id` (`property_id`),
  KEY `extra_id` (`season_id`),
  KEY `date_start` (`date_start`),
  KEY `date_end` (`date_end`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci ROW_FORMAT=FIXED;

/*!40000 ALTER TABLE `booking__propertyrange` DISABLE KEYS */;
/*!40000 ALTER TABLE `booking__propertyrange` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `booking__season` (
  `season_id` int(11) NOT NULL AUTO_INCREMENT,
  `ordre` int(11) DEFAULT '0',
  `color_id` int(11) DEFAULT '1',
  PRIMARY KEY (`season_id`),
  KEY `color_id` (`color_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci ROW_FORMAT=DYNAMIC;

/*!40000 ALTER TABLE `booking__season` DISABLE KEYS */;
INSERT INTO `booking__season` (`season_id`, `ordre`, `color_id`) VALUES
	(1, 1, 1),
	(2, 2, 5),
	(3, 3, 8),
	(4, 4, 9),
	(5, 7, 11),
	(6, 5, 14);
/*!40000 ALTER TABLE `booking__season` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `booking__season_language` (
  `season_id` int(11) NOT NULL,
  `language` char(3) COLLATE utf8_spanish2_ci NOT NULL,
  `season` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `comment` text COLLATE utf8_spanish2_ci NOT NULL,
  KEY `service_id` (`season_id`),
  KEY `language` (`language`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci ROW_FORMAT=DYNAMIC;

/*!40000 ALTER TABLE `booking__season_language` DISABLE KEYS */;
INSERT INTO `booking__season_language` (`season_id`, `language`, `season`, `comment`) VALUES
	(1, 'cat', 'Baixa', ''),
	(1, 'spa', 'Baja', ''),
	(1, 'eng', 'Low', ''),
	(1, 'fra', 'Basse', ''),
	(1, 'deu', 'Neben', ''),
	(2, 'cat', 'Mitja', ''),
	(2, 'spa', 'Media', ''),
	(2, 'eng', 'Medium', ''),
	(2, 'fra', 'Moyenne', ''),
	(2, 'deu', 'Mitte', ''),
	(3, 'cat', 'Alta', ''),
	(3, 'spa', 'Alta', ''),
	(3, 'eng', 'High', ''),
	(3, 'fra', 'Haute', ''),
	(3, 'deu', 'Hoch', ''),
	(4, 'cat', 'Molt alta', ''),
	(4, 'spa', 'Muy alta', ''),
	(4, 'eng', 'Very high', ''),
	(4, 'fra', 'Très haute', ''),
	(4, 'deu', 'Sehr hoch', ''),
	(5, 'cat', 'Setmana Santa', ''),
	(5, 'spa', 'Semana Santa', ''),
	(5, 'eng', 'Holy Week', ''),
	(5, 'fra', 'Pâques', ''),
	(5, 'deu', 'Ostern', ''),
	(6, 'cat', 'Nadal', ''),
	(6, 'spa', 'Navidad', ''),
	(6, 'eng', 'Christmas', ''),
	(6, 'fra', 'Noël', ''),
	(6, 'deu', 'Weihnachten', '');
/*!40000 ALTER TABLE `booking__season_language` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `client__configpublic` (
  `configpublic_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `value` varchar(100) COLLATE utf8_spanish2_ci NOT NULL,
  `page_text_id` varchar(100) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `type` enum('text','htmlbasic','html','htmlfull','date','datetime','int') COLLATE utf8_spanish2_ci NOT NULL DEFAULT 'text',
  `ordre` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`configpublic_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci ROW_FORMAT=DYNAMIC;

/*!40000 ALTER TABLE `client__configpublic` DISABLE KEYS */;
INSERT INTO `client__configpublic` (`configpublic_id`, `name`, `value`, `page_text_id`, `type`, `ordre`) VALUES
	(1, 'inici_text_1', '', 'home', 'text', 10),
	(2, 'inici_text_2', '', 'home', 'text', 11),
	(3, 'inici_text_3', '', 'home', 'text', 12),
	(4, 'inici_text_4', '', 'home', 'text', 13),
	(5, 'contacte_text_1', '', 'contact', 'text', 10),
	(6, 'contacte_text_2', '', 'contact', 'text', 11),
	(7, 'politica_privacitat_text_1', '', 'contact', 'text', 12),
	(8, 'politica_privacitat_text_2', '', 'contact', 'text', 13),
	(9, 'menu_mobil', '', '', 'text', 10),
	(10, 'ordenar_per', '', '', 'text', 5),
	(11, 'descarga', '', 'new', 'text', 2),
	(12, 'links_interes', '', 'new', 'text', 3),
	(13, 'missatge_autoresposta_email', '', 'contact', 'text', 0),
	(14, 'accepto_informacio', '', 'contact', 'text', 15);
/*!40000 ALTER TABLE `client__configpublic` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `client__configpublic_language` (
  `configpublic_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(50) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `value` text COLLATE utf8_spanish2_ci NOT NULL,
  `language` char(3) COLLATE utf8_spanish2_ci NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci ROW_FORMAT=DYNAMIC;

/*!40000 ALTER TABLE `client__configpublic_language` DISABLE KEYS */;
INSERT INTO `client__configpublic_language` (`configpublic_id`, `name`, `value`, `language`) VALUES
	(0, 'inici_text_1', 'Presentació empresa', 'cat'),
	(0, 'inici_text_1', 'Presentació empresa', 'eng'),
	(0, 'inici_text_2', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestiae, nisi impedit amet perspiciatis similique illum magnam laudantium iusto qui unde ullam optio quos commodi voluptate beatae! Incidunt, accusantium hic ex.', 'cat'),
	(0, 'inici_text_1', 'Presentació empresa', 'spa'),
	(0, 'inici_text_2', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestiae, nisi impedit amet perspiciatis similique illum magnam laudantium iusto qui unde ullam optio quos commodi voluptate beatae! Incidunt, accusantium hic ex.', 'spa'),
	(0, 'inici_text_2', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestiae, nisi impedit amet perspiciatis similique illum magnam laudantium iusto qui unde ullam optio quos commodi voluptate beatae! Incidunt, accusantium hic ex.', 'eng'),
	(0, 'inici_text_2', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestiae, nisi impedit amet perspiciatis similique illum magnam laudantium iusto qui unde ullam optio quos commodi voluptate beatae! Incidunt, accusantium hic ex.', 'fra'),
	(0, 'inici_text_3', 'Apartat destacat', 'cat'),
	(0, 'inici_text_3', 'Apartat destacat', 'spa'),
	(0, 'inici_text_3', 'Apartat destacat', 'eng'),
	(0, 'inici_text_3', 'Apartat destacat', 'fra'),
	(0, 'inici_text_4', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestiae, nisi impedit amet perspiciatis similique illum magnam laudantium iusto qui unde ullam optio quos commodi voluptate beatae! Incidunt, accusantium hic ex.', 'cat'),
	(0, 'inici_text_4', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestiae, nisi impedit amet perspiciatis similique illum magnam laudantium iusto qui unde ullam optio quos commodi voluptate beatae! Incidunt, accusantium hic ex.', 'spa'),
	(0, 'inici_text_4', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestiae, nisi impedit amet perspiciatis similique illum magnam laudantium iusto qui unde ullam optio quos commodi voluptate beatae! Incidunt, accusantium hic ex.', 'eng'),
	(0, 'inici_text_4', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestiae, nisi impedit amet perspiciatis similique illum magnam laudantium iusto qui unde ullam optio quos commodi voluptate beatae! Incidunt, accusantium hic ex.', 'fra'),
	(0, 'inici_text_1', 'Presentació empresa', 'fra'),
	(0, 'contacte_text_1', 'Si tiene cualquier consulta o duda, se puede poner en contacto con nosotros mediante el siguiente formulario, a través de', 'spa'),
	(0, 'contacte_text_1', 'If you have any questions or doubts, please contact us, through this form, by mail', 'eng'),
	(0, 'contacte_text_1', 'Si vous avez des questions ou des prèoccupations, contactez–nous  à travèrs ce formulaire, par  e-mail à', 'fra'),
	(0, 'contacte_text_1', 'Si te qualsevol consulta o dubte, es pot posar en contacte amb nosaltres mitjançant aquest formulari, a travès de ', 'cat'),
	(0, 'contacte_text_2', 'o trucant al', 'cat'),
	(0, 'contacte_text_2', 'o llamando al', 'spa'),
	(0, 'contacte_text_2', 'or calling at', 'eng'),
	(0, 'contacte_text_2', 'ou par téléphone au', 'fra'),
	(0, 'politica_privacitat_text_1', 'He leído y acepto la', 'spa'),
	(0, 'politica_privacitat_text_1', 'I have read and accept the', 'eng'),
	(0, 'politica_privacitat_text_1', 'J’ai lu et j’accepte les', 'fra'),
	(0, 'politica_privacitat_text_1', 'He llegit i accepto la ', 'cat'),
	(0, 'politica_privacitat_text_2', 'política de privacidad', 'spa'),
	(0, 'politica_privacitat_text_2', 'privacy policy', 'eng'),
	(0, 'politica_privacitat_text_2', 'politiques des conditions  d’utilisations', 'fra'),
	(0, 'politica_privacitat_text_2', 'política de privacitat', 'cat'),
	(0, 'menu_mobil', 'Menú', 'cat'),
	(0, 'menu_mobil', 'Menú', 'spa'),
	(0, 'menu_mobil', 'Menu', 'eng'),
	(0, 'menu_mobil', 'Menu', 'fra'),
	(0, 'ordenar_per', 'Ordenar per', 'cat'),
	(0, 'ordenar_per', 'Ordenar por', 'spa'),
	(0, 'ordenar_per', 'Order by', 'eng'),
	(0, 'ordenar_per', 'Ordonner par', 'fra'),
	(0, 'descarga', 'Descarregar', 'cat'),
	(0, 'descarga', 'Descargar', 'spa'),
	(0, 'descarga', 'Download', 'eng'),
	(0, 'descarga', 'Télecharger', 'fra'),
	(0, 'links_interes', 'Links', 'cat'),
	(0, 'links_interes', 'Links', 'spa'),
	(0, 'links_interes', 'Links', 'eng'),
	(0, 'links_interes', 'Links', 'fra'),
	(0, 'missatge_autoresposta_email', '', 'cat'),
	(0, 'missatge_autoresposta_email', '', 'spa'),
	(0, 'missatge_autoresposta_email', '', 'eng'),
	(0, 'missatge_autoresposta_email', '', 'fra'),
	(0, 'accepto_informacio', 'Accepto rebre informació sobre les activitats, serveis i\r\nproductes de', 'cat'),
	(0, 'accepto_informacio', 'Acepto recibir información sobre las actividades, servicios y productos de', 'spa'),
	(0, 'accepto_informacio', 'I accept to receive information about activities, services and products from', 'eng'),
	(0, 'accepto_informacio', '', 'fra');
/*!40000 ALTER TABLE `client__configpublic_language` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `contact__configadmin` (
  `name` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `value` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `configadmin_id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`configadmin_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `contact__configadmin` DISABLE KEYS */;
INSERT INTO `contact__configadmin` (`name`, `value`, `configadmin_id`) VALUES
	('custumer_table', 'custumer__custumer', 1);
/*!40000 ALTER TABLE `contact__configadmin` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `contact__configpublic` (
  `name` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `value` varchar(1024) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `configpublic_id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`configpublic_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `contact__configpublic` DISABLE KEYS */;
INSERT INTO `contact__configpublic` (`name`, `value`, `configpublic_id`) VALUES
	('from_address', 'marc@letnd.com', 1),
	('m_host', 'mail.letnd.com', 2),
	('m_username', 'marc@letnd.com', 3),
	('m_password', 'tanreb', 4),
	('google_analytics_conversion', '', 5),
	('send_mail_copy', '1', 6),
	('use_mail_template', '1', 7);
/*!40000 ALTER TABLE `contact__configpublic` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `contact__contact` (
  `contact_id` int(11) NOT NULL AUTO_INCREMENT,
  `accept_info` tinyint(4) NOT NULL DEFAULT '0',
  `company` varchar(250) COLLATE utf8_spanish2_ci NOT NULL,
  `name` varchar(100) COLLATE utf8_spanish2_ci NOT NULL,
  `surnames` varchar(100) COLLATE utf8_spanish2_ci NOT NULL,
  `mail` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `to_address` varchar(250) COLLATE utf8_spanish2_ci NOT NULL,
  `adress` varchar(250) COLLATE utf8_spanish2_ci NOT NULL,
  `matter` varchar(100) COLLATE utf8_spanish2_ci NOT NULL,
  `comment` text COLLATE utf8_spanish2_ci NOT NULL,
  `phone` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `city` varchar(150) COLLATE utf8_spanish2_ci NOT NULL,
  `entered` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`contact_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `contact__contact` DISABLE KEYS */;
/*!40000 ALTER TABLE `contact__contact` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `custumer__configadmin` (
  `name` varchar(50) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `value` varchar(255) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci ROW_FORMAT=DYNAMIC;

/*!40000 ALTER TABLE `custumer__configadmin` DISABLE KEYS */;
INSERT INTO `custumer__configadmin` (`name`, `value`) VALUES
	('default_country', 'ES');
/*!40000 ALTER TABLE `custumer__configadmin` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `custumer__configpublic` (
  `configpublic_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `value` varchar(255) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`configpublic_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci ROW_FORMAT=DYNAMIC;

/*!40000 ALTER TABLE `custumer__configpublic` DISABLE KEYS */;
INSERT INTO `custumer__configpublic` (`configpublic_id`, `name`, `value`) VALUES
	(1, 'required_add', ''),
	(2, 'required_remove', '');
/*!40000 ALTER TABLE `custumer__configpublic` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `custumer__custumer` (
  `custumer_id` int(11) NOT NULL AUTO_INCREMENT,
  `remove_key` char(50) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '0',
  `removed` tinyint(1) NOT NULL DEFAULT '0',
  `company` varchar(150) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `comercial_name` varchar(250) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `cif` varchar(10) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `job` varchar(150) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `name` varchar(50) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `is_invoice` tinyint(1) NOT NULL DEFAULT '0',
  `surname1` varchar(50) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `surname2` varchar(50) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `vat` varchar(50) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `vat_type` enum('D','P','C','I','N','X') COLLATE utf8_spanish2_ci DEFAULT 'D',
  `vat_date` date NOT NULL DEFAULT '0000-00-00',
  `phone1` varchar(50) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `phone2` varchar(50) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `phone3` varchar(50) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `phone4` varchar(50) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `adress` text COLLATE utf8_spanish2_ci,
  `numstreet` varchar(10) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `block` varchar(10) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `flat` varchar(10) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `door` varchar(10) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `town` varchar(50) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `province` varchar(50) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `country` varchar(50) COLLATE utf8_spanish2_ci NOT NULL DEFAULT 'ES',
  `nationality` char(2) COLLATE utf8_spanish2_ci NOT NULL DEFAULT 'ES',
  `zip` varchar(20) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `bank` varchar(50) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `bic` varchar(20) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `comta` varchar(50) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `account_holder` varchar(200) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `billing_mail` varchar(200) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `billing_type` enum('bank_receipt','wire_transfer','bank_draft','cash') COLLATE utf8_spanish2_ci DEFAULT NULL,
  `billing` enum('company','citizen') COLLATE utf8_spanish2_ci DEFAULT NULL,
  `invoice_informations` text COLLATE utf8_spanish2_ci,
  `mail` varchar(50) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `website` varchar(150) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `prefered_language` char(3) COLLATE utf8_spanish2_ci NOT NULL DEFAULT 'cat',
  `observations1` text COLLATE utf8_spanish2_ci,
  `observations2` text COLLATE utf8_spanish2_ci,
  `bin` tinyint(1) NOT NULL DEFAULT '0',
  `enteredc` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `treatment` enum('','sr','sra') COLLATE utf8_spanish2_ci NOT NULL,
  `marital_status` enum('','married','divorced','separated','single','widower','commonlaw') COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `birthdate` date NOT NULL DEFAULT '0000-00-00',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `password` varchar(50) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `book_group` int(11) NOT NULL DEFAULT '0',
  `book_main` tinyint(4) NOT NULL DEFAULT '0',
  `activated` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`custumer_id`),
  KEY `removed` (`removed`),
  KEY `bin` (`bin`),
  KEY `user_id` (`user_id`),
  KEY `activated` (`activated`),
  FULLTEXT KEY `name` (`name`,`surname1`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `custumer__custumer` DISABLE KEYS */;
/*!40000 ALTER TABLE `custumer__custumer` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `custumer__custumer_file` (
  `file_id` int(11) NOT NULL AUTO_INCREMENT,
  `custumer_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `name_original` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `size` int(11) NOT NULL DEFAULT '0',
  `ordre` int(11) NOT NULL DEFAULT '0',
  `main_file` int(1) NOT NULL DEFAULT '0',
  `filecat_id` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`file_id`),
  KEY `custumer_id` (`custumer_id`),
  KEY `filecat_id` (`filecat_id`),
  KEY `main_file` (`main_file`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `custumer__custumer_file` DISABLE KEYS */;
/*!40000 ALTER TABLE `custumer__custumer_file` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `custumer__custumer_to_property` (
  `custumer_property_id` int(11) NOT NULL AUTO_INCREMENT,
  `custumer_id` int(11) NOT NULL DEFAULT '0',
  `property_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`custumer_property_id`),
  KEY `custumer_id` (`custumer_id`),
  KEY `property_id` (`property_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `custumer__custumer_to_property` DISABLE KEYS */;
/*!40000 ALTER TABLE `custumer__custumer_to_property` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `custumer__demand` (
  `demand_id` int(11) NOT NULL AUTO_INCREMENT,
  `custumer_id` int(11) NOT NULL DEFAULT '0',
  `tipus` enum('sell','rent','temp','moblat','selloption') COLLATE utf8_spanish2_ci NOT NULL DEFAULT 'sell',
  `tipus2` enum('0','new','second') COLLATE utf8_spanish2_ci NOT NULL DEFAULT '0',
  `maxprice` int(11) NOT NULL DEFAULT '0',
  `floor_space` int(6) NOT NULL DEFAULT '0',
  `land` decimal(11,2) NOT NULL DEFAULT '0.00',
  `land_units` enum('ha','m2') COLLATE utf8_spanish2_ci DEFAULT NULL,
  `country_id` int(11) NOT NULL DEFAULT '1',
  `provincia_id` int(11) NOT NULL DEFAULT '0',
  `comarca_id` int(11) NOT NULL DEFAULT '0',
  `description` text COLLATE utf8_spanish2_ci NOT NULL,
  `price` int(11) NOT NULL DEFAULT '0',
  `efficiency` enum('progress','exempt','a','b','c','d','e','f','g') COLLATE utf8_spanish2_ci NOT NULL DEFAULT 'progress',
  `parket` tinyint(1) NOT NULL,
  `laminated` tinyint(1) NOT NULL,
  `gres` tinyint(1) NOT NULL,
  `marble` tinyint(1) NOT NULL,
  `ceramic` tinyint(1) NOT NULL,
  `estrato` enum('e1','e2','e3','e4','e5','e6') COLLATE utf8_spanish2_ci NOT NULL,
  `entered` date NOT NULL DEFAULT '0000-00-00',
  `room` tinyint(2) NOT NULL DEFAULT '0',
  `bathroom` tinyint(2) NOT NULL DEFAULT '0',
  `wc` tinyint(2) NOT NULL DEFAULT '0',
  `living` tinyint(2) NOT NULL,
  `dinning` tinyint(2) NOT NULL,
  `furniture` tinyint(1) NOT NULL DEFAULT '0',
  `laundry` tinyint(1) NOT NULL DEFAULT '0',
  `parking` tinyint(1) NOT NULL DEFAULT '0',
  `garage` tinyint(1) NOT NULL DEFAULT '0',
  `storage` tinyint(1) NOT NULL DEFAULT '0',
  `storage_area` tinyint(3) NOT NULL DEFAULT '0',
  `balcony` tinyint(1) NOT NULL DEFAULT '0',
  `terrace` tinyint(1) NOT NULL DEFAULT '0',
  `terrace_area` tinyint(3) NOT NULL DEFAULT '0',
  `office` tinyint(1) NOT NULL DEFAULT '0',
  `elevator` tinyint(1) NOT NULL DEFAULT '0',
  `garden` tinyint(1) NOT NULL DEFAULT '0',
  `garden_area` tinyint(5) NOT NULL DEFAULT '0',
  `garage_area` tinyint(3) NOT NULL DEFAULT '0',
  `swimmingpool` tinyint(1) NOT NULL DEFAULT '0',
  `citygas` tinyint(1) NOT NULL DEFAULT '0',
  `centralheating` tinyint(1) NOT NULL DEFAULT '0',
  `airconditioned` tinyint(1) NOT NULL DEFAULT '0',
  `facing` enum('','n','ne','e','se','s','so','o','no') COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `level_count` int(11) NOT NULL DEFAULT '0',
  `service_room` tinyint(1) NOT NULL,
  `service_bath` tinyint(1) NOT NULL,
  `study` tinyint(1) NOT NULL,
  `courtyard` tinyint(1) NOT NULL,
  `courtyard_area` int(5) NOT NULL,
  `sea_view` tinyint(1) NOT NULL DEFAULT '0',
  `clear_view` tinyint(1) NOT NULL DEFAULT '0',
  `observations` text COLLATE utf8_spanish2_ci NOT NULL,
  `xxmunicipi_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`demand_id`),
  KEY `custumer_id` (`custumer_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `custumer__demand` DISABLE KEYS */;
/*!40000 ALTER TABLE `custumer__demand` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `custumer__demand_to_category` (
  `demand_category_id` int(11) NOT NULL AUTO_INCREMENT,
  `demand_id` int(11) DEFAULT '0',
  `category_id` int(11) DEFAULT '0',
  PRIMARY KEY (`demand_category_id`),
  KEY `demand_id` (`demand_id`),
  KEY `category_id` (`category_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `custumer__demand_to_category` DISABLE KEYS */;
/*!40000 ALTER TABLE `custumer__demand_to_category` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `custumer__demand_to_municipi` (
  `demand_municipi_id` int(11) NOT NULL AUTO_INCREMENT,
  `demand_id` int(11) DEFAULT '0',
  `municipi_id` int(11) DEFAULT '0',
  `comarca_id` int(11) DEFAULT '0',
  PRIMARY KEY (`demand_municipi_id`),
  KEY `demand_id` (`demand_id`),
  KEY `municipi_id` (`municipi_id`),
  KEY `comarca_id` (`comarca_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `custumer__demand_to_municipi` DISABLE KEYS */;
/*!40000 ALTER TABLE `custumer__demand_to_municipi` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `custumer__demand_to_property` (
  `demand_property_id` int(11) NOT NULL AUTO_INCREMENT,
  `demand_id` int(11) DEFAULT '0',
  `property_id` int(11) DEFAULT '0',
  `client_id` int(11) DEFAULT '0',
  `liked` tinyint(1) DEFAULT '0',
  `mailed` tinyint(1) DEFAULT '0',
  `refused` tinyint(1) DEFAULT '0',
  `message_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`demand_property_id`),
  KEY `demand_id` (`demand_id`),
  KEY `property_id` (`property_id`),
  KEY `message_id` (`message_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `custumer__demand_to_property` DISABLE KEYS */;
/*!40000 ALTER TABLE `custumer__demand_to_property` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `custumer__history` (
  `history_id` int(11) NOT NULL AUTO_INCREMENT,
  `custumer_id` int(11) NOT NULL DEFAULT '0',
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` enum('sort_out','finished','canceled') COLLATE utf8_spanish2_ci NOT NULL,
  `finished_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `tipus` enum('various','to_call','waiting_call','assembly','show_property','like_property','send_information','demand_refused','demand_reseted','office','offer','price_changed') COLLATE utf8_spanish2_ci NOT NULL,
  `history_title` varchar(255) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `history` text COLLATE utf8_spanish2_ci NOT NULL,
  `property_id` int(11) NOT NULL DEFAULT '0',
  `message_id` int(11) NOT NULL DEFAULT '0',
  `demand_id` int(11) NOT NULL DEFAULT '0',
  `client_id` int(11) NOT NULL DEFAULT '0',
  `bin` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`history_id`),
  KEY `custumer_id` (`custumer_id`),
  KEY `property_id` (`property_id`),
  KEY `message_id` (`message_id`),
  KEY `demand_id` (`demand_id`),
  KEY `client_id` (`client_id`),
  KEY `bin` (`bin`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `custumer__history` DISABLE KEYS */;
INSERT INTO `custumer__history` (`history_id`, `custumer_id`, `date`, `status`, `finished_date`, `tipus`, `history_title`, `history`, `property_id`, `message_id`, `demand_id`, `client_id`, `bin`) VALUES
	(1, 0, '2014-02-19 00:00:00', 'finished', '2014-02-19 00:00:00', 'price_changed', '100000', '', 1, 0, 0, 0, 0),
	(2, 0, '2014-02-19 00:00:00', 'finished', '2014-02-19 00:00:00', 'price_changed', '130000', '', 2, 0, 0, 0, 0),
	(3, 0, '2014-02-19 00:00:00', 'finished', '2014-02-19 00:00:00', 'price_changed', '50000', '', 3, 0, 0, 0, 0),
	(4, 0, '2014-02-19 00:00:00', 'finished', '2014-02-19 00:00:00', 'price_changed', '160000', '', 4, 0, 0, 0, 0),
	(5, 0, '2014-02-19 00:00:00', 'finished', '2014-02-19 00:00:00', 'price_changed', '220000', '', 5, 0, 0, 0, 0),
	(6, 0, '2014-02-19 00:00:00', 'finished', '2014-02-19 00:00:00', 'price_changed', '100000', '', 11, 0, 0, 0, 0),
	(7, 0, '2014-02-19 00:00:00', 'finished', '2014-02-19 00:00:00', 'price_changed', '130000', '', 12, 0, 0, 0, 0),
	(8, 0, '2014-02-19 00:00:00', 'finished', '2014-02-19 00:00:00', 'price_changed', '50000', '', 13, 0, 0, 0, 0),
	(9, 0, '2014-02-19 00:00:00', 'finished', '2014-02-19 00:00:00', 'price_changed', '160000', '', 14, 0, 0, 0, 0),
	(10, 0, '2014-02-19 00:00:00', 'finished', '2014-02-19 00:00:00', 'price_changed', '220000', '', 15, 0, 0, 0, 0),
	(11, 0, '2014-02-19 00:00:00', 'finished', '2014-02-19 00:00:00', 'price_changed', '100000', '', 21, 0, 0, 0, 0),
	(12, 0, '2014-02-19 00:00:00', 'finished', '2014-02-19 00:00:00', 'price_changed', '130000', '', 22, 0, 0, 0, 0),
	(13, 0, '2014-02-19 00:00:00', 'finished', '2014-02-19 00:00:00', 'price_changed', '50000', '', 23, 0, 0, 0, 0),
	(14, 0, '2014-02-19 00:00:00', 'finished', '2014-02-19 00:00:00', 'price_changed', '160000', '', 24, 0, 0, 0, 0),
	(15, 0, '2014-02-19 00:00:00', 'finished', '2014-02-19 00:00:00', 'price_changed', '220000', '', 25, 0, 0, 0, 0),
	(16, 0, '2014-02-19 00:00:00', 'finished', '2014-02-19 00:00:00', 'price_changed', '100000', '', 31, 0, 0, 0, 0),
	(17, 0, '2014-02-19 00:00:00', 'finished', '2014-02-19 00:00:00', 'price_changed', '130000', '', 32, 0, 0, 0, 0),
	(18, 0, '2014-02-19 00:00:00', 'finished', '2014-02-19 00:00:00', 'price_changed', '50000', '', 33, 0, 0, 0, 0),
	(19, 0, '2014-02-19 00:00:00', 'finished', '2014-02-19 00:00:00', 'price_changed', '160000', '', 34, 0, 0, 0, 0),
	(20, 0, '2014-02-19 00:00:00', 'finished', '2014-02-19 00:00:00', 'price_changed', '220000', '', 35, 0, 0, 0, 0),
	(21, 0, '2014-02-19 00:00:00', 'finished', '2014-02-19 00:00:00', 'price_changed', '100000', '', 41, 0, 0, 0, 0),
	(22, 0, '2014-02-19 00:00:00', 'finished', '2014-02-19 00:00:00', 'price_changed', '130000', '', 42, 0, 0, 0, 0),
	(23, 0, '2014-02-19 00:00:00', 'finished', '2014-02-19 00:00:00', 'price_changed', '50000', '', 43, 0, 0, 0, 0),
	(24, 0, '2014-02-19 00:00:00', 'finished', '2014-02-19 00:00:00', 'price_changed', '160000', '', 44, 0, 0, 0, 0),
	(25, 0, '2014-02-19 00:00:00', 'finished', '2014-02-19 00:00:00', 'price_changed', '220000', '', 45, 0, 0, 0, 0);
/*!40000 ALTER TABLE `custumer__history` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `inmo__category` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `ref_code` varchar(10) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `ordre` int(11) NOT NULL DEFAULT '0',
  `blocked` tinyint(1) NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`category_id`),
  KEY `active` (`active`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `inmo__category` DISABLE KEYS */;
INSERT INTO `inmo__category` (`category_id`, `ref_code`, `ordre`, `blocked`, `active`) VALUES
	(1, '', 0, 0, 1),
	(2, '', 0, 0, 1),
	(3, '', 0, 0, 1),
	(4, '', 0, 0, 1),
	(5, '', 0, 0, 1),
	(10, '', 0, 0, 1),
	(7, '', 0, 0, 1),
	(8, '', 0, 0, 1),
	(11, '', 0, 0, 1),
	(12, '', 0, 0, 1),
	(13, '', 0, 0, 1),
	(14, '', 0, 0, 1),
	(15, '', 0, 0, 1),
	(16, '', 0, 0, 1),
	(17, '', 0, 0, 1),
	(18, '', 0, 0, 1),
	(19, '', 0, 0, 1),
	(20, '', 0, 0, 1),
	(21, '', 0, 0, 1),
	(22, '', 0, 0, 1),
	(23, '', 0, 0, 1),
	(24, '', 0, 0, 1),
	(25, '', 0, 0, 1),
	(26, '', 0, 0, 1),
	(27, '', 0, 0, 1);
/*!40000 ALTER TABLE `inmo__category` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `inmo__category_language` (
  `category_id` int(11) NOT NULL DEFAULT '0',
  `language` char(3) COLLATE utf8_spanish2_ci NOT NULL,
  `category_original` varchar(100) COLLATE utf8_spanish2_ci NOT NULL,
  `category` varchar(100) COLLATE utf8_spanish2_ci NOT NULL,
  KEY `category_id` (`category_id`),
  KEY `language` (`language`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `inmo__category_language` DISABLE KEYS */;
INSERT INTO `inmo__category_language` (`category_id`, `language`, `category_original`, `category`) VALUES
	(1, 'cat', 'Masia - Rústica', 'Masia - Rústica'),
	(1, 'spa', 'Masía - Rústica', 'Masía - Rústica'),
	(1, 'eng', 'Country house', 'Country house'),
	(2, 'cat', 'Casa de poble', 'Casa de poble'),
	(2, 'eng', 'Rural house', 'Rural house'),
	(2, 'spa', 'Casa de pueblo', 'Casa de pueblo'),
	(3, 'cat', 'Casa urbana', 'Casa urbana'),
	(3, 'eng', 'Urban house', 'Urban house'),
	(3, 'spa', 'Casa urbana', 'Casa urbana'),
	(4, 'cat', 'Pis', 'Pis'),
	(4, 'eng', 'Flat', 'Flat'),
	(4, 'spa', 'Piso', 'Piso'),
	(10, 'eng', 'Parking lot', 'Parking lot'),
	(10, 'spa', 'Parking', 'Parking'),
	(10, 'cat', 'Pàrquing', 'Pàrquing'),
	(5, 'cat', 'Terreny', 'Terreny'),
	(5, 'eng', 'Plot', 'Plot'),
	(5, 'spa', 'Terreno', 'Terreno'),
	(7, 'cat', 'Local', 'Local'),
	(7, 'spa', 'Local', 'Local'),
	(7, 'eng', 'Premises', 'Premises'),
	(8, 'cat', 'Obra nova', 'Obra nova'),
	(8, 'spa', 'Obra nueva', 'Obra nueva'),
	(8, 'eng', 'New construction', 'New construction'),
	(11, 'cat', 'Amarratge', 'Amarratge'),
	(11, 'spa', 'Amarre', 'Amarre'),
	(11, 'eng', 'Mooring', 'Mooring'),
	(12, 'cat', 'Apartament', 'Apartament'),
	(12, 'spa', 'Apartamento', 'Apartamento'),
	(12, 'eng', 'Apartment', 'Apartment'),
	(13, 'cat', 'Xalet', 'Xalet'),
	(13, 'spa', 'Chalé', 'Chalé'),
	(13, 'eng', 'Villa', 'Villa'),
	(14, 'cat', 'Àtic', 'Àtic'),
	(14, 'spa', 'Ático', 'Ático'),
	(14, 'eng', 'Attic', 'Attic'),
	(15, 'cat', 'Dúplex', 'Dúplex'),
	(15, 'spa', 'Dúplex', 'Dúplex'),
	(15, 'eng', 'Duplex', 'Duplex'),
	(16, 'cat', 'Loft', 'Loft'),
	(16, 'spa', 'Loft', 'Loft'),
	(16, 'eng', 'Loft', 'Loft'),
	(17, 'cat', 'Nau', 'Nau'),
	(17, 'spa', 'Nave', 'Nave'),
	(17, 'eng', 'Warehouse', 'Warehouse'),
	(1, 'fra', 'Ferme', 'Ferme'),
	(2, 'fra', 'Maison de village', 'Maison de village'),
	(3, 'fra', 'Maison urbaine', 'Maison urbaine'),
	(4, 'fra', 'Appartement', 'Appartement'),
	(10, 'fra', 'Parking', 'Parking'),
	(5, 'fra', 'Terrain', 'Terrain'),
	(7, 'fra', 'Magasin', 'Magasin'),
	(8, 'fra', 'Nouvelle construction', 'Nouvelle construction'),
	(11, 'fra', 'Amarrage', 'Amarrage'),
	(12, 'fra', 'Appartement', 'Appartement'),
	(13, 'fra', 'Villa', 'Villa'),
	(14, 'fra', 'Attique', 'Attique'),
	(15, 'fra', 'Duplex', 'Duplex'),
	(16, 'fra', 'Loft', 'Loft'),
	(17, 'fra', 'Entrepôt', 'Entrepôt'),
	(1, 'deu', 'Gehöft/Landhaus', 'Gehöft/Landhaus'),
	(2, 'deu', 'Dorfhaus', 'Dorfhaus'),
	(3, 'deu', 'Stadthaus', 'Stadthaus'),
	(4, 'deu', 'Wohnung', 'Wohnung'),
	(10, 'deu', 'Parkhaus', 'Parkhaus'),
	(5, 'deu', 'Grundstück', 'Grundstück'),
	(7, 'deu', 'Lokal', 'Lokal'),
	(8, 'deu', 'Neubau', 'Neubau'),
	(11, 'deu', 'Anlegestelle ', 'Anlegestelle '),
	(12, 'deu', 'Appartement', 'Appartement'),
	(13, 'deu', 'Ferienhaus', 'Ferienhaus'),
	(14, 'deu', 'Penthouse', 'Penthouse'),
	(15, 'deu', 'Maisonette', 'Maisonette'),
	(16, 'deu', 'Loft', 'Loft'),
	(17, 'deu', 'Halle', 'Halle'),
	(18, 'cat', 'Protecció oficial', 'Protecció oficial'),
	(18, 'spa', 'Protección oficial', 'Protección oficial'),
	(18, 'eng', 'State-subsidized', 'State-subsidized'),
	(18, 'deu', 'State-subsidized', 'State-subsidized'),
	(18, 'fra', 'State-subsidized', 'State-subsidized'),
	(19, 'cat', 'Ruïna', 'Ruïna'),
	(19, 'spa', 'Ruina', 'Ruina'),
	(19, 'eng', 'Ruin', 'Ruin'),
	(19, 'deu', 'Ruine', 'Ruine'),
	(19, 'fra', 'Ruin', 'Ruin'),
	(20, 'cat', 'Negoci', 'Negoci'),
	(20, 'spa', 'Negocio', 'Negocio'),
	(20, 'eng', 'Business', 'Business'),
	(20, 'deu', 'Betrieb', 'Betrieb'),
	(20, 'fra', 'Affaire', 'Affaire'),
	(21, 'cat', 'Finca exclusiva', 'Finca exclusiva'),
	(21, 'spa', 'Finca exclusiva', 'Finca exclusiva'),
	(21, 'eng', 'Exclusive property', 'Exclusive property'),
	(21, 'deu', 'Exklusive immobilie', 'Exklusive immobilie'),
	(21, 'fra', 'Propieté exclusive', 'Propieté exclusive'),
	(22, 'cat', 'Casa adosada', 'Casa adosada'),
	(22, 'deu', 'Reihenhaus', 'Reihenhaus'),
	(22, 'eng', 'Serial house', 'Serial house'),
	(22, 'fra', 'Maison mitoyenne', 'Maison mitoyenne'),
	(22, 'spa', 'Casa adosada', 'Casa adosada'),
	(23, 'cat', 'Casa aparellada', 'Casa aparellada'),
	(23, 'deu', 'Zweifamilienhaus', 'Zweifamilienhaus'),
	(23, 'eng', 'Single family house', 'Single family house'),
	(23, 'fra', 'Maison jumelée', 'Maison jumelée'),
	(23, 'spa', 'Casa pareada', 'Casa pareada'),
	(24, 'cat', 'Casa unifamiliar', 'Casa unifamiliar'),
	(24, 'deu', '', ''),
	(24, 'eng', '', ''),
	(24, 'fra', '', ''),
	(24, 'spa', 'Casa unifamiliar', 'Casa unifamiliar'),
	(25, 'cat', 'Pis planta baixa', 'Pis planta baixa'),
	(25, 'deu', 'Apartment Erdgeschoss', 'Apartment Erdgeschoss'),
	(25, 'eng', 'Ground floor apartment ', 'Ground floor apartment '),
	(25, 'fra', 'Appartement rez-de-chaussée', 'Appartement rez-de-chaussée'),
	(25, 'spa', 'Piso planta baja', 'Piso planta baja'),
	(26, 'cat', 'Garatge', 'Garatge'),
	(26, 'deu', 'Garage', 'Garage'),
	(26, 'eng', 'Garage', 'Garage'),
	(26, 'fra', 'Garage', 'Garage'),
	(26, 'spa', 'Garaje', 'Garaje'),
	(27, 'cat', 'Edifici', 'Edifici'),
	(27, 'deu', 'Gebäude', 'Gebäude'),
	(27, 'eng', 'Building', 'Building'),
	(27, 'fra', 'Édifice', 'Édifice'),
	(27, 'spa', 'Edificio', 'Edificio');
/*!40000 ALTER TABLE `inmo__category_language` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `inmo__comarca` (
  `comarca_id` int(11) NOT NULL AUTO_INCREMENT,
  `comarca` varchar(100) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `provincia_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`comarca_id`),
  KEY `provincia_id` (`provincia_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `inmo__comarca` DISABLE KEYS */;
INSERT INTO `inmo__comarca` (`comarca_id`, `comarca`, `provincia_id`) VALUES
	(1, 'Alt Camp', 43),
	(2, 'Alt Empordà', 17),
	(3, 'Alt Penedès', 8),
	(4, 'Alt Urgell', 25),
	(5, 'Alta Ribagorça', 25),
	(6, 'Anoia', 8),
	(7, 'Bages', 8),
	(8, 'Baix Camp', 43),
	(9, 'Baix Ebre', 43),
	(10, 'Baix Empordà', 17),
	(11, 'Baix Llobregat', 8),
	(12, 'Baix Penedès', 43),
	(13, 'Barcelonès', 8),
	(14, 'Berguedà', 8),
	(15, 'Cerdanya', 17),
	(16, 'Conca de Barberà', 43),
	(17, 'Garraf', 8),
	(18, 'Garrigues', 25),
	(19, 'Garrotxa', 17),
	(20, 'Gironès', 17),
	(21, 'Maresme', 8),
	(22, 'Montsià', 43),
	(23, 'Noguera', 25),
	(24, 'Osona', 8),
	(25, 'Pallars Jussà', 25),
	(26, 'Pallars Sobirà', 25),
	(27, 'Pla d\'Urgell', 25),
	(28, 'Pla de l\'Estany', 17),
	(29, 'Priorat', 43),
	(30, 'Ribera d\'Ebre', 43),
	(31, 'Ripollès', 17),
	(32, 'Segarra', 25),
	(33, 'Segrià', 25),
	(34, 'Selva', 17),
	(35, 'Solsonès', 25),
	(36, 'Tarragonès', 43),
	(37, 'Terra Alta', 43),
	(38, 'Urgell', 25),
	(39, 'Val d\'Aran', 25),
	(40, 'Vallès Occidental', 8),
	(41, 'Vallès Oriental', 8);
/*!40000 ALTER TABLE `inmo__comarca` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `inmo__configadmin` (
  `configadmin_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `value` varchar(255) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`configadmin_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `inmo__configadmin` DISABLE KEYS */;
INSERT INTO `inmo__configadmin` (`configadmin_id`, `name`, `value`) VALUES
	(1, 'provincies', '17,8'),
	(2, 'languages', 'cat,spa,eng'),
	(3, 'property_hidden', ''),
	(4, 'units', ''),
	(5, 'im_big_w', '1944'),
	(6, 'im_big_h', '1296'),
	(7, 'im_admin_thumb_q', '80'),
	(8, 'im_details_w', '700'),
	(9, 'im_details_h', '466'),
	(10, 'im_thumb_w', '300'),
	(11, 'im_thumb_h', '200'),
	(12, 'im_admin_thumb_w', '200'),
	(13, 'im_admin_thumb_h', '133'),
	(14, 'im_thumb_q', '80'),
	(15, 'im_details_q', '80'),
	(16, 'im_list_cols', '4'),
	(17, 'property_auto_ref', '1'),
	(18, 'properties_ref_formula', '$'),
	(19, 'menu_price', '0,250000,400000,600000'),
	(20, 'im_medium_w', '960'),
	(21, 'im_medium_h', '640'),
	(22, 'im_medium_q', '80'),
	(23, 'default_provincia', '1'),
	(24, 'default_comarca', '1'),
	(25, 'default_municipi', '1'),
	(26, 'google_center_latitude', '43.51668853502906'),
	(27, 'google_center_longitude', '4.3505859375'),
	(28, 'google_zoom', '5'),
	(29, 'showwindow_size', 'a4'),
	(30, 'showwindow_pages', '1'),
	(31, 'showwindow_show_zone', '0'),
	(32, 'showwindow_show_municipi', '0'),
	(33, 'showwindow_show_comarca', '1'),
	(34, 'showwindow_show_provincia', '0'),
	(35, 'showwindow_top_fields', '16'),
	(36, 'album_size', 'a4'),
	(37, 'album_pages', '2'),
	(38, 'album_show_zone', '0'),
	(39, 'album_show_municipi', '0'),
	(40, 'album_show_comarca', '1'),
	(41, 'album_show_provincia', '0'),
	(42, 'showwindow_language', 'cat'),
	(43, 'album_language', 'cat'),
	(44, 'watermark', '0'),
	(45, 'watermark_halign', 'center'),
	(72, 'custom5_name', ''),
	(47, 'watermark_valign', 'middle'),
	(48, 'im_custom_ratios', 'Imatge Slider=2.4;'),
	(49, 'custom1_name', ''),
	(50, 'custom1_type', ''),
	(51, 'custom1_filter', ''),
	(52, 'custom2_name', ''),
	(53, 'custom2_type', ''),
	(54, 'custom2_filter', ''),
	(55, 'custom3_name', ''),
	(56, 'custom3_type', ''),
	(57, 'custom3_filter', ''),
	(58, 'custom4_name', ''),
	(59, 'custom4_type', ''),
	(60, 'custom4_filter', ''),
	(61, 'showwindow_template_id', ''),
	(62, 'album_template_id', ''),
	(63, 'is_custumer_on', '1'),
	(64, 'is_history_on', '1'),
	(65, 'default_zone', '0'),
	(66, 'default_tipus', 'sell'),
	(67, 'is_mls_on', '1'),
	(68, 'is_booking_on', '1'),
	(69, 'videoframe_info_size', ''),
	(70, 'tv_num_images', ''),
	(71, 'apicat_export_all', '0'),
	(73, 'custom5_type', ''),
	(74, 'custom5_filter', ''),
	(75, 'custom6_name', ''),
	(76, 'custom6_type', ''),
	(77, 'custom6_filter', ''),
	(78, 'custom7_name', ''),
	(79, 'custom7_type', ''),
	(80, 'custom7_filter', ''),
	(81, 'custom8_name', ''),
	(82, 'custom8_type', ''),
	(83, 'custom8_filter', ''),
	(84, 'fotocasa_export_all', '0'),
	(85, 'apicat_import_only_first_time', '0'),
	(86, 'habitaclia_export_all', '0');
/*!40000 ALTER TABLE `inmo__configadmin` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `inmo__configadmin_language` (
  `configadmin_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(50) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `value` varchar(255) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `language` char(3) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  KEY `configadmin_id` (`configadmin_id`),
  KEY `language` (`language`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `inmo__configadmin_language` DISABLE KEYS */;
INSERT INTO `inmo__configadmin_language` (`configadmin_id`, `name`, `value`, `language`) VALUES
	(0, 'custom1_name', '', 'cat'),
	(0, 'custom2_name', '', 'cat'),
	(0, 'custom3_name', '', 'cat'),
	(0, 'custom4_name', '', 'cat'),
	(0, 'custom1_name', '', 'spa'),
	(0, 'custom2_name', '', 'spa'),
	(0, 'custom3_name', '', 'spa'),
	(0, 'custom4_name', '', 'spa'),
	(0, 'custom1_name', '', 'eng'),
	(0, 'custom2_name', '', 'eng'),
	(0, 'custom3_name', '', 'eng'),
	(0, 'custom4_name', '', 'eng'),
	(0, 'custom1_name', '', 'fra'),
	(0, 'custom2_name', '', 'fra'),
	(0, 'custom3_name', '', 'fra'),
	(0, 'custom4_name', '', 'fra'),
	(0, 'custom5_name', '', 'cat'),
	(0, 'custom6_name', '', 'cat'),
	(0, 'custom7_name', '', 'cat'),
	(0, 'custom8_name', '', 'cat'),
	(0, 'custom5_name', '', 'spa'),
	(0, 'custom6_name', '', 'spa'),
	(0, 'custom7_name', '', 'spa'),
	(0, 'custom8_name', '', 'spa'),
	(0, 'custom5_name', '', 'eng'),
	(0, 'custom6_name', '', 'eng'),
	(0, 'custom7_name', '', 'eng'),
	(0, 'custom8_name', '', 'eng'),
	(0, 'custom5_name', '', 'fra'),
	(0, 'custom6_name', '', 'fra'),
	(0, 'custom7_name', '', 'fra'),
	(0, 'custom8_name', '', 'fra');
/*!40000 ALTER TABLE `inmo__configadmin_language` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `inmo__configpublic` (
  `configpublic_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `value` varchar(255) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`configpublic_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `inmo__configpublic` DISABLE KEYS */;
INSERT INTO `inmo__configpublic` (`configpublic_id`, `name`, `value`) VALUES
	(18, 'show_as_title', 'property'),
	(7, 'home_max_results', '5'),
	(8, 'menu_price', '0,250000,400000,600000'),
	(9, 'im_list_cols', '2'),
	(10, 'home_title', ''),
	(11, 'visited_max_results', '10'),
	(13, 'show_zone', '0'),
	(14, 'show_municipi', '1'),
	(15, 'show_comarca', '1'),
	(16, 'show_provincia', '1'),
	(17, 'show_country', '1'),
	(19, 'home_show_first_category', '0'),
	(20, 'home_tool_show_first_category', '1'),
	(21, 'menu_floor_space', '0,50,100,150,200'),
	(22, 'home_tool_max_results', '5'),
	(23, 'home_list_all', '0'),
	(24, 'block_max_results', '3'),
	(25, 'default_order', ''),
	(26, 'menu_price_rent', '0,400,500,600,700,800,900,1000'),
	(27, 'default_tipus', ''),
	(28, 'home_show_checked', '1'),
	(29, 'home_tool_show_checked', '1'),
	(30, 'recent_time', '30');
/*!40000 ALTER TABLE `inmo__configpublic` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `inmo__configpublic_language` (
  `configpublic_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(50) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `value` varchar(255) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `language` char(3) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  KEY `configpublic_id` (`configpublic_id`),
  KEY `language` (`language`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `inmo__configpublic_language` DISABLE KEYS */;
INSERT INTO `inmo__configpublic_language` (`configpublic_id`, `name`, `value`, `language`) VALUES
	(10, 'home_title', '', 'cat'),
	(10, 'home_title', '', 'spa'),
	(10, 'home_title', '', 'eng'),
	(10, 'home_title', '', 'fra');
/*!40000 ALTER TABLE `inmo__configpublic_language` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `inmo__country` (
  `country_id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`country_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `inmo__country` DISABLE KEYS */;
INSERT INTO `inmo__country` (`country_id`) VALUES
	(1);
/*!40000 ALTER TABLE `inmo__country` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `inmo__country_language` (
  `country_id` int(11) NOT NULL DEFAULT '0',
  `language` char(3) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '0',
  `country` varchar(150) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  KEY `country_id` (`country_id`),
  KEY `language` (`language`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `inmo__country_language` DISABLE KEYS */;
INSERT INTO `inmo__country_language` (`country_id`, `language`, `country`) VALUES
	(1, 'cat', 'Espanya'),
	(1, 'spa', 'España'),
	(1, 'eng', 'Spain'),
	(1, 'fra', 'Espagne'),
	(1, 'deu', 'Spanien'),
	(1, 'dut', 'Spanje');
/*!40000 ALTER TABLE `inmo__country_language` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `inmo__municipi` (
  `municipi_id` int(11) NOT NULL AUTO_INCREMENT,
  `municipi` varchar(100) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `comarca_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`municipi_id`),
  KEY `comarca_id` (`comarca_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `inmo__municipi` DISABLE KEYS */;
INSERT INTO `inmo__municipi` (`municipi_id`, `municipi`, `comarca_id`) VALUES
	(1, 'Bausen', 39),
	(2, 'Canejan', 39),
	(3, 'Les', 39),
	(4, 'Naut Aran', 39),
	(5, 'Bossòst', 39),
	(6, 'Vielha e Mijaran', 39),
	(7, 'Vilamòs', 39),
	(8, 'Alt Àneu', 26),
	(9, 'Arres', 39),
	(10, 'Bòrdes, es', 39),
	(11, 'Lladorre', 26),
	(12, 'Guingueta d\'Àneu, la', 26),
	(13, 'Alins', 26),
	(14, 'Vall de Cardós', 26),
	(15, 'Esterri d\'Àneu', 26),
	(16, 'Vall de Boí, la', 5),
	(17, 'Vilaller', 5),
	(18, 'Espot', 26),
	(19, 'Esterri de Cardós', 26),
	(20, 'Torre de Cabdella, la', 25),
	(21, 'Llavorsí', 26),
	(22, 'Tírvia', 26),
	(23, 'Sort', 26),
	(24, 'Rialp', 26),
	(25, 'Farrera', 26),
	(26, 'Lles de Cerdanya', 15),
	(27, 'Pont de Suert, el', 5),
	(28, 'Sarroca de Bellera', 25),
	(29, 'Meranges', 15),
	(30, 'Llívia', 15),
	(31, 'Guils de Cerdanya', 15),
	(32, 'Ger', 15),
	(33, 'Jonquera, la', 2),
	(34, 'Montferrer i Castellbò', 4),
	(35, 'Espolla', 2),
	(36, 'Rabós', 2),
	(37, 'Puigcerdà', 15),
	(38, 'Soriguera', 26),
	(39, 'Cantallops', 2),
	(40, 'Bolvir', 15),
	(41, 'Setcases', 31),
	(42, 'Agullana', 2),
	(43, 'Prullans', 15),
	(44, 'Portbou', 2),
	(45, 'Sant Climent Sescebes', 2),
	(46, 'Baix Pallars', 26),
	(47, 'Pont de Bar, el', 4),
	(48, 'Queralbs', 31),
	(49, 'Estamariu', 4),
	(50, 'Colera', 2),
	(51, 'Maçanet de Cabrenys', 2),
	(52, 'Isòvol', 15),
	(53, 'Vajol, la', 2),
	(54, 'Fontanals de Cerdanya', 15),
	(55, 'Molló', 31),
	(56, 'Darnius', 2),
	(57, 'Das', 15),
	(58, 'Capmany', 2),
	(59, 'Vilallonga de Ter', 31),
	(60, 'Llançà', 2),
	(61, 'Vilamaniscle', 2),
	(62, 'Alp', 15),
	(63, 'Valls d\'Aguilar, les', 4),
	(64, 'Albanyà', 2),
	(65, 'Llanars', 31),
	(66, 'Valls de Valira, les', 4),
	(67, 'Arsèguel', 4),
	(68, 'Camprodon', 31),
	(69, 'Montellà i Martinet', 15),
	(70, 'Garriguella', 2),
	(71, 'Alàs i Cerc', 4),
	(72, 'Mollet de Peralada', 2),
	(73, 'Toses', 31),
	(74, 'Senterada', 25),
	(75, 'Urús', 15),
	(76, 'Planoles', 31),
	(77, 'Seu d\'Urgell, la', 4),
	(78, 'Cava', 4),
	(79, 'Masarac', 2),
	(80, 'Port de la Selva, el', 2),
	(81, 'Biure', 2),
	(82, 'Peralada', 2),
	(83, 'Vilajuïga', 2),
	(84, 'Sant Llorenç de la Muga', 2),
	(85, 'Boadella d\'Empordà', 2),
	(86, 'Ribes de Freser', 31),
	(87, 'Pardines', 31),
	(88, 'Terrades', 2),
	(89, 'Pont de Molins', 2),
	(90, 'Montagut', 19),
	(91, 'Selva de Mar, la', 2),
	(92, 'Pobla de Segur, la', 25),
	(93, 'Cabanes', 2),
	(94, 'Pau', 2),
	(95, 'Palau-saverdera', 2),
	(96, 'Pedret i Marzà', 2),
	(97, 'Cadaqués', 2),
	(98, 'Llers', 2),
	(99, 'Castellar de n\'Hug', 14),
	(100, 'Campelles', 31),
	(101, 'Vansa i Fórnols, la', 4),
	(102, 'Roses', 2),
	(103, 'Cabanelles', 2),
	(104, 'Cistella', 2),
	(105, 'Gisclareny', 14),
	(106, 'Sales de Llierca', 19),
	(107, 'Conca de Dalt', 25),
	(108, 'Ogassa', 31),
	(109, 'Josa i Tuixén', 4),
	(110, 'Bagà', 14),
	(111, 'Gombrèn', 31),
	(112, 'Cabó', 4),
	(113, 'Vilabertran', 2),
	(114, 'Ribera d\'Urgellet', 4),
	(115, 'Figueres', 2),
	(116, 'Sant Pau de Segúries', 31),
	(117, 'Castelló d\'Empúries', 2),
	(118, 'Saldes', 14),
	(119, 'Vilanant', 2),
	(120, 'Fígols i Alinyà', 4),
	(121, 'Salàs de Pallars', 25),
	(122, 'Avinyonet de Puigventós', 2),
	(123, 'Campdevànol', 31),
	(124, 'Lladó', 2),
	(125, 'Sant Joan de les Abadesses', 31),
	(126, 'Vilafant', 2),
	(127, 'Vila-sacra', 2),
	(128, 'Beuda', 19),
	(129, 'Tortellà', 19),
	(130, 'Far d\'Empordà, el', 2),
	(131, 'Pobla de Lillet, la', 14),
	(132, 'Gósol', 14),
	(133, 'Fortià', 2),
	(134, 'Organyà', 4),
	(135, 'Navata', 2),
	(136, 'Vallcebre', 14),
	(137, 'Abella de la Conca', 25),
	(138, 'Ripoll', 31),
	(139, 'Sant Julià de Cerdanyola', 14),
	(140, 'Riumors', 2),
	(141, 'Santa Llogaia d\'Àlguema', 2),
	(142, 'Vilamalla', 2),
	(143, 'Borrassà', 2),
	(144, 'Maià de Montcal', 19),
	(145, 'Coll de Nargó', 4),
	(146, 'Sant Joan les Fonts', 19),
	(147, 'Ordis', 2),
	(148, 'Talarn', 25),
	(149, 'Siurana', 2),
	(150, 'Sant Ferriol', 19),
	(151, 'Sant Jaume de Llierca', 19),
	(152, 'Argelaguer', 19),
	(153, 'Sant Jaume de Frontanyà', 14),
	(154, 'Castellfollit de la Roca', 19),
	(155, 'Sant Pere Pescador', 2),
	(156, 'Coma i la Pedra, la', 35),
	(157, 'Riudaura', 19),
	(158, 'Torroella de Fluvià', 2),
	(159, 'Nou de Berguedà, la', 14),
	(160, 'Garrigàs', 2),
	(161, 'Vilamacolum', 2),
	(162, 'Crespià', 28),
	(163, 'Vallfogona de Ripollès', 31),
	(164, 'Besalú', 19),
	(165, 'Castell de l\'Areny', 14),
	(166, 'Vall de Bianya, la', 19),
	(167, 'Olot', 19),
	(168, 'Pontós', 2),
	(169, 'Cercs', 14),
	(170, 'Vilademuls', 28),
	(171, 'Palau de Santa Eulàlia', 2),
	(172, 'Fígols', 14),
	(173, 'Santa Pau', 19),
	(174, 'Odèn', 35),
	(175, 'Guixers', 35),
	(176, 'Esponellà', 28),
	(177, 'Serinyà', 28),
	(178, 'Vall d\'en Bas, la', 19),
	(179, 'Sant Miquel de Fluvià', 2),
	(180, 'Armentera, l\'', 2),
	(181, 'Ventalló', 2),
	(182, 'Vidrà', 24),
	(183, 'Sant Miquel de Campmajor', 28),
	(184, 'Vilada', 14),
	(185, 'Borredà', 14),
	(186, 'Preses, les', 19),
	(187, 'Sant Mori', 2),
	(188, 'Bàscara', 2),
	(189, 'Oliana', 4),
	(190, 'Castell de Mur', 25),
	(191, 'Castellar del Riu', 14),
	(192, 'Fontcoberta', 28),
	(193, 'Porqueres', 28),
	(194, 'Gavet de la Conca', 25),
	(195, 'Tremp', 25),
	(196, 'Sant Llorenç de Morunys', 35),
	(197, 'Escala, l\'', 2),
	(198, 'Mieres', 19),
	(199, 'Vilaür', 2),
	(200, 'Sant Esteve de la Sarga', 25),
	(201, 'Alpens', 24),
	(202, 'Santa Maria de Besora', 24),
	(203, 'Saus', 2),
	(204, 'Viladamat', 2),
	(205, 'Peramola', 4),
	(206, 'Banyoles', 28),
	(207, 'Vilopriu', 10),
	(208, 'Sora', 24),
	(209, 'Baronia de Rialb, la', 23),
	(210, 'Sant Feliu de Pallerols', 19),
	(211, 'Llosses, les', 31),
	(212, 'Quar, la', 14),
	(213, 'Navès', 35),
	(214, 'Montesquiu', 24),
	(215, 'Sant Quirze de Besora', 24),
	(216, 'Berga', 14),
	(217, 'Garrigoles', 10),
	(218, 'Albons', 10),
	(219, 'Sant Aniol de Finestres', 19),
	(220, 'Lluçà', 24),
	(221, 'Capolat', 14),
	(222, 'Cornellà del Terri', 28),
	(223, 'Lladurs', 35),
	(224, 'Sant Pere de Torelló', 24),
	(225, 'Planes d\'Hostoles, les', 19),
	(226, 'Viladasens', 20),
	(227, 'Llimiana', 25),
	(228, 'Tallada d\'Empordà, la', 10),
	(229, 'Bellcaire d\'Empordà', 10),
	(230, 'Sant Agustí de Lluçanès', 24),
	(231, 'Camós', 28),
	(232, 'Olvan', 14),
	(233, 'Orís', 24),
	(234, 'Àger', 23),
	(235, 'Avià', 14),
	(236, 'Sant Jordi Desvalls', 20),
	(237, 'Colomers', 10),
	(238, 'Jafre', 10),
	(239, 'Canet d\'Adri', 20),
	(240, 'Castellar de la Ribera', 35),
	(241, 'Sant Martí de Llémena', 20),
	(242, 'Verges', 10),
	(243, 'Santa Maria de Corcó', 24),
	(244, 'Cervià de Ter', 20),
	(245, 'Sant Vicenç de Torelló', 24),
	(246, 'Foixà', 10),
	(247, 'Sant Boi de Lluçanès', 24),
	(248, 'Perafita', 24),
	(249, 'Palol de Revardit', 28),
	(250, 'Artesa de Segre', 23),
	(251, 'Rupit i Pruit', 24),
	(252, 'Sagàs', 14),
	(253, 'Sant Julià de Ramis', 20),
	(254, 'Ullà', 10),
	(255, 'Sant Martí d\'Albars', 24),
	(256, 'Torelló', 24),
	(257, 'Bordils', 20),
	(258, 'Isona i Conca Dellà', 25),
	(259, 'Flaçà', 20),
	(260, 'Susqueda', 34),
	(261, 'Espunyola, l\'', 14),
	(262, 'Sant Joan de Mollet', 20),
	(263, 'Santa Maria de Merlès', 14),
	(264, 'Olius', 35),
	(265, 'Masies de Voltregà, les', 24),
	(266, 'Casserres', 14),
	(267, 'Montclar', 14),
	(268, 'Ultramort', 10),
	(269, 'Vilanova de Meià', 23),
	(270, 'Amer', 34),
	(271, 'Serra de Daró', 10),
	(272, 'Gironella', 14),
	(273, 'Celrà', 20),
	(274, 'Torroella de Montgrí', 10),
	(275, 'Camarasa', 23),
	(276, 'Sobremunt', 24),
	(277, 'Sant Gregori', 20),
	(278, 'Sant Martí Vell', 20),
	(279, 'Fontanilles', 10),
	(280, 'Gualta', 10),
	(281, 'Montmajor', 14),
	(282, 'Tavertet', 24),
	(283, 'Juià', 20),
	(284, 'Pera, la', 10),
	(285, 'Manlleu', 24),
	(286, 'Parlavà', 10),
	(287, 'Solsona', 35),
	(288, 'Olost', 24),
	(289, 'Girona', 20),
	(290, 'Rupià', 10),
	(291, 'Prats de Lluçanès', 24),
	(292, 'Sarrià de Ter', 20),
	(293, 'Sant Hipòlit de Voltregà', 24),
	(294, 'Ullastret', 10),
	(295, 'Os de Balaguer', 23),
	(296, 'Santa Cecília de Voltregà', 24),
	(297, 'Corçà', 10),
	(298, 'Sant Bartomeu del Grau', 24),
	(299, 'Puig-reig', 14),
	(300, 'Pals', 10),
	(301, 'Tiurana', 23),
	(302, 'Viver i Serrateix', 14),
	(303, 'Pinell de Solsonès', 35),
	(304, 'Madremanya', 20),
	(305, 'Masies de Roda, les', 24),
	(306, 'Palau-sator', 10),
	(307, 'Bassella', 4),
	(308, 'Osor', 34),
	(309, 'Gurb', 24),
	(310, 'Clariana de Cardener', 35),
	(311, 'Oristà', 24),
	(312, 'Avellanes i Santa Linya, les', 23),
	(313, 'Sant Julià del Llor i Bonmatí', 34),
	(314, 'Alòs de Balaguer', 23),
	(315, 'Vilanova de Sau', 24),
	(316, 'Roda de Ter', 24),
	(317, 'Forallac', 10),
	(318, 'Bisbal d\'Empordà, la', 10),
	(319, 'Cruïlles, Monells i Sant Sadurní de l\'Heura', 10),
	(320, 'Cellera de Ter, la', 34),
	(321, 'Salt', 20),
	(322, 'Quart', 20),
	(323, 'Tavèrnoles', 24),
	(324, 'Sant Hilari Sacalm', 34),
	(325, 'Bescanó', 20),
	(326, 'Begur', 10),
	(327, 'Muntanyola', 24),
	(328, 'Sant Feliu Sasserra', 7),
	(329, 'Riner', 35),
	(330, 'Llobera', 35),
	(331, 'Gaià', 7),
	(332, 'Vilanova de l\'Aguda', 23),
	(333, 'Cardona', 7),
	(334, 'Torrent', 10),
	(335, 'Ponts', 23),
	(336, 'Vilablareix', 20),
	(337, 'Anglès', 34),
	(338, 'Regencós', 10),
	(339, 'Oliola', 23),
	(340, 'Aiguaviva', 20),
	(341, 'Brunyola', 34),
	(342, 'Fornells de la Selva', 20),
	(343, 'Palafrugell', 10),
	(344, 'Llambilles', 20),
	(345, 'Sant Sadurní d\'Osormort', 24),
	(346, 'Biosca', 32),
	(347, 'Navàs', 7),
	(348, 'Santa Eulàlia de Riuprimer', 24),
	(349, 'Ivars de Noguera', 23),
	(350, 'Avinyó', 7),
	(351, 'Vilobí d\'Onyar', 34),
	(352, 'Foradada', 23),
	(353, 'Pinós', 35),
	(354, 'Mont-ras', 10),
	(355, 'Campllong', 20),
	(356, 'Santa Maria d\'Oló', 7),
	(357, 'Torà', 32),
	(358, 'Santa Coloma de Farners', 34),
	(359, 'Malla', 24),
	(360, 'Riudellots de la Selva', 34),
	(361, 'Sanaüja', 32),
	(362, 'Sallent', 7),
	(363, 'Cassà de la Selva', 20),
	(364, 'Algerri', 23),
	(365, 'Balsareny', 7),
	(366, 'Taradell', 24),
	(367, 'Calonge', 10),
	(368, 'Vall-llobrega', 10),
	(369, 'Espinelves', 24),
	(370, 'Estany, l\'', 7),
	(371, 'Cubells', 23),
	(372, 'Viladrau', 24),
	(373, 'Palamós', 10),
	(374, 'Sant Andreu Salou', 20),
	(375, 'Cabanabona', 23),
	(376, 'Santa Cristina d\'Aro', 10),
	(377, 'Caldes de Malavella', 34),
	(378, 'Castellnou de Bages', 7),
	(379, 'Alfarràs', 33),
	(380, 'Sant Mateu de Bages', 7),
	(381, 'Tona', 24),
	(382, 'Agramunt', 38),
	(383, 'Castelló de Farfanya', 23),
	(384, 'Arbúcies', 34),
	(385, 'Súria', 7),
	(386, 'Torrefeta i Florejacs', 32),
	(387, 'Sils', 34),
	(388, 'Moià', 7),
	(389, 'Llagostera', 20),
	(390, 'Collsuspina', 24),
	(391, 'Preixens', 23),
	(392, 'Montgai', 23),
	(393, 'Riudarenes', 34),
	(394, 'Sentiu de Sió, la', 23),
	(395, 'Balaguer', 23),
	(396, 'Almenar', 33),
	(397, 'Castell - Platja d\'Aro', 10),
	(398, 'Balenyà', 24),
	(399, 'Massoteres', 32),
	(400, 'Brull, el', 24),
	(401, 'Callús', 7),
	(402, 'Sant Feliu de Buixalleu', 34),
	(403, 'Guissona', 32),
	(404, 'Seva', 24),
	(405, 'Molsosa, la', 35),
	(406, 'Artés', 7),
	(407, 'Vallfogona de Balaguer', 23),
	(408, 'Albesa', 23),
	(409, 'Santpedor', 7),
	(410, 'Ivorra', 32),
	(411, 'Centelles', 24),
	(412, 'Castellfollit de Riubregós', 6),
	(413, 'Montseny', 41),
	(414, 'Puigverd d\'Agramunt', 38),
	(415, 'Plans de Sió, els', 32),
	(416, 'Fonollosa', 7),
	(417, 'Bellcaire d\'Urgell', 23),
	(418, 'Vidreres', 34),
	(419, 'Calders', 7),
	(420, 'Aiguafreda', 41),
	(421, 'Castellcir', 41),
	(422, 'Massanes', 34),
	(423, 'Ossó de Sió', 38),
	(424, 'Sant Guim de la Plana', 32),
	(425, 'Sant Feliu de Guíxols', 10),
	(426, 'Calonge de Segarra', 6),
	(427, 'Tagamanent', 41),
	(428, 'Sant Joan de Vilatorrada', 7),
	(429, 'Riells i Viabrea', 34),
	(430, 'Bellmunt d\'Urgell', 23),
	(431, 'Maçanet de la Selva', 34),
	(432, 'Monistrol de Calders', 7),
	(433, 'Menàrguens', 23),
	(434, 'Penelles', 23),
	(435, 'Almacelles', 33),
	(436, 'Castellserà', 38),
	(437, 'Alguaire', 33),
	(438, 'Fogars de Montclús', 41),
	(439, 'Sant Pere Sallavinera', 6),
	(440, 'Sant Fruitós de Bages', 7),
	(441, 'Castellterçol', 41),
	(442, 'Portella, la', 33),
	(443, 'Sant Martí de Centelles', 24),
	(444, 'Navarcles', 7),
	(445, 'Aguilar de Segarra', 7),
	(446, 'Térmens', 23),
	(447, 'Tossa de Mar', 34),
	(448, 'Gualba', 41),
	(449, 'Lleida', 33),
	(450, 'Breda', 34),
	(451, 'Sant Ramon', 32),
	(452, 'Sant Quirze Safaja', 41),
	(453, 'Manresa', 7),
	(454, 'Lloret de Mar', 34),
	(455, 'Rajadell', 7),
	(456, 'Sant Pere de Vilamajor', 41),
	(457, 'Bellvís', 27),
	(458, 'Linyola', 27),
	(459, 'Talamanca', 7),
	(460, 'Tàrrega', 38),
	(461, 'Tarroja de Segarra', 32),
	(462, 'Estaràs', 32),
	(463, 'Hostalric', 34),
	(464, 'Granera', 41),
	(465, 'Torrelameu', 23),
	(466, 'Cervera', 32),
	(467, 'Fuliola, la', 38),
	(468, 'Fogars de la Selva', 34),
	(469, 'Vilanova de Segrià', 33),
	(470, 'Pujalt', 6),
	(471, 'Cànoves i Samalús', 41),
	(472, 'Calaf', 6),
	(473, 'Campins', 41),
	(474, 'Torrefarrera', 33),
	(475, 'Tordera', 21),
	(476, 'Corbins', 33),
	(477, 'Gimenells i el Pla de la Font', 33),
	(478, 'Tornabous', 38),
	(479, 'Prats de Rei, els', 6),
	(480, 'Pont de Vilomara i Rocafort, el', 7),
	(481, 'Figaró-Montmany', 41),
	(482, 'Sant Esteve de Palautordera', 41),
	(483, 'Vilanova de la Barca', 33),
	(484, 'Mura', 7),
	(485, 'Rosselló', 33),
	(486, 'Sant Celoni', 41),
	(487, 'Blanes', 34),
	(488, 'Benavent de Segrià', 33),
	(489, 'Ivars d\'Urgell', 27),
	(490, 'Oluges, les', 32),
	(491, 'Sant Martí Sesgueioles', 6),
	(492, 'Castellfollit del Boix', 7),
	(493, 'Garriga, la', 41),
	(494, 'Torre-serona', 33),
	(495, 'Alpicat', 33),
	(496, 'Gallifa', 40),
	(497, 'Sant Llorenç Savall', 40),
	(498, 'Sant Feliu de Codines', 41),
	(499, 'Sant Salvador de Guardiola', 7),
	(500, 'Bigues i Riells', 41),
	(501, 'Santa Maria de Palautordera', 41),
	(502, 'Barbens', 27),
	(503, 'Anglesola', 38),
	(504, 'Poal, el', 27),
	(505, 'Vila-sana', 27),
	(506, 'Ametlla del Vallès, l\'', 41),
	(507, 'Rubió', 6),
	(508, 'Vilagrassa', 38),
	(509, 'Castellgalí', 7),
	(510, 'Granyanella', 32),
	(511, 'Sant Guim de Freixenet', 32),
	(512, 'Sant Vicenç de Castellet', 7),
	(513, 'Bellpuig', 38),
	(514, 'Caldes de Montbui', 41),
	(515, 'Palafolls', 21),
	(516, 'Alcoletge', 33),
	(517, 'Castellnou de Seana', 27),
	(518, 'Palau d\'Anglesola, el', 27),
	(519, 'Bell-lloc d\'Urgell', 27),
	(520, 'Franqueses del Vallès, les', 41),
	(521, 'Copons', 6),
	(522, 'Sant Antoni de Vilamajor', 41),
	(523, 'Llinars del Vallès', 41),
	(524, 'Cardedeu', 41),
	(525, 'Santa Susanna', 21),
	(526, 'Castellbell i el Vilar', 7),
	(527, 'Òdena', 6),
	(528, 'Rellinars', 40),
	(529, 'Vallgorguina', 41),
	(530, 'Castellar del Vallès', 40),
	(531, 'Santa Eulàlia de Ronçana', 41),
	(532, 'Malgrat de Mar', 21),
	(533, 'Marganell', 7),
	(534, 'Golmés', 27),
	(535, 'Monistrol de Montserrat	', 7),
	(536, 'Sentmenat', 40),
	(537, 'Matadepera', 40),
	(538, 'Sant Iscle de Vallalta', 21),
	(539, 'Mollerussa', 27),
	(540, 'Fondarella', 27),
	(541, 'Sant Cebrià de Vallalta', 21),
	(542, 'Bruc, el', 6),
	(543, 'Vilalba Sasserra', 41),
	(544, 'Sidamon', 27),
	(545, 'Veciana', 6),
	(546, 'Vacarisses', 40),
	(547, 'Terrassa', 40),
	(548, 'Granyena de Segarra', 32),
	(549, 'Pineda de Mar', 21),
	(550, 'Vilanova de Bellpuig', 27),
	(551, 'Argençola', 6),
	(552, 'Ribera d\'Ondara', 32),
	(553, 'Montmaneu', 6),
	(554, 'Jorba', 6),
	(555, 'Arenys de Munt', 21),
	(556, 'Preixana', 38),
	(557, 'Canovelles', 41),
	(558, 'Calella', 21),
	(559, 'Verdú', 38),
	(560, 'Lliçà d\'Amunt', 41),
	(561, 'Roca del Vallès, la', 41),
	(562, 'Miralcamp', 27),
	(563, 'Montoliu de Segarra', 32),
	(564, 'Alamús, els', 33),
	(565, 'Torregrossa', 27),
	(566, 'Talavera', 32),
	(567, 'Dosrius', 21),
	(568, 'Castellolí', 6),
	(569, 'Sant Pol de Mar', 21),
	(570, 'Montornès de Segarra', 32),
	(571, 'Granollers', 41),
	(572, 'Torres de Segre', 33),
	(573, 'Belianes', 38),
	(574, 'Canet de Mar', 21),
	(575, 'Soses', 33),
	(576, 'Guimerà', 38),
	(577, 'Collbató', 11),
	(578, 'Palau de Plegamans', 40),
	(579, 'Lliçà de Vall', 41),
	(580, 'Igualada', 6),
	(581, 'Arbeca', 18),
	(582, 'Sant Martí de Riucorb', 38),
	(583, 'Albatàrrec', 33),
	(584, 'Sant Vicenç de Montalt', 21),
	(585, 'Sant Andreu de Llavaneres', 21),
	(586, 'Juneda', 18),
	(587, 'Sudanell', 33),
	(588, 'Montoliu de Lleida', 33),
	(589, 'Puiggròs', 18),
	(590, 'Artesa de Lleida', 33),
	(591, 'Esparreguera', 11),
	(592, 'Argentona', 21),
	(593, 'Arenys de Mar', 21),
	(594, 'Sant Martí de Tous', 6),
	(595, 'Santa Margarida de Montbui', 6),
	(596, 'Sabadell', 40),
	(597, 'Pobla de Claramunt, la', 6),
	(598, 'Santa Coloma de Queralt', 16),
	(599, 'Vilanova del Camí', 6),
	(600, 'Piera', 6),
	(601, 'Puigverd de Lleida', 33),
	(602, 'Ciutadilla', 38),
	(603, 'Nalec', 38),
	(604, 'Parets del Vallès', 41),
	(605, 'Alcarràs', 33),
	(606, 'Viladecavalls', 40),
	(607, 'Llorac', 16),
	(608, 'Vallfogona de Riucorb', 16),
	(609, 'Olesa de Montserrat', 11),
	(610, 'Hostalets de Pierola, els', 6),
	(611, 'Mataró', 21),
	(612, 'Polinyà', 40),
	(613, 'Vilanova del Vallès', 41),
	(614, 'Mollet del Vallès', 41),
	(615, 'Alfés', 33),
	(616, 'Montcada i Reixac', 40),
	(617, 'Montmeló', 41),
	(618, 'Caldes d\'Estrac', 21),
	(619, 'Òrrius', 21),
	(620, 'Montornès del Vallès', 41),
	(621, 'Maldà', 38),
	(622, 'Savallà del Comtat', 16),
	(623, 'Sunyer', 33),
	(624, 'Passanant', 16),
	(625, 'Santa Perpètua de Mogoda', 40),
	(626, 'Aitona', 33),
	(627, 'Vallbona d\'Anoia', 6),
	(628, 'Castelldans', 18),
	(629, 'Carme', 6),
	(630, 'Borges Blanques, les', 18),
	(631, 'Conesa', 16),
	(632, 'Vallbona de les Monges', 38),
	(633, 'Torre de Claramunt, la', 6),
	(634, 'Orpí', 6),
	(635, 'Santa Maria de Miralles', 6),
	(636, 'Bellprat', 6),
	(637, 'Capellades', 6),
	(638, 'Vilassar de Dalt', 21),
	(639, 'Rubí', 40),
	(640, 'Sant Quirze del Vallès', 40),
	(641, 'Vallromanes', 41),
	(642, 'Cabrils', 21),
	(643, 'Floresta, la', 18),
	(644, 'Ullastrell', 40),
	(645, 'Martorelles', 41),
	(646, 'Cabrera de Mar', 21),
	(647, 'Abrera', 11),
	(648, 'Piles, les', 16),
	(649, 'Aspa', 33),
	(650, 'Barberà del Vallès', 40),
	(651, 'Omellons, els', 18),
	(652, 'Masquefa', 6),
	(653, 'Forès', 16),
	(654, 'Santa Maria de Martorelles', 41),
	(655, 'Espluga Calba, l\'', 18),
	(656, 'Sant Fost de Campsentelles', 41),
	(657, 'Sarroca de Lleida', 33),
	(658, 'Omells de na Gaia, els', 38),
	(659, 'Cabrera d\'Igualada', 6),
	(660, 'Llagosta, la', 41),
	(661, 'Castellbisbal', 40),
	(662, 'Premià de Dalt', 21),
	(663, 'Pontils', 16),
	(664, 'Sant Cugat del Vallès', 40),
	(665, 'Llacuna, la', 6),
	(666, 'Badia del Vallès', 40),
	(667, 'Mediona', 3),
	(668, 'Teià', 21),
	(669, 'Seròs', 33),
	(670, 'Senan', 16),
	(671, 'Alcanó', 33),
	(672, 'Vilassar de Mar', 21),
	(673, 'Alella', 21),
	(674, 'Massalcoreig', 33),
	(675, 'Cerdanyola del Vallès', 40),
	(676, 'Sant Esteve Sesrovires', 11),
	(677, 'Ripollet', 40),
	(678, 'Rocafort de Queralt', 16),
	(679, 'Martorell', 11),
	(680, 'Sarral', 16),
	(681, 'Tiana', 21),
	(682, 'Solivella', 16),
	(683, 'Sant Llorenç d\'Hortons', 3),
	(684, 'Cogul, el', 18),
	(685, 'Premià de Mar', 21),
	(686, 'Vinaixa', 18),
	(687, 'Fulleda', 18),
	(688, 'Blancafort', 16),
	(689, 'Badalona', 13),
	(690, 'Albagés, l\'', 18),
	(691, 'Masnou, el', 21),
	(692, 'Espluga de Francolí, l\'', 16),
	(693, 'Querol', 1),
	(694, 'Albi, l\'', 18),
	(695, 'Cervià de les Garrigues', 18),
	(696, 'Montgat', 21),
	(697, 'Granyena de les Garrigues', 18),
	(698, 'Sant Quintí de Mediona', 3),
	(699, 'Sant Pere de Riudebitlles', 3),
	(700, 'Torrebesses', 33),
	(701, 'Santa Coloma de Gramenet', 13),
	(702, 'Torrelavit', 3),
	(703, 'Sant Sadurní d\'Anoia', 3),
	(704, 'Llardecans', 33),
	(705, 'Tarrés', 18),
	(706, 'Castellví de Rosanes', 11),
	(707, 'Granja d\'Escarp, la', 33),
	(708, 'Gelida', 3),
	(709, 'Sant Andreu de la Barca', 11),
	(710, 'Soleràs, el', 18),
	(711, 'Juncosa', 18),
	(712, 'Papiol, el', 11),
	(713, 'Font-rubí', 3),
	(714, 'Pont d\'Armentera, el', 1),
	(715, 'Barberà de la Conca', 16),
	(716, 'Torrelles de Foix', 3),
	(717, 'Vimbodí', 16),
	(718, 'Pobla de Cérvoles, la', 18),
	(719, 'Pira', 16),
	(720, 'Pontons', 3),
	(721, 'Molins de Rei', 11),
	(722, 'Pallejà', 11),
	(723, 'Granadella, la', 18),
	(724, 'Subirats', 3),
	(725, 'Pla del Penedès, el', 3),
	(726, 'Montblanc', 16),
	(727, 'Cabra del Camp', 1),
	(728, 'Barcelona', 13),
	(729, 'Torms, els', 18),
	(730, 'Sant Adrià de Besòs', 13),
	(731, 'Vilosell, el', 18),
	(732, 'Sant Feliu de Llobregat', 11),
	(733, 'Sant Martí Sarroca', 3),
	(734, 'Maials', 33),
	(735, 'Puigdàlber', 3),
	(736, 'Aiguamúrcia', 1),
	(737, 'Vilobí del Penedès', 3),
	(738, 'Sant Just Desvern', 11),
	(739, 'Vallclara', 16),
	(740, 'Vallirana', 11),
	(741, 'Santa Fe del Penedès', 3),
	(742, 'Avinyonet del Penedès', 3),
	(743, 'Esplugues de Llobregat', 11),
	(744, 'Figuerola del Camp', 1),
	(745, 'Pla de Santa Maria, el', 1),
	(746, 'Montmell, el', 12),
	(747, 'Granada, la', 3),
	(748, 'Olesa de Bonesvalls', 3),
	(749, 'Bellaguarda', 18),
	(750, 'Almatret', 33),
	(751, 'Santa Coloma de Cervelló', 11),
	(752, 'Cabanyes, les', 3),
	(753, 'Hospitalet de Llobregat, l\'', 13),
	(754, 'Ulldemolins', 29),
	(755, 'Sant Joan Despí', 11),
	(756, 'Torrelles de Llobregat', 11),
	(757, 'Pacs del Penedès', 3),
	(758, 'Vilafranca del Penedès', 3),
	(759, 'Sant Cugat Sesgarrigues', 3),
	(760, 'Vilanova de Prades', 16),
	(761, 'Cornellà de Llobregat', 11),
	(762, 'Vilaverd', 16),
	(763, 'Castellví de la Marca', 3),
	(764, 'Prades', 8),
	(765, 'Olèrdola', 3),
	(766, 'Sant Boi de Llobregat', 11),
	(767, 'Bovera', 18),
	(768, 'Riba-roja d\'Ebre', 30),
	(769, 'Sant Climent de Llobregat', 11),
	(770, 'Begues', 11),
	(771, 'Valls', 1),
	(772, 'Margalef', 29),
	(773, 'Santa Margarida i els Monjos', 3),
	(774, 'Flix', 30),
	(775, 'Sant Jaume dels Domenys', 12),
	(776, 'Prat de Llobregat, el', 11),
	(777, 'Cornudella de Montsant', 29),
	(778, 'Vila-rodona', 1),
	(779, 'Mont-ral', 1),
	(780, 'Viladecans', 11),
	(781, 'Capafonts', 8),
	(782, 'Riba, la', 1),
	(783, 'Olivella', 17),
	(784, 'Morera de Montsant, la', 29),
	(785, 'Bisbal de Falset, la', 29),
	(786, 'Gavà', 11),
	(787, 'Palma d\'Ebre, la', 30),
	(788, 'Bisbal del Penedès, la', 12),
	(789, 'Alió', 1),
	(790, 'Castellet i la Gornal', 3),
	(791, 'Arboç, l\'', 12),
	(792, 'Alcover', 1),
	(793, 'Febró, la', 8),
	(794, 'Canyelles', 17),
	(795, 'Llorenç del Penedès', 12),
	(796, 'Puigpelat', 1),
	(797, 'Rodonyà', 1),
	(798, 'Sant Pere de Ribes', 17),
	(799, 'Bràfim', 1),
	(800, 'Castelldefels', 11),
	(801, 'Banyeres del Penedès', 12),
	(802, 'Montferri', 1),
	(803, 'Sitges', 17),
	(804, 'Cabacés', 29),
	(805, 'Vilaplana', 8),
	(806, 'Vallmoll', 1),
	(807, 'Masllorenç', 12),
	(808, 'Vilabella', 1),
	(809, 'Nulles', 1),
	(810, 'Santa Oliva', 12),
	(811, 'Albiol, l\'', 8),
	(812, 'Arbolí', 8),
	(813, 'Vilanova i la Geltrú', 17),
	(814, 'Albinyana', 12),
	(815, 'Milà, el', 1),
	(816, 'Poboleda', 29),
	(817, 'Vilella Alta, la', 29),
	(818, 'Vinebre', 30),
	(819, 'Bellvei', 12),
	(820, 'Torroja del Priorat', 29),
	(821, 'Pobla de Massaluca, la', 37),
	(822, 'Salomó', 36),
	(823, 'Masó, la', 1),
	(824, 'Bonastre', 12),
	(825, 'Vilella Baixa, la', 29),
	(826, 'Alforja', 8),
	(827, 'Fatarella, la', 37),
	(828, 'Aleixar, l\'', 8),
	(829, 'Selva del Camp, la', 8),
	(830, 'Torre de l\'Espanyol, la', 30),
	(831, 'Cubelles', 17),
	(832, 'Figuera, la', 29),
	(833, 'Renau', 36),
	(834, 'Vendrell, el', 12),
	(835, 'Secuita, la', 36),
	(836, 'Vilallonga del Camp', 36),
	(837, 'Vilalba dels Arcs', 37),
	(838, 'Calafell', 12),
	(839, 'Cunit', 12),
	(840, 'Rourell, el', 1),
	(841, 'Perafort', 36),
	(842, 'Ascó', 30),
	(843, 'Gratallops', 29),
	(844, 'Vespella de Gaià', 36),
	(845, 'Garidells, els', 1),
	(846, 'Lloar, el', 29),
	(847, 'Porrera', 29),
	(848, 'Almoster', 8),
	(849, 'Pobla de Montornès, la', 36),
	(850, 'Creixell', 36),
	(851, 'Castellvell del Camp', 8),
	(852, 'Roda de Barà', 36),
	(853, 'Maspujols', 8),
	(854, 'Borges del Camp, les', 8),
	(855, 'Morell, el', 36),
	(856, 'Nou de Gaià, la', 36),
	(857, 'Molar, el', 29),
	(858, 'Riudecols', 8),
	(859, 'Falset', 29),
	(860, 'Pobla de Mafumet, la', 36),
	(861, 'Garcia', 30),
	(862, 'Constantí', 36),
	(863, 'Reus', 8),
	(864, 'Duesaigües', 8),
	(865, 'Pradell de la Teixeta', 29),
	(866, 'Batea', 37),
	(867, 'Botarell', 8),
	(868, 'Catllar, el', 36),
	(869, 'Pallaresos, els', 36),
	(870, 'Bellmunt del Priorat', 29),
	(871, 'Riudoms', 8),
	(872, 'Riera de Gaià, la', 36),
	(873, 'Altafulla', 36),
	(874, 'Tarragona', 36),
	(875, 'Torredembarra', 36),
	(876, 'Masroig, el', 29),
	(877, 'Riudecanyes', 8),
	(878, 'Argentera, l\'', 8),
	(879, 'Corbera d\'Ebre', 37),
	(880, 'Marçà', 29),
	(881, 'Móra d\'Ebre', 30),
	(882, 'Montbrió del Camp', 8),
	(883, 'Torre de Fontaubella, la', 29),
	(884, 'Vilanova d\'Escornalbou', 8),
	(885, 'Vila-seca', 36),
	(886, 'Guiamets, els', 29),
	(887, 'Vinyols i els Arcs', 8),
	(888, 'Capçanes', 29),
	(889, 'Móra la Nova', 30),
	(890, 'Gandesa', 37),
	(891, 'Colldejou', 8),
	(892, 'Mont-roig del Camp', 8),
	(893, 'Cambrils', 8),
	(894, 'Tivissa', 30),
	(895, 'Benissanet', 30),
	(896, 'Salou', 36),
	(897, 'Caseres', 37),
	(898, 'Pratdip', 8),
	(899, 'Miravet', 30),
	(900, 'Pinell de Brai, el', 37),
	(901, 'Vandellòs i l\'Hospitalet de l\'Infant', 8),
	(902, 'Bot', 37),
	(903, 'Ginestar', 30),
	(904, 'Horta de Sant Joan', 37),
	(905, 'Rasquera', 30),
	(906, 'Prat de Comte', 37),
	(907, 'Benifallet', 9),
	(908, 'Paüls', 9),
	(909, 'Xerta', 9),
	(910, 'Tivenys', 9),
	(911, 'Arnes', 37),
	(912, 'Perelló, el', 9),
	(913, 'Ametlla de Mar, l\'', 9),
	(914, 'Alfara de Carles', 9),
	(915, 'Aldover', 9),
	(916, 'Roquetes', 9),
	(917, 'Tortosa', 9),
	(918, 'Ampolla, l\'', 9),
	(919, 'Camarles', 9),
	(920, 'Aldea, l\'', 9),
	(921, 'Deltebre', 9),
	(922, 'Sénia, la', 22),
	(923, 'Mas de Barberans', 22),
	(924, 'Amposta', 22),
	(925, 'Santa Bàrbara', 22),
	(926, 'Masdenverge', 22),
	(927, 'Galera, la', 22),
	(928, 'Sant Jaume d\'Enveja', 22),
	(929, 'Godall', 22),
	(930, 'Ulldecona', 22),
	(931, 'Freginals', 22),
	(932, 'Sant Carles de la Ràpita', 22),
	(933, 'Alcanar', 22),
	(934, 'Bellver de Cerdanya', 15),
	(935, 'Prats i Sansor', 15),
	(936, 'Guardiola de Berguedà', 14),
	(937, 'Folgueroles', 24),
	(938, 'Vic', 24),
	(939, 'Sant Julià de Vilatorta', 24),
	(940, 'Santa Eugènia de Berga', 24),
	(941, 'Corbera de Llobregat', 11),
	(942, 'Cervelló', 11),
	(943, 'Sant Vicenç dels Horts', 11),
	(944, 'Calldetenes', 24),
	(945, 'Riu de Cerdanya', 15),
	(946, 'Palma de Cervelló, la', 11),
	(2100, 'Dénia', 210),
	(2110, 'Eivissa (Ibiza)', 211),
	(1000, 'Empuriabrava', 2),
	(1001, 'Estartit, l\'', 10),
	(1002, 'Molina, la', 15),
	(1003, 'Platja d\'Aro', 10),
	(1004, 'Sant Antoni de Calonge', 10),
	(1005, 'Vilafreser', 28),
	(1006, 'Fonolleres', 10),
	(1007, 'Agaró, s\'', 10),
	(1008, 'Tamariu', 10),
	(1009, 'Esclanyà', 10),
	(1010, 'Llafranc', 10),
	(1011, 'Calella de Palafrugell', 10),
	(1012, 'Santa Margarida	', 2),
	(1013, 'Terradelles', 28),
	(1014, 'Llofriu', 10);
/*!40000 ALTER TABLE `inmo__municipi` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `inmo__property` (
  `property_id` int(11) NOT NULL AUTO_INCREMENT,
  `ref` int(11) NOT NULL,
  `ref_number` int(11) NOT NULL DEFAULT '0',
  `ref_collaborator` varchar(50) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `ref_cadastre` varchar(20) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `cedula` varchar(20) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `property_private` varchar(100) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `adress` varchar(255) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `place_id` int(11) NOT NULL DEFAULT '0',
  `numstreet` varchar(10) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `block` varchar(10) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `flat` varchar(10) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `door` varchar(10) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `zip` varchar(20) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `numregister` varchar(10) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `locationregister` int(11) NOT NULL DEFAULT '0',
  `tomregister` varchar(10) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `bookregister` varchar(10) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `pageregister` varchar(10) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `numberpropertyregister` varchar(10) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `inscriptionregister` varchar(10) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `descriptionregister` text COLLATE utf8_spanish2_ci NOT NULL,
  `construction_date` date NOT NULL DEFAULT '0000-00-00',
  `observations` text COLLATE utf8_spanish2_ci NOT NULL,
  `exclusive` tinyint(1) NOT NULL DEFAULT '0',
  `tipus` enum('sell','rent','temp','moblat','selloption') COLLATE utf8_spanish2_ci NOT NULL DEFAULT 'sell',
  `tipus2` enum('new','second') COLLATE utf8_spanish2_ci NOT NULL DEFAULT 'new',
  `category_id` int(11) NOT NULL DEFAULT '0',
  `floor_space` int(6) NOT NULL DEFAULT '0',
  `floor_space_build` int(6) NOT NULL DEFAULT '0',
  `land` decimal(11,2) NOT NULL DEFAULT '0.00',
  `land_units` enum('ha','m2') COLLATE utf8_spanish2_ci DEFAULT NULL,
  `country_id` int(11) NOT NULL DEFAULT '1',
  `provincia_id` int(11) NOT NULL DEFAULT '0',
  `comarca_id` int(11) NOT NULL DEFAULT '0',
  `municipi_id` int(11) NOT NULL DEFAULT '0',
  `zone_id` int(11) NOT NULL DEFAULT '0',
  `price` int(11) NOT NULL DEFAULT '0',
  `price_consult` tinyint(1) NOT NULL DEFAULT '0',
  `price_private` int(11) NOT NULL DEFAULT '0',
  `price_tmp` decimal(8,2) NOT NULL DEFAULT '0.00',
  `owner_id` int(11) NOT NULL DEFAULT '0',
  `entered` date NOT NULL DEFAULT '0000-00-00',
  `modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `bin` tinyint(1) NOT NULL DEFAULT '0',
  `facebook_post_id` varchar(255) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `facebook_image_id` varchar(255) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `facebook_publish_id` varchar(255) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `facebook_letnd_user_id` varchar(255) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `facebook_user_id` text COLLATE utf8_spanish2_ci NOT NULL,
  `twitter_status_id` varchar(255) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `twitter_letnd_user_id` varchar(255) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `twitter_user_id` text COLLATE utf8_spanish2_ci NOT NULL,
  `status` enum('prepare','review','onsale','reserved','sold','archived') COLLATE utf8_spanish2_ci NOT NULL DEFAULT 'prepare',
  `expired` date NOT NULL DEFAULT '0000-00-00',
  `efficiency` enum('progress','exempt','a','b','c','d','e','f','g') COLLATE utf8_spanish2_ci NOT NULL DEFAULT 'progress',
  `efficiency_number` int(11) NOT NULL DEFAULT '0',
  `efficiency2` enum('notdefined','a','b','c','d','e','f','g') COLLATE utf8_spanish2_ci NOT NULL DEFAULT 'notdefined',
  `efficiency_number2` int(11) NOT NULL DEFAULT '0',
  `efficiency_date` date NOT NULL,
  `parket` tinyint(1) NOT NULL,
  `laminated` tinyint(1) NOT NULL,
  `gres` tinyint(1) NOT NULL,
  `marble` tinyint(1) NOT NULL,
  `ceramic` tinyint(1) NOT NULL,
  `estrato` enum('e1','e2','e3','e4','e5','e6') COLLATE utf8_spanish2_ci NOT NULL,
  `home` tinyint(1) NOT NULL DEFAULT '0',
  `offer` tinyint(1) NOT NULL,
  `room` tinyint(2) NOT NULL DEFAULT '0',
  `bathroom` tinyint(2) NOT NULL DEFAULT '0',
  `wc` tinyint(2) NOT NULL DEFAULT '0',
  `living` tinyint(2) NOT NULL,
  `dinning` tinyint(2) NOT NULL,
  `furniture` tinyint(1) NOT NULL DEFAULT '0',
  `laundry` tinyint(1) NOT NULL DEFAULT '0',
  `parking` tinyint(1) NOT NULL DEFAULT '0',
  `parking_area` int(3) NOT NULL DEFAULT '0',
  `garage` tinyint(1) NOT NULL DEFAULT '0',
  `garage_area` int(3) NOT NULL DEFAULT '0',
  `storage` tinyint(1) NOT NULL DEFAULT '0',
  `storage_area` int(3) NOT NULL DEFAULT '0',
  `balcony` tinyint(1) NOT NULL DEFAULT '0',
  `terrace` tinyint(1) NOT NULL DEFAULT '0',
  `terrace_area` int(3) NOT NULL DEFAULT '0',
  `office` tinyint(1) NOT NULL DEFAULT '0',
  `office_area` int(3) NOT NULL DEFAULT '0',
  `property_height` int(5) NOT NULL DEFAULT '0',
  `elevator` tinyint(1) NOT NULL DEFAULT '0',
  `garden` tinyint(1) NOT NULL DEFAULT '0',
  `garden_area` int(5) NOT NULL DEFAULT '0',
  `swimmingpool` tinyint(1) NOT NULL DEFAULT '0',
  `swimmingpool_area` int(3) NOT NULL DEFAULT '0',
  `citygas` tinyint(1) NOT NULL DEFAULT '0',
  `centralheating` tinyint(1) NOT NULL DEFAULT '0',
  `airconditioned` tinyint(1) NOT NULL DEFAULT '0',
  `service_room` tinyint(1) NOT NULL,
  `service_bath` tinyint(1) NOT NULL,
  `study` tinyint(1) NOT NULL,
  `courtyard` tinyint(1) NOT NULL,
  `courtyard_area` int(5) NOT NULL,
  `sea_view` tinyint(1) NOT NULL DEFAULT '0',
  `clear_view` tinyint(1) NOT NULL DEFAULT '0',
  `blocked` tinyint(1) NOT NULL DEFAULT '0',
  `comission` tinyint(2) NOT NULL,
  `mls` tinyint(1) NOT NULL DEFAULT '0',
  `latitude` decimal(9,7) NOT NULL,
  `longitude` decimal(9,7) NOT NULL,
  `center_latitude` decimal(9,7) NOT NULL,
  `center_longitude` decimal(9,7) NOT NULL,
  `zoom` tinyint(2) NOT NULL,
  `show_map` tinyint(1) NOT NULL DEFAULT '0',
  `show_map_point` tinyint(1) NOT NULL DEFAULT '1',
  `custom1` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `custom2` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `custom3` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `custom4` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `custom5` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `custom6` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `custom7` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `custom8` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `person` tinyint(1) NOT NULL DEFAULT '0',
  `facing` enum('','n','ne','e','se','s','so','o','no') COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `videoframe` text COLLATE utf8_spanish2_ci NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `level_count` int(11) NOT NULL DEFAULT '0',
  `hut` varchar(30) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `time_in` time NOT NULL DEFAULT '00:00:00',
  `time_out` time NOT NULL DEFAULT '00:00:00',
  `managed_by_owner` tinyint(1) NOT NULL DEFAULT '0',
  `star` enum('0','1','2','3','4','5') COLLATE utf8_spanish2_ci NOT NULL DEFAULT '0',
  `community_expenses` int(11) NOT NULL DEFAULT '0',
  `ibi` int(11) NOT NULL DEFAULT '0',
  `municipal_tax` int(11) NOT NULL DEFAULT '0',
  `sea_distance` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`property_id`),
  KEY `place_id` (`place_id`),
  KEY `category_id` (`category_id`),
  KEY `municipi_id` (`municipi_id`),
  KEY `comarca_id` (`comarca_id`),
  KEY `provincia_id` (`provincia_id`),
  KEY `zone_id` (`zone_id`),
  KEY `bin` (`bin`),
  KEY `user_id` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `inmo__property` DISABLE KEYS */;
INSERT INTO `inmo__property` (`property_id`, `ref`, `ref_number`, `ref_collaborator`, `ref_cadastre`, `cedula`, `property_private`, `adress`, `place_id`, `numstreet`, `block`, `flat`, `door`, `zip`, `numregister`, `locationregister`, `tomregister`, `bookregister`, `pageregister`, `numberpropertyregister`, `inscriptionregister`, `descriptionregister`, `construction_date`, `observations`, `exclusive`, `tipus`, `tipus2`, `category_id`, `floor_space`, `floor_space_build`, `land`, `land_units`, `country_id`, `provincia_id`, `comarca_id`, `municipi_id`, `zone_id`, `price`, `price_consult`, `price_private`, `price_tmp`, `owner_id`, `entered`, `modified`, `bin`, `facebook_post_id`, `facebook_image_id`, `facebook_publish_id`, `facebook_letnd_user_id`, `facebook_user_id`, `twitter_status_id`, `twitter_letnd_user_id`, `twitter_user_id`, `status`, `expired`, `efficiency`, `efficiency_number`, `efficiency2`, `efficiency_number2`, `efficiency_date`, `parket`, `laminated`, `gres`, `marble`, `ceramic`, `estrato`, `home`, `offer`, `room`, `bathroom`, `wc`, `living`, `dinning`, `furniture`, `laundry`, `parking`, `parking_area`, `garage`, `garage_area`, `storage`, `storage_area`, `balcony`, `terrace`, `terrace_area`, `office`, `office_area`, `property_height`, `elevator`, `garden`, `garden_area`, `swimmingpool`, `swimmingpool_area`, `citygas`, `centralheating`, `airconditioned`, `service_room`, `service_bath`, `study`, `courtyard`, `courtyard_area`, `sea_view`, `clear_view`, `blocked`, `comission`, `mls`, `latitude`, `longitude`, `center_latitude`, `center_longitude`, `zoom`, `show_map`, `show_map_point`, `custom1`, `custom2`, `custom3`, `custom4`, `custom5`, `custom6`, `custom7`, `custom8`, `person`, `facing`, `videoframe`, `user_id`, `level_count`, `hut`, `time_in`, `time_out`, `managed_by_owner`, `star`, `community_expenses`, `ibi`, `municipal_tax`, `sea_distance`) VALUES
	(1, 1, 1, '', '', '', '', '', 0, '', '', '', '', '', '', 0, '', '', '', '', '', '', '0000-00-00', '', 0, 'sell', 'new', 12, 200, 0, 0.00, 'm2', 1, 17, 2, 188, 0, 100000, 0, 0, 0.00, 0, '2014-02-19', '2017-10-11 10:33:16', 0, '', '', '', '', '', '', '', '', 'onsale', '0000-00-00', 'b', 0, 'notdefined', 0, '0000-00-00', 0, 0, 0, 0, 0, 'e1', 0, 0, 2, 2, 0, 1, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 50, 0, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0, 0, 1, '', '', '', '', '', '', '', '', 0, '', '<iframe src="https://player.vimeo.com/video/164022180" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>', 3, 0, '', '00:00:00', '00:00:00', 0, '0', 0, 0, 0, 0),
	(2, 2, 2, '', '', '', '', '', 0, '', '', '', '', '', '', 0, '', '', '', '', '', '', '0000-00-00', '', 0, 'sell', 'new', 15, 0, 0, 120.00, 'm2', 1, 17, 2, 81, 0, 130000, 1, 0, 0.00, 0, '2014-02-19', '2017-10-11 10:33:16', 0, '', '', '', '', '', '', '', '', 'onsale', '0000-00-00', 'c', 0, 'notdefined', 0, '0000-00-00', 0, 0, 0, 0, 0, 'e1', 1, 1, 4, 2, 3, 1, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 1, 0, 1, 1, 1, 0, 0, 0, 0, 0, 50, 0, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0, 0, 1, '', '', '', '', '', '', '', '', 0, '', '<iframe src="https://player.vimeo.com/video/164022180" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>', 3, 0, '', '00:00:00', '00:00:00', 0, '0', 0, 0, 0, 0),
	(3, 3, 3, '', '', '', '', '', 0, '', '', '', '', '', '', 0, '', '', '', '', '', '', '0000-00-00', '', 0, 'sell', 'new', 12, 50, 0, 100000.00, 'ha', 1, 17, 28, 231, 0, 50000, 0, 0, 0.00, 0, '2014-02-19', '2017-10-11 10:33:16', 0, '', '', '', '', '', '', '', '', 'onsale', '0000-00-00', 'd', 0, 'notdefined', 0, '0000-00-00', 0, 0, 0, 0, 0, 'e1', 0, 0, 2, 4, 0, 0, 2, 1, 0, 1, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 50, 0, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0, 0, 1, '', '', '', '', '', '', '', '', 0, '', '<iframe src="https://player.vimeo.com/video/164022180" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>', 3, 0, '', '00:00:00', '00:00:00', 0, '0', 0, 0, 0, 0),
	(4, 4, 4, '', '', '', '', '', 0, '', '', '', '', '', '', 0, '', '', '', '', '', '', '0000-00-00', '', 0, 'sell', 'new', 12, 67, 0, 150.00, 'm2', 1, 17, 20, 355, 0, 160000, 1, 0, 0.00, 0, '2014-02-19', '2017-10-11 10:33:16', 0, '', '', '', '', '', '', '', '', 'onsale', '0000-00-00', 'b', 0, 'notdefined', 0, '0000-00-00', 0, 0, 0, 0, 0, 'e1', 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 1, 0, 1, 0, 1, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 50, 0, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0, 0, 1, '', '', '', '', '', '', '', '', 0, '', '<iframe src="https://player.vimeo.com/video/164022180" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>', 3, 0, '', '00:00:00', '00:00:00', 0, '0', 0, 0, 0, 0),
	(5, 5, 5, '', '', '', '', '', 0, '', '', '', '', '', '', 0, '', '', '', '', '', '', '0000-00-00', '', 0, 'sell', 'new', 2, 340, 0, 100.00, 'm2', 1, 17, 19, 128, 0, 220000, 0, 0, 0.00, 0, '2014-02-19', '2017-10-11 10:33:16', 0, '', '', '', '', '', '', '', '', 'onsale', '0000-00-00', 'b', 0, 'notdefined', 0, '0000-00-00', 1, 1, 1, 0, 1, 'e1', 0, 0, 2, 2, 1, 4, 2, 1, 1, 1, 0, 1, 40, 1, 7, 1, 1, 5, 0, 0, 0, 1, 1, 56, 1, 0, 1, 1, 1, 1, 1, 1, 1, 30, 0, 0, 0, 50, 0, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0, 0, 1, '', '', '', '', '', '', '', '', 0, '', '<iframe src="https://player.vimeo.com/video/164022180" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>', 3, 0, '', '00:00:00', '00:00:00', 0, '0', 0, 0, 0, 0),
	(11, 11, 11, '', '', '', '', '', 0, '', '', '', '', '', '', 0, '', '', '', '', '', '', '0000-00-00', '', 0, 'sell', 'new', 12, 200, 0, 0.00, 'm2', 1, 17, 2, 188, 0, 100000, 0, 0, 0.00, 0, '2014-02-19', '2017-10-11 10:33:16', 0, '', '', '', '', '', '', '', '', 'onsale', '0000-00-00', 'b', 0, 'notdefined', 0, '0000-00-00', 0, 0, 0, 0, 0, 'e1', 0, 0, 2, 2, 0, 1, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 50, 0, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0, 0, 1, '', '', '', '', '', '', '', '', 0, '', '<iframe src="https://player.vimeo.com/video/164022180" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>', 3, 0, '', '00:00:00', '00:00:00', 0, '0', 0, 0, 0, 0),
	(12, 12, 12, '', '', '', '', '', 0, '', '', '', '', '', '', 0, '', '', '', '', '', '', '0000-00-00', '', 0, 'sell', 'new', 15, 0, 0, 50.00, 'm2', 1, 17, 2, 81, 0, 130000, 0, 0, 0.00, 0, '2014-02-19', '2017-10-11 10:33:16', 0, '', '', '', '', '', '', '', '', 'onsale', '0000-00-00', 'c', 0, 'notdefined', 0, '0000-00-00', 0, 0, 0, 0, 0, 'e1', 1, 1, 4, 2, 3, 1, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 1, 0, 1, 1, 1, 0, 0, 0, 0, 0, 50, 0, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0, 0, 1, '', '', '', '', '', '', '', '', 0, '', '<iframe src="https://player.vimeo.com/video/164022180" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>', 3, 0, '', '00:00:00', '00:00:00', 0, '0', 0, 0, 0, 0),
	(13, 13, 13, '', '', '', '', '', 0, '', '', '', '', '', '', 0, '', '', '', '', '', '', '0000-00-00', '', 0, 'sell', 'new', 12, 50, 0, 100000.00, 'ha', 1, 17, 28, 231, 0, 50000, 0, 0, 0.00, 0, '2014-02-19', '2017-10-11 10:33:16', 0, '', '', '', '', '', '', '', '', 'onsale', '0000-00-00', 'd', 0, 'notdefined', 0, '0000-00-00', 0, 0, 0, 0, 0, 'e1', 0, 0, 2, 4, 0, 0, 2, 1, 0, 1, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 50, 0, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0, 0, 1, '', '', '', '', '', '', '', '', 0, '', '<iframe src="https://player.vimeo.com/video/164022180" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>', 3, 0, '', '00:00:00', '00:00:00', 0, '0', 0, 0, 0, 0),
	(14, 14, 14, '', '', '', '', '', 0, '', '', '', '', '', '', 0, '', '', '', '', '', '', '0000-00-00', '', 0, 'sell', 'new', 12, 67, 0, 150.00, 'm2', 1, 17, 20, 355, 0, 160000, 0, 0, 0.00, 0, '2014-02-19', '2017-10-11 10:33:16', 0, '', '', '', '', '', '', '', '', 'onsale', '0000-00-00', 'b', 0, 'notdefined', 0, '0000-00-00', 0, 0, 0, 0, 0, 'e1', 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 1, 0, 1, 0, 1, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 50, 0, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0, 0, 1, '', '', '', '', '', '', '', '', 0, '', '<iframe src="https://player.vimeo.com/video/164022180" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>', 3, 0, '', '00:00:00', '00:00:00', 0, '0', 0, 0, 0, 0),
	(15, 15, 15, '', '', '', '', '', 0, '', '', '', '', '', '', 0, '', '', '', '', '', '', '0000-00-00', '', 0, 'sell', 'new', 2, 340, 0, 0.00, 'm2', 1, 17, 19, 128, 0, 220000, 0, 0, 0.00, 0, '2014-02-19', '2017-10-11 10:33:16', 0, '', '', '', '', '', '', '', '', 'onsale', '0000-00-00', 'b', 0, 'notdefined', 0, '0000-00-00', 1, 1, 1, 0, 1, 'e1', 1, 0, 2, 2, 1, 4, 2, 1, 1, 1, 0, 1, 40, 1, 7, 1, 1, 5, 0, 0, 0, 1, 1, 56, 1, 0, 1, 1, 1, 1, 1, 1, 1, 30, 0, 0, 0, 50, 0, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0, 0, 1, '', '', '', '', '', '', '', '', 0, '', '<iframe src="https://player.vimeo.com/video/164022180" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>', 3, 0, '', '00:00:00', '00:00:00', 0, '0', 0, 0, 0, 0),
	(21, 21, 21, '', '', '', '', '', 0, '', '', '', '', '', '', 0, '', '', '', '', '', '', '0000-00-00', '', 0, 'sell', 'new', 12, 200, 0, 0.00, 'm2', 1, 17, 2, 188, 0, 100000, 0, 0, 0.00, 0, '2014-02-19', '2017-10-11 10:33:16', 0, '', '', '', '', '', '', '', '', 'onsale', '0000-00-00', 'b', 0, 'notdefined', 0, '0000-00-00', 0, 0, 0, 0, 0, 'e1', 0, 0, 2, 2, 0, 1, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 50, 0, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0, 0, 1, '', '', '', '', '', '', '', '', 0, '', '<iframe src="https://player.vimeo.com/video/164022180" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>', 3, 0, '', '00:00:00', '00:00:00', 0, '0', 0, 0, 0, 0),
	(22, 22, 22, '', '', '', '', '', 0, '', '', '', '', '', '', 0, '', '', '', '', '', '', '0000-00-00', '', 0, 'sell', 'new', 15, 0, 0, 0.00, 'm2', 1, 17, 2, 81, 0, 130000, 0, 0, 0.00, 0, '2014-02-19', '2017-10-11 10:33:16', 0, '', '', '', '', '', '', '', '', 'onsale', '0000-00-00', 'c', 0, 'notdefined', 0, '0000-00-00', 0, 0, 0, 0, 0, 'e1', 1, 1, 4, 2, 3, 1, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 1, 0, 1, 1, 1, 0, 0, 0, 0, 0, 50, 0, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0, 0, 1, '', '', '', '', '', '', '', '', 0, '', '<iframe src="https://player.vimeo.com/video/164022180" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>', 3, 0, '', '00:00:00', '00:00:00', 0, '0', 0, 0, 0, 0),
	(23, 23, 23, '', '', '', '', '', 0, '', '', '', '', '', '', 0, '', '', '', '', '', '', '0000-00-00', '', 0, 'sell', 'new', 12, 50, 0, 100000.00, 'ha', 1, 17, 28, 231, 0, 50000, 0, 0, 0.00, 0, '2014-02-19', '2017-10-11 10:33:16', 0, '', '', '', '', '', '', '', '', 'onsale', '0000-00-00', 'd', 0, 'notdefined', 0, '0000-00-00', 0, 0, 0, 0, 0, 'e1', 0, 0, 2, 4, 0, 0, 2, 1, 0, 1, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 50, 0, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0, 0, 1, '', '', '', '', '', '', '', '', 0, '', '<iframe src="https://player.vimeo.com/video/164022180" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>', 3, 0, '', '00:00:00', '00:00:00', 0, '0', 0, 0, 0, 0),
	(24, 24, 24, '', '', '', '', '', 0, '', '', '', '', '', '', 0, '', '', '', '', '', '', '0000-00-00', '', 0, 'sell', 'new', 12, 67, 0, 150.00, 'm2', 1, 17, 20, 355, 0, 160000, 0, 0, 0.00, 0, '2014-02-19', '2017-10-11 10:33:16', 0, '', '', '', '', '', '', '', '', 'sold', '0000-00-00', 'b', 0, 'notdefined', 0, '0000-00-00', 0, 0, 0, 0, 0, 'e1', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 1, 0, 1, 0, 1, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 50, 0, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0, 0, 1, '', '', '', '', '', '', '', '', 0, '', '<iframe src="https://player.vimeo.com/video/164022180" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>', 3, 0, '', '00:00:00', '00:00:00', 0, '0', 0, 0, 0, 0),
	(25, 25, 25, '', '', '', '', '', 0, '', '', '', '', '', '', 0, '', '', '', '', '', '', '0000-00-00', '', 0, 'sell', 'new', 2, 340, 0, 0.00, 'm2', 1, 17, 19, 128, 0, 220000, 0, 0, 0.00, 0, '2014-02-19', '2017-10-11 10:33:16', 0, '', '', '', '', '', '', '', '', 'onsale', '0000-00-00', 'b', 0, 'notdefined', 0, '0000-00-00', 1, 1, 1, 0, 1, 'e1', 0, 0, 2, 2, 1, 4, 2, 1, 1, 1, 0, 1, 40, 1, 7, 1, 1, 5, 0, 0, 0, 1, 1, 56, 1, 0, 1, 1, 1, 1, 1, 1, 1, 30, 0, 0, 0, 50, 0, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0, 0, 1, '', '', '', '', '', '', '', '', 0, '', '<iframe src="https://player.vimeo.com/video/164022180" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>', 3, 0, '', '00:00:00', '00:00:00', 0, '0', 0, 0, 0, 0),
	(31, 31, 31, '', '', '', '', '', 0, '', '', '', '', '', '', 0, '', '', '', '', '', '', '0000-00-00', '', 0, 'sell', 'new', 12, 200, 0, 0.00, 'm2', 1, 17, 2, 188, 0, 100000, 0, 0, 0.00, 0, '2014-02-19', '2017-10-11 10:33:16', 0, '', '', '', '', '', '', '', '', 'onsale', '0000-00-00', 'b', 0, 'notdefined', 0, '0000-00-00', 0, 0, 0, 0, 0, 'e1', 0, 0, 2, 2, 0, 1, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 50, 0, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0, 0, 1, '', '', '', '', '', '', '', '', 0, '', '<iframe src="https://player.vimeo.com/video/164022180" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>', 3, 0, '', '00:00:00', '00:00:00', 0, '0', 0, 0, 0, 0),
	(32, 32, 32, '', '', '', '', '', 0, '', '', '', '', '', '', 0, '', '', '', '', '', '', '0000-00-00', '', 0, 'sell', 'new', 15, 0, 0, 0.00, 'm2', 1, 17, 2, 81, 0, 130000, 0, 0, 0.00, 0, '2014-02-19', '2017-10-11 10:33:16', 0, '', '', '', '', '', '', '', '', 'onsale', '0000-00-00', 'c', 0, 'notdefined', 0, '0000-00-00', 0, 0, 0, 0, 0, 'e1', 0, 1, 4, 2, 3, 1, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 1, 0, 1, 1, 1, 0, 0, 0, 0, 0, 50, 0, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0, 0, 1, '', '', '', '', '', '', '', '', 0, '', '<iframe src="https://player.vimeo.com/video/164022180" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>', 3, 0, '', '00:00:00', '00:00:00', 0, '0', 0, 0, 0, 0),
	(33, 33, 33, '', '', '', '', '', 0, '', '', '', '', '', '', 0, '', '', '', '', '', '', '0000-00-00', '', 0, 'sell', 'new', 12, 50, 0, 100000.00, 'ha', 1, 17, 28, 231, 0, 50000, 0, 0, 0.00, 0, '2014-02-19', '2017-10-11 10:33:16', 0, '', '', '', '', '', '', '', '', 'onsale', '0000-00-00', 'd', 0, 'notdefined', 0, '0000-00-00', 0, 0, 0, 0, 0, 'e1', 0, 0, 2, 4, 0, 0, 2, 1, 0, 1, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 50, 0, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0, 0, 1, '', '', '', '', '', '', '', '', 0, '', '<iframe src="https://player.vimeo.com/video/164022180" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>', 3, 0, '', '00:00:00', '00:00:00', 0, '0', 0, 0, 0, 0),
	(34, 34, 34, '', '', '', '', '', 0, '', '', '', '', '', '', 0, '', '', '', '', '', '', '0000-00-00', '', 0, 'sell', 'new', 12, 67, 0, 150.00, 'm2', 1, 17, 20, 355, 0, 160000, 0, 0, 0.00, 0, '2014-02-19', '2017-10-11 10:33:16', 0, '', '', '', '', '', '', '', '', 'onsale', '0000-00-00', 'b', 0, 'notdefined', 0, '0000-00-00', 0, 0, 0, 0, 0, 'e1', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 1, 0, 1, 0, 1, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 50, 0, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0, 0, 1, '', '', '', '', '', '', '', '', 0, '', '<iframe src="https://player.vimeo.com/video/164022180" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>', 3, 0, '', '00:00:00', '00:00:00', 0, '0', 0, 0, 0, 0),
	(35, 35, 35, '', '', '', '', '', 0, '', '', '', '', '', '', 0, '', '', '', '', '', '', '0000-00-00', '', 0, 'sell', 'new', 2, 340, 0, 0.00, 'm2', 1, 17, 19, 128, 0, 220000, 0, 0, 0.00, 0, '2014-02-19', '2017-10-11 10:33:16', 0, '', '', '', '', '', '', '', '', 'onsale', '0000-00-00', 'b', 0, 'notdefined', 0, '0000-00-00', 1, 1, 1, 0, 1, 'e1', 1, 0, 2, 2, 1, 4, 2, 1, 1, 1, 0, 1, 40, 1, 7, 1, 1, 5, 0, 0, 0, 1, 1, 56, 1, 0, 1, 1, 1, 1, 1, 1, 1, 30, 0, 0, 0, 50, 0, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0, 0, 1, '', '', '', '', '', '', '', '', 0, '', '<iframe src="https://player.vimeo.com/video/164022180" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>', 3, 0, '', '00:00:00', '00:00:00', 0, '0', 0, 0, 0, 0),
	(41, 41, 41, '', '', '', '', '', 0, '', '', '', '', '', '', 0, '', '', '', '', '', '', '0000-00-00', '', 0, 'rent', 'new', 12, 200, 0, 0.00, 'm2', 1, 17, 2, 188, 0, 100000, 0, 0, 0.00, 0, '2014-02-19', '2017-10-11 10:33:16', 0, '', '', '', '', '', '', '', '', 'onsale', '0000-00-00', 'b', 0, 'notdefined', 0, '0000-00-00', 0, 0, 0, 0, 0, 'e1', 0, 0, 2, 2, 0, 1, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 50, 0, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0, 0, 1, '', '', '', '', '', '', '', '', 0, '', '<iframe src="https://player.vimeo.com/video/164022180" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>', 3, 0, '', '00:00:00', '00:00:00', 0, '0', 0, 0, 0, 0),
	(42, 42, 42, '', '', '', '', '', 0, '', '', '', '', '', '', 0, '', '', '', '', '', '', '0000-00-00', '', 0, 'rent', 'new', 15, 0, 0, 0.00, 'm2', 1, 17, 2, 81, 0, 130000, 0, 0, 0.00, 0, '2014-02-19', '2017-10-11 10:33:16', 0, '', '', '', '', '', '', '', '', 'onsale', '0000-00-00', 'c', 0, 'notdefined', 0, '0000-00-00', 0, 0, 0, 0, 0, 'e1', 1, 0, 4, 2, 3, 1, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 1, 0, 1, 1, 1, 0, 0, 0, 0, 0, 50, 0, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0, 0, 1, '', '', '', '', '', '', '', '', 0, '', '<iframe src="https://player.vimeo.com/video/164022180" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>', 3, 0, '', '00:00:00', '00:00:00', 0, '0', 0, 0, 0, 0),
	(43, 43, 43, '', '', '', '', '', 0, '', '', '', '', '', '', 0, '', '', '', '', '', '', '0000-00-00', '', 0, 'sell', 'new', 12, 50, 0, 100000.00, 'ha', 1, 17, 28, 231, 0, 50000, 0, 0, 0.00, 0, '2014-02-19', '2017-10-11 10:33:16', 0, '', '', '', '', '', '', '', '', 'onsale', '0000-00-00', 'd', 0, 'notdefined', 0, '0000-00-00', 0, 0, 0, 0, 0, 'e1', 1, 0, 2, 4, 0, 0, 2, 1, 0, 1, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 50, 0, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0, 0, 1, '', '', '', '', '', '', '', '', 0, '', '<iframe src="https://player.vimeo.com/video/164022180" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>', 3, 0, '', '00:00:00', '00:00:00', 0, '0', 0, 0, 0, 0),
	(44, 44, 44, '', '', '', '', '', 0, '', '', '', '', '', '', 0, '', '', '', '', '', '', '0000-00-00', '', 0, 'sell', 'new', 12, 67, 0, 150.00, 'm2', 1, 17, 20, 355, 0, 160000, 0, 0, 0.00, 0, '2014-02-19', '2017-10-11 10:33:16', 0, '', '', '', '', '', '', '', '', 'onsale', '0000-00-00', 'b', 0, 'notdefined', 0, '0000-00-00', 0, 0, 0, 0, 0, 'e1', 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 1, 0, 1, 0, 1, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 50, 0, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0, 0, 1, '', '', '', '', '', '', '', '', 0, '', '<iframe src="https://player.vimeo.com/video/164022180" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>', 3, 0, '', '00:00:00', '00:00:00', 0, '0', 0, 0, 0, 0),
	(45, 45, 45, '', '', '', '', '', 0, '', '', '', '', '', '', 0, '', '', '', '', '', '', '0000-00-00', '', 0, 'sell', 'new', 2, 340, 0, 20.00, 'm2', 1, 17, 19, 128, 0, 220000, 0, 0, 0.00, 0, '2014-02-19', '2017-10-11 10:33:16', 0, '', '', '', '', '', '', '', '', 'onsale', '0000-00-00', 'b', 0, 'notdefined', 0, '0000-00-00', 1, 1, 1, 0, 1, 'e1', 1, 1, 2, 2, 1, 4, 2, 1, 1, 1, 0, 1, 40, 1, 7, 1, 1, 5, 0, 0, 0, 1, 1, 56, 1, 0, 1, 1, 1, 1, 1, 1, 1, 30, 0, 0, 0, 50, 0, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0, 0, 1, '', '', '', '', '', '', '', '', 0, '', '<iframe src="https://player.vimeo.com/video/164022180" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>', 3, 0, '', '00:00:00', '00:00:00', 0, '0', 0, 0, 0, 0);
/*!40000 ALTER TABLE `inmo__property` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `inmo__property_file` (
  `file_id` int(11) NOT NULL AUTO_INCREMENT,
  `property_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `name_original` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `size` int(11) NOT NULL DEFAULT '0',
  `ordre` int(11) NOT NULL DEFAULT '0',
  `main_file` int(1) NOT NULL DEFAULT '0',
  `filecat_id` int(11) NOT NULL DEFAULT '1',
  `public` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`file_id`),
  KEY `property_id` (`property_id`),
  KEY `filecat_id` (`filecat_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `inmo__property_file` DISABLE KEYS */;
/*!40000 ALTER TABLE `inmo__property_file` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `inmo__property_image` (
  `image_id` int(11) NOT NULL AUTO_INCREMENT,
  `property_id` int(11) NOT NULL DEFAULT '0',
  `imagecat_id` int(11) NOT NULL DEFAULT '1',
  `name` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `name_original` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `size` int(11) NOT NULL DEFAULT '0',
  `ordre` int(11) NOT NULL DEFAULT '0',
  `main_image` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`image_id`),
  KEY `property_id` (`property_id`),
  KEY `imagecat_id` (`imagecat_id`),
  KEY `main_image` (`main_image`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `inmo__property_image` DISABLE KEYS */;
INSERT INTO `inmo__property_image` (`image_id`, `property_id`, `imagecat_id`, `name`, `name_original`, `size`, `ordre`, `main_image`) VALUES
	(30, 5, 1, 'Interior-Design-Ideas-Screensaver[1].jpg', 'Interior-Design-Ideas-Screensaver[1].jpg', 44128, 0, 1),
	(28, 3, 1, 'Barcelona_mies_v_d_rohe_pavillon_weltausstellung1999_03[1].jpg', 'Barcelona_mies_v_d_rohe_pavillon_weltausstellung1999_03[1].jpg', 82702, 0, 1),
	(35, 5, 1, '2534237017_25a6aabf4b_o[1].jpg', '2534237017_25a6aabf4b_o[1].jpg', 61385, 6, 0),
	(29, 4, 1, 'HSBCMC25[1].jpg', 'HSBCMC25[1].jpg', 109782, 1, 1),
	(27, 1, 1, '2534237017_25a6aabf4b_o[1].jpg', '2534237017_25a6aabf4b_o[1].jpg', 61385, 1, 1),
	(36, 5, 1, '2835891662_d301eb526e_b[1].jpg', '2835891662_d301eb526e_b[1].jpg', 88728, 7, 0),
	(34, 5, 1, 'HSBCMC25[1].jpg', 'HSBCMC25[1].jpg', 109782, 5, 0),
	(37, 5, 1, 'Bauhaus-Dessau_Verbindung[1].jpg', 'Bauhaus-Dessau_Verbindung[1].jpg', 121691, 8, 0),
	(21, 5, 2, 'Interior-Design-Ideas-Screensaver[1].jpg', 'Interior-Design-Ideas-Screensaver[1].jpg', 65247, 9, 0),
	(22, 4, 2, 'HSBCMC25[1].jpg', 'HSBCMC25[1].jpg', 261559, 2, 0),
	(23, 3, 2, 'Barcelona_mies_v_d_rohe_pavillon_weltausstellung1999_03[1].jpg', 'Barcelona_mies_v_d_rohe_pavillon_weltausstellung1999_03[1].jpg', 60808, 1, 0),
	(24, 2, 2, '2835891662_d301eb526e_b[1].jpg', '2835891662_d301eb526e_b[1].jpg', 86823, 1, 0),
	(25, 1, 2, '2534237017_25a6aabf4b_o[1].jpg', '2534237017_25a6aabf4b_o[1].jpg', 50221, 2, 0),
	(26, 2, 1, '2835891662_d301eb526e_b[1].jpg', '2835891662_d301eb526e_b[1].jpg', 88728, 0, 1),
	(31, 5, 1, '481368[1].jpg', '481368[1].jpg', 141288, 2, 0),
	(32, 5, 1, '2558057[1].jpg', '2558057[1].jpg', 103629, 3, 0),
	(33, 5, 1, '2197550923_5fdd46517f_o[1].jpg', '2197550923_5fdd46517f_o[1].jpg', 84230, 4, 0),
	(38, 5, 1, '2558057[1].jpg', '2558057[1].jpg', 46345, 1, 0),
	(39, 15, 1, 'Casa+A-M,+Barcelona+(6).jpg', 'Casa+A-M,+Barcelona+(6).jpg', 364845, 0, 1),
	(40, 25, 1, 'casa+minimalista.jpg', 'casa+minimalista.jpg', 355192, 0, 1),
	(41, 35, 1, 'CASA+PONCE+-+Couti%C2%A7o+%26+Ponce+06.jpg', 'CASA+PONCE+-+Couti%C2%A7o+%26+Ponce+06.jpg', 416892, 0, 1),
	(42, 45, 1, 'casa1.jpg', 'casa1.jpg', 368652, 0, 1),
	(43, 14, 1, 'Casa-de-la-cascada-4.jpg', 'Casa-de-la-cascada-4.jpg', 523698, 0, 1),
	(44, 24, 1, 'casa-del-agua-18.jpg', 'casa-del-agua-18.jpg', 403778, 0, 1),
	(45, 34, 1, 'casas-2201.jpg', 'casas-2201.jpg', 532241, 0, 1),
	(46, 44, 1, 'fachada-de-casa-moderna-blanca-con-piscina.jpg', 'fachada-de-casa-moderna-blanca-con-piscina.jpg', 353710, 1, 1),
	(47, 13, 1, 'fachada-de-casa-moderna-bonita-pequena-dos-niveles.jpg', 'fachada-de-casa-moderna-bonita-pequena-dos-niveles.jpg', 354692, 0, 1),
	(48, 23, 1, 'Fachadas-en-3D-de-Casa.jpg', 'Fachadas-en-3D-de-Casa.jpg', 482845, 0, 1),
	(49, 33, 1, 'images.jpg', 'images.jpg', 174492, 0, 1),
	(50, 43, 1, 'images2.jpg', 'images2.jpg', 204783, 0, 1),
	(51, 12, 1, 'Oscar-Ricoy-Final.jpg', 'Oscar-Ricoy-Final.jpg', 377543, 0, 1),
	(52, 22, 1, 'casa-bauza-3.jpg', 'casa-bauza-3.jpg', 406810, 0, 1),
	(53, 32, 1, '1330601734_casa_g16_mira_arquitetos_06.jpg', '1330601734_casa_g16_mira_arquitetos_06.jpg', 406523, 0, 1),
	(54, 42, 1, 'casa+minimalista.jpg', 'casa+minimalista.jpg', 355192, 0, 1),
	(55, 11, 1, 'casa-del-agua-18.jpg', 'casa-del-agua-18.jpg', 403778, 0, 1),
	(56, 21, 1, '002-casa-en-la-encantada-javier-artadi.jpg', '002-casa-en-la-encantada-javier-artadi.jpg', 274162, 0, 1),
	(57, 31, 1, 'casa-bauza-3.jpg', 'casa-bauza-3.jpg', 406810, 0, 1),
	(58, 41, 1, 'casa+minimalista.jpg', 'casa+minimalista.jpg', 355192, 0, 1),
	(59, 44, 1, '002-casa-en-la-encantada-javier-artadi.jpg', '002-casa-en-la-encantada-javier-artadi.jpg', 274162, 2, 0),
	(60, 44, 1, '1330601734_casa_g16_mira_arquitetos_06.jpg', '1330601734_casa_g16_mira_arquitetos_06.jpg', 406523, 3, 0);
/*!40000 ALTER TABLE `inmo__property_image` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `inmo__property_imagecat` (
  `imagecat_id` int(11) NOT NULL AUTO_INCREMENT,
  `imagecat` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `ordre` int(11) DEFAULT NULL,
  PRIMARY KEY (`imagecat_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci ROW_FORMAT=COMPACT;

/*!40000 ALTER TABLE `inmo__property_imagecat` DISABLE KEYS */;
INSERT INTO `inmo__property_imagecat` (`imagecat_id`, `imagecat`, `ordre`) VALUES
	(1, 'Fitxa i llistat', NULL),
	(2, 'Home', NULL);
/*!40000 ALTER TABLE `inmo__property_imagecat` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `inmo__property_image_language` (
  `image_id` int(11) NOT NULL DEFAULT '0',
  `language` char(3) COLLATE utf8_spanish2_ci NOT NULL,
  `image_alt` varchar(150) COLLATE utf8_spanish2_ci NOT NULL,
  `image_title` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  KEY `image_id` (`image_id`),
  KEY `language` (`language`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `inmo__property_image_language` DISABLE KEYS */;
INSERT INTO `inmo__property_image_language` (`image_id`, `language`, `image_alt`, `image_title`) VALUES
	(28, 'fra', '', ''),
	(28, 'eng', '', ''),
	(28, 'spa', '', ''),
	(28, 'cat', '', ''),
	(30, 'fra', '', ''),
	(30, 'eng', '', ''),
	(30, 'spa', '', ''),
	(30, 'cat', '', ''),
	(29, 'fra', '', ''),
	(29, 'eng', '', ''),
	(29, 'spa', '', ''),
	(27, 'fra', '', ''),
	(27, 'eng', '', ''),
	(29, 'cat', '', ''),
	(35, 'fra', '', ''),
	(35, 'eng', '', ''),
	(35, 'spa', '', ''),
	(35, 'cat', '', ''),
	(34, 'fra', '', ''),
	(34, 'eng', '', ''),
	(34, 'spa', '', ''),
	(34, 'cat', '', ''),
	(37, 'eng', '', ''),
	(37, 'spa', '', ''),
	(37, 'cat', '', ''),
	(36, 'fra', '', ''),
	(36, 'eng', '', ''),
	(36, 'spa', '', ''),
	(36, 'cat', '', ''),
	(27, 'spa', '', ''),
	(27, 'cat', '', ''),
	(21, 'fra', '', ''),
	(21, 'eng', '', ''),
	(21, 'spa', '', ''),
	(21, 'cat', '', ''),
	(22, 'cat', '', ''),
	(22, 'spa', '', ''),
	(22, 'eng', '', ''),
	(22, 'fra', '', ''),
	(23, 'cat', '', ''),
	(23, 'spa', '', ''),
	(23, 'eng', '', ''),
	(23, 'fra', '', ''),
	(24, 'cat', '', ''),
	(24, 'spa', '', ''),
	(24, 'eng', '', ''),
	(24, 'fra', '', ''),
	(25, 'cat', '', ''),
	(25, 'spa', '', ''),
	(25, 'eng', '', ''),
	(25, 'fra', '', ''),
	(26, 'cat', '', ''),
	(26, 'spa', '', ''),
	(26, 'eng', '', ''),
	(26, 'fra', '', ''),
	(31, 'cat', '', ''),
	(31, 'spa', '', ''),
	(31, 'eng', '', ''),
	(31, 'fra', '', ''),
	(32, 'cat', '', ''),
	(32, 'spa', '', ''),
	(32, 'eng', '', ''),
	(32, 'fra', '', ''),
	(33, 'cat', '', ''),
	(33, 'spa', '', ''),
	(33, 'eng', '', ''),
	(33, 'fra', '', ''),
	(37, 'fra', '', ''),
	(38, 'cat', '', ''),
	(38, 'spa', '', ''),
	(38, 'eng', '', ''),
	(38, 'fra', '', ''),
	(39, 'cat', '', ''),
	(39, 'spa', '', ''),
	(39, 'eng', '', ''),
	(39, 'fra', '', ''),
	(40, 'cat', '', ''),
	(40, 'spa', '', ''),
	(40, 'eng', '', ''),
	(40, 'fra', '', ''),
	(41, 'cat', '', ''),
	(41, 'spa', '', ''),
	(41, 'eng', '', ''),
	(41, 'fra', '', ''),
	(42, 'cat', '', ''),
	(42, 'spa', '', ''),
	(42, 'eng', '', ''),
	(42, 'fra', '', ''),
	(43, 'cat', '', ''),
	(43, 'spa', '', ''),
	(43, 'eng', '', ''),
	(43, 'fra', '', ''),
	(44, 'cat', '', ''),
	(44, 'spa', '', ''),
	(44, 'eng', '', ''),
	(44, 'fra', '', ''),
	(45, 'cat', '', ''),
	(45, 'spa', '', ''),
	(45, 'eng', '', ''),
	(45, 'fra', '', ''),
	(46, 'cat', '', ''),
	(46, 'spa', '', ''),
	(46, 'eng', '', ''),
	(46, 'fra', '', ''),
	(47, 'cat', '', ''),
	(47, 'spa', '', ''),
	(47, 'eng', '', ''),
	(47, 'fra', '', ''),
	(48, 'cat', '', ''),
	(48, 'spa', '', ''),
	(48, 'eng', '', ''),
	(48, 'fra', '', ''),
	(49, 'cat', '', ''),
	(49, 'spa', '', ''),
	(49, 'eng', '', ''),
	(49, 'fra', '', ''),
	(50, 'cat', '', ''),
	(50, 'spa', '', ''),
	(50, 'eng', '', ''),
	(50, 'fra', '', ''),
	(51, 'cat', '', ''),
	(51, 'spa', '', ''),
	(51, 'eng', '', ''),
	(51, 'fra', '', ''),
	(52, 'cat', '', ''),
	(52, 'spa', '', ''),
	(52, 'eng', '', ''),
	(52, 'fra', '', ''),
	(53, 'cat', '', ''),
	(53, 'spa', '', ''),
	(53, 'eng', '', ''),
	(53, 'fra', '', ''),
	(54, 'cat', '', ''),
	(54, 'spa', '', ''),
	(54, 'eng', '', ''),
	(54, 'fra', '', ''),
	(55, 'cat', '', ''),
	(55, 'spa', '', ''),
	(55, 'eng', '', ''),
	(55, 'fra', '', ''),
	(56, 'cat', '', ''),
	(56, 'spa', '', ''),
	(56, 'eng', '', ''),
	(56, 'fra', '', ''),
	(57, 'cat', '', ''),
	(57, 'spa', '', ''),
	(57, 'eng', '', ''),
	(57, 'fra', '', ''),
	(58, 'cat', '', ''),
	(58, 'spa', '', ''),
	(58, 'eng', '', ''),
	(58, 'fra', '', ''),
	(59, 'cat', '', ''),
	(59, 'spa', '', ''),
	(59, 'eng', '', ''),
	(59, 'fra', '', ''),
	(60, 'cat', '', ''),
	(60, 'spa', '', ''),
	(60, 'eng', '', ''),
	(60, 'fra', '', '');
/*!40000 ALTER TABLE `inmo__property_image_language` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `inmo__property_language` (
  `property_id` int(11) NOT NULL DEFAULT '0',
  `language` char(3) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '0',
  `description` text COLLATE utf8_spanish2_ci NOT NULL,
  `property` varchar(150) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `property_title` varchar(150) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `custom1` varchar(255) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `custom2` varchar(255) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `custom3` varchar(255) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `custom4` varchar(255) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `custom5` varchar(255) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `custom6` varchar(255) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `custom7` varchar(255) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `custom8` varchar(255) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `page_title` varchar(255) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `page_description` varchar(255) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `page_keywords` varchar(255) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `property_file_name` varchar(100) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `property_old_file_name` varchar(100) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  KEY `property_id` (`property_id`),
  KEY `language` (`language`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `inmo__property_language` DISABLE KEYS */;
INSERT INTO `inmo__property_language` (`property_id`, `language`, `description`, `property`, `property_title`, `custom1`, `custom2`, `custom3`, `custom4`, `custom5`, `custom6`, `custom7`, `custom8`, `page_title`, `page_description`, `page_keywords`, `property_file_name`, `property_old_file_name`) VALUES
	(1, 'cat', 'Totam, quisquam, blanditiis fugiat ex laudantium necessitatibus eaque minus libero adipisci dolorem ipsam magnam hic quam? Consequuntur, odio rem praesentium nemo eveniet.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium, quam, omnis, officia error quae consequuntur tempora mollitia numquam debitis deleniti harum aspernatur non consequatur natus ut cupiditate quisquam eaque fugiat!', 'Dolorem soluta magnam esse dolorum', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(1, 'spa', 'Totam, quisquam, blanditiis fugiat ex laudantium necessitatibus eaque minus libero adipisci dolorem ipsam magnam hic quam? Consequuntur, odio rem praesentium nemo eveniet.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium, quam, omnis, officia error quae consequuntur tempora mollitia numquam debitis deleniti harum aspernatur non consequatur natus ut cupiditate quisquam eaque fugiat!', 'Dolorem soluta', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(1, 'eng', 'Totam, quisquam, blanditiis fugiat ex laudantium necessitatibus eaque minus libero adipisci dolorem ipsam magnam hic quam? Consequuntur, odio rem praesentium nemo eveniet.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium, quam, omnis, officia error quae consequuntur tempora mollitia numquam debitis deleniti harum aspernatur non consequatur natus ut cupiditate quisquam eaque fugiat!', 'Dolorem soluta', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(1, 'fra', 'Totam, quisquam, blanditiis fugiat ex laudantium necessitatibus eaque minus libero adipisci dolorem ipsam magnam hic quam? Consequuntur, odio rem praesentium nemo eveniet.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium, quam, omnis, officia error quae consequuntur tempora mollitia numquam debitis deleniti harum aspernatur non consequatur natus ut cupiditate quisquam eaque fugiat!', 'Dolorem soluta magnam esse dolorum', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(2, 'cat', 'At, autem omnis veritatis. Repellat, esse, asperiores, voluptatum nesciunt tempore nisi praesentium assumenda inventore temporibus rem tempora cum consectetur nam est ratione. Ex, excepturi laudantium quo possimus consequuntur neque iusto assumenda culpa eius asperiores soluta sapiente tenetur nihil praesentium suscipit unde earum provident impedit!', 'Minus aliquid maxime molestiae voluptate', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(2, 'spa', 'At, autem omnis veritatis. Repellat, esse, asperiores, voluptatum nesciunt tempore nisi praesentium assumenda inventore temporibus rem tempora cum consectetur nam est ratione. Ex, excepturi laudantium quo possimus consequuntur neque iusto assumenda culpa eius asperiores soluta sapiente tenetur nihil praesentium suscipit unde earum provident impedit!', 'Minus aliquid maxime molestiae voluptate', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(2, 'eng', 'At, autem omnis veritatis. Repellat, esse, asperiores, voluptatum nesciunt tempore nisi praesentium assumenda inventore temporibus rem tempora cum consectetur nam est ratione. Ex, excepturi laudantium quo possimus consequuntur neque iusto assumenda culpa eius asperiores soluta sapiente tenetur nihil praesentium suscipit unde earum provident impedit!', 'Minus aliquid maxime molestiae voluptate', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(2, 'fra', 'At, autem omnis veritatis. Repellat, esse, asperiores, voluptatum nesciunt tempore nisi praesentium assumenda inventore temporibus rem tempora cum consectetur nam est ratione. Ex, excepturi laudantium quo possimus consequuntur neque iusto assumenda culpa eius asperiores soluta sapiente tenetur nihil praesentium suscipit unde earum provident impedit!', 'Minus aliquid', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(3, 'cat', 'Maiores, repellat officiis consectetur nisi impedit dolor possimus natus quo a iste. Non, veritatis explicabo earum magnam quibusdam quasi illo eaque iste? Similique, iste, a dolor sed eaque doloremque. Quam, nesciunt officia voluptate adipisci accusantium iusto excepturi voluptatum impedit! Dolorem totam id nesciunt asperiores.', 'Praesentium', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(3, 'spa', 'Maiores, repellat officiis consectetur nisi impedit dolor possimus natus quo a iste. Non, veritatis explicabo earum magnam quibusdam quasi illo eaque iste? Similique, iste, a dolor sed eaque doloremque. Quam, nesciunt officia voluptate adipisci accusantium iusto excepturi voluptatum impedit! Dolorem totam id nesciunt asperiores.', 'Praesentium', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(3, 'eng', 'Maiores, repellat officiis consectetur nisi impedit dolor possimus natus quo a iste. Non, veritatis explicabo earum magnam quibusdam quasi illo eaque iste? Similique, iste, a dolor sed eaque doloremque. Quam, nesciunt officia voluptate adipisci accusantium iusto excepturi voluptatum impedit! Dolorem totam id nesciunt asperiores.', 'Praesentium', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(3, 'fra', 'Maiores, repellat officiis consectetur nisi impedit dolor possimus natus quo a iste. Non, veritatis explicabo earum magnam quibusdam quasi illo eaque iste? Similique, iste, a dolor sed eaque doloremque. Quam, nesciunt officia voluptate adipisci accusantium iusto excepturi voluptatum impedit! Dolorem totam id nesciunt asperiores.', 'Praesentium adipisci aperiam', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(4, 'cat', 'A, sapiente amet molestiae dignissimos numquam quas qui cum beatae expedita eaque dolor magni quibusdam repudiandae velit soluta esse fuga atque quidem.', 'Consequuntur odio rem praesentium', 'Totam, quisquam, blanditiis fugiat ex laudantium necessitatibus', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(4, 'spa', 'A, sapiente amet molestiae dignissimos numquam quas qui cum beatae expedita eaque dolor magni quibusdam repudiandae velit soluta esse fuga atque quidem.', 'Consequuntur odio rem praesentium', 'Totam, quisquam, blanditiis fugiat ex laudantium necessitatibus', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(4, 'eng', 'A, sapiente amet molestiae dignissimos numquam quas qui cum beatae expedita eaque dolor magni quibusdam repudiandae velit soluta esse fuga atque quidem.', 'Consequuntur odio rem praesentium', 'Totam, quisquam, blanditiis fugiat ex laudantium necessitatibus', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(4, 'fra', 'A, sapiente amet molestiae dignissimos numquam quas qui cum beatae expedita eaque dolor magni quibusdam repudiandae velit soluta esse fuga atque quidem.', 'Consequuntur odio rem praesentium', 'Totam, quisquam, blanditiis fugiat ex laudantium necessitatibus', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(5, 'cat', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium, modi, consequatur quidem voluptate delectus id fugiat quaerat illum maiores molestiae harum nam natus consectetur sequi a tempora amet rem vitae!', 'Tots els extres', 'Tots els extres descripcio curta', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(5, 'spa', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium, modi, consequatur quidem voluptate delectus id fugiat quaerat illum maiores molestiae harum nam natus consectetur sequi a tempora amet rem vitae!', 'Tots els extres', 'Tots els extres descripcio curta', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(5, 'eng', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium, modi, consequatur quidem voluptate delectus id fugiat quaerat illum maiores molestiae harum nam natus consectetur sequi a tempora amet rem vitae!', 'Tots els extres', 'Tots els extres descripcio curta', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(5, 'fra', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium, modi, consequatur quidem voluptate delectus id fugiat quaerat illum maiores molestiae harum nam natus consectetur sequi a tempora amet rem vitae!', 'Tots els extres', 'Tots els extres descripcio curta', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(11, 'cat', 'Totam, quisquam, blanditiis fugiat ex laudantium necessitatibus eaque minus libero adipisci dolorem ipsam magnam hic quam? Consequuntur, odio rem praesentium nemo eveniet.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium, quam, omnis, officia error quae consequuntur tempora mollitia numquam debitis deleniti harum aspernatur non consequatur natus ut cupiditate quisquam eaque fugiat!', 'Dolorem soluta magnam esse dolorum', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(12, 'cat', 'At, autem omnis veritatis. Repellat, esse, asperiores, voluptatum nesciunt tempore nisi praesentium assumenda inventore temporibus rem tempora cum consectetur nam est ratione. Ex, excepturi laudantium quo possimus consequuntur neque iusto assumenda culpa eius asperiores soluta sapiente tenetur nihil praesentium suscipit unde earum provident impedit!', 'Minus aliquid maxime molestiae voluptate', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(13, 'cat', 'Maiores, repellat officiis consectetur nisi impedit dolor possimus natus quo a iste. Non, veritatis explicabo earum magnam quibusdam quasi illo eaque iste? Similique, iste, a dolor sed eaque doloremque. Quam, nesciunt officia voluptate adipisci accusantium iusto excepturi voluptatum impedit! Dolorem totam id nesciunt asperiores.', 'Praesentium', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(14, 'cat', 'A, sapiente amet molestiae dignissimos numquam quas qui cum beatae expedita eaque dolor magni quibusdam repudiandae velit soluta esse fuga atque quidem.', 'Consequuntur odio rem praesentium', 'Totam, quisquam, blanditiis fugiat ex laudantium necessitatibus', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(15, 'cat', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium, modi, consequatur quidem voluptate delectus id fugiat quaerat illum maiores molestiae harum nam natus consectetur sequi a tempora amet rem vitae!', 'Tots els extres', 'Tots els extres descripcio curta', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(21, 'cat', 'Totam, quisquam, blanditiis fugiat ex laudantium necessitatibus eaque minus libero adipisci dolorem ipsam magnam hic quam? Consequuntur, odio rem praesentium nemo eveniet.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium, quam, omnis, officia error quae consequuntur tempora mollitia numquam debitis deleniti harum aspernatur non consequatur natus ut cupiditate quisquam eaque fugiat!', 'Dolorem soluta magnam esse dolorum', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(22, 'cat', 'At, autem omnis veritatis. Repellat, esse, asperiores, voluptatum nesciunt tempore nisi praesentium assumenda inventore temporibus rem tempora cum consectetur nam est ratione. Ex, excepturi laudantium quo possimus consequuntur neque iusto assumenda culpa eius asperiores soluta sapiente tenetur nihil praesentium suscipit unde earum provident impedit!', 'Minus aliquid maxime molestiae voluptate', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(23, 'cat', 'Maiores, repellat officiis consectetur nisi impedit dolor possimus natus quo a iste. Non, veritatis explicabo earum magnam quibusdam quasi illo eaque iste? Similique, iste, a dolor sed eaque doloremque. Quam, nesciunt officia voluptate adipisci accusantium iusto excepturi voluptatum impedit! Dolorem totam id nesciunt asperiores.', 'Praesentium', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(24, 'cat', 'A, sapiente amet molestiae dignissimos numquam quas qui cum beatae expedita eaque dolor magni quibusdam repudiandae velit soluta esse fuga atque quidem.', 'Consequuntur odio rem praesentium', 'Totam, quisquam, blanditiis fugiat ex laudantium necessitatibus', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(25, 'cat', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium, modi, consequatur quidem voluptate delectus id fugiat quaerat illum maiores molestiae harum nam natus consectetur sequi a tempora amet rem vitae!', 'Tots els extres', 'Tots els extres descripcio curta', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(31, 'cat', 'Totam, quisquam, blanditiis fugiat ex laudantium necessitatibus eaque minus libero adipisci dolorem ipsam magnam hic quam? Consequuntur, odio rem praesentium nemo eveniet.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium, quam, omnis, officia error quae consequuntur tempora mollitia numquam debitis deleniti harum aspernatur non consequatur natus ut cupiditate quisquam eaque fugiat!', 'Dolorem soluta magnam esse dolorum', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(32, 'cat', 'At, autem omnis veritatis. Repellat, esse, asperiores, voluptatum nesciunt tempore nisi praesentium assumenda inventore temporibus rem tempora cum consectetur nam est ratione. Ex, excepturi laudantium quo possimus consequuntur neque iusto assumenda culpa eius asperiores soluta sapiente tenetur nihil praesentium suscipit unde earum provident impedit!', 'Minus aliquid maxime molestiae voluptate', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(33, 'cat', 'Maiores, repellat officiis consectetur nisi impedit dolor possimus natus quo a iste. Non, veritatis explicabo earum magnam quibusdam quasi illo eaque iste? Similique, iste, a dolor sed eaque doloremque. Quam, nesciunt officia voluptate adipisci accusantium iusto excepturi voluptatum impedit! Dolorem totam id nesciunt asperiores.', 'Praesentium adipisci aperiam', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(34, 'cat', 'A, sapiente amet molestiae dignissimos numquam quas qui cum beatae expedita eaque dolor magni quibusdam repudiandae velit soluta esse fuga atque quidem.', 'Consequuntur odio', 'Totam, quisquam, blanditiis fugiat ex laudantium necessitatibus', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(35, 'cat', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium, modi, consequatur quidem voluptate delectus id fugiat quaerat illum maiores molestiae harum nam natus consectetur sequi a tempora amet rem vitae!', 'Tots els extres', 'Tots els extres descripcio curta', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(41, 'cat', 'Totam, quisquam, blanditiis fugiat ex laudantium necessitatibus eaque minus libero adipisci dolorem ipsam magnam hic quam? Consequuntur, odio rem praesentium nemo eveniet.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium, quam, omnis, officia error quae consequuntur tempora mollitia numquam debitis deleniti harum aspernatur non consequatur natus ut cupiditate quisquam eaque fugiat!', 'Dolorem soluta magnam esse dolorum', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(42, 'cat', 'At, autem omnis veritatis. Repellat, esse, asperiores, voluptatum nesciunt tempore nisi praesentium assumenda inventore temporibus rem tempora cum consectetur nam est ratione. Ex, excepturi laudantium quo possimus consequuntur neque iusto assumenda culpa eius asperiores soluta sapiente tenetur nihil praesentium suscipit unde earum provident impedit!', 'Minus aliquid', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(43, 'cat', 'Maiores, repellat officiis consectetur nisi impedit dolor possimus natus quo a iste. Non, veritatis explicabo earum magnam quibusdam quasi illo eaque iste? Similique, iste, a dolor sed eaque doloremque. Quam, nesciunt officia voluptate adipisci accusantium iusto excepturi voluptatum impedit! Dolorem totam id nesciunt asperiores.', 'Praesentium adipisci aperiam', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(44, 'cat', 'A, sapiente amet molestiae dignissimos numquam quas qui cum beatae expedita eaque dolor magni quibusdam repudiandae velit soluta esse fuga atque quidem.', 'Consequuntur odio rem praesentium', 'Totam, quisquam, blanditiis fugiat ex laudantium necessitatibus', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(45, 'cat', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium, modi, consequatur quidem voluptate delectus id fugiat quaerat illum maiores molestiae harum nam natus consectetur sequi a tempora amet rem vitae!', 'Tots els extres', 'Tots els extres descripcio curta', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(11, 'spa', 'Totam, quisquam, blanditiis fugiat ex laudantium necessitatibus eaque minus libero adipisci dolorem ipsam magnam hic quam? Consequuntur, odio rem praesentium nemo eveniet.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium, quam, omnis, officia error quae consequuntur tempora mollitia numquam debitis deleniti harum aspernatur non consequatur natus ut cupiditate quisquam eaque fugiat!', 'Dolorem solut', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(12, 'spa', 'At, autem omnis veritatis. Repellat, esse, asperiores, voluptatum nesciunt tempore nisi praesentium assumenda inventore temporibus rem tempora cum consectetur nam est ratione. Ex, excepturi laudantium quo possimus consequuntur neque iusto assumenda culpa eius asperiores soluta sapiente tenetur nihil praesentium suscipit unde earum provident impedit!', 'Minus aliquid maxime molestiae voluptate', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(13, 'spa', 'Maiores, repellat officiis consectetur nisi impedit dolor possimus natus quo a iste. Non, veritatis explicabo earum magnam quibusdam quasi illo eaque iste? Similique, iste, a dolor sed eaque doloremque. Quam, nesciunt officia voluptate adipisci accusantium iusto excepturi voluptatum impedit! Dolorem totam id nesciunt asperiores.', 'Praesentium adipisci aperiam', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(14, 'spa', 'A, sapiente amet molestiae dignissimos numquam quas qui cum beatae expedita eaque dolor magni quibusdam repudiandae velit soluta esse fuga atque quidem.', 'Consequuntur', 'Totam, quisquam, blanditiis fugiat ex laudantium necessitatibus', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(15, 'spa', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium, modi, consequatur quidem voluptate delectus id fugiat quaerat illum maiores molestiae harum nam natus consectetur sequi a tempora amet rem vitae!', 'Tots els extres', 'Tots els extres descripcio curta', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(21, 'spa', 'Totam, quisquam, blanditiis fugiat ex laudantium necessitatibus eaque minus libero adipisci dolorem ipsam magnam hic quam? Consequuntur, odio rem praesentium nemo eveniet.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium, quam, omnis, officia error quae consequuntur tempora mollitia numquam debitis deleniti harum aspernatur non consequatur natus ut cupiditate quisquam eaque fugiat!', 'Dolorem soluta magnam esse dolorum', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(22, 'spa', 'At, autem omnis veritatis. Repellat, esse, asperiores, voluptatum nesciunt tempore nisi praesentium assumenda inventore temporibus rem tempora cum consectetur nam est ratione. Ex, excepturi laudantium quo possimus consequuntur neque iusto assumenda culpa eius asperiores soluta sapiente tenetur nihil praesentium suscipit unde earum provident impedit!', 'Minus aliquid', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(23, 'spa', 'Maiores, repellat officiis consectetur nisi impedit dolor possimus natus quo a iste. Non, veritatis explicabo earum magnam quibusdam quasi illo eaque iste? Similique, iste, a dolor sed eaque doloremque. Quam, nesciunt officia voluptate adipisci accusantium iusto excepturi voluptatum impedit! Dolorem totam id nesciunt asperiores.', 'Praesentium adipisci aperiam', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(24, 'spa', 'A, sapiente amet molestiae dignissimos numquam quas qui cum beatae expedita eaque dolor magni quibusdam repudiandae velit soluta esse fuga atque quidem.', 'Consequuntur', 'Totam, quisquam, blanditiis fugiat ex laudantium necessitatibus', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(25, 'spa', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium, modi, consequatur quidem voluptate delectus id fugiat quaerat illum maiores molestiae harum nam natus consectetur sequi a tempora amet rem vitae!', 'Tots els extres', 'Tots els extres descripcio curta', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(31, 'spa', 'Totam, quisquam, blanditiis fugiat ex laudantium necessitatibus eaque minus libero adipisci dolorem ipsam magnam hic quam? Consequuntur, odio rem praesentium nemo eveniet.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium, quam, omnis, officia error quae consequuntur tempora mollitia numquam debitis deleniti harum aspernatur non consequatur natus ut cupiditate quisquam eaque fugiat!', 'Dolorem soluta magnam esse dolorum', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(32, 'spa', 'At, autem omnis veritatis. Repellat, esse, asperiores, voluptatum nesciunt tempore nisi praesentium assumenda inventore temporibus rem tempora cum consectetur nam est ratione. Ex, excepturi laudantium quo possimus consequuntur neque iusto assumenda culpa eius asperiores soluta sapiente tenetur nihil praesentium suscipit unde earum provident impedit!', 'Minus aliquid maxime molestiae voluptate', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(33, 'spa', 'Maiores, repellat officiis consectetur nisi impedit dolor possimus natus quo a iste. Non, veritatis explicabo earum magnam quibusdam quasi illo eaque iste? Similique, iste, a dolor sed eaque doloremque. Quam, nesciunt officia voluptate adipisci accusantium iusto excepturi voluptatum impedit! Dolorem totam id nesciunt asperiores.', 'Praesentiu', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(34, 'spa', 'A, sapiente amet molestiae dignissimos numquam quas qui cum beatae expedita eaque dolor magni quibusdam repudiandae velit soluta esse fuga atque quidem.', 'Consequuntur odio rem praesentium', 'Totam, quisquam, blanditiis fugiat ex laudantium necessitatibus', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(35, 'spa', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium, modi, consequatur quidem voluptate delectus id fugiat quaerat illum maiores molestiae harum nam natus consectetur sequi a tempora amet rem vitae!', 'Tots els extres', 'Tots els extres descripcio curta', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(41, 'spa', 'Totam, quisquam, blanditiis fugiat ex laudantium necessitatibus eaque minus libero adipisci dolorem ipsam magnam hic quam? Consequuntur, odio rem praesentium nemo eveniet.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium, quam, omnis, officia error quae consequuntur tempora mollitia numquam debitis deleniti harum aspernatur non consequatur natus ut cupiditate quisquam eaque fugiat!', 'Dolorem soluta', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(42, 'spa', 'At, autem omnis veritatis. Repellat, esse, asperiores, voluptatum nesciunt tempore nisi praesentium assumenda inventore temporibus rem tempora cum consectetur nam est ratione. Ex, excepturi laudantium quo possimus consequuntur neque iusto assumenda culpa eius asperiores soluta sapiente tenetur nihil praesentium suscipit unde earum provident impedit!', 'Minus aliquid maxime molestiae voluptate', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(43, 'spa', 'Maiores, repellat officiis consectetur nisi impedit dolor possimus natus quo a iste. Non, veritatis explicabo earum magnam quibusdam quasi illo eaque iste? Similique, iste, a dolor sed eaque doloremque. Quam, nesciunt officia voluptate adipisci accusantium iusto excepturi voluptatum impedit! Dolorem totam id nesciunt asperiores.', 'Praesentium', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(44, 'spa', 'A, sapiente amet molestiae dignissimos numquam quas qui cum beatae expedita eaque dolor magni quibusdam repudiandae velit soluta esse fuga atque quidem.', 'Consequuntur odio rem praesentium', 'Totam, quisquam, blanditiis fugiat ex laudantium necessitatibus', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(45, 'spa', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium, modi, consequatur quidem voluptate delectus id fugiat quaerat illum maiores molestiae harum nam natus consectetur sequi a tempora amet rem vitae!', 'Tots els extres', 'Tots els extres descripcio curta', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(11, 'fra', 'Totam, quisquam, blanditiis fugiat ex laudantium necessitatibus eaque minus libero adipisci dolorem ipsam magnam hic quam? Consequuntur, odio rem praesentium nemo eveniet.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium, quam, omnis, officia error quae consequuntur tempora mollitia numquam debitis deleniti harum aspernatur non consequatur natus ut cupiditate quisquam eaque fugiat!', 'Dolorem', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(12, 'fra', 'At, autem omnis veritatis. Repellat, esse, asperiores, voluptatum nesciunt tempore nisi praesentium assumenda inventore temporibus rem tempora cum consectetur nam est ratione. Ex, excepturi laudantium quo possimus consequuntur neque iusto assumenda culpa eius asperiores soluta sapiente tenetur nihil praesentium suscipit unde earum provident impedit!', 'Minus aliquid maxime molestiae voluptate', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(13, 'fra', 'Maiores, repellat officiis consectetur nisi impedit dolor possimus natus quo a iste. Non, veritatis explicabo earum magnam quibusdam quasi illo eaque iste? Similique, iste, a dolor sed eaque doloremque. Quam, nesciunt officia voluptate adipisci accusantium iusto excepturi voluptatum impedit! Dolorem totam id nesciunt asperiores.', 'Praesentium adipisci aperiam', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(14, 'fra', 'A, sapiente amet molestiae dignissimos numquam quas qui cum beatae expedita eaque dolor magni quibusdam repudiandae velit soluta esse fuga atque quidem.', 'Consequuntur odio rem praesentium', 'Totam, quisquam, blanditiis fugiat ex laudantium necessitatibus', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(15, 'fra', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium, modi, consequatur quidem voluptate delectus id fugiat quaerat illum maiores molestiae harum nam natus consectetur sequi a tempora amet rem vitae!', 'Tots els extres', 'Tots els extres descripcio curta', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(21, 'fra', 'Totam, quisquam, blanditiis fugiat ex laudantium necessitatibus eaque minus libero adipisci dolorem ipsam magnam hic quam? Consequuntur, odio rem praesentium nemo eveniet.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium, quam, omnis, officia error quae consequuntur tempora mollitia numquam debitis deleniti harum aspernatur non consequatur natus ut cupiditate quisquam eaque fugiat!', 'Dolorem soluta magnam esse dolorum', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(22, 'fra', 'At, autem omnis veritatis. Repellat, esse, asperiores, voluptatum nesciunt tempore nisi praesentium assumenda inventore temporibus rem tempora cum consectetur nam est ratione. Ex, excepturi laudantium quo possimus consequuntur neque iusto assumenda culpa eius asperiores soluta sapiente tenetur nihil praesentium suscipit unde earum provident impedit!', 'Minus aliquid', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(23, 'fra', 'Maiores, repellat officiis consectetur nisi impedit dolor possimus natus quo a iste. Non, veritatis explicabo earum magnam quibusdam quasi illo eaque iste? Similique, iste, a dolor sed eaque doloremque. Quam, nesciunt officia voluptate adipisci accusantium iusto excepturi voluptatum impedit! Dolorem totam id nesciunt asperiores.', 'Praesentium adipisci aperiam', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(24, 'fra', 'A, sapiente amet molestiae dignissimos numquam quas qui cum beatae expedita eaque dolor magni quibusdam repudiandae velit soluta esse fuga atque quidem.', 'Consequuntur odio rem praesentium', 'Totam, quisquam, blanditiis fugiat ex laudantium necessitatibus', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(25, 'fra', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium, modi, consequatur quidem voluptate delectus id fugiat quaerat illum maiores molestiae harum nam natus consectetur sequi a tempora amet rem vitae!', 'Tots els extres', 'Tots els extres descripcio curta', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(31, 'fra', 'Totam, quisquam, blanditiis fugiat ex laudantium necessitatibus eaque minus libero adipisci dolorem ipsam magnam hic quam? Consequuntur, odio rem praesentium nemo eveniet.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium, quam, omnis, officia error quae consequuntur tempora mollitia numquam debitis deleniti harum aspernatur non consequatur natus ut cupiditate quisquam eaque fugiat!', 'Dolorem soluta magnam esse dolorum', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(32, 'fra', 'At, autem omnis veritatis. Repellat, esse, asperiores, voluptatum nesciunt tempore nisi praesentium assumenda inventore temporibus rem tempora cum consectetur nam est ratione. Ex, excepturi laudantium quo possimus consequuntur neque iusto assumenda culpa eius asperiores soluta sapiente tenetur nihil praesentium suscipit unde earum provident impedit!', 'Minus aliquid maxime', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(33, 'fra', 'Maiores, repellat officiis consectetur nisi impedit dolor possimus natus quo a iste. Non, veritatis explicabo earum magnam quibusdam quasi illo eaque iste? Similique, iste, a dolor sed eaque doloremque. Quam, nesciunt officia voluptate adipisci accusantium iusto excepturi voluptatum impedit! Dolorem totam id nesciunt asperiores.', 'Praesentium adipisci aperiam', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(34, 'fra', 'A, sapiente amet molestiae dignissimos numquam quas qui cum beatae expedita eaque dolor magni quibusdam repudiandae velit soluta esse fuga atque quidem.', 'Consequuntur', 'Totam, quisquam, blanditiis fugiat ex laudantium necessitatibus', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(35, 'fra', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium, modi, consequatur quidem voluptate delectus id fugiat quaerat illum maiores molestiae harum nam natus consectetur sequi a tempora amet rem vitae!', 'Tots els extres', 'Tots els extres descripcio curta', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(41, 'fra', 'Totam, quisquam, blanditiis fugiat ex laudantium necessitatibus eaque minus libero adipisci dolorem ipsam magnam hic quam? Consequuntur, odio rem praesentium nemo eveniet.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium, quam, omnis, officia error quae consequuntur tempora mollitia numquam debitis deleniti harum aspernatur non consequatur natus ut cupiditate quisquam eaque fugiat!', 'Dolorem soluta magnam esse dolorum', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(42, 'fra', 'At, autem omnis veritatis. Repellat, esse, asperiores, voluptatum nesciunt tempore nisi praesentium assumenda inventore temporibus rem tempora cum consectetur nam est ratione. Ex, excepturi laudantium quo possimus consequuntur neque iusto assumenda culpa eius asperiores soluta sapiente tenetur nihil praesentium suscipit unde earum provident impedit!', 'Minus aliquid', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(43, 'fra', 'Maiores, repellat officiis consectetur nisi impedit dolor possimus natus quo a iste. Non, veritatis explicabo earum magnam quibusdam quasi illo eaque iste? Similique, iste, a dolor sed eaque doloremque. Quam, nesciunt officia voluptate adipisci accusantium iusto excepturi voluptatum impedit! Dolorem totam id nesciunt asperiores.', 'Praesentium adipisci aperiam', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(44, 'fra', 'A, sapiente amet molestiae dignissimos numquam quas qui cum beatae expedita eaque dolor magni quibusdam repudiandae velit soluta esse fuga atque quidem.', 'Consequuntur', 'Totam, quisquam, blanditiis fugiat ex laudantium necessitatibus', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(45, 'fra', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium, modi, consequatur quidem voluptate delectus id fugiat quaerat illum maiores molestiae harum nam natus consectetur sequi a tempora amet rem vitae!', 'Tots els extres', 'Tots els extres descripcio curta', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(11, 'eng', 'Totam, quisquam, blanditiis fugiat ex laudantium necessitatibus eaque minus libero adipisci dolorem ipsam magnam hic quam? Consequuntur, odio rem praesentium nemo eveniet.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium, quam, omnis, officia error quae consequuntur tempora mollitia numquam debitis deleniti harum aspernatur non consequatur natus ut cupiditate quisquam eaque fugiat!', 'Dolorem soluta magnam esse dolorum', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(12, 'eng', 'At, autem omnis veritatis. Repellat, esse, asperiores, voluptatum nesciunt tempore nisi praesentium assumenda inventore temporibus rem tempora cum consectetur nam est ratione. Ex, excepturi laudantium quo possimus consequuntur neque iusto assumenda culpa eius asperiores soluta sapiente tenetur nihil praesentium suscipit unde earum provident impedit!', 'Minus aliquid maxime molestiae voluptate', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(13, 'eng', 'Maiores, repellat officiis consectetur nisi impedit dolor possimus natus quo a iste. Non, veritatis explicabo earum magnam quibusdam quasi illo eaque iste? Similique, iste, a dolor sed eaque doloremque. Quam, nesciunt officia voluptate adipisci accusantium iusto excepturi voluptatum impedit! Dolorem totam id nesciunt asperiores.', 'Praesentium', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(14, 'eng', 'A, sapiente amet molestiae dignissimos numquam quas qui cum beatae expedita eaque dolor magni quibusdam repudiandae velit soluta esse fuga atque quidem.', 'Consequuntur odio rem praesentium', 'Totam, quisquam, blanditiis fugiat ex laudantium necessitatibus', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(15, 'eng', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium, modi, consequatur quidem voluptate delectus id fugiat quaerat illum maiores molestiae harum nam natus consectetur sequi a tempora amet rem vitae!', 'Tots els extres', 'Tots els extres descripcio curta', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(21, 'eng', 'Totam, quisquam, blanditiis fugiat ex laudantium necessitatibus eaque minus libero adipisci dolorem ipsam magnam hic quam? Consequuntur, odio rem praesentium nemo eveniet.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium, quam, omnis, officia error quae consequuntur tempora mollitia numquam debitis deleniti harum aspernatur non consequatur natus ut cupiditate quisquam eaque fugiat!', 'Dolorem soluta magnam esse dolorum', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(22, 'eng', 'At, autem omnis veritatis. Repellat, esse, asperiores, voluptatum nesciunt tempore nisi praesentium assumenda inventore temporibus rem tempora cum consectetur nam est ratione. Ex, excepturi laudantium quo possimus consequuntur neque iusto assumenda culpa eius asperiores soluta sapiente tenetur nihil praesentium suscipit unde earum provident impedit!', 'Minus aliquid', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(23, 'eng', 'Maiores, repellat officiis consectetur nisi impedit dolor possimus natus quo a iste. Non, veritatis explicabo earum magnam quibusdam quasi illo eaque iste? Similique, iste, a dolor sed eaque doloremque. Quam, nesciunt officia voluptate adipisci accusantium iusto excepturi voluptatum impedit! Dolorem totam id nesciunt asperiores.', 'Praesentium adipisci aperiam', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(24, 'eng', 'A, sapiente amet molestiae dignissimos numquam quas qui cum beatae expedita eaque dolor magni quibusdam repudiandae velit soluta esse fuga atque quidem.', 'Consequuntur odio rem praesentium', 'Totam, quisquam, blanditiis fugiat ex laudantium necessitatibus', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(25, 'eng', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium, modi, consequatur quidem voluptate delectus id fugiat quaerat illum maiores molestiae harum nam natus consectetur sequi a tempora amet rem vitae!', 'Tots els extres', 'Tots els extres descripcio curta', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(31, 'eng', 'Totam, quisquam, blanditiis fugiat ex laudantium necessitatibus eaque minus libero adipisci dolorem ipsam magnam hic quam? Consequuntur, odio rem praesentium nemo eveniet.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium, quam, omnis, officia error quae consequuntur tempora mollitia numquam debitis deleniti harum aspernatur non consequatur natus ut cupiditate quisquam eaque fugiat!', 'Dolorem soluta', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(32, 'eng', 'At, autem omnis veritatis. Repellat, esse, asperiores, voluptatum nesciunt tempore nisi praesentium assumenda inventore temporibus rem tempora cum consectetur nam est ratione. Ex, excepturi laudantium quo possimus consequuntur neque iusto assumenda culpa eius asperiores soluta sapiente tenetur nihil praesentium suscipit unde earum provident impedit!', 'Minus aliquid maxime molestiae voluptate', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(33, 'eng', 'Maiores, repellat officiis consectetur nisi impedit dolor possimus natus quo a iste. Non, veritatis explicabo earum magnam quibusdam quasi illo eaque iste? Similique, iste, a dolor sed eaque doloremque. Quam, nesciunt officia voluptate adipisci accusantium iusto excepturi voluptatum impedit! Dolorem totam id nesciunt asperiores.', 'Praesentium adipisci aperiam', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(34, 'eng', 'A, sapiente amet molestiae dignissimos numquam quas qui cum beatae expedita eaque dolor magni quibusdam repudiandae velit soluta esse fuga atque quidem.', 'Consequuntur', 'Totam, quisquam, blanditiis fugiat ex laudantium necessitatibus', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(35, 'eng', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium, modi, consequatur quidem voluptate delectus id fugiat quaerat illum maiores molestiae harum nam natus consectetur sequi a tempora amet rem vitae!', 'Tots els extres', 'Tots els extres descripcio curta', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(41, 'eng', 'Totam, quisquam, blanditiis fugiat ex laudantium necessitatibus eaque minus libero adipisci dolorem ipsam magnam hic quam? Consequuntur, odio rem praesentium nemo eveniet.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium, quam, omnis, officia error quae consequuntur tempora mollitia numquam debitis deleniti harum aspernatur non consequatur natus ut cupiditate quisquam eaque fugiat!', 'Dolorem soluta magnam esse dolorum', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(42, 'eng', 'At, autem omnis veritatis. Repellat, esse, asperiores, voluptatum nesciunt tempore nisi praesentium assumenda inventore temporibus rem tempora cum consectetur nam est ratione. Ex, excepturi laudantium quo possimus consequuntur neque iusto assumenda culpa eius asperiores soluta sapiente tenetur nihil praesentium suscipit unde earum provident impedit!', 'Minus aliquid maxime molestiae voluptate', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(43, 'eng', 'Maiores, repellat officiis consectetur nisi impedit dolor possimus natus quo a iste. Non, veritatis explicabo earum magnam quibusdam quasi illo eaque iste? Similique, iste, a dolor sed eaque doloremque. Quam, nesciunt officia voluptate adipisci accusantium iusto excepturi voluptatum impedit! Dolorem totam id nesciunt asperiores.', 'Praesentium adipisci aperiam', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(44, 'eng', 'A, sapiente amet molestiae dignissimos numquam quas qui cum beatae expedita eaque dolor magni quibusdam repudiandae velit soluta esse fuga atque quidem.', 'Consequuntur odio rem praesentium', 'Totam, quisquam, blanditiis fugiat ex laudantium necessitatibus', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(45, 'eng', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium, modi, consequatur quidem voluptate delectus id fugiat quaerat illum maiores molestiae harum nam natus consectetur sequi a tempora amet rem vitae!', 'Tots els extres', 'Tots els extres descripcio curta', '', '', '', '', '', '', '', '', '', '', '', '', '');
/*!40000 ALTER TABLE `inmo__property_language` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `inmo__property_to_portal` (
  `property_portal_id` int(11) NOT NULL AUTO_INCREMENT,
  `property_id` int(11) NOT NULL DEFAULT '0',
  `apicat` tinyint(1) NOT NULL DEFAULT '0',
  `fotocasa` tinyint(1) NOT NULL DEFAULT '0',
  `ceigrup` tinyint(1) NOT NULL DEFAULT '0',
  `habitaclia` tinyint(1) NOT NULL DEFAULT '0',
  `vreasy` tinyint(1) NOT NULL DEFAULT '0',
  `synced` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`property_portal_id`),
  KEY `property_id` (`property_id`),
  KEY `apicat` (`apicat`),
  KEY `fotocasa` (`fotocasa`),
  KEY `ceigrup` (`ceigrup`),
  KEY `vreasy` (`vreasy`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `inmo__property_to_portal` DISABLE KEYS */;
/*!40000 ALTER TABLE `inmo__property_to_portal` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `inmo__property_to_tv` (
  `property_id` int(11) NOT NULL DEFAULT '0',
  `tv_id` int(11) NOT NULL DEFAULT '1',
  KEY `property_id` (`property_id`),
  KEY `tv_id` (`tv_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `inmo__property_to_tv` DISABLE KEYS */;
/*!40000 ALTER TABLE `inmo__property_to_tv` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `inmo__property_to_vreasy` (
  `property_vreasy_id` int(11) NOT NULL AUTO_INCREMENT,
  `property_id` int(11) NOT NULL DEFAULT '0',
  `vreasy_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`property_vreasy_id`),
  KEY `property_id` (`property_id`),
  KEY `vreasy_id` (`vreasy_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `inmo__property_to_vreasy` DISABLE KEYS */;
/*!40000 ALTER TABLE `inmo__property_to_vreasy` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `inmo__property_video` (
  `video_id` int(11) NOT NULL AUTO_INCREMENT,
  `property_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(100) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `size` int(11) NOT NULL DEFAULT '0',
  `ordre` int(11) NOT NULL DEFAULT '0',
  `main_video` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`video_id`),
  KEY `property_id` (`property_id`),
  KEY `main_video` (`main_video`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `inmo__property_video` DISABLE KEYS */;
/*!40000 ALTER TABLE `inmo__property_video` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `inmo__provincia` (
  `provincia_id` int(11) NOT NULL AUTO_INCREMENT,
  `provincia` varchar(100) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `country_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`provincia_id`),
  KEY `country_id` (`country_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `inmo__provincia` DISABLE KEYS */;
INSERT INTO `inmo__provincia` (`provincia_id`, `provincia`, `country_id`) VALUES
	(8, 'Barcelona', 1),
	(17, 'Girona', 1),
	(25, 'Lleida', 1),
	(43, 'Tarragona', 1);
/*!40000 ALTER TABLE `inmo__provincia` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `inmo__zone` (
  `zone_id` int(11) NOT NULL AUTO_INCREMENT,
  `municipi_id` int(11) NOT NULL DEFAULT '0',
  `zone` varchar(100) COLLATE utf8_spanish2_ci NOT NULL,
  PRIMARY KEY (`zone_id`),
  KEY `municipi_id` (`municipi_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `inmo__zone` DISABLE KEYS */;
/*!40000 ALTER TABLE `inmo__zone` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `missatge__missatge` (
  `missatge_id` int(11) NOT NULL AUTO_INCREMENT,
  `status` enum('prepare','waiting','send','readed') COLLATE utf8_spanish2_ci NOT NULL,
  `kind` enum('mls','general','letnd') COLLATE utf8_spanish2_ci DEFAULT 'general',
  `mls_from_id` int(11) NOT NULL,
  `mls_to_id` int(11) NOT NULL,
  `subject` varchar(250) COLLATE utf8_spanish2_ci NOT NULL,
  `missatge` text COLLATE utf8_spanish2_ci NOT NULL,
  `blocked` int(1) NOT NULL DEFAULT '0',
  `entered` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `sent` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link` varchar(250) COLLATE utf8_spanish2_ci NOT NULL,
  `link_text` varchar(250) COLLATE utf8_spanish2_ci NOT NULL,
  `creator_user_id` int(11) NOT NULL DEFAULT '0',
  `top_reply_id` int(11) NOT NULL DEFAULT '0',
  `parent_reply_id` int(11) NOT NULL DEFAULT '0',
  `bin` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`missatge_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `missatge__missatge` DISABLE KEYS */;
INSERT INTO `missatge__missatge` (`missatge_id`, `status`, `kind`, `mls_from_id`, `mls_to_id`, `subject`, `missatge`, `blocked`, `entered`, `modified`, `sent`, `link`, `link_text`, `creator_user_id`, `top_reply_id`, `parent_reply_id`, `bin`) VALUES
	(1, 'send', 'general', 0, 0, 'Actualització portal apicat', 'Per poder publicar immobles en el portal "api.cat", sol·liciteu-nos informació a <a href="mailto:info@letnd.com?subject=Informacio publicació portal api.cat">info@letnd.com</a>', 0, '2016-03-03 12:27:00', '2016-03-03 12:27:00', '2016-03-03 12:27:00', '', '', 0, 0, 0, 0);
/*!40000 ALTER TABLE `missatge__missatge` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `missatge__missatge_read_to_user` (
  `missatge_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  KEY `missatge_id` (`missatge_id`),
  KEY `user_id` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `missatge__missatge_read_to_user` DISABLE KEYS */;
INSERT INTO `missatge__missatge_read_to_user` (`missatge_id`, `user_id`) VALUES
	(1, 1);
/*!40000 ALTER TABLE `missatge__missatge_read_to_user` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `missatge__missatge_to_group` (
  `missatge_id` int(11) NOT NULL DEFAULT '0',
  `group_id` int(11) NOT NULL DEFAULT '0',
  KEY `missatge_id` (`missatge_id`),
  KEY `group_id` (`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `missatge__missatge_to_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `missatge__missatge_to_group` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `missatge__missatge_to_user` (
  `missatge_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  KEY `missatge_id` (`missatge_id`),
  KEY `user_id` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `missatge__missatge_to_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `missatge__missatge_to_user` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `newsletter__configadmin` (
  `name` varchar(50) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `value` varchar(255) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `configadmin_id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`configadmin_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `newsletter__configadmin` DISABLE KEYS */;
INSERT INTO `newsletter__configadmin` (`name`, `value`, `configadmin_id`) VALUES
	('from_address', '', 1),
	('from_name', '', 2),
	('reply_to_name', '', 4),
	('reply_to', '', 3),
	('subject', '', 5),
	('format', 'html', 6),
	('mail_method', 'SMTP', 7),
	('templatetext_id', '0', 9),
	('templatehtml_id', '1', 10),
	('send_method', 'one_by_one', 8),
	('only_test', '0', 11),
	('m_host', 'mail.letnd.com', 12),
	('m_username', 'marc@letnd.com', 13),
	('m_password', 'tanreb', 14),
	('custumer_table', 'custumer__custumer', 15),
	('content_extra', '', 16),
	('separate_content_extra', '1', 17),
	('default_group_admin', '0', 18),
	('default_group_public', '1', 19),
	('mail_auto_inline_css', '1', 20),
	('max_sends_per_message', '1500', 21);
/*!40000 ALTER TABLE `newsletter__configadmin` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `newsletter__custumer` (
  `custumer_id` int(11) NOT NULL AUTO_INCREMENT,
  `remove_key` char(50) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '0',
  `removed` tinyint(1) NOT NULL DEFAULT '0',
  `name` varchar(50) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `surname1` varchar(50) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `surname2` varchar(50) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `vat` varchar(50) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `phone1` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `phone2` varchar(50) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `phone3` varchar(50) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `numstreet` varchar(10) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `block` varchar(10) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `flat` varchar(10) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `door` varchar(10) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `town` varchar(50) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `province` varchar(50) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `country` varchar(50) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `zip` varchar(10) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `mail` varchar(50) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `observations1` text COLLATE utf8_spanish2_ci,
  `prefered_language` char(3) COLLATE utf8_spanish2_ci NOT NULL DEFAULT 'cat',
  `bin` int(1) NOT NULL DEFAULT '0',
  `enteredc` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `adress` text COLLATE utf8_spanish2_ci,
  PRIMARY KEY (`custumer_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `newsletter__custumer` DISABLE KEYS */;
/*!40000 ALTER TABLE `newsletter__custumer` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `newsletter__custumer_to_group` (
  `custumer_id` int(11) NOT NULL DEFAULT '0',
  `group_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `newsletter__custumer_to_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `newsletter__custumer_to_group` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `newsletter__group` (
  `group_id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `newsletter__group` DISABLE KEYS */;
INSERT INTO `newsletter__group` (`group_id`) VALUES
	(1),
	(2);
/*!40000 ALTER TABLE `newsletter__group` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `newsletter__group_language` (
  `group_id` int(11) NOT NULL DEFAULT '0',
  `language` char(3) COLLATE utf8_spanish2_ci NOT NULL,
  `group_name` varchar(100) COLLATE utf8_spanish2_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `newsletter__group_language` DISABLE KEYS */;
INSERT INTO `newsletter__group_language` (`group_id`, `language`, `group_name`, `description`) VALUES
	(1, 'cat', 'Web', ''),
	(1, 'spa', 'Web', ''),
	(1, 'eng', 'Web', ''),
	(1, 'fra', 'Web', ''),
	(2, 'cat', 'Test', ''),
	(2, 'spa', 'Test', ''),
	(2, 'eng', 'Test', ''),
	(2, 'fra', 'Test', '');
/*!40000 ALTER TABLE `newsletter__group_language` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `newsletter__message` (
  `message_id` int(11) NOT NULL AUTO_INCREMENT,
  `from_address` varchar(255) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `from_name` varchar(255) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `reply_to_name` varchar(255) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `reply_to` varchar(255) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `subject` varchar(255) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `message` text COLLATE utf8_spanish2_ci NOT NULL,
  `entered` datetime DEFAULT NULL,
  `modified` datetime DEFAULT '0000-00-00 00:00:00',
  `sent` datetime DEFAULT NULL,
  `sendstart` datetime DEFAULT NULL,
  `format` enum('text','html','html-text') COLLATE utf8_spanish2_ci DEFAULT NULL,
  `send_method` enum('CC','BCC','one_by_one') COLLATE utf8_spanish2_ci DEFAULT NULL,
  `status` enum('sent','saved','canceled','error0','error1','error2') COLLATE utf8_spanish2_ci DEFAULT NULL,
  `language_id` varchar(3) COLLATE utf8_spanish2_ci NOT NULL,
  `send_to_same_language_only` tinyint(1) NOT NULL DEFAULT '0',
  `send_to_no_language_too` tinyint(1) NOT NULL DEFAULT '0',
  `templatehtml_id` int(11) NOT NULL DEFAULT '0',
  `templatetext_id` int(11) NOT NULL DEFAULT '0',
  `bouncecount` int(11) DEFAULT '0',
  `bin` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`message_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `newsletter__message` DISABLE KEYS */;
/*!40000 ALTER TABLE `newsletter__message` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `newsletter__message_file` (
  `file_id` int(11) NOT NULL AUTO_INCREMENT,
  `message_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `name_original` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `size` int(11) NOT NULL DEFAULT '0',
  `ordre` int(11) NOT NULL DEFAULT '0',
  `main_image` int(1) NOT NULL DEFAULT '0',
  `filecat_id` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`file_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `newsletter__message_file` DISABLE KEYS */;
/*!40000 ALTER TABLE `newsletter__message_file` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `newsletter__message_picture` (
  `picture_id` int(11) NOT NULL AUTO_INCREMENT,
  `message_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `name_original` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `size` int(11) NOT NULL DEFAULT '0',
  `ordre` int(11) NOT NULL DEFAULT '0',
  `main_image` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`picture_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `newsletter__message_picture` DISABLE KEYS */;
/*!40000 ALTER TABLE `newsletter__message_picture` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `newsletter__message_to_content` (
  `message_content_id` int(11) NOT NULL AUTO_INCREMENT,
  `tool` varchar(100) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `section` varchar(100) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `action` varchar(100) COLLATE utf8_spanish2_ci NOT NULL,
  `content_id` int(11) NOT NULL DEFAULT '0',
  `message_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`message_content_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `newsletter__message_to_content` DISABLE KEYS */;
/*!40000 ALTER TABLE `newsletter__message_to_content` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `newsletter__message_to_custumer` (
  `message_address_id` int(11) NOT NULL AUTO_INCREMENT,
  `message_id` int(11) NOT NULL DEFAULT '0',
  `custumer_id` varchar(100) COLLATE utf8_spanish2_ci NOT NULL,
  `sent` int(1) NOT NULL DEFAULT '0',
  `manually_selected` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`message_address_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `newsletter__message_to_custumer` DISABLE KEYS */;
/*!40000 ALTER TABLE `newsletter__message_to_custumer` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `newsletter__message_to_group` (
  `message_id` int(11) NOT NULL DEFAULT '0',
  `group_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `newsletter__message_to_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `newsletter__message_to_group` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `newsletter__templatehtml` (
  `templatehtml_id` int(11) NOT NULL AUTO_INCREMENT,
  `file_name` varchar(100) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`templatehtml_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `newsletter__templatehtml` DISABLE KEYS */;
INSERT INTO `newsletter__templatehtml` (`templatehtml_id`, `file_name`) VALUES
	(1, 'message.tpl'),
	(2, 'message2.tpl'),
	(3, 'message3.tpl');
/*!40000 ALTER TABLE `newsletter__templatehtml` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `newsletter__templatehtml_language` (
  `templatehtml_id` int(11) NOT NULL DEFAULT '0',
  `language` varchar(3) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `templatehtml` varchar(100) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `description` varchar(255) COLLATE utf8_spanish2_ci NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `newsletter__templatehtml_language` DISABLE KEYS */;
INSERT INTO `newsletter__templatehtml_language` (`templatehtml_id`, `language`, `templatehtml`, `description`) VALUES
	(1, 'cat', 'Imatges al costat', ''),
	(1, 'spa', 'Imágenes al lado', ''),
	(1, 'eng', 'Imatges al costat', ''),
	(2, 'cat', 'Imatges a sota', ''),
	(2, 'spa', 'Imágenes debajo', ''),
	(2, 'eng', 'Imatges a sota', ''),
	(3, 'cat', 'Imatges a sota 2 cols', ''),
	(3, 'spa', 'Imágenes debajo 2 cols', ''),
	(3, 'eng', 'Imatges a sota 2 cols', ''),
	(1, 'fra', '', ''),
	(2, 'fra', '', ''),
	(3, 'fra', '', '');
/*!40000 ALTER TABLE `newsletter__templatehtml_language` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `newsletter__templatetext` (
  `templatetext_id` int(11) NOT NULL AUTO_INCREMENT,
  `header` text COLLATE utf8_spanish2_ci NOT NULL,
  `footer` text COLLATE utf8_spanish2_ci NOT NULL,
  `template` varchar(100) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`templatetext_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `newsletter__templatetext` DISABLE KEYS */;
/*!40000 ALTER TABLE `newsletter__templatetext` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `newsletter__templatetext_language` (
  `templatetext_id` int(11) NOT NULL DEFAULT '0',
  `language` varchar(3) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `templatetext` varchar(100) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `description` varchar(255) COLLATE utf8_spanish2_ci NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `newsletter__templatetext_language` DISABLE KEYS */;
/*!40000 ALTER TABLE `newsletter__templatetext_language` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `news__category` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `listtpl_id` int(11) NOT NULL DEFAULT '1',
  `ref_code` varchar(10) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `ordre` int(11) NOT NULL DEFAULT '0',
  `blocked` tinyint(1) NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`category_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `news__category` DISABLE KEYS */;
INSERT INTO `news__category` (`category_id`, `listtpl_id`, `ref_code`, `ordre`, `blocked`, `active`) VALUES
	(1, 1, '', 0, 0, 1);
/*!40000 ALTER TABLE `news__category` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `news__category_language` (
  `category_id` int(11) NOT NULL DEFAULT '0',
  `language` char(3) COLLATE utf8_spanish2_ci NOT NULL,
  `category` varchar(100) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `news__category_language` DISABLE KEYS */;
INSERT INTO `news__category_language` (`category_id`, `language`, `category`) VALUES
	(1, 'cat', 'General'),
	(1, 'spa', 'General'),
	(1, 'eng', 'General'),
	(1, 'fra', 'Général');
/*!40000 ALTER TABLE `news__category_language` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `news__configadmin` (
  `configadmin_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `value` varchar(255) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `language` char(3) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`configadmin_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `news__configadmin` DISABLE KEYS */;
INSERT INTO `news__configadmin` (`configadmin_id`, `name`, `value`, `language`) VALUES
	(1, 'im_list_cols', '4', ''),
	(12, 'im_details_q', '80', ''),
	(11, 'im_thumb_q', '80', ''),
	(10, 'im_admin_thumb_h', '133', ''),
	(9, 'im_admin_thumb_w', '200', ''),
	(8, 'im_thumb_h', '200', ''),
	(7, 'im_thumb_w', '300', ''),
	(6, 'im_details_h', '360', ''),
	(5, 'im_details_w', '540', ''),
	(4, 'im_admin_thumb_q', '80', ''),
	(3, 'im_big_h', '800', ''),
	(2, 'im_big_w', '1200', ''),
	(14, 'im_medium_h', '640', ''),
	(13, 'im_medium_w', '960', ''),
	(15, 'im_medium_q', '80', ''),
	(16, 'im_custom_ratios', 'Imatge Slider=2.4;', ''),
	(17, 'videoframe_info_size', 'Ample: 400 px', ''),
	(18, 'has_files_show_home', '0', '');
/*!40000 ALTER TABLE `news__configadmin` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `news__configpublic` (
  `configadmin_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `value` varchar(255) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `language` char(3) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`configadmin_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `news__configpublic` DISABLE KEYS */;
INSERT INTO `news__configpublic` (`configadmin_id`, `name`, `value`, `language`) VALUES
	(1, 'im_list_cols', '4', ''),
	(17, 'block_max_results', '3', ''),
	(16, 'show_entered_time', '0', ''),
	(19, 'add_dots_length', '300', ''),
	(20, 'ignore_publish_date', '0', ''),
	(21, 'default_category_id', '', ''),
	(22, 'home_list_all', '1', '');
/*!40000 ALTER TABLE `news__configpublic` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `news__formtpl` (
  `formtpl_id` int(11) NOT NULL AUTO_INCREMENT,
  `file_name` varchar(100) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `ordre` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`formtpl_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `news__formtpl` DISABLE KEYS */;
/*!40000 ALTER TABLE `news__formtpl` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `news__formtpl_language` (
  `formtpl_id` int(11) NOT NULL DEFAULT '0',
  `language` varchar(3) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `formtpl` varchar(100) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `description` varchar(255) COLLATE utf8_spanish2_ci NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `news__formtpl_language` DISABLE KEYS */;
/*!40000 ALTER TABLE `news__formtpl_language` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `news__listtpl` (
  `listtpl_id` int(11) NOT NULL AUTO_INCREMENT,
  `file_name` varchar(100) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `ordre` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`listtpl_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `news__listtpl` DISABLE KEYS */;
/*!40000 ALTER TABLE `news__listtpl` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `news__listtpl_language` (
  `listtpl_id` int(11) NOT NULL DEFAULT '0',
  `language` varchar(3) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `listtpl` varchar(100) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `description` varchar(255) COLLATE utf8_spanish2_ci NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `news__listtpl_language` DISABLE KEYS */;
/*!40000 ALTER TABLE `news__listtpl_language` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `news__new` (
  `new_id` int(11) NOT NULL AUTO_INCREMENT,
  `videoframe` text COLLATE utf8_spanish2_ci NOT NULL,
  `ordre` int(11) NOT NULL DEFAULT '0',
  `entered` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `expired` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` enum('prepare','review','public','archived') COLLATE utf8_spanish2_ci NOT NULL DEFAULT 'prepare',
  `in_home` tinyint(1) NOT NULL DEFAULT '1',
  `destacat` tinyint(1) NOT NULL DEFAULT '0',
  `category_id` int(11) NOT NULL,
  `formtpl_id` int(11) NOT NULL DEFAULT '1',
  `bin` tinyint(4) NOT NULL DEFAULT '0',
  `facebook_post_id` varchar(255) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `facebook_image_id` varchar(255) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `facebook_publish_id` varchar(255) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `facebook_letnd_user_id` varchar(255) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `facebook_user_id` text COLLATE utf8_spanish2_ci NOT NULL,
  `twitter_status_id` varchar(255) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `twitter_letnd_user_id` varchar(255) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `twitter_user_id` text COLLATE utf8_spanish2_ci NOT NULL,
  PRIMARY KEY (`new_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `news__new` DISABLE KEYS */;
INSERT INTO `news__new` (`new_id`, `videoframe`, `ordre`, `entered`, `modified`, `expired`, `status`, `in_home`, `destacat`, `category_id`, `formtpl_id`, `bin`, `facebook_post_id`, `facebook_image_id`, `facebook_publish_id`, `facebook_letnd_user_id`, `facebook_user_id`, `twitter_status_id`, `twitter_letnd_user_id`, `twitter_user_id`) VALUES
	(4, '', 0, '2011-01-19 08:16:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'public', 1, 0, 1, 1, 0, '', '', '', '', '', '', '', ''),
	(5, '<iframe src="https://player.vimeo.com/video/164022180" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>', 0, '2011-04-20 08:17:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'public', 1, 0, 1, 1, 0, '', '', '', '', '', '', '', ''),
	(3, '', 0, '2010-12-22 08:11:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'public', 1, 0, 1, 1, 0, '', '', '', '', '', '', '', ''),
	(6, '<iframe src="https://player.vimeo.com/video/164022180" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>', 0, '2011-06-17 08:22:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'public', 1, 1, 1, 1, 0, '', '', '', '', '', '', '', ''),
	(7, '<iframe src="https://player.vimeo.com/video/164022180" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>', 0, '2011-10-06 08:23:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'public', 1, 1, 1, 1, 0, '', '', '', '', '', '', '', ''),
	(8, '<iframe src="https://player.vimeo.com/video/164022180" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>', 0, '2011-10-27 08:26:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'public', 1, 1, 1, 1, 0, '', '', '', '', '', '', '', ''),
	(9, '<iframe src="https://player.vimeo.com/video/164022180" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>', 0, '2012-01-11 08:33:00', '2014-02-20 09:09:10', '0000-00-00 00:00:00', 'public', 1, 1, 1, 1, 0, '', '', '', '', '', '', '', '');
/*!40000 ALTER TABLE `news__new` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `news__new_file` (
  `file_id` int(11) NOT NULL AUTO_INCREMENT,
  `new_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `name_original` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `size` int(11) NOT NULL,
  `ordre` int(11) NOT NULL,
  `main_file` int(1) NOT NULL,
  `show_home` tinyint(1) NOT NULL DEFAULT '0',
  `filecat_id` int(11) NOT NULL DEFAULT '1',
  `public` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`file_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `news__new_file` DISABLE KEYS */;
INSERT INTO `news__new_file` (`file_id`, `new_id`, `name`, `name_original`, `title`, `size`, `ordre`, `main_file`, `show_home`, `filecat_id`, `public`) VALUES
	(1, 9, 'Document  catala.docx', 'Document  catala.docx', 'Document  catala.docx', 0, 0, 0, 0, 1, 1),
	(2, 9, 'Document  castella.docx', 'Document  castella.docx', 'Document  castella.docx', 0, 1, 0, 0, 1, 1);
/*!40000 ALTER TABLE `news__new_file` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `news__new_image` (
  `image_id` int(11) NOT NULL AUTO_INCREMENT,
  `new_id` int(11) NOT NULL DEFAULT '0',
  `imagecat_id` int(11) NOT NULL DEFAULT '1',
  `name` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `name_original` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `size` int(11) NOT NULL DEFAULT '0',
  `ordre` int(11) NOT NULL DEFAULT '0',
  `main_image` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`image_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `news__new_image` DISABLE KEYS */;
INSERT INTO `news__new_image` (`image_id`, `new_id`, `imagecat_id`, `name`, `name_original`, `size`, `ordre`, `main_image`) VALUES
	(13, 4, 1, 'fotos-naturaleza-mariposas.jpg', 'fotos-naturaleza-mariposas.jpg', 71298, 0, 1),
	(12, 5, 1, 'fotos-mariposas-flores.jpg', 'fotos-mariposas-flores.jpg', 93690, 0, 1),
	(11, 6, 1, 'fotos-mariposas-alas-transparentes.jpg', 'fotos-mariposas-alas-transparentes.jpg', 65670, 0, 1),
	(10, 7, 1, 'fotos-mariposas.jpg', 'fotos-mariposas.jpg', 57702, 0, 1),
	(9, 8, 1, 'fotos-mariposa-azul.jpg', 'fotos-mariposa-azul.jpg', 120124, 0, 1),
	(8, 9, 1, 'fotos-flores-26.jpg', 'fotos-flores-26.jpg', 60540, 0, 1),
	(14, 3, 1, 'flores-25.jpg', 'flores-25.jpg', 75907, 0, 1);
/*!40000 ALTER TABLE `news__new_image` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `news__new_imagecat` (
  `imagecat_id` int(11) NOT NULL AUTO_INCREMENT,
  `imagecat` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `ordre` int(11) DEFAULT NULL,
  PRIMARY KEY (`imagecat_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `news__new_imagecat` DISABLE KEYS */;
INSERT INTO `news__new_imagecat` (`imagecat_id`, `imagecat`, `ordre`) VALUES
	(1, 'Fitxa i llistat', NULL),
	(2, 'Home', NULL);
/*!40000 ALTER TABLE `news__new_imagecat` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `news__new_image_language` (
  `image_id` int(11) NOT NULL DEFAULT '0',
  `language` char(3) COLLATE utf8_spanish2_ci NOT NULL,
  `image_alt` varchar(150) COLLATE utf8_spanish2_ci NOT NULL,
  `image_title` varchar(255) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `news__new_image_language` DISABLE KEYS */;
INSERT INTO `news__new_image_language` (`image_id`, `language`, `image_alt`, `image_title`) VALUES
	(14, 'fra', '', ''),
	(14, 'eng', '', ''),
	(14, 'spa', '', ''),
	(14, 'cat', '', ''),
	(13, 'fra', '', ''),
	(13, 'eng', '', ''),
	(13, 'spa', '', ''),
	(13, 'cat', '', ''),
	(12, 'fra', '', ''),
	(12, 'eng', '', ''),
	(12, 'spa', '', ''),
	(12, 'cat', '', ''),
	(11, 'fra', '', ''),
	(11, 'eng', '', ''),
	(11, 'spa', '', ''),
	(11, 'cat', '', ''),
	(10, 'fra', '', ''),
	(10, 'eng', '', ''),
	(10, 'spa', '', ''),
	(10, 'cat', '', ''),
	(9, 'fra', '', ''),
	(9, 'eng', '', ''),
	(9, 'spa', '', ''),
	(9, 'cat', '', ''),
	(8, 'fra', '', ''),
	(8, 'eng', '', ''),
	(8, 'spa', '', ''),
	(8, 'cat', '', '');
/*!40000 ALTER TABLE `news__new_image_language` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `news__new_language` (
  `new_id` int(11) NOT NULL DEFAULT '0',
  `language` char(3) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `new` varchar(255) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `subtitle` varchar(255) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `content_list` text COLLATE utf8_spanish2_ci NOT NULL,
  `content` longtext COLLATE utf8_spanish2_ci NOT NULL,
  `url1_name` varchar(100) COLLATE utf8_spanish2_ci NOT NULL,
  `url2_name` varchar(100) COLLATE utf8_spanish2_ci NOT NULL,
  `url3_name` varchar(100) COLLATE utf8_spanish2_ci NOT NULL,
  `url1` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `url2` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `url3` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `page_title` varchar(255) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `page_description` varchar(255) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `page_keywords` varchar(255) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `new_file_name` varchar(100) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `new_old_file_name` varchar(100) COLLATE utf8_spanish2_ci NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `news__new_language` DISABLE KEYS */;
INSERT INTO `news__new_language` (`new_id`, `language`, `new`, `subtitle`, `content_list`, `content`, `url1_name`, `url2_name`, `url3_name`, `url1`, `url2`, `url3`, `page_title`, `page_description`, `page_keywords`, `new_file_name`, `new_old_file_name`) VALUES
	(3, 'cat', 'Lorem ipsum', 'Sed ut perspiciatis unde omnis iste natus error', '', '<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>\r\n<p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</p>\r\n<p>Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</p>', '', '', '', '', '', '', '', '', '', '', ''),
	(3, 'spa', 'Lorem ipsum', 'Sed ut perspiciatis unde omnis iste natus error', '', '<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>\r\n<p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</p>\r\n<p>Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</p>', '', '', '', '', '', '', '', '', '', '', ''),
	(4, 'cat', 'Sed ut perspiciatis unde omnis iste', 'Neque porro quisquam est, qui dolorem ipsum', '', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>', '', '', '', '', '', '', '', '', '', '', ''),
	(4, 'spa', 'Sed ut perspiciatis unde omnis iste', 'Neque porro quisquam est, qui dolorem ipsum', '', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>', '', '', '', '', '', '', '', '', '', '', ''),
	(5, 'cat', 'Excepteur sint occaecat cupidatat dolor in reprehenderit in voluptate', 'Duis aute irure dolor in reprehenderit in voluptate', '', '<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>\r\n<p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</p>\r\n<p>Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</p>', 'Google', 'You tube', '', 'http://www.google.com', 'http://youtube.com', '', '', '', '', '', ''),
	(3, 'eng', 'Lorem ipsum', 'Sed ut perspiciatis unde omnis iste natus error', '', '<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>', '', '', '', '', '', '', '', '', '', '', ''),
	(4, 'eng', 'Sed ut perspiciatis unde omnis iste', 'Neque porro quisquam est, qui dolorem ipsum', '', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>', '', '', '', '', '', '', '', '', '', '', ''),
	(5, 'eng', 'Excepteur sint occaecat cupidatat dolor in reprehenderit in voluptate', 'Duis aute irure dolor in reprehenderit in voluptate', '', '<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>', 'Google', 'You tube', '', 'http://www.google.com', 'http://youtube.com', '', '', '', '', '', ''),
	(6, 'eng', 'Duis aute irure dolor', 'Sed ut perspiciatis unde omnis iste natus error', '', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>', 'Google', 'You tube', '', 'http://www.google.com', 'http://youtube.com', '', '', '', '', '', ''),
	(7, 'eng', 'Quis autem vel eum iure porro quisquam est, qui dolorem ipsum', 'Neque porro quisquam est, qui dolorem ipsum', '', '<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>', 'Google', 'You tube', '', 'http://www.google.com', 'http://youtube.com', '', '', '', '', '', ''),
	(9, 'eng', 'Nemo enim ipsam voluptatem', 'Duis aute irure dolor in reprehenderit in voluptate', '', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>', 'Google', 'You tube', '', 'http://www.google.com', 'http://youtube.com', '', '', '', '', 'noticia', ''),
	(8, 'eng', 'Sed ut perspiciatis', 'Sed ut perspiciatis unde omnis iste natus error', '', '<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>', 'Google', 'You tube', '', 'http://www.google.com', 'http://youtube.com', '', '', '', '', '', ''),
	(5, 'spa', 'Excepteur sint occaecat cupidatat dolor in reprehenderit in voluptate', 'Duis aute irure dolor in reprehenderit in voluptate', '', '<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>\r\n<p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</p>\r\n<p>Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</p>', 'Google', 'You tube', '', 'http://www.google.com', 'http://youtube.com', '', '', '', '', '', ''),
	(6, 'cat', 'Duis aute irure dolor', 'Sed ut perspiciatis unde omnis iste natus error', '', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>', 'Google', 'You tube', '', 'http://www.google.com', 'http://youtube.com', '', '', '', '', '', ''),
	(6, 'spa', 'Duis aute irure dolor', 'Sed ut perspiciatis unde omnis iste natus error', '', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>', 'Google', 'You tube', '', 'http://www.google.com', 'http://youtube.com', '', '', '', '', '', ''),
	(7, 'cat', 'Quis autem vel eum iure porro quisquam est, qui dolorem ipsum', 'Neque porro quisquam est, qui dolorem ipsum', '', '<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>\r\n<p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</p>\r\n<p>Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</p>', 'Google', 'You tube', '', 'http://www.google.com', 'http://youtube.com', '', '', '', '', '', ''),
	(7, 'spa', 'Quis autem vel eum iure porro quisquam est, qui dolorem ipsum', 'Neque porro quisquam est, qui dolorem ipsum', '', '<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>\r\n<p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</p>\r\n<p>Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</p>', 'Google', 'You tube', '', 'http://www.google.com', 'http://youtube.com', '', '', '', '', '', ''),
	(9, 'cat', 'Nemo enim ipsam voluptatem', 'Duis aute irure dolor in reprehenderit in voluptate', '', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>', 'Google', 'You tube', '', 'http://www.google.com', 'http://youtube.com', '', '', '', '', 'noticia', ''),
	(9, 'spa', 'Nemo enim ipsam voluptatem', 'Duis aute irure dolor in reprehenderit in voluptate', '', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>', 'Google', 'You tube', '', 'http://www.google.com', 'http://youtube.com', '', '', '', '', 'noticia', ''),
	(8, 'cat', 'Sed ut perspiciatis', 'Sed ut perspiciatis unde omnis iste natus error', '', '<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>\r\n<p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</p>\r\n<p>Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</p>', 'Google', 'You tube', '', 'http://www.google.com', 'http://youtube.com', '', '', '', '', '', ''),
	(8, 'spa', 'Sed ut perspiciatis', 'Sed ut perspiciatis unde omnis iste natus error', '', '<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>\r\n<p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</p>\r\n<p>Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</p>', 'Google', 'You tube', '', 'http://www.google.com', 'http://youtube.com', '', '', '', '', '', ''),
	(3, 'fra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(4, 'fra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(5, 'fra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(6, 'fra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(7, 'fra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(9, 'fra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(8, 'fra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
/*!40000 ALTER TABLE `news__new_language` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `print__contract` (
  `contract_id` int(11) NOT NULL AUTO_INCREMENT,
  `property_id` int(11) NOT NULL,
  `signed` date NOT NULL,
  `registered` date NOT NULL,
  `rescinded` date NOT NULL,
  `kind_id` int(11) NOT NULL,
  `duration` int(4) NOT NULL,
  `diposit` int(6) NOT NULL,
  `place` varchar(250) COLLATE utf8_spanish2_ci NOT NULL,
  `state` enum('current','canceled') COLLATE utf8_spanish2_ci NOT NULL,
  `language_id` char(3) COLLATE utf8_spanish2_ci NOT NULL,
  `bin` tinyint(1) NOT NULL,
  `contract_observations` text COLLATE utf8_spanish2_ci NOT NULL,
  PRIMARY KEY (`contract_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `print__contract` DISABLE KEYS */;
/*!40000 ALTER TABLE `print__contract` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `print__contract_to_custumer` (
  `contract_to_custumer_id` int(11) NOT NULL AUTO_INCREMENT,
  `contract_id` int(11) NOT NULL DEFAULT '0',
  `custumer_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`contract_to_custumer_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `print__contract_to_custumer` DISABLE KEYS */;
/*!40000 ALTER TABLE `print__contract_to_custumer` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `print__ipc` (
  `ipc_id` int(11) NOT NULL AUTO_INCREMENT,
  `contract_id` int(11) NOT NULL,
  `rise` date NOT NULL,
  `rised` tinyint(1) NOT NULL,
  PRIMARY KEY (`ipc_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `print__ipc` DISABLE KEYS */;
/*!40000 ALTER TABLE `print__ipc` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `print__kind` (
  `kind_id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`kind_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `print__kind` DISABLE KEYS */;
INSERT INTO `print__kind` (`kind_id`) VALUES
	(1),
	(2),
	(3),
	(4),
	(5),
	(6),
	(7),
	(8),
	(9);
/*!40000 ALTER TABLE `print__kind` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `print__kind_language` (
  `kind_id` int(11) NOT NULL,
  `language` char(3) COLLATE utf8_spanish2_ci NOT NULL,
  `kind` varchar(200) COLLATE utf8_spanish2_ci NOT NULL,
  KEY `language` (`language`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `print__kind_language` DISABLE KEYS */;
INSERT INTO `print__kind_language` (`kind_id`, `language`, `kind`) VALUES
	(1, 'cat', 'Arres'),
	(1, 'spa', 'Arras'),
	(1, 'eng', ''),
	(2, 'cat', 'Compravenda'),
	(2, 'spa', 'Compaventa'),
	(2, 'eng', ''),
	(3, 'cat', 'Lloguer habitatge nou'),
	(3, 'spa', 'Alquiler vivienda nueva'),
	(3, 'eng', ''),
	(4, 'cat', 'Lloguer de temporada'),
	(4, 'spa', 'Alquiler de temporadal'),
	(4, 'eng', ''),
	(5, 'cat', 'Lloguer turístic'),
	(5, 'spa', 'Alquiler turístico'),
	(5, 'eng', ''),
	(1, 'fra', ''),
	(2, 'fra', ''),
	(3, 'fra', ''),
	(4, 'fra', ''),
	(5, 'fra', ''),
	(6, 'cat', ''),
	(6, 'spa', ''),
	(6, 'eng', ''),
	(6, 'fra', ''),
	(7, 'cat', ''),
	(7, 'spa', ''),
	(7, 'eng', ''),
	(7, 'fra', ''),
	(8, 'cat', ''),
	(8, 'spa', ''),
	(8, 'eng', ''),
	(8, 'fra', ''),
	(9, 'cat', ''),
	(9, 'spa', ''),
	(9, 'eng', ''),
	(9, 'fra', '');
/*!40000 ALTER TABLE `print__kind_language` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `print__template` (
  `template_id` int(11) NOT NULL AUTO_INCREMENT,
  `file_name` varchar(100) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `type` enum('showwindow','album') COLLATE utf8_spanish2_ci NOT NULL DEFAULT 'showwindow',
  `size` enum('A4','A3','A2','A1','A0','A5') COLLATE utf8_spanish2_ci NOT NULL DEFAULT 'A4',
  `orientation` enum('P','L') COLLATE utf8_spanish2_ci NOT NULL DEFAULT 'P',
  `ignore_config` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`template_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `print__template` DISABLE KEYS */;
/*!40000 ALTER TABLE `print__template` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `print__template_language` (
  `template_id` int(11) NOT NULL DEFAULT '0',
  `language` varchar(3) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `template` varchar(100) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `description` varchar(255) COLLATE utf8_spanish2_ci NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `print__template_language` DISABLE KEYS */;
/*!40000 ALTER TABLE `print__template_language` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `product__address` (
  `address_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `a_name` varchar(100) COLLATE utf8_spanish2_ci NOT NULL,
  `a_surname` varchar(100) COLLATE utf8_spanish2_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `zip` varchar(10) COLLATE utf8_spanish2_ci NOT NULL,
  `city` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `country_id` char(2) COLLATE utf8_spanish2_ci NOT NULL,
  `provincia_id` int(11) NOT NULL,
  `invoice` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`address_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `product__address` DISABLE KEYS */;
INSERT INTO `product__address` (`address_id`, `customer_id`, `a_name`, `a_surname`, `address`, `zip`, `city`, `country_id`, `provincia_id`, `invoice`) VALUES
	(1, 1, '', '', 'Carrer Promer', '17000', 'Girona', 'ES', 3119840, 1);
/*!40000 ALTER TABLE `product__address` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `product__brand` (
  `brand_id` int(11) NOT NULL AUTO_INCREMENT,
  `brand_name` varchar(50) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `ordre` int(11) NOT NULL DEFAULT '0',
  `send_rate_spain` decimal(7,2) NOT NULL DEFAULT '0.00',
  `send_rate_france` decimal(7,2) NOT NULL DEFAULT '0.00',
  `send_rate_andorra` decimal(7,2) NOT NULL DEFAULT '0.00',
  `send_rate_world` decimal(7,2) NOT NULL DEFAULT '0.00',
  `minimum_buy_free_send` decimal(9,2) NOT NULL DEFAULT '0.00',
  `bin` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`brand_id`),
  KEY `bin` (`bin`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `product__brand` DISABLE KEYS */;
/*!40000 ALTER TABLE `product__brand` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `product__brand_image` (
  `image_id` int(11) NOT NULL AUTO_INCREMENT,
  `brand_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `name_original` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `size` int(11) NOT NULL DEFAULT '0',
  `ordre` int(11) NOT NULL DEFAULT '0',
  `main_image` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`image_id`),
  KEY `brand_id` (`brand_id`),
  KEY `main_image` (`main_image`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `product__brand_image` DISABLE KEYS */;
/*!40000 ALTER TABLE `product__brand_image` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `product__brand_image_language` (
  `image_id` int(11) NOT NULL DEFAULT '0',
  `language` char(3) COLLATE utf8_spanish2_ci NOT NULL,
  `image_alt` varchar(150) COLLATE utf8_spanish2_ci NOT NULL,
  `image_title` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  KEY `image_id` (`image_id`),
  KEY `language` (`language`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `product__brand_image_language` DISABLE KEYS */;
/*!40000 ALTER TABLE `product__brand_image_language` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `product__brand_language` (
  `brand_id` int(11) NOT NULL,
  `language` char(3) COLLATE utf8_spanish2_ci NOT NULL,
  `brand_description` text COLLATE utf8_spanish2_ci NOT NULL,
  `page_title` varchar(255) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `page_description` varchar(255) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `page_keywords` varchar(255) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  KEY `brand_id` (`brand_id`),
  KEY `language` (`language`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `product__brand_language` DISABLE KEYS */;
/*!40000 ALTER TABLE `product__brand_language` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `product__color` (
  `color_id` int(11) NOT NULL AUTO_INCREMENT,
  `observations` text COLLATE utf8_spanish2_ci NOT NULL,
  `bin` int(1) NOT NULL,
  PRIMARY KEY (`color_id`),
  KEY `bin` (`bin`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `product__color` DISABLE KEYS */;
/*!40000 ALTER TABLE `product__color` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `product__color_language` (
  `color_id` int(11) NOT NULL,
  `language` char(3) COLLATE utf8_spanish2_ci NOT NULL,
  `color_name` varchar(100) COLLATE utf8_spanish2_ci NOT NULL,
  KEY `color_id` (`color_id`),
  KEY `language` (`language`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `product__color_language` DISABLE KEYS */;
/*!40000 ALTER TABLE `product__color_language` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `product__configadmin` (
  `configadmin_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `value` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  PRIMARY KEY (`configadmin_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `product__configadmin` DISABLE KEYS */;
INSERT INTO `product__configadmin` (`configadmin_id`, `name`, `value`) VALUES
	(2, 'languages', 'cat,spa,eng'),
	(5, 'im_big_w', '1200'),
	(6, 'im_big_h', '800'),
	(7, 'im_admin_thumb_q', '80'),
	(8, 'im_details_w', '560'),
	(9, 'im_details_h', ''),
	(10, 'im_thumb_w', '300'),
	(11, 'im_thumb_h', ''),
	(12, 'im_admin_thumb_w', '200'),
	(13, 'im_admin_thumb_h', '133'),
	(14, 'im_thumb_q', '80'),
	(15, 'im_details_q', '80'),
	(16, 'im_list_cols', '2'),
	(20, 'im_medium_w', '900'),
	(21, 'im_medium_h', '600'),
	(22, 'im_medium_q', '80'),
	(23, 'im_big_q', '70'),
	(24, 'default_sell', '1'),
	(25, 'pvp_required', '1'),
	(49, 'im_custom_ratios', ''),
	(27, 'stock_control', '0'),
	(28, 'allow_null_stock', '0'),
	(29, 'xxx send_rate_spain', '8'),
	(30, 'xxx send_rate_france', '20'),
	(31, 'send_rate_world', '50'),
	(32, 'allow_free_send', '0'),
	(33, 'minimum_buy_free_send', '300'),
	(34, 'others1', ''),
	(35, 'others2', ''),
	(36, 'others3', ''),
	(37, 'others4', ''),
	(39, 'auto_ref', '0'),
	(40, 'watermark', '0'),
	(41, 'watermark_halign', 'center'),
	(120, 'hide_fields', ''),
	(43, 'watermark_valign', 'middle'),
	(45, 'is_public_tax_included', '1'),
	(46, 'is_wholesaler_tax_included', '0'),
	(48, 'has_equivalencia_default', '0'),
	(58, 'equivalencia', '0'),
	(80, 'variation_others1', ''),
	(60, 'videoframe_info_size', 'Ample: 400 px'),
	(61, 'is_sector_on', '1'),
	(62, 'is_sell_on', '1'),
	(63, 'is_digital_product_on', '1'),
	(64, 'is_brand_rate_on', '0'),
	(65, 'xxx send_rate_andorra', '20'),
	(66, 'others1_type', 'text_no_lang'),
	(67, 'others2_type', 'text_no_lang'),
	(68, 'others3_type', 'text_no_lang'),
	(69, 'others4_type', 'text_no_lang'),
	(70, 'others5', ''),
	(71, 'others5_type', 'text_no_lang'),
	(72, 'others6', ''),
	(73, 'others6_type', 'text_no_lang'),
	(74, 'others7', ''),
	(75, 'others7_type', 'text_no_lang'),
	(76, 'others8', ''),
	(77, 'others8_type', 'text_no_lang'),
	(78, 'invoice_prefix', ''),
	(79, 'has_invoice', '0'),
	(81, 'variation_others1_type', 'text_no_lang'),
	(82, 'variation_others2', ''),
	(83, 'variation_others2_type', 'text_no_lang'),
	(84, 'variation_others3', ''),
	(85, 'variation_others3_type', 'text_no_lang'),
	(86, 'variation_others4', ''),
	(87, 'variation_others4_type', 'text_no_lang'),
	(88, 'mail_auto_inline_css', '1'),
	(89, 'qr_width', '2'),
	(90, 'qr_height', '2'),
	(91, 'qr_margin_top', '0.2'),
	(92, 'qr_margin_left', '0.2'),
	(93, 'is_description_html', '0'),
	(94, 'others9', ''),
	(95, 'others9_type', 'text_no_lang'),
	(96, 'others10', ''),
	(97, 'others10_type', 'text_no_lang'),
	(98, 'send_rate_encrease_ondelivery_world', '0'),
	(99, 'others11', ''),
	(100, 'others11_type', 'text_no_lang'),
	(101, 'others12', ''),
	(102, 'others12_type', 'text_no_lang'),
	(103, 'others13', ''),
	(104, 'others13_type', 'text_no_lang'),
	(105, 'others14', ''),
	(106, 'others14_type', 'text_no_lang'),
	(107, 'others15', ''),
	(108, 'others15_type', 'text_no_lang'),
	(109, 'others16', ''),
	(110, 'others16_type', 'text_no_lang'),
	(111, 'others17', ''),
	(112, 'others17_type', 'text_no_lang'),
	(113, 'others18', ''),
	(114, 'others18_type', 'text_no_lang'),
	(115, 'others19', ''),
	(116, 'others19_type', 'text_no_lang'),
	(117, 'others20', ''),
	(118, 'others20_type', 'text_no_lang'),
	(119, 'is_specs_on', '1'),
	(121, 'has_order_date', '0'),
	(122, 'has_family_calendar', '0'),
	(123, 'has_rate', '1'),
	(124, 'show_sector_at_listing', '0'),
	(125, 'others_private1', ''),
	(126, 'others_private1_type', 'checkbox'),
	(127, 'others_private2', ''),
	(128, 'others_private2_type', 'checkbox'),
	(129, 'others_private3', ''),
	(130, 'others_private3_type', 'checkbox'),
	(131, 'has_private_familys', '0');
/*!40000 ALTER TABLE `product__configadmin` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `product__configadmin_family` (
  `configadmin_family_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `value` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  PRIMARY KEY (`configadmin_family_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `product__configadmin_family` DISABLE KEYS */;
INSERT INTO `product__configadmin_family` (`configadmin_family_id`, `name`, `value`) VALUES
	(1, 'im_thumb_w', '200'),
	(2, 'im_thumb_h', '150'),
	(3, 'im_thumb_q', '80'),
	(4, 'im_details_w', '300'),
	(5, 'im_details_h', '225'),
	(6, 'im_details_q', '80'),
	(7, 'im_medium_w', '900'),
	(8, 'im_medium_h', '675'),
	(9, 'im_medium_q', '70'),
	(10, 'im_big_w', '1200'),
	(11, 'im_big_h', '900'),
	(12, 'im_big_q', '70'),
	(13, 'im_list_cols', '2'),
	(14, 'im_custom_ratios', '');
/*!40000 ALTER TABLE `product__configadmin_family` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `product__configadmin_language` (
  `configadmin_id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `value` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `language` char(3) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `product__configadmin_language` DISABLE KEYS */;
INSERT INTO `product__configadmin_language` (`configadmin_id`, `name`, `value`, `language`) VALUES
	(34, 'others1', 'Característica 1', 'cat'),
	(34, 'others1', 'Característica 1', 'spa'),
	(34, 'others1', 'Characteristic 1', 'eng'),
	(34, 'others1', 'Caractéristique 1', 'fra'),
	(35, 'others2', 'Característica 2', 'cat'),
	(35, 'others2', 'Característica 2', 'spa'),
	(35, 'others2', 'Characteristic 2', 'eng'),
	(35, 'others2', 'Caractéristique 2', 'fra'),
	(36, 'others3', 'Característica 3', 'cat'),
	(36, 'others3', 'Característica 3', 'spa'),
	(36, 'others3', 'Characteristic 3', 'eng'),
	(36, 'others3', 'Caractéristique 3', 'fra'),
	(37, 'others4', 'Característica 4', 'cat'),
	(37, 'others4', 'Característica 4', 'spa'),
	(37, 'others4', 'Characteristic 4', 'eng'),
	(37, 'others4', 'Caractéristique 4', 'fra'),
	(0, 'others5', '', 'cat'),
	(0, 'others6', '', 'cat'),
	(0, 'others7', '', 'cat'),
	(0, 'others8', '', 'cat'),
	(0, 'others5', '', 'spa'),
	(0, 'others6', '', 'spa'),
	(0, 'others7', '', 'spa'),
	(0, 'others8', '', 'spa'),
	(0, 'others5', '', 'eng'),
	(0, 'others6', '', 'eng'),
	(0, 'others7', '', 'eng'),
	(0, 'others8', '', 'eng'),
	(0, 'others5', '', 'fra'),
	(0, 'others6', '', 'fra'),
	(0, 'others7', '', 'fra'),
	(0, 'others8', '', 'fra'),
	(0, 'variation_others1', '', 'cat'),
	(0, 'variation_others2', '', 'cat'),
	(0, 'variation_others3', '', 'cat'),
	(0, 'variation_others4', '', 'cat'),
	(0, 'variation_others1', '', 'spa'),
	(0, 'variation_others2', '', 'spa'),
	(0, 'variation_others3', '', 'spa'),
	(0, 'variation_others4', '', 'spa'),
	(0, 'variation_others1', '', 'eng'),
	(0, 'variation_others2', '', 'eng'),
	(0, 'variation_others3', '', 'eng'),
	(0, 'variation_others4', '', 'eng'),
	(0, 'variation_others1', '', 'fra'),
	(0, 'variation_others2', '', 'fra'),
	(0, 'variation_others3', '', 'fra'),
	(0, 'variation_others4', '', 'fra'),
	(0, 'others9', '', 'cat'),
	(0, 'others10', '', 'cat'),
	(0, 'others9', '', 'spa'),
	(0, 'others10', '', 'spa'),
	(0, 'others9', '', 'eng'),
	(0, 'others10', '', 'eng'),
	(0, 'others9', '', 'fra'),
	(0, 'others10', '', 'fra'),
	(0, 'others11', '', 'cat'),
	(0, 'others12', '', 'cat'),
	(0, 'others13', '', 'cat'),
	(0, 'others14', '', 'cat'),
	(0, 'others15', '', 'cat'),
	(0, 'others16', '', 'cat'),
	(0, 'others17', '', 'cat'),
	(0, 'others18', '', 'cat'),
	(0, 'others19', '', 'cat'),
	(0, 'others20', '', 'cat'),
	(0, 'others11', '', 'spa'),
	(0, 'others12', '', 'spa'),
	(0, 'others13', '', 'spa'),
	(0, 'others14', '', 'spa'),
	(0, 'others15', '', 'spa'),
	(0, 'others16', '', 'spa'),
	(0, 'others17', '', 'spa'),
	(0, 'others18', '', 'spa'),
	(0, 'others19', '', 'spa'),
	(0, 'others20', '', 'spa'),
	(0, 'others11', '', 'eng'),
	(0, 'others12', '', 'eng'),
	(0, 'others13', '', 'eng'),
	(0, 'others14', '', 'eng'),
	(0, 'others15', '', 'eng'),
	(0, 'others16', '', 'eng'),
	(0, 'others17', '', 'eng'),
	(0, 'others18', '', 'eng'),
	(0, 'others19', '', 'eng'),
	(0, 'others20', '', 'eng'),
	(0, 'others11', '', 'fra'),
	(0, 'others12', '', 'fra'),
	(0, 'others13', '', 'fra'),
	(0, 'others14', '', 'fra'),
	(0, 'others15', '', 'fra'),
	(0, 'others16', '', 'fra'),
	(0, 'others17', '', 'fra'),
	(0, 'others18', '', 'fra'),
	(0, 'others19', '', 'fra'),
	(0, 'others20', '', 'fra'),
	(0, 'others_private1', '', 'cat'),
	(0, 'others_private2', '', 'cat'),
	(0, 'others_private3', '', 'cat'),
	(0, 'others_private1', '', 'spa'),
	(0, 'others_private2', '', 'spa'),
	(0, 'others_private3', '', 'spa'),
	(0, 'others_private1', '', 'eng'),
	(0, 'others_private2', '', 'eng'),
	(0, 'others_private3', '', 'eng'),
	(0, 'others_private1', '', 'fra'),
	(0, 'others_private2', '', 'fra'),
	(0, 'others_private3', '', 'fra');
/*!40000 ALTER TABLE `product__configadmin_language` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `product__configadmin_sellpoint` (
  `configadmin_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `value` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  PRIMARY KEY (`configadmin_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `product__configadmin_sellpoint` DISABLE KEYS */;
INSERT INTO `product__configadmin_sellpoint` (`configadmin_id`, `name`, `value`) VALUES
	(5, 'im_big_w', '1200'),
	(6, 'im_big_h', '800'),
	(8, 'im_details_w', '495'),
	(9, 'im_details_h', '330'),
	(10, 'im_thumb_w', '200'),
	(11, 'im_thumb_h', '133'),
	(14, 'im_thumb_q', '80'),
	(15, 'im_details_q', '80'),
	(16, 'im_list_cols', '2'),
	(20, 'im_medium_w', '900'),
	(21, 'im_medium_h', '600'),
	(22, 'im_medium_q', '80'),
	(23, 'im_big_q', '70'),
	(49, 'im_custom_ratios', ''),
	(50, 'show_as_title', ''),
	(51, 'show_as_subtitle', ''),
	(52, 'home_default_condition', '');
/*!40000 ALTER TABLE `product__configadmin_sellpoint` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `product__configadmin_variation` (
  `configadmin_variation_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `value` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  PRIMARY KEY (`configadmin_variation_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `product__configadmin_variation` DISABLE KEYS */;
INSERT INTO `product__configadmin_variation` (`configadmin_variation_id`, `name`, `value`) VALUES
	(1, 'im_thumb_w', '200'),
	(2, 'im_thumb_h', '150'),
	(3, 'im_thumb_q', '80'),
	(4, 'im_details_w', '300'),
	(5, 'im_details_h', '225'),
	(6, 'im_details_q', '80'),
	(7, 'im_medium_w', '900'),
	(8, 'im_medium_h', '675'),
	(9, 'im_medium_q', '70'),
	(10, 'im_big_w', '1200'),
	(11, 'im_big_h', '900'),
	(12, 'im_big_q', '70'),
	(13, 'im_list_cols', '2'),
	(14, 'im_custom_ratios', '');
/*!40000 ALTER TABLE `product__configadmin_variation` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `product__configpublic` (
  `configpublic_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `value` varchar(1024) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`configpublic_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `product__configpublic` DISABLE KEYS */;
INSERT INTO `product__configpublic` (`configpublic_id`, `name`, `value`) VALUES
	(7, 'home_max_results', '5'),
	(9, 'im_list_cols', '2'),
	(10, 'visited_max_results', '10'),
	(11, 'no_sell_text', ''),
	(12, 'home_title', ''),
	(13, 'paypal_sandbox', '0'),
	(14, 'banc_name', ''),
	(15, 'account_number', '1234 000 00 00000000'),
	(16, 'paypal_mail', 'sanahuja@gmail.com'),
	(17, 'enable_product_visited', '1'),
	(18, 'form_title', 'ref'),
	(19, 'home_show_marked', '1'),
	(20, 'home_show_familys', '1'),
	(21, 'home_max_familys', '10'),
	(22, 'home_max_family_products', '4'),
	(23, 'payment_methods', 'paypal,account,ondelivery'),
	(24, 'cart_parse_template', '1'),
	(25, 'variations_type', 'radios'),
	(26, 'is_private_shop', '0'),
	(27, 'show_as_title', 'product_title'),
	(31, 'show_first_customer_menu', '1'),
	(29, 'always_parse_template', '0'),
	(30, 'discount_max_results', '5'),
	(33, 'destacat_max_results', '5'),
	(34, '4b_simulador', '1'),
	(35, '4b_id_comercio', ''),
	(38, 'activate_customer_by_default', '1'),
	(39, 'sell_only_to_wholesalers', '0'),
	(43, 'lacaixa_simulador', '1'),
	(44, 'lacaixa_merchant_code', ''),
	(45, 'lacaixa_clave', ''),
	(46, 'lacaixa_terminal', '1'),
	(47, 'home_show_first_family', '0'),
	(48, 'home_tool_show_first_family', '0'),
	(49, 'format_price_as_currency', '1'),
	(50, 'google_analytics_conversion', ''),
	(51, 'show_products_family_and_subfamilys', '1'),
	(52, 'show_subfamilys', '0'),
	(53, 'home_show_subfamilys', '0'),
	(54, 'home_tool_show_subfamilys', '0'),
	(55, 'show_as_subtitle', ''),
	(56, 'home_list_all', '0'),
	(57, 'lacaixa_clave_sha2', 'sq7HjrUOBfKmC576ILgskD5srU870gJ7'),
	(64, 'listing_title', 'family'),
	(59, 'use_v2_cart', '0'),
	(60, 'related_max_results', '6'),
	(61, 'can_collect', '0'),
	(62, 'show_javascript_message_on_login', '1'),
	(63, 'search_fields', 'product_title,product_subtitle,others1,others2,others3,others4,others5,others6,others7,others8,product_description'),
	(65, 'listing_body_subtitle', ''),
	(66, 'listing_body_description', ''),
	(67, 'deposit_payment_on', '0'),
	(68, 'deposit_payment_percent', '0'),
	(69, 'deposit_payment_round', '0'),
	(70, 'deposit_rest_round', '0'),
	(71, 'show_empty_familys', '0');
/*!40000 ALTER TABLE `product__configpublic` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `product__configpublic_language` (
  `configpublic_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `value` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `language` char(3) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `product__configpublic_language` DISABLE KEYS */;
INSERT INTO `product__configpublic_language` (`configpublic_id`, `name`, `value`, `language`) VALUES
	(11, 'no_sell_text', ' ', 'cat'),
	(11, 'no_sell_text', ' ', 'spa'),
	(11, 'no_sell_text', ' ', 'eng'),
	(11, 'no_sell_text', ' ', 'fra'),
	(12, 'home_title', 'Títol home', 'cat'),
	(12, 'home_title', 'Títol home', 'spa'),
	(12, 'home_title', '', 'eng'),
	(12, 'home_title', '', 'fra');
/*!40000 ALTER TABLE `product__configpublic_language` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `product__courier` (
  `courier_id` int(11) NOT NULL AUTO_INCREMENT,
  `courier` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `courier_url` varchar(150) COLLATE utf8_spanish2_ci NOT NULL,
  `default_courier` tinyint(1) NOT NULL DEFAULT '0',
  `bin` tinyint(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`courier_id`),
  KEY `bin` (`bin`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `product__courier` DISABLE KEYS */;
INSERT INTO `product__courier` (`courier_id`, `courier`, `courier_url`, `default_courier`, `bin`) VALUES
	(1, 'TNT', 'www.tnt.com', 0, 0),
	(2, 'Seur', 'www.seur.com', 0, 0),
	(3, 'FedEx', 'www.fedex.com', 0, 0),
	(4, 'DHL', 'www.dhl.es', 0, 0),
	(5, 'UPS', 'www.ups.com', 0, 0),
	(6, 'Correos', 'www.correos.es', 0, 0),
	(7, 'Envialia', 'www.envialia.com', 0, 0),
	(8, 'Correos express', 'www.correosexpress.com', 0, 0);
/*!40000 ALTER TABLE `product__courier` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `product__customer` (
  `customer_id` int(11) NOT NULL AUTO_INCREMENT,
  `remove_key` char(50) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '0',
  `removed` tinyint(1) NOT NULL DEFAULT '0',
  `treatment` enum('sr','sra') COLLATE utf8_spanish2_ci NOT NULL,
  `company` varchar(150) COLLATE utf8_spanish2_ci NOT NULL,
  `name` varchar(50) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `surname` varchar(100) COLLATE utf8_spanish2_ci NOT NULL,
  `birthdate` date NOT NULL,
  `mail` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `password` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `telephone` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `telephone_mobile` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `dni` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `prefered_language` char(3) COLLATE utf8_spanish2_ci NOT NULL DEFAULT 'cat',
  `comment` text COLLATE utf8_spanish2_ci NOT NULL,
  `bin` int(1) NOT NULL DEFAULT '0',
  `enteredc` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `delivery_id` int(11) NOT NULL DEFAULT '0',
  `is_wholesaler` tinyint(1) NOT NULL DEFAULT '0',
  `wholesaler_rate` enum('1','2','3','4','5') COLLATE utf8_spanish2_ci NOT NULL DEFAULT '1',
  `has_equivalencia` tinyint(1) NOT NULL DEFAULT '0',
  `activated` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`customer_id`),
  KEY `bin` (`bin`),
  KEY `activated` (`activated`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `product__customer` DISABLE KEYS */;
INSERT INTO `product__customer` (`customer_id`, `remove_key`, `removed`, `treatment`, `company`, `name`, `surname`, `birthdate`, `mail`, `password`, `telephone`, `telephone_mobile`, `dni`, `prefered_language`, `comment`, `bin`, `enteredc`, `delivery_id`, `is_wholesaler`, `wholesaler_rate`, `has_equivalencia`, `activated`) VALUES
	(1, 'qWWvMAGPZBhf0J8tCAjpfaaIMpOCkf3qHwVlMwWZLznMIX8KUi', 0, 'sr', '', 'Marc', 'Sanahuja', '2013-03-08', 'info@letnd.com', '1', '972000000', '667516651', '40325226q', 'cat', '', 0, '2013-03-01 16:01:19', 0, 0, '1', 0, 1);
/*!40000 ALTER TABLE `product__customer` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `product__customer_to_promcode` (
  `customer_promcode_id` int(10) NOT NULL AUTO_INCREMENT,
  `customer_id` int(10) NOT NULL,
  `promcode_id` int(10) NOT NULL,
  PRIMARY KEY (`customer_promcode_id`),
  KEY `customer_id` (`customer_id`),
  KEY `promcode_id` (`promcode_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `product__customer_to_promcode` DISABLE KEYS */;
/*!40000 ALTER TABLE `product__customer_to_promcode` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `product__family` (
  `family_id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `sell` tinyint(1) NOT NULL DEFAULT '1',
  `observations` text COLLATE utf8_spanish2_ci NOT NULL,
  `destacat` tinyint(1) NOT NULL DEFAULT '0',
  `is_calendar_public` tinyint(1) NOT NULL DEFAULT '1',
  `has_calendar` tinyint(1) NOT NULL DEFAULT '0',
  `is_family_private` tinyint(1) NOT NULL DEFAULT '0',
  `pass_code` tinytext COLLATE utf8_spanish2_ci NOT NULL,
  `accepts_deposit_payment` tinyint(1) NOT NULL DEFAULT '1',
  `ordre` int(11) NOT NULL DEFAULT '0',
  `bin` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`family_id`),
  KEY `parent_id` (`parent_id`),
  KEY `bin` (`bin`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `product__family` DISABLE KEYS */;
INSERT INTO `product__family` (`family_id`, `parent_id`, `sell`, `observations`, `destacat`, `is_calendar_public`, `has_calendar`, `is_family_private`, `pass_code`, `accepts_deposit_payment`, `ordre`, `bin`) VALUES
	(1, 0, 1, '', 1, 1, 0, 0, '', 1, 0, 0),
	(2, 0, 1, '', 0, 1, 0, 0, '', 1, 0, 0),
	(3, 0, 1, '', 0, 1, 0, 0, '', 1, 0, 0);
/*!40000 ALTER TABLE `product__family` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `product__familybusy` (
  `familybusy_id` int(11) NOT NULL AUTO_INCREMENT,
  `family_id` int(11) NOT NULL,
  `date_busy` date NOT NULL,
  `familybusy_entered` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`familybusy_id`),
  KEY `family` (`family_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `product__familybusy` DISABLE KEYS */;
/*!40000 ALTER TABLE `product__familybusy` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `product__family_image` (
  `image_id` int(11) NOT NULL AUTO_INCREMENT,
  `family_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `name_original` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `size` int(11) NOT NULL DEFAULT '0',
  `ordre` int(11) NOT NULL DEFAULT '0',
  `main_image` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`image_id`),
  KEY `family_id` (`family_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `product__family_image` DISABLE KEYS */;
/*!40000 ALTER TABLE `product__family_image` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `product__family_image_language` (
  `image_id` int(11) NOT NULL DEFAULT '0',
  `language` char(3) COLLATE utf8_spanish2_ci NOT NULL,
  `image_alt` varchar(150) COLLATE utf8_spanish2_ci NOT NULL,
  `image_title` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  KEY `language` (`language`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `product__family_image_language` DISABLE KEYS */;
/*!40000 ALTER TABLE `product__family_image_language` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `product__family_language` (
  `family_id` int(11) NOT NULL DEFAULT '0',
  `language` char(3) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `family` varchar(100) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `family_alt` varchar(150) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `family_subtitle` varchar(255) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `family_content_list` text COLLATE utf8_spanish2_ci NOT NULL,
  `family_description` text COLLATE utf8_spanish2_ci NOT NULL,
  `page_title` varchar(255) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `page_description` varchar(255) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `page_keywords` varchar(255) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  KEY `family_id` (`family_id`),
  KEY `language` (`language`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `product__family_language` DISABLE KEYS */;
INSERT INTO `product__family_language` (`family_id`, `language`, `family`, `family_alt`, `family_subtitle`, `family_content_list`, `family_description`, `page_title`, `page_description`, `page_keywords`) VALUES
	(1, 'cat', 'Champú antiaída del cabello', '', '', '', 'Descripció a títol d\'exemple de família 1\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Proin vel mauris aliquet, consectetur ex sit amet, imperdiet dolor. Vivamus viverra nibh non diam egestas luctus. Sed porta cursus erat sit amet mollis. Cras tincidunt molestie massa, ut accumsan felis ornare eu.  luctus. Sed porta cursus erat sit amet mollis. Cras tincidunt ', '', '', ''),
	(1, 'spa', 'Champú antiaída del cabello', '', '', '', 'Descripció a títol d\'exemple de família 1\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Proin vel mauris aliquet, consectetur ex sit amet, imperdiet dolor. Vivamus viverra nibh non diam egestas luctus. Sed porta cursus erat sit amet mollis. Cras tincidunt molestie massa, ut accumsan felis ornare eu.  luctus. Sed porta cursus erat sit amet mollis. Cras tincidunt ', '', '', ''),
	(1, 'eng', '', '', '', '', 'Descripció a títol d\'exemple de família 1', '', '', ''),
	(1, 'fra', '', '', '', '', 'Descripció a títol d\'exemple de família 1', '', '', ''),
	(2, 'cat', 'Champú acondicionador', '', '', '', '', '', '', ''),
	(2, 'spa', 'Champú acondicionador', '', '', '', '', '', '', ''),
	(2, 'eng', '', '', '', '', '', '', '', ''),
	(2, 'fra', '', '', '', '', '', '', '', ''),
	(3, 'cat', 'Cremas hombre', '', '', '', '', '', '', ''),
	(3, 'spa', 'Cremas hombre', '', '', '', '', '', '', ''),
	(3, 'eng', '', '', '', '', '', '', '', ''),
	(3, 'fra', '', '', '', '', '', '', '', '');
/*!40000 ALTER TABLE `product__family_language` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `product__material` (
  `material_id` int(11) NOT NULL AUTO_INCREMENT,
  `observations` text COLLATE utf8_spanish2_ci NOT NULL,
  `bin` int(1) NOT NULL,
  PRIMARY KEY (`material_id`),
  KEY `bin` (`bin`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `product__material` DISABLE KEYS */;
/*!40000 ALTER TABLE `product__material` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `product__material_language` (
  `material_id` int(11) NOT NULL,
  `language` char(3) COLLATE utf8_spanish2_ci NOT NULL,
  `material` varchar(100) COLLATE utf8_spanish2_ci NOT NULL,
  KEY `material_id` (`material_id`),
  KEY `language` (`language`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `product__material_language` DISABLE KEYS */;
/*!40000 ALTER TABLE `product__material_language` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `product__order` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT,
  `transaction_id` char(17) COLLATE utf8_spanish2_ci NOT NULL,
  `deposit_transaction_id` char(17) COLLATE utf8_spanish2_ci NOT NULL,
  `approval_code` char(17) COLLATE utf8_spanish2_ci NOT NULL,
  `customer_id` int(11) NOT NULL,
  `total_base` decimal(11,2) NOT NULL,
  `total_tax` decimal(11,2) NOT NULL,
  `total_equivalencia` decimal(11,2) NOT NULL,
  `total_basetax` decimal(11,2) NOT NULL,
  `rate_base` decimal(11,2) NOT NULL,
  `rate_tax` decimal(11,2) NOT NULL,
  `rate_basetax` decimal(11,2) NOT NULL,
  `all_base` decimal(11,2) NOT NULL,
  `all_tax` decimal(11,2) NOT NULL,
  `all_equivalencia` decimal(11,2) NOT NULL,
  `all_basetax` decimal(11,2) NOT NULL,
  `promcode_discount` decimal(11,2) NOT NULL,
  `deposit` decimal(11,2) NOT NULL,
  `collect` tinyint(4) NOT NULL DEFAULT '0',
  `entered_order` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `prepared_emailed` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `delivered` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `delivered_emailed` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `shipped` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `shipped_emailed` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `order_status` enum('paying','failed','deposit','pending','denied','voided','refunded','completed','preparing','shipped','delivered','collect') COLLATE utf8_spanish2_ci NOT NULL,
  `deposit_status` enum('paying','failed','pending','denied','voided','refunded','completed') COLLATE utf8_spanish2_ci NOT NULL,
  `payment_method` enum('paypal','account','ondelivery','directdebit','4b','lacaixa') COLLATE utf8_spanish2_ci NOT NULL,
  `deposit_payment_method` enum('none','paypal','account','ondelivery','directdebit','4b','lacaixa','cash','physicaltpv') COLLATE utf8_spanish2_ci NOT NULL DEFAULT 'none',
  `delivery_id` int(11) NOT NULL,
  `address_invoice_id` int(11) NOT NULL,
  `comment` text COLLATE utf8_spanish2_ci NOT NULL,
  `error_code` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `error_description` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `courier_id` int(11) NOT NULL DEFAULT '0',
  `courier_follow_code` varchar(100) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `invoice_id` int(11) NOT NULL DEFAULT '0',
  `invoice_date` date NOT NULL,
  `invoice_refund_id` int(11) NOT NULL DEFAULT '0',
  `invoice_refund_date` date NOT NULL,
  `customer_comment` text COLLATE utf8_spanish2_ci NOT NULL,
  `bin` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`order_id`),
  KEY `product_id` (`customer_id`),
  KEY `bin` (`bin`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `product__order` DISABLE KEYS */;
/*!40000 ALTER TABLE `product__order` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `product__orderitem` (
  `orderitem_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `ref` varchar(20) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `product_title` varchar(100) COLLATE utf8_spanish2_ci NOT NULL,
  `product_variation_ids` varchar(300) COLLATE utf8_spanish2_ci NOT NULL,
  `product_variations` varchar(600) COLLATE utf8_spanish2_ci NOT NULL,
  `product_variation_categorys` varchar(600) COLLATE utf8_spanish2_ci NOT NULL,
  `quantity` int(3) NOT NULL,
  `product_date` date NOT NULL,
  `product_base` decimal(11,2) NOT NULL,
  `product_tax` decimal(11,2) NOT NULL,
  `product_equivalencia` decimal(11,2) NOT NULL,
  `product_basetax` decimal(11,2) NOT NULL,
  `product_total_base` decimal(11,2) NOT NULL,
  `product_total_tax` decimal(11,2) NOT NULL,
  `product_total_equivalencia` decimal(11,2) NOT NULL,
  `product_total_basetax` decimal(11,2) NOT NULL,
  PRIMARY KEY (`orderitem_id`),
  KEY `order_id` (`order_id`),
  KEY `product_id` (`product_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `product__orderitem` DISABLE KEYS */;
/*!40000 ALTER TABLE `product__orderitem` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `product__order_file` (
  `file_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `name_original` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `size` int(11) NOT NULL,
  `ordre` int(11) NOT NULL,
  `main_file` int(1) NOT NULL,
  `public` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`file_id`),
  KEY `order_id` (`order_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `product__order_file` DISABLE KEYS */;
/*!40000 ALTER TABLE `product__order_file` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `product__product` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `ref` varchar(255) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `ref_number` int(11) NOT NULL DEFAULT '0',
  `ref_supplier` varchar(50) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `ean` varchar(20) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `product_private` varchar(100) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `family_id` int(11) NOT NULL DEFAULT '0',
  `subfamily_id` int(6) NOT NULL DEFAULT '0',
  `subsubfamily_id` int(6) NOT NULL DEFAULT '0',
  `pvp` decimal(11,2) NOT NULL DEFAULT '0.00',
  `pvd` decimal(11,2) NOT NULL DEFAULT '0.00',
  `pvd2` decimal(11,2) NOT NULL DEFAULT '0.00',
  `pvd3` decimal(11,2) NOT NULL DEFAULT '0.00',
  `pvd4` decimal(11,2) NOT NULL DEFAULT '0.00',
  `pvd5` decimal(11,2) NOT NULL DEFAULT '0.00',
  `price_cost` decimal(11,2) NOT NULL DEFAULT '0.00',
  `price_consult` tinyint(1) NOT NULL DEFAULT '0',
  `tax_id` int(11) NOT NULL DEFAULT '0',
  `destacat` tinyint(1) NOT NULL DEFAULT '0',
  `is_new_product` tinyint(1) NOT NULL DEFAULT '0',
  `prominent` tinyint(1) NOT NULL DEFAULT '0',
  `discount` tinyint(2) NOT NULL DEFAULT '0',
  `discount_wholesaler` tinyint(2) NOT NULL DEFAULT '0',
  `discount_fixed_price` decimal(11,2) NOT NULL,
  `discount_fixed_price_wholesaler` decimal(11,2) NOT NULL,
  `price_unit` enum('unit','couple','ten','dozen','hundred','thousand','m2') COLLATE utf8_spanish2_ci NOT NULL DEFAULT 'unit',
  `brand_id` int(11) NOT NULL DEFAULT '0',
  `sell` tinyint(1) NOT NULL DEFAULT '1',
  `guarantee` tinyint(2) NOT NULL DEFAULT '0',
  `guarantee_period` enum('da','mo','yr') COLLATE utf8_spanish2_ci NOT NULL DEFAULT 'da',
  `year` year(4) NOT NULL,
  `observations` text COLLATE utf8_spanish2_ci NOT NULL,
  `status` enum('prepare','review','onsale','nostock','nocatalog','archived') COLLATE utf8_spanish2_ci NOT NULL DEFAULT 'prepare',
  `home` tinyint(1) NOT NULL DEFAULT '0',
  `free_send` tinyint(1) NOT NULL DEFAULT '1',
  `stock` int(11) NOT NULL,
  `last_stock_units_public` int(11) NOT NULL,
  `last_stock_units_admin` int(11) NOT NULL,
  `sells` int(11) NOT NULL,
  `ordre` int(11) NOT NULL DEFAULT '0',
  `videoframe` varchar(1000) COLLATE utf8_spanish2_ci NOT NULL,
  `ishandmade` tinyint(1) NOT NULL DEFAULT '0',
  `format` varchar(100) COLLATE utf8_spanish2_ci NOT NULL,
  `power` varchar(250) COLLATE utf8_spanish2_ci NOT NULL,
  `alimentacion` varchar(100) COLLATE utf8_spanish2_ci NOT NULL,
  `weight` int(11) NOT NULL,
  `weight_unit` enum('gram','kilo') COLLATE utf8_spanish2_ci NOT NULL,
  `height` int(11) NOT NULL,
  `height_unit` enum('mm','cm','mt','km','feet') COLLATE utf8_spanish2_ci NOT NULL,
  `temperature` int(11) NOT NULL,
  `temperature_unit` enum('celcius','fahrenheit') COLLATE utf8_spanish2_ci NOT NULL,
  `util_life` int(11) NOT NULL,
  `util_life_unit` enum('hours','days','months','years','meters','km') COLLATE utf8_spanish2_ci NOT NULL,
  `color_id` int(11) NOT NULL,
  `material_id` int(11) NOT NULL,
  `certification` varchar(100) COLLATE utf8_spanish2_ci NOT NULL,
  `min_quantity` int(11) NOT NULL DEFAULT '1',
  `max_quantity` int(11) NOT NULL,
  `quantityconcept_id` int(11) NOT NULL,
  `others1` varchar(2000) COLLATE utf8_spanish2_ci NOT NULL,
  `others2` varchar(2000) COLLATE utf8_spanish2_ci NOT NULL,
  `others3` varchar(2000) COLLATE utf8_spanish2_ci NOT NULL,
  `others4` varchar(2000) COLLATE utf8_spanish2_ci NOT NULL,
  `others5` varchar(2000) COLLATE utf8_spanish2_ci NOT NULL,
  `others6` varchar(2000) COLLATE utf8_spanish2_ci NOT NULL,
  `others7` varchar(2000) COLLATE utf8_spanish2_ci NOT NULL,
  `others8` varchar(2000) COLLATE utf8_spanish2_ci NOT NULL,
  `others9` text COLLATE utf8_spanish2_ci NOT NULL,
  `others10` text COLLATE utf8_spanish2_ci NOT NULL,
  `others11` text COLLATE utf8_spanish2_ci NOT NULL,
  `others12` text COLLATE utf8_spanish2_ci NOT NULL,
  `others13` text COLLATE utf8_spanish2_ci NOT NULL,
  `others14` text COLLATE utf8_spanish2_ci NOT NULL,
  `others15` text COLLATE utf8_spanish2_ci NOT NULL,
  `others16` text COLLATE utf8_spanish2_ci NOT NULL,
  `others17` text COLLATE utf8_spanish2_ci NOT NULL,
  `others18` text COLLATE utf8_spanish2_ci NOT NULL,
  `others19` text COLLATE utf8_spanish2_ci NOT NULL,
  `others20` text COLLATE utf8_spanish2_ci NOT NULL,
  `others_private1` text COLLATE utf8_spanish2_ci NOT NULL,
  `others_private2` text COLLATE utf8_spanish2_ci NOT NULL,
  `others_private3` text COLLATE utf8_spanish2_ci NOT NULL,
  `entered` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `bin` tinyint(4) NOT NULL DEFAULT '0',
  `facebook_post_id` varchar(255) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `facebook_image_id` varchar(255) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `facebook_publish_id` varchar(255) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `facebook_letnd_user_id` varchar(255) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `facebook_user_id` text COLLATE utf8_spanish2_ci NOT NULL,
  `twitter_status_id` varchar(255) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `twitter_letnd_user_id` varchar(255) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `twitter_user_id` text COLLATE utf8_spanish2_ci NOT NULL,
  PRIMARY KEY (`product_id`),
  KEY `family_id` (`family_id`),
  KEY `subfamily_id` (`subfamily_id`),
  KEY `subsubfamily_id` (`subsubfamily_id`),
  KEY `brand_id` (`brand_id`),
  KEY `bin` (`bin`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `product__product` DISABLE KEYS */;
INSERT INTO `product__product` (`product_id`, `ref`, `ref_number`, `ref_supplier`, `ean`, `product_private`, `family_id`, `subfamily_id`, `subsubfamily_id`, `pvp`, `pvd`, `pvd2`, `pvd3`, `pvd4`, `pvd5`, `price_cost`, `price_consult`, `tax_id`, `destacat`, `is_new_product`, `prominent`, `discount`, `discount_wholesaler`, `discount_fixed_price`, `discount_fixed_price_wholesaler`, `price_unit`, `brand_id`, `sell`, `guarantee`, `guarantee_period`, `year`, `observations`, `status`, `home`, `free_send`, `stock`, `last_stock_units_public`, `last_stock_units_admin`, `sells`, `ordre`, `videoframe`, `ishandmade`, `format`, `power`, `alimentacion`, `weight`, `weight_unit`, `height`, `height_unit`, `temperature`, `temperature_unit`, `util_life`, `util_life_unit`, `color_id`, `material_id`, `certification`, `min_quantity`, `max_quantity`, `quantityconcept_id`, `others1`, `others2`, `others3`, `others4`, `others5`, `others6`, `others7`, `others8`, `others9`, `others10`, `others11`, `others12`, `others13`, `others14`, `others15`, `others16`, `others17`, `others18`, `others19`, `others20`, `others_private1`, `others_private2`, `others_private3`, `entered`, `modified`, `bin`, `facebook_post_id`, `facebook_image_id`, `facebook_publish_id`, `facebook_letnd_user_id`, `facebook_user_id`, `twitter_status_id`, `twitter_letnd_user_id`, `twitter_user_id`) VALUES
	(1, '1', 0, '', '', '', 2, 0, 0, 20.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 1, 1, 0, 0, 0, 0, 0.00, 0.00, 'unit', 0, 1, 0, 'yr', '0000', '', 'onsale', 0, 0, 0, 0, 0, 1, 0, '<iframe src="https://player.vimeo.com/video/164022180" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>', 0, '', '', '', 0, 'kilo', 0, 'cm', 0, 'celcius', 0, 'hours', 0, 0, '', 1, 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2018-05-31 16:51:50', '2018-05-31 16:51:50', 0, '', '', '', '', '', '', '', ''),
	(2, '2', 0, '', '', '', 1, 0, 0, 30.50, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 1, 1, 0, 0, 5, 0, 0.00, 0.00, 'unit', 0, 1, 10, 'yr', '1990', '', 'onsale', 0, 0, 0, 0, 0, 1, 0, '<iframe src="https://player.vimeo.com/video/164022180" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>', 0, '', '', '', 0, 'kilo', 10, 'cm', 0, 'celcius', 5, 'hours', 0, 0, '', 1, 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2018-05-31 16:51:50', '2018-05-31 16:51:50', 0, '', '', '', '', '', '', '', ''),
	(3, '3', 0, '', '', '', 3, 0, 0, 30.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 1, 1, 0, 1, 0, 0, 0.00, 0.00, 'unit', 0, 1, 5, 'yr', '1920', '', 'onsale', 0, 0, 0, 0, 0, 5, 0, '<iframe src="https://player.vimeo.com/video/164022180" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>', 0, '', '120', '', 4, 'gram', 10, 'cm', 5, 'celcius', 4, 'hours', 0, 0, '', 1, 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2018-05-31 16:51:50', '2018-05-31 16:51:50', 0, '', '', '', '', '', '', '', ''),
	(4, '4', 0, '', '', '', 3, 0, 0, 14.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 1, 1, 0, 0, 0, 0, 0.00, 0.00, 'unit', 0, 1, 0, 'yr', '0000', '', 'onsale', 0, 0, 0, 0, 0, 3, 0, '<iframe src="https://player.vimeo.com/video/164022180" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>', 0, '', '', '', 0, 'kilo', 0, 'cm', 0, 'celcius', 0, 'hours', 0, 0, '', 1, 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2018-05-31 16:51:50', '2018-05-31 16:51:50', 0, '', '', '', '', '', '', '', ''),
	(5, '1', 0, '', '', '', 2, 0, 0, 20.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 1, 1, 0, 0, 0, 0, 0.00, 0.00, 'unit', 0, 1, 0, 'yr', '0000', '', 'onsale', 0, 0, 0, 0, 0, 1, 0, '<iframe src="https://player.vimeo.com/video/164022180" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>', 0, '', '', '', 0, 'kilo', 0, 'cm', 0, 'celcius', 0, 'hours', 0, 0, '', 1, 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2018-05-31 16:51:50', '2018-05-31 16:51:50', 0, '', '', '', '', '', '', '', ''),
	(6, '2', 0, '', '', '', 3, 0, 0, 30.50, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 1, 1, 0, 0, 5, 0, 0.00, 0.00, 'unit', 0, 1, 10, 'yr', '1990', '', 'onsale', 0, 0, 0, 0, 0, 1, 0, '<iframe src="https://player.vimeo.com/video/164022180" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>', 0, '', '', '', 0, 'kilo', 10, 'cm', 0, 'celcius', 5, 'hours', 0, 0, '', 1, 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2018-05-31 16:51:50', '2018-05-31 16:51:50', 0, '', '', '', '', '', '', '', ''),
	(7, '3', 0, '', '', '', 3, 0, 0, 30.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 1, 1, 0, 1, 0, 0, 0.00, 0.00, 'unit', 0, 1, 5, 'yr', '0000', '', 'onsale', 0, 0, 0, 0, 0, 5, 0, '<iframe src="https://player.vimeo.com/video/164022180" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>', 0, '', '', '', 560, 'kilo', 0, 'cm', 5, 'celcius', 0, 'hours', 0, 0, '', 1, 0, 0, '6.70 x 3.80 x 1/ 1.60', '6 x 3.10 x 1 / 1.60', '28', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2018-05-31 16:51:50', '2018-05-31 16:51:50', 0, '', '', '', '', '', '', '', ''),
	(8, '4', 0, '', '', '', 3, 0, 0, 14.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 1, 1, 0, 0, 0, 0, 0.00, 0.00, 'unit', 0, 1, 0, 'yr', '0000', '', 'onsale', 0, 0, 0, 0, 0, 3, 0, '<iframe src="https://player.vimeo.com/video/164022180" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>', 0, '', '', '', 0, 'kilo', 0, 'cm', 0, 'celcius', 0, 'hours', 0, 0, '', 1, 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2018-05-31 16:51:50', '2018-05-31 16:51:50', 0, '', '', '', '', '', '', '', '');
/*!40000 ALTER TABLE `product__product` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `product__product_download` (
  `download_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `name_original` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `size` int(11) NOT NULL,
  `ordre` int(11) NOT NULL,
  `main_file` int(1) NOT NULL,
  PRIMARY KEY (`download_id`),
  KEY `product_id` (`product_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `product__product_download` DISABLE KEYS */;
/*!40000 ALTER TABLE `product__product_download` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `product__product_file` (
  `file_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `name_original` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `size` int(11) NOT NULL,
  `ordre` int(11) NOT NULL,
  `main_file` int(1) NOT NULL,
  `filecat_id` int(11) NOT NULL DEFAULT '1',
  `public` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`file_id`),
  KEY `product_id` (`product_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `product__product_file` DISABLE KEYS */;
/*!40000 ALTER TABLE `product__product_file` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `product__product_image` (
  `image_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `product_variation_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `name_original` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `size` int(11) NOT NULL,
  `ordre` int(11) NOT NULL,
  `main_image` int(1) NOT NULL,
  PRIMARY KEY (`image_id`),
  KEY `product_id` (`product_id`),
  KEY `main_image` (`main_image`),
  KEY `product_variation_id` (`product_variation_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `product__product_image` DISABLE KEYS */;
INSERT INTO `product__product_image` (`image_id`, `product_id`, `product_variation_id`, `name`, `name_original`, `size`, `ordre`, `main_image`) VALUES
	(9, 3, 0, '4.jpg', '4.jpg', 377505, 2, 0),
	(10, 4, 0, '3.jpg', '3.jpg', 341946, 0, 1),
	(8, 3, 0, '1.jpg', '1.jpg', 514222, 1, 0),
	(7, 3, 0, '5.jpg', '5.jpg', 348498, 0, 1),
	(11, 4, 0, '6.jpg', '6.jpg', 72614, 1, 0),
	(12, 4, 0, '7.jpg', '7.jpg', 62943, 2, 0),
	(13, 1, 0, '2.jpg', '2.jpg', 82068, 0, 1),
	(14, 1, 0, '9.jpg', '9.jpg', 135955, 1, 0),
	(15, 2, 0, '8.jpg', '8.jpg', 83310, 0, 1),
	(16, 2, 0, '10.jpg', '10.jpg', 141005, 1, 0);
/*!40000 ALTER TABLE `product__product_image` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `product__product_image_language` (
  `image_id` int(11) NOT NULL DEFAULT '0',
  `language` char(3) COLLATE utf8_spanish2_ci NOT NULL,
  `image_alt` varchar(150) COLLATE utf8_spanish2_ci NOT NULL,
  `image_title` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  KEY `image_id` (`image_id`),
  KEY `language` (`language`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `product__product_image_language` DISABLE KEYS */;
INSERT INTO `product__product_image_language` (`image_id`, `language`, `image_alt`, `image_title`) VALUES
	(12, 'eng', '', ''),
	(11, 'fra', '', ''),
	(11, 'eng', '', ''),
	(11, 'spa', '', ''),
	(11, 'cat', '', ''),
	(10, 'fra', '', ''),
	(10, 'eng', '', ''),
	(10, 'spa', '', ''),
	(10, 'cat', '', ''),
	(12, 'spa', '', ''),
	(12, 'cat', '', ''),
	(9, 'fra', '', ''),
	(9, 'eng', '', ''),
	(9, 'spa', '', ''),
	(9, 'cat', '', ''),
	(8, 'fra', '', ''),
	(8, 'eng', '', ''),
	(8, 'spa', '', ''),
	(8, 'cat', '', ''),
	(7, 'fra', '', ''),
	(7, 'eng', '', ''),
	(7, 'spa', '', ''),
	(7, 'cat', '', ''),
	(12, 'fra', '', ''),
	(13, 'cat', '', ''),
	(13, 'spa', '', ''),
	(13, 'eng', '', ''),
	(13, 'fra', '', ''),
	(14, 'cat', '', ''),
	(14, 'spa', '', ''),
	(14, 'eng', '', ''),
	(14, 'fra', '', ''),
	(15, 'cat', '', ''),
	(15, 'spa', '', ''),
	(15, 'eng', '', ''),
	(15, 'fra', '', ''),
	(16, 'cat', '', ''),
	(16, 'spa', '', ''),
	(16, 'eng', '', ''),
	(16, 'fra', '', '');
/*!40000 ALTER TABLE `product__product_image_language` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `product__product_language` (
  `product_id` int(11) NOT NULL DEFAULT '0',
  `language` char(3) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `product_title` varchar(100) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `product_subtitle` varchar(255) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `product_description` text COLLATE utf8_spanish2_ci NOT NULL,
  `product_other` text COLLATE utf8_spanish2_ci NOT NULL,
  `page_title` varchar(255) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `page_description` varchar(255) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `page_keywords` varchar(255) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `product_file_name` varchar(100) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `product_old_file_name` varchar(100) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `others1` varchar(2000) COLLATE utf8_spanish2_ci NOT NULL,
  `others2` varchar(2000) COLLATE utf8_spanish2_ci NOT NULL,
  `others3` varchar(2000) COLLATE utf8_spanish2_ci NOT NULL,
  `others4` varchar(2000) COLLATE utf8_spanish2_ci NOT NULL,
  `others5` varchar(2000) COLLATE utf8_spanish2_ci NOT NULL,
  `others6` varchar(2000) COLLATE utf8_spanish2_ci NOT NULL,
  `others7` varchar(2000) COLLATE utf8_spanish2_ci NOT NULL,
  `others8` varchar(2000) COLLATE utf8_spanish2_ci NOT NULL,
  `others9` varchar(2000) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `others10` varchar(2000) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `others11` text COLLATE utf8_spanish2_ci NOT NULL,
  `others12` text COLLATE utf8_spanish2_ci NOT NULL,
  `others13` text COLLATE utf8_spanish2_ci NOT NULL,
  `others14` text COLLATE utf8_spanish2_ci NOT NULL,
  `others15` text COLLATE utf8_spanish2_ci NOT NULL,
  `others16` text COLLATE utf8_spanish2_ci NOT NULL,
  `others17` text COLLATE utf8_spanish2_ci NOT NULL,
  `others18` text COLLATE utf8_spanish2_ci NOT NULL,
  `others19` text COLLATE utf8_spanish2_ci NOT NULL,
  `others20` text COLLATE utf8_spanish2_ci NOT NULL,
  KEY `product_id` (`product_id`),
  KEY `language` (`language`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `product__product_language` DISABLE KEYS */;
INSERT INTO `product__product_language` (`product_id`, `language`, `product_title`, `product_subtitle`, `product_description`, `product_other`, `page_title`, `page_description`, `page_keywords`, `product_file_name`, `product_old_file_name`, `others1`, `others2`, `others3`, `others4`, `others5`, `others6`, `others7`, `others8`, `others9`, `others10`, `others11`, `others12`, `others13`, `others14`, `others15`, `others16`, `others17`, `others18`, `others19`, `others20`) VALUES
	(1, 'cat', 'Títol Exemple de producte 1', 'Subtítol Exemple de producte 1', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto asperiores blanditiis, distinctio dolores error fugit hic modi nobis placeat provident repellat reprehenderit soluta sunt tempora ullam, veniam veritatis voluptates, voluptatibus.\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid asperiores dolorem, enim ex fugiat inventore itaque nostrum sapiente! Culpa, odit sit? Asperiores culpa cupiditate dolore ea eligendi iusto laboriosam quo!', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(1, 'spa', 'Títol Exemple de producte 1', 'Subtítol Exemple de producte 1', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto asperiores blanditiis, distinctio dolores error fugit hic modi nobis placeat provident repellat reprehenderit soluta sunt tempora ullam, veniam veritatis voluptates, voluptatibus.\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid asperiores dolorem, enim ex fugiat inventore itaque nostrum sapiente! Culpa, odit sit? Asperiores culpa cupiditate dolore ea eligendi iusto laboriosam quo!', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(1, 'eng', 'Títol Exemple de producte 1', 'Subtítol Exemple de producte 1', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto asperiores blanditiis, distinctio dolores error fugit hic modi nobis placeat provident repellat reprehenderit soluta sunt tempora ullam, veniam veritatis voluptates, voluptatibus.\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid asperiores dolorem, enim ex fugiat inventore itaque nostrum sapiente! Culpa, odit sit? Asperiores culpa cupiditate dolore ea eligendi iusto laboriosam quo!', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(1, 'fra', 'Títol Exemple de producte 1', 'Subtítol Exemple de producte 1', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto asperiores blanditiis, distinctio dolores error fugit hic modi nobis placeat provident repellat reprehenderit soluta sunt tempora ullam, veniam veritatis voluptates, voluptatibus.\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid asperiores dolorem, enim ex fugiat inventore itaque nostrum sapiente! Culpa, odit sit? Asperiores culpa cupiditate dolore ea eligendi iusto laboriosam quo!', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(2, 'cat', 'Títol Exemple de producte 2', 'Subtítol Exemple de producte 2', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium adipisci et fugiat odio vel! Accusamus alias aliquid aut ea est eveniet iusto libero magnam magni minima, mollitia, reprehenderit, sunt vero.\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. A assumenda dolor, eligendi enim facilis hic impedit incidunt iusto laborum praesentium. Delectus dolor est molestiae numquam saepe sint, ullam velit voluptate!\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Amet deleniti expedita id maxime nisi non, nulla quibusdam recusandae repellat, similique temporibus voluptates. Commodi corporis cupiditate doloribus mollitia nesciunt, officia quod?', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(2, 'spa', 'Títol Exemple de producte 2', 'Subtítol Exemple de producte 2', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium adipisci et fugiat odio vel! Accusamus alias aliquid aut ea est eveniet iusto libero magnam magni minima, mollitia, reprehenderit, sunt vero.\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. A assumenda dolor, eligendi enim facilis hic impedit incidunt iusto laborum praesentium. Delectus dolor est molestiae numquam saepe sint, ullam velit voluptate!\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Amet deleniti expedita id maxime nisi non, nulla quibusdam recusandae repellat, similique temporibus voluptates. Commodi corporis cupiditate doloribus mollitia nesciunt, officia quod?', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(2, 'eng', 'Títol Exemple de producte 2', 'Subtítol Exemple de producte 2', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium adipisci et fugiat odio vel! Accusamus alias aliquid aut ea est eveniet iusto libero magnam magni minima, mollitia, reprehenderit, sunt vero.\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. A assumenda dolor, eligendi enim facilis hic impedit incidunt iusto laborum praesentium. Delectus dolor est molestiae numquam saepe sint, ullam velit voluptate!\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Amet deleniti expedita id maxime nisi non, nulla quibusdam recusandae repellat, similique temporibus voluptates. Commodi corporis cupiditate doloribus mollitia nesciunt, officia quod?', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(2, 'fra', 'Títol Exemple de producte 2', 'Subtítol Exemple de producte 2', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium adipisci et fugiat odio vel! Accusamus alias aliquid aut ea est eveniet iusto libero magnam magni minima, mollitia, reprehenderit, sunt vero.\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. A assumenda dolor, eligendi enim facilis hic impedit incidunt iusto laborum praesentium. Delectus dolor est molestiae numquam saepe sint, ullam velit voluptate!\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Amet deleniti expedita id maxime nisi non, nulla quibusdam recusandae repellat, similique temporibus voluptates. Commodi corporis cupiditate doloribus mollitia nesciunt, officia quod?', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(3, 'cat', 'Producte de prova 3', 'Subtítol Exemple de producte 3', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae cum dolorem dolores ducimus est fugiat illo in laudantium magni minima nemo nostrum officiis perspiciatis praesentium, provident quisquam soluta ullam voluptatum?\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam aut dolor eligendi iusto labore laboriosam molestias nam nobis, recusandae repellat, repellendus reprehenderit repudiandae saepe sit tempore tenetur unde voluptas voluptatibus.', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(3, 'spa', 'Producte de prova 3', 'Subtítol Exemple de producte 3', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae cum dolorem dolores ducimus est fugiat illo in laudantium magni minima nemo nostrum officiis perspiciatis praesentium, provident quisquam soluta ullam voluptatum?\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam aut dolor eligendi iusto labore laboriosam molestias nam nobis, recusandae repellat, repellendus reprehenderit repudiandae saepe sit tempore tenetur unde voluptas voluptatibus.', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(3, 'eng', 'Producte de prova 3', 'Subtítol Exemple de producte 3', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae cum dolorem dolores ducimus est fugiat illo in laudantium magni minima nemo nostrum officiis perspiciatis praesentium, provident quisquam soluta ullam voluptatum?\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam aut dolor eligendi iusto labore laboriosam molestias nam nobis, recusandae repellat, repellendus reprehenderit repudiandae saepe sit tempore tenetur unde voluptas voluptatibus.', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(3, 'fra', 'Producte de prova 3', 'Subtítol Exemple de producte 3', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae cum dolorem dolores ducimus est fugiat illo in laudantium magni minima nemo nostrum officiis perspiciatis praesentium, provident quisquam soluta ullam voluptatum?\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam aut dolor eligendi iusto labore laboriosam molestias nam nobis, recusandae repellat, repellendus reprehenderit repudiandae saepe sit tempore tenetur unde voluptas voluptatibus.', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(4, 'cat', 'Producte de prova 4', 'Subtítol Exemple de producte 4', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto asperiores blanditiis, distinctio dolores error fugit hic modi nobis placeat provident repellat reprehenderit soluta sunt tempora ullam, veniam veritatis voluptates, voluptatibus.\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid asperiores dolorem, enim ex fugiat inventore itaque nostrum sapiente! Culpa, odit sit? Asperiores culpa cupiditate dolore ea eligendi iusto laboriosam quo!', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(4, 'spa', 'Producte de prova 4', 'Subtítol Exemple de producte 4', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto asperiores blanditiis, distinctio dolores error fugit hic modi nobis placeat provident repellat reprehenderit soluta sunt tempora ullam, veniam veritatis voluptates, voluptatibus.\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid asperiores dolorem, enim ex fugiat inventore itaque nostrum sapiente! Culpa, odit sit? Asperiores culpa cupiditate dolore ea eligendi iusto laboriosam quo!', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(4, 'eng', 'Producte de prova 4', 'Subtítol Exemple de producte 4', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto asperiores blanditiis, distinctio dolores error fugit hic modi nobis placeat provident repellat reprehenderit soluta sunt tempora ullam, veniam veritatis voluptates, voluptatibus.\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid asperiores dolorem, enim ex fugiat inventore itaque nostrum sapiente! Culpa, odit sit? Asperiores culpa cupiditate dolore ea eligendi iusto laboriosam quo!', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(4, 'fra', 'Producte de prova 4', 'Subtítol Exemple de producte 4', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto asperiores blanditiis, distinctio dolores error fugit hic modi nobis placeat provident repellat reprehenderit soluta sunt tempora ullam, veniam veritatis voluptates, voluptatibus.\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid asperiores dolorem, enim ex fugiat inventore itaque nostrum sapiente! Culpa, odit sit? Asperiores culpa cupiditate dolore ea eligendi iusto laboriosam quo!', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(5, 'cat', 'Producte de prova 5', 'Subtítol Exemple de producte 4', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto asperiores blanditiis, distinctio dolores error fugit hic modi nobis placeat provident repellat reprehenderit soluta sunt tempora ullam, veniam veritatis voluptates, voluptatibus.\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid asperiores dolorem, enim ex fugiat inventore itaque nostrum sapiente! Culpa, odit sit? Asperiores culpa cupiditate dolore ea eligendi iusto laboriosam quo!', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(5, 'spa', 'Producte de prova 5', 'Subtítol Exemple de producte 4', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto asperiores blanditiis, distinctio dolores error fugit hic modi nobis placeat provident repellat reprehenderit soluta sunt tempora ullam, veniam veritatis voluptates, voluptatibus.\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid asperiores dolorem, enim ex fugiat inventore itaque nostrum sapiente! Culpa, odit sit? Asperiores culpa cupiditate dolore ea eligendi iusto laboriosam quo!', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(5, 'eng', 'Producte de prova 5', 'Subtítol Exemple de producte 4', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto asperiores blanditiis, distinctio dolores error fugit hic modi nobis placeat provident repellat reprehenderit soluta sunt tempora ullam, veniam veritatis voluptates, voluptatibus.\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid asperiores dolorem, enim ex fugiat inventore itaque nostrum sapiente! Culpa, odit sit? Asperiores culpa cupiditate dolore ea eligendi iusto laboriosam quo!', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(5, 'fra', 'Producte de prova 5', 'Subtítol Exemple de producte 4', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto asperiores blanditiis, distinctio dolores error fugit hic modi nobis placeat provident repellat reprehenderit soluta sunt tempora ullam, veniam veritatis voluptates, voluptatibus.\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid asperiores dolorem, enim ex fugiat inventore itaque nostrum sapiente! Culpa, odit sit? Asperiores culpa cupiditate dolore ea eligendi iusto laboriosam quo!', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(6, 'cat', 'Producte de prova 6', 'Subtítol Exemple de producte 4', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto asperiores blanditiis, distinctio dolores error fugit hic modi nobis placeat provident repellat reprehenderit soluta sunt tempora ullam, veniam veritatis voluptates, voluptatibus.\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid asperiores dolorem, enim ex fugiat inventore itaque nostrum sapiente! Culpa, odit sit? Asperiores culpa cupiditate dolore ea eligendi iusto laboriosam quo!', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(6, 'spa', 'Producte de prova 6', 'Subtítol Exemple de producte 4', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto asperiores blanditiis, distinctio dolores error fugit hic modi nobis placeat provident repellat reprehenderit soluta sunt tempora ullam, veniam veritatis voluptates, voluptatibus.\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid asperiores dolorem, enim ex fugiat inventore itaque nostrum sapiente! Culpa, odit sit? Asperiores culpa cupiditate dolore ea eligendi iusto laboriosam quo!', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(6, 'eng', 'Producte de prova 6', 'Subtítol Exemple de producte 4', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto asperiores blanditiis, distinctio dolores error fugit hic modi nobis placeat provident repellat reprehenderit soluta sunt tempora ullam, veniam veritatis voluptates, voluptatibus.\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid asperiores dolorem, enim ex fugiat inventore itaque nostrum sapiente! Culpa, odit sit? Asperiores culpa cupiditate dolore ea eligendi iusto laboriosam quo!', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(6, 'fra', 'Producte de prova 6', 'Subtítol Exemple de producte 4', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto asperiores blanditiis, distinctio dolores error fugit hic modi nobis placeat provident repellat reprehenderit soluta sunt tempora ullam, veniam veritatis voluptates, voluptatibus.\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid asperiores dolorem, enim ex fugiat inventore itaque nostrum sapiente! Culpa, odit sit? Asperiores culpa cupiditate dolore ea eligendi iusto laboriosam quo!', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(7, 'cat', 'Producte de prova 7', 'Subtítol Exemple de producte 4', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto asperiores blanditiis, distinctio dolores error fugit hic modi nobis placeat provident repellat reprehenderit soluta sunt tempora ullam, veniam veritatis voluptates, voluptatibus.\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid asperiores dolorem, enim ex fugiat inventore itaque nostrum sapiente! Culpa, odit sit? Asperiores culpa cupiditate dolore ea eligendi iusto laboriosam quo!', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(7, 'spa', 'Producte de prova 7', 'Subtítol Exemple de producte 4', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto asperiores blanditiis, distinctio dolores error fugit hic modi nobis placeat provident repellat reprehenderit soluta sunt tempora ullam, veniam veritatis voluptates, voluptatibus.\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid asperiores dolorem, enim ex fugiat inventore itaque nostrum sapiente! Culpa, odit sit? Asperiores culpa cupiditate dolore ea eligendi iusto laboriosam quo!', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(7, 'eng', 'Producte de prova 7', 'Subtítol Exemple de producte 4', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto asperiores blanditiis, distinctio dolores error fugit hic modi nobis placeat provident repellat reprehenderit soluta sunt tempora ullam, veniam veritatis voluptates, voluptatibus.\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid asperiores dolorem, enim ex fugiat inventore itaque nostrum sapiente! Culpa, odit sit? Asperiores culpa cupiditate dolore ea eligendi iusto laboriosam quo!', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(7, 'fra', 'Producte de prova 7', 'Subtítol Exemple de producte 4', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto asperiores blanditiis, distinctio dolores error fugit hic modi nobis placeat provident repellat reprehenderit soluta sunt tempora ullam, veniam veritatis voluptates, voluptatibus.\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid asperiores dolorem, enim ex fugiat inventore itaque nostrum sapiente! Culpa, odit sit? Asperiores culpa cupiditate dolore ea eligendi iusto laboriosam quo!', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(8, 'cat', 'Producte de prova 8', 'Subtítol Exemple de producte 4', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto asperiores blanditiis, distinctio dolores error fugit hic modi nobis placeat provident repellat reprehenderit soluta sunt tempora ullam, veniam veritatis voluptates, voluptatibus.\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid asperiores dolorem, enim ex fugiat inventore itaque nostrum sapiente! Culpa, odit sit? Asperiores culpa cupiditate dolore ea eligendi iusto laboriosam quo!', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(8, 'spa', 'Producte de prova 8', 'Subtítol Exemple de producte 4', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto asperiores blanditiis, distinctio dolores error fugit hic modi nobis placeat provident repellat reprehenderit soluta sunt tempora ullam, veniam veritatis voluptates, voluptatibus.\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid asperiores dolorem, enim ex fugiat inventore itaque nostrum sapiente! Culpa, odit sit? Asperiores culpa cupiditate dolore ea eligendi iusto laboriosam quo!', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(8, 'eng', 'Producte de prova 8', 'Subtítol Exemple de producte 4', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto asperiores blanditiis, distinctio dolores error fugit hic modi nobis placeat provident repellat reprehenderit soluta sunt tempora ullam, veniam veritatis voluptates, voluptatibus.\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid asperiores dolorem, enim ex fugiat inventore itaque nostrum sapiente! Culpa, odit sit? Asperiores culpa cupiditate dolore ea eligendi iusto laboriosam quo!', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
	(8, 'fra', 'Producte de prova 8', 'Subtítol Exemple de producte 4', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto asperiores blanditiis, distinctio dolores error fugit hic modi nobis placeat provident repellat reprehenderit soluta sunt tempora ullam, veniam veritatis voluptates, voluptatibus.\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid asperiores dolorem, enim ex fugiat inventore itaque nostrum sapiente! Culpa, odit sit? Asperiores culpa cupiditate dolore ea eligendi iusto laboriosam quo!', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
/*!40000 ALTER TABLE `product__product_language` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `product__product_related` (
  `product_related_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `product2_id` int(11) NOT NULL,
  `ordre` int(11) NOT NULL,
  PRIMARY KEY (`product_related_id`),
  KEY `product_id` (`product_id`),
  KEY `product2_id` (`product2_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `product__product_related` DISABLE KEYS */;
/*!40000 ALTER TABLE `product__product_related` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `product__product_to_sector` (
  `product_id` int(11) NOT NULL DEFAULT '0',
  `sector_id` int(11) NOT NULL DEFAULT '0',
  KEY `product_id` (`product_id`),
  KEY `sector_id` (`sector_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `product__product_to_sector` DISABLE KEYS */;
/*!40000 ALTER TABLE `product__product_to_sector` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `product__product_to_spec` (
  `product_spec_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL DEFAULT '0',
  `spec_id` int(11) NOT NULL DEFAULT '0',
  `spec` varchar(255) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`product_spec_id`),
  KEY `spec_id` (`spec_id`),
  KEY `product_id` (`product_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `product__product_to_spec` DISABLE KEYS */;
/*!40000 ALTER TABLE `product__product_to_spec` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `product__product_to_spec_language` (
  `product_spec_id` int(11) NOT NULL DEFAULT '0',
  `product_id` int(11) NOT NULL DEFAULT '0',
  `language` char(3) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `spec` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  KEY `product_spec_id` (`product_spec_id`),
  KEY `language` (`language`),
  KEY `product_id` (`product_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci ROW_FORMAT=DYNAMIC;

/*!40000 ALTER TABLE `product__product_to_spec_language` DISABLE KEYS */;
/*!40000 ALTER TABLE `product__product_to_spec_language` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `product__product_to_variation` (
  `product_variation_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL DEFAULT '0',
  `variation_id` int(11) NOT NULL DEFAULT '0',
  `variation_pvp` decimal(11,2) NOT NULL DEFAULT '0.00',
  `variation_pvd` decimal(11,2) NOT NULL DEFAULT '0.00',
  `variation_pvd2` decimal(11,2) NOT NULL DEFAULT '0.00',
  `variation_pvd3` decimal(11,2) NOT NULL DEFAULT '0.00',
  `variation_pvd4` decimal(11,2) NOT NULL DEFAULT '0.00',
  `variation_pvd5` decimal(11,2) NOT NULL DEFAULT '0.00',
  `variation_default` tinyint(1) NOT NULL DEFAULT '0',
  `variation_stock` int(11) NOT NULL DEFAULT '0',
  `variation_last_stock_units_public` int(11) NOT NULL DEFAULT '0',
  `variation_last_stock_units_admin` int(11) NOT NULL DEFAULT '0',
  `variation_sells` int(11) NOT NULL DEFAULT '0',
  `variation_ref` varchar(255) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `variation_ref_supplier` varchar(50) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `variation_others1` varchar(255) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `variation_others2` varchar(255) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `variation_others3` varchar(255) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `variation_others4` varchar(255) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `variation_status` enum('disabled','nostock','onsale') COLLATE utf8_spanish2_ci NOT NULL DEFAULT 'onsale',
  PRIMARY KEY (`product_variation_id`),
  KEY `product_id` (`product_id`),
  KEY `variation_id` (`variation_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `product__product_to_variation` DISABLE KEYS */;
INSERT INTO `product__product_to_variation` (`product_variation_id`, `product_id`, `variation_id`, `variation_pvp`, `variation_pvd`, `variation_pvd2`, `variation_pvd3`, `variation_pvd4`, `variation_pvd5`, `variation_default`, `variation_stock`, `variation_last_stock_units_public`, `variation_last_stock_units_admin`, `variation_sells`, `variation_ref`, `variation_ref_supplier`, `variation_others1`, `variation_others2`, `variation_others3`, `variation_others4`, `variation_status`) VALUES
	(1, 2, 1, 1.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, '', '', '', '', '', '', 'onsale'),
	(2, 2, 2, 2.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, '', '', '', '', '', '', 'onsale'),
	(3, 2, 3, 3.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, '', '', '', '', '', '', 'onsale'),
	(4, 2, 4, 4.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, '', '', '', '', '', '', 'onsale'),
	(5, 2, 6, 5.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, '', '', '', '', '', '', 'onsale'),
	(6, 2, 5, 6.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 1, '', '', '', '', '', '', 'onsale'),
	(7, 3, 2, 5.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, '', '', '', '', '', '', 'onsale'),
	(8, 3, 3, 10.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, '', '', '', '', '', '', 'onsale'),
	(9, 3, 4, 20.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, '', '', '', '', '', '', 'onsale'),
	(10, 3, 6, 30.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, '', '', '', '', '', '', 'onsale'),
	(11, 3, 5, 40.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, '', '', '', '', '', '', 'onsale');
/*!40000 ALTER TABLE `product__product_to_variation` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `product__product_to_variation_language` (
  `product_variation_id` int(11) NOT NULL DEFAULT '0',
  `product_id` int(11) NOT NULL DEFAULT '0',
  `language` char(3) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `variation_others1` text COLLATE utf8_spanish2_ci NOT NULL,
  `variation_others2` text COLLATE utf8_spanish2_ci NOT NULL,
  `variation_others3` text COLLATE utf8_spanish2_ci NOT NULL,
  `variation_others4` text COLLATE utf8_spanish2_ci NOT NULL,
  KEY `product_variation_id` (`product_variation_id`),
  KEY `product_id` (`product_id`),
  KEY `language` (`language`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci ROW_FORMAT=DYNAMIC;

/*!40000 ALTER TABLE `product__product_to_variation_language` DISABLE KEYS */;
/*!40000 ALTER TABLE `product__product_to_variation_language` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `product__promcode` (
  `promcode_id` int(10) NOT NULL AUTO_INCREMENT,
  `promcode` varchar(100) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `discount_pvp` decimal(11,2) NOT NULL DEFAULT '0.00',
  `discount_pvd` decimal(11,2) NOT NULL DEFAULT '0.00',
  `discount_pvd2` decimal(11,2) NOT NULL DEFAULT '0.00',
  `discount_pvd3` decimal(11,2) NOT NULL DEFAULT '0.00',
  `discount_pvd4` decimal(11,2) NOT NULL DEFAULT '0.00',
  `discount_pvd5` decimal(11,2) NOT NULL DEFAULT '0.00',
  `discount_type` enum('fixed','percent') COLLATE utf8_spanish2_ci NOT NULL DEFAULT 'fixed',
  `minimum_amount` decimal(11,2) NOT NULL DEFAULT '0.00',
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `bin` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`promcode_id`),
  KEY `bin` (`bin`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `product__promcode` DISABLE KEYS */;
/*!40000 ALTER TABLE `product__promcode` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `product__promcode_language` (
  `promcode_id` int(11) NOT NULL DEFAULT '0',
  `language` char(3) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `name` varchar(100) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  KEY `promcode_id` (`promcode_id`),
  KEY `language` (`language`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `product__promcode_language` DISABLE KEYS */;
/*!40000 ALTER TABLE `product__promcode_language` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `product__quantityconcept` (
  `quantityconcept_id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`quantityconcept_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `product__quantityconcept` DISABLE KEYS */;
/*!40000 ALTER TABLE `product__quantityconcept` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `product__quantityconcept_language` (
  `quantityconcept_id` int(11) NOT NULL DEFAULT '0',
  `language` char(3) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `quantityconcept` varchar(150) COLLATE utf8_spanish2_ci NOT NULL,
  KEY `quantityconcept_id` (`quantityconcept_id`),
  KEY `language` (`language`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `product__quantityconcept_language` DISABLE KEYS */;
/*!40000 ALTER TABLE `product__quantityconcept_language` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `product__rate` (
  `rate_id` int(11) NOT NULL AUTO_INCREMENT,
  `country_id` char(2) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `provincia_id` int(11) DEFAULT NULL,
  `rate` decimal(8,2) DEFAULT NULL,
  `rate_encrease_ondelivery` decimal(8,2) DEFAULT NULL,
  `minimum_buy_free_send` decimal(8,2) DEFAULT '0.00',
  PRIMARY KEY (`rate_id`),
  KEY `country_id` (`country_id`),
  KEY `provincia_id` (`provincia_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `product__rate` DISABLE KEYS */;
INSERT INTO `product__rate` (`rate_id`, `country_id`, `provincia_id`, `rate`, `rate_encrease_ondelivery`, `minimum_buy_free_send`) VALUES
	(3, 'ES', NULL, 8.00, NULL, 0.00),
	(4, 'AD', NULL, 20.00, NULL, 0.00),
	(5, 'FR', NULL, 20.00, NULL, 0.00);
/*!40000 ALTER TABLE `product__rate` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `product__sector` (
  `sector_id` int(11) NOT NULL AUTO_INCREMENT,
  `ordre` int(11) NOT NULL DEFAULT '0',
  `bin` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`sector_id`),
  KEY `bin` (`bin`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `product__sector` DISABLE KEYS */;
/*!40000 ALTER TABLE `product__sector` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `product__sector_image` (
  `image_id` int(11) NOT NULL AUTO_INCREMENT,
  `sector_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `name_original` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `size` int(11) NOT NULL DEFAULT '0',
  `ordre` int(11) NOT NULL DEFAULT '0',
  `main_image` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`image_id`),
  KEY `sector_id` (`sector_id`),
  KEY `main_image` (`main_image`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `product__sector_image` DISABLE KEYS */;
/*!40000 ALTER TABLE `product__sector_image` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `product__sector_image_language` (
  `image_id` int(11) NOT NULL DEFAULT '0',
  `language` char(3) COLLATE utf8_spanish2_ci NOT NULL,
  `image_alt` varchar(150) COLLATE utf8_spanish2_ci NOT NULL,
  `image_title` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  KEY `image_id` (`image_id`),
  KEY `language` (`language`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `product__sector_image_language` DISABLE KEYS */;
/*!40000 ALTER TABLE `product__sector_image_language` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `product__sector_language` (
  `sector_id` int(11) NOT NULL DEFAULT '0',
  `language` char(3) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `sector` varchar(100) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `sector_description` text COLLATE utf8_spanish2_ci NOT NULL,
  `page_title` varchar(255) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `page_description` varchar(255) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `page_keywords` varchar(255) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  KEY `sector_id` (`sector_id`),
  KEY `language` (`language`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `product__sector_language` DISABLE KEYS */;
/*!40000 ALTER TABLE `product__sector_language` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `product__sellpoint` (
  `sellpoint_id` int(11) NOT NULL AUTO_INCREMENT,
  `empresa` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `adress` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `poblacio` varchar(100) COLLATE utf8_spanish2_ci NOT NULL,
  `cp` char(50) COLLATE utf8_spanish2_ci NOT NULL,
  `country_id` char(2) COLLATE utf8_spanish2_ci NOT NULL,
  `municipi_id` int(11) NOT NULL,
  `comarca_id` int(11) NOT NULL,
  `provincia_id` int(11) NOT NULL,
  `comunitat_id` int(11) NOT NULL,
  `telefon` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `telefon2` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `fax` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `correu` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `correu2` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `bin` tinyint(2) NOT NULL DEFAULT '0',
  `latitude` decimal(9,7) NOT NULL DEFAULT '0.0000000',
  `longitude` decimal(9,7) NOT NULL DEFAULT '0.0000000',
  `zoom` tinyint(1) NOT NULL DEFAULT '0',
  `status` enum('prepare','review','public','archived') COLLATE utf8_spanish2_ci NOT NULL DEFAULT 'prepare',
  `seller_kind` enum('shop','dealer') COLLATE utf8_spanish2_ci NOT NULL DEFAULT 'shop',
  PRIMARY KEY (`sellpoint_id`),
  KEY `country_id` (`country_id`),
  KEY `municipi_id` (`municipi_id`),
  KEY `comarca_id` (`comarca_id`),
  KEY `provincia_id` (`provincia_id`),
  KEY `comunitat_id` (`comunitat_id`),
  KEY `bin` (`bin`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `product__sellpoint` DISABLE KEYS */;
/*!40000 ALTER TABLE `product__sellpoint` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `product__sellpoint_image` (
  `image_id` int(11) NOT NULL AUTO_INCREMENT,
  `sellpoint_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `name_original` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `size` int(11) NOT NULL DEFAULT '0',
  `ordre` int(11) NOT NULL DEFAULT '0',
  `main_image` int(1) NOT NULL DEFAULT '0',
  `imagecat_id` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`image_id`),
  KEY `sellpoint_id` (`sellpoint_id`),
  KEY `main_image` (`main_image`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `product__sellpoint_image` DISABLE KEYS */;
/*!40000 ALTER TABLE `product__sellpoint_image` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `product__sellpoint_imagecat` (
  `imagecat_id` int(11) NOT NULL AUTO_INCREMENT,
  `imagecat` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `ordre` int(11) DEFAULT NULL,
  PRIMARY KEY (`imagecat_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci ROW_FORMAT=COMPACT;

/*!40000 ALTER TABLE `product__sellpoint_imagecat` DISABLE KEYS */;
/*!40000 ALTER TABLE `product__sellpoint_imagecat` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `product__sellpoint_image_language` (
  `image_id` int(11) NOT NULL DEFAULT '0',
  `language` char(3) COLLATE utf8_spanish2_ci NOT NULL,
  `image_alt` varchar(150) COLLATE utf8_spanish2_ci NOT NULL,
  `image_title` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  KEY `image_id` (`image_id`),
  KEY `language` (`language`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `product__sellpoint_image_language` DISABLE KEYS */;
/*!40000 ALTER TABLE `product__sellpoint_image_language` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `product__sellpoint_language` (
  `sellpoint_id` int(11) NOT NULL DEFAULT '0',
  `language` char(3) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `sellpoint` text COLLATE utf8_spanish2_ci NOT NULL,
  `sellpoint_description` text COLLATE utf8_spanish2_ci NOT NULL,
  `sellpoint_file_name` varchar(100) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `sellpoint_old_file_name` varchar(100) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `url1` varchar(255) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `url1_name` varchar(100) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  KEY `sellpoint_id` (`sellpoint_id`),
  KEY `language` (`language`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `product__sellpoint_language` DISABLE KEYS */;
/*!40000 ALTER TABLE `product__sellpoint_language` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `product__spec` (
  `spec_id` int(11) NOT NULL AUTO_INCREMENT,
  `spec_category_id` int(11) NOT NULL,
  `spec_type` enum('text_no_lang','text','textarea','checkbox','int','select') COLLATE utf8_spanish2_ci NOT NULL,
  `spec_unit` varchar(10) COLLATE utf8_spanish2_ci NOT NULL,
  `ordre` int(11) NOT NULL DEFAULT '0',
  `bin` tinyint(1) NOT NULL,
  PRIMARY KEY (`spec_id`),
  KEY `spec_category_id` (`spec_category_id`),
  KEY `bin` (`bin`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `product__spec` DISABLE KEYS */;
/*!40000 ALTER TABLE `product__spec` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `product__spec_category` (
  `spec_category_id` int(11) NOT NULL AUTO_INCREMENT,
  `ordre` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`spec_category_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `product__spec_category` DISABLE KEYS */;
/*!40000 ALTER TABLE `product__spec_category` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `product__spec_category_language` (
  `spec_category_id` int(11) NOT NULL,
  `language` char(3) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `spec_category` varchar(200) COLLATE utf8_spanish2_ci NOT NULL,
  KEY `spec_category_id` (`spec_category_id`),
  KEY `language` (`language`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `product__spec_category_language` DISABLE KEYS */;
/*!40000 ALTER TABLE `product__spec_category_language` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `product__spec_language` (
  `spec_id` int(11) NOT NULL DEFAULT '0',
  `language` char(3) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `spec` varchar(400) COLLATE utf8_spanish2_ci NOT NULL,
  KEY `spec_id` (`spec_id`),
  KEY `language` (`language`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `product__spec_language` DISABLE KEYS */;
/*!40000 ALTER TABLE `product__spec_language` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `product__tax` (
  `tax_id` int(11) NOT NULL AUTO_INCREMENT,
  `tax_name` varchar(50) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `tax_value` tinyint(2) NOT NULL DEFAULT '0',
  `equivalencia` decimal(2,1) NOT NULL DEFAULT '0.0',
  `bin` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`tax_id`),
  KEY `bin` (`bin`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `product__tax` DISABLE KEYS */;
INSERT INTO `product__tax` (`tax_id`, `tax_name`, `tax_value`, `equivalencia`, `bin`) VALUES
	(1, 'Normal', 21, 4.0, 0),
	(2, 'Reduit', 10, 1.0, 0),
	(3, 'Super Reduit', 4, 0.5, 0),
	(4, 'Intracomunitari', 20, 0.0, 0);
/*!40000 ALTER TABLE `product__tax` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `product__variation` (
  `variation_id` int(11) NOT NULL AUTO_INCREMENT,
  `variation_category_id` int(11) NOT NULL,
  `variation_pvp` decimal(11,2) NOT NULL,
  `variation_pvd` decimal(11,2) NOT NULL,
  `variation_pvd2` decimal(11,2) NOT NULL DEFAULT '0.00',
  `variation_pvd3` decimal(11,2) NOT NULL DEFAULT '0.00',
  `variation_pvd4` decimal(11,2) NOT NULL DEFAULT '0.00',
  `variation_pvd5` decimal(11,2) NOT NULL DEFAULT '0.00',
  `variation_checked` tinyint(1) NOT NULL DEFAULT '0',
  `variation_default` tinyint(1) NOT NULL DEFAULT '0',
  `ordre` int(11) NOT NULL DEFAULT '0',
  `bin` tinyint(1) NOT NULL,
  PRIMARY KEY (`variation_id`),
  KEY `variation_category_id` (`variation_category_id`),
  KEY `variation_checked` (`variation_checked`),
  KEY `variation_default` (`variation_default`),
  KEY `bin` (`bin`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `product__variation` DISABLE KEYS */;
INSERT INTO `product__variation` (`variation_id`, `variation_category_id`, `variation_pvp`, `variation_pvd`, `variation_pvd2`, `variation_pvd3`, `variation_pvd4`, `variation_pvd5`, `variation_checked`, `variation_default`, `ordre`, `bin`) VALUES
	(1, 2, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0),
	(2, 2, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0),
	(3, 2, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0),
	(4, 2, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0),
	(5, 1, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0),
	(6, 1, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0);
/*!40000 ALTER TABLE `product__variation` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `product__variation_category` (
  `variation_category_id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`variation_category_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `product__variation_category` DISABLE KEYS */;
INSERT INTO `product__variation_category` (`variation_category_id`) VALUES
	(1),
	(2);
/*!40000 ALTER TABLE `product__variation_category` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `product__variation_category_language` (
  `variation_category_id` int(11) NOT NULL,
  `language` char(3) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `variation_category` varchar(200) COLLATE utf8_spanish2_ci NOT NULL,
  `variation_description` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  KEY `variation_category_id` (`variation_category_id`),
  KEY `language` (`language`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `product__variation_category_language` DISABLE KEYS */;
INSERT INTO `product__variation_category_language` (`variation_category_id`, `language`, `variation_category`, `variation_description`) VALUES
	(1, 'cat', 'Talla', ''),
	(1, 'spa', 'Talla', ''),
	(1, 'eng', 'Talla', ''),
	(1, 'fra', 'Talla', ''),
	(2, 'cat', 'Volum', ''),
	(2, 'spa', 'Volum', ''),
	(2, 'eng', 'Volum', ''),
	(2, 'fra', 'Volum', '');
/*!40000 ALTER TABLE `product__variation_category_language` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `product__variation_image` (
  `image_id` int(11) NOT NULL AUTO_INCREMENT,
  `variation_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `name_original` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `size` int(11) NOT NULL DEFAULT '0',
  `ordre` int(11) NOT NULL DEFAULT '0',
  `main_image` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`image_id`),
  KEY `variation_id` (`variation_id`),
  KEY `main_image` (`main_image`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `product__variation_image` DISABLE KEYS */;
/*!40000 ALTER TABLE `product__variation_image` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `product__variation_image_language` (
  `image_id` int(11) NOT NULL DEFAULT '0',
  `language` char(3) COLLATE utf8_spanish2_ci NOT NULL,
  `image_alt` varchar(150) COLLATE utf8_spanish2_ci NOT NULL,
  `image_title` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  KEY `image_id` (`image_id`),
  KEY `language` (`language`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `product__variation_image_language` DISABLE KEYS */;
/*!40000 ALTER TABLE `product__variation_image_language` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `product__variation_language` (
  `variation_id` int(11) NOT NULL DEFAULT '0',
  `language` char(3) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `variation` varchar(255) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `variation_subtitle` varchar(255) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `variation_description` text COLLATE utf8_spanish2_ci NOT NULL,
  KEY `variation_id` (`variation_id`),
  KEY `language` (`language`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `product__variation_language` DISABLE KEYS */;
INSERT INTO `product__variation_language` (`variation_id`, `language`, `variation`, `variation_subtitle`, `variation_description`) VALUES
	(1, 'cat', '100 ml', '', ''),
	(1, 'spa', '100 ml', '', ''),
	(1, 'eng', '100 ml', '', ''),
	(1, 'fra', '100 ml', '', ''),
	(2, 'cat', '200 ml', '', ''),
	(2, 'spa', '200 ml', '', ''),
	(2, 'eng', '200 ml', '', ''),
	(2, 'fra', '200 ml', '', ''),
	(3, 'cat', '300 ml', '', ''),
	(3, 'spa', '300 ml', '', ''),
	(3, 'eng', '300 ml', '', ''),
	(3, 'fra', '300 ml', '', ''),
	(4, 'cat', '400 ml', '', ''),
	(4, 'spa', '400 ml', '', ''),
	(4, 'eng', '400 ml', '', ''),
	(4, 'fra', '400 ml', '', ''),
	(5, 'cat', 'S', '', ''),
	(5, 'spa', 'S', '', ''),
	(5, 'eng', 'S', '', ''),
	(5, 'fra', 'S', '', ''),
	(6, 'cat', 'M', '', ''),
	(6, 'spa', 'M', '', ''),
	(6, 'eng', 'M', '', ''),
	(6, 'fra', 'M', '', '');
/*!40000 ALTER TABLE `product__variation_language` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `user__configadmin` (
  `configadmin_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `value` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  PRIMARY KEY (`configadmin_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `user__configadmin` DISABLE KEYS */;
INSERT INTO `user__configadmin` (`configadmin_id`, `name`, `value`) VALUES
	(1, 'im_admin_thumb_w', '200'),
	(2, 'im_admin_thumb_h', '133'),
	(3, 'im_admin_thumb_q', '80'),
	(4, 'im_thumb_w', '200'),
	(5, 'im_thumb_h', '150'),
	(6, 'im_thumb_q', '80'),
	(7, 'im_details_w', '540'),
	(8, 'im_details_h', '360'),
	(9, 'im_details_q', '80'),
	(10, 'im_medium_w', '900'),
	(11, 'im_medium_h', '675'),
	(12, 'im_medium_q', '70'),
	(13, 'im_big_w', '1200'),
	(14, 'im_big_h', '900'),
	(15, 'im_big_q', '70'),
	(16, 'im_list_cols', '2'),
	(17, 'im_custom_ratios', '');
/*!40000 ALTER TABLE `user__configadmin` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `user__group` (
  `group_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `ordre` int(11) NOT NULL DEFAULT '0',
  `edit_only_itself` tinyint(1) NOT NULL DEFAULT '0',
  `show_seo` tinyint(1) NOT NULL DEFAULT '0',
  `show_html_editor` tinyint(1) NOT NULL DEFAULT '0',
  `edit_only_descriptions` tinyint(1) NOT NULL DEFAULT '0',
  `is_editable` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `user__group` DISABLE KEYS */;
INSERT INTO `user__group` (`group_id`, `name`, `ordre`, `edit_only_itself`, `show_seo`, `show_html_editor`, `edit_only_descriptions`, `is_editable`) VALUES
	(1, 'admin total', 0, 0, 1, 1, 0, 1),
	(2, 'Administrador', 0, 0, 1, 0, 0, 1),
	(3, 'Lectors', 0, 0, 0, 0, 0, 1),
	(4, 'Escritors', 0, 0, 0, 0, 0, 1);
/*!40000 ALTER TABLE `user__group` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `user__log` (
  `log_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `date` date NOT NULL DEFAULT '0000-00-00',
  `login` time NOT NULL DEFAULT '00:00:00',
  `logout` time NOT NULL DEFAULT '00:00:00',
  `ip` varchar(50) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`log_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `user__log` DISABLE KEYS */;
INSERT INTO `user__log` (`log_id`, `user_id`, `date`, `login`, `logout`, `ip`) VALUES
	(1, 1, '2018-01-11', '19:18:10', '00:00:00', '127.0.0.1'),
	(2, 1, '2018-05-31', '16:52:01', '00:00:00', '127.0.0.1'),
	(3, 1, '2019-02-06', '15:35:22', '00:00:00', '127.0.0.1'),
	(4, 1, '2019-06-04', '10:17:22', '00:00:00', '127.0.0.1'),
	(5, 1, '2019-07-23', '09:51:21', '00:00:00', '127.0.0.1');
/*!40000 ALTER TABLE `user__log` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `user__user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(100) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `passw` varchar(100) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `name` varchar(100) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `surname` varchar(100) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `email` varchar(200) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `phone1` varchar(50) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `phone2` varchar(50) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `phone3` varchar(50) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `phone4` varchar(50) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `group_id` int(11) NOT NULL DEFAULT '0',
  `observations` text COLLATE utf8_spanish2_ci NOT NULL,
  `facebook_access_token` text COLLATE utf8_spanish2_ci NOT NULL,
  `facebook_user_id` text COLLATE utf8_spanish2_ci NOT NULL,
  `facebook_user_details` varchar(400) COLLATE utf8_spanish2_ci NOT NULL,
  `twitter_user_id` text COLLATE utf8_spanish2_ci NOT NULL,
  `twitter_token` text COLLATE utf8_spanish2_ci NOT NULL,
  `twitter_token_secret` text COLLATE utf8_spanish2_ci NOT NULL,
  `twitter_user_details` varchar(400) COLLATE utf8_spanish2_ci NOT NULL,
  `status` enum('private','public') COLLATE utf8_spanish2_ci NOT NULL DEFAULT 'private',
  `bin` tinyint(2) NOT NULL DEFAULT '0',
  `latitude` decimal(9,7) NOT NULL,
  `longitude` decimal(9,7) NOT NULL,
  `zoom` tinyint(2) NOT NULL,
  `center_latitude` decimal(9,7) NOT NULL,
  `center_longitude` decimal(9,7) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `user__user` DISABLE KEYS */;
INSERT INTO `user__user` (`user_id`, `login`, `passw`, `name`, `surname`, `email`, `phone1`, `phone2`, `phone3`, `phone4`, `group_id`, `observations`, `facebook_access_token`, `facebook_user_id`, `facebook_user_details`, `twitter_user_id`, `twitter_token`, `twitter_token_secret`, `twitter_user_details`, `status`, `bin`, `latitude`, `longitude`, `zoom`, `center_latitude`, `center_longitude`) VALUES
	(1, 'letndmarc', 'P@sswor1', 'Marc', 'Sanahuja', 'sanahuja@mail.com', '', '', '', '', 1, '', '', '', '', '', '', '', '', 'private', 0, 0.0000000, 0.0000000, 0, 0.0000000, 0.0000000),
	(2, 'letndmarc', 'P@sswor1', 'Marc', 'Sanahuja', 'info@letnd.com', '', '', '', '', 1, '', '', '', '', '', '', '', '', 'private', 0, 0.0000000, 0.0000000, 0, 0.0000000, 0.0000000),
	(3, 'usuari', 'usuari', 'Usuari', '', '', '', '', '', '', 2, '', '', '', '', '', '', '', '', 'private', 0, 0.0000000, 0.0000000, 0, 0.0000000, 0.0000000);
/*!40000 ALTER TABLE `user__user` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `user__user_image` (
  `image_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `imagecat_id` int(11) NOT NULL DEFAULT '1',
  `name` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `name_original` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `size` int(11) NOT NULL DEFAULT '0',
  `ordre` int(11) NOT NULL DEFAULT '0',
  `main_image` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`image_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `user__user_image` DISABLE KEYS */;
/*!40000 ALTER TABLE `user__user_image` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `user__user_image_language` (
  `image_id` int(11) NOT NULL DEFAULT '0',
  `language` char(3) COLLATE utf8_spanish2_ci NOT NULL,
  `image_alt` varchar(150) COLLATE utf8_spanish2_ci NOT NULL,
  `image_title` varchar(255) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `user__user_image_language` DISABLE KEYS */;
/*!40000 ALTER TABLE `user__user_image_language` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `user__user_language` (
  `user_id` int(11) NOT NULL DEFAULT '0',
  `language` char(3) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `description` text COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `user__user_language` DISABLE KEYS */;
INSERT INTO `user__user_language` (`user_id`, `language`, `description`) VALUES
	(1, 'cat', ''),
	(2, 'cat', ''),
	(3, 'cat', ''),
	(1, 'spa', ''),
	(2, 'spa', ''),
	(3, 'spa', ''),
	(1, 'eng', ''),
	(2, 'eng', ''),
	(3, 'eng', ''),
	(1, 'fra', ''),
	(2, 'fra', ''),
	(3, 'fra', '');
/*!40000 ALTER TABLE `user__user_language` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `user__user_to_language` (
  `user_to_language_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `language_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_to_language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `user__user_to_language` DISABLE KEYS */;
/*!40000 ALTER TABLE `user__user_to_language` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `utility__configadmin` (
  `configadmin_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `value` varchar(255) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`name`,`configadmin_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `utility__configadmin` DISABLE KEYS */;
INSERT INTO `utility__configadmin` (`configadmin_id`, `name`, `value`) VALUES
	(1, 'register_percent', '0.5'),
	(2, 'notary_percent', '1'),
	(3, 'mortgage_valuation', '250'),
	(4, 'mortgage_register_percent', '0.5'),
	(5, 'mortgage_percent', '80'),
	(6, 'mortgage_open_percent', '0.5'),
	(7, 'mortgage_notary_percent', '1'),
	(8, 'mortgage_length', '25'),
	(9, 'mortgage_initial_interest', '2.7'),
	(11, 'mortgage_cancel_percent', '0.5'),
	(12, 'habitability_card', '200'),
	(13, 'estate_commission_percent', '3'),
	(14, 'efficiency', '150'),
	(15, 'agency_percent', '0');
/*!40000 ALTER TABLE `utility__configadmin` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `utility__taxtype` (
  `taxtype_id` int(11) NOT NULL AUTO_INCREMENT,
  `tax` int(11) DEFAULT NULL,
  `taxtype` enum('IVA','ITP') COLLATE utf8_spanish2_ci DEFAULT NULL,
  PRIMARY KEY (`taxtype_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `utility__taxtype` DISABLE KEYS */;
INSERT INTO `utility__taxtype` (`taxtype_id`, `tax`, `taxtype`) VALUES
	(1, 10, 'IVA'),
	(2, 10, 'ITP'),
	(3, 21, 'IVA'),
	(4, 5, 'ITP'),
	(5, 5, 'ITP');
/*!40000 ALTER TABLE `utility__taxtype` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `utility__taxtype_language` (
  `taxtype_id` int(11) NOT NULL,
  `language` char(3) COLLATE utf8_spanish2_ci NOT NULL,
  `description` varchar(200) COLLATE utf8_spanish2_ci NOT NULL,
  KEY `taxtype_id` (`taxtype_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `utility__taxtype_language` DISABLE KEYS */;
INSERT INTO `utility__taxtype_language` (`taxtype_id`, `language`, `description`) VALUES
	(1, 'cat', 'Vivenda nova'),
	(2, 'cat', 'Vivenda segona mà'),
	(3, 'cat', 'Local i garatge nou'),
	(4, 'cat', 'Reducció menors 32 anys renda inferior a 30.000'),
	(5, 'cat', 'Discapacitats'),
	(1, 'spa', 'Vivienda nueva'),
	(2, 'spa', 'Vivienda segunda mano'),
	(3, 'spa', 'Local i garaje nuevo'),
	(4, 'spa', 'Reducción menores 32 años renta inferior a 30.000'),
	(5, 'spa', 'Discapacitados'),
	(1, 'fra', 'Vivenda nova'),
	(2, 'fra', 'Vivenda segona mà'),
	(3, 'fra', 'Local i garatge nou'),
	(4, 'fra', 'Reducció menors 32 anys renda inferior a 30.000'),
	(5, 'fra', 'Discapacitats'),
	(1, 'eng', ''),
	(2, 'eng', ''),
	(3, 'eng', ''),
	(4, 'eng', ''),
	(5, 'eng', '');
/*!40000 ALTER TABLE `utility__taxtype_language` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `web__banner` (
  `banner_id` int(11) NOT NULL AUTO_INCREMENT,
  `url1_target` enum('_blank','_self') COLLATE utf8_spanish2_ci NOT NULL DEFAULT '_blank',
  `ordre` int(11) NOT NULL DEFAULT '0',
  `status` enum('prepare','review','public','archived') COLLATE utf8_spanish2_ci NOT NULL DEFAULT 'prepare',
  `place_id` int(11) NOT NULL DEFAULT '0',
  `bin` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`banner_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `web__banner` DISABLE KEYS */;
/*!40000 ALTER TABLE `web__banner` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `web__banner_file` (
  `file_id` int(11) NOT NULL AUTO_INCREMENT,
  `banner_id` int(11) NOT NULL DEFAULT '0',
  `language` varchar(3) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `name` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `name_original` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `size` int(11) NOT NULL DEFAULT '0',
  `ordre` int(11) NOT NULL DEFAULT '0',
  `main_file` int(1) NOT NULL DEFAULT '0',
  `filecat_id` int(11) NOT NULL DEFAULT '1',
  `public` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`file_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `web__banner_file` DISABLE KEYS */;
/*!40000 ALTER TABLE `web__banner_file` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `web__banner_language` (
  `banner_id` int(11) NOT NULL DEFAULT '0',
  `language` varchar(3) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `url1` varchar(250) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `url1_name` varchar(100) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `banner_text` text COLLATE utf8_spanish2_ci NOT NULL,
  `banner_text2` text COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `web__banner_language` DISABLE KEYS */;
/*!40000 ALTER TABLE `web__banner_language` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `web__banner_place` (
  `place_id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`place_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `web__banner_place` DISABLE KEYS */;
INSERT INTO `web__banner_place` (`place_id`) VALUES
	(1);
/*!40000 ALTER TABLE `web__banner_place` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `web__banner_place_language` (
  `place_id` int(11) NOT NULL DEFAULT '0',
  `language` char(3) COLLATE utf8_spanish2_ci NOT NULL,
  `place` varchar(100) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `web__banner_place_language` DISABLE KEYS */;
INSERT INTO `web__banner_place_language` (`place_id`, `language`, `place`) VALUES
	(1, 'cat', 'General'),
	(1, 'spa', 'General'),
	(1, 'eng', 'General'),
	(1, 'fra', '');
/*!40000 ALTER TABLE `web__banner_place_language` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `web__configadmin_banner` (
  `configadmin_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `value` varchar(255) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `language` char(3) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`configadmin_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci ROW_FORMAT=DYNAMIC;

/*!40000 ALTER TABLE `web__configadmin_banner` DISABLE KEYS */;
INSERT INTO `web__configadmin_banner` (`configadmin_id`, `name`, `value`, `language`) VALUES
	(1, 'banner_info_size', '', ''),
	(2, 'show_banner_text2', '0', ''),
	(3, 'im_admin_thumb_w', '200', ''),
	(4, 'banner_text_type', 'html', ''),
	(5, 'banner_text2_type', 'html', '');
/*!40000 ALTER TABLE `web__configadmin_banner` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `web__place` (
  `place_id` int(11) NOT NULL AUTO_INCREMENT,
  `place` varchar(100) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `adress` varchar(255) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `numstreet` varchar(10) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `block` varchar(10) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `flat` varchar(10) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `door` varchar(10) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `zip` varchar(20) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `zone_id` int(11) NOT NULL DEFAULT '0',
  `municipi_id` int(11) NOT NULL DEFAULT '0',
  `comarca_id` int(11) NOT NULL DEFAULT '0',
  `provincia_id` int(11) NOT NULL DEFAULT '0',
  `country_id` int(11) NOT NULL DEFAULT '1',
  `status` enum('prepare','review','public','archived') COLLATE utf8_spanish2_ci NOT NULL DEFAULT 'prepare',
  PRIMARY KEY (`place_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40000 ALTER TABLE `web__place` DISABLE KEYS */;
/*!40000 ALTER TABLE `web__place` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
