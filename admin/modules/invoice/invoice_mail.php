<?

use PHPMailer\PHPMailer\Exception;


/**
 * InvoiceMail
 *
 * S'ha d'afegir un camp a
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2017
 * @version $Id$
 * @access public
 *
 */
class InvoiceMail {
	var $body_count = 0, $rs, $module, $custumer_name, $from = 'admin@letnd.cat', $tpl, $subject, $body, $insert_id, $cf;

	function __construct( &$module ) {

		header( 'Content-type: text/html; charset=utf-8' ); // necessari pel flush
		@ini_set('zlib.output_compression',0);
		@ini_set('implicit_flush',1);
		@ob_end_clean();
		ob_implicit_flush(1);

		Main::load_class( 'invoice', 'pdf', 'admin' );

		$this->module = &$module;

		$this->tpl = new phemplate( PATH_TEMPLATES_PUBLIC );

		global $configuration;
		$configuration['m_username'] = $this->from;
		$configuration['m_password'] = $configuration['m_password_invoice'];

		$this->start_send_mail();
	}

	public function send( $invoice_id ) {

		$this->tpl->set_file( 'mail.tpl', true );
		$this->set_customer( $invoice_id );
		$this->set_vars_mail();
		$this->set_body();
		$this->insert_message_rs( $invoice_id );
		$invoice_pdf = $this->get_pdf( $invoice_id );
		$this->send_email( $invoice_pdf );
		$this->delete_pdf ( $invoice_pdf );

	}

	private function set_customer( $invoice_id ) {

		$this->rs = Db::get_row( "SELECT * FROM invoice__invoice WHERE invoice_id = $invoice_id" );

		$rs = &$this->rs;
		if ( $rs['custumer_billing'] == 'citizen' ) {
			$this->custumer_name = $rs['custumer_name'] . ' ' . $rs['custumer_surname1'] . ' ' . $rs['custumer_surname2'];
		} else {
			$this->custumer_name = $rs['custumer_company'];
		}

	}

	private function set_vars_mail() {

		$tpl = &$this->tpl;

		$custumer_name  = $this->custumer_name;
		$invoice_serial = $this->rs['invoice_serial'];
		$invoice_number = $this->rs['invoice_number'];
		$this->subject  = "Factura 9Sistema Serveis Informàtics 2005 S.L. - LETND - $invoice_serial$invoice_number";

		$preview = false;
		$this->tpl->set_vars( $this->module->caption );
		$tpl->set_var( "url", HOST_URL );
		$tpl->set_var( 'short_host_url', substr( HOST_URL, 7, - 1 ) );
		$tpl->set_var( "preview", $preview );
		$tpl->set_var( 'remove_link', '' );
		$tpl->set_var( 'remove_text', '' );
		$tpl->set_var( 'page_title', $this->subject );

	}

	private function set_body() {

		// TODO-i Enviar en l'idioma del client
		$this->body = "
			<p>Benvolgut client,</p>
			<p>L’informem que properament es girarà un rebut amb l’import de les factures adjuntes a aquest correu electrònic.</p>
			<p></p>
			<p>En el cas que el mètode de pagament establert sigui el de transferència bancària, el número de compte pertinent es troba també en la factura adjuntada.</p>
			<p></p>
			<p></p>
			<p></p>
			<p>Atentament Letnd.</p>
		";
		$this->tpl->set_var( 'content', $this->body );

	}

	private function insert_message_rs( $invoice_id ) {

		// TODO-i Quan s'ha parat per algún error de mail incorrecte, hauria de comprobar si existeix el missatge abans de insertar, sino queden duplicats i amb status "start"
		$subject     = Db::qstr( $this->subject );
		$custumer_id = $this->rs['custumer_id'];

		$query = "
			INSERT INTO invoice__message
			 (`entered`, `subject`, `status`, `custumer_id`, `invoice_id`) 
			 VALUES (now(), $subject, 'start', '$custumer_id', '$invoice_id');";
		Db::execute( $query );

		$this->insert_id = Db::insert_id();

	}

	private function send_email( $invoice_pdf ) {

		$custumer_name = $this->custumer_name;
		$billing_mail  = $this->rs['custumer_billing_mail'];

		$this->output_js( "Preparant missatge", 'vert' );

		$mail = $GLOBALS['gl_current_mailer'] = get_mailer( $this->cf, true );
		$mail->IsHTML(true);

		//$mail->ConfirmReadingTo = $this->from;

		if ( ! $this->send_mail_add_files( $mail, $invoice_pdf ) ) {
			return;
		}

		$mail->Subject = $this->subject;
		$mail->addAddress( $billing_mail );

		$body = $this->tpl->process();
		$this->emogrify( $body );
		$mail->Body = $body;

		// envio
		try {

			$mail->Send();

			$this->output_js( " - Missatge enviat a : <strong>$custumer_name</strong> - $billing_mail<br><br>", 'vert' );
			$this->set_message_status( 'sent' );

		} catch ( Exception $e ) {

			// error i no s'ha enviat el mail
			$this->output_js( " - Error al enviar mail a : <strong>$custumer_name</strong> - $billing_mail<br><br>", 'vermell' );

			$error = $e->errorMessage();

			$this->output_js( "$error<br><br>", 'vermell', true );
			$this->set_message_error( $error );


		} catch ( \Exception $e ) {

			// error i no s'ha enviat el mail
			$this->output_js( " - Error al enviar mail a : <strong>$custumer_name</strong> - $billing_mail<br><br>", 'vermell' );

			$error = $e->getMessage();

			$this->output_js("$error<br><br>", 'vermell' );
			$this->set_message_error( $error );
		}

	}

	private function send_mail_add_files( &$mail, $invoice_pdf ) {

		// Logo letnd
		if ( is_file( CLIENT_PATH . 'images/logo_newsletter.jpg' ) && ( $_POST["format"] == "html" ) ) {
			$mail->AddEmbeddedImage( CLIENT_PATH . 'images/logo_newsletter.jpg', 'logo', 'logo.jpg' );
		} elseif ( is_file( CLIENT_PATH . 'images/logo_newsletter.gif' ) && ( $_POST["format"] == "html" ) ) {
			$mail->AddEmbeddedImage( CLIENT_PATH . 'images/logo_newsletter.gif', 'logo', 'logo.gif' );
		}

		// pdf
		if ( ! is_file( $invoice_pdf ) ) {
			$this->set_message_status( 'no_pdf' );
			$this->output_js( " - No s'ha pogut generar la factura: $invoice_pdf<br><br>", 'vermell' );

			return false;
		}
		$mail->AddAttachment( $invoice_pdf );

		return true;
	}

	private function emogrify( $output ) {

		$instyler = new \Pelago\Emogrifier( $output );
		$output   = $instyler->emogrify();

		return $output;

	}

	private function get_pdf( $invoice_id ) {

		$pdf = New InvoicePdf( $this->module );
		$pdf->get_body( $invoice_id );

		$invoice_pdf = $pdf->output( false, ( CLIENT_PATH . 'invoice/invoices/' ) );

		return $invoice_pdf;
	}

	private function delete_pdf( $invoice_pdf ) {
		unlink ( $invoice_pdf);
	}

	private function set_message_status( $status ) {

		$query = "UPDATE invoice__message SET status = '$status'
              WHERE message_id = " . $this->insert_id;
		Db::execute( $query );

	}

	private function set_message_error( $error ) {

		$error = Db::qstr( $error );

		$query = "UPDATE invoice__message SET error = $error
              WHERE message_id = " . $this->insert_id;
		Db::execute( $query );

	}

	private function start_send_mail() {
		print_javascript('top.$("#message_container").show();top.$("#message").html("");');
		$this->output_js ( "Enviament d'emails:<br><br>", 'resaltat');
	}


	public function output_js( $message, $class, $encode = false ) {

		$message = json_encode("<span class=\"$class\">$message</span>");


		echo( '<SCRIPT LANGUAGE="JavaScript" TYPE="text/javascript">
			parent.Invoice.write_report(' . $message . ');
			</SCRIPT>' );

	    // this is to make the buffer achieve the minimum size in order to flush data
	    echo str_repeat(' ',1024*64);
		ob_flush();
		flush();

	}


}