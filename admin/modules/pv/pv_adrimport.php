<?

/**
 * PvAdrimport
 *
 *
 * Utitlitza les taules:
 *          pv__adr
 *
 *
 * /admin?tool=pv&tool_section=adrimport&action=show_form_edit&menu_id=2602
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 7/2011
 * @version $Id$
 * @access public
 */
class PvAdrimport extends Module {
	var $querys = '', $cg, $import_fields, $import_exclude_fields, $import_include_fields, $import_fields_functions, $import_fields_sql, $import_exists_col, $is_import, $show_data, $show_querys, $group_id = 3;

	function __construct() {
		parent::__construct();

		// Camps que hi ha a l'excel ( important lletres excel en majúscula )
		$this->import_fields = array(
			'A' => 'codi_producte', // empresa
			'B' => 'nom_comercial', // empresa
			'C' => 'capacitat_envas', // mail
		);
		// Camps que no vulll que surtin a la consulta ( estan en una taula apart )
		$this->import_exclude_fields = array(
		);
		// Camps que no estan a l'excel, han d'anar amb el mateix ordre en que es criden les funcions
		$this->import_include_fields = array(
			'unitats_quantitat' => 'unitats_quantitat'
		);

		// Funcions que es criden per camp
		$this->import_fields_functions = array(
			'capacitat_envas' => 'get_capacitat_envas',
		);

		$this->import_fields = array_merge( $this->import_fields, $this->import_include_fields );

		$fields = '';
		foreach ( $this->import_fields as $key => $val ) {
			// poso lletra corresponent als exclosos
			if ( isset( $this->import_exclude_fields[ $val ] ) ) {
				$this->import_exclude_fields[ $val ] = $key;
			}
			else {
				$fields .= $val . ',';
			}
		}
		$this->import_fields_sql = substr( $fields, 0, - 1 );

		foreach ( $this->import_fields as $key => $val ) {
			if ( $val == 'codi_producte' ) {
				$this->import_exists_col = $key;
				break;
			}
		}

		Debug::p( $this->import_fields, 'import_fields' );
		//Debug::p( $this->import_exclude_fields, 'import_exclude_fields' );

	}

	function show_form() {
		$GLOBALS['gl_content'] = $this->get_form();
	}

	function get_form() {
		$this->set_file( '/pv/import_form.tpl' );
		$this->set_vars( $this->caption );

		return $this->process();
	}

	function show_data_header() {

		$this->show_data = R::post( 'show_data' );
		if ( ! $this->show_data ) {
			return;
		}


		$ths = $tds = '';
		foreach ( $this->import_fields as $key => $val ) {
			$ths .= "<th>" . $val . "</th>";
			$tds .= "<td></td>";
		}


		set_inner_html( 'data_holder', '
				<div>
					<h6>Previsualització importació</h6>
					<table>
						<thead>
							<tr>
								<th></th>
								' . $ths . '
							</tr>
						</thead>
						<tbody id="data_table">
						<tr>
								' . $tds . '
						</tr>
						</tbody>
					</table></div>' );
	}

	function show_data( &$rs, $exists, $mail ) {

		static $conta = 1;

		if ( ! $this->show_data ) {
			return;
		}

		$tds = "<td>" . $conta . "</td>";
		$conta++;
		foreach ( $rs as $key => $val ) {
			$tds .= "<td>" . $val . "</td>";
		}

		$class_mail = $mail != $exists ? ' class="vermell"' : ' class="import-gris"';

		set_inner_html( 'data_table', '
						<tr' . $class_mail . '>							
							' . $tds . '
						</tr>', true );


		flush();
	}


	function show_querys_header() {

		$this->show_querys = R::post( 'show_querys' );
		if ( ! $this->show_querys ) {
			return;
		}


		set_inner_html( 'querys_holder', '
					<h6>Consultes</h6>
					<table>
						<tbody>
						<tr><td id="querys"></td></tr>
						</tbody>
					</table>
					' );
	}

	function show_query( $query ) {

		if ( ! $this->show_querys ) {
			return;
		}

		set_inner_html( 'querys', $query . '
					<br>
					', true );
	}


	function show_exists_header() {

		set_inner_html( 'mails_holder', '
					<h6>Duplicats</h6>
					<table>
						<tbody>
						<tr><td id="mails"></td></tr>
						</tbody>
					</table>
					' );
	}

	function show_exists( $mail ) {

		static $conta = 1;

		set_inner_html( 'mails', $conta . ' - ' . $mail . '
					<br>
					', true );

		$conta++;
	}

	function write_record() {

		Debug::p( $_POST, 'post' );
		Debug::p( $_FILES, 'files' );

		if ( $_FILES ) {

			// recullo arxiu
			foreach ( $_FILES as $key => $upload_file ) {
				if ( $upload_file['error'] == 1 ) {
					global $gl_errors, $gl_reload;
					if ( ! $gl_errors['post_file_max_size'] ) {
						$gl_message                      = ' ' . $this->messages['post_file_max_size'] . (int) ini_get( 'upload_max_filesize' ) . ' Mb';
						$gl_errors['post_file_max_size'] = true;
						$gl_reload                       = true;
					}
					$this->end();
				}
			}

			// objecte excel
			include( DOCUMENT_ROOT . 'common/includes/phpoffice/PHPExcel.php' );
			$objPHPExcel = PHPExcel_IOFactory::load( $upload_file['tmp_name'] );
			$results     = $objPHPExcel->getActiveSheet()->toArray( null, true, true, true );

			// mode import o preview
			$this->is_import = isset( $_POST['import'] );
			// config de newsletter
			$this->cg = $this->get_config( 'admin', 'newsletter__configadmin' );

			/////////////////
			// IMPORT
			/////////////////

			// control de flush
			@apache_setenv( 'no-gzip', 1 );
			@ini_set( 'zlib.output_compression', 0 );
			@ini_set( 'implicit_flush', 1 );

			$this->show_data_header();
			$this->show_querys_header();
			$this->show_exists_header();

			$conta = 1;

			foreach ( $results as $key => $rs ) {

				$exist_col = $rs[ $this->import_exists_col ];
				if ( $key != 1 ) {

					$exists = $this->check_exists( $exist_col );

					$this->add_database_record( $rs, $exists );

					$this->show_data( $rs, $exists, $exist_col );
					$conta ++;
				}
			}

			if ( $this->is_import ) {
				print_javascript( 'top.$("#confirm").hide();' );
				print_javascript( 'top.$("#done").show();' );
			} else {
				print_javascript( 'top.$("#confirm").show();' );
				print_javascript( 'top.$("#done").hide();' );
			}

		}
		$this->end();

	}

	function add_database_record( &$rs, $exists ) {

		if ( $exists ) {
			$insert_id = '';
		} else {

			// Abans de res miro que no sigui una fila buida
			$has_value = false;
			foreach ( $rs as $key => $val ) {
				if ( isset($this->import_fields[$key]) ) {
					if (trim($val) != '') {
						$has_value = true;
						break;
					}
				}
			}
			if (!$has_value) {
				$this->show_query( 'NO VALUE' );
				return;
			}

			// Primer processo els resultats
			foreach ( $this->import_fields as $key => $val ) {
				if ( isset( $this->import_fields_functions[ $val ] ) ) {
					$rs[ $key ] = $this->{$this->import_fields_functions[ $val ]}( $rs, $rs[ $key ] );
				}
			}
			//Debug::p( $rs );
			//Debug::p( $this->import_fields );

			// Després els executo
			$values = '';
			foreach ( $rs as $key => $val ) {
				if (    isset($this->import_fields[$key]) && ! in_array( $key,  $this->import_exclude_fields ) ) {
					$values .= Db::qstr( trim ($val) ) . ',';
				}
			}

			$values = substr( $values, 0, - 1 );

			$query = "INSERT INTO " . $this->table . " ( " . $this->import_fields_sql . ") 
						VALUES (" . $values . ");";
			if ( $this->is_import ) {
				Db::execute( $query );
			}
			$this->show_query( $query );

			$insert_id = Db::insert_id();
		}

	}


	/*
	 * Funcions per registre
	 *
	 */
	function get_capacitat_envas( &$rs, $val ) {

		$capacitat_envas = (double)$val;

		if ( strpos( strtolower($val), 'l' ) !== false ) {
			$unitats_quantitat = 'l';
		}
		else if ( strpos( strtolower($val), 'lt' ) !== false ) {
			$unitats_quantitat = 'l';
		}
		else if ( strpos( strtolower($val), 'kg' ) !== false ) {
			$unitats_quantitat = 'kg';
		}
		else if ( strpos( strtolower($val), 'ml' ) !== false ) {
			$unitats_quantitat = 'l';
			$capacitat_envas = $capacitat_envas / 1000;
		}
		else if ( strpos( strtolower($val), 'un' ) !== false ) {
			$unitats_quantitat = '';
		}
		else if ( strpos( strtolower($val), 'caja' ) !== false ) {
			$unitats_quantitat = '';
		}
		else if ( strpos( strtolower($val), 'kit' ) !== false ) {
			$unitats_quantitat = '';
		}
		else {
			$unitats_quantitat = 'NOT FOUND';
		}


		$rs['capacitat_envas'] = $capacitat_envas;
		return $surname1;
	}

	function get_group_id( &$rs, $val ) {

		// Forço a un grup, el client ja el té creat i tinc un excel només per un grup
		$this->group_id = 3;
		return;

		// comprovo si grup existeix ja a bbdd
		$val = trim( $val );

		if (!$val)
			$val = 'Importats sense classificar';

		$query = "SELECT group_id, group_name FROM newsletter__group_language WHERE group_name='" . $val . "'";

		$group_rs = Db::get_row( $query );

		// si el grup existeix no el creo
		if ( $group_rs ) {
			$this->group_id = $group_rs['group_id'];
		} // si no existeix el grup el creo si en mode import
		else {
			$this->group_id = $this->add_group( $val );
		}
	}

	function add_group( $group ) {

		$group = Db::qstr( $group );

		$query = "INSERT INTO newsletter__group () VALUES ()";
		if ( $this->is_import ) Db::execute( $query );
		$this->show_query( $query );

		$group_id = Db::insert_id();

		global $gl_languages;
		foreach ( $gl_languages['public'] as $lang ) {
			$query = "INSERT INTO `newsletter__group_language` (`group_id`, `language`, `group_name`)
						VALUES (" . $group_id . ", '" . $lang . "', " . $group . ")";
			if ( $this->is_import ) Db::execute( $query );
			$this->show_query( $query );
		}

		return $group_id;
	}


	/*
	 *
	 * Altres funcions
	 *
	 *
	 */


	function check_exists( $exist_col_value ) {

		// sino hi ha mail deixo importar igualment
		if ( ! $exist_col_value ) {
			return false;
		}

		$query = "SELECT adr_id FROM " . $this->table . " WHERE mail = '" . $exist_col_value . "'";

		// Permeto que espugui repetir email, en el cas de attitude, però notifico quins
		if (Db::get_first( $query ) ) {
			$this->show_exists($exist_col_value);
			Debug::p( 'Mail repetit' );

		}
		return false;

		// return Db::get_first( $query ) ? true : false;
	}

	function end( $message = false ) {

		if ( $message ) {
			$GLOBALS['gl_page']->show_message( $message );
		}
		/*if ( $this->querys )
			Debug::add( 'Save rows querys', $this->querys, 2 );*/

		Debug::p_all();
		die();
	}

}

?>