<?

/**
 * Other
 *
 * Funcions comuns per portfoli tant a public com admin
 * TODO-i S'ha de fer per general,  news, inmo, product ....
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) Novembre 2015
 * @version $Id$
 * @access public
 */
class Other {

	public $caption;

	function __construct() {

	}

	static public function set_fields( &$m ) {

		$table = $m->table;
		$cg    = &$m->config;
		$i     = 1;

		// defineixo els tipus de other que hi han
		while ( isset( $cg[ 'others' . $i ] ) ) {

			$id   = 'others' . $i;
			$type = $cg[ $id . '_type' ];
			$order = $cg[ $id . '_order' ];

			if ( $type != 'null' ) {
				self::set_field( $m, $cg, $table, $id, $type, $order );
			}

			$i ++;
		}
	}

	/**
	 * @param $m
	 * @param $cg
	 * @param $table
	 * @param $id
	 * @param $type
	 * @param $order
	 */
	private static function set_field( &$m, &$cg, $table, $id, $type, $order ) {

		$m->add_field( $id, array(
			'form_admin'        => 'input',
			'form_public'       => 'text',
			'list_public'       => 'text',
			'change_field_name' => '' . $table . '.' . $id . ' AS ' . $id,
			'type'              => $type
		) );

		if ($order) {
			$m->set_field_position( $id, $order );
		}

		$m->caption[ 'c_' . $id ] = $cg[ $id ];

		if ( $type == 'text' ) {
			$m->language_fields[] = $id;
			$m->set_field( $id, 'change_field_name', $table . '_language.' . $id . ' AS ' . $id );
			$m->set_field( $id, 'text_maxlength', '500' );

		} elseif ( $type == 'text_no_lang' ) {
			$m->set_field( $id, 'type', 'text' );
			$m->set_field( $id, 'text_maxlength', '500' );

		} elseif ( $type == 'textarea' ) {
			$m->language_fields[] = $id;
			$m->set_field( $id, 'change_field_name', $table . '_language.' . $id . ' AS ' . $id );
			$m->set_field( $id, 'textarea_rows', '4' );
			$m->set_field( $id, 'textarea_cols', '50' );
			$m->set_field( $id, 'text_maxlength', '500' );

		} elseif ( $type == 'checkbox' ) {
			$m->set_field( $id, 'select_fields', '0,1' );

		} elseif ( $type == 'select' ) {

			$m->set_field( $id, 'type', 'select' );

// TODO - Falta el select
//				$m->set_field( $id, 'select_fields', '1,2,3,4' );
//				$m->caption[ 'c_' . $id . '_1' ] = "";
		}
	}


}

?>