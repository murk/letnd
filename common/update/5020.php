<?

/* 5-6-13 
  Modificacions families productes, brand i sectors
  afegir titol pagina, descripcio i keywords

 */

if (is_table('product__family_language')) {	
	
	$update_sql .="
		ALTER TABLE `product__family_language`
			ADD COLUMN `page_title` VARCHAR(255) NOT NULL DEFAULT '' AFTER `family_description`,
			ADD COLUMN `page_description` VARCHAR(255) NOT NULL DEFAULT '' AFTER `page_title`,
			ADD COLUMN `page_keywords` VARCHAR(255) NOT NULL DEFAULT '' AFTER `page_description`;";	
	
}

if (is_table('product__brand_language')) {	
	
	$update_sql .="
		ALTER TABLE `product__brand_language`
			ADD COLUMN `page_title` VARCHAR(255) NOT NULL DEFAULT '' AFTER `brand_description`,
			ADD COLUMN `page_description` VARCHAR(255) NOT NULL DEFAULT '' AFTER `page_title`,
			ADD COLUMN `page_keywords` VARCHAR(255) NOT NULL DEFAULT '' AFTER `page_description`;";	
	
}

if (is_table('product__sector_language')) {	
	
	$update_sql .="
		ALTER TABLE `product__sector_language`
			ADD COLUMN `page_title` VARCHAR(255) NOT NULL DEFAULT '' AFTER `sector_description`,
			ADD COLUMN `page_description` VARCHAR(255) NOT NULL DEFAULT '' AFTER `page_title`,
			ADD COLUMN `page_keywords` VARCHAR(255) NOT NULL DEFAULT '' AFTER `page_description`;";	
	
}


?>