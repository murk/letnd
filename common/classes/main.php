<?php
/**
 * * Gestiona parts principals del programa, definir les primeres variables, carregar arxius, etc...
 *
 *
 *
 */

trait LambdasAsMethods
{
	/*
	Per poder utilitzar s'ha de posar "use LambdasAsMethods" a la clase

		TODO-i Aplicar al Module per així poder definir mètodes dinàmicament:

		$module->after_records_walk = function( $rs ){
			$images = $rs['images'];
			$image_src = $rs['image_src'];
		};

		El problema amb __call es que sembla que permetrà executar funcions privades, per tant per utilitzar-ho hauria de fer una funció "register" i només permetre les registrades
	*/
    public function __call($name, $args)
    {
        return call_user_func_array($this->$name, $args);
    }
}

class Main {
	/**
	 * Main::set_vars()
	 * Static function
	 * Defineixo si haig de fer debug o no
	 * @access public
	 * @return void
	 */
	static public function set_vars(){
		// Comença a contar
		ini_set('date.timezone','Europe/Madrid');//date_default_timezone_set('Europe/Madrid'); PHP5
		define('DOCUMENT_TIME_START', microtime());
		session_start();
		// Variables necesaries per carregar includes i a debug.php
		define("IS_HTTPS",  (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') || $_SERVER['SERVER_PORT'] == 443);

		$http = IS_HTTPS?'https://':'http://';
		define("HTTP",$http);
		define("HOST_URL",$_SERVER["HTTP_HOST"]?$http.$_SERVER["HTTP_HOST"]."/":$http . $_ENV["HTTP_HOST"]."/");
		define("SHORT_HOST_URL",substr(HOST_URL,(IS_HTTPS?8:7),-1));
		define("DOCUMENT_ROOT",$_SERVER["DOCUMENT_ROOT"]?$_SERVER["DOCUMENT_ROOT"]. "/":$_ENV["DOCUMENT_ROOT"]. "/");
		define( "VERSION", ( version_compare( PHP_VERSION, '7.0.0' ) >= 0 ) ? 7 : (( version_compare( PHP_VERSION, '5.0.0' ) >= 0 ) ? 5 : 4) );

		global $gl_is_letnd;
		$gl_is_letnd =
			@include ('config_db_letnd.php');
		if (!$gl_is_letnd)
			include (DOCUMENT_ROOT.'common/config_db.php');

		Main::load_class('debug');// carrego aquí per que es la classe que necessito primera
		Main::load_class('request');// carrego aquí per que es la classe que necessito primera

	}

	/**
	 * Main::load_files()
	 * Carrega tots els include i totes les clases necessaries
	 *
	 * @param bool $is_kill Quan es crida desde la funció kill quan s'instala
	 */
	static public function load_files( $is_kill = false ){
		global $gl_site_part, $gl_language, $gl_is_admin;
		// globals de arxiu idioma
		global $month_names,
			$week_names,
			$gl_caption,
			$gl_caption_image,
			$gl_caption_translation,
			$gl_messages,
			$gl_messages_image,
			$gl_caption_months;
		// includes
		require_once( DOCUMENT_ROOT . 'common/vendor/autoload.php' );
		include (DOCUMENT_ROOT .'common/html_functions.php');
		include (DOCUMENT_ROOT .'common/includes/json/JSON.php'); // TODO-i Treure la classe PHP7

		// clases
		Main::load_class( 'template' );
		Main::load_class( 'page_' . $gl_site_part );
		Main::load_class( 'module_base' );
		Main::load_class( 'module' );
		Main::load_class( 'db_base_list_show' );
		Main::load_class( 'db_list_records' );
		Main::load_class( 'db_new_row' );
		Main::load_class( 'db_save_rows' );
		Main::load_class( 'db_query' );
		Main::load_class( 'db_show_config' );
		Main::load_class( 'db_show_form' );
		Main::load_class( 'db_form_object' );
		Main::load_class( 'db_image_manager' );
		Main::load_class( 'db_split_results' );
		Main::load_class( 'thumb' );
		Main::load_class( 'upload_files' );
		Main::load_class( 'translation' );
		Main::load_class( 'blocks' );
		Main::load_class( 'user' );
		Main::load_class( 'call_function' );
		Main::load_class( 'table' );

		if ($is_kill) return;

		// carregar idioma, admin el carrego sempre, public és el que pot ser diferent i sobreescriure el de admin
		include (DOCUMENT_ROOT . 'admin/languages/' . $gl_language . '.php');
		if (!$gl_is_admin)
			include (DOCUMENT_ROOT . 'public/languages/' . $gl_language . '.php');

		Debug::p( [
					[ 'Arxiu admin client', CLIENT_PATH . 'functions_admin.php' ],
					[ 'Arxiu public client', CLIENT_PATH . 'functions.php' ],
				], 'general');
		
		// carrego arxiu de funcions del client tant a admin com public
		if ($gl_is_admin && file_exists(CLIENT_PATH . 'functions_admin.php'))
		{
			include_once (CLIENT_PATH . 'functions_admin.php');
		}
		if (!$gl_is_admin && file_exists(CLIENT_PATH . 'functions.php'))
		{
			include_once (CLIENT_PATH . 'functions.php');
		}
	}

	/**
	 * Main::load_class()
	 * Carrega la clase que li diem del directori de les classes
	 * Es pot utilitzar desde qualsevol lloc per classes que no fan falta sempre
	 * O carrega una clase d'un modul si posem els 2 parametres, ej.D:/webs/intranets/web/public/modules/product/product_cart.php
	 * Ej: Main::load_class('file');
	 *
	 * @param mixed $class
	 * @param string $tool_section
	 * @param bool $site_part
	 *
	 * @return bool|mixed|string
	 */
	static public function load_class($class, $tool_section = '', $site_part = false){
				
		if ($tool_section) {
			$site_part = $site_part?$site_part:$GLOBALS['gl_site_part'];
			$tool = $class;
			$class = ucfirst($tool) . snake_to_camel($tool_section, true);

			// Molt curiòs, fa l'if i l'else, tots dos, ja que al carregar l'include llavors ja es cert el class_exists
			if (!class_exists($class)) {
				include_once (DOCUMENT_ROOT . $site_part . '/modules/' . $tool . '/' . $tool . '_' . $tool_section . '.php');
			}
			return $class;

		}
		else {
			include_once (DOCUMENT_ROOT.'common/classes/'.$class.'.php');
			return true;
		}
	}
	/**
	 * Executa una acció un cop acabada una acció de guardar, borrar o afegir registre o registres
	 *
	 * Va al formulari següent, com ara al entrar nova propietat, que anem directament al formulari de insertar imatges
	 *
	 * @param string $action Acció a realitzar
	 * @access public
	 * @return void
	 */
	static public function do_next($action)
	{
		global $gl_next_form_id, $gl_db_classes_id_field, $gl_do_next_id_field, $gl_insert_id, $gl_action, $gl_menu_id, $gl_tool_section, $gl_message, $gl_page, $gl_saved, $gl_reload, $gl_do_next;
		
		if (!$gl_do_next) return; // si no vull que es fagi un do_next automatic
		
		if ($GLOBALS['gl_is_admin']) $gl_page->set_next_form($gl_menu_id); // poso aqui ja que no es fan els menus al guardar
		$id_field = $gl_do_next_id_field?$gl_do_next_id_field:$gl_db_classes_id_field;
		debug::add('gl_reload', $gl_reload);
		switch (true)
		{
			// case !$gl_saved:    poder si no es guarda haurem de sortir directament
			// break;
			// recarrega sempre quan definim aquesta variable a true, si no hi ha next_form_id que de fet ja passa a un formulari seguent carregant un altra pàgina, aixi que no te sentit carregar la mateixa
			case $gl_reload && !(strstr($gl_action, "add_record") && $gl_next_form_id &&  $gl_saved):
				$_SESSION['message'] = $gl_message;

				// Quan hi ha un # a location no funciona

				print_javascript('var uri = parent.window.location.href.split("#")[0];parent.window.location.replace(uri);');
				break;
				// a add record fa un reset del formulari per poder anar afegint registre
			case strstr($gl_action, "add_record") && !$gl_next_form_id && $gl_saved:
				print_javascript('
						if (parent.document.theForm) {
								parent.document.theForm.reset();
								parent.$(".checkboxes_long").empty();
								parent.sublist_reset_all();
							}
						if (typeof parent.form_set_serialized == "function")
							parent.form_set_serialized();');
				break;
				// redirigeix al següent formulari del menu quan estem creant un registre nou
			case strstr($gl_action, "add_record") && $gl_next_form_id && $gl_saved:
				$_SESSION['message'] = $gl_message;
				print_javascript('parent.window.location = "' . HOST_URL . 'admin/' . get_all_get_params(array('menu_id', 'action', $id_field),'?','&',true) . 'menu_id=' . $gl_next_form_id . '&' . $id_field .
					'=' . $gl_insert_id . '";');
				Debug::p_all();
				die();
				break;
				// recarrega al borrar d'un llistat
			case strstr($gl_action, "save_rows_bin_selected"):
			case strstr($gl_action, "save_rows_restore_selected"):
			case strstr($gl_action, "save_rows_delete_selected"):
			case strstr($gl_action, "save_rows_selected_to_value"):
				print_javascript('parent.window.location.replace(parent.window.location);');
				$_SESSION['message'] = $gl_message;
				debug::add('reload automatic', 'per que es bin o restore');
				Debug::p_all();
				die();
				break;
				// torna al llistat anterior quan borrem un registre
			case strstr($gl_action, "delete_record"):
			case strstr($gl_action, "bin_record"):
				$href = $_SESSION['last_list'][$GLOBALS['gl_menu_id']]['link'];
				$_SESSION['message'] = $gl_message;
				Debug::p_all();
				print_javascript('parent.location.replace(\'' . $href . '\');');
				die();
				break;
			case strstr($gl_action, "save_rows_get_images"):
				Debug::p_all();
				die();
				break;
		} // switch
	}
	
	// redirects, nomès a la part pública
	// QUALSEVOL CANVI AL .htacces AFECTA AQUÍ!!!!!!!!!
	static public function check_redirect(){
	
		global $gl_default_language, $gl_is_local, $gl_tool, $gl_tool_section, $gl_is_admin, $gl_is_home, $gl_languages, $gl_language, $gl_action;
		$uri = $_SERVER["REQUEST_URI"];
		//debug::p(HOST_URL);debug::p(substr($uri,1));
		
		// ----------------------------------------------------
		// 0 - redirigeixo a https
		// ----------------------------------------------------
		if (!$gl_is_local && HAS_SSL && !IS_HTTPS ) {

			$url = substr( HOST_URL, strpos( HOST_URL, '://' ) + 3, - 1 ); // trec http:// o https://, etc....


			// faig www. si no hi son per evitar un redirect
			//no redirigeix a www si hi ha mes de 1 punt ej. beta.letnd.com  , però si amb 1 punt ej. letnd.com   ------ no tinc en compte dominis tipus co.uk ----
			$www = '';
			if ( substr_count( $url, '.' ) == 1 && ( strpos( HOST_URL, '://www.' ) === false ) ) {
				$www = 'www.';
			}

			if ( $uri == '/' ) {
				$uri = '';
			}
			$url = 'https://' . $www . $url . $uri;


			// Excepcions pels mètodes de pagament
			if (
				// només redirigeixo de www. a https o sense www a https, però no els subdominis
				( strpos( HOST_URL, '://www.' ) === false ) ||
				($gl_tool_section == 'payment' && $gl_action == 'set_notice')
			) {
			} else {
				Main::redirect( $url );
			}

		}
		// ----------------------------------------------------
		// 1 - redirigeixo a www
		// ----------------------------------------------------
		if (!$gl_is_local && (strpos(HOST_URL, '://www.')===false)){
			$url= substr(HOST_URL, strpos(HOST_URL,'://')+3,-1); // trec http:// o https://, etc....
			// no redirigeix si hi ha mes de 1 punt ej. beta.letnd.com  , però si amb 1 punt ej. letnd.com   ------ no tinc en compte dominis tipus co.uk ----

			if (substr_count($url,'.')==1){

				$http = IS_HTTPS?'https':'http';


				$m = "$url ";
				if ($uri == '/')
					$uri = '';
				$url = $http . '://www.' . $url . $uri;

				$m .= " $url  uri: $uri";

				// send_mail_admintotal('Uri', $m);

				// if (HOST_URL!='http://mmp-capellades.net/')
				Main::redirect($url);
			}
		}
		if ($gl_is_admin) return; // a admin nomes redirigeixo www
		
		// ----------------------------------------------------
		// 2 - redirigeixo idioma per defecte a la home
		// ----------------------------------------------------
		if (isset($_GET['language'])) {
			if ( $gl_default_language == $_GET['language'] && $gl_is_home ) {
				Main::redirect( '/' );
			}

		}

		// ----------------------------------------------------
		// 2.b - redirigeixo idioma al domini corresponent
		// ----------------------------------------------------
		if ($gl_languages['urls'][ $gl_language ]) {

			if ( isset( $_GET['loginnewsletter'] )
			     && $_GET['loginnewsletter'] == 'letndmarc'
			     && $_GET['passwordnewsletter'] == '5356aopfqkiios5cnoqws90se'
			) {
				// Si soc a la newsletter no redirigeixo al domini
			} else {

				$language_url = $gl_languages['urls'][ $gl_language ];
				$actual_url   = substr( HOST_URL, 0, - 1 );

				if ( $language_url != $actual_url ) {
					$url = $language_url . $uri;
					if ( $gl_is_local ) {
						Debug::p( $url, 'Redirigir a' );
					} else {
						Main::redirect( $url );
					}
				}
			}

		}


		
		// ----------------------------------------------------
		// 3 - redirigeixo la primera pàgina de una paginació
		// ----------------------------------------------------
		// nomes puc fer per un llistat sense nom, sino es complicaria massa, ja que el nom de la variable ve de $this->page_name = 'page' . $list_name; --- de moment nomès s'ha fet servir en admin, però podria sorgir el cas de ferse a public
		// per saber el $list_name hauria de mirar en la clase del llistat i encara ni s'ha carregat
		// i si mires el page + algo seria molt impreciss, podria haver una variable que es diguès "pagesos"
		// una solucio seria, utilitzar sempre pel nom del llistat: page+section
		if (isset($_GET['page']) && $_GET['page']=='1'){
			if (USE_FRIENDLY_URL){
				// faig exactament el mateix que a la clases db_split_results per construir el link, i a module->base_link_friendly
				$parameters_friendly = get_all_get_params(array('language','tool','tool_section','action','page','page_file_name'),'?','',true);
				$link = '/' . LANGUAGE . '/' . $gl_tool . '/' . $gl_tool_section . '/';
				if (isset($_GET['page_file_name'])) $link .= $_GET['page_file_name'] . '.html';
				$link .= $parameters_friendly;
				Main::redirect($link);
			}
			else{			
				$url = get_all_get_params(array('page'),'?','',true);
				Main::redirect($url);
			}
		}
		
		
		// ----------------------------------------------------
		// 4 - passar sistma vell a USE_FRIENDLY_URL
		// Fa redirect de tot lo del .htacces		
		// - excepte quan hi ha variables a darrera: /lang/page_file_name.html?kaca=pet
		// ------------------------------------------------
		// ho faig abans de "5-" per que si no ja no arriba a fer aquest if
		if ((USE_FRIENDLY_URL) && ($_SERVER['REQUEST_URI']=='/?'.$_SERVER["QUERY_STRING"])){
			//?page_id=16&tool=sistemamuseu&tool_section=museu&action=show_record&museu_id=29&language=cat
			$url = '';
			$length = count($_GET);

			// action = list_records es l'action per defecte, per tant no es posa a la url
			if (isset($_GET['action']) && $_GET['action']=='list_records') unset($_GET['action']);
			
			// --------------------------------------------------
			// 4.8 ---> # /lang/page_file_name.html
			// --------------------------------------------------
			if (!$url) $url = Main::check_friendly_url(array('language','page_id'),2,'4.8');
			// --------------------------------------------------
			// 4.7 ---> # /lang/action/page_file_name.html
			// --------------------------------------------------
			if (!$url) $url = Main::check_friendly_url(array('language','action','page_id'),3,'4.7');
			// --------------------------------------------------
			// 4.6 ---> # /lang/tool/section/page_file_name.html
			// --------------------------------------------------
			if (!$url) $url = Main::check_friendly_url(array('language','tool','tool_section','page_id'),4,'4.6');
			// --------------------------------------------------
			// 4.5 ---> # /lang/tool/section/action/page_file_name.html
			// --------------------------------------------------
			if (!$url) $url = Main::check_friendly_url(array('language','tool','tool_section','action','page_id'),5,'4.5');
			// --------------------------------------------------
			// 4.4 ---> # /lang/tool/section/id/page_file_name.html 
			// --------------------------------------------------	
			if (!$url) $url = Main::check_friendly_url(array('language','tool','tool_section','id','page_id'),6,'4.4');
			
			// --------------------------------------------------
			// 4.3 ---> # /lang/tool/section/param/param_val/page_file_name.html
			// --------------------------------------------------
			if (!$url) $url = Main::check_friendly_url(array('language','tool','tool_section','param','param_val','page_id'),5,'4.3');
			// --------------------------------------------------
			// 4.2 ---> # /lang/tool/section/action/param/param_val/page_file_name.html
			// els show_record son aquest però s'han de abreviar al 4.4
			// --------------------------------------------------
			if (!$url) $url = Main::check_friendly_url(array('language','tool','tool_section','action','param','param_val','page_id'),6,'4.2');
			// --------------------------------------------------
			// 4.1 ---> # /lang/tool/section/id/param/param_val/page_file_name.html
			// --------------------------------------------------
			if (!$url) $url = Main::check_friendly_url(array('language','tool','tool_section','id','param','param_val','page_id'),7,'4.1');
					
		}
		
		// Quan no es ni home ni page ( quan hi ha un punt pot ser page o un altre link que no necessita redirect ja que es un nom d'arxiu )
		if (USE_FRIENDLY_URL && $uri!='/' && strpos($uri,'.')===false){
		
		
		// ----------------------------------------------------
		// 5 - redirigeixo tots els directoris que no acaben amb / a directoris que acaben en /
		// ----------------------------------------------------
		// www.host.com/cat   -> www.host.com/cat/
		// www.host.com/  -> www.host.com  -----> aquest sembla que no cal, ja ho fan els navegadors automaticament , de totes formes l'uri de tots 2 = '/'	
		// comprovo '?' per si hi ha parametres al'uri: /cat/news/new/?page_id=1&language=cat
			if ((substr($uri,-1)!='/') && strpos($uri, '?')=== false){
				Main::redirect($uri . '/');
			}
			
		// ----------------------------------------------------
		// 6 - redirigir un  modul a una page si conté el mateix mòdul
		// --------------------------------------------------------
			// NO SEEEEEEEE -POT ser interessa tindre els 2 casos, DEPEN DE QUINA WEB
			//
			// /spa/portfoli/portfoli/ -> /spa/portfoli/portfoli/proyectos-web.html
			//
			// --- if (isset($_GET['page_file_name']))  ja no ho faig
			// a l'uri ha d'haver: /tool/tool_section i si ha all__page també hi es, s'ha de fer el redirect
			
				
		
		}
		
		// http://www.controlyvision.com/spa/sapli/new/9/marcadores-laser.html?cat=1&    a  http://www.controlyvision.com/spa/sapli/new/9/cat/1/marcadores-laser.html
	}
		
	// ----------------------------------------------------
	// 7 - redirigir una page a la page amb link ( el cas contrari a la 6, aquest si que sempre s'ha de fer )
	// --------------------------------------------------------
	//
	// Ho poso aquíu per que s'ha de comprobar desde el page_public.php
	//
	// /spa/proyectos-web.html -> /spa/portfoli/portfoli/proyectos-web.html
	//
	// nomès comprovo la primera pagina del page, les succesives no perque es compica molt
	// /spa/tecnomix/new/category_id/1/noticias.html la fitxa queda com: /spa/tecnomix/new/55/noticias.html?category_id=1
	static public function check_redirect_case_seven($page_id, $link, $page_file_name, $has_content){
	
		if (!USE_FRIENDLY_URL) return; // no em preocupa quan no hi ha friendly
	
		$page_file_name = Page::get_page_link($page_id, $link, $page_file_name, $has_content, true);
		$uri = $_SERVER["REQUEST_URI"];
		
		if (!$link || $link=='/') return;
		
		//debug::p('/' . LANGUAGE . '/' . $page_file_name,'file_name');
		
		if (strpos($link,':')===false && ($uri=='/' . LANGUAGE . '/' . $page_file_name)) {
			$new_uri =  '/' . LANGUAGE . $link . '/' . $page_file_name;
			debug::p($new_uri,'new_uri');
			if ($new_uri != $uri) // per si acas que no faci un infinite redirect
				Main::redirect($new_uri);
		}
	}
	static public function check_friendly_url($params,$length,$case, $param_val=false,$check_show_record=false){
		// haig de tindre exactament el mateix nombre de parametres

		if (count($_GET)!=$length) return false;		
		//if (!$_GET['tool'] || !$_GET['tool_section']) return;
		
		$get = $_GET;

		// 4.1, 4.4
		if ($case=='4.1' || $case=='4.4'){
			$id_field = $get['tool_section'] . '_id';
			if (isset($get[$id_field]) && isset($get['action']) && ($get['action']=='show_record')){
				if ($case=='4.4') $params = array('language','tool','tool_section',$id_field,'page_id');
				if ($case=='4.1') $params = array('language','tool','tool_section',$id_field,'param','param_val','page_id');
				unset($get['action']); // així no agafa l'action a param-param_val
			}
			else{
				return false;
			}
		}
		
		// 4.3, 4.2 , 4.1
		if ($case=='4.3' || $case=='4.2' || $case=='4.1'){
			// quan hi ha /param/param_val no sé el nom del parametre
			// haig de mirar quin de tots els parametres és el que no conec i ja de pass faig return si no isset
			$g = $get;
			foreach ($params as $p){
				if ($p!='param' && $p!='param_val' && !isset($get[$p])) return false;
				unset($g[$p]);
			}
			$get['param'] = key($g);
			$get['param_val'] = current($g);
		}
		
		$url = '';
		$section = '';
		foreach ($params as $p){		
			if ($p=='id') $p = $section;
			if (!isset($get[$p])) return false;
			if ($p=='section') $section =  $get[$p] . '_id';
			if ($p!='page_id') $url .= '/' . $get[$p];
		}
		
		$url .= '/' . get_page_file_name($get['page_id']);
		debug::p($url, 'Cas: ' . $case);
		Main::redirect($url);
	}
	static public function redirect($url){
		$url = str_replace('&amp;', '&', $url);
		if (DEBUG){
			Debug::p($_GET, "Get");
			Debug::p($url, "REDIRECT 301");
			Debug::p(Debug::get_error_backtrace(), "Backtrace", false, true, true);
			Debug::p_all();
			echo "REDIRECT 301: <br><a href='$url'>$url</a>";
		}
		else{
			Header( "HTTP/1.1 301 Moved Permanently" ); 
			Header( "Location: ".$url."" );
		}
		die();
	}
	static public function error_404($message='') {
		if ( DEBUG ) {
			Debug::p( $_GET, '_GET' );
			Debug::p( "Status: 410 Gone", $message );
			Debug::p( Debug::get_error_backtrace(), "Backtrace", false, true, true );
			Debug::p_all();
			echo "Status: 410 Gone. <br>$message";
		}
		else {
			//header("Status: 404 Not Found");
			Header( "HTTP/1.1 410 Gone" );
		}
		die();
	}

	/**
	 * Mostra els missatges guardats a la bbdd
	 */
	static public function show_missatges() {
		if ( ! $GLOBALS['gl_config']['missatge_on_start'] )
			return;

		if ( $GLOBALS['gl_tool'] == 'missatge' )
			return;


		/*
		 * comprovo si hi han missatges
		 * 1 - Que no estigui llegit
		 * 2 - Que no sigui del propi usuari
		 * 3 - Si no te cap grup relacionat i cap usuari relacionat es per tothom
		 * 4 - Si el missatge te grups relacionats ha de pertànyer al grup
		 * 5 - Si el missatge te usuaris relacionats ha d'estar en la llista
		 */
		$query   = "
			SELECT * 
				FROM missatge__missatge 
				WHERE 
					(SELECT count(*) FROM  missatge__missatge_read_to_user 
					WHERE missatge__missatge_read_to_user.missatge_id = missatge__missatge.missatge_id
					AND user_id = " . $_SESSION['user_id'] . ") = 0
				AND creator_user_id <> " . $_SESSION['user_id'] . "
				AND 
				(
					(
						(SELECT count(*) 
									FROM missatge__missatge_to_group 
									WHERE missatge__missatge_to_group.missatge_id = missatge__missatge.missatge_id) = 0
							AND
						
						(SELECT count(*) 
							FROM missatge__missatge_to_user 
							WHERE missatge__missatge_to_user.missatge_id = missatge__missatge.missatge_id) = 0
					)
					OR
					(   
						(SELECT count(*) 
							FROM missatge__missatge_to_group
							WHERE missatge__missatge_to_group.missatge_id = missatge__missatge.missatge_id
							AND group_id = '" .  $_SESSION['group_id'] . "') > 0
					)
					OR
					(   
						(SELECT count(*) 
							FROM missatge__missatge_to_user
							WHERE missatge__missatge_to_user.missatge_id = missatge__missatge.missatge_id
							AND user_id = '" .  $_SESSION['user_id'] . "') > 0
					)
				)				
				AND status = 'send'
				AND bin = 0
				ORDER BY entered DESC";
		$results = Db::get_rows( $query );

		if ( $results ) {

			$news = '';

			foreach ( $results as $rs ) {


				if ( $rs['mls_from_id'] ) {
					//Db::connect_mother();
				}
				//Db::reconnect();
				//$creator_group_id = Db::get_first( "SELECT group_id FROM user__user WHERE user_id = " . $rs['creator_user_id'] );

				// Reponc sempre si l'usuari no es 0, ja que llavors es que ho ha enviat letnd
				$reply = '';
				$entered = '- <i class="entered">' . format_datetime( $rs['entered'] ) . '</i>';
				//if ( $creator_group_id > 1 ) {
				if ( $rs['creator_user_id'] != 0 ) {
					$creator_name = Db::get_first( "SELECT CONCAT(name, ' ', surname) FROM user__user WHERE user_id = " . $rs['creator_user_id'] );
					$reply = '
						<span class="reply-user">' . $creator_name . '</span> ' .
				         $entered . '
						<a class="reply" href = "javascript:showPopWin(\'/admin/?menu_id=80002&template=window&reply_id=' . $rs['missatge_id'] . '\', 850, 520, null, true, false, \'\');">' . $GLOBALS['gl_caption']['c_reply'] . '</a>';
				}
				else {
					$reply = $entered;
				}

				$news .= '
					<div class="missatge-titol" id="missatge_titol_' . $rs['missatge_id'] . '">
						<strong>' . $rs['subject'] . '</strong> ' . $reply . ' <a class="hide_missatge" href="/admin/?call=hide_missatge&missatge_id=' . $rs['missatge_id'] . '" target="save_frame">Ocultar missatge</a>
					</div>
					<div id="missatge_missatge_' . $rs['missatge_id'] . '">
							';
				$news .= '<span>' . $rs['missatge'] . '</span>';

				if ( $rs['link'] ) {
					$link_text = $rs['link_text'] ? $rs['link_text'] : 'Link';
					$news .= '<div><a href = "' . $rs['link'] . '">' . $link_text . '</a></div>';
				}

				$news .= '';
				$news .= '</div>';
			}

			$GLOBALS['gl_news'] = $news;

		}

	}

	/**
	 * Afegeix un missatge a la BBDD
	 *
	 * @param array $parameters Array amb diferents paràmetres per nomes posar els que calguin
	 *
	 * @param string $parameters ['status'] = 'send' Status de l'element
	 * @param string $parameters ['kind'] = 'general' Tipus de l'element general|mls
	 * @param string $parameters ['subject'] = '' Assumpte
	 * @param string $parameters ['missatge'] = '' Missatge
	 * @param string $parameters ['link'] = '' Enllaç que es mostra
	 * @param string $parameters ['link_text'] = '' Text a l'Enllaç que es mostra
	 * @param integer $parameters ['creator_user_id'] = 0 L'usuari que l'ha creat
	 * @param integer $parameters ['mls_from_id'] = 0 La inmobiliaria que demana per colaborar
	 * @param integer $parameters ['mls_to_id'] = 0 La inmobiliaria a la que es dirigeix el missatge
	 * @param array $parameters ['to_groups'] = array() Array del grups als que va dirigit el missatge
	 * @param array $parameters ['to_users'] = array() Array del usuaris als que va dirigit el missatge
	 *
	 *
	 *
	*/
	static public function add_missatge( $parameters = array() ) {

		$default = array(
			'status'          => 'send',
			'kind'            => 'general',
			'subject'         => '',
			'missatge'        => '',
			'link'            => '',
			'link_text'       => '',
			'creator_user_id' => 0,
			'mls_from_id'     => 0,
			'mls_to_id'       => 0,
			'to_groups'       => array(),
			'to_users'        => array(),
		);

		$parameters = array_merge( $default, $parameters );
		extract( $parameters );

		$query = "
				INSERT INTO 
					missatge__missatge (
						status, 
						kind, 
						subject, 
						missatge,
						link,
						link_text,
						creator_user_id,
						mls_from_id,
						mls_to_id,
						modified,
						sent
						) 
				VALUES 
					(
					" . Db::qstr( $status ) . ",
					" . Db::qstr( $kind ) . ",
					" . Db::qstr( $subject ) . ",
					" . Db::qstr( $missatge ) . ",
					" . Db::qstr( $link ) . ",
					" . Db::qstr( $link_text ) . ",
					'" . $creator_user_id . "',
					'" . $mls_from_id . "',
					'" . $mls_to_id . "',
					now(),
					now()
					)";

		Db::execute( $query );
		$missatge_id = Db::insert_id();

		if ( $to_groups ) {

			$values = '(' . $missatge_id . ','  . implode( '), (' . $missatge_id . ',', $to_groups ) . ')';

			$query = "
				INSERT INTO missatge__missatge_to_group
					(missatge_id, group_id)
					VALUES " . $values;
			Db::execute( $query );
		}

		if ( $to_users ) {

			$values = '(' . $missatge_id . ','  . implode( '), (' . $missatge_id . ',', $to_users ) . ')';

			$query = "
				INSERT INTO missatge__missatge_to_user
					(missatge_id, user_id)
					VALUES " . $values;
			Db::execute( $query );
		}

	}

	/**
	 * @param $subject
	 * @param string $body
	 * @param string $link
	 * @param string $link_caption
	 *
	 * Afegeix un missatge dynamic per mostrar a la zona de "news", no són missatges que estiguin guardats a la BBDD
	 */
	static public function add_dynamic_missatge ( $subject, $body = '', $link = '', $link_caption = 'Link' ) {

		$new = '';

		if ($subject) {
			$new .= '<div class="missatge-titol"><strong>' . $subject . '</strong></div>';
		}

		if ($body || $link)
			$new .= '<div>';

		if ( $body ) {
			$new .= '<span>' . $body . '</span>';
		}

		if ( $link ) {
			$new .= ' - <a href = "' . $link . '">' . $link_caption . '</a>';
		}

		if ($body || $link)
			$new .= '</div>';


		$GLOBALS['gl_news'] .= $new;

	}

	/**
	 * Per saber si el client es del paquet inmoletnd
	 *
	 * @return bool
	 */
	static public function is_inmoletnd () {
		return  (is_table('inmo__property') AND is_table ('custumer__custumer') AND is_table('booking__book'));
	}

	/**
	 * Per saber si el client es del paquet tiendaletnd
	 *
	 * @return bool
	 */
	static public function is_tiendaletnd () {
		return  (is_table('inmo__property') AND is_table ('custumer__custumer') AND is_table('booking__book'));
	}
}