<?
$gl_caption_guestbook['c_new_guest_subject'] = 'Nova entrada en el llibre de visites';
$gl_caption_guestbook['c_new_guest_body'] = '
	<strong>Nom</strong>:<br /> %s <br /><br />
	<strong>Comentari</strong>:<br /> %s <br /><br />
	<strong>Data</strong>:<br /> %s <br /><br />
	<strong>Idioma</strong>:<br /> %s <br />
';
$gl_caption_guestbook['c_guestbook_required'] = 'Nom i comentari són obligatoris';
?>