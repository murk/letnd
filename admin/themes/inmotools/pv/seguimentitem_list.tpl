<!-- IGUAL LIST.TPL excepte 'is_pdf' i print -->
<? if ( ! $results ): ?>
	<? if ( ! $is_pdf ): ?>
		<? if ( $options_bar ): ?>
			<div id='opcions_holder'>
				<table width="100%" border="0" cellspacing="0" cellpadding="0" id="opcions">
					<tr>
						<td class="opcions">
							<div id="opcions_fixed_bar">
								<div class="more"></div>
							</div>
							<table class="filters">
								<tr valign="middle">
									<form>
										<td><?= $trimestre_filter ?></td>
									</form>
									<? foreach ( $options_filters as $options_filter ): ?>
										<form>
										<td><?= $options_filter ?></td>
										</form>
									<? endforeach //$options_filters?>
								</tr>
							</table><? if ( $options_filters ): ?>
								<div class="sep"></div><? endif //options_filters?>
							<table border="0" cellpadding="2" cellspacing="0">
								<tr valign="middle">
									<? if ( $options_buttons ): ?>
										<td class="buttons">
										<? foreach ( $options_buttons as $options_button ): ?><?= $options_button ?><? endforeach //$options_button?>
										</td><? endif //options_buttons?>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>
		<? endif //$options_bar?>
	<? endif //$is_pdf?>
<? endif //!$results?>
<? if ( $results ): ?>
	<? if ( ! $is_pdf ): ?>
		<? if ( $split_count ): ?>
			<div id='split_holder'>
				<table width="100%" border="0" cellpadding="0" cellspacing="0" id="split">
					<tr valign="middle">
						<td width="20%" align="left" nowrap class="pageResults"><?= $split_results_count ?></td>
						<td align="right" nowrap class="pageResults"><?= $split_results ?></td>
					</tr>
				</table>
			</div>
		<? endif //$split_count?>
		<? if ( $options_bar ): ?>
			<div id='opcions_holder'>
				<table width="100%" border="0" cellspacing="0" cellpadding="0" id="opcions">
					<tr>
						<td class="opcionsTitol"><strong><?= $c_options_bar ?></strong></td>
					</tr>
					<tr>
						<td class="opcions">
							<div id="opcions_fixed_bar">
								<div class="more"></div>
							</div>
							<table class="filters">
								<tr valign="middle">
									<? foreach ( $options_move_tos as $options_move_to ): ?>
										<td><?= $options_move_to ?></td>
									<? endforeach //$options_move_tos?>
									<form>
										<td><?= $trimestre_filter ?></td>
									</form>
									<? foreach ( $options_filters as $options_filter ): ?>
										<form>
										<td><?= $options_filter ?></td>
										</form>
									<? endforeach //$options_filters?>
									<? if ( $has_add_dots ): ?>
										<form>
											<td>
												<label>
													<?= $c_add_dots ?>
													<input <?= $add_dots_checked ?> type="checkbox" onchange="javascript:save_frame.window.location='<?= $add_dots_link ?>'" class="filter" name="add_dots"/>
												</label>
											</td>
										</form>
									<? endif //has_add_dots?>
								</tr>
							</table><? if ( $options_filters ): ?>
								<div class="sep"></div><? endif //options_filters?>
							<table border="0" cellpadding="2" cellspacing="0">
								<tr valign="middle">
									<? if ( $options_buttons ): ?>
										<td class="buttons">
										<? foreach ( $options_buttons as $options_button ): ?><?= $options_button ?><? endforeach //$options_button?>
										</td><? endif //options_buttons?>
									<? if ( $save_button ): ?>
										<td>
											<strong><a class="botoOpcionsGuardar" href="javascript:submit_form_save('save_rows_save_records<?= $action_add ?>')">
													<?= $c_send_edit ?>
												</a></strong></td>
									<? endif //save_button?><? if ( $select_button ): ?>
										<td><?= $c_select ?>
											&nbsp;<strong><a class="opcions" href="javascript:select_all(true, document.list)">
													<?= $c_all ?>
												</a>&nbsp;-&nbsp;<a class="opcions" href="javascript:select_all(false, document.list)">
													<?= $c_nothing ?>
												</a></strong></td>
									<? endif //select_button?><? if ( $delete_button ): ?>
										<? if ( $has_bin ): ?>
											<? if ( $is_bin ): ?>
												<td>
													<strong><a class="botoOpcionsBorrar" href="javascript:submit_list('save_rows_delete_selected<?= $action_add ?>', '<?= $c_confirm_deleted ?>', true)">
															<?= $c_delete_selected ?>
														</a></strong></td>
												<td>
													<strong><a class="botoOpcionsGuardar" href="javascript:submit_list('save_rows_restore_selected<?= $action_add ?>')">
															<?= $c_restore_selected ?>
														</a></strong></td>
											<? else: ?>
												<td>
													<strong><a class="botoOpcionsBorrar" href="javascript:submit_list('save_rows_bin_selected<?= $action_add ?>')">
															<?= $c_delete_selected ?>
														</a></strong></td>
											<? endif // is_bin?>
										<? else: ?>
											<td>
												<strong><a class="botoOpcionsBorrar" href="javascript:submit_list('save_rows_delete_selected<?= $action_add ?>', '<?= $c_confirm_deleted ?>', true)">
														<?= $c_delete_selected ?>
													</a></strong></td>
											<? /*<td>
							<td>&nbsp;</td><strong><a class="botoOpcions" href="/admin?menu_id=<?=$menu_id?>&sort_by=0&sort_order=<?=$sort_order0?>">
								<?=$c_move_to?>
							
						<input type="submit" name="bin_selected" value="<?=$c_delete_selected?>" class="boto1">
							</a></strong></td>*/ ?>
										<? endif // has_bin?><? endif //delete_button?>
									<? if ( $print_button ): ?>
										<td>
											<strong><a class="botoOpcions" href="javascript:open_print_window_seguimentitem('?menu_id=<?= $menu_id ?>&action=list_records&tool_section=seguimentitem&seguiment_id=<?= $seguiment_id ?>&template=print');">
													<?= $c_print_all ?>
												</a></strong></td>
									<? endif //print_button?>
									<? if ( $has_swap_edit ): ?>
										<td>
											<strong><a class="<?= $swap_class ?>" target="save_frame" href="<?= $swap_edit_link ?>">
													<?= $swap_edit_button ?>
												</a></strong></td>
									<? endif //has_swap_edit?>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>
			<div class="sep"></div>
		<? endif //$options_bar?>
		<form method="post" action="<?= $form_link ?>" name="list" onsubmit="return check_selected(this)" target="save_frame">

		<!-- FI IGUAL LIST.TPL -->
	<? endif //is_pdf?>
	<input name="action" type="hidden" value="save_rows"><?= $out_loop_hidden ?>


	<link rel="stylesheet" type="text/css" href="themes/inmotools/pv/styles.css" media="all"/>
	<div id="pv" class="<?= $pdf ?> <?= $is_editable?'editable':'no-editable' ?>">
		<table>
			<thead>
			<tr>
				<td colspan="<?= $colspan ?>">
					<table class="pv-head">
						<tr>
							<td class="pv-head-logo"><img src="themes/inmotools/pv/logo_<?= PvCommon::get_consultor() ?>.png" width="<?= SEGUIMENT_LOGO_WIDTH ?>"/></td>
							<td class="pv-head-titol" style="background-color:<?= CORPORATE_COLOR ?>;">
								<? if ( $is_trimestre ): ?>
									<strong>
										<?= $c_titol_trimestre ?>
									</strong>
									<span><?= $trimestre_seleccionat ?></span>
								<? else: //is_trimestral?>
									<strong><?= $c_titol_index ?></strong>
								<? endif //is_trimestral?>
								<div>Repertori d’evolucions de <?= $client ?></div>
							</td>
							<td class="pv-head-logo last"><? if ( $image_name_details ): ?><img <?= get_image_attrs( "/clients/" . PvCommon::get_consultor() . "/pv/client/images/" . $image_name_details, '', '120', '53', false ) ?> /><? endif //image?></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr class="pv-cols-head-sep">
				<td colspan="<?= $colspan ?>"></td>
			</tr>
			<tr class="pv-cols-head">
				<td class="first"><?= $c_codi_lloc ?></td>
				<td><?= $c_titol_norma ?></td>
				<td class="pv-small"><?= $c_classificacio ?></td>
				<td><?= $c_requisit ?></td>
				<? if ( $show_actiu ): ?>
					<td class="pv-small"><?= $c_evaluacio ?></td>
				<? endif //show_actiu?>
				<td class="pv-small"><?= $c_comentari ?></td>
				<td class="pv-small"><?= $c_evidencia ?></td>
				<td class="pv-small <?= $is_pdf ? 'last' : '' ?>"><?= $c_arxiu ?></td>
				<? if ( $is_editable ): ?>
					<td class="pv-small"><?= $c_is_actiu ?></td>
				<? endif //is_editable?>
				<? if ( ! $is_pdf ): // el boto de fitxa?>
					<td class="pv-small last"></td>
				<? endif //is_pdf?>
			</tr>
			<tr class="pv-cols-head-sep2">
				<td colspan="<?= $colspan ?>"></td>
			</tr>
			</thead>
			<? if ( $is_pdf ): ?>
			<tfoot>
			<tr class="pv-foot">
				<td colspan="<?= $colspan ?>">
					<?if(PvCommon::get_consultor() == 'arumsa'):?>
						<div><span>Avinguda Catalunya 96, entresòl 2. Tàrrega</span>
						<span>Tel: 973 28 33 15 - info@arumsa.com</span>
						<span>http://www.arumsa.com</span></div>
					<?elseif(PvCommon::get_consultor() == 'gpmconsultors') : //?>
						<div><span>C/ Ull de llebre, 13, 1-B, P.I. Clot de Moja 08734 Olèrdola</span>
						<span>Tel: 93.817.45.24</span>
						<span>http://www.gpmconsultors.com</span></div>
					<?else: //?>
						<div><span>C/ Llibertat, 159 2-C  17820  Banyoles</span>
						<span>Tel: 972 58 33 38  Fax: 972 58 33 39</span>
						<span>http://www.consultoriapv.com</span></div>
					<?endif // arumsa?>
				</td>
			</tr>
			</tfoot>
			<tbody>
			<? endif //is_pdf?>


			<? /*
		//
		// CONTINGUT 
		//		
		*/ ?>


			<? foreach ( $loop as $l ): extract( $l ) ?>
				<? if ( $group_fields ): ?>
					<tr class="pv-categoria">
						<td colspan="<?= $colspan ?>"><?= $categoria ?></td>
					</tr>
				<? endif //group_fields?>
				<tr class="pv-tr-norma<?= $norma_last ?>">
					<? if ( $codi_lloc ): ?>
						<td class="pv-codi" rowspan="<?= $norma_rowspan ?>">
							<div><?= $codi_lloc ?></div>
							<span class="pv-entered"><?= $entered_top ?></span>

							<? /* quan nomes hi ha una evolucio ( ej. llistat per trimestre */ ?>
							<? if ( $evolucio ): ?>
								<p class="pv-evolucio" rowspan="<?= $norma_rowspan ?>">
									<strong>Evolució <?= $evolucio ?></strong>
									<br/>
									<?= $entered ?>
								</p>
							<? endif //evolucio?>

						</td>
					<? else: //codilloc?>
						<? if ( $titol_norma ): ?>
							<td class="pv-evolucio" rowspan="<?= $norma_rowspan ?>">
								<strong>Evolució <?= $evolucio ?></strong>
								<br/>
								<?= $entered ?>
							</td>
						<? endif //titol_norma?>
					<? endif //codi_lloc?>

					<? if ( $titol_norma ): ?>
						<td rowspan="<?= $norma_rowspan ?>" class="pv-norma"><?= $titol_norma ?></td>
					<? endif //titol_norma?>

					<td class="pv-classificacio">
						<?= $classificacio ?>
						<input name="seguimentitem_id[<?= $seguimentitem_id ?>]" type="hidden" value="<?= $seguimentitem_id ?>"/>
					</td>
					<td class="pv-requisit">

						<table class="table-requisit">
							<tr>
								<th><?= $num_requisit ?></th>
								<td><?= $requisit ?></td>
							</tr>
						</table>

					</td>
					<? if ( $show_actiu ): ?>
						<td class="pv-evaluacio">

							<? /* EVALUACIO */ ?>
							<div class="pv-evaluacio <?= $evaluacio_class ?>">
								<?= $evaluacio ?>
								<div id="texte1_evaluacio_<?= $id ?>" class="<?= $evaluacio_text_class ?>">

									<p class="pv-obertura-codi">
										<?= $no_conforme_codi_lloc ?>
									</p>
									<?if(!$is_editable):?>
										<p class="pv-obertura clearfix">
										<strong><?=$c_obertura?>:</strong>
										<?=$obertura?>
										</p>
									<?endif //is_editable?>
								</div>
								<?if($is_editable):?>
									<p class="pv-obertura margin clearfix">
									<strong><?=$c_obertura?>:</strong>
									<?=$obertura?>
									</p>
								<?endif //is_editable?>
								<div id="texte2_evaluacio_<?=$id?>" class="<?=$evaluacio_text_class?>">
									<p class="pv-tancament clearfix">
										<strong><?= $c_tancament ?>:</strong>
										<?= $tancament ?>
									</p>
									<? if ( $is_editable ): ?>

										<p class="pv-tancat">
											<?= $c_tancat ?> <?= $tancat ?>
										</p>
									<? endif //is_editable?>

									<p class="pv-tancament"><?= $evaluacio_texte ?></p>
								</div>
							</div>

							<? /* AVISOS */ ?>

							<? if ( $avis || $is_editable ): ?>
							<div class="pv-avis <?= $avis_class ?>">
								<? if ( $avis ): ?>
									<p class="clearfix">

										<strong><?= $c_avis ?></strong>
										<? if ( $is_editable ): ?>
											<?= $avis ?>
										<? endif // $is_editable ?>

										<? if ( $avis_data ): ?>
											<span id="input_avis_<?= $id ?>" class="<?= $avis_text_class ?>"><?= $avis_data ?></span>
										<? endif // $avis_data ?>
									</p>
								<? endif //$avis?>

								<div id="texte_avis_<?= $id ?>" class="<?= $avis_text_class ?>">
									<p><?= $avis_texte ?></p>
									<? if ( $is_editable ): ?>
										<p><?= $c_avis_en_tramit ?>: <?= $avis_en_tramit ?></p>
									<? endif // $avis_en_tramit ?>
								</div>
							</div>
							<? endif //$avis?>

						</td>
					<? endif //show_actiu?>

					<td>

						<?= $comentari ?>

					</td>
					<td>

						<div id="evidencia_<?= $seguimentitem_id ?>">
							<? foreach ( $evidencias as $f ): extract( $f ) ?>
								<p><a href="<?= $evidencia_src ?>" title=""><?= $evidencia_name_original ?></a></p>
							<? endforeach //$evidencias?>
						</div>

						<? if ( $is_editable ): ?>
							<a class="boto1 pv-boto-evidencies" href="javascript:pv_open_window('/admin/?action=show_form_edit&field_to_edit=evidencia&process=edit_evidencia_from_list&template=window&tool=pv&tool_section=seguimentitem&seguimentitem_id=<?= $seguimentitem_id ?>')"> <?= $c_edit_evidencies ?> </a>
						<? endif //$is_editable ?>

					</td>
					<? if ( $titol_norma ): ?>
						<td rowspan="<?= $norma_rowspan ?>">

							<? foreach ( $files as $f ): extract( $f ) ?>
								<p><a href="<?= $file_src ?>" title=""><?= $file_name_original ?></a></p>
							<? endforeach //$files?>

						</td>
					<? endif //titol_norma?>
					<? if ( $is_editable ): ?>
						<td class="pv-small">

							<?= $is_actiu ?>

						</td>
					<? endif //is_editable?>
					<? if ( ! $is_pdf ): ?>
						<? if ( $codi_lloc ): ?>
							<td rowspan="<?= $norma_rowspan ?>">
								<a class="boto1" href="<?= $fitxa_link ?><?= $norma_id ?>"><?= $c_fitxa ?></a>
							</td>
						<? endif //codi_lloc?>
					<? endif //is_pdf?>
				</tr>
			<? endforeach //loop?>
			</tbody>
		</table>
	</div>


	<? if ( ! $is_pdf ): ?>

		<!-- IGUAL LIST.TPL -->

		</form>
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td height="10"></td>
			</tr>
		</table> <? if ( $split_count ): ?>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr valign="middle">
				<td width="20%" align="left" nowrap class="pageResults"><?= $split_results_count ?></td>
				<td align="right" nowrap class="pageResults"><?= $split_results ?></td>
			</tr>
			</table><? endif //$split_count?>
	<? endif //is_pdf?>
<? endif //results?>
<!-- FI IGUAL LIST.TPL -->