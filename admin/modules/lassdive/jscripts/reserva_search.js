var ReservaSearch = {

	view_format: '',
	$view_format: {},

	init: function () {
		var _this = this;
		$(document).ready(
			function () {

				_this.$view_format = $('#view-format-holder');

				_this.init_dates();
				_this.init_view_format();
				_this.init_auto_completes();
				_this.init_reset();
			}
		)
	},
	init_view_format: function () {
		this.$view_format.find('[data-value=' + this.view_format + ']').addClass('active');
	},
	set_view_format: function (val) {
		this.$view_format.find('a').removeClass('active');
		this.$view_format.find('[data-value=' + val + ']').addClass('active');
		$('#view-format-hidden').val(val);
	},
	init_dates: function () {

		$('#date_from').datepicker({
			changeMonth: true,
			changeYear: true,
			yearRange: '+0:+5',
			onClose: function (selectedDate) {
				var date = $(this).datepicker('getDate');
				if (date) {
					date.setDate(date.getDate());
				}
				$('#date_to').datepicker('option', 'minDate', date);
			}
		});
		$('#date_to').datepicker({
			changeMonth: true,
			changeYear: true,
			yearRange: '+0:+5',
			onClose: function (selectedDate) {
				var date = $(this).datepicker('getDate');
				if (date) {
					date.setDate(date.getDate());
				}
				$('#date_from').datepicker('option', 'maxDate', date);
			}
		});

	},
	init_auto_completes: function () {
		// lassdive
		add_select_long('lassdive', 'reserva', '2303', '', 'user_id', false, true, "get_search_results_users");

		add_select_long('lassdive', 'reserva', '2303', '', 'product_id', false, true, "get_search_results_products");

		add_select_long('lassdive', 'reserva', '2303', '', 'centre_id', false, true, "get_search_results_centres");
	},
	init_reset: function () {
		var _this = this;
		$('#reset-search').click(function () {

			_this.set_view_format('all');

			var $search = $('#search-advanced');

			$search.find(
				"input[name='q']," +
				"input[name='product_id_add']," +
				"input[name='product_id']," +
				"input[name='user_id_add']," +
				"input[name='user_id']," +
				"input[name='centre_id_add']," +
				"input[name='centre_id']," +
				"input[name='date_from']," +
				"input[name='date_to']"
			).val('');

			var select = "select[name='order_status']";
			$search.find(select).prop("selectedIndex", 0);

			var radio = "" +
				"input:radio[name='date_search']:eq(1)," +
				"input:radio[name='is_done']:first," +
				"input:radio[name='source']:first";
			$search.find(radio).attr('checked',true);
		});
	}
};