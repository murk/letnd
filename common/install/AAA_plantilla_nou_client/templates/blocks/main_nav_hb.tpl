<ul>
	<? foreach ( $loop as $l ):extract( $l ) ?>
		<li>
			<a href="<?= $link ?>" class="active<?= $selected ?> <?= $class ?>" title="<?= htmlspecialchars( $item ) ?>"><?= $item ?></a>
		</li>
	<? endforeach //loop?>
</ul>