
// Funcions per copiar a functions.js si fan falta

var client = {

	/*
	Dins de init()
	client.init_scroll_smooth();
	*/

	init_scroll_smooth: function () {

		$("a[href^='#']").click(function () {
			var link = $(this).attr('href');
			var offset = new Object;
			if (link == '#') {
				offset.top = 0;
			}
			else if (link != '#') {
				link = link.substr(1);
				offset = $("a[name='" + link + "']").offset();
			}

			if (typeof offset.top != 'undefined') {
				$('html, body').animate({scrollTop: offset.top}, 'slow');
				return false;
			}
		});
	},


	/*
	Dins de init()
	client.fixa_menus();
	*/

	fixa_menus: function () {


		var was_fixed = $('#header').css('position') == 'fixed';

      // aquest dispara el fixe, ha de ser just quan entra, per tant hauria de ser l'element anterior
		client.top_menu = $('#header .top').outerHeight();


		if ($(window).height() < ($('#main-nav').height() + 400)) return;
		// Fixa menus

		// nomès per plantilla general
		if ($('#main-nav').length) {

			// FIXAR SCROLL
			$(window).scroll(function (event) {

				var y = $(this).scrollTop();
				var x = $(this).scrollLeft();

				var holder = '#header';
				var fixed_class_target = '#header,body';


				if (y >= client.top_menu) {
					if (!client.is_fixed) {


						var total_height = $(holder).height();
						dp('total_height', total_height);

						$(fixed_class_target).addClass('fixed');
						$(fixed_class_target).removeClass('nofixed');

						if (!was_fixed) $('body').css('padding-top', total_height);

						client.is_fixed = true;
					}

				} else {

					$('body').css('padding-top', '0');
					$(fixed_class_target).removeClass('fixed');
					$(fixed_class_target).addClass('nofixed');

					client.is_fixed = false;

				}

			});
		}
	},


	/*
	AL clicar en un input, posa la clase 'focus' al parent , així podem canviar el css del label quan estem
	Dins de init()
	client.init_contact_focus();
	*/
	init_contact_focus: function () {

		if (gl_tool != 'contact') return;

		var $inputs = $('#contact-form input[type="text"],#contact-form textarea');

		$inputs.focus(function () {
			$(this).parent().parent().addClass('focus');
		}).blur(function () {
			$(this).parent().parent().removeClass('focus');
		});
	},

};