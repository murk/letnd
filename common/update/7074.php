<?php
/* 23-3-19
Arregla taula config de booking booking
*/


if ( is_table( 'booking__configadmin' ) && ! is_field( 'booking__configadmin', 'configadmin_id' ) ) {

	$update_sql[] = "ALTER TABLE `booking__configadmin`
	ADD COLUMN `configadmin_id` INT NOT NULL AUTO_INCREMENT FIRST,
	DROP PRIMARY KEY,
	ADD PRIMARY KEY (`configadmin_id`),
	ADD INDEX `name` (`name`);";

}
else {
	if ( is_table( 'booking__configadmin' ) ) {
		trigger_error( 'Ja existeixen el camp configadmin_id' );
	}
}

