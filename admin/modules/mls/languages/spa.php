<?php
include_once(DOCUMENT_ROOT . '/admin/modules/custumer/languages/'.LANGUAGE.'.php');
// menus eina
/*
define('CUSTUMER_MENU_CUSTUMER','Gestió de clients');
define('CUSTUMER_MENU_NEW','Entrar nou client');
define('CUSTUMER_MENU_LIST','Llistar tots els clients');
define('CUSTUMER_MENU_LIST_OWNERS','Llistar venedors');
define('CUSTUMER_MENU_LIST_BUYERS','Llistar sol·licitants');
define('CUSTUMER_MENU_LIST_TENANTS','Llistar llogaters');
define('CUSTUMER_MENU_BIN','Papelera de reciclatge');
define('CUSTUMER_MENU_','Sol·licituds de clients');
define('CUSTUMER_MENU_DEMAND','Llistar sol·licituds');
define('CUSTUMER_MENU_DEMAND_LIST','Llistar sol·licituds');
define('CUSTUMER_MENU_DEMAND_CHECK','Búsquedes exactes');
define('CUSTUMER_MENU_DEMAND_PROX','Búsquedes proximatives');
define('CUSTUMER_SELECT_WINDOW_TILE','Selecionar propietats');
define('MLS_MENU_COMPANIES','MLS');
define('MLS_MENU_COMPANIES_LIST','Llistat empreses');
define('MLS_MENU_COMPANIES_YES','Empreses compartides');
*/

define('MLS_MENU_SEARCH','Búsquedes');
define('MLS_MENU_SEARCH_EXACT','Búsquedes exactes');
define('MLS_MENU_SEARCH_PROX','Búsquedes proximatives');

$gl_caption['c_fiscalname'] = 'Nom fiscal';
$gl_caption['c_comercialname'] = 'Nom comercial';
$gl_caption['c_adress'] = 'Adreça';
$gl_caption['c_town'] = 'Ciutat';
$gl_caption['c_province'] = 'Provincia';
$gl_caption['c_zip'] = 'Codi Postal';
$gl_caption['c_country'] = 'País';
$gl_caption['c_respname'] = 'Nom';
$gl_caption['c_surnames'] = 'Cognoms';
$gl_caption['c_phone1'] = 'Telèfon (1)';
$gl_caption['c_phone2'] = 'Telèfon (2)';
$gl_caption['c_fax'] = 'Fax';
$gl_caption['c_mail1'] = 'Mail';
$gl_caption['c_domain'] = 'Pàgina web';

$gl_caption_companies['c_client_id2'] = 'Empressa compartida';
$gl_caption_companies['c_add_company_title']='Afegir empresa MLS';
$gl_caption_companies['c_badd']='Afegir empresa';
$gl_caption_companies['c_nif']='NIF';
$gl_caption_companies['c_mlscode']='Codi MLS';
$gl_caption_companies['c_permission']='Estat';
$gl_caption_companies['c_permission_askfor']='Esperant a que ens accepti';
$gl_caption_companies['c_permission_askedfor']='<span class="vert">Ens sol·licita permis per compartir<span>';
$gl_caption_companies['c_permission_denied']='<span class="vermell">Ens ha denegat permis per compartir<span>';
$gl_caption_companies['c_permission_canreaccept']='Li hem denegat permís';
$gl_caption_companies['c_permission_accepted']='Compartint';
$gl_caption_companies['c_permission_']='Cap relació';

$gl_caption ['c_add_company_button'] = 'Sol.licitar MLS';
$gl_caption ['c_accept_company_button']='Acceptar a MLS';
$gl_caption ['c_deny_company_button']='Denegar MLS';
$gl_caption ['c_erase_shared_company_button']='Eliminar del MLS';
//$gl_caption ['c_waiting_to_be_accepted_company_button']='Esperant ser acceptat a MLS';
$gl_caption ['c_denied_company_reaccept_company_button']='Reacceptar a MLS';
//$gl_caption['c_i_was_denied_company_button']='Aquesta empresa no em permet solicitar MLS';


$gl_messages ['company_deleted']='Empresa eliminada del MLS.';
$gl_messages ['company_authorized']='Empresa autorizada a compartir MLS.';
$gl_messages ['company_waiting']='Sol·licitud MLS enviada, esperant acceptació.';
$gl_messages ['company_denied']='Sol.licitdud MLS denegada.';
$gl_messages ['company_reaccepted']='Empresa reacceptada a compartir MLS';

$gl_caption ['c_missatge_add_company']='L\'empressa <a href = "%s">%s</a> a sol·licitat compartir propietats MLS amb vostè';
$gl_caption ['c_asunto_add_company'] = 'Compartir propietats amb MLS';
$gl_caption ['c_missatge_authorized_company']='L\'empressa <a href = "%s">%s</a> ha permes compartir propietats MLS amb vostè';
$gl_caption ['c_asunto_authorized_company'] = 'MLS autoritzat';
$gl_caption ['c_missatge_denied_company']='L\'empressa <a href = "%s">%s</a> a denegat compartir propietats MLS amb vostè';
$gl_caption ['c_asunto_denied_company'] = 'Compartir propietats amb MLS denegada';
$gl_caption ['c_missatge_reaccepted_company']='L\'empressa <a href = "%s">%s</a> a reacceptat compartir propietats MLS amb vostè';
$gl_caption ['c_asunto_reaccepted_company'] = 'Ha sigut reacceptada la sol·licitud de compartir MLS';
$gl_caption ['c_missatge_erase_shared_company']='L\'empressa <a href = "%s">%s</a> a  finalitzat la relació MLS amb vostè';
$gl_caption ['c_asunto_erase_shared_company'] = 'MLS eliminat';

?>