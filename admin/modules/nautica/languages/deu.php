<?php
// menus eina
define('NAUTICA_MENU_EMBARCACION_LIST', 'Llistat i editar embarcacions');
define('NAUTICA_MENU_EMBARCACION', 'Embarcacions');
define('NAUTICA_MENU_EMBARCACION_NEW', 'Nova embarcació');
define('NAUTICA_MENU_EMBARCACION_BIN', 'Paperera de reciclatge');
define('NAUTICA_MENU_TIPUS', 'Tipus');
define('NAUTICA_MENU_TIPUS_LIST', 'Llistat de tipus');
define('NAUTICA_MENU_TIPUS_NEW', 'Nou tipus');
define('NAUTICA_MENU_SHIPYARD', 'Astillers');
define('NAUTICA_MENU_SHIPYARD_LIST', 'Llistat d\'astillers');
define('NAUTICA_MENU_SHIPYARD_NEW', 'Nou astiller');
define('NAUTICA_MENU_FLAG', 'Banderes');
define('NAUTICA_MENU_FLAG_LIST', 'Llistat de banderes');
define('NAUTICA_MENU_FLAG_NEW', 'Nova bandera');

// captions seccio
$gl_caption_embarcacion['c_status'] = 'Stände';
$gl_caption_embarcacion['c_clau_dossier'] = 'Interne informationen';
$gl_caption_embarcacion['c_clau'] = 'Schlüssel';
$gl_caption_embarcacion['c_dossier'] = 'Ordner';
$gl_caption_embarcacion['c_entered'] = 'Eingang';
$gl_caption_embarcacion['c_entered_form'] = 'Eingangsdatum';
$gl_caption_embarcacion['c_prepare'] = 'In Vorbreitung';
$gl_caption_embarcacion['c_review'] = 'Checken';
$gl_caption_embarcacion['c_onsale'] = 'Für verkaufen';
$gl_caption_embarcacion['c_sold'] = 'Verkauft';
$gl_caption_embarcacion['c_archived'] = 'Eingereicht';
$gl_caption_embarcacion['c_reserved'] = 'Anhängig';
$gl_caption_embarcacion['c_tipus_id'] = 'Modell';
$gl_caption['c_shipyard'] = 'Werft';
$gl_caption_embarcacion['c_shipyard_id'] = 'Werft';
$gl_caption_embarcacion['c_model'] = 'Modell';
$gl_caption_embarcacion['c_buildyear'] = 'Baujahr';
$gl_caption_embarcacion['c_buildyear_form'] = 'Baujahr';
$gl_caption_embarcacion['c_length'] = 'Länge';
$gl_caption_embarcacion['c_breadth'] = 'Breite';
$gl_caption_embarcacion['c_weight'] = 'Gewicht';
$gl_caption_embarcacion['c_depth'] = 'Tiefgang';
$gl_caption_embarcacion['c_depth2'] = 'Tiefgang';
$gl_caption_embarcacion['c_engine'] = 'Motoren';
$gl_caption_embarcacion['c_fuel'] = 'Kraftstoffart';
$gl_caption_embarcacion['c_fuel_gasoline'] = 'Benzin';
$gl_caption_embarcacion['c_fuel_diesel'] = 'Diesel';
$gl_caption['c_flag'] = 'Zulassung';
$gl_caption_embarcacion['c_flag_id'] = 'Zulassung';
$gl_caption_embarcacion['c_hour_use'] = 'Stunden';
$gl_caption_embarcacion['c_price'] = 'Preis';
$gl_caption_embarcacion['c_price_pounds'] = 'Preis (&)';

$gl_caption_embarcacion['c_vhf'] = 'VHF';
$gl_caption_embarcacion['c_depthsounder'] = 'Echolot';
$gl_caption_embarcacion['c_speedlog'] = 'Sumlog';
$gl_caption_embarcacion['c_radar'] = 'Radar';
$gl_caption_embarcacion['c_gps'] = 'GPS';
$gl_caption_embarcacion['c_plotter'] = 'Plotter';
$gl_caption_embarcacion['c_autopilot'] = 'Autopilot';
$gl_caption_embarcacion['c_trimmtabs'] = 'Hydr. Trimmklappen';
$gl_caption_embarcacion['c_searchlight'] = 'Suchscheinwerfer';
$gl_caption_embarcacion['c_electrical_capstan'] = 'El. Ankerwinsch';
$gl_caption_embarcacion['c_gangway'] = 'Gangway';
$gl_caption_embarcacion['c_liferaft'] = 'Rettungsinsel';
$gl_caption_embarcacion['c_tender'] = 'Dinghy';
$gl_caption_embarcacion['c_offshore_connexion'] = 'Landanschluss';
$gl_caption_embarcacion['c_battery_charger'] = 'Ladegerät';
$gl_caption_embarcacion['c_generator'] = 'Generator';
$gl_caption_embarcacion['c_cover'] = 'Persening';
$gl_caption_embarcacion['c_camping_cover'] = 'Cabriolet';
$gl_caption_embarcacion['c_bimini_top'] = 'Sonnenverdeck';
$gl_caption_embarcacion['c_bowthruster'] = 'Bugstrahlruder';
$gl_caption_embarcacion['c_hot_water'] = 'Warmwasser';
$gl_caption_embarcacion['c_davit'] = 'Davit';
$gl_caption_embarcacion['c_anemometer'] = 'Wind Log';
$gl_caption_embarcacion['c_mainsail'] = 'Grossegel';
$gl_caption_embarcacion['c_genoa'] = 'Genua';
$gl_caption_embarcacion['c_mizzen'] = 'Besansegel';
$gl_caption_embarcacion['c_jib'] = 'Sturmsegel';
$gl_caption_embarcacion['c_stormsail'] = 'Strom segel';
$gl_caption_embarcacion['c_forsail2'] = '2.Vorsegel';
$gl_caption_embarcacion['c_spinaker'] = 'Spinaker';
$gl_caption_embarcacion['c_spi_boom'] = 'Spi-Baum';
$gl_caption_embarcacion['c_roller_genoa'] = 'Rollreef Genua';

$gl_caption_embarcacion['c_music_equipment'] = 'Rado';
$gl_caption_embarcacion['c_television'] = 'Televisió';
$gl_caption_embarcacion['c_attached_engine'] = 'Aussenborder motoren';
$gl_caption_embarcacion['c_fridge'] = 'Kühlschrank';
$gl_caption_embarcacion['c_heating'] = 'Heizung';
$gl_caption_embarcacion['c_air_conditioning'] = 'Klimaanlage';
$gl_caption_embarcacion['c_teka'] = 'Teak deck';
$gl_caption_embarcacion['c_wc'] = 'WC';
$gl_caption_embarcacion['c_black_water'] = 'Fekalien tank';
$gl_caption_embarcacion['c_roller_mainsail'] = 'Rollreef Grossegel';




$gl_caption_embarcacion['c_equipament'] = 'Ausstattung';

$gl_caption_embarcacion['c_data'] = 'Boots charakteristiken';
$gl_caption_embarcacion['c_ref'] = 'Referenz';
$gl_caption_embarcacion['c_general_data'] = 'Generelle Beschreibung';
$gl_caption_embarcacion['c_measures_weight'] = 'Mides - Pes';
$gl_caption_embarcacion['c_measures'] = 'Messung';
$gl_caption_embarcacion['c_weight_units'] = 'kg.';
$gl_caption_embarcacion['c_characteristics_electronic'] = 'Elektronisch';
$gl_caption_embarcacion['c_characteristics_electronic_caps'] = 'ELEKTRONISCH';
$gl_caption_embarcacion['c_characteristics_extra'] = 'Zubehör';
$gl_caption_embarcacion['c_characteristics_sail'] = 'Segel';
$gl_caption_embarcacion['c_characteristics'] = 'Technishen daten';

$gl_caption_embarcacion['c_filter_shipyard_id'] = 'Alle Werften';
$gl_caption_embarcacion['c_filter_tipus_id'] = 'Alle Typen';
$gl_caption_embarcacion['c_filter_status'] = 'Alle zustände';
$gl_caption_embarcacion['c_filter_length'] = 'Alle Längen';
$gl_caption_embarcacion['c_filter_price'] = 'Alle Preise';
$gl_caption_embarcacion['c_print_pdf'] = 'Print pdf';
$gl_caption_embarcacion['c_changeable'] = 'Acepto embarcación a cambio';

$gl_caption_tipus['c_ordre']='Ordre';
$gl_caption_tipus['c_tipus']='Tipus';
?>