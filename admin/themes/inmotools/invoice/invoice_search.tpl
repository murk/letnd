<style>
	div.search.invoice-search input[type=text], div.search.invoice-search input.boto-search {
	border-radius:3px;
}

	div.search.invoice-search input.boto-search {
		border-left-width: 1px;
	}
	div.search.invoice-search input[name="q"] {
		margin-right: 30px;
	}
	div.search.invoice-search .date2 {
		padding-left: 10px;
	}
</style>

<script language="JavaScript">

	$(document).ready(
		function () {

			$('#date1').datepicker({
				changeMonth: true,
				changeYear: true,
				yearRange: '+0:+5',
				onClose: function (selectedDate) {
					/*
					var date = $(this).datepicker('getDate');
					$( '#date1' ).datepicker( 'option', 'minDate', date );
					if (date) {
						  date.setDate(date.getDate() + 4);
					}
					$( '#date2' ).datepicker( 'option', 'maxDate', date );
					*/
				}
			});
			$('#date2').datepicker({
				changeMonth: true,
				changeYear: true,
				yearRange: '+0:+5',
				onClose: function (selectedDate) {
					/*
					var date = $(this).datepicker('getDate');
					$( '#date1' ).datepicker( 'option', 'maxDate', date );
					if (date) {
						  date.setDate(date.getDate() - 4);
					}
					$( '#date1' ).datepicker( 'option', 'minDate', date );
					*/
				}
			});
		}
	)

</script>


<div class="search invoice-search">
	<h3><?= $c_search_title ?></h3>
	<form name="search" method="get" action="/admin/">
		<input name="action" type="hidden" value="search">
		<input name="menu_id" type="hidden" value="<?= $menu_id ?>">
		<table border="0" cellspacing="0" cellpadding="0">
			<tr valign="middle">
            <td><?=$c_by_words?></td>
            <td>
              <input name="q" type="text" size="30" value="<?=$q?>"></td>
				<td><?= $c_dates_from ?>
					<input id="date1" name="date1" type="text" size="8" value="<?= $date1 ?>"></td>
				<td class="date2"><?= $c_dates_to ?>
					<input id="date2" name="date2" type="text" size="8" value="<?= $date2 ?>"></td>
				<td class="quarter padding-left-2"><?= $c_quarter ?>
					<?= $select_quarter ?> <?= $select_year ?></td>
				<td class="invoice_serial padding-left-2"><?= $c_invoice_serial ?>
					<?= $select_invoice_serial ?></td>
				<td class="buttons-td padding-left-2">
					<input type="submit" name="Submit" value="ok" class="boto-search"></td>
			</tr>
		</table>
	</form>
</div>