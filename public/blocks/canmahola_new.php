<?php
global $gl_is_home;

if (!defined('SHOW_ENTERED_TIME')) {
	$cf = get_config_array('public', 'news');
	$block_max_results = $cf['block_max_results'];
}
else {
	$block_max_results = NEWS_BLOCK_MAX_RESULTS;
}

$in_home = isset($vars['in_home'])?$vars['in_home']:false;
$show_images = isset($vars['show_images'])?$vars['show_images']:false;

$query = "SELECT news__new.new_id as new_id, 
				new, 
				subtitle,
				content_list,
				entered,
				url1,
				url1_name,
				category_id
				FROM news__new_language, news__new
				WHERE news__new_language.new_id = news__new.new_id
				AND (status = 'public')
				AND (entered=0 OR entered <= now())
				AND (expired=0 OR expired > (curdate()))
				AND bin <> 1
				AND language = '" . LANGUAGE . "'
				AND category_id = '1'
				ORDER BY ordre ASC,  entered DESC, new_id
				LIMIT " . $block_max_results;

$results1 = Db::get_rows($query);
$query = "SELECT news__new.new_id as new_id, 
				new, 
				subtitle,
				content_list,
				entered,
				url1,
				url1_name,
				category_id
				FROM news__new_language, news__new
				WHERE news__new_language.new_id = news__new.new_id
				AND (status = 'public')
				AND (entered=0 OR entered <= now())
				AND (expired=0 OR expired > (curdate()))
				AND bin <> 1
				AND language = '" . LANGUAGE . "'
				AND category_id = '2'
				ORDER BY ordre ASC,  entered DESC, new_id
				LIMIT " . $block_max_results;

$results2 = Db::get_rows($query);

$loop_count1 = count($results1);
$loop_count2 = count($results2);

$loop_count = $loop_count1>$loop_count2?$loop_count1:$loop_count2;

$empty_array = array (
    'new_id' => false,
    'new' => '',
    'subtitle' => '',
    'content_list' => '',
    'entered' => '',
    'url1' => '',
    'url1_name' => '',
    'category_id' => '',
  );

// barrejo results intercalant
$results = array();
for ($i=0; $i<$loop_count;$i++){
	$results []= isset($results1[$i])?$results1[$i]:$empty_array;
	$results []= isset($results2[$i])?$results2[$i]:$empty_array;
}

//debug::p($results);


$selected_found = false;
if ($results)
{	
	$conta = 0;
	$loop_count = count($results);
    foreach($results as $rs)
    {	
		$l = array();
        if (isset($_GET["new_id"]) && ($_GET["new_id"] == $rs["new_id"]))
		{
			$selected = 1;
			$selected_found = true;
		}
		else
		{
			$selected = 0;
		}
        
        
		if (USE_FRIENDLY_URL){
			$l['link'] = '/' . LANGUAGE . '/news/new/' . $rs['new_id'] . '/category_id/' . $rs['category_id'];
				
			
			// EVITAR DUPLICATS
			// haig de mirar si un modul està posat en un page		
			$query = "SELECT  page_file_name FROM all__page INNER JOIN all__page_language USING(page_id) WHERE language='".LANGUAGE."' AND link = '/news/new'";
			$page_name = Db::get_first($query);
			if ($page_name) $l['link'] .= '/' . $page_name . '.html';
			
		}
		else{		
			$l['link'] = '/?tool=news&tool_section=new&action=show_record&new_id=' . $rs['new_id'] . '&language=' . LANGUAGE;
			if (isset($_GET['page_id'])) $l['link'] .= '&page_id=' . $rs['page_id'];
		}

		$category = Db::get_first("SELECT category FROM news__category_language WHERE category_id = '" . $rs['category_id'] . "' AND language = '" . LANGUAGE . "'");
		
		$l['selected'] = $selected;
        $l['new'] = $rs['new'];
		$l['subtitle']=$rs['subtitle'];
		$l['content']=$rs['content_list'];
		$l['category_id']=$rs['category_id'];
		$l['category']=$category;
		$l['id'] = $rs['new_id'];				
		$l['entered']=$a = format_date_form ($rs['entered']);
		$l['url1'] = $rs['url1'];
		$l['url1_name'] = $rs['url1_name']?$rs['url1_name']:$rs['url1'];
		$l['conta'] = $conta++;
		$l['class'] = '';
		if ($show_images) {
		$l += UploadFiles::get_record_images($rs['new_id'], true, 'news', 'new');
		}
		$l['loop_count'] = $loop_count;
        $loop[] = $l;
    }
	$loop[0]['class'] = 'first';	
	$loop[count($loop)-1]['class'] = 'last';
	if (!$selected_found) $loop[0]['selected'] = 1;
	
	// separo per categoria, faig un loop per cada categoria
	$vars['categories'] = array_slice($loop, 0, 2);
	$vars['loop1'] = array();
	$vars['loop2'] = array();
	
	foreach ($loop as $rs){	
		$vars['loop'.$rs['category_id']][]=$rs;
	}
	$conta=0;
	foreach ($vars['loop1'] as $k=>$v){	
		$vars['loop1'][$k]['conta']=$conta;
		$conta++;
	}
	$conta=0;
	foreach ($vars['loop2'] as $k=>$v){	
		$vars['loop2'][$k]['conta']=$conta;
		$conta++;
	}
		
}
else {
/* si no hi ha cap resultat no mostro el block */
$show_block = false;
return;
}

?>