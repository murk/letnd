<?
/**
 * WebBanner
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2012
 * @version $Id$
 * @access public
 */
class WebBanner extends Module{

	function __construct(){
		parent::__construct();
	}
	function on_load() {
		parent::on_load();
		if ( $this->config['show_banner_text2'] == '1' ) {
			$this->set_field( 'banner_text2', 'form_admin', 'input' );
			$this->set_field( 'banner_text2', 'list_admin', 'text' );
			$this->set_field( 'banner_text2', 'type', $this->config['banner_text2_type'] );
		}
		else {
			$this->unset_field( 'banner_text2' );
		}
		$this->set_field( 'banner_text', 'type', $this->config['banner_text_type'] );
	}

	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function get_records()
	{
	    $listing = new ListRecords($this);
		$listing->add_swap_edit();
		$listing->order_by = 'ordre ASC, banner_id';
		$listing->show_langs = true;
		$listing->add_filter('place_id');
		$listing->add_filter('status');
		$listing->paginate = false;
	    $listing->set_records();
		foreach ($listing->loop as $key=>$val){
			$rs = &$listing->loop[$key];
			// Debug::p($rs['files']);
			if ($rs['files']){
				$rs['image_src_admin_thumb'] = $rs['files'][0]['file_src'];
				$rs['image_name'] = true;
			}
			else{
				$other_file_id = Db::get_first("
						SELECT file_id
						FROM web__banner_file
						WHERE banner_id = " . ( $rs['banner_id'] ) . "
						LIMIT 1");

				if ($other_file_id) {
					$rs['image_src_admin_thumb'] = UploadFiles::get_download_link( $this->tool, $this->tool_section, 'file', $other_file_id );
					$rs['image_name'] = true;
				}
				else {
					$rs['image_src_admin_thumb'] = '';
					$rs['image_name'] = false;
				}
			}
		}
		$listing->set_var ('has_images', true);
		if ($listing->loop) return $listing->process();
	}
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{
		if ($this->config['banner_info_size']) $this->caption['c_file'] = $this->caption['c_file'] . '<br /> ( ' . $this->config['banner_info_size'] . ' )';
		
	    $show = new ShowForm($this);
	    return $show->show_form();
	}

	function save_rows()
	{
	    $save_rows = new SaveRows($this);
	    $save_rows->save();
	}

	function write_record()
	{
	    $writerec = new SaveRows($this);
	    $writerec->save();
	}

	function manage_images()
	{
	    $image_manager = new ImageManager($this);
	    $image_manager->execute();
	}
}
?>