<?

/* 25-7-14 
  Modificacions immo reserves

 */

if (is_table('booking__extra_to_property')) {	
	
	$update_sql .= "
		ALTER TABLE `booking__extra_to_property`
	DROP COLUMN `extra_type`;
		";
	$update_sql .= "
		ALTER TABLE `booking__extra_to_property`
	CHANGE COLUMN `price` `extra_price` DECIMAL(6,2) NOT NULL DEFAULT '0.00' AFTER `extra_id`;
		";
	
}

if (is_table('booking__extra_to_book')) {	
	
	$update_sql .= "
		ALTER TABLE `booking__extra_to_book`
	CHANGE COLUMN `price` `extra_price` DECIMAL(6,2) NOT NULL DEFAULT '0.00' AFTER `extra_id`;
		";
	
}

if (is_table('booking__book')) {	
	
	$update_sql .= "
		ALTER TABLE `booking__book`
			ADD COLUMN `adult` INT(11) NOT NULL AFTER `property_id`,
			ADD COLUMN `child` INT(11) NOT NULL AFTER `adult`,
			ADD COLUMN `baby` INT(11) NOT NULL AFTER `child`;
		";
	
}

if (is_table('custumer__custumer')) {	
	
	$update_sql .= "
		ALTER TABLE `custumer__custumer`
			ADD COLUMN `password` VARCHAR(50) NOT NULL DEFAULT '' AFTER `user_id`;
		";
	
	$update_sql .= "
		ALTER TABLE `custumer__custumer`
			ADD COLUMN `book_group` INT(11) NOT NULL DEFAULT '0' AFTER `password`;
		";
	
	$update_sql .= "
		ALTER TABLE `custumer__custumer`
	ADD COLUMN `book_main` TINYINT NOT NULL DEFAULT '0' AFTER `book_group`;
		";
	
}

if (is_table('booking__book_to_custumer')) {	
		
	$update_sql .= "
		ALTER TABLE `booking__book_to_custumer`
	ADD COLUMN `ordre` INT(11) NULL DEFAULT '0' AFTER `custumer_id`;
		";
	
}

if (is_table('custumer__custumer') && !is_table('custumer__configpublic')) {	
		
	$update_sql .= "
		CREATE TABLE `custumer__configpublic` (
		`configpublic_id` INT(11) NOT NULL AUTO_INCREMENT,
		`name` VARCHAR(50) NOT NULL DEFAULT '' COLLATE 'utf8_spanish2_ci',
		`value` VARCHAR(255) NOT NULL DEFAULT '' COLLATE 'utf8_spanish2_ci',
		PRIMARY KEY (`configpublic_id`)
		)
		COLLATE='utf8_spanish2_ci'
		ENGINE=MyISAM
		ROW_FORMAT=DYNAMIC
		AUTO_INCREMENT=1;
		";
	
		
	$update_sql .= "
		ALTER TABLE `custumer__custumer`
	CHANGE COLUMN `mail` `mail` VARCHAR(50) NOT NULL DEFAULT '' COLLATE 'utf8_spanish2_ci' AFTER `comta`;
		";
	
}
?>