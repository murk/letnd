<?
/**
 * __Tool__Section
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2010
 * @version $Id$
 * @access public
 */
class SistemamuseuMuseu extends Module{

	function __construct(){
		parent::__construct();
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function get_records()
	{
		$listing = new ListRecords($this);
		$listing->condition = "status='public'";
		$listing->paginate = false;
		$listing->template = 'sistemamuseu/museu_map_list';
	    return $listing->list_records();
	}
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{
	    $_GET['museu_id']= $_GET['ref']?Db::get_first("SELECT museu_id FROM sistemamuseu__museu WHERE ref = '" . R::escape('ref') . "'"):0;	
		
		$show = new ShowForm($this);
		
		$show->get_values();
		$rs = &$show->rs;
		
		$rs['latitude']=$rs['latitude']!=0?$rs['latitude']:SISTEMAMUSEU_GOOGLE_CENTER_LATITUDE;
		$rs['longitude']=$rs['longitude']!=0?$rs['longitude']:SISTEMAMUSEU_GOOGLE_CENTER_LONGITUDE;
		$rs['zoom']=$rs['zoom']?$rs['zoom']:SISTEMAMUSEU_GOOGLE_ZOOM;
		
		$GLOBALS['gl_page']->page_title = $show->rs['museu_title'];
		$GLOBALS['gl_page']->page_description = add_dots_meta_description($show->rs['museu_description']);
		
		// si l'url no te un nom mostro url
		//$rs['url2_name']=$rs['url2_name']?$rs['url2_name']:$rs['url2'];
		
		
		// Detecto url automaticament
		$sch = &$rs['schedule'];	
		$pos1 = strpos($sch, 'www.');
		if ($pos1!==false) {
			$pos2 = strpos($sch,' ',$pos1);
			$pos3 = strpos($sch,"\n",$pos1);
			$pos4 = strpos($sch,"\r",$pos1);
			$pos5 = strpos($sch,"\t",$pos1);
			$pos2 = ($pos3!==false && $pos3<$pos2)?$pos3:$pos2;
			$pos2 = ($pos4!==false && $pos4<$pos2)?$pos4:$pos2;
			$pos2 = ($pos5!==false && $pos5<$pos2)?$pos5:$pos2;
			$sch = substr ($sch, 0, $pos1) . '<a href="http://'.substr ($sch, $pos1, $pos2-$pos1). '">' . substr ($sch, $pos1, $pos2-$pos1) . '</a>' . substr ($sch, $pos2);
			$sch = nl2br($sch);
			$this->set_field('schedule','type','none');
		}
		$show->condition = "status = 'public'";
		$show->get_values();		
		if ($show->has_results)
		{
			return $show->show_form();
		}
		else
		{
			Main::redirect('/');
		}
		
	    return $show->show_form();
	}

	function manage_images()
	{
	    $image_manager = new ImageManager($this);
	    $image_manager->execute();
	}
}
?>