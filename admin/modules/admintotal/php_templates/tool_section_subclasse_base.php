<?
/**
 * __Tool__Section
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2011
 * @version $Id$
 * @access public
 */
class __Tool__Section extends Module{

	public function __construct(){
		parent::__construct();
	}
	/* funció que es crida per fer una acció del modul automatica o desde un altre modul ( list_records, show_form ) etc...*/
	public function do_action($action = ''){
		switch ($this->parent){
			// nom del module parent
			case '__Parent__Tool__Section':			
			//$this->unset_field ('__Field');		
			break;
		}
		// el camp parent_section nomès el tinc quan creo un registre nou
		// un cop creat el registre no es modifica mai aquest camp, el registre pertany sempre a aquella secció
	    if ($this->action=='add_record') {
			$this->add_field('parent_module'); 
			$this->set_field ('parent_module','override_save_value',"'" . $this->parent . "'"); // sempre sobreescric el parent_module
		}
		return parent::do_action($action);
	}
	public function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	public function get_records()
	{
	    $listing = new ListRecords($this);
		$listing->condition = "parent_module = '" . $this->parent . "'";
	    return $listing->list_records();
	}
	public function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	public function get_form()
	{
	    $show = new ShowForm($this);
	    return $show->show_form();
	}

	public function save_rows()
	{
	    $save_rows = new SaveRows($this);
	    $save_rows->save();
	}

	function write_record()
	{
	    $writerec = new SaveRows($this);
	    $writerec->save();
	}

	public function manage_images()
	{
	    $image_manager = new ImageManager($this);
	    $image_manager->execute();
	}
}