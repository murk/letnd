<?
/*
RESTAURANT MASSANA
*/
if (!defined('CLIENT_MENU_MASSANA')){
	define( 'CLIENT_MENU_MASSANA', 'Cartes' );
	define( 'CLIENT_MENU_MASSANA_NEW', 'Pujar arxius cartes' );
}


// Importacio transfluid
$gl_caption['c_import_preview'] = 'Previsualitzar importació';
$gl_caption['c_import'] = 'Importar';
$gl_caption['c_import_title'] = 'Importar adreces';
$gl_caption['c_select_file'] = 'Seleccionar arxiu';
$gl_caption['c_group'] = 'Grup';