<script language="JavaScript" src="/admin/jscripts/validacio.js?v=<?=$version?>"></script>
<script language="JavaScript" src="/admin/jscripts/euro_calc.js?v=<?=$version?>"></script>
<script language="JavaScript" src="/common/jscripts/jquery.dropshadow.js?v=<?=$version?>"></script>
<!--<script language="JavaScript" src="/admin/modules/portal/jscripts/chained_selects.js?v=<?=$version?>"></script>-->
<!--<script language="JavaScript" src="/admin/modules/portal/jscripts/chained_selects2.js?v=<?=$version?>"></script>-->
<script type="text/javascript">
$(function() {
	set_focus(document.theForm);
});
</script><form name="form_delete" method="post" action="<?=$link_action_delete?>" target="save_frame">
    <input name="<?=$id_field?>" type="hidden" value="<?=$id?>">
  </form><form name="theForm" method="post" action="<?=$link_action?>" onsubmit="<?=$js_string?>" encType="multipart/form-data" target="save_frame">
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="form"> 
  <tr>
    <td>
        <?if ($form_new):?>
        <div id="form_buttons_holder"><table id ="form_buttons">
          <tr> 
            <td class="buttons_top"><div class="right"><input type="submit" name="Submit" value="<?=$c_send_new?>" class="btn_form1"></div></td>
          </tr>
        </table></div>
        <?endif //form_new?><?if ($form_edit):?>
            <div id="form_buttons_holder"><table id ="form_buttons">
              <tr>
                <td valign="bottom"><div class="menu"><div><?=$c_edit_data?></div><?if ($has_images):?>
            	<a href="<?=$image_link?>"><?=$c_edit_images?></a>
         <?endif //has_images?>
			<a href="?menu_id=<?=$menu_id?>&customer_id=<?=$customer_id?>&action=show_form_map"><?=$c_edit_map?></a></div>
					</td>
                <td align="right" class="buttons_top"><div class="right"><input type="submit" name="Submit" value="<?=$c_send_edit?>" class="btn_form1" />
                  &nbsp;&nbsp;<a class="btn_form_delete" href="javascript:submit_form('<?=$c_confirm_form_bin?>', true);">
                  <?=$c_delete?>
                  </a>&nbsp;<a class="btn_form1" href="<?=$preview_link?>" target="_blank">
                  <?=$c_preview?>
                  </a></div></td>
              </tr>
          </table></div>
          <?endif //form_edit?> 
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr> 
            <td><table width="100%" cellpadding="4" cellspacing="0">
                <tr> 
                  <td class="formCaptionTitle"><strong>
                  <?=$c_data?>&nbsp;</strong></td>
                </tr>
              </table>
              <table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                  <td>
<table width="100%" cellspacing="0" class="formstbl">
                    <tr>
                      <td width="150" class="formsCaption"><strong>
                        <?=$c_customer_private?>:</strong></td>
                      <td class="forms"><input name="customer_id[<?=$customer_id?>]" type="hidden" value="<?=$customer_id?>" />
                          <input name="customer_id_javascript" type="hidden" value="<?=$customer_id?>" />
                          <?=$customer_private?></td>
                    </tr>
                    <tr>
                      <td valign="top" class="formsCaption"><strong>
                        <?=$c_observations?>: </strong></td>
                      <td class="forms"><?=$observations?></td>
                    </tr>
					 <tr>
                      <td valign="top" class="formsCaption"><strong>
                        <?=$c_owner_name?>: </strong></td>
                      <td class="forms"><?=$owner_name?></td>
                    </tr>
					<tr>
                      <td valign="top" class="formsCaption"><strong>
                        <?=$c_owner_surnames?>: </strong></td>
                      <td class="forms"><?=$owner_surnames?></td>
                    </tr>				
					 <tr>
                      <td valign="top" class="formsCaption"><strong>
                        <?=$c_owner_phone?>: </strong></td>
                      <td class="forms"><?=$owner_phone?></td>
                    </tr>
                  </table>
                    <table width="100%" cellpadding="4" cellspacing="0">
                      <tr>
                        <td width="150" valign="top" class="formsCaptionHor"><?=$c_data_customer?>: </td>
                      </tr>
							</table>
                    <table width="100%" cellpadding="4" cellspacing="0" class="formstbl">
                      <tr>
                        <td width="150"  valign="top" class="formsCaption"><strong>
                          <?=$c_category_id?>:</strong></td>
                        <td class="forms"><?=$category_id?></td>
                      </tr>                      
                      <tr>
                        <td class="formsCaption"><strong>
                          <?=$c_telephone?>:</strong></td>
                        <td class="forms"><?=$telephone?></td>
                      </tr>
                      <tr>
                        <td class="formsCaption"><strong>
                          <?=$c_telephone_mobile?>:</strong></td>
                        <td class="forms"><?=$telephone_mobile?>
                          &nbsp;</td>
                      </tr>
                      <tr>
                        <td class="formsCaption"><strong>
                          <?=$c_fax?>:</strong></td>
                        <td class="forms"><?=$fax?></td>
                      </tr>
                      <tr>
                        <td class="formsCaption"><strong>
                          <?=$c_adress?>:</strong></td>
                        <td class="forms"><?=$adress?></td>
                      </tr>
                      <tr>
                        <td class="formsCaption"><strong>
                          <?=$c_country_id?>:</strong></td>
                        <td class="forms"><?=$country_id?></td>
                      </tr>
					  <tr>
                        <td class="formsCaption"><strong>
                          <?=$c_provincia_id?>:</strong></td>
                        <td class="forms"><?=$provincia_id?></td>
                      </tr>
                      <tr>
                        <td class="formsCaption"><strong>
                          <?=$c_comarca_id?>:</strong></td>
                        <td class="forms"><?=$comarca_id?>
                          <input name="old_comarca_id[<?=$customer_id?>]" type="hidden" value="0" /></td>
                      </tr>
                      <tr>
                        <td class="formsCaption"><strong>
                          <?=$c_municipi_id?>:</strong></td>
                        <td class="forms"><?=$municipi_id?>
                          <input name="old_municipi_id[<?=$customer_id?>]" type="hidden" value="0" /></td>
                      </tr>
                      <tr>
                        <td class="formsCaption"><strong><strong>
                          <?=$c_mail?>
                        </strong> </strong>:</td>
                        <td class="forms"><?=$mail?></td>
                      </tr>
                      <tr>
                        <td class="formsCaption"><strong><strong>
                          <?=$c_url?>
                        </strong> </strong>:</td>
                        <td class="forms"><?=$url?></td>
                      </tr>
                        <tr>
                          <td class="formsCaption"><strong><strong>
                            <?=$c_price_id?>
                          </strong></strong>:</td>
                          <td class="forms"><?=$price_id?></td>
                        </tr>
                        <td class="formsCaption"><strong><strong>
                          <?=$c_status?>
                        </strong> </strong>:</td>
                        <td class="forms"><?=$status?></td>
                      </tr>
                      <tr>
                        <td class="formsCaption"><strong><strong>
                          <?=$c_ordre?>:</strong></strong></td>
                        <td class="forms"><?=$ordre?></td>
                      </tr>
					   <tr>
                        <td valign="top" class="formsCaption"><strong><strong>
                          <?=$c_file?>:</strong></strong></td>
                        <td class="forms"><?=$file?></td>
                      </tr>
                    </table></td>
                </tr>
              </table><div class="sep_dark"></div>
              <table width="100%" cellpadding="4" cellspacing="0">
                <tr> 
                  <td class="formCaptionTitle"><strong><?=$c_descriptions?></strong></td>
                </tr>
              </table>
              <table width="100%" cellpadding="0" cellspacing="0" class="formstbl">
                <tr> 
                  <td><table cellpadding="4" cellspacing="0">
                      <tr> 
                        <td width="150" valign="top" class="formsCaption"><strong>
                        <?=$c_customer_title?>:</strong></td>
                        <td><table cellpadding="4" cellspacing="0"><?foreach($customer_title as $l): extract ($l)?>
                      <tr> 
                        <td class="forms"><?=$lang?></td><td>
                          <?=$customer_title?> 
                        </td>
                      </tr>
                        <?endforeach //$customer_title?>
                    </table></td>
                      </tr>
                    </table></td>
                </tr>
              </table><table width="100%" cellpadding="0" cellspacing="0">
                  	<tr> 
                        <td class="formsCaptionHor"><?=$c_description?></td>
                      <tr> 
                    </table><table width="100%" cellpadding="0" cellspacing="0" class="formstbl">
                <tr> 
                  <td>
                    <table width="100%" cellpadding="4" cellspacing="0">
                      <?foreach($customer_description as $l): extract ($l)?>
                      <tr> 
                        <td class="formsHor"><?=$lang?><br><?=$customer_description?>
                        </td>
                      </tr>
                      <?endforeach //$product_description?>
                    </table>
					</td>
                </tr>
              </table>
				  <table width="100%" cellpadding="0" cellspacing="0">
                  	<tr> 
                        <td class="formsCaptionHor"><?=$c_schedule?></td>
                      <tr> 
                    </table>
				  <table width="100%" cellpadding="0" cellspacing="0" class="formstbl">
                <tr> 
                  <td>                    
                    <table width="100%" cellpadding="4" cellspacing="0">
                      <?foreach($schedule as $l): extract ($l)?>
                      <tr> 
                        <td class="formsHor"><?=$lang?><br><?=$schedule?>
                        </td>
                      </tr>
                      <?endforeach //$schedule?>
                    </table>
					</td>
                </tr>
              </table>
				  
              <?if ($form_new):?>
              <table width="100%" cellpadding="4" cellspacing="2" class="formsButtonstbl">
                <tr> 
                  <td align="center" class="formsButtons"> <input type="submit" name="Submit" value="<?=$c_send_new?>" class="btn_form1"> 
                  </td>
                </tr>
              </table>
              <?endif //form_new?><?if ($form_edit):?>
              <table width="100%" cellpadding="4" cellspacing="2" class="formsButtonstbl">
                <tr> 
                  <td align="center" class="formsButtons"><input type="submit" name="Submit" value="<?=$c_send_edit?>" class="btn_form1">&nbsp;&nbsp;<a class="btn_form_delete" href="javascript:submit_form('<?=$c_confirm_form_bin?>', true);">
                      <?=$c_delete?>
                      </a>&nbsp;<a class="btn_form1" href="<?=$preview_link?>" target="_blank">
                <?=$c_preview?>
                </a>&nbsp;</td>
                </tr>
              </table>
              <?endif //form_edit?></td>
          </tr>
        </table></td>
  </tr>
</table>
      </form>
