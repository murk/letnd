<?php
// FUNCIONS
// Buscador principal

/**
 *
 * @access public
 * @return void
 */
function make_menu_tipus()
{
    global $tpl;
    $query = "SELECT Distinct tipus
				FROM inmo__property
				WHERE bin = 0";

    $results = Db::get_rows($query);

    if ($results)
    {
        foreach($results as $rs)
        {
            isset($_GET["tipus"]) && ($_GET["tipus"] == $rs["tipus"])?$marked = 'Selected':$marked = '';

            $rs["tipus_lang"] = get_property_tipus('tipus', $rs["tipus"]);
            $rs["marked"] = $marked;
            $loop[] = $rs;
        }
    }
    $tpl->set_loop('menu_tipus', $loop);
}

/**
 *
 * @access public
 * @return void
 */
function make_filter_tipus()
{
    global $tpl;
    $query = "SELECT Distinct tipus
				FROM inmo__property
				WHERE bin = 0";

    $results = Db::get_rows($query);

    if ($results)
    {
        foreach($results as $rs)
        {
            $q = &$_GET["tipus"];
            isset($q[$rs["tipus"]]) && ($q[$rs["tipus"]] == $rs["tipus"])?$marked = 'Checked':$marked = '';

            $rs["tipus_lang"] = get_property_tipus('tipus', $rs["tipus"]);
            $rs["marked"] = $marked;
            $rs["class_name"] = $marked?'Down':'Odd';
            $loop[] = $rs;
        }
    }
    $tpl->set_loop('filter_tipus', $loop);
}
/**
 *
 * @access public
 * @return void
 */
function make_menu_comarques()
{
    global $tpl;
    $query = "SELECT Distinct comarca, inmo__comarca.comarca_id
				FROM inmo__comarca, inmo__property
				WHERE inmo__comarca.comarca_id = inmo__property.comarca_id
				AND bin = 0
				ORDER BY comarca";

    $results = Db::get_rows($query);

    if ($results)
    {
        foreach($results as $rs)
        {
            (isset($_GET["comarca_id"])) && ($_GET["comarca_id"] == $rs["comarca_id"])?$marked = 'Selected':$marked = '';

            $rs["marked"] = $marked;
            $loop[] = $rs;
        }
    }
    $tpl->set_loop('menu_comarques', $loop);
}
/**
 *
 * @access public
 * @return void
 */
function make_filter_comarques()
{
    global $tpl;
    $query = "SELECT Distinct comarca, inmo__comarca.comarca_id
				FROM inmo__comarca, inmo__property
				WHERE inmo__comarca.comarca_id = inmo__property.comarca_id
				AND bin = 0
				ORDER BY comarca";

    $results = Db::get_rows($query);

    if ($results)
    {
        $q = &$_GET["comarca_id"];
        foreach($results as $rs)
        {
            isset($q[$rs["comarca_id"]]) && ($q[$rs["comarca_id"]] == $rs["comarca_id"])?$marked = ' checked':$marked = '';

            $rs["marked"] = $marked;
            $rs["class_name"] = $marked?'Down':'Odd';
            $loop[] = $rs;
        }
    }
    $tpl->set_loop('filter_comarques', $loop);
}
/**
 *
 * @access public
 * @return void
 */
function make_filter_municipis()
{
    global $tpl;
    $query = "SELECT Distinct municipi, inmo__municipi.municipi_id
				FROM inmo__municipi, inmo__property
				WHERE inmo__municipi.municipi_id = inmo__property.municipi_id
				AND bin = 0
				ORDER BY municipi";

    $results = Db::get_rows($query);

    if ($results)
    {
        $q = &$_GET["municipi_id"];
        foreach($results as $rs)
        {
            isset($q[$rs["municipi_id"]]) && ($q[$rs["municipi_id"]] == $rs["municipi_id"])?$marked = ' checked':$marked = '';

            $rs["marked"] = $marked;
            $rs["class_name"] = $marked?'Down':'Odd';
            $loop[] = $rs;
        }
    }
    $tpl->set_loop('filter_municipis', $loop);
}
/**
 *
 * @access public
 * @return void
 */
function make_filter_zones()
{
    global $tpl;
    $query = "SELECT Distinct zone, inmo__zone.zone_id
				FROM inmo__zone, inmo__property
				WHERE inmo__zone.zone_id = inmo__property.zone_id
				AND bin = 0
				ORDER BY zone";

    $results = Db::get_rows($query);

    if ($results)
    {
        $q = &$_GET["zone_id"];
        foreach($results as $rs)
        {
            isset($q[$rs["zone_id"]]) && ($q[$rs["zone_id"]] == $rs["zone_id"])?$marked = ' checked':$marked = '';

            $rs["marked"] = $marked;
            $rs["class_name"] = $marked?'Down':'Odd';
            $loop[] = $rs;
        }
    }
    $tpl->set_loop('filter_zones', $loop);
}
/**
 *
 * @access public
 * @return void
 */
function make_menu_categories()
{
    global $tpl, $gl_language;
    $query = "SELECT Distinct category, inmo__category_language.category_id
				FROM inmo__category_language, inmo__property
				WHERE inmo__category_language.category_id = inmo__property.category_id
				AND language = '" . $gl_language . "'
				AND bin = 0
				ORDER BY category";

    $results = Db::get_rows($query);
    Debug::add('Consulta categoris menu', $query);

    if ($results)
    {
        foreach($results as $rs)
        {
            isset($_GET["category_id"]) && ($_GET["category_id"] == $rs["category_id"])?$marked = 'Selected':$marked = '';

            $rs["marked"] = $marked;
            $loop[] = $rs;
        }
    }
    $tpl->set_loop('menu_categories', $loop);
}

/**
 *
 * @access public
 * @return void
 */
function make_filter_categories()
{
    global $tpl, $gl_language;
    $query = "SELECT Distinct category, inmo__category_language.category_id
				FROM inmo__category_language, inmo__property
				WHERE inmo__category_language.category_id = inmo__property.category_id
				AND language = '" . $gl_language . "'
				AND bin = 0
				ORDER BY category";

    $results = Db::get_rows($query);
    Debug::add('Consulta categoris menu', $query);

    if ($results)
    {
        $q = &$_GET["category_id"];
        foreach($results as $rs)
        {
            isset($q[$rs["category_id"]]) && ($q[$rs["category_id"]] == $rs["category_id"])?$marked = ' checked':$marked = '';
            $rs["marked"] = $marked;
            $rs["class_name"] = $marked?'Down':'Odd';
            $loop[] = $rs;
        }
    }
    $tpl->set_loop('filter_categories', $loop);
}

/**
 *
 * @access public
 * @return void
 */
function make_menu_status()
{
    global $tpl;
    $query = "SELECT Distinct status
				FROM inmo__property
				WHERE bin = 0";

    $results = Db::get_rows($query);

    if ($results)
    {
        foreach($results as $rs)
        {
            isset($_GET["status"]) && ($_GET["status"] == $rs["status"])?$marked = 'Selected':$marked = '';

            $rs["status_lang"] = get_property_status('status', $rs["status"]);
            $rs["marked"] = $marked;
            $loop[] = $rs;
        }
    }
    $tpl->set_loop('menu_status', $loop);
}

/**
 *
 * @access public
 * @return void
 */
function make_filter_status()
{
    global $tpl;
    $query = "SELECT Distinct status
				FROM inmo__property
				WHERE bin = 0";

    $results = Db::get_rows($query);

    if ($results)
    {
        foreach($results as $rs)
        {
            $q = &$_GET["status"];
            isset($q[$rs["status"]]) && ($q[$rs["status"]] == $rs["status"])?$marked = 'Checked':$marked = '';

            $rs["status_lang"] = get_property_status('status', $rs["status"]);
            $rs["marked"] = $marked;
            $rs["class_name"] = $marked?'Down':'Odd';
            $loop[] = $rs;
        }
    }
    $tpl->set_loop('filter_status', $loop);
}

/**
 *
 * @access public
 * @return void
 */
function make_menu_prices()
{
    global $tpl;
    $prices = explode(',', INMO_MENU_PRICE);
    if ($prices)
    {
        $conta = 0;
        $previous_price = 0;
        $prices_count = count($prices);
        foreach($prices as $price)
        {
            isset($_GET["price"]) && ($_GET["price"] == $price)?$marked = 'Selected':$marked = '';
            if ($conta == 1)
            {
                $tpl->set_var('menu_first_price', $previous_price);
                $tpl->set_var('menu_second_price', $price);
                $tpl->set_var('menu_first_price_marked', isset($_GET["price1"]) && ($_GET["price1"] == $previous_price)?'Selected':'');
                // $rs["marked"] = $marked;
            } elseif ($conta > 1)
            {
                $loop[] = array("price" => $previous_price,
                    "marked" => isset($_GET["price1"]) && ($_GET["price1"] == $previous_price)?'Selected':'',
                    "next_price" => $price);
            }
            $conta++;
            $previous_price = $price;
        }
        $tpl->set_var('menu_before_last_price', $price);
        $tpl->set_var('menu_last_price_marked', isset($_GET["price1"]) && ($_GET["price1"] == $price)?'Selected':'');
    }
    $tpl->set_loop('menu_prices', $loop);
}

function make_filter_others()
{
    global $tpl;
    $vars = array ('price1','price2','floor_space1','floor_space2','land1','land2');

    foreach($vars as $var){
	    $val = isset($_GET[$var])?$_GET[$var]:'';
	    $tpl->set_var($var, $val);
	    $tpl->set_var($var . '_class', $val?'Down':'Odd');
    }
    // land_units
    $tpl->set_var('land_units2', (isset($_GET['images']) && $_GET['land_units']=='ha')?' selected':'');

	// filter_images
    $marked = (isset($_GET['images']) && $_GET['images'] == '1')?' Checked':'';
	$tpl->set_var('has_images_class', $marked?'Down':'Odd');
	$tpl->set_var('has_images', $marked);
    $marked = (isset($_GET['images']) && $_GET['images'] == '0')?' Checked':'';
	$tpl->set_var('has_no_images_class', $marked?'Down':'Odd');
    $tpl->set_var('has_no_images', $marked);

	// filter_tipus2
    $marked = (isset($_GET['tipus2']) && $_GET['tipus2'] == 'new')?' Checked':'';
	$tpl->set_var('new_class_name', $marked?'Down':'Odd');
	$tpl->set_var('new_marked', $marked);
    $marked = (isset($_GET['tipus2']) && $_GET['tipus2'] == 'second')?' Checked':'';
	$tpl->set_var('second_class_name', $marked?'Down':'Odd');
	$tpl->set_var('second_marked', $marked);
}

/**
 *
 * @access public
 * @return void
 */
function make_menu_images()
{
    global $tpl;
    // marca el menu d'imatges 1=primer menu, 0=segon menu
    // pero si no està definida la variable sempre ho posa no marcat, per que vol dir que estem en un altre menu
    $tpl->set_var('menu_first_images_marked', isset($_GET["images"]) && $_GET["images"]?'Selected':'');
    $tpl->set_var('menu_second_images_marked', isset($_GET["images"]) && !$_GET["images"]?'Selected':'');
}

function make_filter_order()
{
    global $gl_db_classes_fields, $gl_caption, $tpl;
    for ($i = 0; $i < 5; $i++)
    {
        $rs['select'] = '<select name="order_field[' . $i . ']"><option value=""></option>';
        foreach($gl_db_classes_fields as $key => $field)
        {
            if ($field['list_admin'] == 'text' || $field['list_admin'] == 'input')
            {
	            $selected = (isset($_GET['order_field'][$i]) && $_GET['order_field'][$i]==$key)?' selected':'';
                $rs['select'] .= ' <option value="' . $key . '"' . $selected . '>' . $gl_caption['c_' . $key] . '</option>';
            }
        }
        $rs['select'] .= '</select>';
        $rs['conta'] = $i + 1;
        $marked = (isset($_GET['order_way'][$i]) && $_GET['order_way'][$i]=='desc')?' checked':'';
        $rs['desc_class_name'] = $marked?'Down':'Odd';
        $rs['desc'] = '<input type="radio" name="order_way[' . $i . ']" value="desc"' . $marked . ' onclick="return false" />';
        $marked = !$marked?' checked':'';
        $rs['asc_class_name'] = $marked?'Down':'Odd';
        $rs['asc'] = '<input type="radio" name="order_way[' . $i . ']" value="asc"' . $marked . ' onclick="return false" />';
        $loop[] = $rs;
    }
    $tpl->set_loop('filter_order', $loop);
}

/**
 *
 * @access public
 * @return void
 */
function make_filter_description()
{
    global $tpl;
    $query = "SELECT language, code
				FROM all__language
				WHERE public=1 OR public=2
				ORDER BY ordre";

    $results = Db::get_rows($query);
    if ($results)
    {
        foreach($results as $rs)
        {
            isset($_GET['description'][$rs["code"]]) && ($_GET['description'][$rs["code"]] == $rs["code"])?$marked =' checked':$marked='';

            $rs['marked'] = $marked;
            $rs["class_name"] = $marked?'Down':'Odd';
            $loop[] = $rs;
        }
    }
    $tpl->set_loop('filter_description', $loop);
}
/**
 *
 * @access public
 * @return void
 */
function make_filter_user()
{
    global $tpl;
    $query = "SELECT name, surname, user_id
				FROM user__user
				WHERE group_id != 1
				AND bin = 0
				ORDER BY name";

    $results = Db::get_rows($query);

    if ($results)
    {
        $q = &$_GET["user_id"];
        foreach($results as $rs)
        {
            isset($q[$rs["user_id"]]) && ($q[$rs["user_id"]] == $rs["user_id"])?$marked = ' checked':$marked = '';

            $rs["marked"] = $marked;
            $rs["class_name"] = $marked?'Down':'Odd';
            $loop[] = $rs;
        }
    }
    $tpl->set_loop('filter_users', $loop);
}
/**
 *
 * @access public
 * @return void
 */
function make_filter_custom()
{
    
    global $gl_config, $tpl;
		// defineixo els tipus de custom que hi han
	
	$loop = array();
	
	for ($i = 1; $i <= 4; $i++) {
		$l = array();
		if ($gl_config['custom'.$i.'_name']) {
			$l['custom_id'] = $custom_id = 'custom'.$i;
			$l['name'] = $gl_config['custom'.$i.'_name'];
			
			$l["class_name"] = 'Odd';
			
			
			if ($gl_config['custom'.$i.'_type'] == 'text') {
				$text = isset($_GET[$custom_id])?$_GET[$custom_id]:'';
				$l['input'] = 
						'<input name="'.$custom_id.'" type="text" size="10" style="text-align: right" value="'.
						$text.'" onblur="click_input(this, \'blur\',\'\')" />';
			}
			elseif ($gl_config['custom'.$i.'_type'] == 'int') {
				$text = isset($_GET[$custom_id])?$_GET[$custom_id]:'';
				$l['input'] = 
						'<input name="'.$custom_id.'" type="text" size="10" style="text-align: right" value="'.
						$text.'" onblur="click_input(this, \'blur\',\'\')" />';
			}
			elseif ($gl_config['custom'.$i.'_type'] == 'checkbox') {
				$q = &$_GET["custom_id"];
				isset($q[$custom_id]) && ($q[$custom_id] == $custom_id)?$marked = ' checked':$marked = '';
				$l["class_name"] = $marked?'Down':'Odd';
				
				$l['input'] = 
						'<input type="checkbox" name="custom_id['.
						$custom_id.']" value="'.
						$custom_id.'" '.
						$marked.' onclick="return false" />';
			}
			$loop[] = $l;
		}
	}
	
    $tpl->set_loop('filter_customs', $loop);
}

/**
 *
 * @access public
 * @return void
 */
function search_list_condition(&$listing)
{
    global $gl_page;
    // consultes menus
    $search_condition = '';
    if (isset($_GET['comarca_id']))
    {
        $search_condition = "comarca_id = " . R::id('comarca_id');
        $gl_page->title = TITLE_COMARCA_ADMIN . get_property_comarca(R::id('comarca_id'));
    }
    if (isset($_GET['category_id']))
    {
        $search_condition = "category_id = " . R::id('category_id');
        $gl_page->title = get_property_category(R::id('category_id'));
    }
    if (isset($_GET['status']))
    {
        $search_condition = "status = '" . R::escape('status') . "'";
        $gl_page->title = get_property_status('status', R::escape('status'));
    }
    if (isset($_GET['price1']))
    {
        if (isset($_GET['price2']))
        {
            $search_condition = "(price BETWEEN " . R::escape('price1') . " AND " . R::escape('price2') . ")";
            if ($_GET['price1'] == 0)
            {
                $gl_page->title = sprintf(TITLE_PRICE, R::escape('price2'));
            }
            else
            {
                $gl_page->title = sprintf(TITLE_PRICE_2, R::escape('price1'), R::escape('price2'));
            }
        }
        else
        {
            $search_condition = "price > " . R::escape('price1');
            $gl_page->title = sprintf(TITLE_PRICE_3, R::escape('price1'));
        }
        $listing->order_by = 'price asc, ref desc, entered desc';
    }
    if (isset($_GET['images']))
    {
        if ($_GET['images'])
        {
            $listing->join = ",inmo__property_image";
            $search_condition = 'inmo__property.property_id = inmo__property_image.property_id';
            $listing->change_sql_id_field = "DISTINCT inmo__property.property_id";
            $gl_page->title = get_property_status('status', R::escape('status'));
        }
        else
        {
            $search_condition .= "inmo__property.property_id IN (SELECT inmo__property.property_id
			FROM inmo__property
			LEFT OUTER JOIN inmo__property_image USING (property_id)
			WHERE image_id is null)";
            $gl_page->title = get_property_status('status', R::escape('status'));
        }
    }

    return $search_condition;
}

/**
 *
 * @access public
 * @return string
 *
 * Es pot posar un hidden o checkbox per buscar només en referencies <input type="hidden" value="1" name="ref_search">
 * O es pot buscar nomes per referencies canviant q per qref
 *
 * TODO-i Passar tots els searchs a process enlloc de action
 */
function search_search_condition($listing)
{
    global $gl_page, $gl_site_part, $gl_language;
	$q = R::escape('q');
    $ref_search =  R::escape('ref_search'); // per buscar només per referència
    $qref =  R::escape('qref'); // per buscar només per referència

    // ref funciona si es substitueix per q nomes ( així em reservo de poder posar-ho com a camp de text a més del q )
    if ($qref && !$q){
        $q = $qref;
        $ref_search =  true;
    }


    $is_admin = $gl_site_part == 'admin';
    // MIRAR SI LA CADENA Q ES UNA REFERENCIA O VARIES SEPARADES PER ESPAIS
    // si es una referencia en la busqueda per paraules fem busqueda per referencia
    // ''''1 caracter+numero''' o '''numero'''

    if (!$q) return '';

    // Busco dins la fitxa per idiomes
    // else
    if ( ! $ref_search ) {

        $search_condition = "(
									 floor_space like '%" . $q . "%'
									 OR land like '%" . $q . "%'
									 OR price like '%" . $q . "%'
									 OR ref like '%" . $q . "%'
										";


        $query = "SELECT property_id FROM inmo__property_language WHERE (
									 description like '%" . $q . "%' OR
									 property like '%" . $q . "%' OR
									 property_title like '%" . $q . "%'
										)";
        if ( ! $is_admin )
            $query .= " AND language = '" . $gl_language . "'";
        $results = Db::get_rows( $query );
        $search_condition .= search_search_get_ids( $results, 'property_id' );

        if ( $is_admin ) {
            $query   = "SELECT property_id FROM custumer__custumer_to_property INNER JOIN custumer__custumer USING(custumer_id) WHERE (concat(name,' ', surname1, ' ', surname2) like '%" . $q . "%')";
            $results = Db::get_rows( $query );
            $search_condition .= search_search_get_ids( $results, 'property_id' );
        }

        $query = "SELECT category_id FROM inmo__category_language WHERE category like '%" . $q . "%' ";
        if ( ! $is_admin )
            $query .= " AND language = '" . $gl_language . "'";
        $results = Db::get_rows( $query );
        $search_condition .= search_search_get_ids( $results, 'category_id' );

        $query   = "SELECT zone_id FROM inmo__zone WHERE zone like '%" . $q . "%' ";
        $results = Db::get_rows( $query );
        $search_condition .= search_search_get_ids( $results, 'zone_id' );

        $query   = "SELECT comarca_id FROM inmo__comarca WHERE comarca like '%" . $q . "%' ";
        $results = Db::get_rows( $query );
        $search_condition .= search_search_get_ids( $results, 'comarca_id' );

        $query   = "SELECT provincia_id FROM inmo__provincia WHERE provincia like '%" . $q . "%' ";
        $results = Db::get_rows( $query );
        $search_condition .= search_search_get_ids( $results, 'provincia_id' );
        // REVISAR municipi ha de ser una opcio que surti a public o no
        if ( $is_admin ) {
            $search_condition .= "OR property_private like '%" . $q . "%'
									 OR observations like '%" . $q . "%'
									 OR adress like '%" . $q . "%'
										";
            $query   = "SELECT municipi_id FROM inmo__municipi WHERE municipi like '%" . $q . "%' ";
            $results = Db::get_rows( $query );
            $search_condition .= search_search_get_ids( $results, 'municipi_id' );
        }

        $search_condition .= ") ";
    }
    // Per si la referencia en  si porta espais o comes, s'ha de buscar la cadena complerta també
    else {
        $search_condition = "(	ref like '%" . $q . "%'  )";
    }

    //
    // busqueda automatica de referencies separades per espais o comes
    //
    $ref_array = explode( ",", $q ); // si l'array hem dona 1 probo de fer-ho amb espais, si es una sola referncia donarà el mateix resultat
    //if (INMO_PROPERTY_AUTO_REF == 1)
    count( $ref_array ) == 1 ? $ref_array = explode( " ", $q ) : false;

    foreach ( $ref_array as $key => $value ) {
        if ( is_numeric( substr( $value, 1 ) ) || is_numeric( $value ) ) {
            $ref_array[ $key ] = is_numeric( $value ) ? $value : substr( $value, 1 );
        } elseif ( INMO_PROPERTY_AUTO_REF == 1 ) {
            $ref_array = '';
            break;
        }
    }
    // consultes menus
    if ( $ref_array ) {
        $query_ref = "";
        foreach ( $ref_array as $key ) {
            $query_ref .= "ref = '" . $key . "' OR ";
        }
        $query_ref = "(" . substr( $query_ref, 0, - 3 ) . ") ";

        $search_condition = '( ' . $query_ref . ' OR ' . $search_condition . ')'; // per afegir tota la consulta de sota
    }

    $gl_page->title = TITLE_SEARCH . '<strong>&nbsp;&nbsp;"' . $q . '"</strong>';
   
    return $search_condition;
}
function search_search_get_ids(&$results, $field)
{
    if (!$results) return '';
    return "OR inmo__property." . $field . " IN (" . implode_field($results, $field) . ") ";
}
/**
 *
 * @access public
 * @return void
 */
function search_filter_condition(&$listing)
{
    global $gl_page, $gl_config;

    $search_condition = '';
    // consultes menus
    if (isset($_GET['tipus']))
    {
        $search_condition .= " AND (";
        foreach($_GET['tipus'] as $tipus)
        {
            $search_condition .= "tipus = '" . R::escape($tipus,false,false) . "' OR ";
        }
        $search_condition = substr($search_condition, 0, -4);
        $search_condition .= " )";
        //$gl_page->title = get_property_tipus('',  $tipus);
    }
    if (isset($_GET['tipus2']))
    {
        $search_condition .= " AND tipus2='" . R::escape('tipus2') . "'";
        //$gl_page->title = get_property_status('status', $_GET['status']);
    }
    if (isset($_GET['comarca_id']))
    {
        $search_condition .= " AND (";
        foreach($_GET['comarca_id'] as $comarca_id)
        {
            $search_condition .= "comarca_id = " . R::escape($comarca_id,false,false) . " OR ";
        }
        $search_condition = substr($search_condition, 0, -4);
        $search_condition .= " )";
        //$gl_page->title = TITLE_COMARCA_ADMIN . get_property_comarca($comarca_id);
    }
    if (isset($_GET['municipi_id']))
    {
        $search_condition .= " AND (";
        foreach($_GET['municipi_id'] as $municipi_id)
        {
            $search_condition .= "municipi_id = " . R::escape($municipi_id,false,false) . " OR ";
        }
        $search_condition = substr($search_condition, 0, -4);
        $search_condition .= " )";
        //$gl_page->title = TITLE_COMARCA_ADMIN . get_property_comarca($municipi_id);
    }
    if (isset($_GET['zone_id']))
    {
        $search_condition .= " AND (";
        foreach($_GET['zone_id'] as $zone_id)
        {
            $search_condition .= "zone_id = " . R::escape($zone_id,false,false) . " OR ";
        }
        $search_condition = substr($search_condition, 0, -4);
        $search_condition .= " )";
        //$gl_page->title = TITLE_COMARCA_ADMIN . get_property_comarca($zone_id);
    }
    if (isset($_GET['category_id']))
    {
        $search_condition .= " AND (";
        foreach($_GET['category_id'] as $category_id)
        {
            $search_condition .= "category_id = " . R::escape($category_id,false,false) . " OR ";
        }
        $search_condition = substr($search_condition, 0, -4);
        $search_condition .= " )";
        //$gl_page->title = get_property_category($category_id);
    }
    if (isset($_GET['status']))
    {
        $search_condition .= " AND (";
        foreach($_GET['status'] as $status)
        {
            $search_condition .= "status = '" . R::escape($status,false,false) . "' OR ";
        }
        $search_condition = substr($search_condition, 0, -4);
        $search_condition .= " )";
        //$gl_page->title = get_property_status('status', $_GET['status']);
    }
    if ($_GET['price1'] || $_GET['price2'])
    {
        $p1 = unformat_int($_GET['price1'], true);
        $p2 = unformat_int($_GET['price2'], true);
        if ($p1 == '0' || $p1 == '')
        {
            $search_condition .= " AND (price <= " . $p2 . ")";
        } elseif ($p2 == '0' || $p2 == '')
        {
            $search_condition .= " AND (price >= " . $p1 . ")";
        }
        else
        {
            $search_condition .= " AND (price BETWEEN " . $p1 . " AND " . $p2 . ")";
        }
    }
    if (isset($_GET['user_id']))
    {
        $search_condition .= " AND (";
        foreach($_GET['user_id'] as $user_id)
        {
            $search_condition .= "user_id = " . R::escape($user_id,false,false) . " OR ";
        }
        $search_condition = substr($search_condition, 0, -4);
        $search_condition .= " )";
    }
	
	$custom_search = array();
	for ($i = 1; $i <= 4; $i++) {
		$custom_id = 'custom'. $i;
		// checkbox
		if (isset($_GET["custom_id"][$custom_id])){
			$custom_search[]= "inmo__property.".$custom_id."='1'";			
			
		}
		elseif (isset($_GET[$custom_id]) && $_GET[$custom_id]!='')
		{
			// busco a les una de les 2 taules per si es text o int
			$table = ($gl_config['custom'.$i.'_type']=='text')?'inmo__property_language':'inmo__property';
			$custom_search[]= $table.".".$custom_id."='".$_GET[$custom_id]."'";
		}
	}
	if ($custom_search) $search_condition .= " AND (".  implode(' OR ', $custom_search).")";
	
    if (isset($_GET['images']))
    {		
        if ($_GET['images'] == '1')
        {
            $listing->join = ",inmo__property_image";
            $search_condition .= ' AND inmo__property.property_id = inmo__property_image.property_id';
            $listing->change_sql_id_field = "DISTINCT inmo__property.property_id";
        } elseif ($_GET['images'] == '0')
        {
            $search_condition .= " AND inmo__property.property_id IN (SELECT inmo__property.property_id
			FROM inmo__property
			LEFT OUTER JOIN inmo__property_image USING (property_id)
			WHERE image_id is null)";
        }
    }
    if ($_GET['floor_space1'] || $_GET['floor_space2'])
    {
        $p1 = unformat_int($_GET['floor_space1'],true);
        $p2 = unformat_int($_GET['floor_space2'],true);
        if ($p1 == '0' || $p1 == '')
        {
            $search_condition .= " AND (floor_space <= " . $p2 . ")";
        } elseif ($p2 == '0' || $p2 == '')
        {
            $search_condition .= " AND (floor_space >= " . $p1 . ")";
        }
        else
        {
            $search_condition .= " AND (floor_space BETWEEN " . $p1 . " AND " . $p2 . ")";
        }
    }
    if ($_GET['land1'] || $_GET['land2'])
    {
        if ($_GET['land_units']=='ha') {
	        $p1 = unformat_int($_GET['land1']*10000,true);
	        $p2 = unformat_int($_GET['land2']*10000,true);
        }
        else{
	        $p1 = unformat_int($_GET['land1'],true);
	        $p2 = unformat_int($_GET['land2'],true);
        }
		
        if ($p1 == '0' || $p1 == '')
        {
            $search_condition .= " AND (land <= " . R::escape($p2,false,false) . ")";
        } elseif ($p2 == '0' || $p2 == '')
        {
            $search_condition .= " AND (land >= " . R::escape($p1,false,false) . ")";
        }
        else
        {
            $search_condition .= " AND (land BETWEEN " . $p1 . " AND " . $p2 . ")";
        }
    }
    if ($_GET['order_field'])
    {
        $order_by = '';
        foreach ($_GET['order_field'] as $key => $order_field)
        {
            if ($order_field != '')
            {
                $order_by .= $order_field . ' ' . R::escape($_GET['order_way'][$key],false,false) . " ,";
            }
        }
        $order_by = substr($order_by, 0, -1);
        $listing->order_by = $order_by ;
    }
    if (isset($_GET['description'])) {
    $search_condition_lang = '';
	    foreach($_GET['description'] as $lang){
	    	$search_condition_lang .= "(language = '" . R::escape($lang,false,false) . "' AND description = '') OR ";
	    }
	    $search_condition_lang = substr($search_condition_lang, 0, -4);
        $search_condition .= " AND (inmo__property.property_id IN (
									SELECT property_id
									FROM  inmo__property_language
									WHERE " . $search_condition_lang . "
									))";
    }
    if ($search_condition) $search_condition = substr($search_condition, 5);
    return $search_condition;
}
function search_price_condition(&$listing, $tipus, $and = 'AND') {
	
	global $gl_page;
	$price1 = R::number('price1',0);
	$price2 = R::number('price2');
	$prices = R::text_id('prices');
	
	$condition = '';

	if ($prices){
		$prices = explode('-',$prices);
		$price1 = $prices[0];
		$price2 = $prices[1];
	}
	
	if (!$price1 && !$price2) return '';
	

	
	if ($tipus == 'temp') {

		$listing->order_by = str_replace('price ', 'price_tmp ', $listing->order_by);
		
		$results = Db::get_rows("SELECT property_id,price FROM inmo__property WHERE " . $listing->condition);
		Debug::p($listing->condition, 'max_results');
		//$conta = 0;
		$ids = array();
		foreach ($results as $key => &$rs){
			
			extract($rs);
			Main::load_class('booking', 'common', 'admin');
			$price_arr = BookingCommon::get_property_price($property_id, $price, true);
			$price = $price_arr['price'];
			
			if ($price2 && ($price>=$price1 && $price<=$price2)){
				$ids []= $property_id;
			}
			elseif ($price<$price1){
				$ids []= $property_id;
			}
			
			// Necessito tots els resultats per poder paginar
			//$conta++;
			//if ($conta == $listing->config['max_results']) break;
		}
		
		if ($ids) {
			$condition = ' ' . $and . ' property_id IN (' . implode(',',$ids) . ')';
		}
		else{
			$condition = ' ' . $and . ' property_id IN (0)';			
		}
		
		Debug::p($results, 'Query preu temp');
		Debug::p($condition, 'Query preu temp');
		return $condition;
	}
	
	

	if ($price2)
	{
		$condition .= " AND (price BETWEEN " . $price1 . " AND " . $price2 . ")";
		if ($price1 == 0) 					{
			$condition .= " AND (price <>0)";
			$gl_page->title = sprintf(TITLE_PRICE, $price2);
		} 					else 					{
			$gl_page->title = sprintf(TITLE_PRICE_2, $price1, $price2);
		}
	} 				
	else
	{
		$condition .= " AND price > " . $price1;
		$gl_page->title = sprintf(TITLE_PRICE_3, $price1);
	}
	$condition .= " AND price_consult = 0";
	
	return $condition;
}

function search_book_condition(&$listing, $and = 'AND') {
	
	
	$adult = R::escape('adult');
	$child = R::escape('child');
	$baby = R::escape('baby');
	$person = (int)$adult + (int)$child;
	$date_in = R::escape('date_in');
	$date_out = R::escape('date_out');
	
	if (!$date_in || !$date_out) return false;
	
	$date_in = unformat_date($date_in);
	$date_out = unformat_date($date_out);	
	

	
	// Buscar propietats que estigui assignada una temporada per a cada un dels dies que es vol fer reserva
	$begin = new DateTime($date_in);
	$end = new DateTime($date_out);

	$interval = DateInterval::createFromDateString('1 day');

    $period = new DatePeriod($begin, $interval, $end);
    $total_nights = iterator_count($period);

	Main::load_class('booking', 'book', 'public');
	Main::load_class('booking', 'price', 'admin');
	$price_module = New BookingPrice($this);

	
	// busco totes les propietats que cumpleixen les condicions:
	//		- lloguer temp
	//		- estan online
	//      - max persones
	
	$results = Db::get_rows(
			
		"SELECT property_id 
			FROM inmo__property
			WHERE tipus = 'temp'		
			AND (status='onsale')
			AND person >= " . $person
			);
	
	/*
		- que no tenen cap reserva feta entre les dates
		- cada un dels dies tingui assignat temporada
			no puc saber si tenen tots els dies temporada si no faig un loop dia a dia, ja que pot abarcar més d'una temporada i propietat a propietat
		- cumpleixi el mínim de nits
		- si està marcat setmana sencera
		- si cumpleix el dia de la setmana que comença
	*/
	
	$ids = '';
	$other_ids = '';
	//Debug::p($results, 'text');
	foreach ($results as $rs){
		
		$property_id = $rs['property_id'];
		// miro que no estigui reservat	
		if (!Db::get_first("
				SELECT count(*) FROM booking__book 
					WHERE '" . $date_in . "' < date_out
					AND '" . $date_out . "' > date_in
					AND ( status = 'booked' || status = 'confirmed_email' || status = 'confirmed_post' || status = 'contract_signed')
					AND property_id = " . $property_id
			))
		{
			




            $period = new DatePeriod($begin, $interval, $end);
			// miro que cada un dels dies tingui temporada
			foreach ( $period as $dt ){

				$date = $dt->format( "Y-m-d" );
				$query = "
					SELECT property_id
						FROM booking__propertyrange
						WHERE '$date' >= date_start 
						AND '$date' < date_end
						#AND minimum_nights <= " . $total_nights . "
						AND property_id = " . $property_id;
				//Debug::p($query, 'query range');
				$is_ok = Db::get_first($query);
				
				if (!$is_ok){
					break;
				}
			}

			// obtinc preu per comprobar setmanes senceres, ( el dia que comença no va aquí )
			if ( $is_ok ) {

				$other_ids .= $property_id . ',';

				$prices = $price_module->get_price(
						$date_in,
						$date_out,
						0,
						$property_id,
						false,
						false,
						$adult,
						$child
				);
				if ( $prices['error'] ) {
					$is_ok = false;
				}
			}

			if ($is_ok) $ids .= $property_id . ',';
		}

	}
		
	
	//Debug::p($ids, 'text');
	
	// No ha d'estar feta reserva
	// Ha d'estar assignat preu temporada
	
	if ($ids){
		$ids = substr($ids, 0, -1);
	}
	else{
		if ( $other_ids ) {
			$GLOBALS['gl_message'] = $GLOBALS['gl_caption']['c_booking_close_results_message'];
			$ids =  substr($other_ids, 0, -1);
		}
		else {
			$ids = 0;

		}
	}
		
	
	$condition = " " . $and . "
		property_id IN (
			" . $ids . "
		)";
	
	return $condition;
}
function search_details_condition(&$listing, $details) {
	
	
	$condition = '';
	
	foreach ($details as $detail){
		$detail_value = R::escape($detail);
		
		if ($detail_value) {
			$is_min = R::id($detail . '_is_min');

			// is_min per defecte: person
			if ($detail == 'person') $is_min = true;

			$sign = $is_min?'>=':'=';
			
			// si es custom, busco que el valor no sigui '' - De moment nomes es er amarres de bravahomestanding
			if (strpos($detail,'custom')!== false)					
			{
				$type = $listing->config["detail_type"];
				$change_field = $listing->fields[$detail]['change_field_name'];
				if (strpos($change_field,'inmo__property_language')!== false) 
				{
					$detail = 'inmo__property_language.' . $detail;
				}
				else {
					$detail = 'inmo__property.' . $detail;
				}

				if ($type=='text') {
					$condition .= " AND ($detail <> '')";
				}
				else {
					$condition .= " AND ($detail <> '') AND ($detail <> '0')";
				}
			}
			else {
				$condition .= " AND (".$detail." ".$sign." '" . $detail_value . "')";
			}
			

		}
		
	}
	
	return $condition;
	
}

?>