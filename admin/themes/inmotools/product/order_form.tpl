<script language="JavaScript" src="/admin/modules/product/jscripts/functions.js?v=<?=$version?>"></script>
<script language="JavaScript" src="/admin/jscripts/validacio.js?v=<?=$version?>"></script>

<script type="text/javascript">

	$(function() {
		order.prepared_emailed = '<?=$prepared_emailed?>';
		order.shipped_emailed = '<?=$shipped_emailed?>';
		order.delivered_emailed = '<?=$delivered_emailed?>';
		order.c_sending_email = <?=json_encode($c_sending_email)?>;
		order.c_email_sended_correctly = <?=json_encode($c_email_sended_correctly)?>;
		order.c_mail_sent = <?=json_encode($c_mail_sent)?>;
		order.customer_language = '<?=$customer_language?>';
		order.init();
	});

</script>



<form name="form_delete" method="post" action="<?=$link_action_delete?>" target="save_frame">
	<input name="<?=$id_field?>" type="hidden" value="<?=$id?>">
</form><form id="theForm" name="theForm" method="post" action="<?=$link_action?>" onsubmit="<?=$js_string?>"
      encType="multipart/form-data" target="save_frame">
<input name="order_id[<?=$id?>]" type="hidden" value="<?=$id?>" />
<input name="order_id_javascript" type="hidden" value="<?=$id?>" />
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="form">
<tr>
	<td><?if ($form_new):?><?if ($save_button):?>
		<div id="form_buttons_holder"><table id ="form_buttons">
			<tr>
			<td class="buttons_top"><div class="right"><input type="submit" name="Submit" value="<?=$c_send_new?>" class="btn_form1" /></div></td>
			</tr>
		</table></div>
		<?endif //save_button?>
		<?endif //form_new?>
		<?if ($form_edit):?>
		<div id="form_buttons_holder"><table id ="form_buttons">
			<tr>
			<td valign="bottom"><div class="menu"><div><?=$c_edit_data?></div><?if ($has_images):?>
				<a href="<?=$image_link?>"><?=$c_edit_images?></a>
		<?endif //has_images?></div></td>
			<td class="buttons_top"><div class="right">
			<? if ($save_button):?><input type="submit" name="Submit" value="<?=$c_send_edit?>" class="btn_form1"><div class="btn_sep"></div><? endif //save_button?><? if ($delete_button):?><a class="btn_form_delete" href="javascript:submit_form('<?if ($has_bin):?><?=$c_confirm_form_bin?><?else:?><?=$c_confirm_form_deleted?><?endif //has_bin?>', true);">
				<?=$c_delete?>
				</a><? endif //delete_button?><? if ($preview_button):?><a class="btn_form1" href="<?=$preview_link?>" target="_blank">
				<?=$c_preview?>
				</a><? endif //preview_button?><? if ($print_button):?><a class="btn_form1 btn_print" href="<?=$print_link?>">
				<?=$c_print?>
				</a> <? endif //print_button?></div></td>
			</tr>
		</table></div>
		<?endif //form_edit?></td>
</tr>
<tr>
	<td><table width="100%" cellpadding="0" cellspacing="0">
	<tr>
		<td class="formCaptionTitle"><strong>
		<?=$c_order_title_admin?></strong>&nbsp;
		<?=$c_order_num?><?=$id?>
		&nbsp;</td>
	</tr>
	</table>
	<table width="100%" cellpadding="0" cellspacing="0" class="formstbl">
			<tr>
				<td width="150" class="formsCaption" valign="top"><strong>
				<?=$c_entered_order?>:</strong></td>
				<td valign="top" class="forms">
					<span class="padding-right-4"><?=$entered_order?></span>
				<span id="prepared_emailed" class="hidden vert"><?= $c_mail_sent ?>: <?= $prepared_emailed ?></span>
				<a id="prepared_mail" class="hidden" href="javascript:order.send_mail('prepared')"><?= $c_send_mail_prepared ?></a>
				<a id="prepared_mail_re" class="hidden padding-left-2" href="javascript:order.send_mail('prepared')"><?= $c_resend_mail ?></a></td>
			</tr>
			<tr>
				<td width="150" class="formsCaption" valign="top"><strong>
				<?=$c_order_status?>:</strong></td>
				<td valign="top" class="forms"><?=$order_status?></td>
			</tr>
			<tr>
				<td width="150" class="formsCaption" valign="top"><strong>
				<?=$c_shipped?>:</strong></td>
			<td valign="top" class="forms">
				<span class="padding-right-4"><?=$shipped?></span>
				<span id="shipped_emailed" class="hidden vert"><?= $c_mail_sent ?>: <?= $shipped_emailed ?></span>
				<a id="shipped_mail" class="hidden" href="javascript:order.send_mail('shipped')"><?= $c_send_mail_shipped ?></a>
				<a id="shipped_mail_re" class="hidden padding-left-2" href="javascript:order.send_mail('shipped')"><?= $c_resend_mail ?></a>
			</td>
			</tr>
			<tr>
				<td width="150" class="formsCaption" valign="top"><strong>
				<?=$c_delivered?>:</strong></td>
				<td valign="top" class="forms">
					<span class="padding-right-4"><?=$delivered?></span>
					<span id="delivered_emailed" class="hidden vert"><?= $c_mail_sent ?>: <?= $delivered_emailed ?></span>
					<a id="delivered_mail" class="hidden" href="javascript:order.send_mail('delivered')"><?= $c_send_mail_delivered ?></a>
					<a id="delivered_mail_re" class="hidden padding-left-2" href="javascript:order.send_mail('delivered')"><?= $c_resend_mail ?></a></td>
			</tr>
			<tr>
				<td valign="top" class="formsCaption"><strong>
				<?=$c_courier_id?>: </strong></td>
				<td class="forms">
					<table border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td class="padding-right-2 middle" style="vertical-align:middle;padding-right:20px;"><?=$courier_id?></td>
							<td class="padding-right-1 middle" style="vertical-align:middle;padding-right:10px;"><?= $c_courier_follow_code ?> </td>
							<td style="vertical-align:middle;"><?= $courier_follow_code ?></td>
						</tr>
					</table>




				</td>
			</tr>
			<tr>
				<td valign="top" class="formsCaption"><strong>
				<?=$c_comment?>: </strong></td>
				<td class="forms"><?=$comment?></td>
			</tr>
		<? if( $customer_comment ): ?>
			<tr>
				<td valign="top" class="formsCaption"><strong>
						<?= $c_customer_comment ?>: </strong></td>
				<td class="forms"><?= $customer_comment ?></td>
			</tr>
		<? endif // $customer_comment ?>
			<tr>
				<td valign="top" class="formsCaption"><strong>
				<?=$c_file?>: </strong></td>
				<td class="forms"><?=$file?></td>
			</tr>
			</table>
	<? if($show_account):?>
	<table width="100%" cellpadding="4" cellspacing="0">
		<tr>
		<td class="formCaptionTitle"><div class="title">
			<strong>
			<?=$c_account_title?>
		</strong></div></td>
		</tr>
	</table>
	<table  class="formstbl">
		<tr>
		<td width="150" valign="top"><strong>
			<?=$banc_name?>:</strong></td>
		<td valign="top"><?=$account_number?></td>
		</tr>
		<tr>
		<td width="150" valign="top"><strong>
			<?=$c_account_amount?>:</strong></td>
		<td valign="top"><?=$total?>
			<?=CURRENCY_NAME?></td>
		</tr>
		<tr>
		<td width="150" valign="top"><strong>
			<?=$c_account_concept?>:</strong></td>
		<td valign="top"><?=$concept?></td>
		</tr>
	</table>
	<? endif // show_account?>
	<div class="sep_dark"></div>
	<table width="100%" cellpadding="4" cellspacing="0">
		<tr>
		<td class="formCaptionTitle"><strong>
			<?=$c_order_title2?>
		</strong></td>
		</tr>
	</table>
	<table width="100%" border="0" cellpadding="0" cellspacing="0" class="formstbl">
		<tr>
			<td style="padding-top:18px;padding-bottom:18px;"><?=$orderitems?></td>
		</tr>
		</table>
	<div class="sep_dark"></div>
	<table width="100%" cellpadding="4" cellspacing="0">
		<tr>
		<td class="formCaptionTitle"><strong>
			<?=$c_order_title3?>
		</strong></td>
		</tr>
	</table>

	<table width="100%" border="0" cellpadding="8" cellspacing="0" class="formstbl">
		<tr>
			<td style="padding-top:18px;padding-bottom:18px;">

				<?if($invoice_id_value != '0' && $invoice_id_value != '-1'):?>
				<p>
					<strong><?=$c_invoice_id?>:</strong> <?=$invoice_id?><br />
					<strong><?=$c_invoice_date?>:</strong> <?=$invoice_date?><br />
				</p>
				<?endif //invoice_id?>

				<?if($invoice_refund_id_value):?>
				<p>
					<strong><?=$c_invoice_refund_id?>:</strong> <?=$invoice_refund_id?><br />
					<strong><?=$c_invoice_refund_date?>:</strong> <?=$invoice_refund_date?><br />
				</p>
				<?endif //invoice_id?>

				<?=$address_invoice?>

				<?if($invoice_id_value != '0' && $invoice_id_value != '-1'):?>
				<p><a href="javascript:customer_show_invoice('<?=$order_id?>');" class="boto1"><?=$c_view_invoice?></a></p>
				<?endif //invoice_id?>
				<?if($invoice_refund_id_value):?>
				<p><a href="javascript:customer_show_invoice_refund('<?=$order_id?>');" class="boto1"><?=$c_view_invoice_refund?></a></p>
				<?endif //invoice_id?>
			</td>
		</tr>
	</table>

	<div class="sep_dark"></div>
	<table width="100%" cellpadding="4" cellspacing="0">
		<tr>
		<td class="formCaptionTitle"><strong>
			<?=$c_order_title4?>
		</strong></td>
		</tr>
	</table>
	<table width="100%" border="0" cellpadding="8" cellspacing="0" class="formstbl">
		<tr>
			<td style="padding-top:18px;padding-bottom:18px;"><?=$address_delivery?></td>
		</tr>
		</table>
	<div class="sep_dark"></div>
	<table width="100%" cellpadding="4" cellspacing="0">
		<tr>
		<td class="formCaptionTitle"><strong>
			<?=$c_order_title5?>
		</strong></td>
		</tr>
	</table>
	<table width="100%" border="0" cellpadding="8" cellspacing="0" class="formstbl">
		<tr>
			<td style="padding-top:18px;padding-bottom:18px;"><?=$payment_method?></td>
		</tr>
		</table>
	<?if ($form_new):?>
	<table width="100%" cellpadding="4" cellspacing="2" class="formsButtonstbl">
		<tr>
		<td align="center" class="formsButtons"><input type="submit" name="Submit" value="<?=$c_send_new?>" class="btn_form1" /></td>
		</tr>
	</table>
	<?endif //form_new?>
	<?if ($form_edit):?>
	<table width="100%" cellpadding="4" cellspacing="2" class="formsButtonstbl">
		<tr>
		<td align="center" class="formsButtons"><input type="submit" name="Submit" value="<?=$c_send_edit?>" class="btn_form1" />
			&nbsp;&nbsp;<a class="btn_form_delete" href="javascript:submit_form('<?=$c_confirm_form_bin?>', true);">
			<?=$c_delete?>
			</a>&nbsp;</td>
		</tr>
	</table>
	<?endif //form_edit?></td>
</tr>
</table></form>
