# Change Log
Los cambios notables del pryecto se reflejarán en este documento:

Formato basado en [Keep a Changelog](http://keepachangelog.com/)
y el proyecto adherido a [Semantic Versioning](http://semver.org/).

## [1.0.0] - 2020-11-02
- [LETND-dev](https://gitlab.com/murk/letnd/-/tree/dev) _rama de desarrollo_
- [LETND-stable](https://gitlab.com/murk/letnd/-/tree/stable) _rama stable_

### Added
- Rama _dev_ del proyecto. Opera: dev -> master.
- Rama _stable_ del proyecto. Opera master -> stable.

### Changed

### Fixed

## [1.0.0] - 2020-11-02
- [LETND-master](https://gitlab.com/murk/letnd/)

### Added
- Rama principal del proyecto -master-.

### Changed

### Fixed