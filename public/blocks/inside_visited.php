<?php
// si estic al carro no mostro el bloc de visited
global $gl_module;
$m = &$gl_module;
if (isset($_GET['tours_visited_more'])){
	$show_block = false;
	return;
}
// si no n'hi ha cap o estic a la pàgina de veure tots els tours visitats, doncs no mostro el block
if (!isset($_SESSION['visited_tours']) || isset($_GET['tours_visited_more'])) {
	$show_block = false;
	return;
}

$max_visited = 8;


$tours = array_reverse($_SESSION['visited_tours']);

R::escape_array( $tours );

$ids = implode(',', $tours);


$module = Module::load('inside','tour');
$module->parent = 'Block';

$module->condition = 
	"AND inside__tour.tour_id IN (" . $ids . ")";
	
$module->limit = "0, $max_visited";
$module->order_by = "FIELD(inside__tour.tour_id, " . $ids . ")";

$loop = $module->do_action('get_records');

$vars['more'] = false;
if (count($_SESSION['visited_tours']) > $max_visited)
{
	$vars['more'] = $gl_caption['c_tour_list_view_all'];

	if (USE_FRIENDLY_URL) {

		$vars['more_link'] =
					get_link( array(
								'tool' => 'inside',
								'tool_section' => 'tour',
								'friendly_params' => array('tour_visited_more'=>'true'),
								'detect_page' => true
						));
	}
	else {

		$vars['more_link'] = '/?tool=inside&amp;tool_section=tour&amp;tour_visited_more=true&amp;language=' . LANGUAGE;
	}
}
?>