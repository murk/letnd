<? if ( ! $results ): ?>
	<? if ( $options_bar ): ?>
		<div id='opcions_holder'>
			<table width="100%" border="0" cellspacing="0" cellpadding="0" id="opcions">
				<tr>
					<td class="opcions">
						<div id="opcions_fixed_bar">
							<div class="more"></div>
						</div>
						<table class="filters">
							<tr valign="middle">
								<? foreach ( $options_filters as $options_filter ): ?>
									<form>
										<td><?= $options_filter ?></td>
									</form>
								<? endforeach //$options_filters?>
							</tr>
						</table><? if ( $options_filters ): ?>
							<div class="sep"></div><? endif //options_filters?>
						<table border="0" cellpadding="2" cellspacing="0">
							<tr valign="middle">
								<? if ( $options_buttons ): ?>
									<td class="buttons">
									<? foreach ( $options_buttons as $options_button ): ?><?= $options_button ?><? endforeach //$options_button?>
									</td><? endif //options_buttons?>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>
	<? endif //$options_bar?>
<? endif //!$results?>
<? if ( $results ): ?>
	<? if ( $split_count ): ?>
		<div id='split_holder'>
			<table width="100%" border="0" cellpadding="0" cellspacing="0" id="split">
				<tr valign="middle">
					<td width="20%" align="left" nowrap class="pageResults"><?= $split_results_count ?></td>
					<td align="right" nowrap class="pageResults"><?= $split_results ?></td>
				</tr>
			</table>
		</div>
	<? endif //$split_count?>
	<? if ( $options_bar ): ?>
		<div id='opcions_holder'>
			<table width="100%" border="0" cellspacing="0" cellpadding="0" id="opcions">
				<tr>
					<td class="opcionsTitol"><strong><?= $c_options_bar ?></strong></td>
				</tr>
				<tr>
					<td class="opcions">
						<div id="opcions_fixed_bar">
							<div class="more"></div>
						</div>
						<table class="filters">
							<tr valign="middle">
								<? foreach ( $options_move_tos as $options_move_to ): ?>
									<td><?= $options_move_to ?></td>
								<? endforeach //$options_move_tos?>
								<? foreach ( $options_filters as $options_filter ): ?>
									<form>
										<td><?= $options_filter ?></td>
									</form>
								<? endforeach //$options_filters?>
								<? if ( $has_add_dots ): ?>
									<form>
										<td>
											<label>
												<?= $c_add_dots ?>
												<input <?= $add_dots_checked ?> type="checkbox" onchange="javascript:save_frame.window.location='<?= $add_dots_link ?>'" class="filter" name="add_dots"/>
											</label>
										</td>
									</form>
								<? endif //has_add_dots?>
							</tr>
						</table><? if ( $options_filters ): ?>
							<div class="sep"></div><? endif //options_filters?>
						<table border="0" cellpadding="2" cellspacing="0">
							<tr valign="middle">
								<? if ( $options_buttons ): ?>
									<td class="buttons">
									<? foreach ( $options_buttons as $options_button ): ?><?= $options_button ?><? endforeach //$options_button?>
									</td><? endif //options_buttons?>
								<? if ( $save_button ): ?>
									<td>
										<strong><a class="botoOpcionsGuardar" href="javascript:submit_form_save('save_rows_save_records<?= $action_add ?>')">
												<?= $c_send_edit ?>
											</a></strong></td>
								<? endif //save_button?><? if ( $select_button ): ?>
									<td><?= $c_select ?>
										&nbsp;<strong><a class="opcions" href="javascript:select_all(true, document.list)">
												<?= $c_all ?>
											</a>&nbsp;-&nbsp;<a class="opcions" href="javascript:select_all(false, document.list)">
												<?= $c_nothing ?>
											</a></strong></td>
								<? endif //select_button?><? if ( $delete_button ): ?>
									<? if ( $has_bin ): ?>
										<? if ( $is_bin ): ?>
											<td>
												<strong><a class="botoOpcionsBorrar" href="javascript:submit_list('save_rows_delete_selected<?= $action_add ?>', '<?= $c_confirm_deleted ?>', true)">
														<?= $c_delete_selected ?>
													</a></strong></td>
											<td>
												<strong><a class="botoOpcionsGuardar" href="javascript:submit_list('save_rows_restore_selected<?= $action_add ?>')">
														<?= $c_restore_selected ?>
													</a></strong></td>
										<? else: ?>
											<td>
												<strong><a class="botoOpcionsBorrar" href="javascript:submit_list('save_rows_bin_selected<?= $action_add ?>')">
														<?= $c_delete_selected ?>
													</a></strong></td>
										<? endif // is_bin?>
									<? else: ?>
										<td>
											<strong><a class="botoOpcionsBorrar" href="javascript:submit_list('save_rows_delete_selected<?= $action_add ?>', '<?= $c_confirm_deleted ?>', true)">
													<?= $c_delete_selected ?>
												</a></strong></td>
										<? /*<td>
							<td>&nbsp;</td><strong><a class="botoOpcions" href="/admin?menu_id=<?=$menu_id?>&sort_by=0&sort_order=<?=$sort_order0?>">
								<?=$c_move_to?>

						<input type="submit" name="bin_selected" value="<?=$c_delete_selected?>" class="boto1">
							</a></strong></td>*/ ?>
									<? endif // has_bin?><? endif //delete_button?>
								<? if ( $print_button ): ?>
									<td><strong><a class="botoOpcions" href="<?= $print_link ?>">
												<?= $c_print_all ?>
											</a></strong></td>
								<? endif //print_button?>
								<? if ( $has_swap_edit ): ?>
									<td>
										<strong><a class="<?= $swap_class ?>" target="save_frame" href="<?= $swap_edit_link ?>">
												<?= $swap_edit_button ?>
											</a></strong></td>
								<? endif //has_swap_edit?>

								<? if ( $options_buttons_after ): ?>
									<td class="buttons">
										<? foreach ( $options_buttons_after as $options_button ): ?><?= $options_button ?><? endforeach //$options_button?>
									</td>
								<? endif //options_buttons_after?>

							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>
		<div class="sep"></div>
	<? endif //$options_bar?>
	<? if ( ! $is_new_records ): ?>
		<form method="post" action="<?= $form_link ?>" name="list" onsubmit="return check_selected(this)" target="save_frame">
	<? endif // $is_new_records ?>

	<? if ( $content_top ): ?>
		<div class="content_top"><?= $content_top ?></div>
	<? endif //$content_top?>

	<? if ( ! $is_sublist ): ?>
		<input name="action" type="hidden" value="save_rows">
	<? endif // !$is_sublist ?>
	<?= $out_loop_hidden ?>
	<? if ( ! $is_new_records ): ?>
	<table id="<?= $tr_inputs_prefix ?>list_table" width="100%" border="0" cellspacing="0" cellpadding="0" class="listRecord <?= $table_class ?>"><? endif // $is_new_records ?>
	<? if ( $order_bar ): ?>
		<thead>
	<tr><? if ( $has_images ): ?>
		<td valign="middle" class="listCaption" width="<?= $im_admin_thumb_w ?>">
				&nbsp;</td><? endif // has_images?>
		<? if ( $options_checkboxes ): ?>
			<td width="30" valign="top" class="listCaption">&nbsp;</td><? endif // options_checkboxes?>
		<? foreach ( $fields as $l ): extract( $l ) ?>
			<td valign="top" nowrap="nowrap" class="listCaption">
				<strong><? if ( $order_link ): ?><a class="opcions" href="<?= $order_link ?>">
						<?= $field ?>
						</a><? else://order_image?> <?= $field ?><? endif //order_image?>
				</strong><? if ( $order_image ): ?>
					<img src="/admin/themes/inmotools/images/<?= $order_image ?>.gif" width="9" height="9" border="0" align="middle"><? endif //order_image?>
				&nbsp;
			</td>
		<? endforeach //$field?>
		<td valign="top" class="listCaption">&nbsp;</td>
	</tr></thead><? endif //order_bar?>
	<? foreach ( $loop as $l ): extract( $l ) ?>
		<? if ( $group_fields ): ?>
			<tr>
			<td colspan="<?= $reserva_col_span ?>" class="listGroup"><? foreach ( $group_fields as $f ): ?><?= $f ?><? endforeach //$field?></td>
			</tr><? endif // group_fields?>
		<tr class="subListRecord-before listItem<?= $odd_even ?> <?= $tr_class ?>"<? if ( $onmousedown ): ?> onmousedown="change_bg_color(this, 'Down', '<?= $id ?>')"<? endif // onmousedown?> id="<?= $tr_inputs_prefix ?>list_row_<?= $id ?>" <?= $tr_jscript ?>><? if ( $has_images ): ?>
				<td valign="top" class="listImage">
					<? if ( $image_name ): ?>
						<div><img style="max-width:<?= $im_admin_thumb_w ?>px;" src="<?= $image_src_admin_thumb ?>">
						</div>
					<? else: // image_name?>
						<div class="noimage" style="min-height:<?= $im_admin_thumb_h ?>px;"></div>
					<? endif // image_name?>
				</td>
			<? endif // has_images?>
			<? if ( $options_checkboxes ): ?>
				<td width="30" valign="top">
				<? if ( $is_row_selectable ): ?>
					<input name="selected[<?= $conta ?>]" type="checkbox" value="<?= $id ?>" onclick="return false;"<?= $selected ?> />
				<? endif // $is_row_selectable ?>
				</td><? endif // options_checkboxes?>

			<? /* Disseny custom */ ?>

			<td valign="top">
				<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%"><?= $order_id ?><input name="order_id[<?= $order_id ?>]" type="hidden" value="<?= $order_id ?>"/>&nbsp;</td>
						</tr>
				</table>
			</td>
			<td valign="top">
				<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%"><?= $source_id ?>&nbsp;</td>
						</tr>
				</table>
			</td>
			<td valign="top">
				<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%"><?= $customer_id ?>&nbsp;</td>
						</tr>
				</table>
			</td>
			<td valign="top">
				<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%"><?= $entered_order ?>&nbsp;</td>
						</tr>
				</table>
			</td>
			<td valign="top">
				<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%"><?= $order_status ?>&nbsp;</td>
						</tr>
				</table>
			</td>
			<td valign="top">
				<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%"><?= $payment_method ?>&nbsp;</td>
						</tr>
				</table>
			</td>
			<td valign="top">
				<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%"><?= $deposit_status ?>&nbsp;</td>
						</tr>
				</table>
			</td>
			<td valign="top">
				<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%"><?= $deposit_payment_method ?>&nbsp;</td>
						</tr>
				</table>
			</td>
			<td valign="top">
				<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%"><?= $deposit ?>&nbsp;</td>
						</tr>
				</table>
			</td>
			<td valign="top">
				<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%"><?= $deposit_rest ?>&nbsp;</td>
						</tr>
				</table>
			</td>
			<td valign="top" class="<?= $is_gift_card?'gift-card':'' ?>">
				<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%"><?= $all_basetax ?>&nbsp;</td>
						</tr>
				</table>
			</td>
			<td valign="top">
				<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%"><?= $operator_id ?>&nbsp;</td>
						</tr>
				</table>
			</td>
			<? /* Fi disseny custom */ ?>

			<td valign="top" nowrap="nowrap" align="right"><a href="/admin/?menu_id=2303&action=show_form_edit&order_id=<?= $order_id ?>" class="button-edit-admin"></a>
				<div data-order-id="<?= $order_id ?>" class="button-print"></div></td>
		</tr>
		<tr class="subListRecord-holder">
			<td colspan="<?= $reserva_col_span-1 ?>">
				<?= $orderitems ?>
			</td>
			<td></td>
		</tr>
	<? endforeach //$loop?>
	<? if ( ! $is_new_records ): ?>
		</table>
		</form>
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td height="10"></td>
			</tr>
		</table>
	<? endif // $is_new_records ?>


	<? if ( $split_count ): ?>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr valign="middle">
			<td width="20%" align="left" nowrap class="pageResults"><?= $split_results_count ?></td>
			<td align="right" nowrap class="pageResults"><?= $split_results ?></td>
		</tr>
		</table>
	<? endif //$split_count?>

<? endif //results?>