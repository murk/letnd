
const LASSDIVE_PRINTING = "Imprimint";
const LASSDIVE_NUM_RESERVA = 'Número de reserva: ';
const LASSDIVE_PAGAT = 'Pagat';
const LASSDIVE_PAGA_SENYAL = 'Paga i senyal: ';
const LASSDIVE_PENDENT = 'Pendent: ';
const LASSDIVE_RESERVA_ITEM_DATE = 'Dia';
const LASSDIVE_HORARI = 'Horari';
const LASSDIVE_CENTRE = 'Centre';
const LASSDIVE_USER_IDS = 'Instructor';

const LASSDIVE_HORA_MES_GRAN = 'La segunda hora seleccionada tiene que ser más grande que la primera';
const LASSDIVE_NO_DISPONIBLE = 'No se puede seleccionar esta franja, no hay disponibilitat';
const LASSDIVE_HORA_DIFERENTS_CENTRES = 'No se pueden seleccionar horas de distintos centros';

const LASSDIVE_NO_RESPONSE = ' La impresora no responde';
const LASSDIVE_PRINT_SUCCESS = ' Ticket impreso correctament';
const LASSDIVE_ASB_DRAWER_KICK = ' Status of the drawer kick number 3 connector pin= "H"';
const LASSDIVE_ASB_BATTERY_OFFLINE = ' Offline due to a weak battery';
const LASSDIVE_ASB_OFF_LINE = ' Offline status';
const LASSDIVE_ASB_COVER_OPEN = ' La cubierta esta abierta';
const LASSDIVE_ASB_PAPER_FEED = ' Paper feed switch is feeding paper';
const LASSDIVE_ASB_WAIT_ON_LINE = ' Waiting for online recovery';
const LASSDIVE_ASB_PANEL_SWITCH = ' Panel switch is ON';
const LASSDIVE_ASB_MECHANICAL_ERR = ' Mechanical error generated';
const LASSDIVE_ASB_AUTOCUTTER_ERR = ' Auto cutter error generated';
const LASSDIVE_ASB_UNRECOVER_ERR = ' Unrecoverable error generated';
const LASSDIVE_ASB_AUTORECOVER_ERR = ' Auto recovery error generated';
const LASSDIVE_ASB_RECEIPT_NEAR_END = ' No paper in the roll paper near end detector';
const LASSDIVE_ASB_RECEIPT_END = ' No paper in the roll paper end detector';
const LASSDIVE_ASB_BUZZER = ' Sounding the buzzer';
const LASSDIVE_ASB_WAIT_REMOVE_LABEL = ' Waiting to remove label';
const LASSDIVE_ASB_SPOOLER_IS_STOPPED = ' Stop the spooler';

const LASSDIVE_ESCOLLIR_CENTRE = 'Tienes que escoger un centro';
const LASSDIVE_ESCOLLIR_PRODUCT_CENTRE = 'En vista semanal hay que escoger producto y centro';

// TIPS
const LASSDIVE_RESERVA = 'Reserva';
const LASSDIVE_PAYED = 'Pagado';
const LASSDIVE_PENDING = 'Pendiente';

const LASSDIVE_NO_DATE = 'Ninguna fecha';
const LASSDIVE_CONFIRMAR_ESBORRAR_HORARI = "Se borrarán los horarios, centro e instructores asignados a esta actividad, ¿seguro que quieres continuar?";
const LASSDIVE_NO_ESBORRAT_HORARI = "El horario no se ha podido borrar";