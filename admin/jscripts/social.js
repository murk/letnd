$(function () {
	$('#social_language').change(function () {
		var link = window.location.href;
		if (link.indexOf('social_language') != -1) {
			pos = link.indexOf('&social_language=');
			link = link.substring(0, pos);
		}
		link = link + '&social_language=' + this.value;
		window.location.href = link;
	});


	$('input[name=facebook_publish_as]').click(function(){
		var val = $(this).val();

		$('#facebook-image,#facebook-link').hide();
		$('#facebook-' + val).show();


	});

});
