<link href="../../templates/styles/general.css" rel="stylesheet" type="text/css" />
<table width="95%" border="0" cellpadding="0" cellspacing="0" class="fonsgeneralsclars">
  <tr>
    <td width="10"><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="10" height="1"></td>
    <td><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="1" height="15"></td>
    <td width="10"><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="10" height="1"></td>
  </tr>
  <tr>
    <td width="10">&nbsp;</td>
    <td align="left" valign="top"><img src="/clients/agenciaportselva/templates/images/logoasseguran.jpg" width="180" height="112"></td>
    <td width="10">&nbsp;</td>
  </tr>
  <tr>
    <td width="10">&nbsp;</td>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="30"><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="30" height="1"></td>
        <td><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="1" height="25"></td>
        <td width="30"><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="30" height="1"></td>
      </tr>
      <tr>
        <td width="30">&nbsp;</td>
        <td height="600" align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td align="left"><p class="text12verdanawhitejustify"><strong>Qui Som</strong></p>
              <p class="text12verdanawhitejustify">Un equip de professionals de la mediaci&oacute; amb mitjans posats a disposici&oacute; dels seus clients. Tenim els coneixements i l'experiencia per poder-ho fer, operem en el mercat de la mediaci&oacute; d'assegurances des de 1983, com a Agent d'Assegurances i del 1988 com a Corredor d'Assegurances </p></td>
          </tr>
          <tr>
            <td><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="1" height="15"/></td>
          </tr>
          <tr>
            <td align="center"><table width="80%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td  class="text12verdanawhite" align="center"><div align="center"><strong>Assessorar-lo en la contractaci&oacute; de les seves pol&iacute;sses d'assegurances aix&iacute; com representar el seus interessos en el moment de rebre el servei</strong> </div></td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="1" height="15"/></td>
          </tr>
          <tr>
            <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="17"><img src="/clients/agenciaportselva/templates/images/logopetit.gif" width="18" height="18"/></td>
                    <td width="15"><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="15" height="1"/></td>
                    <td class="text10verdanawhite"><strong>Els nostres objectius </strong></td>
                  </tr>
                  <tr>
                    <td width="17"></td>
                    <td width="15">&nbsp;</td>
                    <td><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="15" height="15"/></td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>Qualitat de servei als nostres clients mitjan&ccedil;ant: </td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="15" height="15"/></td>
                  </tr>
                  <tr>
                    <td width="17">&nbsp;</td>
                    <td width="15">&nbsp;</td>
                    <td>
                      <li>Absoluta independ&egrave;ncia enfront les asseguradores.</li>
                      <li>Defensa dels interessos dels nostres clients enfornt les asseguradores.</li>
                      <li>Formaci&oacute; continuada.</li>
                      <li>Especialitzaci&oacute;.</li>
                      <li>Disseny i desenvolupament de productes adequats a cada necessitat.</li>
                      <li>Tracte i servei personalitzat en l'estudi, en la contractaci&oacute; i en le manteniment de l'asseguran&ccedil;a, aix&iacute; com en la tramitaci&oacute; i gesti&oacute; del sinistre.     </li></td>
                  </tr>
                </table></td>
                <td width="15"><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="15" height="1"/></td>
                <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="17"><img src="/clients/agenciaportselva/templates/images/logopetit.gif" width="18" height="18"/></td>
                    <td width="15"><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="15" height="1"/></td>
                    <td class="text10verdanawhite"><strong>Productes en companyies de primer ordre </strong></td>
                  </tr>
                  <tr>
                    <td width="17"></td>
                    <td width="15">&nbsp;</td>
                    <td><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="15" height="15"/></td>
                  </tr>

                  <tr>
                    <td width="17">&nbsp;</td>
                    <td width="15">&nbsp;</td>
                    <td><li>Incendis i complementaris.</li>
                        <li>Robatori i complementaris.</li>						
						<li>Multirisc (Llar, Comunitats, Negocis, Ind&uacute;stries...) </li>						  
	                        <li>Vehicles a motor</li>
	                        <li>Protecci&oacute; jur&iacute;dica</li>
	                        <li>Responsabilitat civil (General, Explotaci&oacute;, Privada, Productes, Ca&ccedil;a i Pesca) </li>
	                        <li>Transports (Cascs, Embarcacions) </li>
	                        <li>T&egrave;cnics (Construcci&oacute;, Desenal de danys, Equips electr&ograve;nics) </li>
	                        <li>Agradis</li>
	                        <li>Vida (individual, col&middot;lectius i de conveni)</li>
	                        <li>Jubilació</li>
	                        <li>Plans de pensions </li>
	                        <li>Fons d'inversi&oacute;</li>
	                        <li>Decessos</li>
	                        <li>Salut</li>
	                        <li>Accidents (individual, col&middot;lectius i de conveni)</li>
	                        <li>Viatges</li>
	                        <li>Dental</li>
	                        <li>Subsidi diari per baixa laboral</li>
	                        <li>De depend&egrave;ncia</li>
	                        <li>Conting&egrave;ncies diverses   </li></td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                  </tr>
                </table></td>
              </tr>

            </table></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
        </table><p class="text12verdanawhitejustify">&nbsp;</p>          </td>
        <td width="30">&nbsp;</td>
      </tr>
    </table></td>
    <td width="10">&nbsp;</td>
  </tr>
  <tr>
    <td width="10">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td width="10">&nbsp;</td>
  </tr>
  <tr>
    <td width="10">&nbsp;</td>
    <td><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="40" height="15"></td>
    <td width="10">&nbsp;</td>
  </tr>
</table>
