<?
$gl_caption['c_init_session'] = 'Iniciar sessió';
$gl_caption['c_personal_data'] = 'Dades personals';
$gl_caption['c_already_custumer'] = 'Si ja ets client';
$gl_caption['c_register_now'] = 'Si no registra\'t ara';
$gl_caption['c_title_login'] = 'Inici de sessió';
$gl_caption['c_register'] = 'Registrar-se';
$gl_caption['c_make_reservation'] = 'Fés la reserva';
$gl_caption['c_book_host_main'] = 'Adult 1';
$gl_caption['c_add_new_host'] = 'Afegir nou';
$gl_caption['c_edit_host'] = 'Editar';

$gl_caption['c_custumer_menu_personal_data'] = 'Dades personals';
$gl_caption['c_custumer_menu_books'] = 'Reserves';
$gl_caption['c_custumer_menu_book'] = 'Reserva';
$gl_caption['c_custumer_menu_logout'] = 'Desconnectar';

$gl_caption_custumer['c_phone1'] = 'Telèfon';
?>