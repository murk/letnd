<link href="../../templates/styles/general.css" rel="stylesheet" type="text/css" />
<table width="95%" border="0" cellpadding="0" cellspacing="0" class="fonsgeneralsclars">
  <tr>
    <td width="10"><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="10" height="1"></td>
    <td><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="1" height="15"></td>
    <td width="10"><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="10" height="1"></td>
  </tr>
  <tr>
    <td width="10">&nbsp;</td>
    <td align="left" valign="top"><img src="/clients/agenciaportselva/templates/images/logoasseguran.jpg" width="180" height="112"></td>
    <td width="10">&nbsp;</td>
  </tr>
  <tr>
    <td width="10">&nbsp;</td>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="30"><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="30" height="1"></td>
        <td><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="1" height="25"></td>
        <td width="30"><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="30" height="1"></td>
      </tr>
      <tr>
        <td width="30">&nbsp;</td>
        <td height="600" align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td align="left"><p class="text12verdanawhitejustify"><strong>Qui sommes-nous ?</strong></p>
              <p class="text12verdanawhitejustify">Une équipe de professionnels qui met les moyens nécessaires à la disposition de ses clients. Intermédiaires (depuis 1983), agents et courtiers dassurance (depuis 1988), nous avons toutes les connaissances et lexpérience nécessaires pour mener à bien vos projets. </p></td>
          </tr>
          <tr>
            <td><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="1" height="15"/></td>
          </tr>
          <tr>
            <td align="center"><table width="80%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td  class="text12verdanawhite" align="center"><div align="center"><strong>Nous vous conseillerons dans la souscription à votre police dassurance et nous  représenterons vos intérêts au moment de bénéficier de ses services.</strong> </div></td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="1" height="15"/></td>
          </tr>
          <tr>
            <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="17"><img src="/clients/agenciaportselva/templates/images/logopetit.gif" width="18" height="18"/></td>
                    <td width="15"><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="15" height="1"/></td>
                    <td class="text10verdanawhite"><strong>Nos objectifs  </strong></td>
                  </tr>
                  <tr>
                    <td width="17"></td>
                    <td width="15">&nbsp;</td>
                    <td><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="15" height="15"/></td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>Un service de qualité, grâce à: </td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="15" height="15"/></td>
                  </tr>
                  <tr>
                    <td width="17">&nbsp;</td>
                    <td width="15">&nbsp;</td>
                    <td>
                      
                          <li>Une  totale ind&eacute;pendance par rapport aux compagnies d&rsquo;assurance.</li>
                          <li>La  d&eacute;fense des int&eacute;r&ecirc;ts de nos clients face aux compagnies d&rsquo;assurance.</li>
                          <li>Une  formation continue.</li>
                          <li>Une  sp&eacute;cialisation.</li>
                          <li>La  cr&eacute;ation et le d&eacute;veloppement de produits adapt&eacute;s &agrave; chaque n&eacute;cessit&eacute;.</li>
                          <li>
                        Un  service et une attention personnalis&eacute;s en ce qui concerne l&rsquo;analyse, la  souscription et le bon fonctionnement de l&rsquo;assurance mais aussi pour  l&rsquo;instruction et la gestion des sinistres.</li>
                      </td>
                  </tr>
                </table></td>
                <td width="15"><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="15" height="1"/></td>
                <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="17"><img src="/clients/agenciaportselva/templates/images/logopetit.gif" width="18" height="18"/></td>
                    <td width="15"><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="15" height="1"/></td>
                    <td class="text10verdanawhite"><strong>Produits de compagnies de premier ordre</strong></td>
                  </tr>
                  <tr>
                    <td width="17"></td>
                    <td width="15">&nbsp;</td>
                    <td><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="15" height="15"/></td>
                  </tr>

                  <tr>
                    <td width="17">&nbsp;</td>
                    <td width="15">&nbsp;</td>
                    <td>
                        <li>Incendie</li>
                        <li>Vol</li>
                        <li>Tous  risques</li>
                        <li>-  Logement </li>
                        <li>-  Association de voisins</li>
                        <li>-  Entreprise</li>
                        <li>Industries</li>
                        <li>V&eacute;hicules  &agrave; moteur</li>
                        <li>Protection  juridique</li>
                        <li>Responsabilit&eacute;  civile</li>
                        <li>-  G&eacute;n&eacute;rale</li>
                        <li>-  Commerciale</li>
                        <li>-  Priv&eacute;e</li>
                        <li>-  Produits</li>
                        <li>-  Chasse et p&ecirc;che</li>
                        <li>Transports</li>
                        <li>-  Casques</li>
                        <li>-  Embarcations</li>
                        <li>Assurances  techniques</li>
                        <li>-  Construction</li>
                        <li>-  Assurance dommages</li>
                        <li>-  &Eacute;quipements &eacute;lectriques</li>
                        <li>Agricole</li>
                        <li>Assurances-vie  (individuelle, collective et convention)</li>
                        <li>Retraite</li>
                        <li>Caisses  de retraite</li>
                        <li>Fonds  d&rsquo;investissement</li>
                        <li>D&eacute;c&egrave;s</li>
                        <li>Sant&eacute;</li>
                        <li>Accidents  (individuelle, collective et convention)</li>
                        <li>Voyages</li>
                        <li>Assurances  dentaires</li>
                        <li>Prestations  journali&egrave;res en cas de cong&eacute; maladie</li>
                        <li>D&eacute;pendance</li>
                      <li>
                      Impr&eacute;vus  divers</li>
                        </td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                  </tr>
                </table></td>
              </tr>

            </table></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
        </table><p class="text12verdanawhitejustify">&nbsp;</p>          </td>
        <td width="30">&nbsp;</td>
      </tr>
    </table></td>
    <td width="10">&nbsp;</td>
  </tr>
  <tr>
    <td width="10">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td width="10">&nbsp;</td>
  </tr>
  <tr>
    <td width="10">&nbsp;</td>
    <td><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="40" height="15"></td>
    <td width="10">&nbsp;</td>
  </tr>
</table>
