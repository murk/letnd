<?

/**
 * UtilitySell
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2015
 * @version 1
 * @access public
 */
class UtilitySell extends Module {

	public function __construct() {
		parent::__construct();
	}
	public function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}

	/**
	 * @param $val
	 *
	 * @return mixed
	 */
	public function format_decimal  ($val){
		return str_replace( '.', ',', $val );
	}
	public function get_form()
	{
		$show = new ShowForm($this);

		$price = R::get('price');
		if ( $price ) {
			$this->set_field('price','default_value',$this->format_decimal($price));
		}
		$date_bought = R::get('date_bought');
		if ( $date_bought ) {
			$this->set_field('date_bought','default_value',$this->format_decimal($date_bought));
		}
		$price_bought = R::get('price_bought');
		if ( $price_bought ) {
			$this->set_field('price_bought','default_value',$this->format_decimal($price_bought));
		}

		$this->set_field('mortgage_cancel_percent','default_value',$this->format_decimal(UTILITY_MORTGAGE_CANCEL_PERCENT));
		$this->set_field('estate_commission_percent','default_value',$this->format_decimal(UTILITY_ESTATE_COMMISSION_PERCENT));
		$this->set_field('habitability_card','default_value',$this->format_decimal(UTILITY_HABITABILITY_CARD));
		$this->set_field('efficiency','default_value',$this->format_decimal(UTILITY_EFFICIENCY));
		$this->set_field('date_sold','default_value',now());

		return $show->show_form();
	}

	function get_plusvalua_vars_ajax() {

//		$ret['plusvalua_increment'] = 2.7;
//		$ret['plusvalua_tribute_percent'] = 26;
//		die (json_encode($ret));

		Db::connect_mother();

		$q = R::escape('query');
		$q = strtolower($q);
		$ret = array();
		$ret['suggestions'] = array();

		$r = &$ret['suggestions'];

		if ($q)
		{
			// primer resultats que comencin amb el text
			$query = "SELECT municipi_id AS id, municipi AS name, comarca, provincia, coeficient, tipus_impositiu
				FROM apigirona.inmo__municipi
				INNER JOIN inmo__comarca USING (comarca_id)
				INNER JOIN inmo__provincia USING (provincia_id)
				WHERE municipi LIKE '" . $q . "%'
				ORDER BY municipi LIMIT 100";

			$results = Db::get_rows($query);
			foreach ($results as $rs) {
				$r[] = array(
					'value' => $rs['name'] . " - " . $rs['comarca'] . "",
					'data'  => "${rs['coeficient']},${rs['tipus_impositiu']}"
				);
			}

			// Segon, resultats que continguin el text
			$query = "SELECT municipi_id AS id, municipi AS name, comarca, provincia, coeficient, tipus_impositiu
				FROM apigirona.inmo__municipi
				INNER JOIN inmo__comarca USING (comarca_id)
				INNER JOIN inmo__provincia USING (provincia_id)
				WHERE municipi LIKE '%" . $q . "%'
				AND municipi not LIKE '" . $q . "%'
				ORDER BY municipi LIMIT 100";

			$results = Db::get_rows($query);
			foreach ($results as $rs) {
				$r[] = array(
					'value' => $rs['name'] . " - " . $rs['comarca'] . "",
					'data'  => "${rs['coeficient']},${rs['tipus_impositiu']}"
				);
			}
		}

		Db::reconnect();
		die (json_encode($ret));

	}

	function write_record()
	{
		/*$writerec = new SaveRows($this);
		$writerec->save();*/
	}
}

?>