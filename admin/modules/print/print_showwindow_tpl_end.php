<?php
// CREAR ARXIU TEMPORAL DEL PDF I FUNCIÓ PER BORRAR-LO CADA X DIES
define('TEMP_PATH', CLIENT_PATH . '/temp/');
$file = basename(tempnam(TEMP_PATH, 'tmp'));
rename(TEMP_PATH . $file, TEMP_PATH . $file . '.pdf');
$file .= '.pdf';
// Guardar el PDF en un fichero
if (isset($return_string) && $return_string){
	$return_string = $pdf->Output($file, 'S');
	return;
}
else{
	$pdf->Output(TEMP_PATH . $file, 'F');
}
// Redirección con JavaScript
//echo "<HTML><SCRIPT>document.location.replace('/" . CLIENT_DIR . "/temp/" . $file . "');</SCRIPT></HTML>";

CleanFiles(TEMP_PATH);
$file = TEMP_PATH . $file;
UploadFiles::serve_download($file,$file_serve_name);

function CleanFiles($dir)
{
    // Borrar arxius temporals
    $t = time();
    $h = opendir($dir);
    while ($file = readdir($h))
    {
        // if (strpos($file, 'tmp') !== false && substr($file, -4) == '.pdf')
        if (substr($file, -4) == '.pdf') // així es boirren els que començaven per 'sw'
        {
            $path = $dir . '/' . $file;
            if ($t - filemtime($path) > 3600)
                @unlink($path);
        }
    }
    closedir($h);
}
?>