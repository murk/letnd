# Contributing

Cuanto tengas que controbuïr en este repositorio, por favor, préviamente discute el cambio que quieras
aplicar mediante _issue_, enviando un email, o cualquier otro método, con el propietario del repositorio
o algún colaborador activo.

Hay un pequeño código de conducta para, que la escalabildad solo afecte al código y no al ~~mal~~ rollo entre usuarios ;)

## Proceso de Pull Request

1. Asegurate que cualquier dependencia o build se haya validado antes (ya sea desde Próximos Cambios o autopropuestas validadas por usuarios implicados con el proyecto).
2. Actualiza el README.md/CHANGELOG.md con detalles de los cambios.
3. Asegurate de comentar tanto cómo sea posible el código, ejemplificando al máximo el código que vaya a reutilizable. **Los espaguetis con boloñesa por favor**.
4. Podrás hacer el merge del Pull Request una vez que un developer del proyecto haya verificado lo anterior..

## Conducta esperada

LETND. Este es el nombre del proyecto en el que colaboramos. Cualquier cosa extracción peyorativa del de un tema no relacionado con el término, quedará automáticamente fuera del proyecto. En otras palabras: edad, cuerpo, discapacidad, etnia, identidad y expresión de género, nivel de experiencia, nacionalidad, apariencia, religión, identidad sexual y orientación, no igualitarismo, etc. son sólamente algunos de los motivos para quedar baneado.

### Responsabilidades

Los responsables del proyecto son responsables de tomar medidas si cualquiera de los puntos nombrados anteriormente es pasado por alto, por lo que se comprometen a tomar medidas correctivas si algo sucediera acerca de.

### Alcance y resolución

Esta conducta es esperada y respetada para todos los participantes del proyecto sin excepción alguna. 

Si cómo usuario detectas alguna conducta, tanto a nivel ético cómo de proyecto inadecuada, rogamos por favor que te pongas en contacto con algún miembro del equipo mediante el correo de broadcast o te dirigas directamente a: murk@riseup.net


[homepage]: https://gitlab.com/murk/letnd
[version]: https://gitlab.com/murk/letnd/-/tree/stable