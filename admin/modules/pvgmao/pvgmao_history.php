<?
/**
 * PvgmaoHistory
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2011
 * @version $Id$
 * @access public
 *
 * TODO - Observacions al historial
 *
 */
class PvgmaoHistory extends Module{

	var $needs_client_id = true;
	var $group_id, $user_is_admin, $user_is_client, $client_id;

	function __construct(){
		parent::__construct();
	}

	function on_load() {
		parent::on_load();

		$this->set_field( 'maquina_id', 'select_condition', 'client_id=' . $this->client_id );
		$this->set_field( 'estat_id', 'select_condition', 'client_id=' . $this->client_id );

	}

	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function get_records()
	{
	    $listing = new ListRecords($this);
		$listing->condition = "client_id = " . $this->client_id;

        $template = R::get('template');

        if ($template == 'clean') {
        	$listing->set_options(0,0,0,0,0,0);
	        $maquina_id = R::id( 'maquina_id' );
	        $listing->condition .= " AND maquina_id = $maquina_id";
        }
        else {
        	$listing->set_options(1,1,1,1,1,1,0);
	        $listing->add_filter( 'maquina_id' );
        }

		$listing->has_bin = false;
	    return $listing->list_records();
	}
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form() {
		// No hi ha res que espugui editar, es podria afegir una observació
		return;
		/* $show = new ShowForm($this);
		$show->condition = "client_id = " . $this->client_id;
		$show->has_bin = false;
	    return $show->show_form();*/
	}

	function save_rows()
	{
	    $save_rows = new SaveRows($this);
	    $save_rows->save();
	}

	function write_record()
	{
	    $writerec = new SaveRows($this);
	    $writerec->save();
	}

	function manage_images()
	{
	    $image_manager = new ImageManager($this);
	    $image_manager->execute();
	}
}
?>