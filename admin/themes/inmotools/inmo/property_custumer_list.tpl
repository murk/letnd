<?if ($results):?>
<br />
<div id='split_holder'><table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" id="split">
  <tr valign="middle">
    <td width="20%" align="left" nowrap="nowrap" class="pageResults"><?=$split_results_count?></td>
    <td align="right" nowrap="nowrap" class="pageResults"><?=$split_results?></td>
  </tr>
</table></div>
<table width="100%" height="0" border="0" cellspacing="0" cellpadding="0" class="listRecord">
  <tr>
    <td style="padding:0px"></td>
  </tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="listRecord" style="margin-top:0px">
    <?foreach($loop as $l): extract ($l)?>
      <tr class="listItem<?=$odd_even?>" id="list_row_<?=$id?>">
        <td align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0" class="noPadding">
            <tr>
              <td><table border="0" cellspacing="0" cellpadding="0">
                  <tr><?if ($options_checkboxes):?> 
                        <?endif // options_checkboxes?>
                    <td class="listTitle"><strong>
                      <?=$hidden?>&nbsp;
              <?=$name?> <?=$surname1?> <?=$surname2?>
              </strong></td>
                </tr>
              </table></td>
          </tr>
            <tr>
              <td><table width="100%" border="0" cellpadding="0" cellspacing="0" style="padding-right:8px">
                  <tr>
                    <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0" class="listDetails2">
  <tr>
    <td><table border="0" cellpadding="0" cellspacing="6"><?php if($phone1||$phone2||$phone3):?>
                      <tr>
                        <td valign="top"><?php if($phone1||$phone2):?><strong class="listItem">
                        <?=$c_phone?>:</strong>&nbsp;<?php endif //$phone1||$phone2?></td>
                        <td valign="top"><strong class="resaltatSmall"><?if ($phone1):?><?=$phone1?>&nbsp;&nbsp;<?endif //$phone1?><?if ($phone2):?>-&nbsp;&nbsp;<?=$phone2?>&nbsp;&nbsp;&nbsp;<?endif //$phone2?></strong><?if ($phone3):?><strong class="listItem"><?=$c_phone3?>:</strong>&nbsp;<?=$phone3?>&nbsp;&nbsp;&nbsp;<?endif //phone3?></td>
                      </tr><?php endif //$phone1||$phone2||$phone3?><?if ($mail):?>
                      <tr>
                        <td valign="top"><strong class="listItem">
                          <?=$c_mail?>:</strong>&nbsp;</td>
                        <td valign="top"><?=$mail?></td>
                      </tr><?endif //mail?><?if ($adress):?>
                      <tr>
                        <td valign="top"><strong class="listItem">
                          <?=$c_address?>:</strong>&nbsp;</td>
                        <td valign="top"><?=$adress?>&nbsp;<?if ($numstreet):?><?=$numstreet?>&nbsp;<?endif //numstreet?><?if ($block):?><?=$c_block?>&nbsp;<?=$block?>&nbsp;<?endif //block?><?if ($flat):?><?=$flat?><?endif //flat?><?if ($door):?>-<?=$door?><?endif //door?></td>
                      </tr><?endif //adress?><?php if ($town || $province):?>
                      <tr>
                        <td valign="top"><strong class="listItem">
                        <?=$c_town?>:</strong>&nbsp;</td>
                        <td valign="top"><?=$town?><?if ($province):?>&nbsp;(<?=strtoupper($province)?>)<?endif //province?></td>
                      </tr><?php endif //town?><?if ($vat):?>
                      <tr>
                        <td valign="top"><strong class="listItem">
                          <?=$c_vat?>:</strong>&nbsp;</td>
                        <td valign="top"><?=$vat?></td>
                      </tr><?endif //vat?>
                    </table></td>
  </tr>
</table>
</td>
                    <td width="35%" valign="top" class="listDescription"><?if ($observations1):?><strong><?=$c_observations1?></strong><br /><?=$observations1?><?endif //observations?></td>
                  </tr>
                </table>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td align="right"><div class="right"><?foreach($buttons as $button):?><?=$button?><?endforeach //$buttons?></div></td>
                  </tr>
                  <tr>
                    <td><img src="/admin/themes/inmotools/images/spacer.gif" width="1" height="10"></td>
                  </tr>
                </table></td>
          </tr>
          </table></td>
    </tr>
      <?endforeach //$loop?>
</table>
<br />
<?endif //results?>