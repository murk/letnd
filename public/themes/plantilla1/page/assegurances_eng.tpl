<link href="../../templates/styles/general.css" rel="stylesheet" type="text/css" />
<table width="95%" border="0" cellpadding="0" cellspacing="0" class="fonsgeneralsclars">
  <tr>
    <td width="10"><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="10" height="1"></td>
    <td><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="1" height="15"></td>
    <td width="10"><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="10" height="1"></td>
  </tr>
  <tr>
    <td width="10">&nbsp;</td>
    <td align="left" valign="top"><img src="/clients/agenciaportselva/templates/images/logoasseguran.jpg" width="180" height="112"></td>
    <td width="10">&nbsp;</td>
  </tr>
  <tr>
    <td width="10">&nbsp;</td>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="30"><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="30" height="1"></td>
        <td><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="1" height="25"></td>
        <td width="30"><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="30" height="1"></td>
      </tr>
      <tr>
        <td width="30">&nbsp;</td>
        <td height="600" align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td align="left"><p class="text12verdanawhitejustify"><strong>About us </strong></p>
              <p class="text12verdanawhitejustify">A professional team of  intermediaries with resources placed at the disposal of our clients. We have  the know-how and experience to get things done: we have been working in the  insurance intermediary field since 1983 as insurance agents and since 1988 as  insurance brokers.</p></td>
          </tr>
          <tr>
            <td><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="1" height="15"/></td>
          </tr>
          <tr>
            <td align="center"><table width="80%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td  class="text12verdanawhite" align="center"><div align="center"><strong>We can advise you on taking out your insurance policies and represent your interests when it comes to receiving the service.</strong> </div></td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="1" height="15"/></td>
          </tr>
          <tr>
            <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="17"><img src="/clients/agenciaportselva/templates/images/logopetit.gif" width="18" height="18"/></td>
                    <td width="15"><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="15" height="1"/></td>
                    <td class="text10verdanawhite"><strong>Our mission  </strong></td>
                  </tr>
                  <tr>
                    <td width="17"></td>
                    <td width="15">&nbsp;</td>
                    <td><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="15" height="15"/></td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td><p>To provide  our clients with a quality service by means of:</p></td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="15" height="15"/></td>
                  </tr>
                  <tr>
                    <td width="17">&nbsp;</td>
                    <td width="15">&nbsp;</td>
                    <td>
                    
                    <li>Complete  independence from insurance companies</li>
                          <li>Defending  our clients&rsquo; interests when dealing with insurance companies</li>
                          <li>Continuous  professional training</li>
                          <li>Specialisation</li>
                          <li>The  design and development of products to meet every need</li>
                          <li>A  personalised service when studying, taking out and managing insurance policies,  as well as in the processing and administration of claims</li>
                        
                      
                      </td>
                  </tr>
                </table></td>
                <td width="15"><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="15" height="1"/></td>
                <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="17"><img src="/clients/agenciaportselva/templates/images/logopetit.gif" width="18" height="18"/></td>
                    <td width="15"><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="15" height="1"/></td>
                    <td class="text10verdanawhite"><strong>Products from top-ranking  companies</strong></td>
                  </tr>
                  <tr>
                    <td width="17"></td>
                    <td width="15">&nbsp;</td>
                    <td><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="15" height="15"/></td>
                  </tr>

                  <tr>
                    <td width="17">&nbsp;</td>
                    <td width="15">&nbsp;</td>
                    <td>
                        <li>Fire  and additional</li>
                        <li>Theft  and additional</li>
                        <li>Comprehensive</li>
                        <li>Home </li>
                        <li>Community of properties</li>
                        <li>Business</li>
                        <li>Industries</li>
                        <li>Motor  vehicles </li>
                        <li>Legal  protection</li>
                        <li>Third-party  liability</li>
                        <li>General</li>
                        <li>Operational</li>
                        <li>Private</li>
                        <li>Product</li>
                        <li>Hunting and fishing</li>
                        <li>Transports</li>
                        <li>Hull</li>
                        <li>Boats</li>
                        <li>Technical</li>
                        <li>Construction</li>
                        <li>Ten-year property damage</li>
                        <li>Electronic equipment</li>
                        <li>Agricultural</li>
                        <li>Life  (individual, groups and collective bargaining)</li>
                     
                        
                            <li>Retirement</li>
                            <li>Pension  plans</li>
                            <li>Investment  funds</li>
                            <li>Death  benefit</li>
                            <li>Health</li>
                            <li>Accidents  (individual, groups and collective bargaining)</li>
                            <li>Travel</li>
                            <li>Dental</li>
                            <li>Daily  allowance for loss of income</li>
                            <li>Disability</li>
                            <li>Sundry  contingencies</li>
                         </td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                  </tr>
                </table></td>
              </tr>

            </table></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
        </table><p class="text12verdanawhitejustify">&nbsp;</p>          </td>
        <td width="30">&nbsp;</td>
      </tr>
    </table></td>
    <td width="10">&nbsp;</td>
  </tr>
  <tr>
    <td width="10">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td width="10">&nbsp;</td>
  </tr>
  <tr>
    <td width="10">&nbsp;</td>
    <td><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="40" height="15"></td>
    <td width="10">&nbsp;</td>
  </tr>
</table>
