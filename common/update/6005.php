<?php

/* 19-11-14 
  
 * PORTFOLI: Afegir categories

 */

	
	
if (is_table('portfoli__configadmin') && !(Db::get_first("SELECT count(*) FROM portfoli__configadmin WHERE name='destacat_max_results'"))){
	
	$update_sql .= "
		INSERT INTO portfoli__configadmin (`name`, `value`) VALUES ('destacat_max_results', '24');
	";	
	
}	
	
	
if (is_table('portfoli__configadmin') && !(Db::get_first("SELECT count(*) FROM portfoli__configadmin WHERE name='show_page_id'"))){
	
	$update_sql .= "
		INSERT INTO portfoli__configadmin (`name`, `value`) VALUES ('show_page_id', '0');
	";	
	
}	

if (is_table('portfoli__portfoli')) {
	
	$update_sql .= "
		CREATE TABLE `portfoli__category` (
			`category_id` INT(11) NOT NULL AUTO_INCREMENT,
			`ordre` INT(11) NOT NULL DEFAULT '0',
			`blocked` TINYINT(1) NOT NULL DEFAULT '0',
			PRIMARY KEY (`category_id`)
		)
		COLLATE='utf8_spanish2_ci'
		ENGINE=MyISAM
		AUTO_INCREMENT=1;
	";	
	
	$update_sql .= "
		CREATE TABLE `portfoli__category_language` (
			`category_id` INT(11) NOT NULL DEFAULT '0',
			`language` CHAR(3) NOT NULL COLLATE 'utf8_spanish2_ci',
			`category` VARCHAR(100) NOT NULL COLLATE 'utf8_spanish2_ci'
		)
		COLLATE='utf8_spanish2_ci'
		ENGINE=MyISAM;
	";	
	
	$update_sql .= "
		ALTER TABLE `portfoli__portfoli`
			ADD COLUMN `category_id` INT(11) NOT NULL AFTER `destacat`;
	";	
	
	$update_sql .= "
		ALTER TABLE `portfoli__portfoli`
			CHANGE COLUMN `modified` `modified` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00' AFTER `ordre`,
			ADD COLUMN `entered` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `modified`;
	";	
	
	$update_sql .= "
		INSERT INTO `all__menu` (`menu_id`, `menu_group`, `tool`, `tool_section`, `action`, `parent_id`, `toolmode_id`, `variable`, `ordre`, `link`, `process`, `bin`, `read1`, `write1`) VALUES (70005, 'portfoli', 'portfoli', 'category', '', 70000, 1, 'PORTFOLI_MENU_CATEGORY', 2, '', '', 0, ',1,', ',1,');
		INSERT INTO `all__menu` (`menu_id`, `menu_group`, `tool`, `tool_section`, `action`, `parent_id`, `toolmode_id`, `variable`, `ordre`, `link`, `process`, `bin`, `read1`, `write1`) VALUES (70007, 'portfoli', 'portfoli', 'category', 'list_records', 70005, 1, 'PORTFOLI_MENU_CATEGORY_LIST', 2, '', '', 0, ',1,', ',1,');
		INSERT INTO `all__menu` (`menu_id`, `menu_group`, `tool`, `tool_section`, `action`, `parent_id`, `toolmode_id`, `variable`, `ordre`, `link`, `process`, `bin`, `read1`, `write1`) VALUES (70006, 'portfoli', 'portfoli', 'category', 'show_form_new', 70005, 1, 'PORTFOLI_MENU_CATEGORY_NEW', 1, '', '', 0, ',1,', ',1,');
	";	
}
?>