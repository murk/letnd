<?php

/* 29-10-13 
  
 * Product: Nou modul de punts de venta

 */


if (is_table('product__product') && !is_table('product__sellpoint')){
	
	$update_sql = "
					INSERT INTO `all__menu` (`menu_id`, `menu_group`, `tool`, `tool_section`, `action`, `parent_id`, `toolmode_id`, `variable`, `ordre`, `link`, `process`, `bin`, `read1`, `write1`) VALUES (20400, 'sellpoint', 'product', 'sellpoint', '', 0, 1, 'SELLPOINT_MENU_ROOT', 3, '20403', '', 0, ',1,', ',1,');
INSERT INTO `all__menu` (`menu_id`, `menu_group`, `tool`, `tool_section`, `action`, `parent_id`, `toolmode_id`, `variable`, `ordre`, `link`, `process`, `bin`, `read1`, `write1`) VALUES (20401, 'sellpoint', 'product', 'sellpoint', '', 20400, 1, 'SELLPOINT_MENU_SELLPOINT', 1, '', '', 0, ',1,', ',1,');
INSERT INTO `all__menu` (`menu_id`, `menu_group`, `tool`, `tool_section`, `action`, `parent_id`, `toolmode_id`, `variable`, `ordre`, `link`, `process`, `bin`, `read1`, `write1`) VALUES (20402, 'sellpoint', 'product', 'sellpoint', 'show_form_new', 20401, 1, 'SELLPOINT_MENU_SELLPOINT_NEW', 1, '', '', 0, ',1,', ',1,');
INSERT INTO `all__menu` (`menu_id`, `menu_group`, `tool`, `tool_section`, `action`, `parent_id`, `toolmode_id`, `variable`, `ordre`, `link`, `process`, `bin`, `read1`, `write1`) VALUES (20403, 'sellpoint', 'product', 'sellpoint', 'list_records', 20401, 1, 'SELLPOINT_MENU_SELLPOINT_LIST', 2, '', '', 0, ',1,', ',1,');
INSERT INTO `all__menu` (`menu_id`, `menu_group`, `tool`, `tool_section`, `action`, `parent_id`, `toolmode_id`, `variable`, `ordre`, `link`, `process`, `bin`, `read1`, `write1`) VALUES (20404, 'sellpoint', 'product', 'sellpoint', 'list_records_bin', 20401, 1, 'SELLPOINT_MENU_SELLPOINT_BIN', 3, '', '', 0, ',1,', ',1,');
INSERT INTO `all__menu` (`menu_id`, `menu_group`, `tool`, `tool_section`, `action`, `parent_id`, `toolmode_id`, `variable`, `ordre`, `link`, `process`, `bin`, `read1`, `write1`) VALUES (20405, 'sellpoint', 'product', 'sellpoint', 'show_form_new', 20402, 1, 'SELLPOINT_MENU_SELLPOINT_NEW', 1, '', '', 0, ',1,', ',1,');
INSERT INTO `all__menu` (`menu_id`, `menu_group`, `tool`, `tool_section`, `action`, `parent_id`, `toolmode_id`, `variable`, `ordre`, `link`, `process`, `bin`, `read1`, `write1`) VALUES (20406, 'sellpoint', 'product', 'sellpoint', 'list_records_images', 20402, 1, 'SELLPOINT_MENU_SELLPOINT_NEW', 2, '', '', 0, ',1,', ',1,');
INSERT INTO `all__menu` (`menu_id`, `menu_group`, `tool`, `tool_section`, `action`, `parent_id`, `toolmode_id`, `variable`, `ordre`, `link`, `process`, `bin`, `read1`, `write1`) VALUES (20407, 'sellpoint', 'product', 'sellpoint', 'show_form_new', 20403, 1, 'SELLPOINT_MENU_SELLPOINT_NEW', 1, '', '', 0, ',1,', ',1,');
INSERT INTO `all__menu` (`menu_id`, `menu_group`, `tool`, `tool_section`, `action`, `parent_id`, `toolmode_id`, `variable`, `ordre`, `link`, `process`, `bin`, `read1`, `write1`) VALUES (20408, 'sellpoint', 'product', 'sellpoint', 'list_records_images', 20403, 1, 'SELLPOINT_MENU_SELLPOINT_NEW', 2, '', '', 0, ',1,', ',1,');

					";
	
	$update_sql .= "CREATE TABLE `product__configadmin_sellpoint` (
	`configadmin_id` INT(11) NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(50) NOT NULL COLLATE 'utf8_spanish2_ci',
	`value` VARCHAR(255) NOT NULL COLLATE 'utf8_spanish2_ci',
	PRIMARY KEY (`configadmin_id`)
	)
	COLLATE='utf8_spanish2_ci'
	ENGINE=MyISAM;";
	$update_sql .= "INSERT INTO `product__configadmin_sellpoint` (`configadmin_id`, `name`, `value`) VALUES (5, 'im_big_w', '1200');
INSERT INTO `product__configadmin_sellpoint` (`configadmin_id`, `name`, `value`) VALUES (6, 'im_big_h', '800');
INSERT INTO `product__configadmin_sellpoint` (`configadmin_id`, `name`, `value`) VALUES (8, 'im_details_w', '495');
INSERT INTO `product__configadmin_sellpoint` (`configadmin_id`, `name`, `value`) VALUES (9, 'im_details_h', '330');
INSERT INTO `product__configadmin_sellpoint` (`configadmin_id`, `name`, `value`) VALUES (10, 'im_thumb_w', '200');
INSERT INTO `product__configadmin_sellpoint` (`configadmin_id`, `name`, `value`) VALUES (11, 'im_thumb_h', '133');
INSERT INTO `product__configadmin_sellpoint` (`configadmin_id`, `name`, `value`) VALUES (14, 'im_thumb_q', '80');
INSERT INTO `product__configadmin_sellpoint` (`configadmin_id`, `name`, `value`) VALUES (15, 'im_details_q', '80');
INSERT INTO `product__configadmin_sellpoint` (`configadmin_id`, `name`, `value`) VALUES (16, 'im_list_cols', '2');
INSERT INTO `product__configadmin_sellpoint` (`configadmin_id`, `name`, `value`) VALUES (20, 'im_medium_w', '900');
INSERT INTO `product__configadmin_sellpoint` (`configadmin_id`, `name`, `value`) VALUES (21, 'im_medium_h', '600');
INSERT INTO `product__configadmin_sellpoint` (`configadmin_id`, `name`, `value`) VALUES (22, 'im_medium_q', '80');
INSERT INTO `product__configadmin_sellpoint` (`configadmin_id`, `name`, `value`) VALUES (23, 'im_big_q', '70');
INSERT INTO `product__configadmin_sellpoint` (`configadmin_id`, `name`, `value`) VALUES (49, 'im_custom_ratios', '');

CREATE TABLE `product__sellpoint` (
	`sellpoint_id` INT(11) NOT NULL AUTO_INCREMENT,
	`empresa` VARCHAR(255) NOT NULL COLLATE 'utf8_spanish2_ci',
	`adress` VARCHAR(255) NOT NULL COLLATE 'utf8_spanish2_ci',
	`poblacio` VARCHAR(100) NOT NULL COLLATE 'utf8_spanish2_ci',
	`cp` CHAR(50) NOT NULL COLLATE 'utf8_spanish2_ci',
	`municipi_id` INT(11) NOT NULL,
	`comarca_id` INT(11) NOT NULL,
	`provincia_id` INT(11) NOT NULL,
	`telefon` VARCHAR(50) NOT NULL COLLATE 'utf8_spanish2_ci',
	`telefon2` VARCHAR(50) NOT NULL COLLATE 'utf8_spanish2_ci',
	`fax` VARCHAR(50) NOT NULL COLLATE 'utf8_spanish2_ci',
	`correu` VARCHAR(255) NOT NULL COLLATE 'utf8_spanish2_ci',
	`correu2` VARCHAR(255) NOT NULL COLLATE 'utf8_spanish2_ci',
	`bin` TINYINT(2) NOT NULL DEFAULT '0',
	`latitude` DECIMAL(9,7) NOT NULL DEFAULT '0.0000000',
	`longitude` DECIMAL(9,7) NOT NULL DEFAULT '0.0000000',
	`zoom` TINYINT(1) NOT NULL DEFAULT '0',
	`status` ENUM('prepare','review','public','archived') NOT NULL DEFAULT 'prepare' COLLATE 'utf8_spanish2_ci',
	PRIMARY KEY (`sellpoint_id`)
)
COLLATE='utf8_spanish2_ci'
ENGINE=MyISAM;

CREATE TABLE `product__sellpoint_image` (
	`image_id` INT(11) NOT NULL AUTO_INCREMENT,
	`sellpoint_id` INT(11) NOT NULL DEFAULT '0',
	`name` VARCHAR(100) NOT NULL DEFAULT '' COLLATE 'utf8_spanish2_ci',
	`size` INT(11) NOT NULL DEFAULT '0',
	`ordre` INT(11) NOT NULL DEFAULT '0',
	`main_image` INT(1) NOT NULL DEFAULT '0',
	`imagecat_id` INT(11) NOT NULL DEFAULT '1',
	PRIMARY KEY (`image_id`)
)
COLLATE='utf8_spanish2_ci'
ENGINE=MyISAM;

CREATE TABLE `product__sellpoint_imagecat` (
	`imagecat_id` INT(11) NOT NULL AUTO_INCREMENT,
	`imagecat` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_spanish2_ci',
	`ordre` INT(11) NULL DEFAULT NULL,
	PRIMARY KEY (`imagecat_id`)
)
COLLATE='utf8_spanish2_ci'
ENGINE=InnoDB
ROW_FORMAT=COMPACT;

CREATE TABLE `product__sellpoint_image_language` (
	`image_id` INT(11) NOT NULL DEFAULT '0',
	`language` CHAR(3) NOT NULL COLLATE 'utf8_spanish2_ci',
	`image_alt` VARCHAR(150) NOT NULL COLLATE 'utf8_spanish2_ci',
	`image_title` VARCHAR(255) NOT NULL COLLATE 'utf8_spanish2_ci'
)
COLLATE='utf8_spanish2_ci'
ENGINE=MyISAM;

CREATE TABLE `product__sellpoint_language` (
	`sellpoint_id` INT(11) NOT NULL DEFAULT '0',
	`language` CHAR(3) NOT NULL DEFAULT '' COLLATE 'utf8_spanish2_ci',
	`sellpoint` TEXT NOT NULL COLLATE 'utf8_spanish2_ci',
	`sellpoint_description` TEXT NOT NULL COLLATE 'utf8_spanish2_ci',
	`sellpoint_file_name` VARCHAR(100) NOT NULL DEFAULT '' COLLATE 'utf8_spanish2_ci',
	`sellpoint_old_file_name` VARCHAR(100) NOT NULL DEFAULT '' COLLATE 'utf8_spanish2_ci',
	`url1` VARCHAR(255) NOT NULL DEFAULT '' COLLATE 'utf8_spanish2_ci',
	`url1_name` VARCHAR(100) NOT NULL DEFAULT '' COLLATE 'utf8_spanish2_ci'
)
COLLATE='utf8_spanish2_ci'
ENGINE=MyISAM;

";
}

?>