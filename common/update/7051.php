<?php
/* 30-7-18
Afegir mínim despesa per país o provincia

*/


if ( is_table( 'product__rate' ) && ! is_field( 'product__rate', 'minimum_buy_free_send' ) ) {

	$update_sql[] = "ALTER TABLE `product__rate`
	ADD COLUMN `minimum_buy_free_send` DECIMAL(8,2) NULL DEFAULT '0.00' NULL AFTER `rate_encrease_ondelivery`;";

}
else {
	if ( is_table( 'product__rate' ) ) {
		trigger_error( 'Ja existeixen el camp minimum_buy_free_send' );
	}
}

if (is_table('product__product')){

	$update_sql[] = "INSERT INTO `all__menu` (`menu_id`, `menu_group`, `tool`, `tool_section`, `action`, `parent_id`, `toolmode_id`, `variable`, `ordre`, `link`, `process`, `bin`, `read1`, `write1`) VALUES (20135, 'customer', 'product', 'country', 'list_records', 20131, 1, 'CUSTOMER_MENU_COUNTRY_LIST', 4, '', '', 0, ',1,2,', ',1,');";

}

