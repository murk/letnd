<?php
/**
 * AdmintotalMls
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2011
 * @version $Id$
 * @access public
 */
class AdmintotalMls extends Module {

	var $conta = 0;
	var $is_cli = false;

	function __construct() {
		include_once( 'config_mls.php' ); // està al path includes
		parent::__construct();
	}

	function _start() {

		Debug::add(date( "d-m-Y H:i" ),"");

		set_time_limit( 60 * 60 );

		echo '<link href="/admin/themes/inmotools/styles/export.css" rel="stylesheet" type="text/css">';

		Db::connect_mother();

		$this->copy_mls();

		$this->_mls_get_fields_array();

		send_mail_admintotal('MLS - Exportació finalitzada', 'Data: ' . now(true));

	}

	function copy_mls() {
		// 1er_recullo de la bd de dades de client__client tots els que volen MLS, es a dir que serà 1
		$query   = "SELECT client_id, dbname, domain FROM letnd.client__client WHERE MLS = 1";
		$results = Db::get_rows( $query );

		// Debug::p( $results, 'Clients' );
		$this->p( 'Truncates', 1 );

		// 2on buido tota la taula inmo__property i la inmo__property_language
		$query = 'TRUNCATE letnd.inmo__property' ;
		Db::execute( $query );
		$this->p( $query, 2 );

		$query = 'TRUNCATE letnd.inmo__property_language' ;
		Db::execute( $query );
		$this->p( $query, 2 );

		$query = 'TRUNCATE letnd.inmo__property_image' ;
		Db::execute( $query );
		$this->p( $query, 2 );

		$query = 'TRUNCATE letnd.inmo__property_image_language' ;
		Db::execute( $query );
		$this->p( $query, 2 );

		// 3er copio
		// Agafo totes les propietats que tenen MLS = 1 i els estat es "onsale"

		$camps_iguals = $this->mls_get_insert_fields();

		$this->p( 'Immo property', 1 );

		foreach ( $results as $rs ) {
			$query = "
			INSERT INTO letnd.inmo__property
				(property_id_client, client_id, domain, status, " . $camps_iguals . ")
			SELECT
				property_id, '" . $rs['client_id'] . "', '" . $rs['domain'] . "', 'onsale', " . $camps_iguals . "
				FROM `" . $rs['dbname'] . "`.inmo__property
				WHERE status = 'onsale'
				AND MLS = '1'";

			Db::execute( $query );
			$this->p( $query, 2 );

			// inserto idiomes
			$query = "
			INSERT INTO 
				letnd.inmo__property_language (
					letnd.inmo__property_language.property_id, 
					letnd.inmo__property_language.language, 
					letnd.inmo__property_language.description, 
					letnd.inmo__property_language.property, 
					letnd.inmo__property_language.property_title,
					letnd.inmo__property_language.page_title, 
					letnd.inmo__property_language.page_description, 
					letnd.inmo__property_language.page_keywords, 
					letnd.inmo__property_language.property_file_name, 
					letnd.inmo__property_language.property_old_file_name
					)
			SELECT 
					letnd.inmo__property.property_id, 
					`" . $rs['dbname'] . "`.inmo__property_language.language, 
					`" . $rs['dbname'] . "`.inmo__property_language.description, 
					`" . $rs['dbname'] . "`.inmo__property_language.property, 
					`" . $rs['dbname'] . "`.inmo__property_language.property_title,
					`" . $rs['dbname'] . "`.inmo__property_language.page_title, 
					`" . $rs['dbname'] . "`.inmo__property_language.page_description, 
					`" . $rs['dbname'] . "`.inmo__property_language.page_keywords, 
					`" . $rs['dbname'] . "`.inmo__property_language.property_file_name, 
					`" . $rs['dbname'] . "`.inmo__property_language.property_old_file_name
				FROM `" . $rs['dbname'] . "`.inmo__property_language, letnd.inmo__property
				WHERE letnd.inmo__property.property_id_client = `" . $rs['dbname'] . "`.inmo__property_language.property_id
				AND letnd.inmo__property.client_id = " . $rs['client_id'] . "
 ";
			Db::execute( $query );
			$this->p( $query, 2 );
			//Debug::p($query);


			// inserto imatges
			$query = "INSERT INTO
						letnd.inmo__property_image
					SELECT image_id, letnd.inmo__property.property_id, name, size, ordre, main_image
						FROM `" . $rs['dbname'] . "`.inmo__property_image, letnd.inmo__property
						WHERE letnd.inmo__property.property_id_client = `" . $rs['dbname'] . "`.inmo__property_image.property_id
						AND letnd.inmo__property.client_id = " . $rs['client_id'] . "
 ";

			Db::execute( $query );
			$this->p( $query, 2 );
			// Debug::p($query);

			// NO ES REAL, NOMES SERVEIX PER LLISTAR, S'HAURIEN dE CANVIAR IDS PER QUE HI HA REPETITS
			// inserto imatges
			$query = "
			INSERT INTO letnd.inmo__property_image_language
				(
				letnd.inmo__property_image_language.image_id, 
				letnd.inmo__property_image_language.language)
			SELECT 
				inmo__property_image_language.image_id, 
				inmo__property_image_language.language
			FROM `" . $rs['dbname'] . "`.inmo__property_image_language, letnd.inmo__property_image
			WHERE letnd.inmo__property_image.image_id = `" . $rs['dbname'] . "`.inmo__property_image_language.image_id
			";

			Db::execute( $query );
			$this->p( $query, 2 );

		}
		if (!$this->is_cli) Db::reconnect();
	}

	function mls_get_insert_fields() {
		$fields = array(
			//'property_id',
			//'property_id_client',
			//'client_id',
			//'domain',
			'ref',
			'ref_number',
			//'ref_collaborator',
			//'ref_cadastre',
			'cedula',
			//'property_private',
			//'adress',
			'place_id',
			//'numstreet',
			//'block',
			//'flat',
			//'door',
			//'zip',
			//'numregister',
			//'locationregister',
			//'tomregister',
			//'bookregister',
			//'pageregister',
			//'numberpropertyregister',
			//'inscriptionregister',
			//'descriptionregister',
			'construction_date',
			//'observations',
			'exclusive',
			'tipus',
			'tipus2',
			'category_id',
			'floor_space',
			'floor_space_build',
			'land',
			'land_units',
			'country_id',
			'provincia_id',
			'comarca_id',
			'municipi_id',
			'zone_id',
			'price',
			'price_consult',
			//'price_private',
			'price_tmp',
			//'owner_id',
			'entered',
			//'bin',
			//'facebook_post_id',
			//'facebook_image_id',
			//'facebook_publish_id',
			//'facebook_letnd_user_id',
			//'facebook_user_id',
			//'twitter_status_id',
			//'twitter_letnd_user_id',
			//'twitter_user_id',
			//'status',
			'expired',
			'efficiency',
			'efficiency_number',
			'efficiency2',
			'efficiency_number2',
			'efficiency_date',
			'parket',
			'laminated',
			'gres',
			'marble',
			'ceramic',
			'estrato',
			//'home',
			'offer',
			'room',
			'bathroom',
			'wc',
			'living',
			'dinning',
			'furniture',
			'laundry',
			'parking',
			'parking_area',
			'garage',
			'garage_area',
			'storage',
			'storage_area',
			'balcony',
			'terrace',
			'terrace_area',
			'office',
			'office_area',
			'property_height',
			'elevator',
			'garden',
			'garden_area',
			'swimmingpool',
			'swimmingpool_area',
			'citygas',
			'centralheating',
			'airconditioned',
			'service_room',
			'service_bath',
			'study',
			'courtyard',
			'courtyard_area',
			'sea_view',
			'clear_view',
			//'blocked',
			//'comission',
			//'mls',
			'latitude',
			'longitude',
			'center_latitude',
			'center_longitude',
			'zoom',
			'show_map',
			'show_map_point',
			//'custom1',
			//'custom2',
			//'custom3',
			//'custom4',
			'person',
			'facing',
			'videoframe',
			//'user_id',
			'level_count',
			'hut',
			'time_in',
			'time_out',
			//'managed_by_owner',
			'star',
			'community_expenses',
			'ibi',
			'municipal_tax',
			'sea_distance'
		);

		return '`' . implode( '`,`', $fields ) . '`';
	}

	// Nomes utitlitzar mentres programo per copy paste a mls_get_insert_fields
	// Actualitzar taula letnd.inmo__property, posar a exclude els que no vull i cridar aquesta funció que em genera l'array de dalt
	// es per poder generar array dels fields
	function _mls_get_fields_array() {


		$exclude_fields = array(
			'property_id',
			'property_id_client',
			'client_id',
			'domain',
			'ref_collaborator',
			'ref_cadastre',
			'property_private',
			'adress',
			'numstreet',
			'block',
			'flat',
			'door',
			'zip',
			'numregister',
			'locationregister',
			'tomregister',
			'bookregister',
			'pageregister',
			'numberpropertyregister',
			'inscriptionregister',
			'descriptionregister',
			'observations',
			'price_private',
			'owner_id',
			'bin',
			'facebook_post_id',
			'facebook_image_id',
			'facebook_publish_id',
			'facebook_letnd_user_id',
			'facebook_user_id',
			'twitter_status_id',
			'twitter_letnd_user_id',
			'twitter_user_id',
			'status',
			'home',
			'blocked',
			'comission',
			'mls',
			'custom1',
			'custom2',
			'custom3',
			'custom4',
			'user_id',
			'managed_by_owner'
		);


		$query   = "SHOW COLUMNS FROM letnd.inmo__property";
		$results = Db::get_rows( $query );
		echo "<br><br><pre>Camps a letnd.inmo__property:\n\narray(\n";
		foreach ( $results as $rs ) {
			extract( $rs );
			echo "\t";
			if ( in_array( $Field, $exclude_fields ) ) {
				echo "//";
			}
			echo "'$Field',\n";
		}
		echo ');</pre>';
	}


	function p( $txt, $level ) {

		if ( $level == 2 ) {
			$this->conta ++;
			$txt = '<em>' . $this->conta . '</em> ' . $txt . ' - <i>(' . $execution_time = Debug::get_execution_time() . ' s.) </i>';
		}
		if ( $level == 'error' ) {
			$txt .= ' (error)';
		}
		echo '<p class="m' . $level . '">' . $txt . '</p>';
		//@ob_flush();
		if ( $this->conta % 1000 == 0 && $this->conta != 0 ) {
			flush();
		}
	}
}
?>