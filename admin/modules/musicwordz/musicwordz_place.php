<?
/**
 * MusicwordzPlace
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2016
 * @version $Id$
 * @access public
 */
class MusicwordzPlace extends Module{

	function __construct(){
		parent::__construct();
	}

	function on_load() {

		Main::load_class( 'musicwordz','common' );
		MusicwordzCommon::add_dynamic_missatges( $this->caption );
		
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function get_records()
	{
		$this->set_field( 'place', 'class', '' );
		$this->set_field( 'address', 'class', '' );
		$this->set_field( 'municipi_id', 'class', '' );
		$this->set_field( 'zip', 'class', '' );
		$this->set_field( 'phone1', 'text_size', '' );
		$this->set_field( 'mail', 'text_size', '' );

	    $listing = new ListRecords($this);
		MusicwordzCommon::set_listing( $listing );

	    return $listing->list_records();
	}
	function records_walk_artist(&$listing){
		MusicwordzCommon::records_walk_artist($listing);
	}
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{
	    $show = new ShowForm($this);
		MusicwordzCommon::set_show( $show );

		$show->form_level1_titles = array( 'entered' );

		if ( MusicwordzCommon::is_artist() ) {
			$show->unset_field('zip');
			$show->unset_field('country_id');
			$show->unset_field('provincia_id');
			$show->unset_field('comarca_id');
			$show->unset_field('municipi_id');
			$show->unset_field('phone1');
			$show->unset_field('mail');
		}

	    return $show->show_form();
	}

	function save_rows()
	{
	    $save_rows = new SaveRows($this);
		$save_rows->field_unique = "place";
	    $save_rows->save();
	}

	function write_record()
	{
	    $writerec = new SaveRows($this);
		MusicwordzCommon::set_writerec( $writerec );
		$writerec->field_unique = "place";
	    $writerec->save();
	}

	function manage_images()
	{
	    $image_manager = new ImageManager($this);
	    $image_manager->execute();
	}
	public function get_select_long_values() {
		{

			//	$field = R::get( 'field' );
			//	$id = R::get( 'id' );
			$limit = 100;

			$q = R::escape( 'query' );

			$ret                = array();
			$ret['suggestions'] = array();

			$r = &$ret['suggestions'];

			//	if ($q)
			//	{
			// 1 - Comarca, que comenci
			$query = "SELECT comarca_id, comarca, '0' AS municipi_id, '' AS municipi
					FROM inmo__comarca
					WHERE comarca LIKE '" . $q . "%'
					ORDER BY comarca
					LIMIT " . $limit;

			$results = Db::get_rows( $query );

			$rest = $limit - count( $results );

			// 2 - Comarca, qualsevol posició
			if ( $rest > 0 ) {

				$query = "SELECT comarca_id, comarca, '0' AS municipi_id, '' AS municipi
						FROM inmo__comarca
						WHERE comarca LIKE '%" . $q . "%'
			            AND comarca NOT LIKE '" . $q . "%'
						ORDER BY comarca
						LIMIT " . $rest;

				$results2 = Db::get_rows( $query );

				$results = array_merge( $results, $results2 );

			}


			$rest = $limit - count( $results );

			// 3 - Municipis, que comenci
			if ( $rest > 0 ) {
				$query = "SELECT municipi_id, municipi, comarca_id, comarca
				FROM inmo__municipi
				INNER JOIN inmo__comarca USING (comarca_id)
				WHERE municipi LIKE '" . $q . "%'
				ORDER BY municipi
				LIMIT " . $rest;

				$results2 = Db::get_rows( $query );

				$results = array_merge( $results, $results2 );
			}

			$rest = $limit - count( $results );

			// 4 - Municipis, qualsevol posició i a comarca
			if ( $rest > 0 ) {
				$query = "SELECT municipi_id, municipi, comarca_id, comarca
				FROM inmo__municipi
				INNER JOIN inmo__comarca USING (comarca_id)
				WHERE
						(municipi LIKE '%" . $q . "%'
						AND municipi NOT LIKE '" . $q . "%')
			        OR
						(comarca LIKE '%" . $q . "%')
				ORDER BY municipi
				LIMIT " . $rest;

				$results2 = Db::get_rows( $query );

				$results = array_merge( $results, $results2 );
			}

			// Presento resultats
			foreach ( $results as $rs ) {
				$value = $rs['municipi'] ?
					$rs['municipi'] . " - " . $rs['comarca'] :
					$rs['comarca'];
				$r[]   = array(
					'value' => $value,
					'data'  => $rs['municipi_id']
				);
			}
			//	}


			$rest = $limit - count( $results );
			$ret['is_complete'] = $rest>0;

			die ( json_encode( $ret ) );


		}

	}

}
?>