<script language="JavaScript" src="/public/jscripts/ahorro.js"></script>
<script language="JavaScript" src="/public/jscripts/hcomun.js"></script>
<script language="JavaScript" src="/public/jscripts/gastos.js"></script>
<link href="../../templates/styles/general.css" rel="stylesheet" type="text/css" /><table width="85%" border="0" cellpadding="0" cellspacing="0" class="fonsgeneralsclars">
  <tr>
    <td><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="10" height="10" /></td>
    <td><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="10" height="10" /></td>
    <td><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="10" height="10" /></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td align="left"><strong class="text20verdanaboldcolor">CALCULATE PROPERTY PURCHASE EXPENSES</strong> </td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="10" height="10" /></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="15"><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="15" height="10" /></td>
          <td align="left"><span class="text10verdanawhite">This calculator allows you  to calculate the expenses you will incur on purchasing you home, plus the costs  of taking out a mortgage. <br>
            <br/>
            If you only want to look at  the expenses involved in buying your home, leave the &ldquo;amount requested&rdquo; field  blank. If you only want to find out the costs of taking out a mortgage, leave  the &ldquo;Home price&rdquo; field blank. </span></td>
        </tr>
      </table></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="10" height="25" /></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td width="10" align="left"><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="10" height="1" /></td>
    <td align="left"><form name="formu" action="">
        <table width="70%" border="0" cellpadding="0" cellspacing="2">
          <tr>
            <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr class="detalletabla1">
                  <td width="15"><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="15" height="10" /></td>
                  <td width="200" align="left">Amount requested</td>
                  <td align="left"><input name="importeh" class="formulari" id="importeh" onKeyPress="PonPuntos(event, this, 'positive',11,'c');" size="25"></td>
                </tr>
                <tr>
                  <td height="7"><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="10" height="7" /></td>
                  <td height="7" align="left"><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="10" height="7" /></td>
                  <td height="7" align="left"><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="10" height="7" /></td>
                </tr>
                <tr>
                  <td width="15">&nbsp;</td>
                  <td width="200" align="left">Autonomous Community</td>
                  <td align="left"><select name="provinciah" class="select" id="provinciah" style="width: 120px;">
                      <option value="0.04">ALAVA</option>
                      <option value="0.06">ANDALUCIA</option>
                      <option value="0.07">ARAGON</option>
                      <option value="0.06">ASTURIAS</option>
                      <option value="0.06">C. VALENCIANA</option>
                      <option value="0.06">CANARIAS</option>
                      <option value="0.06">CANTABRIA</option>
                      <option value="0.06">CASTILLA LA MANCHA</option>
                      <option value="0.06">CASTILLA Y LEON</option>
                      <option value="0.07" selected="selected">CATALUNYA</option>
                      <option value="0.06">CEUTA Y MELILLA</option>
                      <option value="0.07">EXTREMADURA</option>
                      <option value="0.07">GALICIA</option>
                      <option value="0.06">GUIPUZCOA</option>
                      <option value="0.06">ILLES BALEARS</option>
                      <option value="0.06">LA RIOJA</option>
                      <option value="0.07">MADRID</option>
                      <option value="0.06">NAVARRA</option>
                      <option value="0.07">REGION DE MURCIA</option>
                      <option value="0.06">VIZCAYA</option>
                  </select></td>
                </tr>
                <tr>
                  <td height="7"><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="10" height="7" /></td>
                  <td height="7" align="left"><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="10" height="7" /></td>
                  <td height="7" align="left"><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="10" height="7" /></td>
                </tr>
                <tr>
                  <td width="15">&nbsp;</td>
                  <td width="200" align="left">Home price  (<?=CURRENCY_NAME?>) </td>
                  <td align="left"><input class="formulari" name="valorh" onKeyPress="PonPuntos(event, this, 'positive',11,'c');" size="25" value=""></td>
                </tr>
                <tr>
                  <td width="15" height="7"><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="10" height="7" /></td>
                  <td width="200" height="7" align="left"><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="10" height="7" /></td>
                  <td height="7" align="left"><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="10" height="7" /></td>
                </tr>
                <tr>
                  <td width="15">&nbsp;</td>
                  <td width="200" align="left">Home</td>
                  <td align="left"><table border="0" cellpadding="3" cellspacing="0" width="200">
                      <tr class="detalletabla1">
                        <td><input class="noborder" style="border:0px;" checked="checked" name="radioviv" value="nueva" type="radio"></td>
                        <td>New</td>
                        <td><input class="formulari" style="border:0px;" name="radioviv" value="usada" type="radio"></td>
                        <td>Pre-owned</td>
                      </tr>
                  </table></td>
                </tr>
                <tr>
                  <td width="15" height="7"><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="10" height="7" /></td>
                  <td width="200" height="7" align="left"><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="10" height="7" />
                  <input  style="border:0px;" name="radio"  value="pts" type="hidden">
                  <input style="border:0px;" name="radio" type="hidden"  value="euro" checked></td>
                  <td height="7" align="left"><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="10" height="7" /></td>
                </tr>
              </table></td>
          </tr>
          <tr>
            <td colspan="3" align="center"><table border="0" cellpadding="3" cellspacing="0" width="20%">
                <tr class="detalletabla1">
                  <td><img src="/clients/agenciaportselva/templates/images/bcalcular.gif" width="19" height="19" alt="calcular" onclick="return calcular_onclick('t')" style="cursor:pointer"></td>
                  <td width="5"><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="5" height="7" /></td>
                  <td align="left" valign="middle"><strong>calculate</strong></td>
                  <td width="10"><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="20" height="7" /></td>
                  <td><img alt="imprimir" src="/clients/agenciaportselva/templates/images/bprint.gif" width="19" height="19" onClick="javascript:window.print()" style="cursor:pointer" /></td>
                  <td width="5"><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="5" height="7" /></td>
                  <td height="19" align="left" valign="middle"><strong>print</strong></td>
                </tr>
              </table></td>
          </tr>
        </table>
      </form></td>
    <td width="10"><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="10" height="1" /></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="10" height="25" /></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><form name="f_result" action="">
        <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="detalletabla1">
          <tr class="detalletabla1">
            <td align="left" valign="top" width="45%"><table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr align="left">
                  <td height="15" colspan="2" class="text15verdanaboldcolor"><p><strong>Mortgage  expenses</strong>: </p>
                  </td>
                </tr>
                <tr class="detalletabla1">
                  <td align="left"><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="10" height="10" /></td>
                  <td align="center"><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="10" height="10" /></td>
                </tr>
                <tr class="detalletabla1">
                  <td align="left">Commitment fee: </td>
                  <td align="center"><input class="formulari" maxlength="11" name="comision" readonly="readonly" size="11"></td>
                </tr>
                <tr class="detalletabla1">
                  <td align="left">Valuation:</td>
                  <td align="center"><input class="formulari" maxlength="11" name="tasacion" readonly="readonly" size="11"></td>
                </tr>
                <tr class="detalletabla1">
                  <td align="left">Stamp duty:</td>
                  <td align="center"><input class="formulari" maxlength="11" name="impuesto" readonly="readonly" size="11"></td>
                </tr>
                <tr class="detalletabla1">
                  <td align="left">Notary:</td>
                  <td align="center"><input class="formulari" maxlength="11" name="notario" readonly="readonly" size="11"></td>
                </tr>
                <tr class="detalletabla1">
                  <td align="left">Accountant:</td>
                  <td align="center"><input class="formulari" maxlength="11" name="gestion" readonly="readonly" size="11"></td>
                </tr>
                <tr class="detalletabla1">
                  <td align="left">Property register:</td>
                  <td align="center"><input class="formulari" maxlength="11" name="registro" readonly="readonly" size="11"></td>
                </tr>
                <tr class="detalletabla1">
                  <td align="left">VAT (notary, accountant,  register):</td>
                  <td align="center"><input class="formulari" maxlength="11" name="iva" readonly="readonly" size="11"></td>
                </tr>
                <tr class="detalletabla1">
                  <td align="left">Insurance:</td>
                  <td align="center"><input class="formulari" maxlength="11" name="seguro" readonly="readonly" size="11"></td>
                </tr>
                <tr>
                  <td align="left"><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="15" height="10" /></td>
                  <td align="center"><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="15" height="10" /></td>
                </tr>
                <tr class="detalletabla1">
                  <td align="left"><b>Total mortgage:</b></td>
                  <td align="center"><input class="formulari" maxlength="11" name="total_hipo" readonly="readonly" size="11" value="0"></td>
                </tr>
              </table></td>
            <td width="15"><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="15" height="10" /></td>
            <td align="right" valign="top" width="45%"><table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr align="left">
                  <td height="15" colspan="2" class="text15verdanaboldcolor"><p><strong>Costs of  buying with mortgage</strong>:</p>
                  </td>
                </tr>
                <tr align="left" class="detalletabla1">
                  <td height="9" colspan="2">&nbsp;</td>
                </tr>
                <tr align="left" valign="middle" class="detalletabla1">
                  <td height="9" colspan="2"><b class="text10verdanaboldcolor">New home :</b></td>
                </tr>
                <tr class="detalletabla1">
                  <td align="left">VAT on home </td>
                  <td align="center"><input class="formulari" maxlength="11" name="ivaviv" readonly="readonly" size="11"></td>
                </tr>
                <tr class="detalletabla1">
                  <td align="left">Stamp duty:</td>
                  <td align="center"><input class="formulari" maxlength="11" name="impuesto2" readonly="readonly" size="11"></td>
                </tr>
                <tr align="left" class="detalletabla1">
                  <td><b class="text10verdanaboldcolor">Pre-owned home :</b></td>
                </tr>
                <tr class="detalletabla1">
                  <td align="left">Imp. Tran. Patrimonials</td>
                  <td align="center"><input class="formulari" maxlength="11" name="itp" readonly="readonly" size="11">
                  </td>
                </tr>
                <tr align="left" class="detalletabla1">
                  <td><b class="text10verdanaboldcolor">Capital transfer tax :</b></td>
                </tr>
                <tr class="detalletabla1">
                  <td align="left">Notary:</td>
                  <td align="center"><input class="formulari" maxlength="11" name="notariocv" readonly="readonly" size="11"></td>
                </tr>
                <tr class="detalletabla1">
                  <td align="left">Property register: </td>
                  <td align="center"><input class="formulari" maxlength="11" name="registrocv" readonly="readonly" size="11"></td>
                </tr>
                <tr class="detalletabla1">
                  <td align="left">Accountant:</td>
                  <td align="center"><input class="formulari" maxlength="11" name="gestioncv" readonly="readonly" size="11"></td>
                </tr>
                <tr class="detalletabla1">
                  <td align="left">VAT (notary, account, regiter) :</td>
                  <td align="center"><input class="formulari" maxlength="11" name="ivacv" readonly="readonly" size="11"></td>
                </tr>
                <tr class="detalletabla1">
                  <td align="left"><b>Total purchase:</b></td>
                  <td align="center"><input class="formulari" maxlength="11" name="total_compra" readonly="readonly" size="11" value="0"></td>
                </tr>
                <tr class="detalletabla1">
                  <td align="left"><b>Total general:</b></td>
                  <td align="center"><input class="formulari" maxlength="11" name="total" readonly="readonly" size="11" value="0"></td>
                </tr>
              </table></td>
          </tr>
        </table>
      </form></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="10" height="25" /></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td align="left">The information obtained  from this simulation is for guidance purposes only and does not imply any  undertaking nor any legal or other relationship.. </td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="10" height="40" /></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td align="center"><table width="218" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td><a href="?page_id=8&language=eng&tipus=sell" class="noborder"><img src="/clients/agenciaportselva/templates/images/bhipotecaeng.gif" alt="calcul de quotes" width="89" height="18" border="0" /></a></td>
        <td width="40"><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="40" height="1" /></td>
        <td width="19"><a href="?page_id=11&language=eng&tipus=sell" class="noborder"><img src="/clients/agenciaportselva/templates/images/bestalvieng.gif" alt="estalvi fiscal" width="89" height="18" border="0" /></a></td>
        </tr>
    </table></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="10" height="14" /></td>
    <td>&nbsp;</td>
  </tr>
</table>