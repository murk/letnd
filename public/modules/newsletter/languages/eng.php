<?
if (!defined('NEWSLETTER_ADDRESSES_MESSAGE_ADDED')){
define('NEWSLETTER_ADDRESSES_MESSAGE_ADDED','Gràcies per inscriure\'t a la nostra llista');
define('NEWSLETTER_ADDRESSES_MESSAGE_ADD_EXISTS','Aquesta adreça de mail ja figura a la base de dades!');
}


$gl_caption['c_title_edit'] = 'Personal data';
$gl_caption['c_title_register'] = 'Register newsletter';
$gl_caption['c_title_login'] = 'Login newsletter';

$gl_caption ['c_register']='Register';

$gl_caption_customer ['c_mail_log']='Insert your e-mail';
$gl_caption_customer ['c_password_log']='Insert your password';

$gl_caption['c_password_forgotten'] = '¿have you forgotten the password?';
$gl_caption['c_password_insert_mail'] = 'Please enter your email address and send you a new password to your email account.';
$gl_caption['c_password_mail_error'] = 'The email address provided does not exist in our database, please write the address correctly';
$gl_caption['c_password_sent'] = 'It has sent a new password to your email address';
$gl_caption['c_password_mail_subject'] = 'Your new password';
$gl_caption['c_password_mail_text_1'] = 'Your new password is';
$gl_caption['c_password_mail_text_2'] = 'You can change your password at our website';

$gl_messages_customer['registered_succesful']="The registration has been succesful.<br><br>In short we'll notify you the activation of your account";
$gl_caption_customer['c_subject'] = 'New customer registered';
$gl_caption_customer['c_body_1'] = 'A new customer has been registered at the Newsletter';


$gl_caption_remove['c_admin_subject'] = 'Request of unsubscription from the newsletter';
$gl_caption_remove['c_admin_body'] = '<br />The client<strong>%s %s</strong> with mail address <strong>%s</strong> has unsubscribed from our newsletter.<br /><br />You can see all unsubscribed clients at: <a href="%s/admin/?menu_id=17">%sadmin/?menu_id=17</a>';


$gl_caption_remove['c_remove_confirm_1'] = 'You are applying for the removal from our mailing list of the following e-mail address:';
$gl_caption_remove['c_remove_confirm_2'] = 'Remember that this action can not be reversed. If you wish to confirm your unsubscription to out mailing list, please click:';
$gl_caption_remove['c_remove_confirm_button'] = 'Confirm';

$gl_caption_remove['c_remove_confirmed_1'] = 'Your subscription to our mailing list has been canceled.';
$gl_caption_remove['c_remove_confirmed_2'] = 'We hope to regain contact with you very soon.';


$gl_messages['added']=NEWSLETTER_ADDRESSES_MESSAGE_ADDED;
$gl_messages['add_exists']=NEWSLETTER_ADDRESSES_MESSAGE_ADD_EXISTS;
?>