<?php
// menus eina
define('NAUTICA_MENU_EMBARCACION_LIST', 'Llistat i editar embarcacions');
define('NAUTICA_MENU_EMBARCACION', 'Embarcacions');
define('NAUTICA_MENU_EMBARCACION_NEW', 'Nova embarcació');
define('NAUTICA_MENU_EMBARCACION_BIN', 'Paperera de reciclatge');
define('NAUTICA_MENU_TIPUS', 'Tipus');
define('NAUTICA_MENU_TIPUS_LIST', 'Llistat de tipus');
define('NAUTICA_MENU_TIPUS_NEW', 'Nou tipus');
define('NAUTICA_MENU_SHIPYARD', 'Astillers');
define('NAUTICA_MENU_SHIPYARD_LIST', 'Llistat d\'astillers');
define('NAUTICA_MENU_SHIPYARD_NEW', 'Nou astiller');
define('NAUTICA_MENU_FLAG', 'Banderes');
define('NAUTICA_MENU_FLAG_LIST', 'Llistat de banderes');
define('NAUTICA_MENU_FLAG_NEW', 'Nova bandera');

// captions seccio
$gl_caption_embarcacion['c_status'] = 'État';
$gl_caption_embarcacion['c_clau_dossier'] = 'Information practique';
$gl_caption_embarcacion['c_clau'] = 'Clé';
$gl_caption_embarcacion['c_dossier'] = 'Dossier';
$gl_caption_embarcacion['c_entered'] = 'Entrée';
$gl_caption_embarcacion['c_entered_form'] = 'Date entrée';
$gl_caption_embarcacion['c_prepare'] = 'En preparation';
$gl_caption_embarcacion['c_review'] = 'Reviser';
$gl_caption_embarcacion['c_onsale'] = 'A vendre';
$gl_caption_embarcacion['c_sold'] = 'Vendu';
$gl_caption_embarcacion['c_archived'] = 'Classé';
$gl_caption_embarcacion['c_reserved'] = 'Pending';
$gl_caption_embarcacion['c_tipus_id'] = 'Type de baetau';
$gl_caption['c_shipyard'] = 'Chantier';
$gl_caption_embarcacion['c_shipyard_id'] = 'Chantier';
$gl_caption_embarcacion['c_model'] = 'Modèle';
$gl_caption_embarcacion['c_buildyear'] = 'Année';
$gl_caption_embarcacion['c_buildyear_form'] = 'Année';
$gl_caption_embarcacion['c_length'] = 'Longeur';
$gl_caption_embarcacion['c_breadth'] = 'Largeur';
$gl_caption_embarcacion['c_weight'] = 'Poid';
$gl_caption_embarcacion['c_depth'] = 'Tirant d´eau';
$gl_caption_embarcacion['c_depth2'] = 'Tirant d´eau';
$gl_caption_embarcacion['c_engine'] = 'Moteur';
$gl_caption_embarcacion['c_fuel'] = 'Carburant';
$gl_caption_embarcacion['c_fuel_gasoline'] = 'Essence';
$gl_caption_embarcacion['c_fuel_diesel'] = 'Diesel';
$gl_caption['c_flag'] = 'Pavillon';
$gl_caption_embarcacion['c_flag_id'] = 'Pavillon';
$gl_caption_embarcacion['c_hour_use'] = 'Heures';
$gl_caption_embarcacion['c_price'] = 'Prix';
$gl_caption_embarcacion['c_price_pounds'] = 'Prix (&)';

$gl_caption_embarcacion['c_vhf'] = 'VHF';
$gl_caption_embarcacion['c_depthsounder'] = 'Sondeur';
$gl_caption_embarcacion['c_speedlog'] = 'Loch';
$gl_caption_embarcacion['c_radar'] = 'Radar';
$gl_caption_embarcacion['c_gps'] = 'GPS';
$gl_caption_embarcacion['c_plotter'] = 'Traceur de cartes';
$gl_caption_embarcacion['c_autopilot'] = 'Pilote Automatique';
$gl_caption_embarcacion['c_trimmtabs'] = 'Flaps Hydr.';
$gl_caption_embarcacion['c_searchlight'] = 'Phare';
$gl_caption_embarcacion['c_electrical_capstan'] = 'Guindeau Electrique';
$gl_caption_embarcacion['c_gangway'] = 'Passarelle';
$gl_caption_embarcacion['c_liferaft'] = 'Canot de survie';
$gl_caption_embarcacion['c_tender'] = 'Annexe';
$gl_caption_embarcacion['c_offshore_connexion'] = 'Prise a quai 220V';
$gl_caption_embarcacion['c_battery_charger'] = 'Chargeur batterie';
$gl_caption_embarcacion['c_generator'] = 'Groupe électrogène';
$gl_caption_embarcacion['c_cover'] = 'Taud';
$gl_caption_embarcacion['c_camping_cover'] = 'Capote';
$gl_caption_embarcacion['c_bimini_top'] = 'Tente solaire';
$gl_caption_embarcacion['c_bowthruster'] = 'Propulseur d\'etrave';
$gl_caption_embarcacion['c_hot_water'] = 'Eau chaud';
$gl_caption_embarcacion['c_davit'] = 'Boisoir';
$gl_caption_embarcacion['c_anemometer'] = 'Equipe de vent';
$gl_caption_embarcacion['c_mainsail'] = 'Grand voile';
$gl_caption_embarcacion['c_genoa'] = 'Genois';
$gl_caption_embarcacion['c_mizzen'] = 'Misaine';
$gl_caption_embarcacion['c_jib'] = 'Foque';
$gl_caption_embarcacion['c_stormsail'] = 'Tormentin';
$gl_caption_embarcacion['c_forsail2'] = 'Trinquette';
$gl_caption_embarcacion['c_spinaker'] = 'Spinaker';
$gl_caption_embarcacion['c_spi_boom'] = 'Tangon';
$gl_caption_embarcacion['c_roller_genoa'] = 'Enrouleur Genois';

$gl_caption_embarcacion['c_music_equipment'] = 'Poste radoo';
$gl_caption_embarcacion['c_television'] = 'TV';
$gl_caption_embarcacion['c_attached_engine'] = 'Moteur anexe';
$gl_caption_embarcacion['c_fridge'] = 'Frigo';
$gl_caption_embarcacion['c_heating'] = 'Chaufage';
$gl_caption_embarcacion['c_air_conditioning'] = 'Climatisation';
$gl_caption_embarcacion['c_teka'] = 'Pont en teck';
$gl_caption_embarcacion['c_wc'] = 'WC';
$gl_caption_embarcacion['c_black_water'] = 'Reservoir eaux noires';
$gl_caption_embarcacion['c_roller_mainsail'] = 'Enrouleur Grand voile';




$gl_caption_embarcacion['c_equipament'] = 'Equipement';

$gl_caption_embarcacion['c_data'] = 'Navire';
$gl_caption_embarcacion['c_ref'] = 'Reference';
$gl_caption_embarcacion['c_general_data'] = 'Information général';
$gl_caption_embarcacion['c_measures_weight'] = 'Mesures';
$gl_caption_embarcacion['c_measures'] = 'Mesures';
$gl_caption_embarcacion['c_weight_units'] = 'kg.';
$gl_caption_embarcacion['c_characteristics_electronic'] = 'Electronique';
$gl_caption_embarcacion['c_characteristics_electronic_caps'] = 'ELECTRONIQUE';
$gl_caption_embarcacion['c_characteristics_extra'] = 'Accessoires';
$gl_caption_embarcacion['c_characteristics_sail'] = 'Voile';
$gl_caption_embarcacion['c_characteristics'] = 'Caracteristiques';

$gl_caption_embarcacion['c_filter_shipyard_id'] = 'Tous les chantiers';
$gl_caption_embarcacion['c_filter_tipus_id'] = 'Tous les modèles';
$gl_caption_embarcacion['c_filter_status'] = 'Tous les états';
$gl_caption_embarcacion['c_filter_length'] = 'Toutes les longeurs';
$gl_caption_embarcacion['c_filter_price'] = 'Touts les prix';
$gl_caption_embarcacion['c_print_pdf'] = 'Imprimer pdf';
$gl_caption_embarcacion['c_changeable'] = 'Acepto embarcación a cambio';

$gl_caption_tipus['c_ordre']='Ordre';
$gl_caption_tipus['c_tipus']='Type';
?>