<?php
// no es poden posar ; entre mig de la consulta ja que parteix les consultes per el ;


if (!is_table('custumer__history') && is_table('custumer__custumer')){
	// historia clients
	$update_sql .=
		"
		CREATE TABLE IF NOT EXISTS `custumer__history` (
		  `history_id` int(11) NOT NULL auto_increment,
		  `custumer_id` int(11) NOT NULL,
		  `date` date NOT NULL,
		  `status` enum('sort_out','finished') NOT NULL,
		  `finished_date` date NOT NULL,
		  `tipus` enum('various','to_call','waiting_call','assembly','show_property','like_property','send_information') character set utf8 collate utf8_spanish2_ci NOT NULL,
		  `history` text collate utf8_spanish2_ci NOT NULL,
		  `bin` int(11) NOT NULL,
		  PRIMARY KEY  (`history_id`)
		) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

		INSERT INTO `all__menu` ( `menu_id` , `menu_group` , `tool` , `tool_section` , `action` , `parent_id` , `toolmode_id` , `variable` , `ordre` , `link` , `process` , `bin` , `read1` , `write1` )
		VALUES ( 5025, 'custumer', 'custumer', 'history', '', 5000, 1, 'CUSTUMER_MENU_HISTORY', 1, '', '', 0, ',1,2,3,4,', ',1,2,4,' ) ,
		( 5026, 'custumer', 'custumer', 'history', 'list_records', 5025, 1, 'CUSTUMER_MENU_HISTORY', 2, '', '', 0, ',1,2,3,4,', ',1,2,4,' ) ,
		( 5027, 'custumer', 'custumer', 'history', 'list_records_bin', 5025, 1, 'CUSTUMER_MENU_HISTORY_BIN', 3, '', '', 0, ',1,2,3,4,', ',1,2,4,' ) ;

		INSERT INTO `all__menu` ( `menu_id` , `menu_group` , `tool` , `tool_section` , `action` , `parent_id` , `toolmode_id` , `variable` , `ordre` , `link` , `process` , `bin` , `read1` , `write1` )
		VALUES ( 5028, 'custumer', 'custumer', 'history', 'show_form_new', 5025, 1, 'CUSTUMER_MENU_HISTORY_NEW', 3, '', '', 0, ',1,2,3,4,', ',1,2,4,' ) ;




		";
}

if (is_table('inmo__property')){
// map i booking
	$update_sql .=
"
	ALTER TABLE `inmo__property` ADD `latitude` DECIMAL( 9, 7 ) NOT NULL ,
	ADD `longitude` DECIMAL( 9, 7 ) NOT NULL ;
	ALTER TABLE `inmo__property` ADD `zoom` TINYINT( 2 ) NOT NULL ;


	CREATE TABLE IF NOT EXISTS `booking__booking` (
	  `booking_id` int(11) NOT NULL auto_increment,
	  `property_id` int(11) NOT NULL,
	  `date_in` date NOT NULL,
	  `date_out` date NOT NULL,
	  `name` varchar(100) collate utf8_spanish2_ci NOT NULL,
	  `surnames` varchar(100) collate utf8_spanish2_ci NOT NULL,
	  `mail` varchar(50) collate utf8_spanish2_ci NOT NULL,
	  `adress` varchar(250) collate utf8_spanish2_ci NOT NULL,
	  `comment` text collate utf8_spanish2_ci NOT NULL,
	  `phone` int(10) NOT NULL,
	  `entered` timestamp NOT NULL default CURRENT_TIMESTAMP,
	  PRIMARY KEY  (`booking_id`),
	  KEY `id_Apartamentos` (`property_id`)
	) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;
";
}

if (!Db::get_first("SELECT count(*) FROM all__configpublic WHERE name='google_api'")){
	// configuracio google maps
	$update_sql .=
	"
	INSERT INTO `all__configadmin` (
	`configadmin_id` ,
	`name` ,
	`value`
	)
	VALUES (
	NULL , 'google_api', ''
	), (
	NULL , 'google_latitude', ''
	), (
	NULL , 'google_longitude', ''
	), (
	NULL , 'google_center_latitude', ''
	), (
	NULL , 'google_center_longitude', ''
	);
	INSERT INTO `all__configadmin` (`configadmin_id`, `name`, `value`) VALUES (NULL, 'google_zoom', '');
	INSERT INTO `all__configpublic` (
	`configpublic_id` ,
	`name` ,
	`value`
	)
	VALUES (
	NULL , 'google_api', ''
	), (
	NULL , 'google_latitude', ''
	), (
	NULL , 'google_longitude', ''
	), (
	NULL , 'google_center_latitude', ''
	), (
	NULL , 'google_center_longitude', ''
	);
	INSERT INTO `all__configpublic` (
	`configpublic_id` ,
	`name` ,
	`value`
	)
	VALUES (
	NULL , 'google_zoom', ''
	);


	";
}
?>