<?php
// menus eina
define( 'LESTANY_MENU_ITINERARI', 'Elements' );
define( 'LESTANY_MENU_ITINERARI_NEW', 'Nou element' );
define( 'LESTANY_MENU_ITINERARI_LIST', 'Elements' );
define( 'LESTANY_MENU_ITINERARI_BIN', 'Paperera de reciclatge' );
define( 'LESTANY_MENU_APARTAT', 'Itinerari' );
define( 'LESTANY_MENU_APARTAT_LIST', 'Llistar itineraris' );

// captions seccio
$gl_caption_itinerari['c_referencia']                = 'Referència';
$gl_caption['c_apartat_id']                          = 'Itinerari';
$gl_caption_itinerari['c_apartat_aigua']             = 'Itinerari aigua';
$gl_caption_itinerari['c_apartat_terra']             = 'Itinerari terra';
$gl_caption_itinerari['c_apartat_fonts']             = 'Itinerari fonts';
$gl_caption_itinerari['c_latitude']                  = 'Latitud';
$gl_caption_itinerari['c_longitude']                 = 'Longitud';
$gl_caption_itinerari['c_entered']                   = 'Data de creació';
$gl_caption_itinerari['c_modified']                  = 'Data de Modificació';
$gl_caption_itinerari['c_nom']                       = 'Nom element';
$gl_caption_itinerari['c_tipus_element']             = 'Tipus d’element';
$gl_caption_itinerari['c_catalogacio']               = 'Catalogació';
$gl_caption_itinerari['c_estil_epoca']               = 'Estil i època';
$gl_caption_itinerari['c_cronologia']                = 'Cronologia';
$gl_caption_itinerari['c_autoria']                   = 'Autoria';
$gl_caption_itinerari['c_parcela']                   = 'Parcel·la';
$gl_caption_itinerari['c_context']                   = 'Context';
$gl_caption_itinerari['c_elements']                  = 'Elements';
$gl_caption_itinerari['c_us_actual']                 = 'Ús actual';
$gl_caption_itinerari['c_us_original']               = 'Ús original';
$gl_caption_itinerari['c_estat_conservacio']         = 'Estat de conservació';
$gl_caption_itinerari['c_entorn_proteccio']          = 'Entorn de protecció';
$gl_caption_itinerari['c_situacions_risc']           = 'Situacions de risc';
$gl_caption_itinerari['c_localitzacio']              = 'Localització';
$gl_caption_itinerari['c_codi_ine']                  = 'Codi INE';
$gl_caption_itinerari['c_adreca']                    = 'Adreça';
$gl_caption_itinerari['c_coordenades']               = 'Coordenades UTM';
$gl_caption_itinerari['c_grafisme']                  = 'Grafisme';
$gl_caption_itinerari['c_identificacio']             = 'Identificació';
$gl_caption_itinerari['c_descripcio_historica']      = 'Descripció històrica';
$gl_caption_itinerari['c_informacio_complementaria'] = 'Informació complementària';
$gl_caption_itinerari['c_bibliografia']              = 'Bibliografia';
$gl_caption_itinerari['c_videoframe']                = 'Vídeo (Ample 390px)';
$gl_caption_itinerari['c_videoframe2']               = 'Vídeo (Ample 390px)';

$gl_caption_itinerari['c_form_title']                             = "Descripció de l'element";
$gl_caption_itinerari['c_form_level1_title_localitzacio']         = 'Localització';
$gl_caption_itinerari['c_form_level1_title_descripcio_historica'] = 'Descripcions';
$gl_caption_itinerari['c_form_level1_title_videoframe']           = 'Vídeos';
$gl_caption_itinerari['c_figures']                                = 'Figures';
$gl_caption_itinerari['c_multimedia']                             = 'Multimèdia';
$gl_caption_itinerari['c_mes_info']                               = 'Més informació';

// No cal traduir

$gl_caption['c_apartat']                                = $gl_caption['c_apartat_id'];
$gl_caption_itinerari['c_status']                       = 'Estat';
$gl_caption_itinerari['c_prepare']                      = 'En preparació';
$gl_caption_itinerari['c_review']                       = 'Per revisar';
$gl_caption_itinerari['c_public']                       = 'Públic';
$gl_caption_itinerari['c_archived']                     = 'Arxivat';
$gl_caption_itinerari['c_form_level1_title_page_title'] = $GLOBALS['gl_caption']['c_seo'];
$gl_caption_itinerari['c_itinerari_file_name']          = $GLOBALS['gl_caption']['c_file_name'];


// mapes
$gl_caption['c_search_adress']   = 'Buscar adreça';
$gl_caption['c_latitude']        = 'latitud';
$gl_caption['c_longitude']       = 'longitud';
$gl_caption['c_center_coords']   = 'Centre';
$gl_caption['c_found_direction'] = 'adreça trobada';
$gl_caption['c_edit_map']        = 'Mapa';
$gl_caption['c_save_map']        = 'Guardar ubicació';
$gl_caption['c_click_map']       = 'Fes un click en un punt o més del mapa per guardar la ubicació.';
$gl_caption['c_form_map_title']  = 'Ubicació';
$gl_messages['point_saved']      = 'Nova ubicació guardada correctament';
$gl_messages['point_not_saved']  = 'No s\'ha pogut guardar l\'ubicació';
$gl_caption['c_remove_polyline'] = 'Borrar traçat';
?>