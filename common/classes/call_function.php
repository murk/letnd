<?
/**
 * * Clase per cridar funcions diverses desde el get 
 *
 *  ej. /admin/?call=drop_upload_image
 *  si faig un return false ja no executa res més
 */ 

/**
 * CallFunction
 *
 * @package
 * @author Sanahuja
 * @copyright Copyright (c) 2013
 * @version $Id$
 * @access public
 */
class CallFunction { 
	
	// si faig un return false ja no executa res més
	static public function call(){
		
		$call = R::text_id('call');
		
		if ($call){
		
			$ret = call_user_func('CallFunction::' . $call); 
			
			if (!$ret){
				Debug::add('Function Call Die', $call);
				Debug::p_all();
				die();
			}
		}
		return false;
	}
	
	// per que es pugui pujar una imatge desde un llistat amb un drop
	static public function drop_upload_image(){

		global $gl_message;
		header('Content-Type: application/json');

		$id = R::id('id');
		$tool = R::text_id('tool');
		$tool_section = R::text_id('tool_section');
		
		// poso el id en aquest format per que ho recullin les classes
		$_POST[$tool_section . '_id'] = $id;
		
		Debug::add('Get', $_GET);
		Debug::add('Files', $_FILES);

		if (SaveRows::is_post_too_big()){
			Header( "HTTP/1.1 409 Conflict" );
			echo(' { "error": ' . json_encode( $gl_message ). ' }' );
			return false;
		}
		
		if ($GLOBALS['gl_module']->is_v3){
			$image_manager = new ImageManager($GLOBALS['gl_module']);
		}
		else{
			$image_manager = new ImageManager();
		}
		$image_manager->drop_save_image($id, $tool, $tool_section);

		if ( $GLOBALS ['gl_errors']['post_file_max_size'] ) {

			Header( "HTTP/1.1 409 Conflict" );
			echo( ' { "error": ' . json_encode( $gl_message ) . ' }' );

		}

		return false;
	}

	// per que es pugui pujar una imatge desde un llistat amb un drop
	static public function drop_upload_file() {

		global $gl_message;
		header('Content-Type: application/json');
		ini_set('post_max_size','100M');
		ini_set('upload_max_filesize','100M');

		Debug::add( 'upload_max_filesize', ini_get( 'upload_max_filesize' ) );
		Debug::add( 'post_max_size', ini_get('post_max_size') );

		$id           = R::id( 'id' );
		$tool         = R::text_id( 'tool' );
		$tool_section = R::text_id( 'tool_section' );
		$name         = R::text_id( 'name' );

		// poso el id en aquest format per que ho recullin les classes
		$_POST[ $tool_section . '_id' ] = $id;

		Debug::add( 'Get', $_GET );
		Debug::add( 'Files', $_FILES );

		if (SaveRows::is_post_too_big()){
			Header( "HTTP/1.1 409 Conflict" );
			echo(' { "error": ' . json_encode( $gl_message ). ' }' );
			return false;
		}

		$GLOBALS ['gl_module']->uploads [ $name ]->get_files();


		if ($GLOBALS ['gl_errors']['post_file_max_size']) {
			Header( "HTTP/1.1 409 Conflict" );
			echo(' { "error": ' . json_encode( $gl_message ). ' }' );
			return false;
		}

		$results = Db::get_rows_array( 'SELECT file_id, name FROM directory__document_file WHERE document_id = ' . $id );
		$files   = '';

		// això es igual que a  FormObject::get_file()
		foreach ( $results as $rs ) {
			$input_name    = $name . '[' . $id . ']';
			$file_ext      = strrchr( substr( $rs[1], - 5, 5 ), '.' );
			$download_link = UploadFiles::get_download_link( $tool, $tool_section, $name, $rs[0], $file_ext );

			$files .= '<div><input type="checkbox" value="' . $rs[0] . '" name="' . $input_name . "[" . $rs[0] . ']" checked />&nbsp;<a href="' . $download_link . '">' . $rs[1] . '</a></div>';
		}

		echo( '{"files":' . json_encode( $files ) . '}' ); // si poso cometes simples al json no funciona

		return false;
	}
	
	// canviar llistat de editable a no editable
	static public function swap_edit(){
		$_SESSION['last_list'][$GLOBALS['gl_menu_id']]['edit'] = !$_SESSION['last_list'][$GLOBALS['gl_menu_id']]['edit'];
		print_javascript('top.window.location.replace(top.window.location)');
		return false;
	}
	
	// canviar llistat de retallar texte a no retallar
	static public function swap_add_dots(){
		$_SESSION['last_list'][$GLOBALS['gl_menu_id']]['add_dots'] = !$_SESSION['last_list'][$GLOBALS['gl_menu_id']]['add_dots'];
		print_javascript('top.window.location.replace(top.window.location)');
		return false;
	}
	
	// surt del facebook
	static public function facebook_logout(){
		Main::load_class('social');
		Social::facebook_logout();
		print_javascript('top.window.location.replace("' . str_replace( "&amp;","&",urldecode(R::escape('return_url'))) . '")');
		return false;
	}
	
	// surt del twitter
	static public function twitter_logout(){
		Main::load_class('social');
		Social::twitter_logout();
		print_javascript('top.window.location.replace("' . str_replace( "&amp;","&",urldecode(R::escape('return_url'))) . '")');
		return false;
	}
	
	// amaga missatge de gl_news
	static public function hide_missatge(){
		
		$missatge_id = R::id('missatge_id');

		$query = "
			SELECT count(*) 
			FROM missatge__missatge_read_to_user
			WHERE missatge_id = " . $missatge_id . "
			AND user_id = " . $_SESSION['user_id'];

		if (!Db::get_first( $query )){
			$query = "
				INSERT INTO missatge__missatge_read_to_user
					(missatge_id, user_id)
					VALUES (" . $missatge_id . "," . $_SESSION['user_id'] . ")";
			Db::execute( $query );
		}
		
		print_javascript("
			top.$('#missatge_titol_" . $missatge_id . "').remove();
			top.$('#missatge_missatge_" . $missatge_id . "').remove()
			
			if( !top.$.trim( top.$('#news').html() ) ) {
				top.$('#news').hide();
			}
			
			
			");
		
		return false;
	}

	// Inmo canvia a mls
	static public function swap_mls () {
		$_SESSION['inmo_show_mls'] = !$_SESSION['inmo_show_mls'];
		print_javascript('top.window.location.replace(top.window.location)');
		return false;
	}

}