<link href="../../templates/styles/general.css" rel="stylesheet" type="text/css" />
<link href="clients/agenciaportselva/templates/styles/general.css" rel="stylesheet" type="text/css" />
<script language="JavaScript" src="/public/jscripts/utilities.js"></script>
<script language="JavaScript" src="/public/jscripts/hcomun.js"></script>
<form name="_form" method="post" onSubmit="return ReCalculate(this);" >
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="fonsgeneralsclars">
  <tr>
    <td width="10"><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="10" height="1" /></td>
    <td><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="10" height="10" /></td>
    <td width="10"><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="10" height="1" /></td>
  </tr>
  <tr>
    <td width="10">&nbsp;</td>
    <td class="titolFitxa" ><strong class="text20verdanaboldcolor">C&Agrave;LCUL DE QUOTES HIPOTECA </strong> </td>
    <td width="10">&nbsp;</td>
  </tr>
  <tr>
    <td width="10">&nbsp;</td>
    <td><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="10" height="30" /></td>
    <td width="10">&nbsp;</td>
  </tr>
  <tr>
    <td width="10">&nbsp;</td>
    <td><table width="50%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="15" align="left"><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="15" height="7" /></td>
          <td align="left">Preu (<?=CURRENCY_NAME?>) : </td>
          <td width="10"><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="10" height="7" /></td>
          <td align="left"><strong>
            <input name="Fprice" type="text" id="Fprice" onblur="formatNumber()">
          </strong></td>
        </tr>
        <tr>
          <td width="15" align="left">&nbsp;</td>
          <td align="left"><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="10" height="7" /></td>
          <td width="10">&nbsp;</td>
          <td align="left">&nbsp;</td>
        </tr>
        <tr>
          <td width="15" align="left">&nbsp;</td>
          <td align="left">Entrada (<?=CURRENCY_NAME?>) : </td>
          <td width="10">&nbsp;</td>
          <td align="left"><strong>
            <input name="Fdownpayment" type="text" id="Fdownpayment" value="0" />
          </strong></td>
        </tr>
        <tr>
          <td width="15" align="left">&nbsp;</td>
          <td align="left"><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="10" height="7" /></td>
          <td width="10">&nbsp;</td>
          <td align="left">&nbsp;</td>
        </tr>
        <tr>
          <td width="15" align="left">&nbsp;</td>
          <td align="left">Inter&egrave;s (<?=CURRENCY_NAME?>) : </td>
          <td width="10">&nbsp;</td>
          <td align="left"><strong>
            <input name="Frate" type="text" id="Frate" value="0" />
          </strong></td>
        </tr>
        <tr>
          <td align="left">&nbsp;</td>
          <td align="left"><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="10" height="7" /></td>
          <td>&nbsp;</td>
          <td align="left">&nbsp;</td>
        </tr>
        <tr>
          <td align="left">&nbsp;</td>
          <td align="left">Anys:</td>
          <td>&nbsp;</td>
          <td align="left"><span class="formsButtons">
            <select name="Fterm" size="1" id="Fterm">
              <option value="5">5</option>
              <option value="6">6</option>
              <option value="7">7</option>
              <option value="8">8</option>
              <option value="9">9</option>
              <option value="10">10</option>
              <option value="11">11</option>
              <option value="12">12</option>
              <option value="13">13</option>
              <option value="14">14</option>
              <option value="15">15</option>
              <option value="16">16</option>
              <option value="17">17</option>
              <option value="18">18</option>
              <option value="19">19</option>
              <option value="20">20</option>
              <option value="21">21</option>
              <option value="22">22</option>
              <option value="23">23</option>
              <option value="24">24</option>
              <option value="25">25</option>
              <option value="26">26</option>
              <option value="27">27</option>
              <option value="28">28</option>
              <option value="29">29</option>
              <option value="30" selected="selected">30</option>
              <option value="31">31</option>
              <option value="32">32</option>
              <option value="33">33</option>
              <option value="34">34</option>
              <option value="35">35</option>
              <option value="36">36</option>
              <option value="37">37</option>
              <option value="38">38</option>
              <option value="39">39</option>
              <option value="40">40</option>
              <option value="41">41</option>
              <option value="42">42</option>
              <option value="43">43</option>
              <option value="44">44</option>
              <option value="45">45</option>
              <option value="46">46</option>
              <option value="48">47</option>
              <option value="49">49</option>
              <option value="50">50</option>
            </select>
          </span></td>
        </tr>
      </table></td>
    <td width="10">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="10" height="14" /></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><table width="40%" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
          <td width="19"><input type="image" border="0"  alt="calcualr" src="/clients/agenciaportselva/templates/images/bcalcular.gif" width="19" height="19" /></td>
          <td width="10"><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="10" height="14" /></td>
          <td>calcular</td>
          <td width="10"><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="10" height="14" /></td>
          <td width="19"><img src="/clients/agenciaportselva/templates/images/bprint.gif" alt="imprimir" width="19" height="19" /></td>
          <td width="10"><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="10" height="14" /></td>
          <td>imprimir</td>
        </tr>
      </table></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="10" height="25" /></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><table width="50%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="15" align="left"><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="15" height="7" /></td>
        <td align="left">Quota (<?=CURRENCY_NAME?>) : </td>
        <td width="10"><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="10" height="7" /></td>
        <td align="left"><strong>
          <input name="iPaymentTotal" type="text" id="iPaymentTotal" />
        </strong></td>
      </tr>

    </table></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="10" height="25" /></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="text10verdanawhite">La informaci&oacute; obtinguda en aquesta simulaci&oacute; de c&agrave;lcul &eacute;s totalment orientativa i no implica cap comprom&iacute;s, ni cap vincle jur&iacute;dic-legal. </td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="10" height="14" /></td>
    <td>&nbsp;</td>
  </tr>
</table>
</form>