<link href="/admin/themes/inmotools/styles/inmo_booking.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
#booking{
	height:<?=$table_height?>px;
	width:100%;
}
-->
</style>
<script language="JavaScript" src="/admin/jscripts/validacio.js?v=<?=$version?>"></script>
<script type="text/javascript">
function show_form(booking_id){		
	showPopWin('/admin/?menu_id=103&property_id=<?=$property_id?>&process=booking&action=show_form_edit&template=window&set_month=<?=$current_month?>&booking_id=' + booking_id, 670, 489, null, true, false, '');
}
gl_start = true;
var gl_data_in_obj;
var gl_data_out_obj;
function set_day(booking_day){
	booking_id = document.theForm.booking_id_javascript.value;
	if (gl_start){
		document.theForm['date_in['+booking_id+']'].value = booking_day;
	}
	else{
		document.theForm['date_out['+booking_id+']'].value = booking_day;
	}
	gl_start=!gl_start;
}
function xvalidar(obj){
	d_in_arr = obj.date_in.value.split('-');
	d_out_arr = obj.date_out.value.split('-');
	
	d = d_in_arr;
	d_in = new Date(d[0],d[1]-1,d[2]);
	d = d_out_arr;
	d_out = new Date(d[0],d[1]-1,d[2]);
	if (d_out>d_in) return true;	
	alert('La fecha de salida debe ser mayor a la fecha de entrada');
	return false;
}
</script>
<div class="form">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td valign="bottom"><div class="menu">
    <a href="?action=show_form_edit&menu_id=103&property_id=<?=$property_id?>"><?=$c_edit_data?></a>
    <a href="?action=list_records_images&menu_id=103&property_id=<?=$property_id?>"><?=$c_edit_images?></a>
        <span class="private">&nbsp;<a href="?menu_id=<?=$menu_id?>&amp;property_id=<?=$property_id?>&amp;action=list_records_custumer"><?=$c_edit_custumer?></a></span>
        <span class="private"><a href="?menu_id=<?=$menu_id?>&property_id=<?=$property_id?>&action=show_form_map"><?=$c_edit_map?></a></span>
        <div><?=$c_edit_booking?></div></div></td>
  </tr>
</table>
<form id="theForm" name="theForm" method="post" action="<?=$link_action?>"  onsubmit="<?=$js_string?>" target="save_frame">
       
              <table width="100%" cellpadding="0" cellspacing="0">
                  <tr>
                    <td class="formCaptionTitle"><strong>
                      <?=$c_form_booking_title?></strong>&nbsp;&nbsp;<?=$ref?><? if ($property_private):?> - <?=$property_private?> <? endif //property_private?>
                    </td>
                  </tr>
              </table>
              <table width="100%" cellpadding="4" cellspacing="0" class="formstbl">
               
                  <tr>
                    <td align="left" valign="top" class="forms"><table width="99%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td>
<table border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td align="center"><div id="booking"><table border="0" cellpadding="0" cellspacing="0">
        <tr>
          <? foreach($calendar as $l): extract ($l)?>
            <td valign="top"><div class="nom_mes"><?=$month?></div><table border="0" cellspacing="0" cellpadding="0" bgcolor="#999999" class="mes">
                <? foreach($propertys as $pr): extract ($pr)?>
                <? foreach($weeks as $w): extract ($w)?>
                  <tr>
                    <? foreach($days as $d): extract ($d)?>
                      <td><div class="dia"><? if ($class2):?><div class="<?=$class1?>" <?=$onclick1?>><?=$day?></div><div class="<?=$class2?>" <?=$onclick2?>><?=$day?></div><? else:?><div class="<?=$class1?>" <?=$onclick1?>><?=$day?></div><? endif //?></div></td>
                      <? endforeach //$days?>
                  </tr>
                  <? endforeach //$weeks?>
                  <? endforeach //$cases?>
              </table></td>
            <? endforeach //$calendar?>
        </tr>
      </table></div></td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="00">
      <tr>
        <td><a href="?menu_id=<?=$menu_id?>&property_id=<?=$property_id?>&action=<?=$_GET['action']?>&process=booking&set_month=<?=$previous_month?>">&lt; anterior</a></td>
        <td align="right"><a href="?menu_id=<?=$menu_id?>&property_id=<?=$property_id?>&action=<?=$_GET['action']?>&process=booking&set_month=<?=$next_month?>">siguiente &gt;</a></td>
      </tr>
    </table></td>
  </tr>
</table>

						</td>
                      </tr>
                    </table>
                      
                      </td>
                </tr>
              </table>
              <div class="sep_dark" style="margin-top:30px;"></div>
<table width="100%" cellpadding="0" cellspacing="0">
                        <tr>
                          <td class="formCaptionTitle"><strong>
                            <?=$c_form_new?>
                          </strong></td>
                        </tr>
                      </table>
                      <table width="100%" cellpadding="0" cellspacing="0" class="formstbl">
                        <tr>
                          <td width="150" class="formsCaption"><strong>
                            <?=$c_name?>
      :</strong></td>
                          <td class="forms"><?=$name?>
				<input name="property_id[0]" type="hidden" value="<?=$property_id?>" />
				<input name="old_property_id[0]" type="hidden" value="<?=$property_id?>" />
				<input name="booking_id[0]" type="hidden" value="0" />
                <input name="booking_id_javascript" type="hidden" value="0" />
                <input type="hidden" name="set_month" value="<?=$current_month?>" />
                <input type="hidden" name="action" value="add_record" /></td>
                        </tr>
                        <tr>
                          <td class="formsCaption"><strong>
                            <?=$c_surnames?>
      :</strong></td>
                          <td class="forms"><?=$surnames?></td>
                        </tr>
                        <tr>
                          <td width="150" class="formsCaption"><strong>
                            <?=$c_date_in?>
      :</strong></td>
                          <td class="forms"><?=$date_in?>
				          </td>
                        </tr>
                        <tr>
                          <td class="formsCaption"><strong>
                            <?=$c_date_out?>
      :</strong></td>
                          <td class="forms"><?=$date_out?></td>
                        </tr>
                        <tr>
                          <td width="150" class="formsCaption"><strong>
                            <?=$c_mail?>
      :</strong></td>
                          <td class="forms"><?=$mail?>
				          </td>
                        </tr>
                        <tr>
                          <td class="formsCaption"><strong>
                            <?=$c_adress?>
      :</strong></td>
                          <td class="forms"><?=$adress?></td>
                        </tr>
                        <tr>
                          <td class="formsCaption"><strong>
                            <?=$c_phone?>
      :</strong></td>
                          <td class="forms"><?=$phone?></td>
                        </tr>
                        <tr>
                          <td valign="top" class="formsCaption"><strong>
                            <?=$c_comment?>
      :</strong></td>
                          <td class="forms"><?=$comment?></td>
                        </tr>
                      </table>
<table width="100%" cellpadding="4" cellspacing="2" class="formsButtonstbl">
  <tr>
    <td align="center" class="formsButtons"><div class="centerFloat1">
                        <div class="centerFloat2"><input type="submit" name="Submit" value="<?=$c_send_new?>" class="btn_form1"></div></div></td>
  </tr>
</table>
</form>
</div>