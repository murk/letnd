<?php
global $gl_is_home;
$vars['module']=$vars['parent_module']=='ClinkNew'?'new':'medio';

$query = "SELECT clink__new.new_id as new_id, new, source, entered
				FROM clink__new_language, clink__new
				WHERE clink__new_language.new_id = clink__new.new_id
				AND status = 'public'
				AND bin <> 1
				AND parent_module = '" . $vars['parent_module'] . "'
				AND language = '" . LANGUAGE . "'
				ORDER BY entered DESC, new_id LIMIT 7";

$results = Db::get_rows($query);
Debug::add('Results block: clink_new',$results);
$selected_found = false;
if ($results)
{
	$conta = 0;
	$loop_count = count($results);
    foreach($results as $rs)
    {
        if (isset($_GET["new_id"]) && ($_GET["new_id"] == $rs["new_id"]))
		{
			$selected = 1;
			$selected_found = true;
		}
		else
		{
			$selected = 0;
		}
        
        
		if (USE_FRIENDLY_URL){
			$l['link'] = '/' . LANGUAGE . '/clink/' . $vars['module'] . '/' . $rs['new_id'] . '/';
			//if (isset($_GET['page_file_name'])) $l['link'] .= '/' . $_GET['page_file_name'] . '.html';
		}
		else{
			$l['link'] = '/?tool=clink&tool_section=new&new_id=' . $rs['new_id'] . '&language=' . LANGUAGE;
			//if (isset($_GET['page_id'])) $l['link'] .= '&page_id=' . $rs['page_id'];
		}
        $l['selected'] = $selected;
        $l['item'] = $rs['new'];
        $l['entered'] = format_date_list($rs['entered']);
        $l['source'] = $rs['source'];        
		$l['id'] = $rs['new_id'];
		$l['conta'] = $conta++;
		$l['class'] = '';
		$l['loop_count'] = $loop_count;
        $loop2[] = $l;
    }
	$loop2[0]['class'] = 'first';	
	$loop2[count($loop2)-1]['class'] = 'last';
	if (!$selected_found) $loop2[0]['selected'] = 1;
    $loop[] = array ('loop' => $loop2,
        'item' => '');
}

?>