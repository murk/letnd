<?
/**
 * * Clase base per clase module
 *    i totes les clases on es passa per referencia qualsevol clase basada en Module ( listRecords, FormObjecet, ImageManager, ShowForm, etc... )
 *
 *    Conté les variables que son comunes en totes les clases mecinoades,
 *   i un metode per inicialitzar-les desde les clases on es passen per referencia
 *
 */

/**
 * ModuleBase
 *
 * @package
 * @author Calveres
 * @copyright Copyright (c) 2010
 * @version $Id$
 * @access public
 */
class ModuleBase {
	// PROPIES DE MODUL
	var
		$is_v3 = true,
		$is_index = true,

		// GENERALS
		/**
		 * Accio que es realitza
		 *
		 * @access public
		 * @var string
		 */
		$action,
		/**
		 * Process per complementar una acció
		 *
		 * @access public
		 * @var array
		 */
		$process,
		/**
		 * Array on es guarden tots els parametres de configuració de les taules config...
		 *
		 * @access public
		 * @var array
		 */
		$config,
		/**
		 * Modul de la pàgina on som
		 *
		 * @access public
		 * @var string
		 */
		$tool,
		/**
		 * tool del modul "parent"
		 *
		 * @access public
		 * @var string
		 */
		$parent_tool = '',
		/**
		 * tool del modul "child"
		 * Quan es carrega un modul:
		 * news desde desde museumoli, news te com a parent museumoli, i museumoli te com a child news
		 * @access public
		 * @var string
		 */
		$child_tool = '',
		/**
		 * Secció de la pàgina on som
		 *
		 * @access public
		 * @var string
		 */
		$tool_section,
		/**
		 * Secció del modul "parent" ( a get_form ho necessito per trobar l'id )
		 *
		 * @access public
		 * @var string
		 */
		$parent_tool_section = '',
		/**
		 * Secció del modul "child" ( a all_block_language ho necessito )
		 *
		 * @access public
		 * @var string
		 */
		$child_tool_section = '',
		/**
		 * Link base del modul i seccio, per fer tots els links de boto editar, editar imatges, form_action , etc....
		 * Ej : /?tool=tool&tool_section=tool_section&language=language
		 * @access public
		 * @var string
		 */
		$base_link, // link base per cridar el modul
		/**
		 * Link base del modul i seccio, per fer tots els links de boto editar, editar imatges, form_action , etc....
		 * Ej : /?tool=tool&tool_section=tool_section&language=language
		 * @access public
		 * @var string
		 */
		$base_link_friendly, // link base per cridar el modul
		/**
		 * Link base del modul a public amb les 3 variables necessaries
		 * Ej : /?tool=tool&tool_section=tool_section&language=language
		 * @access public
		 * @var string
		 */
		$link, // link base per cridar el modul

		// IDIOMES
		/**
		 * Array on es guarden tots els texts que surten a la pàgina en l'idioma corresponet
		 *
		 * @example ../inmo/languages/cat.php
		 * @access public
		 * @var array
		 */
		$caption,

		// IDIOMES
		/**
		 * Array on es guarden tots els texts que surten a la pàgina en l'idioma corresponet als llistats ( si es vol posar diferent en un llistat que en un formulari
		 *
		 * @example ../inmo/languages/cat.php
		 * @access public
		 * @var array
		 */
		$caption_list = array(), $caption_image,
		/**
		 * Array on es guarden tots els missatges que surten a la pàgina en l'idioma corresponet
		 *
		 * @example ../inmo/languages/cat.php
		 * @access public
		 * @var array
		 */
		$messages,

		// ARXIUS CONFIG
		/**
		 * Llistat de camps que van amb idiomes
		 *
		 * @access private
		 * @var array
		 */
		$language_fields,
		/**
		 * Array que conté els noms dels camps de la BBDD que es fan servir en el formulari <br><br>
		 * "Nom del camp" => "Valor" <br><br>
		 * Si s'epecifica 'Valor':
		 * - Si estem editant ignora 'Valor' i l'agafa de la BBDD
		 * - Si esem insertant un nou registe, posa 'Valor' com a valor per defecte del camp (ej. en una data volem que posi la data d'avui per defecte)
		 *
		 * @access public
		 * @var array
		 */
		$fields,
		/**
		 * Camp id principal
		 *
		 * @access public
		 * @var string
		 */
		$id_field,
		/**
		 * Taula de la bbdd
		 *
		 * @access public
		 * @var string
		 */
		$table,
		$related_tables,
		$list_settings,
		$form_settings,
		$upload_settings,
		$module, // referencia al modul que ha cridat el list_records, show_form, etc....

		// UPLOADS
		$uploads, // conté tots els objectes Upload
		$has_images = false, // true si el modul te imatges relacionades

		// FRIENLY
		$friendly_params = array(),


		// Aquesta variable es per compatibilitat nomès, els moduls hauran de ser una extensió del tpl, ara de moment creo les fincions que ho simulin
		$tpl,
		// per poder extendre tpl més endavant
		$vars = array(), /// Holds all the template variables
		$loops = array(),
		$path, /// path to templates
		$file; /// template

	/**
	 * ModuleBase::link_modul()
	 * Linka variables del modul amb les variables de la clases
	 * on es passa modul per referencia
	 *
	 * @param $module
	 */
	function link_module( &$module ) {

		if ( ! $module ) {
			Debug::add( 'Cuidado , si estas treballant en sitema de clases has de passar el parametre de modul així: <b>new ShowForm(&$module)</b>!!!!!!!!!!!!<br>', 'Nom de la clase: ' . get_class( $this ) );
		}

		// linko a variables del modul, així puc modificarles en qualsevol moment i canvien també aquí
		if ( $module ) {
			$this->module = &$module;
			// MODUL
			$this->is_v3    = &$module->is_v3;
			$this->is_index = &$module->is_index;
			// GENERALS
			$this->action              = &$module->action;
			$this->process             = &$module->process;
			$this->config              = &$module->config;
			$this->tool                = &$module->tool;
			$this->tool_section        = &$module->tool_section;
			$this->parent_tool         = &$module->parent_tool;
			$this->parent_tool_section = &$module->parent_tool_section;
			$this->tpl                 = &$module->tpl;
			$this->base_link           = &$module->base_link;
			$this->base_link_friendly  = &$module->base_link_friendly;
			$this->link                = &$module->link;
			$this->friendly_params     = &$module->friendly_params;

			// TEMPLATES
			$this->vars  = &$this->tpl->vars; /// Holds all the template variables
			$this->loops = &$this->tpl->loops;
			$this->path  = &$this->tpl->path; /// path to templates
			$this->file  = &$this->tpl->file; /// template

			// IDIOMES
			$this->caption  = &$module->caption;
			$this->messages = &$module->messages;

			// ARXIUS _CONFIG
			$this->language_fields = &$module->language_fields;
			$this->fields          = &$module->fields;
			$this->id_field        = &$module->id_field;
			$this->table           = &$module->table;
			$this->upload_settings = &$module->upload_settings;
			$this->uploads         = &$module->uploads;
			$this->has_images      = &$module->has_images;
			$this->related_tables  = &$module->related_tables;
			$this->list_settings   = &$module->list_settings;
			$this->form_settings   = &$module->form_settings;
		} else {
			// globals per eliminar a v3
			global $gl_action, $gl_process, $gl_config, $gl_tool, $gl_tool_section, $tpl, $gl_caption, $gl_messages, $gl_db_classes_language_fields, $gl_db_classes_fields, $gl_db_classes_id_field, $gl_db_classes_table, $gl_db_classes_uploads, $gl_upload, $gl_db_classes_related_tables, $gl_db_classes_form, $gl_has_images;
			Debug::add( 'Messages', $gl_messages );
			$this->module             = $module;
			$this->is_v3              = false;
			$this->is_index           = true; // per que ho faig servir nomès a gl_content, i en aquest cas abans actuava com si fos true
			$this->action             = $gl_action;
			$this->process            = $gl_process;
			$this->config             = $gl_config;
			$this->tool               = $gl_tool;
			$this->tool_section       = $gl_tool_section;
			$this->tpl                = &$tpl;
			$this->base_link          = 'tool=' . $this->tool . '&amp;tool_section=' . $this->tool_section;
			$this->base_link_friendly = $this->tool . '/' . $this->tool_section;
			$this->link               = $GLOBALS['gl_is_admin'] ? '/admin/?' . $this->base_link : '/?' . $this->base_link . '&amp;language=' . $GLOBALS['gl_language'];

			$this->caption         = &$gl_caption;// de moment així per el tema de l'array_merge
			$this->messages        = &$gl_messages;// de moment així per el tema de l'array_merge
			$this->language_fields = $gl_db_classes_language_fields;
			$this->fields          = $gl_db_classes_fields;
			$this->id_field        = $gl_db_classes_id_field;
			$this->table           = $gl_db_classes_table;
			$this->upload_settings = $gl_db_classes_uploads;
			$this->uploads         = $gl_upload;
			$this->has_images      = &$gl_has_images; // ja que hi havia llocs on canviava entre mig
			$this->related_tables  = isset( $gl_db_classes_related_tables ) ? $gl_db_classes_related_tables : false;
			$this->list_settings   = isset( $gl_db_classes_list ) ? $gl_db_classes_list : false;
			$this->form_settings   = isset( $gl_db_classes_form ) ? $gl_db_classes_form : false;
		}
	}

	/**
	 * Canvia el valor d'una opció ($key) d'un camp ($field) del congif
	 * Ej. $this->set_field('property','required','1'); canvia en el field 'property', l'opció 'required' = 1
	 *
	 * @param mixed $field
	 * @param mixed $key
	 * @param mixed $value
	 */
	function set_field( $field, $key, $value ) {

		if ( is_null( $value ) ) {
			unset ( $this->fields[ $field ][ $key ] );
			return;
		}

		$this->fields[ $field ][ $key ] = $value;
	}

	/**
	 * Retorna el valor d'un camp de configuració
	 *
	 * @param $field
	 * @param $key
	 *
	 * @return mixed
	 */
	function get_field( $field, $key ) {
		return $this->fields[ $field ][ $key ];
	}

	function unset_field( $field ) {
		unset( $this->fields[ $field ] );
		foreach ( $this->language_fields as $key => $val ) {
			if ( $val == $field ) {
				array_splice( $this->language_fields, $key, 1 );
				break;
			}
		}
	}

	/**
	 * Mou a la posició enumerada
	 *
	 * @param string $field Nom del camp
	 * @param integer $pos Posició on es posarà el camp
	 *
	 */
	function set_field_position( $field, $pos ) {

		$new_fields      = array();
		$field_to_change = $this->fields [ $field ];
		unset ( $this->fields [ $field ] );


		$i = 1;
		foreach ( $this->fields as $key => $value ) {
			if ( $i == $pos )
				$new_fields[ $field ] = $field_to_change;
			$new_fields[ $key ] = $value;
			$i++;
		}
		$this->fields = $new_fields;
	}

	/**
	 * Intercanvia la posició ed 2 camps
	 *
	 * @param string $key1 Nom del primer camp
	 * @param string $key2 Nom del segon camp
	 */
	function swap_fields( $key1, $key2 ) {
		$value1     = $this->fields[ $key1 ];
		$value2     = $this->fields[ $key2 ];
		$new_fields = array();

		foreach ( $this->fields as $key => $value ) {
			if ( $key == $key1 ) {
				$key   = $key2;
				$value = $value2;
			} elseif ( $key == $key2 ) {
				$key   = $key1;
				$value = $value1;
			}
			$new_fields[ $key ] = $value;
		}
		$this->fields = $new_fields;
	}

	/**
	 * Mou camp a la primera posició
	 *
	 * @param string $key Nom del camp
	 */
	function send_field_top( $key ) {
		$first_field[ $key ] = $this->fields[ $key ];
		unset( $this->fields[ $key ] );
		$this->fields = array_merge( $first_field, $this->fields );
	}

	/**
	 * Mou camp a la última posició
	 *
	 * @param string $key Nom del camp
	 */
	function send_field_bottom( $key ) {
		$last_field[ $key ] = $this->fields[ $key ];
		unset( $this->fields[ $key ] );
		$this->fields = array_merge( $this->fields, $last_field );
	}

	/**
	 * Module::add_field()
	 * Crea un nou camp a fields amb uns valors per defecte, es poden sobreescriure passant com a array
	 * Ej. $this->add_field('property',array('type' => 'input', 'enabled' => '0')); canvia en el field 'property', l'opció 'required' = 1
	 *
	 * @param mixed $field
	 * @param array $values
	 *
	 */
	public function add_field( $field, $values = array() ) {
		$this->fields[ $field ] =
			array(
				'type'                => 'text',
				'enabled'             => '1',
				'form_admin'          => '0',
				'form_public'         => '0',
				'list_admin'          => '0',
				'list_public'         => '0',
				'order'               => '0',
				'default_value'       => '',
				'override_save_value' => '',
				'class'               => '',
				'javascript'          => '',
				'text_size'           => '0',
				'text_maxlength'      => '0',
				'textarea_cols'       => '',
				'textarea_rows'       => '',
				'select_caption'      => '',
				'select_size'         => '',
				'select_table'        => '',
				'select_fields'       => '',
				'select_condition'    => '',
			);
		if ( $values ) {
			$this->fields[ $field ] = array_merge( $this->fields[ $field ], $values );
		}
	}

	// posa variables per que s'introduiexi primer entre la friendly_url y despès a fora, /lang/tool/section/id/param/param_val/page_file_name.html,  i no al final amb ?param=param
	// Les variables van amb l'ordre definit, si pasa de 2 queden fora -> /var2/val/?var3=val	
	function set_default_friendly_params() {
		// poso parametres automàtics
		// list_records		
		// no poso els gets perque ho haig de poder controlar en cada lloc, view i page si que es propaguen, i si show_record_link no
		$this->friendly_params['orderby'] = false;//isset($_GET['orderby'])?$_GET['orderby']:'';
		$this->friendly_params['view']    = false;//isset($_GET['view'])?$_GET['view']:'';
		$this->friendly_params['page']    = false;//isset($_GET['page'])?$_GET['page']:'';	 // no funciona amb page+nom list
	}

	function set_friendly_params( $params ) {
		// els nous van primers   ej. brand_id, order_by, view, page
		$this->friendly_params = array_merge( $params, $this->friendly_params );
	}

	// AQUESTES FUNCIONS SON PER COMPTABILITAT V2
	// Son les mateixes funcions del phemplate, serà més fàcil cambiar-ho per extends phemplate quan ja no hi hagi la variable global $tpl
	function set_file( $file, $clear = false ) {
		$this->tpl->set_file( $file, $clear );
	}

	function set_var( $name, $value ) {
		$this->tpl->set_var( $name, $value );
	}

	function set_vars( $vars, $clear = false ) {
		$this->tpl->set_vars( $vars, $clear );
	}

	function set_loop( $loop_name, $loop ) {
		$this->tpl->set_loop( $loop_name, $loop );
	}

	function get_var( $name ) {
		return $this->tpl->vars[ $name ];
	}

	function process() {
		return $this->tpl->process();
	}

}

?>