<?php
$tipus = isset($vars['tipus'])?$vars['tipus']:false;
$page_id = isset($vars['page_id']) ? $vars['page_id'] : false;

include_once('inmo_block_functions.php');
$condition = '';
$condition .= inmo_block_get_tipus($tipus);

$vars['var_name'] = 'zone_id';

$query = "SELECT Distinct zone, inmo__zone.zone_id
				FROM inmo__zone, inmo__property
				WHERE inmo__zone.zone_id = inmo__property.zone_id
				AND bin = 0 AND (status = 'onsale' OR status = 'sold' OR status = 'reserved') " . $condition . "
				ORDER BY zone";

$results = Db::get_rows($query);


$zone_id = R::number('zone_id');
$vars['block_selected'] = $zone_id?1:0;

if ($results) {
    foreach($results as $rs) {
        $zone_id == $rs["zone_id"]?$selected = 1:$selected = 0;

		// pel filtre
		if ( isset( $_GET['zone_ids'] ) &&
		     in_array( $rs["zone_id"], $_GET['zone_ids'] )
		) {
			$selected = 1;
			$vars['block_selected'] = 1;
		}
		
		/*
		  if (USE_FRIENDLY_URL) {
		  $friendly_params['zone_id'] = $rs['zone_id'];
		  $l['link'] = Page::get_link('inmo','property',false,false,$friendly_params,false,'',false,false);
		  } else {
		  $l['link'] = '/?tool=inmo&amp;tool_section=property&amp;zone_id=' . $rs['zone_id'] . '&amp;language=' . LANGUAGE;
		  }

		

		 */
		
		$l['link'] = get_link(array(
				'tool' => 'inmo', 
				'tool_section' => 'property', 
				'friendly_params' => array('tipus'=>$tipus==INMO_DEFAULT_TIPUS?'':$tipus,'zone_id'=>$rs['zone_id']),
				'link_vars' => array('tipus'=>$tipus),
				'detect_page' => $page_id?false:true,
				'page_id' => $page_id
			));
		
        $l['selected'] = $selected;
        $l['value'] = $rs['zone_id'];
        $l['item'] = $rs['zone'];
		$l['class'] = '';
        $loop2[] = $l;
    }
	
	$loop2[0]['class'] = 'first';	
	$loop2[count($loop2)-1]['class'] = 'last';
	
$loop[] = array ('loop' => $loop2,
    'item' => '');
}

$vars['var_name'] = 'zone_id';


?>