<div class="login form">
	<div class="col">
		<form name="theForm" method="post" action="<?= $link_action ?>" onsubmit="<?= $js_string ?>" target="save_frame">
			<h3><?= $c_is_customer ?></h3>
			<label>
				<?= $c_mail_log ?>
				<input name="mail" type="text" id="mail" class="account_input"/>
			</label>
			<label>
				<?= $c_password_log ?>
				<input name="password" type="password" id="password" class="account_input"/>
			</label>
			<input class="boto" type="submit" name="button" value="Ok"/>
		</form>
		<p class="lost_password">
			<a href="javascript:customer_show_password();"><?= $c_password_forgotten ?></a></p>

		<div id="password_form" class="hidden">
			<form name="passwordForm" method="post" action="<?= $link_send_password ?>" onsubmit="return validar(document.passwordForm,'mail','<?= $c_mail ?>','email1');" target="save_frame">
				<label><?= $c_password_insert_mail ?>
					<input name="mail" type="text" autocorrect="off" autocapitalize="none" id="mail_password" class="account_input"/></label>
				<input class="boto" type="submit" name="button" value="Ok"/>
			</form>
		</div>
	</div>

	<div class="col">

		<h3><?= $c_new_customer ?></h3>

		<p><?= $c_text_register ?></p>
		<a class="boto" href="<?= $link_register ?>"><?= $c_register ?></a>

	</div>

</div>