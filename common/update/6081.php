<?

/* 15-7-16

	INMO

	UP-DATE
	-Afegir camp de titular compte bancari

 */


if ( is_table( 'custumer__custumer' ) && ! is_field( 'custumer__custumer', 'account_holder' ) ) {

	$update_sql .= "ALTER TABLE `custumer__custumer`
	ADD COLUMN `account_holder` VARCHAR(200) NULL DEFAULT NULL AFTER `comta`;
";

} else {
	if ( is_table( 'inmo__property' ) ) {
		trigger_error( 'Ja existeixen el camp account_holder' );
	}
}

?>