<?
$gl_caption['c_init_session'] = 'Iniciar sessión';
$gl_caption['c_personal_data'] = 'Datos personales';
$gl_caption['c_already_custumer'] = 'Si ya eres cliente';
$gl_caption['c_register_now'] = 'Si no regístrate ahora';
$gl_caption['c_title_login'] = 'Inicio de sesión';
$gl_caption['c_register'] = 'Registrarse';
$gl_caption['c_make_reservation'] = 'Haz la reserva';
$gl_caption['c_book_host_main'] = 'Adulto 1';
$gl_caption['c_add_new_host'] = 'Añadir nuevo';
$gl_caption['c_edit_host'] = 'Editar';

$gl_caption['c_custumer_menu_personal_data'] = 'Datos personales';
$gl_caption['c_custumer_menu_books'] = 'Reservas';
$gl_caption['c_custumer_menu_book'] = 'Reserva';
$gl_caption['c_custumer_menu_logout'] = 'Desconectar';

$gl_caption_custumer['c_phone1'] = 'Teléfono';
?>