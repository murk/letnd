<?php
// menus eina
if (!defined('DIRECTORY_MENU_DOCUMENT')){
	define('DIRECTORY_MENU_DOCUMENT', 'Arxius');
	define('DIRECTORY_MENU_DOCUMENT_NEW', 'Entrar nou arxiu');
	define('DIRECTORY_MENU_DOCUMENT_LIST', 'Llistar i editar arxius');
	define('DIRECTORY_MENU_DOCUMENT_BIN', 'Paperera de reciclatge');
	define('DIRECTORY_MENU_CATEGORY', 'Carpetes');
	define('DIRECTORY_MENU_CATEGORY_NEW', 'Entrar nova carpeta');
	define('DIRECTORY_MENU_CATEGORY_LIST', 'Llistar carpetes');
	define('DIRECTORY_MENU_DOCUMENTUSER', 'Arxius');
	define('DIRECTORY_MENU_DOCUMENTUSER_LIST', 'Carpetes');
	define('DIRECTORY_COMMENT_DOCUMENTUSER', 'Només visualització d\'arxius sense permisos d\'edició');
}

$gl_messages_document['no_category']='Per poder insertar documents hi ha d\'haver almenys una carpeta';

// captions seccio
$gl_caption_document['c_prepare'] = 'En preparació';
$gl_caption_document['c_review'] = 'Per revisar';
$gl_caption_document['c_public'] = 'Públic';
$gl_caption_document['c_archived'] = 'Arxivat';
$gl_caption_document['c_document'] = 'Arxiu';
$gl_caption_document['c_description'] = 'Descripció';
$gl_caption_document['c_entered'] = 'Data creació';
$gl_caption_document['c_modified'] = 'Data modificació';
$gl_caption_document['c_status'] = 'Estat';
$gl_caption_document['c_category_id'] = 'Carpeta';
$gl_caption_document['c_filter_category_id'] = 'Totes les carpetes';
$gl_caption_document['c_filter_status'] = 'Tots els estats';
$gl_caption_document['c_file']='Arxius';
$gl_caption_document['c_videoframe']='Video (youtube, vimeo...)';
$gl_caption_document['c_download_all']='Descarregar carpeta';
$gl_caption_document['c_download_selected']='Descarregar seleccionats';
$gl_caption_document['c_download']='Descarregar';
$gl_caption_document['c_selected_zip_name']='arxius_seleccionats';
$gl_caption_document['c_zip_name']='arxius';

$gl_caption_category['c_category'] = 'Carpeta';
$gl_caption_category['c_category_description'] = 'Descripció';
$gl_caption_category['c_list_documents_button']='Veure arxius';
$gl_caption['c_document_icon']='';

$gl_caption['c_concession']='Permisos';
$gl_caption['c_concession_everybody']='Tothom';
$gl_caption['c_concession_change']='Canviar permisos';
$gl_caption['c_selected']='Permisos establerts';
$gl_caption_concession['c_group_id'] = 'Grup';
?>
