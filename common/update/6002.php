<?php

/* 18-11-14 
  
 * Immo: Inserto seasons

 */

	
	
if (is_table('booking__season') && !(Db::get_first("SELECT count(*) FROM booking__season"))){
	
	$update_sql = "
		INSERT INTO `booking__season_language` (`season_id`, `language`, `season`, `comment`) VALUES (1, 'cat', 'Baixa', '');
INSERT INTO `booking__season_language` (`season_id`, `language`, `season`, `comment`) VALUES (1, 'spa', 'Baja', '');
INSERT INTO `booking__season_language` (`season_id`, `language`, `season`, `comment`) VALUES (1, 'eng', 'Low', '');
INSERT INTO `booking__season_language` (`season_id`, `language`, `season`, `comment`) VALUES (1, 'fra', 'Basse', '');
INSERT INTO `booking__season_language` (`season_id`, `language`, `season`, `comment`) VALUES (1, 'deu', 'Neben', '');
INSERT INTO `booking__season_language` (`season_id`, `language`, `season`, `comment`) VALUES (2, 'cat', 'Mitja', '');
INSERT INTO `booking__season_language` (`season_id`, `language`, `season`, `comment`) VALUES (2, 'spa', 'Media', '');
INSERT INTO `booking__season_language` (`season_id`, `language`, `season`, `comment`) VALUES (2, 'eng', 'Medium', '');
INSERT INTO `booking__season_language` (`season_id`, `language`, `season`, `comment`) VALUES (2, 'fra', 'Moyenne', '');
INSERT INTO `booking__season_language` (`season_id`, `language`, `season`, `comment`) VALUES (2, 'deu', 'Mitte', '');
INSERT INTO `booking__season_language` (`season_id`, `language`, `season`, `comment`) VALUES (3, 'cat', 'Alta', '');
INSERT INTO `booking__season_language` (`season_id`, `language`, `season`, `comment`) VALUES (3, 'spa', 'Alta', '');
INSERT INTO `booking__season_language` (`season_id`, `language`, `season`, `comment`) VALUES (3, 'eng', 'High', '');
INSERT INTO `booking__season_language` (`season_id`, `language`, `season`, `comment`) VALUES (3, 'fra', 'Haute', '');
INSERT INTO `booking__season_language` (`season_id`, `language`, `season`, `comment`) VALUES (3, 'deu', 'Hoch', '');
INSERT INTO `booking__season_language` (`season_id`, `language`, `season`, `comment`) VALUES (4, 'cat', 'Molt alta', '');
INSERT INTO `booking__season_language` (`season_id`, `language`, `season`, `comment`) VALUES (4, 'spa', 'Muy alta', '');
INSERT INTO `booking__season_language` (`season_id`, `language`, `season`, `comment`) VALUES (4, 'eng', 'Very high', '');
INSERT INTO `booking__season_language` (`season_id`, `language`, `season`, `comment`) VALUES (4, 'fra', 'Très haute', '');
INSERT INTO `booking__season_language` (`season_id`, `language`, `season`, `comment`) VALUES (4, 'deu', 'Sehr hoch', '');
INSERT INTO `booking__season_language` (`season_id`, `language`, `season`, `comment`) VALUES (5, 'cat', 'Setmana Santa', '');
INSERT INTO `booking__season_language` (`season_id`, `language`, `season`, `comment`) VALUES (5, 'spa', 'Semana Santa', '');
INSERT INTO `booking__season_language` (`season_id`, `language`, `season`, `comment`) VALUES (5, 'eng', 'Holy Week', '');
INSERT INTO `booking__season_language` (`season_id`, `language`, `season`, `comment`) VALUES (5, 'fra', 'Pâques', '');
INSERT INTO `booking__season_language` (`season_id`, `language`, `season`, `comment`) VALUES (5, 'deu', 'Ostern', '');
INSERT INTO `booking__season_language` (`season_id`, `language`, `season`, `comment`) VALUES (6, 'cat', 'Nadal', '');
INSERT INTO `booking__season_language` (`season_id`, `language`, `season`, `comment`) VALUES (6, 'spa', 'Navidad', '');
INSERT INTO `booking__season_language` (`season_id`, `language`, `season`, `comment`) VALUES (6, 'eng', 'Christmas', '');
INSERT INTO `booking__season_language` (`season_id`, `language`, `season`, `comment`) VALUES (6, 'fra', 'Noël', '');
INSERT INTO `booking__season_language` (`season_id`, `language`, `season`, `comment`) VALUES (6, 'deu', 'Weihnachten', '');

INSERT INTO `booking__season` (`season_id`, `ordre`, `color_id`) VALUES (1, 1, 1);
INSERT INTO `booking__season` (`season_id`, `ordre`, `color_id`) VALUES (2, 2, 5);
INSERT INTO `booking__season` (`season_id`, `ordre`, `color_id`) VALUES (3, 3, 8);
INSERT INTO `booking__season` (`season_id`, `ordre`, `color_id`) VALUES (4, 4, 9);
INSERT INTO `booking__season` (`season_id`, `ordre`, `color_id`) VALUES (5, 7, 11);
INSERT INTO `booking__season` (`season_id`, `ordre`, `color_id`) VALUES (6, 5, 14);

	";	
	
}
?>