<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<?=load_admin_css()?>
<link href="/common/jscripts/jquery-ui-1.8.20/css/ui-lightness/jquery-ui-1.8.20.custom.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/common/jscripts/jquery/jquery-1.7.2.min.js"></script>
<? if (!DEBUG):?>
<script type="text/javascript" src="/common/jscripts/jquery.onerror.js?v=<?=$version?>"></script>
<? endif //DEBUG?>
<script type="text/javascript" src="/common/jscripts/jquery.dump.js?v=<?=$version?>"></script>
<script type="text/javascript" src="/common/jscripts/jquery.mousewheel.js?v=<?=$version?>"></script>
<script type="text/javascript" src="/common/jscripts/jquery.timepicker.js?v=<?=$version?>"></script>
<script type="text/javascript" src="/common/jscripts/jquery.cookie.js?v=<?=$version?>"></script>
<script type="text/javascript" src="/common/jscripts/jquery.blockUI.js?v=<?=$version?>"></script>
<script type="text/javascript" src="/common/jscripts/jquery.dropshadow.js?v=<?=$version?>"></script>
<script type="text/javascript" src="/admin/jscripts/main.js?v=<?=$version?>"></script>
<script type="text/javascript" src="/admin/languages/<?=LANGUAGE?>.js?v=<?=$version?>"></script>


<script language="JavaScript" src="/admin/modules/product/jscripts/functions.js?v=<?=$version?>"></script>
<script language="JavaScript" src="/admin/jscripts/assign.js?v=<?=$version?>"></script>
<script language="JavaScript" src="/admin/modules/product/jscripts/list_pick_frame.js?v=<?=$version?>"></script>
</head>
<body class="select-frame">
<div class="search">
	<form name="search" method="get" action="/admin/" target="listAddresses">
	<table width="100%" border="0" cellpadding="0" cellspacing="0" class="search_select_frame">   
		<tr valign="middle"> 
				<td><table border="0" cellspacing="0" cellpadding="0">
				  <tr valign="middle">
					<td><?=$category_select?></td>
				  </tr>
				</table></td>
			</tr>
	</table></form>
</div>
<table width="100%" height="1" border="0" cellpadding="0" cellspacing="0" bgcolor="#999999">
  <tr>
    <td></td>
  </tr>
</table>
<table border="0" cellspacing="0" cellpadding="0" height="448">
  <tr>
    <td width="610"><iframe id="listItems" scrolling="yes" marginwidth="0" marginheight="0" frameborder="0" vspace="0" hspace="0" style="overflow:visible; width:610px; height:448px;"  src="/admin/?action=list_records&process=select_variation&menu_id=<?=$menu_id?>"></iframe></td>
    <td width="240" class="assign_right" valign="top">
		<h5><?=$c_selected?></h5>
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		  <tr>
			<td id="write_cell"></td>
		  </tr>
		</table>
		</td>
  </tr>
</table>
<table width="100%" height="1" border="0" cellpadding="0" cellspacing="0" bgcolor="#999999">
  <tr>
    <td height="30" align="center">	
	  <form action="" method="post" name="theForm" target="save_frame">
      <div class="centerFloat1"><div class="centerFloat2">
	  <input class="btn_save_select_frame" type="button" name="Submit" value="<?=$c_ok?>" onClick="Assign.pass_selected_items()">
	  <input class="btn_select_frame" type="button" name="Submit" value="<?=$c_cancel?>" onClick="top.hidePopWin(false);"></div></div>
	</form>  	  
    </td>
  </tr>
</table>
</body>
</html>