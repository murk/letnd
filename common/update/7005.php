<?php
/* 4-4-17
Calendari families

*/
if ( is_table( 'product__product' ) ) {

	if ( ! is_table( 'product__familybusy' ) ) {

		$update_sql [] = "CREATE TABLE `product__familybusy` (
								`familybusy_id` INT(11) NOT NULL AUTO_INCREMENT,
								`family_id` INT(11) NOT NULL,
								`date_busy` DATE NOT NULL,
								`familybusy_entered` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
								PRIMARY KEY (`familybusy_id`),
								INDEX `family` (`family_id`)
							)
							COLLATE='utf8_spanish2_ci'
							ENGINE=MyISAM
							;
";

	} else {
		trigger_error( 'Ja existeixen la taula product__familybusy' );
	}

}

?>