<?php
/* 18-12-13
 * 
 * Block de circuits de la home de sahara
 * Nomes va amb USE_FRIENLY_URL
 *  
 */



$module = Module::load('sahara','circuit');
$module->parent = 'Block';

$loop = $module->do_action('get_records');


if (!$loop) {
	$show_block = false;
	return;
}

$loop_count = count($loop);
$conta = 0;

$query = "SELECT  page_file_name FROM all__page_language WHERE language='".LANGUAGE."' AND page_id = '11'";
			
$page_file_name = Db::get_first($query);

foreach ($loop as $key=>$val){
	$rs = &$loop[$key];
	$rs['class'] = '';
	$rs['conta'] = $conta++;
	$rs['loop_count'] = $loop_count;
	
	$friendly_params['place_id'] = $rs['place_id_value'];


	$rs['link'] = Page::get_link('sahara','circuit',false,$rs['circuit_id'],$friendly_params,false,$rs['circuit_file_name'],false,$page_file_name);
	
}

$loop[0]['class'] = 'first';	
$loop[count($loop)-1]['class'] = 'last';
?>