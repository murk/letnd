<?
/**
 * BookingBook
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) Juliol 2014
 * @version $Id$
 * @access public
 */
class BookingBook extends Module{
	
	var $show_booking, $show_results, $url, $one_property, $propertys, $property_id, $is_ajax = false, $reserved_message, $price_module, $form_template = '', $is_property_book = false, $is_book_edit = true, $is_add_custumers = false;
	
	function __construct(){
		parent::__construct();
		
	}
	function on_load(){ 		
		
		Main::load_class('booking', 'price', 'admin');
		$this->price_module = New BookingPrice($this);
		$this->price_module->action = $this->action;
		
		// valors al tpl per defecte
		$this->set_var('is_add_custumers',false);			
		$this->set_var('just_booked',false);
	}
	function add_custumers() {
		// nomes es fa si s'està logejat
		if(!BookingBook::is_logged()) {
			
			$module = Module::load('custumer', 'custumer', $this);
			$custumer = $module->do_action('show_login');
			
			return;
			
		}

		$account_html = '';
		if ( R::get( 'show_account' ) !== false ) {
			$module = Module::load('booking', 'payment', $this);
			$account_html = $module->do_action('show_account');
		}
		
		$this->action = 'show_form_edit';
		$this->is_add_custumers = true;
		$this->is_book_edit = false;
		$this->process = $_GET['process'] = 'add_custumers'; // forço el get perque surti al link_action
		$this->set_var('just_booked',true);
		$this->set_var('is_add_custumers',true);
		
		$GLOBALS['gl_content'] = $account_html . $this->get_form();
	}
	function get_rest_of_custumers(&$rs, $custumer_id) {		
		
		Main::load_class('booking', 'common', 'admin');
		$bcommon = New BookingCommon();
		$bcommon->caption = &$this->caption;
		return $bcommon->get_rest_of_custumers($rs, $custumer_id, $this->action);
		
	}
	
	function show_form()
	{
		// de moment no es poden retocar les reserves
		$this->set_field('adult','form_public','text');
		$this->set_field('child','form_public','text');
		$this->set_field('baby','form_public','text');
		$this->set_field('date_in','form_public','text');
		$this->set_field('date_out','form_public','text');
		
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_calendar($property_id) {
		
		include_once DOCUMENT_ROOT . '/admin/modules/booking/booking_calendar.php';
		$calendar = New BookingCalendar($this->process,true,$property_id);
		
		return $calendar->get_calendar_array();
	}

	function get_payments(&$show) {

		if ($this->config['has_payment'] == '0') return;

		$price_total = Db::get_first("SELECT price_total FROM booking__book WHERE book_id = '" . $show->id . "'");

		$is_whole = $show->rs["payment_1"] ==  $price_total;

		if ( $is_whole ) {
			$show->caption['c_payment_1'] = $show->caption['c_payment_whole'];
		}

		for ($i = 1; $i <= 4; $i++) {

			$show->set_field( "payment_$i", 'type', 'none' );
			$show->set_field( "payment_$i", 'form_public', 'text' );

			if ( $show->rs["payment_$i"] == '0.00' ) {
				$show->rs["payment_$i"] = false;
			}
			// Pendent de pagar
			elseif ( $show->rs["payment_done_$i"] == '0.00' ) {

				$pay_link = LANGUAGE . '/booking/payment/show-method/book_id/' . $show->id . '/';

				if ( $i > 1 ) {
					$pay_link .= '?payment_number=' . $i;
				}

				$pay_link = "<a class='button pay-link' href = \"" . HOST_URL . "$pay_link\">" . $this->caption['c_payment_pay'] . "</a>";

				 $show->rs["payment_$i"] = format_currency( $show->rs["payment_$i"] ) . ' ' . $this->config['currency_name'] . ' - <span class="payment-pending">' . $this->caption['c_payment_pending'] . '</span> ' . $pay_link;
			}
			// Pagat
			else {
				$show->rs["payment_$i"] = format_currency( $show->rs["payment_$i"] ) . ' ' . $this->config['currency_name'] . ' - <span class="payment-done">' . $this->caption['c_payment_paid'] . '</span>';
			}

		}

		$show->rs['show_account'] =
			$show->rs['payment_method_1'] == 'account' ||
			$show->rs['payment_method_2'] == 'account' ||
			$show->rs['payment_method_3'] == 'account' ||
			$show->rs['payment_method_4'] == 'account';

		$show->rs['concept'] = sprintf( $this->caption['c_book_form_title'], $show->id );

		Debug::p( $show->rs, 'rs' );

	}
	
	/*
	 * S'utilitza en 3 casos
	 * 
	 * 1 - Nova reserva desde una propietat -> $this->is_property_book
	 * 2 - Segon pas reserva
	 * 3 - Editar reserva     
		$GLOBALS['gl_page']->title = $this->caption['c_custumer_menu_book'];
	 * 
	 */
	
	function get_form()
	{
		if (($this->action == 'show_form_edit' || $this->action == 'get_form_edit') && !BookingBook::is_logged()){
			
			Main::redirect('/' . LANGUAGE . '/custumer/custumer/show-login/');
			
		}
				
		$show = new ShowForm($this);
		$show->has_bin = false;
		$show->get_values();
		
		// si editem, agafo de la bbdd els preus
		if ($show->id) {
			$show->set_field('price_book','form_public','text');
			$show->set_field('price_extras','form_public','text');
			$show->set_field('price_total','form_public','text');
		}
		
		// no es poden editar aquest camps, s'ha de esborrar reserva i tornar-hi, o per email
		if(($this->action == 'show_form_edit' || $this->action == 'get_form_edit')){
			
			$show->set_field('adult', 'form_public', 'text');
			$show->set_field('child', 'form_public', 'text');
			$show->set_field('baby', 'form_public', 'text');
			$show->set_field('date_in', 'form_public', 'text');
			$show->set_field('date_out', 'form_public', 'text');
		}

		// cas 3, nomes quan s'edita reserva
		if (!$this->parent){
			$this->get_payments( $show );
		}

		
		$date_in = isset($show->rs['date_in'])?$show->rs['date_in']:0;
		$date_out = isset($show->rs['date_out'])?$show->rs['date_out']:0;
		$adult = isset($show->rs['adult'])?$show->rs['adult']:R::get('adult');
		$child = isset($show->rs['$child'])?$show->rs['$child']:R::get('child');
		
		if ($this->property_id) $property_id = $this->property_id;
		else $property_id = $show->rs['property_id'];
		
		$this->set_vars( [
			'property_id' => $property_id,
		] );
				
		// calendari
		$this->set_vars($this->get_calendar($property_id));
				
		// form booking	
		if ($this->is_property_book) {
			
			
			Main::load_class('booking', 'common', 'admin');
			BookingCommon::set_book_vars($this, $property_id);			
			
			$this->set_field('date_in', 'type', 'hidden');
			$this->set_field('date_out', 'type', 'hidden');
			
		}
		
		
		$this->merge_module('custumer', 'custumer', false);
		
		
		
		$show->hidden = '<input type="hidden" value="'.$property_id.'" name="property_id[0]" id="property_id_0"><input type="hidden" value="'.$property_id.'" name="old_property_id[0]" id="old_property_id_0">';
		
		if ($this->form_template) $show->template = $this->form_template;
		
		// custumer principal
		$module = Module::load('custumer', 'custumer', $this);
		//$module->never_mind_logged;
		$module->process = $this->process;
		$module->is_book_edit = $this->is_book_edit;
		$module->is_property_book = $this->is_property_book;
		$module->is_add_custumers = $this->is_add_custumers;
		$action = $this->action == 'show_record'?'get_record':'get_form_new';
		
		$custumer = $module->do_action($action);
		$customer_js_string = $module->js_string;

		// Dades de l'inmoble		
		$show->set_vars($this->get_property_vars($property_id));
		
		
		// resta de custumers
		$rest_of_custumers = $module->rs?$this->get_rest_of_custumers($show->rs, $module->rs['custumer_id']):'';
		
		// extres
		$is_editable = true;
		$now = unformat_date(now(false, 1));

		// TODO-i Posar com a opcional
		// TODO-i Al guardar actualitzar totals de la reserva
		if ($this->is_book_edit && is_date_bigger($now, $date_in)) {
			$is_editable = false;
		}
		$module = Module::load('booking', 'extra', $this);
		$module->set_var('is_logged',BookingBook::is_logged());
		$extras = $module->get_book_extras($show->id, $property_id, $is_editable);

		// seasons
		$module = Module::load('booking', 'propertyrange', $this);
		$module->set_var('is_logged',BookingBook::is_logged());
		$module->property_id = $property_id;
		$seasons = $module->do_action('get_records');		
		
		// variables
		$show->set_vars(array(
			'custumer' => $custumer,
			'extras' => $extras,
			'seasons' => $seasons,
			'rest_of_custumers' => $rest_of_custumers,
			'is_logged' => BookingBook::is_logged()
			));				
		
		// preus
		$show->set_vars(
				$this->price_module->get_price(
						$date_in,
						$date_out, 
						$show->id, 
						$property_id,
						true,
						true,
						$adult,
						$child
						)
				);
		
		
		
		if ($this->is_index) {
			$GLOBALS['gl_page']->set_title(sprintf($this->caption['c_book_form_title'], $show->rs['book_id'], $show->rs['date_in'],$show->rs['date_out']));
		}
		$show->set_record();

		$GLOBALS['gl_page']->javascript .= '
			bookingform.js_string = "' . $show->js_string . '";';

		$show->set_var('js_string', "return validar(this" . $show->js_string . $customer_js_string . ');');

		return $show->process();
		
	}
	
	// Mostra reserva per una propietat
	// es crida desdel la fitxa propietats
	public function get_property_books()
	{
		$property_id = $this->property_id;		
		$this->action = 'get_form_new';	
		$this->is_property_book = true;
		$this->is_book_edit = false;
		$this->form_template = 'booking/property_book_form.tpl';
		
		return $this->get_form();
		
	}
	
	/*
	 * Calendari
	 */
	
	public function get_month() {
			
		include_once DOCUMENT_ROOT . '/admin/modules/booking/booking_calendar.php';
		$calendar = New BookingCalendar($this->process);
		
		return $calendar->get_month();
	}
	
	/*
	 * Calcul de preus
	 */
	
	function get_price_ajax() {
		$this->is_ajax = true;
		$this->price_module->reserved_message = $this->reserved_message;
		$this->price_module->get_price_ajax($this);
		
	}
	
	// Pas 2 de la reserva
	// Guarda formulari de custumer del segon pas amb la resta de les dades del custumer
	// Desprès de guardar envia contracte i mail de reserva
	function write_record_add_custumers($book_id)
	{			
		// si ha logejat assigno custumer_id

		if (BookingBook::is_logged()){
			$custumer_id = $_GET['custumer_id'] = $_SESSION['custumer']['custumer_id'];	
			$this->save_custumer();

			// envio mail al premer finalitzar reserva, que es l ultim pas per reservar, 
			// i ja em entrat totes les dades per fer el contracte
			// 
			// Obtinc preu pel mail;
			$rs = Db::get_row("SELECT property_id, date_in, date_out, adult, child, is_owner FROM booking__book WHERE book_id = '" . $book_id . "'");
			extract($rs);
			
			$this->property_id = $property_id;
			
			$prices = $this->price_module->get_price(
					$date_in,
					$date_out, 
					$book_id,
					$property_id,
					true,
					true,
					$adult,
					$child);
			
			// envio mail al finalitzar el segon pas, això s'ha canviat i ara es fa al primer pas
			/*if (!$is_owner)
				$this->send_mail_booked($book_id, $custumer_id, $prices);*/

			// redirigeixo a pàgina de reserva
			$redirect = '/' . LANGUAGE . '/booking/book/show-form-edit/book_id/' . $book_id . '/';
			print_javascript("top.window.location.href = '" .$redirect. "'");
		}


		// si ha caducat sessió
		else{
			// algo farem
		}

		Debug::p_all();
		die();
	}
	
	function write_record()
	{

		Main::load_class('booking', 'common', 'admin');
		$bcommon = New BookingCommon();

		if ($this->action == 'delete_record') die();
		///////// Guardar

		$writerec = new SaveRows($this);

		BookingCommon::set_portal_on_save( $writerec );
		
		$book_id = $writerec->id;
		$adult = $writerec->get_value('adult');
		$children = $writerec->get_value('child');
		$baby = $writerec->get_value('baby');
		
		$property_id = $this->property_id =  $writerec->get_value('property_id');
		// si no hi ha property_id ( quan editem nomes reserva) l'agafo de la reserva
		if (!$property_id){
			$property_id = Db::get_first("SELECT property_id FROM booking__book WHERE book_id = " . $book_id);
		}
		
		$date_in2 = $writerec->get_value('date_in'); 
		$date_out2 = $writerec->get_value('date_out'); 
		
		$date_in = unformat_date($date_in2);
		$date_out = unformat_date($date_out2);
		
		// giro les dates si la de començament es major que la final
		if ($date_in2 && $date_out2 && is_date_bigger($date_in, $date_out)){
			
			$t = $date_in2;
			$date_in2 = $date_out2;
			$date_out2 = $t;
			
			$_POST['date_in'][$book_id] = $date_in2;
			$_POST['date_out'][$book_id] = $date_out2;
			
			$t = $date_in;
			$date_in = $date_out;
			$date_out = $t;
			
		}	
		
		// guardo altres custumers
		$selected_custumers = R::post('selected_custumer_id');
		Debug::p($selected_custumers, 'Selected custumers');
		
		if ($selected_custumers){
			R::escape_array($selected_custumers);
			$query = New Query('', 'booking__book_to_custumer','book_id,ordre','custumer_id');
			foreach ($selected_custumers as $ordre => $selected_custumer_id) {
				if ($selected_custumer_id) {	
					$query->show_querys = true;
					$query->save_related($book_id, $ordre, $selected_custumer_id);
				}
			}
		Debug::p($selected_custumers, 'Selected custumers');
			// el principal no el borro mai
			$query->delete_condition = "ordre <> 1";
			$query->delete_others(); // agafa el primer fixe sempre
			
			unset($query);
		}
		
				
		// quan nomes guardo custumers, ja que $is_reserved es false llavors
		if ($this->process == 'add_custumers'){
			$this->write_record_add_custumers($book_id);
			return;		
			
		}
		
		
		// nomes comprovo si està reservat quan els camps son editables
		if ($date_in2 && $date_out2){
		
			$property = 'ref. ' . Db::get_first("SELECT ref FROM inmo__property WHERE property_id = $property_id");

			$is_reserved = $this->check_is_reserved($date_in, $date_out, $date_in2, $date_out2, $book_id, $property_id, $adult, $children, $baby);
			
			$resevation_change = true;
			

		}
		else{
			
			$resevation_change = false;
			
		}
		
		
		
		
		if (!$resevation_change || !$is_reserved)
		{
			
			if ($resevation_change) {
			
				if ($this->action == "save_record")
				{
						$writerec->message_saved = sprintf($this->messages['saved2'], $property, $date_in2, $date_out2);
				}
				else
				{
						$writerec->message_added = sprintf($this->messages['added2'], $property, $date_in2, $date_out2);					
				}	
			}
			// si ha logejat assigno custumer_id

			if (BookingBook::is_logged()){
				$custumer_id = $_SESSION['custumer']['custumer_id'];
				$writerec->save();
				$this->save_custumer(); // així dona sempre missatge de modificat, encara que no es modifiqui res de la reserva
			}

			// creo nou client si no ha logejat			
			elseif (isset($_POST['custumer_id']) && $this->action == "add_record"){
				$custumer_id = $this->save_custumer();			
							
				if ($custumer_id) {
					$writerec->save(); // nomes si s'ha pogut guardar custumer
					
					// assigno com a custumer principal
					Db::execute("UPDATE custumer__custumer SET book_group=" . $custumer_id . ", book_main=1 WHERE custumer_id=" . $custumer_id);
				}
			}
			
			// si ha caducat sessió per tant no ha omplert formulari nou client
			else{
				// algo farem
			}
			
			
			$book_id = $writerec->id; // agafo el nou id
			
			// en cas que hi hagi quantitat, borro si es 0 perque no es grabi a la bbdd
			foreach ($_POST['extra_book_selected'] as $extra_id){
				if (isset($_POST['quantity'][$extra_id])){
					if ($_POST['quantity'][$extra_id]==0)
						unset($_POST['extra_book_selected'][$extra_id]);
				}
			}

			Debug::p($_POST['extra_book_selected'], 'extra_book_selected');	
			
			// si es un nou formulari
			if ($this->action=='add_record'){
				
				
				// relaciono el client principal
				Debug::p($custumer_id, 'custumer_id');

				if ($custumer_id){								
					$query = New Query('', 'booking__book_to_custumer','book_id,custumer_id');
					$query->save_related($book_id, $custumer_id);
				}
							
				
			}
			
			// relaciona amb extres
			$query = New Query('', 'booking__extra_to_book', 'book_id,extra_id', 'extra_price');
			$query->messages = &$this->messages;
			$query->set_fixed_value($book_id);
			$query->set_config ('int','int','currency');	
			$query->save_related_post('extra_book_selected');
				
			// actualitzo quantitat
			$quantity = R::post('quantity');
			Debug::add('Quantity',$quantity);
			if ( $quantity ) {
				foreach ( $quantity as $key => $val ) {
					if ( $val != '0' ) {
						$query = "
						UPDATE booking__extra_to_book
							SET quantity= '" . $val . "'
							WHERE book_id = '" . $book_id . "'
							AND extra_id = '" . $key . "'";
						Db::execute( $query );
						Debug::add( 'Quantity', $query );
					}
				}
			}
			// Obtinc preu per guardar amb la reserva, abans de save();
			$prices = $this->price_module->get_price(
				$date_in,
				$date_out, 
				$book_id,
				$property_id,
				true,
				true,
				$adult,
				$children
			);

			// Guardo el promcode
			if ($prices['promcode_id'] && $this->action=='add_record'){
				Db::execute("INSERT INTO booking__custumer_to_promcode 
								(custumer_id, promcode_id) VALUES
								('". $custumer_id ."', '" . $prices['promcode_id'] . "');");
			}
			if (isset($_SESSION['book']['promcode'])) unset($_SESSION['book']['promcode']);

			// Guardo o actualitzo preus a la reserva
			// o faig sempre, ja s'encarrega la clase prices de si ho agafa de la bbdd o no
			Db::execute("
				UPDATE booking__book
					SET promcode_discount = ${prices['promcode_discount_value']},
					price_book= ${prices['price_book_value']}, 
					price_extras = ${prices['price_extras_value']}, 
					price_tt = ${prices['price_tt_value']}, 
					price_total = ${prices['price_total_value']} WHERE  
					book_id = '$book_id';
				");

			if ($this->action=='add_record') {

				// Poso valors inicials de payment_1 i payment_2 i contracte i default_status
				$initial_rs = $bcommon->get_initial_percent();

				if ($initial_rs['whole_import'] == '0') {
					$payment_1 = round( $prices['price_book_promcode_discounted_value'] * $initial_rs['initial_percent'] / 100, 2 );
				}
				else {
					$payment_1 = round( $prices['price_total_value'] * $initial_rs['initial_percent'] / 100, 2 );
				}
				$payment_2 = $prices['price_total_value']-$payment_1;

				$update_sql = "UPDATE booking__book
						SET
						payment_1=".$payment_1.",
						payment_2=".$payment_2.",
						contract_id=".$initial_rs['contract_id'].",
						status='".$this->config['public_default_status']."'
					WHERE book_id='".$book_id."'";

				Db::execute($update_sql);

				// Marco com a propietari
				if ($prices['is_owner']){
					Db::execute("
						UPDATE booking__book
							SET is_owner=1 WHERE  
							book_id='".$book_id."';
					");
					print_javascript("top.window.location.href = top.window.location.href");
				}
				else {
				
					// envio mail -> envio quan es prem finalitzar reserva	
					if (!$prices['is_owner']) {
						/*if ( ! $GLOBALS['gl_is_local'] )
							$this->send_mail_booked( $book_id, $custumer_id, $prices );*/

						// Si no te pagament online, envio mail aquí
						$send_only_admin = $this->config['has_payment'];
						$this->send_mail_booked( $book_id, $custumer_id, $prices, [], false, $send_only_admin );
					}

					// redirigeixo a seguent pas reserves

					if ($this->config['has_payment']) {
						$redirect = '/' . LANGUAGE . '/booking/payment/show-method/book_id/' . $book_id . '/';
					}
					else {
						$redirect = '/' . LANGUAGE . '/booking/book/add-custumers/book_id/' . $book_id . '/';
					}
					print_javascript("top.window.location.href = '" .$redirect. "'");
				}

			}

			// Exporto al portal
            BookingCommon::export_to_portal($book_id, $this->action);
			
		}
		// si està ocupada la reserva
		else
		{	
			$gl_message = $this->reserved_message;			
			
			$GLOBALS['gl_page']->show_message($this->reserved_message);
			
			Debug::p_all();
			die();
		}
		
	}
	
	function save_custumer() {

		$module = Module::load('custumer','custumer');
		$module->parent = 'InmoCustomer';		
		
		$module->do_action(BookingBook::is_logged()?'save_record':'add_record');
		
		if ($module->saved) {
			return $module->id;
		}
		// sino guarda es perque el mail el te algú altre
		else{
			$js = "parent.$('#mail_0').addClass('input_error').focus();";
			print_javascript($js);
			$GLOBALS['gl_page']->show_message($GLOBALS['gl_message']);
			
			Debug::p_all();
			die();
		}			
		
	}
	
	
	
	function check_is_reserved($date_in, $date_out, $date_in2, $date_out2, $book_id, $property_id, $adult, $children, $baby) {
		
		Main::load_class('booking', 'common', 'admin');
		$bcommon = New BookingCommon();
		$bcommon->messages = $this->messages;
		$bcommon->reserved_message = &$this->reserved_message;
		$new_line = $this->is_ajax ? "\n" : '<br>';
		return $bcommon->check_is_reserved(
				$date_in, $date_out, $date_in2, $date_out2, $book_id, $property_id, $adult, $children, $baby, $new_line);
		
	}
	
	/**
	 * Saber si està loguejat
	 * @return
	 */
	static function is_logged(){
		return (isset($_SESSION['custumer']['custumer_id']) && isset($_SESSION['custumer']['mail']));
	}
	
	
	//// funcio de TEST per veure com queda el mail quan estem maquetant, nomès en local w.elquesigui, order_id i payment_method es opcional
	// TEST    /cat/booking/book/send_mail_test/
	// TEST    /cat/booking/book/send_mail_test/?book_id=1
	
	
	
	function send_mail_test(){	
		if (!$GLOBALS['gl_is_local']) Main::error_404();
		
		$book_id =R::id('book_id');
		if (!$book_id){
			$book_id = Db::get_first('SELECT book_id FROM booking__book LIMIT 1;');
		}
		$custumer_id = Db::get_first('SELECT custumer_id FROM booking__book_to_custumer WHERE book_id = ' . $book_id);

		$this->property_id = Db::get_first("SELECT property_id FROM booking__book WHERE book_id = " . $book_id);
		
		$this->send_mail_booked($book_id, $custumer_id, false, [], true);
	}
	
	function send_mail_booked($book_id, $custumer_id, $prices, $payed_message = [], $is_test=false, $send_only_admin = false ){
		
		ini_set('max_execution_time', 120);

		$is_new_book = true; // la faré servir per mostrar text nova reserva, així es podrà reenviar el mail si fa falta
		
		// agafo dades reserva
		$old_action = $this->action;
		$this->action = 'get_record';
		$property_id = $this->property_id;
		
		$show = new ShowForm($this);
		$show->id = $book_id;
		$show->has_bin = false;
		$show->default_template = 'mail_form';
		$show->get_values();
		$this->get_payments( $show );
		
		$date_in = isset($show->rs['date_in'])?$show->rs['date_in']:0;
		$date_out = isset($show->rs['date_out'])?$show->rs['date_out']:0;
		$adult = isset($show->rs['adult'])?$show->rs['adult']:0;
		$child = isset($show->rs['child'])?$show->rs['child']:0;
		$status = $show->rs['status'];

		$book = $show->show_form();
		$this->action = $old_action;

		// dades banc
		$account_vars = [
			'show_account' =>
				$show->rs['payment_method_1_value'] == 'account' ||
				$show->rs['payment_method_2_value'] == 'account' ||
				$show->rs['payment_method_3_value'] == 'account' ||
				$show->rs['payment_method_4_value'] == 'account'
			,
			'banc_name'         => $this->config['banc_name'],
			'account_number'    => $this->config['account_number'],
			'concept'           => sprintf( $this->caption['c_book_form_title'], $book_id ),
		];
		
		
		
		// Agafo formulari del custumer, però amb el form.tpl per defecte
		$module = Module::load('custumer', 'custumer', '', false);
		$module->action = 'get_record';
		$show = new ShowForm($module);
		$show->id = $custumer_id;
		$show->set_field('password','form_public','no');
		$show->default_template = 'mail_form';
		$custumer = $show->show_form();		
		
		
		// extres
		$module = Module::load('booking', 'extra', $this);
		$module->is_mail = true;
		$module->set_var('is_logged',BookingBook::is_logged());
		$extras = $module->get_book_extras($book_id, $property_id, false);

		$page = clone $GLOBALS['gl_page'];
		$page->template = 'mail.tpl';
		$page->set_var('preview',$is_test);
		$page->set_var('remove_text','');
		$page->set_var('is_new_book',$is_new_book);
		$page->set_vars($this->caption);
		$page->set_vars($payed_message);
		$page->set_vars($account_vars);
		$page->set_vars(array(
			'book'=>$book, 
			'custumer'=>$custumer,
			'extras'=>$extras,
			'url' => HOST_URL,
			'short_host_url' => substr(HOST_URL,7,-1)
			));
		
		// preus
		if ($is_test) $prices = $this->price_module->get_price(
						$date_in,
						$date_out, 
						$book_id, 
						$property_id,
						false,
						true,
						$adult,
						$child
				);
		
		$page->set_vars($prices);
			
		
		// Dades de l'inmoble		
		$page->set_vars($this->get_property_vars($property_id));
		
		// Necessito mailer aquí per poder incrustar imatges en la plantilla
		$mailer =  $GLOBALS['gl_current_mailer'] = get_mailer($this->config);
		$body = $page->get();


		if($is_test) html_end($body);

		$instyler = new \Pelago\Emogrifier($body);
		$body = $instyler->emogrify();
		
		//echo $body;die();
		
		// envio mail
		if (is_file(CLIENT_PATH . 'images/logo_newsletter.jpg'))
			$mailer->AddEmbeddedImage(CLIENT_PATH . 'images/logo_newsletter.jpg', 'logo', 'logo.jpg');
		elseif(is_file(CLIENT_PATH . 'images/logo_newsletter.gif')) 
			$mailer->AddEmbeddedImage(CLIENT_PATH . 'images/logo_newsletter.gif', 'logo', 'logo.gif');
		
		$mailer->Body = $body;
		$mailer->Subject = $this->caption['c_subject_booked_owner'] . $book_id;
		$mailer->IsHTML(true);
		$mailer->AddAddress ($this->config['default_mail']);
		send_mail($mailer, false);
		
		if ($send_only_admin) return;

		$mailer->ClearAllRecipients( );
		$mailer->Body = $body;
		$mailer->Subject = $this->caption['c_subject_booked'] . $book_id;
		$mailer->AddAddress ($_SESSION['custumer']['mail']);

		// TODO Envio contracte o es pot descarregar a public en qualsevol estat ( no tenim cap client que a public només es faci pre-reserva )
		// L'usuari l'ha de poder descarregar per poder enviar per mail i confirmar reserva o no

		/*if (
			$this->config['has_contracts'] &&
			( $status == 'booked' || $status == 'confirmed_email' || $status == 'confirmed_post' || $status == 'contract_signed')
		) {*/


		if ($this->config['has_contracts']) {
			$contract = $this->download_contract($book_id,true);
			$mailer->addStringAttachment($contract['string'], $contract['name']);
		}
		send_mail($mailer, false);
	}
	
	function login_ajax() {
		
		$module = Module::load('custumer', 'custumer',$this);
		$module->is_ajax = true;
		$message = $module->login();
		
		
		$is_logged = $ret['is_logged']= $message === true;
		
		if ($is_logged) {			
			
			$module = Module::load('custumer', 'custumer', $this);
			$ret['custumer_form'] = $module->do_action('get_form_new');
			$ret['custumer_form_js_string'] = $module->js_string;

		}
		else{
					
			$ret['message']= $message;
			
		}
		
		die (json_encode($ret));
	}
	
	
	
	/*
	 * 
	 * 
	 * Llistat de books del client al seu compte personal
	 * 
	 * 
	 */
	
	function get_records()
	{
		if (!BookingBook::is_logged()) return false; // en aquest modul nomès puc entrar per un altre posant logged = true
		
		Main::load_class('booking', 'common', 'admin');
		
		$listing = new ListRecords($this);
		$listing->has_bin = false;
		$listing->add_button('edit_book','tool=custumer&tool_section=custumer&action=show_book', 'button', 'before', '', '', false, false);
		
		if ($this->config['has_contracts']) {			
			$listing->add_button('download_contract','', 'button contract', 'after', '', '', false, false, '/?tool=booking&tool_section=book&action=download_contract');
		}
		Debug::p($listing->buttons['download_contract'], 'text');
		$listing->set_options(0,0,0,0,0,0,0,0,0,1);
		
		$listing->order_by = 'date_out DESC';
		
		$listing->call('records_walk','season_id',$this);
		
		$listing->condition = 'book_id IN (
			SELECT book_id 
			FROM booking__book_to_custumer 
			WHERE custumer_id = ' . $_SESSION['custumer']['custumer_id'] . '
				)';
		
	    return $listing->list_records();
	}
	function records_walk(&$listing) {
			
		$id = $listing->rs['book_id'];
		$date_in = $listing->rs['date_in'];
		$date_out = $listing->rs['date_out'];
		$property_id = $listing->rs['property_id'];
		$price_total = $listing->rs['price_total'];
		$is_owner =  $listing->rs['is_owner'];
		$status =  $listing->rs['status'];
		
		$ret['price_total'] = '<div class="price-total">' . format_currency($price_total) . ' €</div>';
		
		$ret += UploadFiles::get_record_images($property_id, true, 'inmo', 'property','property_');
		
		// boto contracte nomes quan hi ha cusutmer principal
		$custumer_id = BookingCommon::get_main_custumer($id);
		if ($this->config['has_contracts']) {
			
			// busco idioma preferit del usuari
			if ($custumer_id) {
				$prefered_language = Db::get_first("
					SELECT prefered_language 
						FROM custumer__custumer 
						WHERE custumer_id = " . $custumer_id);
				if ( !in_array($prefered_language, $GLOBALS['gl_languages']['public'])){
					$prefered_language = $this->config['contract_default_language'];					
				}
				
				$listing->buttons['download_contract']['params'] = '&language=' . $prefered_language;
			}

			//$listing->buttons['download_contract']['show'] = $custumer_id!==false && !$is_owner;

			/* Tots els estats ocupats
			(( $status == 'booked' || $status == 'confirmed_email' || $status == 'confirmed_post' || $status == 'contract_signed'));
			*/



			/*
			'pre_booked','booked','confirmed_email','confirmed_post','contract_signed','cancelled'
			*/
			// No mostro contracte només si s'ha cancelat
			$listing->buttons['download_contract']['show'] =
				$custumer_id!==false &&
				!$is_owner &&
				( $status != 'cancelled' && $status != 'pre_booked');
		}
		
		return $ret;
		
	}
	function get_property_vars($property_id) {
		
		$rs_property = Db::get_row("
			SELECT person, ref, property_private, property_file_name, inmo__property.property_id as property_id, tipus
			FROM inmo__property, inmo__property_language
			WHERE inmo__property.property_id = inmo__property_language.property_id
			AND language = '" . LANGUAGE . "'
			AND inmo__property.property_id = " . $property_id);
		
		
		$property_record_link = get_link(array(
				'id' => $rs_property['property_id'],
				'file_name' => $rs_property['property_file_name'],
				'tool' => 'inmo', 
				'tool_section' => 'property', 
				'link_vars' => array('tipus'=>$rs_property['tipus']),
				'detect_page' => true));
		$rs_property['property_record_link'] = $property_record_link;
		
		$rs_property +=UploadFiles::get_record_images($property_id, true, 'inmo', 'property','property_');
		
		return $rs_property;
	}
	function download_contract($book_id = false, $get=false) {

		if(!BookingBook::is_logged()) Main::error_404();

		Main::load_class('booking', 'common', 'admin');
		$bcommon = New BookingCommon();
		$bcommon->caption = &$this->caption;
		$ret = $bcommon->get_contract($this,$book_id, $get);
		if ($get) return $ret;
	}
}
?>