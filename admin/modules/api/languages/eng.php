<?php
if (!defined('API_MENU_AGENCIA')){
	// menus eina	
	define('API_MENU_AGENCIA','Agencies');
	define('API_MENU_AGENCIA_NEW','New agency');
	define('API_MENU_AGENCIA_LIST','List agencies');
	define('API_MENU_AGENCIA_BIN','Trash');
	
	define('API_MENU_ONEAGENCIA','Agency');
	define('API_MENU_ONEAGENCIA_EDIT','Edit agencya');
	define('API_MENU_ONEAGENCIA_LIST','List agent');
	define('API_MENU_ONEAGENCIA_LIST_FILES','Documents');
	define('API_COMMENT_ONEAGENCIA','Edit own agency and agents for the group "agents"');
	
	define('API_MENU_AGENTENQUESTA','Poll');
	define('API_MENU_AGENTENQUESTA_NEW','Send poll');
	define('API_COMMENT_AGENTENQUESTA','Poll for the group "agents"');
	
	define('API_MENU_AGENT','Agent');
	define('API_MENU_AGENT_NEW','New agent');
	define('API_MENU_AGENT_LIST','List agents');
	define('API_MENU_AGENT_BIN','Trash');

	define('API_MENU_AGENT_FILECAT_NEW','Nova categoria arxius');
	define('API_MENU_AGENT_FILECAT_LIST','Llistar categories arxius');
	
	define('API_MENU_SPECIALITY','Specialties');
	define('API_MENU_SPECIALITY_NEW','New specialty');
	define('API_MENU_SPECIALITY_LIST','List specialties');
	define('API_MENU_SPECIALITY_BIN','Trash');

	define('API_MENU_PAGINA','Page edition'); 
	define('API_MENU_PAGINA_NEW','Enter new page');
	define('API_MENU_PAGINA_NEW_SECOND','Enter new secondary page');
	define('API_MENU_PAGINA_LIST','List pages'); 
	define('API_MENU_PAGINA_LIST_SECOND','List secondary pages'); 
	define('API_MENU_PAGINA_BIN','Trash'); 

	define('WEB_MENU_BANNER','Banners');
	define('WEB_MENU_BANNER_NEW','New banner');
	define('WEB_MENU_BANNER_LIST','List banners');
	define('WEB_MENU_BANNER_LIST_BIN','Trash');


	define('API_MENU_CURS','Cursos');
	define('API_MENU_CURS_NEW','Nou curs');
	define('API_MENU_CURS_LIST','Llistar cursos');
	define('API_MENU_CURS_BIN','Paperera de reciclatge');

	define( 'UTILITY_MENU_UTILITIES', 'Utilitats' );
	define( 'UTILITY_MENU_BUY', 'Càlcul de despeses compra' );
	define( 'UTILITY_MENU_SELL', 'Càlcul de despeses venda' );
}

/*--------------- nomès castellà i català, no traduit a la resta -------------------*/

/*agencies*/
$gl_caption_agencia['c_agencia'] = 'Agency';
$gl_caption_agencia['c_agents'] = 'Agents';
$gl_caption_agencia['c_user_ids'] = 'Users';
$gl_caption_agencia['c_add_new_agent'] = 'Add agent';
$gl_caption_agencia['c_add_new_user'] = 'Add user';
$gl_caption_agencia['c_view_users'] = 'See users';
$gl_caption_agencia['c_enquesta'] = 'Poll';

$gl_caption_agent['c_agencia'] = 'Agency';
$gl_caption_agent['c_agent'] = 'Agent';
$gl_caption_agent['c_agent_description'] = 'Description';
$gl_caption_agent['c_nom'] = 'Name';
$gl_caption_agent['c_cognoms'] = 'Surname';
$gl_caption_agent['c_adresa'] = 'Address';
$gl_caption_agent['c_adress'] = 'Street';
$gl_caption_agent['c_numstreet'] = 'Number';
$gl_caption_agent['c_block'] = 'Block';
$gl_caption_agent['c_flat'] = 'Flat';
$gl_caption_agent['c_door'] = 'Door';
$gl_caption_agent['c_provincia_id'] = 'Province';
$gl_caption_agent['c_comarca_id'] = 'Comarca';
$gl_caption_agent['c_municipi_id'] = 'Town';
$gl_caption_agent['c_cp'] = 'Postal code';
$gl_caption_agent['c_telefon'] = 'Telephone';
$gl_caption_agent['c_telefon2'] = 'Telephone 2';
$gl_caption_agent['c_fax'] = 'Fax';
$gl_caption_agent['c_correu'] = 'E-mail';
$gl_caption_agent['c_correu2'] = 'E-mail 2';
$gl_caption_agent['c_mail'] = 'E-mail newsletter';
$gl_caption_agent['c_url1'] = 'Web';
$gl_caption_agent['c_url1_name'] = 'Web name';
$gl_caption_agent['c_codi_habitat'] = 'Code habitatsoft';
$gl_caption_agent['c_api_id'] = 'Number API';
$gl_caption_agent['c_aicat_id'] = 'Number AICAT';
$gl_caption_agent['c_nums'] = 'Identificators';

$gl_caption_agent['c_speciality_id'] = 'Specialties';
$gl_caption_agent['c_group_id'] = 'Group newsletter';

$gl_caption['c_filecat_id'] = $gl_caption['c_filecat'] = "Categoria";
$gl_caption['c_ordre'] = "Ordre";
$gl_caption['c_form_files_title'] = "Documents de l'agent: ";
$gl_caption['c_edit_files'] = "Documents";
$gl_caption['c_file'] = "Documents";

$gl_caption_speciality['c_speciality'] = 'Specialty';

$gl_caption['c_filter_actiu'] = 'All agents';
$gl_caption['c_actiu'] = 'Exercises';
$gl_caption['c_actiu_1'] = 'Exercises';
$gl_caption['c_actiu_0'] = 'Does not exercise';

$gl_caption['c_has_files_'] = "All - files / no files";
$gl_caption['c_has_files_yes'] = "Has files";
$gl_caption['c_has_files_no'] = "Has no files";

$gl_caption['c_filter_comarca_id'] = 'All comarques';
$gl_caption['c_filter_municipi_id'] = 'All towns';

$gl_caption['c_desactivar'] = 'Disable';
$gl_caption['c_activar'] = 'Enable';

// mapes
$gl_caption['c_edit_map'] = 'Location';
$gl_caption['c_search_adress']='Search address';
$gl_caption['c_latitude']='latitude';
$gl_caption['c_longitude']='longitude';
$gl_caption['c_found_direction']='found address';
$gl_caption['c_save_map'] = 'Save location';
$gl_caption['c_click_map'] = 'Click on a point within the map to mark location';
$gl_caption['c_form_map_title'] = 'Location';
$gl_messages['point_saved'] = 'New location correctly saved';

/*pagines*/
$gl_caption_pagina['c_edit_title'] = 'Page data';
$gl_caption_pagina['c_page_title_title'] = 'Page data';
$gl_caption_pagina['c_column_title1'] = 'First column';
$gl_caption_pagina['c_column_title2'] = 'Second column';
$gl_caption_pagina['c_column_title3'] = 'Third column';
$gl_caption_pagina['c_column_title4'] = 'Vídeo';
$gl_caption_pagina['c_column_title5'] = 'Google';
$gl_caption_pagina['c_column_title6'] = 'More information';

$gl_caption_pagina['c_breadcrumb_edit_level1'] = '"edit page"';
$gl_caption_pagina['c_breadcrumb_edit_level2'] = '"edit page"';
$gl_caption_pagina['c_breadcrumb_edit_level3'] = '"edit page"';
$gl_caption_pagina['c_breadcrumb_edit_list'] = '"list subsections"';

$gl_caption_pagina['c_subtitle_level2'] = 'List pages';
$gl_caption_pagina['c_subtitle_level3'] = 'List pages';

$gl_caption_pagina['c_filter_status'] = 'All states';
$gl_caption_pagina['c_filter_category_id'] = 'All categories';
$gl_caption_pagina['c_filter_paginatpl_id'] = 'All templates';

$gl_caption_pagina['c_edit_content_button'] = 'Edit page';
$gl_caption_pagina['c_edit_pagina_button'] = 'Edit datas';



$gl_caption_pagina['c_ordre'] = 'Order';
$gl_caption_pagina['c_entered'] = 'Created';
$gl_caption_pagina['c_modified'] = 'Last modification';
$gl_caption_pagina['c_paginatpl_id'] = 'Template profile';
$gl_caption_pagina['c_listtpl_id'] = 'Template list';
$gl_caption_pagina['c_link'] = 'Link';
$gl_caption_pagina['c_pagina_file_name'] = $GLOBALS['gl_caption']['c_file_name'];
$gl_caption_pagina['c_body_supertitle'] = 'Supertitle';
$gl_caption_pagina['c_body_supertitle2'] = 'Supertitle';
$gl_caption_pagina['c_body_supertitle3'] = 'Supertitle';
$gl_caption_pagina['c_title'] = 'Title';
$gl_caption_pagina['c_title2'] = 'Title';
$gl_caption_pagina['c_title3'] = 'Title';
$gl_caption_pagina['c_body_subtitle'] = 'Subtitle';
$gl_caption_pagina['c_body_subtitle2'] = 'Subtitle';
$gl_caption_pagina['c_body_subtitle3'] = 'Subtitle';
$gl_caption_pagina['c_pagina'] = 'Page name at the menu';
$gl_caption_pagina['c_header_1'] = 'Phrase header 1';
$gl_caption_pagina['c_header_2'] = 'Phrase header 2';
$gl_caption_pagina['c_subpagina'] = 'Subsections';
$gl_caption_pagina_list['c_pagina'] = 'Name';
$gl_caption_pagina['c_col_valign'] = $gl_caption_pagina['c_col_valign2'] = $gl_caption_pagina['c_col_valign3'] = 'Vertical Align';
$gl_caption_pagina['c_col_valign_top'] = $gl_caption_pagina['c_col_valign2_top'] = $gl_caption_pagina['c_col_valign3_top'] = 'Top';
$gl_caption_pagina['c_col_valign_middle'] = $gl_caption_pagina['c_col_valign2_middle'] = $gl_caption_pagina['c_col_valign3_middle'] = 'Middle';
$gl_caption_pagina['c_col_valign_bottom'] = $gl_caption_pagina['c_col_valign2_bottom'] = $gl_caption_pagina['c_col_valign3_bottom'] = 'Bottom';
$gl_caption_pagina['c_body_content'] = 'Content';
$gl_caption_pagina['c_body_content2'] = 'Content';
$gl_caption_pagina['c_body_content3'] = 'Content';
$gl_caption_pagina['c_prepare'] = 'In preparation';
$gl_caption_pagina['c_review'] = 'To review';
$gl_caption_pagina['c_public'] = 'Public';
$gl_caption_pagina['c_archived'] = 'Archived';
$gl_caption['c_status'] = 'Estat';
$gl_caption_pagina['c_pagina_id'] = 'Id';
$gl_caption_pagina['c_parent_id'] = 'Belongs to';
$gl_caption_pagina['c_custom_id'] = 'Personalization gallery';
$gl_caption_pagina['c_level'] = '';
$gl_caption_pagina['c_blocked'] = 'Block.';

$gl_caption_pagina['c_add_pagina_button'] = 'Enter new page here';
$gl_caption_pagina['c_list_subpagines'] = 'List all';
$gl_caption_pagina['c_add_subpagina'] = 'Add';
$gl_caption_pagina['c_add_subpagina2'] = 'Add subsection';
$gl_caption_pagina['c_url1_name']='Name URL';
$gl_caption_pagina['c_url1']='URL';
$gl_caption_pagina['c_url2_name']='Name URL(2)';
$gl_caption_pagina['c_url2']='URL(2)';
$gl_caption_pagina['c_url3_name']='Name URL(3)';
$gl_caption_pagina['c_url3']='URL(3)';
$gl_caption_pagina['c_videoframe']='Vídeo (youtube, metacafe...)<br />( 340 x 255 )';
$gl_caption_pagina['c_file']='Files';
$gl_caption_pagina['c_category_id']='Category';

$gl_caption['c_last_listing']='List previous';


$gl_caption_category['c_ordre']='Order';
$gl_caption_category['c_category']='Category';
$gl_caption_category['c_is_clubone'] = 'Menu associates';

$gl_messages['c_restore_items'] = "The marked elements could not be recovered, as the superior page to whom they belong must be recovered first";



/*banners*/
$gl_caption_banner['c_banner_id']='';
$gl_caption_banner['c_ordre']='Order';
$gl_caption_banner['c_url1']='Link';
$gl_caption_banner['c_url1_name']='Text at the link';
$gl_caption_banner['c_url1_target']='Target';
$gl_caption_banner['c_url1_target__blank']='A new window';
$gl_caption_banner['c_url1_target__self']='The same window';
$gl_caption_banner['c_file']='Image banner <br>(960x92 - 248x122)';
$gl_caption_banner['c_banner']='Banner';


$gl_caption_agentenquesta['c_send_new']='Send poll';
$gl_caption_agentenquesta['c_agencia_id']='Real Estate Agency';
$gl_caption_agentenquesta['c_nom_persona']='Name of the person answering the poll';
$gl_caption_agentenquesta['c_entered']='Date';
$gl_messages['enquesta_sent']='The poll has been sent correctly, thank for your participation';
$gl_messages['enquesta_not_sent']="The poll could not be sent, please try again";


// Cursos

$gl_caption_curs['c_prepare'] = 'En preparació';
$gl_caption_curs['c_archived'] = 'Arxivat';
$gl_caption_curs['c_content'] = 'Descripció';
$gl_caption_curs['c_entered'] = 'Creat';
$gl_caption_curs['c_modified'] = 'Modificat';
$gl_caption_curs['c_status'] = 'Estat';
$gl_caption_curs['c_subtitle'] = 'Subtítol';
$gl_caption_curs['c_videoframe'] = 'Vídeo';

$gl_caption['c_curs_file_name'] = $GLOBALS['gl_caption']['c_file_name'];
$gl_caption['c_curs_old_file_name'] = $GLOBALS['gl_caption']['c_old_file_name'];

$gl_caption_curs['c_filter_status'] = 'Tots els estats';

$gl_caption_curs['c_form_level1_title_curs']='Títols pàgina';
$gl_caption_curs['c_form_level1_title_timetable']='Característiques';
$gl_caption_curs['c_form_level1_title_url1_name']='Enllaços';
$gl_caption_curs['c_form_level1_title_file']='Arxius adjunts';
$gl_caption_curs['c_form_level1_title_page_title']='SEO';

$gl_caption_curs['c_url1_name']='Nom URL(1)';
$gl_caption_curs['c_url1']='URL(1)';
$gl_caption_curs['c_url2_name']='Nom URL(2)';
$gl_caption_curs['c_url2']='URL(2)';
$gl_caption_curs['c_url3_name']='Nom URL(3)';
$gl_caption_curs['c_url3']='URL(3)';

// Públic
$gl_caption_curs['c_curs'] = $gl_caption['c_curs_id'] = 'Course';
$gl_caption_curs['c_timetable'] = 'Timetable';
$gl_caption_curs['c_open'] = 'Open';
$gl_caption_curs['c_full'] = 'Closed';
$gl_caption_curs['c_finished'] = 'Finished';


$gl_caption_curs['c_speciality_id'] = 'Specialty';

$gl_caption_curs['c_start_dat'] = 'Start date';
$gl_caption_curs['c_end_date'] = 'Final date';
$gl_caption_curs['c_maximum_person'] = 'Maximum people';
$gl_caption_curs['c_weekday_id'] = 'Days';

$gl_caption['c_durada'] = 'Duration';
$gl_caption['c_modalitat'] = 'Modality';
$gl_caption['c_lloc'] = 'Place';
$gl_caption['c_adressa'] = 'Address';
$gl_caption['c_tipologia'] = 'Typology';
$gl_caption['c_convalidacio'] = 'Validations';
$gl_caption['c_credit'] = 'Credits';
$gl_caption['c_temari'] = 'Temary';
$gl_caption['c_ponent'] = 'Speakers';
$gl_caption['c_avaluacio'] = 'Evaluation system';
$gl_caption['c_preu'] = 'Price';
$gl_caption['c_descompte'] = 'Discount';
$gl_caption['c_titulacio'] = 'Degree';
$gl_caption['c_inscripcio'] = 'Inscription';

$gl_caption_curs['c_modalitat_none'] = "";
$gl_caption_curs['c_modalitat_presencial'] = "In-person";
$gl_caption_curs['c_modalitat_semi-presencial'] = "Semi-attendance";
$gl_caption_curs['c_modalitat_taller-practic'] = "Practical workshop";
$gl_caption_curs['c_modalitat_online'] = "Online";


// Importació
$gl_caption['c_import_title'] = 'Importació de plusvàlues';
$gl_caption['c_select_file'] = "Seleccionar arxiu excel";
$gl_caption['c_import_preview'] = "Previsualitzar";
$gl_caption['c_import'] = "Importar";