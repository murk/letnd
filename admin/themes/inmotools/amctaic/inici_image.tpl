<link href="/admin/themes/inmotools/styles/image_drag.css" rel="stylesheet" type="text/css">
<!--[if IE 6]>

<![endif]-->
<!--[if IE 7]>
<style>
#sort
{
	clear: both;
	width: 100%;
	height: 100%;
}

li.sortableitemPlaceholder
{
	height: 0px;
}
</style>
<![endif]-->
<style>
.sortableitemHandle {
  width:<?=$im_admin_thumb_w?>px;
  height:<?=$im_admin_thumb_h?>px;
}
</style>
<script type="text/javascript">
	var gl_gallery_images = <?= ImageManager::get_gallery_images($images) ?>;
	<?if ($results):?>
	<?else:?>
	image_uploader_link = '<?=$uploader_link?>';
	<?endif//results?>

	im_admin_thumb_w = '<?=$im_admin_thumb_w?>';
	im_admin_thumb_h = '<?=$im_admin_thumb_h?>';
	parent_id =  '<?=$parent_id?>';

</script>
<script language="JavaScript" src="/admin/jscripts/image_drag.js?v=<?=$version?>"></script>
<form method="post" action="<?=$link_action_edit_images?>" name="list" target="save_frame">
<input name="action" type="hidden" value="save_rows_save_records_images<?=$action_add?>">
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="form">
    <tr>
      <td><div id="form_buttons_holder"><table id ="form_buttons">
          <tr>
            <td class="buttons_top">
            <? if ($results):?>
	            <div class="right">
										<? if($back_button): ?><a class="btn_form_back"
										                          href="<?= $back_button ?>"></a><? endif //back_button?>
										<input type="submit" name="Submit" value="<?= $c_send_edit ?>" class="btn_form1"/>
									<a class="btn_form_delete" href="javascript:submit_list('save_rows_delete_selected_images<?= $action_add ?>', '<?= $c_confirm_images_deleted ?>', true)">
										<?= $c_delete_selected ?>
									</a></div>
            <? endif //results?></td>
          </tr>
        </table></div>
        <table width="100%" cellpadding="4" cellspacing="0">
          <tr>
            <td class="formCaptionTitle"><strong>
              <?=$c_edit_data?>
            </strong></td>
          </tr>
        </table>
          <table width="100%" cellpadding="4" cellspacing="0" class="formstbl">
          	<tr>
          		<td width="150" valign="top" class="formsCaption"><strong>
          			Frase
          			:</strong></td>
          		<td class="forms"><?=$frase?></td>
          		</tr>
          	</table>
          <table width="100%" border="0" cellspacing="0" cellpadding="0" class="formstbl">
            <tr>
            	<td id="ajax_list"><br /><table width="100%" border="0" cellspacing="0" cellpadding="0">
            		<tr>
            			<td width="20%" align="left" valign="middle" nowrap="nowrap">&nbsp;
            				<?=$split_results_count?></td>
            			<td align="center" valign="middle" nowrap="nowrap" ><?=$split_results?></td>
            			<td width="20%">&nbsp;</td>
            			</tr>
            		</table>
            		<?if (!$results):?><div class="separador"></div><?endif //results?>
            		<ul id="sort" class="sort">
            			<? foreach($loop as $r): extract ($r)?>
            				<? foreach($loop as $l): extract ($l)?>
            					<? if ($image_src_admin_thumb):?>
            						<li id="<?=$id?>" class="sortableitem">
            							<div id="im_container_<?=$conta?>" class="selectableitem_out">
            								<div id="im_bg_<?=$conta?>"><div class="sortableitemHandle"><img id="im_<?=$conta?>" src="<?=$image_src_admin_thumb?>"></div></div>

            								<div id="select_bg_<?=$conta?>"><input name="<?=$id_field?>[<?=$id?>]" type="hidden" value="<?=$id?>"><?=$ordre?><input name="image_id_<?=$conta?>" type="hidden" value="<?=$id?>">
            									<div class="small">
            										<input name="selected[<?=$conta?>]" type="checkbox" value="<?=$id?>" onclick="checkbox_onclick(this);">
            										<?=$c_delete?>
            										</div>
            									<div class="small">
            										<?=$main_image?>
            										<?=$c_main_image?>
            										</div>
            									</div>
            								</div>
            							</li>
            						<? endif //image_src_admin_thumb?>
            					<? endforeach //$loop?>
            				<? endforeach //$loop?>
           			<div class="cls"></div></ul></td>
            	</tr>
          </table>
          <table width="100%" cellpadding="4" cellspacing="2" class="formsButtonstbl">
            <tr>
              <td align="center" class="formsButtons"><table border="0" cellspacing="0" cellpadding="0">
                  <tr valign="middle">
                    <td><div class="centerFloat1">
                        <div class="centerFloat2">
                          <input type="submit" name="Submit" value="<?=$c_send_edit?>" class="btn_form1" /><div class="btn_sep"></div>
                          <a class="btn_form_delete" href="javascript:submit_list('save_rows_delete_selected_images<?=$action_add?>', '<?=$c_confirm_images_deleted?>', true)"><div class="btn_sep"></div>
                          <?=$c_delete_selected?>
                          </a></div>
                      </div></td>
                  </tr>
                </table></td>
            </tr>
          </table></td>
    </tr>
</table>
</form>
<table width="100%" cellpadding="0" cellspacing="0" class="form">
	<tr>
		<td class="formCaptionTitle formCaptionTitleImageUploader"><?=$c_insert?> </td><td class="formCaptionTitle formCaptionTitleImageUploader formCaptionTitleCheckbox" align="right"><input type="checkbox" id="show_java_uploader"> <?= $c_insert_advanced ?></td>
	</tr>
</table>
<div class="form images"><span class="dropzone" id="dropzone<?= $parent_id ?>"/></span></div>
<iframe src="<?= $uploader_link ?>" scrolling="no" allowtransparency="true" id="imagesFrame" name="imagesFrame"></iframe>