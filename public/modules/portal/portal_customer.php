<?
class PortalCustomer extends Module{
	var $condition = '', $is_map = false;
	function __construct(){
		parent::__construct();
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function list_map(){
		$this->is_map = true;
		$GLOBALS['gl_page']->template = ''; 
		$GLOBALS['gl_content'] = $this->do_action('get_records');
	}
	function search(){
		$this->condition = $this->search_list_condition_words();		
		$this->do_action('list_records');		
	}
	function search_list_condition_words ()
	{	    
		$q = R::escape('q');		
		$query = "SELECT customer_id FROM portal__customer where
				   telephone like '%" . $q . "%' OR
				   telephone_mobile like '%" . $q . "%' OR
				   fax like '%" . $q . "%' OR
				   adress like '%" . $q . "%' OR
				   mail like '%" . $q . "%' OR				   
				   url like '%" . $q . "%'
				   " ;
		$results1 = Db::get_rows ($query);		
		$_1 = count ($results1);
		$query2 = "SELECT customer_id FROM portal__customer_language where				   
				   customer_title like '%" . $q . "%' OR
				   customer_description like '%" . $q . "%'
				   "
				   ;
		$results2 = Db::get_rows ($query2);		
		$_2= count ($results2);
		if ($_1+$_2 > 0 ) {//faig trampa per si no hi ha cap resultat
		    $results = array_merge($results1,$results2 );
			 foreach ($results as $key)
                {
					$ids_arr[] = $key['customer_id'];
                }

                $ids = implode(',', $ids_arr);
			} else {$ids="NULL";}

		$query = "AND customer_id IN (" . $ids . ")";		
		$search_condition = $query;
	    $GLOBALS['gl_page']->title = TITLE_SEARCH;		
	    return $search_condition;
	}
	function get_records()
	{
		$listing = new ListRecords($this);
		if (PORTAL_HOME_TITLE) $GLOBALS['gl_page']->title = PORTAL_HOME_TITLE;
		if ($this->is_map) $listing->template = 'portal/customer_list_map';
		$listing->condition = "(status = 'onsale' OR status = 'sold') ";
		//sumo la condicicio si vinc d'una busqueda
		$listing->condition .= $this->condition;
		 //filto per categoria
		$category_id = R::id('category_id');
		if($category_id){			
		$listing->condition .= $this->filter_by_category($category_id);		
		}
		//filtro per preu
		$price_id = R::id('price_id');
		if($price_id){			
		$listing->condition .= ' AND price_id = '.$price_id;
		}								   				
		$listing->call_function['customer_description'] = 'add_dots';
		/*transformo el price_id pq enlloc de donar-me el resultat del idioma em dongui el id 
		ho necessito per la imatge del llistat*/
		$listing->set_field('price_id', 'select_fields','portal__price.price_id,portal__price.price_id');
		print_r ($this->condition);
		return $listing->list_records();
	}	
	function filter_by_category($category_id){
	$query = "SELECT customer_id FROM portal__customer_to_category where
				   category_id = ".$category_id ;
	$results = Db::get_rows ($query);
	$a = count($results);
	if ($a > 0) {	    	
			foreach ($results as $key)
                {
					$ids_arr[] = $key['customer_id'];
                }

                $ids = implode(',', $ids_arr);
			} else {$ids="NULL";}
	$query = " AND customer_id IN (" . $ids . ")";	
	return $query;	
	}
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{
	    $show = new ShowForm($this);
		$show->set_field('price_id', 'select_fields','portal__price.price_id,portal__price.price_id');	
		$show->get_values();
		$rs = &$show->rs;						
		
	    return $show->show_form();
	}
	
	function manage_images()
	{
	    $image_manager = new ImageManager($this);
	    $image_manager->execute();
	}
}
?>