<?php
// no es poden posar ; entre mig de la consulta ja que parteix les consultes per el ; a no ser que fem servir $update_sql en array()
	
if (is_table("product__family")){	
	$update_sql .="
ALTER TABLE `product__family`
	ADD COLUMN `destacat` TINYINT(1) NOT NULL DEFAULT '0' AFTER `observations`;

ALTER TABLE `product__product_language`
	ADD COLUMN `page_title` VARCHAR(255) NOT NULL DEFAULT '' AFTER `product_description`;

ALTER TABLE `product__product_language`
	ADD COLUMN `page_description` VARCHAR(255) NOT NULL DEFAULT '' AFTER `page_title`,
	ADD COLUMN `page_keywords` VARCHAR(255) NOT NULL DEFAULT '' AFTER `page_description`,
	ADD COLUMN `product_file_name` VARCHAR(100) NOT NULL DEFAULT '' AFTER `page_keywords`;

INSERT INTO `all__configadmin` (`name`,`value`) VALUES ('allow_seo','0');";
}	
if (is_table("news__new")){	
	$update_sql .="
ALTER TABLE `news__new_language`
	ADD COLUMN `page_title` VARCHAR(255) NOT NULL DEFAULT '' AFTER `url2`,
	ADD COLUMN `page_description` VARCHAR(255) NOT NULL DEFAULT '' AFTER `page_title`,
	ADD COLUMN `page_keywords` VARCHAR(255) NOT NULL DEFAULT '' AFTER `page_description`,
	ADD COLUMN `new_file_name` VARCHAR(100) NOT NULL DEFAULT '' AFTER `page_keywords`;

ALTER TABLE `news__new`
	ADD COLUMN `destacat` TINYINT(1) NOT NULL DEFAULT '0' AFTER `in_home`;";
}

?>