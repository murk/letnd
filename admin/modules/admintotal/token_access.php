<?php
/*
 * Access per token desde php
 * Sempre ha de ser application/json
 *
 * Només per funcions expressament autoritzades
 */

//define(DEBUG, true); per activar debug
include ('../../../common/classes/main.php');
Main::set_vars(); // defineixo les 4 variables necessaries abans de fer res
Debug::start(); // començo el debug si s'escau
set_time_limit(0);

include ('../../../common/main.php'); // carrego arxius que tenen variables globals, a la llarga ha d'anar a Main::load_files();
Main::load_files(); // carrego includes i classes



// Obtinc variables
$json = file_get_contents('php://input');
if (!$json) die();
$obj = json_decode($json, true);

// file_put_contents(CLIENT_PATH . '/temp/export.log', $json . "\n", FILE_APPEND);

check_token($obj, $json);
include_once( DOCUMENT_ROOT . 'admin/modules/admintotal/export_common.php' );
ExportCommon::export_from_token_access( $obj, $json );

if (!ExportCommon::$is_ok) {
	Main::error_404();
}

Debug::p_all(); // mostro les variables del debug si toca