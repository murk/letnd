
<table class="letnd-list" border="0" cellspacing="0" cellpadding="0">
	<?foreach($loop as $l): extract($l)?>
	<tr>
		<td class="letnd-list-image <?=$first_last?>" valign="top">
			<?if ($images):?>
				<a title="<?=htmlspecialchars($new)?>" href="<?= HTTP ?><?= SHORT_HOST_URL ?><?=$show_record_link?>"><img src="<?=$image_src_thumb?>" width="180" border="0" alt="<?=htmlspecialchars($new)?>" /></a>
			<?endif //image?>
		</td>		
		<td class="letnd-list-details <?=$first_last?>" valign="top">
			<?if ($new):?>
				<h2>
					<a title="<?=htmlspecialchars($new)?>" href="<?= HTTP ?><?= SHORT_HOST_URL ?><?=$show_record_link?>"><?=$new?></a>
				</h2>
				<? endif ?>
				
			<?if ($subtitle):?><h3><?=$subtitle?></h3><? endif ?>
			
			<?if($content):?>
			<?=$content?>
			<?endif //default?>
			
			<p class="letnd-info"><a title="<?=htmlspecialchars($new)?>" href="<?= HTTP ?><?= SHORT_HOST_URL ?><?=$show_record_link?>"><?=$c_more_info?></a></p>
		</td>
	</tr>	
	<?endforeach //$loop?>
</table>