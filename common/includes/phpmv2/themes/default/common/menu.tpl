<!-- menu -->
<ul id="menu">
{foreach from=$menu key=menuIndex item=info}
	<li>
		<!--<a href="{$url_mod}&amp;mod={$info.modname};menu_id={$menu_id}">{$info.lang|translate}</a>-->
		<a href="{$url_mod2}&mod={$info.modname}&menu_id={$menu_id}">{$info.lang|translate}
		<ul>
		{foreach from=$info.submenus key=itemIndex item=lang}
			<li>
				<a href="{$url_mod}&mod={$info.modname}#a{$itemIndex + 1}">
				{$lang|translate|replace:" ":"&nbsp;"}
				</a>
			</li>
		{/foreach}
		</ul>
  </li>
{/foreach}
</ul>
<!-- /menu -->