var Invoice = {

	write_report: function (message) {
		var report = document.getElementById('message');
		report.innerHTML = report.innerHTML + message;

		report.scrollTop = report.scrollHeight;
	},

	confirm_send_mail: function (invoice_id) {
		if (confirm('Vols enviar factura a aquest client?')) {
			window.save_frame.location.href = "/admin/?menu_id=803&action=send_mail&invoice_id=" + invoice_id;
		}
	},

	confirm_send_mail_batch: function (batch_id) {
		if (confirm('Vols enviar factura a tots els clients de la remesa?')) {
			window.save_frame.location.href = "/admin/?menu_id=817&action=send_mail&batch_id=" + batch_id;
		}
	}

};
var Expense = {

	init: function () {
		if (gl_action == 'show_form_new' || gl_action == 'show_form_edit') {
			Expense.init_form();
		}
	},
	init_form: function () {
		var change_ids = "#price_" + gl_current_form_id +
			",#irpf_percent_" + gl_current_form_id +
			",#vat_included_" + gl_current_form_id +
			",#vat_exempted_" + gl_current_form_id +
			",#is_deductible_" + gl_current_form_id;

		$(change_ids).change(function () {

			var $price = $("#price_" + gl_current_form_id);
			var $irpf_percent = $("#irpf_percent_" + gl_current_form_id);
			var $vat_included = $("#vat_included_" + gl_current_form_id);
			var $vat_exempted = $("#vat_exempted_" + gl_current_form_id);

			var price = unformat_currency_js($price.val());
			var irpf_percent = unformat_currency_js($irpf_percent.val());
			var vat_included = $vat_included.is(':checked');
			var vat_exempted = $vat_exempted.is(':checked');


			var total_price_no_vat, total_vat, total_price, total_irpf, vat_percent = 0.21;

			if (vat_exempted) {
				vat_percent = 0;
			}


			if (vat_included) {

				total_price_no_vat = price / (1 + vat_percent - (irpf_percent / 100 ));
				total_price = price;
			}
			else {

				total_price_no_vat = price;
				total_price = price * (1 + vat_percent - (irpf_percent / 100 ));

			}

			total_irpf = total_price_no_vat * irpf_percent / 100;
			total_vat = total_price - total_price_no_vat + total_irpf;


			dp('vat_included', vat_included);
			dp('total_price_no_vat', total_price_no_vat);


			$('#total_price_no_vat_holder .forms td').html(format_currency_js(total_price_no_vat));
			$('#total_vat_holder .forms td').html(format_currency_js(total_vat));
			$('#total_price_holder .forms td').html(format_currency_js(total_price));
			$('#total_irpf_holder .forms td').html(format_currency_js(total_irpf));

		});
	}

};

var Recurring = {
	init: function () {
		this.init_generate_date();
	},
	init_generate_date: function () {

		var $top_date = $("#generate_top_date");
		$top_date.datepicker({
			changeMonth: true,
			changeYear: true,
			onSelect: function (dateText) {
				Recurring.change_top_date(this.value);
				display("Selected date: " + dateText + "; input's current value: " + this.value);
			}
		});

		if ($.fn.inputmask) {
			$top_date.inputmask(
				"d/m/y",
				{
					"placeholder": "dd/mm/yyyy"
				});
		}
	},
	change_top_date: function (generate_top_date) {
		window.location.href = '/admin/?menu_id=825&generate_top_date=' + generate_top_date;
	}
};

$(function () {
	if (gl_tool_section == 'expense') Expense.init();
	if (gl_tool_section == 'recurring') Recurring.init();
});
