<?
/*
10-06-2019

*/

if ( is_table( 'product__product_to_variation' ) && ! is_field( 'product__product_to_variation', 'variation_status' ) ) {

	$update_sql[] = "ALTER TABLE `product__product_to_variation`
	ADD COLUMN `variation_status` ENUM('disabled','nostock','onsale') NOT NULL DEFAULT 'onsale' AFTER `variation_others4`;";

}
else {
	if ( is_table( 'product__product_to_variation' ) ) {
		trigger_error( 'Ja existeixen el camp variation_status' );
	}
}