<?
$gl_caption['c_subject_booked'] = 'Réservation logement - #';
$gl_caption['c_subject_booked_owner'] = 'Réservation logement - #';
$gl_caption['c_no_date_selected'] = "Il n'y a pas de date sélectionnée";
$gl_caption['c_booked_message_1'] = "La réservation a été enregistrée avec succès.";
$gl_caption['c_booked_message_2'] = "A continuation, introduisez le reste de l'information";
$gl_caption['c_form_book_title'] = "Données de la réservation";
$gl_caption['c_form_book_custumer_title'] = "Données personnelles";
$gl_caption['c_book_finish_button'] = "Finaliser la réservation";
$gl_caption['c_book_form_title'] = "Réservation #%s";

$gl_caption['c_edit_book'] = "Éditer la réservation";

$gl_caption['c_mail_book_details'] = "Détails de la réservation";
$gl_caption['c_mail_custumer_details'] = "Données personnelles";
$gl_caption['c_mail_extras'] = "Extras";
$gl_caption ['c_ref']='Ref.';
$gl_caption['c_promcode_button'] = 'Appliquer';


$gl_caption_payment['c_payment_title'] = 'Faisant l\'achat';
$gl_caption_payment['c_method_title1']="Choisissez le mode de paiement que vous souhaitez utiliser.";
$gl_caption_payment['c_method_title2']="Le total est de <strong>%s&nbsp;%s</strong> TVA compris.";
$gl_caption_payment['c_method_title_initial']="Le paiement initial est <strong>%s&nbsp;%s</strong> TVA compris. Le reste du paiement, %s&nbsp;%s, doit être effectué avant la date d'entrée";
$gl_caption_payment['c_method_title_rest']="Le paiement initial est <strong>%s&nbsp;%s</strong> TVA compris. Le reste du paiement, %s&nbsp;%s, doit être effectué avant la date d'entrée";
$gl_caption_payment['c_method_title_rest']="Le montant du paiement %s est <strong>%s&nbsp;%s</strong> TVA compris.";
$gl_caption_payment['c_method_title_number_2']="deuxième";
$gl_caption_payment['c_method_title_number_3']="troisième";
$gl_caption_payment['c_method_title_number_4']="quatrième";

$gl_caption_payment['c_method_paypal']="Paiement avec PayPal";
$gl_caption_payment['c_method_account']="Paiement par virement bancaire";
$gl_caption_payment['c_method_lacaixa']="Carte de crédit / paiement";

$gl_caption_payment['c_show_paypal']="Paiement à Paypal en cours, si le processus ne démarre pas automatiquement, cliquez ici:";
$gl_caption_payment['c_show_lacaixa']="Paiement avec la carte en cours, si le processus ne démarre pas automatiquement, cliquez ici:";

$gl_caption['c_account_title']= 'Données du transference' ;
$gl_caption['c_account_amount']= 'Montant' ;
$gl_caption['c_account_concept']= 'Concept' ;

$gl_caption_book['c_payment_whole']="Paiement";
$gl_caption_book['c_payment_1']="Premier paiement";
$gl_caption_book['c_payment_2']="Deuxième paiement";
$gl_caption_book['c_payment_3']="Troisième paiement";
$gl_caption_book['c_payment_4']="Quatrième paiement";
$gl_caption_book['c_payment_paid']="Payé";
$gl_caption_book['c_payment_pending']="En attente";
$gl_caption_book['c_payment_pay']="Payer";
?>