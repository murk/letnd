<?php

/* 18-11-14 
  
 * Immo: Hora entrada i hora sortida

 */

	
	
if (is_table('inmo__property') && !(is_field('inmo__property','time_in'))){
	
	$update_sql = "
	ALTER TABLE `inmo__property`
		ADD COLUMN `time_in` TIME NOT NULL DEFAULT '0' AFTER `user_id`,
		ADD COLUMN `time_out` TIME NOT NULL DEFAULT '0' AFTER `time_in`;
	";	
	
}
?>