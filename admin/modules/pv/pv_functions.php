<?
	// Aquesta funció es crida cada cop que es carrega un mòdul a PV
	function pv_on_load (&$module){

		Main::load_class( 'pv', 'common', 'admin' );

		PvCommon::add_page_files( $module );

		PvCommon::set_security( $module );

		PvCommon::check_client_id( $module );

		PvCommon::set_client_menus( $module );

	}
?>