<?php
class Page {
    var $menu_tool;
    var $menu_all_tools;
    var $tpl;
    var $page_title = '';
    var $title;
    var $subtitle = '';
    var $title_right = '';
    var $message;
    var $news;
    var $content;
    var $before_content; // es pot utilitzar desde tool_function.php per carregar jscripts, css, etc... a totes les pàgines
    var $login;
    var $user_name;
    var $user_surname;
    var $template;
    var $tool;
    var $menu_group;
	var $show_menu;
    var $deleted_menus = array();
    var $query_delete_menus = '';
	public $javascript;
	var $is_image_form = false;
	var $is_frame = false;
    var $is_ajax = false;
	var $html_editors = array();
	var $last_menu = array();
	var $last_sub_menu = array();
	var $breadcrumb = array(); // array que guarda nous items definits pels moduls pel breadcumb a part dels que corresponen a la pàgina on som
	var $javascript_files = array();
	var $css_files = array();
	var $path = false;
	var $vars = array();
	var $menu_params = '';

    function __construct()
    {
        global $gl_db_max_results;
		$this->show_menu = true;
        $this->template = isset($_GET['template'])?$_GET['template'] . '.tpl':'general.tpl';
	    if ($this->template == 'none.tpl') $this->template = false;

        if ($this->template == 'print.tpl') {
            $this->menu_tool = false;
            $this->menu_all_tools = false;
            $gl_db_max_results = 1000000;
        } elseif ($this->template == 'clean.tpl' || $this->template == 'window.tpl' || !$this->template) {
		// aquestes plantilles van sense menu, per tant millor no calcular-lo
            $this->menu_tool = false;
            $this->menu_all_tools = false;
        } else {
            $this->menu_tool = true;
            $this->menu_all_tools = true;
        }

        $this->set_page_vars();
    }

    function load_template()
    {
    	$path = $this->path?$this->path:PATH_TEMPLATES;
        $this->tpl = new phemplate($path);
        $this->tpl->set_vars($this->vars);
        $this->tpl->set_file($this->template);
    }

    function set_page_vars()
    {
        global
        $gl_tool,
        $gl_tool_section,
        $gl_tool_top_id,
        $gl_tool_top_variable,
        $gl_db_classes_table,
        $gl_table_overrided,
        $gl_db_classes_id_field,
        $gl_action,
        $gl_process,
        $gl_parent_id,
        $gl_variable,
        $gl_menu_id,
        $gl_action_menu,
        $gl_print_link,
        $gl_write,
        $gl_read,
        $PHP_SELF;
        $query = "SELECT menu_group, tool, tool_section, action, parent_id, variable, process, write1, read1
              FROM all__menu
              WHERE menu_id=" . $gl_menu_id . " AND bin=0";
        $row = Db::get_row($query);

        $this->menu_group =  $row['menu_group'];
    	// així es poden sobreescriure per get o post
        $gl_tool =  $gl_tool?$gl_tool:$row['tool'];
        $gl_tool_section =  $gl_tool_section?$gl_tool_section:$row['tool_section'];

        $action =  $row['action'];
        $gl_parent_id =  $row['parent_id'];
        $gl_variable =  $row['variable'];
        $process =  $row['process'];
        $write1 =  $row['write1'];
        $read1 =  $row['read1'];


        //Debug::add ('Consulta menu actual' , $rs);
        $group_id = isset($_SESSION['group_id'])?$_SESSION['group_id']:'';
        $gl_read = strstr ($read1, ',' . $group_id . ',');
        $gl_write = strstr ($write1, ',' . $group_id . ',');

        $gl_print_link = "javascript:open_print_window('" . get_all_get_params(array('template', 'page'),'?','&',true) . "template=print');";
        if (!$gl_action) $gl_action = $action;
        $gl_action_menu = $action;
        if (!$gl_process) $gl_process = $process;
        if (!$gl_table_overrided) $gl_db_classes_table = $gl_tool . '__' . $gl_tool_section;
        if (!$gl_table_overrided) $gl_db_classes_id_field = $gl_tool_section . '_id';

        if (!isset($_GET['logout'])&&isset($_SESSION['login'])) {
		$this->login = $_SESSION['login'];
        $this->user_name = $_SESSION['user_name'];
        $this->user_surname = $_SESSION['user_surname'];
        }
        $this->tool = $gl_tool;

        $query = "SELECT menu_id, variable
              FROM all__menu
              WHERE parent_id=0 AND toolmode_id<>0
			  AND bin=0
			  AND menu_group='" . $this->menu_group . "'
              ORDER BY ordre";
        $rs = Db::get_rows($query);
        $gl_tool_top_id = $rs[0]['menu_id'];
        $gl_tool_top_variable = $rs[0]['variable'];
    }
    // Menu nivell 1
    // Loop per el menu de totes les eines de la intranet
    function make_menu_all_tools()
    {
        global $gl_tool_top_id, $gl_tool_top_variable;

        if ($this->deleted_menus){
            $this->query_delete_menus = 'AND menu_id NOT IN (' . implode( $this->deleted_menus, ',' ) . ')';
        }

        $query = "SELECT menu_group, menu_id, tool, action, parent_id, variable, link
              FROM all__menu
              WHERE parent_id=0 AND toolmode_id<>0
			  AND bin=0 " . $this->query_delete_menus  . "
              AND read1 LIKE '%," . $_SESSION['group_id'] . ",%'
              ORDER BY ordre";
        $results = Db::get_rows($query);

        $loop = array();

        foreach($results as $rs) {
			// admin total nomes admintotal
			if ($rs['menu_id']!='1000' || $_SESSION['user_id']=='1' || $_SESSION['user_id']=='2') {
				
				$rs['variable'] = constant($rs['variable']);
				if ($rs['menu_group'] == $this->menu_group) {
					$rs['selected'] = 'Selected';
				} else {
					$rs['selected'] = '';
				}
				$loop[] = $rs;
				
			}
        }
        $this->tpl->set_var('menu_tools', $loop);
    }
    // Menu nivell 2
    // Loop per les opcions i subopcions de la dreta, de l'eina on som
    function make_menu_tool()
    {
        global $gl_tool, $gl_tool_top_id;

        if ($this->deleted_menus){
            $this->query_delete_menus = 'AND parent_id NOT IN (' . implode( $this->deleted_menus, ',' ) . ')';
        }

        $query = "SELECT menu_id, variable, write1
              FROM all__menu
              WHERE parent_id=" . $gl_tool_top_id . "
              AND toolmode_id<>0
			  AND bin=0 " . $this->query_delete_menus  . "
              AND read1 LIKE '%," . $_SESSION['group_id'] . ",%'
              ORDER BY ordre";
        $results = Db::get_rows($query);
        $loop = array();

        foreach ($results as $rs) {
            $rs['menu_tools_3'] = $this->make_menu_tool_submenu($rs['menu_id'], $rs['write1']);
            $rs['variable'] = constant($rs['variable']);
            $loop[] = $rs;
        }
        $this->tpl->set_var('menu_tools_2', $loop);
    }
    // Menu nivell 3
    function make_menu_tool_submenu($menu_id, $write_permission)
    {
        global $gl_menu_id, $gl_parent_id;
		$selected_menu_id = isset($_GET['selected_menu_id'])?$_GET['selected_menu_id']:$gl_menu_id;

        strstr($write_permission, ',' . $_SESSION['group_id'] . ',')?$read_only = '':$read_only = " AND action <> 'show_form_new'";

        $query = "SELECT tool, menu_id, variable, link
              FROM all__menu
              WHERE parent_id=" . $menu_id . " AND toolmode_id<>0" . $read_only . "
			  AND bin=0 " . $this->query_delete_menus  . "
              AND read1 LIKE '%," . $_SESSION['group_id'] . ",%'" . // nou a museus
              "ORDER BY ordre";
        $results = Db::get_rows($query);

        $loop = array();

        foreach ($results as $rs) {
            // get_next_form() funciona mentres aquesta funció sigui nomès del nivell 3
            // (en 4 nivells de menus n'hi ha d'haver prou)
            if (($selected_menu_id == $rs['menu_id']) || ($gl_parent_id == $rs['menu_id'])) {
                $rs['selected'] = 'Selected';
                $this->set_next_form($rs['menu_id']);
            } else {
                $rs['selected'] = '';
            }

            if ($rs['variable']) {
	            $variable =  constant($rs['variable']);
                $rs['variable'] =  $variable;
	            if ($selected_menu_id == $rs['menu_id']) $this->page_title = $variable;
	            if ($selected_menu_id == $rs['menu_id']) $this->page_title = $variable;
            }
			
			// poso un ultim menu, per poder posar l'últim llistat, en principi nomès en vull un + un submenu
			$rs['last_menu'] = false;
			$rs['last_sub_menu'] = false;
			
			$last_menu_id = ($rs['menu_id']==$gl_parent_id)?$gl_menu_id:$rs['menu_id'];
			if (isset($this->last_menu[$last_menu_id])){
				$rs['last_menu'] = $this->last_menu[$last_menu_id][0];
				$rs['last_menu_sub'] = $this->last_menu[$last_menu_id][1];
			}
			if (isset($this->last_sub_menu[$last_menu_id])){
				$rs['last_sub_menu'] = $this->last_sub_menu[$last_menu_id];
			}
			
            $loop[] = $rs;
        }
        return $loop;
    }
    // set_next_form() funciona mentres aquesta funció sigui nomès del nivell 4
    // (en 4 nivells de menus n'hi ha d'haver prou)
    function set_next_form($menu_id)
    {
        global $gl_menu_id, $gl_parent_id, $gl_next_form_id;

        $id = ($menu_id == $gl_menu_id)?$gl_menu_id:$gl_parent_id;

        // query per trobar tots elsmenus de 4 nivell de l'eina
        $query_4_level = "
		SELECT menu_id
		FROM `all__menu`
		WHERE parent_id
		IN
			(
			SELECT menu_id
			FROM `all__menu`
			WHERE parent_id
			IN
				(
				SELECT menu_id
				FROM `all__menu`
				WHERE parent_id
				IN
					(
					SELECT menu_id
					FROM `all__menu`
					WHERE parent_id =0
					AND bin=0
					)
				)
				AND bin=0
			)
			AND bin=0
		AND bin=0
		AND tool = '".$this->tool."'
		";
		// miro si hi han fills d'aquest menu id, si no n'hi ha vol dir que som al nivell quatre
        // per tant faig query de tots els que tenen parent el parent_id
		$query = "SELECT menu_id
				FROM `all__menu`
				WHERE IF(
					(
					SELECT count( menu_id )
					FROM all__menu
					WHERE parent_id =" . $gl_menu_id . "
					) >1, parent_id =" . $gl_menu_id . ", parent_id =" . $gl_parent_id . "
				)
				AND bin=0
				AND toolmode_id<>0
				AND menu_id IN (".$query_4_level.")
				ORDER BY ordre";

        $results = Db::get_rows($query);
        $found = false;
        if ($results) {
            foreach ($results as $key => $rs) {
                if (($rs['menu_id'] == $gl_menu_id) && (isset($results[$key + 1]['menu_id']))) {
                    $gl_next_form_id = $results[$key + 1]['menu_id'];
                    $found = true;
                }
            }
            // si no l'hem trobat, vol dir que som al menu del nivell 3, per tant anem al segon form, el primer es el mateix que el nivell 3, repeteixo el primer formulari així tinc tots els formularis seguits en el nivell 4 i es mes facil gestionar
            // sempre hi ha d'aber almenys 2 formularis si no no te sentit posar aquest 4 nivell
            if (!$found) $gl_next_form_id = $results[1]['menu_id'];
            // si no està definit vol dir que es l'ultim, llavors el proxim torna a ser el primer
            if (!$gl_next_form_id) $gl_next_form_id = $results[0]['menu_id'];
        }
    }
    // Assignar totes les variables sencilles de la pàgina, titol, menus fixes, etc...
	// Just abans de fer el ->process
    function assign_variables()
    {
        global $gl_calendar, $gl_language, $gl_language_code, $gl_version, $gl_menu_id, $gl_variable, $gl_tool, $gl_tool_section, $gl_action, $gl_current_form_id, $gl_module;


	    $parent_tool = $parent_tool_section = $child_tool = $child_tool_section = '';
	    if ($gl_module)
	    {
		    if ( property_exists( $gl_module, 'parent_tool' ) ) {
			    $parent_tool = $gl_module->parent_tool;
		    }
		    if ( property_exists( $gl_module, 'parent_tool_section' ) ) {
			    $parent_tool_section = $gl_module->parent_tool_section;
		    }
		    if ( property_exists( $gl_module, 'child_tool' ) ) {
			    $child_tool = $gl_module->child_tool;
		    }
		    if ( property_exists( $gl_module, 'child_tool_section' ) ) {
			    $child_tool_section = $gl_module->child_tool_section;
		    }
	    }

	    if ($this->before_content)  $this->content =  $this->before_content . $this->content;

        $this->tpl->set_var('menu_home_public', MENU_HOME_PUBLIC);
        $this->tpl->set_var('menu_home', MENU_HOME);
        $this->tpl->set_var('menu_logout', MENU_LOGOUT);
        $this->tpl->set_var('page_title', $this->page_title?$this->page_title:PAGE_TITLE);
		$this->tpl->set_var('language', $gl_language);
		$this->tpl->set_var('language_code', $gl_language_code);
        $this->tpl->set_var('client_dir', CLIENT_DIR);
        $this->tpl->set_var('title', $this->title);
        $this->tpl->set_var('subtitle', $this->subtitle);
        $this->tpl->set_var('title_right', $this->title_right);
        $this->tpl->set_var('javascript', $this->javascript);
        $this->tpl->set_var('message', $this->message);
        $this->tpl->set_var('message_class', $this->message?'':'hidden');
        $this->tpl->set_var('news', $this->news);
        $this->tpl->set_var('content', $this->content);
        $this->tpl->set_var('login', $this->login);
        $this->tpl->set_var('user_name', $this->user_name);
        $this->tpl->set_var('user_surname', $this->user_surname);
        $this->tpl->set_var('user_text', USER_TEXT);
        $this->tpl->set_var('modal_text', USER_TEXT);
        $this->tpl->set_var('date', date('j-n-Y', time()));
        $this->tpl->set_var('calendar', $gl_calendar);
        $this->tpl->set_var('showiframe', DEBUG?'visible':'none');
		$this->tpl->set_var('show_menu',$this->show_menu);
		$this->tpl->set_var('is_image_form',$this->is_image_form);
		$this->tpl->set_var('version',$gl_version);
		$this->tpl->set_var('tool',$gl_tool);
		$this->tpl->set_var('tool_section',$gl_tool_section);
		$this->tpl->set_var('parent_tool',$parent_tool);
		$this->tpl->set_var('parent_tool_section',$parent_tool_section);
		$this->tpl->set_var('child_tool',$child_tool);
		$this->tpl->set_var('child_tool_section',$child_tool_section);
		$this->tpl->set_var('action',$gl_action);
		$this->tpl->set_var('current_form_id',$gl_current_form_id);
		$this->tpl->set_var('javascript_files',$this->javascript_files);
		$this->tpl->set_var('css_files',$this->css_files);
		$this->tpl->set_var('menu_params',$this->menu_params);
        $this->tpl->set_vars($GLOBALS['gl_caption']);

		$html_editors = "''";
		$html_editors_params = '';
		if ($this->html_editors){
			$json = new Services_JSON();
			$html_editors = $json->encode($this->html_editors);
			$show_html = $_SESSION['show_html_editor'];
			$html_editors_params =
				"language_code=".$gl_language_code."&amp;c_send_edit=".urlencode($GLOBALS['gl_caption']['c_send_edit'])."&amp;c_file_manager=".urlencode($GLOBALS['gl_caption']['c_file_manager'])."&amp;client_dir=".CLIENT_DIR."&amp;tinymce_indent=".TINYMCE_INDENT."&amp;v=".$gl_version."&amp;show_html=".$show_html;
			/* $html_editors_params =
			"language_code=".$gl_language_code."&amp;c_send_edit=".urlencode($GLOBALS['gl_caption']['c_send_edit'])."&amp;c_file_manager=".urlencode($GLOBALS['gl_caption']['c_file_manager'])."&amp;client_dir=".CLIENT_DIR."&amp;tinymce_indent=".TINYMCE_INDENT."&amp;v=".$gl_version."&amp;show_html=".$show_html."&amp;theme_advanced_fonts=".$GLOBALS['gl_config']['theme_advanced_fonts']."&amp;theme_advanced_font_sizes=".$GLOBALS['gl_config']['theme_advanced_font_sizes'];*/
		}
		
		
		$this->tpl->set_var('html_editors',$html_editors);
		$this->tpl->set_var('html_editors_params',$html_editors_params);

		$breadcrumb = '';
		if (count($this->breadcrumb)>0){
			$breadcrumb .= '<a href="?menu_id='.$gl_menu_id.'">'.constant($gl_variable).'</a> &gt; ';
			foreach ($this->breadcrumb as $b){
				if ($b['link']) {
					$breadcrumb .='<a href="'.$b['link'].'">'.$b['item'].'</a> &gt; ';
				}
				else{
					$breadcrumb .=$b['item'] . '&gt; ';			
				}
			}
		}
		$this->tpl->set_var('breadcrumb',substr($breadcrumb,0,-5));
    }
    // Mostrar la pagina un cop acabat
    function show()
    {
        global $gl_message, $gl_news, $gl_content, $gl_tool_top_variable, $gl_variable;
        // ho assigno aquí ja que necessito l'arxiu d'idiomes per aquesta variable
        if (!isset($this->title)) {
            //$this->title = constant($gl_tool_top_variable) . " - " . constant($gl_variable);
            $this->title = constant($gl_variable);
        }
        $this->message = isset($this->message)?$this->message:$gl_message; // així no mostra missatge quan $gl_page->message='';
        $this->news = $gl_news;
        $this->content = $gl_content;
        if (($this->template)) {
            $this->load_template();
            if ($this->menu_all_tools && (isset($_SESSION['group_id']))) $this->make_menu_all_tools();
            if ($this->menu_tool && (isset($_SESSION['group_id']))) $this->make_menu_tool();
            $this->assign_variables();
            echo $this->tpl->process();
        } else {
            echo $this->content;
        }
    }
	// obtindre la pagina manualment
	// el content li assigno jo
	// i el missatge també
	function get(){
		$this->load_template();
		$this->assign_variables();
		return $this->tpl->process();
	}
    function show_message($message, $target = 'top')
    {		
			
		$js = '
		if ('.$target.'.document.getElementById("message")){
			'.$target.'.document.getElementById("message_container").style.display="block";
			'.$target.'.document.getElementById("message").innerHTML = "'.addslashes($message).'";
		}
		if (typeof '.$target.'.hidePopWin == "function") '.$target.'.window.setTimeout("hidePopWin(false);", 800);
		if (typeof '.$target.'.showPopWin == "function") '.$target.'.showPopWin("", 300, 100, null, false, false, "'.addslashes($message).'");';

		if (!DEBUG && !$this->is_ajax) $js .='window.location.replace("/admin/themes/inmotools/blank.htm");';
        print_javascript($js);
    }
	// afegir items al breadcumb
	// es pot cridar Page::add_breadcrumb(.. per això el global
	static public function add_breadcrumb($item,$link=''){
		$GLOBALS['gl_page']->breadcrumb []= array('item'=>$item,'link'=>$link,'selected'=>0);
	}

	/**
     * Per que no es mostri un menú concret
     * @param $menu_id
     */
    public function delete_menu ($menu_id) {
       $this->deleted_menus []= $menu_id;
    }

	/**
	 * Afegir arxiu javascript al general.tpl
	 *
	 * @param $file - Nom del'arxiu ruta relativa a l'arrel
	 *
	 * @return bool
	 */
	static public function add_javascript_file( $file ) {
		if ( ! $GLOBALS['gl_page'] ) {
			return false;
		}
		$GLOBALS['gl_page']->javascript_files [] = array( 'javascript_file' => $file );
		return true;
	}

	/**
	 * Afegir arxiu css al general.tpl
	 *
	 * @param $file - Nom del'arxiu ruta relativa a l'arrel
	 *
	 * @return bool
	 */
	static public function add_css_file( $file ) {
		if ( ! $GLOBALS['gl_page'] ) {
			return false;
		}
		$GLOBALS['gl_page']->css_files [] = array( 'css_file' => $file );
		return true;
	}

	/**
	 * Afegir parametre als menus
	 *
	 * @param $params - Paràmetres
	 *
	 * @return bool
	 */
	static public function add_menu_params( $params ) {
		if ( ! $GLOBALS['gl_page'] ) {
			return false;
		}
		$GLOBALS['gl_page']->menu_params = '&' . $params;
		return true;
	}
	
	
	/*
	 * 
	 * 
	 * 
	 * 
	 * COPIA DE LA DE PUBLIC NOMES PER EL PREVIEW DE NEWSLETTER
	 * 
	 * 
	 * 
	 * 
	 */
    static public function get_page($page_id, $content = false, $get_file_name=false) // lo de content no està fet, es per si necessitessim un content de la pagina
    {
        global $gl_language, $gl_is_home;
        $query = "SELECT all__page.page_id as page_id, link, page, title, page_file_name, has_content
				FROM all__page_language, all__page
				WHERE all__page_language.page_id = all__page.page_id
				AND status = 'public'
				AND bin <> 1
				AND language = '" . $gl_language . "'
				AND all__page.page_id = " . $page_id;
        $results = Db::get_rows($query);
        if ($results) {
            extract (current($results));
			if ($get_file_name) return Page::get_page_link($page_id, $link, $page_file_name, $has_content, $get_file_name);
            $r['p_link'] = Page::get_page_link($page_id, $link, $page_file_name, $has_content);
            $r['p_item'] = $page;
			$r['p_title'] = $title?$title:$page;
			
			$g_id = R::id('page_id');
			$g_page_file_name = R::text_id('page_file_name');
			
            ($g_id == $page_id || $g_page_file_name == $page_file_name)?$r['p_selected'] = 1:$r['p_selected'] = 0;
            if (($link == '/') && ($gl_is_home)) $r['p_selected'] = 1;
            return $r;
        }else {
            return array();
        }
    }
	/*
	 * 
	 * 
	 * 
	 * 
	 * COPIA DE LA DE PUBLIC NOMES PER EL PREVIEW DE NEWSLETTER
	 * 
	 * 
	 * 
	 * 
	 */
    static public function get_page_link($page_id, &$link, $page_file_name, $has_content, $get_file_name=false, $language = LANGUAGE)
    {
		// si la pàgina no te contingut, agafo el link i tot del primer fill que es la pàgina que es mostra, així el link es exactamnet el mateix que el fill
		if (!$has_content){
			//$rs=Page::get_first_child($page_id);
			if ($rs){
				$link = $rs['link'];
				$page_id = $rs['page_id'];
				$page_file_name = $rs['page_file_name'];
			}
		}
		if ($get_file_name) return  $page_file_name?$page_file_name . '.html':'';
		/*
		BASE_URL_PUBLIC  ---> serveix per fer probes si no podem fer servir el index.php a public, no està fet per fer anar sempre la web així, per exemple no detectaria la homepage
		
		*/
		// JAVASCRIPT
        if (strpos($link, 'javascript:') === 0) {
            $ret = $link;
        } 
		// ENLLAÇ, s'ha de posar al camp link = link: , i agafa l'enllaç del camp page_file_name
		// ARXIU, s'ha de posar al camp link = file: , i agafa l'enllaç del camp page_file_name
        elseif ($link == 'file:' || $link == 'link:') {
            $ret = $page_file_name;
        } 
		// LINK A UN ALTRE MODUL
		elseif ($link)
        {
            if (USE_FRIENDLY_URL){
				$ret = '/' . $language;
				if ($link == '/') // la home page no porta el page_id
				{
					$ret .= '/';
				}
				else{
					$ret .= $link . '/' . $page_file_name . '.html';				
				}
			}
			else{
				$sep = strstr($link, '?')?'&amp;':'?';
				if (BASE_URL_PUBLIC){				
				$ret = '/'.BASE_URL_PUBLIC . '?' . $link . '&language=' . $language;
				}
				else{
				$ret = $link . $sep . 'language=' . $language;
				}
				if ($link != '/') // la home page no porta el page_id
				{
					$ret .= '&amp;page_id=' . $page_id;
				}
			}
        }
		// LINK A LA PAGINA
        else
        {
            if (USE_FRIENDLY_URL){
				$ret = '/' . $language . '/' . $page_file_name . '.html';
			}
			else{
				$ret = '/' . BASE_URL_PUBLIC . '?page_id=' . $page_id . '&amp;language=' . $language;
			}
        }
		// EVITAR DUPLICATS
		// si es la home, trec el link del menu d'idiomes
		if ($GLOBALS['gl_default_language']==$language && $link == '/') $ret = '/';
		
		return $ret;
		
    }

	// AQUESTES FUNCIONS SON PER COMPTABILITAT V2
	// Son les mateixes funcions del phemplate, serà més fàcil cambiar-ho per extends phemplate quan ja no hi hagi la variable global $tpl
	function set_var($name, $value)
    {
        $this->vars[$name] = $value;
    }
    function set_vars($vars, $clear = false)
    {
        if ($clear)
        {
            $this->vars = $vars;
        }
        else
        {
            if (is_array($vars)) $this->vars = array_merge($this->vars, $vars);
        }
    }
}
?>