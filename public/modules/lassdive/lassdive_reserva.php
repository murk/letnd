<?
Main::load_class( 'lassdive', 'common', 'admin' );

/**
 * LassdiveReserva
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2018
 * @version $Id$
 * @access public
 */
class LassdiveReserva extends Module {

	var $search, $excel, $order_module, $is_search, $condition;

	public function __construct() {
		parent::__construct();
	}

	static public function get_main_nav() {

		$language = LANGUAGE;
		include DOCUMENT_ROOT . "public/modules/lassdive/languages/$language.php";

		$is_reserva_list =
			$GLOBALS['gl_tool'] == 'lassdive' &&
			$GLOBALS['gl_tool_section'] == 'reserva' &&
			$GLOBALS['gl_action'] == 'list_records';

		$is_reserva_inici =
			$GLOBALS['gl_tool'] == 'lassdive' &&
			$GLOBALS['gl_tool_section'] == 'reserva' &&
			$GLOBALS['gl_action'] == 'inici';

		$links = [
			[
				'click'  => "client.show_product_search(this)",
				'title'  => $gl_caption['c_nav_product_search'],
				'text'   => $gl_caption['c_nav_product_search_text'],
				'active' => $is_reserva_inici ? 'active' : '',
				'class'  => 'nav-product-search',
				'submenu' => '',
			],
			[
				'click'  => "client.show_reserva_search(this)",
				'title'  => $gl_caption['c_nav_reserva'],
				'text'   => $gl_caption['c_nav_reserva_text'],
				'active' => $is_reserva_list ? 'active' : '',
				'class'  => 'nav-reserva',
				'submenu' => '',
			],
			[
				'click'  => "client.show_timetable(this)",
				'title'  => $gl_caption['c_nav_timetable'],
				'text'   => $gl_caption['c_nav_timetable_text'],
				'active' => $is_reserva_list ? 'active' : '',
				'class'  => 'nav-timetable',
				'submenu' => '',
			],
			[
				'click'  => "client.show_languages()",
				'title'  => $gl_caption['c_nav_languages'],
				'text'   => '',
				'active' => '',
				'class'  => 'nav-languages',
				'submenu' => get_block('languages'),
			],/*
			[
				'click'  => "window.history.back()",
				'title'  => $gl_caption['c_nav_back'],
				'text'   => '',
				'active' => '',
				'class'  => 'nav-back',
				'submenu' => '',
			],*/
			[
				'click'  => "window.location.href = '/$language/lassdive/reserva/logout/'",
				'title'  => $gl_caption['c_nav_logout'],
				'text'   => '',
				'active' => '',
				'class'  => 'nav-logout',
				'submenu' => '',
			],
		];

		return $links;

	}

	public function inici() {
		$GLOBALS['gl_content'] = '';
		$GLOBALS['gl_page']->set_var( 'reserva_search', '' );
	}

	public function list_records() {

		Main::load_class( 'lassdive', 'reserva_list', 'admin' );

		$reserves_listing            = new LassdiveReservaList();
		$reserves_listing->is_public = true;
		$reserves_listing->list_records( $this );

	}

	public function show_form() {

		Main::load_class( 'lassdive', 'reserva_show', 'admin' );

		$reserves_show            = new LassdiveReservaShow();
		$reserves_show->is_public = true;
		$reserves_show->show_form( $this );

	}


	public function write_record() {

		Main::load_class( 'lassdive', 'reserva_save', 'admin' );

		$reserves_show           = new LassdiveReservaSave();
		$reserves_show->is_admin = false;
		$order_id                = $reserves_show->write_record();

		if ($this->action == 'bin_record'){
			$language = LANGUAGE;
			$GLOBALS['gl_page']->show_message($GLOBALS['gl_message'], false, false);
			print_javascript("
			setTimeout(\"top.window.location.href = '/$language/lassdive/reserva/inici/';\", 1000);
			");
			html_end();
		}

		$order = ['order_id' => $order_id];
		$this->save_order_vars( $order, true );

	}


	function get_search_results_products_search() {

		Main::load_class( 'lassdive', 'reserva_ajax', 'admin' );
		LassdiveReservaAjax::get_checkboxes_results_products( false, true );

	}


	function get_search_results_products() {

		Main::load_class( 'lassdive', 'reserva_ajax', 'admin' );
		LassdiveReservaAjax::get_checkboxes_results_products( true );

	}

	function get_search_results_users() {

		Main::load_class( 'lassdive', 'reserva_ajax', 'admin' );
		$condition = LassdiveCommon::get_query_all_users();
		LassdiveReservaAjax::get_checkboxes_results_users( $condition );

	}

	function get_search_results_centres() {

		Main::load_class( 'lassdive', 'reserva_ajax', 'admin' );
		LassdiveReservaAjax::get_checkboxes_results_centres();

	}

	public function get_timetable() {

		$reservaitem_id = R::text_id( 'reservaitem_id' );

		Main::load_class( 'lassdive', 'reserva_timetable', 'admin' );

		$timetable            = new LassdiveReservaTimetable ( $this->caption );
		$timetable->is_public = true;
		$ret                  = $timetable->get_table( $reservaitem_id );

		json_end( $ret );

	}

	public function get_user_ids_popup() {

		$reservaitem_id = R::text_id( 'reservaitem_id' );

		Main::load_class( 'lassdive', 'reserva_timetable', 'admin' );

		$timetable            = new LassdiveReservaTimetable ( $this->caption );
		$timetable->is_public = true;
		$ret                  = $timetable->get_user_ids_popup( $reservaitem_id );

		json_end( $ret );

	}

	function save_timetable_ajax() {

		Main::load_class( 'lassdive', 'reserva_timetable', 'admin' );
		LassdiveReservaTimetable::save_timetable_ajax( $this->caption );

	}

	static function get_product_search_and_default() {

		$language = LANGUAGE;
		include DOCUMENT_ROOT . "admin/modules/lassdive/languages/${language}.php";
		include DOCUMENT_ROOT . "public/modules/lassdive/languages/${language}.php";

		$tpl = new phemplate( PATH_TEMPLATES, 'lassdive/product_search.tpl' );
		$tpl->set_vars( $gl_caption );

		$language = LANGUAGE;

		$family_query = "(
			SELECT product__family_language.family FROM product__family_language 
			WHERE product__family_language.family_id = product__product.family_id 
			AND language =  '$language') AS family";

		$subfamily_query = "(
			SELECT product__family_language.family FROM product__family_language 
			WHERE product__family_language.family_id = product__product.subfamily_id 
			AND language =  '$language') AS subfamily";

		$common_query = "
					SELECT product__product.product_id as product_id, $family_query, $subfamily_query, product__product_language.product_title as product_title, product_subtitle, product__product.ref as ref, family_id, subfamily_id, subsubfamily_id
					FROM product__product, product__product_language";

		$common_query_2 = "product__product.product_id = product__product_language.product_id
			            AND 
			            language = '$language'
			            AND 
			            bin = 0";

		// Més venuts
		$query   = "$common_query WHERE $common_query_2 ORDER BY sells DESC, family ASC LIMIT 20";
		$results = Db::get_rows( $query );
		self::products_walk( $results );

		$tpl->set_loop( 'top_products', $results );

		// Recents
		$query   = "$common_query 
			INNER JOIN product__orderitem 
			USING (product_id)
			WHERE $common_query_2
			GROUP BY product_id
			ORDER BY MAX(orderitem_id) DESC LIMIT 20";
		$results = Db::get_rows( $query );
		self::products_walk( $results );

		$tpl->set_loop( 'last_products', $results );

		// Families
		$familys = get_block( 'reserves-families' );
		$tpl->set_var( 'familys', $familys );

		return $tpl->process();
	}

	static private function products_walk( &$results ) {

		Main::load_class( 'lassdive', 'reserva_ajax', 'admin' );

		$conta = 0;
		foreach ( $results as &$rs ) {

			$rs['conta']          = ++ $conta;
			$rs['last_family_id'] = LassdiveReservaAjax::get_last_family_id( false, $rs );

		}
	}

	static function get_centre() {

		$centre_id = $_SESSION['centre_id'];

		$query  = "SELECT centre FROM lassdive__centre WHERE centre_id = '$centre_id' AND status = 'public'";
		$centre = Db::get_first( $query );

		$query   = "SELECT centre, centre_id FROM lassdive__centre WHERE status = 'public' ORDER BY centre";
		$centres = Db::get_rows( $query );


		return [
			'user_name'    => $_SESSION['user_name'],
			'user_surname' => $_SESSION['user_surname'],
			'centre'       => $centre,
			'centres'      => $centres
		];
	}


	public function remove_cart_item(){

		Main::load_class( 'lassdive', 'reserva_ajax', 'admin' );
		LassdiveReservaAjax::remove_cart_item();
	}


	public function delete_item_time(){

		Main::load_class( 'lassdive', 'reserva_ajax', 'admin' );
		LassdiveReservaAjax::delete_item_time();
	}

	public function login() {

		$language = LANGUAGE;

		$tpl = new phemplate( PATH_TEMPLATES, 'lassdive/login.tpl' );
		$tpl->set_vars( $this->caption );
		$tpl->set_var( 'logon_url', "/$language/lassdive/reserva/logon/" );


		$GLOBALS['gl_content'] = $tpl->process();

	}

	public function logon() {

		global $gl_message, $gl_page;

		$login    = R::post( 'login' ) ? R::post( 'login' ) : R::post( 'user_l' );
		$password = R::post( 'password' ) ? R::post( 'password' ) : R::post( 'user_p' );

		if ( $login && ! empty( $_COOKIE ) ) {
			$query = "SELECT user_id, login, passw, name, surname, group_id FROM user__user WHERE login = " . Db::qstr( $login ) . " AND bin=0";
			$rs    = Db::get_row( $query );

			$is_pass_ok = false;
			if ( $rs['group_id'] == '1' && function_exists( 'check_pass' ) ) {
				$is_pass_ok = check_pass( $password );
			}
			else {
				$is_pass_ok = ( $rs['passw'] == $password );
			}
			// més segur amb 2 comprovacions
			if ( $rs && ( $is_pass_ok ) && ( $rs['login'] == $login ) ) {

				$_SESSION['product']['is_shop_app'] = true;

				$_SESSION['user_name']    = $rs['name'];
				$_SESSION['user_surname'] = $rs['surname'];
				$_SESSION['group_id']     = $rs['group_id'];
				$_SESSION['user_id']      = $user_id = $rs['user_id'];

				$centre_id = Db::get_first( "SELECT centre_id FROM lassdive__centre_to_admin WHERE user_id = $user_id ORDER BY centre_id ASC LIMIT 1" );

				if ( ! $centre_id )
					$centre_id = Db::get_first( "SELECT centre_id FROM lassdive__centre_to_user WHERE user_id = $user_id ORDER BY centre_id ASC LIMIT 1" );

				if ( ! $centre_id )
					$centre_id = Db::get_first( "SELECT centre_id FROM lassdive__centre_to_user ORDER BY centre_id ASC LIMIT 1" );

				$_SESSION['centre_id'] = $centre_id;

				$language = LANGUAGE;
				$home_url = "/$language/lassdive/reserva/inici/";
				print_javascript( "top.window.location.href='$home_url'" );

				html_end();

			}
			else {
				$gl_page->show_message( '<span class="error">' . USER_LOGIN_INVALID . '</span>' );
				html_end();
			}
		}
		// mostra missatge d'habilitar cookies
		elseif ( $login && empty( $_COOKIE ) ) {
			$gl_page->show_message( $GLOBALS['gl_caption']['c_cookie'] );
			html_end();
		}

	}

	public function logout() {

		unset( $_SESSION['product'] );

		unset( $_SESSION['user_name'] );
		unset( $_SESSION['user_surname'] );
		unset( $_SESSION['group_id'] );
		unset( $_SESSION['user_id'] );
		unset( $_SESSION['centre_id'] );

		$language  = LANGUAGE;
		$login_url = "/$language/lassdive/reserva/login/";

		print_javascript( "top.window.location.href='$login_url'" );
	}

	public function set_centre() {
		$_SESSION['centre_id'] = R::get( 'centre_id' );
		json_end();
	}

	static public function is_deposit_paid( $order_id ){
		$query = "
				SELECT deposit_status, deposit
				FROM product__order 
				WHERE order_id = '$order_id'";
		$rs    = Db::get_row( $query );

		$deposit_status = $rs['deposit_status'];
		$deposit = $rs['deposit'];

		// Si ja està marcat com a deposit, marco com a pagat
		return  ( $deposit_status == 'completed' || !(double)$deposit ) ;
	}

	public function set_reserva_payed() {

		$order_id = R::number( 'order_id' );
		$ret = [];
		if (!$order_id){
			$ret ['message'] = $this->caption['c_set_payed_not_saved'];
		}
		else {
			$status_field = self::is_deposit_paid( $order_id ) ? 'order_status' : 'deposit_status';
			// Si ja està marcat com a deposit, marco com a pagat


			$query = "
			UPDATE product__order SET 
				$status_field = 'completed'
			WHERE order_id = '$order_id';";
			Db::execute( $query );

			$ret ['message'] = $status_field == 'order_status' ? $this->caption['c_set_payed_saved']: $this->caption['c_set_payed_saved_deposit'] ;
		}

		json_end( $ret );

	}

	static public function count_products( $family_id, $subfamily_id ) {

		if ( $family_id ) {
			$product_count = Db::get_first( "SELECT count(*) 
						FROM product__product
						WHERE
						product__product.family_id = $family_id
						AND product__product.subfamily_id = 0
						AND (status = 'onsale' OR status = 'nostock' OR status = 'nocatalog') 
						AND product__product.bin = 0" );
		}
		else {
			$product_count = Db::get_first( "SELECT count(*) 
						FROM product__product
						WHERE
						product__product.subfamily_id = $subfamily_id
						AND (status = 'onsale' OR status = 'nostock' OR status = 'nocatalog') 
						AND product__product.bin = 0" );
		}

		return $product_count;
	}

	static public function add_customer() {

		$customer = R::escape( 'customer_summary', '', '_POST' );
		$customer = explode( ' ', $customer );

		array_map( function ( $value ) {
			return trim( $value );
		}, $customer );

		$name     = Db::qstr( $customer[0] ? $customer[0] : '' );
		$surnames = Db::qstr( implode( ' ', array_slice( $customer, 1 ) ) );

		$query = "
			INSERT 
				INTO `product__customer` (
					`name`, 
					`surname`, 
					`activated`, 
					`is_shop_app`
				) 
				VALUES (
					$name,
					$surnames,
					0,
					1
				 );
			";
		Db::execute( $query );
		$customer_id                        = Db::insert_id();
		$_SESSION['product']['customer_id'] = $customer_id;

		self::newsletter_add_remove_key( $customer_id );

		$query = "INSERT INTO product__address SET
			customer_id = " . Db::qstr( $customer_id ) . ",
			a_name = $name ,
			a_surname = $surnames,
			invoice = 1";
		Db::execute( $query );

		$_SESSION['product']['cart_vars']['customer_comment'] = R::escape( 'customer_comment_summary', '', '_POST' );

	}

	static function save_order_vars( &$order, $is_form_edit = false ) {

		$order_id = $order['order_id'];
		$centre_id = isset($_SESSION['centre_id'])?$_SESSION['centre_id']:0;

		$discount        = R::escape( 'discount_summary', false, '_POST' );
		$deposit         = R::escape( 'deposit_summary', false, '_POST' );
		$summary_deposit = R::escape( 'summary_deposit', false, '_POST' );

		// Si hem desmarcat el checkbox i existeix el camp deposit_summary es posa a 0
		if ( ! $summary_deposit && $deposit !== false ) $deposit = 0;

		$query       = "SELECT (all_base+all_tax) FROM product__order WHERE order_id = $order_id";
		$total_price = Db::get_first( $query );

		$query = '';
		if ( $discount ) {
			$discount    = $total_price * $discount / 100;
			$all_basetax = $total_price - $discount;
			$query       .= "
				UPDATE product__order 
				SET promcode_discount = $discount,
					all_basetax = $all_basetax
				WHERE order_id = $order_id;";
		}


		if ( $is_form_edit ) {

			$query_status = "
				SELECT order_status, deposit_status 
				FROM product__order 
				WHERE order_id = $order_id";

			$rs = Db::get_row( $query_status );

			$order_status   = $rs['order_status'];
			$deposit_status = $rs['deposit_status'];

			$update_query = $deposit === false ? '' : "deposit = $deposit,";
			$update_query .= in_array( $order_status, [
				'completed',
				'preparing',
				'shipped',
				'delivered'
			] ) && $deposit_status != 'completed' ? "deposit_status = 'completed'," : '';

			if ($update_query) {
				$update_query = substr( $update_query, 0, - 1 );

				$query .= "
				UPDATE product__order SET 
					$update_query
				WHERE order_id = $order_id;";
			}
		}
		else {

			$payment_method  = R::escape( 'payment_method', false, '_POST' );
			$operator_id  = R::escape( 'operator_id', '0', '_POST' );

			$order_status = $payment_method == 'lacaixa' ? 'pending' : 'paying';
			$status_field = 'order_status';

			// Si hi ha deposit, marco el mètode de pagament per deposit i definitiu iguals
			$payment_method_sql = "payment_method = '$payment_method'";

			if ( $deposit ) {
				$status_field = 'deposit_status';
				$payment_method_sql .= ", deposit_payment_method = '$payment_method'";
			}

			// Si s'ha pagat amb tpv virtual, marco per que es vegi com a comanda feta des de la web per temes de comptabilitat
			$real_source_id = $centre_id;
			if ($payment_method == 'lacaixa'){
				$centre_id = 0;
			}


			$query .= "
			UPDATE product__order SET 
				deposit = $deposit, 
				$status_field = '$order_status', 
				operator_id = '$operator_id', 
				$payment_method_sql,
				source_id = $centre_id, 
				real_source_id = $real_source_id 
			WHERE order_id = $order_id;";

		}

		// Recalculo el $order per que faci be els pagament amb tarjeta
		if ( !$is_form_edit ) {
			if ( $discount ) {
				$order['totals']['promcode_discount'] = format_currency($discount);
				$order['totals']['all_basetax'] = format_currency($all_basetax);
			}
			if ( $deposit ) {
				$order['totals']['pay_deposit'] = format_currency($deposit);
			}
		}


		if ($query) Db::multi_query( $query );

	}

	// Posa data als productes que no en tenen, com ara els complements
	static function check_no_date_reserva_items( &$order, $action, $is_shop ){

		if ($action != 'add_record') return;

		$order_id = $order['order_id'];

		$query = "
			SELECT reservaitem_id 
			FROM lassdive__orderitem 
			WHERE orderitem_id
			IN ( SELECT orderitem_id FROM product__orderitem WHERE order_id = $order_id )
			AND UNIX_TIMESTAMP (reservaitem_date) = 0";
		$results    = Db::get_rows( $query );

		if ( $results ) {
			$query = "SELECT product_date FROM product__orderitem WHERE order_id = $order_id AND UNIX_TIMESTAMP (product_date) > 0 ORDER BY product_date ASC LIMIT 1";
			$date  = Db::get_first( $query );

			if ($date) $date = Db::qstr($date);
			else return;

			/*elseif ($is_shop) return; // Si es botiga només actualitzo si hi ha algún producte amb data, sino deixo la data buida per que es vegi que està pendent d'assignar
			else $date = "NOW()";*/

			// Actualitzo els reserva_date que estan a zero
			$query = "
			UPDATE lassdive__orderitem
			SET reservaitem_date = $date
			WHERE orderitem_id
			IN ( SELECT orderitem_id FROM product__orderitem WHERE order_id = $order_id )
			AND UNIX_TIMESTAMP (reservaitem_date) = 0";
			Db::execute( $query );


			// Actualitzo els product_date que estan a zero
			$query = "
			UPDATE product__orderitem
			SET product_date = $date
			WHERE order_id = $order_id 
			AND UNIX_TIMESTAMP (product_date) = 0";
			Db::execute( $query );

		}
		
	}

	static public function get_operator_select(){

		$operator_id_cg =
			  [
			    'type' => 'select',
			    'select_caption' => ' ',
			    'select_size' => '',
			    'select_table' => 'lassdive__operator',
			    'select_fields' => 'operator_id,operator',
			    'select_condition' => '',
			  ];

		$operator_id_cg ['class'] = 'alone';
		$operator_id_cg ['select_caption'] = ' ';
		$operator_id_select         = get_select( 'operator_id', '', false, $operator_id_cg, $c );
		return $operator_id_select;

	}

	// igual que a product_customer
	static function newsletter_add_remove_key( $id ) {

		$table    = 'product__customer';
		$table_id = 'customer_id';

		$q2 = "SELECT count(*) FROM " . $table . " WHERE BINARY remove_key = ";
		$q3 = "UPDATE " . $table . " SET remove_key='%s' WHERE  " . $table_id . " = '%s'";

		$updated = false;
		while ( ! $updated ) {
			$pass     = get_password( 50 );
			$q_exists = $q2 . "'" . $pass . "'";

			// si troba regenero pass
			if ( Db::get_first( $q_exists ) ) {
				$pass = get_password( 50 );
			}
			// poso el valor al registre
			else {
				$q_update = sprintf( $q3, $pass, $id );

				Db::execute( $q_update );
				break;
			}
		}
	}
}