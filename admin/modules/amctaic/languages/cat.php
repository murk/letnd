<?php
// menus eina
define('AMCTAIC_MENU_INICI', 'Pàgina inici');
define('AMCTAIC_MENU_INICI_EDIT', 'Editar pàgina inici');

define('AMCTAIC_MENU_AGENDA', 'Actualitat pàgina inici');
define('AMCTAIC_MENU_AGENDA_NEW', 'Entrar nova notícia');
define('AMCTAIC_MENU_AGENDA_LIST', 'Llistar notícies');
define('AMCTAIC_MENU_AGENDA_BIN', 'Paperera de reciclatge');

define('AMCTAIC_MENU_FORUM', 'Fòrum, viatges, sortida, notícies');
define('AMCTAIC_MENU_FORUM_NEW', 'Entrar nou');
define('AMCTAIC_MENU_FORUM_LIST', 'Llistar');
define('AMCTAIC_MENU_FORUM_BIN', 'Paperera de reciclatge');

define('AMCTAIC_MENU_PREMI', 'Premis');
define('AMCTAIC_MENU_PREMI_NEW', 'Entrar nou premi');
define('AMCTAIC_MENU_PREMI_LIST', 'Llistar premis');
define('AMCTAIC_MENU_PREMI_BIN', 'Paperera de reciclatge');

define('AMCTAIC_MENU_EDITOR', 'Edició pàgines');
define('AMCTAIC_MENU_EDITOR_LIST', 'Llistar pàgines');
define('AMCTAIC_MENU_EDITOR_BIN', 'Paperera de reciclatge');

define('AMCTAIC_MENU_TRANSPARENCIA', 'Transparència');
define('AMCTAIC_MENU_TRANSPARENCIA_EDIT', 'Editar pàgina - pujar arxius');

// captions seccio
$gl_caption_agenda['c_agenda'] = 'Notícia';
$gl_caption_agenda['c_title'] = 'Títol';
$gl_caption_agenda['c_subtitle'] = 'Subtítol';
$gl_caption_agenda['c_data_agenda'] = 'Data';
$gl_caption_agenda['c_entered'] = 'Data';

$gl_caption['c_status'] = 'Estat';
$gl_caption['c_url1']='Enllaç';
$gl_caption['c_url1_name']='Nom enllaç';
$gl_caption['c_prepare'] = 'En preparació';
$gl_caption['c_review'] = 'Per revisar';
$gl_caption['c_public'] = 'Públic';
$gl_caption['c_archived'] = 'Arxivat';

$gl_caption_forum['c_tipus']='Tipus';
$gl_caption_forum['c_filter_tipus']='Tipus:';
$gl_caption_forum['c_filter_status']='Estat:';
$gl_caption_forum['c_data_forum']='Data';
$gl_caption['c_autor']='Autor';
$gl_caption_forum['c_entered']='data d\'entrada';
$gl_caption_forum['c_ordre']='Ordre';
$gl_caption_forum['c_forum']='Títol';
$gl_caption_forum['c_subtitle']='Subtítol';
$gl_caption_forum['c_carrec']='Càrrec';
$gl_caption_forum['c_content']='Text';
$gl_caption_forum['c_tipus_forum']='Fòrum';
$gl_caption_forum['c_tipus_viatge']='Viatge';
$gl_caption_forum['c_tipus_sortida']='Sortida';
$gl_caption_forum['c_tipus_noticia']='Notícia';
$gl_caption_forum['c_file']='Documents';
$gl_caption_forum['c_videoframe']='Video (Ample 468px)';



$gl_caption_bonaplata['c_bonaplata']='Títol';
$gl_caption_bonaplata['c_tipus']='Tipus';
$gl_caption_bonaplata['c_tipus_adult']='Adults';
$gl_caption_bonaplata['c_tipus_jove']='Joves';
$gl_caption_bonaplata['c_content']='Veredicte';
$gl_caption_bonaplata['c_page_id']='Categoría';
$gl_caption_bonaplata['c_entered'] = 'Data';


// Transparencia
$gl_caption['c_balance'] = "Balanç";
$gl_caption['c_compte'] = "Compte d'Explotació";
$gl_caption['c_pressupost'] = "Pressupost";
$gl_caption['c_activitat'] = "Memòria d'Activitats";
$gl_caption['c_actuacio'] = "Pla d'Actuació";
$gl_caption['c_content'] = "Explicació";