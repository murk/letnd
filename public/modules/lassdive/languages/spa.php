<?
$gl_caption['c_order_status'] = 'Pagado';
$gl_caption['c_order_status_paying'] = 'Pendiente';
$gl_caption['c_order_status_failed'] = 'Fallido';
$gl_caption['c_order_status_deposit'] = 'Paga y señal';
$gl_caption['c_order_status_pending'] = 'Pendiente';
$gl_caption['c_order_status_denied'] = 'Rechazado';
$gl_caption['c_order_status_voided'] = 'Anulado';
$gl_caption['c_order_status_refunded'] = 'Devuelto';
$gl_caption['c_order_status_completed'] = 'Pagado';
$gl_caption['c_order_status_preparing'] = 'Preparando';
$gl_caption['c_order_status_shipped'] = 'Enviado';
$gl_caption['c_order_status_delivered'] = 'Entregado';

$gl_caption['c_nav_product_search'] = "Nueva reserva";
$gl_caption['c_nav_product_search_text'] = "Nueva";
$gl_caption['c_nav_reserva'] = "Listar reservas";
$gl_caption['c_nav_reserva_text'] = "Reservas";
$gl_caption['c_nav_timetable'] = "Calendario";
$gl_caption['c_nav_timetable_text'] = "Calendario";
$gl_caption['c_nav_back'] = "Atrás";
$gl_caption['c_nav_languages'] = "Idioma";
$gl_caption['c_nav_logout'] = "Desconectar";

$gl_caption['c_explicacio_buscador'] = "Buscar por palabras o Referéncias <i>(ej: \"456 245 45\")</i>";
$gl_caption['c_explicacio_buscador_producte'] = "Buscar actividades";
$gl_caption['c_top_products'] = "Más vendidas";
$gl_caption['c_last_products'] = "Últimas actividades";
$gl_caption['c_families'] = "Familias";
$gl_caption['c_ref_list'] = "ref.";
$gl_caption['c_activitat'] = "Actividad";
$gl_caption['c_reservar_ara'] = "Finalizar reserva";

$gl_caption['c_titol_paperera'] = "Papelera de reciclaje";
$gl_caption['c_titol_paperera_2'] = "Reservas eliminadas";

$gl_caption['c_set_payed_not_saved'] = "No se ha podido marcar como pagado, por favor vuelve a probarlo";
$gl_caption['c_set_payed_saved'] = "La reserva se ha marcado como pagada";
$gl_caption['c_set_payed_saved_deposit'] = "El depósito se ha marcado como pagado";

$gl_messages ['no_records'] = 'No se ha encontrado ningún pedido';