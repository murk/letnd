<?php
global $gl_db_classes_table, $gl_config;
$gl_db_classes_table = $gl_config['custumer_table'];

function save_rows()
{
	if ($GLOBALS['gl_config']['custumer_table']=='product__customer') {
		$module = Module::load('product','customer');
		$module->do_action();
	}
	elseif ($GLOBALS['gl_config']['custumer_table']=='api__agent') {
		$module = Module::load('api','agent');
		$module->do_action();
	}
	else{
		$save_rows = new SaveRows;
		$save_rows->field_unique = "mail";
		$save_rows->save();
	}
}

function list_records() {
	global $newsletter_addresses_list, $gl_action, $gl_page, $gl_message, $gl_messages;

	$customer_table = $GLOBALS['gl_config']['custumer_table'];
	$customer_id    = explode( '__', $customer_table );
	$customer_id    = $customer_id[1] . '_id';
	$search_content = '';

	// trec l'extensió "action_add" per que xuti amb v3, i l'acció original la poso a process
	$process = $gl_action;
	if ( strpos( $gl_action, 'list_records_bin' ) !== false ) {
		$gl_action = 'list_records_bin';
	} elseif ( strpos( $gl_action, 'list_records' ) !== false ) {
		$gl_action = 'list_records';
	}

	if ( $customer_table == 'product__customer' ) {
		$module         = Module::load( 'product', 'customer' );
		$module->parent = 'NewsletterCustumer';
		$action         = str_replace( "list_", 'get_', $gl_action );
		$listing        = $module->do_action( $action );
		//$listing->set_field('group_id','list_admin','text');
		if ( $process != "list_records_select_list" && $process != "list_records_search_select_addresses" ) {
			$listing->add_button( 'edit_button', 'action=show_form_edit&tool=newsletter&tool_section=custumer' );
		}
		//$listing->set_field('group_id','select_caption',$GLOBALS['gl_caption']['c_group_caption']);
		$listing->order_by = 'name ASC, surname ASC';
	} elseif ( $customer_table == 'api__agent' ) {
		$module         = Module::load( 'api', 'agent' );
		$module->parent = 'NewsletterCustumer';
		$action         = str_replace( "list_", 'get_', $gl_action );
		$listing        = $module->do_action( $action );
		//$listing->set_field('group_id','list_admin','text');
		if ( $process != "list_records_select_list" && $process != "list_records_search_select_addresses" ) {
			$listing->add_button( 'edit_button', 'action=show_form_edit&tool=newsletter&tool_section=custumer' );
			$listing->set_options( 1, 0, 0, 0, 0, 0, 0, 0 );
		}
		//$listing->set_field('group_id','select_caption',$GLOBALS['gl_caption']['c_group_caption']);
		$listing->order_by = 'nom ASC, cognoms ASC';
	}
	// Aquests comparteixen el newsletter_custumer_config
	else {
		$listing = new ListRecords;
		if ( $customer_table == 'newsletter__custumer' ) {
			$listing->unset_field( 'user_id' );
			$listing->unset_field( 'category_id' );
		}elseif ( $customer_table == 'custumer__custumer' ) {

		}
		$listing->order_by = 'name ASC, surname1 ASC, surname2 ASC';
	}

	if ( $GLOBALS['gl_process'] == 'removed' ) {
		$listing->condition = "removed = '1'";
	} // a la paperera si que llisto removed i no removed
	elseif ( strpos( $gl_action, 'list_records_bin' ) === false ) {
		$listing->condition = "removed = '0'";

	}

	// per tots trec els clients no actius
	if ($customer_table=='custumer__custumer') {
		$listing->condition .= $listing->condition ? ' AND ' : '';
		$listing->condition .= "activated = '1'";
	}
	// Debug::p( $listing->condition );

	switch ($process)
    {
        // si estem a la finestra per selecionar adresses desdel missatge
        case "list_records_mail_not_sent":
				/*$listing->condition .= " 
					AND custumer_id IN 
						( SELECT " . $customer_id . " 
							FROM newsletter__message_to_custumer
							WHERE message_id= " . R::id('message_id') . '
						)';*/
			
				$listing->condition .= " 
					AND message_id= " . R::id('message_id') . '
						';
			
				$listing->set_options(0, 0, 0, 0, 0, 0, 0, 0);
				$listing->unset_field('phone1');
				$listing->unset_field('phone2');
				$listing->unset_field('enteredc');
				$listing->unset_field('prefered_language');
				$listing->unset_field('user_id');
				$listing->unset_field('category_id');
				$listing->set_field('sent','list_admin','text');
				$listing->group_fields = array('sent');
				$listing->order_by = "sent ASC, " . $listing->order_by;
				
				$listing->add_button ('delete_button', '', 'boto1', 'after','delete_not_sent_address');
				
				$listing->group_fields_function = 'format_groupfield';
				
				$listing->join = "INNER JOIN newsletter__message_to_custumer 
				ON ".$customer_table.".".$customer_id."=newsletter__message_to_custumer.custumer_id";
				
				
				$listing->call('list_records_walk_not_sent', 'sent',true);				
				
				
				$GLOBALS['gl_content'] = $listing->list_records() . '<script language="JavaScript" src="/admin/modules/newsletter/jscripts/send_mail.js"></script>';
				return;
            break;
        case "list_records_select_frames":
            select_addresses();
            return;
            break;
        case "list_records_select_list":
			if ($customer_table=='custumer__custumer'){
				$listing->add_filter('user_id');
				$listing->add_filter('category_id','custumer__demand');
				$listing->add_filter('prefered_language');
			}
			
        case "list_records_search_select_addresses":

            $gl_page->menu_tool = false;
            $gl_page->menu_all_tools = false;
            $gl_page->template = 'clean.tpl';
            $gl_page->title = '';
            $listing->set_options(1, 0, 0, 0, 0, 0, 0, 0);
        	if ($customer_table=='product__customer')
            	$listing->call('list_records_walk2', 'customer_id,name,surname,mail');
        	elseif ($customer_table=='api__agent')
            	$listing->call('list_records_walk2', 'agent_id,nom,cognoms,mail');
			else
				$listing->call('list_records_walk', 'custumer_id,name,surname1,surname2,mail');
			
			$listing->condition .= ' AND mail <> \'\'';
            $gl_message = $gl_messages['select_address'];

            if ($process == "list_records_select_list")
            {
                // per llistar desde grups
                if (isset($_GET["group_id"]) && $_GET["group_id"]!='null')
                {
					$listing->join = "INNER JOIN newsletter__custumer_to_group ON ".$customer_table.".".$customer_id."=newsletter__custumer_to_group.custumer_id";
                    $listing->condition .= " AND group_id = " . $_GET["group_id"];
                }
            } elseif ($process == "list_records_search_select_addresses")
            {
                $q = $_POST['search'];
				
				get_search_condition($q, $listing, $customer_table);
				
            }
            break;
        default:
            // per llistar desde grups
            if (isset($_GET["group_id"]) && $_GET["group_id"] &&  $_GET["group_id"]!='null')
            {
				//$listing->unset_field('group_id');
				$listing->join = "INNER JOIN newsletter__custumer_to_group ON ".$customer_table.".".$customer_id."=newsletter__custumer_to_group.custumer_id";
                $listing->condition .= " AND group_id = " . $_GET["group_id"];
            }
			
			$q = R::get('q');
			$listing->add_filter('group_id');
			get_search_condition($q, $listing, $customer_table);
            $search_content = get_search_form();
			//$listing->always_parse_template=true;
			$GLOBALS['gl_content'] = $search_content . $listing->list_records();

	        return;

    } // switch
	//$listing->group_fields = array('group_id');
	//$listing->add_move_to('group_id');
	$listing->list_records(false);
	if ($listing->results)
		$GLOBALS['gl_content'] = $search_content . $listing->parse_template();
	else{
		$GLOBALS['gl_message'] = $search_content . $listing->no_records_message;
	}
}

function get_search_condition($q,&$listing, $customer_table){
	
	global $gl_messages;
	
	if (!$q) return;
	
	if ($customer_table=='product__customer'){
		$listing->condition .= " AND (
			name like '%" . $q . "%'
			OR surname like '%" . $q . "%'
			OR concat(name,' ',surname) like '%" . $q . "%'
			OR mail like '%" . $q . "%'
			OR dni like '%" . $q . "%'
			OR telephone like '%" . $q . "%'
			OR telephone_mobile like '%" . $q . "%'
			OR comment like '%" . $q . "%'
			" ;


		// busco a address
		$query = "SELECT customer_id FROM product__address WHERE (
							 a_name like '%" . $q . "%' OR
							 a_surname like '%" . $q . "%' OR
							 address like '%" . $q . "%' OR
							 zip like '%" . $q . "%' OR
							 city like '%" . $q . "%' OR
							 concat(a_name,' ',a_surname) like '%" . $q . "%'
								)" ;
		$results = Db::get_rows_array($query);
		//$results = array_merge($results, Db::get_rows_array($query));

		$listing->condition .= get_ids_query($results, 'customer_id');
		$listing->condition .= ") " ;
	}
	elseif ($customer_table=='api__agent'){
		$listing->condition .= " AND (bin = 0) AND
			(
				(nom LIKE '%" . $q . "%')
				or
				(cognoms LIKE '%" . $q . "%')
				or
				(mail LIKE '%" . $q . "%')
			)";
	}
	else{
		$listing->condition .= " AND (bin = 0) AND
			(
				(name LIKE '%" . $q . "%')
				or
				(surname1 LIKE '%" . $q . "%')
				or
				(surname2 LIKE '%" . $q . "%')
				or
				(mail LIKE '%" . $q . "%')
			)";
	}
	$listing->no_records_message = $gl_messages['select_search_no_records'] . "'" . $q . "'";
}

function get_search_form(){
	
	
		$tpl = new phemplate(PATH_TEMPLATES);
		$GLOBALS['gl_caption']['c_by_ref']='';
		$tpl->set_vars($GLOBALS['gl_caption']);
		$tpl->set_file('newsletter/custumer_search.tpl');
		$tpl->set_var('menu_id',$GLOBALS['gl_menu_id']);
		$tpl->set_var('q',  htmlspecialchars(R::get('q')));
		return $tpl->process();
}

function get_ids_query(&$results, $field)
{
	if (!$results) return '';
	$new_arr = array();
	foreach ($results as $rs){
		$new_arr [$rs[0]] = $rs[0];
	}
	return "OR " . $field . " IN (" . implode(',', $new_arr) . ") ";
}
function list_records_walk($custumer_id, $name, $surname1, $surname2, $mail)
{
	$name=  $name . ' ' . $surname1 . ' ' . $surname2 . ' (' . $mail . ')';
	$name= addslashes($name); // cambio ' per \'
	$name= htmlspecialchars($name); // cambio només cometes dobles per  &quot;
    $ret['tr_jscript'] = 'onClick="parent.insert_address(' . $custumer_id . ',\'' . $name . '\')"';
    return $ret;
}
function list_records_walk2($custumer_id, $name, $surname, $mail)
{
	$name=  $name . ' ' . $surname . ' (' . $mail . ')';
	$name= addslashes($name); // cambio ' per \'
	$name= htmlspecialchars($name); // cambio només cometes dobles per  &quot;
    $ret['tr_jscript'] = 'onClick="parent.insert_address(' . $custumer_id . ',\'' . $name . '\')"';
    return $ret;
}
function list_records_walk_not_sent(&$listing,$sent)
{
	$listing->buttons['delete_button']['show'] = $sent=='0';
	$listing->buttons['delete_button']['params'] = "','".R::id('message_id');
	return array();
}

function show_form()
{
	global $tpl,$gl_content;
	include ('check_settings.php');
    check_groups();


	if ($GLOBALS['gl_config']['custumer_table']=='product__customer') {
		$module = Module::load('product','customer');
		$module->parent = 'NewsletterCustumer';
		$module->do_action();
		return;
	}
	elseif ($GLOBALS['gl_config']['custumer_table']=='api__agent') {
		$module = Module::load('api','agent');
		$module->parent = 'NewsletterCustumer';
		$module->do_action();
		return;
	}
	else{
    	$show = new ShowForm;
		
		// nomes per inmo
		if ($GLOBALS['gl_config']['custumer_table']=='newsletter__custumer'){
			$show->unset_field('user_id');			
			$show->unset_field('category_id');
		}
		$template = 'newsletter/custumer_form';
	    $show->template = $template;
	}
    $show->set_vars_common();
    $show->show_form();
}

function write_record()
{
    if ($GLOBALS['gl_process']=="delete_not_sent"){
		
		
		$GLOBALS['gl_message'] = $message = $GLOBALS['gl_messages']['deleted_not_sent'];
		
		$custumer_id = R::id('custumer_id');
		$message_id = R::id('message_id');
		
		Db::execute ('DELETE FROM newsletter__message_to_custumer
			WHERE  custumer_id = ' . $custumer_id .'
			AND message_id = ' . $message_id);
		
		
		if (!Db::get_first("SELECT count(*) FROM newsletter__message_to_custumer
			WHERE  sent = 0
			AND message_id = " . $message_id)){
			
			Db::execute ("UPDATE newsletter__message SET status='sent' WHERE  message_id = " . $message_id);
		}
		
		$js = '
			if (parent.document.getElementById("message")){
				parent.document.getElementById("message_container").style.display="block";
				parent.document.getElementById("message").innerHTML = "'.addslashes($message).'";
			}
			parent.document.getElementById("list_row_'.$custumer_id.'").style.display="none";
		';
		
		print_javascript($js);
		
		// no es mostra missatge i no es tanca finestra
		Debug::p_all();
		die();
		
	}
	
	global $gl_action;

	if ($GLOBALS['gl_config']['custumer_table']=='product__customer') {
		$module = Module::load('product','customer');
		$module->do_action();
	}
	elseif ($GLOBALS['gl_config']['custumer_table']=='api__agent') {
		$module = Module::load('api','agent');
		$module->do_action();
	}
	else{
		$writerec = new SaveRows;
		$writerec->field_unique = "mail";
		$writerec->save();
		
		if ($gl_action=='add_record' && $GLOBALS['gl_insert_id'])
			newsletter_add_remove_key($GLOBALS['gl_config']['custumer_table'], $GLOBALS['gl_insert_id']);
	}

}


function newsletter_add_remove_key($table, $id) {	
	
		$table_id = strpos($table, "customer")!==false?'customer_id':'custumer_id';
	
		$q2 = "SELECT count(*) FROM ". $table . " WHERE BINARY remove_key = ";
		$q3 = "UPDATE ". $table . " SET remove_key='%s' WHERE  ". $table_id . " = '%s'";
		
		$updated = false; 
		while (!$updated){
			$pass = get_password(50);
			$q_exists = $q2 . "'".$pass."'";	
			
			// si troba regenero pass
			if(Db::get_first($q_exists)){
				$pass = get_password(50);							
			}
			// poso el valor al registre
			else{				
				$q_update = sprintf($q3, $pass, $id);
				
				Db::execute($q_update);
				break;
			}
		}
}

/**
 *
 * @access public
 * @return void
 */
function select_addresses()
{
    global $tpl, $gl_action, $gl_page, $gl_content, $gl_caption;

    $gl_page->menu_tool = false;
    $gl_page->menu_all_tools = false;
    $gl_page->template = '';

    $tpl->set_file('newsletter/address_select_frames.tpl');
    $tpl->set_vars($gl_caption);

    $form_object = new FormObject;
    $form_object->select_table = "newsletter__group, newsletter__group_language";
    $form_object->select_fields = "newsletter__group.group_id, group_name";
    $form_object->condition = "newsletter__group.group_id=newsletter__group_language.group_id AND language='".$GLOBALS['gl_language']."'";
    $form_object->input_name = $form_object->input_id = "group_id";
    $form_object->name = "group_id";
    $form_object->javascript = "onchange = \"go_url_select(this, this.form, '/admin/?action=list_records_select_list&menu_id=11&group_id=','listAddresses')\"";
    $form_object->select_caption = $gl_caption['c_select_all_groups'];
    $form_object->is_input = true;
    $group_select = $form_object->get_select();

    $tpl->set_var("group_id", $group_select);

    $gl_content = $tpl->process();
}

function format_groupfield(&$rs, $group_field, $val){
	
		$caption = &$GLOBALS['gl_caption'];
		if ($rs['sent_value']==1)
			return $caption['c_group_sent'];
		if ($rs['sent_value']==0)
			return $caption['c_group_not_sent'];
}

?>