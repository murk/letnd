
<script language="JavaScript" src="/admin/jscripts/validacio.js?v=<?=$version?>"></script>
<form action="<?=$link_action_delete?>" method="post" name="form_delete" id="form_delete" target="save_frame">
		<input name="<?=$id_field?>" type="hidden" value="<?=$id?>" />
	</form>
<form action="<?=$link_action?>" method="post" name="theForm" id="theForm" onsubmit="<?=$js_string?>" target="save_frame">
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="form">  
		<tr>
			<td><? if ($form_new):?>
					<div id="form_buttons_holder"><table id ="form_buttons">
						<tr>
							<td align="right" class="buttons_top"><div class="right"><input type="submit" name="Submit" value="<?=$c_send_new?>" class="btn_form1"></div></td>
						</tr>
					</table></div>
					<? endif //form_new?>
				<? if ($form_edit):?>
					<div id="form_buttons_holder"><table id ="form_buttons">
						<tr>
							<td valign="bottom"><div class="menu"><div><?=$c_edit_data?></div></div></td>
							<td align="right" class="buttons_top"><div class="right"><input type="submit" name="Submit" value="<?=$c_send_edit?>" class="btn_form1">
								&nbsp;&nbsp;<a class="btn_form_delete" href="javascript:submit_form('<?=$c_confirm_form_bin?>', true);">
								<?=$c_delete?>
								</a></div></td>
						</tr>
					</table></div>
					<? endif //form_edit?>
				<table width="100%" cellpadding="4" cellspacing="0">
					<tr>
						<td class="formCaptionTitle"><strong> </strong>
							<?if($is_custumer):?>
							<table border="0" cellpadding="0" cellspacing="0">
								<tr>
									<td><strong>
										<?=$c_name?>:</strong></td>
									<td>&nbsp;
										<?=$name?>
										&nbsp;</td>
									<td><strong>&nbsp;
										<?=$c_surname1?>:&nbsp;</strong></td>
									<td>&nbsp;
										<?=$surname1?>
										&nbsp;
										<?=$surname2?></td>
								</tr>
							</table>
							<?else: //?>
							<table border="0" cellpadding="0" cellspacing="0">
								<tr>
									<td><strong>
										<?=$c_property?>:</strong></td>
									<td>&nbsp;
										<?=$ref?> <?=$property_private?>
										&nbsp;</td>
								</tr>
							</table>
							<?endif //is_custumer?>
						</td>
					</tr>
				</table><table width="100%" cellpadding="0" cellspacing="0" class="formstbl">
								<tr>
									<td valign="top" class="formsCaption"><strong>
										<strong>
										<?=$c_dates;?>
										</strong>:</strong></td>
									<td class="forms">
										<table border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td><?= $c_date ?></td>
												<td><?= $c_finished_date; ?></td>
											</tr>
											<tr>
												<td><?= $date ?></td>
												<td><?= $finished_date ?></td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td valign="top" class="formsCaption"><strong>
										<?=$c_status?>:</strong></td>
									<td class="forms"><span class="formsCaption">
										<?=$status?>
									</span>
										<input name="<?=$id_field?>[<?=$id?>]" type="hidden" value="<?=$id?>" />
										<input name="<?=$id_field?>_javascript" type="hidden" value="<?=$id?>" />
										</td>
								</tr>
								<tr>
									<td width="150" valign="top" class="formsCaption"><strong>
										<?=$c_tipus?>: </strong></td>
									<td class="forms"><?=$tipus?></td>
								</tr>
								<?if($message_id):?>
								<tr>
									<td valign="top" class="formsCaption"><strong>
										<?=$c_property_id?>: </strong></td>
									<td class="forms"><?=$property_id?></td>
								</tr>
								<tr>
									<td valign="top" class="formsCaption"><strong>
										<?=$c_custumer_id?>: </strong></td>
									<td class="forms"><?=$custumer_id?></td>
								</tr>
								<?elseif($is_custumer):?>
								<tr>
									<td valign="top" class="formsCaption"><strong>
										<?=$c_property_id?>: </strong></td>
									<td class="forms"><?=$property_id?>
										<input name="custumer_id[0]" type="hidden" id="custumer_id[0]" value="<?=$custumer?>" />
								<input name="old_custumer_id[0]" type="hidden" id="custumer_id[0]" value="<?=$custumer?>" /></td>
								</tr>
								<?else: // is_custumer?>
								<tr>
									<td valign="top" class="formsCaption"><strong>
										<?=$c_custumer_id?>: </strong></td>
									<td class="forms"><?=$custumer_id?><?=$property_id?></td>
								</tr>
								
								<?endif //is_custumer?>
								<tr>
									<td valign="top" class="formsCaption"><strong>
										<?=$c_history_title?>: </strong></td>
									<td class="forms"><?=$history_title?></td>
								</tr>
								<tr>
									<td valign="top" class="formsCaption"><strong>
										<?=$c_history?>: </strong></td>
									<td class="forms"><?=$history?></td>
								</tr>
						</table>
				<?if ($form_new):?>
					<table width="100%" cellpadding="4" cellspacing="2" class="formsButtonstbl">
						<tr>
							<td align="center" class="formsButtons"><div class="centerFloat1">
												<div class="centerFloat2"><input type="submit" name="Submit" value="<?=$c_send_new?>" class="btn_form1"></div></div>
							</td>
						</tr>
					</table>
					<?endif //form_new?>
				<?if ($form_edit):?>
					<table width="100%" cellpadding="4" cellspacing="2" class="formsButtonstbl">
						<tr>
							<td align="center" class="formsButtons"><div class="centerFloat1">
												<div class="centerFloat2"><input type="submit" name="Submit" value="<?=$c_send_edit?>" class="btn_form1">
								&nbsp;&nbsp;<a class="btn_form_delete" href="javascript:submit_form('<?=$c_confirm_form_bin?>', true);">
								<?=$c_delete?>
								</a></div></div></td>
						</tr>
					</table>
					<?endif //form_edit?></td>
		</tr>
</table>
</form>
