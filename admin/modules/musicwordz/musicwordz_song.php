<?
/**
 * MusicwordzSong
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2016
 * @version $Id$
 * @access public
 */
class MusicwordzSong extends Module{

	function __construct(){
		parent::__construct();
	}

	function on_load() {

		Main::load_class( 'musicwordz','common' );
		MusicwordzCommon::add_dynamic_missatges( $this->caption );

	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function get_records()
	{
		$this->set_field( 'song', 'class', '' );

		$listing = new ListRecords($this);
		MusicwordzCommon::set_listing( $listing );
		
	    return $listing->list_records();
	}
	function records_walk_artist(&$listing){
		MusicwordzCommon::records_walk_artist($listing);
	}
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{
	    $show = new ShowForm($this);
		MusicwordzCommon::set_show( $show );

		$show->form_level1_titles = array( 'entered' );

	    return $show->show_form();
	}

	function save_rows()
	{
	    $save_rows = new SaveRows($this);

		if ($this->action == 'save_rows_save_records') {
			foreach ( $save_rows->id as $id ) {
				$status = $save_rows->get_value( 'status', '', $id );
				$user_id = Db::get_first("SELECT user_id FROM musicwordz__song WHERE song_id = " . $id);
				$text = $save_rows->get_value( 'song', '', $id );

				$this->check_send_message_approved( $id, $status, $user_id, $text );

			}
		}

	    $save_rows->save();
	}

	function write_record()
	{
	    $writerec = new SaveRows($this);		
		

		// Envio missatge al canviar status a approved
		$id = $writerec->id;
		$status = $writerec->get_value( 'status' );
		$user_id = $writerec->get_value( 'user_id' );
		$text = $writerec->get_value( 'song' );

		$this->check_send_message_approved( $id, $status, $user_id, $text );

		
		MusicwordzCommon::set_writerec( $writerec );

	    $writerec->save();
	}

	function check_send_message_approved($id, $status, $user_id, $text) {

		if ( $this->action == 'add_record' && $status == 'approved' ) {
			$this->send_message_approved ($user_id, $text);
		}
		elseif (
			$this->action == 'save_record' && $status == 'approved'
			|| $this->action == 'save_rows_save_records' && $status == 'approved'
		) {
			$query = "SELECT status FROM musicwordz__song WHERE song_id = " . $id;
			$old_status = Db::get_first( $query );
			if ($status != $old_status)
				$this->send_message_approved ($user_id, $text);
		}

	}
	function send_message_approved ($user_id, $song){

		if ( MusicwordzCommon::is_artist() ) {
			return;
		}
		$subject = sprintf( $this->caption['c_song_subject'], $song );

		Main::add_missatge(
			array(
							'subject' => $subject,
							'link' => '/admin/?menu_id=2303',
							'link_text' => $this->caption['c_see_songs'],
							'creator_user_id' => $_SESSION['user_id'],
							'to_users' => array ($user_id),
			)
		);
	}

	function manage_images()
	{
	    $image_manager = new ImageManager($this);
	    $image_manager->execute();
	}
}
?>