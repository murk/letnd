<?php
/*
 *
 * Important posar qualsevol canvi en el formulari també a la funció get_content_sections()
 *
 *
 *
 */
global $gl_tool;

// si el client te plantilla especifica. El client o te theme o te plantilla especifica
if (is_dir(PATH_TEMPLATES_PUBLIC .  $gl_tool . '/message_templates'))
define('PATH_TEMPLATES_NEWSLETTER_MAIL', PATH_TEMPLATES_PUBLIC . $gl_tool . '/message_templates');
else
define('PATH_TEMPLATES_NEWSLETTER_MAIL', PATH_THEMES_PUBLIC . $gl_tool . '/message_templates');

include ('check_settings.php');
function save_rows()
{
    $save_rows = new SaveRows;
    $save_rows->save();
}

function list_records()
{
    global $gl_process, $gl_content;
    // Debug::add("Process", $gl_process);
    $listing = new ListRecords;
    $listing->order_by = 'entered desc';
    $listing->set_options(1, 1, 1, 1, 1, 1, 1, 0, 1); // set_options ($options_bar = 1, $options_checkboxes = 1, $save_button = 1, $select_button = 1, $delete_button = 1, $print_button = 1, $edit_buttons = 1, $image_buttons = 1, $split_count = 1)
    switch ($gl_process)
    {
        case 'send_mail_history':
            $listing->add_button ('re_send_button', 'action=show_form_copy', 'boto1', 'before');
            $listing->set_options(1, 1, 1, 1, 1, 1, 0, 0, 1);
            $listing->condition = "status = 'sent' AND bin = 0";
            $listing->order_by = 'sent desc';
            break;
        case 'send_mail_saved':
            $listing->condition = "status = 'saved'";
            break;
        case 'send_mail_not_sent':
            $listing->add_button ('not_send_button', 'action=write_record', 'boto1', 'before', '', 'target="save_frame"');
            $listing->add_button ('re_send_button', 'menu_id=9&action=show_form_copy', 'boto1', 'before');
            $listing->condition = "(status = 'error0' OR status = 'error1' OR status = 'error2')";
            $listing->set_options(1, 1, 1, 1, 1, 1, 0, 0, 1);
            $listing->order_by = 'sendstart desc';
	        $listing->unset_field( 'sent' );
	        $listing->set_field( 'sendstart', 'list_admin', 'text' );
            $listing->call("count_sent_messages", "message_id");
            break;
    } // switch
    $listing->list_records();
    $gl_content .= '<script language="JavaScript" src="/admin/modules/newsletter/jscripts/send_mail.js"></script>';
}

function show_form()
{
    global $gl_action, $tpl, $gl_process, $gl_config;

    check_config();
    check_groups();
    if ($gl_action == "show_preview")
    {
        preview_mail();
        return;
    }

    $tpl->set_vars(get_content_sections(true));
    //$content_extra = explode (',',$gl_config['content_extra']);
    //foreach($content_extra as $section=>$val){
    //	$tpl->set_var($section, $val);
    //}


    $show = new ShowForm;

    if ($gl_action == "show_form_new")
    {
        // si es un nou missate agafo els valors de la configuració
        get_values_config($show);

        // Si tinc subject o body per get els agafo
        $subject = R::get( 'subject' );
        if ($subject)
            $show->set_field( 'subject', 'default_value',$subject );

        $message = R::get( 'message' );
        if ($message)
            $show->set_field( 'message', 'default_value', $message );

        $prefered_language = R::get( 'prefered_language' );
        if ($prefered_language)
            $show->set_field( 'language_id', 'default_value', $prefered_language );


    }

    $message_return_url = R::get( 'message_return_url', '' );
    $show->set_var( 'message_return_url', $message_return_url );

    // per relacionar amb una propietat sense haver d'adjuntar-la al mail
    $related_property_ids = R::ids( 'related_property_ids', '' );
    $show->set_var( 'related_property_ids', $related_property_ids );

    // assignar valors de addresses seleccionades, no s'inclou ja que es relació molts a molts
    set_addresses_vars();
    set_content_vars($show);
    $show->show_form();
}

function write_record()
{
    global $gl_process, $gl_action, $gl_page, $gl_reload, $gl_insert_id, $gl_message, $gl_errors;
    // enviar resta missatges amb error enviament, no es guarda res, simplement enviem la resta de missatges
    if ($gl_process == 'send_mail_not_sent')
    {
        get_record_message();
        $id = $_GET['message_id'];
    }
    else
    {
        $writerec = new SaveRows;
        $id = current($_POST['message_id']);
        if ($_POST["format"][$id] == "text")
        {
            $_POST["templatehtml_id"][$id] = "0";
        } elseif ($_POST["format"][$id] == "html")
        {
            $_POST["templatetext_id"][$id] = "0";
        }
        // abans d'enviar els mails, l'estatus nomès pot ser o guardat o en error, si s'envia canviarem error per sent
        if (isset($_POST["send"]))
        {
            $writerec->fields['sendstart']['override_save_value'] = 'now()';
            $writerec->fields['status']['override_save_value'] = "'error0'";
        }
        else
        {
            $writerec->fields['status']['override_save_value'] = "'saved'";
        }
        switch ($gl_action)
        {
            case "add_record":
            case "copy_record":
		        $gl_reload = true; // REVISAR hauria de fer un reset de les parts del formulari en jscript (seleccionar adreces, seleccionar inmobles, ara no tinc temps)
                $writerec->fields['entered']['override_save_value'] = 'now()';
                break;
            case "save_record":
                break;
        }
        $writerec->save();
        get_old_post(); // obtinc el post com a la versio 1.0 per facilitar les coses
        update_messages_addresses();
        update_messages_content();
    }
    // enviem el mail als destinataris
    if (isset($_POST["send"]) && !$gl_errors['post_file_max_size'])
    {
        $gl_reload = false; // evito que la clase upload fagi el reload
        start_send_mail();
        newsletter_send_mail();
        write_return_link();
        // die();
    }
    elseif ($gl_errors['post_file_max_size'])
    {
        // si estavem enviant, haig de posar status=saved i sendstart=null
		if (isset($_POST["send"])) {
			$query = "UPDATE newsletter__message SET sendstart = null, status = 'saved' WHERE message_id = " . ($gl_insert_id?$gl_insert_id:$_POST['message_id']);
			Db::execute($query);
        }
		if (($gl_action=='add_record' || $gl_action=='copy_record')) {
			$_SESSION['message'] = $gl_message;
			print_javascript('top.document.location = \'/admin/?action=show_form_edit_saved&action_previous_list=list_records_saved&menu_id=7&message_id=' . $gl_insert_id . '\'');
			die();
        }
	}
}
function get_record_message()
{
    // poso les variables en un POST per fer com si envies des d'un formulari
    $show = new ShowForm;
    $show->get_values();
	$message_id = $show->id;

    $p = $show->rs;
    $p['sendstart'] = 'now()';
    // obtinc els grups
	$query      = "SELECT group_id FROM newsletter__message_to_group WHERE message_id = " . $message_id;
    $results    = Db::get_rows($query);
    foreach ($results as $rs)
    {
        $p['group_id'][$rs['group_id']] = $rs['group_id'];
    }
    // obtinc adresses
    $query = "SELECT custumer_id FROM newsletter__message_to_custumer WHERE manually_selected=1 AND message_id=" . $message_id;
    $results = Db::get_rows($query);
    $p['selected_addresses_ids'] = implode_field($results, 'custumer_id', ',');

    // Extres
	$extras = get_content_sections();

	$query = "SELECT tool, section, content_id, action
            FROM newsletter__message_to_content
            WHERE message_id = $message_id
			ORDER BY tool, section";

	// Debug::add("Continguts", $query);
	$results = Db::get_rows($query);

	$p['tools'] = $p['sections'] = [];

	foreach ( $extras as $extra => $value ) {
		if ( $value ) {
			$extra_arr       = explode( '_', $extra );
			$tool            = $extra_arr[0];
			$section         = $extra_arr[1];
			$p['tools'][]    = $tool;
			$p['sections'][] = $section;

			$p[ 'selected_' . $extra . '_ids' ]          = '';
			$p[ 'selected_' . $extra . '_names' ]        = '';
			$p[ 'selected_' . $extra . '_action' ]  = '';
		}
	}
	if ($results)
        {
            $old_section = $results[0]['tool'] . '_' . $results[0]['section'];
            $tool = $results[0]['tool'];
            $action = $results[0]['action'];
            // afegeixo un ultim loop de fake
            $results[count($results)]['section'] = $results[count($results)]['tool'] = $results[count($results)]['content_id'] = $results[count($results)]['action'] = '';
            $content_ids = array();
            $ids = array();
            $names = array();
            foreach ($results as $rs)
            {
                $new_section = $rs['tool'] . '_' . $rs['section'];
                if ($new_section != $old_section)
                {
                    include_once(DOCUMENT_ROOT . 'public/modules/' . $tool . '/newsletter_content_extra.php');
                    // crido funcio de la secció ej. get_vars_inmo a public/modules/inmo
                    call_user_func_array('get_vars_' . $old_section, array($content_ids, &$ids, &$names));
                    // poso els resultats al tpl
                    $p['selected_' . $old_section . '_ids'] = implode (',', $ids);
                    $p['selected_' . $old_section . '_names'] = implode ('#;#', $names);
                    $p['selected_' . $old_section . '_action'] = $action;
                    // resetejo variables per la pròxima secció
                    $content_ids = array();
                    $ids = array();
                    $names = array();
                    $old_section = $new_section;
                }
                $tool = $rs['tool'];
                $action = $rs['action'];
                $content_ids[] = $rs['content_id'];
            }
        }


    // que envii
    $p['send'] = true;

    $_POST = $p;
}

function get_old_post()
{
    global $gl_db_classes_fields;
    foreach($gl_db_classes_fields as $key => $value)
    {
        if (isset($_POST[$key])) $_POST[$key] = $_POST['old_' . $key] = current($_POST[$key]);
    }
}

function get_values_config(&$show)
{
    $query = "SELECT name,value FROM newsletter__configadmin";
    $results = Db::get_rows($query);

    foreach($results as $col)
    {
        if (isset($show->fields[$col['name']]))
        {
            $show->fields[$col['name']]['default_value'] = $col['value'];
        }
    }
}

function update_messages_addresses()
{
    // si el missatge es nou, insertar addreses i posar manually_selected a 1
    // si edito un missatge, vol dir que no s'ha enviat, borrar tots i posar a 1
    global $gl_db_classes_id_field, $gl_insert_id, $gl_process;
    $id_field = $gl_insert_id?$gl_insert_id:$_POST[$gl_db_classes_id_field];
    // Mirar de no insertar si pertany a algun grup dels seleccionats,
    // de moment no passa res ja que el missatge nomès s'enviarà un sol cop, i al actualitzar a enviat s'actualitzaran les dues copies, però millor poder controlar això
    // $groups_ids = implode(',',$_POST["group_id"])
    $query = "DELETE FROM newsletter__message_to_custumer
      WHERE message_id = " . $id_field;
    // Debug::add("Consulta Borra messages_mail", $query);
    Db::execute($query);
    if ($_POST["selected_addresses_ids"])
    {
        $selected_addresses = explode(',', $_POST["selected_addresses_ids"]);
        foreach ($selected_addresses as $custumer_id)
        {
            $query = "INSERT INTO newsletter__message_to_custumer (
          message_id, custumer_id, manually_selected
          ) values ('"
             . $id_field . "', '" . $custumer_id . "', '1')";
            // Debug::add("Consulta Inserta messages_mail", $query);
            Db::execute($query);
        }
    }
}

function set_addresses_vars()
{
    global $gl_db_classes_id_field, $tpl, $gl_config;
	
	$customer_table  = $gl_config['custumer_table'];
	$customer_id_field  = explode('__',$customer_table);
	$customer_id_field  = $customer_id_field[1].'_id';
	
	$custumer_ids = R::ids('custumer_ids');
    $ids = '';
    $names = '';
    $namesp = '';
    $ids_arr = array();
    $names_arr = array();
    $rs = array();

    if (isset($_GET[$gl_db_classes_id_field]))
    {
        if ($customer_table=='product__customer'){
		$query = "SELECT " . $customer_table . ".customer_id as custumer_id, name, surname, mail
		 			FROM newsletter__message_to_custumer, " . $customer_table . "
		 			WHERE newsletter__message_to_custumer.custumer_id = " . $customer_table . ".customer_id
					AND bin = '0'
					AND message_id = '" . $_GET[$gl_db_classes_id_field] . "'
					AND manually_selected = '1'";
		}
        elseif ($customer_table=='api__agent'){
		$query = "SELECT " . $customer_table . ".agent_id as custumer_id, nom as name, cognoms as surname, mail
		 			FROM newsletter__message_to_custumer, " . $customer_table . "
		 			WHERE newsletter__message_to_custumer.custumer_id = " . $customer_table . ".agent_id
					AND bin = '0'
					AND message_id = '" . $_GET[$gl_db_classes_id_field] . "'
					AND manually_selected = '1'";
		}
		else
		{
		$query = "SELECT " . $customer_table . ".custumer_id, name, surname1, surname2, mail
		 			FROM newsletter__message_to_custumer, " . $customer_table . "
		 			WHERE newsletter__message_to_custumer.custumer_id = " . $customer_table . ".custumer_id
					AND bin = '0'
					AND message_id = '" . $_GET[$gl_db_classes_id_field] . "'
					AND manually_selected = '1'";
		}
        // Debug::add("Adresses selecionades", $query);
       
		$rs = Db::get_rows($query);
    }
	elseif ($custumer_ids){
		
		$query = "SELECT " . $customer_table . ".custumer_id, name, surname1, surname2, mail
		 			FROM " . $customer_table . "
		 			WHERE " . $customer_id_field . " IN (" . $custumer_ids . ")
					AND mail<>''";
		$rs = Db::get_rows($query);
	}
	
	if ($rs){
		
        foreach ($rs as $key)
        {
            $ids_arr[] = $key['custumer_id'];
			if ($customer_table=='product__customer'){
				$str = $key['name'] . ' ' . $key['surname'] . ' (' . $key['mail'] . ')';
				$str = ($key['name'] || $key['surname'])?$str:$str;
			}
			elseif ($customer_table=='api__agent'){
				$str = $key['name'] . ' ' . $key['surname'] . ' (' . $key['mail'] . ')';
				$str = ($key['name'] || $key['surname'])?$str:$str;
			}
			else
			{
				$str = $key['name'] . ' ' . $key['surname1'] . ' ' . $key['surname2'] . '(' . $key['mail'] . ')';
				$str = ($key['name'] || $key['surname'] || $key['surname1'])?$str:$str;
			}
            $names_arr[] = $str;
        }
        $ids = implode(',', $ids_arr);
        $names = implode('#;#', $names_arr);
        $namesp = implode('<br>', $names_arr);
		
	}

    $tpl->set_var("selected_addresses_ids", $ids);
    $tpl->set_var("selected_addresses_names", $names);
    $tpl->set_var("selected_addresses_namesp", $namesp);
}
// contingut extra per el missatge, de fitxe, esdeveniments, etc...

function update_messages_content()
{
    global $gl_db_classes_id_field, $gl_insert_id;
    $id_field = $gl_insert_id?$gl_insert_id:$_POST[$gl_db_classes_id_field];
    $query = "DELETE FROM newsletter__message_to_content
      WHERE message_id = " . $id_field;
    Db::execute($query);
    // Debug::add('Consulta messges content', $query);

	foreach ($_POST['tools'] as $x=>$val)
    {
        $tool = $_POST['tools'][$x];
        $section = $_POST['sections'][$x];
        $content_ids = explode(',', $_POST['selected_' . $tool . '_' . $section . '_ids']);
        $action = $_POST['selected_' . $tool . '_' . $section . '_action'];

        $glue_end = "'),";
        $glue_start = "('" . $tool . "', '" . $section . "', '" . $action . "', '" . $id_field . "', '";
        $glue = $glue_end . $glue_start;
        $query = $glue_start . implode($glue, $content_ids) . "')";

        $query = "INSERT INTO newsletter__message_to_content (
          tool, section, action, message_id, content_id
          ) values "
         . $query;
        Db::execute($query);
        // Debug::add('Consulta messages content', $query);
    }
}

function set_content_vars(&$show)
{
    global $gl_db_classes_id_field, $tpl, $gl_action;
	
	$tpl->set_var('hidden_demand_results', '');
	
    // array per seccions que es vulguin vicular a newsletter, ej. enviar la carta de canxiquet
    $extras = get_content_sections();
	
	// per que agafi propietats del get
	$property_ids = R::ids('property_ids');
	
    // inicialitzar variables per form_new
    foreach($extras as $extra=>$value)
    {
        $tpl->set_var('selected_' . $extra . '_ids', '');
        $tpl->set_var('selected_' . $extra . '_names', '');
        $tpl->set_var('selected_' . $extra . '_namesp', '');
        $tpl->set_var('selected_' . $extra . '_show_record', 'checked');
        $tpl->set_var('selected_' . $extra . '_list_records', '');
    }
    // quan editem un formulari, agafem les dades de la taula newsletter__message_to_content
    if ($gl_action != "show_form_new" || $property_ids)
    {
        
		if ($property_ids){
			
			$properties = explode(',', $property_ids);
			$results = array();
			
			foreach ($properties as $property) {
				$results[]= array(
					'tool' => 'inmo',
					'section' => 'property',
					'content_id' => $property,
					'action' => 'show_record',
				);
			}
			
			// passo les variable get en una variable en el post
			$post = array();
			$post['property_ids'] = $_GET['property_ids'];
			if (isset($_GET['demand_ids'])) $post['demand_ids'] = $_GET['demand_ids'];
			if (isset($_GET['demand_id'])) $post['demand_id'] = $_GET['demand_id'];
			$post['custumer_ids'] = $_GET['custumer_ids'];
			
			$tpl->set_var('hidden_demand_results', '<input type="hidden" name="demand_results" value="' . htmlentities(serialize($post)) . '">');
			
			
		}
		else{
			
			$query = "SELECT tool, section, content_id, action
		 			FROM newsletter__message_to_content
		 			WHERE message_id = " . R::id($gl_db_classes_id_field) . "
					ORDER BY tool, section";
			
			// Debug::add("Continguts", $query);
			$results = Db::get_rows($query);
		}

        if ($results)
        {
            $old_section = $results[0]['tool'] . '_' . $results[0]['section'];
            $tool = $results[0]['tool'];
            $action = $results[0]['action'];
            // afegeixo un ultim loop de fake
            $results[count($results)]['section'] = $results[count($results)]['tool'] = $results[count($results)]['content_id'] = $results[count($results)]['action'] = '';
            $content_ids = array();
            $ids = array();
            $names = array();
            foreach ($results as $rs)
            {
                $new_section = $rs['tool'] . '_' . $rs['section'];
                if ($new_section != $old_section)
                {
                    include_once(DOCUMENT_ROOT . 'public/modules/' . $tool . '/newsletter_content_extra.php');
                    // crido funcio de la secció ej. get_vars_inmo a public/modules/inmo
                    call_user_func_array('get_vars_' . $old_section, array($content_ids, &$ids, &$names));
                    // poso els resultats al tpl
                    $tpl->set_var('selected_' . $old_section . '_ids', implode (',', $ids));
                    $tpl->set_var('selected_' . $old_section . '_names', implode ('#;#', $names));
                    $tpl->set_var('selected_' . $old_section . '_namesp', implode ('<br>', $names));
                    $tpl->set_var('selected_' . $old_section . '_show_record', $action == 'show_record'?'checked':'');
                    $tpl->set_var('selected_' . $old_section . '_list_records', $action == 'list_records'?'checked':'');
                    // resetejo variables per la pròxima secció
                    $content_ids = array();
                    $ids = array();
                    $names = array();
                    $old_section = $new_section;
                }
                $tool = $rs['tool'];
                $action = $rs['action'];
                $content_ids[] = $rs['content_id'];
            }
        }
    }
}
// Aquí posem el llistat de possibles eines que es pot linkar desde el newsletter
function get_content_sections($ret_hidden=false){
	$extras = array(
		0 =>
		array (
			'tool'=>'inmo',
			'section' => 'property'),
		1 =>
		array (
			'tool'=>'product',
			'section' => 'product'),
		2 =>
		array (
			'tool'=>'news',
			'section' => 'new'),
		3 =>
		array (
			'tool'=>'llemena',
			'section' => 'agenda'),
		4 =>
		array (
			'tool'=>'lassdive',
			'section' => 'activitat'),
		);
	$content_sections = array();
	if ($ret_hidden)
		$content_sections['hidden_content_extra'] = '';
	foreach ($extras as $key=>$extra)
	{
		extract($extra);
		$is_table = is_table($tool.'__'.$section);
		$content_sections[$tool.'_'.$section] = $is_table;

		if ($ret_hidden && $is_table){
			$content_sections['hidden_content_extra'].=
			'<input name="tools['.$key.']" type="hidden" value="'.$tool.'" />
			<input name="sections['.$key.']" type="hidden" value="'.$section.'" />';
		}
	}
	return $content_sections;
}

function count_sent_messages($message_id)
{
    global $gl_caption, $gl_config;
    $count_sent = 0;
    $count_not_sent = 0;
	if ($gl_config['custumer_table']=='product__customer'){
    $query = "SELECT sent,COUNT(DISTINCT " . $gl_config['custumer_table'] . ".customer_id) AS total
				FROM newsletter__message_to_custumer, " . $gl_config['custumer_table'] . "
				WHERE message_id='" . $message_id . "'
				AND newsletter__message_to_custumer.custumer_id = " . $gl_config['custumer_table'] . ".customer_id
				AND bin='0'
				GROUP BY sent";
	}
	elseif ($gl_config['custumer_table']=='api__agent'){
    $query = "SELECT sent,COUNT(DISTINCT " . $gl_config['custumer_table'] . ".agent_id) AS total
				FROM newsletter__message_to_custumer, " . $gl_config['custumer_table'] . "
				WHERE message_id='" . $message_id . "'
				AND newsletter__message_to_custumer.custumer_id = " . $gl_config['custumer_table'] . ".agent_id
				AND bin='0'
				GROUP BY sent";
	}
	else{
    $query = "SELECT sent,COUNT(DISTINCT " . $gl_config['custumer_table'] . ".custumer_id) AS total
				FROM newsletter__message_to_custumer, " . $gl_config['custumer_table'] . "
				WHERE message_id='" . $message_id . "'
				AND newsletter__message_to_custumer.custumer_id = " . $gl_config['custumer_table'] . ".custumer_id
				AND bin='0'
				GROUP BY sent";
	}
    // Debug::add("Consulta count messages", $query);
    $rs = Db::get_rows($query);
    foreach ($rs as $result)
    {
        $result['sent'] == 1?$count_sent = $result['total']:$count_not_sent = $result['total'];
    }
    $count_total = $count_sent + $count_not_sent;
    if ($count_total == 0)
    {
        $percent_sent = 0;
        $count_sent = 0;
        $count_not_sent = 1;
        $count_total = 1;
    }
    else
    {
        $percent_sent = ($count_sent * 100) / $count_total;
    }
    $ret['messages_sent'] = '<table onclick="show_not_sent(' . $message_id . ',event)" style="cursor:pointer;" width="100" height="12" border="0" cellpadding="0" cellspacing="1" bgcolor="#666666">
          <tr>
            <td style="padding:0;" bgcolor="#FF3300" title="' . $count_sent . ' ' . $gl_caption['c_messages_sent_message'] . ' ' . $count_total . '">
			<table width="' . $percent_sent . '%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                  <td style="padding:0;;background-color:#000099;height:1px;"></td>
          </tr>
                <tr>
                  <td style="padding:0;background-color:#000099;height:10px;" width="100%"></td>
          </tr>
                <tr>
                  <td style="padding:0;;background-color:#000099;height:1px;"></td>
          </tr>
        </table>
		</td>
          </tr>
        </table>';
    return $ret;
}

?>