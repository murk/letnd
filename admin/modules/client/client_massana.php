<?

/**
 * ClientMassana
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2011
 * @version $Id$
 * @access public
 */
class ClientMassana extends Module {

	public function __construct() {
		parent::__construct();
	}

	public function show_form() {
		$GLOBALS['gl_content'] = $this->get_form();
	}

	public function get_form() {

		$this->set_file( '/client/massana_form.tpl' );
		$this->set_vars( $this->caption );

		return $this->process();
	}

	public function write_record() {
		global $gl_page, $gl_message;


		if ( SaveRows::is_post_too_big() ) {
			$gl_page->show_message( $gl_message );
			Debug::p_all();
			die();
		}

		foreach ( $_FILES as $key => $upload_file ) {
			if ( $upload_file['error'] == 1 ) {
				$message = ' ' . $this->messages['post_file_max_size'] . (int) ini_get( 'upload_max_filesize' ) . ' Mb';

				$gl_page->show_message( $message );
				Debug::p_all();
				die();

			} else if ($upload_file['error'] == 0) {


				$file_ext = strrchr( substr( $upload_file['name'], - 5, 5 ), '.' );
				$file = CLIENT_PATH . "cartes/$key$file_ext";
				$moved = move_uploaded_file( $upload_file['tmp_name'], $file );

				if ( $moved ) {
					chmod( $file, 0776 );
				}
			}
		}
		 $gl_message = "Els arxius s'han guardat correctament";
	}
}