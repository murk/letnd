/**
 * Convert a single file-input element into a 'multiple' input list
 *
 * Usage:
 *
 *   1. Create a file input element (no name)
 *      eg. <input type="file" id="first_file_element">
 *
 *   2. Create a DIV for the output to be written to
 *      eg. <div id="files_list"></div>
 *
 *   3. Instantiate a MultiSelector object, passing in the DIV and an (optional) maximum number of files
 *      eg. var multi_selector = new MultiSelector( document.getElementById( 'files_list' ), 3 );
 *
 *   4. Add the first element
 *      eg. multi_selector.addElement( document.getElementById( 'first_file_element' ) );
 *
 *   5. That's it.
 *
 *   You might (will) want to play around with the addListRow() method to make the output prettier.
 *
 *   You might also want to change the line
 *       element.name = 'file_' + this.count;
 *   ...to a naming convention that makes more sense to you.
 *
 * Licence:
 *   Use this however/wherever you like, just don't blame me if it breaks anything.
 *
 * Credit:
 *   If you're nice, you'll leave this bit:
 *
 *   Class by Stickman -- http://www.the-stickman.com
 *      with thanks to:
 *      [for Safari fixes]
 *         Luis Torrefranca -- http://www.law.pitt.edu
 *         and
 *         Shawn Parker & John Pennypacker -- http://www.fuzzycoconut.com
 *      [for duplicate name bug]
 *         'neal'
 */
function MultiSelector( list_target, max, prefix, accept){
	
	
	// Where to write the list
	this.list_target = list_target;
	// Prefix of the input
	this.prefix = prefix;
	// si accepta tot menys no autoritzats
	this.not_accept = !accept;
	// si no estan definits el tipus autoritzats, limito als no autoritzats
	if (!accept) accept = 'exe|com|pif|bat|php|reg|scr|js|vbs|cgi|dll';
	// Expression for Files to accept
	this.acceptExp = new RegExp('\\.('+(accept)+')$','gi');
	// Files to accept
	this.accept = accept;
	// How many elements?
	this.count = 0;
	// How many elements?
	this.id = 0;
	// Is there a maximum?
	if( max ){
		this.max = max;
	} else {
		this.max = -1;
	};
	// creates a <table> element and a <tbody> element
    tbl     = document.createElement("table");
    this.tblBody = document.createElement("tbody");
	// put the <tbody> in the <table>
    tbl.appendChild(this.tblBody);
    tbl.className = 'multifile';
    // appends <table> into lis_target
	this.list_target.appendChild( tbl );
    // sets the border attribute of tbl to 2;
    //tbl.setAttribute("border", "1");
	/**
	 * Add a new file input element
	 */
	this.addElement = function( element ){

		// Make sure it's a file input element
		if( element.tagName == 'INPUT' && element.type == 'file' ){

			// Element name -- what number am I?
			element.name = this.prefix + this.id++;

			// Add reference to this object
			element.multi_selector = this;

			// What to do when a file is selected
			element.onchange = function(){


				// New file input
				var new_element = document.createElement( 'input' );
				new_element.type = 'file';
				new_element.className = 'file';

				// Add new element
				this.parentNode.insertBefore( new_element, this );

				// Apply 'update' to element
				this.multi_selector.addElement( new_element );
				
				if (!this.multi_selector.canAccept(this.value))
				{
					// Remove element from form
					this.parentNode.removeChild( this );		
					// Decrement counter
					this.multi_selector.count--;
					accept_alert_text = this.multi_selector.not_accept?MULTIFILE_NOT_ACCEPT + this.multi_selector.accept:MULTIFILE_ACCEPT + this.multi_selector.accept;
					alert(accept_alert_text);
					return false;
				}

				// Update list
				this.multi_selector.addListRow( this );

				// Hide this: we can't use display:none because Safari doesn't like it
				this.style.position = 'absolute';
				this.style.left = '-1000px';

			};
			// If we've reached maximum number, disable input element
			if( this.max != -1 && this.count >= this.max ){
				element.disabled = true;
			};

			// File element counter
			this.count++;
			// Most recent element
			this.current_element = element;

		} else {
			// This can only be applied to file input elements!
			alert( 'Error: not a file input element' );
		};

	};
	
	this.canAccept = function (v) {
		ret_value = (v.match(this.acceptExp)) || (v=='');
		if (this.not_accept) ret_value = !ret_value;
        return ret_value;
	}

	/**
	 * Add a new row to the list of files
	 */
	this.addListRow = function( element ){

		// creates a table row
        var new_row = document.createElement("tr");
		// Delete button
		var new_row_button = document.createElement( 'a' );
		new_row_button.setAttribute('href','#');
        var new_row_text = document.createTextNode(FILE_DELETE);
        new_row_button.appendChild(new_row_text);
		new_row_button.className = 'multifile';

		// References
		new_row.element = element;

		// Delete function
		new_row_button.onclick= function(){

			// Remove element from form
			this.parentNode.parentNode.element.parentNode.removeChild( this.parentNode.parentNode.element );

			// Remove this row from the list
			this.parentNode.parentNode.parentNode.removeChild( this.parentNode.parentNode );

			// Decrement counter
			this.parentNode.parentNode.element.multi_selector.count--;

			// Re-enable input element (if it's disabled)
			this.parentNode.parentNode.element.multi_selector.current_element.disabled = false;

			// Appease Safari
			//    without it Safari wants to reload the browser window
			//    which nixes your already queued uploads
			return false;
		};

		// Cela boto borrar
		var cell = document.createElement("td");
		
        var cellText = document.createTextNode('[');
        cell.appendChild(cellText);
        cell.appendChild(new_row_button);
        var cellText = document.createTextNode(']');
        cell.appendChild(cellText);
        new_row.appendChild(cell);
		this.tblBody.appendChild(new_row);
		// Cela nom arxiu
		var cell = document.createElement("td");
        var cellText = document.createTextNode(element.value);
        cell.appendChild(cellText);
        new_row.appendChild(cell);

	};

    function start() {



        // creating all cells
        for (var j = 0; j < 2; j++) {
            // creates a table row
            var row = document.createElement("tr");

            for (var i = 0; i < 2; i++) {
                // Create a <td> element and a text node, make the text
                // node the contents of the <td>, and put the <td> at
                // the end of the table row
                var cell = document.createElement("td");
                var cellText = document.createTextNode("cell is row "+j+", column "+i);
                cell.appendChild(cellText);
                row.appendChild(cell);
            }

            // add the row to the end of the table body
            tblBody.appendChild(row);
        }

        // put the <tbody> in the <table>
        tbl.appendChild(tblBody);
        // appends <table> into <body>
        body.appendChild(tbl);
        // sets the border attribute of tbl to 2;
        tbl.setAttribute("border", "2");
    }


};