<script language="JavaScript" src="/admin/jscripts/validacio.js?v=<?= $version ?>"></script>
<script language="JavaScript" src="/admin/jscripts/euro_calc.js?v=<?= $version ?>"></script>
<? if ( $has_images ): ?><? if ( $image_name ): ?>
	<script type="text/javascript">

		 var gl_gallery_images = <?= ImageManager::get_gallery_images( $images ) ?>;

		 gl_default_imatge = '<?=$images[0]['image_name_medium']?>';
	</script>
<? endif // image_name?><? endif // has_images?>

<form name="form_delete" method="post" action="<?= $link_action_delete ?>" target="save_frame">
	<input name="<?= $id_field ?>" type="hidden" value="<?= $id ?>">
</form>
<form name="theForm" method="post" action="<?= $link_action ?>" onsubmit="<?= $js_string ?>" encType="multipart/form-data" target="save_frame">
	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="form">
		<tr>
			<td>
				<? if ( $form_edit ): ?>
					<div id="form_buttons_holder">
						<table id="form_buttons">
							<tr>
								<td valign="bottom">
									<div class="menu">

										<a href="<?= $data_link ?>"><?= $c_edit_data ?></a>
										<a href="<?= $image_link ?>"><?= $c_edit_images ?></a>

										<? foreach($form_tabs as $r): extract( $r ) ?>
							                <? if ( $tab_action ): ?>
								                <a href="?action=<?= $tab_action ?>&<?= $id_field ?>=<?= $id ?>&menu_id=<?= $menu_id ?>"><?= $tab_caption ?></a>
						                    <? else: // ?>
												<div><?= $tab_caption ?></div>
							                <? endif // $tab_action ?>
						                <? endforeach //$form_tabs?>
									</div>
								</td>
								<td class="buttons_top">
									<div class="right">
										<? if ( $show_previous_link ): ?><a class="boto2 btn_show_previous_link"
										                                    href="<?= $show_previous_link ?>"></a><? endif //show_previous_link?>
										<? if ( $show_next_link ): ?><a class="boto2 btn_show_next_link"
										                                href="<?= $show_next_link ?>"></a><? endif //show_next_link?>
										<? if ( $back_button ): ?>
										<a class="btn_form_back" href="<?= $back_button ?>"></a><? endif //back_button?>
										<? if ( $save_button ): ?>
											<input type="submit" name="Submit" value="<?= $c_send_edit ?>" class="btn_form1"><? endif //save_button?><? if ( $delete_button ): ?>
										<a class="btn_form_delete" href="javascript:submit_form('<? if ( $has_bin ): ?><?= $c_confirm_form_bin ?><? else: ?><?= $c_confirm_form_deleted ?><? endif //has_bin?>', true);">
											<?= $c_delete ?>
											</a><? endif //delete_button?><? if ( $preview_button ): ?>
										<a class="btn_form1" href="<?= $preview_link ?>" target="_blank">
											<?= $c_preview ?>
											</a><? endif //preview_button?><? if ( $print_button ): ?>
											<a class="btn_form1 btn_print" href="<?= $print_link ?>">
												<?= $c_print ?>
											</a> <? endif //print_button?></div>
								</td>
							</tr>
						</table>
					</div>
				<? endif //form_edit?>


				<? ////////////   TITOL  ////////////////?>


				<table width="100%" cellpadding="0" cellspacing="0">
					<tr>
						<td class="formCaptionTitle"><strong>
								<?= $c_form_files_title ?>
								&nbsp;&nbsp;</strong>
							<? if ( $form_edit ): ?><span><?= $agencia ?></span><? endif //form_edit?></td>
					</tr>
				</table>

				<? ////////////   CAMPS  ////////////////?>

				<input name="agent_id[<?= $id ?>]" type="hidden" value="<?= $id ?>"/>
				<input name="agent_id_javascript" type="hidden" value="<?= $id ?>"/>

				<table width="100%" cellpadding="4" cellspacing="0" class="formstbl">
					<tr>
						<td width="150" class="formsCaption"><strong>
								<?= $c_file ?>:</strong></td>
						<td class="forms"><?= $file ?></td>
					</tr>
				</table>

				<? if ( $form_edit ): ?>
					<table width="100%" cellpadding="4" cellspacing="2" class="formsButtonstbl">
						<tr>
							<td align="center" class="formsButtons">
								<div class="centerFloat1">
									<div class="centerFloat2">
										<input type="submit" name="Submit" value="<?= $c_send_edit ?>" class="btn_form1">
									</div>
								</div>
							</td>
						</tr>
					</table>
				<? endif //form_edit?></td>
		</tr>
	</table>
</form>