<?
global $gl_caption;
$sql = "SELECT ref FROM inmo__property WHERE property_id = " . $_GET['property_id'];

$ref = Db::get_first($sql);

$sql = "SELECT image_id, name, main_image FROM inmo__property_image WHERE property_id='" . $_GET['property_id'] . "' ORDER BY ordre";
$images = Db::get_rows($sql);
	foreach ($images as $key=>$value){
		$images[$key] = ImageManager::get_file_name($value['image_id'], $value['name'],'inmo','property');
	}

echo('<?xml version="1.0" encoding="UTF-8"?>');?>

<images title="" frameColor="0xeae9e9" frameWidth="0" borderColor="0xeae9e9" borderWidth="0" stagePadding="50" imagePadding="120" thumbSelectedWidth="2" enableRightClickOpen="true">
<? foreach($images as $image): extract ($image)?><? if ($image_name_details):?>
<image>
   <path>/<?=CLIENT_DIR?>/inmo/property/images/<?=$image_name_medium?></path>
   <caption><?=$gl_caption['c_ref']?> <?=$ref?></caption>
   <? list($width, $height) = getimagesize(DOCUMENT_ROOT . CLIENT_DIR . "/inmo/property/images/" . $image_name_medium);?>
   <width><?=$width?></width>
   <height><?=$height?></height>
</image>
<? endif //$image_name_details?><? endforeach //$loop?>
</images>
<?die();
?>