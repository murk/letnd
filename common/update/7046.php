<?php
/* 7-5-18
Productes, configuració dels index de la BBDD, hi havia consultes de +20 segons

*/

if ( is_table( 'product__product' ) ) {

	$update_sql .= "
	ALTER TABLE `product__orderitem`
	DROP INDEX `product_id`,
	ADD INDEX `order_id` (`order_id`),
	ADD INDEX `product_id` (`product_id`);
";

}