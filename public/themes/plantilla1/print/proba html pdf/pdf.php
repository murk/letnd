<?php
$ref = 'Ref. U425';
$image = '2.JPG';
$logo = 'logo_showwindow.jpg';
$property = 'Pis a Besalú';
$fields = array(
	'Situació'=>'Besalú',
	'Superfície'=>'90 m2.',
	'Parcel·la'=>'Si',
	'Dormitoris'=>'3 dobles, 1 sencill',
	'Banys'=>'Tortellà',
	'Garatge'=>'Tortellà',
	'Serveis'=>'Tortellà',
	'Altres'=>'Tortellà',
	'Preu'=>'Tortellà10',
);

require('../common/includes/fpdf/fpdf.php');

$pdf = new FPDF('P', 'pt', 'A4');
$pdf->SetMargins(1, 1 , 1);
$pdf->SetDisplayMode('real');
$pdf->AddPage();
$pdf->AddFont('verdanab','','verdanab.php');
// ratlles grises
$pdf->SetDrawColor(204, 201, 199);
$pdf->Rect(15, 14, 565, 50, 'D');
$pdf->Rect(15, 75, 565, 751.5, 'D');
// quadres verds
$pdf->SetFillColor(67, 86, 8);
$pdf->Rect(15, 13.5, 565, 5, 'F');
$pdf->Rect(15, 821.5, 565, 5, 'F');
// quadre verd clar
$pdf->SetFillColor(221, 232, 172);
$pdf->Rect(30.5, 466, 534, 35.6, 'F');
// ratlla verda vertical
$pdf->SetFillColor(67, 86, 8);
$pdf->Rect(158.5, 518, 1, 286, 'F');

// referencia
$pdf->SetFont('Verdanab', '', 20);
$pdf->Text(26, 53.5, $ref);

// Titol mig
$pdf->SetFont('Arial', 'B', 22);
$pdf->SetFont('Arial', 'B', 25);
$pdf->Text(36.5, 492, $property);

$i = 0;
foreach($fields as $key=>$value)
{
	$pdf->SetFont('Arial', 'B', 19);
	$pdf->SetTextColor(67, 86, 8);
    $pdf->Text(30.5, 532+$i, $key);
	$pdf->SetFont('Arial', '', 19);
	$pdf->SetTextColor(0, 0, 0);
    $pdf->Text(185, 532+$i, $value);
    $i +=34;
}

$pdf->Image($image, 30.5, 89 , 534 , 356);
$pdf->Image($logo, 406.3, 24.46, 163);

$pdf->Output();

?>