<?
/**
 * ProductOrder
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2010
 * @version $Id$
 * @access public
 */

include_once(DOCUMENT_ROOT . 'admin/modules/product/product_common.php');

class ProductOrder extends Module{
	var $is_logged = false, $is_invoice = false;
	var $id = '', $is_buyer = false, $is_seller = false, $results=array(); // desde payment->send_mails
	var $payment_method; // desde payment->save_cart
	var $is_shop_app = false;
	function __construct(){
		parent::__construct();
	}
	function on_load(){
		// si es privada no deixo fer res excepte entrar login i password
		if ($this->is_index && $this->config['is_private_shop'] && !$this->is_logged()) {
			redirect('/?tool=product&tool_section=customer&action=login&language='.LANGUAGE);
			die();
		}
		if ($this->config['has_gift_card']){
			$this->set_field( 'is_gift_card', 'form_public', 'text' );
			$this->set_field( 'is_gift_card', 'list_public', 'text' );
		}
		else{
			$this->unset_field('is_gift_card');
		}
	}
	// faig servir desde customer
	function get_records()
	{
		if (!$this->is_logged) return false; // en aquest modul nomès puc entrar per un altre posant logged = true
		$listing = new ListRecords($this);
		$listing->add_button('show_record','action=show_order&tool=product&tool_section=customer');

		$listing->add_button('pay_now','action=show_method&process=method&tool=product&tool_section=payment');
		if (PRODUCT_HAS_INVOICE) {
			$listing->add_button('show_invoice', '', 'boto1', 'before', 'customer_show_invoice');
			$listing->add_button('show_invoice_refund', '', 'boto1', 'before', 'customer_show_invoice_refund');
		}
		$listing->set_options(0,0,0,0,0,0,0,0,0,1);
		$listing->order_by = 'entered_order DESC';
		$listing->call('records_walk', 'shipped,delivered,invoice_id,invoice_refund_id,invoice_refund_date', true);
		$listing->condition = 'customer_id =' . $_SESSION['product']['customer_id'];
	    return $listing->list_records();
	}
	function records_walk($listing, $shipped, $delivered, $invoice_id, $invoice_refund_id, $invoice_refund_date){
		if ($shipped=='0000-00-00 00:00:00'){
			$ret['shipped'] = $this->caption['c_no_shipped'];
			$listing->set_field('shipped', 'type','text'); // així no ho processa com a datatime
		}
		if ($delivered=='0000-00-00 00:00:00'){
			$ret['delivered'] = $this->caption['c_no_delivered'];
			$listing->set_field('delivered', 'type','text'); // així no ho processa com a datatime
		}

		$status = $listing->rs['order_status'];

		if ( $this->config['deposit_payment_on'] ) {
			$deposit = $listing->rs['deposit'];
			if ( (double) $deposit ) {
				$status = $listing->rs['deposit_status'];
			}
		}

		if (
			$status=='paying' ||
			$status=='failed' ||
			$status=='denied' ||
			$status=='voided' ||
			$status=='refunded'
		){
			$listing->buttons['pay_now']['show']= true;
		}
		else {
			$listing->buttons['pay_now']['show']= false;
		}
		
		if (PRODUCT_HAS_INVOICE) {
			if ($invoice_id=='-1' || $invoice_id=='0'){
				$ret['invoice_id'] = '';
				$ret['invoice_refund_id'] = '';
				$listing->buttons['show_invoice']['show']= false;
				$listing->buttons['show_invoice_refund']['show']= false;
			}
			else
			{
				// $ret['invoice_id'] = $this->config['invoice_prefix'] . $invoice_id;
				$ret['invoice_id'] = product_get_invoice_number($this->config['invoice_prefix'] ,$invoice_id, $listing->rs['invoice_date']);
				$ret['invoice_refund_id'] = product_get_invoice_number($this->config['invoice_prefix'] ,$invoice_refund_id, $listing->rs['invoice_refund_date']);

				// si es el form, mostro el id de factura caqnviat si es refund
				if ($this->action=='get_record' && $this->is_invoice == 'refund'){
					$ret['invoice_id'] = $ret['invoice_refund_id'];
					$ret['invoice_date'] = $invoice_refund_date;
				}

				$listing->buttons['show_invoice']['show']= true;
				if ($invoice_refund_id == 0) 
					$listing->buttons['show_invoice_refund']['show']= false;
				else
					$listing->buttons['show_invoice_refund']['show']= true;
			}
		}
		
		return $ret;
	}
	// faig servir desde customer, i desde payment->send_mails
	function get_form()
	{
		if (!$this->is_logged) return false;

		// dades de la comanda
		$show = new ShowForm($this);
		$is_mail = false;

		if ($this->parent=='ProductPayment') {
			// desde payment
			$is_mail = true;
			$show->id = $this->id;
		}
		if ($this->is_invoice) {
			$show->template="product/invoice_form";			
			Page::set_page_title($this->config['company_name'] . ' ' . $this->caption['c_invoice_id'] . ' ' . $this->config['invoice_prefix'] . R::id('invoice_id'));
		}
		$show->call('records_walk', 'shipped,delivered,invoice_id,invoice_refund_id,invoice_refund_date', true);
		$show->has_bin = false;
		$show->get_values();
		
		// faig boto de repago
		// estat de pending no ho faig per que es paypal, en el cas de transacció, que quan hagi fet el pago ho notifica
		$status = $show->rs['order_status'];
		if (
				$status=='paying' ||
				$status=='failed' ||
				$status=='denied' ||
				$status=='voided' ||
				$status=='refunded'
				){
			
			$this->set_var('repay_form',''); // '/?tool=product&tool_section=payment&language='.LANGUAGE.'&action=show_method&process=method' TODO-i REPAY posar un enllaça per tornar a pagar la comanda
		}
		else{
			$this->set_var('repay_form',false);
		}

		// nomès es mostra el compte quan es methode account
		$show->set_vars(array(
			'show_account' => $show->rs['payment_method']=='account' || $show->rs['deposit_payment_method']=='account',
			'concept' => $this->caption['c_order_id'] . ' ' . $show->id));

		// obtinc llistat productes
		$module = Module::load('product','orderitem', '', false); // així no necessito el product_orderitem.php
		$module->action = 'get_records';

		$listing = new ListRecords($module);	
		$listing->paginate = false;	
		$listing->has_bin = false;
		$listing->condition = 'order_id = ' . $show->rs['order_id'];
		$listing->template = 'product/cart_list.tpl';
		
		// calculo si l'iva està inclòs en el preu
		$is_tax_included = $this->is_wholesaler()?$this->config['is_wholesaler_tax_included']:$this->config['is_public_tax_included'];
		
		$price_sufix =  $is_tax_included?'_basetax':'_base';		
		
		$show->rs['rate_price'] = $show->rs['rate' . $price_sufix];
		$show->rs['total_price'] = $show->rs['total' . $price_sufix];
		$show->rs['all_price'] = $show->rs['all' . $price_sufix];
		$deposit_rest = (double ) $show->rs['all_price'] - (double ) $show->rs['deposit'];
		
		$c = &$this->caption;
		// s'utilitza a kupukupu, de moment nomes calen aquests
		$c_total_price = $is_tax_included?$c['c_total_basetax']:$c['c_total_base'];
		$c_all_price = $is_tax_included?$c['c_all_basetax']:$c['c_all_base'];

		$listing->set_vars( array(
			'is_block'           => false,
			'is_summary'         => true,
			'is_ajax'            => true,
			'is_invoice'         => $this->is_invoice,
			'is_payment'         => false,
			'is_order'           => true,
			'total_base'         => format_currency( $show->rs['total_base'] ),
			'total_tax'          => format_currency( $show->rs['total_tax'] ),
			'total_price'        => format_currency( $show->rs['total_price'] ),
			'total_basetax'      => format_currency( $show->rs['total_basetax'] ),
			'rate_base'          => format_currency( $show->rs['rate_base'] ),
			'rate_tax'           => format_currency( $show->rs['rate_tax'] ),
			'rate_price'         => format_currency( $show->rs['rate_price'] ),
			'rate_basetax'       => format_currency( $show->rs['rate_basetax'] ),
			'all_base'           => format_currency( $show->rs['all_base'] ),
			'all_tax'            => format_currency( $show->rs['all_tax'] ),
			'all_equivalencia'   => format_currency( $show->rs['all_equivalencia'], false ),
			'all_price'          => format_currency( $show->rs['all_price'] ),
			'all_basetax'        => format_currency( $show->rs['all_basetax'] ),
			'deposit_value'      => (double) $show->rs['deposit'],
			'deposit'            => format_currency( $show->rs['deposit'] ),
			'deposit_rest'       => format_currency( $deposit_rest ),
			'promcode_discount'  => format_currency( $show->rs['promcode_discount'] ),
			'show_rate'          => true,
			'c_total_price'      => $c_total_price,
			'c_all_price'        => $c_all_price,
			'is_tax_included'    => $is_tax_included,
			'show_promcode'      => $show->rs['promcode_discount'] != '0.00' ? true : false,
			'show_promcode_form' => false,
			'customer_comment'   => nl2br( htmlspecialchars( $show->rs['customer_comment'] ) ),
			'is_gift_card_value'   => $show->rs['is_gift_card'],
			'is_gift_card'   => $show->rs['is_gift_card'] ? $show->caption['c_yes'] : $show->caption['c_no'],
			'has_gift_card'   => $show->config['has_gift_card'] ? true : false,
			'old_price'          => ''
		) );

		if ( $this->is_invoice ) {
			$listing->set_var('customer_comment', false);
			$listing->set_var('is_gift_card_value', false);
		}
			
		// poso les variacions separades per comes
		// una altra forma enlloc de fer un list_records_walk
	    $listing->list_records(false);

		Main::load_class( 'product', 'common_order', 'admin');



		$is_refund = $this->is_invoice == 'refund';


		ProductCommonOrder::get_order_item_downloads($show->id, $status, $this, $listing);
		ProductCommonOrder::set_refund_negative_vals($is_refund, $show, $listing);
		ProductCommonOrder::get_order_item_variations( $listing, $price_sufix, $is_refund );

		
		$listing->parse_template(false);
		
		// canvio el show_record_link per que apunti als productes i no al orderitem
		foreach ($listing->loop as $key=>$val){
			$rs = &$listing->loop[$key];
			//debug::p($rs);		
			$rs['show_record_link'] = '/' . LANGUAGE . '/product/product/'. $rs['product_id'] . '/';
		}

		$listing->call_after_set_records('after_records_walk', 'loop', $this);
		
		$orderitems = $listing->process();
		$this->results = &$listing->results;

		if ($this->is_shop_app) return $this->results;


		$show->set_vars( array(
			'orderitems'    => $orderitems,
			'deposit_rest'  => format_currency( $deposit_rest ),
			'status_change' => false,
			'is_mail'       => $is_mail
		) );

		$show->set_vars($this->vars); // &$tpl // això s'ha de fer si separo els tpls de this, show i listing

		// obtinc adresses
		$module = Module::load('product', 'address');
		$module->invoice_id = $show->rs['address_invoice_id'];
		$module->is_invoice = $this->is_invoice;
		$module->delivery_id = $show->rs['delivery_id'];
		$module->is_seller = $this->is_seller;
		$show->set_vars($module->do_action('get_addresses'));

		// obtinc form
	    $ret = $show->show_form();
	    return $ret;
	}

	function after_records_walk( $rs ) {

		return ProductCommonOrder::cart_get_variation_image( $rs );

	}
	function save_order( $order_id ) {

		if ( ! $order_id ) {
			// lassdive - es pot implementar com a user_user_on_end - user_user_on_load
			if ( function_exists( 'lassdive_product_order_before_save' ) ) lassdive_product_order_before_save( $this->action );
		}

		// carrego cart per obtindre totals i resultats de productes del carro
		$module                 = Module::load( 'product', 'cart' );
		$module->parent         = 'ProductOrder';
		$module->order_id       = $order_id; // Per saber si agafo session o BBDD
		$module->payment_method = $this->payment_method;
		$order                  = $module->do_action( 'get_records' );

		$order['totals']['customer_id']    = $_SESSION['product']['customer_id'];
		$order['totals']['payment_method'] = $this->payment_method;

		// Lassdive
		if ($this->config['deposit_payment_on']){
			if ( $order['totals']['deposit'] ) {
				$order['totals']['pay_deposit'] = $order['totals']['deposit'];

				// Per famílies que no accpten deposit
				if ( $order['totals']['deposit_value'] == 0 ) {
					unset ($order['totals']['deposit_payment_method']); // valor per defecte de la BBDD
					$order['totals']['payment_method'] = $this->payment_method;
				}
				else {
					unset ($order['totals']['payment_method']); // valor per defecte de la BBDD
					$order['totals']['deposit_payment_method'] = $this->payment_method;
				}
			}
		}

		if ( !$order_id ) {
			$order['totals']['collect']          =  $_SESSION['product']['cart_vars']['collect'];
			$order['totals']['customer_comment'] = $_SESSION['product']['cart_vars']['customer_comment'];
			$order['totals']['is_gift_card'] = $_SESSION['product']['cart_vars']['is_gift_card'];
		}

		// obtinc ids de les adreses del client
		$order['totals'] += Db::get_row( 'SELECT delivery_id, address_id as address_invoice_id
									FROM product__customer
									JOIN product__address
									USING(customer_id)
									WHERE invoice = 1
									AND customer_id = ' . $_SESSION['product']['customer_id'] );

		Debug::add('Tots guardar comanda', $order['totals']);

		( $order_id )
			? $this->update_saved_cart( $order, $order_id )
			: $this->save_cart( $order );

		
		return $order;
		
	}

	// Actualitzo només el sistema de pagament, ja que al fer un repay es pot canviar de mètode
	function update_saved_cart( &$order, $order_id ){

		$order['order_id'] = $order_id;
		$query = "
			UPDATE product__order 
			SET payment_method = '$this->payment_method'
			WHERE order_id = $order_id";
		Db::execute( $query );
		
	}
	// desde payment show_method() , grabo quan començo a fer el pagament al tercer pas,
	// just abans de conectar al paypal
	// retorno array amb valors necessaris per paypal i altres sistemes
	function save_cart( &$order ){

		// desde public sempre es add_record, el client no pot modificar les comandes
		$this->action = $action = 'add_record';

		// grabo
		$writerec = new SaveRows($this);

		// posa be el post per guardar nou
		$writerec->convert_rs($order['totals']);
		$writerec->save();

		$order['order_id'] = $GLOBALS['gl_insert_id'];

		// lassdive - es pot implementar com a user_user_on_end - user_user_on_load
		if (function_exists('lassdive_product_order_on_save')) lassdive_product_order_on_save ($order, $action);

		$module = Module::load('product','orderitem','',false); // utilitzant-ho així no carrego l'arxiu principal del mòdul, ho faig tot desde aquí, utilitzant $module només com a contenidor, per poder passar al SaveRows

		$module->action = 'add_record';
		//Debug::p($results);
		foreach ($order['results'] as &$rs){
			$rs['order_id'] = $order['order_id'];

			$writerec = new SaveRows($module);
			$writerec->convert_rs($rs);
			$writerec->save();

			// lassdive
			if (function_exists('lassdive_product_orderitem_on_insert')) lassdive_product_orderitem_on_insert ( $writerec->id, $rs, $action);
		}
		
		// Guardo codi promocional si s'ha utilitzat
		if ($order['totals']['show_promcode']){
			Db::execute("INSERT INTO product__customer_to_promcode 
							(customer_id, promcode_id) VALUES
							('". $_SESSION['product']['customer_id']."', '" . $order['totals']['promcode_id'] . "');");
		}

		// Lassdive
		if (function_exists('lassdive_product_orderitems_on_insert_all')) lassdive_product_orderitems_on_insert_all ($order, $action);
		
	}
	function _get_downloads($product_id, $order_id){	
		$query = "
			SELECT download_id, name, size
			FROM product__product_download
			WHERE product_id = '". $product_id. "'
			ORDER BY ordre, download_id";
		
		$results = Db::get_rows($query);
		$ret = '';
		foreach ($results as $rs){
			$ret .= '<a href="/?tool=product&tool_section=customer&action=get_download&order_id=' . $order_id . '&download_id=' . $rs['download_id'] . '"> ' . $rs['name'] . '</a><br />';
		}
		$ret = substr($ret, 0, -6);
		if ($ret){
			$ret = '<div class="downloads">' . $ret . '</div>';
		}
		return $ret;
	}
	
	function is_wholesaler(){
		return (isset($_SESSION['product']['customer_id']) && isset($_SESSION['product']['mail']) && isset($_SESSION['product']['is_wholesaler']) && $_SESSION['product']['is_wholesaler']);
	}

	/**
	 * ProductCustomer::is_logged()
	 * Saber si està loguejat
	 * @return
	 */
	function is_logged(){
		// miro si hi ha les 2 sessions per més seguretat de que esta logged
		return (isset($_SESSION['product']['customer_id']) && isset($_SESSION['product']['mail']));
	}	
}
?>