<?
// blocks

define('INMO_PRICE_FIRST','Up to %s&nbsp;'.CURRENCY_NAME);
define('INMO_PRICE_MIDDLE','From %s to %s&nbsp;'.CURRENCY_NAME);
define('INMO_PRICE_LAST','More than %s&nbsp;'.CURRENCY_NAME);
define('INMO_FLOOR_SPACE_FIRST','Up to %s m<sup>2</sup>');
define('INMO_FLOOR_SPACE_MIDDLE','From %s to %s m<sup>2</sup>');
define('INMO_FLOOR_SPACE_LAST','More than %s m<sup>2</sup>');
define('INMO_PERSON_FIRST','Up to %s');
define('INMO_PERSON_MIDDLE','From %s to %s');
define('INMO_PERSON_LAST','More than %s');
define ('LETND','Letnd');

$gl_caption['c_offers'] = 'Offers';


// captions

$gl_caption['c_send_mail'] = 'send e-mail';
$gl_caption['c_more_info'] = 'More info';
$gl_caption['c_read_more'] = 'Read more';
$gl_caption['c_title_search'] = 'Search';
$gl_caption['c_yes'] = 'Yes';
$gl_caption['c_no'] = 'No';
$gl_caption['c_send'] = 'Send';
$gl_caption['c_clear'] = 'Clear';
$gl_caption['c_send_to_friend']='send to a friend';
$gl_caption['c_print']='print';
$gl_caption['c_zoom_link']='zoom images';
$gl_caption['c_contact_link']='contact';

// bloc inmo_categories
$gl_caption['c_sell'] = 'Sale';
$gl_caption['c_rent'] = 'Rental';
$gl_caption['c_temp'] = 'Tourist rent';
$gl_caption['c_moblat'] = 'Rental with furniture';
$gl_caption['c_selloption'] = 'Rental buying option';

// seo
$gl_caption['c_seo_verd_1']='<a href="http://www.letnd.com" target="_blank" title="Diseño páginas web en Girona">Diseño y programación web:<img src="/common/images/logoletnd.png" width="95" height="16" alt="Letnd: Diseño web, posicionamiento web, páginas web en Girona" style="vertical-align:text-bottom" /></a>';
$gl_caption['c_seo_verd_2']='<a href="http://www.letnd.com" target="_blank" title="Web site design in Girona">Web programming:<img src="/common/images/logoletnd.png" width="95" height="16" alt="Letnd: Web design, web position, web pages girona" style="vertical-align:text-bottom" /></a>';
$gl_caption['c_seo_verd_3']='<a href="http://www.letnd.com" target="_blank" title="Diseño páginas web en Girona">Creado por:<img src="/common/images/logoletnd.png" width="95" height="16" alt="Letnd: Diseño web, posicionamiento web, páginas web en Girona" style="vertical-align:text-bottom" /></a>';
$gl_caption['c_seo_verd_4']='<a href="http://www.letnd.com" target="_blank" title="Diseño páginas web en Girona">Diseño web:<img src="/common/images/logoletnd.png" width="95" height="16" alt="Letnd: Diseño web, posicionamiento web, páginas web en Girona" style="vertical-align:text-bottom" /></a>';

$gl_caption['c_seo_blanc_1']='<a href="http://www.letnd.com" target="_blank" title="Diseño páginas web en Girona">Diseño y programación web:<img src="/common/images/logoblanc.png" width="95" height="16" alt="Letnd: Diseño web, posicionamiento web, páginas web en Girona" style="vertical-align:text-bottom" /></a>';
$gl_caption['c_seo_blanc_2']='<a href="http://www.letnd.com" target="_blank" title="Diseño páginas web en Girona">Programación web:<img src="/common/images/logoblanc.png" width="95" height="16" alt="Letnd: Diseño web, posicionamiento web, páginas web en Girona" style="vertical-align:text-bottom" /></a>';
$gl_caption['c_seo_blanc_3']='<a href="http://www.letnd.com" target="_blank" title="Diseño páginas web en Girona">Creado por:<img src="/common/images/logoblanc.png" width="95" height="16" alt="Letnd: Diseño web, posicionamiento web, páginas web en Girona" style="vertical-align:text-bottom" /></a>';
$gl_caption['c_seo_blanc_4']='<a href="http://www.letnd.com" target="_blank" title="Diseño páginas web en Girona">Diseño web:<img src="/common/images/logoblanc.png" width="95" height="16" alt="Letnd: Diseño web, posicionamiento web, páginas web en Girona" style="vertical-align:text-bottom" /></a>';

$gl_caption['c_seo_custom_2']='<a href="http://www.letnd.com" target="_blank" title="Diseño páginas web en Girona">Programación web:<img src="/'.CLIENT_DIR.'/templates/images/letnd.png" alt="Letnd: Diseño web, posicionamiento web, páginas web en Girona" /></a>';
$gl_caption['c_seo_custom_4']='<a href="http://www.letnd.com" target="_blank" title="Diseño páginas web en Girona">Diseño web:<img src="/'.CLIENT_DIR.'/templates/images/letnd.png" alt="Letnd: Diseño web, posicionamiento web, páginas web en Girona" /></a>';

$gl_caption['c_tercersetena'] = 'Diseño <a class="tercersetena" target="_blank" href="http://www.tercersetena.com">Tercersetena.com</a>';

// NO TRADUIT
$gl_caption['c_tipus_temp'] = 'Season rental';
$gl_caption['c_tipus_moblat'] = 'Rental furniture';
// FI NO TRADUIT

// TAMBÉ A ADMIN
// Caption default
$gl_caption['c_form_title'] = 'Edit data';
$gl_caption['c_config_title'] = 'Edit configuration';
$gl_caption['c_edit'] = 'Edit details';


$gl_caption['c_edit_button'] = 'Edit data';
$gl_caption['c_view_button'] = 'View data';
$gl_caption['c_edit_images_button'] = 'Edit images';
$gl_caption['c_add_images_button'] = 'Add images';



$gl_caption['c_edit_data'] = 'Edit data';
$gl_caption['c_edit_images'] = 'Edit images';
$gl_caption['c_add_images'] = 'Add images';
$gl_caption['c_search_form'] = 'Search';
$gl_caption['c_send_new'] = 'Save';
$gl_caption['c_send_edit'] = 'Save changes';
$gl_caption['c_delete'] = 'Delete';
$gl_caption['c_action'] = 'Action';
$gl_caption['c_delete_selected'] = 'Delete selected';
$gl_caption['c_restore_selected'] = 'Restore selected';
$gl_caption['c_confirm_deleted'] = 'The selected elements will be completely deleted from the database \\n\\nDo you wish to continue?';
$gl_caption['c_confirm_images_deleted'] = 'The selected images will be completely deleted from the database \\n\\nDo you wish to continue?';
$gl_caption['c_confirm_form_bin'] = 'The record will be sent to the recycle bin \\n\\nDo you wish to continue?';
$gl_caption['c_confirm_form_deleted'] = 'The record will be completely deleted from the database \\n\\nDo you wish to continue?';
$gl_caption['c_move_to'] = 'Move to:';
$gl_caption['c_print_all'] = 'Print all';
$gl_caption['c_print'] = 'Print';
$gl_caption['c_order_by'] = 'Sort by:';
$gl_caption['c_select'] = 'Select';
$gl_caption['c_all'] = 'all';
$gl_caption['c_nothing'] = 'nothing';
$gl_caption['c_preview'] = 'Preview';
$gl_caption['c_yes'] = 'Yes';
$gl_caption['c_no'] = 'No';
$gl_caption['c_options_bar'] = 'Options';
$gl_caption['c_file_maximum'] = 'maximum %s files at a time';
$gl_caption['c_file_maximum_1'] = 'maximum 1 file at a time';































$gl_caption['c_form_sent'] = 'Thank you for contacting us, we’ll respond to your message as soon as possible';
$gl_caption['c_form_not_sent'] = 'The form hasn\'t been sent, please check the email address is correctly written';

$gl_caption['c_cookies_message'] = 'This website uses proprietary and third-party cookies to improve the browsing experience as well as for analysis. By continuing browsing, we understand that you agree the';
$gl_caption['c_cookies_message_more'] = 'use of cookies';
$gl_caption['c_cookies_message_accept'] = 'Accept';

$gl_caption['c_cookies_text'] = '

<p class="accept-cookies-title">What are cookies?</p>
<p>A cookie is a small file stored on the user\'s computer that allows us to recognize it. The set of cookies help us improve the quality of our website, allowing us to monitor which pages are useful to the website users or and which are not.</p>
<p>Cookies are essential for the operation of the Internet, providing innumerable advantages in the provision of interactive services, providing us with the navigation and usability of our website. Please note that cookies can not harm your computer and, if enabled, they help us identify and resolve errors.</p>
<p class="accept-cookies-title">What kind of cookies are used in this site?</p>
<p>Propietary cookies: those that are sent to the user\'s computer from a computer or domain managed by the publisher and from which provides the service requested by the user.</p>
<p>Third party cookies:those that are sent to the user\'s computer from a computer or domain not managed by the publisher but by another entity that analyzes the data collected. .</p>
<p>Persisting cookies: a type of cookie in which data are still stored in the terminal and can be accessed and processed by the cookie\'s responsible.</p>
<p>Analytics cookies: using web analytics to get information on the number of users accessing the site, number of pages viewed, and repetition frequency of visits, the duration, the browser used, operator who provides the service, the language used by the terminal or the city to which is assigned the IP address. Information that allows a better and more appropriate service by this website.</p>
<p class="accept-cookies-title">Accept the use of cookies.</p>
<p>We assume that you accept cookies. However, you can restrict, block or erase cookies from this site or any page from the browser settings. </p>
<p>Next you can find the main browsers help pages:</p>
<ul>
<li>Internet Explorer: <a href="http://windows.microsoft.com/es-xl/internet-explorer/delete-manage-cookies#ie="ie-10"" target="_blank">windows.microsoft.com/es-xl/internet-explorer/delete-manage-cookies#ie="ie-10"</a></li>
<li>FireFox: <a href="http://support.mozilla.org/es/kb/Borrar%20cookies" target="_blank">support.mozilla.org/es/kb/Borrar%20cookies</a></li>
<li>Chrome: <a href="http://support.google.com/chrome/answer/95647?hl="es"" target="_blank">support.google.com/chrome/answer/95647?hl="es"</a></li>
<li>Safari: <a href="http://www.apple.com/es/privacy/use-of-cookies/" target="_blank">www.apple.com/es/privacy/use-of-cookies/</a></li>
</ul>
';

// contrasenyes
$gl_caption['c_password_forgotten'] = 'Have you forgotten the password?';
$gl_caption['c_password_insert_mail'] = 'Please, enter your e-mail, and we\'ll send you a new one to your mail account';
$gl_caption['c_password_insert_mail_exists'] = 'If you have forgotten your password, please click accept and we\'ll send you a new one to your mail account';
$gl_caption['c_password_mail_error'] = 'The e- mail address entered, doesn´t exist, please enter the correct';
$gl_caption['c_password_sent'] = 'A new password have been sent to your e-mail';
$gl_caption['c_password_mail_subject'] = 'The new password';
$gl_caption['c_password_mail_text_1'] = 'The new password is';
$gl_caption['c_password_mail_text_2'] = 'You can change the password in our website';
$gl_caption['c_welcome'] = 'Wellcome';

$gl_caption['c_new_custumer_exists'] = 'You have already introduced a person with this name, full name or date of birth. You can:';
$gl_caption['c_new_custumer_exists_change_name'] = 'Correct the name or date of birth
';
$gl_caption['c_new_custumer_exists_return'] = 'Return to the reservation and select a person on the list';

// captcha
$gl_caption['c_recaptcha_error'] = 'Please validate the "CAPTCHA" correctly';
?>