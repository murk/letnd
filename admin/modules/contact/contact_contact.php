<?
/**
 * ContactContact
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2012
 * @version $Id$
 * @access public
 */
class ContactContact extends Module{

	function __construct(){
		parent::__construct();
		$this->order_by = 'entered DESC,name ASC';
	}
	function on_load(){
		if (is_file(CLIENT_PATH . '/contact_contact_config.php')){
			include (CLIENT_PATH . '/contact_contact_config.php');
			if ($fields) $this->fields = array_merge ($this->fields, $fields);
		}	
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function get_records()
	{
	    $listing = new ListRecords($this);
		$listing->set_field( 'mail', 'type', 'none' );
		$listing->add_options_button('export_button', 'action=export', 'botoOpcions1 excel', 'top_after');
		$listing->order_by = $this->order_by;
		
		/* Ho desactivo perque no està ni fet
		//////////////////////////////////////
		if ($gl_config['custumer_table'] == 'custumer__custumer') {
			$listing->add_button ('assign_custumer', '', 'boto2', 'before','add_newsletter');
		}
		
		if ($gl_config['custumer_table'] == 'newsletter__custumer') {
			$listing->add_button ('assign_newsletter', 'action=save_rows_add_custumer', 'boto2', 'before');
		}
		*/
		$listing->call( 'records_walk', '', true );
		
		$listing->has_bin = false;

	    return $listing->list_records();
	}


	function records_walk( &$listing ) {

		$rs = &$listing->rs;

		$ret['mail'] = '<a href="mailto:' . $rs['mail'] . '">' . $rs['mail'] . '</a>';

		return $ret;

	}

	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{
	    $show = new ShowForm($this);
		$show->has_bin = false;
		$show->order_by = $this->order_by;
	    return $show->show_form();
	}

	function save_rows()
	{
	    $save_rows = new SaveRows($this);
	    $save_rows->save();
	}

	function write_record()
	{
	    $writerec = new SaveRows($this);
	    $writerec->save();
	}

	function manage_images()
	{
	    $image_manager = new ImageManager($this);
	    $image_manager->execute();
	}

	function export() {
		Main::load_class( 'excel' );

		$excel = new Excel( $this );
		$excel->set_visible_fields()
		      ->file_name( 'contact' )
		      ->order_by( $this->order_by )
		      ->make();
	}
}
?>