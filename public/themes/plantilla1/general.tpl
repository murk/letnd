<!DOCTYPE html	PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>
<?=$page_title?>
</title>
<meta name="Description"
content="<?=$page_description?>" />
<!-- <?=$page_description?> -->
<meta name="Keywords"
content="<?=$page_keywords?>" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="/public/themes/plantilla1/styles/general.css" rel="stylesheet" type="text/css">
</head>
<script type="text/javascript">
var a_vars = Array();
var pagename='<?=$title?>';
var phpmyvisitesSite = 1;
var phpmyvisitesURL = "<?=HOST_URL?>common/includes/phpmv2/phpmyvisites.php";
</script>
<script language="javascript" src="/common/includes/phpmv2/phpmyvisites.js" type="text/javascript"></script>
<script language="JavaScript" src="/public/jscripts/hide_m.js"></script>
<script type="text/javascript" src="/common/jscripts/mootools.js"></script>
<script type="text/javascript" src="/common/jscripts/modalbox.js"></script>
<body>
<div id="container">
  <div id="header">
    <div id="header_left">
	</div>
    <div id="header_right">
      <h1>
        <?=$page_slogan?>
      </h1>
        <?if ($blocks_ht):?>
        <div id="header_top">
            <?foreach($blocks_ht as $b): extract ($b)?>
              <?=$block?>
              <?endforeach //$blocks?>
        </div>
          <?endif //$blocks_ht?>
        <?if ($blocks_hb):?>
          <div id="header_bottom">
            <?foreach($blocks_hb as $b): extract ($b)?>
              <?=$block?>
              <?endforeach //$blocks?>
          </div>
          <?endif //$blocks_hb?>
	</div>
  </div><div class="clearfloat"></div>
  <!-- Tenca header -->
  <div id="content">
    <div id="content_top">
      <?if ($blocks_t):?>
        <div id="content_t">
          <?foreach($blocks_t as $b): extract ($b)?>
              <?=$block?>
              <?endforeach //$blocks?>
        </div>
        <?endif //$blocks_t?>
    </div>
	<div id="content_all_center"><!--inclou el left, mt, mb, right i content_center -->
      <div id="content_left"><table border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td align="left" class="t14ptboldnegra">777 888 999</td>
        </tr>
        <tr>
          <td><img src="/public/themes/plantilla1/images/spacer.gif" width="1" height="10" /></td>
        </tr>
        <tr>
          <td><table border="0" cellspacing="0" cellpadding="0">
              <form action="/index.php?tool=inmo&tool_section=property" method="get" name="form1" id="form1">
                <tr>
                  <td><input name="tool" type="hidden" value="inmo" />
                    <input name="tool_section" type="hidden" value="property" />
                    <input name="action" type="hidden" value="list_records_search_search" />
                    <input name="q" type="text" size="18" /></td>
                  <td>&nbsp;
                    <input type="image" name="imageField" src="/public/themes/plantilla1/images/lupa2.gif" style="border:0" /></td></tr>
              </form>
            </table></td>
        </tr>
      </table>   
		<?if ($blocks_l):  ?> 
        <div id="content_left_l">
          <?foreach($blocks_l as $b): extract ($b)?>
            <?=$block?>
            <?endforeach //$blocks  ?>
        </div>
        <?endif //$blocks_l?> 
	  </div><!--tenca content_left -->	     
	<div id="content_middle_top">
	<?if ($blocks_mt):?>
	 <div id="content_mt">	
     <?foreach($blocks_mt as $b): extract ($b)?><?=$block?>
      <?endforeach //$blocks?>
	  </div><!-- tenca content mt -->
     <?endif //$blocks_mt?>
	</div><!-- tenca content_midde_top -->
	<div id=content_content>
	  <?=$content;?>
	  <?if ($message):?>
  	<br />
  	<br />
      <br />
      <br />
      <br />
      <table align="center">
        <tr>
          <td align="center"><?=$message?></td>
        </tr>
      </table>
    <?endif //message?></div><!-- Tenco el content_content -->	
    <div id="content_middle_bottom">
	<?if ($blocks_mt):?>
	 <div id="content_mb">	
     <?foreach($blocks_mt as $b): extract ($b)?><?=$block?>
      <?endforeach //$blocks?>
	  </div><!-- tenca content bt -->
     <?endif //$blocks_mb?>
	</div><!-- tenca content_midde_bottom -->
	</div>	<!--tenca content_all_center -->
	</div> <!-- Tenca content -->
  <div id="footer">
    <?=$page_address?>
    <br /><a href="mailto:<?=$default_mail?>" class="links_generals">
  <?=$default_mail?>
  </a>
  </div>
</div><!-- tenca container-->
</body>
</html>
