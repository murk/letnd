<?php
// Aquest arxiu es genera automaticament
// Qualsevol canvi que es faci es podrà sobrescriure involuntariament desde l'eina de gestió
// Achtung!
$gl_file_protected             = false;
$gl_db_classes_fields_included = array();
$gl_db_classes_language_fields = array(
	0 => '',
);
$gl_db_classes_fields          = array(
	'familybusy_id'           =>
		array(
			'type'                => 'hidden',
			'enabled'             => '1',
			'form_admin'          => 'input',
			'form_public'         => '0',
			'list_admin'          => 'text',
			'list_public'         => '0',
			'order'               => '0',
			'default_value'       => '',
			'override_save_value' => '',
			'class'               => '',
			'javascript'          => '',
			'text_size'           => '',
			'text_maxlength'      => '11',
			'textarea_cols'       => '',
			'textarea_rows'       => '',
			'select_caption'      => '',
			'select_size'         => '',
			'select_table'        => '',
			'select_fields'       => '',
			'select_condition'    => '',
		),
	'family_id'       =>
		array(
			'type'                => 'hidden',
			'enabled'             => '1',
			'form_admin'          => 'input',
			'form_public'         => '0',
			'list_admin'          => '0',
			'list_public'         => '0',
			'order'               => '0',
			'default_value'       => R::id( 'family_id' ),
			'override_save_value' => '',
			'class'               => '',
			'javascript'          => '',
			'text_size'           => '',
			'text_maxlength'      => '11',
			'textarea_cols'       => '',
			'textarea_rows'       => '',
			'select_caption'      => '',
			'select_size'         => '',
			'select_table'        => '',
			'select_fields'       => '',
			'select_condition'    => '',
		),
	'date_busy'           =>
		array(
			'type'                => 'date',
			'enabled'             => '1',
			'form_admin'          => 'input',
			'form_public'         => 'input',
			'list_admin'          => 'text',
			'list_public'         => 'text',
			'order'               => '7',
			'default_value'       => '',
			'override_save_value' => '',
			'class'               => '',
			'javascript'          => '',
			'text_size'           => '',
			'text_maxlength'      => '50',
			'textarea_cols'       => '4',
			'textarea_rows'       => '25',
			'select_caption'      => '',
			'select_size'         => '',
			'select_table'        => '',
			'select_fields'       => '',
			'select_condition'    => '',
		)
);
$gl_db_classes_related_tables  = array(
);
$gl_db_classes_list            = array(
	'cols' => '1',
);
$gl_db_classes_form            = array(
	'cols'         => '1',
	'images_title' => '',
);
?>