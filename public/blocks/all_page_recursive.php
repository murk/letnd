<?php
$page = &$GLOBALS['gl_page'];
include_once ('all_page_recursive_functions.php');

// valors per defecte
// $vars['level'] l'agafa de la taula all__block -> vars, s'escriu: menu=2&start_level=2&last_level=1
$start_level = isset($vars['start_level'])?$vars['start_level']:'1';
$last_level = isset($vars['last_level'])?$vars['last_level']:'1';
$menu = isset($vars['menu'])?$vars['menu']:'1';
$set_top_title = isset($vars['set_top_title'])?$vars['set_top_title']:'1';
$select_from_child = isset($vars['select_from_child'])?$vars['select_from_child']:'0';

// si poso parent id al block, sempre m'agafarà els menus d'aquell parent_id, 
// si no dependrà de on siguem, per tant el submenu canvia depenent de en quin apartat som

$is_parent_id_fixed = isset($vars['parent_id']);
$parent_id = isset($vars['parent_id'])?$vars['parent_id']:$page->parent_id; 

Debug::add('Page_id',$page->page_id);
Debug::add('Parent_id',$parent_id);
// faig servir el top id per marcar una pàgina, ja que pot ser que estiguem en un submenu i s'hagi de marcar la pagina d'on penja
$page_id = $vars['page_id'] = $page->page_id; // amb ['vars] passo la variable al tpl
$vars['parent_id'] = $parent_id; // així el tinc al block
$level = $vars['level'] = $page->level;

// quan no hi ha un GET[page], ho he de posar tot a 0 perque mostri els menus igualment
if (!$page_id) $page_id = 0;
if (!$parent_id) $parent_id = 0;
if (!$level) $level = 0;


$parents = $selected_ids = array();
$parents[$level] = $parent_id; // el parent d'aquest nivell on som
$current_parent = $parent_id;
$selected_ids[$level] = $page_id; // el parent d'aquest nivell on som
$i = $level-1;


if (!$is_parent_id_fixed){
	if ($level<$start_level){
		// si estem en un nivell superior del que volem mostrar, haig de cambiar el parent_id
		// En aquest cas nomès poden ser els fills dierctes d'aquest menu, per tant puc tindre un menu principal que mostra nivells 1-2 i un secuhdari que mostra 3-4, però el secundari no pot mostrar 4 nomès, ja que no sé quin es
		if (($start_level-$level) >1) {
			// si no hi ha resultats no presento el block
			if (!isset($GLOBALS['gl_show_block_page_recursive'])) $show_block = false;
			return array();
			}
		$current_parent = $page_id;	
	}
	// TODO Això també s'hauria de fer quan es $is_parent_id_fixed
	else{
		// busco el parent del level d'inici i els tots els sleccionats fins el nivell inicial
		//  (si es un submenu puc començar del segon nivell o tercer, el primer nivell pot ser en un menu a dalt)
		while ($i>=$start_level){
			$parents[$i] = $current_parent = Db::get_first(
			"SELECT parent_id FROM all__page WHERE page_id = " . $parents[$i+1]);
			$selected_ids[$i] = $parents[$i+1]; // es el page_id actual, si a la consulta de dalt ho he posat ... page_id = " . $parents[$i+1]);
			$i--;
		}
	}
}

// per seleccionar igualment el menu quan un submenu està seleccionat encara que estigui definit el parent_id
elseif ($select_from_child){
	
	$parents[$i+1] = $page->parent_id;
			
	while ($i>=$start_level){
			$parents[$i] = Db::get_first(
			"SELECT parent_id FROM all__page WHERE page_id = " . $parents[$i+1]);
			$selected_ids[$i] = $parents[$i+1]; // es el page_id actual, si a la consulta de dalt ho he posat ... page_id = " . $parents[$i+1]);
			$i--;
		}
	
}

Debug::add('Page_id',$page->page_id);
Debug::add('nivell',$level);	
Debug::add('parents',$parents);	
Debug::add('current_parent',$current_parent);	
Debug::add('selected_ids',$selected_ids);	

$loop = _block_all_page_get_children($current_parent, $start_level, $selected_ids, $menu, $last_level);

// si no hi ha resultats no presento el block, ej. serveix per quan tinc un block d'un 2 o 3 nivell i no hi ha resultats
if (!$loop && !isset($GLOBALS['gl_show_block_page_recursive'])) $show_block = false;

// funcionarà sempre i quan hi hagi el menu del primer nivell ( en principi crec que sempre hi serà )
// nomès es pels que son menu=1, el menu secondary no influeix en el top_id
// comprovo el $selected_ids[1], ja que quan no hi ha GET[page] no hi ha cap seleccionat
if ($start_level == 1 && $menu==1 && isset($selected_ids[1])) _block_all_page_set_top_vars($loop, $selected_ids[1]);
// si som al primer nivell, el top_title es igual que el title, per tant borro el title
// si set_top_title = 0, no cambio el title, em serveix per quan nomès tinc un nivell de menus
// posant $page->top_title en el if, crec que ja no cal el set_top_title
if ($level == 1 && $set_top_title && $page->top_title) $page->title = '';

//debug::p($loop);

?>