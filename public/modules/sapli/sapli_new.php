<?
class SapliNew extends Module{

	function __construct(){
		parent::__construct();
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function get_records()
	{	    								
		$sapli=R::id('sapli');
		$tpl = new phemplate(PATH_TEMPLATES);
		switch($sapli){
			case '1' : $tpl->set_file('sapli/new_categories1.tpl');
						$category_1='1';
						$category_2='2';
						$cat_projects='1';
				break;
			case '2': 
						$tpl->set_file('sapli/new_categories2.tpl');
						$category_1='3';
						$category_2='4';
						$cat_projects='2';
				break;
			case '3': 
						$tpl->set_file('sapli/new_categories3.tpl');
						$category_1='5';
						$category_2='6';
						$cat_projects='3';
				break;
		} 		
		$tpl->set_vars($this->caption);
		$tpl->set_loop('category_1',$this->_get_category_records($category_1));
		$tpl->set_loop('category_2',$this->_get_category_records($category_2));
		$tpl->set_loop('projectes',$this->_get_caracter_records($cat_projects));
		return $tpl->process();		
	}
	function _get_category_records($category_id){
		$listing = new ListRecords($this);
		$listing->use_no_template = true;
		$listing->condition = "status = 'public'";
		$listing->condition .= " AND category_id = '" . $category_id . "'";
		$listing->call_function['content'] = 'add_dots';		
		$listing->order_by = 'ordre';
		$listing->paginate=false;
		$listing->set_records();		
		return $listing->loop;
	}
	function _get_caracter_records($category_id){
		$module = Module::load('projects','new', '', false); // així no necessito el sapli_caracter.php
		$listing = new ListRecords($module);
		/*$listing->use_default_template = false;
		$listing->set_records();*/
		$listing->use_no_template = true;
		$listing->condition = "status = 'public'";
		$listing->condition .= " AND category_id = '" . $category_id . "'";
		$listing->call_function['content'] = 'add_dots';		
		$listing->order_by = 'ordre';
		$listing->paginate=false;
		$listing->set_records();		
		return $listing->loop;
	}
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{
	    //$this->set_news_fields();
		$show = new ShowForm($this);
		
		// si no hi ha id, agafo per defecte la primera noticia de la categoria, i si no hi ha categoria, de la primera categoria
		if (!$show->id){
			// si no hi ha categoria agafo la primera ordenat per nom i id
			$category_id = R::id('category_id');
			
			if (!$category_id) {
				$query = "SELECT sapli__category.category_id as category_id
					FROM sapli__category_language, sapli__category
					WHERE sapli__category_language.category_id = sapli__category.category_id
					AND language = '" . LANGUAGE . "'
					ORDER BY category, category_id
					LIMIT 0,1";
				$category_id = Db::get_first($query);
			}
			$query = "SELECT sapli__new.new_id as new_id
				FROM sapli__new_language, sapli__new
				WHERE sapli__new_language.new_id = sapli__new.new_id
				AND status = 'public'
				AND bin <> 1
				AND category_id = '" . $category_id . "'
				AND language = '" . LANGUAGE . "'
				ORDER BY ordre DESC, new_id
				LIMIT 0,1";
			$show->id = Db::get_first($query);
		}
		$show->condition = "status = 'public'";
		$show->call('records_walk','url1,url1_name');
		$show->get_values();		
		if ($show->has_results)
		{
			$GLOBALS['gl_page']->title = $show->rs['new'];
			return $show->show_form();
		}
		else
		{
			$this->do_action('list_records');
		}
	}

	function records_walk($url1,$url1_name){		
		// Això ho poso aquí així no cal posar-ho al tpl, simplement posar: <a href="<=$url1>" target="_blank"><=$url1_name></a>
		$ret['url1_name'] = $url1_name?$url1_name:$url1;			
		$ret['url1']=str_replace('http://','',$url1);
		return $ret;
	}	
}	
?>