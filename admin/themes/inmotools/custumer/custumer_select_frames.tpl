<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<?=load_admin_css()?>
<link href="/common/jscripts/jquery-ui-1.8.20/css/ui-lightness/jquery-ui-1.8.20.custom.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/common/jscripts/jquery/jquery-1.7.2.min.js"></script>
<? if (!DEBUG):?>
<script type="text/javascript" src="/common/jscripts/jquery.onerror.js?v=<?=$version?>"></script>
<? endif //DEBUG?>
<script type="text/javascript" src="/common/jscripts/jquery-ui-1.8.20/js/jquery-ui-1.8.20.custom.min.js?v=<?=$version?>"></script>
<script type="text/javascript" src="/common/jscripts/jquery-ui-1.8.20/languages/jquery.ui.datepicker-<?=$language_code?>.js?v=<?=$version?>"></script>
<script type="text/javascript" src="/common/jscripts/jquery.dump.js?v=<?=$version?>"></script>
<script type="text/javascript" src="/common/jscripts/jquery.mousewheel.js?v=<?=$version?>"></script>
<script type="text/javascript" src="/common/jscripts/jquery.timepicker.js?v=<?=$version?>"></script>
<script type="text/javascript" src="/common/jscripts/jquery.cookie.js?v=<?=$version?>"></script>
<script type="text/javascript" src="/common/jscripts/jquery.blockUI.js?v=<?=$version?>"></script>
<script type="text/javascript" src="/common/jscripts/jquery.dropshadow.js?v=<?=$version?>"></script>
<script type="text/javascript" src="/admin/jscripts/main.js?v=<?=$version?>"></script>
<script type="text/javascript" src="/admin/languages/<?=$language?>.js?v=<?=$version?>"></script>


<script language="JavaScript" src="/admin/modules/custumer/jscripts/custumer.js?v=<?=$version?>"></script>
<script language="JavaScript" src="/admin/jscripts/assign.js?v=<?=$version?>"></script>
<SCRIPT LANGUAGE="JavaScript" TYPE="text/javascript">
<!--
var addreses_selected = new Array();
//-->
</SCRIPT>
</head>
<body class="select-frame">
<div class="search">
	<form action="/admin/" method="get" name="search" target="listAddresses">
	<table border="0" cellpadding="0" cellspacing="0" class="search_select_frame">   
		<tr> 
		  <td><?=$c_by_words?></td>
		  <td><input name="action" type="hidden" value="list_records_select_search_words_property"><input name="menu_id" type="hidden" value="5002">
			<input name="q" type="text" size="30"></td><td><input type="submit" name="Submit2" value=">" class="boto-search"> &nbsp;&nbsp;
				  <?=$c_by_ref?></td>
		</tr>	   
	</table>
	</form>
</div>
<table width="100%" height="1" border="0" cellpadding="0" cellspacing="0" bgcolor="#999999">
  <tr>
    <td></td>
  </tr>
</table>
<table border="0" cellspacing="0" cellpadding="0" height="448">
  <tr>
    <td valign="top"><iframe name="listAddresses" scrolling="yes" marginwidth="0" marginheight="0" frameborder="0" vspace="0" hspace="0" style="overflow:visible; width: <?=(isset($_GET['process']))?950:710?>px; height:<?=(isset($_GET['process']))?478:448?>px;"  src="/admin/?action=list_records_select_property&menu_id=5002"></iframe></td>
    <td class="assign_right" style="width:<?=(isset($_GET['process']))?0:240?>px" valign="top">
		<h5><?=$c_selected?></h5>
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		  <tr>
			<td id="write_cell"></td>
		  </tr>
		</table>
<SCRIPT LANGUAGE="JavaScript" TYPE="text/javascript">
<!--
populate_table();
//-->
</SCRIPT></td>
  </tr>
</table>
<?if(!isset($_GET['process'])):?>
<form action="/admin/?action=save_rows_selected&menu_id=5002" method="post" name="theForm" target="save_frame">
<table width="100%" height="1" border="0" cellpadding="0" cellspacing="0" bgcolor="#999999">
  <tr>
    <td height="30" align="center">	
	  
	  <input name="property_ids" type="hidden" value="">
	  <input name="custumer_id" type="hidden" value="<?=$custumer_id?>">
      <div class="centerFloat1"><div class="centerFloat2">
	  <input class="btn_save_select_frame" type="button" name="Submit" value="<?=$c_ok?>" onClick="pass_selected_addresses()">
	  <input class="btn_select_frame" type="button" name="Submit" value="<?=$c_cancel?>" onClick="top.hidePopWin(false);"></div></div>
	  <input name="selected_addresses_ids" type="hidden" value="">
      <input name="selected_addresses_names" type="hidden" value="">
	  	  
    </td>
  </tr>
</table>
</form>
<?endif //process?>
</body>
</html>