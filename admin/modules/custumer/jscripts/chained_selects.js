window.onload = init_values;
var arrRelated_options = new Array(); // holds values for each object
var arrRelated_options2 = new Array(); // holds values for each object
var selProvincies,selComarques,selMunicipis; // Name and Values Select object

var dosIniciat = false;

function init_values()
{
	property_id = $('input[name=property_id_javascript]').val();
	selProvincies = $('select[name="provincia_id['+property_id+']"]').get(0); //eval('document.theForm[\'provincia_id['+document.theForm.property_id_javascript.value+']\']');
	selComarques =  $('select[name="comarca_id['+property_id+']"]').get(0); //eval('document.theForm[\'comarca_id['+document.theForm.property_id_javascript.value+']\']');
	selMunicipis =  $('select[name="municipi_id['+property_id+']"]').get(0); //eval('document.theForm[\'municipi_id['+document.theForm.property_id_javascript.value+']\']');
	selProvincies.onchange = change_values;
	selComarques.onchange = change_values2;

	for (var i=0; i != selProvincies.length; i++)
	{
		arrRelated_options[selProvincies.options[i].value] = new RelatedOptions(selProvincies.options[i].value, selComarques);
	}

  for (var i=0; i != selComarques.length; i++)
	{
    arrRelated_options2[selComarques.options[i].value] = new RelatedOptions(selComarques.options[i].value, selMunicipis);
	}
	dosIniciat = true;
	change_values()
	change_values2()
	load_all()
}

function change_values()
{
	var i = selProvincies.options[selProvincies.selectedIndex].value;
	arrRelated_options[i].show();
  if (dosIniciat)change_values2();
}

function change_values2()
{
	var i = selComarques.options[selComarques.selectedIndex].value;
	arrRelated_options2[i].show();
}


// -------------------
// class related_options
// -------------------

function RelatedOptions(_id, selValues)
{
	_id= "#"+_id+"#";
	this.values =  new Array();
	this.texts =   new Array();
  this.selectedIndex = 0;
	this.show = show;

	// store values and text in each array
	var _text, count = 0;
	for(var i=0; i!=selValues.length; i++)
	{
		if (selValues.options[i].text.indexOf(_id)==0)
		{
			this.values[count] = selValues.options[i].value;
			_text =  selValues.options[i].text;
			this.texts[count] = _text.substring(_id.length,_text.length);
      // ja no es un més ja que he tret el valor per defecte
      if (selValues.selectedIndex==i) this.selectedIndex = count;
			count++;
		}
	}

	// reset and repopulate the Select
	function show()
	{
		selValues.length = this.values.length;

  	//selValues.options[0].value = 0;
  	//selValues.options[0].text = '';

		for(var i=0; i!=this.values.length; i++)
		{
			selValues.options[i].value = this.values[i];
			selValues.options[i].text = this.texts[i];
		}
    selValues.selectedIndex=this.selectedIndex;
	}
}
