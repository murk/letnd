

var product = {
	is_cart_down: false,
	open_cart: function (){
		if (this.is_login_down) this.open_login();
		if (this.is_cart_down){
			$('#cart_block .cart-block').slideUp(200);
			$('#cart_block').removeClass('down');
		}
		else{
			$('#cart_block .cart-block').slideDown(200);
			$('#cart_block').addClass('down');
		}
		this.is_cart_down = !this.is_cart_down;
	},
	is_login_down: false,
	open_login: function (){

		if (this.is_cart_down) this.open_cart();

		if (this.is_login_down){
			$('#login-submenu').slideUp(200);
			$('#login13').removeClass('down');
		}
		else{
			$('#login-submenu').slideDown(200);
			$('#login13').addClass('down');
		}
		this.is_login_down = !this.is_login_down;
	},
	logout: function(){

		link = '/'+gl_language+'/product/customer/logout/';

		$.ajax({
			url: link
		})
			.done(function(data) {

				location.reload();

			});
	},
	// TODO-i Personalitzar missatge
	show_message: function (tool_section, action, message, code, vars) {
		if (code == 'logged') {
			name = vars['name'];
			surname = vars['surname'];

			alert(message + ' ' + name + ' ' + surname);
		}
	},
	variation_on_select: function (product_pvp, variation_category_id, product_variation_id){
		$('#variation_category_holder_' + variation_category_id).toggleClass('opened');
	},
	variation_set_images: function (base_price, variation_category_id, product_variation_id) {

		client.reset_product_slider();

		$('.slider-product-thumbs').hide();
		$('.slider-product').hide();

		if ($('#slider-product-' + product_variation_id).length) {
			$('#slider-product-thumbs-' + product_variation_id).show();
			$('#slider-product-' + product_variation_id).show();
		}
		// Imatges per defecte
		else {
			$('#slider-product-thumbs-0').show();
			$('#slider-product-0').show();
		}
		product_set_price(base_price, variation_category_id, product_variation_id);

		client.init_product_slider();

	}
};