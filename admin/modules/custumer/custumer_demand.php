<?php
global $gl_caption_property;
// obtinc caption de properties si no l'he obtingut ja desde custumer_custumer.php
if (!isset($gl_caption_property)) set_language_vars('inmo', 'property', 'demand');
// obtinc el config de inmo de provincies
set_inmo_provinces();

include_once (DOCUMENT_ROOT . '/admin/modules/inmo/inmo_property_functions.php');
include_once( DOCUMENT_ROOT . 'admin/modules/custumer/custumer_demand_common.php' );
//include_once (DOCUMENT_ROOT . '/admin/modules/inmo/custumer_demand_functions.php');

function list_records()
    {
   	global $gl_action, $gl_message, $gl_messages ,$gl_resultats, $gl_menu_id, $tpl;

    if ($gl_action == 'list_records_get_checkboxes_long_values')  demand_get_checkboxes_long_values();

    $type = $gl_action == "list_records_exact"?'exact':'prox';
    if (($gl_action == "list_records_exact") || ($gl_action == "list_records_prox")) check_demands($type);

    if ($gl_resultats)
    {
        list_demands($type);
    }else
    {

	    // si mostrem resultats per un client concret no s'ha de mostrar tot el llistat de demandes
		if (isset($_GET['custumer_id'])){
			$rs = Db::get_row('SELECT name, surname1, surname2 FROM custumer__custumer WHERE custumer_id=' . $_GET['custumer_id']);
			$gl_message = $gl_messages['no_records_custumer'] . ' ' . $rs['name'] . ' ' . $rs['surname1'] . ' ' . $rs['surname2'];			
		}
		else
		{
			// si estavem a llistar sol.licituds i no ha trobat cap resultat mostrar el missatge
		    if (($gl_action == "list_records_exact") || ($gl_action == "list_records_prox"))
			{
				$gl_message = isset($_GET['demand_id'])?$gl_messages['no_records_demand'] . ' ' . $_GET['demand_id']:$gl_messages['no_records_demands'];
			}
			// no mostro llistat si no hi ha resultats i estem buscant			
			else{
				$tpl->set_var('is_tool_custumer', false); // per diferenciar del llistat de dins form custumer
				$listing = new ListRecords;
				$listing->has_bin = false;
				$listing->join = "INNER JOIN custumer__custumer USING (custumer_id)";
				$listing->condition = 'custumer__custumer.bin = 0';
				$listing->set_options(1, 1, 0, 1, 1, 1, 1);
				$listing->add_button ('searchexact_button', 'action=list_records_exact', 'boto1', 'after');
				$listing->add_button ('searchprox_button', 'action=list_records_prox', 'boto1', 'after');
				$listing->add_filter ('tipus');
				$listing->add_filter ('category_id','custumer__demand_to_category INNER JOIN custumer__demand USING (demand_id)');
				$listing->call('demand_records_walk','land,land_units,demand_id',true);

				$listing->add_field( 'activated', array(
					'type'=> 'select',
					'list_public'=> '0',
					'list_admin'=> '0',
					'select_fields' => '0,1'
				));
				$listing->add_filter ('activated');


				$listing->list_records();
			}
		}
	}
}
function show_form()
{
	global $gl_action, $gl_content, $gl_write, $tpl;
	// formulari demanda
    $show = new ShowForm;
    $show->join = "INNER JOIN custumer__custumer USING (custumer_id)";

	// poso a none perque es un cas especial, agafa comarques i municipis
	$show->set_field( 'municipi_id', 'type', 'none' );

	$show->call('demand_records_walk','land,land_units,demand_id',true);
	$show->has_bin = false;
    if ($gl_write) {
        //$show->call_function['comarca_id'] = "get_select_comarques";
        //$show->call_function['municipi_id'] = "get_select_municipis";
    }

//    $query_provincies = "provincia_id='" . str_replace(",", "' OR provincia_id='", INMO_PROVINCIES) . "'";
//    $show->fields["provincia_id"]['select_condition'] = $query_provincies;

	$show->show_form();
}
function set_inmo_provinces()
{
    global $gl_config, $gl_tool;

    $query = "SELECT name, value
				FROM inmo__configadmin
				WHERE name='provincies'";
    $results = Db::get_rows($query);
    foreach ($results as $rs) {
    define('INMO_PROVINCIES' , $rs['value']);
    $gl_config[$rs['name']] = $rs['value'];
    return $rs['value'];
    }
}
function write_record()
{
    global $gl_message, $gl_action;
    $writerec = new SaveRows;
	$writerec->unset_field( 'municipi_id' );
    if (strstr($gl_action, "add_record")) {
        $writerec->fields["entered"]['override_save_value'] = "now()"; //afageixo la d'ata d'alta
    }
    if (current($_POST['land_units'])=='ha') {
    	$_POST['land'][current($_POST['demand_id'])] = current($_POST['land'])*10000;
    }
	// obtinc valor municipis abans de guardar ( per el add_record es necessari
	$municipis = $writerec->get_value( 'municipi_id' );

    $writerec->save();


	// Guardo municipis i comarques

	Debug::p( $municipis );
	//Debug::p( $writerec->id );

	if ($writerec->id) {
		// Primer borro tots els relacionats
		Db::execute( "DELETE FROM custumer__demand_to_municipi WHERE demand_id = " . $writerec->id );

		if ( $municipis ) {
			foreach ( $municipis as $k => $v ) {
				$vals = explode( '_', $v );
				Debug::p( $vals );
				$query = "INSERT INTO
							custumer__demand_to_municipi
							(demand_id,comarca_id,municipi_id) VALUES
							 ('" . $writerec->id . "','" . $vals[0] . "','" . $vals[1] . "');";
				Db::execute( $query );
			}
		}
	}

	// incloc demanda al formulari per jscript
	if (isset($_GET['template']) && $_GET['template']=='window') {
		global $tpl;
		$tpl->set_var('is_tool_custumer', false); // per diferenciar del llistat de dins form custumer
		print_javascript("top.show('demands');");
		set_inner_html('demands',list_demands_of_custumer(current($_POST['custumer_id'])));
	}
	else
	{
	    $_SESSION['message'] = $gl_message;
    }

}
function js_show($class,$id){
	return "top.$('".$id . " ." . $class . "').removeClass('hidden-important');";
}
function js_hide($class,$id){
	return "top.$('".$id . " ." . $class . "').addClass('hidden-important');";
}
function save_rows()
{
    // moure en una funcio propia quan v3
	$process = $GLOBALS['gl_process'];
	$caption = &$GLOBALS['gl_caption'];
	if($process){
		Debug::p($_GET, 'text');
		Main::load_class('custumer','history');
		
		$demand_id = R::id('demand_id');
		$custumer_id = R::id('custumer_id');
		$property_id = R::id('property_id');
		$client_id = R::id('client_id',0);
		$history = R::post('history');
		$history_title = R::post('history_title');
		$js = '';
		$js_id = "#list_row_".$property_id."_demand_".$demand_id . "_client_".$client_id;
		
		$condition = 
				" WHERE  `demand_id` = $demand_id
					AND property_id = $property_id
					AND client_id = $client_id";
		
		if ($process == 'refuse_property'){			
			
			$tipus = "demand_refused";
			$update = "refused=1,liked=0";
			$insert = "1,0";
			
			$js .= js_show('refused', $js_id);
			$js .= js_hide('liked', $js_id);
			
			$js .= js_hide('btn_refuse', $js_id);
			$js .= js_show('btn_like', $js_id);
			$js .= js_show('btn_reset', $js_id);
		}
		elseif ($process == 'reset_property'){
			
			$tipus = "demand_reseted";
			$update = "refused=0,liked=0";
			$insert = false;
			// no hi ha insert, nomes podem vindre si ja està a la taula
			
			$js .= js_hide('refused', $js_id);
			$js .= js_hide('liked', $js_id);
			
			$js .= js_show('btn_refuse', $js_id);
			$js .= js_show('btn_like', $js_id);
			$js .= js_hide('btn_reset', $js_id);

		}
		elseif ($process == 'like_property'){
			
			$tipus = "like_property";
			$update = "refused=0,liked=1";
			$insert = "0,1";
			
			$js .= js_hide('refused', $js_id);
			$js .= js_show('liked', $js_id);
			
			$js .= js_show('btn_refuse', $js_id);
			$js .= js_hide('btn_like', $js_id);
			$js .= js_show('btn_reset', $js_id);
			
		}
		
		Debug::add('Demand_to_property Insert', $insert);
		Debug::add('Demand_to_property Update', $update);
		Debug::add('Demand_to_property Insert', $condition);
		
		// entro custumer__demand_to_property
		if(
			Db::get_first("
			SELECT count(*) FROM custumer__demand_to_property" . $condition
			)
		){

			Db::execute("
				UPDATE custumer__demand_to_property
				SET 
				" . $update . $condition);

		}
		elseif ($insert) {
			
			Db::execute("
				INSERT INTO custumer__demand_to_property (
				`demand_id`, `property_id`, `client_id`, refused,liked) 
				VALUES ($demand_id, $property_id, $client_id, $insert);");
		}
		
		
		// escric a l'historial
		CustumerHistory::add_demand_to_history(
				$history, 
				$history_title, 
				$tipus, 
				$custumer_id, 
				$property_id, 
				$demand_id,
				$client_id);
		
		// afegeixo botons
		
		print_javascript("top.$('#history-holder').remove();" . $js);
		$GLOBALS['gl_message'] = $GLOBALS['gl_messages']['saved'];
	}
	
	else {
		$save_rows = new SaveRows;
		$save_rows->save();
	}
}
function manage_images()
{
    // $image_manager = new ImageManager;
    // $image_manager->execute();
}
function get_form_municipi_id(){

}

function demand_get_checkboxes_long_values () {

//	$field = R::get( 'field' );
//	$id = R::get( 'id' );
	$limit = 100;

	$q = R::escape('query');

	$ret = array();
	$ret['suggestions'] = array();

	$r = &$ret['suggestions'];


		// 1 - Comarca, que comenci
		$query = "SELECT comarca_id, comarca, '0' as municipi_id, '' as municipi
					FROM inmo__comarca
					WHERE comarca LIKE '" . $q . "%'
					ORDER BY comarca
					LIMIT " . $limit;

		$results = Db::get_rows($query);

		$rest = $limit - count( $results );

		// 2 - Comarca, qualsevol posició
		if ($rest > 0) {

			$query = "SELECT comarca_id, comarca, '0' as municipi_id, '' as municipi
						FROM inmo__comarca
						WHERE comarca LIKE '%" . $q . "%'
			            AND comarca not LIKE '" . $q . "%'
						ORDER BY comarca
						LIMIT " . $rest;

			$results2 = Db::get_rows($query);

			$results = array_merge( $results, $results2 );

		}


		$rest = $limit - count( $results );

		// 3 - Municipis, que comenci
		if ($rest > 0) {
			$query = "SELECT municipi_id, municipi, comarca_id, comarca
				FROM inmo__municipi
				INNER JOIN inmo__comarca USING (comarca_id)
				WHERE municipi LIKE '" . $q . "%'
				ORDER BY municipi
				LIMIT " . $rest;

			$results2 = Db::get_rows( $query );

			$results = array_merge( $results, $results2 );
		}

		$rest = $limit - count( $results );

		// 4 - Municipis, qualsevol posició i a comarca
		if ($rest > 0) {
			$query = "SELECT municipi_id, municipi, comarca_id, comarca
				FROM inmo__municipi
				INNER JOIN inmo__comarca USING (comarca_id)
				WHERE
						(municipi LIKE '%" . $q . "%'
						AND municipi not LIKE '" . $q . "%')
			        OR
						(comarca LIKE '%" . $q . "%')
				ORDER BY municipi
				LIMIT " . $rest;

			$results2 = Db::get_rows( $query );

			$results = array_merge( $results, $results2 );
		}

		// Presento resultats
		foreach ($results as $rs)
		{
			$value = $rs['municipi'] ?
				$rs['municipi'] . " - " . $rs['comarca'] :
				$rs['comarca'];
			$r[] = array(
				'value' => $value,
				'data'  => $rs['comarca_id'] . '_' . $rs['municipi_id']
			);
		}


	$rest = $limit - count( $results );
	$ret['is_complete'] = $rest>0;

	die (json_encode($ret));


}

?>