<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>


	<?=load_admin_css()?>

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<form method="post" name="images" action="<?=$link_action_upload_images?>">
    <input name="<?=$section_id_field?>" type="hidden" value="<?=$section_id?>">
</form>
<table width="100%" cellpadding="4" cellspacing="0">      
  
  <tr> 
    <td class="formCaptionTitle"><strong><a href="javascript:void(0)" onClick="javascript:(document.getElementById('image_upload_div').style.display=='block')?document.getElementById('image_upload_div').style.display='none':document.getElementById('image_upload_div').style.display='block';">
      <?=$c_insert?>
    </a></strong></td>
  </tr>
</table>
<div id="image_upload_div" style="display:block">
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="formstbl">
  <tr>
    <td width="760" colspan="2" class="formsCaptionHor"><table cellpadding="0" cellspacing="0" border="0">
      <tr>
        <td background="Images/ToolbarBackground.gif" height="26"><table cellspacing="0" border="0">
          <tr>
            <td id="Send" class="btn" 
                      onmouseover="this.className='btnOver';" 
                      onmouseout="this.className='btn';"
		               onclick="ImageUploader.Send();"><strong>&nbsp;<?=$c_add?></strong>&nbsp;</td>
            <td>| </td>
            <td id="Stop" class="btn" 
                      onmouseover="this.className='btnOver';" 
                      onmouseout="this.className='btn';"
                      onclick="ImageUploader.Stop();">&nbsp;<?=$c_stop?>&nbsp;</td>
            <td>| </td>
            <td id="SelectAll" class="btn" noselect
                      onmouseover="this.className='btnOver';" 
                      onmouseout="this.className='btn';"
                      onclick="ImageUploader.SelectAll();">&nbsp;<?=$c_select_all?>&nbsp;</td>
            <td>| </td>
            <td id="DeslectAll" class="btn" 
                      onmouseover="this.className='btnOver';" 
                      onmouseout="this.className='btn';"
                      onclick="ImageUploader.DeselectAll();">&nbsp;<?=$c_deselect_all?>&nbsp;</td>
            <td>| </td>
            <td id="Refresh" class="btn" 
                      onmouseover="this.className='btnOver';" 
                      onmouseout="this.className='btn';"
                      onclick="ImageUploader.Refresh();">&nbsp;<?=$c_refresh?>&nbsp;</td>
            <td>| </td>
            <td><select id="selectView" onChange="ImageUploader.FolderView=this.options[this.selectedIndex].value;">
              <option value="0" selected>
                <?=$c_thumbnails?>
                </option>
              <option value="1">
                <?=$c_icons?>
                </option>
              <option value="2">
                <?=$c_list?>
                </option>
              <option value="3">
                <?=$c_details?>
                </option>
            </select>
            </td>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
  <!--<tr> 
    <td>
      <form method="post" encType="multipart/form-data" action="<?=$link_action_upload_images?>">
       <input name="<?=$section_id_field?>" type="hidden" value="<?=$section_id?>"> 
      <input name="submit" type="submit">

      <input name="PackageIndex" type="text" value="-1">
      <input name="PackageCount" type="text" value="-1">
      <input name="PackageGuid" type="text" 
		value="{1DCF0D04-55D3-4B07-B622-C8F7D5D4DCC5}">
      <input name="FileCount" type="text" value="2">
<br>
<br>

      	
      <input name="SourceFileSize_1" type="text">
      <input name="Width_1" type="text">
      <input name="Height_1" type="text">
      <input name="Angle_1" type="text">
      <input name="Thumbnail1FileSize_1" type="text">
      <input name="Thumbnail2FileSize_1" type="text">
      <input name="Thumbnail3FileSize_1" type="text">      
      <input name="Description_1" type="text">
      <br>
<br>

      <input name="SourceFile_1" type="file">
      <input name="Thumbnail1_1" type="file">
      <input name="Thumbnail2_1" type="file">
      <input name="Thumbnail3_1" type="file">
      <br>
<br>
<br>
<br>

      	
      <input name="SourceFileSize_2" type="text">
      <input name="Width_2" type="text">
      <input name="Height_2" type="text">
      <input name="Angle_2" type="text">
      <input name="Thumbnail1FileSize_2" type="text">
      <input name="Thumbnail2FileSize_2" type="text">
      <input name="Thumbnail3FileSize_2" type="text">      
      <input name="Description_2" type="text">      
      <br>
<br>

      <input name="SourceFile_2" type="file">
      <input name="Thumbnail1_2" type="file">
      <input name="Thumbnail2_2" type="file">
      <input name="Thumbnail3_2" type="file">
    </form>

</td>
  </tr>-->
</table>
<script for="ImageUploader" event="Progress(Status, Progress, ValueMax, Value, StatusText)">
if (Status=="COMPLETE"){
	//parent.window.location = "<?=$reload_link?>";
	top.window.save_frame.location.href = '<?=$reload_link?>&is_ajax=true';
}
</script>
<object type="application/x-oleobject" 
            classid="clsid:FD18DD5E-B398-452A-B22A-B54636BA9F0D" width="100%" height="520" 
            codebase="/common/includes/imageuploader/ImageUploader2.cab" id="ImageUploader" name="ImageUploader" viewastext>
  <param name="Layout" value="TwoPanes" />
  <param name="UploadSourceFile" value="False" />
  <param name="EnableRotate" value="True" />
  <param name="UploadThumbnail1FitMode" value="<?=($im_big_w&&$im_big_h)?'Fit':($im_big_w?'Width':'Height')?>" />
  <param name="UploadThumbnail1Width" value="<?=$im_big_w?>" />
  <param name="UploadThumbnail1Height" value="<?=$im_big_h?>" />
  <param name="UploadThumbnail1JpegQuality" value="80" />
  <param name="UploadThumbnail2FitMode" value="<?=($im_medium_w&&$im_medium_h)?'Fit':($im_medium_w?'Width':'Height')?>" />
  <param name="UploadThumbnail2Width" value="<?=($im_medium_w)?$im_medium_w:$im_medium_h?>" />
  <param name="UploadThumbnail2Height" value="<?=$im_medium_h?>" />
  <param name="UploadThumbnail2JpegQuality" value="<?=$im_medium_q?>" />
  <param name="ShowButtons" value="False" />
  <param name="ShowDebugWindow" value="False" />
  <param name="AdditionalFormName" value="images" />
  <param name="Action" value="<?=$link_action_upload_images?>" />
  <param name="MaxFileCount" value="25" />
  <param name="MaxTotalFileSize" value="50242880" />
  <param name="Padding" value="0" />
  <param name="TreePaneWidth" value="220" />
  <param name="FileMask" value="*.jpg;*.jpeg;*.jpe;*.bmp;*.gif" />
  <param name="PreviewThumbnailSize" value="200" />
  <param name="BackgroundColor" value="#FFFFFF" />
  <param name="ShowDescriptions" value="False" />
  <param name="ProgressDialogTitleText" value="<?=$c_progress_dialog_title_text?>" />
</object>
<table border="0" align="center" cellpadding="0" cellspacing="0" style="margin-top:2px;">
  <tr>
    <td onmouseover="this.className='btnOver';" 
                      onmouseout="this.className='btn';" class="btn"><table cellspacing="0" border="0">
      <tr >
        <td onclick="ImageUploader.Send();" style="cursor:pointer"><img src="/admin/themes/inmotools/images/upload_image.gif" width="41" height="42" alt="<?=$c_add?>"></td>
        <td id="Send2" onclick="ImageUploader.Send();">&nbsp;<?=$c_add?>&nbsp;</td>
        </tr>
    </table></td>
  </tr>
</table>
</div>
<script type="text/javascript">
if (document.getElementById('im_container_0')){	// si hi ha una imatge
 	document.getElementById('image_upload_div').style.display='none';
 }
</script>
</body>
</html>