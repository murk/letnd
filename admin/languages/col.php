<?
define("MENU_HOME_PUBLIC","web pública");
define("MENU_HOME","página inicial");
define("MENU_LOGOUT","desconectar");


// configurable
define("USER_TEXT","usuario");
define('USER_LOGIN_INVALID','El nombre de usuario o password no son válidos');
define('USER_LOGIN_VALID','Bienvenido a ' . HOST_URL);

// la linea de sota no es pot tocar, que te dit que no la toquis!!!!!!!
// menus
define('EXPERIENCIA_MENU_ROOT','Experiencia');
define('NEWSLETTER_MENU_ROOT','Newsletter');
define('TRANSLATION_MENU_ROOT','Traducciones');
define('FACTURACIO_MENU_ROOT','Facturación');
define('STATISTICS_MENU_ROOT','Estadísticas');
define('USER_MENU_ROOT','Usuarios');
define('ADMINTOTAL_MENU_ROOT','Admin total');
define('WEB_MENU_ROOT','Gestión Letnd');
define('INMO_MENU_ROOT','Propiedades');
//define('PRINT_MENU_ROOT','Imprimir');
define('OWNER_MENU_ROOT','Propietarios');
define('REPORT_MENU_ROOT','Documentos');
define('CONFIG_MENU_ROOT','Configuración');
define('NEWS_MENU_ROOT','Noticias');
define('GALERY_MENU_ROOT','Galería imágenes');
define('RENOVATS_MENU_ROOT','Postales regalo');
define('SHOPWINDOW_MENU_ROOT','Escaparates');
define('UTILITY_MENU_ROOT','Utilidades');
define ('MLS_MENU_ROOT','MLS');
define('CUSTUMER_MENU_ROOT','Clientes');
define('WORKS_MENU_ROOT','Trabajos');
define('PRODUCT_MENU_ROOT','Productos');
define('CANXIQUET_MENU_ROOT','Cartas');
define('RATE_MENU_ROOT','Tarifas');
define('CARS_MENU_ROOT','Coxes');
define('NEW_MENU_ROOT','Noticias');
define('PACIENTES_MENU_ROOT','Clientes');
define('MISSATGE_MENU_ROOT','Mensajes');
define('CONTACT_MENU_ROOT','Contacto');
define('PRINT_MENU_ROOT','Documentos');





define('ESPECTACLES_MENU_ROOT','Espectáculos');
define('ESPECTACLES_MENU_ESPECTACLES','Espectáculos');
define('ESPECTACLES_MENU_LIST','Lista de espectáculos');
define('ESPECTACLES_MENU_LIST_BIN','Papelera de reciclaje');
define('ESPECTACLES_MENU_NEW','Insertar espectáculo');


define('ACTUALITAT_MENU_ESDEVENIMENT','Acontecimientos');
define('ACTUALITAT_MENU_ESDEVENIMENT_LIST','Lista de acontecimientos');
define('ACTUALITAT_MENU_ESDEVENIMENT_LIST_SEND','Enviar acontecimiento');
define('ACTUALITAT_MENU_ESDEVENIMENT_LIST_BIN','Papelera de reciclaje');
define('ACTUALITAT_MENU_ESDEVENIMENT_NEW','Insertar acontecimiento');
define('ACTUALITAT_MENU_NOTICIA','Noticias');
define('ACTUALITAT_MENU_NOTICIA_LIST','Lista de noticias');
define('ACTUALITAT_MENU_NOTICIA_LIST_BIN','Papelera de reciclaje');
define('ACTUALITAT_MENU_NOTICIA_NEW','Insertar noticia');
define('ACTUALITAT_MENU_ROOT','Actualidad');

define('FITXES_MENU_FITXES','Fichas');
define('FITXES_MENU_LIST','Lista de fichas');
define('FITXES_MENU_LIST_SEND','Enviar ficha');
define('FITXES_MENU_LIST _BIN','Papelera de reciclaje');
define('FITXES_MENU_NEW','Insertar ficha');
define('FITXES_MENU_ROOT','Fichas');


define('CARTA_MENU_ROOT','Carta y menús');
define('CARTA_MENU_PLATS','Carta');
define('CARTA_MENU_PLATS_NEW','Insertar plato');
define('CARTA_MENU_PLATS_LIST','Carta');
define('CARTA_MENU_PLATS_BIN','Papelera de reciclaje');

define('CARTA_MENU_MENUS','Menús');
define('CARTA_MENU_MENUS_NEW','Insertar menú');
define('CARTA_MENU_MENUS_LIST','Menús');
define('CARTA_MENU_MENUS_BIN','Papelera de reciclaje');


define('MENUDELDIA_MENU_ROOT','Menú del día');
define('MENUDELDIA_MENU_MENUDELDIA','Menú del día');
define('MENUDELDIA_MENU_MENUDELDIA_MAIL_END','Enviar');




define('DB_SPLIT_TITLE_FIRST_PAGE', 'Inicio');
define('DB_SPLIT_TITLE_PREVIOUS_PAGE', 'Página anterior');
define('DB_SPLIT_TITLE_NEXT_PAGE', 'Página siguiente');
define('DB_SPLIT_TITLE_LAST_PAGE', 'Final');
define('DB_SPLIT_TITLE_PAGE_NO', 'Página %d');
define('DB_SPLIT_TITLE_PREV_SET_OF_NO_PAGE', 'Anteriores %d páginas');
define('DB_SPLIT_TITLE_NEXT_SET_OF_NO_PAGE', 'Siguientes %d páginas');
define('DB_SPLIT_BUTTON_FIRST', '&lt;&lt;PRINCIPIO');
define('DB_SPLIT_BUTTON_PREV', '&lt;&lt;&nbsp;Anterior');
define('DB_SPLIT_BUTTON_NEXT', 'Siguiente&nbsp;&gt;&gt;');
define('DB_SPLIT_BUTTON_LAST', 'FINAL&gt;&gt;');
define('DB_SPLIT_DISPLAY_NUMBER_OF_RESULTS', 'Mostrando <b>%d</b> a <b>%d</b> (de <b>%d</b>)');
define ('TITLE_SEARCH','Resultados de la búsqueda');

define('DB_SPLIT_PAGE_TITLE', 'Página');
define('DB_SPLIT_PAGE_DESCRIPTION', 'Resultados %d a %d de %d');

$month_names = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
$week_names = array("Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado");


// Caption default
$gl_caption['c_form_title'] = 'Editar datos';
$gl_caption['c_print_title'] = 'Datos';
$gl_caption['c_config_title'] = 'Editar configuración';
$gl_caption['c_edit'] = 'Editar';


$gl_caption['c_edit_button'] = 'Editar datos';
$gl_caption['c_view_button'] = 'Ver datos';
$gl_caption['c_edit_images_button'] = 'Editar imágenes';
$gl_caption['c_add_images_button'] = 'Añadir imágenes';



$gl_caption['c_edit_data'] = 'Editar datos';
$gl_caption['c_edit_images'] = 'Editar imágenes';
$gl_caption['c_add_images'] = 'Añadir imágenes';
$gl_caption['c_search_form'] = 'Buscar';
$gl_caption['c_send_new'] = 'Guardar';
$gl_caption['c_send_edit'] = 'Guardar cambios';
$gl_caption['c_delete'] = 'Eliminar';
$gl_caption['c_action'] = 'Acción';
$gl_caption['c_delete_selected'] = 'Borrar seleccionados';
$gl_caption['c_restore_selected'] = 'Recuperar seleccionados';
$gl_caption['c_confirm_deleted'] = 'Los elementos seleccionados se eliminarán definitivamente de la base de datos \\n\\n¿Desea continuar?';
$gl_caption['c_confirm_images_deleted'] = 'Las imágenes seleccionadas se eliminarán definitivamente \\n\\n¿Desea continuar?';
$gl_caption['c_confirm_form_bin'] = 'El registro se enviará a la papelera de reciclaje \\n\\n¿Desea continuar?';
$gl_caption['c_confirm_form_deleted'] = 'El registro se eliminará definitivamente de la base de datos \\n\\n¿Desea continuar?';
$gl_caption['c_move_to'] = 'Mover a:';
$gl_caption['c_print_all'] = 'Imprimir todo';
$gl_caption['c_print'] = 'Imprimir';
$gl_caption['c_order_by'] = 'Ordenar por:';
$gl_caption['c_select'] = 'Seleccionar';
$gl_caption['c_all'] = 'todo';
$gl_caption['c_nothing'] = 'nada';
$gl_caption['c_preview'] = 'Previsualizar';
$gl_caption['c_yes'] = 'Sí';
$gl_caption['c_no'] = 'No';
$gl_caption['c_options_bar'] = 'Opciones';
$gl_caption['c_file_maximum'] = 'máximo %s archivos a la vez';
$gl_caption['c_file_maximum_1'] = 'máximo 1 archivo a la vez';

// buscadors
$gl_caption['c_search_title'] = 'Buscar';
$gl_caption['c_by_ref'] = '(o referencias separadas por espacios, como por ej: "456 245 45")';
$gl_caption['c_by_words'] = 'Por palabras';

// sobreescrits caption images
$gl_caption_image['c_edit'] = 'Imágenes de';
$gl_caption_image['c_insert'] = 'Añadir imágenes desde tu ordenador';
$gl_caption_image['c_delete_selected'] = 'Eliminar imágenes seleccionadas';
$gl_caption_image['c_confirm_deleted'] = 'Las imágenes se eliminarán definitivamente \\n\\n¿Desea continuar?';
$gl_caption_image['c_select_all'] = 'Seleccionar todas';
$gl_caption_image['c_deselect_all'] = 'Deseleccionar todas';

// nous images
$gl_caption_image['c_stop'] = 'Parar';
$gl_caption_image['c_refresh'] = 'Actualizar';
$gl_caption_image['c_add'] = 'Añadir imágenes';
$gl_caption_image['c_thumbnails'] = 'Imágenes';
$gl_caption_image['c_icons'] = 'Iconos';
$gl_caption_image['c_list'] = 'Lista';
$gl_caption_image['c_details'] = 'Detalles';
$gl_caption_image['c_progress_dialog_title_text'] = 'Añadiendo imágenes';

// camps images
$gl_caption_image['c_ordre'] = 'orden';
$gl_caption_image['c_main_image'] = 'principal';
$gl_caption_image['c_name'] = 'Nombre';
$gl_caption_image['c_imagecat_id'] = 'Categoría';
$gl_caption_image['c_image_alt'] = 'Alt';
$gl_caption_image['c_image_title'] = 'Título';

// sobreescrits caption traduccions i traduccions_news.tpl
$gl_caption_translation['c_send_edit'] = 'Enviar a traducir';
$gl_caption_translation['c_page_words_title'] = 'Total de palabras para traducir en esta página';
$gl_caption_translation['c_all_page_words_title'] = 'Total de palabras para traducir en todas las páginas';
$gl_caption_translation['c_total'] = 'Total';

// sobreescrits end


// Message default
$gl_messages['title_edit']='Editar';
$gl_messages['title_copy']='Copiar datos';
$gl_messages['added']='El registro se ha añadido correctamente.';
$gl_messages['add_exists']='Ya existe un registro con este/a  %1$s: %2$s';
$gl_messages['saved']='El registro se ha modificado correctamente';
$gl_messages['not_saved']='El registro no se ha modificado';
$gl_messages['no_records']='No hay ningún registro en esta lista';
$gl_messages['no_records_bin']='No hay ningún registro en la papelera';
$gl_messages['list_saved']='Los cambios en los registros se han efectuado correctamente';
$gl_messages['list_not_saved']='No se ha modificado ningún registro ni detectado ningún cambio';
$gl_messages['list_deleted']='Los registros se han eliminado';
$gl_messages['list_bined']='Los registros se han enviado a la papelera de reciclaje';
$gl_messages['list_restored']='Los registros se han recuperado correctamente';
$gl_messages['concession_no_acces']='No tiene permisos para esta sección';
$gl_messages['concession_no_write']='No tiene permisos de escritura para esta sección';
$gl_messages['select_item']='Haga clic en un registro de la lista para seleccionar';
$gl_messages['related_not_deleted']='Los registros marcados no se han podido borrar, ya que tienen registros relacionados';



// sobreescrits message image
$gl_messages_image['list_saved']='Los cambios en las imágenes se han efectuado correctamente.';
$gl_messages_image['list_not_saved']='No se ha modificado ningún registro ni detectado ningún cambio';
$gl_messages_image['list_deleted']='Las imágenes se han eliminado';
// sobreescrits end



$gl_messages['deleted']='El registro ha sido borrado';
$gl_messages['bined']='El registro  se ha enviado a la papelera de reciclaje';
$gl_messages['restored']='El registre se ha recuperado correctamente';
$gl_caption['c_ok'] = 'Aceptar';
$gl_caption['c_cancel'] = 'Cancelar';
$gl_caption ['c_close']='Cerar';
$gl_messages['post_max_size']='El peso del formulario (contenido + archivos adjuntos) excede el límite de ';
$gl_messages['post_file_max_size']='El peso de los archvios adjuntos excede el límit de ';
$gl_messages['assigned']='Registros seleccionados';
$gl_caption['c_black']='Negro';
$gl_caption['c_white']='Blanco';
$gl_caption['c_silver']='Plata';
$gl_caption['c_grey']='Gis';
$gl_caption['c_blue']='Azul';
$gl_caption['c_red']='Rojo';
$gl_caption['c_orange']='Naranja';
$gl_caption['c_green']='Verde';
$gl_caption['c_purple']='Lila';
$gl_caption['c_pink']='Rosa';
$gl_caption['c_golden']='Dorado';
$gl_caption['c_yellow']='Amarillo';
$gl_caption['c_maroon']='Grana';
$gl_caption['c_brown']='Marrón';

$gl_caption['c_login'] = 'Login';
$gl_caption['c_password'] = 'Contraseña';
$gl_caption['c_mail'] = 'E-mail';
$gl_caption['c_mail_repeat'] = 'Repetir E-mail';
$gl_caption['c_password_repeat'] = 'Repetir contraseña';
$gl_caption['c_noscript'] = '<p><strong>Su navegador no soporta o tiene desactivado Javascript. </strong></p><p>Debe <a href="//www.google.com/support/websearch/bin/answer.py?answer=23852">habilitar Javascript</a> para poder utilizar Letnd</p>';
$gl_caption['c_cookie'] = '<p><strong>Su navegador no soporta o tiene desactivadas las Cookies. </strong></p><p>Debe <a href="//www.google.com/support/websearch/bin/answer.py?answer=35851">habilitar las Cookies</a> para poder utilizar Letnd</p>';

$gl_messages_image['images_uploaded']='Imagenes guardadas';
$gl_messages['form_sent']='El formulario se ha enviado correctamente';
$gl_messages['form_not_sent']='El formulario no se ha podido enviar';

$gl_caption['c_search_adress'] = 'Buscar dirección';
$gl_caption['c_map_frame_1'] = 'Se ha encontrado coincidencia para';
$gl_caption['c_map_frame_2'] = 'Si la ubicación és correcta';
$gl_caption['c_map_frame_3'] = 'clica aquí para guardar';
$gl_caption['c_map_frame_4'] = 'No se ha encontrado ninguna coincidencia';

$gl_caption['c_list_view_1'] = 'Normal';
$gl_caption['c_list_view_2'] = 'Iconos grandes';
$gl_caption['c_list_view_3'] = 'Lista';



// SOCIAL
$gl_caption['c_edit_social'] = 'Xarxes socials';
$gl_caption['c_edit_social_title'] = 'Publicar';
?>