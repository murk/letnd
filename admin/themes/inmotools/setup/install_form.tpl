<!DOCTYPE html>
<html lang="ca">
	<head>
		
		<script type="text/javascript" src="/common/jscripts/jquery/jquery-1.7.2.min.js"></script>
		
		<script>
		
		function submit_form(action){
			$('input[name="action"]').val(action);
			$('#form').submit();
		}
		
		</script>
		
		<style>
			html, body{
				height:100%;
				width:100%;
				margin:0;
			}
			div{
				display:table;
				height:100%;
				width:100%;
			}
			div.row{
				text-align:center;
				display:table-cell;
				vertical-align:middle;
				height:100%;
				width:100%;
			}
			form{
				text-align:left;
				display:inline-block;
				margin:0 auto;
				border:1px solid #ddd;
				padding: 100px;
			}
			h1{
				font-size: 36px;
				margin:0 0 20px;
				font-weight:400;
			}
			p{
				margin: 0;
			}
			a{
				padding: 20px 50px;
				font-size: 26px;
				color: #000;
				text-decoration:none;
				font-weight:300;
				display:block;
			}
				a:hover{
					background-color:#ececec;
				}
		
		</style>

	</head>
	
	<body>
		<div>
			<div class="row">
				<form action="" method="post" id="form">
					<h1>Instalar nou client</h1>
					<p><a href="javascript:submit_form('product')">Botiga</a></p>
					<p><a href="javascript:submit_form('inmo')">Immobiliària</a></p>
					<p><a href="javascript:submit_form('web')">Web corporativa</a></p>
					<input type="hidden" name="action">
				</form>
			</div>
		</div>
	</body>	
</html>