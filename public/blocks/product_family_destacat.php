<?php
/* 28-3-13
 * 
 * Llistat de families destacades
 * Nomes va amb USE_FRIENLY_URL
 *  
 */

$destacat_type = isset($vars['destacat_type'])?$vars['destacat_type']:'all'; // destacat ( nomes destacats, all ( tots, destacats i no destacats, primer es mostren els destacats )
$show_images = isset($vars['show_images'])?$vars['show_images']:false;
$parent_id = isset($vars['parent_id'])?$vars['parent_id']:false;
$block_max_results = isset($vars['block_max_results'])?$vars['block_max_results']:'10';
$add_dots_length = isset($vars['add_dots_length'])?$vars['add_dots_length']:false; // add_dots_length

$destacat_order = $destacat_type=='all'?'':'destacat DESC, ';
$order = " ORDER BY ".$destacat_order."ordre ASC, family ";
$parent_id_query = $parent_id? " AND paren_id = $parent_id" : '';

$query = "SELECT family, product__family.family_id as family_id, family_description, family_alt
				FROM product__family_language, product__family
				WHERE product__family_language.family_id = product__family.family_id
				AND language = '" . LANGUAGE . "'
				AND bin = 0
				$parent_id_query
				" . $order . 
				"LIMIT " . $block_max_results;

$results = Db::get_rows($query);

$selected_found = false;
if ($results)
{	
	$conta = 0;
	$loop_count = count($results);
    foreach($results as $rs)
    {	
		$l = array();
		$l['id'] = $rs['family_id'];
		$l['link'] = Page::get_link('product','product',false,false,array(),false,'',$rs['family_id'],'');
		$l['family_description']=add_dots('family_description',nl2br(htmlspecialchars($rs['family_description'])),$add_dots_length); // per la home nomès
		$l['family_alt']=htmlspecialchars($rs['family_alt']);
		$l['item'] = $rs['family'];
		$l['class'] = '';
		$l['loop_count'] = $loop_count;
		if ($show_images) 
			$l += UploadFiles::get_record_images($rs['family_id'], true, 'product', 'family');
		
		$l['conta'] = $conta++;
		
		$loop[] = $l;
	}
	
	if ($loop){
		$loop[0]['class'] = 'first';	
		$loop[count($loop)-1]['class'] = 'last';
	}

}
else {
	$show_block = false;
	return;
}