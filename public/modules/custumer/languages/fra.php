<?
$gl_caption['c_init_session'] = 'Se connecter';
$gl_caption['c_personal_data'] = 'Données personnelles';
$gl_caption['c_already_custumer'] = 'Si vous êtes déjà client';
$gl_caption['c_register_now'] = 'Sinon, enregistrez-vous maintenant';
$gl_caption['c_title_login'] = 'Connexion';
$gl_caption['c_register'] = 'Enregistrez-vous';
$gl_caption['c_make_reservation'] = 'Réservez';
$gl_caption['c_book_host_main'] = 'Adulte 1';
$gl_caption['c_add_new_host'] = 'Ajouter un nouveau';
$gl_caption['c_edit_host'] = 'Éditer';

$gl_caption['c_custumer_menu_personal_data'] = 'Données personnelles';
$gl_caption['c_custumer_menu_books'] = 'Réservations';
$gl_caption['c_custumer_menu_book'] = 'Réservations';
$gl_caption['c_custumer_menu_logout'] = 'Déconnecter';

$gl_caption_custumer['c_phone1'] = 'Téléphone';
?>