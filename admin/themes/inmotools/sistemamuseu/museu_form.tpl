<script language="JavaScript" src="/admin/jscripts/validacio.js?v=<?=$version?>"></script>
<script language="JavaScript" src="/admin/jscripts/euro_calc.js?v=<?=$version?>"></script>
<script language="JavaScript" src="/common/jscripts/jquery.dropshadow.js?v=<?=$version?>"></script>
<script language="JavaScript" src="/admin/modules/sistemamuseu/jscripts/chained_selects.js?v=<?=$version?>"></script>
<script type="text/javascript">
$(function() {
	set_focus(document.theForm);
});
</script>
<form name="form_delete" method="post" action="<?=$link_action_delete?>" target="save_frame">
    <input name="<?=$id_field?>" type="hidden" value="<?=$id?>">
</form><form name="theForm" method="post" action="<?=$link_action?>" onsubmit="<?=$js_string?>" encType="multipart/form-data" target="save_frame">
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="form"> 
  <tr>
    <td>
        <?if ($form_new):?>
        <div id="form_buttons_holder"><table id ="form_buttons">
          <tr> 
            <td class="buttons_top"><div class="right"><input type="submit" name="Submit" value="<?=$c_send_new?>" class="btn_form1"></div></td>
          </tr>
        </table></div>
        <?endif //form_new?><?if ($form_edit):?>
            <div id="form_buttons_holder"><table id ="form_buttons">
              <tr>
                <td valign="bottom"><div class="menu"><div><?=$c_edit_data?></div><?if ($has_images):?>
            	<a href="<?=$image_link?>"><?=$c_edit_images?></a>
         <?endif //has_images?><a href="?menu_id=<?=$menu_id?>&museu_id=<?=$museu_id?>&action=show_form_map"><?=$c_edit_map?></a></div></td>
                <td class="buttons_top"><div class="right"><input type="submit" name="Submit" value="<?=$c_send_edit?>" class="btn_form1" /><a class="btn_form1" href="<?=$preview_link?>&page_id=16" target="_blank">
                  <?=$c_preview?>
                  </a></div></td>
              </tr>
          </table></div>
          <?endif //form_edit?> 
          	<table width="100%" cellpadding="0" cellspacing="0" >
                <tr> 
                  <td class="formCaptionTitle"><strong>
                  <?=$c_data?>&nbsp;&nbsp;</strong><?=$c_ref?>. <?=$ref?></td>
                </tr>
              </table>  
          	<table width="100%" cellpadding="0" cellspacing="0" >
                <tr> 
                  <td class="formsCaptionHorFirst">
                  <?=$c_general_data?></td>
                </tr>
              </table>                                        
                    <table width="100%" cellpadding="0" cellspacing="0" class="formstbl"><?if ($status):?>
                      <tr>
                        <td class="formsCaption"><strong>
                          <?=$c_status?>:</strong></td>
                        <td class="forms"><?=$status?></td>
                      </tr><?endif //status?>
                      <tr>
                        <td valign="top" class="formsCaption"><strong>
                          <?=$c_museu_title?>
                        <input name="museu_id[<?=$museu_id?>]" type="hidden" value="<?=$museu_id?>" />
                        <input name="museu_id_javascript" type="hidden" value="<?=$museu_id?>" />:</strong></td>
                        <td><table cellpadding="4" cellspacing="0">
                          <?foreach($museu_title as $l): extract ($l)?>
                          <tr>
                            <td class="forms"><?=$lang?></td>
                            <td><?=$museu_title?></td>
                          </tr>
                          <?endforeach //$museu_title?>
                        </table></td>
                      </tr>
                      <tr>
                        <td valign="top" class="formsCaption"><strong>
                          <?=$c_museu_subtitle?>:</strong></td>
                        <td><table cellpadding="4" cellspacing="0">
                          <?foreach($museu_subtitle as $l): extract ($l)?>
                          <tr>
                            <td class="forms"><?=$lang?></td>
                            <td><?=$museu_subtitle?></td>
                          </tr>
                          <?endforeach //$museu_title?>
                        </table></td>
                      </tr>
                      <tr>
                        <td valign="top" class="formsCaption"><strong>
                          <?=$c_url1_name?>:</strong></td>
                        <td class="forms"><table cellpadding="4" cellspacing="0">
                          <?foreach($url1_name as $l): extract ($l)?>
                          <tr>
                            <td class="forms"><?=$lang?></td>
                            <td><?=$url1_name?></td>
                          </tr>
                          <?endforeach //$museu_title?>
                        </table></td>
                      </tr>
                      <tr>
                        <td valign="top" class="formsCaption"><strong>
                          <?=$c_url1?>:</strong></td>
                        <td class="forms"><table border="0" cellspacing="0" cellpadding="0">
                        <tr>
                              <td><?=$c_url1?></td>
                              <td><?=$c_url2?></td>
                            </tr>
                            <tr>
                              <td><?=$url1?>
                                &nbsp;&nbsp;</td>
                              <td><?=$url2?>
                                &nbsp;&nbsp;</td>
                            </tr>
                        </table></td>
                      </tr>
                      <tr>
                        <td class="formsCaption"><strong>
                          <?=$c_visita?>:</strong></td>
                        <td class="forms"><?=$visita?></td>
                      </tr>
                      <tr>
								<td valign="top" class="formsCaption"><strong>
                          <?=$c_videoframe?>:</strong></td>
                        <td class="forms"><?=$videoframe;?></td>
                      </tr>
                      <tr>
                        <td class="formsCaptionHor2"><?=$c_adress_plural?>:</td>
                        <td height="1" class="formsCaptionHor2"><img src="/admin/themes/inmotools/images/spacer.gif" width="1" height="1" /></td>
                      </tr>
                      <tr>
                        <td valign="top" class="formsCaption"><strong>
                          <?=$c_adress?>:</strong></td>
                        <td class="forms"><?=$adress?>
                          <div style="height:7px"></div>
                          <table border="0" cellspacing="0" cellpadding="0">
                            <tr>
                              <td><?=$c_provincia_id?></td>
                              <td><?=$c_comarca_id?></td>
                              <td><?=$c_municipi_id;?></td>
                              <td><?=$c_cp?></td>
                            </tr>
                            <tr>
                              <td><?=$provincia_id?>
                              &nbsp;&nbsp;</td>
                              <td><?=$comarca_id?>
                                &nbsp;&nbsp;</td>
                              <td><?=$municipi_id?>
                                &nbsp;&nbsp;</td>
                              <td><?=$cp?></td>
                            </tr>
                          </table>
                          <input name="old_comarca_id[<?=$museu_id?>]" type="hidden" value="0" />
                          <input name="old_municipi_id[<?=$museu_id?>]" type="hidden" value="0" /></td>
                      </tr>
                      <tr>
                        <td class="formsCaption"><strong>
                          <?=$c_phone?>:</strong></td>
                        <td class="forms">&nbsp;
                          <table border="0" cellspacing="0" cellpadding="0">
                            <tr>
                              <td><?=$c_phone?></td>
                              <td><?=$c_phone2?></td>
                              <td><?=$c_phone3?></td>
                              <td><?=$c_fax?></td>
                            </tr>
                            <tr>
                              <td><?=$phone?>
                                &nbsp;&nbsp;</td>
                              <td><?=$phone2?>
                                &nbsp;&nbsp;</td>
                              <td><?=$phone3?>
                                &nbsp;&nbsp;</td>
                              <td><?=$fax?>
                                &nbsp;&nbsp;</td>
                            </tr>
                          </table></td>
                      </tr>
                      <tr>
                        <td class="formsCaption"><strong><strong>
                          <?=$c_mail?>
                        </strong></strong>:</td>
                        <td class="forms"><table border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td><?=$c_mail?></td>
                            <td><?=$c_mail2?></td>
                          </tr>
                          <tr>
                            <td><?=$mail?>                              &nbsp;&nbsp;</td>
                            <td><?=$mail2?>                              &nbsp;&nbsp;</td>
                          </tr>
                        </table></td>
                      </tr>
                      <tr><!--
                        <td valign="top" class="formsCaption"><strong><strong>
                          <?=$c_file?>:</strong></strong></td>
                        <td class="forms"><?=$file?></td>
                      </tr>-->
                      <tr>
                        <td class="formsCaptionHor2"><span class="formsCaptionHor">
                          <?=$c_schedule_plural?>:
                        </span></td>
                        <td height="1" class="formsCaptionHor2"><img src="/admin/themes/inmotools/images/spacer.gif" width="1" height="1" /></td>
                      </tr>
                      <tr>
                        <td valign="top" class="formsCaption"><strong>
                          <?=$c_schedule?>:</strong></td>
                        <td><table cellpadding="4" cellspacing="0">
                          <?foreach($schedule as $l): extract ($l)?>
                          <tr>
                            <td><?=$lang?>
                              <br />
                              <?=$schedule?></td>
                          </tr>
                          <?endforeach //$schedule?>
                        </table></td>
                      </tr>
					</table>
                    <table width="100%" cellpadding="0" cellspacing="0" class="formstbl">
                      <tr>
                        <td class="formsCaptionHor2"><?=$c_descriptions?></td>
                      </tr>
                      <?foreach($museu_description as $l): extract ($l)?>
                      <tr>
                        <td class="formsHor"><?=$lang?>
                          <br />
                          <?=$museu_description?></td>
                      </tr>
                      <?endforeach //$description?>
                    </table><br />
                    <table width="100%" cellpadding="0" cellspacing="0" class="formstbl">
                      <tr>
                        <td class="formsCaptionHor2">Imatges fixes del museu</td>
                      </tr>
                      <tr>
                        <td class="formsHor">
                          <br /><table border="0" cellspacing="5" cellpadding="0">
	<tr><td valign="top"><strong>Fitxa</strong></td><td valign="top"><strong>Logo</strong></td></tr><tr>
		<td valign="top"><img src="/clients/sistemamuseus/sistemamuseu/<?=$ref?>_fitxa.jpg" width="200" align="top" style="padding-right:20px;"></td>
		<td valign="top"><img src="/clients/sistemamuseus/sistemamuseu/<?=$ref?>_logo.gif" width="75" align="top" style="padding-right:20px;"></td>
		</tr>
</table><table border="0" cellspacing="5" cellpadding="0">
	<tr><td valign="top"><strong>Pàgina inici</strong></td></tr><tr>
		<td valign="top"><img src="/clients/sistemamuseus/sistemamuseu/<?=$ref?>_home.jpg" width="731" align="top"></td>
	</tr>
</table>

                          </td>
                      </tr>
                    </table>
<?if ($form_new):?>
              <table width="100%" cellpadding="4" cellspacing="2" class="formsButtonstbl">
                  <tr>
                    <td align="center" class="formsButtons"><input type="submit" name="Submit" value="<?=$c_send_new?>" class="btn_form1"> 
                  </td>
                </tr>
              </table>
              <?endif //form_new?><?if ($form_edit):?>
              <table width="100%" cellpadding="4" cellspacing="2" class="formsButtonstbl">
                  <tr>
                    <td align="center" class="formsButtons"><div class="centerFloat1">
                        <div class="centerFloat2"><input type="submit" name="Submit" value="<?=$c_send_edit?>" class="btn_form1"><a class="btn_form1" href="<?=$preview_link?>" target="_blank">
                <?=$c_preview?>
                </a></div></div></td>
                </tr>
              </table>
              <?endif //form_edit?></td>
  </tr>
</table></form>
