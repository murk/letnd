<?php
/* 4-9-12 */
// Actualitzacio dels caption de les imatges, afegeixo title a imatges


global $gl_languages;
$tables = Db::get_rows('SHOW TABLES');

	foreach($tables as $row){
		$table = current($row);
		if (substr($table,-6)=='_image'){
			$update_sql[]="
			ALTER TABLE `".$table."_language`
				ADD COLUMN `image_title` VARCHAR(255) NOT NULL AFTER `image_alt`;			
			";		
			
		}
	}		
?>