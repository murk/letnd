
<!-- IGUAL LIST.TPL excepte 'is_pdf' -->
<?if (!$results):?>
	<?if(!$is_pdf):?>
	<?if ($options_bar):?>
	<div id='opcions_holder'><table width="100%" border="0" cellspacing="0" cellpadding="0" id="opcions">
		<tr>
		  <td class="opcions"><div id="opcions_fixed_bar"><div class="more"></div></div><table class="filters">
			<tr valign="middle">
			<?foreach($options_filters as $options_filter):?><form>
			  <td><?=$options_filter?></td>
			  </form>
			  <?endforeach //$options_filters?>
			  </tr></table><?if ($options_filters):?><div class="sep"></div><?endif //options_filters?>
			  <table border="0" cellpadding="2" cellspacing="0"><tr valign="middle">
			  <?if ($options_buttons):?>
			  <td class="buttons">
			  <?foreach($options_buttons as $options_button):?><?=$options_button?><?endforeach //$options_button?>
			  </td><?endif //options_buttons?>
			</tr>
		  </table></td>
		</tr>
	</table>
	</div>
	<?endif //$options_bar?>
	<?endif //$is_pdf?>
<?endif //!$results?>
<?if ($results):?>
	<?if(!$is_pdf):?>	
	<?if ($split_count):?>
<div id='split_holder'><table width="100%" border="0" cellpadding="0" cellspacing="0" id="split">
  <tr valign="middle"> 
    <td width="20%" align="left" nowrap class="pageResults"><?=$split_results_count?></td>
    <td align="right" nowrap class="pageResults"><?=$split_results?></td>
  </tr>
</table>
</div>
<?endif //$split_count?>
<?if ($options_bar):?>
<div id='opcions_holder'><table width="100%" border="0" cellspacing="0" cellpadding="0" id="opcions">
    <tr>
      <td class="opcionsTitol"><strong><?=$c_options_bar?></strong></td>
    </tr>
    <tr>
      <td class="opcions"><div id="opcions_fixed_bar"><div class="more"></div></div><table class="filters">
        <tr valign="middle">
		<?foreach($options_move_tos as $options_move_to):?>
          <td><?=$options_move_to?></td>
		  <?endforeach //$options_move_tos?>
		<?foreach($options_filters as $options_filter):?><form>
          <td><?=$options_filter?></td>
          </form>
		  <?endforeach //$options_filters?>
          </tr></table><?if ($options_filters):?><div class="sep"></div><?endif //options_filters?>
          <table border="0" cellpadding="2" cellspacing="0"><tr valign="middle">
		  <?if ($options_buttons):?>
		  <td class="buttons">
		  <?foreach($options_buttons as $options_button):?><?=$options_button?><?endforeach //$options_button?>
		  </td><?endif //options_buttons?>
		<?if ($save_button):?>
          <td><strong><a class="botoOpcionsGuardar" href="javascript:submit_form_save('save_rows_save_records<?=$action_add?>')">
            <?=$c_send_edit?>
          </a></strong></td>
		  <?endif //save_button?><?if ($select_button):?>
          <td><?=$c_select?>
            &nbsp;<strong><a class="opcions" href="javascript:select_all(true, document.list)">
              <?=$c_all?>
              </a>&nbsp;-&nbsp;<a class="opcions" href="javascript:select_all(false, document.list)">
                <?=$c_nothing?>
              </a></strong></td>
		  <?endif //select_button?><?if ($delete_button):?>
          <?if ($has_bin):?>
          <?if ($is_bin):?>
          <td><strong><a class="botoOpcionsBorrar" href="javascript:submit_list('save_rows_delete_selected<?=$action_add?>', '<?=$c_confirm_deleted?>', true)">
            <?=$c_delete_selected?>
          </a></strong></td>
          <td><strong><a class="botoOpcionsGuardar" href="javascript:submit_list('save_rows_restore_selected<?=$action_add?>')">
            <?=$c_restore_selected?>
          </a></strong></td>
          <?else:?>
          <td><strong><a class="botoOpcionsBorrar" href="javascript:submit_list('save_rows_bin_selected<?=$action_add?>')">
            <?=$c_delete_selected?>
          </a></strong></td>
          <?endif // is_bin?>
          <?else:?>
          <td><strong><a class="botoOpcionsBorrar" href="javascript:submit_list('save_rows_delete_selected<?=$action_add?>', '<?=$c_confirm_deleted?>', true)">
            <?=$c_delete_selected?>
          </a></strong></td>
          <?/*<td>
              <td>&nbsp;</td><strong><a class="botoOpcions" href="/admin?menu_id=<?=$menu_id?>&sort_by=0&sort_order=<?=$sort_order0?>">
                <?=$c_move_to?>
               
            <input type="submit" name="bin_selected" value="<?=$c_delete_selected?>" class="boto1">
              </a></strong></td>*/?>
          <?endif // has_bin?><?endif //delete_button?>
		  <?if ($print_button):?>
			  <td><strong><a class="botoOpcions" target="_blank" href="/admin/?action=get_pdf&tool_section=seguimentitem&seguiment_id=<?=$seguiment_id?>">
            <?=$c_print_all?>
          </a></strong></td>
		  <?endif //delete_button?>
        </tr>
      </table></td>
    </tr>
</table></div>
<!--<div class="sep"></div>-->
<?endif //$options_bar?>
  <form method="post" action="<?=$form_link?>" name="list" onsubmit="return check_selected(this)" target="save_frame">

<!-- FI IGUAL LIST.TPL -->
<?endif //is_pdf?>
<input name="action" type="hidden" value="save_rows"><?=$out_loop_hidden?>


<link rel="stylesheet" type="text/css" href="themes/inmotools/pv/styles.css" media="all" />
<div id="pv" class="<?=$pdf?>">
	<table>
	    <thead>
			<tr>
				<td colspan="<?=$colspan?>">
					<table class="pv-head">
						<tr>
							<td class="pv-head-logo"><img src="themes/inmotools/pv/logo_<?= PvCommon::get_consultor() ?>.png" width="<?= SEGUIMENT_LOGO_WIDTH ?>"/></td>
							<td class="pv-head-titol" style="background-color:<?= CORPORATE_COLOR ?>;">
								<div>
									<strong>Seguiment reglamentari</strong>
									Repertori d’evolucions de <?=$client?>
								</div>
							</td>
							<td class="pv-head-codi">
								<div><strong><?=$codi_seguiment?></strong>
									Ed.:1</div>
							</td>
							<td class="pv-head-logo last"><? if ( $image_name_details ): ?><img <?= get_image_attrs( "/clients/" . PvCommon::get_consultor() . "/pv/client/images/" . $image_name_details, '', '120', '53', false ) ?> /><? endif //image?></td>
						</tr>
					</table>	
				</td>
			</tr>
			<tr class="pv-cols-head-sep">
				<td colspan="<?=$colspan?>"></td>
			</tr>
			<tr class="pv-cols-head">
				<td class="first"><?=$c_codi_lloc?></td>
				<td><?=$c_titol_norma?></td>
				<td class="pv-small"><?=$c_classificacio?></td>
				<td><?=$c_requisit?></td>
				<td class="pv-small"><?=$c_evaluacio?></td>
				<td class="pv-small"><?=$c_tancament?></td>
				<td class="pv-small"><?=$c_comentari?></td>
				<td class="pv-small last"><?=$c_Evidencia?></td>
				<td class="pv-small last"><?=$c_arxiu?></td>
				<?if($is_editable):?>
				<td class="pv-small last"><?=$c_is_actiu?></td>
				<?endif //is_editable?>
			</tr>
	    </thead>
		<tfoot>
			<tr class="pv-foot-sep">
				<td colspan="<?=$colspan?>"></td>
			</tr>
			<tr class="pv-foot">
				<td colspan="<?=$colspan?>">C/ Llibertat, 159 2-C  17820  Banyoles               Tel: 972 58 33 38  Fax: 972 58 33 39               www.consultoriapv.com</td>
			</tr>
		</tfoot>
	    <tbody>
		<?foreach($loop as $l): extract ($l)?>
			<?if($group_fields):?>
			<tr class="pv-categoria">
				<td colspan="<?=$colspan?>"><?=$categoria?></td>
			</tr>
			<?endif //group_fields?>
			<tr>
				<?if($codi_lloc):?>
				<td class="pv-codi"><div><?=$codi_lloc?></div></td>
				<?else: //codilloc?>
				<td class="pv-evolucio">
					<strong>Evolució <?=$evolucio?></strong>
					<br />
					<?=$entered?>
				</td>
				<?endif //codi_lloc?>
				
				<td class="pv-norma">
					<?=$titol_norma?>
				</td>
				
				<td class="pv-classificacio">
					<?foreach($requisits as $r): extract ($r)?>
						<div>
							<?=$classificacio?><input name="seguimentitem_id[<?=$seguimentitem_id?>]" type="hidden" value="<?=$seguimentitem_id?>" />
						</div>
					<?endforeach //$loop?>
				</td>
				<td class="pv-requisit">
					
					<table class="table-requisit">
					<?foreach($requisits as $r): extract ($r)?>
						<tr>
							<th><?=$num_requisit?></th><td><?=$requisit?></td>
						</tr>
					<?endforeach //$loop?>
					</table>
					
				</td>
				<td class="pv-evaluacio">
					
					<?foreach($requisits as $r): extract ($r)?>
						<div class="<?=$evaluacio_class?>">
							<?=$evaluacio?>
						</div>
					<?endforeach //$loop?>
					
				</td>
				<td class="pv-tancament">
					
					<?foreach($requisits as $r): extract ($r)?>
						<div>
							<?=$tancament?>
						</div>
					<?endforeach //$loop?>
					
				</td>
				<td>
					
					<?foreach($requisits as $r): extract ($r)?>
						<div>
							<?=$comentari?>
						</div>
					<?endforeach //$loop?>
					
				</td>
				<td>
					
					<?foreach($requisits as $r): extract ($r)?>
						<div>
							<a href="" title=""><?= $Evidencia ?></a>
						</div>
					<?endforeach //$loop?>
					
				</td>
				<td>

					<?foreach($requisits as $r): extract ($r)?>
						<div>
							<a href="" title="">Arxiu</a>
						</div>
					<?endforeach //$loop?>

				</td>
				<?if($is_editable):?>
				<td class="pv-small">
					
					<?foreach($requisits as $r): extract ($r)?>
						<div>
							<?=$is_actiu?>
						</div>
					<?endforeach //$loop?>
				
				</td>
				<?endif //is_editable?>
			</tr>
			<? endforeach //loop?>
		</tbody>
	</table>
</div>	
		
		
<?if(!$is_pdf):?>
	
<!-- IGUAL LIST.TPL -->

  </form>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td height="10"></td>
  </tr>
</table> <?if ($split_count):?>
  <table width="100%" border="0" cellpadding="0" cellspacing="0">
    <tr valign="middle">
      <td width="20%" align="left" nowrap class="pageResults"><?=$split_results_count?></td>
      <td align="right" nowrap class="pageResults"><?=$split_results?></td>
    </tr>
  </table><?endif //$split_count?>
<?endif //is_pdf?>
<?endif //results?>
<!-- FI IGUAL LIST.TPL -->