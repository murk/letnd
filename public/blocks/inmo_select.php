<?php

// per fer filtres de qualsevol camp de inmo__property $field<>0 <--------- de moment nomes numeric

$field = isset($vars['field'])?$vars['field']:'room';
$table = isset($vars['table'])?$vars['table']:'inmo__property';
$tipus = isset($vars['tipus'])?$vars['tipus']:false;
$page_id = isset($vars['page_id']) ? $vars['page_id'] : false;

include_once('inmo_block_functions.php');
$condition = '';
$condition .= inmo_block_get_tipus($tipus);


$vars['var_name'] = $field;

$query = "SELECT Distinct $field
				FROM $table
				WHERE $field<>0
				AND bin = 0 AND (status = 'onsale' OR status = 'sold' OR status = 'reserved') " . $condition . "
				ORDER BY $field";

$results = Db::get_rows($query);


$field_value = R::get($field);
$vars['block_selected'] = $field_value?1:0;

if ($results) {
    foreach($results as $rs) {
        ($field_value == $rs[$field])?$selected = 1:$selected = 0;

		// pel filtre
		if ( isset( $_GET[$field . 's'] ) &&
		     in_array( $rs[$field], $_GET[$field . 's'] )
		) {
			$selected = 1;
			$vars['block_selected'] = 1;
		}
		
		$l['link'] = get_link(array(
				'tool' => 'inmo', 
				'tool_section' => 'property', 
				'friendly_params' => array('tipus'=>$tipus==INMO_DEFAULT_TIPUS?'':$tipus,$field=>$rs[$field]),
				'link_vars' => array('tipus'=>$tipus),
				'detect_page' => $page_id?false:true,
				'page_id' => $page_id
			));
		
        $l['selected'] = $selected;
        $l['value'] = $rs[$field];
        $l['item'] = $rs[$field];
		$l['class'] = '';
        $loop2[] = $l;
    }
	
	
$loop2[0]['class'] = 'first';	
$loop2[count($loop2)-1]['class'] = 'last';
	
$loop[] = array ('loop' => $loop2,
    'item' => '');
}


?>