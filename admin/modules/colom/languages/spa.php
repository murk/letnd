<?php
// menus eina
define('COLOM_MENU_COLOM','Edición de páginas'); 
define('COLOM_MENU_COLOM_LIST','Página principal'); 
define('COLOM_MENU_NEW', 'Programas');
define('COLOM_MENU_NEW_NEW', 'Entrar nuevo programa');
define('COLOM_MENU_NEW_LIST', 'Listar i editar programas');
define('COLOM_MENU_NEW_BIN', 'Papelera de reciclaje');

$gl_caption['c_actualitat']='Actualidad';
$gl_caption['c_biografia']='Biografía català';
$gl_caption['c_biografia_spa']='Biografía castellano';
$gl_caption['c_biografia_eng']='Biografía inglés';
$gl_caption['c_biografia_fra']='Biografía francés';
$gl_caption['c_biografia_deu']='Biografía alemán';
$gl_caption['c_adressa']='Dirección';
$gl_caption['c_autor']='Autores';
$gl_caption['c_obra']='Obras';
$gl_caption['c_cambra']='Música de cámara';
$gl_caption['c_concert']='Conciertos';
$gl_caption['c_docencia']='Docencia';
$gl_caption['c_premsa']='Prensa';
$gl_caption['c_group_1']='Actualidad';
$gl_caption['c_group_2']='Biografía';


$gl_caption['c_config_title'] = 'Página principal';



// NOTICIES
$gl_caption_new['c_prepare'] = 'En preparación';
$gl_caption_new['c_review'] = 'Para revisar';
$gl_caption_new['c_public'] = 'Pública';
$gl_caption_new['c_archived'] = 'Archivada';
$gl_caption_new['c_new'] = 'Programa';
$gl_caption_new['c_title'] = 'Título';
$gl_caption_new['c_subtitle'] = 'Subtítulo';
$gl_caption_new['c_content'] = 'Contenido';
$gl_caption_new['c_entered'] = 'Fecha';
$gl_caption_new['c_expired'] = 'Fecha caducidad';
$gl_caption_new['c_modified'] = 'Fecha modificación';
$gl_caption_new['c_status'] = 'Estado';
$gl_caption_new['c_category_id'] = 'Categoría';
$gl_caption_new['c_filter_category_id'] = 'Todas las categorías';
$gl_caption_new['c_filter_status'] = 'Todos los estados';
$gl_caption_new['c_filter_in_home'] = 'Todas en Home';
$gl_caption_new['c_filter_destacat'] = 'Todas las destacadas';
$gl_caption_new['c_ordre'] = 'Orden';

$gl_caption_category['c_category'] = 'Categoría';
$gl_caption_category['c_new_button'] = 'Ver noticias';
$gl_caption_category['c_ordre'] = 'Orden';
$gl_caption_image['c_name'] = 'Nombre';
$gl_caption_new['c_url1_name']='Nombre URL(1)';
$gl_caption_new['c_url1']='URL(1)';
$gl_caption_new['c_url2_name']='Nombre URL(2)';
$gl_caption_new['c_url2']='URL(2)';
$gl_caption_new['c_url3_name']='Nombre URL(3)';
$gl_caption_new['c_url3']='URL(3)';
$gl_caption_new['c_videoframe']='Vídeo (youtube, metacafe...)';
$gl_caption_new['c_file']='Archivos';
$gl_caption_new['c_in_home']='Sale en la home';
$gl_caption_new['c_in_home_0']='No sale en la Home';
$gl_caption_new['c_in_home_1']='Sale en la home';
$gl_caption_new['c_content_list']='Descripción corta en el listado';
$gl_caption_new['c_destacat']='Noticia destacada';
$gl_caption_new['c_destacat_0']='No destacada';
$gl_caption_new['c_destacat_1']='Destacada';

$gl_caption['c_new_file_name'] = $gl_caption['c_file_name'];
$gl_caption['c_new_old_file_name'] = $gl_caption['c_old_file_name'];

$gl_caption_new['c_selected']='Noticias seleccionadas';
?>