var NONE_SELECTED = "No hay ninguna fila seleccionada";

var VALIDAR_TEXT1 = ' es obligatorio';
var VALIDAR_TEXT2 = 'Carácteres no válidos';
var VALIDAR_TEXT3 = ' no es una dirección de E-Mail correcta';
var VALIDAR_TEXT4 = ' tiene que tener un mínimo de 3 carácteres y @';
var VALIDAR_TEXT5 = ' tiene que ser un número';
var VALIDAR_TEXT6 = ' tiene que ser un número de 5 dígitos';
var VALIDAR_TEXT7 = ' contiene ';
var VALIDAR_TEXT8 = ' carácteres, no puede ser más largo de ';
var VALIDAR_TEXT9 = ' Falta poner el año o no es numérico';
var VALIDAR_TEXT10 = ' El año no es correcto';
var VALIDAR_TEXT11 = ' La fecha no es correcta';
var VALIDAR_TEXT12 = ' no es correcto, escribe 8 dígitos y la letra';
var VALIDAR_TEXT13 = ' no es correcto';
var VALIDAR_TEXT14 = ' la lletra no es correcta';
var VALIDAR_TEXT15 = ' no es correcto';
var VALIDAR_TEXT16 = 'Debe seleccionar ';
var VALIDAR_TEXT17 = 'No ha rellenado correctamente el formulario:';
var VALIDAR_TEXT18 = ' Debe escoger al menos una opción';
var VALIDAR_TEXT19 = ' Debes escoger al menos una dirección o grupo';
var VALIDAR_TEXT20 = ' El núnero de la tarjeta de crédito es incorrecto';
var VALIDAR_TEXT21 = ' La fecha de caducidad de la tarjeta de crédito es incorrecta';
var VALIDAR_TEXT22 = ' no es correcto';
var VALIDAR_TEXT23 = ' no es correcto';
var VALIDAR_TEXT24 = ' tiene que ser un número';
var VALIDAR_TEXT25 = ' no coinciden las contraseñas';
var VALIDAR_TEXT26 = ' no coincide la confirmación';
var VALIDAR_TEXT27 = ' sólo puede contener números, carácteres en minúsculas de la "a a la z" o guiones ("_" o "-")';
var VALIDAR_TEXT28 = 'Debe aceptar la política de privacidad';
var MODALBOX_SAVE = ' Enviando datos ...';
var MODALBOX_CLOSE = 'cerrar';
var MULTIFILE_ACCEPT = ' Sólo se pueden subir archivos del tipo: ';
var MULTIFILE_NOT_ACCEPT = ' No se pueden subir archivos del tipo: ';
var CONFIRM_TEXT = 'Seguro que deseas realitzar esta acción?';
var WAIT_TEXT = 'Un momento por favor ...';

var MONTH_NAMES = new Array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");

var FILE_DELETE = "Borrar";
// NO TRADUIT
var IMMO_MAP_POINT_SELECTED = "Haz un clic en un punto del mapa para establecer la situación";
var PRODUCT_CART_ADDED = "Se ha añadido el producto a su cesta";