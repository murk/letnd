<?
/*
22-05-2019
Habitaclia

*/


if ( is_table( 'inmo__property_to_portal' ) && ! is_field( 'inmo__property_to_portal', 'habitaclia' ) ) {

	$update_sql[] = "ALTER TABLE `inmo__property_to_portal`
	ADD COLUMN `habitaclia` TINYINT(1) NOT NULL DEFAULT '0' AFTER `ceigrup`;";

}
else {
	if ( is_table( 'inmo__property_to_portal' ) ) {
		trigger_error( 'Ja existeixen el camp habitaclia' );
	}
}

if ( is_table( 'inmo__configadmin' ) && ! Db::get_first( "SELECT * FROM inmo__configadmin WHERE name='habitaclia_export_all'" ) ) {
	$update_sql[] = "
		INSERT INTO inmo__configadmin (`name`, `value`) VALUES ('habitaclia_export_all', '0');";

}
else {
	if ( is_table( 'inmo__configadmin' ) ) {
		trigger_error( 'Ja existeixen el camp habitaclia_export_all' );
	}
}
