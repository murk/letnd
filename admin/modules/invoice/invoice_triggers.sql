

DROP TRIGGER IF EXISTS `z_expense_insert`;
DROP TRIGGER IF EXISTS `z_expense_update`;
DROP TRIGGER IF EXISTS `z_recurringconcept_insert`;
DROP TRIGGER IF EXISTS `z_recurringconcept_delete`;
DROP TRIGGER IF EXISTS `z_recurringconcept_update`;
DROP TRIGGER IF EXISTS `z_recurring_update`;
DROP TRIGGER IF EXISTS `z_recurring_insert`;
DROP TRIGGER IF EXISTS `z_invoice_update`;
DROP TRIGGER IF EXISTS `z_invoiceconcept_delete`;
DROP TRIGGER IF EXISTS `z_invoiceconcept_insert`;
DROP TRIGGER IF EXISTS `z_invoice_insert`;
DROP TRIGGER IF EXISTS `z_invoiceconcept_update`;
DROP PROCEDURE IF EXISTS `z_recurringconcept_on_change`;
DROP PROCEDURE IF EXISTS `z_invoiceconcept_on_change`;
DROP PROCEDURE IF EXISTS `z_expense_on_change`;



## INSERT
DELIMITER //
CREATE TRIGGER z_invoiceconcept_insert
AFTER INSERT ON `invoice__invoiceconcept`
FOR EACH ROW

	BEGIN
		CALL z_invoiceconcept_on_change(NEW.invoice_id, NEW.quantity, 0, NEW.price, 0, NEW.cost_price, 0);
	END//
DELIMITER ;

## UPDATE
DELIMITER //
CREATE TRIGGER z_invoiceconcept_update
AFTER UPDATE ON `invoice__invoiceconcept`
FOR EACH ROW
	BEGIN
		CALL z_invoiceconcept_on_change(NEW.invoice_id, NEW.quantity, OLD.quantity, NEW.price, OLD.price,
		                                NEW.cost_price, OLD.cost_price);
	END//
DELIMITER ;

## DELETE
DELIMITER //
CREATE TRIGGER z_invoiceconcept_delete
AFTER DELETE ON `invoice__invoiceconcept`
FOR EACH ROW
	BEGIN
		CALL z_invoiceconcept_on_change(OLD.invoice_id, 0, OLD.quantity, 0, OLD.price, 0, OLD.cost_price);
	END//
DELIMITER ;

## CHANGE
DELIMITER //

CREATE PROCEDURE z_invoiceconcept_on_change(
	IN id             INT(11),
	IN quantity       INT(11),
	IN old_quantity   INT(11),
	IN price          INT(11),
	IN old_price      INT(11),
	IN cost_price     INT(11),
	IN old_cost_price INT(11))
READS SQL DATA
	BEGIN

		UPDATE invoice__invoice
		SET total_price_no_vat      = total_price_no_vat + (quantity * price) - (old_quantity * old_price),
			total_cost_price_no_vat = total_cost_price_no_vat + (quantity * cost_price) -
			                          (old_quantity * old_cost_price)
		WHERE invoice_id = id;

		UPDATE invoice__invoice
		SET total_vat = total_price_no_vat * 0.21
		WHERE invoice_id = id;

		UPDATE invoice__invoice
		SET total_price = total_price_no_vat + total_vat,
			modified    = NOW()
		WHERE invoice_id = id;

	END//
DELIMITER ;


###############################################
## Modified
###############################################

## INSERT
DELIMITER //
CREATE TRIGGER z_invoice_insert
BEFORE INSERT ON `invoice__invoice`
FOR EACH ROW
	BEGIN
		SET NEW.modified = NOW();
	END//
DELIMITER ;

## UPDATE
DELIMITER //
CREATE TRIGGER z_invoice_update
BEFORE UPDATE ON `invoice__invoice`
FOR EACH ROW
	BEGIN
		SET NEW.modified = NOW();
	END//
DELIMITER ;


##############################################
###  RECURRING
##############################################


DELIMITER //
CREATE PROCEDURE `z_recurringconcept_on_change`(
	IN `id`             INT(11),
	IN `quantity`       DECIMAL(6, 2),
	IN `old_quantity`   DECIMAL(6, 2),
	IN `price`          DECIMAL(11, 2),
	IN `old_price`      DECIMAL(11, 2),
	IN `cost_price`     DECIMAL(11, 2),
	IN `old_cost_price` DECIMAL(11, 2)


)
READS SQL DATA
	BEGIN

		UPDATE invoice__recurring
		SET total_price_no_vat      = total_price_no_vat + (quantity * price) - (old_quantity * old_price),
			total_cost_price_no_vat = total_cost_price_no_vat + (quantity * cost_price) -
			                          (old_quantity * old_cost_price)
		WHERE recurring_id = id;

		UPDATE invoice__recurring
		SET total_vat = total_price_no_vat * 0.21
		WHERE recurring_id = id;

		UPDATE invoice__recurring
		SET total_price = total_price_no_vat + total_vat,
			modified    = NOW()
		WHERE recurring_id = id;

	END//
DELIMITER ;


DELIMITER //
CREATE TRIGGER `z_recurringconcept_delete`
AFTER DELETE ON `invoice__recurringconcept`
FOR EACH ROW
	BEGIN
		CALL z_recurringconcept_on_change(OLD.recurring_id, 0, OLD.quantity, 0, OLD.price, 0, OLD.cost_price);
	END//
DELIMITER ;


DELIMITER //
CREATE TRIGGER `z_recurringconcept_insert`
AFTER INSERT ON `invoice__recurringconcept`
FOR EACH ROW
	BEGIN
		CALL z_recurringconcept_on_change(NEW.recurring_id, NEW.quantity, 0, NEW.price, 0, NEW.cost_price, 0);
	END//
DELIMITER ;


DELIMITER //
CREATE TRIGGER `z_recurringconcept_update`
AFTER UPDATE ON `invoice__recurringconcept`
FOR EACH ROW
	BEGIN
		CALL z_recurringconcept_on_change(NEW.recurring_id, NEW.quantity, OLD.quantity, NEW.price, OLD.price,
		                                  NEW.cost_price, OLD.cost_price);
	END//
DELIMITER ;


DELIMITER //
CREATE TRIGGER `z_recurring_insert`
BEFORE INSERT ON `invoice__recurring`
FOR EACH ROW
	BEGIN
		SET NEW.modified = NOW();
	END//
DELIMITER ;


DELIMITER //
CREATE TRIGGER `z_recurring_update`
BEFORE UPDATE ON `invoice__recurring`
FOR EACH ROW
	BEGIN
		SET NEW.modified = NOW();
	END//
DELIMITER ;


##############################################
###  EXPENSE
##############################################

DELIMITER //
CREATE TRIGGER `z_expense_insert`
BEFORE INSERT ON `invoice__expense`
FOR EACH ROW
	BEGIN
		CALL z_expense_on_change(NEW.expense_id, NEW.price, NEW.vat_included, NEW.vat_exempted, NEW.total_price_no_vat, NEW.total_vat, NEW.total_price, NEW.irpf_percent, NEW.total_irpf);
	END//
DELIMITER ;


DELIMITER //
CREATE TRIGGER `z_expense_update`
BEFORE UPDATE ON `invoice__expense`
FOR EACH ROW
	BEGIN
		CALL z_expense_on_change(NEW.expense_id, NEW.price, NEW.vat_included, NEW.vat_exempted, NEW.total_price_no_vat, NEW.total_vat, NEW.total_price, NEW.irpf_percent, NEW.total_irpf);
	END//
DELIMITER ;


DELIMITER //
CREATE PROCEDURE `z_expense_on_change`(
	IN `id`   INT,
	IN `price`        DECIMAL(11, 2),
	IN `vat_included` TINYINT(1),
	IN `vat_exempted` TINYINT(1),
	INOUT  `total_price_no_vat`        DECIMAL(11, 2),
	INOUT  `total_vat`        DECIMAL(11, 2),
	INOUT  `total_price`        DECIMAL(11, 2),
	INOUT  `irpf_percent`        DECIMAL(5, 2),
	INOUT  `total_irpf`        DECIMAL(11, 2)
)
READS SQL DATA
	BEGIN

		DECLARE vat_percent DECIMAL(5, 2) DEFAULT 0.21;

		IF vat_exempted = 1
		THEN
			SET vat_percent = 0;
		END IF;

		IF vat_included = 1
			THEN
				SET total_price_no_vat = price / (1 + vat_percent - (irpf_percent/100) );
				SET total_price = price;
			ELSE
				SET total_price_no_vat = price;
				SET total_price = price * (1 + vat_percent - (irpf_percent/100) );
		END IF;


		SET total_irpf = total_price_no_vat * irpf_percent / 100;
		SET total_vat = total_price - total_price_no_vat + total_irpf;

	END//
DELIMITER ;