<?
/**
 * PvExpedicio
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2011
 * @version $Id$
 * @access public
 */
class PvExpedicio extends Module{

	var $needs_client_id = true;
	var $group_id, $user_is_admin, $user_is_client, $client_id;

	function __construct(){
		parent::__construct();
	}
	function on_load()
	{
		parent::on_load();
		$this->set_field( 'destinatari_id', 'add_new_link', '/admin/?menu_id=2622&client_id=' . $this->client_id );
		$this->set_field( 'destinatari_id', 'select_condition', 'client_id=' . $this->client_id );
		$this->set_field( 'conductor_id', 'add_new_link', '/admin/?menu_id=2642&client_id=' . $this->client_id );
		$this->set_field( 'conductor_id', 'select_condition', 'client_id=' . $this->client_id );
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function get_records()
	{
	    $listing = new ListRecords($this);

		$listing->order_by = "data_expedicio DESC";
		$listing->condition = "client_id = " . $this->client_id;
		// $listing->call( 'list_records_walk', '', true );

		$listing->add_button ('get_carta_button', 'action=get_carta', 'boto2', 'after');

	    return $listing->list_records();
	}
/*
	function list_records_walk(&$listing) {


		$rs =  &$listing->rs;
		$ret = $this->calculate_adr($rs);


		$listing->buttons['get_carta_adr_button']['show'] = $ret['is_adr'];
		$listing->buttons['get_carta_exempcio_button']['show'] = !$ret['is_adr'];

		return $ret;

	}*/



	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{
	    $show = new ShowForm($this);
		$show->condition = "client_id = " . $this->client_id;
	    return $show->show_form();
	}
	function save_rows()
	{
	    $save_rows = new SaveRows($this);
	    $save_rows->save();
	}

	function write_record()
	{
	    $writerec = new SaveRows($this);
	    $writerec->save();
	}

	public function get_sublist_html($id, $field, $is_input, $is_word = false){
		$listing = NewRow::get_sublist_html($this, $id, $field, $is_input );

		$listing->order_by = 'nom_comercial ASC, codi_producte ASC';

		if ( $is_word ) {
			$listing->add_field( 'adr_id' );
			$listing->set_field( 'adr_id', 'type', 'int' );
			$listing->set_field( 'adr_id', 'list_admin', 'text' );
			$listing->set_editable(false);
			$listing->set_records();
			return $listing->loop;
		}
		else {
			/*	$listing->set_records();

				foreach ( $listing->loop as &$rs ) {
					$rs['volum'] = '100';
					$rs['volum_value'] = '100';
				}
				return $listing->process();*/

			$listing->list_records( false );

			if ( $listing->has_results ) {

				foreach ( $listing->results as $key => $val ) {
					$rs = &$listing->results[ $key ];
					$rs['volum'] = $rs['quantitat'] * $rs['capacitat_envas'];
				}
			}

			return $listing->parse_template();
		}
	}

	// ajax
	public function get_sublist_item(){

		$listing = new NewRow($this);
		$listing->unset_field( 'volum' );

		$adr_id = $listing->selected_id;

		// Valors per defecte i no modificables
		$rs = array (
			'codi_producte' => Db::get_first("SELECT pv__adr.codi_producte FROM pv__adr WHERE pv__adr.adr_id = '$adr_id'"),
			'nom_comercial' =>  Db::get_first("SELECT pv__adr.nom_comercial FROM pv__adr WHERE pv__adr.adr_id = '$adr_id'"),
			'designacio' =>  Db::get_first("SELECT pv__adr.designacio FROM pv__adr WHERE pv__adr.adr_id = '$adr_id'"),
			'categoria_transport' =>  Db::get_first("SELECT pv__adr.categoria_transport FROM pv__adr WHERE pv__adr.adr_id = '$adr_id'"),
			'capacitat_envas' =>  Db::get_first("SELECT pv__adr.capacitat_envas FROM pv__adr WHERE pv__adr.adr_id = '$adr_id'"),
			'unitats_quantitat' =>  Db::get_first("SELECT pv__adr.unitats_quantitat FROM pv__adr WHERE pv__adr.adr_id = '$adr_id'"),
			'envas_id' =>  Db::get_first("SELECT pv__adr.envas_id FROM pv__adr WHERE pv__adr.adr_id = '$adr_id'"),
		);

		$listing->add_row ($rs);
		$listing->new_row();
	}
	// ajax
	public function get_checkboxes_long_values() {
		{
			$limit = 100;

			$q = R::escape( 'query' );
			$id = R::id( 'id' );
			$selected_ids = R::get( 'selected_ids' );
			$exclude_selected_ids = '';
			$exclude_saved_ids = '';

			if ($selected_ids) {
				R::escape_array( $selected_ids );
				$selected_ids = implode( ',', $selected_ids );
				$exclude_selected_ids = " AND ( adr_id NOT IN ($selected_ids))";
			}

			if ($id) {
				$exclude_saved_ids = " AND ( adr_id NOT IN (SELECT adr_id FROM pv__expedicioitem WHERE expedicio_id = $id))";
			}

			$ret                = array();
			$ret['suggestions'] = array();

			$r = &$ret['suggestions'];

			//	if ($q)
			//	{
			// 1 - Que comenci
			$query = "SELECT adr_id, nom_comercial, designacio, codi_producte
					FROM pv__adr
					WHERE (nom_comercial LIKE '$q%'
					OR designacio LIKE '$q%'
					OR codi_producte LIKE '$q%')
					$exclude_selected_ids					
					$exclude_saved_ids
					AND client_id = $this->client_id
					AND bin = 0
					ORDER BY nom_comercial ASC, designacio ASC, codi_producte ASC
					LIMIT " . $limit;

			$results = Db::get_rows( $query );

			$rest = $limit - count( $results );

			// 2 - Qualsevol posició
			if ( $rest > 0 ) {

				$query = "SELECT adr_id, nom_comercial, designacio, codi_producte
						FROM pv__adr
						WHERE 
							( nom_comercial LIKE '%$q%'
			                AND nom_comercial NOT LIKE '$q%' 
			                )
							OR ( designacio LIKE '%$q%'
			                AND designacio NOT LIKE '$q%' 
			                )
							OR ( codi_producte LIKE '%$q%'
			                AND codi_producte NOT LIKE '$q%' 
			                )
							$exclude_selected_ids
							$exclude_saved_ids
							AND client_id = $this->client_id
							AND bin = 0
						ORDER BY nom_comercial ASC, designacio ASC, codi_producte ASC
						LIMIT " . $rest;

				$results2 = Db::get_rows( $query );

				$results = array_merge( $results, $results2 );

			}


			// Presento resultats
			foreach ( $results as $rs ) {

				$value = '';

				if ($rs['nom_comercial']) {
					$value = $rs['nom_comercial'];
				}

				if ($rs['designacio']) {
					if ($value)
						$value .= ', ';
					$value .= $rs['designacio'] ;
				}

				if ($rs['designacio']) {
					if ($value)
						$value .= ', ';
					$value .= $rs['codi_producte'];
				}



				$r[]   = array(
					'value' => $value,
					'data'  => $rs['adr_id']
				);
			}
			//	}



			$rest = $limit - count( $results );
			$ret['is_complete'] = $rest>0;

			die ( json_encode( $ret ) );


		}

	}
	public function get_carta() {

		$expedicio_id = R::id( 'expedicio_id' );

		$this->action = "show_record";
	    $show = new ShowForm($this);
		$show->add_field( 'client_id' );
		$show->id = $expedicio_id;
		$show->show_form(false);
	    $show->show_form(false);

		// obtinc totes les dades per emplenar el word
		$expedicio_rs = $show->rs;
		// Debug::p( $rs );

		$client_rs = Db::get_row( "SELECT * FROM pv__client WHERE client_id = '" . $expedicio_rs['client_id'] . "'");
		$client_rs['provincia'] = Db::get_first("SELECT name FROM all__provincia WHERE provincia_id = '" . $client_rs['provincia_id'] . "'");

		$client_rs['country'] = Db::get_first("SELECT country FROM all__country WHERE country_id = '" . $client_rs['country_id'] . "'");
		// Debug::p( $client_rs  );

		$conductor_rs = Db::get_row( "SELECT * FROM pv__conductor WHERE conductor_id = '" . $expedicio_rs['conductor_id'] . "'");
		$destinatari_rs = Db::get_row( "SELECT * FROM pv__destinatari WHERE destinatari_id = '" . $expedicio_rs['destinatari_id'] . "'");

		$destinatari_rs['provincia'] = Db::get_first("SELECT name FROM all__provincia WHERE provincia_id = '" . $destinatari_rs['provincia_id'] . "'");

		$destinatari_rs['country'] = Db::get_first("SELECT country FROM all__country WHERE country_id = '" . $destinatari_rs['country_id'] . "'");


		$items_results = $this->get_sublist_html($expedicio_id, 'expedicioitems', false, true);

		foreach ( $items_results as &$rs ) {
			Debug::p( $rs );
			$adr_id = $rs['adr_id'];

			$adr_rs = Db::get_row( "
				SELECT categoria_transport, tunels, embalatge, numero_onu 
					FROM pv__adr 
					WHERE adr_id = $adr_id;" );

			$rs['etiqueta'] = $this->get_etiqueta_carta( $adr_id );
			$rs['categoria_transport'] = $adr_rs['categoria_transport'];
			$rs['tunels'] = $adr_rs['tunels'] ? "({$adr_rs['tunels']})" : '';
			$rs['embalatge'] = $adr_rs['embalatge'];
			$rs['numero_onu'] = $adr_rs['numero_onu'];
			$rs['volum_value'] = $rs['quantitat_value'] * $rs['capacitat_envas_value'];
			$rs['volum'] = format_decimal ($rs['volum_value']);
		}

		$categoria_rs = $this->calculate_categories($items_results);


		// genero el word
		$file_serve_name = $this->caption['c_carta'] . ' ' . $expedicio_id . '.docx';
		$file_serve_path = CLIENT_PATH . 'pv/expedicio/cartas/' . $this->caption['c_carta'] . ' ' . $expedicio_id . '.docx';

		include_once( DOCUMENT_ROOT . 'common/includes/word/templates/opentbs/tbs_class.php' );
		include_once( DOCUMENT_ROOT . 'common/includes/word/templates/opentbs/plugins/tbs_plugin_opentbs.php' );

		$tbs = new clsTinyButStrong;
		$tbs->Plugin( TBS_INSTALL, OPENTBS_PLUGIN );

		$tbs->NoErr    = ! DEBUG;
		$word_template = PATH_TEMPLATES . 'pv/carta de port.docx';

		// Load the template to TBS
		$tbs->LoadTemplate( $word_template, OPENTBS_ALREADY_UTF8 );


		//Debug::p( $tpl->vars);

		// @see http://www.tinybutstrong.com/manual.php#php_mergeblock
		$tbs->MergeBlock( 'expedicio', array ($expedicio_rs) );
		$tbs->MergeBlock( 'categoria', array ($categoria_rs) );
		$tbs->MergeBlock( 'client', array ($client_rs) );
		$tbs->MergeBlock( 'conductor', array ($conductor_rs) );
		$tbs->MergeBlock( 'destinatari', array ($destinatari_rs) );
		$tbs->MergeBlock( 'item', $items_results );

		$tbs->Show( OPENTBS_DOWNLOAD, $file_serve_name );
		die();

		// $tbs->Show( OPENTBS_FILE, $file_serve_path );


	}

	private function get_etiqueta_carta ( $adr_id ){
		$query = "
			SELECT etiqueta 
			FROM pv__etiqueta
			INNER JOIN pv__adr_to_etiqueta
			USING (etiqueta_id)
			WHERE adr_id = %s
			ORDER BY ordre ASC, etiqueta ASC";
		$results = Db::get( $query, $adr_id );

		if (!$results) return '';

		$ret = $results[0]['etiqueta'];

		$results = array_slice( $results, 1 );

		if (!$results) return $ret;

		$ret2 = implode_field( $results, 'etiqueta', ', ' );

		$ret = "$ret ($ret2)";

		return $ret;
	}


	private function calculate_categories($items_results) {


		$ret['total_cat_1'] = $ret['total_cat_2'] = $ret['total_cat_3'] = $ret['total_cat_4'] = 0;


		foreach ( $items_results as $rs ) {
			$ret['total_cat_' . $rs['categoria_transport']] += $rs['volum_value'];
		}

		// Al word i va el total_n , i el total_cat_n es per si decas em fa falta el total de la categoria per alguna altra cosa
		$calculated_total = $ret['total_cat_1'] * 50 + $ret['total_cat_2'] * 3 + $ret['total_cat_3'];
		if (
			($calculated_total) > 1000
		){
			$ret['total_1'] = $ret['total_2'] = $ret['total_3'] = $ret['total_4'] = '';
			$GLOBALS['gl_delete_categorias'] = 1;
		}
		else {
			$ret['total_1'] = $ret['total_cat_1'];
			$ret['total_2'] = $ret['total_cat_2'];
			$ret['total_3'] = $ret['total_cat_3'];
			$ret['total_4'] = $ret['total_cat_4'];
			$GLOBALS['gl_delete_categorias'] = 0;
		}


		return $ret;
	}
}
?>