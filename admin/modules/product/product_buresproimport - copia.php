<?
/**
 * ProductBuresproimport
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 12/2013
 * @version $Id$
 * @access public
 */
class ProductBuresproimport extends Module{
	function __construct(){
		Db::$debug = true;
		echo "kkkkkk";
	}
	
	function list_records() {
		
			set_time_limit ( 300 );
			
			$idiomes = array(				
				2 => 'spa',
				4 => 'eng',
				3 => 'fra'
			);
		
			// productes
			// product__product 
			// product__product_to_sector 
			
			Db::connect('localhost', 'root', '', 'burespro_vella');			
			$productes = Db::get_rows("SELECT * FORM producte");
			
			Db::reconnect();
			
			foreach($productes as $rs){
				
				extract($rs);
				$product_id = $id;
				
				if ($grup_id == 10) $grup_id = 0;
				
				Db::execute ("INSERT INTO product__product (product_id,family_id,status) VALUES ($product_id,$familia_id,'onsale')");
				Db::execute ("INSERT INTO product__product_to_sector (product_id, sector_id) values ($product_id,$grup_id)");
				
				
				
			}
			
			// productes idiomes
			// product__product_language
			
			Db::connect('localhost', 'root', '', 'burespro_vella');				
			$productes = Db::get_row("SELECT * FROM producte_idioma");
			
			Db::reconnect();
			
			foreach ($productes as $rs){
				extract($rs);
				
				$titol = Db::qstr($titol . "\n" . $subtitol);
				$descripcio = Db::qstr($descripcio);
				$idioma = $idiomes[$idioma];
				
				Db::execute ("INSERT INTO product__product_language (product_id, product_title, product_description, language) values ($producte_id,'$titol','$descripcio','$idioma')");				
				
			}
			
			// grups
			// product__sector
			// product__sector_language
			
			Db::connect('localhost', 'root', '', 'burespro_vella');				
			$grups = Db::get_row("SELECT * FROM grups WHERE grup_id <> 10");
			
			Db::reconnect();
			
			foreach ($grups as $rs){
				extract($rs);
				$sector_id = $id;
				
				Db::execute ("INSERT INTO product__sector (sector_id, ordre) values ($sector_id,$ordre)");
				
				foreach ($idiomes as $key=>$idioma){
					
					$sector = Db::qstr(${'nom_lg'.$key});
					Db::execute ("INSERT INTO product__sector_language (sector_id, language, sector) values ($sector_id,'$idioma','$sector')");
					
				}
				
			}
			
			// families CATALEG
			// product__family
			// product__family_language
			
			Db::connect('localhost', 'root', '', 'burespro_vella');				
			$families = Db::get_row("SELECT * FROM catalegs");
			
			Db::reconnect();
			
			foreach ($families as $rs){
				extract($rs);
				
				Db::execute ("INSERT INTO product__family (family_id) values ($id)");
				
				foreach ($idiomes as $key=>$idioma){
					
					$family = Db::qstr(${'nom_lg'.$key});
					Db::execute ("INSERT INTO product__family_language (family_id, language, family) values ($id,'$idioma','$family')");
					
				}
				
			}
			// sub families - FAMILIES
			// product__family
			// product__family_language
			
			Db::connect('localhost', 'root', '', 'burespro_vella');				
			$families = Db::get_row("SELECT * FROM families");
			
			Db::reconnect();
			
			foreach ($families as $rs){
				extract($rs);
				
				Db::execute ("INSERT INTO product__family (family_id, ordre, parent_id) values ($id,$ordre,$cataleg_id)");
				
				foreach ($idiomes as $key=>$idioma){
					
					$family = Db::qstr(${'nom_lg'.$key});
					Db::execute ("INSERT INTO product__family_language (family_id, language, family) values ($id,'$idioma','$family')");
					
				}
				
			}
			
			
			// sub producte
			
			
			
			
			// noticies
			// news__new
			// news__new_language
			
			Db::connect('localhost', 'root', '', 'burespro_vella');				
			$news = Db::get_row("SELECT * FROM noticies");
			
			Db::reconnect();
			
			foreach ($news as $rs){
				extract($rs);
				
				$titol = Db::qstr($titol);
				$breu = Db::qstr($breu);
				$descripcio = Db::qstr($descripcio);
				$idioma = $idiomes[$idioma];
				
				Db::execute ("INSERT INTO news__new (new_id, entered, expired, status,category_id,in_home) values ($codi,'$data_publicacio','$data_caducitat','public',1,1)");				
				Db::execute ("INSERT INTO news__new_language (new_id, new, subtitle, content, language) values ($codi,'$titol','$breu','$descripcio','$idioma')");				
				
			}


	}
}

/* RESET TAULES
 * ------------

TRUNCATE `product__product`;
TRUNCATE `product__product_to_sector`;

TRUNCATE `product__product_language`;

TRUNCATE `product__sector`;
TRUNCATE `product__sector_language`;

TRUNCATE `product__family`;
TRUNCATE `product__family_language`;

TRUNCATE `news__new_language`;
TRUNCATE `news__new`;

 */

?>