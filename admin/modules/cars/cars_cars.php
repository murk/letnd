<?php
function save_rows()
{
    $save_rows = new SaveRows;
	$save_rows->field_unique = "ref";
    $save_rows->save();
}

function list_records()
{
    global $gl_content, $gl_action;
	$listing = new ListRecords;
	switch ($gl_action)
    {
	case 'list_records_search_nif':
            $listing->condition .= search_search_condition();
			$listing -> order_by = 'ordre';
            break;
    case 'list_records_search_words':
            $listing->condition .= search_list_condition_words();
			$listing -> order_by = 'ordre';
            break;
            // default:
    } // switch
	$listing -> order_by = 'ordre';
    $listing->list_records();
	$gl_content = get_search_content() . $gl_content;
}

function show_form()
{
   global  $tpl, $gl_action, $gl_write, $gl_content;

    $show = new ShowForm;
	$template = 'cars/cars_form';
	$show->show_form();
	$gl_content = get_search_content() . $gl_content;
}

function write_record()
{
    $writerec = new SaveRows;
    $writerec->field_unique = "ref";
	$writerec->save();
}

function manage_images()
{
	$image_manager = new ImageManager;
	$image_manager->set_options (1, 1, 1, 0);
    $image_manager->execute();
}
function get_search_content()
{
    global $tpl, $gl_caption, $gl_menu_id;
    $template = 'cars/cars_search.tpl';
    $tpl->set_file($template);
    $tpl->set_var('menu_id', $gl_menu_id);
    $tpl->set_vars($gl_caption);
    return $tpl->process();
}
function search_list_condition_words ()
{
    global $gl_page;
    $q = R::escape('words');
    // miro quantes paraules hi ha:
    $query = " ref like '%" . $q . "%' OR
			   ref_number like '%" . $q . "%' OR
			   ref_supplier like '%" . $q . "%' OR
			   fuel like '%" . $q . "%' OR
			   cc like '%" . $q . "%' OR
			   color like '%" . $q . "%' OR
			   pvp like '%" . $q . "%' OR
			   pvd like '%" . $q . "%' OR
			   price_consult like '%" . $q . "%' OR
			   observations like '%" . $q . "%'
			   AND bin = 0" ;
    $search_condition = $query;
    $gl_page->title = TITLE_SEARCH;
	return $search_condition;
}
function search_search_condition()
{
    global $gl_page;

	$ref = R::escape('ref','NULL');
	
    $query = "ref = '" . $ref . "'";
    $search_condition = $query;
    $gl_page->title = TITLE_SEARCH;
    return $search_condition;
}
?>