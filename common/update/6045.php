<?php

/* 12-06-15
  INMO - Canvi en solicituds, afegir municipis molts a molts, i tots els camps que falten
			Afegir segona lletra efficiencia

UP-DATE
Nou sistema per entrar els camps horaris, com ara data d'entrada i de sortida de la fitxa d'inmobles
Nou sistema per triar municipis a sol·licituds, ara es seleccionen els municipis amb un camp tipus cercador, i també hi ha l'opció de triar només comarca, només municipi o més d'una comarca o municipi
S'han afegit nous camps a les sol·licituds, en corcondància amb els camps de la fitxa d'inmobles
S'han simplificat els llistats de sol·licituds i resultats per facilitar la lectura de l'informació
El camp "Tipus d'obra" d'una sol·licitud ara és optatiu
S'ha afegit una segona lletra d'efficiencia energètica
Nou sistema per seleccionar clients en l'historial d'un inmoble, quan hi ha molts clients es feia molt difícil amb un desplegable normal, ara es selecionen amb un camp tipus cercador

 */

if ( is_table( 'inmo__property' ) ) {

	if ( ! is_table( 'custumer__demand_to_municipi' ) ) {

		$update_sql .= "
			CREATE TABLE `custumer__demand_to_municipi` (
				`demand_municipi_id` INT(11) NOT NULL AUTO_INCREMENT,
				`demand_id` INT(11) NULL DEFAULT '0',
				`municipi_id` INT(11) NULL DEFAULT '0',
				`comarca_id` INT(11) NULL DEFAULT '0',
				PRIMARY KEY (`demand_municipi_id`),
				INDEX `demand_id` (`demand_id`),
				INDEX `municipi_id` (`municipi_id`),
				INDEX `comarca_id` (`comarca_id`)
			)
			COLLATE='utf8_spanish2_ci'
			ENGINE=MyISAM;";

		$update_sql .= "
			INSERT INTO custumer__demand_to_municipi (demand_id,municipi_id,comarca_id) SELECT demand_id,municipi_id,comarca_id FROM custumer__demand WHERE municipi_id <> 0;";

		$update_sql .= "
			ALTER TABLE `custumer__demand`
				CHANGE COLUMN `municipi_id` `xxmunicipi_id` INT(11) NOT NULL DEFAULT '0' AFTER `observations`;";
		$update_sql .= "
			ALTER TABLE `custumer__demand`
				CHANGE COLUMN `tipus2` `tipus2` ENUM('0','new','second') NOT NULL DEFAULT '0' COLLATE 'utf8_spanish2_ci' AFTER `tipus`;";
		$update_sql .= "
			ALTER TABLE `custumer__custumer`
				CHANGE COLUMN `surname2` `surname2` VARCHAR(50) NOT NULL
				DEFAULT '' COLLATE 'utf8_spanish2_ci'
				AFTER `surname1`;";

	}

	if ( is_table( 'custumer__demand' ) && ! is_field( 'custumer__demand', 'living' ) ) {

		$update_sql .= "
			ALTER TABLE `custumer__demand`
				ADD `living` TINYINT( 2 ) NOT NULL AFTER `wc` ,
				ADD `dinning` TINYINT( 2 ) NOT NULL AFTER `living` ;

			ALTER TABLE `custumer__demand`
				ADD `service_room` TINYINT( 1 ) NOT NULL AFTER `airconditioned` ,
				ADD `service_bath` TINYINT( 1 ) NOT NULL AFTER `service_room` ,
				ADD `study` TINYINT( 1 ) NOT NULL AFTER `service_bath` ,
				ADD `courtyard` TINYINT( 1 ) NOT NULL AFTER `study` ,
				ADD `courtyard_area` INT( 5 ) NOT NULL AFTER `courtyard`;

			ALTER TABLE `custumer__demand`
				ADD `parket` TINYINT( 1 ) NOT NULL AFTER `price` ,
				ADD `ceramic` TINYINT( 1 ) NOT NULL AFTER `parket` ,
				ADD `gres` TINYINT( 1 ) NOT NULL AFTER `parket` ,
				ADD `estrato` ENUM( 'e1', 'e2', 'e3', 'e4', 'e5', 'e6' ) NOT NULL AFTER `ceramic` ;

			ALTER TABLE `custumer__demand`
				ADD `laminated` TINYINT( 1 ) NOT NULL AFTER `parket` ;

			ALTER TABLE `custumer__demand` ADD `parking` TINYINT( 1 ) NOT NULL DEFAULT '0' AFTER `laundry` ;

			ALTER TABLE `custumer__demand`
				CHANGE COLUMN `tipus` `tipus` ENUM('sell','rent','temp','moblat','selloption') NOT NULL DEFAULT 'sell';
			ALTER TABLE `custumer__demand`
				ADD COLUMN `efficiency` ENUM('progress','exempt','a','b','c','d','e','f','g')  NOT NULL DEFAULT 'progress' AFTER `price`;

			ALTER TABLE `custumer__demand`
				ADD COLUMN `facing` ENUM('','n','ne','e','se','s','so','o','no') NOT NULL DEFAULT '' AFTER `airconditioned`;

			ALTER TABLE `custumer__demand`
				ADD COLUMN `sea_view` TINYINT(1) NOT NULL DEFAULT '0' AFTER `courtyard_area`,
				ADD COLUMN `clear_view` TINYINT(1) NOT NULL DEFAULT '0' AFTER `sea_view`;

			ALTER TABLE `custumer__demand`
				ADD COLUMN `level_count` INT(11) NOT NULL DEFAULT '0' AFTER `facing`,
				ADD COLUMN `marble` TINYINT(1) NOT NULL AFTER `gres`;


			ALTER TABLE custumer__demand
				ADD COLUMN `office` TINYINT(1) NOT NULL DEFAULT '0' AFTER `terrace_area`;
				";
	} else {
		if ( is_table( 'custumer__demand' ) ) {
			trigger_error( 'Ja existeixen el camp living' );
		}
	}

	// afegeixo segona lletra de eficiencia
	if ( is_table( 'inmo__property' ) && ! is_field( 'inmo__property', 'efficiency2' ) ) {

		$update_sql .= "
		ALTER TABLE `inmo__property`
			ADD COLUMN `efficiency2` ENUM('notdefined','a','b','c','d','e','f','g')  NOT NULL DEFAULT 'notdefined' AFTER `efficiency`;";

	} else {
		if ( is_table( 'inmo__property' ) ) {
			trigger_error( 'Ja existeixen el camp efficiency2' );
		}
	}

}

?>