<?

/**
 * ProductCommonSpec
 *
 * Funcions comuns per spec tant a public com admin
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) Setembre 2016
 * @version $Id$
 * @access public
 */
class ProductCommonSpec {

	public $is_mail = false;
	public $is_contract = false;
	public $is_editable = true;
	public $reserved_message = '';
	public $messages;
	public $caption;

	function __construct() {

	}


	static public function show_specs( $id ) {


		// obtinc les categories
		$query   = "SELECT DISTINCT spec_category_id, spec_category 
					FROM product__spec_category_language 
					INNER JOIN product__spec
					USING (spec_category_id)
					WHERE language = '" . LANGUAGE . "'
					AND product__spec.bin=0
					ORDER BY ordre ASC, spec_category ASC";
		$results = Db::get_rows( $query );
		$loop    = array();
		$conta   = 0;

		//$assign_selected_ids = array();
		foreach ( $results as $rs ) {
			$specs  = ProductCommonSpec::get_specs( $rs['spec_category_id'], $id );
			$loop[] = array(
				'specs'            => $specs,
				'spec_category_id' => $rs['spec_category_id'],
				'spec_category'    => $rs['spec_category'],
				'conta'            => $conta
			);

			// $assign_selected_ids = array_merge($assign_selected_ids, $specs['ids']);
		}

		return $loop;
	}

	static function get_specs( $spec_category_id, $id ) {

		$query = "SELECT product__spec.spec_id AS spec_id, spec as spec_name, spec_type, spec_unit 
					FROM product__spec_language , product__spec
					WHERE language = '" . LANGUAGE . "'
					AND product__spec_language.spec_id = product__spec.spec_id
					AND product__spec.bin=0
					AND spec_category_id = " . $spec_category_id . "
					ORDER BY ordre ASC, spec ASC";

		$specs = Db::get_rows( $query );

		$odd_even = 'Odd';
		$conta    = 0;
		$ret_specs = array();
		foreach ( $specs as &$rs ) {

			$odd_even       = $odd_even == 'Odd' ? 'Even' : 'Odd';
			$rs['odd_even'] = $odd_even;

			$spec_input            = ProductCommonSpec::get_spec_input( $rs['spec_id'], $rs['spec_type'], $id );
			$rs['spec']      = $spec_input['spec'];
			$rs['language_specs'] = $spec_input['language_specs'];
			$rs['spec_conta']      = $conta ++;

			// per admin sempre retorna tot, a public només si spec té un valor
			if ($spec_input['spec'] || $spec_input['language_specs']) {
				if ($spec_input['spec'] === true) $rs['spec'] = ''; // Al checkbox públic només es mostra el nom de spec
				$ret_specs [] = $rs;
			}

		}

		return $ret_specs;
	}


	static function get_spec_input( $spec_id, $spec_type, $product_id ) {

		$id              = 'spec_' . $spec_id;
		$name            = 'spec[' . $spec_id . ']';
		$value           = ProductCommonSpec::get_spec_val( $spec_id, $spec_type, $product_id );
		$language_specs = array();

		$spec = '';

		$is_admin = $GLOBALS['gl_is_admin'];

		switch ( $spec_type ) {
			// text_no_lang
			case $spec_type == 'text_no_lang' && $is_admin:
				$spec = '<input id="' . $id . '" name="' . $name . '" type="text" value="' . $value . '" size="50" maxlength="255" /><input name="old_' . $name . '" type="hidden" value="' . $value . '" />';
				break;
			case $spec_type == 'text_no_lang' && ! $is_admin:
				$spec = htmlspecialchars( $value );
				break;

			// text
			case $spec_type == 'text' && $is_admin:
				global $gl_languages;

				foreach ( $gl_languages['public'] as $language ) {
					$name_language  = $name . '[' . $language . ']';
					$id_language    = $id . '_' . $language;
					$value_language = isset( $value[ $language ] ) ? $value[ $language ] : '';

					$in_lang            = '<input id="' . $id_language . '" name="' . $name_language . '" type="text" value="' . $value_language . '" size="50" maxlength="255" /><input name="old_' . $name_language . '" type="hidden" value="' . $value_language . '" />';
					$language_specs [] = array(
						'lang'                => $gl_languages['names'][ $language ],
						'language_spec' => $in_lang
					);
				}
				break;

			case $spec_type == 'text' && ! $is_admin:
				$value_language = isset( $value[ LANGUAGE ] ) ? $value[ LANGUAGE ] : '';
				$spec = htmlspecialchars( $value_language );
				break;

			// textarea
			case $spec_type == 'textarea' && $is_admin:
				$spec = '<textarea id="' . $id . '" name="' . $name . '" cols="6" rows="4"  /><input name="old_' . $name . '" type="hidden" value="' . $value . '" />' . $value . '</textarea>';

				break;
			case $spec_type == 'textarea' && ! $is_admin:
				$spec = nl2br( htmlspecialchars( $value ) );

				break;

			// checkbox
			case $spec_type == 'checkbox' && $is_admin:
				$checked = $value?' checked':'';
				$spec = '<input value="1" id="' . $id . '" name="' . $name . '" type="checkbox" ' . $checked . ' /><input name="old_' . $name . '" type="hidden" value="' . $value . '" />';
				break;
			case $spec_type == 'checkbox' && ! $is_admin:
				$spec = $value?true:false;
				break;

			// int
			case $spec_type == 'int':

				break;

			// select
			case $spec_type == 'select':

				break;
		}

		return array( 'spec' => $spec, 'language_specs' => $language_specs );
	}

	static function get_spec_val( $spec_id, $spec_type, $product_id, $show_all_languages = true ) {

		$is_lang = $spec_type == 'text';

		$table = $is_lang ? 'product__product_to_spec_language, product__product_to_spec' : 'product__product_to_spec';
		$field = $is_lang ? 'product__product_to_spec_language.spec AS spec, language' : 'spec';
		$query = "SELECT " . $field . " FROM " . $table . " WHERE spec_id = " . $spec_id;

		if ( $is_lang ) {
			$query .= " AND product__product_to_spec_language.product_spec_id = product__product_to_spec.product_spec_id";
		}
		$query .= " AND product__product_to_spec.product_id = " . $product_id;


		if ( $is_lang ) {

			$results = Db::get_rows( $query );
			$ret     = array();
			foreach ( $results as $rs ) {
				$ret[ $rs['language'] ] = $rs['spec'];
			}

			return $show_all_languages?$ret:$ret[LANGUAGE];
		} else {
			return Db::get_first( $query );
		}

	}


	/**
	 * Funció per poder obtindre varis specs de un registre separats per $sep, per poder-se utilitzar des de el llistat
	 * Ej: ProductProduct::get_listed_specs ( $id, '10,20,30', ' ');
	 *
	 * @param $product_id Id del producte
	 * @param $spec_ids Ids dels specs separats per comes
	 * @param $sep Separador dels specs
	 *
	 * @return string
	 */
	static function get_listed_specs( $product_id, $spec_ids, $sep ){

		if (!defined('PRODUCT_IS_SPECS_ON')) Module::get_config_values('product');

		if ( PRODUCT_IS_SPECS_ON == '0' )
			return '';


		$query = "SELECT spec_id AS spec_id, spec_type, spec_unit 
					FROM product__spec
					WHERE bin=0
					AND spec_id IN (" . $spec_ids . ")
					ORDER BY FIELD(spec_id, " . $spec_ids . ")";

		$specs = Db::get_rows( $query );
		$ret_array = array();
		foreach ( $specs as &$rs ) {
			$spec =  ProductCommonSpec::get_spec_val( $rs ['spec_id'], $rs ['spec_type'], $product_id, false );
			if ($rs ['spec_unit']) $spec .=  $rs ['spec_unit'];
			$ret_array [] = $spec;
		}

		$ret = implode( $sep, $ret_array );

		return $ret;
	}

	/**
	 * Per obtindre els specs d'una categoria en concret
	 *
	 * @param $spec_categorys
	 * @param $spec_category_id
	 *
	 * @return array
	 */
	public static function get_specs_category( $spec_categorys, $spec_category_id ){

		if (!$spec_categorys) return [];

		foreach ( $spec_categorys as $rs ) {
			$id = $rs['spec_category_id'];
			if ($spec_category_id == $id) return $rs;
		}

		return [];
	}

	static function save_rows_specs() {
		$product_id = R::id( 'product_id' );
		if ( ! $product_id ) {
			die();
		}
		//Debug::p( $_POST );
		//die();

		$specs = $_POST['old_spec'];

		foreach ( $specs as $spec_id => $val ) {
			$spec = $_POST['spec'][ $spec_id ];
			if ( is_array( $spec ) ) {
				R::escape_array( $spec );
				$is_lang = true;
			} else {
				$spec    = R::escape( $spec, '', false );
				$is_lang = false;
			}


			// per introduir el registre a product__product_to_spec amb spec = '' quan es idioma
			if ( $is_lang ) {
				$spec_value = '';
			} else {
				$spec_value = $spec;
			}

			// Comprova si ja s'ha entrat un registre per l'id
			$product_spec_id = DB::get_first( "
					SELECT product_spec_id 
					FROM product__product_to_spec 
					WHERE product_id =" . $product_id . "
					AND spec_id = '" . $spec_id . "'" );

			if ( $product_spec_id ) {

				$query = "
						UPDATE product__product_to_spec
							SET `spec`='" . $spec_value . "'
							WHERE  `product_spec_id`='" . $product_spec_id . "';";
				Db::execute( $query );

			} else {

				$query = "
					INSERT INTO product__product_to_spec
					    (`product_id`, `spec_id`, `spec`) 
				        VALUES 
				        ('" . $product_id . "', '" . $spec_id . "', '" . $spec_value . "');";
				Db::execute( $query );

				$product_spec_id = Db::insert_id();

			}


			if ( $is_lang ) {


				// poso un spec en cada idioma
				foreach ( $spec as $lang => $spec_lang ) {

					$can_update_language = DB::get_first( "
					SELECT product_spec_id 
					FROM product__product_to_spec_language 
					WHERE product_spec_id =" . $product_spec_id . "
					AND language = '" . $lang . "'" );

					if ( $can_update_language ) {

						$query = "
							UPDATE product__product_to_spec_language
								SET `spec`='" . $spec_lang . "'
								WHERE  `product_spec_id`='" . $product_spec_id . "'
								AND  `language`='" . $lang . "';";
						Db::execute( $query );
						Debug::p( $query );
					} else {
						$query = "
							INSERT INTO product__product_to_spec_language
							    (`product_spec_id`, product_id, `language`, `spec`) 
						        VALUES 
						        ('" . $product_spec_id . "', '" . $product_id . "', '" . $lang . "', '" . $spec_lang . "');";
						Db::execute( $query );
					}
				}


			}


		}


		// borro tots els registres que no tenen una especificació ( poder s'ha borrat l'especificació ), podria ser que el post falli i no m'en puc refiar per borrar la resta, però amb els registres que hi ha a la taula de specs sí
		$query = "
			DELETE FROM product__product_to_spec 
			WHERE product_id = " . $product_id . " 
			AND spec_id NOT IN (
				SELECT spec_id FROM product__spec
			) ";
		Db::execute( $query );
		$query = "
			DELETE FROM product__product_to_spec_language 
			WHERE product_id = " . $product_id . " 
			AND product_spec_id NOT IN (
				SELECT product_spec_id FROM product__product_to_spec
			) ";
		Db::execute( $query );

		$js = '
		if (typeof parent.hidePopWin == "function") parent.window.setTimeout("hidePopWin(false);", 800);
		if (typeof parent.showPopWin == "function") parent.showPopWin("", 300, 100, null, false, false, "' . addslashes( $GLOBALS['gl_messages']['list_saved'] ) . '");';

		print_javascript( $js );
		Debug::p_all();
		die();
	}

}

?>