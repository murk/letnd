<?

/*15-11-13
  Afegir tipus de sellpoints
	
 
 */

if (is_table('product__sellpoint') && !is_field("product__sellpoint",'seller_kind')) {	
	
	$update_sql[] = "
		ALTER TABLE `product__sellpoint`
	ADD COLUMN `seller_kind` ENUM('shop','dealer') NOT NULL DEFAULT 'shop' AFTER `status`;";
	$update_sql[] = "
		ALTER TABLE `product__sellpoint`
	ADD COLUMN `country_id` CHAR(2) NOT NULL AFTER `cp`;";
	$update_sql[] = "
		ALTER TABLE `product__sellpoint`
	ADD COLUMN `comunitat_id` INT(11) NOT NULL AFTER `provincia_id`;";
	
}

if (is_table('all__provincia') && is_field("all__provincia",'admin1_id')) {	
	
	$update_sql[] = "
		ALTER TABLE `all__provincia`
	CHANGE COLUMN `admin1_id` `comunitat_id` VARCHAR(20) NULL DEFAULT NULL AFTER `admin2_id`;";
	
}

?>