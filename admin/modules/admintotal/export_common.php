<?
include_once ( 'config_mls.php' ); // està al path includes
include_once ( DOCUMENT_ROOT . 'common/includes/rest/Unirest.php' );

class ExportCommon {

	// Per poder exportar nomes una agencia i un immoble, en temps real
	public $agencia_id = 0;
	public static $conta = 0;
	public $ids = [];
	public $portal_vars = [];
	public $deleted_results;
	public static $is_ok = false;



	function __construct(){


	}
	static function init_unirest(&$module){

		if ( isset ( $GLOBALS['gl_version'] ) ) {
			$version = $GLOBALS['gl_version'];
		}
		else {
			$version = Db::get_first( "SELECT value FROM webletnd.all__configadmin WHERE name = 'version';" );
		}


		$module->headers = [
			'Accept'       => 'application/json',
			'Content-Type' => 'application/json',
			'user-agent'   => 'Letnd-' . $version
		];
	}

	/**
	 *
	 * Actualitza al moment a fotocasa o vreasy quan s'ha guardat algún canvi o s'ha creat un nou immoble
	 * Es crida un php amb curl i es deixa que s'executi pel seu compte amb un màxim de 0.1 segons
	 * El php no espera que acabi així si tarda molt no relanteix l'acció de guardar un inmoble
	 *
	 * El problema pot ser si no se'n fa prou amb 0.1 per enviar els ids al script que s'encarrega de actualitzar a fotocasa
	 *
	 * @param $action
	 * @param $section
	 */
	function export_async( $action, $section ) {

		if ( ! $this->ids ) {
			$ids = false;
		}
		elseif ( is_array( $this->ids ) ) {
			$ids = array_values( $this->ids );
		}
		else {
			$ids = [ $this->ids ];
		}

		if ($ids) {
			self::mark_for_async( $ids, $section, '0' );
		}
		else if ( $this->portal_vars ) {
			self::mark_for_async( $this->portal_vars['all_ids'], $section, '0' );
		}

		$token      = get_token();
		$expiration = microtime( true ) + 5;

		set_config_var( 'temp_token', $token, 'all__configadmin', true );
		set_config_var( 'temp_token_expiration', $expiration, 'all__configadmin', true );

		$vars = [
			'agencia_id'   => $this->agencia_id,
			'ids' => $ids,
			'portal_vars' => $this->portal_vars,
			'deleted_results' => $this->deleted_results,
			'section'       => $section,
			'action'       => 'save_to_portal',
			'temp_tkn'     => $token,
		];

		$url = HOST_URL . $GLOBALS['configuration']['token_url'];
		if ( $GLOBALS['gl_is_local'] || DEBUG ) {
			$url .= '-debug';
		}


		switch ( $action ) {
			case 'save_portal':
				$vars['process'] = 'on_save_portal';
				break;
			case 'bin_record':
			case 'save_rows_bin_selected':
				$vars['process'] = 'on_save_bin';
				break;
			case 'save_rows_delete_selected':
			case 'delete_record':
				$vars['process'] = 'on_save_delete';
				break;
			case 'save_rows_restore_selected':
				$vars['process'] = 'on_save_restore';
				break;
			default:
				$vars['process'] = 'on_save_update';
		};

		ExportCommon::trace( print_r ($vars, true), 'Preparació Export assync', true);

		$vars = json_encode( $vars );

		$curl = curl_init();
		curl_setopt( $curl, CURLOPT_URL, $url );
		curl_setopt( $curl, CURLOPT_CUSTOMREQUEST, "POST" );
		curl_setopt( $curl, CURLOPT_POSTFIELDS, $vars );

		// Comentar  Per fer debug i esperar resposta
		curl_setopt( $curl, CURLOPT_RETURNTRANSFER, false );
		curl_setopt( $curl, CURLOPT_TIMEOUT_MS, 100 );
		curl_setopt( $curl, CURLOPT_HEADER, true );
		curl_setopt( $curl, CURLOPT_NOSIGNAL, 1 );


		// Descomentar Per fer debug i esperar resposta
		// curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

		curl_setopt( $curl, CURLOPT_HTTPHEADER,
			array(
				'User-Agent:Letnd-' . $GLOBALS['gl_version'],
				'Content-Type:application/json',
				'Content-Length: ' . strlen( $vars )
			)
		);
		$content = curl_exec( $curl );
		curl_close( $curl );

	}
	/*
	 * Marca com a pendent abans de fer asynch, i poso fet just al acabar el export_from_token_access
	 */
	static public function mark_for_async( $ids, $section, $state, $db_name = '' ) {

		if (!$ids ) return;
		$table = '';

		if ( $db_name ) {
			$table = "`$db_name`.";
		}

		if ( $section == 'book' ) {
			$table .= 'booking__book';
			$id_field = 'book_id';
		}
		elseif ( $section == 'property' ) {
			$table .= 'inmo__property_to_portal';
			$id_field = 'property_id';
		}
		else{
			return;
		}

		foreach ( $ids as $id ) {
			$query = "UPDATE $table SET synced='$state' WHERE  $id_field=$id;";
			Db::execute( $query );
		}

	}

	static function export_from_token_access( $obj, $json ) {

		$action  = $GLOBALS['gl_action'] = $obj ['action']; // el gl_ es per el mail
		$process = $GLOBALS['gl_process'] = $obj ['process'];
		$section = $obj ['section'];
		$ids = $obj ['ids'];
		$portal_vars = $obj ['portal_vars'];
		$deleted_results = $obj ['deleted_results'];
		$agencia_id = $obj ['agencia_id'];


		ExportCommon::trace( print_r ($obj, true), '', 'export_from_token_access');
		// ExportCommon::trace( print_r ($_SESSION, true), 'Sessio');

		// Només permeto unes accions concretes
		if ( $action != 'save_to_portal' ) return;

		// Només permeto uns process concrets
		$is_process_allowed = (
			$process == 'on_save_portal' ||
			$process == 'on_save_delete' ||
			$process == 'on_save_bin' ||
			$process == 'on_save_restore' ||
			$process == 'on_save_update'
		     );
		if ( !$is_process_allowed ) return;


		ExportCommon::trace( $section, 'Comença exportació vreasy book');
		/*
		BOOK - Només s'exporta a vreasy
		*/
		// VREASY
		if ( $_SESSION['inmo_vreasy'] && $section == 'book' ) {
			include( DOCUMENT_ROOT . 'admin/modules/admintotal/admintotal_vreasy_book.php' );

			$export = New AdmintotalVreasyBook;

			if ( $ids ) $export->book_ids = $ids;

			$export->agencia_id = $agencia_id;
			$export->deleted_results = $deleted_results;
			$export->set_on_vars();
			$export->$process();
			self::$is_ok = true;
			Db::reconnect();
		}

		ExportCommon::trace( $process, 'Comença exportació vreasy');
		// VREASY -
		if ( $_SESSION['inmo_vreasy'] && $section == 'property' ) {
			include( DOCUMENT_ROOT . 'admin/modules/admintotal/admintotal_vreasy.php' );

			$export = New AdmintotalVreasy;

			if ( $ids ) $export->property_ids = $ids;
			if ( $portal_vars ) $export->portal_vars = $portal_vars;

			$export->agencia_id = $agencia_id;
			$export->$process();
			self::$is_ok = true;
			Db::reconnect();
		}

		ExportCommon::trace( $process, 'Comença exportació fotocasa');
		/*
		PROPERTY - S'exporta a tots
		*/
		// FOTOCASA - Al guardar un immoble, borrar o guardar borrar des del llistat
		if ( $_SESSION['inmo_fotocasa'] && $section == 'property' ) {
			include( DOCUMENT_ROOT . 'admin/modules/admintotal/admintotal_fotocasa.php' );

			$export = New AdmintotalFotocasa;

			if ( $ids ) $export->property_ids = $ids;
			if ( $portal_vars ) $export->portal_vars = $portal_vars;

			$export->agencia_id = $agencia_id;
			$export->$process();
			self::$is_ok = true;
			Db::reconnect();
		}

		// Desmarco tots els marcats per sincronitzar desde portal, ja que també hi ha marcats els que no tenen el portal activat
		if ($portal_vars) {

			Db::connect_mother();
			$dbname = Db::get_first ("SELECT dbname FROM letnd.client__client WHERE client_id = $agencia_id");
			self::mark_for_async( $portal_vars ['all_ids'], 'property', '1', $dbname);
		}

		Debug::p_all();
		die();
	}

	/*
	 *
	 * Variables de l'agència
	 *
	 */
	static public function get_agencia_config( &$agencia, $portal ) {

		$dbname = $agencia['dbname'];

		$query = "SELECT value FROM `$dbname`.inmo__configadmin WHERE name = '${portal}_export_all'";
		$agencia["${portal}_export_all"] = Db::get_first( $query );


		$query = "SELECT value FROM `$dbname`.inmo__configadmin WHERE name = 'property_auto_ref'";
		$agencia["property_auto_ref"] = Db::get_first( $query );


		$query = "SELECT value FROM `$dbname`.all__configpublic WHERE name = 'company_name'";

		$agencia['company_name'] = Db::get_first( $query );


		$query = "SELECT value FROM `$dbname`.all__configpublic WHERE name = 'default_mail'";

		$agencia['default_mail'] = Db::get_first( $query );


		$query = "SELECT value FROM `$dbname`.all__configpublic WHERE name = 'page_phone'";

		$agencia['page_phone'] = Db::get_first( $query );


		$query = "SELECT value FROM `$dbname`.all__configpublic WHERE name = 'page_fax'";

		$agencia['page_fax'] = Db::get_first( $query );


		$query   = "SELECT value FROM `$dbname`.all__configadmin WHERE name = 'has_ssl'";
		$has_ssl = Db::get_first( $query );
		$has_ssl = $has_ssl ? 's' : '';

		$domain = $agencia['domain'];
		if ( substr_count( $domain, '.' ) == 1 && ( strpos( $domain, 'www.' ) === false ) ) {
			$domain = 'www.' . $domain;
		}

		$agencia['url'] = "http$has_ssl://" . $domain;


	}

	/**
	 * Comprova si pot exportar per no enviar sempre la exportació asíncrona
	 *
	 * Ara hi ha els portals fotocasa i vreasy
	 *
	 * @return bool
	 */
	public function can_export() {
		return (
			$_SESSION['inmo_fotocasa']
			|| $_SESSION['inmo_vreasy']
		);
	}

	static public function get_agencias( $portal, $agencia_id ) {

		$query = "SELECT client_id, comercialname, nif, province, town, typestreet, adress, numstreet, zip, respname, surnames, phone1, fax, mail1, domain, dbname, client_dir, inmo_fotocasa_user, inmo_fotocasa_pass, inmo_fotocasa_id_agencia, inmo_fotocasa_id_agente, inmo_vreasy_user, inmo_habitaclia_user FROM letnd.client__client WHERE $portal = 1";

		if ( $agencia_id ) {
			$query .= " AND client_id = " . $agencia_id;
		}

		$agencias = Db::get_rows( $query );

		return $agencias;
	}

	public static function get_ref( $ref, $rs, $agencia_rs ){

		$category_id = $rs['category_id'];

		$dbname   = $agencia_rs['dbname'];
		$auto_ref   = (bool) $agencia_rs['property_auto_ref'];

		if ( !$auto_ref ) return $ref;

		$query    = "SELECT ref_code
				FROM `$dbname`.inmo__category
				WHERE category_id = $category_id";
		$ref_code = Db::get_first( $query );
		$ref_code .= $ref;

		return $ref_code;
	}

	public static function p( $txt, $level ) {

		if ( $level == 2 ) {
			self::$conta ++;
			$txt = '<em>' . self::$conta . '</em> ' . $txt . ' - <i>(' . $execution_time = Debug::get_execution_time() . ' s.) </i>';
		}
		if ( $level == 'error' ) {
			$txt .= ' (error)';
		}
		echo '<p class="m' . $level . '">' . $txt . '</p>';
		//@ob_flush();
		if ( self::$conta % 1000 == 0 && self::$conta != 0 ) {
			flush();
		}
	}

	public static function dump($obj){
		echo "<pre>" . var_export($obj, true) . "</pre>";
	}


	public static function show_end() {
		ExportCommon::p( 'Exportació finalitzada  - <i>' . self::$conta . ' operacions en ' . $execution_time = Debug::get_execution_time() . ' segons </i>', 1 );
		echo "
		<script>
			a = false;
		</script></body></html>";
	}

	public static function end( &$module ) {

		$ids = $module->{$module->section . '_ids'};

		if ($ids) self::mark_for_async( $ids, $module->section, '1', $module->agencia_rs['dbname']);

		$agencia_id = $module->agencia_id;
		$response_errors = $module->response_errors;
		$exporter = strtoupper( $module->exporter );

		$body = 'Data: ' . now( true ) . "<br>Host: " . HOST_URL;

		//if ( !$GLOBALS['gl_is_local'] ) {
		if ( $response_errors ) {
			$body .= "<br><br><pre>" . print_r( $response_errors, true ) . "</pre>";
		}

		if ( $agencia_id ) {
			$body .= "<br><br>" . $agencia_id . "</pre>";
		}
		$body .= "<br><br>Action:" . $GLOBALS['gl_action'];
		$body .= "<br><br>Process:" . $GLOBALS['gl_process'];


		send_mail_admintotal( "$exporter - Exportació finalitzada", $body );

	}


	public static function log_json( $api_url, $json_sent, $json_response, &$agencia_rs, $portal ) {

		$client_id     = $agencia_rs['client_id'];
		$comercialname = $agencia_rs['comercialname'];

		$json_sent     = Db::qstr( $json_sent );
		$json_response = Db::qstr( json_encode($json_response) );
		$comercialname = Db::qstr( $comercialname );

		$query = "INSERT INTO letnd.export__${portal}_json_log (api_url, json_sent, json_response, client_id, comercialname) VALUES ('$api_url', $json_sent, $json_response, '$client_id', $comercialname);";
		Db::execute( $query );
	}


	/**
	 * ExportCommon::trace()
	 * Crea un arxiu de log
	 *
	 * @param $text
	 * @param string $name
	 * @param bool $title
	 * @param $file
	 */
	static public function trace($text, $name = '', $title = false, $file = 'export'){

		if (!CLIENT_PATH) return;

		$fp = fopen (CLIENT_PATH . '/temp/'.$file.'.log', "a+");

		$log = '';
		if ($title){
			$log .="\n\n\n================================================\n" . $title . ' - ' .date("Y-m-d H:i:s"). " - " . $_SERVER['REMOTE_ADDR'] . "\n================================================\n";
		}
		if ($name){
			$name .= ": ";
		}
		$log .= "\n" . $name . $text;

		fwrite($fp,$log);
		fclose($fp);
	}

}