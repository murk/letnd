<?

/**
 * InvoiceRecurring
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2011
 * @version $Id$
 * @access public
 */
class InvoiceRecurring extends Module {

	var $recurring_order = "DAY(bill_date) ASC, DAY(bill_due) ASC, recurring_id ASC";

	function __construct() {
		parent::__construct();
	}

	function list_records() {
		$GLOBALS['gl_content'] = $this->get_records();
	}

	function get_records() {

		/*
		 TODO-i, posar filtres per poder veure per paquets i gual que ala fulla i poder comprovar si està bé cada import

		SELECT
  quantity, concept, price,
  ( SELECT comercial_name FROM custumer__custumer WHERE custumer__custumer.custumer_id = invoice__recurring.custumer_id ) as comercial
FROM
  invoice__recurringconcept,
  invoice__recurring
WHERE
  invoice__recurringconcept.recurring_id = invoice__recurring.recurring_id
  AND invoice__recurringconcept.recurring_id
  AND concept_id = 9
  AND bin = 0
ORDER BY comercial;
		 */



		$this->set_news();

		$listing = new ListRecords( $this );

		$listing->order_by = $this->recurring_order;

		return $listing->list_records();
	}

	function show_form() {
		$GLOBALS['gl_content'] = $this->get_form();
	}

	function get_form() {
		$show = new ShowForm( $this );

		return $show->show_form();
	}

	function save_rows() {
		$save_rows = new SaveRows( $this );
		$save_rows->save();
	}

	function write_record() {
		$writerec = new SaveRows( $this );
		$writerec->save();
	}

	public function get_sublist_html( $id, $field, $is_input ) {

		return InvoiceCommon::get_concept_sublist_html( $id, $field, $is_input, $this );

	}

	// ajax
	public function get_sublist_item() {

		InvoiceCommon::get_concept_sublist_item( $this );

	}

	// Resultats per el autocomplete
	public function get_checkboxes_long_values() {

		InvoiceCommon::get_concept_checkboxes_long_values();

	}

	// Clients
	public function get_select_long_values() {

		InvoiceCommon::get_custumer_select_long_values();

	}

	function set_news( $top_date = false ) {

		global $gl_news;
		$total          = 0;
		$total_no_vat   = 0;
		$total_concepts = 0;

		$top_date = $top_date ? " AND invoice__recurring.bill_date = '$top_date'" : '';

		$language  = LANGUAGE;
		$categorys = Db::get_rows( "
				SELECT DISTINCT (invoice__concept.category_id), invoice__category_language.category AS category FROM invoice__recurring, invoice__recurringconcept, invoice__concept, invoice__category_language 
					WHERE invoice__recurringconcept.recurring_id = invoice__recurring.recurring_id
					AND invoice__recurring.bin = 0
					$top_date
					AND invoice__recurringconcept.concept_id = invoice__concept.concept_id 
					AND invoice__concept.category_id = invoice__category_language.category_id 
					AND language = '$language'
					ORDER BY invoice__category_language.category;" );

		foreach ( $categorys as $category ) {

			$category_id = $category['category_id'];
			$category    = $category['category'];

			$gl_news .= "
			<div class=\"missatge-titol\"><strong>$category</strong></div>";

			$concepts = Db::get_rows( "
					SELECT DISTINCT (invoice__concept.concept_id), invoice__concept_language.concept AS concept FROM invoice__recurring, invoice__recurringconcept,invoice__concept, invoice__concept_language 
						WHERE invoice__recurringconcept.recurring_id = invoice__recurring.recurring_id
						AND invoice__recurring.bin = 0
						$top_date
						AND invoice__recurringconcept.concept_id = invoice__concept.concept_id 
						AND invoice__concept.concept_id = invoice__concept_language.concept_id
						AND language = '$language' 
						AND invoice__concept.category_id = $category_id 
						ORDER BY invoice__concept_language.concept;" );

			foreach ( $concepts as $concept ) {

				$concept_id = $concept['concept_id'];
				$concept    = $concept['concept'];

				$rs = Db::get_row( "
					SELECT sum(price*quantity) AS total_no_vat, count(*) as category_concepts 
						FROM invoice__recurringconcept, invoice__recurring
						WHERE invoice__recurringconcept.recurring_id = invoice__recurring.recurring_id
						AND invoice__recurringconcept.recurring_id 
						$top_date
						AND concept_id = $concept_id
						AND bin = 0;" );

				$total_concept_no_vat = $rs['total_no_vat'];
				$total_concept        = $total_concept_no_vat * 1.21;
				$total_no_vat += $total_concept_no_vat;
				$total += $total_concept;
				$total_concepts += $rs['category_concepts'];
				$category_concepts = $rs['category_concepts'];

				$total_concept_no_vat = format_currency( $total_concept_no_vat );
				$total_concept        = format_currency( $total_concept );

				$gl_news .= "
					<p>$concept ($category_concepts): <strong>$total_concept_no_vat €</strong> - $total_concept € IVA inclòs</p>";

			}
		}
		$total_no_vat = format_currency( $total_no_vat );
		$total        = format_currency( $total );

		$total_invoices = Db::get_first( "
			SELECT count(*) as total_invoices 
				FROM invoice__recurring
				WHERE bin = 0
				$top_date;" );

		$gl_news .= "
			<div class=\"missatge-titol\"><strong>Totals</strong></div>";
		$gl_news .= "
			<p><strong>$total_no_vat €</strong> - $total € IVA inclòs</p>
			<p><strong>Total conceptes</strong> - $total_concepts</p>
			<p><strong>Total factures</strong> - $total_invoices</p>";

	}

	public function generate_invoices() {

		$this->set_vars( $this->caption );

		$generate_top_date = R::escape( 'generate_top_date' );
		$generate_top_date = $generate_top_date?$generate_top_date:now( false, 5 );

		$this->set_var( 'generate_top_date', $generate_top_date );

		// busco les factures i faig report
		$invoices = $this->get_generate_invoices( unformat_date ($generate_top_date) );

		$this->set_vars( $invoices );


		/*$listing = New listRecords($this);
		$listing->results =  $invoices['invoices_before'];
		$invoices_before_listing = $listing->list_records();

		$listing = New listRecords($this);
		$listing->set =  $invoices['invoices_after'];
		$invoices_after_listing = $listing->list_records();

		$this->set_vars( [
			'invoices_before_listing' => $invoices_before_listing,
			'invoices_after_listing' => $invoices_after_listing,
		] );*/


		$this->set_file( 'invoice/recurring_generate_invoices.tpl' );
		$GLOBALS['gl_content'] = $this->process();
	}

	public function generate_invoices_save() {


		$generate_top_date = R::escape( 'generate_top_date', false, '_POST' );

		$invoices = $this->get_generate_invoices( unformat_date ($generate_top_date) );

		$invoices_before = $invoices['invoices_before'];

		foreach ( $invoices_before as $rs ) {

			$recurring_id = $rs['recurring_id'];
			$next_date = $rs['next_date'];
			$next_due = $rs['next_due'];

			// TODO-i Aquí s'hauria de comprobar de no generar factures amb data passada, i que no en tinguem cap que superi en data a les que es generen ( al fer la remesa també s'haurà de comprobar tot això )

			// TODO-i Falta la data d'expiració

			// Copio factures

			// No poso els preus, ja que els posa el trigger al insertar conceptes ( sino els duplica )
			$recurring_fields = "
					`invoice_serial`,
					`custumer_id`,
					`entered`,
					`modified`,
					`billing_type`,
					'$next_date',
					'$next_due',
					`recurring_id`
				";

			$invoice_fields = "
					`invoice_serial`,
					`custumer_id`,
					`entered`,
					`modified`,
					`billing_type`,
					`bill_date`,
					`bill_due`,
					`recurring_id`
				";

			$query = "
				INSERT INTO invoice__invoice ($invoice_fields) 
					SELECT $recurring_fields 
					FROM invoice__recurring 
					WHERE recurring_id = $recurring_id;";
			Db::execute( $query );

			$invoice_id = Db::insert_id();

			// Relaciono factures amb recurring
			$query = "INSERT INTO invoice__invoice_to_recurring ( 
									recurring_id,
									invoice_id,
									original_generated_date) 
									VALUES (
									'$recurring_id',
									'$invoice_id',
									'$next_date');";
			Db::execute( $query );

			// Copio recurring
			// TODO-i Els conceptes poden canviar, s'hauria d'actualitzar de la taula de conceptes abans de copiar
			$recurringconcept_fields = "
					'$invoice_id',
					`concept_id`,
					`quantity`,
					`category`,
					`concept`,
					`price`,
					`cost_price`,
					`ordre`
				";

			$invoiceconcept_fields = "
					`invoice_id`,
					`concept_id`,
					`quantity`,
					`category`,
					`concept`,
					`price`,
					`cost_price`,
					`ordre`
				";

			$query = "
				INSERT INTO invoice__invoiceconcept ($invoiceconcept_fields) 
					SELECT $recurringconcept_fields 
					FROM invoice__recurringconcept 
					WHERE recurring_id = $recurring_id;";

			Db::execute( $query );

		}

		print_javascript('
			top.$("#report").html();
		');

		$GLOBALS['gl_page']->show_message($this->caption['c_generated_correctly']);

		Debug::p_all();
		die();

	}

	public function get_generate_invoices( $generate_top_date ) {

		$results = Db::get_rows( "
			SELECT * FROM invoice__recurring 
			WHERE bin = 0
			ORDER BY $this->recurring_order;");

		$invoices_before       = array();
		$invoices_after = array();

		foreach ( $results as &$rs ) {

			$bill_date  = $rs['bill_date'];
			$bill_due  = $rs['bill_due'];
			$recurring_id = $rs['recurring_id'];

			$rs += $this->get_next_invoice_dates( $bill_date, $bill_due, $recurring_id );

			// top_date està inclosa
			if ( is_date_bigger( $rs['next_date'], $generate_top_date ) ) {
				$invoices_after []= $rs;
			}
			else {
				$invoices_before []= $rs;
			}

		}

		Debug::p( [
					[ 'invoices_before', $invoices_before ],
					[ 'invoices_after', $invoices_after ]
				], 'invoiceRecurring');
		return [
			'invoices_before' => $invoices_before,
			'invoices_after' => $invoices_after,
			'invoices_before_count' => count ($invoices_before),
			'invoices_after_count' => count ($invoices_after),
		];


	}

	// TODO-i De moment tots són mensuals
	/*
		Vigilar això:
		echo date( "Y-m-d", strtotime( "2009-01-31 +1 month" ) ); // PHP:  2009-03-03
		echo date( "Y-m-d", strtotime( "2009-01-31 +2 month" ) ); // PHP:  2009-03-31
	*/

	private function get_next_invoice_dates( $bill_date, $bill_due, $recurring_id ) {

		$bill_date_unix = strtotime( $bill_date );
		$bill_date_day       = strftime( '%d', $bill_date_unix );
		$bill_month     = strftime( '%m', $bill_date_unix );
		$bill_year      = strftime( '%Y', $bill_date_unix );

		$bill_due_unix = strtotime( $bill_due );
		$bill_due_day       = strftime( '%d', $bill_due_unix );

		Debug::p( [
					[ 'bill_date', $bill_date ],
					[ 'bill_day', $bill_date_day ],
				], 'invoiceRecurring');

		// busco l'última factura generada
		$last_invoice = Db::get_first( "
				SELECT original_generated_date
				FROM invoice__invoice_to_recurring
				WHERE recurring_id = $recurring_id
				ORDER BY original_generated_date DESC
				LIMIT 1" );

		if ( $last_invoice ) {
			$last_invoice_unix = strtotime( $last_invoice );

		} else {
			$last_invoice_unix = strtotime( "now" );
		}

		// $last_invoice_day   = strftime( '%e', $last_invoice_unix );
		$last_invoice_month = strftime( '%m', $last_invoice_unix );
		$last_invoice_year  = strftime( '%Y', $last_invoice_unix );

		if ( $last_invoice ) {

			// Si ja se n'ha generat alguna, simplement poso la mateixa data ( canviant el dia per si s'ha canviat dia de factura, i li sumo un mes
			$next_date = strtotime("$last_invoice_year-$last_invoice_month-$bill_date_day +1 month" );
			$next_due = strtotime("$last_invoice_year-$last_invoice_month-$bill_due_day +1 month" );

		} else {

			// Si no s'ha generat mai, genero amb data actual canviant el numero del mes
			$next_date = strtotime( "$last_invoice_year-$last_invoice_month-$bill_date_day" );
			$next_due = strtotime( "$last_invoice_year-$last_invoice_month-$bill_due_day" );

			// Si la data es inferior a avui, genero mes següent

			// TODO-i Que passa si la genero el dia 1, no me la farà fins el més vinent!
			if ($next_date < $last_invoice_unix){
				$next_date = strtotime( "$last_invoice_year-$last_invoice_month-$bill_date_day +1 month" );
				$next_due = strtotime( "$last_invoice_year-$last_invoice_month-$bill_due_day +1 month" );
			}
		}

		return [
			'next_date' => time_to_date( $next_date ),
			'next_due' => time_to_date( $next_due ),
		];

	}

}