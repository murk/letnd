<?
/**
 * ProductFamilybusy
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) Juliol 2014
 * @version $Id$
 * @access public
 */
class ProductFamilybusy extends Module{
	
	var $show_booking, $show_results, $url, $one_property, $propertys, $property_id, $is_ajax = false, $reserved_message, $price_module, $form_template = '', $is_property_book = false, $is_book_edit = true, $is_add_custumers = false;
	
	function __construct(){
		parent::__construct();
		
	}
	function get_calendar($family_id, $product_id) {

		Main::load_class( 'product', 'calendar', 'admin' );

		ProductCalendar::set_calendar_vars( $this, $family_id, $product_id );

		$calendar = New ProductCalendar($this->process,true, $family_id);
		return $calendar->get_calendar_array($product_id);
	}

	/*
	 * Calendari
	 */
	
	public function get_month() {

		Main::load_class( 'product', 'calendar', 'admin' );

		$calendar = New ProductCalendar($this->process);
		
		return $calendar->get_month();
	}
}
?>