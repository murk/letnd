<?php
global $gl_version, $gl_config, $gl_sentence_num, $gl_error_mail, $gl_all_errors, $gl_last_error_handle, $gl_is_local;

Debug::add('Versió de letnd',$gl_version);
if (file_exists(DOCUMENT_ROOT . '/common/update/' . ($gl_version + 1) . '.php')){
	$gl_sentence_num = 0;
	$gl_error_mail = '';
	$gl_all_errors = '';
	$gl_last_error_handle = set_error_handler("gl_on_error");
    gl_update_new_version();
	restore_error_handler();
}

// Funcio per actualitzar automaticament les bases de dades
// s'executa el primer cop d'entrar a la web
// parteix les consultes per els punts i comes, per tant no pot haver punt i coma enlloc més
// els arxius d'actualització han des ser correlatius
function gl_update_new_version()
{
    global $gl_version, $gl_sentence_num, $gl_all_errors, $gl_error_mail;
    $update_dir = DOCUMENT_ROOT . '/common/update/';
    Debug::p('', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;S\'HAN TROBAT NOVES VERSIONS DE LETND');
    while (file_exists($update_dir . (++$gl_version) . '.php'))
    {
        Debug::p($gl_version / 1000, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Instal·lant versió');
        $update_sql = [];
        $gl_sentence_num = $gl_error_mail = '';
        include ($update_dir . ($gl_version) . '.php');
        if ($update_sql)
        {   // si ja es un array no faig explode
            if (is_array($update_sql)){
            	$update_arr = $update_sql;
			}
			else{
				$update_sql = trim($update_sql);
				$update_sql = trim($update_sql, ';');
				$update_arr = explode(";", $update_sql);
			}
            foreach ($update_arr as $query)
            {
                gl_update_execute_query($query);
            }
            if ($gl_error_mail)
                $gl_all_errors .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong><span style="color:#900">Versió:</span> ' . ($gl_version / 1000) . '</strong><br><br>' . $gl_error_mail;
        }
    }
	$gl_version--;

    Debug::p($gl_version / 1000, "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NOVA VERSIÓ");
    mysqli_query(Db::cn(), "UPDATE all__configadmin SET value = '" . ($gl_version) . "' WHERE name = 'version'");

    if ($gl_all_errors)
    {

        Db::connect_mother();
        $client = mysqli_fetch_array(mysqli_query(Db::cn(), 'SELECT fiscalname FROM client__client WHERE client_id = ' . $_SESSION['client_id']));
        Db::reconnect();
        $gl_all_errors = '<b style="color:#900">CLIENT:</b> ' . $client['fiscalname'] . '&nbsp;<b style="color:#900">URL:</b> ' . HOST_URL . '<br><br>'.$_SERVER["SERVER_SOFTWARE"].'<br>' . $gl_all_errors;

	    if (!$GLOBALS['gl_is_local']) {
	        /*ob_start();
		    phpinfo();
		    $info = ob_get_contents();
		    ob_end_clean();*/

		    $new_host = substr( HOST_URL, strpos( HOST_URL, '://' ) + 3, - 1 );

		    $sufix    = $new_host . '_' . date( "d-m-Y_G.i.s" );
		    $log_file = "/var/www/letnd.com/datos/motor/data/logs/error_actualitzacions_$sufix.htm";
		    file_put_contents( $log_file, $gl_all_errors );
	        send_mail_admintotal ("Letnd - Actualitzacions erronees - " . DOCUMENT_ROOT, $gl_all_errors);
	    }


    }
}

function gl_update_execute_query($query){
	
	$result = mysqli_query(Db::cn(), $query);
	if (!$result)
	{
		gl_print_error_debug ('Error en la sentencia', 'Query', $query, mysqli_error(Db::cn()));
	}
	else
	{
		gl_print_ok_debug($query);
	}
}


function gl_print_error_debug ($message, $type, $sentence, $error){
	global $gl_error_mail, $gl_sentence_num;
	$gl_sentence_num++;
	$t = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>'. $type . ':</b> ' . $sentence . '<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Error:</b> ' . $error . '<br>';
	Debug::p('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>' . $gl_sentence_num . '</b> - ' . $message, '', false, true, '100');
	Debug::p($t, '', false, true, '100');
	$gl_error_mail .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="color:#900">'.$message.':</span> ' . $gl_sentence_num . '<br><br>' . $t . '<br><br>';
}
function gl_print_ok_debug($sentence){
	global $gl_error_mail, $gl_sentence_num;
	$gl_sentence_num++;
	Debug::p("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>" . $gl_sentence_num . '</b> - ' . trim($sentence) . "<br>", '', false, true, '100');
}
// Gestió d'errors'
function gl_on_error($num_err, $cadena_err, $archivo_err, $linea_err)
{
	switch ($num_err) {
    case E_WARNING:
        $type = 'E_WARNING';
        break;
    case E_NOTICE:
        $type = 'E_NOTICE';
        break;
    case E_USER_ERROR:
        $type = 'E_USER_ERROR';
        break;

    case E_USER_WARNING:
        $type = 'E_USER_WARNING';
        break;

    case E_USER_NOTICE:
        $type = 'E_USER_NOTICE';
        break;

    default:
        $type = 'DESCONEGUT';
        break;
    }

    gl_print_error_debug ('Error PHP', $type, '['.$num_err.'] '.$cadena_err, 'Linea '. $linea_err . ' , arxiu ' . $archivo_err);
    return true;
}
?>