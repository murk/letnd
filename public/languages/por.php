<?
// blocks

define('INMO_PRICE_FIRST','Hasta %s&nbsp;'.CURRENCY_NAME);
define('INMO_PRICE_MIDDLE','De %s a %s&nbsp;'.CURRENCY_NAME);
define('INMO_PRICE_LAST','Más de %s&nbsp;'.CURRENCY_NAME);
define('INMO_FLOOR_SPACE_FIRST','Hasta %s m<sup>2</sup>');
define('INMO_FLOOR_SPACE_MIDDLE','De %s a %s m<sup>2</sup>');
define('INMO_FLOOR_SPACE_LAST','Más de %s m<sup>2</sup>');
define('INMO_PERSON_FIRST','Hasta %s');
define('INMO_PERSON_MIDDLE','De %s a %s');
define('INMO_PERSON_LAST','Más de %s');
define ('LETND','Letnd');

$gl_caption['c_offers'] = 'Ofertas';


// captions

$gl_caption['c_send_mail'] = 'enviar mail';
$gl_caption['c_more_info'] = 'Más info';
$gl_caption['c_read_more'] = 'Leer más';
$gl_caption['c_title_search'] = 'buscar';
$gl_caption['c_yes'] = 'Sí';
$gl_caption['c_no'] = 'No';
$gl_caption['c_send'] = 'Enviar';
$gl_caption['c_clear'] = 'Borrar';
$gl_caption['c_send_to_friend']='enviar a un amigo';
$gl_caption['c_print']='imprimir';
$gl_caption['c_zoom_link']='ampliar imágenes';
$gl_caption['c_contact_link']='contactar';

// bloc inmo_categories
$gl_caption['c_sell'] = 'Venta';
$gl_caption['c_rent'] = 'Alquiler';
$gl_caption['c_temp'] = 'Alquiler turístico';
$gl_caption['c_moblat'] = 'Alquiler con muebles';
$gl_caption['c_selloption'] = 'Alquiler opción compra';
// seo
$gl_caption['c_seo_verd_1']='<a href="http://www.letnd.com" target="_blank" title="Diseño páginas web en Girona">Diseño y programación web:<img src="/common/images/logoletnd.png" width="95" height="16" alt="Letnd: Diseño web, posicionamiento web, páginas web en Girona" style="vertical-align:text-bottom" /></a>';
$gl_caption['c_seo_verd_2']='<a href="http://www.letnd.com" target="_blank" title="Diseño páginas web en Girona">Programación web:<img src="/common/images/logoletnd.png" width="95" height="16" alt="Letnd: Diseño web, posicionamiento web, páginas web en Girona" style="vertical-align:text-bottom" /></a>';
$gl_caption['c_seo_verd_3']='<a href="http://www.letnd.com" target="_blank" title="Diseño páginas web en Girona">Creado por:<img src="/common/images/logoletnd.png" width="95" height="16" alt="Letnd: Diseño web, posicionamiento web, páginas web en Girona" style="vertical-align:text-bottom" /></a>';
$gl_caption['c_seo_verd_4']='<a href="http://www.letnd.com" target="_blank" title="Diseño páginas web en Girona">Diseño web:<img src="/common/images/logoletnd.png" width="95" height="16" alt="Letnd: Diseño web, posicionamiento web, páginas web en Girona" style="vertical-align:text-bottom" /></a>';
$gl_caption['c_seo_blanc_1']='<a href="http://www.letnd.com" target="_blank" title="Diseño páginas web en Girona">Diseño y programación web:<img src="/common/images/logoblanc.png" width="95" height="16" alt="Letnd: Diseño web, posicionamiento web, páginas web en Girona" style="vertical-align:text-bottom" /></a>';
$gl_caption['c_seo_blanc_2']='<a href="http://www.letnd.com" target="_blank" title="Diseño páginas web en Girona">Programación web:<img src="/common/images/logoblanc.png" width="95" height="16" alt="Letnd: Diseño web, posicionamiento web, páginas web en Girona" style="vertical-align:text-bottom" /></a>';
$gl_caption['c_seo_blanc_3']='<a href="http://www.letnd.com" target="_blank" title="Diseño páginas web en Girona">Creado por:<img src="/common/images/logoblanc.png" width="95" height="16" alt="Letnd: Diseño web, posicionamiento web, páginas web en Girona" style="vertical-align:text-bottom" /></a>';
$gl_caption['c_seo_blanc_4']='<a href="http://www.letnd.com" target="_blank" title="Diseño páginas web en Girona">Diseño web:<img src="/common/images/logoblanc.png" width="95" height="16" alt="Letnd: Diseño web, posicionamiento web, páginas web en Girona" style="vertical-align:text-bottom" /></a>';

$gl_caption['c_seo_custom_2']='<a href="http://www.letnd.com" target="_blank" title="Diseño páginas web en Girona">Programación web:<img src="/'.CLIENT_DIR.'/templates/images/letnd.png" alt="Letnd: Diseño web, posicionamiento web, páginas web en Girona" /></a>';
$gl_caption['c_seo_custom_4']='<a href="http://www.letnd.com" target="_blank" title="Diseño páginas web en Girona">Diseño web:<img src="/'.CLIENT_DIR.'/templates/images/letnd.png" alt="Letnd: Diseño web, posicionamiento web, páginas web en Girona" /></a>';

$gl_caption['c_tercersetena'] = 'Diseño <a class="tercersetena" target="_blank" href="http://www.tercersetena.com">Tercersetena.com</a>';


// NO TRADUIT
$gl_caption['c_tipus_temp'] = 'Alquiler de temporada';
$gl_caption['c_tipus_moblat'] = 'Alquiler con muebles';
// FI NO TRADUIT

// TAMBÉ A ADMIN
// Caption default
$gl_caption['c_form_title'] = 'Editar datos';
$gl_caption['c_config_title'] = 'Editar configuración';
$gl_caption['c_edit'] = 'Editar';


$gl_caption['c_edit_button'] = 'Editar datos';
$gl_caption['c_view_button'] = 'Ver datos';
$gl_caption['c_edit_images_button'] = 'Editar imágenes';
$gl_caption['c_add_images_button'] = 'Añadir imágenes';



$gl_caption['c_edit_data'] = 'Editar datos';
$gl_caption['c_edit_images'] = 'Editar imágenes';
$gl_caption['c_add_images'] = 'Añadir imágenes';
$gl_caption['c_search_form'] = 'Buscar';
$gl_caption['c_send_new'] = 'Guardar';
$gl_caption['c_send_edit'] = 'Guardar cambios';
$gl_caption['c_delete'] = 'Eliminar';
$gl_caption['c_action'] = 'Acción';
$gl_caption['c_delete_selected'] = 'Borrar seleccionados';
$gl_caption['c_restore_selected'] = 'Recuperar seleccionados';
$gl_caption['c_confirm_deleted'] = 'Los elementos seleccionados se eliminarán definitivamente de la base de datos \\n\\n¿Desea continuar?';
$gl_caption['c_confirm_images_deleted'] = 'Las imágenes seleccionadas se eliminarán definitivamente \\n\\n¿Desea continuar?';
$gl_caption['c_confirm_form_bin'] = 'El registro se enviará a la papelera de reciclaje \\n\\n¿Desea continuar?';
$gl_caption['c_confirm_form_deleted'] = 'El registro se eliminará definitivamente de la base de datos \\n\\n¿Desea continuar?';
$gl_caption['c_move_to'] = 'Mover a:';
$gl_caption['c_print_all'] = 'Imprimir todo';
$gl_caption['c_print'] = 'Imprimir';
$gl_caption['c_order_by'] = 'Ordenar por:';
$gl_caption['c_select'] = 'Seleccionar';
$gl_caption['c_all'] = 'todo';
$gl_caption['c_nothing'] = 'nada';
$gl_caption['c_preview'] = 'Previsualizar';
$gl_caption['c_yes'] = 'Sí';
$gl_caption['c_no'] = 'No';
$gl_caption['c_options_bar'] = 'Opciones';
$gl_caption['c_file_maximum'] = 'máximo %s archivos a la vez';
$gl_caption['c_file_maximum_1'] = 'máximo 1 archivo a la vez';


$gl_caption['c_form_sent'] = 'Muito obrigado pelo seu contato. Em breve estaremos respondendo à sua mensagem.
';
$gl_caption['c_form_not_sent'] = 'O formulário não pode ser enviado, verifique se o endereço de e-mail que você digitou está correto.
';

$gl_caption['c_cookies_message'] = 'Este site usa cookies próprios e de terceiros para melhorar a experiência de navegação, assim como para tarefas de análise. Ao continuar navegando, entendemos que você aceita o';
$gl_caption['c_cookies_message_more'] = 'uso de cookies';
$gl_caption['c_cookies_message_accept'] = 'Eu aceito';

$gl_caption['c_cookies_text'] = '

<p class="accept-cookies-title">O que são cookies?
?</p>
<p>Um cookie é um arquivo pequeno que é armazenado no computador do usuário e nos permite reconhecê-lo. O conjunto de cookies nos ajuda a melhorar a qualidade de nosso site, o que nos permite monitorar as páginas que são úteis, e quais não são úteis para os nossos usuários.</p>
<p>Os cookies são essenciais para o funcionamento da Internet, proporcionando muitas vantagens na prestação de serviços interativos, facilitando a navegação e uso do nosso site. Por favor note que os cookies não podem danificar o seu computador, e caso seja ativado nos ajudará a identificar e resolver possíveis erros.</p>
<p class="accept-cookies-title">Que tipo de cookies são utilizados neste site?</p>
<p>Cookies próprios: são aqueles que são enviados ao computador do usuário a partir de um computador ou domínio administrado pelo editor e a partir do qual o serviço solicitado pelo usuário é fornecido.</p>
<p>Os cookies de terceiros: são aqueles enviados para o computador do usuário a partir de um computador ou domínio não administrado pelo publisher, mas por outra entidade que analisa os dados obtidos.
</p>
<p>Os cookies persistentes: são um tipo de cookie no qual os dados são armazenados no terminal e podem ser acessados e processados pelo responsável do cookie.
</p>
<p>Cookies da análise do site: através da análise do site é obtida informação referente ao número de usuários que acessam o site, o número de visualizações, a freqüência e repetição das visitais, a sua duração, o navegador utilizado, o operador que fornece o serviço, a língua, o terminal utilizado ou a cidade em que o endereço IP é atribuído. Informação que permite um serviço melhor e mais apropriado deste site.
</p>
<p class="accept-cookies-title">Aceitação do uso de cookies.
.</p>
<p>Nós assumimos que você aceita o uso de cookies. No entanto você pode restringir, bloquear ou apagar os cookies deste site ou qualquer outra página a partir da configuração do seu navegador. 
.</p>
<p>A seguir você terá as páginas de suporte dos principais navegadores:
:</p>
<ul>
<li>Internet Explorer: <a href="http://windows.microsoft.com/es-xl/internet-explorer/delete-manage-cookies#ie="ie-10"" target="_blank">windows.microsoft.com/es-xl/internet-explorer/delete-manage-cookies#ie="ie-10"</a></li>
<li>FireFox: <a href="http://support.mozilla.org/es/kb/Borrar%20cookies" target="_blank">support.mozilla.org/es/kb/Borrar%20cookies</a></li>
<li>Chrome: <a href="http://support.google.com/chrome/answer/95647?hl="es"" target="_blank">support.google.com/chrome/answer/95647?hl="es"</a></li>
<li>Safari: <a href="http://www.apple.com/es/privacy/use-of-cookies/" target="_blank">www.apple.com/es/privacy/use-of-cookies/</a></li>
</ul>
';

// contrasenyes
$gl_caption['c_password_forgotten'] = '¿Ha olvidado su contraseña?';
$gl_caption['c_password_insert_mail'] = 'Por favor, introduzca su dirección de email y le enviaremos una contraseña nueva a su cuenta de correo.';
$gl_caption['c_password_insert_mail_exists'] = 'Si ha olvidado su contraseña, por favor, clique aceptar y le enviaremos una contraseña nueva a su cuenta de correo.';
$gl_caption['c_password_mail_error'] = 'La dirección de email proporcionada no existe en nuestra base de datos, por favor, escriba la dirección correctamente';
$gl_caption['c_password_sent'] = 'Se ha enviado una nueva contraseña a su dirección de email';
$gl_caption['c_password_mail_subject'] = 'Su nueva contraseña';
$gl_caption['c_password_mail_text_1'] = 'Su nueva contraseña es';
$gl_caption['c_password_mail_text_2'] = 'Puede cambiar la contraseña en nuestra página web';
$gl_caption['c_welcome'] = 'Bienvenido';

$gl_caption['c_new_custumer_exists'] = 'Ya ha entrado los datos de una persona con este nombre y apellidos o fecha de nacimiento, puede:';
$gl_caption['c_new_custumer_exists_change_name'] = 'Corregir nombre o fecha de nacimiento';
$gl_caption['c_new_custumer_exists_return'] = 'Volver a la reserva y seleccionar persona de la lista';

// captcha
$gl_caption['c_recaptcha_error'] = 'Por favor valide el "CAPTCHA" correctamente';
?>