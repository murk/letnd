<?
/*
26-06-2019
Afegeix estat de comanda
*/

if ( is_table( 'product__order' ) ) {

	$update_sql [] = "
					ALTER TABLE `product__order`
							CHANGE COLUMN `order_status` `order_status` ENUM('paying','failed','deposit','pending','denied','voided','refunded','completed','preparing','shipped','delivered','collect') NOT NULL COLLATE 'utf8_spanish2_ci';
					";

}