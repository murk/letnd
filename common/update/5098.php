<?

/* 27-10-14 
  ACtualitzacions product QR

 */

if (is_table('product__configadmin') && !Db::get_first("SELECT * FROM product__configadmin WHERE name='qr_width'" )) {	
	
	$update_sql []= "
		INSERT INTO product__configadmin (`name`,value) VALUES ('qr_width','2');
		";	
	
	$update_sql []= "
		INSERT INTO product__configadmin (`name`,value) VALUES ('qr_height','2');
		";	
	
	$update_sql []= "
		INSERT INTO product__configadmin (`name`,value) VALUES ('qr_margin_top','0.2');
		";	
	
	$update_sql []= "
		INSERT INTO product__configadmin (`name`,value) VALUES ('qr_margin_left','0.2');
		";	
}
else{
	if (is_table('product__configadmin')) {		
		trigger_error('Ja existeixen el name qr_width');
	}
}

/*


Trec el config de colors d'inmo, va amb taula al final
 */

if (is_table('booking__configadmin')) {	
	
	$update_sql []= "
		DELETE FROM `booking__configadmin` WHERE  `name`='season_colors';
		";
}

?>