<?php
/* 17-8-17
producte nou

*/

if ( is_table( 'product__product' ) && ! is_field( 'product__product', 'is_new_product' ) ) {

	$update_sql[] = "ALTER TABLE `product__product`
	ADD COLUMN `is_new_product` TINYINT(1) NOT NULL DEFAULT '0' AFTER `destacat`;";

} else {
	if ( is_table( 'product__product' ) ) {
		trigger_error( 'Ja existeixen el camp is_new_product' );
	}
}

?>