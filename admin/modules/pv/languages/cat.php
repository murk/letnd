<?php
// menus eina
if (!defined('PV_MENU_REQUISIT_BIN')) {
	define( 'PV_MENU_REQUISIT_BIN', 'Paperera de reciclatge' );
	define( 'PV_MENU_REQUISIT', 'Requisits' );
	define( 'PV_MENU_REQUISIT_NEW', 'Entrar nou requisit' );
	define( 'PV_MENU_REQUISIT_LIST', 'Llistar requisits' );
	define( 'PV_MENU_NORMA_BIN', 'Paperera de reciclatge' );
	define( 'PV_MENU_NORMA_LIST', 'Llistar normes' );
	define( 'PV_MENU_NORMA_NEW', 'Entrar nova norma' );
	define( 'PV_MENU_NORMA', 'Normes - Evolucions' );
	define( 'PV_MENU_SEGUIMENT_BIN', 'Paperera de reciclatge' );
	define( 'PV_MENU_SEGUIMENT_LIST', 'Llistar seguiments' );
	define( 'PV_MENU_SEGUIMENT_NEW', 'Entrar nou seguiment' );
	define( 'PV_MENU_SEGUIMENT', 'Seguiments reglamentaris' );
	define( 'PV_MENU_SEGUIMENTITEM', 'Seguiment client' );
	define( 'PV_MENU_CATEGORIA', 'Tipus' );
	define( 'PV_MENU_CATEGORIA_NEW', 'Entrar nou tipus' );
	define( 'PV_MENU_CATEGORIA_LIST', 'Llistar tipus' );
	define( 'PV_MENU_DOC', 'Ajuda' );
	define( 'PV_MENU_DOC_LIST', 'Guia' );
	define( 'PV_MENU_DOC2_LIST', 'Per fer' );
	define( 'PV_MENU_CLIENT_BIN', 'Paperera de reciclatge' );
	define( 'PV_MENU_CLIENT_LIST', 'Llistar clients' );
	define( 'PV_MENU_CLIENT_NEW', 'Entrar nou client' );
	define( 'PV_MENU_CLIENT', 'Clients' );

	define( 'PV_MENU_ADR_BIN', 'Paperera de reciclatge' );
	define( 'PV_MENU_ADR_LIST', 'Llistar productes' );
	define( 'PV_MENU_ADR_NEW', 'Entrar nou producte' );
	define( 'PV_MENU_ADR', 'Productes' );

	define( 'PV_MENU_EXPEDICIO_BIN', 'Paperera de reciclatge' );
	define( 'PV_MENU_EXPEDICIO_LIST', 'Llistar expedicions' );
	define( 'PV_MENU_EXPEDICIO_NEW', 'Entrar nova expedició' );
	define( 'PV_MENU_EXPEDICIO', 'Expedicions' );

	define( 'PV_MENU_DESTINATARI_LIST', 'Llistar destinataris' );
	define( 'PV_MENU_DESTINATARI_NEW', 'Entrar nou destinatari' );
	define( 'PV_MENU_DESTINATARI', 'Destinataris' );

	define( 'PV_MENU_CONDUCTOR_LIST', 'Llistar conductors' );
	define( 'PV_MENU_CONDUCTOR_NEW', 'Entrar nou conductor' );
	define( 'PV_MENU_CONDUCTOR', 'Conductors' );
}

// captions seccio
$gl_caption['c_client_id'] = 'Client';
$gl_caption['c_client_id2'] = 'Id Client';
$gl_caption['c_client'] = 'Nom client';
$gl_caption['c_can_edit_seguiment'] = 'Edita seguiment';
$gl_caption['c_can_edit_seguiment_0'] = 'No';
$gl_caption['c_can_edit_seguiment_1'] = 'Sí';
$gl_caption['c_has_seguiment'] = 'Seguiment';
$gl_caption['c_has_seguiment_0'] = 'No';
$gl_caption['c_has_seguiment_1'] = 'Sí';
$gl_caption['c_has_adr'] = 'Adr';
$gl_caption['c_has_adr_0'] = 'No';
$gl_caption['c_has_adr_1'] = 'Sí';
$gl_caption['c_has_gmao'] = 'Manteniment';
$gl_caption['c_has_gmao_0'] = 'No';
$gl_caption['c_has_gmao_1'] = 'Sí';
$gl_caption['c_canviar_client'] = 'Canviar client ...';
$gl_caption['c_titol_canviar_client'] = 'Escull un client per continuar';
$gl_caption['c_email'] = "Email";
$gl_caption['c_email2'] = "Email 2";
$gl_caption['c_email_admin'] = "Email administrador";

$gl_caption['c_select_client_button'] = 'Nom client';
$gl_caption_seguiment['c_codi_seguiment'] = 'Codi segiment';

$gl_caption_seguiment['c_view_segimentitem_button'] = 'Veure seguiment';
$gl_caption_seguiment['c_view_segimentitem_inactive_button'] = 'Veure inactius';


$gl_caption['c_norma'] = 'Norma';
$gl_caption['c_parent_norma'] = 'Norma';
$gl_caption['c_evolucio'] = 'Evolució';
$gl_caption['c_titol_norma'] = 'Títol textos';
$gl_caption['c_descripcio_norma'] = 'Resum';
$gl_caption['c_nota_norma'] = 'Notes aclaratories';
$gl_caption['c_codi_lloc'] = 'Codi lloc';
$gl_caption['c_codi_lloc_ESP'] = 'ESP';
$gl_caption['c_codi_lloc_CAT'] = 'CAT';
$gl_caption['c_codi_lloc_EUR'] = 'EUR';
$gl_caption['c_codi_lloc_MUN'] = 'MUN';
$gl_caption['c_codi_lloc_NAV'] = 'NAV';
$gl_caption['c_categoria_id'] = 'Tipus';
$gl_caption_norma['c_numero'] = 'Número';
$gl_caption['c_entered'] = 'Data';
$gl_caption_norma['c_nivell'] = 'Nivell';
$gl_caption_norma['c_file'] = 'Arxius';

$gl_caption_norma['c_add_evolucio_button'] = 'Afegir evolució';
$gl_caption_norma['c_add_requisit_button'] = 'Afegir requisit';
$gl_caption_norma['c_list_evolucions_button'] = 'Llistar evolucions';
$gl_caption_norma['c_list_requisits_button'] = 'Llistar requisits';
$gl_caption['c_filter_categoria_id'] = $gl_caption['c_filter_categoria'] = 'Tots els tipus';

$gl_caption_norma['c_evolucions'] = 'Evolucions';
$gl_caption_norma['c_requisits'] = 'Requisits';

$gl_caption['c_codi_categoria'] = 'Codi';
$gl_caption['c_categoria'] = 'Tipus';
$gl_caption['c_ordre'] = 'Ordre';

$gl_caption['c_top_id'] = 'Norma';
$gl_caption['c_num_requisit'] = 'Número';
$gl_caption['c_requisit'] = 'Requisit';
$gl_caption['c_descripcio_requisit'] = 'Descripció';
$gl_caption['c_classificacio'] = 'Classificació';
$gl_caption['c_classificacio_A'] = 'Aplicable - A';
$gl_caption['c_classificacio_I'] = 'Informació - I';
$gl_caption['c_trimestral'] = 'Només al trimestre';
$gl_caption_list['c_trimestral'] = 'Trimestre';
$gl_caption['c_trimestral_0'] = 'No';
$gl_caption['c_trimestral_1'] = 'Sí';

$gl_caption_seguimentitem['c_titol_trimestre'] = 'Seguiment reglamentari';
$gl_caption_seguimentitem['c_titol_index'] = 'Index de textos reglamentaris';


$gl_caption_seguimentitem['c_trimestral_0'] = 'Global';
$gl_caption_seguimentitem['c_trimestral_1'] = 'Trimestral';

$gl_caption['c_evaluacio'] = 'Avaluació';
$gl_caption['c_evaluacio_texte'] = 'Descripció avaluació';
$gl_caption['c_descripcio_requisit'] = 'Requisit';
$gl_caption['c_comentari'] = 'Aplicació';
$gl_caption['c_comentari_client'] = 'Aplicació';
$gl_caption_seguimentfitxa['c_comentari_client'] = 'Aplicació client';
$gl_caption['c_evaluacio_cap'] = '';
$gl_caption['c_evaluacio_conforme'] = 'Conforme';
$gl_caption['c_evaluacio_noconforme'] = 'No conforme';
$gl_caption['c_evaluacio_informacio'] = 'Informació';
$gl_caption['c_evaluacio_noaplica'] = 'No aplica';
$gl_caption['c_obertura'] = 'Obertura';
$gl_caption['c_tancament'] = 'Data tancament';
$gl_caption['c_tancat'] = 'Tancat';
$gl_caption['c_is_actiu'] = 'Actiu';
$gl_caption['c_is_actiu_1'] = 'Actiu';
$gl_caption['c_is_actiu_0'] = 'Inactiu';
$gl_caption['c_has_been_actiu'] = '';
$gl_caption['c_avis'] = "Avís";
$gl_caption['c_filter_avis'] = 'Avisos';
$gl_caption['c_avis_1'] = "Avís";
$gl_caption['c_avis_0'] = "No avís";
$gl_caption['c_avis_data'] = "Data";
$gl_caption['c_avis_texte'] = "";
$gl_caption['c_avis_en_tramit'] = "En tràmit";
$gl_caption['c_avis_subject'] = "Aviso de seguimiento reglamentario";
$gl_caption['c_avis_not_sent'] = "No s'ha pogut enviar l'avís, no té cap email configurat";
$gl_caption['c_avis_not_sent_admin'] = "No s'ha pogut enviar l'avís a l'administrador, no té cap email configurat";

$gl_caption['c_filter_top_id'] = 'Totes les normes';
$gl_caption['c_filter_codi_lloc'] = 'Tots els codis';
$gl_caption['c_filter_evaluacio'] = 'Totes les evaluacions';


$gl_caption_doc['c_doc'] = 'Descripció';
$gl_caption_doc['c_edit_list_button_1'] = 'No editar';
$gl_caption_doc['c_edit_list_button_'] = 'Editar';


$gl_caption_seguimentitem_list['c_titol_norma'] = 'Textos';
$gl_caption_seguimentitem_list['c_classificacio'] = 'Class.';
$gl_caption_seguimentitem_list['c_requisit'] = 'Requisits';
$gl_caption_seguimentitem_list['c_tancament'] = 'Tancament';
$gl_caption_seguimentitem_list['c_arxiu'] = 'Arxiu';
$gl_caption_seguimentitem_list['c_classificacio_A'] = 'A';
$gl_caption_seguimentitem_list['c_classificacio_I'] = 'I';
$gl_caption['c_fitxa'] = 'Fitxa';
$gl_caption_seguimentitem['c_edit_evidencies'] = 'Editar ev.';
$gl_caption_seguimentitem['c_evidencia'] = 'Evidències';

$gl_caption['c_trimestre_all'] = 'Tots els trimestres';
$gl_caption['c_trimestre_1'] = 'Primer trimestre';
$gl_caption['c_trimestre_2'] = 'Segon trimestre';
$gl_caption['c_trimestre_3'] = 'Tercer trimestre';
$gl_caption['c_trimestre_4'] = 'Quart trimestre';

$gl_caption['c_edit_comentari_client'] = 'Editar aplicació client';

$gl_caption['c_add_new_user'] = 'Afegir usuari';
$gl_caption['c_user_ids'] = 'Usuaris';

// buscador
$gl_caption['c_by_ref'] = '( <strong>Text</strong>, <strong>Codi:</strong> "esp ag 1" , "ESP AG" , "AG 2". <strong>Data:</strong> "13/05" , "2013" , "13/05/2013" )';
$gl_caption_requisit['c_by_ref'] = '';


// ADR
$gl_caption['c_codi_producte'] = 'Codi producte';
$gl_caption['c_nom_comercial'] = 'Nom comercial';
$gl_caption_adr['c_numero_onu'] = 'ONU';
$gl_caption['c_designacio'] = 'Designació oficial';
$gl_caption['c_etiqueta'] = 'Etiqueta';
$gl_caption_adr['c_etiqueta_id'] = 'Etiqueta';
$gl_caption['c_adr_to_etiqueta'] = 'Etiqueta';
$gl_caption_adr['c_clase'] = 'Clase';
$gl_caption_adr['c_clase_1'] = '1';
$gl_caption_adr['c_clase_2'] = '2';
$gl_caption_adr['c_clase_3'] = '3';
$gl_caption_adr['c_clase_8'] = '8';
$gl_caption_adr['c_clase_9'] = '9';
$gl_caption_adr['c_embalatge'] = 'Embalatge';
$gl_caption_adr['c_embalatge_null'] = '';
$gl_caption_adr['c_lq'] = 'LQ';
$gl_caption_adr['c_unitats_lq'] = 'Untats LQ';
$gl_caption_adr['c_unitats_lq_l'] = 'Litres';
$gl_caption_adr['c_unitats_lq_kg'] = 'Quilograms';
$gl_caption_adr['c_categoria_transport'] = 'Categoria transport';
$gl_caption_expedicioitem['c_categoria_transport'] = 'Cat.';
$gl_caption_adr['c_categoria_transport_0'] = '0';
$gl_caption_adr['c_categoria_transport_1'] = '1';
$gl_caption_adr['c_categoria_transport_2'] = '2';
$gl_caption_adr['c_categoria_transport_3'] = '3';
$gl_caption_adr['c_categoria_transport_4'] = '4';
$gl_caption_adr['c_tunels'] = 'Túnels';
$gl_caption_adr['c_ip'] = 'IP';
$gl_caption_adr['c_marca_id'] = 'Marca';
$gl_caption['c_form_level1_title_capacitat_envas'] = "Envasos";


// expedicions
$gl_caption['c_destinatari_id'] = "Destinatari";
$gl_caption['c_codi_client'] = "Codi client";
$gl_caption['c_destinatari_id_caption'] = "Cap destinatari";
$gl_caption['c_destinatari_id_format'] = "%s %s";
$gl_caption['c_conductor_id'] = "Transportista";
$gl_caption['c_conductor_id_caption'] = "Cap conductor";
$gl_caption['c_conductor_id_format'] = "%s %s";
$gl_caption['c_expedicioitems'] = "Productes";
$gl_caption['c_data_expedicio'] = "Data expedició";
$gl_caption['c_data_modificacio'] = "Modificat";

// expedició item
$gl_caption['c_adr_id'] = "Producte";
$gl_caption['c_capacitat_envas'] = "Capacitat envàs";
$gl_caption['c_envas_id'] = "Tipus envàs";
$gl_caption['c_quantitat'] = "Num. envasos";
$gl_caption['c_unitats_quantitat'] = "Unitats";
$gl_caption['c_unitats_quantitat_l'] = "Litres";
$gl_caption['c_unitats_quantitat_kg'] = "Quilos";
$gl_caption['c_descripcio'] = "Descripció";
$gl_caption['c_volum'] = "Massa / Volum";
$gl_caption['c_carta'] = "Carta de port";
$gl_caption['c_get_carta_button'] = "Descarregar carta";

// destinatari
// conductor
$gl_caption['c_empresa_transportista'] = 'Empresa transportista';
$gl_caption['c_nom'] = 'Nom';
$gl_caption['c_cognoms'] = 'Cognoms';
$gl_caption['c_dni'] = 'DNI - CIF';
$gl_caption['c_address'] = 'Adreça';
$gl_caption['c_zip'] = 'Codi postal';
$gl_caption['c_city'] = 'Població';
$gl_caption['c_provincia_id'] = 'Provincia';
$gl_caption['c_country_id'] = 'País';
$gl_caption['c_phone'] = 'Telèfon';

$gl_caption['c_matricula_tractora'] = 'Matrícula tractora';
$gl_caption['c_matricula_remolque'] = 'Matrícula remolque';


$gl_caption['c_matricula_remolque'] = 'Matrícula remolque';

// Importació
$gl_caption_adr['c_import_title'] = 'Importació de productes ADR';
$gl_caption['c_select_file'] = "Seleccionar arxiu excel";
$gl_caption['c_import_preview'] = "Previsualitzar";
$gl_caption['c_import'] = "Importar";


?>
