<?
/**
 * ClubmotoPagina
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2012
 * @version $Id$
 * @access public 
 
 */
class ClubmotoPagina extends Module{
	var $clubone_js = '';
	function __construct(){
		parent::__construct();
	}
	function get_records()
	{
		$pagina_id = R::id('pagina_id');
		if (!$pagina_id) Main::error_404();
		
		$this->set_next_prev($pagina_id);
		$this->increment_counter($pagina_id);
		
		$listing = new ListRecords($this);
		
		$listing->always_parse_template = true;	
		$listing->order_by = 'ordre ASC, pagina_id';
		$listing->extra_fields = '(SELECT file_name FROM clubmoto__paginatpl WHERE clubmoto__paginatpl.paginatpl_id = clubmoto__pagina.paginatpl_id) as file_name';
		$listing->condition = "level=3 AND parent_id = " . $pagina_id . $GLOBALS['gl_clubmoto_status'];
		$listing->call('list_records_walk','pagina_id',true);
			
	    $ret = $listing->list_records();
		$this->clubone_js = 'clubmoto.clubone = {' . substr($this->clubone_js,0,-1) . '}';
		$GLOBALS['gl_page']->javascript .= $this->clubone_js . ';clubmoto.is_logged = ' . ($this->is_logged()?1:0) . ';';
	    return $ret;
	}
	
	function list_records_walk(&$listing,$pagina_id){
		$rs = &$listing->rs;
		$ret['show_col_1']= ($rs['body_supertitle']||$rs['title']||$rs['body_subtitle']||$rs['body_content']);
		$ret['show_col_2']= ($rs['body_supertitle2']||$rs['title2']||$rs['body_subtitle2']||$rs['body_content2']);
		$ret['show_col_3']= ($rs['body_supertitle3']||$rs['title3']||$rs['body_subtitle3']||$rs['body_content3']);
		$ret['show_video']= (isset($rs['images'][1]) || $rs['videoframe']);
		if (!$rs['videoframe'] && isset($rs['images'][1])){
			$ret['image2'] = $rs['images'][1];
		}
		$ret['show_register'] = $rs['is_clubone'] && !$this->is_logged();
		$this->clubone_js .= '"'.$rs['conta'].'":'.$rs['is_clubone'].',';
		return $ret;
	}
	function show_form()
	{
		
		// Faig un show form per les dades de la pàgina
		$show = new ShowForm($this);
		$show->condition = "level=2" . $GLOBALS['gl_clubmoto_status'];
		
		$show->get_values();
		
		if (!$show->rs) Main::error_404();	
		// defineixo les variables de la pàgina
		$GLOBALS['gl_page']->page_title = 
				$show->rs['page_title']?$show->rs['page_title']:$show->rs['pagina'];				
		$GLOBALS['gl_page']->page_description = $show->rs['page_description'];		
		$GLOBALS['gl_page']->page_keywords = $show->rs['page_keywords'];
		
		$this->action = 'list_records';
		$GLOBALS['gl_content'] = $this->get_records();
	}
	
	function set_next_prev($pagina_id)
	{
		$results = array();
		$loops = array('1','0');
		foreach ($loops as $key){
			$parent_id = Db::get_first("SELECT parent_id
						FROM clubmoto__pagina
						WHERE bin <> 1
						".$GLOBALS['gl_clubmoto_status']."
						AND pagina_id = '".$pagina_id."'");
			$query = "SELECT clubmoto__pagina.pagina_id AS pagina_id,
							(SELECT clubmoto__category.ordre 
								FROM clubmoto__category 
								WHERE clubmoto__category.category_id = clubmoto__pagina.category_id) AS category,
							(SELECT clubmoto__category_language.category 
								FROM clubmoto__category_language 
								WHERE clubmoto__category_language.category_id = clubmoto__pagina.category_id
								AND language = '".LANGUAGE."') AS category_name,
							(SELECT clubmoto__pagina_language.pagina 
								FROM clubmoto__pagina_language 
								WHERE clubmoto__pagina_language.pagina_id = clubmoto__pagina.pagina_id
								AND language = '".LANGUAGE."') AS pagina
						FROM clubmoto__pagina
						WHERE clubmoto__pagina.bin = 0 
						".$GLOBALS['gl_clubmoto_status']."
						AND level = 2 
						AND (SELECT clubmoto__category.is_clubone 
								FROM clubmoto__category 
								WHERE clubmoto__category.category_id = clubmoto__pagina.category_id) = ".$key."
						AND parent_id = '".$parent_id."'
						ORDER BY category ASC, category_id, ordre ASC, pagina_id";
			$results = array_merge(Db::get_rows($query),$results);
		}
		// fins aquí tot igual que al block clubmoto_apartat.php menys el merge dels results
		
		foreach ($results as $key=>$rs){
			if ($pagina_id == $rs["pagina_id"]){
			
				if (isset($results[$key-1])){
					$vars['previous_apartat_link'] = clubmoto_get_pagina_link($results[$key-1]['pagina_id']);
					$vars['previous_apartat_title'] = $results[$key-1]['pagina'];
				}
				else{
					$vars['previous_apartat_link'] = '' ;
					$vars['previoust_apartat_title'] = '';				
				}
				if (isset($results[$key+1])){
					$vars['next_apartat_link'] = clubmoto_get_pagina_link($results[$key+1]['pagina_id']);
					$vars['next_apartat_title'] = $results[$key+1]['pagina'];
				}
				else{
					$vars['next_apartat_link'] = '' ;
					$vars['next_apartat_title'] = '';				
				}
			
				$this->set_vars($vars);
				break;
			}
		}
	}
	function increment_counter($pagina_id){
		if (VERSION==4) {include (DOCUMENT_ROOT . 'common/includes/browser/browser_php4.php');} else {include (DOCUMENT_ROOT . 'common/includes/browser/browser.php');}
		$browser = new Browser();
		$name = $browser->getBrowser();
		
		
		if (VERSION==4) {
			$is_ok=( 
				$name != $browser->BROWSER_GOOGLEBOT &&
			$name != $browser->BROWSER_SLURP &&
			$name != $browser->BROWSER_W3CVALIDATOR &&
			$name != $browser->BROWSER_MSNBOT &&
			$name != $browser->BROWSER_UNKNOWN
				);
		}
		else{
			include (DOCUMENT_ROOT . 'public/modules/clubmoto/browser_php5.php');
		}
		
		
		if ($is_ok){
			// si no està en sessió la conto, nomès un cop per usuari
			if (!isset($_SESSION['clubmoto']['paginas'][$pagina_id])){
				$_SESSION['clubmoto']['paginas'][$pagina_id] = true;
				Db::execute("UPDATE clubmoto__pagina SET counter=(counter+1) WHERE  pagina_id='" .$pagina_id . "'");
			}			
		}
		
		
		
	}

	/**
	 * Es el logged de newsletter
	 * NewsletterCustomer::is_logged()
	 * Saber si està loguejat
	 * @return
	 */
	function is_logged(){
		return (isset($_SESSION['newsletter']['customer_id']) && isset($_SESSION['newsletter']['mail']));
	}
}
?>