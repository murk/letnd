<?php
// menus eina
if (!defined('EICA_MENU_CURS')){
	define('EICA_MENU_CURS', 'Cursos');
	define('EICA_MENU_CURS_NEW', 'Entrar nou curs');
	define('EICA_MENU_CURS_LIST', 'Llistar i editar cursos');
	define('EICA_MENU_CURS_BIN', 'Paperera de reciclatge');
	define('EICA_MENU_FREQUENCY', 'Freqüència');
	define('EICA_MENU_FREQUENCY_NEW', 'Entrar nova freqüencia');
	define('EICA_MENU_FREQUENCY_LIST', 'Llistar freqüències');
	define('EICA_MENU_TARGET', 'Orientació');
	define('EICA_MENU_TARGET_NEW', 'Entrar nova orientació');
	define('EICA_MENU_TARGET_LIST', 'Llistar orientacions');
	define('EICA_MENU_IDIOMA', 'Idioma');
	define('EICA_MENU_IDIOMA_NEW', 'Entrar nou idioma');
	define('EICA_MENU_IDIOMA_LIST', 'Llistar idiomes');

	define('EICA_MENU_LEVEL', 'Nivells');
	define('EICA_MENU_LEVEL_NEW', 'Entrar nou nivell');
	define('EICA_MENU_LEVEL_LIST', 'Llistar nivells');

	define('EICA_MENU_MODALITY', 'Modalitats');
	define('EICA_MENU_MODALITY_NEW', 'Entrar nova modalitat');
	define('EICA_MENU_MODALITY_LIST', 'Llistar modalitats');

	define('EICA_MENU_ELEMENT','Inclòs / no inclòs');
	define('EICA_MENU_ELEMENT_NEW','Nou ítem');
	define('EICA_MENU_ELEMENT_LIST','Llistar ítems');
	
	define('EICA_MENU_PRICE', 'Preus');
	define('EICA_MENU_PRICE_NEW', 'Entrar nou preu');
	define('EICA_MENU_PRICE_LIST', 'Llistar preus');
	define('EICA_MENU_PRICE_BIN', 'Paperera de reciclatge');
}

$gl_messages_curs['no_category']='Per poder insertar notícies hi ha d\'haver almenys una categoria';

// captions seccio
$gl_caption_curs['c_prepare'] = 'En preparació';
$gl_caption_curs['c_review'] = 'Per revisar';
$gl_caption_curs['c_public'] = 'Públic';
$gl_caption_curs['c_archived'] = 'Arxivat';
$gl_caption_curs['c_title'] = 'Títol';
$gl_caption_curs['c_subtitle'] = 'Subtítol';
$gl_caption_curs['c_content'] = 'Descripció';
$gl_caption_curs['c_entered'] = 'Data publicació';
$gl_caption_curs['c_expired'] = 'Data caducitat';
$gl_caption_curs['c_modified'] = 'Data modificació';
$gl_caption_curs['c_status'] = 'Estat';
$gl_caption_curs['c_category_id'] = 'Categoria';
$gl_caption_curs['c_filter_category_id'] = 'Totes les categories';
$gl_caption_curs['c_filter_status'] = 'Tots els estats';
$gl_caption_curs['c_filter_in_home'] = 'Totes, home o no';
$gl_caption_curs['c_filter_destacat'] = 'Totes, destacades o no';
$gl_caption_curs['c_ordre'] = 'Ordre';
$gl_caption_curs['c_weekday_id'] = 'Dies';
$gl_caption_curs['c_price'] = 'Precio';
$gl_caption_curs['c_price_text'] = 'Precio texto';
$gl_caption_curs['c_price_enrolment'] = 'Matrícula';
$gl_caption_curs_admin['c_price_enrolment'] = 'Precio Matrícula';
$gl_caption_curs['c_and'] = 'et';
$gl_caption_curs['c_or'] = 'ou';

$gl_caption_category['c_category'] = 'Categoria';
$gl_caption_category['c_curs_button'] = 'Veure cursos';
$gl_caption_category['c_ordre'] = 'Ordre';
$gl_caption_image['c_name'] = 'Nom';
$gl_caption['c_idioma'] = $gl_caption['c_idioma_id'] = 'Idioma';
$gl_caption_curs['c_url1_name']='Nom URL(1)';
$gl_caption_curs['c_url1']='URL(1)';
$gl_caption_curs['c_url2_name']='Nom URL(2)';
$gl_caption_curs['c_url2']='URL(2)';
$gl_caption_curs['c_url3_name']='Nom URL(3)';
$gl_caption_curs['c_url3']='URL(3)';
$gl_caption_curs['c_videoframe']='Video (youtube, metacafe...)';
$gl_caption_curs['c_file']='Arxius';
$gl_caption_curs['c_in_home']='Surt a la Home';
$gl_caption_curs['c_in_home_0']='No surt a la Home';
$gl_caption_curs['c_in_home_1']='Surt a la Home';
$gl_caption_curs['c_content_list']='Descripció curta al llistat';
$gl_caption_curs['c_destacat']='Notícia destacada';
$gl_caption_curs['c_destacat_0']='No destacada';
$gl_caption_curs['c_destacat_1']='Destacada';
$gl_caption_curs['c_frequency_text']='Freqüència';
$gl_caption_curs['c_timetable']='Horari';
$gl_caption['c_level_id'] = $gl_caption['c_level'] = 'Nivell';
$gl_caption['c_modality_id'] = $gl_caption['c_modality'] = 'Modalitat';
$gl_caption['c_includes'] = 'Inclou';
$gl_caption['c_excludes'] = 'No inclou';
$gl_caption_element['c_element'] = 'Ítem';

$gl_caption_curs['c_form_level1_title_idioma_id']='Característiques';
$gl_caption_curs['c_form_level1_title_curs']='Descripcions';
$gl_caption_curs['c_form_level1_title_url1_name']='Enllaços';
$gl_caption_curs['c_form_level1_title_file']='Arxius adjunts';
$gl_caption_curs['c_form_level1_title_page_title']='SEO';
$gl_caption_curs['c_form_level2_title_entered']='Dates';


$gl_caption['c_curs_file_name'] = $GLOBALS['gl_caption']['c_file_name'];
$gl_caption['c_curs_old_file_name'] = $GLOBALS['gl_caption']['c_old_file_name'];

$gl_caption_curs['c_selected']='Cursos seleccionats';

$gl_caption['c_ordre']='Ordre';

// public
$gl_caption['c_curs'] = $gl_caption['c_curs_id'] = 'Curs';
$gl_caption_curs['c_duration']='Durada';
$gl_caption_curs['c_exam']='Exàmens oficials';
$gl_caption['c_target_none']='Tothom';
$gl_caption['c_frequency']=$gl_caption['c_frequency_id']='Freqüència';
$gl_caption['c_target']=$gl_caption['c_target_id']='Orientat a';
$gl_caption['c_price_id']='Preus';
$gl_caption['c_price_month']='Preu mensual';
$gl_caption['c_price_quarter']='Preu trimestral';
?>