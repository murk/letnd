<?php
// menus eina
define('SAPLI_MENU_NEW', 'Aplicaciones');
define('SAPLI_MENU_NEW_NEW', 'Entrar nueva aplicación');
define('SAPLI_MENU_NEW_LIST', 'Listar y editar aplicaciones');
define('SAPLI_MENU_NEW_BIN', 'Papelera de reciclage');
define('SAPLI_MENU_CATEGORY', 'Categorias');
define('SAPLI_MENU_CATEGORY_NEW', 'Entrar nueva categoria');
define('SAPLI_MENU_CATEGORY_LIST', 'Listar categorias');

$gl_messages_new['no_category']='Para poder insertar aplicaciones tiene que haber almenos una categoria';

// captions seccio
$gl_caption_new['c_prepare'] = 'En preparación';
$gl_caption_new['c_review'] = 'Para revisar';
$gl_caption_new['c_public'] = 'Público';
$gl_caption_new['c_archived'] = 'Arxivado';
$gl_caption_new['c_new'] = 'Título';
$gl_caption_new['c_title'] = 'Título';
$gl_caption_new['c_subtitle'] = 'Subtítulo';
$gl_caption_new['c_description'] = 'Descripción';
$gl_caption_new['c_entered'] = 'Fecha';
$gl_caption_new['c_status'] = 'Estado';
$gl_caption_new['c_category_id'] = 'Categoria';
$gl_caption_new['c_filter_category_id'] = 'Todas las categorias';
$gl_caption_new['c_ordre']='Orden';
$gl_caption_new['c_new_list']='Título Listado';

$gl_caption_category['c_category'] = 'Categoria';
$gl_caption_category['c_new_button'] = 'Veure notícies';
$gl_caption_category['c_ordre'] = 'Ordre';
$gl_caption_image['c_name'] = 'Nom';
$gl_caption_new['c_url1_name']='Nom URL(1)';
$gl_caption_new['c_url1']='URL(1)';
$gl_caption_new['c_url2_name']='Nom URL(2)';
$gl_caption_new['c_url2']='URL(2)';
$gl_caption_new['c_videoframe']='Video (youtube, metacafe...)';
$gl_caption_new['c_file']='Arxius';
$gl_caption_new['c_in_home']='Surt a la home';
?>
