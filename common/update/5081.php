<?
/*13-5-13
  Canvi historial inmobles
 
 */

if (is_table("custumer__history")){
	
	$update_sql .="ALTER TABLE `custumer__history`
	CHANGE COLUMN `tipus` `tipus` ENUM('various','to_call','waiting_call','assembly','show_property','like_property','send_information','demand_refused','demand_reseted') NOT NULL COLLATE 'utf8_spanish2_ci' AFTER `finished_date`;";
	
	if (!is_field("custumer__history", "client_id")){
		$update_sql .="ALTER TABLE `custumer__history`
						ADD COLUMN `client_id` INT(11) NOT NULL DEFAULT '0' AFTER `demand_id`;";
	}
}

if (is_table("inmo__property")){
	$update_sql .="ALTER TABLE `inmo__property`
	CHANGE COLUMN `ref_number` `ref_number` INT(11) NOT NULL DEFAULT '0' AFTER `ref`;";
}
?>