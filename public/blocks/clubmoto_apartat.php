<?php
// a la home agafo la primera revista que trobo
if (!isset($_GET["pagina_id"])){
	$parent_id = clubmoto_get_current_revista();
	$pagina_id = '';
}
else{
	$pagina_id = R::id("pagina_id");
	// primer busco quina revista es
	$parent_id = Db::get_first("SELECT parent_id
					FROM clubmoto__pagina
					WHERE bin <> 1
					".$GLOBALS['gl_clubmoto_status']."
					AND pagina_id = '".$pagina_id."'");
}

debug::add('revista actual',$parent_id);			
if (!$parent_id) {
	$show_block = false;
	return;
}
// busco apartats
$loops = array(0,1);
foreach ($loops as $key){
	$results[$key] = clubmoto_get_apartats_revista($parent_id, $key);
}
Debug::add('block category resultats',$results);


if (!$results) {
	$show_block = false;
	return;
}

$lo = array();
$selected_found = false;
if ($results)
{
foreach ($loops as $key){	
	$conta = 0;
	$loop_count = count($results[$key]);
	$last_category ='';
	$lo[$key] = array();
    foreach($results[$key] as $rs)
    {		
		$images = UploadFiles::get_record_images($rs['pagina_id'], false, 'clubmoto', 'pagina');
		if ($last_category!=$rs['category_name']){
		
			// poso be categoria anterior
			if (isset($lo[$key][$last_category])){
				$last_loop = &$lo[$key][$last_category];
				$last_loop['loop'][0]['class'] = 'first';
				$count = count($last_loop['loop']);
				$last_loop['loop'][$count-1]['class'] = 'last';
				$last_loop['loop_count'] = $count;
				$last_loop['category_selected'] = $selected_found;
				$selected_found = false;
			}
		
			$last_category = $rs['category_name'];
			$lo[$key][$last_category]['item'] = $rs['category_name'];
			$lo[$key][$last_category]['category'] = $rs['category_name'];
			$lo[$key][$last_category]['link'] = clubmoto_get_pagina_link($rs['pagina_id']);
			$lo[$key][$last_category]['id'] = $rs['pagina_id'];
			$lo[$key][$last_category]['image'] = $images;
			$lo[$key][$last_category]['loop'] = array();
		}
		
        ($pagina_id == $rs["pagina_id"])?$selected = 1:$selected = 0;	
		$selected_found = $selected_found || $selected;
			
		
		$l['link'] = clubmoto_get_pagina_link($rs['pagina_id']);
        $l['selected'] = $selected;
        $l['item'] = $rs['pagina'];
		$l['id'] = $rs['pagina_id'];
		$l['image'] = $images;
		$l['conta'] = $conta++;
		$l['class'] = '';
		$l['loop_count'] = $loop_count;
		
		$lo[$key][$last_category]['loop'][] = $l;
        //$lo[$key][] = $l;		
    }
	if ($results[$key]){
		$last_loop = &$lo[$key][$last_category];
		$last_loop['loop'][0]['class'] = 'first';
		$count = count($last_loop['loop']);
		$last_loop['loop'][$count-1]['class'] = 'last';
		$last_loop['loop_count'] = $count;
		$last_loop['category_selected'] = $selected_found;
	}
}
}
$loop = $lo[0];
$vars['loop2'] = $lo[1];

?>