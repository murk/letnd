//FUNCIONES UTILIZADAS EN EL SIMULADOR PARA FORMATEAR NÚMEROS

function Formato(numero){

	var nombre = "";

	//alert("En Formato_numero, Nombre recibido :" + name);
		var num = numero;
	//alert("Numero recibido :" + num);

	if (num != ""){

	//alert ("Entra en num !=");

		var str = "";
		while (num > 999){

			n1 = Math.floor(num/1000);
			n2 = num - 1000 * n1;

			if (n2 < 10){
			   n2 = "00" + n2;
	        	}
			else{
			   if (n2 < 100)
	       		      n2 = "0" + n2;
	                }
			str = "." + n2 + str;
			num = n1;

		}
		str = num + str;
		//return (str);
	}
	return (str);
}

//////////////////////////////////////////////////////////////////////////////////
//Función para eliminar los puntos de miles
//////////////////////////////////////////////////////////////////////////////////

function Delete_miles(numero_puntos){

//alert("En Delete_miles");

	var num_p;
	num_p = numero_puntos;
	//alert("Valor de num_p :" + num_p);

	var longitud = 0;
	longitud = num_p.length;
	//alert("Longitud de la cifra :" + longitud);
	var n = 0;

	var num_sp = "";

	while (n<longitud){

		//alert ("Dentro del while");
		var car = ' ';
		var car = num_p.charAt(n);
		//alert("Carácter recogido :" + car);
		if (car != '.')
				num_sp += num_p.charAt(n);
		n++;

	}
	//alert("num_sp :" + num_sp);

	var sin_puntos = parseInt(num_sp,10);

	//alert("Valor que devuelve Delete_miles :" + sin_puntos);
	return sin_puntos;
}

// función que valida según vas tecleando un importe poniendole los puntos
// parámetro de entrada	: obj (string)
//				: sign ("positive"/cualquier otra cosa)
// parámetro de salida 	: no procesa el evento si se teclea incorrectamente


function PonPuntos(obj1, obj2, sign, imaxlength, idioma){
	//comprueba que el length no supere el maximo para esa caja de texto
	
	
	if(idioma == null){
		idioma = 'e';
	}
	
	if(idioma=='c'){
		var PonPuntos_me1 = "No es possible introduïr valor 0";
	}
	
	if(idioma=='i'){
		var PonPuntos_me1 = "It is not possible to introduce value 0";
	}
	
	if(idioma=='f'){
		var PonPuntos_me1 = "Il n'est pas possible d'introduire valeur 0";
	}
	
	if(idioma=='a'){
		var PonPuntos_me1 = "It is not possible to introduce value 0";
	}
	
	if(idioma=='t'){
		var PonPuntos_me1 = "It is not possible to introduce value 0";
	}
	
	if(idioma=='e'){
		var PonPuntos_me1 = "No es posible introducir valor 0";
	}
	
	var snumero;
	var navegador = navigator.appName;
	
	if (navegador == "Microsoft Internet Explorer"){
		navegador = "IE"
		var key = event.keyCode
	}else{
		var key = window.event ? obj1.keyCode : obj1.which;
		//var keyreturnValue = obj1.preventDefault();	   
   }
   
	snumero = obj2.value + 1;

	if (snumero.length > imaxlength)
	{
		if(navegador=='IE'){
			event.returnValue = false;
		} else {
			//keyreturnValue;
			obj1.preventDefault();
		}
		return false;
	}


//no dejar que ponga cero al principio de teclear el campo

	if (isEmpty(obj2.value) && (key == 48)) {
		
		alert(PonPuntos_me1);
		if(navegador=='IE'){
			event.returnValue = false;
		} else {
			//keyreturnValue;
			obj1.preventDefault();
		}
		
	}else if (sign == "positive"){
		
		if ((key< 48) || (key > 57)){
			if(navegador=='IE'){
				event.returnValue = false;
			} else {
				//keyreturnValue;
				obj1.preventDefault();
			}
			
		}else{
			
			var s_pre = obj2.value;
			var s_inter = "";
			var s_post = "";
			var single_character = "";
			var poner_punto = 1;

			for (var i = 0; i < s_pre.length; i++){
				single_character = s_pre.substring(i,i+1);
				if (single_character != "."){
					s_inter = s_inter + single_character;
				}
			}

			//s_inter = s_inter + String.fromCharCode(event.keyCode);
			s_inter = s_inter + String.fromCharCode(key);

			for (var j = s_inter.length; j > 0; j--, poner_punto++){
				s_post = s_inter.substring(j-1,j) + s_post;
	
				if ((poner_punto == 3) && (j != 1)){
					poner_punto = 0;
					s_post = "." + s_post;
				}
			}

			obj2.value = s_post;
			if(navegador=='IE'){
				event.returnValue = false;
			} else {
				//keyreturnValue;
				obj1.preventDefault();
			}
		}
	}else{
		
		if ((isEmpty(obj2.value)&&((key < 48) || (key > 57))&&(key != 43)&&(key != 45))||((obj2.value.length > 0)&&((key < 48) || (key > 57)))){
			
			if(navegador=='IE'){
				event.returnValue = false;
			} else {
				//keyreturnValue;
				obj1.preventDefault();
			}
			
		}else if (obj2.value != "+" && obj.value != "-"){
			
			var s_pre = obj2.value;
			var s_inter = "";
			var s_post = "";
			var single_character = "";
			var poner_punto = 1;
			var sign_character = "";

			if ((s_pre.substring(0, 1) == "+") || (s_pre.substring(0, 1) == "-")){
				
				sign_character = s_pre.substring(0, 1);
				s_pre = s_pre.substring(1, s_pre.length);
				
			}

			for (var i = 0; i < s_pre.length; i++){
				
				single_character = s_pre.substring(i,i+1);
				if (single_character != "."){
					s_inter = s_inter + single_character;
				}
				
			}

			s_inter = s_inter + String.fromCharCode(key);//s_inter = s_inter + String.fromCharCode(event.keyCode);

			for (var j = s_inter.length; j > 0; j--, poner_punto++){
				
				s_post = s_inter.substring(j-1,j) + s_post;
				if ((poner_punto == 3) && (j != 1)){
				poner_punto = 0;
				s_post = "." + s_post;
				}
				
			}

			obj2.value = sign_character + s_post;
			if(navegador=='IE'){
				event.returnValue = false;
			} else {
				//keyreturnValue;
				obj1.preventDefault();
			}
		
		}
	}
	return true;
}

function Formato_numero(numero){

	var nombre = "";

	//alert("En Formato_numero, Nombre recibido :" + name);
		var num = numero;
	//alert("Numero recibido :" + num);

	if (num != ""){

	//alert ("Entra en num !=");

		var str = "";
		var n1;
		var n2;
		
		while (num > 999){
			
			n1 = Math.floor(num/1000);
			n2 = num - 1000 * n1;

			if (n2 < 10){
			   n2 = "00" + n2;
	        	}
			else{
			   if (n2 < 100)
	       		      n2 = "0" + n2;
	                }
			str = "." + n2 + str;
			num = n1;

		}
		str = num + str;
		//return (str);
	}
	return (str);
}

//FUNCIONES DE VALIDACIÓN NUMERICA
function EsNumerico(dato)
{
	var i;
	var longitud;
	var caracter;
	var sumo = 0;
	var valido;
	
	longitud = dato.length;
	valido = "0123456789.";
	for ( i=0; i <= longitud; i++)
	{
		caracter = dato.substring(i,i+1)
		if (valido.indexOf(caracter) == -1)
				   return false;
	}
	return true;
}

function borrar(obj) {

obj.value = "";

}

function isEmpty(s)
{   
	return ((s == null) || (s.length == 0))
}