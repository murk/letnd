<?php
include_once ('llemena_pagina_recursive_functions.php');
// nomes serveixen els pages que tinguin menu=1 per fer el beradcrumb, no es fa de cap menu secundari
// nomes que hi hagi un menu malament en tota l'estructura ja no es mostrarà ( ej. que hi s'hagi colat un menu = 2 entre mig )

$page = &$GLOBALS['gl_page'];

$pagina_id = R::id('pagina_id');

$condition = $pagina_id?("pagina_id = " . $pagina_id):("link='/'"); // agafo l'id de la home
$rs = Db::get_row('SELECT pagina_id,level,parent_id FROM llemena__pagina WHERE ' . $condition);

// agafo el page_id com a parent id, així la primera consulta del bucle es el page actual
$parent_id = $rs['pagina_id'];

$conta = 0;
$loop_count = $rs['level'];

if ($GLOBALS['gl_breadcrumb']) $loop_count++;

// si nomes hi ha un no el mostro ( nomès mostro quan una ruta es d'almenys 2 elements
if ($loop_count == 1){
	$show_form = false;
	return;
}

do{
	$query = "SELECT llemena__pagina.pagina_id as pagina_id, link, pagina, parent_id, has_content
				FROM llemena__pagina_language, llemena__pagina
				WHERE llemena__pagina_language.pagina_id = llemena__pagina.pagina_id
				AND status = 'public'
				AND bin <> 1
				AND llemena__pagina.pagina_id = '" . $parent_id . "'
				AND language = '" . LANGUAGE . "'";
	
	$results = Db::get_rows($query);
	
	// si nomes hi ha breadcrumb no es mostra ( ja està bé, un tot sol no s'ha de mostrar )
	if (!$results){
		// nomès que hi hagi un esglaó malament ja no mostro el breadcrumb, sempre hi hauria d'haver resultats fins a l'arrel
		$show_block = false;
		return;
	}
	else
	{		
		foreach($results as $rs)
		{
			((($pagina_id == $rs["pagina_id"])
			|| ($rs['link']=='/' && $GLOBALS['gl_is_home'])) && !$GLOBALS['gl_breadcrumb'])?$selected = 1:$selected = 0;
			
			$l['link'] = _block_llemena_pagina_get_pagina_link($rs['pagina_id'],$rs['link'],'',$rs['has_content']);
			$l['selected'] = $selected;
			$l['item'] = $rs['pagina'];
			$l['id'] = $rs['pagina_id'];
			$l['conta'] = $conta++;
			$l['class'] = '';
			$l['loop_count'] = $loop_count;
			$loop[] = $l;
		}
		
		$parent_id = $rs['parent_id'];
	}
} while ($parent_id!=0);

$loop = array_reverse($loop);

if ($GLOBALS['gl_breadcrumb']){
	$l['link'] = '';
	$l['selected'] = 1;
	$l['item'] = $GLOBALS['gl_breadcrumb'];
	$l['id'] = '';
	$l['conta'] = $conta++;
	$l['class'] = '';
	$l['loop_count'] = $loop_count;
	$loop[] = $l;
}

$loop[0]['class'] = 'first';	
$loop[count($loop)-1]['class'] = 'last';

?>