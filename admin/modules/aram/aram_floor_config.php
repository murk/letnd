<?php
	// Aquest arxiu es genera automaticament
	// Qualsevol canvi que es faci es podrà sobrescriure involuntariament desde l'eina de gestió
	// Achtung!
	$gl_file_protected = false;
	$gl_db_classes_fields_included = array (
  'floor' => '1',
  'floor_num' => '1',
  'category_id' => '1',
);
	$gl_db_classes_language_fields = array (
  0 => 'floor',
  1 => 'file',
);
	$gl_db_classes_fields = array (
  'floor_id' =>
  array (
    'type' => 'hidden',
    'enabled' => '1',
    'form_admin' => 'input',
    'form_public' => '0',
    'list_admin' => '0',
    'list_public' => '0',
    'order' => '0',
    'required' => '1',
    'default_value' => '',
    'override_save_value' => '',
    'class' => '',
    'javascript' => '',
    'text_size' => '',
    'text_maxlength' => '11',
    'textarea_cols' => '',
    'textarea_rows' => '',
    'select_caption' => '',
    'select_size' => '',
    'select_table' => '',
    'select_fields' => '',
    'select_condition' => '',
  ),
  'floor_num' =>
  array (
    'type' => 'int',
    'enabled' => '1',
    'form_admin' => 'input',
    'form_public' => '0',
    'list_admin' => 'input',
    'list_public' => '0',
    'order' => '0',
    'default_value' => '',
    'override_save_value' => '',
    'class' => '',
    'javascript' => '',
    'text_size' => '',
    'text_maxlength' => '11',
    'textarea_cols' => '',
    'textarea_rows' => '',
    'select_caption' => '',
    'select_size' => '',
    'select_table' => '',
    'select_fields' => '',
    'select_condition' => '',
  ),
  'floor' =>
  array (
    'type' => 'text',
    'enabled' => '1',
    'form_admin' => 'input',
    'form_public' => 'text',
    'list_admin' => 'input',
    'list_public' => 'text',
    'order' => '0',
    'default_value' => '',
    'override_save_value' => '',
    'class' => '',
    'javascript' => '',
    'text_size' => '',
    'text_maxlength' => '100',
    'textarea_cols' => '',
    'textarea_rows' => '',
    'select_caption' => '',
    'select_size' => '',
    'select_table' => '',
    'select_fields' => '',
    'select_condition' => '',
  ),
  'file' =>
  array (
    'type' => 'file',
    'enabled' => '1',
    'form_admin' => 'input',
    'form_public' => '0',
    'list_admin' => '0',
    'list_public' => '0',
    'order' => '0',
    'default_value' => '',
    'override_save_value' => '',
    'class' => '',
    'javascript' => '',
    'text_size' => '',
    'text_maxlength' => '5',
    'textarea_cols' => '',
    'textarea_rows' => '',
    'select_caption' => '',
    'select_size' => '',
    'select_table' => 'aram__floor_file',
    'select_fields' => 'file_id, name',
    'select_condition' => '',
    'accept' => 'pdf|doc|docx',
  ), 
);
	$gl_db_classes_list = array (
  'cols' => '1',
);
	$gl_db_classes_form = array (
  'cols' => '1',
  'images_title' => '',
);
	$gl_db_classes_related_tables = array (
  '0' =>
  array (
    'table' => 'aram__habitatge',
    'action' => 'notify',
  ),
);
$gl_db_classes_uploads = array (
	'file' => '',
);
	?>