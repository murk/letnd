<article>
	<h2>Products delivery.</h2>
	<p>The delivery of the products will be made within 3 to 14 working days* from the purchasing date.</p>
	<p>If necessary, you will be informed through an e-mail about any change or problem arises relative to the availability, price, payment or delivery of the product.</p>
	<p>In case an order is sent and the address provided by the user is wrong, the supplementary freight costs will be charged to the customer.</p>
	<p>* Bank holidays and weekends will be deemed as non-working days.
    </p>
</article>