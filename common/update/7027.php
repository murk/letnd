<?

/* 17-8-13
  Afegir característiques a inmo



 */
if (is_table("inmo__property")) {

	$update_sql[] = "
					ALTER TABLE `inmo__property`
						ADD COLUMN `custom5` VARCHAR(50) NOT NULL AFTER `custom4`,
						ADD COLUMN `custom6` VARCHAR(50) NOT NULL AFTER `custom5`,
						ADD COLUMN `custom7` VARCHAR(50) NOT NULL AFTER `custom6`,
						ADD COLUMN `custom8` VARCHAR(50) NOT NULL AFTER `custom7`;
					";	

	$update_sql[] = "
					ALTER TABLE `inmo__property_language`
						ADD COLUMN `custom5` VARCHAR(255) NOT NULL DEFAULT '' AFTER `custom4`,
						ADD COLUMN `custom6` VARCHAR(255) NOT NULL DEFAULT '' AFTER `custom5`,
						ADD COLUMN `custom7` VARCHAR(255) NOT NULL DEFAULT '' AFTER `custom6`,
						ADD COLUMN `custom8` VARCHAR(255) NOT NULL DEFAULT '' AFTER `custom7`;
					";
	
	
	$update_sql[]="
			INSERT INTO `inmo__configadmin` (`name`) 
			VALUES ('custom5_name');";
	$update_sql[]="
			INSERT INTO `inmo__configadmin` (`name`) 
			VALUES ('custom5_type');";
	$update_sql[]="
			INSERT INTO `inmo__configadmin` (`name`) 
			VALUES ('custom5_filter');";
	
	
	$update_sql[]="
			INSERT INTO `inmo__configadmin` (`name`) 
			VALUES ('custom6_name');";
	$update_sql[]="
			INSERT INTO `inmo__configadmin` (`name`) 
			VALUES ('custom6_type');";
	$update_sql[]="
			INSERT INTO `inmo__configadmin` (`name`) 
			VALUES ('custom6_filter');";
	
	
	$update_sql[]="
			INSERT INTO `inmo__configadmin` (`name`) 
			VALUES ('custom7_name');";
	$update_sql[]="
			INSERT INTO `inmo__configadmin` (`name`) 
			VALUES ('custom7_type');";
	$update_sql[]="
			INSERT INTO `inmo__configadmin` (`name`) 
			VALUES ('custom7_filter');";
	
	
	$update_sql[]="
			INSERT INTO `inmo__configadmin` (`name`) 
			VALUES ('custom8_name');";
	$update_sql[]="
			INSERT INTO `inmo__configadmin` (`name`) 
			VALUES ('custom8_type');";
	$update_sql[]="
			INSERT INTO `inmo__configadmin` (`name`) 
			VALUES ('custom8_filter');";
	
	
	global $gl_languages;
	foreach($gl_languages['names'] as $lang=>$val){
		$update_sql[]="
			INSERT INTO `inmo__configadmin_language` (`name`, `language`) 
			VALUES ('custom5_name', '".$lang."');";
		$update_sql[]="
			INSERT INTO `inmo__configadmin_language` (`name`, `language`) 
			VALUES ('custom6_name', '".$lang."');";
		$update_sql[]="
			INSERT INTO `inmo__configadmin_language` (`name`, `language`) 
			VALUES ('custom7_name', '".$lang."');";
		$update_sql[]="
			INSERT INTO `inmo__configadmin_language` (`name`, `language`) 
			VALUES ('custom8_name', '".$lang."');";
	}
}
?>