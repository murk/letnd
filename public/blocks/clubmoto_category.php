<?php
// a la home agafo la primera revista que trobo i no hi ha cap category seleccionada
if (!isset($_GET["pagina_id"])){
	$parent_id = clubmoto_get_current_revista();
	$category_id  = '';
}
else{
	$pagina_id = R::id("pagina_id");
	// primer busco quina revista es
	$rs = Db::get_row("SELECT parent_id, category_id
					FROM clubmoto__pagina
					WHERE bin <> 1
					".$GLOBALS['gl_clubmoto_status']."
					AND pagina_id = '".$pagina_id."'");
					
				
	$parent_id  = $rs['parent_id'];
	$category_id  = $rs['category_id'];
}
debug::add('revista actual',$parent_id);			
if (!$parent_id) {
	$show_block = false;
	return;
}
// busco apartats
$loops = array('0','1');
foreach ($loops as $key){					
$query = "SELECT clubmoto__category_language.category as category, clubmoto__category.category_id as category_id, (
						SELECT pagina_id
							FROM clubmoto__pagina
							WHERE clubmoto__pagina.bin = 0  
							".$GLOBALS['gl_clubmoto_status']."
							AND level = 2 
							AND parent_id = '".$parent_id."'
							AND clubmoto__pagina.category_id = clubmoto__category.category_id
							ORDER BY ordre ASC, pagina_id
							LIMIT 1) as pagina_id
					FROM clubmoto__category, clubmoto__category_language
					WHERE clubmoto__category.category_id = clubmoto__category_language.category_id
					AND NOT ISNULL((
						SELECT pagina_id
							FROM clubmoto__pagina
							WHERE clubmoto__pagina.bin = 0  
							".$GLOBALS['gl_clubmoto_status']."
							AND level = 2 
							AND parent_id = '".$parent_id."'
							AND clubmoto__pagina.category_id = clubmoto__category.category_id
							ORDER BY ordre ASC, pagina_id
							LIMIT 1))
					AND language = '".LANGUAGE."'
					AND is_clubone = '".$key."'
					ORDER BY ordre ASC, clubmoto__category.category_id";

$results[$key] = Db::get_rows($query);
}
Debug::add('block category resultats',$results);


if (!$results) {
	$show_block = false;
	return;
}

$lo = array();
if ($results)
{
foreach ($loops as $key){	
	$conta = 0;
	$loop_count = count($results[$key]);
	$lo[$key] = array();
    foreach($results[$key] as $rs)
    {
        ($category_id == $rs["category_id"])?$selected = 1:$selected = 0;		
		
		$l['link'] = clubmoto_get_pagina_link($rs['pagina_id']);
        $l['selected'] = $selected;
        $l['item'] = $rs['category'];
		$l['id'] = $rs['pagina_id'];
		$l['conta'] = $conta++;
		$l['class'] = '';
		$l['loop_count'] = $loop_count;
        $lo[$key][] = $l;		
    }
	if ($results[$key]){
		$lo[$key][0]['class'] = 'first';	
		$lo[$key][count($lo[$key])-1]['class'] = 'last';
	}
}
}
$loop = $lo[0];
$vars['loop2'] = $lo[1];

?>