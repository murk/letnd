<?php
class SistemamuseuMuseu extends Module{
	var $group_id;
	var $is_museu;
	function __construct(){
		parent::__construct();
		$this->group_id = $_SESSION['group_id'];
		$this->is_museu = $this->group_id>3;
	}
	function list_records()
	{ 
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function get_records()
	{		 
	    $listing = new ListRecords($this);
		$listing->order_by = 'ordre ASC';
		$listing->call_function['museu_description'] = 'add_dots';
		$listing->add_filter('tipus');
		$listing->add_filter('provincia_id');
	    /*PER CANVIAR A HTML $listing->list_records(false);
		foreach ($listing->results as $rs){
			$query = "UPDATE `sistemamuseu__museu_language` SET `museu_description`='".addslashes(nl2br($rs['museu_description']))."' WHERE `museu_id`=".$rs['museu_id']." AND `language`='cat' LIMIT 1;";
			Db::execute ($query);
			debug::p ($query);
		}*/
	    return $listing->list_records();
	}
	function show_form()
	{					
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{	    
		$show = new ShowForm($this);
		if ($this->is_museu){
			$show->id = $this->group_id;
			$show->unset_field('status');
			$show->set_var('status',false);
		}
		if ($GLOBALS['gl_write'])
		{
			$show->call_function['comarca_id'] = "get_select_comarques";
			$show->call_function['municipi_id'] = "get_select_municipis";
		}
	    return $show->show_form();
	}

	function save_rows()
	{
	    // un museu no pot guardar llistats de museus
		if ($this->is_museu) return;
		$save_rows = new SaveRows($this);
	    $save_rows->save();
	}
	function write_record()
	{
	    // m'asseguro que nomes guardi lo del propi museu, no cal però no se sab mai		
		if ($this->is_museu){
			$this->set_field('museu_id','override_save_value',$this->group_id);
		}
		
		$writerec = new SaveRows($this);
	    $writerec->save();
	}

	function manage_images()
	{
	    $image_manager = new ImageManager($this);
	    $image_manager->execute();
	}
	function show_form_map (){ //esenyo el napa
	global $gl_news;
						
	$rs = Db::get_row('SELECT * FROM sistemamuseu__museu  INNER JOIN sistemamuseu__municipi USING (municipi_id) WHERE museu_id = ' . $_GET['museu_id']);
	$provincia = Db::get_first('SELECT provincia FROM sistemamuseu__provincia  WHERE provincia_id = ' . $rs['provincia_id']);
   	$rs['museu_title'] = Db::get_first('SELECT museu_title FROM sistemamuseu__museu_language  WHERE museu_id = ' . $_GET['museu_id'] . ' AND language = LANGUAGE');
	$rs['is_default_point'] = ($rs['latitude']!=0)?0:1;
   	$rs['latitude']=$rs['latitude']!=0?$rs['latitude']:SISTEMAMUSEU_GOOGLE_CENTER_LATITUDE;
   	$rs['longitude']=$rs['longitude']!=0?$rs['longitude']:SISTEMAMUSEU_GOOGLE_CENTER_LONGITUDE;
   	$rs['zoom']=$rs['zoom']?$rs['zoom']:SISTEMAMUSEU_GOOGLE_ZOOM;
   	$rs['menu_id'] =  $_GET['menu_id'];		
	$rs['tool'] =  $this->tool;		
	$rs['tool_section'] =  $this->tool_section;		
   	$rs['id'] =  $rs[$this->id_field];		
   	$rs['id_field'] =  $this->id_field;		
	$this->set_vars($rs);//passo els resultats del array
	$this->set_file('sistemamuseu/museu_map.tpl'); //crido el arxiu
	$this->set_vars($this->caption); //passo tots els captions
	$gl_news = $this->caption['c_click_map'];
	$content = $this->process();//processo
	$GLOBALS['gl_content'] = $content;		
	}
	function save_record_map(){
	global $gl_page;
	extract($_GET);    	
	    	Db::execute ("UPDATE `sistemamuseu__museu` SET
				`latitude` = '$latitude',
				`longitude` = '$longitude',
				`zoom` = '$zoom'
				WHERE `museu_id` =$museu_id
				LIMIT 1") ;
			$gl_message = $gl_messages['point_saved'];
			$gl_page->show_message($this->messages['point_saved']);
			
	}
}

// call->function crida funcions fora la clase
function get_select_comarques($key, $value)
{
    $query_provincies = "1=1 ORDER BY comarca";
    return get_select_sub($value, $key, "sistemamuseu__comarca", "comarca, provincia_id, comarca_id", $query_provincies, '');
}
function get_select_municipis($key, $value)
{
    $query_comarques = "(sistemamuseu__municipi.comarca_id = sistemamuseu__comarca.comarca_id)
									ORDER BY municipi";
    return get_select_sub($value, $key, "sistemamuseu__municipi, sistemamuseu__comarca", "municipi, sistemamuseu__comarca.comarca_id, municipi_id", $query_comarques, '');
}


function get_select_sub($value, $name, $table, $fields, $condition = '', $caption = '', $onchange = '', $disabled = '')
{
    $html = '';
    $results = get_results_sub($table, $fields, $condition);

    if ($caption)
        $html .= '<option value="0">' . $caption . '</option>';
    if ($results)
    {
        foreach ($results as $rs)
        {
            $selected = '';
            if ($rs[2] == $value)
                $selected = " selected";
            $html .= '<option value="' . $rs[2] . '"' . $selected . '>#' . $rs[1] . "#" . $rs[0] . '</option>';
        }
    }
    if ($onchange)
        $onchange = ' onChange="' . $this->onchange . '"';
    if ($disabled)
        $disabled = ' disabled';
    $html = '<select name="' . $name . '"' . $onchange . $disabled . '>' . $html . '</select>';
    Debug::add("Get select", $html);
    return $html;
}

function get_results_sub($table, $fields, $condition)
{

    $query = "SELECT " . $fields . " FROM " . $table;
    if ($condition)
        $query .= " WHERE " . $condition;
    Debug::add("Consulta form objects", $query);
    $results = Db::get_rows_array($query);
    return $results;
}
?>