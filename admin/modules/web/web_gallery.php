<?
/**
 * WebGallery
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 24/05/2014
 * @version $Id$
 * @access public
 */
class WebGallery extends Module{
	var $is_newsletter = false;
	function __construct(){
		parent::__construct();
	}
	function on_load(){
			
		/*
		// social
		Main::load_class('social');
		$this->form_tabs = 
				array('0' => 
					array(
						'tab_action'=>'show_form_social&amp;menu_id=' . $GLOBALS['gl_menu_id'],
						'tab_caption'=>$this->caption['c_edit_social']
					)
				);
		*/
		
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function get_records()
	{
	    $listing = new ListRecords($this);
		
		
		$listing->call('records_walk','content,content_list', true);
		//$listing->add_button ('edit_content_button', 'action=show_form_copy', 'boto1');

        switch ($this->process) { // process-> propietat invetada que passo pel post del tpl
            case 'select_gallery': // llistat dins iframe per seleccionar productes
                $page = &$GLOBALS['gl_page'];
                $page->template = 'clean.tpl';
                $page->title = '';
				$this->is_newsletter = true;
				$this->set_field('content', 'list_admin', '0');
                $listing->set_options(0, 0, 0, 0, 0, 0, 0, 0, 1);
				$listing->condition = "(status = 'public')
							AND (entered=0 OR entered <= now())
							AND (expired=0 OR expired > (curdate()))";
                $GLOBALS['gl_message'] = $GLOBALS['gl_messages']['select_item'];

                break;
        	default:
        } // switch*/
			
		$listing->add_filter('status');
		
		$listing->add_filter('in_home');
		$listing->add_filter('destacat');
		
		// social
		//Social::init_listing($listing);
		
		if (isset($_GET['in_home']) && $_GET['in_home']!='null') {
			$listing->set_field('in_home','list_admin','input');
		}
		if (isset($_GET['destacat']) && $_GET['destacat']!='null') {
			$listing->set_field('destacat','list_admin','input');
		}
		
		$listing->order_by = 'ordre ASC, gallery ASC';		
	    return $listing->list_records();
	}
	function records_walk(&$listing, $content=false,$content_list=false){		
		
		if ($this->is_newsletter){
			$ret = $this->list_records_walk_select ($listing->rs['gallery_id'], $listing->rs['gallery']);
		}
		
		// si es llistat passa la variable content
		if ($content!==false){
			$ret['content']=$content_list?$content_list:add_dots('content', $content);
		}
		// social		
		//$this->set_buttons_social($listing);
		
		return $ret;
	}
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{
	
		$this->set_field('entered','default_value',now(true));
		$this->set_field('modified','default_value',now(true));
		
		$show = new ShowForm($this);
		//activo o desactivo el seo
		if (!$_SESSION['show_seo']) {
			$show->unset_field('page_title');	
			$show->unset_field('page_description');
			$show->unset_field('page_keywords');
			$show->unset_field('new_file_name');		    
		}		
	    return $show->show_form();
	}

	function save_rows()
	{
	    $save_rows = new SaveRows($this);
	    $save_rows->save();
	}

	function write_record()
	{
	    $writerec = new SaveRows($this);
		$writerec->set_field('modified','override_save_value','now()');			
		$writerec->field_unique = "gallery_file_name";
		$writerec->save();
	}

	function manage_images()
	{
	    $image_manager = new ImageManager($this);
		//$image_manager->form_tabs = $this->form_tabs;
	    $image_manager->execute();
	}
	
	/////////////
	// SOCIAL
	///////////////
	function show_form_social() {
		$id = R::id($this->id_field);
		$language = Social::get_language();
		
		$query = "
			SELECT 
				gallery, 
				subtitle, 
				content_list,
				content,
				gallery_file_name as file_name
			FROM " . $this->table . "_language 
			WHERE " . $this->id_field . "='" . $id . "'
			AND language = '" . $language . "'";
		$rs = Db::get_row($query);
		
		if (!$rs) return;
		$link = HOST_URL . $language . '/web/gallery/' . $id . '.htm';
		
		$facebook_params = array(
			'message' => '',
			'name' => htmlspecialchars($rs['gallery']),
			'caption' => htmlspecialchars($rs['subtitle']),
			'description' => htmlspecialchars( p2nl($rs['content_list']?$rs['content_list']:$rs['content']))
		);
		$twitter_params = array(
			'status' => htmlspecialchars($rs['gallery'])
		);
		
		$social = new Social($this, $id, $facebook_params, $twitter_params, $link);
		$GLOBALS['gl_content'] = $social->get_form();
	}
	function write_record_social() {
		$social = new Social($this);		
		$social->publish();
	}
	function set_buttons_social(&$listing) {
		
		Social::set_buttons_listing($listing);
		
	}
	
	/////////////
	// NEWSLETTER
	///////////////
    // Per poder escollir productes desde una finestra amb frame, de moment desde newsletter
    function select_frames()
    {
        $page = &$GLOBALS['gl_page'];

        $page->menu_tool = false;
        $page->menu_all_tools = false;
        $page->template = '';

        $this->set_file('web/gallery_select_frames.tpl');
        $this->set_vars($this->caption);
        $this->set_var('menu_id', $GLOBALS['gl_menu_id']);
        // $this->set_var('custumer_id',$_GET['custumer_id']);
        $GLOBALS['gl_content'] = $this->process();
    }
	// per posar el onclick al llistat
	function list_records_walk_select ($product_id, $product_title)
	{
		$product_title= addslashes($product_title); // cambio ' per \'
		$product_title= htmlspecialchars($product_title); // cambio nomÃƒÂ¨s cometes dobles per  &quot;
		$ret['tr_jscript'] = 'onClick="parent.insert_address(' . $product_id . ',\'' . $product_title . '\')"';
		return $ret;
	}
}
?>