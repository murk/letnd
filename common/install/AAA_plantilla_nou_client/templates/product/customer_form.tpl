
	<div class="customer<? if ($is_summary) echo" summary" ?>">
		<? if(!$is_summary): ?>
		<form name="theForm" method="post" action="<?= $link_action ?>" onsubmit="<?= $js_string ?>" encType="multipart/form-data" target="save_frame">
		<? endif ?>

			<? if($is_summary):?>
				<a class="lnk-right" href="<?=$edit_link?>">
				<?=$c_edit_button?>
				</a>
			<? endif //is_summary?>
			<h3 <? if ($form_new) echo ' class="first"'?>>
				<?=$c_form_title?> 
			</h3> 
			<table class="form">
				<? if ($is_summary):?>
				<tr>
					<th>
						<?=$c_name?>:
					</th>
					<td class="treatment">
						<?=$name?> <?=$surname?>
					</td>
				</tr>
				<? endif //is_summary?>
				<? if (!$is_summary):?>
				<tr>
					<th><?=$c_name?>:</th>
					<td>
						<?=$hidden?><?=$name?>
					</td>
				</tr>
				<tr>
					<th><?=$c_surname?>:</th>
					<td><?=$surname?></td>
				</tr>
				<? endif //!is_summary?>
				<tr>
					<th><?=$c_birthdate?>:</th>
					<td><?=$birthdate?></td>
				</tr>
				<tr>
					<th><?=$c_telephone?>:</th>
					<td>
						<?=$telephone?>
						<? if ($is_summary && $telephone_mobile):?>
							- <?=$telephone_mobile?>
						<? endif //is_summary?>
					</td>
				</tr>
				<? if (!$is_summary):?>
					<tr>
						<th><?=$c_telephone_mobile?>:</th>
						<td><?=$telephone_mobile?></td>
					</tr>
				<? endif //!is_summary?>
				<? if ($is_summary):?>
				<tr>
					<th><?=$c_mail?>:</th>
					<td><?=$mail?></td>
				</tr>
				<? endif //summary?>
			</table>
			<? if (!$is_summary):?>
			<h3>
				<?=$c_form_title2?>
			</h3>
			<table class="form">
				<tr>
					<th><?=$c_mail?>:</th>
					<td><?=$mail?></td>
				</tr>
				<tr>
					<th><?=$c_password?>:</th>
					<td><?=$password?></td>
				</tr>
				<tr>
					<th><?=$c_password_repeat?>:</th>
					<td><?=$password_repeat?></td>
				</tr>
			</table>
			<? endif //!summary?>
			<? if ($is_summary):?>
			<table class="form">
				<tr>
					<th><?=$c_form_title3?>:</th>
					<td><?=$address_invoice?></td>
				</tr>
			</table>			
			<? endif //summary?>
			<? if (!$is_summary):?>
			<h3>
				<?=$c_form_title3?>
			</h3>
			<table class="form">
				<tr>
					<th><?=$c_dni?>:</th>
					<td><?=$dni?></td>
				</tr>
				<tr>
					<th><?=$c_address?>:</th>
					<td><?=$address?></td>
				</tr>
				<tr>
					<th><?=$c_city?>:</th>
					<td><?=$city?></td>
				</tr>
				<tr>
					<th><?=$c_zip?>:</th>
					<td><?=$zip?></td>
				</tr>
				<tr>
					<th><?=$c_country_id?>:</th>
					<td><?=$country_id?></td>
				</tr>
				<tr id="provincia_id_holder">
					<th><?=$c_provincia_id?>:</th>
					<td><?=$provincia_id?></td>
				</tr>
			</table>
			<? if ($form_edit):?>
				<a class="lnk-right h3" href="/<?=LANGUAGE?>/product/address/show_form_new/customer_id/<?=$id?>/?template=clean" data-iframe="true">
				<?=$c_add_button?>
				</a>
			<? endif //form_new?>
			<h3>
				<?=$c_form_title4?>
			</h3>
			<? endif //!summary?>
			<? if ($is_summary):?>		
			<table class="form">
				<tr>
					<th><?=$c_form_title4?>:</th>
					<td><?=$address_delivery?></td>
				</tr>

				<? if ( $can_collect ): ?>

					<tr>
						<th class="collect"><?= $c_collect ?>:</th>
						<td><?= $collect ?></td>
					</tr>

				<? endif // $can_collect ?>

			</table>			
			<? endif //summary?>			
			<? if ($form_new):?>
			<table class="form same-address">
			  <tr>
				 <th><?=$c_same_address?>:</th>
				 <td class="checkbox-same-address"><input onclick="customer_show_address(this)" name="same_address" type="checkbox" id="same_address" checked="checked" /></td>
			  </tr>
			</table>
			<div class="hidden" id="delivery_address">
			  <table class="form">
				 <tr>
					<th><?=$c_a_name?>:</th>
					<td><input name="a_name2" type="text" value="" maxlength="50" /></td>
				 </tr>
				 <tr>
					<th><?=$c_a_surname?>:</th>
					<td><input name="a_surname2" type="text" value="" maxlength="50" /></td>
				 </tr>
				 <tr>
					<th><?=$c_address?>:</th>
					<td><input name="address2" type="text" value="" maxlength="255" /></td>
				 </tr>
				 <tr>
					<th><?=$c_city?>:</th>
					<td><input name="city2" type="text" value="" maxlength="50" /></td>
				 </tr>
				 <tr>
					<th><?=$c_zip?>:</th>
					<td><input name="zip2" type="text" value="" maxlength="10" /></td>
				 </tr>
				 <tr>
					<th><?=$c_country_id?>:</th>
					<td><?=$country_id2?></td>
				 </tr>
				  <tr id="provincia_id2_holder">
					  <th><?=$c_provincia_id?>:</th>
					  <td><?=$provincia_id2?></td>
				  </tr>
			  </table>
			</div>
			<? endif //form_new?>
			<? if ($form_edit):?>
				<div id="addresses_list">
					<?=$addresses?>
				</div>
			<? endif //form_edit?>
			
			<? if (!$is_summary):?>

				<? if ($form_new):?>

					<div class="accepta">

						<input class="accept-condicions" id="accepto" type="checkbox" name="accepto" value="1"/>
						<div><?= $c_politica_privacitat_text_1 ?>
							<a href="<?= get_page_link( 'politica' ) ?>?template=clean"  data-iframe="true"><?= $c_politica_privacitat_text_2 ?></a>
						</div>
					</div>

				<? endif //form_new?>

				<div class="buttons">
				<? if ($is_payment):?>
					<input class="boto" type="submit" name="Submit" value="<?=$c_next_payment_step?>" />
					<? endif //is_payment?>
				<? if (!$is_payment):?>
					<? if ($form_new):?>
						<input class="boto" type="submit" name="Submit2" value="<?=$c_send_new?>" />
					<? endif //form_new?>
					<? if ($form_edit):?>
						<input class="boto" type="submit" name="Submit" value="<?=$c_send_edit?>" />
					<? endif //form_edit?>
				<? endif //!is_payment?>
				</div>
			<?endif //is_summary?>

		<? if(!$is_summary): ?></form><? endif ?>

		<? if(!$is_summary): ?>
		<script type="text/javascript">
		$(function() {
			set_focus(document.theForm);
		});
		</script>
		<? endif ?>

	</div>
