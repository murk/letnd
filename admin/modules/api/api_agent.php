<?
/**
 * ApiAgent
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2011
 * @version $Id$
 * @access public
 */
class ApiAgent extends Module{

	var $condition = '';
	var $q = false;
	var $form_tabs;
	
	var $group_id, $user_is_admin, $user_is_agent, $agencia_id;

	function __construct(){
		parent::__construct();
		
		$this->group_id = $_SESSION['group_id'];
		
		$this->user_is_admin = $this->group_id < 3; // grups 2 i 1
		// els usuaris es el grup 3
		$this->user_is_agent = $this->group_id == 4; // grups 4
		
		if ($this->user_is_agent){
			$this->agencia_id = Db::get_first('SELECT agencia_id FROM user__user WHERE user_id = ' . $_SESSION['user_id']);
		}
		$this->order_by = 'nom ASC';
	}
	function on_load(){
		$this->form_tabs = [
			[
				'tab_action'  => $this->action == 'show_form_files'?false:'show_form_files&amp;tool=api&amp;tool_section=agent',
				'tab_caption' => $this->caption['c_edit_files']
			],
			[
				'tab_action'  => $this->action == 'show_form_map'?false:'show_form_map&amp;tool=api&amp;tool_section=agent',
				'tab_caption' => $this->caption['c_edit_map']
			],
		];
	}
	function list_records()
	{
		$content = $this->get_records();
		if ($this->q) {
			$GLOBALS['gl_page']->javascript .= "
				$('table.listRecord').highlight('".addslashes(R::get('q'))."');			
			";
		}
		$GLOBALS['gl_content'] = $this->get_search_form() . $content;
	}
	function get_records()
	{
	    // per defecte filtre actiu
		//if (!isset($_GET['actiu']) && (!$this->condition)) $_GET['actiu'] = 1;
		
		$listing = new ListRecords($this);
		$listing->set_var( 'user_is_agent', $this->user_is_agent );


		if ( $this->parent != 'NewsletterCustumer' ) {
			if ( ! $this->user_is_agent ) {
				$listing->add_button( 'edit_files', 'action=show_form_files', 'boto1', 'after' );
			}
			$listing->add_button( 'edit_map', 'action=show_form_map', 'boto1', 'after' );
		}
		
		//$listing->group_fields = array('agencia');
		
		// search
		if ($this->condition) {
			$listing->condition = $this->condition;
		}		
		if ($this->user_is_agent){
			$listing->condition .= $this->condition?' AND ':'';
			$listing->condition .= 'agencia_id = ' . $this->agencia_id;
			$listing->set_options (0, 0, 0, 0, 0, 0);
			$listing->set_field('api_id','list_admin','text');
			$listing->set_field('aicat_id','list_admin','text');
		}
		else{
			$listing->set_options (1, 1, 1);		
			if ($this->action!='list_records_bin') {
				$listing->add_filter('actiu');
				$listing->add_filter('comarca_id');
				$listing->add_filter('municipi_id');
				$listing->add_custom_filter(
					'has_files',
					[ '', 'yes', 'no' ]
				);

				$has_files = R::get( 'has_files', 'null' );

				$and = $this->condition?' AND ':'';
				if ($has_files == 'yes') {
					$listing->condition  .="$and
						((SELECT COUNT(*) 
						FROM api__agent_file 
						WHERE api__agent.agent_id = api__agent_file.agent_id) > 0
						)";
				}
				elseif ($has_files == 'no'){
					$listing->condition  .="$and  
						((SELECT COUNT(*) 
						FROM api__agent_file 
						WHERE api__agent.agent_id = api__agent_file.agent_id) = 0
						)";
				}
			}
		}
		if ($this->parent == 'NewsletterCustumer') {
			$listing->set_field('aicat_id', 'list_admin', '0');
			$listing->set_field('codi_habitat', 'list_admin', '0');
			$listing->set_field('agent_description', 'list_admin', '0');
			$listing->set_field('provincia_id', 'list_admin', '0');
			$listing->set_field('comarca_id', 'list_admin', '0');
			$listing->set_field('cp', 'list_admin', '0');
			$listing->set_field('telefon', 'list_admin', '0');
			$listing->set_field('telefon2', 'list_admin', '0');
			$listing->set_field('fax', 'list_admin', '0');
			$listing->set_field('correu', 'list_admin', '0');
			$listing->set_field('correu2', 'list_admin', '0');
			$listing->set_field('speciality_id', 'list_admin', '0');
			$listing->use_default_template = true;
			$listing->always_parse_template = true;
			return $listing;
		}
		
		$listing->order_by = $this->order_by;
		//$listing->condition = "actiu = '1'";
		//$listing->extra_fields = "(SELECT api__agencia.agencia FROM api__agencia WHERE api__agencia.agencia_id = api__agent.agencia_id) AS agencia";
		
	    return $listing->list_records();
	}
	function show_form_files()
	{
		if ( $this->user_is_agent ) {
			Main::error_404('Els agents no poden entrar aquí');
		}


	    $show = new ShowForm($this);
		$show->action = 'show_form_edit';
		$show->order_by = $this->order_by;
		$show->form_tabs = $this->form_tabs;

		$show->set_file('api/agent_files_form.tpl');

		$show->set_var('user_is_agent', false);

		$content =  $show->show_form();


		$GLOBALS['gl_content'] = $content;

	}
	function show_form()
	{
		$GLOBALS['gl_content'] =
		$this->get_form();
	}
	function get_form()
	{
	    $show = new ShowForm($this);
		$show->order_by = $this->order_by;
		$show->form_tabs = $this->form_tabs;
		$show->set_var('user_is_agent', $this->user_is_agent);
		
		if ($this->user_is_agent){
		
			$show->set_options (1, 0,  0, 0);
			
			$show->condition = 'agencia_id = ' . $this->agencia_id;
			$show->set_field('nom','form_admin','text');
			$show->set_field('cognoms','form_admin','text');
			$show->set_field('adresa','form_admin','text');
			$show->set_field('provincia_id','form_admin','text');
			$show->set_field('comarca_id','form_admin','text');
			$show->set_field('municipi_id','form_admin','text');
			$show->set_field('cp','form_admin','text');
			$show->set_field('telefon','form_admin','text');
			$show->set_field('telefon2','form_admin','text');
			$show->set_field('fax','form_admin','text');
			$show->set_field('correu','form_admin','text');
			$show->set_field('codi_habitat','form_admin','text');
			$show->set_field('url1','form_admin','text');
			$show->set_field('api_id','form_admin','text');
			$show->set_field('aicat_id','form_admin','text');
			$show->set_field('actiu','form_admin','0');
			$show->set_field('group_id','form_admin','0');
			
			// posar a text quan es canvi l'adreça
			$show->set_field('adress','form_admin','0');
			$show->set_field('numstreet','form_admin','0');
			$show->set_field('block','form_admin','0');
			$show->set_field('flat','form_admin','0');
			$show->set_field('door','form_admin','0');
		}
		else{
			$show->call_function['comarca_id'] = "get_select_comarques";
			$show->call_function['municipi_id'] = "get_select_municipis"; // comentat autocomplete
			$query_provincies = "provincia_id='" . str_replace(",", "' OR provincia_id='", API_PROVINCIES). "'";
			$show->fields["provincia_id"]['select_condition'] = $query_provincies;
			
			if($this->parent == 'NewsletterCustumer'){
			$show->form_tabs = array();
			}
		}
		
		
	    return $show->show_form() .
		'<script language="JavaScript" src="/admin/modules/api/jscripts/chained_selects.js?v='.$GLOBALS['gl_version'].'"></script>';
	}

	function save_rows()
	{
		// un agent no pot guardar llistats d'agents
		if ($this->user_is_agent) return;
		
	    $save_rows = new SaveRows($this);
	    $save_rows->save();
	}

	function write_record()
	{	    
		// un agent no pot borrar-se
		if ($this->user_is_agent){
			if (($this->action=='bin_record') || ($this->action=='delete_record')) return;
		}
		
		$writerec = new SaveRows($this);
		
		// m'asseguro que guardi nomes una agent de la agencia
		if ($this->user_is_agent){
		
			$query = 'SELECT agent_id FROM api__agent WHERE agent_id = '. $writerec->id .' AND agencia_id = ' . $this->agencia_id;
			
			if (!Db::get_first($query)) return;
		}
		
		
		if ($this->action == 'add_record') {
			$agencia_id = R::id('agencia_id');			
			$writerec->set_field('agencia_id','override_save_value',$agencia_id);
			
			// nomes guardo form new si hi ha agencia_id, nomes poden existir agents relacionats amb agencies
			if ($agencia_id) {
				$writerec->save();				
					
		
				// poso el remove key automaticament
				if ($this->action=='add_record' && $GLOBALS['gl_insert_id'])
					$this->newsletter_add_remove_key('api__agent', $GLOBALS['gl_insert_id']);
					}
			
		}
		else {
			$writerec->save();
		}
		
		if ($this->process=='agencia') {
			print_javascript('top.window.location.reload();');
		}
	}

	function manage_images()
	{
	    $image_manager = new ImageManager($this);
		$image_manager->form_tabs = $this->form_tabs;
	    $image_manager->execute();
	}
	//
	// FUNCIONS BUSCADOR
	//
	function search(){

		$q = $this->q = R::escape('q');
		
		$search_condition = '';

		if ($q)
		{
			// Sempre busco a tot arreu, abans si detectava que eren refrencies no hi  buscava, posaré checkbox de "buscar nomès referencies"

			$search_condition = "(
				nom like '%" . $q . "%'
				OR cognoms like '%" . $q . "%'
				OR CONCAT(nom, ' ', cognoms) like '%" . $q . "%'
				OR adresa like '%" . $q . "%'
				OR cp like '%" . $q . "%'
				OR telefon like '%" . $q . "%'
				OR telefon2 like '%" . $q . "%'
				OR fax like '%" . $q . "%'
				OR correu like '%" . $q . "%'
				OR url1 like '%" . $q . "%'
				OR agent_description like '%" . $q . "%'
				" ;

			// busco a agencia
			$query = "SELECT agencia_id FROM api__agencia WHERE (
								 agencia like '%" . $q . "%'
									)" ;
			$results = Db::get_rows_array($query);

			$search_condition .= $this->get_ids_query($results, 'agencia_id');
			$search_condition .= ") " ;
			$search_condition = $this->get_ref_query('agent_id',$q, $search_condition);

			$GLOBALS['gl_page']->title = TITLE_SEARCH . '<strong>&nbsp;&nbsp;"' . $q . '"</strong>';
		}
		$this->condition = $search_condition;
		Debug::add('Search condition', $this->condition);
		$this->do_action('list_records');
	}

	function get_ids_query(&$results, $field)
	{
		if (!$results) return '';
		$new_arr = array();
		foreach ($results as $rs){
			$new_arr [$rs[0]] = $rs[0];
		}
		return "OR " . $field . " IN (" . implode(',', $new_arr) . ") ";
	}
	function get_ref_query($field, $q, $search_condition){
		//
		// busqueda automatica de referencies separades per espais o comes
		//
		$ref_array = explode (",", $q); // si l'array hem dona 1 probo de fer-ho amb espais, si es una sola referncia donarà el mateix resultat
		count($ref_array) == 1?$ref_array = explode (" ", $q):false;

		foreach($ref_array as $key => $value)
		{
			if (!is_numeric($value))
			{
				$ref_array = '';
				break;
			}
		}

		// consulta
		if ($ref_array)
		{
			$query_ref = "";
			foreach ($ref_array as $key)
			{
				$query_ref .= $field . " = '" . $key . "' OR ";
			}
			$query_ref = "(" . substr($query_ref, 0, -3) . ") ";

			return '( ' . $query_ref . ' OR ' . $search_condition . ')';
		}
		else{
			return $search_condition;
		}
	}

	function get_search_form(){
		$this->caption['c_by_ref']='';
		$this->set_vars($this->caption);
		$this->set_file('search.tpl');
		$this->set_var('menu_id',$GLOBALS['gl_menu_id']);
		$this->set_var('q',  htmlspecialchars(R::get('q')));
		return $this->process();
	}
	//
	// FUNCIONS MAPA
	//	
	function show_form_map (){ //esenyo el napa
		global $gl_news;
		
		$agent_id = R::id('agent_id');
		
		// m'asseguro que guardi nomes una agent de la agencia
		if ($this->user_is_agent){
		
			$query = 'SELECT agent_id FROM api__agent WHERE agent_id = '. $agent_id .' AND agencia_id = ' . $this->agencia_id;
			
			if (!Db::get_first($query)) return;
		}
		
		
		$menu_id = R::id('menu_id');
		
		$rs = Db::get_row('SELECT * FROM api__agent WHERE agent_id = ' . $agent_id);
		
		$rs['municipi']=Db::get_first("SELECT municipi FROM inmo__municipi  WHERE municipi_id = " . $rs['municipi_id'] . "");
		
		$rs['title'] = 		
			Db::get_first("SELECT agencia FROM api__agencia  WHERE agencia_id = " . $rs['agencia_id'] . "") .
			' - ' .
			Db::get_first("SELECT CONCAT(nom,' ',cognoms) as nom FROM api__agent  WHERE agent_id = " . $agent_id . "");
		
		$rs['address'] = Db::get_first('SELECT adresa FROM api__agent  WHERE agent_id = ' . $agent_id);		
		$rs['is_default_point'] = ($rs['latitude']!=0)?0:1;
		$rs['latitude']=$rs['latitude']!=0?$rs['latitude']:GOOGLE_CENTER_LATITUDE;
		$rs['longitude']=$rs['longitude']!=0?$rs['longitude']:GOOGLE_CENTER_LONGITUDE;
		$rs['zoom']=$rs['zoom']?$rs['zoom']:GOOGLE_ZOOM;
		$rs['menu_id'] =  $menu_id;		
		$rs['tool'] =  $this->tool;		
		$rs['tool_section'] =  $this->tool_section;		
		$rs['menu_id'] =  $menu_id;		
		$rs['id'] =  $rs[$this->id_field];
		$rs['id_field'] =  $this->id_field;

	    $this->set_var ('form_tabs' , $this->form_tabs);

		$this->set_vars($rs);//passo els resultats del array
		$this->set_file('api/agent_map.tpl'); //crido el arxiu
		$this->set_vars($this->caption); //passo tots els captions
		$gl_news = $this->caption['c_click_map'];
		$content = $this->process();//processo
		$GLOBALS['gl_content'] = $content;		
	}
	function save_record_map(){
		global $gl_page;
		extract($_GET); 


		// m'asseguro que guardi nomes una agent de la agencia
		if ($this->user_is_agent){
		
			$query = 'SELECT agent_id FROM api__agent WHERE agent_id = '. $agent_id .' AND agencia_id = ' . $this->agencia_id;
			
			if (!Db::get_first($query)) return;
		}

		
		Db::execute ("UPDATE `api__agent` SET
			`latitude` = '$latitude',
			`longitude` = '$longitude',
			`zoom` = '$zoom'
			WHERE `agent_id` =$agent_id
			LIMIT 1") ;
		$gl_page->show_message($this->messages['point_saved']);
		Debug::p_all();
		die();			
	}
	function newsletter_add_remove_key($table, $id) {	

			$table_id = 'agent_id';

			$q2 = "SELECT count(*) FROM ". $table . " WHERE BINARY remove_key = ";
			$q3 = "UPDATE ". $table . " SET remove_key='%s' WHERE  ". $table_id . " = '%s'";

			$updated = false; 
			while (!$updated){
				$pass = get_password(50);
				$q_exists = $q2 . "'".$pass."'";	

				// si troba regenero pass
				if(Db::get_first($q_exists)){
					$pass = get_password(50);							
				}
				// poso el valor al registre
				else{				
					$q_update = sprintf($q3, $pass, $id);

					Db::execute($q_update);
					break;
				}
			}
	}

	// Comprova si existeixen els arxius
	function admintotal_check_files(){
		$query   = "SELECT * FROM api__agent_file";
		$dir = '/letnd/clients/api/api/agent/files/';
		$results = Db::get( $query );
		$conta = 0;
		foreach ( $results as $rs ) {
			$file_id = $rs['file_id'];
			$file_ext = strrchr(substr($rs['name'], -5, 5), '.');
			$file = "$dir$file_id$file_ext";
			$new_file = "${dir}si/${file_id}$file_ext";
			if (file_exists($file)){
				// rename($file, $new_file);
			}
			else {
				$conta ++;
				echo "$conta: $file <br> ";
			}
		}


		html_end();
	}

	// Comprova ids que falten
	function admintotal_check_ids(){
	$conta_falta = 0;
	$total = 0;
	$html = '';
		for ( $i = 3178; $i <= 5004; $i ++ ) {
			$total ++;
			$query   = "SELECT * FROM api__agent_file WHERE file_id = $i";
			$rs = Db::get_row( $query );
			if ($rs){
				$agent_id = $rs['agent_id'];
				$query   = "SELECT * FROM api__agent WHERE agent_id = $agent_id";
				$rs = Db::get_row( $query );
				$nom = $rs['nom'];
				$cognoms = $rs['cognoms'];
				$agencia_id = $rs['agencia_id'];
				$query   = "SELECT agencia FROM api__agencia WHERE agencia_id = $agencia_id";
				$agencia = Db::get_first( $query );

				$html .= "$i: <strong>$agencia</strong>: $nom $cognoms <br>";

			}
			else {
				$conta_falta ++;
				$html .= "$i <br>";
			}
		}

		$html = "
			<div style='padding-left: 50px;'>
				<div style='padding-bottom: 40px;'><strong>Falten:</strong> $conta_falta - <strong>Total:</strong> $total</div>
				$html
			</div>
		";

		html_end($html);
	}

}

// fora la clase
function get_select_comarques($key, $value)
{
	$query_provincies = "provincia_id='" . str_replace(",", "'
									OR provincia_id='", API_PROVINCIES) . "'
									ORDER BY comarca";
	return get_select_sub($value, $key, "inmo__comarca", "comarca, provincia_id, comarca_id", $query_provincies, '');
}
function get_select_municipis($key, $value)
{
	$query_comarques = "(inmo__municipi.comarca_id = inmo__comarca.comarca_id)
									AND (provincia_id='" . str_replace(",", "'
									OR provincia_id='", API_PROVINCIES) . "')
									ORDER BY municipi";
	return get_select_sub($value, $key, "inmo__municipi, inmo__comarca", "municipi, inmo__comarca.comarca_id, municipi_id", $query_comarques, '');
}

function get_select_sub($value, $name, $table, $fields, $condition = '', $caption = '', $onchange = '', $disabled = '')
{
	$html = '';
	$results = get_results_sub($table, $fields, $condition);

	if ($caption)
		$html .= '<option value="0">' . $caption . '</option>';
	if ($results)
	{
		foreach ($results as $rs)
		{
			$selected = '';
			if ($rs[2] == $value)
				$selected = " selected";
			$html .= '<option value="' . $rs[2] . '"' . $selected . '>#' . $rs[1] . "#" . $rs[0] . '</option>';
		}
	}
	if ($onchange)
		$onchange = ' onChange="' . $this->onchange . '"';
	if ($disabled)
		$disabled = ' disabled';
	$html = '<select name="' . $name . '"' . $onchange . $disabled . '>' . $html . '</select>';
	Debug::add("Get select", $html);
	return $html . '<input name="old_'.$name.'" type="hidden" value="0" />';
}

function get_results_sub($table, $fields, $condition)
{

	$query = "SELECT " . $fields . " FROM " . $table;
	if ($condition)
		$query .= " WHERE " . $condition;
	Debug::add("Consulta form objects", $query);
	$results = Db::get_rows_array($query);
	return $results;
}
?>