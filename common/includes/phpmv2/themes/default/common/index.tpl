<h1>Welcome in phpmyvisites 2.2 stable!</h1>

<p>This is the stable version of phpMyVisites! It should be fast, bug free (if not, tell us), and very easy to use.</p>
<p>You can read <a href="CHANGELOG">the changelog</a> for this release</p>
<p>If you have problems, <a href="http://www.phpmyvisites.us/forums/">try the official forums</a>.</p>

<p>If you are satisfied with phpMyVisites, please send us a message to fill in our "Use Cases" part on the website ; you can also help with the translation, or giving support on the forums...
<p><b>Our objective is that phpMyVisites becomes a great web analytics solution, stable and professional. To reach this objective, we need your help : don't hesitate, post feature requests, ask for ergonomic changes, interface improvements, give us ideas! What you or your company need to use phpMyVisites instead of a commercial tool? What is your "dream feature" that we could implement in phpMyVisites?
</b></p><p><a href="http://www.phpmyvisites.us/"> phpMyVisites needs your help / suggestions / review!</a></p>
<p align="center"><b>Enjoy your statistics!</b></p>