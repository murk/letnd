<?/* 
////////////////////////////////////////
BLOCK
////////////////////////////////////////
*/?>


<?if ($is_block):?>
	
		<a class="open" href="javascript:product.open_cart();">
			<?=$c_cart?>:			
			<span>(<?=isset($total_quantity) && $total_quantity!=0?$total_quantity:0?>)</span>
		</a>

		<?if($loop):?>
			<a class="buy" href="<?=$payment_link?>"><?=$c_buy?></a>			
		<?endif //loop?>
		
		<div class="cart-block<?=$cart_block_class?> popup">
			<h5><?=$c_title_cart?></h5>
			<?if ($loop):?>
				<?foreach($loop as $l): extract ($l)?>
				<div class="item">
					<?if($image_src_thumb):?>
						<a class="image" href="<?=$show_record_link?>" title="<?=htmlspecialchars($product_title)?>">
							<img <?=get_image_attrs($image_src_details, $image_alt?$image_alt:$product_title, 100, 100, false)?> />
						</a>
					<?endif //image?>
					<div class="text">
						<div class="ref"><?=$c_ref?>: <?=$ref?></div>
						<a class="title" href="<?=$show_record_link?>" title="<?=htmlspecialchars($product_title)?>">
							<span><?=$product_title?></span>
							<? if ($product_variation_categorys):?><br /><?=$product_variation_categorys?><? endif //?>
						</a>
						<table>
							<tr>
								<th><?=$c_product_basetax?>:</th>
								<td><?=$product_price?>&nbsp;<?=CURRENCY_NAME?></td>
							<tr>
								<th><?=$c_quantity?>:</th>
								<td class="quantity"><div>
										<a class="minus" href="javascript:cart_substract_quantity(<?=$id?>,1,'<?=$product_variation_ids?>');" title="-">-</a>
										<span><input onchange="cart_change_quantity(<?=$id?>, this.value, 1,'<?=$product_variation_ids?>')" type="text" name="quantity[<?=$id?>][<?=$product_variation_ids?>]" id="quantity_<?=$id?>_<?=$product_variation_ids?>" value="<?=$quantity?>"  /></span>
										<a class="plus" href="javascript:cart_add_quantity(<?=$id?>,1,'<?=$product_variation_ids?>');" title="+">+</a>
									</div></td>
							</tr>
							<tr class="item-total">
								<th><?=$c_product_total_basetax?>:</th>
								<td><?=$product_total_price?>&nbsp;<?=CURRENCY_NAME?></td>
							</tr>
						</table>
						<a class="eliminar" href="javascript:cart_delete_item('<?=$product_id?>', 1, '<?=$product_variation_ids?>')"><?= $c_delete ?></a>
					</div>
				</div>
				<? endforeach //$loop?>
				
				<div class="total">
					<?if ($is_logged):?>
					<p class="totaltax"><?=$c_rate_basetax?>: <?=$rate_price?>&nbsp;<?=CURRENCY_NAME?></p>
					<? endif //logged?>
					<p><?=$c_product_total_basetax?>: <strong><?=$all_price?>&nbsp;<?=CURRENCY_NAME?></strong></p>
				</div>
				
				<div class="buy">
					<a href="<?=$payment_link?>"><?=$c_buy?></a>
				</div>
				
			<?else: //loop?>
				<div class="norecords"><?=$c_no_records?></div>
			<?endif //loop?>
			
			<a class="close" href="javascript:product.open_cart()"></a>
			
		</div>
<?endif //is_block?>


<?/* 
////////////////////////////////////////
PÀGINA CISTELLA
////////////////////////////////////////
*/?>


<? if (!$is_block):?>
	<div class="table-holder" id="cart">
		<table class="table-products products">
			<tr class="tr-header">
				<? if(!$is_invoice): ?>
					<th></th>
				<? endif // $is_invoice ?>
				<th class="th-left"><?=$c_product_title?></th>
				<th class="th-left"><?=$c_ref?></th>
				<? if ($is_downloadable):?>
                    <th class="th-left" valign="top"><?=$c_data_digital?></th>
				<? endif //is_downloadable?>
				<th class="th-left"><?=$c_product_basetax?></th>
				<th class="th-left"><?=$c_quantity?></th>
				<th><?=$c_product_total_basetax?></th>
			</tr><?foreach($loop as $l): extract ($l)?>
			<tr class="<?=$first_last?>">
				<? if(!$is_invoice  ): ?>
					<td class="td-image">
						<? if($image_name): ?>
							<a href="<?= HOST_URL ?><?= $show_record_link ?>" title="<?= htmlspecialchars( $c_product_title ) ?>">
								<img <?= get_image_attrs( $image_src_details, $image_alt?$image_alt:$product_title, 120, 120, false, HOST_URL ) ?> />
							</a>
						<? endif //image_name?>
					</td>
				<? endif // !$is_invoice ?>
				<td class="td-title">
					<a href="<?=HOST_URL?><?=$show_record_link?>" title="<?=htmlspecialchars($c_product_title)?>" class="title">
						<?=$product_title?>
					</a>
					<?foreach($product_variation_loop as $l): extract ($l)?>
					<p><strong><?=$product_variation_category?></strong>: <?=$product_variation?></p>
					<?endforeach //$product_variations_loop?>
				</td>
				<td><strong class="mobile"><?= $c_ref ?>: </strong><?=$ref?></td>
				<? if($is_downloadable): ?>
					<td><strong class="mobile"><?= $c_data_digital ?>: </strong><?= $product_downloads ?></td>
				<? endif //is_downloadable?>
				<td><strong class="mobile"><?= $c_price ?>: </strong><? if ($old_price && !$is_invoice):?><span class="old-price"><?=$old_price?>&nbsp;<?=CURRENCY_NAME?></span> <? endif //old_price?><?= $is_invoice?$product_base:$product_price ?>&nbsp;<?=CURRENCY_NAME?></td>
				<td class="td-quantity">
					<? if ($is_summary):?>
						<strong class="mobile"><?= $c_quantity ?>: </strong><?=$quantity?>
					<? else: //is_summary?>
						<a href="javascript:cart_substract_quantity(<?=$id?>,0,'<?=$product_variation_ids?>');" title="-">-</a>
						<input onchange="cart_change_quantity(<?=$id?>, this.value, 0,'<?=$product_variation_ids?>')" type="text" name="quantity[<?=$id?>][<?=$product_variation_ids?>]" id="quantity_<?=$id?>_<?=$product_variation_ids?>" value="<?=$quantity?>"  />
						<a href="javascript:cart_add_quantity(<?=$id?>,0,'<?=$product_variation_ids?>');" title="+">+</a>
					<? endif //is_summary?>
				</td>
				<td class="td-price"><strong class="mobile"><?= $is_invoice?$c_product_total_base:$c_product_total_basetax ?>: </strong><strong class="price"><?= $is_invoice?$product_total_base:$product_total_price ?>&nbsp;<?=CURRENCY_NAME?></strong></td>
			</tr><?endforeach //$loop?>
		</table>

		<?///////////   TOTALS     ///////////////////?>

		<table class="table-products totals">
				<? if ($show_rate): // Si està logejat ?>

					<? if($is_invoice): ?>
						<tr class="total-base invoice-base">
							<th><?= $c_total_base ?>:</th>
							<td class="td-price" id="total_base_product"><?= $total_base ?>&nbsp;<?= CURRENCY_NAME ?></td>
						</tr>
						<tr class="rate-base invoice-base">
							<th><?= $c_rate_basetax ?>:</th>
							<td class="td-price" id="total_base_shipping"><?= $rate_base ?>&nbsp;<?= CURRENCY_NAME ?></td>
						</tr>
						<tr class="all-base invoice-base">
							<th><?= $c_all_base ?>:</th>
							<td class="td-price" id="total_all_base"><?= $all_base ?>&nbsp;<?= CURRENCY_NAME ?></td>
						</tr>
					<? else: // $is_invoice ?>
						<tr>
						  <th><?=$c_total_price?>:</th>
						  <td class="td-price" id="total_product"><?=$total_price?>&nbsp;<?=CURRENCY_NAME?></td>
						</tr>
						<tr>
						  <th><?=$c_rate_basetax?>:</th>
						  <td class="td-price" id="total_shipping" ><?=$rate_price?>&nbsp;<?=CURRENCY_NAME?></td>
						</tr>
					<? endif // $is_invoice ?>

					<? if ($is_tax_included):?>

						<? if ($show_promcode):?>
							<tr class="tr-promcode">
								<th><?=$c_promcode_basetax?>:</th>
								<td class="td-price"><strong>-<?=$promcode_discount?>&nbsp;<?=CURRENCY_NAME?></strong></td>
							</tr>
							<? endif //show_promcode?>

					<? endif // is_tax_included?>

					<? if(!$is_invoice): ?>
						<tr class="tr-total-price">
							<th><?= $c_all_price ?><? if($is_tax_included): ?> <?= $c_tax_included ?><? endif //is_tax_included?>:
							</th>
							<td class="td-price total" id="total_price"><?= $all_price ?>&nbsp;<?= CURRENCY_NAME ?></td>
						</tr>
					<? endif // $is_invoice ?>

					<? if (!$is_tax_included || $is_invoice):?>
						<tr>
						  <th><?=$c_all_tax?>:</th>
						  <td class="td-price"><?=$all_tax?>&nbsp;<?=CURRENCY_NAME?></td>
						</tr>

						<? if ($all_equivalencia):?>
							<tr>
							  <th><?=$c_all_equivalencia?>:</th>
							  <td class="td-price"><?=$all_equivalencia?>&nbsp;<?=CURRENCY_NAME?></td>
							</tr>
						<? endif //equivalencia?>

						<? if ($show_promcode):?>
						<tr class="tr-promcode">
							<th><?=$c_promcode_basetax?>:</th>
							<td class="td-price"><strong>-<?=$promcode_discount?>&nbsp;<?=CURRENCY_NAME?></strong></td>
						</tr>
						<? endif //show_promcode?>

						<tr class="tr-total-price all">
						  <th><?=$c_all_basetax?>:</th>
						  <td class="td-price total"><?=$all_basetax?>&nbsp;<?=CURRENCY_NAME?></td>
						</tr>
					<? endif // !is_tax_included?>

				<? endif //show_rate?>

				<? if (!$show_rate):?>
					<tr class="tr-total-price">
					  <th><?=$c_total_price?>:</th>
					  <td class="td-price total" id="total_price"><?=$total_price?>&nbsp;<?=CURRENCY_NAME?></td>
					</tr>
				<? endif //show_rate?>


		<?///////////  FI  TOTALS     ///////////////////?>


		</table>

		<? if ($show_promcode_form):?>
		<div class="promcode_form">
			<div class="error"><?=$promcode_error?></div>
			<div><span><?=$c_promcode?>: </span><?= $promcode_input ?><?= $promcode_button ?></div>
		</div>
		<? endif //show_promcode_form?>

		<? if ( $customer_comment && $is_payment ): ?>
			<label class="customer-comment form">
				<span><?= $c_customer_comment ?></span>
				<?= $customer_comment ?>
			</label>
		<? elseif ($customer_comment && $is_order ): ?>
			<div class="customer-comment form">
				<div><?= $c_customer_comment ?></div>
				<?= $customer_comment ?>
			</div>
		<? endif // customer_comment ?>

	</div>
							
							
				<? if (!$is_ajax):?>
				<script language="javascript">c_quantity = '<?=$c_quantity?>';</script>
					<? if (!$is_summary):?>
						<p class="cart_navigation"><a class="boto" href="/?language=<?=$language?>">
							<?=$c_continue_shopping?>
							</a><a class="boto" href="<?=$payment_link?>">
							<?=$c_next_payment_step?>
							</a></p>
						<div class="cls"></div>
					<? endif //is_summary?>
				<? endif //!is_ajax?>

<? endif //!is_block?>