<section id="banner-home">
	<div id="slider-home">
		<?foreach($loop as $l): extract ($l)?>
		<article class="item" title="<?=htmlspecialchars($property)?>">
		
			<div class="text">
				<h3><?=$property?></h3>
				
				<div class="ref">
					<?=$c_ref?> <?=$ref?>
				</div>
				
				<?=
				get_property_details(									
					'category_id,floor_space,room,bathroom,wc,land,municipi_id',$this, $l
					)
				?>
	
				<? if ($price):?>
				<div class="price"><?=$price?> <?=CURRENCY_NAME?></div>
				<? endif //price?>
				
				<? if ($price_consult):?>
				<div class="price-consult"><?=$price_consult?></div>
				<? endif //price_consult?>
				
			</div>			
			
			<?if($images_cat_2) extract($images_cat_2[0])?>
			
			<?if($image_name):?>
			<a href="<?=$link?>" title="<?=htmlspecialchars($property)?>"><img src="<?=$image_src_medium?>" width="960" height="400" alt="<?=$image_alt?$image_alt:$property?>" /></a>
			<?endif //images?>		
			
		</article>
		<?endforeach //$loop?>
	</div>
</section>