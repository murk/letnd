<?php
// no es poden posar ; entre mig de la consulta ja que parteix les consultes per el, a no ser que fem servir $update_sql en array() ;

if (is_table('product__configadmin')){
	$update_sql .="ALTER TABLE product__configadmin DROP language;";
}
if (is_table('product__configpublic')){
	$update_sql .="ALTER TABLE product__configpublic DROP language;";
}
if (is_table('contact__configadmin')){
	$update_sql .="ALTER TABLE contact__configadmin DROP language;";
}
if (is_table('contact__configpublic')){
	$update_sql .="ALTER TABLE contact__configpublic DROP language;";
}
if (is_table('newsletter__configadmin')){
	$update_sql .="ALTER TABLE newsletter__configadmin DROP language;";
}
if (is_table('missatge__missatge')){
	$update_sql .="ALTER TABLE missatge__missatge DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish2_ci;";
	$update_sql .="ALTER TABLE missatge__missatge CHANGE `kind` `kind` ENUM( 'mls', 'general' ) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NULL DEFAULT NULL;";
}
if (is_table('contact__configadmin')){
	$update_sql .="ALTER TABLE contact__configadmin DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish2_ci;";
	$update_sql .="ALTER TABLE `contact__configadmin` CHANGE `name` `name` VARCHAR( 50 ) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL;";
	$update_sql .="ALTER TABLE `contact__configadmin` CHANGE `value` `value` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL;";
}
if (is_table('contact__configpublic')){
	$update_sql .="ALTER TABLE contact__configpublic DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish2_ci;";
	$update_sql .="ALTER TABLE `contact__configpublic` CHANGE `name` `name` VARCHAR( 50 ) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL;";
	$update_sql .="ALTER TABLE `contact__configpublic` CHANGE `value` `value` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL;";
}
if (is_table('product__configadmin_language')){
	$update_sql .="ALTER TABLE `product__configadmin_language` CHANGE `value` `value` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL;";
}
if (is_table('product__configpublic_language')){
	$update_sql .="ALTER TABLE `product__configpublic_language` CHANGE `value` `value` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL;";
}
?>