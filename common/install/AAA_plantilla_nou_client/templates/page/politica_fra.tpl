<article>
	<p>Politique de confidentialité et protection des données <?=COMPANY_NAME?> fait partie de la volonté de fournir le meilleur service et une sécurité maximum pour nos clients. Les données fournies par nos clients en aucun cas être utilisés à des fins autres que celles énumérées ci-dessous</p>
	<p><?=COMPANY_NAME?> respecte l'accomplissement de la Loi 5/92 du 29 Octobre sur le traitement automatisé des données à caractère personnel. Par conséquent, le client ou l'utilisateur de notre site Web peuvent exercer leurs droits d'accès, de rectification, annulation et opposition en écrivant à l'adresse indiquée <?=COMPANY_NAME?> sur cette page.</p>
	<p><?=COMPANY_NAME?> se réserve le droit d'utiliser les informations fournies par le client ou l'utilisateur d'être en mesure d'envoyer du matériel de marketing, à moins que le client n'en dispose autrement.</p>
	<p>Les données sont automatiquement stockées dans un accès sécurisé aux fichiers limité. Dans tous les cas, les informations transmises à des tiers. Cette base de données est créé uniquement à des fins d'information liés à la commercialisation, des promotions et des statistiques <?=COMPANY_NAME?>.</p>
  <?if(COMPANY_DETAILS):?>
  <p><?=COMPANY_DETAILS?></p>
  <?endif //COMPANY_DETAILS?>
</article>