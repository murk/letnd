<?
if (!defined('USER_LOGIN_TITLE')) {

	define('USER_LOGIN_TITLE','Bienvenido a Letnd');
	define('USER_LOGOUT_TITLE','Estás desconectado');
	define('USER_LOGIN_TEXT','Introduce el login y password y pulsa aceptar');
	define('USER_LOGIN_AGAIN_TEXT','La sesión ha caducado. Vuelve a introducir el login y password');
	define('USER_LOGOUT_TEXT','Para volver a conectar introduce el login y password');



	// menus eina
	define('USER_MENU_USER','Usuarios');
	define('USER_MENU_USER_NEW','Crear Usuario');
	define('USER_MENU_USER_LIST','Listar Usuarios');
	define ('USER_MENU_USER_BIN','Papelera');
	define('USER_MENU_CONCESSION','Privilegios');
	define('USER_MENU_CONCESSION_LIST','Asignar Privilegios');
	define ('USER_MENU_GROUP','Grupos de usuarios');
	define('USER_MENU_GROUP_NEW','Crear grupo');
	define('USER_MENU_GROUP_LIST','Listar grupos');
	define ('USER_MENU_GROUP_BIN','Papelera');
	define ('USER_MENU_LOG','Historial');
	define ('USER_MENU_LOG_LIST','Listar historial');
	define ('USER_LOGIN_BROWSER_INCORRECT','Para poder utilizar este problema es necesario al menos Internet Explorer 8, Firefox 3.6, Safari 5 o Google Chrome 7<br>Estás utilizando %s versión %s');
	define('USER_MENU_ONEUSER', USER_MENU_USER);
	define('USER_MENU_ONEUSER_EDIT', 'Editar datos');
	define('USER_COMMENT_ONEUSER', 'Sólo editar datos propios del usuario');

	define('USER_MENU_PUBLIC','Usuarios público');
	define('USER_MENU_PUBLIC_NEW','Crear Usuario');
	define('USER_MENU_PUBLIC_LIST','Listar Usuari0s');
	define ('USER_MENU_PUBLIC_BIN','Papelera');
}


$users_login_form['c_login'] = 'Login';
$users_login_form['c_password'] = 'Password';
$users_login_form['c_send'] = 'Aceptar';




// captions seccio
$gl_caption_concession['c_tool'] = 'Módulo';
$gl_caption_concession['c_tool_section'] = 'Sección';
$gl_caption_concession['c_read1'] = 'Lectura';
$gl_caption_concession['c_write1'] = 'Escritura';
$gl_caption_concession['c_group_id'] = '';
$gl_caption_concession['c_comment'] = '';
$gl_caption_concession['c_bin'] = '';
$gl_caption_concession['c_search1'] = 'Elige grupo:';
$gl_caption_group['c_name']= "Nombre del grupo";
$gl_caption_group['c_view_users']= "Listar usuarios";
$gl_caption_group['c_show_html_editor']= "Permitir html en WYSIWYG";
$gl_caption_group['c_show_seo']= "Permitir SEO";
$gl_caption_group['c_is_editable']= "Editable";
$gl_caption_group['c_show_seo_0']= "No";
$gl_caption_group['c_show_seo_1']= "Sí";
$gl_caption_user['c_group_id']="Nombre del grupo";
$gl_caption_user['c_name']= "Nombre";
$gl_caption_user['c_surname']= "Apellidos";
$gl_caption_user['c_login']= "Nombre de usuario";
$gl_caption_user['c_passw']= "Contraseña";
$gl_caption_user['c_passw_repeat']= "Repetir contraseña";
$gl_caption_user['c_email']= "E-mail";
$gl_caption_user['c_email_repeat']= "Repetir e-mail";
$gl_caption_log['c_ip']= "Ip del ordenador";
$gl_caption_log['c_user_id']= "Usuario";
$gl_caption_log['c_date']= "Fecha";
$gl_caption_log['c_login']= "Hora de entrada";
$gl_caption_log['c_logout']= "Hora de salida";


$gl_messages['Error_no_groups'] = "Error!, Antes de crear usuaros, se tienen que crear grupos.";
$gl_messages['user_exist_del']='Error!, El grupo: <b>"%s"</b> contiene usuarios activos, no se puede borrar.';
$gl_messages['user_admin_del']='Error!, El grupo: <b>"%s"</b> no se puede borrar.';
$gl_messages['add_exists']='Ya existe un registro con este nombre de usuario.';

// NO TRADUIT

$gl_caption_public['c_group_id']="Nombre del grupo";
$gl_caption_public['c_name']= "Nombre";
$gl_caption_public['c_surname']= "Apellidos";
$gl_caption_public['c_login']= "Nombre de usuario";
$gl_caption_public['c_passw']= "Contraseña";
$gl_caption_public['c_email']= "E-mail";
// FI NO TRADUIT

$gl_caption_user['c_phone1'] = 'Principal';
$gl_caption_user['c_phone2'] = 'Trabajo';
$gl_caption_user['c_phone4'] = 'Privado';
$gl_caption_user['c_phone3'] = 'Fax';

$gl_caption_user['c_language_id'] = 'Idiomas';
$gl_caption_user['c_description'] = 'Descripción';
$gl_caption_user['c_form_level2_title_description'] = 'Descripciones';
$gl_caption_user['c_status'] = 'Estado';
$gl_caption_user['c_status_private'] = 'Privado';
$gl_caption_user['c_status_public'] = 'Público';

$gl_caption_user['c_edit_map'] = 'Mapa';
$gl_caption_user['c_form_map_title'] = 'Ubicación del agente';
$gl_caption_user['c_save_map'] = 'Guardar ubicación';
$gl_caption_user['c_latitude'] = 'Latitud';
$gl_caption_user['c_longitude'] = 'Longitud';
$gl_caption_user ['c_center_longitude'] = 'Longitud centro mapa';
$gl_caption_user ['c_center_latitude'] = 'Latitud centro mapa';
$gl_caption_user['c_zoom'] = 'Zoom';

$gl_caption_user ['c_center_coords'] = 'Centro';
$gl_caption_user['c_click_map'] = 'Haz un clic en un punto del mapa para establecer la situación';
$gl_messages['point_saved'] = 'Nueva ubicación guardada correctamente';
?>