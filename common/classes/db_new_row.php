<?php

/*
Per afegir files una a una en un formulari.
Es un list records per una sóla fila i per entrar com a nou registre
Es retorna directament en ajax
*/

class NewRow extends ListRecords {
	var $get_record_only = false;
	var $json = array();
	var $new_record_id;
	var $selected_id;
	var $related_id_parent;
	var $related_field_parent;
	var $related_field;
	static $hidden_config = array(
			'type'        => 'hidden',
			'enabled'     => '1',
			'list_admin'  => 'input',
			'list_public' => 'input',
		);

	/**
	 *
	 * @access public
	 *
	 * @param $parent_module
	 *
	 * @return NewRow
	 * @internal param bool $module
	 *
	 */
	function __construct( &$parent_module ) {
		// No ha de cridar el parent construct

		$fields                     =  &$parent_module->fields;
		$field                      = R::text_id( 'field' );
		$tool                       = $fields[ $field ]['sublist_tool'];
		$tool_section               = $fields[ $field ]['sublist_tool_section'];
		$this->related_field_parent = $parent_module->id_field;
		$this->related_field        = $fields[ $field ]['sublist_related_field'];

		// obtinc llistat
		$module         = Module::load( $tool, $tool_section, '', false );
		$module->action = 'get_newrow';

		BaseListShow::__construct( $module );
		$this->inputs_prefix  = $this->tool . "__" . $this->tool_section . '_new';
		$this->list_show_type = 'list';

		// heredades
		$this->has_bin               = false;
		$this->paginate              = false;
		$this->marked_ids            = array();
		$this->is_new_records        = true;
		$this->show_langs            = false; // per defecte nomès mostrem un idioma en els llistat
		$this->always_parse_template = true;
		$this->cols                  = 1;

		// pròpies
		$this->related_id_parent = R::id( 'id' );
		$this->new_record_id     = R::id( 'new_record_id' );
		$this->selected_id       = R::id( 'selected_id' );
		// si es major a 1 es que ja n'hem passat almenys 1 per ajax, per tant no mostrarem les columnes
		$this->get_record_only = $this->new_record_id > 1;


		if ( $module && is_array( $module->caption_list ) ) {
			$this->caption = array_merge( $this->caption, $module->caption_list );
		} // es $module->caption_list per que no està vinculat a link_module, ja que nomès es fa servir a list


		foreach ( $this->fields as $key => &$f ) {
			$f ['not_sortable'] = 1;
			if ( ! empty( $f ['list_new_admin'] ) ) {
				$f ['list_admin'] = $f ['list_new_admin'];
			}
		}

		// afegeixo els camps relacionat per no haver de posar-ho sempre al config
		$this->add_field( $this->related_field_parent, self::$hidden_config );
		$this->add_field( $this->related_field, self::$hidden_config );

		$inputs_prefix = $this->inputs_prefix;
		$related_field = $this->related_field;

		$this->add_button( 'delete', "'$field','$inputs_prefix','$related_field','new'",'btn_form_delete', 'before', 'sublist_delete' );

	}
	// ----------------------
	// Funció principal
	// ----------------------
	//
	// 1  - new_row()  -> ho fa tot, fa el query, obté results, processa els resultats i processa el template retornant el content
	// 2a - new_row(false) -> fa el query i obté la variable $this->results
	// 2b - parse_template() -> processa els resultats i processa el template retornant el content
	// 3  - set_row() -> fa tot menys processar el template i retornar el content
	// 3b - process() -> retorna el content després de fer un set_records()
	function new_row( $parse_template = true ) {

		$order_bar = $this->get_record_only ? false : true;
		// el tercer paràmetre ha de ser 1, sino no mostra els inputs
		$this->set_options( 0, 0, 1, 0, 0, 0, 0, 0, 0, $order_bar );

		$this->list_show();
		$this->set_vars_common();

		$this->has_results = ! ( empty( $this->results ) );

		if ( $parse_template ) {
			$ret['html'] = $this->parse_template();
			if (!DEBUG) header( 'Content-Type: application/json' );
			die ( json_encode( $ret ) );
		}
	}

	function set_row() {
		$this->new_row( false );
		$this->parse_template( false );
	}

	function add_row( $rs = array() ) {


		// Poso el valor per defecte del camp
		foreach ( $this->fields as $key => $field ) {
			if ( ! isset( $rs[ $key ] ) ) {
				$rs[ $key ] = $field['default_value'];
			}
		}
		// L'id del nou record
		$rs[ $this->id_field ] = $this->new_record_id;

		// L'id que relaciona amb les 2 taules relacionades
		$rs[ $this->related_field_parent ] = $this->related_id_parent;
		$rs[ $this->related_field ]        = $this->selected_id;


		/*		if ( ! $rs ) {
					$action = $this->module->action;
					$this->module->action = 'get_form_new';
					$show   = new ShowForm( $this->module );
					$show->show_form(false);
					$this->module->action = $action;

					$rs = $show->rs;
				}*/

		$this->results [] = $rs;
	}

	// Crido static per que no vull que sigui una clase NewRow, sino un ListRecords
	static function get_sublist_html( &$parent_module, $id, $field, $is_input ) {

		$fields                     =  &$parent_module->fields;
		$cg = $fields[ $field ];
		$tool                       = $cg['sublist_tool'];
		$tool_section               = $cg['sublist_tool_section'];
		$related_field_parent = $parent_module->id_field;
		$related_field        = $cg['sublist_related_field'];
		$inputs_prefix  = $tool . "__" . $tool_section . '_saved';


		// obtinc llistat
		$module         = Module::load( $tool, $tool_section, '', false );
		$module->action = 'get_records';

		$listing            = new ListRecords( $module );
		$listing->is_sublist = true;
		$listing->paginate  = false;
		$listing->has_bin   = false;

		if ($is_input) {
			$listing->is_editable = $is_input;
			$listing->set_options( 0, 0, 1, 0, 0, 0, 0, 0, 0, 1 );
			$listing->add_button( 'delete', "'$field','$inputs_prefix','$related_field','saved'", 'btn_form_delete', 'before', 'sublist_delete' );
		}
		else {
			$listing->is_editable = $is_input;
			$listing->set_options( 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 );
		}

		$listing->inputs_prefix  =$inputs_prefix;
		$listing->table_class = 'sublist sublist-saved';
		$listing->condition = "$related_field_parent =  $id";

		$listing->out_loop_hidden = "<input name=\"${inputs_prefix}[delete_record_ids]\" type=\"hidden\" value=\"\">";


		// afegeixo els camps relacionat per no haver de posar-ho sempre al config
		$listing->add_field( $related_field_parent, self::$hidden_config );
		$listing->add_field( $related_field,self::$hidden_config );
		$listing->no_records_message = '';

		return $listing;

	}
	/** Per guardar desde SaveRows
	 * @param $insert_id
	 * @param $cg
	 * @param $related_field_parent
	 * @param $action
	 */
	static public function save_records($insert_id, $field_parent, $cg, $related_field_parent, $action, &$caller_module) {

    	if (!$cg['sublist_tool'] || !$cg['sublist_tool_section']) die('No està ben configurat');

		if ( $action == 'save_record' ) {
	        $inputs_prefix = $cg['sublist_tool'] . '__' . $cg['sublist_tool_section'] . '_saved';
			self::save_saved_records($cg, $field_parent, $inputs_prefix, $caller_module);

			// reset dels nous registres i recarreguem el sublist
			/*print_javascript( '
				if (parent.document.theForm) {
					parent.sublist_reset_all();
				}' );*/

			// de moment recarrego tota la pàgina
			$GLOBALS['gl_reload'] = true;
		}


    	// tots els noms de les bbdd automàtics
	    $inputs_prefix = $cg['sublist_tool'] . '__' . $cg['sublist_tool_section'] . '_new';
		self::add_sublist_records($insert_id, $field_parent, $cg, $related_field_parent, $inputs_prefix, $caller_module);
	}

	// guarda els registres guardats
	static private function save_saved_records($cg, $field_parent, $inputs_prefix, &$caller_module) {

		// $related_field = $cg['sublist_related_field'];

		if ( !isset( $_POST[ $inputs_prefix ] ) ) return;

		// creo mòdul per guardar
		$module         = Module::load( $cg['sublist_tool'], $cg['sublist_tool_section'], '', false );
		$module->action = 'save_rows_save_records';

		$save_rows                = new SaveRows( $module, $inputs_prefix );

		$post = &$save_rows->post;

		// poso el old_post
		foreach ( $post[ $module->id_field ] as $key => $val ) {

			foreach ( $module->fields as $field => $f ) {
				if ( isset( $post[ $field ][ $val ] ) ) {
					$post[ 'old_' . $field ][$val] = $post[ $field ][ $val ];
				}
			}

		}

		$save_rows->save();

		// Borro els que s'han desmarcat
		$post_to_delete = $post['delete_record_ids'];

		$post_to_delete = $post_to_delete?explode( ',', $post_to_delete ):[];

		foreach ( $post_to_delete as $delete_id ) {
			$query = "DELETE FROM " . $cg['sublist_tool'] . "__" . $cg['sublist_tool_section'] . " WHERE " . $cg['sublist_tool_section'] . "_id = '$delete_id'";
			Db::execute( $query );
		}

		$method_name = $field_parent . '_on_save';
		if ( method_exists( $caller_module, $method_name ) ) {
			$caller_module->$method_name($save_rows->id, $post_to_delete, 'save_records') ;
		}

	}
	// guarda els registres nous
	static private function add_sublist_records( $parent_id, $field_parent, $cg, $related_field_parent, $inputs_prefix, $caller_module ) {

		$related_field = $cg['sublist_related_field'];

		if ( !isset( $_POST[ $inputs_prefix ] ) ) return;

		// creo mòdul per guardar
		$module         = Module::load( $cg['sublist_tool'], $cg['sublist_tool_section'], '', false );
		$module->action = 'add_record';

		$writerec                = new SaveRows( $module, $inputs_prefix );

		// afegeixo els camps relacionat per no haver de posar-ho sempre al config
		$writerec->add_field( $related_field_parent, self::$hidden_config );
		$writerec->add_field( $related_field, self::$hidden_config );


		$post = &$writerec->post;
		$add_ids = [];

		// poso el que haig de guardar amb id=0 que es com es guarda un add_record normal
		// i el old_post
		foreach ( $post[ $module->id_field ] as $key => $val ) {

			foreach ( $module->fields as $field => $f ) {
				if ( isset( $post[ $field ][ $val ] ) ) {
					$post[ $field ][0]          = $post[ $field ][ $val ];
					$post[ 'old_' . $field ][0] = $post[ $field ][ $val ];
				} else {
					unset( $post[ $field ][0] ); // per si de cas
					unset( $post[ 'old_' . $field ][0] ); // per si de cas
				}
			}

			// Necessari per quan s'afeggeix un sublist d'esde un registre nou
			$post[ $related_field_parent ][0]          = $parent_id;
			$post[ 'old_' . $related_field_parent ][0] = $parent_id;

			$writerec->save();
			$add_ids []= $writerec->id;
		}

		$method_name = $field_parent . '_on_save';
		if ( method_exists( $caller_module, $method_name ) ) {
			$caller_module->$method_name($add_ids, [], 'add_record') ;
		}
	}
}

?>