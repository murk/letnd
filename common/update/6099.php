<?

/* 3-11-16

	BOOKING - Afegir contractes en word


 */

if ( is_table( 'booking__configadmin' ) && ! Db::get_first( "SELECT * FROM booking__configadmin WHERE name='contract_format'" ) ) {
	$update_sql[] = "
		INSERT INTO booking__configadmin (`name`, `value`) VALUES ('contract_format', 'pdf');";

} else {
	if ( is_table( 'booking__configadmin' ) ) {
		trigger_error( 'Ja existeixen el camp contract_format' );
	}
}

?>