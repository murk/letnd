<?php
/* 2-5-18
Afegir opció entrega

*/

if( is_table( 'product__order' ) && !is_field( 'product__order', 'collect' ) ){

	$update_sql[]="ALTER TABLE `product__order`
	ADD COLUMN `collect` TINYINT NOT NULL DEFAULT '0' AFTER `deposit`;";

}else{
	if( is_table( 'product__order' ) ){
		trigger_error( 'Ja existeixen el camp collect' );
	}
}

if ( is_table( 'product__configadmin' ) && ! Db::get_first( "SELECT * FROM product__configadmin WHERE name='show_sector_at_listing'" ) ) {
	$update_sql[] = "
		INSERT INTO product__configadmin (`name`, `value`) VALUES ('show_sector_at_listing', '0');";

}
else {
	if ( is_table( 'product__configadmin' ) ) {
		trigger_error( 'Ja existeixen el camp show_sector_at_listing' );
	}
}