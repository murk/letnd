Clockwork.directive('prism', function () {
	return {
		restrict: 'A',
		link: function ($scope, element, attrs) {
			element.ready(function () {

				let $element = $(element[0]);
				let code = $element.html();

				code = window.sqlFormatter.format(code, {
							    language: "n1ql", // Defaults to "sql"
							    indent: "  "   // Defaults to two spaces
							});
				$element.html(code);

				Prism.highlightElement(element[0]);
			});

			$(element).on('click', function () {

				let el = element[0];
				let tag = $(el).parent().prop("tagName")

				if(tag === 'PRE') {
					$(el).unwrap()
				}
				else {
					$(el).wrap('<pre></pre>')
				}
			})
		}
	}
})
