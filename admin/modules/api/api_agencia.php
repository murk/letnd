<?
/**
 * ApiAgencia
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2011
 * @version $Id$
 * @access public
 */
 
/*

1 - Les agencies no son actives o inactives nomes els agents
		si una agencia te un inactiu surt al filtrat de inactius
		si una agencia te un actiu surt al filtrat de actius

2 - Els logos son a les agencies
	Els agents poden tindre imatges que poden ser fotos de l'oficina o d l'equip, etc...

3 - Al eliminar o enviar a la paperera, el grup d'usuaris relacionat farà el mateix

5 - Al enviar agencia a la paperera els seus usuaris també van a la paperera

6 - Al eliminar agencia elimino també usuaris

7 - Al restaurar agencia restauro també usuaris


*/

class ApiAgencia extends Module{

	var $agent_condition = 'api__agent.bin=0';

	var $condition = '';
	var $q = false;
	
	var $group_id, $user_is_admin, $user_is_agent, $agencia_id;
	
	function __construct(){
		parent::__construct();
		
		$this->group_id = $_SESSION['group_id'];
		
		$this->user_is_admin = $this->group_id < 3; // grups 2 i 1
		// els usuaris es el grup 3
		$this->user_is_agent = $this->group_id == 4; // grups 4
		
		if ($this->user_is_agent){
			$this->agencia_id = Db::get_first('SELECT agencia_id FROM user__user WHERE user_id = ' . $_SESSION['user_id']);
		}
		$this->order_by = 'agencia ASC';
	}
	function list_records()
	{
		// un agent no pot llistar d'agencies
		if ($this->user_is_agent) return;
		
		$content = $this->get_records();
		if ($this->q) {
			$GLOBALS['gl_page']->javascript .= "
				$('table.listRecord').highlight('".addslashes(R::get('q'))."');			
			";
		}
		$GLOBALS['gl_content'] = $this->get_search_form() . $content;
	}
	function get_records()
	{
	    // per defecte filtre actiu
		if (!isset($_GET['actiu']) && (!$this->condition)) $_GET['actiu'] = 1;
		
		
	    $listing = new ListRecords($this);
		
		$listing->set_options (1, 1, 0);
		
		//$listing->condition = '((SELECT count(*) FROM api__agent WHERE actiu=1 AND api__agent.agencia_id = api__agencia.agencia_id) >0)'; --> es molt lent		
		
		//$listing->join = 'LEFT JOIN api__agent USING (agencia_id)';
		//$listing->change_sql_id_field = "DISTINCT api__agencia.agencia_id";
		
		//$listing->condition = $this->agent_condition;
		
		// search
		if ($this->condition) {
			$listing->condition .= $this->condition;
		}	
		
		//$listing->add_filter('actiu');	
		
		$listing->order_by = $this->order_by;
		
		$listing->call('list_records_walk','',true);
		
		if ($this->user_is_admin){
			$listing->add_button ('add_new_agent', 'action=show_form_new&tool=api&tool_section=agent', 'boto2','before');
			$listing->add_button ('add_new_user', '', 'boto2','before','add_new_user');
			$listing->add_field('user_ids');
			$listing->set_field('user_ids','type','none');
			$listing->set_field('user_ids','list_admin','out');
			$listing->set_field('enquesta','list_admin','text');
		}
		
	    return '<script language="JavaScript" src="/admin/modules/api/jscripts/functions.js?v='.$GLOBALS['gl_version'].'"></script>' . $listing->list_records();
	}
	function list_records_walk($listing){
		$agencia_id = $listing->rs['agencia_id'];
		$ret['agents'] = '';
		$query = "SELECT agent_id, nom, cognoms, adresa, telefon, actiu, api_id, aicat_id FROM api__agent WHERE " . $this->agent_condition . " AND agencia_id = " . $agencia_id. ' ORDER BY nom ASC, cognoms ASC';
		//$query = "SELECT agent_id, nom, cognoms, adresa, telefon, actiu FROM api__agent WHERE actiu = " . $_GET['actiu'] . " AND " . $this->agent_condition . " AND agencia_id = " . $agencia_id;
		
		$results = Db::get_rows($query);
		
		$style = ($listing->list_show_type == 'form')?' style="margin-top:0;"':'';
		
		foreach ($results as $rs){
			$class = $rs['actiu'] ? '' : ' class="inactive"';
			
			$ret['agents'] .= 
				'<p'.$class . $style.'><strong>' . $rs['nom'] . ' ' . $rs['cognoms'] . '</strong><br />'  .
				$rs['adresa'] . ' tel. ' . $rs['telefon'] . '<br />'  .
				'API: ' . $rs['api_id'] . ' - AICAT: ' . $rs['aicat_id'] . '</p>';
			
			if (!$this->user_is_agent)
				$ret['agents'] .= 
				'<p style="margin:2px 0 12px;">
				<a href="/admin/?menu_id=2303&amp;tool=api&amp;tool_section=agent&amp;action=show_form_edit&amp;agent_id='.$rs['agent_id'].'">'.$this->caption['c_edit'].'</a>
				</p>';
			/* Per activar des daqui
			$ret['agents'] .= 
				'<p'.$class.'><strong>' . $rs['nom'] . ' ' . $rs['cognoms'] . '</strong><br />'  .
				$rs['adresa'] . ' tel. ' . $rs['telefon'] . '</p>
				<p style="margin:5px 0 9px;">
					<a href="/admin/?menu_id=2303&amp;tool=api&amp;tool_section=agent&amp;action=show_form_edit&amp;agent_id='.$rs['agent_id'].'">'.$this->caption['c_edit'].'</a>
					<a style="margin:0 0 0 8px;" href="/admin/?menu_id=2303&amp;tool=api&amp;tool_section=agent&amp;action=show_form_edit&amp;agent_id='.$rs['agent_id'].'">'.$this->caption['c_desactivar'].'</a>
				</p>';
			*/
		}
		// usuaris
		
		$ret['user_ids'] = '';
		
			$query = "SELECT user_id, name, surname, login FROM user__user WHERE agencia_id = " . $listing->rs['agencia_id'] . ' ORDER BY name ASC, surname ASC';
			$results = Db::get_rows($query);
			
			
			foreach ($results as $rs){
				
				$ret['user_ids'] .= 
					'<p'.$style.'><strong>' . $rs['name'] . ' ' . $rs['surname'] . '</strong></p>
					<p style="margin:2px 0 12px;">
					<a href="/admin/?menu_id=7003&action=show_form_edit&user_id='.$rs['user_id'].'">'.$this->caption['c_edit'].'</a>
					</p>';
			}
		
		// boto users
		//$listing->buttons['view_users']['params'] = '&user_ids=' . $listing->rs['user_ids'];
		
		// agefir usuaris i agents desde el formulari de l'agencia
		if ($listing->list_show_type == 'form' && !$this->user_is_agent) {
			$ret['agents'] .= '<a class="boto1" style="margin:10px 0 20px;" href="javascript:add_new_agent('.$agencia_id.')">'.$this->caption['c_add_new_agent'].'</a>';
			$ret['user_ids'] .= '<a class="boto1" style="margin:10px 0 20px;"  href="javascript:add_new_user('.$agencia_id.')">'.$this->caption['c_add_new_user'].'</a>';
		}
		
		return $ret;
	}
	
	
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{
	    $show = new ShowForm($this);
		$show->order_by = $this->order_by;
		
		if ($this->user_is_agent){
			$show->id = $this->agencia_id;
			$show->set_options (1, 0,  0, 0);
		}
			
		if ($this->user_is_admin){
			$show->add_field('user_ids');
			$show->set_field('user_ids','type','none');
			$show->set_field('user_ids','form_admin','out');
		}
		
		if ($this->action == 'show_form_new') {
			$show->unset_field('user_ids');
			$show->unset_field('agents');
		}
		else{
			$show->call('list_records_walk','',true);
		}
		
	    return '<script language="JavaScript" src="/admin/modules/api/jscripts/functions.js?v='.$GLOBALS['gl_version'].'"></script>' . $show->show_form();
	}

	function save_rows()
	{
		// un agent no pot guardar llistats d'agents
		if ($this->user_is_agent) return;
		
	    $save_rows = new SaveRows($this);		
	    $save_rows->save();
		
		$query = false;
		
		if ($save_rows->id) {
			// al enviar a la paperera, enviar grup a la paperera
			if ($this->action == 'save_rows_bin_selected' ){		
				$ids = implode (',', $save_rows->id);
				$query = "UPDATE `user__user` SET `bin`=1 WHERE  `agencia_id` IN (" .  $ids . ")";
			}
			
			// al recuperar, recuperar grup		
			if ($this->action == 'save_rows_restore_selected'){
				$ids = implode (',', $save_rows->id);
				$query = "UPDATE `user__user` SET `bin`=0 WHERE  `agencia_id` IN (" .  $ids . ")";
			}
			
			// al eliminar, eliminar grup de la paperera
			if ($this->action == 'save_rows_delete_selected'){
				$ids = implode (',', $save_rows->id);
				$query = "DELETE FROM `user__user` WHERE  `agencia_id` IN (" .  $ids . ")";	
			}
			
			if ($query) {
				Db::execute($query);
				debug::add('query grup',$query);
			}
		}
	}

	function write_record()
	{
		// un agent no pot borrar-se
		if ($this->user_is_agent){
			if (($this->action=='bin_record') || ($this->action=='delete_record')) return;
		}
		
	    // m'asseguro que nomes guardi lo de la propia agencia, no cal però no se sab mai
		if ($this->user_is_agent){
			$this->set_field('agencia_id','override_save_value',$this->agencia_id);
		}
		
	    $writerec = new SaveRows($this);
	    $writerec->save();
		
		// al enviar a la paperera, enviar usuaris a la paperera
		if ($this->action == 'bin_record') {
			$query = "UPDATE `user__user` SET `bin`=1 WHERE  `agencia_id`=" .  $writerec->id;
			Db::execute($query);
			debug::add('Usuari a la paperera',$query);
		}
		// al eliminar, eliminar usuaris
		if ($this->action == 'delete_record') {
			$query = "DELETE FROM `user__user` WHERE  `agencia_id`=" .  $writerec->id;
			Db::execute($query);
			debug::add('Usuari eliminat',$query);
		}
	}

	function manage_images()
	{
	    $image_manager = new ImageManager($this);
	    $image_manager->execute();
	}
	
	public static function user_user_on_end($action,$module){
		/*Debug::p($_GET);
		Debug::p($_POST);
		Debug::p($action);
		Debug::p($module);*/
		
		if ($action=='add_record' && $GLOBALS['gl_saved']  && isset($_GET['agencia_id'])){
		
			$agencia_id = $_GET['agencia_id'];
			$user_id = $GLOBALS['gl_insert_id'];			
			
			$query = "UPDATE `user__user` SET group_id=4,`agencia_id`=" . $agencia_id . " WHERE user_id = " . $user_id;			
			Debug::p($query, 'Update user');
			Db::execute($query);
			
			print_javascript('top.window.location.reload();');
			Debug::p_all();
			die();
			 
			/*$href = $_SESSION['last_list']['2303']['link'];
			if ($href) print_javascript('parent.window.location = "' . $href . '";');*/
		}
		if (!$GLOBALS['gl_saved']){
				$js = '
			if (typeof parent.hidePopWin == "function") parent.window.setTimeout("hidePopWin(false);", 800);
			if (typeof parent.showPopWin == "function") parent.showPopWin("", 300, 100, null, false, false, "'.addslashes($GLOBALS['gl_message']).'");';

			print_javascript($js);
			Debug::p_all();
			die();
		}
	}
	//
	// FUNCIONS BUSCADOR
	//
	function search(){

		$q = $this->q = R::escape('q');
		
		$search_condition = '';

		if ($q)
		{
			// Sempre busco a tot arreu, abans si detectava que eren refrencies no hi  buscava, posaré checkbox de "buscar nomès referencies"

			$search_condition = "(
				agencia like '%" . $q . "%'
				" ;

			// busco a agencia
			$query = "SELECT agencia_id FROM api__agent, api__agent_language WHERE (
				nom like '%" . $q . "%'
				OR cognoms like '%" . $q . "%'
				OR CONCAT(nom, ' ', cognoms) like '%" . $q . "%'
				OR adresa like '%" . $q . "%'
				OR cp like '%" . $q . "%'
				OR telefon like '%" . $q . "%'
				OR telefon2 like '%" . $q . "%'
				OR fax like '%" . $q . "%'
				OR correu like '%" . $q . "%'
				OR url1 like '%" . $q . "%'
				OR agent_description like '%" . $q . "%'
									)				
				AND api__agent.agent_id = api__agent_language.agent_id" ;
				
			$results = Db::get_rows_array($query);

			$search_condition .= $this->get_ids_query($results, 'agencia_id');
			$search_condition .= ") " ;
			$search_condition = $this->get_ref_query('agencia_id',$q, $search_condition);

			$GLOBALS['gl_page']->title = TITLE_SEARCH . '<strong>&nbsp;&nbsp;"' . $q . '"</strong>';
		}
		$this->condition = $search_condition;
		Debug::add('Search condition', $this->condition);
		$this->do_action('list_records');
	}

	function get_ids_query(&$results, $field)
	{
		if (!$results) return '';
		$new_arr = array();
		foreach ($results as $rs){
			$new_arr [$rs[0]] = $rs[0];
		}
		return "OR " . $field . " IN (" . implode(',', $new_arr) . ") ";
	}
	function get_ref_query($field, $q, $search_condition){
		//
		// busqueda automatica de referencies separades per espais o comes
		//
		$ref_array = explode (",", $q); // si l'array hem dona 1 probo de fer-ho amb espais, si es una sola referncia donarà el mateix resultat
		count($ref_array) == 1?$ref_array = explode (" ", $q):false;

		foreach($ref_array as $key => $value)
		{
			if (!is_numeric($value))
			{
				$ref_array = '';
				break;
			}
		}

		// consulta
		if ($ref_array)
		{
			$query_ref = "";
			foreach ($ref_array as $key)
			{
				$query_ref .= $field . " = '" . $key . "' OR ";
			}
			$query_ref = "(" . substr($query_ref, 0, -3) . ") ";

			return '( ' . $query_ref . ' OR ' . $search_condition . ')';
		}
		else{
			return $search_condition;
		}
	}

	function get_search_form(){
		$this->caption['c_by_ref']='';
		$this->set_vars($this->caption);
		$this->set_file('search.tpl');
		$this->set_var('menu_id',$GLOBALS['gl_menu_id']);
		$this->set_var('q',  htmlspecialchars(R::get('q')));
		return $this->process();
	}

	
	
	
	
	//
	// IMPORTAR
	//
	function _import()
	{
	    if (!$GLOBALS['gl_is_local']) return;
		
		ini_set('memory_limit', '512M');
		
		setlocale(LC_ALL, 'ca_ES'); 
		
		/////////////////////////////////////////////////////////
		// poso be els noms a api__agents
		/////////////////////////////////////////////////////////
		$results = Db::get_rows(
			"SELECT  `agent_id`,  LCASE(`nom vell`) as nom FROM `api__agent` ORDER BY `agencia_id` ASC");
		
		foreach ($results as $rs){
			
			R::escape_array($rs);
			extract($rs);
				
			$names = explode(',', $nom);
			
			$nom = ucwords (trim ($names[1] ));
			$cognoms = ucwords  (trim ($names[0] ));
		
			$query = 
				"UPDATE `api__agent` SET `nom`='$nom', `cognoms`='$cognoms' WHERE  `agent_id`=$agent_id;";
			Debug::p($query);
			Db::execute($query);
		}
		/////////////////////////////////////////////////////////
		// poso be els noms a api__agencia
		/////////////////////////////////////////////////////////
		$results = Db::get_rows(
			"SELECT  `agencia_id`,  LCASE(`agencia`) as agencia FROM `api__agencia` ORDER BY `agencia_id` ASC");
		
		foreach ($results as $rs){
			
			R::escape_array($rs);
			extract($rs);
						
			$agencia = ucwords (trim ($agencia ));
		
			$query = 
				"UPDATE `api__agencia` SET `agencia`='$agencia' WHERE  `agencia_id`=$agencia_id;";
			Debug::p($query);
			Db::execute($query);
		}
		
		/////////////////////////////////////////////////////////
		// importo usuaris
		/////////////////////////////////////////////////////////
		
		// grups
		/*$query = "INSERT INTO user__group (group_id, name, edit_only_itself) (SELECT agencia_id, agencia, 1 FROM api__agencia);";
		Db::execute($query);*/
		
		// usuaris
		$results = Db::get_rows(
			"SELECT  `agent_id`, nom, cognoms,  `agencia_id`,  `adresa`,  `cp`,  `municipi_id`,  `comarca_id`,  `poblacio`,  `comarca`,  `telefon`,  `telefon2`,  `fax`,  `correu`,  `web`,  `exerceix`,  `actiu`,  `usr`,  `pwd`,  `codi_habitat` FROM `api__agent` ORDER BY `agencia_id` ASC");
		
		foreach ($results as $rs){
			
			R::escape_array($rs);
			extract($rs);
			
			// si no he insertat ja l'usuari, per si està repetit
			if (!Db::get_first("
				SELECT  user_id FROM `user__user` WHERE passw = '$pwd' AND login='$usr'
				")){
			
				$query = 
					"INSERT INTO `user__user` 
						(user_id, `login`, `passw`, `name`, `surname`, `email`, `group_id`,agencia_id) VALUES 
						('$agent_id', '$usr', '$pwd', '$nom', '$cognoms', '$correu', 4, $agencia_id);";
				Debug::p($query, 'SI');
				Db::execute($query);				
			
			}
			else{
			
				Debug::p("'$agent_id', '$usr', '$pwd', '$nom', '$cognoms', '$correu', $agencia_id", 'NO, REPE');
			
			}
		}
		
		// borro login i pass de la taula agent
		
		$query = "ALTER TABLE `api__agent`
			DROP COLUMN `nom vell`,
			DROP COLUMN `usr`,
			DROP COLUMN `pwd`;";		
		Db::execute($query);
		
		
		/////////////////////////////////////////////////////////
		// importo logos
		/////////////////////////////////////////////////////////
		$results = Db::get_rows(
			"SELECT  `agencia_id`,  `agencia`,  `logo`,  `codi_habitat`,  `bin` FROM `api`.`api__agencia` WHERE logo is not null AND logo<>''");
			
		foreach ($results as $rs){
			R::escape_array($rs);
			extract($rs);
			$logo = strtolower($logo);
			$logo = str_replace ('.bmp','.jpg',$logo);
			
			$image_old = "D:\webs\altres\apigirona\logos\\$logo";
			
			if (file_exists($image_old)){			
				$size = filesize  ($image_old);
				
				$query = "INSERT INTO `api__agencia_image` 
					(`agencia_id`, `name`, `size`, `main_image`) VALUES 
					($agencia_id, '$logo', $size, 1);";
				Debug::p($query);						
				Db::execute($query);
				
				$image_id = Db::insert_id();
				
				
				
				$image_ext = strrchr(substr($logo, -5, 5), '.');
				
				$image_new = IMAGES_DIR . $image_id . $image_ext;
				copy($image_old, $image_new);
				
				$cg = $this->get_config('admin','inmo__configadmin');
				
				// DESCOMENTAR PER COPIAR LOGOS 
				ImageManager::resize_images($image_id, $image_ext, $cg);
				
			}
			else{
				Debug::p($image_old,'No Existeix la imatge');	
			}
		}
				
		foreach($GLOBALS['gl_languages']['names'] as $lang=>$val){
			$query="
			INSERT INTO api__agencia_image_language (image_id, language) SELECT DISTINCT image_id, '".$lang."' FROM api__agencia_image;";
			Db::execute($query);
			$query="
			INSERT INTO api__agent_language (agent_id, language) SELECT agent_id, '".$lang."' FROM api__agent;";
			Db::execute($query);
		}
		
		// poso web en idiomes
		$query="
		UPDATE api__agent_language, api__agent set url1 = web 
			WHERE api__agent_language.agent_id = api__agent.agent_id;";
		Db::execute($query);
		$query="
		ALTER TABLE `api__agent`
			DROP COLUMN `web`;;";
		Db::execute($query);
	}
}
?>