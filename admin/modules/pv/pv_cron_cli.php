<?
/*
TAREA
E:\Dropbox\z-Intranet\data\logs\cron.bat

DOS
C:\server\php5\php.exe -f "D:\Letnd\admin\modules\pv\pv_cron_cli.php" > d:\letnd\data\logs\pv.htm
C:\server\php5\php.exe -f "H:\z-Intranet\web\admin\modules\pv\pv_cron_cli.php" > E:\Dropbox\z-Intranet\data\logs\pv.htm

*/

$dir = str_replace( '/admin/modules/pv', '/common/', __DIR__ );
$dir = str_replace( '\admin\modules\pv', '\\common\\', $dir );

// INCLUDE CLI_INIT
if ( php_sapi_name() != 'cli' ) return;

// carrego per la configuració del mail
$gl_is_gli    = true;
$gl_is_local  = false;
$gl_is_linux  = ! is_dir( 'c:\\' );
$gl_site_part = 'admin';
$gl_is_admin  = true;
$gl_language  = 'cat';
$gl_version   = 10000000; // m'asseguro
$gl_is_mobile = false;
$gl_page      = new stdClass();
$gl_caption   = $gl_caption_list = $gl_caption_image = $gl_config = $gl_messages = [];

define( "PATH_TEMPLATES", '' );
define( "CLIENT_PATH", '' );
define( "VERSION", ( version_compare( PHP_VERSION, '7.0.0' ) >= 0 ) ? 7 : (( version_compare( PHP_VERSION, '5.0.0' ) >= 0 ) ? 5 : 4) );
define( 'LANGUAGE', 'cat' );
define( 'LANGUAGE_CODE', 'ca' );
define( 'CLIENT_DIR', '' );

if ( is_dir( 'D:/letnd' ) ) {
	define( 'HOST_URL', 'http://w.letnd/' );
	$_SERVER["HTTP_HOST"] = 'w.letnd';
	define( "DOCUMENT_ROOT", "d:/letnd/" );
	$gl_is_letnd = true;
	@include( 'config_db_letnd.php' );
}
else {
	define( 'HOST_URL', 'http://gestio.consultoriapv.com/' );
	$_SERVER["HTTP_HOST"] = 'gestio.consultoriapv.com';
	define( "DOCUMENT_ROOT", "E:/Dropbox/z-Intranet/web/" );
	$gl_is_letnd = false;
	include( DOCUMENT_ROOT . 'common/config_db.php' );
}


// Comença a contar
ini_set( 'date.timezone', 'Europe/Madrid' );//date_default_timezone_set('Europe/Madrid'); PHP5
define( 'DOCUMENT_TIME_START', microtime() );


include( DOCUMENT_ROOT . 'common/classes/debug_cli.php' );
include( DOCUMENT_ROOT . 'common/classes/request.php' );

Debug::start(); // començo el debug si s'escau

require_once( DOCUMENT_ROOT . 'common/vendor/autoload.php' );
include( DOCUMENT_ROOT . 'common/classes/module_base.php' );
include( DOCUMENT_ROOT . 'common/classes/module.php' );
include( DOCUMENT_ROOT . 'common/classes/db.php' );
include( DOCUMENT_ROOT . 'common/classes/db_query.php' );
include( DOCUMENT_ROOT . 'common/classes/thumb.php' );
include( DOCUMENT_ROOT . 'common/functions.php' );

Db::connect();
// FI INCLUDE CLI_INIT


include( DOCUMENT_ROOT . 'common/classes/db_form_object.php' );

include( DOCUMENT_ROOT . 'admin/modules/pv/pv_cron.php' );
include( DOCUMENT_ROOT . 'admin/modules/pv/languages/cat.php' );

$pv_cron          = New PvCron;
$pv_cron->caption = $gl_caption;
$pv_cron->is_cli  = true;
$pv_cron->start();