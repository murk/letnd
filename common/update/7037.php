<?

/* 2-2-18
 Vreasy - Fotocasa


 */

if ( is_table( 'booking__book' ) ) {

	$update_sql[] = "ALTER TABLE `booking__book`
	CHANGE COLUMN `book_synced` `synced` TINYINT(4) NOT NULL DEFAULT '1' AFTER `book_owner`;";


}
else {
	if ( is_table( 'booking__book' ) ) {
		trigger_error( 'No s\'ha pogut canviar book_synced' );
	}
}

if ( is_table( 'inmo__property_to_vreasy' ) ) {

	$update_sql[] = "ALTER TABLE `inmo__property_to_vreasy`
	DROP COLUMN `synced`;";

}
else {
	if ( is_table( 'inmo__property_to_portal' ) ) {
		trigger_error( 'No s\'ha pogut esborrar synced a inmo__property_to_vreasy' );
	}
}

if ( is_table( 'inmo__property_to_portal' ) && ! is_field( 'inmo__property_to_portal', 'synced' ) ) {

	$update_sql[] = "ALTER TABLE `inmo__property_to_portal`
	ADD COLUMN `synced` TINYINT NOT NULL DEFAULT '1' AFTER `vreasy`;";

}
else {
	if ( is_table( 'inmo__property_to_portal' ) ) {
		trigger_error( 'Ja existeixen el camp synced' );
	}
}
?>