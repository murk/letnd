<?php
function _block_llemena_pagina_get_children($parent_id, $level, $selected_ids, $menu, $last_level){
	$loop = array();
	if ($last_level == $level) return $loop;
	$query = "SELECT llemena__pagina.pagina_id as pagina_id, link, pagina, title, pagina_file_name, parent_id, has_content
					FROM llemena__pagina_language, llemena__pagina
					WHERE llemena__pagina_language.pagina_id = llemena__pagina.pagina_id
					AND status = 'public'
					AND parent_id = " . $parent_id . "
					AND bin <> 1
					AND menu = '" . $menu . "'
					AND language = '" . LANGUAGE . "'
					ORDER BY ordre ASC, pagina_id";
	Debug::add($query,'Query llemena_pagina_recursive');	
	$results = Db::get_rows($query);

	if ($results)
	{
		$conta = 0;
		$loop_count = count($results);
		foreach($results as $rs)
		{
			
			$selected = (
				(isset($selected_ids[$level]) && ($selected_ids[$level] == $rs["pagina_id"]))
				|| (strpos($rs['link'], 'pagina_id='.(isset($_GET['pagina_id'])?$_GET['pagina_id']:'null')) !== false)
				)?1:0;
			if ($selected) _block_llemena_set_page_title($rs["pagina_id"]);
			
			$l['link'] = _block_llemena_pagina_get_pagina_link($rs['pagina_id'],$rs['link'],$rs['pagina_file_name'],$rs['has_content']);
			$l['selected'] = $selected;
			$l['item'] = $rs['pagina'];
			$l['title'] = $rs['title']; // si no hi ha title, el titol es el pagina ( com era al principi ) -> _block_llemena_pagina_set_top_vars
			$l['has_content'] = $rs['has_content'];
			$l['id'] = $rs['pagina_id'];
			$l['conta'] = $conta;
			$l['loop_count'] = $loop_count;
			$l['loop'] = _block_llemena_pagina_get_children($rs['pagina_id'], $level+1, $selected_ids, $menu, $last_level);
			$conta++;
			$loop[] = $l;
		}
debug::add('loop',$loop);
		//$loop[] = array ('loop' => $loop2,
		//	'item' => '');
	}
	return $loop;
}
function _block_llemena_pagina_set_top_vars(&$loop, $selected_id){
	static $is_set_title = false;
	if ($is_set_title) return;
	$is_set_title = true;
	
	if (!$loop) return;
	foreach ($loop as $l){
		if ($l['id']==$selected_id) {
			$GLOBALS['gl_page']->top_title = $l['title']?$l['title']:$l['item'];// si no hi ha title, el titol es el page ( com era al principi )
			$GLOBALS['gl_page']->top_page_id = $l['id'];
			break;
		}
	}
	
}
// defineixo el titol i la descripcio de la pagina seleccionada, ja que no sempre entra al modul llemena p__pagina, 
// però si que vull que canvii el titol segons la pagina on som
 function _block_llemena_set_page_title($pagina_id){
	if (isset($_GET['pagina_id']) && $pagina_id!=$_GET['pagina_id']) return;
	$rs = Db::get_row("SELECT page_title, title, pagina, body_content 
						FROM llemena__pagina_language 
						WHERE pagina_id = ". $pagina_id . " 
						AND language = '".LANGUAGE."'");
	//El block s'executa desprès del mòdul, per tant si no s'ha definit el page desde el mòdul ho fa el block ( e. llistat de noticies i agenda )
	if (!$GLOBALS['gl_page']->title){
		$GLOBALS['gl_page']->title = $rs['title']?$rs['title']:$rs['pagina'];
		$GLOBALS['gl_page']->page_title = $GLOBALS['gl_page']->page_title?$GLOBALS['gl_page']->page_title:$rs['page_title'];
		$GLOBALS['gl_page']->page_description = add_dots_meta_description($rs['body_content']);
	}
}
function _block_llemena_pagina_get_pagina_link($pagina_id, $link, $pagina_file_name, $has_content)
{
	// si la pàgina no te contingut, agafo el link i tot del primer fill que es la pàgina que es mostra, així el link es exactamnet el mateix que el fill
	if (!$has_content){
		$rs=_block_llemena_get_first_child($pagina_id);
		if ($rs){
			$link = $rs['link'];
			$pagina_id = $rs['pagina_id'];
			$pagina_file_name = $rs['pagina_file_name'];
		}
	}
	
	if (strpos($link, 'javascript:') === 0) {
		$ret = $link;
	} elseif ($link)
	{
		if (USE_FRIENDLY_URL){
			$ret = '/' . LANGUAGE;
			if ($link != '/') // la home pagina no porta el pagina_id
			{
				$ret .= $link . '/' . $pagina_file_name . '.html';
			}
		}
		else{
			$sep = strstr($link, '?')?'&amp;':'?';
			$ret = $link . $sep . 'language=' . LANGUAGE;
			if (strpos($link, 'pagina_id=') === false && $link != '/') $ret .= '&amp;pagina_id=' . $pagina_id;
		}
	}
	else
	{
		if (USE_FRIENDLY_URL){
			$ret = '/' . LANGUAGE . '/' . $pagina_file_name . '.html';
		}
		else{
			$ret = '/?tool=llemena&amp;tool_section=pagina&amp;action=show_record&amp;pagina_id=' . $pagina_id . '&amp;language=' . LANGUAGE;
		}
	}
	// EVITAR DUPLICATS
	// si es la home, trec el link del menu d'idiomes
	if ($GLOBALS['gl_default_language']==LANGUAGE && $link == '/') $ret = '/';
	
	return $ret;
	
}
function _block_llemena_get_first_child($pagina_id){

	global $gl_language;
	
	$query = "SELECT template, link, pagina, page_title, page_description, page_keywords, title, body_subtitle, all__pagina.pagina_id as pagina_id, parent_id, level, has_content, pagina_file_name
			FROM all__pagina_language, all__pagina
			WHERE all__pagina_language.pagina_id = all__pagina.pagina_id
			AND status = 'public'
			AND parent_id = " . $pagina_id . "				
			AND bin <> 1
			AND language = '" . $gl_language . "'
			ORDER BY ordre ASC, pagina_id
			LIMIT 1";
	$rs = Db::get_row($query);	
	if ($rs) {		
		// si m'he equivocat a la taula, i poso que no te content, però tampoc hi ha cap fill, deixo el rs
		if (!$rs['has_content']) 
			return _block_llemena_get_first_child($pagina_id)?_block_llemena_get_first_child($pagina_id):$rs;
		else
			return $rs;
	}
	else {
		return false;
	}
}
?>