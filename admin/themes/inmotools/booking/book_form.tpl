<link href="/admin/themes/inmotools/styles/inmo_booking.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="/admin/jscripts/validacio.js?v=<?=$version?>"></script>
<script type="text/javascript" src="/admin/modules/booking/jscripts/functions.js?v=<?=$version?>"></script>
<script type="text/javascript" src="/admin/jscripts/assign.js?v=<?=$version?>"></script>
<script type="text/javascript" src="/admin/modules/booking/jscripts/list_pick.js?v=<?=$version?>"></script>

<?=$season_styles?>

<form class="form" id="theForm" name="theForm" method="post" action="<?=$link_action?>"  onsubmit="<?=$js_string?>" target="save_frame">

	<input name="book_id[<?=$id?>]" type="hidden" value="<?=$id?>" />
	<input name="book_id_javascript" type="hidden" value="<?=$id?>" />
	<?=$property_id?>
	
	<table width="100%" cellpadding="0" cellspacing="0">
		<tr>
			<td class="formCaptionTitle"><strong>
					<?=$c_form_title?> <?=$id?'- ' . $id:''?> <span class="property-title"> <?=$c_property_title?> Ref. <?=$ref?></span><?if($property_private):?><span class="private"> -  <?=$property_private?></span><?endif?>
				</strong>

				<? if ( $form_edit ): ?>
					<div class="right">
						<a class="btn_form1" href="javascript:open_print_window('?tool=booking&tool_section=book&action=show_form_edit&book_id=<?= $book_id ?>&template=print');"><?= $c_print ?></a>
					</div>
				<? endif // $form_edit ?>
			</td>
		</tr>
	</table>
	

	<table width="100%" cellpadding="0" cellspacing="0" class="formstbl book-record">
		<tr>
			<td width="150" class="formsCaption person"><strong>
				<?=$c_maximum?> <strong><?=$person?></strong> <?=$c_person?>:</strong>
			</td>
			<td class="forms">
				<table class="person">
					<tr>
						<th><?=$c_adult?></th>
						<th><?=$c_child?></th>
						<th><?=$c_baby?></th>
					</tr>
					<tr>
						<td><?=$adult?></td>
						<td><?=$child?></td>
						<td><?=$baby?></td>
					</tr>
				</table>				
			</td>
			
			<td rowspan="10" id="extres-list">
				<div class="extres-titol"><?=$c_extras?></div>
				<?=$extras?>
			</td>
			
		</tr>
		<tr>
			<td width="150" class="formsCaption">&nbsp;</td>
			<td class="forms">
					
			<div id="book-custumer"><?include('book_form_custumers.tpl')?></div>
			
			<?/*
				<div id="custumers"><?=$custumers?></div>
				<div id="select-custumers">
					<a href="javascript:select_custumers(<?=$id?>)"><?//=$c_add_custumers?></a> - 
					<a href="javascript:show_form_new_custumer(<?=$id?>)"><?//=$c_add_new_custumer?></a>
				</div> 
			*/?>
			</td>
			
		</tr>
		<tr>
			<td width="150" class="formsCaption"><strong>
					<?=$c_dates?>:</strong></td>
			<td class="forms">
				<?=$c_dates_from?> &nbsp;&nbsp; <?=$date_in?> &nbsp;&nbsp; <?=$c_dates_to?> &nbsp;&nbsp;<?=$date_out?>
			</td>
		</tr>
		<tr>
			<td width="150" class="formsCaption"><strong>
					<?=$c_status?>:</strong></td>
			<td class="forms">
				<?=$status?>
			</td>
		</tr>
		<tr>
			<td valign="top" class="formsCaption price"><strong>
					<?=$c_price?>:</strong></td>
			<td class="forms price">

				<div id="detail">
					<? if ( $detail ): ?><?= $detail ?><? endif // $detail ?>
				</div>

				<table class="prices">
					<tr>
						<td><label id="is_manual_price_holder"><?= $is_manual_price ?><span><?= $c_is_manual_price ?></span></label></td>
						<th><?=$c_price_book?></th>
						<td>
							<span id="price_book" class="automatic_price_text <?= $automatic_price_book_class ?>">
								<?=$price_book?>
							</span>
							<input value="<?=$price_book?>" type="text" class="manual_price_input <?= $manual_price_book_class ?>" name="manual_price_book" id="manual_price_book"> €</td>
					</tr>
					<tr>
						<td></td>
						<th><?=$c_price_extras?></th>
						<td>
							<span id="price_extras" class="automatic_price_text <?= $automatic_price_book_class ?>">
								<?=$price_extras?>
							</span>
							<input value="<?=$price_extras?> " type="text" class="manual_price_input <?= $manual_price_book_class ?>" name="manual_price_extras" id="manual_price_extras"> €</td>
					</tr>
					
					<tr id="promcode_tr" class="hidden<?=$show_promcode?>">
						<td></td>
						<th><?=$c_promcode?>:</th>
						<td id="promcode_discount">-<?=$promcode_discount?> €</td>
					</tr>
					
					<tr>
						<td></td>
						<th><?=$c_price_tt?>:</th>
						<td id="price_tt"><?=$price_tt?> €</td>
					</tr>
					
					<tr>
						<td></td>
						<th><?=$c_price_total?></th>
						<td id="price_total"><?= $price_total ?> €</td>
					</tr>
				
				</table>

				<? if ( $form_edit ): ?>
					<div id="recalculate"><label id="recalculate_rate"><input type="checkbox" value="1" name="recalculate_rate"><span><?= $c_recalculate_rate ?></span></label></div>
				<? endif // $form_edit ?>
			</td>
		</tr>
		<? if ($form_edit): // trec a l'hora de fer nou perque es complica el javascript ?>
		<tr>
			<td valign="top" class="formsCaption"><strong>
					<?=$c_payment?>:</strong></td>
			<td class="forms">
				<table class="payments">
					<tr>
						<th><?=$c_payment_num?></th>
						<th><?=$c_payment?></th>
						<th><?=$c_payed?></th>
						<? if ( BOOKING_HAS_PAYMENT ): ?><th><?=$c_payment_method?></th><? endif  ?>

						<th><?=$c_payment_entered_1?></th>
						<th></th>
					</tr>
					<tr>
						<td>1 - </td>
						<td><?=$payment_1?></td>
						<td><?=$order_status_1?></td>
						<? if ( BOOKING_HAS_PAYMENT ): ?><td><?=$payment_method_1?></td><? endif  ?>
						<td><?=$payment_entered_1?></td>
						<td><a id="payment_mail_1" class="hidden" href="javascript:bookingform.send_payed_mail(1)">&nbsp;<?= $c_payment_send_mail ?></a></td>
					</tr>
					<tr>
						<td>2 - </td>
						<td><?=$payment_2?></td>
						<td><?=$order_status_2?></td>
						<? if ( BOOKING_HAS_PAYMENT ): ?><td><?=$payment_method_2?></td><? endif  ?>
						<td><?=$payment_entered_2?></td>
						<td><a id="payment_mail_2" class="hidden" href="javascript:bookingform.send_payed_mail(2)">&nbsp;<?= $c_payment_send_mail ?></a></td>
					</tr>
					<tr>
						<td>3 - </td>
						<td><?=$payment_3?></td>
						<td><?=$order_status_3?></td>
						<? if ( BOOKING_HAS_PAYMENT ): ?><td><?=$payment_method_3?></td><? endif  ?>
						<td><?=$payment_entered_3?></td>
						<td><a id="payment_mail_3" class="hidden" href="javascript:bookingform.send_payed_mail(3)">&nbsp;<?= $c_payment_send_mail ?></a></td>
					</tr>
					<tr>
						<td>4 - </td>
						<td><?=$payment_4?></td>
						<td><?=$order_status_4?></td>
						<? if ( BOOKING_HAS_PAYMENT ): ?><td><?=$payment_method_4?></td><? endif  ?>
						<td><?=$payment_entered_4?></td>
						<td><a id="payment_mail_4" class="hidden" href="javascript:bookingform.send_payed_mail(4)">&nbsp;<?= $c_payment_send_mail ?></a></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td valign="top" class="formsCaption"><strong>
					<?=$c_payment_rest?>:</strong></td>
			<td class="forms payment_rest"><span id="payment_rest"></span> €</td>
		</tr>
		<? endif //$form_edit ?>

		<tr>
			<td valign="top" class="formsCaption"><strong>
					<?=$c_promcode?>:</strong></td>
			<td class="forms"><?=$promcode_input?> <?=$promcode_button?></td>
		</tr>
		<tr>
			<td valign="top" class="formsCaption"><strong>
					<?=$c_comment?>:</strong></td>
			<td class="forms"><?=$comment?></td>
		</tr>
		<tr>
			<td valign="top" class="formsCaption"><strong>
					<?=$c_comment_private?>:</strong></td>
			<td class="forms"><?=$comment_private?></td>
		</tr>
	</table>
	
	<table width="100%" cellpadding="4" cellspacing="2" class="formsButtonstbl">
		<tr>
			<td align="center" class="formsButtons"><div class="centerFloat1">
					<div class="centerFloat2"><? if ($form_new):?><? if ($save_button):?>
					<input type="submit" name="Submit2" value="<?=$c_send_new?>" class="btn_form1" />
					<? endif //save_button?><? endif //form_new?>
					<? if ($form_edit):?>
					<? if ($save_button):?>
							<? /* TODO-i Mirar si es posa back prev i next */ ?>

							<input type="submit" name="Submit" value="<?=$c_send_edit?>" class="btn_form1"><? endif //save_button?><?/* <? if ($delete_button):?><a class="btn_form_delete" href="javascript:submit_form('<?if ($has_bin):?><?=$c_confirm_form_bin?><?else:?><?=$c_confirm_form_deleted?><?endif //has_bin?>', true);">
				<?=$c_delete?>
				</a><? endif //delete_button?> */?>
					<? endif //form_edit?></div></td>
		</tr>
	</table>
</form>