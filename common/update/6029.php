<?

/* 5-2-15

Actualització utility
Formularis de càlculs de venda i compra immobles taules taxes

*/

if ( ! is_table( 'utility__taxtype' ) ) {
	$update_sql .= "
		CREATE TABLE `utility__taxtype` (
			`taxtype_id` INT(11) NOT NULL AUTO_INCREMENT,
			`tax` INT(11) NULL DEFAULT NULL,
			`taxtype` ENUM('IVA','ITP') NULL DEFAULT NULL COLLATE 'utf8_spanish2_ci',
			PRIMARY KEY (`taxtype_id`)
		)
		COLLATE='utf8_spanish2_ci'
		ENGINE=MyISAM;
	";
	$update_sql .= "
		INSERT INTO `utility__taxtype` (`taxtype_id`, `tax`, `taxtype`) VALUES (1, 10, 'IVA');
		INSERT INTO `utility__taxtype` (`taxtype_id`, `tax`, `taxtype`) VALUES (2, 10, 'ITP');
		INSERT INTO `utility__taxtype` (`taxtype_id`, `tax`, `taxtype`) VALUES (3, 21, 'IVA');
		INSERT INTO `utility__taxtype` (`taxtype_id`, `tax`, `taxtype`) VALUES (4, 5, 'ITP');
		INSERT INTO `utility__taxtype` (`taxtype_id`, `tax`, `taxtype`) VALUES (5, 5, 'ITP');

	";
} else {
	trigger_error( 'Ja existeixen la taula utility__configadmin' );
}

if ( ! is_table( 'utility__taxtype_language' ) ) {
	$update_sql .= "
		CREATE TABLE `utility__taxtype_language` (
			`taxtype_id` INT(11) NOT NULL,
			`language` CHAR(3) NOT NULL COLLATE 'utf8_spanish2_ci',
			`description` VARCHAR(200) NOT NULL COLLATE 'utf8_spanish2_ci',
			INDEX `taxtype_id` (`taxtype_id`)
		)
		COLLATE='utf8_spanish2_ci'
		ENGINE=MyISAM;
	";
	$update_sql .= "
		INSERT INTO `utility__taxtype_language` (`taxtype_id`, `language`, `description`) VALUES (1, 'cat', 'Vivenda nova');
		INSERT INTO `utility__taxtype_language` (`taxtype_id`, `language`, `description`) VALUES (2, 'cat', 'Vivenda segona mà');
		INSERT INTO `utility__taxtype_language` (`taxtype_id`, `language`, `description`) VALUES (3, 'cat', 'Local i garatge nou');
		INSERT INTO `utility__taxtype_language` (`taxtype_id`, `language`, `description`) VALUES (4, 'cat', 'Reducció menors 32 anys renda inferior a 30.000');
		INSERT INTO `utility__taxtype_language` (`taxtype_id`, `language`, `description`) VALUES (5, 'cat', 'Discapacitats');
		INSERT INTO `utility__taxtype_language` (`taxtype_id`, `language`, `description`) VALUES (1, 'spa', 'Vivienda nueva');
		INSERT INTO `utility__taxtype_language` (`taxtype_id`, `language`, `description`) VALUES (2, 'spa', 'Vivienda segunda mano');
		INSERT INTO `utility__taxtype_language` (`taxtype_id`, `language`, `description`) VALUES (3, 'spa', 'Local i garaje nuevo');
		INSERT INTO `utility__taxtype_language` (`taxtype_id`, `language`, `description`) VALUES (4, 'spa', 'Reducción menores 32 años renta inferior a 30.000');
		INSERT INTO `utility__taxtype_language` (`taxtype_id`, `language`, `description`) VALUES (5, 'spa', 'Discapacitados');
		INSERT INTO `utility__taxtype_language` (`taxtype_id`, `language`, `description`) VALUES (1, 'fra', 'Vivenda nova');
		INSERT INTO `utility__taxtype_language` (`taxtype_id`, `language`, `description`) VALUES (2, 'fra', 'Vivenda segona mà');
		INSERT INTO `utility__taxtype_language` (`taxtype_id`, `language`, `description`) VALUES (3, 'fra', 'Local i garatge nou');
		INSERT INTO `utility__taxtype_language` (`taxtype_id`, `language`, `description`) VALUES (4, 'fra', 'Reducció menors 32 anys renda inferior a 30.000');
		INSERT INTO `utility__taxtype_language` (`taxtype_id`, `language`, `description`) VALUES (5, 'fra', 'Discapacitats');

	";
} else {
	trigger_error( 'Ja existeixen la taula utility__taxtype_language' );
}
?>