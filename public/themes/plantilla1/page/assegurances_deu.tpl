<link href="../../templates/styles/general.css" rel="stylesheet" type="text/css" />
<table width="95%" border="0" cellpadding="0" cellspacing="0" class="fonsgeneralsclars">
  <tr>
    <td width="10"><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="10" height="1"></td>
    <td><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="1" height="15"></td>
    <td width="10"><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="10" height="1"></td>
  </tr>
  <tr>
    <td width="10">&nbsp;</td>
    <td align="left" valign="top"><img src="/clients/agenciaportselva/templates/images/logoasseguran.jpg" width="180" height="112"></td>
    <td width="10">&nbsp;</td>
  </tr>
  <tr>
    <td width="10">&nbsp;</td>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="30"><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="30" height="1"></td>
        <td><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="1" height="25"></td>
        <td width="30"><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="30" height="1"></td>
      </tr>
      <tr>
        <td width="30">&nbsp;</td>
        <td height="600" align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td align="left"><p class="text12verdanawhitejustify"><strong>Wer wir sind</strong></p>
              <p class="text12verdanawhitejustify">Wir sind ein Team aus Fachleuten, das seinen Kunden mit allen verfügbaren Mitteln zur Seite steht. Diesbezüglich verfügen wir über die entsprechenden Kenntnisse und Erfahrungen, da wir in der Versicherungsvermittlung seit 1983 als Versicherungsagent und seit 1988 als Versicherungsmakler tätig sind.</p></td>
          </tr>
          <tr>
            <td><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="1" height="15"/></td>
          </tr>
          <tr>
            <td align="center"><table width="80%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td  class="text12verdanawhite" align="center"><div align="center"><strong>Wir beraten Sie beim Abschluss Ihrer Versicherungspolicen und wahren Ihre Interessen bei der Erbringung der Versicherungsleistungen.</strong> </div></td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="1" height="15"/></td>
          </tr>
          <tr>
            <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="17"><img src="/clients/agenciaportselva/templates/images/logopetit.gif" width="18" height="18"/></td>
                    <td width="15"><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="15" height="1"/></td>
                    <td class="text10verdanawhite"><strong>Unsere Ziele</strong></td>
                  </tr>
                  <tr>
                    <td width="17"></td>
                    <td width="15">&nbsp;</td>
                    <td><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="15" height="15"/></td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td><p>Servicequalität für unsere Kunden anhand der folgenden Vorzüge:</p></td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="15" height="15"/></td>
                  </tr>
                  <tr>
                    <td width="17">&nbsp;</td>
                    <td width="15">&nbsp;</td>
                    <td>
                    
                    <li>Völlige Unabhängigkeit von den Versicherungsunternehmen</li>
                          <li>Vertretung der Kundeninteressen gegenüber den Versicherungsunternehmen</li>
                          <li>Ständige Weiterbildung</li>
                          <li>Spezialisierung</li>
                          <li>Gestaltung und Entwicklung von Versicherungsprodukten für jeden Bedarf</li>
                          <li>Individuelle Behandlung und maßgeschneiderter Service bei Prüfung, Abschluss und Gewährung der Versicherung sowie bei der Bearbeitung und Abwicklung von Schadensfällen</li>
                        
                      
                      </td>
                  </tr>
                </table></td>
                <td width="15"><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="15" height="1"/></td>
                <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="17"><img src="/clients/agenciaportselva/templates/images/logopetit.gif" width="18" height="18"/></td>
                    <td width="15"><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="15" height="1"/></td>
                    <td class="text10verdanawhite"><strong>Produkte von erstrangigen Unternehmen</strong></td>
                  </tr>
                  <tr>
                    <td width="17"></td>
                    <td width="15">&nbsp;</td>
                    <td><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="15" height="15"/></td>
                  </tr>

                  <tr>
                    <td width="17">&nbsp;</td>
                    <td width="15">&nbsp;</td>
                    <td>
                        <li>Brand- und Zusatzversicherungen</li>
                        <li>Diebstahl- und Zusatzversicherungen</li>
                        <li>Verbundene Versicherungen</li>
                        <li>-  Hausratversicherung </li>
                        <li>-  Gebäudeversicherung</li>
                        <li>-  Geschäftsversicherung</li>
                        <li>Industrieversicherung</li>
                        <li>Kfz-Versicherung</li>
                        <li>Rechtsschutzversicherung</li>
                        <li>Haftpflichtversicherung</li>
                        <li>-  Allgemein</li>
                        <li>-  Betriebsversicherung</li>
                        <li>-  Privat</li>
                        <li>-  Produkte</li>
                        <li>-  Jagd und Fischerei</li>
                        <li>Transportversicherungen</li>
                        <li>-  Kaskoversicherung</li>
                        <li>-  Bootsversicherung</li>
                        <li>Technische Versicherungen</li>
                        <li>-  Bauversicherung</li>
                        <li>-  10-Jahres-Versicherung für Bauschäden</li>
                        <li>-  Elektrogeräteversicherung</li>
                        <li>Agrarversicherung</li>
                        <li>Lebensversicherung (für Einzelpersonen, Kollektive oder mit Abkommen)</li>
                     
                        
                            <li>Rentenversicherung</li>
                            <li>Rentensparpläne</li>
                            <li>Investmentfonds</li>
                            <li>Sterbeversicherung</li>
                            <li>Krankenversicherung</li>
                            <li>Unfallversicherung (für Einzelpersonen, Kollektive oder mit Abkommen)</li>
                            <li>Reiseversicherung</li>
                            <li>Zahnversicherung</li>
                            <li>Berufsunfähigkeitsversicherung</li>
                            <li>Pflegeversicherung</li>
                            <li>Verschiedene Risiken</li>
                         </td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                  </tr>
                </table></td>
              </tr>

            </table></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
        </table><p class="text12verdanawhitejustify">&nbsp;</p>          </td>
        <td width="30">&nbsp;</td>
      </tr>
    </table></td>
    <td width="10">&nbsp;</td>
  </tr>
  <tr>
    <td width="10">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td width="10">&nbsp;</td>
  </tr>
  <tr>
    <td width="10">&nbsp;</td>
    <td><img src="/clients/agenciaportselva/templates/images/spacer.gif" width="40" height="15"></td>
    <td width="10">&nbsp;</td>
  </tr>
</table>
