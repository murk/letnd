<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN">
<html>
<head>
	<title><?= $page_title ?></title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

	<?=load_admin_css()?>

	<link rel="stylesheet" href="/common/includes/font-awesome/css/font-awesome.min.css">

	<link href="/common/jscripts/jquery-ui-1.8.20/css/ui-lightness/jquery-ui-1.8.20.custom.css" rel="stylesheet" type="text/css"/>
	<!--[if IE 6]>
	<style>
		#inner_content {
			width: 98%;
		}
	</style>
	<![endif]-->
	<!--[if IE 7]>
	<style>
		#inner_content {
			width: 98%;
		}
	</style>
	<![endif]-->
	<script type="text/javascript" src="/common/jscripts/jquery/jquery-1.7.2.min.js"></script>

	<script type="text/javascript">

		var gl_language = '<?=$language?>';
		var gl_menu_id = '<?=$GLOBALS['gl_menu_id']?>';
		var gl_tool = '<?=$tool?>';
		var gl_tool_section = '<?=$tool_section?>';
		var gl_action = '<?=$action?>';
		var gl_current_form_id = <?=$current_form_id===false?'false':$current_form_id?>;

		var html_editors = <?=$html_editors?>;

		$(document).ready(
			function () {
				$('#content').height($("#popupFrame", parent.document).height());
				<?=$javascript?>
			}
		)
	</script>

	<? if ( $html_editors_params ): ?>

		<script type="text/javascript" src="/common/jscripts/tinymce.5/tinymce.min.js?v=<?= $version ?>"></script>
	<? if ( is_file( PATH_TEMPLATES_PUBLIC . 'tinymce/templates.js' ) ): ?>
		<script type="text/javascript" src="<?= DIR_TEMPLATES_PUBLIC ?>tinymce/templates.js"></script>
	<? endif //$ ?>

		<script type="text/javascript" src="/common/jscripts/html_editor.php?<?= $html_editors_params ?>"></script>
	<? endif //html_editors?>


	<? if( ! DEBUG && !$GLOBALS['gl_is_local']): ?>
		<script type="text/javascript" src="/common/jscripts/jquery.onerror.js?v=<?= $version ?>"></script>
	<? endif //DEBUG?>
	<script type="text/javascript" src="/common/jscripts/dropzone/dropzone.js"></script>
	<script type="text/javascript"
	        src="/common/jscripts/jquery-ui-1.8.20/js/jquery-ui-1.8.20.custom.min.js?v=<?= $version ?>"></script>
	<script type="text/javascript"
	        src="/common/jscripts/jquery-ui-1.8.20/languages/jquery.ui.datepicker-<?= $language_code ?>.js?v=<?= $version ?>"></script>
	<script type="text/javascript" src="/common/jscripts/jquery.highlight-4.js?v=<?= $version ?>"></script>
	<script type="text/javascript" src="/common/jscripts/jquery.dump.js?v=<?= $version ?>"></script>
	<script type="text/javascript" src="/common/jscripts/jquery.mousewheel.js?v=<?= $version ?>"></script>
	<script type="text/javascript" src="/common/jscripts/jquery.timepicker.js?v=<?= $version ?>"></script>
	<script type="text/javascript" src="/common/jscripts/jquery.cookie.js?v=<?= $version ?>"></script>
	<script type="text/javascript" src="/common/jscripts/jquery.blockUI.js?v=<?= $version ?>"></script>
	<script type="text/javascript" src="/common/jscripts/jquery.dropshadow.js?v=<?= $version ?>"></script>
	<script type='text/javascript' src='/common/jscripts/jquery.autocomplete.min.js'></script>
	<script language="JavaScript" src="/common/jscripts/multifile/jquery.MultiFile.js?v=<?= $version ?>"></script>
	<script type="text/javascript" src="/admin/jscripts/main.js?v=<?= $version ?>"></script>
	<script type="text/javascript" src="/admin/languages/<?= $language ?>.js?v=<?= $version ?>"></script>

	<? /* Validacions  */ ?>
	<script type="text/javascript" src="/common/jscripts/jquery-validation/jquery.validate.js"></script>
	<script type="text/javascript" src="/common/jscripts/jquery-validation/localization/messages_<?= $language_code ?>.min.js"></script>
	<script type="text/javascript" src="/common/jscripts/jquery-validation/jquery-validate.bootstrap-tooltip.min.js"></script>
	<script type="text/javascript" src="/common/jscripts/bootstrap-js/tooltip.js"></script>
	<script type="text/javascript" src="/common/jscripts/jquery.inputmask/jquery.inputmask.bundle.min.js"></script>
	<script type="text/javascript" src="/common/jscripts/datejs/date.min.js"></script>
	<script type="text/javascript" src="/common/jscripts/datejs/date-<?= $language_code ?>.min.js"></script>
	<? /* Fi validacions */ ?>

	<? foreach ( $javascript_files as $l ): extract( $l ) ?>
		<script type="text/javascript" src="<?= $javascript_file ?>?v=<?= $version ?>"></script>
	<? endforeach //$javascript_files?>
	<? foreach ( $css_files as $l ): extract( $l ) ?>
		<link href="<?= $css_file ?>?v=<?= $version ?>" rel="stylesheet" type="text/css"/>
	<? endforeach //$css_files?>

</head>
<body class="window">
<div id="content" style="overflow: auto;"><div id="inner_content"><?if ($title_right):?><div class="right"><?=$title_right?></div><?endif //title_right?><?=$content?></div></div>
<iframe name="save_frame" id="save_frame" scrolling="yes" marginwidth="0" marginheight="0" frameborder="1" vspace="0" hspace="0" style="overflow:visible; width:90%; height: 400px; display:<?=$showiframe?>"></iframe>
</body>
</html>
