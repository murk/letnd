<?php
// variables
include (DOCUMENT_ROOT . 'admin/modules/print/print_showwindow_tpl_start.php');

// poso la funció aquesta per obtindre els parametres que depenen del disseny, i així posar la logica al print_showwindow_tpl_start.php
// faig extract d'aquestes variables, cuidado no posar el mateix nom a una altra
function pdf_get_config(){
	return array(
		'text_fields_increment_1' => 30, // distancia entre camps quan pocs camps
		'text_fields_increment_2' => 24, // distancia entre camps quan hi ha mes camps
		'text_fields_increment_3' => 17, // distancia entre camps quan hi ha mes camps
		'max_fields_showwindow' => 10,   // maxim camps per aparador
		'ref_size_album' => 16,   // mida del ref a album
		'ref_size_showwindow' => 20,   // mida del ref a album
		'image1_x' => 31.5,   // imatge 1
		'image1_y' => 111,
		'image1_w' => 0,
		'image1_h' => 354,
		'image2_x' => 31.5,  // imatge 2
		'image2_y' => 468,
		'image2_w' => 0,
		'image2_h' => 354,
	);
}


function set_pdf_header(&$this)
    {
		extract(pdf_get_config());

		// ratlles grises
        //$this->SetDrawColor(204, 201, 199);
        //$this->Rect(15, 14, 565, 50, 'D');
        //$this->Rect(15, 75, 565, 751.5, 'D');
        // quadres verds
        $this->SetFillColor(153,153,153);
        $this->Rect(15, 13.5, 565, 487.6, 'F');
        //$this->Rect(15, 821.5, 565, 5, 'F');
        // referencia
        $this->SetTextColor(204, 204, 204);
        $this->SetFont('Verdanab', '', $this->ref_size);
        $this->Text(30, 107, $this->c_ref . ' ' . $this->ref);
        // logo
        $this->Image($this->logo, 420, 13.5, 150);
    }


$pdf->SetMargins(27, 84);
$pdf->SetAutoPageBreak(true, 30);
$pdf->SetDisplayMode('real');
$pdf->AddFont('verdanab', '', 'verdanab.php');
// ////////////////////////////////////////////////////////////
// ////////                         ///////////////////////////
// /                 ALBUM                       /////////////
// ///////                          /////////////////////////
// /////////////////////////////////////////////////////////
if ($gl_is_album)
{
    // si hi ha imatges a la primera pagina hi va text+foto i les altres 2 fotos
    for ($image_number = 0;$image_number < count($images);$image_number = $image_number++)
    {
        $pdf->AddPage();
        $image1 = isset($images[$image_number])?DOCUMENT_ROOT . $images[$image_number]['image_src']:false;
        $image2 = isset($images[$image_number + 1])?DOCUMENT_ROOT . $images[$image_number + 1]['image_src']:false;
        // $image = file_exists(DOCUMENT_ROOT . $image_src_original)?DOCUMENT_ROOT . $image_src_original:DOCUMENT_ROOT . $image_src;
        // primera imatge
        if ($image1) $pdf->Image($image1, $image1_x, $image1_y , $image1_w , $image1_h);

//print_r($pdf->images[$image1]);die;
        if ($image_number == 0)
        {
            // quadre verd clar
            $pdf->SetFillColor(221, 221, 221);
            $pdf->Rect(15, 501.131, 565, 325.369, 'F');
            // Titol mig
        	$pdf->SetTextColor(204, 204, 204);
            $pdf->SetFont('Arial', '', 18);
            $pdf->Text(30, 492.945, $property);
            // Property_title
        	$pdf->SetTextColor(0, 0, 0);
            $pdf->SetFont('Arial', 'I', 14);
            $pdf->SetY(505);
            if ($property_title)
            {
                $pdf->MultiCell(0, 16, $property_title, 0, 'L');
                // així controlo l'espai entre titol i descripcio
                $pdf->Cell(100, 10, '', 0);
                $pdf->Ln();
            }
            // Descripció
            $pdf->SetFont('Arial', '', 12);
            $pdf->MultiCell(0, 16, $description);
            // detalls
            $i = 0;
            // alçada pagina - marge inferior - alçada celes fields - alçada 2 celes contorns - Preu
            $fields_y = 841.88-30 - (ceil(count($fields_pdf) / 2) * 16)-10-26;
            // si cap a la primera pagina dibuixo el quadre  abaix de tot, sino a continuació del text
            if ($pdf->GetY() > $fields_y-5)
            {
				// posa el quadre tot sencer a l'altre pàgina
                $pdf->AddPage();
            }
			else
			{
				//	sino afegeix 1 salts de linea
                $pdf->Ln(10);
			}
            //if ($pdf->PageNo() == 1) $pdf->SetY($fields_y);
            // contorn superior
            $pdf->SetDrawColor(153,153,153);
            $pdf->Cell(542, 6, '', 'TRL');
            $pdf->Ln();
            foreach($fields_pdf as $key => $value)
            {
                // contorn esquerra i dret
                $L = $i == 0?'L':0;
                $R = $i == 1?'R':0;
                // cel.les
                $pdf->SetTextColor(0, 0, 0);
                $pdf->Cell(100, 16, ' ' . $key, $L);
                $pdf->SetTextColor(124, 124, 124);
                $pdf->Cell(171, 16, $value, $R);
                // cada 4 celes un salt de linea
                $i == 1?$pdf->Ln():false;
                $i = $i == 0?1:0;
            }
            // si es impar dibuixo cela buida per acabar el contorn
            if ($i == 1)
            {
                $pdf->Cell(100, 16, '', 0);
                $pdf->Cell(171, 16, '', 'R');
                $pdf->Ln();
            }
            // contorn inferior
            $pdf->Cell(542, 4, '', 'BRL');
            $pdf->Ln();
            // Preu
            if ($price){
	            $pdf->Cell(100, 11, '', 0);
	            $pdf->Ln();
	            $pdf->SetFont('Arial', 'B', 15);
	            $pdf->SetTextColor(0, 0, 0);
	            $pdf->Cell(100, 15, ' ' . $c_price, 0);
	            $pdf->SetFont('Arial', '', 15);
	            $pdf->SetTextColor(0, 0, 0);
	            $pdf->Cell(171, 15, $price, 0);
            }
        }
        else
        {            
        	$pdf->SetFillColor(153,153,153);
            $pdf->Rect(15, 501.131, 565, 325.369, 'F');
            // segona imatge
            if ($image2) $pdf->Image($image2, $image2_x, $image2_y , $image2_w , $image2_h);
            $image_number++;
        }
        $image_number++;      
        if ($pdf->PageNo()>=$pages) break;
    }
}
// ////////////////////////////////////////////////////////////
// ////////                         ///////////////////////////
// /              APARADOR                       /////////////
// ///////                          /////////////////////////
// /////////////////////////////////////////////////////////
else
{
    // si hi ha imatges a la primera pagina hi va text+foto i les altres 2 fotos
    for ($image_number = 0;$image_number < count($images);$image_number = $image_number++)
    {
        $pdf->AddPage();
        $image1 = isset($images[$image_number])?DOCUMENT_ROOT . $images[$image_number]['image_src']:false;
        $image2 = isset($images[$image_number + 1])?DOCUMENT_ROOT . $images[$image_number + 1]['image_src']:false;
        // $image = file_exists(DOCUMENT_ROOT . $image_src_original)?DOCUMENT_ROOT . $image_src_original:DOCUMENT_ROOT . $image_src;
        // primera imatge
        if ($image1) $pdf->Image($image1, $image1_x, $image1_y , $image1_w , $image1_h);

        if ($image_number == 0)
        {
            // quadre verd clar
            $pdf->SetFillColor(221, 221, 221);
            $pdf->Rect(15, 501.131, 565, 325.369, 'F');
            // ratlla verda vertical
            //$pdf->SetFillColor(67, 86, 8);
            //$pdf->Rect(188.5, 518, 1, 286, 'F');
            // Titol mig
        	$pdf->SetTextColor(204, 204, 204);
            $pdf->SetFont('Arial', '', 25);
            $pdf->Text(30, 492.945, $property);
            $i = 0;
            $conta = 0;
            foreach($fields_pdf as $key => $value)
            {
                $pdf->SetFont('Arial', '', 19);
                $pdf->SetTextColor(0, 0, 0);
                $pdf->Text(30, 532 + $i, $key);
                $pdf->SetFont('Arial', '', 19);
                $pdf->SetTextColor(124, 124, 124);
                $pdf->Text(215, 532 + $i, $value);
                $i += $text_fields_increment;
                $conta++;
            }
        }
        else
        {
            
        	$pdf->SetFillColor(153,153,153);
            $pdf->Rect(15, 501.131, 565, 325.369, 'F');
            // segona imatge
            if ($image2) $pdf->Image($image2, $image2_x, $image2_y , $image2_w , $image2_h);
            $image_number++;
        }

        $image_number++;      
        if ($pdf->PageNo()>=$pages) break;
    }
}

include (DOCUMENT_ROOT . 'admin/modules/print/print_showwindow_tpl_end.php');
?>