
var ListPick = {
	menu_id: 20004,
	delete_variation: function(tr_id,variation_category_id){
		
		$('#'+tr_id).attr('class','listItemDown');
		
		$('#'+tr_id + '>td').height($('#'+tr_id + '>td:first-child').height());
		$('#'+tr_id + ' td').html('');
		$('#'+tr_id + '>td').slideUp(300,function(){
			$('#'+tr_id).remove();
		});
		
		ListPick.check_is_checked(variation_category_id);
	},
	check_is_checked: function(variation_category_id){
		// comprobo que la fila del radio seleccionat el checkbox també ho estigui	
		
		checked_radio = $("input[name='default["+variation_category_id+"]']:checked") ;
		
		if (!checked_radio.length){
			$('input[name="default['+variation_category_id+']"]:first').attr('checked',true);
		}		
	}
};


function select_addresses(related_table_id, menu_id) { 
	ListPick.menu_id = menu_id
	gl_related_table_id = related_table_id;
	showPopWin('/admin/?action=select_frames&menu_id=20062&product_id=' + related_table_id, 850, 530, null, true, false, '');
}