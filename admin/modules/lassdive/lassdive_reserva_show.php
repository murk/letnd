<?

/**
 * LassdiveReservaShow
 *
 * Funcions comuns per Lassdive Reserves
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2018
 * @version 1
 * @access public
 */
class LassdiveReservaShow {

	var $action, $module, $is_admin = false, $is_public = false, $module_caption;

	function show_form( &$module )
	{

		Main::load_class( 'lassdive', 'reserva_list', 'admin' );

		$this->module         = &$module;
		$this->module_caption = &$module->caption;
		$this->action         = $module->action;
		$GLOBALS['gl_content'] = $this->get_form();
	}

	public function get_form() {

		$module = Module::load( 'product', 'order', $this->module, false, 'lassdive/lassdive_order_config.php' );

		$show          = new ShowForm( $module );
		$show->template = 'lassdive/reserva_form.tpl';

		$show->get_values();

		if (!$show->has_results) Main::redirect('/');

		$customer_language = Db::get_first("SELECT prefered_language FROM product__customer WHERE customer_id = " . $show->rs['customer_id']);
		$show->set_vars(array(
			'customer_language'     => $customer_language,
		) );
		$order_id = $show->id;

		$reserva_list           = new LassdiveReservaList();
		$reserva_list->is_reserva_form = true;
		$orderitems        = $reserva_list->get_orderitems( $order_id, true );

		$show->set_loop( 'orderitems', $orderitems );

		// obtinc adresses
		$module_address = Module::load('product', 'address');
		$module_address->invoice_id = $show->rs['address_invoice_id'];
		$module_address->is_invoice = false;
		$module_address->delivery_id = $show->rs['delivery_id'];
		$show->set_vars($module_address->do_action('get_addresses'));

		$order_status = $show->rs['order_status'];
		$deposit = $show->rs['deposit'];
		$all_basetax = $show->rs['all_basetax'];

		if (
			$order_status == 'paying' ||
			$order_status == 'failed' ||
			$order_status == 'pending' ||
			$order_status == 'denied' ||
			$order_status == 'voided' ||
			$order_status == 'refunded' ||
			$order_status == 'preparing'
		) {

			if ( strpos( $deposit, '.00' ) !== false ) {
				$deposit = substr( $deposit, 0, - 3 );
			}

			$show->set_vars([
				'is_paid' => false,
				'checked_deposit' => $deposit?'checked':'',
				'deposit_visible' => $deposit?'visible':'',
				'deposit_text_visible' =>  $deposit?'visible':'',
				'checked_payed' => $deposit?'':'checked',
				'reserva_deposit' => $deposit,
				'reserva_deposit_formatted' => format_currency($deposit) . '  €',
				'all_to_pay' => format_currency($all_basetax - $deposit) . '  €',
				]);
		}
		else {

			$show->set_vars([
				'is_paid' => true,
				'deposit_text_visible' =>  $deposit?'visible':'',
				'reserva_deposit_formatted' => format_currency($deposit),
				'all_to_pay' => $show->caption['c_pagat'],
				]);
		}

	    return $show->show_form();

	}
}

?>