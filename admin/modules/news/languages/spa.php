<?php
// menus eina
if (!defined('NEWS_MENU_NEW')){
	define('NEWS_MENU_NEW', 'Noticias');
	define('NEWS_MENU_NEW_NEW', 'Entrar nueva noticia');
	define('NEWS_MENU_NEW_LIST', 'Listar i editar noticias');
	define('NEWS_MENU_NEW_BIN', 'Papelera de reciclaje');
	define('NEWS_MENU_CATEGORY', 'Categorías');
	define('NEWS_MENU_CATEGORY_NEW', 'Entrar nueva categoría');
	define('NEWS_MENU_CATEGORY_LIST', 'Listar categorías');
}

$gl_messages_new['no_category']='Para poder insertar noticias tiene que haber almenos una categoría';

// captions seccio
$gl_caption_new['c_prepare'] = 'En preparación';
$gl_caption_new['c_review'] = 'Para revisar';
$gl_caption_new['c_public'] = 'Pública';
$gl_caption_new['c_archived'] = 'Archivada';
$gl_caption_new['c_new'] = 'Noticia';
$gl_caption_new['c_title'] = 'Título';
$gl_caption_new['c_subtitle'] = 'Subtítulo';
$gl_caption_new['c_content'] = 'Contenido';
$gl_caption_new['c_entered'] = 'Fecha publicación';
$gl_caption_new['c_expired'] = 'Fecha caducidad';
$gl_caption_new['c_modified'] = 'Fecha modificación';
$gl_caption_new['c_status'] = 'Estado';
$gl_caption_new['c_category_id'] = 'Categoría';
$gl_caption_new['c_filter_category_id'] = 'Todas las categorías';
$gl_caption_new['c_filter_status'] = 'Todos los estados';
$gl_caption_new['c_filter_in_home'] = 'Todas en Home';
$gl_caption_new['c_filter_destacat'] = 'Todas las destacadas';
$gl_caption_new['c_ordre'] = 'Orden';

$gl_caption_category['c_category'] = 'Categoría';
$gl_caption_category['c_new_button'] = 'Ver noticias';
$gl_caption_category['c_ordre'] = 'Orden';
$gl_caption_image['c_name'] = 'Nombre';
$gl_caption_new['c_url1_name']='Nombre URL(1)';
$gl_caption_new['c_url1']='URL(1)';
$gl_caption_new['c_url2_name']='Nombre URL(2)';
$gl_caption_new['c_url2']='URL(2)';
$gl_caption_new['c_url3_name']='Nombre URL(3)';
$gl_caption_new['c_url3']='URL(3)';
$gl_caption_new['c_videoframe']='Vídeo (youtube, metacafe...)';
$gl_caption_new['c_file']='Archivos';
$gl_caption_new['c_in_home']='Sale en la home';
$gl_caption_new['c_in_home_0']='No sale en la Home';
$gl_caption_new['c_in_home_1']='Sale en la home';
$gl_caption_new['c_content_list']='Descripción corta en el listado';
$gl_caption_new['c_destacat']='Noticia destacada';
$gl_caption_new['c_destacat_0']='No destacada';
$gl_caption_new['c_destacat_1']='Destacada';

$gl_caption['c_new_file_name'] = $GLOBALS['gl_caption']['c_file_name'];
$gl_caption['c_new_old_file_name'] = $GLOBALS['gl_caption']['c_old_file_name'];
$gl_caption_new['c_form_level1_title_page_title'] = $GLOBALS['gl_caption']['c_seo'];

$gl_caption_new['c_selected']='Noticias seleccionadas';

$gl_caption['c_listtpl_id'] = 'Plantilla listado';
$gl_caption['c_formtpl_id'] = 'Plantilla';

$gl_caption['c_next_new'] = "Noticia siguiente";
$gl_caption['c_previous_new'] = "Noticia anterior";
?>
