<script language="JavaScript" src="/admin/jscripts/validacio.js?v=<?=$version?>"></script>
<script language="JavaScript" src="/admin/modules/inmo/jscripts/functions.js?v=<?=$version?>"></script>
<script language="JavaScript" src="/admin/jscripts/euro_calc.js?v=<?=$version?>"></script>
<script type="text/javascript">
$(function() {
	set_focus(document.theForm);
});
</script>




<script type='text/javascript' src='/common/jscripts/autocomplete/altres/jquery.autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="/common/jscripts/autocomplete/altres/jquery.autocomplete.css" />

	
<script type="text/javascript">
$(function() {
	$("#municipi").autocomplete('/admin/', {
		extraParams: {action:'list_records_municipis', menu_id:'103'},
		selectFirst: true,
		minChars: 4,
		cacheLength: 20,
		matchSubset: true,
		matchContains: true,
		mustMatch: true,
		delay: 10,
		onItemSelect: itemSelect,
		onRequestData: requestData
	})
});
function itemSelect(item){
	obj = document.theForm;
	theId = obj.property_id_javascript.value;
	if (item.extra) obj['municipi_id[' + theId + ']'].value = item.extra;
	$("#municipi").focus();
}
function requestData(ac){
	obj = document.theForm;
	theId = obj.property_id_javascript.value;
	theCountry =  obj['country_id[' + theId + ']'].value
	ac.setExtraParams({action:'list_records_municipis', menu_id:'103', country_id:theCountry});
}

</script>

<!--
<script language="JavaScript" src="/admin/modules/inmo/jscripts/chained_selects.js"></script>
<script type='text/javascript' src='/common/jscripts/autocomplete/lib/jquery.bgiframe.min.js'></script>
<script type='text/javascript' src='/common/jscripts/autocomplete/lib/jquery.ajaxQueue.js'></script>
<script type='text/javascript' src='/common/jscripts/autocomplete/lib/thickbox-compressed.js'></script>
<script type='text/javascript' src='/common/jscripts/autocomplete/jquery.autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="/common/jscripts/autocomplete/main.css" />
<link rel="stylesheet" type="text/css" href="/common/jscripts/autocomplete/jquery.autocomplete.css" />
<link rel="stylesheet" type="text/css" href="/common/jscripts/autocomplete/lib/thickbox.css" />
	
<script type="text/javascript">
$(function() {
	function format(row) {
		return row.name + '  ( ' + row.comarca + ', ' + row.provincia.toUpperCase()  + ' )';
	}
	$("#municip").autocomplete('/admin/?menu_id=103&action=list_records_municipis', {
		selectFirst: true,
		minChars: 3,
		cacheLength: 20,
		matchSubset: true,
		matchContains: true,
		mustMatch: true,
		delay: 10,
		max: 5000,
		parse: function(data) {
			return $.map(eval(data), function(row) {
				return {
					data: row,
					value: row.id,
					result: row.name
					//result: row.name + ' ( ' + row.comarca + ', ' + row.provincia.toUpperCase() + ' )'
				}
			});
		}
		,
		formatItem: function(item) {
			return format(item);
		}
	}).result(function(e, item) {
		//alert(item.id);
		//$("#content").append("<p>selected " + format(item) + "</p>");
	});
});
</script>
 -->
  









<?if ($image_name):?><script type="text/javascript">
images_dir = '/<?=CLIENT_DIR?>/inmo/property/images/';
gl_default_imatge = '<?=$images[0]['image_name_medium']?>';
images_thumbs =  '<?foreach($images as $image): extract ($image)?><?=$image_name_thumb?>-<?endforeach //$images?>';
images_mediums =  '<?foreach($images as $image): extract ($image)?><?=$image_name_medium?>-<?endforeach //$images?>';
</script><?endif // image_name?>
<form name="form_delete" method="post" action="<?=$link_action_delete?>">
    <input name="<?=$id_field?>" type="hidden" value="<?=$id?>">
</form><form name="theForm" method="post" action="<?=$link_action?>" onsubmit="<?=$js_string?>" encType="multipart/form-data" >
<table width="100%" border="0" cellspacing="0" cellpadding="0"> 
  <tr>
    <td> 
        <?if ($form_new):?> 
        <table width="100%" border="0" cellspacing="2" cellpadding="2">
          <tr>
            <td align="right"><input type="submit" name="Submit" value="<?=$c_send_new?>" class="btn_form1"></td>
          </tr>
        </table>
        <?endif //form_new?><?if ($form_edit):?><?if ($show_tabs):?>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td><table border="0" cellspacing="2" cellpadding="2">
                    <tr>
                      <td class="formCaptionTitle"><strong>
                        <?=$c_edit_data?>
                        &nbsp;</strong></td>
                      <td>&nbsp;<a href="<?=$image_link?>"><strong>
                        <?=$c_edit_images?>
                      </strong></a>&nbsp;</td>
                      <td class="private">&nbsp;<a href="?menu_id=<?=$menu_id?>&amp;property_id=<?=$property_id?>&amp;action=list_records_custumer"><strong>
                        <?=$c_edit_custumer?>
                      </strong></a>&nbsp;</td>
                    </tr>
                </table></td>
                <td align="right"><input type="submit" name="Submit" value="<?=$c_send_edit?>" class="btn_form1" />
                  &nbsp;&nbsp;<a class="btn_form1" href="javascript:submit_form('<?=$c_confirm_form_bin?>', true);">
                  <?=$c_delete?>
                  </a>&nbsp;<a class="btn_form1" href="<?=$preview_link?>" target="_blank">
                  <?=$c_preview?>
                  </a>&nbsp;<a href="/admin/?action=show_record_showwindow&amp;property_id=<?=$property_id?>&amp;menu_id=202" target="_blank" class="btn_form1">
                  <?=$c_showwindow?>
                    </a>&nbsp;<a href="/admin/?action=show_record_album&amp;property_id=<?=$property_id?>&amp;menu_id=202" target="_blank" class="btn_form1">
                  <?=$c_album?></a></td>
              </tr>
          </table><?endif //show_tabs?>
          <?endif //form_edit?> 
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr> 
            <td><table width="100%" cellpadding="4" cellspacing="0">
                <tr> 
                  <td class="formCaptionTitle"><strong>
                        <?=$c_data?>&nbsp;&nbsp;</strong><?=$c_ref?> <?=$ref?>
                  </td>
                </tr>
              </table>
              <table width="100%" border="0" cellpadding="0" cellspacing="0" class="formstbl">
                <tr>
                  <td><table width="100%" border="0" cellspacing="0" cellpadding="0"><tr>
                      <td><?if ($show_tabs):?><?if ($image_name):?><table width="100%" border="0" cellpadding="0" cellspacing="0" class="images">
    <tr>
      <?foreach($images as $image): extract ($image)?>
        <td align="center"><?if ($image_name_thumb):?>
            <a href="#" onClick="return open_flash('<?=$image_name_medium?>')"><img src="<?=$image_src_admin_thumb?>" vspace="10" border="0" /></a>
        <?endif //$image_name_details?></td>
        <?if ($image_conta == 2) break;?>
    <?endforeach //$images?>
    </tr>
</table><?endif // image_name?><?endif // show_tabs?></td>
                      </tr>
                    <tr>
                      <td><table width="100%" cellpadding="4" cellspacing="0" class="private">
                        <tr>
                          <td height="1" valign="top" class="formsCaptionHor"><?=$c_data_private?>                          </td>
                          <td height="1" class="formsCaptionHor"><img src="/admin/themes/inmotools/images/spacer.gif" width="1" height="1" /></td>
                        </tr>
                        <tr>
                          <td width="150" class="formsCaption"><strong>
                            <?=$c_property_private?>
                            :</strong></td>
                          <td class="forms"><input name="property_id[<?=$property_id?>]" type="hidden" value="<?=$property_id?>" />
                              <input name="property_id_javascript" type="hidden" value="<?=$property_id?>" />
                              <?=$property_private?></td>
                        </tr>
                        <tr>
                          <td valign="top" class="formsCaption"><strong>
                            <?=$c_address?>
                            : </strong></td>
                          <td class="forms"><table border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td><?=$c_adress?></td>
                                <td><?=$c_numstreet;?></td>
                                <td><?=$c_block;?></td>
                                <td><?=$c_flat;?></td>
                                <td><?=$c_door;?></td>
                              </tr>
                              <tr>
                                <td><?=$adress?>
                                  &nbsp;&nbsp;</td>
                                <td><?=$numstreet;?>
                                  &nbsp;&nbsp;</td>
                                <td><?=$block;?>
                                  &nbsp;&nbsp;</td>
                                <td><?=$flat;?>
                                  &nbsp;&nbsp;</td>
                                <td><?=$door;?></td>
                              </tr>
                          </table></td>
                        </tr>
                        <tr>
                          <td valign="top" class="formsCaption"><strong>
                            <?=$c_observations?>
                            : </strong></td>
                          <td class="forms"><?=$observations?></td>
                        </tr>
                      </table></td>
                    </tr>                    
                  </table>
                    <table width="100%" cellpadding="0" cellspacing="0">
                      <tr>
                        <td height="1" class="formsCaptionHor"><img src="/admin/themes/inmotools/images/spacer.gif" width="1" height="1" /></td>
                      </tr>
                      <tr> </tr>
                  </table>
                    <table width="100%" cellpadding="4" cellspacing="0">
                      <tr>
                        <td width="150" height="1" valign="top" class="formsCaptionHor"><?=$c_data_public?>
                          : </td>
                        <td height="1" class="formsCaptionHor"><img src="/admin/themes/inmotools/images/spacer.gif" width="1" height="1" /></td>
                      </tr>
                      <tr>
                        <td class="formsCaption"><strong>
                          <?=$c_tipus?>
                          :</strong></td>
                        <td class="forms"><?=$tipus?></td>
                      </tr>
                      <tr>
                        <td class="formsCaption"><strong>
                          <?=$c_tipus2?>
                          :</strong></td>
                        <td class="forms"><?=$tipus2?></td>
                      </tr>
                      <tr>
                        <td valign="top" class="formsCaption"><strong>
                          <?=$c_category?>
                          :</strong></td>
                        <td class="forms"><?=$category_id?>                        </td>
                      </tr>
                      <tr>
                        <td class="formsCaption"><strong>
                          <?=$c_floor_space?>
                          :</strong></td>
                        <td class="forms"><?=$floor_space?></td>
                      </tr>
                      <tr>
                        <td class="formsCaption"><strong>
                          <?=$c_land?>
                          :</strong></td>
                        <td class="forms"><?=$land?>
                          &nbsp;&nbsp;
                        <?=$land_units?></td>
                      </tr>
                      <tr>
                        <td class="formsCaption"><strong>
                          <?=$c_country_id?>
                          :</strong></td>
                        <td class="forms"><?=$country_id?></td>
                      </tr>
                      <tr>
                        <td class="formsCaption"><strong>
                          <?=$c_municipi_id?>
                          :</strong></td>
                        <td class="forms"><?=$municipi_id?>
                            <input name="municipi" type="text" id="municipi" size="80" value="<?=$municipi?>"/></td>
                      </tr>
                      <tr class="private">
                        <td class="formsCaption"><strong>
                          <?=$c_zone_id?>
                          : </strong></td>
                        <td class="forms"><?=$zone_id?>
                            <input name="old_zone_id[<?=$property_id?>]" type="hidden" value="0" /></td>
                      </tr>
                      <tr class="private">
                        <td class="formsCaption"><strong>
                          <?=$c_price_private?>
                          :</strong></td>
                        <td class="forms"><?=$price_private?>
                          &nbsp;<?=CURRENCY_NAME?>&nbsp;&nbsp;
                          (&nbsp;
                          <input name="pts_private" type="text" value="<?=$pts_private?>" onkeyup="write_euros(this,this.form.elements['price_private[<?=$property_id?>]'])" />
                          &nbsp;) pts</td>
                      </tr>
                      <tr>
                        <td class="formsCaption"><strong>
                          <?=$c_price?>
                          :</strong></td>
                        <td class="forms"><?=$price?>
                          &nbsp;<?=CURRENCY_NAME?>&nbsp;&nbsp;
                          (&nbsp;
                          <input name="pts" type="text" value="<?=$pts?>" onkeyup="write_euros(this,this.form.elements['price[<?=$property_id?>]'])" />
                          &nbsp;) pts&nbsp;
                          <?=$price_consult?>
                        <?=$c_price_consult?></td>
                      </tr>
                      <tr>
                        <td class="formsCaption"><strong>
                          <?=$c_status?>
                          :</strong></td>
                        <td class="forms"><?=$status?></td>
                      </tr>
                      <tr>
                        <td class="formsCaption"><strong><strong>
                          <?=$c_home?>
                        </strong> </strong>:</td>
                        <td class="forms"><?=$home?></td>
                      </tr>
                      <tr>
                        <td class="formsCaption"><strong><strong>
                          <?=$c_offer?>
                        </strong> </strong>:</td>
                        <td class="forms"><?=$offer?></td>
                      </tr>
                      <tr>
                        <td valign="top" class="formsCaption"><strong><strong>
                          <?=$c_file?>
                        </strong> </strong>:</td>
                        <td class="forms"><?=$file?></td>
                      </tr>
                      <tr>
                        <td valign="top" class="formsCaption"><strong><strong>
                          <?=$c_video?>
                        </strong> </strong>:</td>
                        <td class="forms"><?=$video?>
</td>
                      </tr>
                    </table></td>
                </tr>
              </table>
<table width="100%" cellpadding="4" cellspacing="0" class="private">
                <tr>
                  <td class="formCaptionTitle"><strong>
                    <?=$c_mls?>
                  </strong></td>
                </tr>
              </table>
              <table width="100%" cellpadding="4" cellspacing="0" class="formstbl">
                <tr class="private">
                  <td width="150" class="formsCaption"><strong><?=$c_share_mls?>
                  :</strong></td>
                  <td class="forms"><?=$mls?></td>
                  <td width="150" class="formsCaption"><strong><?=$c_comission?>
                  :</strong></td>
                  <td class="forms"><?=$comission?></td>
                </tr>
              </table>
              <table width="100%" cellpadding="4" cellspacing="0">
                <tr>
                  <td class="formCaptionTitle"><strong>
                    <?=$c_details?>
                  </strong></td>
                </tr>
              </table>
              <table width="100%" cellpadding="4" cellspacing="0" class="formstbl">
                <tr>
                  <td width="150" class="formsCaption"><strong>
                    <?=$c_room?>
                    :</strong></td>
                  <td class="forms"><?=$room?></td>
                  <td width="150" class="formsCaption"><strong>
                    <?=$c_bathroom?>
                    :</strong></td>
                  <td class="forms"><?=$bathroom?></td>
                </tr>
                <tr>
                  <td width="150" class="formsCaption"><strong>
                    <?=$c_wc?>
                    :</strong></td>
                  <td class="forms"><?=$wc?></td>
                  <td width="150" class="formsCaption"><strong>
                    <?=$c_laundry?>
                    :</strong></td>
                  <td class="forms"><?=$laundry?></td>
                </tr>
                <tr>
                  <td width="150" class="formsCaption"><strong>
                    <?=$c_garage?>
                    :</strong></td>
                  <td class="forms"><table border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td><?=$garage?>&nbsp;</td>
                        <td><?=$c_garage_area?></td>
                        <td>&nbsp;
                        <?=$garage_area?></td></tr>
                    </table></td>
                  <td width="150" class="formsCaption"><strong>
                    <?=$c_storage?>
                    :</strong></td>
                  <td class="forms"><table border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td><?=$storage?>&nbsp;</td>
                      <td><?=$c_storage_area?></td>
                      <td>&nbsp;
                        <?=$storage_area?></td></tr>
                  </table></td>
                </tr>
                <tr>
                  <td width="150" class="formsCaption"><strong>
                    <?=$c_terrace?>
                    :</strong></td>
                  <td class="forms"><table border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td><?=$terrace?>&nbsp;</td>
                      <td><?=$c_terrace_area?></td>
                      <td>&nbsp;
                        <?=$terrace_area?></td></tr>
                  </table></td>
                  <td width="150" class="formsCaption"><strong>
                    <?=$c_garden?>
                    :</strong></td>
                  <td class="forms"><table border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td><?=$garden?>&nbsp;</td>
                      <td><?=$c_garden_area?></td>
                      <td>&nbsp;
                        <?=$garden_area?></td></tr>
                  </table></td>
                </tr>
                <tr>
                  <td width="150" class="formsCaption"><strong>
                    <?=$c_furniture?>
                    :</strong></td>
                  <td class="forms"><?=$furniture?></td>
                  <td width="150" class="formsCaption"><strong>
                    <?=$c_citygas?>
                    :</strong></td>
                  <td class="forms"><?=$citygas?></td>
                </tr>
                <tr>
                  <td width="150" class="formsCaption"><strong>
                    <strong>
                    <?=$c_centralheating?>
                    </strong>:</strong></td>
                  <td class="forms"><?=$centralheating?></td>
                  <td width="150" class="formsCaption"><strong>
                    <?=$c_swimmingpool?>
                    :</strong></td>
                  <td class="forms"><?=$swimmingpool?></td>
                      <td><?=$c_swimmingpool_area?></td>
                      <td>&nbsp;
                        <?=$swimmingpool_area?></td>
                </tr>
                <tr>
                  <td class="formsCaption"><strong>
                    <?=$c_airconditioned?>
                  :</strong></td>
                  <td class="forms"><?=$airconditioned?></td>
                  <td class="formsCaption"><strong>
                    <?=$c_balcony?>
:</strong></td>
                  <td class="forms"><?=$balcony?></td>
                </tr>
                <tr>
                  <td class="formsCaption"><strong>
                    <?=$c_elevator?>
                  :</strong></td>
                  <td class="forms"><?=$elevator?></td>
                  <td class="formsCaption">&nbsp;</td>
                  <td class="forms">&nbsp;</td>
                </tr>
              </table>
              <table width="100%" cellpadding="4" cellspacing="0">
                <tr> 
                  <td class="formCaptionTitle"><strong><?=$c_descriptions?></strong></td>
                </tr>
              </table>
              <table width="100%" cellpadding="0" cellspacing="0" class="formstbl">
                <tr> 
                  <td><table cellpadding="4" cellspacing="0">
                      <tr> 
                        <td width="150" valign="top" class="formsCaption"><strong>
                        <?=$c_property?>:</strong></td>
                        <td><table cellpadding="4" cellspacing="0"><?foreach($property as $l): extract ($l)?>
                      <tr> 
                        <td class="forms"><?=$lang?></td><td>
                          <?=$property?> 
                        </td>
                      </tr>
                        <?endforeach //$property?>
                    </table></td>
                      </tr>
                    </table>
<table cellpadding="4" cellspacing="0">
                      <tr> 
                        <td width="150" valign="top" class="formsCaption"><strong>
                        <?=$c_property_title?>:</strong></td>
                        <td><table cellpadding="4" cellspacing="0"><?foreach($property_title as $l): extract ($l)?>
                      <tr> 
                        <td class="forms"><?=$lang?></td><td>
                          <?=$property_title?> 
                        </td>
                      </tr>
                        <?endforeach //$property_title?>
                    </table></td>
                      </tr>
                    </table>
                    <table width="100%" cellpadding="0" cellspacing="0">
                      <tr> 
                        <td height="1" class="formsCaptionHor"><img src="/admin/themes/inmotools/images/spacer.gif" width="1" height="1"></td>
                      <tr> 
                    </table>
                    <table width="100%" cellpadding="4" cellspacing="0">
                      <td class="formsCaptionHor"><strong><?=$c_description?> 
                        </strong></td>
                      </tr>
                      <?foreach($description as $l): extract ($l)?>
                      <tr> 
                        <td class="formsHor"><?=$lang?><br><?=$description?>
                        </td>
                      </tr>
                      <?endforeach //$description?>
                    </table></td>
                </tr>
              </table>
              <?if ($form_new):?>
              <table width="100%" cellpadding="4" cellspacing="2" class="formsButtonstbl">
                <tr> 
                  <td align="center" class="formsButtons"> <input type="submit" name="Submit" value="<?=$c_send_new?>" class="btn_form1"> 
                  </td>
                </tr>
              </table>
              <?endif //form_new?><?if ($form_edit):?>
              <table width="100%" cellpadding="4" cellspacing="2" class="formsButtonstbl">
                <tr> 
                  <td align="center" class="formsButtons"><input type="submit" name="Submit" value="<?=$c_send_edit?>" class="btn_form1">&nbsp;&nbsp;<a class="btn_form1" href="javascript:submit_form('<?=$c_confirm_form_bin?>', true);">
                      <?=$c_delete?>
                      </a><?if ($show_tabs):?>&nbsp;<a class="btn_form1" href="<?=$preview_link?>" target="_blank">
                <?=$c_preview?>
                </a>&nbsp;<a href="/admin/?action=show_record_showwindow&property_id=<?=$property_id?>&menu_id=202" target="_blank" class="btn_form1">
              <?=$c_showwindow?></a>&nbsp;<a href="/admin/?action=show_record_album&property_id=<?=$property_id?>&menu_id=202" target="_blank" class="btn_form1">
              <?=$c_album?></a><?endif //show_tabs?></td>
                </tr>
              </table>
            <?endif //form_edit?></td>
          </tr>
        </table></td>
  </tr>
</table>
</form>
