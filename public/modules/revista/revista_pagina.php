<?
/**
 * RevistaPagina
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2012
 * @version $Id$
 * @access public 
 
 */
class RevistaPagina extends Module{
	var $clubone_js = '';
	function __construct(){
		parent::__construct();
	}
	function get_records()
	{
		$pagina_id = R::id('pagina_id');
		if (!$pagina_id) Main::error_404();
		
		$this->set_next_prev($pagina_id);
		$this->increment_counter($pagina_id);
		
		$this->set_field('col_valign','type','none');
		$this->set_field('col_valign2','type','none');
		$this->set_field('col_valign3','type','none');
		
		$listing = new ListRecords($this);
		
		$listing->always_parse_template = true;	
		$listing->order_by = 'ordre ASC, pagina_id';
		$listing->extra_fields = '(SELECT file_name FROM revista__paginatpl WHERE revista__paginatpl.paginatpl_id = revista__pagina.paginatpl_id) as file_name';
		$listing->condition = "level=3 AND parent_id = " . $pagina_id . $GLOBALS['gl_revista_status'];
		$listing->call('list_records_walk','pagina_id',true);
			
	    $ret = $listing->list_records();
		$this->clubone_js = 'revista.clubone = {' . substr($this->clubone_js,0,-1) . '}';
		$GLOBALS['gl_page']->javascript .= $this->clubone_js . ';revista.is_logged = ' . ($this->is_logged()?1:0) . ';';
	    return $ret;
	}
	
	function list_records_walk(&$listing,$pagina_id){
		$rs = &$listing->rs;
		
		// customized
		if($rs['custom_id']){			
			$ret['show_register'] = false;	
			$ret['is_custom'] = true;
			$ret +=$this->_get_custom($rs['custom_id']);
		}
		else{
			// plantilles
			$ret['show_video']= (isset($rs['images'][1]) || $rs['videoframe']);
			if (!$rs['videoframe'] && isset($rs['images'][1])){
				$ret['image2'] = $rs['images'][1];
			}
			$ret['show_register'] = $rs['is_clubone'] && !$this->is_logged();
			$ret['is_custom'] = false;
			$this->clubone_js .= '"'.$rs['conta'].'":'.$rs['is_clubone'].',';
		}
		
		// comú
		$ret['show_col_1']= ($rs['body_supertitle']||$rs['title']||$rs['body_subtitle']||$rs['body_content']);
		$ret['show_col_2']= ($rs['body_supertitle2']||$rs['title2']||$rs['body_subtitle2']||$rs['body_content2']);
		$ret['show_col_3']= ($rs['body_supertitle3']||$rs['title3']||$rs['body_subtitle3']||$rs['body_content3']);
		
		return $ret;
	}
	function show_form()
	{
		
		// Faig un show form per les dades de la pàgina
		$show = new ShowForm($this);
		$show->condition = "level=2" . $GLOBALS['gl_revista_status'];
		
		$show->get_values();
		
		if (!$show->rs) Main::error_404();	
		// defineixo les variables de la pàgina
		$GLOBALS['gl_page']->page_title = 
				$show->rs['page_title']?$show->rs['page_title']:$show->rs['pagina'];				
		$GLOBALS['gl_page']->page_description = $show->rs['page_description'];		
		$GLOBALS['gl_page']->page_keywords = $show->rs['page_keywords'];
		
		$this->action = 'list_records';
		$GLOBALS['gl_content'] = $this->get_records();
	}
	
	function set_next_prev($pagina_id)
	{
		$results = array();
		$loops = array('1','0');
		foreach ($loops as $key){
			$parent_id = Db::get_first("SELECT parent_id
						FROM revista__pagina
						WHERE bin <> 1
						".$GLOBALS['gl_revista_status']."
						AND pagina_id = '".$pagina_id."'");
			$query = "SELECT revista__pagina.pagina_id AS pagina_id,
							(SELECT revista__category.ordre 
								FROM revista__category 
								WHERE revista__category.category_id = revista__pagina.category_id) AS category,
							(SELECT revista__category_language.category 
								FROM revista__category_language 
								WHERE revista__category_language.category_id = revista__pagina.category_id
								AND language = '".LANGUAGE."') AS category_name,
							(SELECT revista__pagina_language.pagina 
								FROM revista__pagina_language 
								WHERE revista__pagina_language.pagina_id = revista__pagina.pagina_id
								AND language = '".LANGUAGE."') AS pagina
						FROM revista__pagina
						WHERE revista__pagina.bin = 0 
						".$GLOBALS['gl_revista_status']."
						AND level = 2 
						AND (SELECT revista__category.is_clubone 
								FROM revista__category 
								WHERE revista__category.category_id = revista__pagina.category_id) = ".$key."
						AND parent_id = '".$parent_id."'
						ORDER BY category ASC, category_id, ordre ASC, pagina_id";
			$results = array_merge(Db::get_rows($query),$results);
		}
		// fins aquí tot igual que al block revista_apartat.php menys el merge dels results
		
		foreach ($results as $key=>$rs){
			if ($pagina_id == $rs["pagina_id"]){
			
				if (isset($results[$key-1])){
					$vars['previous_apartat_link'] = revista_get_pagina_link($results[$key-1]['pagina_id']);
					$vars['previous_apartat_title'] = $results[$key-1]['pagina'];
				}
				else{
					$vars['previous_apartat_link'] = '' ;
					$vars['previoust_apartat_title'] = '';				
				}
				if (isset($results[$key+1])){
					$vars['next_apartat_link'] = revista_get_pagina_link($results[$key+1]['pagina_id']);
					$vars['next_apartat_title'] = $results[$key+1]['pagina'];
				}
				else{
					$vars['next_apartat_link'] = '' ;
					$vars['next_apartat_title'] = '';				
				}
			
				$this->set_vars($vars);
				break;
			}
		}
	}
	function increment_counter($pagina_id){
		include (DOCUMENT_ROOT . 'common/includes/browser/browser.php');
		$browser = new Browser();
		$name = $browser->getBrowser();
		
		
		$is_ok=( 
				$name != Browser::BROWSER_GOOGLEBOT &&
			$name != Browser::BROWSER_SLURP &&
			$name != Browser::BROWSER_W3CVALIDATOR &&
			$name != Browser::BROWSER_MSNBOT &&
			$name != Browser::BROWSER_UNKNOWN
				);		
		
		if ($is_ok){
			// si no està en sessió la conto, nomès un cop per usuari
			if (!isset($_SESSION['revista']['paginas'][$pagina_id])){
				$_SESSION['revista']['paginas'][$pagina_id] = true;
				Db::execute("UPDATE revista__pagina SET counter=(counter+1) WHERE  pagina_id='" .$pagina_id . "'");
			}			
		}
		
		
		
	}
	
	/** personalitzador **/
	function _get_custom($custom_id = 1){
		
		$ret = array();
		
		$path = CLIENT_PATH . 'custom-galleries/' . $custom_id . '/';
		$web_path = '/' . CLIENT_DIR . '/custom-galleries/' . $custom_id . '/';
		
		$captions = new SimpleXMLElement($path . 'texts.xml', NULL, true);
		$captions = $captions->{LANGUAGE};
		
		$ret['custom_nom'] = trim((string)$captions->custom->nom);
		
		$ret['image_fons'] = $web_path . 'fons.jpg';
		$ret['categories'] = array();
		
		
		$category_id = 1;
		$conta = 0;					
		foreach ( $captions->categories->item as $category)
		{
			$ret['categories'][] = array(
					'nom'=>trim((string)$category->nom),
					'last_first'=>'',
					'category_id'=>$category_id,
					'category_conta'=>$conta,
					'images' => array()
				);
			$conta++;
			$category_id++;
		}
		$ret['categories'][0]['last_first'] = 'first';
		$total_categories = count ($ret['categories']);
		$ret['categories'][$total_categories-1]['last_first'] = 'last';
		if ($total_categories==1) $ret['categories'][0]['last_first'] = 'last first';
		
		// lligo les categories amb el nom al xml segons l'ordre en que estan
		$categories_dirs = scandir($path);
		$conta = 0;					
		$image_id = 1;
		foreach($categories_dirs as $dir){	
			
			$path_category = $path . $dir;
			$web_path_category = $web_path . $dir . '/';
						
			$image_conta = 0;
			if ($dir!='.' && $dir!='..' && is_dir($path_category)){
				$images_dirs = scandir($path . $dir);
				
				// poso totes les imtges de cada categotia
				$curr = &$ret['categories'][$conta]['images'];
				
				// agafo les imatges
				foreach($images_dirs as $image){
					if ($image!='.' && $image!='..' && $image!='thumbs' && $image!='cat_texts.xml'){
						$curr[] = array( 
							'image_src' => $web_path_category . $image,
							'image_id' => $image_id,										
							'image_alt' => '',				
							'image_conta' => $image_conta++				
							);
						$image_id++;
					}
				}
				
				// agafo els thumbs				
				$image_conta = 0;			
				$thumbs_dirs = scandir($path . $dir . '/thumbs/');
				foreach($thumbs_dirs as $image){
					if ($image!='.' && $image!='..'){
						$curr[$image_conta]['image_thumb_src'] = $web_path_category . 'thumbs/' . $image;
						$image_conta++;
					}
				}
				
				// agafo captions imatges si n'hi ha ( o hi han de ser tots o no cap )
				$image_text_file = $path . $dir . '/cat_texts.xml';
				if (is_file($image_text_file)){
					$captions_cat = new SimpleXMLElement($image_text_file, NULL, true);
					$captions_cat = $captions_cat->{LANGUAGE};
					
					$image_conta = 0;		
					foreach ( $captions_cat->item as $cat_item)
					{
						$curr[$image_conta]['image_alt'] = trim((string)$cat_item->nom);
						$image_conta++;
					}
				}
				
				$conta++;			
			}
		}
		debug::add('Custom array', $ret);
		return $ret;
	}

	/**
	 * Es el logged de newsletter
	 * NewsletterCustomer::is_logged()
	 * Saber si està loguejat
	 * @return
	 */
	function is_logged(){
		return (isset($_SESSION['newsletter']['customer_id']) && isset($_SESSION['newsletter']['mail']));
	}
}
?>