<?

/* 13-11-15

	Actualització product SHA2

 */
if ( is_table( 'product__configpublic' ) && ! Db::get_first( "SELECT * FROM product__configpublic WHERE name='lacaixa_clave_sha2'" ) ) {
	$update_sql[] = "
		INSERT INTO product__configpublic (`name`, `value`) VALUES ('lacaixa_clave_sha2', '');";

} else {
	if ( is_table( 'product__configpublic' ) ) {
		trigger_error( 'Ja existeixen el camp lacaixa_clave_sha2' );
	}
}

?>