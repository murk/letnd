
<script language="JavaScript" src="/admin/jscripts/validacio.js?v=<?=$version?>"></script>
<script language="JavaScript" src="/admin/jscripts/social.js?v=<?=$version?>"></script>
<div  class="form">

<form name="theForm" method="post" action="<?=$link_action?>" onsubmit="return validar(this,'<?=htmlspecialchars(addslashes($c_facebook_message))?>','text','facebook_message','<?=htmlspecialchars(addslashes($c_facebook_description))?>','text','facebook_description');" target="save_frame">
<div id="form_buttons_holder"><table id ="form_buttons">
<tr>
	<td valign="bottom"><div class="menu">
	<a href="<?=$data_link?>"><?=$c_edit_data?></a>
	<?if ($has_images):?>
		<a href="<?=$image_link?>"><?=$c_edit_images?></a>
	<?endif //has_images?>
	<div>
		<?=$c_edit_social?>
	</div></div></td>
	<td class="buttons_top"><div class="right"></div></td>
</tr>  
</table></div>


<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
	<td>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td>
				<table width="100%" cellpadding="4" cellspacing="0">
				<tr>
					<td class="formCaptionTitle"><strong>
			<?=$c_edit_social_title?>
			&quot;<?=$social_title?>&quot;</strong></td>
				</tr>
			</table>
			
			<table width="100%" cellpadding="4" cellspacing="0" class="formstbl">

				<tr>

					<td width="150" valign="top" class="formsCaption"><strong>
							<?=$c_social_language?>:</strong></td>
					<td valign="top" class="forms"><table border="0" cellpadding="4" cellspacing="0">
							<tr>
								<td><?=$facebook_language?></td>
							</tr>
						</table></td>
				</tr>
				
				<?if($facebook_is_active):?>
				<?if($facebook_picture):?>
				<tr>
				
					<td width="150" valign="top" class="formsCaption"><strong>
					<?=$c_facebook_picture?>:</strong></td>
					<td valign="top" class="forms"><table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td><input type="hidden" name="facebook_picture" value="<?=$facebook_picture?>"><img src="<?=$facebook_picture?>" width="<?=$im_admin_thumb_h?>" alt="" /></td>
						</tr>
					</table></td>
				</tr>
				<?endif //picture?>
				<?endif //facebook_is_active?>
				<tr>
					<td colspan="2" class="formCaptionTitle">
						Facebook
						<?if($facebook_is_active):?>
							<?if($facebook_user_name):?> - <a href="<?=$facebook_user_link?>" target="_blank"><?=$facebook_user_name?></a></span><?endif //facebook_user_link?><?if($facebook_is_logged):?> - <a target="save_frame" href="/?call=facebook_logout&return_url=<?=$return_url?>"><?=$c_facebook_logout?></a><?endif //is_logged?>
						<?endif //facebook_is_active?>
						
					</td>
				</tr>

			</table>

			
			<?if($facebook_is_logged && !$facebook_is_published):?>
			

			<table width="100%" cellpadding="4" cellspacing="0" class="formstbl">

				<?if($facebook_publish_select):?>
				<tr>
				
					<td width="150" valign="top" class="formsCaption"><strong>
					<?=$c_facebook_publish_id?>:</strong></td>
					<td valign="top" class="forms"><table border="0" cellpadding="4" cellspacing="0">
						<tr>
							<td><?=$facebook_publish_id?></td>
							<td style="vertical-align: middle;"><input type="checkbox" name="facebook_publish_id_save_default" value="1"><?=$c_facebook_publish_save_default?></td>
						</tr>
					</table></td>
				</tr>
				<?endif //facebook_publish_select?>
			
				<tr>
				
					<td width="150" valign="top" class="formsCaption"><strong>
					<?=$c_facebook_publish_as?>:</strong></td>
					<td valign="top" class="forms"><table border="0" cellpadding="4" cellspacing="0">
						<tr>
							<td><input checked type="radio" name="facebook_publish_as" value="link"><?=$c_facebook_publish_as_link?></td>
							<td><input type="radio" name="facebook_publish_as" value="image"><?=$c_facebook_publish_as_image?></td>
						</tr>
					</table></td>
				</tr>

				<tr>

					<td width="150" valign="top" class="formsCaption"><strong>
					<?=$c_facebook_link?>:</strong></td>
					<td valign="top" class="forms"><table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td><?if(!$facebook_publish_select):?><?=$facebook_publish_id?><?endif //facebook_publish_select?><input type="hidden" name="facebook_link" value="<?=$facebook_link?>"><?=$facebook_link?></td>
						</tr>
					</table></td>
				</tr>

			</table>


			<table width="100%" cellpadding="4" cellspacing="0" class="formstbl" id="facebook-link">

				<tr>
				<td colspan="2" class="formsCaptionHor2"><?=$c_facebook_link?> <span class="padding-left-2 smaller"><?=$c_facebook_link_explanation?></span></td>
				</tr>

				<tr>

					<td width="150" valign="top" class="formsCaption"><strong>
					<?=$c_facebook_message?>:</strong></td>
					<td valign="top" class="forms"><table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td><textarea name="facebook_message" class="wide" rows="10"><?=$facebook_message?></textarea></td>
						</tr>
					</table></td>
				</tr>

			</table>
			<table width="100%" cellpadding="4" cellspacing="0" class="formstbl hidden" id="facebook-image">

				<tr>
				<td colspan="2" class="formsCaptionHor2"><?=$c_facebook_image_title?></td>
				</tr>
			
				<tr>
				
					<td width="150" valign="top" class="formsCaption"><strong>
					<?=$c_facebook_name?>:</strong></td>
					<td valign="top" class="forms"><table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td><input type="text" name="facebook_name" value="<?=$facebook_name?>" class="wide"></td>
						</tr>
					</table></td>
				</tr>
			
				<tr>
				
					<td width="150" valign="top" class="formsCaption"><strong>
					<?=$c_facebook_caption?>:</strong></td>
					<td valign="top" class="forms"><table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td><input type="text" name="facebook_caption" value="<?=$facebook_caption?>" class="wide"></td>
						</tr>
					</table></td>
				</tr>
			
				<tr>
				
					<td width="150" valign="top" class="formsCaption"><strong>
					<?=$c_facebook_description?>:</strong></td>
					<td valign="top" class="forms"><table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td><textarea name="facebook_description" class="wide" rows="10"><?=$facebook_description?></textarea></td>
						</tr>
					</table></td>
				</tr>

			</table>			
			<?endif // facebook_is_logged  facebook_is_published ?>
			
			
			<?if($facebook_is_published):?>
				<div style="text-align:center;margin:40px 0"><?=$c_facebook_delete_text?></div>
			<?endif //facebook_is_published?>
			
			
			<?if(!$facebook_is_active):?>
				<div style="text-align:center;margin:40px 0;font-size:15px;"><?=$c_facebook_activate?></div>
			<?endif //facebook_is_active?>
			
			</td>
		</tr>
		</table></td>
	</tr>
</table>

<?if($facebook_is_logged):?>

<table width="100%" cellpadding="4" cellspacing="2" class="formsButtonstbl">
<tr>
	<td align="center" class="formsButtons"><div class="centerFloat1">
		<div class="centerFloat2">
			<?if(!$facebook_is_published):?>
			<input type="submit" name="facebook_publish" value="<?=$c_facebook_save?>" class="btn_form1">
			<?elseif ($facebook_letnd_user): // facebook_is_published?>
			<?=$c_facebook_cannot_delete?>: <strong><?=$facebook_letnd_user?></strong>
			<?else: // facebook_is_published?>
			<input type="submit" onclick="return confirm('<?=$c_confirm_form_deleted?>');" name="facebook_delete" value="<?=$c_facebook_delete?>" class="btn_form_delete">
			<?endif //facebook_is_published?>
		</div></td>
</tr>
</table>

<?elseif($facebook_is_active): // facebook_is_logged?>

<table width="100%" cellpadding="4" cellspacing="2" style="padding-top:30px;">
<tr>
	<td align="center" class="formsButtons"><div class="centerFloat1">
		<div class="centerFloat2">
			<a href="<?=$facebook_login_url?>" class="btn_form_save">
				<?=$c_facebook_login?>
			</a>
		</div></div></td>
</tr>
</table>


<?endif // facebook_is_logged?>



<?/* TWITTER */?>

			<div class="sep_dark" style="margin-top:30px;"></div>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
	<td>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td>
			<table width="100%" cellspacing="0" cellpadding="4">
				<tr>
					<td class="formCaptionTitle">
						Twitter<?if($twitter_user_name):?> - <span><?=$twitter_user_name?> - <a href="https://twitter.com/<?=$twitter_user_screen_name?>" target="_blank"><?=$twitter_user_screen_name?></a></span><?endif //twitter_user_link?><?if($twitter_is_logged):?> - <a target="save_frame" href="/?call=twitter_logout&return_url=<?=$return_url?>"><?=$c_facebook_logout?></a><?endif //is_logged?>
					</td>
				</tr>
			</table>


			<?if($twitter_is_logged && !$twitter_is_published):?>
			
			<table width="100%" cellpadding="4" cellspacing="0" class="formstbl">
			
				<tr>
				<td colspan="2" class="formsCaptionHor2"><?=$c_facebook_message?></td>			
				</tr>
			
				<tr>
				
					<td width="150" valign="top" class="formsCaption"><strong>
					<?=$c_twitter_message?>:</strong></td>
					<td valign="top" class="forms"><table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td><textarea onkeyup="return textarea_maxlength(this, 140)" name="twitter_status" id="twitter_status_id" class="wide" rows="2"><?=$twitter_status?></textarea><div><strong><?=$c_chars?>: </strong><span id="twitter_status_id_counter"><?=strlen($twitter_status)?></span></div></td>
						</tr>
					</table></td>
				</tr>
			</table>
			<?endif //twitter_is_logged  twitter_is_published?>
			
			
			<?if($twitter_is_published):?>
				<div style="text-align:center;margin:40px 0"><?=$c_twitter_delete_text?></div>
			<?endif //twitter_is_published?>
			
			</td>
		</tr>
		</table></td>
	</tr>
</table>

<?if($twitter_is_logged):?>

<table width="100%" cellpadding="4" cellspacing="2" class="formsButtonstbl">
<tr>
	<td align="center" class="formsButtons">
		<div class="centerFloat1">
		<div class="centerFloat2">
			<?if(!$twitter_is_published):?>
			<input type="submit" name="twitter_publish" value="<?=$c_twitter_save?>" class="btn_form1">	
			<?elseif ($twitter_letnd_user): // facebook_is_published?>
			<?=$c_twitter_cannot_delete?>: <strong><?=$twitter_letnd_user?></strong>
			<?else: // facebook_is_published?>
			<input type="submit" onclick="return confirm('<?=$c_confirm_form_deleted?>');" name="twitter_delete" value="<?=$c_twitter_delete?>" class="btn_form_delete">
			<?endif //facebook_is_published?>			
		</div>
		</div>
	</td>
</tr>
</table>

<?else: // twitter_is_logged?>

<table width="100%" cellpadding="4" cellspacing="2" style="padding-top:30px;">
<tr>
	<td align="center" class="formsButtons"><div class="centerFloat1">
		<div class="centerFloat2">
			<a href="<?=$twitter_login_url?>" class="btn_form_save">
				<?=$c_twitter_login?>
			</a>
		</div></div></td>
</tr>
</table>


<?endif // twitter_is_logged?>

</form>
</div>