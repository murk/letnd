<?php
set_tool('inmo', 'property', 'inmo__property', 'property_id');
set_language_vars('inmo', 'cateegory', 'property');
set_config('inmo/inmo_property_config.php');
include ('inmo_search_functions.php');
include_once ('inmo_common.php');
include ('inmo_property_functions.php');
function save_rows()
{
    $save_rows = new SaveRows;
    $save_rows->save();
}

function list_records()
{
    global $inmo_list, $gl_action, $gl_page, $tpl, $gl_template, $gl_db_classes_fields;

    $listing = new ListRecords;
    $listing->set_options(1, 1, 0, 1, 1, 1, 1);
    if (isset($_GET['category_id'])) {
    	$listing->condition = 'category_id = ' . R::id('category_id');
	    $listing->add_move_to('category_id');
    }
    if (isset($_GET['zone_id'])) {
    	$listing->condition = 'zone_id = ' . R::id('zone_id');
	    // no puc moure directament, ho posarien en una zona que no correspondria amb el municipi
		//$listing->add_move_to('zone_id');
    }

    $listing->call_function['description'] = 'add_dots';

    if ($gl_page->template == 'print.tpl')
    {
        $listing->template = 'inmo/property_list_print';
    }
    // referncia automatica
    if (INMO_PROPERTY_AUTO_REF == 1)
    {
        $listing->call('get_ref', 'ref,category_id');
    }
    $listing->order_by = 'ref desc, entered desc';
    $listing->set_var ('show_mls_button',false);
    $listing->set_var ('is_mls',false);

    $listing->list_records();
}

function show_form()
{
    show_form_property();
}

function write_record()
{
    write_record_property();
}

function manage_images()
{
    $image_manager = new ImageManager;
    $image_manager->execute();
}
?>