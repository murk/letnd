<?
define("MENU_HOME_PUBLIC","web pública");
define("MENU_HOME","pàgina inici");
define("MENU_LOGOUT","desconnectar");


// configurable
define("USER_TEXT","usuari");
define('USER_LOGIN_INVALID','El nom d\'usuari o password no són vàlids');
define('USER_LOGIN_VALID','Benvingut a ' . HOST_URL);

// la linea de sota no es pot tocar, que te dit que no la toquis!!!!!!!
// menus
define('EXPERIENCIA_MENU_ROOT','Experiència');
define('NEWSLETTER_MENU_ROOT','Newsletter');
define('TRANSLATION_MENU_ROOT','Traduccions');
define('FACTURACIO_MENU_ROOT','Facturació');
define('STATISTICS_MENU_ROOT','Estadístiques');
define('USER_MENU_ROOT','Usuaris');
define('ADMINTOTAL_MENU_ROOT','Admin total');
define('WEB_MENU_ROOT','Gestió');
define('INMO_MENU_ROOT','Immobles');
define('OWNER_MENU_ROOT','Propietaris');
define('REPORT_MENU_ROOT','Documents');
define('CONFIG_MENU_ROOT','Configuració');
define('NEWS_MENU_ROOT','Notícies');
define('GALERY_MENU_ROOT','Galeria imatges');
define('RENOVATS_MENU_ROOT','Postals regal');
define('SHOPWINDOW_MENU_ROOT','Aparadors');
define('UTILITY_MENU_ROOT','Utilitats');
define('MLS_MENU_ROOT','MLS');
define('CUSTUMER_MENU_ROOT','Clients');
define('WORKS_MENU_ROOT','Treballs');
define('PRODUCT_MENU_ROOT','Productes');
define('RATE_MENU_ROOT','Tarifes');
define('CARS_MENU_ROOT','Cotxes');
define('NEW_MENU_ROOT','Noticies');
define('PORTFOLI_MENU_ROOT','Portfoli');
define('CONTACT_MENU_ROOT','Contacte');
define('PRINT_MENU_ROOT','Documents');
define('BOOKING_MENU_ROOT','Reserves');
define('MISSATGE_MENU_ROOT','Missatges');
define('CUSTOMER_MENU_ROOT','Clients i comandes');
define('DIRECTORY_MENU_ROOT','Arxius');
define('SELLPOINT_MENU_ROOT','Punts de venda');



define('DB_SPLIT_TITLE_FIRST_PAGE', 'Inici');
define('DB_SPLIT_TITLE_PREVIOUS_PAGE', 'Pàgina anterior');
define('DB_SPLIT_TITLE_NEXT_PAGE', 'Pàgina següent');
define('DB_SPLIT_TITLE_LAST_PAGE', 'Final');
define('DB_SPLIT_TITLE_PAGE_NO', 'Pàgina %d');
define('DB_SPLIT_TITLE_PREV_SET_OF_NO_PAGE', 'Anteriors %d Pàgines');
define('DB_SPLIT_TITLE_NEXT_SET_OF_NO_PAGE', 'Següents %d Pàgines');
define('DB_SPLIT_BUTTON_FIRST', '&lt;&lt;PRINCIPI');
define('DB_SPLIT_BUTTON_PREV', '&lt;&lt;&nbsp;Anterior');
define('DB_SPLIT_BUTTON_NEXT', 'Següent&nbsp;&gt;&gt;');
define('DB_SPLIT_BUTTON_LAST', 'FINAL&gt;&gt;');
define('DB_SPLIT_DISPLAY_NUMBER_OF_RESULTS', 'Mostrant <b>%d</b> a <b>%d</b> (de <b>%d</b>)');
define ('TITLE_SEARCH','Resultats de la búsqueda');

define('DB_SPLIT_PAGE_TITLE', 'Pàgina');
define('DB_SPLIT_PAGE_DESCRIPTION', 'Resultats %d a %d de %d');

$month_names = array("Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь");
$week_names = array("Воскресенье", "Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота");


// Caption default
$gl_caption['c_form_title'] = 'Editar dades';
$gl_caption['c_print_title'] = 'Dades';
$gl_caption['c_config_title'] = 'Editar configuració';
$gl_caption['c_edit'] = 'Editar';


$gl_caption['c_edit_button'] = 'Editar dades';
$gl_caption['c_view_button'] = 'Veure dades';
$gl_caption['c_edit_images_button'] = 'Editar imatges';
$gl_caption['c_add_images_button'] = 'Afegir imatges';



$gl_caption['c_edit_data'] = 'Editar dades';
$gl_caption['c_edit_images'] = 'Editar imatges';
$gl_caption['c_add_images'] = 'Afegir imatges';
$gl_caption['c_search_form'] = 'Buscar';
$gl_caption['c_send_new'] = 'Сохранить';
$gl_caption['c_send_edit'] = 'Сохранить изменения';
$gl_caption['c_delete'] = 'Удалить';
$gl_caption['c_action'] = 'Acció';
$gl_caption['c_delete_selected'] = 'Esborrar seleccionats';
$gl_caption['c_restore_selected'] = 'Recuperar seleccionats';
$gl_caption['c_confirm_deleted'] = 'Els elements seleccionats s\\\'esborraran definitivament de la base de dades \\n\\nDesitges continuar?';
$gl_caption['c_confirm_images_deleted'] = 'Les imatges seleccionats s\\\'esborraran definitivament\\n\\nDesitges continuar?';
$gl_caption['c_confirm_form_bin'] = 'El registre s\\\'enviarà a la paperera de reciclatge \\n\\nDesitges continuar?';
$gl_caption['c_confirm_form_deleted'] = 'El registre s\\\'esborrarà definitivament de la base de dades \\n\\nDesitges continuar?';
$gl_caption['c_move_to'] = 'Moure a:';
$gl_caption['c_print_all'] = 'Напечатать всё';
$gl_caption['c_print'] = 'Напечатать';
$gl_caption['c_order_by'] = 'Упорядочить:';
$gl_caption['c_select'] = ' Выбрать';
$gl_caption['c_multislect_selectable'] = 'Fields';
$gl_caption['c_multislect_selected'] = 'Selected fields';
$gl_caption['c_all'] = ' Всё';
$gl_caption['c_nothing'] = 'Ничего';
$gl_caption['c_preview'] = ' Просмотреть';
$gl_caption['c_yes'] = 'Да';
$gl_caption['c_no'] = 'Нет';
$gl_caption['c_options_bar'] = ' Варианты';
$gl_caption['c_file_maximum'] = 'màxim %s arxius alhora';
$gl_caption['c_file_maximum_1'] = 'màxim 1 arxiu alhora';

// buscadors
$gl_caption['c_search_title'] = ' Искать';
$gl_caption['c_by_ref'] = '(o referències separades per espais, ej: "456 245 45")';
$gl_caption['c_by_words'] = 'Per paraules';

// sobreescrits caption images
$gl_caption_image['c_edit'] = 'Imatges de';
$gl_caption_image['c_insert'] = 'Afegir imatges des del teu ordinador';
$gl_caption_image['c_insert_advanced'] = 'Avançat (java)';
$gl_caption_image['c_delete_selected'] = 'Eliminar imatges seleccionades';
$gl_caption_image['c_confirm_deleted'] = 'Les imatges es esborran definitivament.\\n\\n\\nDesitges continuar?';
$gl_caption_image['c_select_all'] = 'Seleccionar totes';
$gl_caption_image['c_deselect_all'] = 'Deseleccionar totes';

// nous images
$gl_caption_image['c_stop'] = 'Parar';
$gl_caption_image['c_refresh'] = 'Actualitzar';
$gl_caption_image['c_add'] = 'Afegir imatges';
$gl_caption_image['c_thumbnails'] = 'Imatges';
$gl_caption_image['c_icons'] = 'Icones';
$gl_caption_image['c_list'] = 'Llista';
$gl_caption_image['c_details'] = 'Detalls';
$gl_caption_image['c_progress_dialog_title_text'] = 'Afegint imatges';

// camps images
$gl_caption_image['c_ordre'] = 'ordre';
$gl_caption_image['c_main_image'] = 'principal';
$gl_caption_image['c_name'] = 'Nom';
$gl_caption_image['c_imagecat_id'] = 'Categoria';
$gl_caption_image['c_image_alt'] = 'Text alternatiu (alt)';
$gl_caption_image['c_image_title'] = 'Títol';
$gl_caption_image['c_edit_titles_images'] = 'Editar títols imatges';
$gl_caption_image['c_hide_titles_images'] = 'Amagar títols imatges';

// sobreescrits caption traduccions i traduccions_news.tpl
$gl_caption_translation['c_send_edit'] = 'Enviar a traduir';
$gl_caption_translation['c_page_words_title'] = 'Total de paraules per traduir en aquesta pàgina';
$gl_caption_translation['c_all_page_words_title'] = 'Total de paraules per traduir en totes les pàgines';
$gl_caption_translation['c_total'] = 'Total';

// sobreescrits end


// Message default
$gl_messages['title_edit']='Редактировать';
$gl_messages['title_copy']=' Скопировать данные';
$gl_messages['added']=' Регистр добавлен правильно';
$gl_messages['add_exists']='Ja existeix un registre amb aquest/a %1$s: %2$s';
$gl_messages['saved']='Регистр  изменён правильно';
$gl_messages['not_saved']=' Регистр не изменён';
$gl_messages['no_records']=' В этом списке нет  регистра';
$gl_messages['no_records_bin']='В корзине нет файлов';
$gl_messages['list_saved']='Изменения в регистрах произведены правильно';
$gl_messages['list_not_saved']='Не изменён ни один регистр и не обнаружено никаких изменений';
$gl_messages['list_deleted']='Регистры удалены';
$gl_messages['list_bined']='Регистры удалены в корзину';
$gl_messages['list_restored']='Регистры восстановлены правильно';
$gl_messages['concession_no_acces']='No tens permisos per aquesta secció';
$gl_messages['concession_no_write']='No tens permisos d\'escritura per aquesta secció';
$gl_messages['select_item']='Fes un click en un registre de la llista per seleccionar';
$gl_messages['related_not_deleted']='Els registres marcats no s\'han pogut esborrar ja que tenen registres relacionats';



// sobreescrits message image
$gl_messages_image['list_saved']='Els canvis en les imatges s\'han efectuat correctament.';
$gl_messages_image['list_not_saved']='No s\'ha modificat cap registre, no s\'ha detectat cap canvi';
$gl_messages_image['list_deleted']='Les imatges han estat esborrades';
// sobreescrits end


// NO TRADUIT
$gl_messages['deleted']='Регистр удалён';
$gl_messages['bined']='Регистр отправлен в корзину';
$gl_messages['restored']='Регистр восстановлен успешно';
$gl_caption['c_ok'] = 'Принять';
$gl_caption['c_cancel'] = ' Закрыть';
$gl_caption['c_close'] = ' Close';
$gl_messages['post_max_size']='Объём  формуляра (содержимое + приложенные файлы) превышает установленный лимит ';
$gl_messages['post_file_max_size']='Объём приложенных файлов превышает установленный  лимит ';
$gl_messages['assigned']='Registres sel·leccionats';
$gl_caption['c_black']='Negre';
$gl_caption['c_white']='Blanc';
$gl_caption['c_silver']='Plata';
$gl_caption['c_grey']='Verd';
$gl_caption['c_blue']='Blau';
$gl_caption['c_red']='Vermell';
$gl_caption['c_orange']='Taronja';
$gl_caption['c_green']='Verd';
$gl_caption['c_purple']='Lila';
$gl_caption['c_pink']='Rosa';
$gl_caption['c_golden']='Daurat';
$gl_caption['c_yellow']='Groc';
$gl_caption['c_maroon']='Grana';
$gl_caption['c_brown']='Marró';
//creo un array global per els mesos
$gl_caption_months = array("Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь");

$gl_caption ['c_2']='два';
$gl_caption ['c_3']=' три';
$gl_caption ['c_4']=' четыре';
$gl_caption ['c_5']='пять';
$gl_caption ['c_6']=' шесть';
$gl_caption ['c_7']='семь';
$gl_caption ['c_8']='восемь';
$gl_caption ['c_9']='девять';
$gl_caption['c_10']=' десять';
$gl_caption['c_11']='одиннадцать';
$gl_caption['c_12']=' двенадцать';
$gl_caption['c_13']=' тринадцать';
$gl_caption['c_14']=' четырнадцать';
$gl_caption['c_15']=' пятнадцать';
$gl_caption['c_16']=' шестнадцать';
$gl_caption['c_17']=' семьнадцать';
$gl_caption['c_18']=' восемьнадцать';
$gl_caption['c_19']=' девятнадцать';
$gl_caption['c_20']=' двадцать';
$gl_caption['c_30']=' тридцать';
$gl_caption['c_40']=' сорок';
$gl_caption['c_50']=' пятьдесят';
$gl_caption['c_60']=' шестьдесят';
$gl_caption['c_70']=' семьдесят';
$gl_caption['c_80']=' восемьдесят';
$gl_caption['c_90']='девяносто';
$gl_caption['c_1000']='тысяча';


$gl_caption['c_add_button'] = 'Afegir registre';

$gl_caption['c_login'] = ' логин';
$gl_caption['c_password'] = 'пароль';
$gl_caption['c_mail'] = 'E-mail';
$gl_caption['c_mail_repeat'] = 'повторить Е-mail';
$gl_caption['c_password_repeat'] = 'повторить пароль';
$gl_caption['c_noscript'] = '<p><strong>Ваш поисковик не поддерживаем или дезактивирован Javascript. </strong></p><p>Ha d\'<a href="//www.google.com/support/websearch/bin/answer.py?answer=23852">habilitar Javascript</a> per poder utilitzar Letnd</p>';
$gl_caption['c_cookie'] = '<p><strong>Ваш поисковик не поддерживаем или дезактивированы Cookies. </strong></p><p>Ha d\'<a href="//www.google.com/support/websearch/bin/answer.py?answer=35851">habilitar les Cookies</a> per poder utilitzar Letnd</p>';

$gl_messages_image['images_uploaded']='Imatges guardades';
$gl_messages['form_sent']='Формуляр отправлен правильно';
$gl_messages['form_not_sent']='Формуляр не удалось отправить';


$gl_caption['c_search_adress'] = 'Искать адрес';
$gl_caption['c_map_frame_1'] = 'S\'ha trobat coincidència per';
$gl_caption['c_map_frame_2'] = 'Si l\'ubicació és correcta';
$gl_caption['c_map_frame_3'] = 'clica aquí per guardar';
$gl_caption['c_map_frame_4'] = 'No s\'ha trobat cap coincidència';

$gl_caption['c_list_view_1'] = 'Нормально';
$gl_caption['c_list_view_2'] = 'большие логотипы';
$gl_caption['c_list_view_3'] = 'Список';

$gl_caption['c_edit_list_button_1'] = 'No editar llistat';
$gl_caption['c_edit_list_button_'] = 'Editar llistat';
$gl_caption['c_add_dots'] = 'Retallar text';

// SEO
$gl_caption['c_seo']='Eines SEO ( Posicionament web )';
$gl_caption['c_page_title']='Títol de la pàgina. max. 66-69 caràcters. Òptim < 65 car:';
$gl_caption['c_page_description']='Descripció de la pàgina. max. 156-158 caràcters:';
$gl_caption['c_page_keywords']='Paràules clau:';
$gl_caption['c_file_name']='Nom de la pàgina en URL. Ex. www.lamevaweb.com/<b>nom-de-la-pagina</b>.htm max. 90 caràcters:';
$gl_caption['c_file_name_excel']='Nom pàgina';
$gl_caption['c_old_file_name']='Nom vell de la pàgina en URL.';



// SOCIAL
$gl_caption['c_edit_social'] = 'Xarxes socials';
$gl_caption['c_edit_social_title'] = 'Publicar';
$gl_caption['c_social_language'] = 'Idioma';
$gl_caption['c_facebook_message'] = 'Missatge';
$gl_caption['c_facebook_picture'] = 'Imatge';
$gl_caption['c_facebook_link'] = 'Enllaç';
$gl_caption['c_facebook_link_explanation'] = 'Agafa els texts directament de la pàgina';
$gl_caption['c_facebook_name'] = 'Descripció 1<sup>a</sup> línia';
$gl_caption['c_facebook_caption'] = 'Descripció 2<sup>a</sup> línia';
$gl_caption['c_facebook_image_title'] = 'Imatge';
$gl_caption['c_facebook_description'] = 'Descripció';
$gl_caption['c_facebook_save'] = 'Publicar a facebook';
$gl_caption['c_facebook_publish_as'] = 'Publicar com a';
$gl_caption['c_facebook_publish_as_image'] = 'Imatge';
$gl_caption['c_facebook_publish_as_link'] = 'Enllaç';
$gl_caption['c_facebook_publish_save_default']='Predeterminar aquesta opció';
$gl_caption['c_facebook_publish_user']='Mur propi';
$gl_caption['c_facebook_publish_pagina']='Pàgina';
$gl_caption['c_facebook_publish_id']='Publicar a';
$gl_caption['c_facebook_login']='Iniciar sessió a Facebook';
$gl_caption['c_facebook_delete_text']='<p>Els missatges publicats a Facebook només es poden modificar des de Facebook.</p><p>Si vols tornar a publicar des de Letnd primer s\'ha d\'esborrar la publicació anterior</p>';
$gl_caption['c_facebook_delete']='Esborrar publicació a Facebook';
$gl_caption['c_facebook_logout']='Logout';
$gl_caption['c_facebook_cannot_delete']='No es pot esborrar la publicació ja que es va crear amb un altre usuari de Facebook corresponent a';
$gl_caption['c_facebook_activate']='El preu d\'activació d\'aquest servei es de 60€ para poder donar d\'alta a Facebook';

$gl_caption['c_twitter_message'] = 'Missatge';
$gl_caption['c_twitter_save'] = 'Publicar a twitter';
$gl_caption['c_twitter_login']='Iniciar sessió a Twitter';
$gl_caption['c_twitter_delete_text']='<p>Els missatges publicats a Twitter només es poden modificar des de Twitter.</p><p>Si vols tornar a publicar des de Letnd primer s\'ha d\'esborrar la publicació anterior</p>';
$gl_caption['c_twitter_delete']='Esborrar publicació a Twitter';
$gl_caption['c_twitter_cannot_delete']='No es pot esborrar la publicació ja que es va crear amb un altre usuari de Twitter corresponent a';

$gl_caption['c_button_form_twitter_published']=
$gl_caption['c_button_form_facebook_published']=
$gl_caption['c_button_form_twitter']=
$gl_caption['c_button_form_facebook']=	'';
$gl_caption['c_chars']=	'Caràcters';

$gl_messages['social_facebook_no_user']='No estàs loguejat, si us plau torna a probar-ho';
$gl_messages['social_facebook_no_page']='No hi ha cap pàgina amb aquest id';
$gl_messages['social_facebook_published']='El missatge s\'ha publicat correctament a Facebook';
$gl_messages['social_facebook_not_init']='No s\'ha pogut connectar a Facebook';
$gl_messages['social_facebook_error']='Hi ha hagut un error a Facebook: ';
$gl_messages['social_facebook_deleted']='El missatge s\'ha esborrat a Facebook';
$gl_messages['social_facebook_not_deleted']='No hi ha cap missatge per borrar';

$gl_messages['social_twitter_empty']='El missatge està buit, no es pot publicar a Twitter';
$gl_messages['social_twitter_no_user']='No estàs loguejat, si us plau torna a probar-ho';
$gl_messages['social_twitter_published']='El missatge s\'ha publicat correctament a Twitter';
$gl_messages['social_twitter_not_init']='No s\'ha pogut conectar a Twitter';
$gl_messages['social_twitter_error']='Hi ha hagut un error a Twitter: ';
$gl_messages['social_twitter_deleted']='El missatge s\'ha esborrat a Twitter';
$gl_messages['social_twitter_not_deleted']='No hi ha cap missatge per borrar';

// FI NO TRADUIT


$gl_caption['c_file_manager']=	'Gestió d\'arxius';
$gl_messages['no_records_search']='No match found for this search';
$gl_caption['c_back'] = 'Tornar';

$gl_caption['c_asc'] = 'asc';
$gl_caption['c_desc'] = 'desc';

$gl_caption['c_reply'] = 'Respondre';

$gl_caption['c_file_file']='File';
$gl_caption['c_file_title']='Title';
$gl_caption['c_file_show_home']='In list';
$gl_caption['c_file_ordre']='Order';
$gl_caption['c_file_public']='Public';
$gl_caption['c_file_filecat_id']='Cat.';

$gl_caption['c_export_button']= 'Export';

$gl_caption['c_entered'] = 'Created';
$gl_caption['c_modified'] = 'Modified';