<?
// si estic al carro no mostro el bloc del carro

$show_always = isset($vars['show_always'])?$vars['show_always']:'0'; // l'agafa de la taula all__block 

global $gl_module;

if (!$show_always && $gl_module){
	$m = &$gl_module;
	if (($m->tool=='product' &&
		($m->tool_section=='cart' && $m->action='list_records') ||
		($m->tool_section=='payment'))){
		$show_block = false;
		return;
	}
}
// A la resta mostro el block
$module = Module::load('product','cart');
$vars['content'] = $module->do_action('get_records_block');
$vars['has_results'] = $module->has_results;
?>