var gl_is_product_list = false;
// product list
function product_list_substract_quantity(id){
	var quantity = Number($('#quantity_wanted_'+id).val())-1;
	if (quantity>0) {
		$('#quantity_wanted_'+id).val(quantity);
		product_list_check_max_min_quantity(id);
		quantity = $('#quantity_wanted_'+id).val();
		product_list_set_pvp(id, quantity);
	}
}
function product_list_add_quantity(id){
	var quantity = Number($('#quantity_wanted_'+id).val())+1;
	$('#quantity_wanted_'+id).val(quantity);
	product_list_check_max_min_quantity(id);
	quantity = $('#quantity_wanted_'+id).val();
	product_list_set_pvp(id, quantity);
}
function product_list_set_pvp(id, quantity) {
	gl_is_product_list = true;
	var pvp = $('#pvp_' + id);

	if (pvp.length > 0) {
		pvp_per_unit = pvp.data('pvp');
		pvp.html(format_currency(pvp_per_unit * quantity * 100));
	}

  if (typeof product.quantity_on_change === 'function') product.quantity_on_change(id, quantity);
}
function product_list_change_quantity(id) {
	var quantity = Number($('#quantity_wanted_'+id).val());

	if (isNaN(quantity) || quantity <= 0)
	{
		quantity = 1;
		$('#quantity_wanted_'+id).val(1);
	}

	product_list_set_pvp(id, quantity);
	product_list_check_max_min_quantity(id);
}
function product_list_check_max_min_quantity(id) {

	var max_quantity = $('#max_quantity_' + id);
	var min_quantity = $('#min_quantity_' + id);
	var quantity = Number($('#quantity_wanted_'+id).val());

	if (max_quantity.length > 0) {
		var max = Number (max_quantity.val());

		if (quantity > max) {
			$('#quantity_wanted_' + id).val(max);
		}

	}

	if (min_quantity.length > 0) {
		var min = Number (min_quantity.val());

		if (quantity < min) {
			$('#quantity_wanted_' + id).val(min);
		}

	}
}

function product_list_add_item(id){

	gl_is_product_list = true;
	var product_date = '', extras;

	// Lassdive
	if (typeof calendar !== 'undefined' && typeof calendar[id] !== 'undefined') {
		if (! calendar[id].date_busy && !calendar[id].is_empty ) {

			if (calendar[id].is_date_busy_required ) {
				alert(gl_message_choose_date);
				return;
			}
			else {
				product_date = '0000/0/0';
				extras = calendar[id].reserva_items;
			}
		}
		else {
			product_date = calendar[id].date_busy;
			extras = calendar[id].reserva_items;
		}
	}

	var quantity = Number($('#quantity_wanted_'+id).val());
	cart_submit_data('add_item', id, quantity, 1, '', product_date, extras);
}

// product form
function product_substract_quantity(){
	var quantity = Number($('#quantity_wanted').val())-1;
	if (quantity>0) {
		$('#quantity_wanted').val(quantity);
	}
}
function product_add_quantity(){
	var quantity = Number($('#quantity_wanted').val())+1;
	$('#quantity_wanted').val(quantity);
}

function cart_add_item(product_id, is_block, quantity, e){
	if (typeof e != "undefined") {
		e = window.event || e;
		e.stopPropagation ? e.stopPropagation() : (e.cancelBubble=true);
	}
	
	quantity = (typeof quantity == "undefined")?1:quantity; 
	if ((quantity!=Number(quantity)) && (quantity!=''))
	{
		alert(c_quantity + VALIDAR_TEXT5);
		return false;
	}
	cart_submit_data('add_item', product_id, quantity, is_block);
}

// cart list
function cart_delete_item(product_id, is_block, product_variation_ids){
	cart_submit_data('delete_item', product_id, 0, is_block, product_variation_ids);
}
function cart_substract_quantity(product_id, is_block, product_variation_ids){
	quantity = Number($('#quantity_'+product_id+'_'+product_variation_ids).val())-1;
	if (quantity>0) {
		$('#quantity_'+product_id+'_'+product_variation_ids).val(quantity);
		cart_submit_data('change_quantity', product_id, quantity, is_block, product_variation_ids);
	}
}
function cart_change_quantity(product_id, quantity, is_block, product_variation_ids){
	if ((quantity!=Number(quantity)) && (quantity!=''))
	{
		alert(c_quantity + VALIDAR_TEXT5);
		return false;
	}
	if (quantity>0) {
		cart_submit_data('change_quantity', product_id, quantity, is_block, product_variation_ids);
	}
	else{
		$('#quantity_'+product_id+'_'+product_variation_ids).val(1);
	}
}

function cart_add_quantity(product_id, is_block, product_variation_ids){
	quantity = Number($('#quantity_'+product_id+'_'+product_variation_ids).val())+1;
	$('#quantity_'+product_id+'_'+product_variation_ids).val(quantity);
	cart_submit_data('change_quantity', product_id, quantity, is_block, product_variation_ids);
}
// fi funcions llistat

function cart_empty_cart(product_id, quantity, is_block){
	cart_submit_data('empty_cart', product_id, 0, is_block);
}

function cart_submit_data(action, product_id, quantity, is_block, product_variation_ids, product_date, extra_vars)
{
	product_variation_ids = product_variation_ids || '';
	product_date = product_date || '';
	extra_vars = extra_vars || '';

	// en fitxa producte radios
	if (!product_variation_ids){
		
		if ($("input[type=radio][name*='variation[']").length > 0){
			ob =  $("input[name*='variation[']:checked");
			ob.each(function(key, val) {
				product_variation_ids +=$(this).val()+'_';
			}); 
		} 
		// en fitxa producte select
		else if ($("select[name*='variation[']").length > 0){
			ob = $("select[name*='variation[']");
			ob.each(function(key, val) {
				product_variation_ids +=$(this).val()+'_';
			}); 
		}
		var $hidden_variations =
		gl_is_product_list ?
			$('#product_variations_' + product_id + " input[type=hidden][name*='variation[']") :
			$("input[type=hidden][name*='variation[']");

		// en fitxa producte hidden
		if ($hidden_variations.length > 0){
			$hidden_variations.each(function(key, val) {
				product_variation_ids +=$(this).val()+'_';
			}); 
		}
		
		if (!product_variation_ids) product_variation_ids ='0';
 	}
  
	$("body").css('cursor','wait');
	var ajax_config =
		{
			async: true,
			type: "POST",
			url: "/?tool=product&tool_section=cart&action="+action+"&product_id="+product_id+"&product_variation_ids="+product_variation_ids+"&product_date="+product_date+"&quantity="+quantity+"&is_block="+is_block+"&language="+gl_language,
			data: {
				extra_vars: extra_vars
			},
			beforeSend: cart_before_send,
			timeout: 4000,
			error: cart_on_error
		};
  if (action=='add_item') {
  	ajax_config.success = function (dades){cart_on_succes_add(dades,is_block,product_id);};
  }
  else if (action=='delete_item'){
  	ajax_config.success = function (dades){cart_on_succes_delete(dades,is_block);};
  }
  else{
  	ajax_config.success = function (dades){cart_on_succes(dades,is_block);}
  }
  $.ajax(ajax_config);
}

function cart_before_send()
{
  //showPopWin('', 300, 100, null, false, false, '');
}

function cart_on_succes_add(dades,is_block,product_id)
{
  showPopWin('', 300, 100, null, false, false, PRODUCT_CART_ADDED);
  setTimeout("hidePopWin(false);", 800);
  cart_on_succes(dades,is_block,product_id);
}
function cart_on_succes_delete(dades,is_block)
{
  //showPopWin('', 300, 100, null, false, false, PRODUCT_CART_DELETED);
  //setTimeout("hidePopWin(false);", 800);
  cart_on_succes(dades,is_block); 
}
function cart_on_succes(dades,is_block,product_id)
{
  if (is_block){
	  if (typeof dades.content !== 'undefined') {

		  $("#cart_block_content").html(dades.content);

		  $total_quantity = $("#cart_block_total_quantity");
		  $total_quantity.html(dades.total_quantity);

		  $total_price = $("#cart_block_total_price");
		  $total_price.html(dades.total_price);

		  if (dades.total_quantity == 0) {
			  $total_quantity.hide();
			  if (typeof product.open_cart === 'function') product.open_cart(true);
		  }
		  else {
			   $total_quantity.show();
		  }

		  if (dades.js_error){
				eval (dades.js_error);
		  }
	  }
	  else {
		  $("#cart_block").html(dades);
	  }
  }
  else{  
	$("#cart").html(dades);
  }

  if (typeof product.cart_on_succes === 'function') product.cart_on_succes(is_block,product_id);

	$("body").css('cursor','auto');
}

function cart_on_error()
{
  showPopWin('', 300, 100, null, false, false, 'Problemas en el servidor');
  setTimeout("hidePopWin(false);", 800);
}

customer_show_address_first = true;

function customer_show_address(obj){
	$('#delivery_address').toggle(200);
	if (customer_show_address_first){
		$('body').click();
		nom = $("input[name='name[0]']").val();
		cognom = $("input[name='surname[0]']").val();
		$("input[name='a_name']").val(nom);
		$("input[name='a_surname']").val(cognom);
		customer_show_address_first = false;
	}
}

function customer_show_password(sufix){
	if (!sufix) sufix = ''; 
	$('#password_form' + sufix).toggle(200);
	$('#mail_password' + sufix).focus();
	$("#login_form" + sufix).toggle(200, function () {		
	});
}
function customer_show_login(){
	mail = $("#mail_password").val();
	$("#mail").val(mail);
	customer_show_password();
}

function address_add(customer_id){
	showPopWin('/?tool=product&tool_section=address&template=clean&action=show_form_new&language=' + gl_language + '&customer_id='+customer_id, 400, 300, false, true);
}
function address_edit(address_id){
	showPopWin('/?tool=product&tool_section=address&template=clean&action=show_form_edit&address_id='+address_id+'&language=' + gl_language, 400, 300, false, true);

}
function customer_show_invoice(order_id){
	showPopWin('/?tool=product&tool_section=customer&template=clean&action=show_invoice&invoice=invoice&order_id='+order_id+'&language=' + gl_language, 800, 600, false, true, false, false, true);

}
function customer_show_invoice_refund(order_id){
	showPopWin('/?tool=product&tool_section=customer&template=clean&action=show_invoice&invoice=refund&order_id='+order_id+'&language=' + gl_language, 800, 600, false, true, false, false, true);

}
function product_set_price(product_pvp, variation_category_id, product_variation_id, product_id){

	variation_category_id = variation_category_id || false;
	product_variation_id = product_variation_id || false;
	product_id = product_id || false;

	var is_list = product_id !== false && $('#pvp_'+product_id).length;
	var category_id_to_use = is_list ? variation_category_id + '_' + product_id : variation_category_id;

	if (variation_category_id && product_variation_id) {
		$('#variation_' + category_id_to_use).val(product_variation_id);
		$('#variation_category_holder_' + category_id_to_use + ' .active1').removeClass('active1').addClass('active0');
		$('#product_variation_' + product_variation_id).removeClass('active0').addClass('active1');
	}

	var $pvp, $old_price, $ref;

	if (is_list) {
		$pvp = $('#pvp_'+product_id);
		$old_price = $('#old_price_'+product_id);
		$ref = $('#product-ref_'+product_id);
	}
	else {
		$pvp = $('#pvp');
		$old_price = $('#old_price');
		$ref = $('#product-ref');
	}
	
	var price_increment = 0;
	var ref = '';
	var old_price = Number ($old_price.attr('data-old-price'));
	var old_price_increment = 0;

	var ob =  $("input[type=radio][name*='variation[']:checked");

	if (ob.length > 0){ // si son radios
		ob.each(function(key, val) {
			var variation = $("input[name='variation_pvp[" + $(this).val() + "]']");
			var variation_ref = variation.attr('data-variation-ref');
			if (variation_ref) ref = variation_ref;
			price_increment +=Number(variation.val());
			old_price_increment += Number(variation.attr('data-variation-old-price'));
		});
	}
	else{// si es un select
		ob =  $("select[name*='variation[']");		
		ob.each(function(key, val) {
			var variation = $("input[name='variation_pvp[" + $(this).val() + "]']");
			var variation_ref = variation.attr('data-variation-ref');
			if (variation_ref) ref = variation_ref;
			price_increment +=Number(variation.val());
			old_price_increment += Number(variation.attr('data-variation-old-price'));
		});
	}
	
	// si tinc custom variations. No exclou radios, select


	var $hidden_variations =
		gl_is_product_list ?
			$('#product_variations_' + product_id + " input[type=hidden][name*='variation[']") :
			$("input[type=hidden][name*='variation[']");

	if ($hidden_variations.length > 0){ // si son hidden

		$hidden_variations.each(function(key, val) {
			var variation = $("input[name='variation_pvp[" + $(this).val() + "]']");
			var variation_ref = variation.attr('data-variation-ref');
			if (variation_ref) ref = variation_ref;
			price_increment +=Number(variation.val());
			old_price_increment += Number(variation.attr('data-variation-old-price'));
		});
	}

	price_increment = Math.round(price_increment*100); // floating point sum errors
	product_pvp = Math.round(product_pvp*100);
	$pvp.attr('data-pvp', (product_pvp + price_increment)/100);
	$pvp.text(format_currency(product_pvp + price_increment));
	if (ref) $ref.text(ref);


	if (!old_price) {
		$old_price.hide();
	}
	else {
		old_price = Math.round(old_price*100);
		old_price_increment = Math.round(old_price_increment*100);
		$old_price.text(format_currency (old_price + old_price_increment));
	}

  if (typeof product.variation_on_select === 'function') product.variation_on_select(product_pvp, variation_category_id, product_variation_id, product_id);
}
function format_currency(currency){

	currency =currency/100;
	var ret_curr = currency.toString();
	var format_price_as_currency = typeof gl_format_price_as_currency === 'undefined'?'0':gl_format_price_as_currency;
	
	ret_curr = ret_curr.replace('.',',');
	
	if (currency>999){
		var coma_index = ret_curr.indexOf(',');
		if (coma_index==-1) coma_index = ret_curr.length;
		punt_index = coma_index-3;
		ret_curr = ret_curr.substr(0,punt_index) + '.' + ret_curr.substr(punt_index)
	}

	if (format_price_as_currency == '1'){
		var coma_index = ret_curr.indexOf(',');
		if (coma_index == -1){
			ret_curr = ret_curr + ',00';
		}
		var after_coma = ret_curr.substr(coma_index);
		if (after_coma.length == 2) ret_curr = ret_curr + '0';
	}

	return ret_curr;
}

function set_country_change (num, id) {

	if (id==0 || id){

		var country = '#country_id' + num + '_' + id;

		if (!$(country).length) return;

		$(country).change(function(){
			country_on_change (num, this.value)
		});

		country_on_change (num, $(country).val());
	}
}
function payment_get_promcode (form) {
	var old_action = $(form).attr('action');
	$(form).attr('action', '').submit();
	$(form).attr('action', old_action);
}
function country_on_change (num, value) {

	var provincia = $('#provincia_id' + num + '_holder');

	if(value=='ES'){
		provincia.show();
	}
	else{
		provincia.hide();
	}
}

function payment_set_collect () {

	var collect = $('#collect').is(":checked")?'1':'0';
	var url = '/' + gl_language + '/product/cart/set_collect?collect=' + collect;

	$.ajax({
				url: url,
				dataType: 'json'
			})
				.done(function (data) {
					if (data.success) {
						$('#total_shipping').html(data.rate_price + ' ' + data.currency_name);
						$('#total_price').html(data.all_price + ' ' + data.currency_name);
					} else if (data.error) {
						alert(data.error);
					}
				})
				.fail(function (jqXHR, textStatus, errorThrown) {
					alert(textStatus + ': ' + errorThrown);
				})
			;
}

$(function(){

	if (typeof (gl_tool_section) === 'undefined') gl_tool_section = 'customer';

	var id = $("input[name^='" + gl_tool_section + "_id']").val();
	set_country_change ('', id);
	set_country_change ('2', '999');

});