<script language="JavaScript" src="/admin/jscripts/validacio.js?v=<?=$version?>"></script>
<script language="JavaScript" src="/admin/jscripts/euro_calc.js?v=<?=$version?>"></script>
<script language="JavaScript" src="/common/jscripts/jquery.dropshadow.js?v=<?=$version?>"></script>
<script type="text/javascript">
$(function() {
	set_focus(document.theForm);
});
</script>
<form name="form_delete" method="post" action="<?=$link_action_delete?>" target="save_frame">
		<input name="<?=$id_field?>" type="hidden" value="<?=$id?>">
</form><form name="theForm" method="post" action="<?=$link_action?>" onsubmit="<?=$js_string?>" encType="multipart/form-data" target="save_frame">
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="form"> 
	<tr>
		<td>
				<?if ($form_new):?>
				<div id="form_buttons_holder"><table id ="form_buttons">
					<tr> 
						<td class="buttons_top"><div class="right"><input type="submit" name="Submit" value="<?=$c_send_new?>" class="btn_form1"></div></td>
					</tr>
				</table></div>
				<?endif //form_new?><?if ($form_edit):?>
						<div id="form_buttons_holder"><table id ="form_buttons">
							<tr>
								<td valign="bottom"><div class="menu"><div><?=$c_edit_data?></div><?if ($has_images):?>
							<a href="<?=$image_link?>"><?=$c_edit_images?></a>
				 <?endif //has_images?></div></td>
								<td class="buttons_top"><div class="right"><input type="submit" name="Submit" value="<?=$c_send_edit?>" class="btn_form1" /><?=$language_select?><a id="print_pdf" href="/spa/nautica/embarcacion/print_pdf/embarcacion_id/<?=$id?>/" target="_new" class="btn_form1"><?=$c_print_pdf?></a></div></td>
							</tr>
					</table></div>
					<?endif //form_edit?> 
						<table width="100%" cellpadding="0" cellspacing="0" >
								<tr> 
									<td class="formCaptionTitle"><strong>
									<?=$c_data?>&nbsp;&nbsp;</strong><?if ($form_edit):?> " <?=$title_form_nautica?> "<?endif //$form_edit?></td>
								</tr>
						</table>  
						<table width="100%" cellpadding="0" cellspacing="0" >
								<tr> 
									<td class="formsCaptionHorFirst">
									<?=$c_general_data?></td>
								</tr>
						</table>                                        
										<table width="100%" cellpadding="0" cellspacing="0" class="formstbl">
											<tr>
												<td class="formsCaption"><strong>
													<?=$c_clau_dossier?>:</strong>
												<input name="embarcacion_id[<?=$id?>]" type="hidden" value="<?=$id?>" />
												<input name="embarcacion_id_javascript" type="hidden" value="<?=$id?>" /></td>
												<td class="forms"><table border="0" cellspacing="0" cellpadding="0">
													<tr>
															<td><?=$c_dossier?>&nbsp;&nbsp;</td>
															<?if($has_clau):?><td><?=$c_clau?></td><?endif //has_clau?>
															<td><?=$c_entered_form?></td>
														</tr>
														<tr>
															<td><?=$dossier?>
																&nbsp;&nbsp;</td>
															<?if($has_clau):?>
															<td><?=$clau?>
																&nbsp;&nbsp;</td>
																<?endif //has_clau?>
															<td><?=$entered?></td>
														</tr>
												</table></td>
											</tr>
											<tr>
												<td valign="top" class="formsCaption"><strong>
													<?=$c_status?>:</strong></td>
												<td class="forms"><?=$status?></td>
											</tr>
											<tr>
												<td valign="top" class="formsCaption"><strong>
													<?=$c_tipus_id?>:</strong></td>
												<td class="forms"><?=$tipus_id?></td>
											</tr>
											<tr>
												<td valign="top" class="formsCaption"><strong>
													<?=$c_shipyard_id?>:</strong></td>
												<td class="forms"><?=$shipyard_id?></td>
											</tr>
											<tr>
												<td valign="top" class="formsCaption"><strong>
													<?=$c_model?>:</strong></td>
												<td class="forms"><?=$model?></td>
											</tr>
											<tr>
												<td valign="top" class="formsCaption"><strong>
													<?=$c_buildyear_form?>:</strong></td>
												<td class="forms"><?=$buildyear?></td>
											</tr>
											<tr>
												<td valign="top" class="formsCaption"><strong>
													<?=$c_engine?>:</strong></td>
												<td class="forms"><?=$engine?></td>
											</tr>
											<tr>
												<td valign="top" class="formsCaption"><strong>
													<?=$c_engine_type?>:</strong></td>
												<td class="forms"><?=$engine_type?></td>
											</tr>
											<tr>
												<td valign="top" class="formsCaption"><strong>
													<?=$c_fuel?>:</strong></td>
												<td class="forms"><?=$fuel?></td>
											</tr>
											<tr>
												<td valign="top" class="formsCaption"><strong>
													<?=$c_fuel_tank?>:</strong></td>
												<td class="forms"><?=$fuel_tank?> l.</td>
											</tr>
											<tr>
												<td valign="top" class="formsCaption"><strong>
													<?=$c_water_tank?>:</strong></td>
												<td class="forms"><?=$water_tank?> l.</td>
											</tr>
											<tr>
												<td valign="top" class="formsCaption"><strong>
													<?=$c_flag_id?>:</strong></td>
												<td class="forms"><?=$flag_id?></td>
											</tr>
											<tr>
												<td valign="top" class="formsCaption"><strong>
													<?=$c_hour_use?>:</strong></td>
												<td class="forms"><?=$hour_use?></td>
											</tr>
											<tr>
												<td valign="top" class="formsCaption"><strong>
													<?=$c_price?>:</strong></td>
												<td class="forms"><?=$price?> </td>
											</tr>
											</tr>
											<tr>
												<td valign="top" class="formsCaption"><strong>
													<?=$c_changeable?>:</strong></td>
												<td class="forms"><?=$changeable?> </td>
											</tr>
											<tr>
												<td class="formsCaptionHor2"><?=$c_measures_weight?>:</td>
												<td height="1" class="formsCaptionHor2"><img src="/admin/themes/inmotools/images/spacer.gif" width="1" height="1" /></td>
											</tr>
											<tr>
												<td valign="top" class="formsCaption"><strong>
													<?=$c_measures?>:</strong></td>
												<td class="forms">
													<table border="0" cellspacing="0" cellpadding="0">
														<tr>
															<td><?=$c_length?></td>
															<td><?=$c_breadth?></td>
															<td><?=$c_depth?></td>
															<td>&nbsp;</td>
														</tr>
														<tr>
															<td><?=$length?>
															&nbsp;&nbsp;</td>
															<td><?=$breadth?>
																&nbsp;&nbsp;</td>
															<td><?=$depth?>
																&nbsp;-&nbsp;</td>
															<td><?=$depth2?> m.</td>
														</tr>
													</table></td>
											</tr>
											<tr>
												<td class="formsCaption"><strong>
													<?=$c_weight?>:</strong></td>
												<td class="forms"><?=$weight?> <?=$c_weight_units?></td>
											</tr>
					</table>
						<?if($show_electronics):?>
						<table width="100%" cellpadding="0" cellspacing="0" class="formstbl">
											<tr>
												<td class="formsCaptionHor2"><?=$c_characteristics_electronic?></td>
											</tr>
											<tr>
												<td><table border="0" cellspacing="12" cellpadding="0">
													<tr>
														<td align="right"><?=$c_vhf?></td>
														<td style="padding-right:20px;"><?=$vhf?></td>
														<td align="right"><?=$c_depthsounder?> </td>
														<td style="padding-right:20px;"><?=$depthsounder?> </td>
														<td align="right"><?=$c_speedlog?> </td>
														<td style="padding-right:20px;"><?=$speedlog?></td>
														<td align="right"><?=$c_radar?> </td>
														<td><?=$radar?> </td>
													</tr>
													<tr>
														<td align="right"><?=$c_gps?></td>
														<td><?=$gps?></td>
														<td align="right"><?=$c_plotter?></td>
														<td><?=$plotter?></td>
														<td align="right"><?=$c_anemometer?></td>
														<td><?=$anemometer?></td>
														<td align="right"><?=$c_autopilot?></td>
														<td><?=$autopilot?></td>
													</tr>
													<tr>
														<td align="right"><?=$c_music_equipment?></td>
														<td><?=$music_equipment?></td>
														<td align="right"><?=$c_television?></td>
														<td><?=$television?></td>
														<td align="right"><?=$c_compass?></td>
														<td><?=$compass?></td>
														<td align="right">&nbsp;</td>
														<td>&nbsp;</td>
													</tr>
												</table></td>
											</tr>
									 </table>
						<?endif //show_electronics?>
						<?if($show_extras):?>
						 <table width="100%" cellpadding="0" cellspacing="0" class="formstbl">
											<tr>
												<td class="formsCaptionHor2"><?=$c_characteristics_extra?></td>
											</tr>
											<tr>
												<td><table border="0" cellspacing="12" cellpadding="0">
													<tr>
														<td align="right"><?=$c_trimmtabs?></td>
														<td style="padding-right:20px;"><?=$trimmtabs?></td>
														<td align="right"><?=$c_searchlight?></td>
														<td style="padding-right:20px;"><?=$searchlight?></td>
														<td align="right"><?=$c_electrical_capstan?></td>
														<td style="padding-right:20px;"><?=$electrical_capstan?></td>
														<td align="right"><?=$c_gangway?></td>
														<td><?=$gangway?></td>
													</tr>
													<tr>
														<td align="right"><?=$c_liferaft?></td>
														<td><?=$liferaft?></td>
														<td align="right"><?=$c_tender?></td>
														<td><?=$tender?></td>
														<td align="right"><?=$c_attached_engine?></td>
														<td><?=$attached_engine?></td>
														<td align="right"><?=$c_offshore_connexion?></td>
														<td><?=$offshore_connexion?></td>
													</tr>
													<tr>
														<td align="right"><?=$c_fridge?></td>
														<td><?=$fridge?></td>
														<td align="right"><?=$c_battery_charger?></td>
														<td><?=$battery_charger?></td>
														<td align="right"><?=$c_generator?></td>
														<td><?=$generator?></td>
														<td align="right"><?=$c_cover?></td>
														<td><?=$cover?></td>
													</tr>
													<tr>
														<td align="right"><?=$c_camping_cover?></td>
														<td><?=$camping_cover?></td>
														<td align="right"><?=$c_bimini_top?></td>
														<td><?=$bimini_top?></td>
														<td align="right"><?=$c_bowthruster?></td>
														<td><?=$bowthruster?></td>
														<td align="right"><?=$c_hot_water?></td>
														<td><?=$hot_water?></td>
													</tr>
													<tr>
														<td align="right"><?=$c_davit?></td>
														<td><?=$davit?></td>
														<td align="right"><?=$c_heating?></td>
														<td><?=$heating?></td>
														<td align="right"><?=$c_air_conditioning?></td>
														<td><?=$air_conditioning?></td>
														<td align="right"><?=$c_teka?></td>
														<td><?=$teka?></td>
													</tr>
													<tr>
														<td align="right"><?=$c_wc?></td>
														<td><?=$wc?></td>
														<td align="right"><?=$c_black_water?></td>
														<td><?=$black_water?></td>
														<td align="right"><?=$c_bath_stair?></td>
														<td><?=$bath_stair?></td>
														<td align="right"><?=$c_trim?></td>
														<td><?=$trim?></td>
													</tr>
													<tr>
														<td align="right"><?=$c_kitchen?></td>
														<td><?=$kitchen?></td>
														<td align="right"><?=$c_microwave?></td>
														<td><?=$microwave?></td>
														<td align="right"><?=$c_shower?></td>
														<td><?=$shower?></td>
														<td align="right">&nbsp;</td>
														<td>&nbsp;</td>
														<td align="right">&nbsp;</td>
														<td>&nbsp;</td>
													</tr>
												</table></td>
											</tr>
									 </table>
						<?endif //show_extras?>
						<?if($show_sails):?>
						 <table width="100%" cellpadding="0" cellspacing="0" class="formstbl">
											<tr>
												<td class="formsCaptionHor2"><?=$c_characteristics_sail?></td>
											</tr>
											<tr>
												<td><table border="0" cellspacing="12" cellpadding="0">
													<tr>
														<td align="right"><?=$c_mainsail?></td>
														<td><?=$mainsail?></td>
														<td align="right"><?=$c_genoa?></td>
														<td><?=$genoa?></td>
														<td align="right"><?=$c_mizzen?></td>
														<td><?=$mizzen?></td>
														<td align="right"><?=$c_jib?></td>
														<td><?=$jib?></td>
													 </tr>
													<tr>
														<td align="right"><?=$c_stormsail?></td>
														<td><?=$stormsail?></td>
														<td align="right"><?=$c_forsail2?></td>
														<td><?=$forsail2?></td>
														<td align="right"><?=$c_spinaker?></td>
														<td><?=$spinaker?></td>
														<td align="right"><?=$c_spi_boom?></td>
														<td><?=$spi_boom?></td>
													 </tr>
													<tr>
														<td align="right"><?=$c_roller_mainsail?></td>
														<td><?=$roller_mainsail?></td>
														<td align="right"><?=$c_roller_genoa?></td>
														<td><?=$roller_genoa?></td>
														<td align="right">&nbsp;</td>
														<td>&nbsp;</td>
														<td align="right">&nbsp;</td>
														<td>&nbsp;</td>
													</tr>
												</table></td>
											</tr>
									</table>
									<?endif //show_sail?>
									<table width="100%" cellpadding="0" cellspacing="0" class="formstbl">
											<tr>
												<td class="formsCaptionHor2"><?=$c_equipament?></td>
											</tr>
											<?foreach($equipament as $l): extract ($l)?>
											<tr>
												<td class="formsHor"><?=$lang?>
													<br />
													<?=$equipament?></td>
											</tr>
											<?endforeach //$equipament?>
									</table>
											
											
					
					<table width="100%" cellpadding="0" cellspacing="0">
						<tr> 
							<td class="formCaptionTitle"><strong><?=$c_seo?></strong></td>
						</tr>
					</table>
					<table width="100%" cellpadding="0" cellspacing="0">
						<tr> 
							<td class="formsCaptionHor"><?=$c_page_title?></td>
						<tr> 
					</table>
					<table width="100%" cellpadding="0" cellspacing="0" class="formstbl">
						<?foreach($page_title as $l): extract ($l)?>
						<tr> 
							<td class="formsHor"><?=$lang?><br><?=$page_title?>
							</td>
						</tr>
						<?endforeach //$page_title?>
					</table>
					<table width="100%" cellpadding="0" cellspacing="0">
						<tr> 
							<td class="formsCaptionHor"><?=$c_page_description?></td>
						<tr> 
					</table>
					<table width="100%" cellpadding="0" cellspacing="0" class="formstbl">
						<?foreach($page_description as $l): extract ($l)?>
						<tr> 
							<td class="formsHor"><?=$lang?><br><?=$page_description?>
							</td>
						</tr>
						<?endforeach //$page_Description?>
					</table>
					<table width="100%" cellpadding="0" cellspacing="0">
						<tr> 
							<td class="formsCaptionHor"><?=$c_page_keywords?></td>
						<tr> 
					</table>
					<table width="100%" cellpadding="0" cellspacing="0" class="formstbl">
						<?foreach($page_keywords as $l): extract ($l)?>
						<tr> 
							<td class="formsHor"><?=$lang?><br><?=$page_keywords?>
							</td>
						</tr>
						<?endforeach //$page_keywords?>
					</table>	
					<table width="100%" cellpadding="0" cellspacing="0">
						<tr> 
							<td class="formsCaptionHor"><?=$c_embarcacion_file_name?></td>
						<tr> 
					</table>
					<table width="100%" cellpadding="0" cellspacing="0" class="formstbl">
						<?foreach($embarcacion_file_name as $l): extract ($l)?>
						<tr> 
							<td class="formsHor"><?=$lang?><br><?=$embarcacion_file_name?>
							</td>
						</tr>
						<?endforeach //$embarcacion_file_name?>
					</table>
					<table width="100%" cellpadding="0" cellspacing="0">
						<tr> 
							<td class="formsCaptionHor"><?=$c_embarcacion_old_file_name?></td>
						<tr> 
					</table>
					</table>
					<table width="100%" cellpadding="0" cellspacing="0" class="formstbl">
						<?foreach($embarcacion_old_file_name as $l): extract ($l)?>
						<tr> 
							<td class="formsHor"><?=$lang?><br><?=$embarcacion_old_file_name?>
							</td>
						</tr>
						<?endforeach //$embarcacion_old_file_name?>
					</table>
											
											
											
<?if ($form_new):?>
							<table width="100%" cellpadding="4" cellspacing="2" class="formsButtonstbl">
									<tr>
										<td align="center" class="formsButtons"><input type="submit" name="Submit" value="<?=$c_send_new?>" class="btn_form1"> 
									</td>
								</tr>
							</table>
							<?endif //form_new?><?if ($form_edit):?>
							<table width="100%" cellpadding="4" cellspacing="2" class="formsButtonstbl">
									<tr>
										<td align="center" class="formsButtons"><div class="centerFloat1">
												<div class="centerFloat2"><input type="submit" name="Submit" value="<?=$c_send_edit?>" class="btn_form1"></div></div></td>
								</tr>
							</table>
							<?endif //form_edit?></td>
	</tr>
</table></form>
