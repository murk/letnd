<?

/* 3-11-16

	PRODUCT - Transportista


 */

if ( is_table( 'product__product' ) ) {

	if ( ! is_table( 'product__courier' ) ) {

		$update_sql [] = "
CREATE TABLE IF NOT EXISTS `product__courier` (
  `courier_id` INT(11) NOT NULL AUTO_INCREMENT,
  `courier` VARCHAR(255) COLLATE utf8_spanish2_ci NOT NULL,
  `courier_url` VARCHAR(150) COLLATE utf8_spanish2_ci NOT NULL,
  `bin` TINYINT(2) NOT NULL DEFAULT '0',
  PRIMARY KEY  (`courier_id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;
";
		$update_sql [] = "
INSERT INTO `product__courier` (`courier_id`, `courier`, `courier_url`, `bin`) VALUES
	(1, 'TNT', 'www.tnt.com', 0),
	(2, 'Seur', 'www.seur.com', 0),
	(3, 'FedEx', 'www.fedex.com', 0),
	(4, 'DHL', 'www.dhl.es', 0),
	(5, 'UPS', 'www.ups.com', 0),
	(6, 'Correos', 'www.correos.es', 0),
	(7, 'Envialia', 'www.envialia.com', 0),
	(8, 'Correos express', 'www.correosexpress.com', 0);
";

		$update_sql [] = "INSERT INTO `all__menu` (`menu_id`, `menu_group`, `tool`, `tool_section`, `action`, `parent_id`, `toolmode_id`, `variable`, `ordre`, `link`, `process`, `bin`, `read1`, `write1`) VALUES (20141, 'customer', 'product', 'courier', '', 20100, 1, 'CUSTOMER_MENU_COURIER', 4, '', '', 0, ',1,2,3,', ',1,2,');";

		$update_sql [] = "INSERT INTO `all__menu` (`menu_id`, `menu_group`, `tool`, `tool_section`, `action`, `parent_id`, `toolmode_id`, `variable`, `ordre`, `link`, `process`, `bin`, `read1`, `write1`) VALUES (20142, 'customer', 'product', 'courier', 'show_form_new', 20141, 1, 'CUSTOMER_MENU_COURIER_NEW', 1, '', '', 0, ',1,2,3,', ',1,2,');";

		$update_sql [] = "INSERT INTO `all__menu` (`menu_id`, `menu_group`, `tool`, `tool_section`, `action`, `parent_id`, `toolmode_id`, `variable`, `ordre`, `link`, `process`, `bin`, `read1`, `write1`) VALUES (20144, 'customer', 'product', 'courier', 'list_records', 20141, 1, 'CUSTOMER_MENU_COURIER_LIST', 2, '', '', 0, ',1,2,3,', ',1,2,');";
		$update_sql [] = "UPDATE `all__menu` SET `ordre`='5' WHERE  `menu_id`=20121;";


	} else {
		trigger_error( 'Ja existeixen la taula product__courier' );
	}

}

?>