<div class="search">
	<form id="search-advanced" name="search_advanced" method="get" action="/admin/" class="forms-sep">
		<table border="0" cellspacing="0" cellpadding="0" class="search-top">
			<tr valign="middle">
				<td id="view-format-holder">
					<a data-value="all" onclick="ReservaSearch.set_view_format('all')"><?= $c_view_format_all ?></a>
					<a data-value="order" onclick="ReservaSearch.set_view_format('order')"><?= $c_view_format_order ?></a>
					<a data-value="orderitems" onclick="ReservaSearch.set_view_format('orderitems')"><?= $c_view_format_orderitems ?></a>
					<input name="view_format" id="view-format-hidden" type="hidden" value="<?= $view_format ?>" checked="checked">
				</td>
				<td><?= $c_by_words ?></td>
				<td>
					<input name="action" type="hidden" value="<?= $action ?>">
					<input name="menu_id" type="hidden" value="<?= $menu_id ?>">
					<input class="alone" name="q" type="text" value="<?= $q ?>"></td>
				<td class="padding-left-2">
					<?= $c_by_ref ?>
				</td>
			</tr>
		</table>
		<table class="search-top vertical-caption" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td>
					<div><?= $c_product_id ?></div>
					<input class="alone" id="product_id_add" name="product_id_add" type="text" value="<?= $product_id_add ?>">
					<input id="product_id" name="product_id" type="hidden" value="<?= $product_id ?>">
				</td>
				<td>
					<div><?= $c_user_id ?></div>
					<input class="alone" id="user_id_add" name="user_id_add" type="text" value="<?= $user_id_add ?>">
					<input id="user_id" name="user_id" type="hidden" value="<?= $user_id ?>">
				</td>
				<td>
					<div><?= $c_centre_id ?></div>
					<input class="alone" id="centre_id_add" name="centre_id_add" type="text" value="<?= $centre_id_add ?>">
					<input id="centre_id" name="centre_id" type="hidden" value="<?= $centre_id ?>">
				</td>
				<td>
					<div><?= $c_order_status ?></div>
					<?= $order_status_select ?>
				</td>
			</tr>
		</table>
		<table class="search-top vertical-caption" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td>
					<div><?= $c_dates_from ?></div>
					<input autocomplete="off" class="alone" id="date_from" name="date_from" type="text" value="<?= $date_from ?>">
				</td>
				<td>
					<div><?= $c_dates_to ?></div>
					<input autocomplete="off" class="alone" id="date_to" name="date_to" type="text" value="<?= $date_to ?>">
				</td>
					<td>
						<div><?= $c_payment_method ?></div>
						<?= $payment_method_select ?>
					</td>
					<td>
						<div><?= $c_operator_id ?></div>
						<?= $operator_id_select ?>
					</td>
			</tr>
		</table>
		<table class="vertical-caption" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td>
					<div><?= $c_date_search_caption ?></div>
					<?= $date_search_select ?>
				</td>
				<td>
					<div><?= $c_is_done_caption ?></div>
					<?= $is_done_select ?>
				</td>
				<td>
					<div><?= $c_source_caption ?></div>
					<?= $source_select ?>
				</td>
				<td>
					<div><?= $c_has_no_timetable ?></div>
					<?= $has_no_timetable_select ?>
				</td>
				<td class="buttons-td bottom"><input type="submit" name="advanced" value="ok" class="boto-search submit"></td>
				<td class="buttons-td bottom"><input type="button" id="reset-search" name="reset" value="Reset" class="boto-search reset"></td>
				<td class="buttons-td bottom"><input type="submit" name="excel" value="Excel" class="boto-search excel"></td>
			</tr>
		</table>
	</form>
</div>

<script language="JavaScript">

	ReservaSearch.view_format = '<?= $view_format ?>';
	ReservaSearch.init();


</script>