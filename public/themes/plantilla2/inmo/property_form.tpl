<?if (!1):?><link href="/public/themes/plantilla2/styles/general.css" rel="stylesheet" type="text/css" /><br /><?endif //$$?>
<script type="text/javascript">
ref = '<?=$c_ref?> <?=$ref?>';
</script>
<? if ($property):?>
<div class="property">      
  <?=$property?>
</div>
<? endif //property?>
<? if ($property_title):?>
<div class="property_title">
  <?=$property_title?>
</div>
<? endif //property_title?>
<? if ($description):?>
<div class="description2">
  <?=$description?>
</div>
<? endif //description?>
<table border="0" cellspacing="0" cellpadding="2" class="details1">
<? if ($floor_space):?>
        <tr>
          <td class="details1"><strong><?=$c_floor_space?>:</strong></td>
          <td class="details1"><?=$floor_space?> m2</td>
        </tr>
        <? endif //floor_space?>
      <? if ($land):?> 
        <tr>
          <td class="details1"><strong><?=$c_land?>:</strong></td>
          <td class="details1"><?=$land?> <?=$land_units?></td>
        </tr>
      <? endif //land?> 
        <? if ($category_id):?>
        <tr>
          <td class="details1"><strong><?=$c_category_id?>:</strong></td>
          <td class="details1"><?=$category_id?></td>
        </tr>
      <? endif //category_id?>
      <? if ($comarca_id):?>
        <tr>
          <td class="details1"><strong><?=$c_zone?>:</strong>&nbsp;</td>
          <td class="details1"><?=$comarca_id?>,
            <?=$provincia_id?> (<?=$country_id?>)</td>
        </tr>
      <? endif //comarca_id?>
</table>
<div style="padding-bottom:20px"></div>
  <? if ($price):?>
    <div class="price">
    <?=$c_price?>:
    <span><?=$price?>
    <?=CURRENCY_NAME?></span>
  </div>
  <? endif //price?>
  <? if ($price_consult):?>
    <div class="price">
    <span><?=$price_consult?></span>
    </div>
  <? endif //price_consult?>
  <? if ($status):?>
    <div class="sold">
    <?=$status?>
    </div>
  <? endif //status?>
<? if ($details):?>
<div style="padding-bottom:10px"></div>
    <div><img src="/public/themes/plantilla2/images/details_top.gif" width="450" height="9" /></div>
<table width="450" cellpadding="4" cellspacing="0" class="details2">
           <? $con = 0?><?php foreach($details as $d):?><?php if($$d):?>
               <?php if($con==0):?><tr><?php endif //$conn?>
                  <td align="left" nowrap="nowrap" class="details2"><strong><?=${'c_'.$d}?>:</strong></td>
                 <td width="50%" align="left" class="details2"><?=$$d?></td>
               <?php if ($con==0):$con=1?><?php else:$con=0?></tr><?php endif //$con?><?php endif //$$d?><?php endforeach //details?>
  </table>
<div><img src="/public/themes/plantilla2/images/details_bottom.gif" width="450" height="9" /></div>
<? endif //$details?>
