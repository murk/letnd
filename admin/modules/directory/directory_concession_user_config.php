<?php

	// Aquest arxiu es genera automaticament
	// Qualsevol canvi que es faci es podrà sobrescriure involuntariament desde l'eina de gestió
	// Achtung!
	$gl_file_protected = true;
	$gl_db_classes_fields_included = array (
  'group_id' => '1',
  'user_id' => '1',
  'name' => '1',
  'surname' => '1',
  'email' => '1',
  'login' => '1',
  'passw' => '1',
);
	$gl_db_classes_language_fields = array (
  0 => '',
);
	$gl_db_classes_fields = array (
  'user_id' =>
  array (
    'type' => 'hidden',
    'enabled' => '1',
    'form_admin' => 'input',
    'form_public' => 'text',
    'list_admin' => 'text',
    'list_public' => 'text',
    'order' => '0',
    'default_value' => '',
    'override_save_value' => '',
    'class' => '',
    'javascript' => '',
    'text_size' => '',
    'text_maxlength' => '',
    'textarea_cols' => '',
    'textarea_rows' => '',
    'select_caption' => '',
    'select_size' => '',
    'select_table' => '',
    'select_fields' => '',
    'select_condition' => '',
  ),
  'name' =>
  array (
    'type' => 'text',
    'enabled' => '1',
    'form_admin' => 'input',
    'form_public' => 'text',
    'list_admin' => 'text',
    'list_public' => 'text',
    'required' => '1',
    'order' => '1',
    'default_value' => '',
    'override_save_value' => '',
    'class' => '',
    'javascript' => '',
    'text_size' => '60',
    'text_maxlength' => '100',
    'textarea_cols' => '',
    'textarea_rows' => '',
    'select_caption' => '',
    'select_size' => '',
    'select_table' => '',
    'select_fields' => '',
    'select_condition' => '',
  ),
  'surname' =>
  array (
    'type' => 'text',
    'enabled' => '1',
    'form_admin' => 'input',
    'form_public' => 'text',
    'list_admin' => 'text',
    'list_public' => 'text',
    'order' => '2',
    'default_value' => '',
    'override_save_value' => '',
    'class' => '',
    'javascript' => '',
    'text_size' => '60',
    'text_maxlength' => '100',
    'textarea_cols' => '',
    'textarea_rows' => '',
    'select_caption' => '',
    'select_size' => '',
    'select_table' => '',
    'select_fields' => '1,0',
    'select_condition' => '',
  ),
  'group_id' =>
  array (
    'type' => 'select',
    'enabled' => '1',
    'form_admin' => 'input',
    'form_public' => 'text',
    'list_admin' => 'text',
    'list_public' => 'text',
    'order' => '0',
    'default_value' => '',
    'override_save_value' => '',
    'class' => '',
    'javascript' => '',
    'text_size' => '',
    'text_maxlength' => '',
    'textarea_cols' => '',
    'textarea_rows' => '',
    'select_caption' => '',
    'select_size' => '',
    'select_table' => 'user__group',
    'select_fields' => 'user__group.group_id,name',
    'select_condition' => '',
  ),
);
	$gl_db_classes_list = array (
  'cols' => '2',
);
	$gl_db_classes_form = array (
  'cols' => '1',
  'images_title' => '',
);
	?>