<?

/* 3-10-14 
  ACtualitzacions inmo booking

 */


if (is_table('booking__propertyrange') && !is_field('booking__propertyrange', 'price_week_type')) {
	$update_sql []="
		ALTER TABLE `booking__propertyrange`
			ADD COLUMN `price_week_type` ENUM('night','week') NOT NULL DEFAULT 'week' AFTER `price_week`;";

}
elseif (is_table('booking__propertyrange')){
	trigger_error('Ja existeixen el camp price_week_type a booking__propertyrange');
}


if (is_table('booking__book')) {
	$update_sql []="
		ALTER TABLE `booking__book`
	CHANGE COLUMN `entered` `book_entered` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `phone`;";

}


if (is_table('booking__book_to_custumer')) {
	$update_sql []="
		ALTER TABLE `booking__book_to_custumer`
	CHANGE COLUMN `ordre` `ordre` INT(11) NULL DEFAULT '1' AFTER `custumer_id`;";

}

if (is_table('booking__extra')) {
	$update_sql []="
		ALTER TABLE `booking__extra`
	ADD COLUMN `has_quantity` TINYINT(1) NULL DEFAULT '0' AFTER `extra_type`;";

}

if (is_table('booking__extra_to_book')) {
	$update_sql []="
		ALTER TABLE `booking__extra_to_book`
	ADD COLUMN `quantity` INT(11) NOT NULL DEFAULT '0' AFTER `extra_price`;";

}

if (is_table('print__template') && (!is_field('print__template', 'orientation'))) {
	$update_sql []="
		ALTER TABLE `print__template`
	ADD COLUMN `orientation` ENUM('P','L') NOT NULL DEFAULT 'P' AFTER `size`;";

}
elseif (is_table('print__template')){	
	trigger_error('No ha pogut canviar orientation a print__template');
}

if (is_table('inmo__property')) {
	$update_sql []="
		ALTER TABLE `inmo__property`
	ADD COLUMN `sea_view` TINYINT(1) NOT NULL DEFAULT '0' AFTER `courtyard_area`,
	ADD COLUMN `clear_view` TINYINT(1) NOT NULL DEFAULT '0' AFTER `sea_view`;";

}


	$update_sql []="
		UPDATE `all__menu` SET `action`='list_records_book_search', `variable`='BOOKING_MENU_BOOK_SEARCH', `ordre`=1 WHERE  `menu_id`=153;";

	$update_sql []="
		UPDATE `all__menu` SET `ordre`=2 WHERE  `menu_id`=151;";

	$update_sql []="
		UPDATE `all__menu` SET `ordre`=3 WHERE  `menu_id`=152;";

	$update_sql []="
		UPDATE `all__menu` SET `tool`='inmo', `tool_section`='property' WHERE  `menu_id`=153;";


?>