<?php
// menus eina
if (!defined('CLINK_MENU_NEW')){
	define('CLINK_MENU_NEW', 'Noticias');
	define('CLINK_MENU_NEW_NEW', 'Entrar nueva noticia');
	define('CLINK_MENU_NEW_LIST', 'Listar i editar noticias');
	define('CLINK_MENU_NEW_BIN', 'Papelera de reciclaje');

	define('CLINK_MENU_MEDIO', 'Medios');
	define('CLINK_MENU_MEDIO_NEW', 'Entrar nuevo medio');
	define('CLINK_MENU_MEDIO_LIST', 'Listar i editar medios');
	define('CLINK_MENU_MEDIO_BIN', 'Papelera de reciclaje');

	define('CLINK_MENU_ENCUENTRO', 'Encuentros');
	define('CLINK_MENU_ENCUENTRO_NEW', 'Entrar nuevo encuentro');
	define('CLINK_MENU_ENCUENTRO_LIST', 'Listar i editar  encuentros');
	define('CLINK_MENU_ENCUENTRO_BIN', 'Papelera de reciclaje');

	define('CLINK_MENU_CURRICULUM', 'Currículums');
	define('CLINK_MENU_CURRICULUM_NEW', 'Entrar nuevo currículum');
	define('CLINK_MENU_CURRICULUM_LIST', 'Listar i editar currículums');
	define('CLINK_MENU_CURRICULUM_BIN', 'Papelera de reciclaje');
}

$gl_messages_new['no_category']='Para poder insertar noticias tiene que haber almenos una categoría';

// captions seccio
$gl_caption_new['c_prepare'] = 'En preparación';
$gl_caption_new['c_review'] = 'Para revisar';
$gl_caption_new['c_public'] = 'Pública';
$gl_caption_new['c_archived'] = 'Archivada';
$gl_caption_new['c_new'] = 'Noticia';
$gl_caption_new['c_title'] = 'Título';
$gl_caption_new['c_subtitle'] = 'Subtítulo';
$gl_caption_new['c_content'] = 'Contenido';
$gl_caption_new['c_entered'] = 'Fecha';
$gl_caption_new['c_status'] = 'Estado';
$gl_caption_new['c_category_id'] = 'Categoría';
$gl_caption_new['c_filter_category_id'] = 'Todas las categorías';
$gl_caption_new['c_filter_status'] = 'Todas los estados';
$gl_caption_new['c_ordre'] = 'Ordre';
$gl_caption_new['c_source'] = 'Font';

$gl_caption_category['c_category'] = 'Categoría';
$gl_caption_category['c_new_button'] = 'Ver noticias';
$gl_caption_category['c_ordre'] = 'Ordre';
$gl_caption_image['c_name'] = 'Nombre';
$gl_caption_new['c_url1_name']='Nombre URL(1)';
$gl_caption_new['c_url1']='URL(1)';
$gl_caption_new['c_url2_name']='Nombre URL(2)';
$gl_caption_new['c_url2']='URL(2)';
$gl_caption_new['c_videoframe']='Vídeo (youtube, metacafe...)';
$gl_caption_new['c_file']='Archivos';


$gl_caption_new['c_volver']='Retour a actualité';
$gl_caption_medio['c_volver']='Retour a CliNK dans les médias';
$gl_caption_encuentro['c_volver']='Retour a Développement du project';
$gl_caption_new['c_read_more']='Lire la Suite';
$gl_caption_new['c_title']='Actualité et Publications';
$gl_caption_medio['c_title']='CliNK dans les médias';
?>
