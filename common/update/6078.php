<?

/* 31-5-16

	Productes, afegir preus contra reembors

 */
if ( is_table( 'product__rate' ) ) {

	$update_sql [] = "ALTER TABLE `product__rate`
		ADD COLUMN `rate_encrease_ondelivery` DECIMAL(8,2) NULL DEFAULT NULL AFTER `rate`;";
}

if ( is_table( 'product__configadmin' ) && ! Db::get_first( "SELECT * FROM product__configadmin WHERE name='send_rate_encrease_ondelivery_world'" ) ) {
	$update_sql[] = "
		INSERT INTO product__configadmin (`name`, `value`) VALUES ('send_rate_encrease_ondelivery_world', '0');";

} else {
	if ( is_table( 'product__configadmin' ) ) {
		trigger_error( 'Ja existeixen el camp send_rate_encrease_ondelivery_world' );
	}
}



?>