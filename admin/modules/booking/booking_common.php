<?
/**
 * BookingCommon
 * 
 * Funcions comuns per booking tant a public com admin
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) Maig 2014
 * @version $Id$
 * @access public
 */
class BookingCommon{
	
	public $is_mail = false;
	public $is_contract = false;
	public $is_editable = true;
	public $reserved_message = '';
	public $messages;
	public $caption;
	public static $deleted_results;
	
	function __construct(){
		
	}
	
	/*
	 * 
	 *  Seasons per una reserva concreta
	 * 
	 * Plantilla: booking/property_extras_list
	 * 
	 * Deixo en una plantilla apart ja que no entra en el js del submit
	 * 
	 * 
	 */	
	
	static function get_propertyrange_records(&$module, $property_id, $is_admin = false){

		$listing = new ListRecords($module);
		$listing->paginate = false;
		$listing->add_swap_edit();
		$listing->has_bin = false;
		$listing->set_field('start_day', 'type', 'select');	
		
		if(!$listing->is_editable){
			$listing->set_field('whole_week', 'type', 'select');		
		}
		
		// per defecte nomes mostra d'avui en endavant
		//$listing->condition = "date_start >= DATE_FORMAT(NOW() ,'%Y-%m-01')";
		// Ho he canviat perque mostri la temporada si la data final encara es major al dia d'avui
		$listing->condition = "date_end >= DATE_FORMAT(NOW() ,'%Y-%m-01')";

		// per llistat desde la fitxa de l'inmoble
		if ($property_id){
			
			$listing->add_button ('edit_button', '' , 'boto1', 'auto','booking.show_form_edit');
			$listing->set_options(1, 1, 1, 1, 1, 1, 0, 0);
			
			$listing->condition .= ' AND property_id = ' . $property_id;
			$listing->form_link = "/admin/?tool=booking&tool_section=propertyrange&process=save_property_rows&property_id=" . $property_id;
		};
		
		$listing->call('records_walk','season_id',true);
		
		$listing->order_by = 'date_start ASC';
		
		$module->set_var('back_button', $listing->last_listing['link']);

		// torno false si hi ha resultats a la fitxa de l'inmoble
		if ($property_id ) {

	        $listing->list_records(false);
			if ( $listing->results ) {
				return $listing->parse_template();
			}
			else {
				return false;
			}

		}
		else {
	        return $listing->list_records();
		}
		
	}

	static function propertyrange_records_walk(&$listing, $is_admin = false) {

		$rs = &$listing->rs;
		
		if ($is_admin) {			
			$ret['color'] = '<div class="season-list season'.$rs['season_id'].'"></div>';
		}
		else{
			$ret['color'] = Db::get_first("
					SELECT color 
						FROM booking__season, booking__color 
						WHERE booking__season.color_id = booking__color.color_id
						AND season_id = " . $rs['season_id'] . "
			");

			$ret['price_weekend_original_value'] = $rs['price_weekend'];
			$ret['price_week_original_value'] = $rs['price_week'];

			if ( (double) $rs['price_weekend'] == 0 ) {
				$ret['price_weekend'] = $rs['price_night'];
			}

			// Si no hi ha preu setmana, el posem igual que preu nit i el tipus "per nit"
			if ((double)$rs['price_week']== 0){
				$ret['price_week'] = $rs['price_night'];
				$ret['price_week_type'] = 'night';
			}
			
		}
		return $ret;
	}
	
	// Obtinc array dels colors utilitzats a temporades
	static public function get_seasons_colors() {
		$results = Db::get_rows("
			SELECT booking__season.season_id AS season_id, booking__color.color_id AS color_id, color
				FROM booking__season, booking__season_language, booking__color
				WHERE booking__season.season_id = booking__season_language.season_id
				AND booking__season.color_id = booking__color.color_id
				AND language = '".LANGUAGE."'
				ORDER BY ordre ASC, season ASC, season_id ASC");
		
		$ret = array();
		
		foreach ($results as $rs) {
			$ret[$rs['season_id']] = $rs['color'];
		}
		
		return $ret;
	}
	
	/*
	 * 
	 *  Extres per una reserva concreta
	 * 
	 * Plantilla: booking/property_extras_list
	 * 
	 * Deixo en una plantilla apart ja que no entra en el js del submit
	 * 
	 * 
	 */	
	
	function get_book_extras(&$module, $book_id, $property_id, $get_array = false){

		$listing = new ListRecords($module);

		// Mail i contract no son mai editables
		if ($this->is_mail || $this->is_contract)
			$this->is_editable = false;

		$listing->set_options(1, 0, 0, 1, 0, 0, 0, 0, 0, 0);
		$listing->set_field('extra_type', 'list_admin', 'text');
		$listing->set_field('extra', 'list_admin', 'text');
		$listing->set_field('has_quantity', 'type', 'none');
		$listing->paginate = false;			

		//$listing->call('records_walk_book', '', true);

		$listing->add_field('extra_price', array(
					'type'=> 'currency',
					'list_public'=> 'text',
					'list_admin'=> 'text',
					'change_field_name' => '(SELECT extra_price FROM booking__extra_to_property WHERE extra_id =  booking__extra.extra_id AND property_id = ' . $property_id . ') as extra_price'
				)
			);
		
		$listing->add_field('extra_price_book', array(
					'type'=> 'currency',
					'list_public'=> '0',
					'list_admin'=> '0',
					'change_field_name' => '(SELECT extra_price FROM booking__extra_to_book WHERE extra_id =  booking__extra.extra_id AND book_id = ' . $book_id . ') as extra_price_book'
				)
			);

		$listing->add_field('extra_book_input', array(
					'type'=> 'none',
					'list_public'=> 'out',
					'list_admin'=> 'out'
				)
			);

		$listing->add_field('quantity', array(
					'type'=> 'none',
					'list_admin'=> '0',
					'change_field_name' => '(SELECT quantity FROM booking__extra_to_book WHERE extra_id =  booking__extra.extra_id AND book_id = ' . $book_id . ') as quantity'
				)
			);
		
		if ($this->is_mail){
			$listing->template = 'mail_property_extras';
		}	
		/*elseif ($this->is_contract){
			$listing->tpl->path = PATH_TEMPLATES_PUBLIC;
			$listing->template = 'contract_property_extras';
		}	*/		
		else{
			$listing->template = 'booking/book_extras_list';
		}
			
		$listing->join = 'INNER JOIN booking__extra_to_property USING (extra_id)';
		
		$listing->condition = 'property_id = ' . $property_id;			
		
		$module->set_listing_commons($listing);
		
		$listing->set_records();
		
		$vars = array('optionals' => array(),'requireds' => array(),'includeds' => array(),'optionals_no_deposit' => array(),'requireds_no_deposit' => array(),'includeds_no_deposit' => array());

		foreach($listing->loop as &$rs){
			
			$this->get_book_extras_walk($rs);

			// mail nomes els seleccionats
			// contracte nomes els seleccionats
			// !is_editable nomes els seleccionats
			// els altres es llisten tots
			if (
				(!$this->is_editable && $rs['is_selected']) ||
				($this->is_editable)
			) {
				
				if (empty($vars[$rs['extra_type_value'].'s'])){
					$vars[$rs['extra_type_value']] = $rs['extra_type'];
				}

				$vars[$rs['extra_type_value'].'s'][] = $rs;

				// deposit exclòs
				if ($rs['concept_value'] != 'deposit') {
					$vars[ $rs['extra_type_value'] . 's_no_deposit' ][] = $rs;
				}
				
			}
			
		}
		$this->_get_book_extras_type_totals( $vars );
		$this->_get_book_extras_concept_totals($listing->loop, $vars );

		// Debug::p( $vars );

		if ($this->is_contract || $get_array) return $vars;

		$listing->set_vars($vars);

		if ($listing->has_results) {return $listing->process();} else {return '';}
		
	}

	/**
	 * @param $vars
	 *
	 */
	private function _get_book_extras_type_totals( &$vars ) {
		// sumo els extres de cada part
		foreach ( $vars as $key => $val ) {
			// nomes si es array  optionals,'requireds','includeds
			if ( is_array( $val ) ) {
				$temp_price = 0;
				if ( $val ) {
					foreach ( $val as $v ) {
						$temp_price += $v['extra_total_price_value'];
					}
				}
				// TODO-i price_extras_optionals_no_deposi -> està malament el nom de la variable
				// Debug::p( 'price_extras_' . substr($key,0,-1) );
				$vars[ 'price_extras_' . substr($key,0,-1) . '_value' ] = $temp_price;
				$vars[ 'price_extras_' . substr($key,0,-1) ]       = format_currency( $temp_price );
			}
		}
	}

	/**
	 * @param $loop
	 * @param $vars
	 */
	private function _get_book_extras_concept_totals( $loop, &$vars ) {
		// sumo els extres de cada part
		$cleaning = 0;
		$deposit = 0;
		$normal = 0;

		foreach ( $loop as $rs ) {
			if ($rs['is_selected']) {
				if ($rs['concept_value'] == 'cleaning') $cleaning += $rs['extra_total_price_value'];
				if ($rs['concept_value'] == 'deposit') $deposit += $rs['extra_total_price_value'];
				if ($rs['concept_value'] == 'normal') $normal += $rs['extra_total_price_value'];
			}
		}
		$vars[ 'price_extras_cleaning_value' ] = $cleaning;
		$vars[ 'price_extras_cleaning' ] = format_currency($cleaning);
		$vars[ 'price_extras_deposit_value' ]       = $deposit;
		$vars[ 'price_extras_deposit' ]       = format_currency($deposit);
		$vars[ 'price_extras_normal_value' ]       = $normal;
		$vars[ 'price_extras_normal' ]       = format_currency($normal);
	}

	
	function get_book_extras_walk(&$rs) {
		
		
		$rs['is_selected'] = false;
		// si està guardat, agafo el preu guardat
		if (!is_null($rs['extra_price_book'])){
			$rs['extra_price'] = format_currency($rs['extra_price_book']);
			$rs['extra_price_value'] = $rs['extra_price_book'];
			$rs['is_selected'] = true;
		}

		$extra_type = $rs['extra_type_value'];
		$quantity = $rs['quantity'];

		// M'asseguro que els included no tenen preu
		if ( $extra_type == 'included' ) {
			$rs['extra_price'] = format_currency(0);
			$rs['extra_price_value'] = '0';
		}

		// Si no es editable trec els inputs de quantitat i el checkbox, de totes formes hi ha la variable $quantity que s'hauria de combinar amb !$is_editable per quan no es editable ja que igualment s'ha de cambiar la presentació
		
		if ($extra_type == 'optional'){
			
			$checked = is_null($rs['extra_price_book'])?'':' checked="checked"';		
			$rs['extra_book_input'] = '<input data-price="' . $rs['extra_price_value'] . '" type="checkbox" value="' . $rs['extra_id'] . '" name="extra_book_selected[' . $rs['extra_id'] . ']"'.$checked.'>';
			if (!$this->is_editable)
				$rs['extra_book_input'] = '';
			
		}
		// obligatoris i inclosos sempre es guarden
		else{
			$rs['hidden'] .= '<input data-price="' . $rs['extra_price_value'] . '" type="hidden" value="' . $rs['extra_id'] . '" name="extra_book_selected[' . $rs['extra_id'] . ']">';
			$rs['extra_book_input'] = false;
		}
		
		if ($rs['has_quantity']) {
			$options = '';
			for ($i = 0; $i<21; $i++){
				$selected = $quantity==$i?' selected':'';
				$options .= '<option'.$selected.' value = "' . $i . '">' . $i . '</option>';
			}
			$rs['hidden'] .= '<input data-price="' . $rs['extra_price_value'] . '" type="hidden" value="' . $rs['extra_id'] . '" name="extra_book_selected[' . $rs['extra_id'] . ']">';
			$rs['extra_book_input'] = '<select  name="quantity[' . $rs['extra_id'] . ']">' . $options . '</select>';
			if (!$this->is_editable){
				$rs['extra_book_input'] = $quantity?' x ' . $quantity:'';}
		}
		
		$rs['extra_total_price_value'] = $rs['has_quantity']?$rs['quantity']*$rs['extra_price_value']:$rs['extra_price_value']; // per presentar el mail
		$rs['extra_total_price'] = format_currency($rs['extra_total_price_value']);

		$rs['hidden'] .= '<input type="hidden" name="extra_price['.$rs['id'].']" value="'.$rs['extra_price'].'">';
		//Debug::p($rs['extra_total_price'], 'text');
	}

	/**
	 * @param $contract_id Si no hiha contract_id, agafa el últim contracte, així els registres nous sempre tenen l'últim contracte
	 *
	 * @return string
	 */
	public function get_initial_percent( $contract_id = false ) {
		// TODO-i Al posar codi promocional, canviar l'últim pagament segons convingui
		if ( $contract_id ) {
			return Db::get_row( "SELECT initial_percent, contract_id, whole_import FROM booking__contract WHERE contract_id = " . $contract_id );
		} else {
			return Db::get_row( "SELECT initial_percent, contract_id, whole_import FROM booking__contract ORDER BY contract_id DESC LIMIT 1" );
		}
	}

	/**
	 * @param $property_id
	 * @param $original_price
	 * @param bool $get_minimum
	 * @param bool $update_price_tmp
	 *
	 * @return array
	 *
	 *
	 * * Obtinc el preu d'un immoble de lloguer temporal oel llistat
	 *
	 * Preu segons el dia que som
	 *		si hi ha temporada establerta, agafo el preu que toca
	 *		si no hi ha temporada, busco el preu mes baix
	 *      si no hi ha cap d'aquestes, preu a fitxa inmoble
	 *
	 */
	static public function get_property_price($property_id, $original_price, $get_minimum = false, $update_price_tmp = false){
		
		Main::load_class('booking', 'price', 'admin');
		$prices = New BookingPrice();
		$nights = false;
		
		
		if (isset($_GET['date_in']) && isset($_GET['date_out'])) {
			
			$date_in = unformat_date(R::escape('date_in'));
			$date_out = unformat_date(R::escape('date_out'));
			
		}
		else{	
			
			$date_in =date("Y-m-d");

			$date_out = new DateTime('tomorrow');
			$date_out =  $date_out->format('Y-m-d');

			$nights = 1;
			
		}
		

		$prices_arr = 
				$prices->get_price(
						$date_in,
						$date_out, 
						false, 
						$property_id,
						false,
						false);

				
        if ($prices_arr['price_book_value']) {

			if ($get_minimum) $prices_arr['price_book_value'] = $prices->get_minimum();

	        if ($update_price_tmp)
		        self::update_price_tmp( $property_id,  $prices_arr['price_book_value'] );


			$ret = [
				'price' => $prices_arr['price_book_value'],
				'total_nights' => $prices_arr['total_nights']
			];

	        return $ret;
		}
		
		
		// preu més baix establert a temporades
		$price = Db::get_first("
			SELECT MIN(price_night) 
			FROM booking__propertyrange WHERE property_id = '" . $property_id . "' 
			AND price_night != 0
			AND date_start >= DATE_FORMAT(NOW() ,'%Y-%m-01')");
		
		// si nomes es poden fer setmanes senceres
		if (!$price){
			$price_rs = Db::get_row("
			SELECT price_week, price_week_type
			FROM booking__propertyrange WHERE property_id = '" . $property_id . "' 
			AND price_week != 0
			AND date_start >= DATE_FORMAT(NOW() ,'%Y-%m-01')
			ORDER BY price_week ASC
			LIMIT 1");

			if ($price_rs){

				$price = $price_rs['price_week'];
				if ($price_rs['price_week_type'] == 'week') $price = $price/7;

				// calculo que val, es orientatiu ja que no es pot llogar sino es setmana complerta
				if (!$get_minimum) {
					$nights = $nights?$nights:count( $prices->details );
					$price = $price * $nights;
				}
			}
		}
		$price = $price?$price:$original_price;

		if ($update_price_tmp)
			self::update_price_tmp( $property_id, $price );

		$ret = [
			'price' => $price,
			'total_nights' => $nights
		];

		return $ret;
	}

	/**
	 * @param $property_id
	 * @param $price
	 *
	 */
	private static function update_price_tmp( $property_id, $price ) {

		// Grabo el preu en la bbdd en un camp de preu_temporal, per poder ordenar be
		Db::execute( "UPDATE inmo__property SET price_tmp=" . $price . " WHERE property_id=" . $property_id . ";" );

	}
	
	
	/*
	 * 
	 * Formulari de reserva - Variables comuns a admin i common
	 * 
	 * quan vinc de llistat disponibilitat, marco dades per defecte
	 * i marco persones per defecte
	 * 
	 */
	static function set_book_vars(&$module,$property_id, $has_calendar = true) {
		
		if ($has_calendar){
			$GLOBALS['gl_page']->javascript .= "
				booking.init();
				booking.property_id = " . $property_id . ";
				booking.is_season_calendar = 0;			
			";

			$date_in = R::escape('date_in');
			$module->set_var('date_in', $date_in);

			if ($date_in) {
				$date = unformat_date($date_in);
				$date = explode('/', $date);
				//$module->set_var('date_in_day', (int)$date[2]);
				//$module->set_var('date_in_month', $date[1]);
				//$module->set_var('date_in_year', $date[0]);
				$GLOBALS['gl_page']->javascript .= "
					booking.do_action(".(int)$date[2].",".$date[1].",". $date[0] .",'b');";
			}		
			$date_out = R::escape('date_out');
			$module->set_var('date_out', $date_out);

			if ($date_out) {
				$date = unformat_date($date_out);
				$date = explode('/', $date);
				//$module->set_var('date_out_day', (int)$date[2]);
				//$module->set_var('date_out_month', $date[1]);
				//$module->set_var('date_out_year', $date[0]);

				$GLOBALS['gl_page']->javascript .= "booking.do_action(".(int)$date[2].",".$date[1].",". $date[0] .",'a');";
			}
		}
		
		$adult = R::escape('adult');
		if ($adult) { 
			$module->set_field('adult','default_value',$adult);			
		}
		
		
		$child = R::escape('child');
		if ($child) { 
			$module->set_field('child','default_value',$child);			
		}
		
		
		$baby = R::escape('baby');
		if ($baby) { 
			$module->set_field('baby','default_value',$baby);			
		}
		
	}

	// Si s'està guardant una pre-reserva o reserva cancelada,
	// es pot guardar les mateixes dates originals encara que estigui reservada ( si no es podria guardar mai )
	function ignore_is_reserved( $date_in, $date_out, $book_id, $status, $action ) {
		if ( $action == "save_record" && ( $status == 'pre_booked' || $status == 'cancelled' ) ) {


		$is_equal = Db::get_first("SELECT count(*)
							FROM booking__book
							WHERE '$date_in' = date_in
							AND '$date_out' = date_out
							AND book_id = '$book_id'");


			return $is_equal?true:false;
		}
		else {
			return false;
		}
	}

	function check_dates_are_reserved($date_in, $date_out, $book_id, $property_id) {

		$query   = "SELECT *
							FROM booking__book
							WHERE property_id = $property_id
							AND '$date_in' < date_out
							AND '$date_out' > date_in
							AND book_id <> '$book_id'
							AND ( status = 'booked' || status = 'confirmed_email' || status = 'confirmed_post' || status = 'contract_signed')
							ORDER BY date_in";
		$results = Db::get_rows( $query );

		return $results;
	}
	
	function check_is_reserved($date_in, $date_out, $date_in2, $date_out2, $book_id, $property_id, $adult, $children, $baby, $new_line) {
		
		$this->reserved_message = '';
		$total_days = 1; // un més per contar dies, sino son nits
		
		$property_rs = Db::get_row("SELECT property_private, person FROM inmo__property WHERE property_id = $property_id");
		$property = $property_rs['property_private'];
		$person = $property_rs['person'];

		$results = $this->check_dates_are_reserved($date_in, $date_out, $book_id, $property_id);

		if ( $results ) {

			$this->reserved_message = sprintf( $this->messages['full2'], ucfirst( $property ), $date_in2, $date_out2 );
			$nl2                    = $new_line == '\n' ? '\n' : '';
			$this->reserved_message .= $new_line . $new_line . $nl2 . $this->messages['full3'] . $new_line . $nl2;
			foreach ( $results as $rs ) {

				$this->reserved_message .= format_date_list( $rs['date_in'] ) . ' - ' . format_date_list( $rs['date_out'] ) . $nl2;
			}

		}
		
		
		// comprovo que la reserva no es faci en cap dia que no estigui marcat a temporades
		// torna a fer tot el blucle mateix a get_price
		
		$begin = new DateTime($date_in);
		$end = new DateTime($date_out);

		$interval = DateInterval::createFromDateString('1 day');
		$period = new DatePeriod($begin, $interval, $end);

		// fa un bucle des primer a l'ultim -1
		foreach ( $period as $dt ){
			
			$date      = $dt->format( "Y-m-d" );
			$query     = "
				SELECT count(*)
					FROM booking__propertyrange
					WHERE '$date' >= date_start 
					AND '$date' < date_end
					AND property_id = $property_id";
			$has_range = Db::get_first( $query );
			
			if (!$has_range){
				$this->reserved_message .= $this->messages['has_no_range'] .  $new_line;
				break;
			}

			$total_days ++;
			
		}
		
		// comprovo el numero de persones
		$total_person = (int)$adult + (int)$children;
		
		if ($person<$total_person){
			$this->reserved_message .= $this->messages['too_many_person'] .  $new_line;			
		}
	
		debug::add('person',$person);
		debug::add('$total_person',$total_person);
		
		debug::add('adult',$adult);
		debug::add('children',$children);

		debug::add('total days',$total_days);
		debug::p('total days',$total_days);

		// Comprovo que no passi de 31 dies
		if ($total_days > 31) {
			$this->reserved_message .= $this->messages['exceeds_31_days'] .  $new_line;
		}
		
		return $this->reserved_message?true:false;
	}
	
	
	/*
	 * 
	 * Obtinc resta de persones per formulari de reserva
	 * 
	 * 
	 * 
	 */
	function get_rest_of_custumers(&$rs, $custumer_id, $action, $is_admin = false) {
		
		$custumers = array();
		$book_id = !empty($rs['book_id'])?$rs['book_id']:0;
			
		$add_new_link_1 = $is_admin?
				'javascript:bookingform.add_new_custumer('
				:
				'/' . LANGUAGE . '/custumer/custumer/new-custumer/ordre/';
			
		$add_new_link_2 = $is_admin?
				')'
				:
				'/book_id/'.$book_id.'/?return-url=' . urlencode($_SERVER['REQUEST_URI']);
		
		// a admin obtinc també el primer client
		if ($is_admin) {
			
			if ($custumer_id){
				$cu = Db::get_row("
					SELECT name, surname1, surname2, custumer__custumer.custumer_id as custumer_id FROM custumer__custumer
					WHERE custumer_id = ". $custumer_id
				);
			
				$select = '<input value="'.$custumer_id.'" type="hidden" name="selected_custumer_id[1]" id="selected_custumer_id_1">' . $cu['name'] . ' ' . $cu['surname1'] . ' ' . $cu['surname2'];
			}
			else{
				$select = '<input value="0" type="hidden" name="selected_custumer_id[1]" id="selected_custumer_id_1">';				
			}
			
			$custumers[] = array(
				'custumer_select' => $select,
				'selected_id' => $custumer_id?$custumer_id:0,
				'add_new_custumer_link' =>$add_new_link_1.'1'.$add_new_link_2,
				'person' => $this->caption['c_adult_singular'],
				'num' => 1,
				'ordre' => 1
			);
			
		}
		
		$total_custumers = !empty($rs)?$rs['adult'] + $rs['child']+ $rs['baby']:1;
		// resta de custumers
		if ($action != 'show_form_new' && $action != 'get_form_new' && $total_custumers>1) {
			
			
			$book_group = $custumer_id;
			
			// select custumers
			
			$custumers_query = '';			
			if (!empty($rs['custumers'])){
				// per ajax afegeixo els custumers que s'han entrat nous o triat
				$custumers_query = 'OR custumer_id IN (' . implode(',', $rs['custumers']) . ')';
			}
			
			$results = array();
			if ($book_group){
				$results = Db::get_rows("
					SELECT name, surname1, surname2, custumer__custumer.custumer_id as custumer_id FROM custumer__custumer
					WHERE (book_group = ". $book_group . "
					AND custumer_id <> ". $book_group . ")" .
					$custumers_query	
				);
			}
			
			for ($i=2; $i<=$total_custumers ; $i++){
				
				$select = '';
				$add_new_custumer_link = $add_new_link_1.$i.$add_new_link_2;
				//		'/' . LANGUAGE . '/custumer/custumer/new-custumer/ordre/' . $i . '/book_id/' . $book_id . '/?return-url=' . urlencode($_SERVER['REQUEST_URI']);
				
				$selected_id  = false;
				if ($results){
					$select = '<select name="selected_custumer_id[' . $i . ']" id="selected_custumer_id_' . $i . '">
						<option value="0">';
					
					// si es ajax pot vindre seleccionat sense estar guardat
					$selected_id  = 
						isset($rs['custumers'][$i])?
							$rs['custumers'][$i]:
							Db::get_first("
								SELECT custumer_id 
								FROM booking__book_to_custumer 
								WHERE ordre = " . $i . "
								AND book_id = " . $book_id);
					
					
					foreach	($results as $cu){
						
						$selected = $cu['custumer_id']==$selected_id?' selected':'';
						
						$select .= '<option' . $selected .' value="' . $cu['custumer_id'] . '">' . $cu['name'] . ' ' . $cu['surname1'] . ' ' . $cu['surname2'] . '</option>';
						
						
					
					}
					
					$select .= '</select>';
				}
				
				// si es adult, nen o bebe
				if($i<=$rs['adult']){
					$person = $this->caption['c_adult_singular'];
					$num = $i;
				}
				elseif ($i<=($rs['adult']+$rs['child'])){
					$person = $this->caption['c_child_singular'];
					$num = $i - $rs['adult'];					
				}
				else{
					$person = $this->caption['c_baby_singular'];	
					$num = $i - $rs['adult'] - $rs['child'];									
				}
				
				// a admin si no he seleccionat un primer client no puc seleccionar els altres, ja que no els puc relacionar
				if ($is_admin && !$custumer_id) {
					$add_new_custumer_link = false;
				}
				
				$custumers[] = array(
					'custumer_select' => $select,
					'selected_id' => $selected_id,
					'add_new_custumer_link' => $add_new_custumer_link,
					'person' => $person,
					'num' => $num,
					'ordre' => $i
				);
			}
		}
		
		// Si es admin i ja s'ha escollit un segon custumer, ja no es pot canviar de adult1
		if ($is_admin && $custumer_id && !empty($results)) {
			$custumers[0]['add_new_custumer_link'] = false;
		}
		
		//Debug::p($custumers, '$custumers');
		return $custumers;
	}
	static function get_main_custumer($book_id) {
		
		$custumer_id = Db::get_first("
			SELECT custumer_id
				FROM custumer__custumer 
				WHERE custumer_id 
					IN (SELECT custumer_id 
							FROM booking__book_to_custumer 
							WHERE book_id = ".$book_id.")
					AND book_main = 1");
		return $custumer_id;
		
	}
	static function get_owner($property_id) {

		$custumer_id = Db::get_first("
			SELECT custumer_id
				FROM custumer__custumer
				WHERE custumer_id
					IN (SELECT custumer_id
							FROM custumer__custumer_to_property
							WHERE property_id = ".$property_id."
							)
			    LIMIT 1");
		return $custumer_id;

	}
	function get_contract(&$module, $book_id = false, $return_string=false) {

		ini_set('max_execution_time', 300);
		// Locals
		Debug::p( 'Language code', LANGUAGE_CODE );
		switch(strtolower(LANGUAGE_CODE)):
			case "es":
				$locale_code = "es_ES";
			break;
			case "ca":
				$locale_code = "ca_ES";
			break;
			case "en":
				$locale_code = "en_EN";
			break;
			case "fr":
				$locale_code = "fr_FR";
			break;
			case "deu":
				$locale_code = "de_DE";
			break;
			default:
				$locale_code = "es_ES";
			break;
		endswitch;

		$locale = setlocale(LC_TIME,$locale_code);

		$contract_format = $module->config['contract_format'];

		// Reserva
		if (!$book_id)
			$book_id = R::id('book_id');
		
		$book = Db::get_row("SELECT property_id, date_in, date_out, book_entered, adult, child, baby, promcode_discount, price_book, price_extras, price_total, contract_id FROM booking__book WHERE book_id = " . $book_id);

		$tpl = new phemplate(PATH_TEMPLATES_PUBLIC);
		$tpl->set_file('booking/book_contract_' . $book['contract_id'] . '_' .LANGUAGE . '.tpl');

		// Preus
		Main::load_class('booking', 'price', 'admin');
		$prices_mod = New BookingPrice();
		$prices = $prices_mod->get_price(
						'',
						'', 
						$book_id, 
						$book['property_id']
					);
		// Extres
		$module_extras = Module::load('booking', 'extra', $module);
		$module_extras->is_contract = true;
		$extras = $module_extras->get_book_extras($book_id, $book['property_id']);

		Debug::p($extras, 'Extras');
		$tpl->set_vars ($extras);

		// Dates
		$book['total_persons'] = $book['adult'] + $book['child'] + $book['baby'];
		$book['child_baby'] = $book['child'] + $book['baby'];

		$book['total_nights'] = $prices_mod->get_number_nights($book['date_in'], $book['date_out']);
		$book['total_weeks'] = format_decimal($book['total_nights']/7);


		$date = strtotime ($book['date_in']);
		$book['date_in_day'] = strftime('%e', $date);
		$book['date_in_month'] = utf8_encode( strftime( '%B', $date ) );
		$book['date_in_year'] = strftime('%Y', $date);

		$book['date_in'] = format_date_form($book['date_in']);


		$date = strtotime ($book['date_out']);
		$book['date_out_day'] = strftime('%e', $date);
		$book['date_out_month'] = utf8_encode( strftime( '%B', $date ) );
		$book['date_out_year'] = strftime('%Y', $date);

		$book['date_out'] = format_date_form($book['date_out']);

		Debug::p( $book );
		
		$tpl->set_var('book_id',$book_id);
		$tpl->set_vars($this->caption);
		$tpl->set_vars($book);
		$tpl->set_vars($prices);

		Debug::p( $prices, 'prices' );
		Debug::p( [
					[ 'prices contract', $prices ],
				], 'bookPrice');

		$initial_rs = $this->get_initial_percent($book['contract_id']);
		$initial_percent = $initial_rs['initial_percent'];
		$whole_import = $initial_rs['whole_import'];

		if ($whole_import == '0') {
			$price_total_initial = round( $prices['price_book_promcode_discounted_value'] * $initial_percent / 100, 2 );
		}
		else {
			$price_total_initial = round( $prices['price_total_value'] * $initial_percent / 100, 2 );
		}

		$price_book_final = $prices['price_book_promcode_discounted_value'] - $price_total_initial;
		$price_total_final = $prices['price_total_value']-$price_total_initial;
		$price_total_no_deposit = $prices['price_total_value']-$extras['price_extras_deposit_value'];
		$price_total_final_no_deposit = $price_total_final-$extras['price_extras_deposit_value'];

		Debug::p($price_total_initial, '$price_total_initial');
		Debug::p($price_total_final, '$price_total_final');
		Debug::p($price_total_no_deposit, '$price_total_no_deposit');
		Debug::p($price_total_final_no_deposit, '$price_total_final_no_deposit');

		$tpl->set_var('price_total_initial_value',$price_total_initial);
		$tpl->set_var('price_total_initial',format_currency($price_total_initial));

		$tpl->set_var('price_book_final_value',$price_book_final);
		$tpl->set_var('price_book_final',format_currency($price_book_final));

		$tpl->set_var('price_total_final_value',$price_total_final);
		$tpl->set_var('price_total_final',format_currency($price_total_final));

		$tpl->set_var('price_total_no_deposit_value',$price_total_no_deposit);
		$tpl->set_var('price_total_no_deposit',format_currency($price_total_no_deposit));

		$tpl->set_var('price_total_final_no_deposit_value',$price_total_final_no_deposit);
		$tpl->set_var('price_total_final_no_deposit',format_currency($price_total_final_no_deposit));

		$tpl->set_var('contract_initial_percent',$initial_percent);

		// Llogater
		$this->get_contract_custumer( $module, $book_id, $tpl );
		
		// Immoble
		$rs = Db::get_row("SELECT ref, property_private, adress, numstreet, block, flat, door, zip, municipi_id, comarca_id, country_id, ref_cadastre, hut, category_id, room, person, time_in, time_out, managed_by_owner
							FROM inmo__property
							WHERE property_id = " . $book['property_id']);
		
		$rs += Db::get_row("SELECT description, property, property_title
							FROM inmo__property_language
							WHERE language = '".LANGUAGE."' 
							AND property_id = " . $book['property_id']);

		$rs['municipi_id_value'] = $rs['municipi_id'];
		$rs['municipi_id'] = Db::get_first("SELECT municipi FROM inmo__municipi WHERE municipi_id = " . $rs['municipi_id']);
		$rs['comarca_id_value'] = $rs['comarca_id'];
		$rs['comarca_id'] = Db::get_first("SELECT comarca FROM inmo__comarca WHERE comarca_id = " . $rs['comarca_id']);
		$rs['country_id_value'] = $rs['country_id'];
		$rs['country_id'] = Db::get_first("SELECT country FROM inmo__country_language WHERE language = '".LANGUAGE."' AND country_id = " . $rs['country_id']);
		$rs['category_id_value'] = $rs['category_id'];
		$rs['category_id'] = Db::get_first("SELECT category FROM inmo__category_language WHERE language = '".LANGUAGE."' AND category_id = " . $rs['category_id']);
		
		$rs['time_in_value'] = $rs['time_in'];
		$rs['time_in'] = format_time($rs['time_in']);
		$rs['time_out_value'] = $rs['time_out'];
		$rs['time_out'] = format_time($rs['time_out']);

		$full_adress = '';
		if ( $rs['adress']) $full_adress .= $rs['adress'];
		if ( $rs['numstreet']) $full_adress .= ', ' .  $rs['numstreet'];
		
		if ($rs['block']) $full_adress .= ', ' .  $module->caption['c_block'] . $rs['block'];
		if ($rs['flat']!='' || $rs['door']) $full_adress .= ', ' .  $rs['flat'] . '-' . $rs['door'];

		$rs['description_value'] = $rs['description'];
		$rs['description'] = nl2br(htmlspecialchars($rs['description']));

		$tpl->set_var ('property_full_adress', $full_adress);


		$property_managed_by_owner = $rs['managed_by_owner'];

		foreach ($rs as $k=>$v){
			$tpl->set_var ('property_' . $k, $v);
		}

		// Propietari
		$has_owner = $this->get_contract_custumer( $module, $book_id, $tpl, $book['property_id'] );
		/*		if (!$has_owner) {
			Debug::p( 'No hi ha propietari a la casa i no es pot fer el contracte' );
			return array();
		}*/

		// dia
		$time = strtotime ($book['book_entered']);
		$tpl->set_var('day', strftime('%e',$time));
		$tpl->set_var( 'month', utf8_encode( strftime( '%B', $time ) ) );
		$tpl->set_var('year', strftime('%Y',$time));

		if ($contract_format == 'pdf')
		{
			// process
			$content = $tpl->process();
			$file_serve_name = $this->caption['c_book_id'] . '_' . $book_id . '.pdf';

			include (DOCUMENT_ROOT . 'admin/modules/booking/booking_contract_start.php');
			$logo = CLIENT_PATH . '/images/logo_showwindow.png';
			$pdf = new PDF('P', 'pt', 'A4', true, 'UTF-8', false);
			$pdf->logo = $logo;
			$pdf->setCellHeightRatio(1.5);

			include (PATH_TEMPLATES_PUBLIC . 'booking/book_contract_pdf.tpl');
			include (DOCUMENT_ROOT . 'admin/modules/print/print_showwindow_tpl_end.php');

		}
		// per crear despres un pdf, http://pandoc.org/MANUAL.html
		elseif ( $contract_format == 'docx' ) {
			$file_serve_name = $this->caption['c_book_id'] . '_' . $book_id . '.docx';

			include_once( DOCUMENT_ROOT . 'common/includes/word/templates/opentbs/tbs_class.php' );
			include_once( DOCUMENT_ROOT . 'common/includes/word/templates/opentbs/plugins/tbs_plugin_opentbs.php' );

			/*include_once( DOCUMENT_ROOT . 'common/includes/word/html-to-word/vsword/vsword/VsWord.php' );
			VsWord::autoLoad();
			$doc    = new VsWord();
			$parser = new HtmlParser( $doc );
			$content = "<b>SSSSSSSSSSSSSSSSSSSSSS</b><u>NNNNNNNNNNNNNNNNNNNNNN</u>";
			$parser->parse( $content );

			$content = $doc->getDocument()->getBodyContent();

			include_once( DOCUMENT_ROOT . 'common/includes/word/html-to-word/HTMLtoOpenXML/HTMLtoOpenXML.php' );
			$content = HTMLtoOpenXML::getInstance()->fromHTML($content);
			//Debug::p($content);die();
			*/

			$tbs = new clsTinyButStrong;
			$tbs->Plugin( TBS_INSTALL, OPENTBS_PLUGIN );

			$tbs->NoErr = false; //! DEBUG;
			$word_template = PATH_TEMPLATES_PUBLIC . 'booking/book_contract_' . $book['contract_id'] . '_' .LANGUAGE . '.docx';

			// Load the template to TBS
			$tbs->LoadTemplate( $word_template, OPENTBS_ALREADY_UTF8 );

			$word_data = array(
				$tpl->vars
				/*array(
					'book_id' => $book_id,
				)*/
			);

			//Debug::p( $tpl->vars);

			// @see http://www.tinybutstrong.com/manual.php#php_mergeblock
			$tbs->MergeBlock( 'b', $word_data  );

			$tbs->Show(OPENTBS_DOWNLOAD, $file_serve_name);

			// Això escriu al servidor: $tbs->Show( OPENTBS_FILE, $file_serve_name );
		}

		if ( $return_string ) {
			return array( 'string' => $return_string, 'name' => $file_serve_name );
		} else {
			die();
		}
		
	}

	/**
	 * @param $module
	 * @param $book_id
	 * @param $tpl
	 *
	 * @return bool
	 */
	private function get_contract_custumer( &$module, $book_id, &$tpl, $property_id = false  ) {

		if ( $property_id ) {
			$custumer_id = BookingCommon::get_owner( $property_id );
			$prefix = 'owner_';
		} else {
			$custumer_id = BookingCommon::get_main_custumer( $book_id );
			$prefix = 'custumer_';
		}
		if ( !$custumer_id ) {
			$GLOBALS['gl_message'] = $this->messages['has_no_owner'];
			return false;
		}

		$module         = Module::load( 'custumer', 'custumer', '', false );
		$module->action = 'show_record';
		$show           = new ShowForm( $module, $custumer_id );
		$show->set_record();

		$rs        = $show->rs;
		$full_name = $rs['name'];
		if ( $rs['surname1'] )
			$full_name .= ' ' . $rs['surname1'];
		if ( $rs['surname2'] )
			$full_name .= ' ' . $rs['surname2'];

		$full_adress = '';
		if ( $rs['adress'] )
			$full_adress .= $rs['adress'];
		if ( $rs['numstreet'] )
			$full_adress .= ', ' . $rs['numstreet'];

		if ( $rs['block'] )
			$full_adress .= ', ' . $module->caption['c_block'] . $rs['block'];
		if ( $rs['flat'] != '' || $rs['door'] )
			$full_adress .= ', ' . $rs['flat'] . '-' . $rs['door'];

		$tpl->set_var( $prefix . 'full_adress', $full_adress );
		$tpl->set_var( $prefix . 'full_name', $full_name );


		foreach ( $rs as $k => $v ) {
			$tpl->set_var( $prefix . $k, $v );
		}

		return true;
	}

	function send_mail($body, $subject, $to, $config) {

		$tpl = new phemplate(PATH_TEMPLATES_PUBLIC);
		$tpl->set_vars($this->caption);
		$tpl->set_file('mail.tpl');
		$tpl->set_vars(array(
			'preview'=>'',
			'book'=>'',
			'custumer'=>'',
			'content'=>$body,
			'url' => HOST_URL,
			'default_mail' => DEFAULT_MAIL,
			'page_slogan' => $config['page_slogan'],
			'short_host_url' => substr(HOST_URL,7,-1)
		));

		$body = $tpl->process();

		$mailer = get_mailer($config);
		if (is_file(CLIENT_PATH . 'images/logo_newsletter.jpg'))
			$mailer->AddEmbeddedImage(CLIENT_PATH . 'images/logo_newsletter.jpg', 'logo', 'logo.jpg');
		elseif(is_file(CLIENT_PATH . 'images/logo_newsletter.gif'))
			$mailer->AddEmbeddedImage(CLIENT_PATH . 'images/logo_newsletter.gif', 'logo', 'logo.gif');
		$mailer->Body = $body;
		$mailer->Subject = $subject;
		$mailer->IsHTML(true);
		$mailer->AddAddress ($to);

		return send_mail($mailer, false);

	}
	public static function set_portal_on_save( &$writerec ) {

		include_once( DOCUMENT_ROOT . 'admin/modules/admintotal/export_common.php' );
		$export = New ExportCommon;

		if ( ! $export->can_export() ) return;

		// No deixo borrar cap de fora
		$notify_ids = [];
		if ( $writerec->action == 'save_rows_delete_selected' ) {
			foreach ( $writerec->id as $k => $id ) {
				$query      = "SELECT book_owner FROM booking__book WHERE book_id = $id";
				$book_owner = Db::get_first( $query );
				if ( $book_owner != 'letnd' ) {
					unset( $writerec->id[ $k ] );
					$notify_ids [] = $id;
				}
			}
			foreach ( $writerec->post["selected"] as $k => $id ) {
				$query      = "SELECT book_owner FROM booking__book WHERE book_id = $id";
				$book_owner = Db::get_first( $query );
				if ( $book_owner != 'letnd' ) {
					unset( $writerec->post["selected"][ $k ] );
					$notify_ids [] = $id;
				}
			}
		}

		// No es borra mai desde la fitxa -> if ( $writerec->action = 'delete_record')

		// Event on_save
		$writerec->on_save (
			function ($ids, $action, $deleted_results, &$save_rows) use ($export, $notify_ids) {
				// BookingCommon::export_to_portal($ids, $action, $deleted_results, $export); Aquí no em serveix perque faig tota una serie d'accions al guardar, com ara actualitzar preus
				BookingCommon::$deleted_results = $deleted_results;

			}, 'book_owner,vreasy_id'
		);

		return $notify_ids;

	}
	public static function export_to_portal( $ids, $action ) {

		$export = New ExportCommon;

		$export->agencia_id = $_SESSION['client_id'];
		if ( $ids ) $export->ids = $ids;
		if ( BookingCommon::$deleted_results ) $export->deleted_results = BookingCommon::$deleted_results;

		$export->export_async( $action, 'book' );

	}
}