<?php
/* 6-9-18
Missatge personalitzat al fer el login a botiga

*/

if ( is_table( 'product__configpublic' ) && ! Db::get_first( "SELECT * FROM product__configpublic WHERE name='show_javascript_message_on_login'" ) ) {
	$update_sql[] = "
		INSERT INTO product__configpublic (`name`, `value`) VALUES ('show_javascript_message_on_login', '0');";

}
else {
	if ( is_table( 'product__configpublic' ) ) {
		trigger_error( 'Ja existeixen el camp show_javascript_message_on_login' );
	}
}