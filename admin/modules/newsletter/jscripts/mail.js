$(function () {

	$('#send_to_same_language_only_' + gl_current_form_id).click(
		function () {
			if ($(this).is(":checked")) {
				$('#send_to_no_language_too_holder').show();
			}
			else {
				$('#send_to_no_language_too_holder').hide();
			}
		}
	);

});



function preview_mail(formu) {
  var m='';
  var oInput;
  var message_id = document.forms[1].message_id_javascript.value;
  //a = window.open('','prev','scrollbars=yes,width=750,height=500');
  showPopWin('about:blank', 80, 80, null, true, true, '', true, '', '%','%');
  for (x=0;x<formu.elements.length; x++)
	{  
      if (document.forms[1].elements[x].type != 'submit') 
      {  
		if (document.forms[1].elements[x].type == 'radio' && !document.forms[1].elements[x].checked) {
		}else{
        oInput=document.createElement("input");
        oInput.type='hidden';
        oInput.name=formu.elements[x].name;
        oInput.value=formu.elements[x].value;
		
		if (oInput.name == 'message['+message_id+']'){
			oInput.value=tinyMCE.get('message_' + message_id).getContent();
		}
        document.previewForm.appendChild(oInput);
		}
      }
  
	  //m+=formu.elements[x].name +": "+formu.elements[x].value+"\n";
	}
  document.previewForm.submit();
  return;
  

  if (a)
  {
  old_target = formu.target;
  old_action = formu.action;
  formu.target = 'prev';
  formu.action = '/admin/newsletter/?action=preview_mail&menu_id=5&template=0';
  formu.submit(); 
  formu.target = old_target;
  formu.action = old_action;
  }

}

function configuration_select(iselect, value)
{
  for (x=0;x<iselect.options.length;x++)
  {
  	if(('val' + iselect.options[x].value) == value) iselect.selectedIndex = x;
  }
}


function select_addresses() { 
  gl_submit_addresses = false;
  gl_default_input_prefix = 'selected_addresses'; 
  showPopWin('/admin/?action=list_records_select_frames&menu_id=11', 950, 530, null, true, false, '');
}
function select_properties() {
  gl_submit_addresses = false; 
  gl_default_input_prefix = 'selected_inmo_property';
  showPopWin('/admin/?action=list_records_select_frames_property&menu_id=5002', 950, 530, null, true, false, '');
}
function select_products() {
  gl_submit_addresses = false; 
  gl_default_input_prefix = 'selected_product_product';
  showPopWin('/admin/?action=select_frames&tool=product&tool_section=product&menu_id=20004', 850, 530, null, true, false, '');
}
function select_news() {
  gl_submit_addresses = false; 
  gl_default_input_prefix = 'selected_news_new';
  showPopWin('/admin/?action=select_frames&too>l=news&tool_section=new&menu_id=3003', 850, 530, null, true, false, '');
}
function select_agendas() {
  gl_submit_addresses = false; 
  gl_default_input_prefix = 'selected_llemena_agenda';
  showPopWin('/admin/?action=select_frames&tool=llemena&tool_section=agenda&menu_id=2303', 850, 530, null, true, false, '');
}
function select_activitats() {
  gl_submit_addresses = false;
  gl_default_input_prefix = 'selected_lassdive_activitat';
  showPopWin('/admin/?action=select_frames&tool=lassdive&tool_section=activitat&menu_id=2203', 850, 530, null, true, false, '');
}
