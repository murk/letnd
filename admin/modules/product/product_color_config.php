<?php
	// Aquest arxiu es genera automaticament
	// Qualsevol canvi que es faci es podrà sobrescriure involuntariament desde l'eina de gestió
	// Achtung!
	$gl_file_protected = false;
	$gl_db_classes_fields_included = array (
  'color_id' => '1',
  'color_name' => '1',
  'observations' => '1',
  'bin' => '1',
);
	$gl_db_classes_language_fields = array (
  0 => 'color_name',
);
	$gl_db_classes_fields = [
  'color_id' => 
  [
    'type' => 'hidden',
    'enabled' => '1',
    'form_admin' => '0',
    'form_public' => '0',
    'list_admin' => '0',
    'list_public' => '0',
    'order' => '0',
    'default_value' => '',
    'override_save_value' => '',
    'class' => '',
    'javascript' => '',
    'text_size' => '',
    'text_maxlength' => '11',
    'textarea_cols' => '',
    'textarea_rows' => '',
    'select_caption' => '',
    'select_size' => '',
    'select_table' => '',
    'select_fields' => '',
    'select_condition' => '',
  ],
  'color_name' => 
  [
    'type' => 'text',
    'enabled' => '1',
    'form_admin' => 'input',
    'form_public' => 'text',
    'list_admin' => 'text',
    'list_public' => 'text',
    'required' => '1',
    'order' => '1',
    'default_value' => '',
    'override_save_value' => '',
    'class' => 'wide',
    'javascript' => '',
    'text_size' => '',
    'text_maxlength' => '50',
    'textarea_cols' => '',
    'textarea_rows' => '',
    'select_caption' => '',
    'select_size' => '',
    'select_table' => '',
    'select_fields' => '',
    'select_condition' => '',
  ],
  'observations' =>
  [
    'type' => 'textarea',
    'enabled' => '1',
    'form_admin' => 'input',
    'form_public' => 'text',
    'list_admin' => '0',
    'list_public' => 'text',
    'order' => '10',
    'default_value' => '',
    'override_save_value' => '',
    'class' => 'wide',
    'javascript' => '',
    'text_size' => '',
    'text_maxlength' => '',
    'textarea_cols' => '40',
    'textarea_rows' => '6',
    'select_caption' => '',
    'select_size' => '',
    'select_table' => '',
    'select_fields' => '',
    'select_condition' => '',
  ],
  'bin' =>
  [
    'type' => 'hidden',
    'enabled' => '1',
    'form_admin' => 'input',
    'form_public' => 'no',
    'list_admin' => '0',
    'list_public' => 'no',
    'order' => '14',
    'default_value' => '',
    'override_save_value' => '',
    'class' => '',
    'javascript' => '',
    'text_size' => '',
    'text_maxlength' => '1',
    'textarea_cols' => '',
    'textarea_rows' => '',
    'select_caption' => '',
    'select_size' => '',
    'select_table' => '',
    'select_fields' => '',
    'select_condition' => '',
  ],
	];
	$gl_db_classes_list = array (
  'cols' => '1',
);
	$gl_db_classes_form = array (
  'cols' => '1',
  'images_title' => '',
);
	?>