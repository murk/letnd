<? if ( $show_map ): ?>
	<script type="text/javascript">

		var gl_google_latitude = '<?=$latitude?>';
		var gl_google_longitude = '<?=$longitude?>';
		var gl_google_center_latitude = '<?=$center_latitude?>';
		var gl_google_center_longitude = '<?=$center_longitude?>';
		var gl_zoom = <?=$zoom;?>;

		<? if( GOOGLE_MAPS_KEY ): ?>
		$(function () {
			initialize_inmoform(true, 'google-map-inmo');
		});
		<? endif // GOOGLE_MAPS_KEY ?>

	</script>
<? endif //show_map?>

	<article>

		<? /* Botons */ ?>
		<div class="show-buttons">
			<? $back_link = $back_button ? $back_button : get_page_link( 'property' ); ?>
			<a class="back-button" href="<?= $back_link ?>"><?= $c_back ?></a>

			<? if ( $show_previous_link || $show_next_link ): ?>
				<? if ( $show_previous_link ): ?>
					<a class="previous-link" href="<?= $show_previous_link ?>"></a>
				<? endif ?>
				<? if ( $show_next_link ): ?>
					<a class="next-link" href="<?= $show_next_link ?>"></a>
				<? endif ?>
			<? endif ?>
		</div>
		<? /* Fi Botons */ ?>

		<? if ( $images_cat_1 ): ?>
			<figure>

				<div id="slider-inmo">

					<? foreach ( $images_cat_1 as $l ):extract( $l ) ?>
						<div class="item<?= $image_conta > 0 ? ' hidden' : '' ?>">

							<a href="<?= $image_src_medium ?>" data-sub-html="<?= htmlspecialchars( $image_title ) ?>" title="<?= htmlspecialchars( $image_title ? $image_title : $property ) ?>"><img <?= get_image_attrs( $image_src_details, $image_alt ? $image_alt : $property, 700, 466, false ) ?> /></a>
							
						</div>
					<? endforeach //images?>

				</div>

				<? if ( count( $images ) > 0 ): ?>

					<div id="slider-inmo-thumbs">

						<? foreach ( $images_cat_1 as $l ):extract( $l ) ?>
							<div class="item">
								<img <?= get_image_attrs( $image_src_thumb, $image_alt ? $image_alt : $property, 150, 100, false ) ?> />
							</div>
						<? endforeach //images?>

					</div>

				<? endif //$images ?>


				<? if ( $status ): ?>
					<div class="flag sold">
						<span><?= $status ?></span>
					</div>
				<? elseif ( $offer ): ?>
					<div class="flag offer">
						<span><?= $c_offer ?></span>
					</div>
				<? elseif ( $recent ): ?>
					<div class="flag recent"><span><?= $c_recent ?></span></div>
				<? endif // $status ?>

			</figure>
		<? endif //images?>


		<? if ( $videoframe ): ?>
			<div class="videoframe"><?= $videoframe ?></div>
		<? endif //videoframe?>

		<header>
			<div class="ref">
				<?= $c_ref ?> <?= $ref ?>
			</div>

			<? if ( $hut ): ?>
				<div class="hut">
					<?= $c_hut ?>: <?= $hut ?>
				</div>
			<? endif //$hut ?>

			<? if ( $property_title ): ?>
				<h2>
					<?= $property_title ?>
				</h2>
			<? endif //property_title?>
		</header>

		<? if ( $description ): ?>
			<div class="description">
				<?= $description ?>
			</div>
		<? endif //description?>

		<? /*
	get_property_details($details, &$tpl, $loop_vars = false, $exclude = false, $type='table', $cols = 2, $sep='', $class = 'property-details')
	*/ ?>
		<?=
		get_property_details(
			'category_id,floor_space,room,bathroom,wc,land,municipi_id,tipus', $this
		)
		?>
		<?=
		get_property_details(
			'category_id,floor_space,room,bathroom,wc,land,municipi_id,tipus', $this, false, true, 'table', 4
		)
		?>

		<? if ( $files ): ?>

			<div class="files">

				<? foreach ( $files as $l ): extract( $l ) ?>
					<a target="_blank" href="<?= $file_src ?>"><?= $file_title ?></a>
				<? endforeach //files?>

			</div>

		<? endif //files?>

		<? if ( $old_price && $price_change_percent_value < 0  && $tipus_value != 'temp' ): ?>
			<div class="old-price"><?= $c_price ?>: <?= $old_price ?> <?= CURRENCY_NAME ?></div>
		<? endif //old_price?>

		<? if ( $price ): ?>
			<div class="price"><?= $c_price ?>: <?= $price ?> <?= CURRENCY_NAME ?><? if($tipus_value == 'temp' && $total_nights == 1): ?>/n<? endif //$ ?>
				
				<? if($tipus_value == 'temp' && $total_nights > 1): ?>
					<span>( <?= R::get( 'date_in' ) ?> - <?= R::get( 'date_out' ) ?> )</span>
				<? endif //$ ?>
			</div>
		<? endif //price?>

		<? if ( $price_consult ): ?>
			<div class="price-consult"><?= $price_consult ?></div>
		<? endif //price_consult?>

		<? if ( $price_change_percent_value < 0  && $tipus_value != 'temp' ): // només mostra quan baixa per defecte ?>
			<div class="price-change-percent">
				<?= $price_change_percent_value > 0 ? '+' : '-' ?><?= $price_change_percent ?>&nbsp;&percnt;
			</div>
		<? endif //price_change_percent?>

		<? if ( $show_map ): ?>
			<aside id="google-map-inmo">
				<? if( !GOOGLE_MAPS_KEY ): ?>
					<?= get_google_iframe( true, $latitude, $longitude, $center_latitude, $center_longitude, $zoom, $show_map_point ) ?>
				<? endif // GOOGLE_MAPS_KEY ?>
			</aside>
		<? endif // show_map?>

	</article>

<? /* ENLLAÇ A CONTACTE */ ?>
	<aside class="contact">
		<? $link = get_page_link( 'contact' );
		$link .= USE_FRIENDLY_URL ? '?' : '&';
		$link .= 'contact_subject=' . $c_ref . '%20' . urlencode( $ref ); ?>
		<div class="email">
			<a href="<?= $link ?>"><?= $c_mail ?></a>
		</div>
	</aside>


<? /* FORMULARIS INTEGRATS DE CONTACTE I RESERVES */ ?>
<? if ( $tipus_value == 'temp' ): ?>

	<aside id="book">
		<?= get_book_form( $property_id ) ?>
	</aside>

<? else: //?>

	<aside class="contact">
		<?= get_contact_form( $c_ref . ' ' . $ref, 'contact/property_contact_form' ) ?>
	</aside>

<? endif //tipus?>