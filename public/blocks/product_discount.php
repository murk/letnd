<?php
if (!defined('PRODUCT_DISCOUNT_MAX_RESULTS')) Module::get_config_values('product');

$module = Module::load('product','product');
$module->parent = 'Block';
$wholesaler_sql = $module->is_wholesaler()?'_wholesaler':'';

$module->condition = 
	"AND (discount" . $wholesaler_sql . "<>0 OR discount_fixed_price" . $wholesaler_sql . "<>'0.00')";
$module->limit = '0,' . PRODUCT_DISCOUNT_MAX_RESULTS;

$loop = $module->do_action('get_records');



/*
$query = "
SELECT
	product__product.product_id AS product_id,
	product_private,
	product_title,
	product_description,
	ref,
	ref_number,
	family_id,
	subfamily_id,
	pvp,
	pvd,
	price_consult,
	tax_id,
	discount,	
	price_unit,
	brand_id,
	sell,
	guarantee,
	guarantee_period,
	year,
	observations,
	status,
	home,
	free_send,
	bin,
	image_id, name
	FROM product__product, product__product_language, product__product_image
	WHERE product__product_language.product_id = product__product.product_id
	AND product__product.product_id = product__product_image.product_id
	AND main_image = 1
	AND (discount" . $wholesaler_sql . "<>0 OR discount_fixed_price" . $wholesaler_sql . "<>'0.00')
	AND language = '" . LANGUAGE . "'
	AND product__product.bin = 0
	AND ( status = 'onsale' OR status = 'sold' OR status = 'reserved')
	ORDER BY product_title
	LIMIT 0," . PRODUCT_VISITED_MAX_RESULTS;

$results = Db::get_rows($query);
Debug::add('Product_visited', $query);
*/
if ($loop)
{
	
}
else{
	$show_block = false;
}


?>