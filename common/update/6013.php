<?

/* 11-12-14 
Modificacions custumer
 */

if (is_table('custumer__custumer') && !is_field('custumer__custumer', 'bic')) {
		
		
		$update_sql .= "
			ALTER TABLE `custumer__custumer`
				ADD COLUMN `bic` VARCHAR(20) NULL DEFAULT NULL AFTER `bank`,
				ADD COLUMN `vat_type` ENUM('D','P','C','I','N','X') NULL DEFAULT 'D' AFTER `vat`,
				ADD COLUMN `vat_date` DATE NOT NULL DEFAULT '0000-00-00' AFTER `vat_type`,
				ADD COLUMN `nationality` CHAR(2) NULL DEFAULT NULL AFTER `country`;";
		
		
		$update_sql .= "
			UPDATE custumer__custumer SET nationality=country;";
}
else{
	if (is_table('custumer__custumer')) {		
		trigger_error('Ja existeixen el field custumer__custumer.bic');
	}
}


if (is_table('booking__configadmin') && !Db::get_first("SELECT * FROM booking__configadmin WHERE name='has_contracts'" )) {	
	
	$update_sql .= "
		INSERT INTO booking__configadmin (`name`,value) VALUES ('has_contracts','0');
		";	
}
else{
	if (is_table('booking__configadmin')) {		
		trigger_error('Ja existeixen el name has_contracts');
	}
}

?>