<?
$gl_caption['c_order_status'] = 'Paid';
$gl_caption['c_order_status_paying'] = 'Pending';
$gl_caption['c_order_status_failed'] = 'Failed';
$gl_caption['c_order_status_deposit'] = 'Deposit payment';
$gl_caption['c_order_status_pending'] = 'Pending';
$gl_caption['c_order_status_denied'] = 'Rejected';
$gl_caption['c_order_status_voided'] = 'Cancelled';
$gl_caption['c_order_status_refunded'] = 'Returned';
$gl_caption['c_order_status_completed'] = 'Paid';
$gl_caption['c_order_status_preparing'] = 'Preparing';
$gl_caption['c_order_status_shipped'] = 'Sent';
$gl_caption['c_order_status_delivered'] = 'Delivered';

$gl_caption['c_nav_product_search'] = "New booking";
$gl_caption['c_nav_product_search_text'] = "New";
$gl_caption['c_nav_reserva'] = "List bookings";
$gl_caption['c_nav_reserva_text'] = "Bookings";
$gl_caption['c_nav_timetable'] = "Calendar";
$gl_caption['c_nav_timetable_text'] = "Calendar";
$gl_caption['c_nav_back'] = "Back";
$gl_caption['c_nav_languages'] = "Language";
$gl_caption['c_nav_logout'] = "Log out";

$gl_caption['c_explicacio_buscador'] = "Search by words or References <i>(ej: \"456 245 45\")</i>";
$gl_caption['c_explicacio_buscador_producte'] = "Search activities";
$gl_caption['c_top_products'] = "Bestsellers";
$gl_caption['c_last_products'] = "Recent activities";
$gl_caption['c_families'] = "Families";
$gl_caption['c_ref_list'] = "ref.";
$gl_caption['c_activitat'] = "Activity";
$gl_caption['c_reservar_ara'] = "Finish booking";

$gl_caption['c_titol_paperera'] = "Recycle bin";
$gl_caption['c_titol_paperera_2'] = "Deleted bookings";

$gl_caption['c_set_payed_not_saved'] = "Couldn't be marked as paid, please try again";
$gl_caption['c_set_payed_saved'] = "Booking has been marked as paid";
$gl_caption['c_set_payed_saved_deposit'] = "Deposit payment marked as paid";

$gl_messages ['no_records'] = 'No bookings found';