<?php
$module = Module::load('portfoli','portfoli');
 
$block_max_results = isset($vars['block_max_results']) ? $vars['block_max_results'] : $module->config['destacat_max_results'];

$destacat_type = isset($vars['destacat_type'])?$vars['destacat_type']:'all'; // destacat, all
$limit_offset = isset($vars['limit_offset'])?$vars['limit_offset']-1:'0'; // destacat, all
$add_dots_length = isset($vars['add_dots_length']) ? $vars['add_dots_length'] : false;


if ($destacat_type == 'all') {
	$query = "";
}
else{
	$query = "AND destacat = 1";
}

$query = "SELECT portfoli__portfoli.portfoli_id as portfoli_id, 
				title, 
				subtitle,
				description
				FROM portfoli__portfoli_language, portfoli__portfoli
				WHERE portfoli__portfoli_language.portfoli_id = portfoli__portfoli.portfoli_id
				AND (status = 'public')
				AND bin <> 1
				AND language = '" . LANGUAGE . "'
				".$query."
				ORDER BY destacat DESC, ordre ASC, title
				LIMIT $limit_offset, $block_max_results;";

$results = Db::get_rows($query);

if ($results) {

	$conta = 0;
	$loop_count = count($results);
	foreach ($results as $rs) {
		$l = array();


		
		$l['link'] = '/' . LANGUAGE . '/portfoli/portfoli/' . $rs['portfoli_id'] . '/';
		$categorys = Db::get_rows("
			SELECT category 
			FROM portfoli__category_language, portfoli__portfoli_to_category
			WHERE language = '".LANGUAGE."'
			AND portfoli__category_language.category_id = portfoli__portfoli_to_category.category_id
			AND portfoli_id = " . $rs['portfoli_id']);
		
		$category = '<ul>';
		foreach ($categorys as $cat){			
			$category .= '<li>' . $cat['category'] . '</li>';
		}
		$category .= '</ul>';
		
		$query = "SELECT page_file_name FROM all__page INNER JOIN all__page_language USING(page_id) WHERE language='".LANGUAGE."' AND page_text_id = 'portfoli'";
		
		$page_name = Db::get_first($query);
		if ($page_name) $l['link'] .= $page_name . '.html';

		$l['show_record_link'] = $l['link'];
		$l['title'] = $rs['title'];
		$l['subtitle'] = $rs['subtitle'];
		$l['description'] = add_dots('description', $rs['description'], $add_dots_length);
		$l['category_id'] = $category;
		$l['id'] = $rs['portfoli_id'];
		$l['conta'] = $conta++;
		$l['class'] = '';
		$l += UploadFiles::get_record_images($rs['portfoli_id'], true, 'portfoli', 'portfoli');
		$l['loop_count'] = $loop_count;
		
		$loop[] = $l;
	}
	$loop[0]['class'] = 'first';
	$loop[count($loop) - 1]['class'] = 'last';
	
}
else {
	/* si no hi ha cap resultat no mostro el block */
	$show_block = false;
	return;
}
?>