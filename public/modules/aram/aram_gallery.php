<?
/**
 * AramGallery
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2012
 * @version $Id$
 * @access public
 */
class AramGallery extends Module{

	function __construct(){
		parent::__construct();
		$_GET['gallery_id'] = '1'; // així sempre edito imatges del registre 1
	}

	function list_records()
	{
		$images = UploadFiles::get_record_images('1', true, 'aram', 'gallery');	
		define('ARAM_GALERY_IMAGE',$images['images'][0]['image_src_details']);
		array_shift ($images['images']);
		$GLOBALS['gl_page']->set_var('images',$images['images']);
	}
}
?>