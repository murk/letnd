<?php
global $gl_caption_admin,
       $gl_caption_public,
       $gl_caption_newsletter,
       $gl_caption_contact_public,
       $gl_caption_inmo_admin,
       $gl_caption_inmo_public,
       $gl_caption_booking_admin,
       $gl_caption_booking_public,
       $gl_caption_translation,
       $gl_caption_custumer,
       $gl_caption_page,
       $gl_caption_save,
       $gl_caption_product_admin,
       $gl_caption_product_public,
       $gl_caption_utility;
// menus eina
if ( ! defined( 'WEB_MENU_TITLE' ) ) {
	define( 'WEB_MENU_TITLE', 'Titre du site' ); //Nom apareix dalt internet explorer
	define( 'WEB_MENU_ROBOTS', 'Mots clés du site' ); //Paraules dels tags
	define( 'WEB_MENU_SAVE', 'Sauvegardes' ); //Paraules dels tags
	define( 'WEB_MENU_SAVE_LIST', 'Crée Sauvegarde' ); //Paraules dels tags
	define( 'WEB_MENU_TRANSLATION', 'Traductions' );
	define( 'WEB_MENU_TRANSLATION_LIST', 'Énumérer toutes les traductions' );
	define( 'WEB_MENU_PAGE_TRANSLATION_LIST', 'Énumérer les articles à traduire' );
	define( 'WEB_MENU_BACKOFFICE_SEARCH', 'Recherche clients' );


	define( 'WEB_MENU_CONFIG', 'Paramètres' );
	define( 'WEB_MENU_CONFIGADMIN_LIST', 'Général part privée' );
	define( 'WEB_MENU_CONFIGPUBLIC_LIST', 'Général part públique' );
	define( 'WEB_MENU_CONFIGINMOADMIN_LIST', 'Propriétés part privée' );
	define( 'WEB_MENU_CONFIGINMOPUBLIC_LIST', 'Propriétés part públique' );
	define( 'WEB_MENU_CONFIGCUSTUMER_LIST', 'Clients' );
	define( 'WEB_MENU_CONFIGNEWSLETTER_LIST', 'Newsletter' );
	define( 'WEB_MENU_CONFIGUTILITY_LIST', 'Utilitaires' );
	define( 'WEB_MENU_CONFIGMLS_LIST', 'MLS' );
	define( 'WEB_MENU_CONFIGCONTACT_PUBLIC_LIST', 'Contacter part públique' );
	define( 'WEB_MENU_CONFIGCONTACT_ADMIN_LIST', 'Contacter part privée' );

	define( 'WEB_MENU_CONFIGBOOKING_ADMIN_LIST', 'Réservations part privée' );
	define( 'WEB_MENU_CONFIGBOOKING_PUBLIC_LIST', 'Réservations part públique' );

	define( 'WEB_MENU_CONFIGPRODUCT_ADMIN_LIST', 'Produits part privée' );
	define( 'WEB_MENU_CONFIGPRODUCT_PUBLIC_LIST', 'Produits part públique' );

	define( 'WEB_MENU_CONFIGCLIENT_LIST', 'Phrases part públique' );

	if ( ! defined( 'WEB_MENU_BANNER' ) ) {
		define( 'WEB_MENU_BANNER', 'Bannière' );
		define( 'WEB_MENU_BANNER_NEW', 'Nouvelle bannière' );
		define( 'WEB_MENU_BANNER_LIST', 'Liste des Bannières' );
		define( 'WEB_MENU_BANNER_LIST_BIN', 'Corbeille' );
	}

	define( 'WEB_MENU_PAGE', 'Édition de pages' );
	define( 'WEB_MENU_PAGE_NEW', 'Nouvelle page' );
	define( 'WEB_MENU_PAGE_LIST', 'Énumérer pages' );
	define( 'WEB_MENU_PAGE_LIST_BIN', 'Corbeille' );

	define( 'WEB_MENU_GUESTBOOK', 'Livre de or' );
	define( 'WEB_MENU_GUESTBOOK_NEW', 'Nouvelle visite' );
	define( 'WEB_MENU_GUESTBOOK_LIST', 'Énumérer visites' );
	define( 'WEB_MENU_GUESTBOOK_LIST_BIN', 'Corbeille' );


	define( 'WEB_MENU_GALLERY', 'Galeries d\'images' );
	define( 'WEB_MENU_GALLERY_NEW', 'Nouvelle galerie' );
	define( 'WEB_MENU_GALLERY_LIST', 'Énumérer galeries' );
	define( 'WEB_MENU_GALLERY_LIST_BIN', 'Corbeille' );


	define( 'WEB_MENU_PLACE', 'Bureaux' );
	define( 'WEB_MENU_PLACE_NEW', 'Nouveau bureau' );
	define( 'WEB_MENU_PLACE_LIST', 'Énumérer bureaux' );
}

if ( ! defined( 'API_MENU_PAGINA' ) ) {
	// per poder banners fer desde api
	define( 'API_MENU_PAGINA', 'Édition de pages' );
	define( 'API_MENU_PAGINA_NEW', 'Entrez nouvelle page' );
	define( 'API_MENU_PAGINA_NEW_SECOND', 'Entrez nouvelle page secondaire' );
	define( 'API_MENU_PAGINA_LIST', 'Énumérer pages' );
	define( 'API_MENU_PAGINA_LIST_SECOND', 'Énumérer pages secondaires' );
	define( 'API_MENU_PAGINA_BIN', 'Corbeille' );
}

/*--------------- no traduit -------------------*/

$gl_caption_product_admin['c_group_15']     = 'Caractéristiques configurables';
for ($i = 1; $i <= 20; $i++) {
	$gl_caption_config['c_others' . $i] = 'Caractéristique ' . $i;
}
for ($i = 1; $i <= 20; $i++) {
	$gl_caption_config['c_others' . $i . '_type'] = 'Type';
}


$gl_caption_config['c_default_home_tool']         = 'Outil par défaut';
$gl_caption_config['c_default_home_tool_section'] = 'Section par défaut';
$gl_caption_config['c_language']                  = 'Langue par défaut';
$gl_caption_config['c_home_max_results']          = 'Nombre de resultats à la page d\'accueil';
$gl_caption_config['c_page_slogan']               = 'Phrase d\'entreprise';
$gl_caption_public['c_page_title']                = 'Titre du site';
$gl_caption_config['c_page_phone']                = 'Téléphone';
$gl_caption_config['c_page_mobile']               = 'Mobile';
$gl_caption_config['c_page_fax']                  = 'Fax';
$gl_caption_config['c_page_address']              = 'Rue';
$gl_caption_config['c_page_numstreet']            = 'Nombre';
$gl_caption_config['c_page_flat']                 = 'Étage';
$gl_caption_config['c_page_door']                 = 'Porte';
$gl_caption_config['c_page_postcode']             = 'Code postal';
$gl_caption_config['c_page_municipi']             = 'Village';
$gl_caption_config['c_page_provincia']            = 'Province';
$gl_caption_config['c_page_country']              = 'Pays';
$gl_caption_config['c_page_title']                = 'Títre Intranet';
$gl_caption_config['c_default_menu_id']           = 'Menu par défaut';
$gl_caption_config['c_theme']                     = 'Sujet';
$gl_caption_config['c_language']                  = 'Langue';
$gl_caption_config['c_max_results']               = 'Nombre de résultats par page';
$gl_caption_config['c_max_page_links']            = 'Nombre de pages à afficher';
$gl_caption_config['c_add_dots_length']           = 'Maximum de mots au descriptions';
$gl_caption_config['c_date_created']              = 'Date de création du web';
$gl_caption_admin['c_group_1']                    = 'Options du listées';
$gl_caption_public['c_group_2']                   = $gl_caption_admin['c_group_1'];
$gl_caption_config['c_templatehtml_id']           = 'Modèle en html';
$gl_caption_config['c_templatetext_id']           = 'Modèle de text';
$gl_caption_config['c_group_1']                   = 'De';
$gl_caption_config['c_group_2']                   = 'Répondre au';
$gl_caption_config['c_group_3']                   = 'Donnés SMTP';
$gl_caption_config['c_group_4']                   = 'Adresse';
$gl_caption_config['c_group_5']                   = 'Donnés entreprise';
$gl_caption_config['c_company_name']              = 'Nom entreprise';
$gl_caption_config['c_company_cif']               = 'CIF / NIF';
$gl_caption_config['c_m_username']                = 'Nom d\'utilisateur';
$gl_caption_config['c_m_password']                = 'Mot du passe';
$gl_caption_config['c_m_password_repeat']         = 'Répétez mot du passe';
$gl_caption_config['c_m_host']                    = 'Serveur';
$gl_caption['c_custumer_table']                   = 'Table d\'adresses';
$gl_caption['c_newsletter__custumer']             = 'newsletter__custumer';
$gl_caption['c_custumer__custumer']               = 'custumer__custumer';
$gl_caption['c_contact__contact']                 = 'contact__contact';
$gl_caption['c_product__customer']                = 'product__customer';
$gl_caption_newsletter['c_content_extra']         = 'Contenu inclus';
$gl_caption_config['c_only_test']                 = 'Mode test';
$gl_caption_config['c_only_test_description']     = 'Si vous cochez cette case le courrier simule la livraison mais il y a pas de envoyer ';

$gl_caption_config['c_text']              = 'text';
$gl_caption_config['c_provincies']        = 'Provinces habituelles';
$gl_caption_config['c_default_provincia'] = 'Province par defaut';
$gl_caption_config['c_default_comarca']   = 'Comarca par defau';
$gl_caption_config['c_default_municipi']  = 'Ville par defau';
$gl_caption_config['c_units']             = 'Unités';

$gl_caption_inmo_admin['c_group_1']      = 'Thumbnail admin';
$gl_caption_config['c_im_admin_thumb_w'] = 'amplada';
$gl_caption_config['c_im_admin_thumb_h'] = 'hauteur';
$gl_caption_config['c_im_admin_thumb_q'] = 'qualitat';

$gl_caption_inmo_admin['c_group_2']  = 'Thumbnail public';
$gl_caption_inmo_public['c_group_2'] = $gl_caption_inmo_admin['c_group_2'];
$gl_caption_config['c_im_thumb_w']   = 'largeur';
$gl_caption_config['c_im_thumb_h']   = 'hauteur';
$gl_caption_config['c_im_thumb_q']   = 'qualité';

$gl_caption_inmo_admin['c_group_3']  = 'Dossier image publique';
$gl_caption_inmo_public['c_group_3'] = $gl_caption_inmo_admin['c_group_3'];
$gl_caption_config['c_im_details_w'] = 'largeur';
$gl_caption_config['c_im_details_h'] = 'hauteura';
$gl_caption_config['c_im_details_q'] = 'qualité';

$gl_caption_inmo_admin['c_group_4']  = 'Grand image public';
$gl_caption_inmo_public['c_group_4'] = $gl_caption_inmo_admin['c_group_4'];
$gl_caption_config['c_im_medium_w']  = 'largeur';
$gl_caption_config['c_im_medium_h']  = 'hauteur';
$gl_caption_config['c_im_medium_q']  = 'qualité';


$gl_caption_inmo_admin['c_group_5'] = 'Grande image impression';
$gl_caption_config['c_im_big_w']    = 'largeur';
$gl_caption_config['c_im_big_h']    = 'hauteur';

$gl_caption_config['c_im_list_cols']        = 'Columns image';
$gl_caption_config['c_property_auto_ref']   = 'Référence automatique';
$gl_caption_config['c_menu_price']          = 'Valeurs du menu du pris';
$gl_caption_config['c_menu_price_rent']     = 'Valeurs du menu du pris location';
$gl_caption_config['c_recent_time']         = 'Jours pour montrer l\'étiquette "nouveau inmeuble"';
$gl_caption_config['c_home_max_results']    = 'Nombre marquée';
$gl_caption_config['c_home_title']          = 'Titre de biens marquées';
$gl_caption_config['c_html']                = 'html';
$gl_caption_config['c_from_address']        = 'E-mail';
$gl_caption_config['c_from_address_repeat'] = 'Répetez e-mail';
$gl_caption_config['c_from_name']           = 'Nom';
$gl_caption_config['c_reply_to']            = 'E-mail';
$gl_caption_config['c_reply_to_repeat']     = 'Répetez e-mail';
$gl_caption_config['c_reply_to_name']       = 'Nom';
$gl_caption_config['c_subject']             = 'Sujet';
$gl_caption_config['c_format']              = 'Format';
$gl_caption_config['c_send_method']         = 'Methode d\'expédition';
$gl_caption_config['c_CC']                  = 'Copiez chaque direction (CC)';
$gl_caption_config['c_BCC']                 = 'Copiez chaquer chaque direction(BCC)';
$gl_caption_config['c_one_by_one']          = 'Un message différent pour chaque direction';
$gl_caption['c_template_caption']           = '[Pas de modèle]';

$gl_caption_translation['c_2']            = 'En traduction';
$gl_caption_translation['c_3']            = 'Terminé';
$gl_caption_translation['c_1']            = 'En attendant';
$gl_caption_translation['c_4']            = 'Erreur';
$gl_caption['c_description']              = 'Description du site';
$gl_caption['c_mail']                     = 'E-mail par default';
$gl_caption['c_title']                    = 'Titre du site';
$gl_caption['c_robots']                   = 'Mots-clé du site';
$gl_caption_custumer['c_group_1']         = 'Recherche approximative';
$gl_caption_custumer['c_group_2']         = 'MLS';
$gl_caption_config['c_search_percent']    = 'Slope au cherches clients et MLS (%)';
$gl_caption_config['c_search_level']      = 'Niveau d\'approche au cherche';
$gl_caption_translation['c_join_date']    = 'Date d\'expédition';
$gl_caption_translation['c_state']        = 'Courant:';
$gl_caption_translation['c_numwords']     = 'Nombre de mots:';
$gl_caption_translation['c_originaltext'] = 'Text original:';

$gl_caption['c_search_level_1'] = 'Restrictf';
$gl_caption['c_search_level_2'] = 'Normal';
$gl_caption['c_search_level_3'] = 'Peu restrictif';

$gl_caption_config ['c_mls_all']           = 'Tick par defaut la boîte MLS des biens';
$gl_caption_config ['c_mls_comission']     = 'Commission par defaut des propriétés MLS (%)';
$gl_caption_config ['c_mls_search_always'] = 'Recherche MLS activé';
$gl_caption_config ['c_mls_allow_listing'] = 'Permettre énumérer immeubles';

$gl_caption_config ['c_0']            = 'Non';
$gl_caption_config ['c_1']            = 'Oui';
$gl_caption_config ['c_default_mail'] = 'E-mail par defautt';
$gl_caption_config ['c_use_custom_foot'] = 'Peu intranet';

$gl_caption_save ['c_save_1'] = 'Clickez ici';
$gl_caption_save ['c_save_2'] = 'pour télécharger la sauvegarde';


$gl_caption_config ['c_google_center_longitude']    = 'Emplacement';
$gl_caption_inmo_admin['c_google_center_longitude'] = 'Position initiale de la carte';
$gl_caption_config ['c_google_latitude']            = 'Latitude';
$gl_caption_config ['c_google_longitude']           = 'Longueur';
$gl_caption_config ['c_google_center_latitude']     = 'Latitude centre carte';
$gl_caption_config ['c_center_longitude']           = 'Longueur centre carte';
$gl_caption_config ['c_google_zoom']                = '';
$gl_caption_config ['c_allow_seo']                  = 'Activer SEO';

// contact
$gl_caption_contact_public['c_group_4']  = 'Options';
$gl_caption_contact_public['c_send_mail_copy']  = 'Envoyer copia au client';
$gl_caption_contact_public['c_use_mail_template']  = 'Utiliser modèle';

// inmo
$gl_caption_inmo_admin['c_custom1_name']   = 'Caractéristique 1';
$gl_caption_inmo_admin['c_custom2_name']   = 'Caractéristique 2';
$gl_caption_inmo_admin['c_custom3_name']   = 'Caractéristique 3';
$gl_caption_inmo_admin['c_custom4_name']   = 'Caractéristique 4';
$gl_caption_inmo_admin['c_custom5_name']   = 'Caractéristique 5';
$gl_caption_inmo_admin['c_custom6_name']   = 'Caractéristique 6';
$gl_caption_inmo_admin['c_custom7_name']   = 'Caractéristique 7';
$gl_caption_inmo_admin['c_custom8_name']   = 'Caractéristique 8';
$gl_caption_inmo_admin['c_custom1_type']   =
$gl_caption_inmo_admin['c_custom2_type'] =
$gl_caption_inmo_admin['c_custom3_type'] =
$gl_caption_inmo_admin['c_custom4_type'] =
$gl_caption_inmo_admin['c_custom5_type'] =
$gl_caption_inmo_admin['c_custom6_type'] =
$gl_caption_inmo_admin['c_custom7_type'] =
$gl_caption_inmo_admin['c_custom8_type'] = 'Types';
$gl_caption_inmo_admin['c_custom1_filter'] =
$gl_caption_inmo_admin['c_custom2_filter'] =
$gl_caption_inmo_admin['c_custom3_filter'] =
$gl_caption_inmo_admin['c_custom4_filter'] =
$gl_caption_inmo_admin['c_custom5_filter'] =
$gl_caption_inmo_admin['c_custom6_filter'] =
$gl_caption_inmo_admin['c_custom7_filter'] =
$gl_caption_inmo_admin['c_custom8_filter'] = 'Filtre';

$gl_caption_config['c_checkbox']     = 'Cochez la case';
$gl_caption_config['c_int']          = 'Nombre';
$gl_caption_config['c_text']         = 'Text au langues';
$gl_caption_config['c_text_no_lang'] = 'Text';
$gl_caption_config['c_textarea']     = 'Text plusieurs lignes';
$gl_caption_config['c_select']       = 'Déroulant';

// booking
$gl_caption_booking_admin ['c_group_1'] = "Reservas";
$gl_caption_booking_admin ['c_group_2'] = "Datos Mossos";
$gl_caption_booking_admin ['c_group_3'] = "Contratos";

$gl_caption['c_ignore_outside_ue']           = "Ignorar fuera de la UE";
$gl_caption['c_default_status']              = "Estado por defecto al reservar desde admin";
$gl_caption['c_public_default_status']       = "Estado por defecto al reservar desde la web";
$gl_caption['c_booked']                      = "Reservado";
$gl_caption['c_pre_booked']                  = "Pre reserva";
$gl_caption['c_has_payment']                 = "Módulo de pago";
$gl_caption['c_nom_establiment']             = "Nombre establecimiento";
$gl_caption['c_codi_establiment']            = "Código establecimiento";
$gl_caption['c_has_contracts']               = "Contratos";
$gl_caption['c_contract_default_language']   = "Idioma por defecto";
$gl_caption['c_contract_format']             = "Formato";
$gl_caption['c_google_analytics_conversion'] = "Codi conversió google analytics";


// productes
$gl_caption_product_admin ['c_default_sell'] = 'Vente par défault';
$gl_caption_product_admin ['c_pvp_required'] = 'P.V.P obligatoire';
$gl_caption_product_admin ['c_home_title']   = 'Titre des produits marquées';
$gl_caption_product_admin ['c_group_1']      = $gl_caption_product_public ['c_group_1'] = $gl_caption_inmo_admin['c_group_1'];
$gl_caption_product_admin ['c_group_2']      = $gl_caption_product_public ['c_group_2'] = $gl_caption_inmo_admin['c_group_2'];
$gl_caption_product_admin ['c_group_3']      = $gl_caption_product_public ['c_group_3'] = $gl_caption_inmo_admin['c_group_3'];
$gl_caption_product_admin ['c_group_4']      = $gl_caption_product_public ['c_group_4'] = $gl_caption_inmo_admin['c_group_4'];
$gl_caption_product_admin ['c_group_5']      = $gl_caption_product_public ['c_group_5'] = $gl_caption_inmo_admin['c_group_5'];

$gl_caption_product_public ['c_group_5']      = $gl_caption_booking_public ['c_group_5'] = 'Options du module de paiement';
$gl_caption_product_public ['c_group_6']      = 'Home';
$gl_caption_product_public ['c_group_7']      = $gl_caption_booking_public ['c_group_7'] = 'Virement bancaire';
$gl_caption_product_public ['c_group_8']      = $gl_caption_booking_public ['c_group_8'] = 'Paypal';
$gl_caption_product_public ['c_group_9']      = $gl_caption_booking_public ['c_group_9'] = 'Carte Redsys';
$gl_caption_product_public ['c_group_10']     = 'Carte 4b';
$gl_caption_product_public ['c_no_sell_text'] = 'Description du produit pas à vendre';

$gl_caption ['c_paypal_sandbox']           = 'Paypal en mode sandbox';
$gl_caption ['c_banc_name']                = 'Nom de la banque';
$gl_caption ['c_account_number']           = 'Numéro de compte';
$gl_caption ['c_paypal_mail']              = 'E-mail du PayPal';
$gl_caption ['c_paypal_mail_repeat']       = 'Répétez e-mail';
$gl_caption_product_public ['c_home_show_marked']         = 'Afficher marquée';
$gl_caption_product_public ['c_home_show_familys']        = 'Afficher  familles et sous-familless';
$gl_caption_product_public ['c_home_max_familys']         = 'Nombre maximum de familles';
$gl_caption_product_public ['c_home_max_family_products'] = 'Nombre maximum de produits dans chaque famille';
$gl_caption ['c_payment_methods']          = 'Modules de paiement actifs';
$gl_caption ['c_paypal']                   = 'PayPal';
$gl_caption ['c_account']                  = 'Virement bancaire';
$gl_caption ['c_ondelivery']               = 'Cash';
$gl_caption ['c_directdebit']              = 'Réception de la Banque';
$gl_caption ['c_4b']                       = 'Carte 4b';
$gl_caption_product_public['c_home_title']                = 'Titre du site';
$gl_caption_product_public ['c_4b_simulador']             = '4b en mode simulateur';
$gl_caption_product_public ['c_4b_id_comercio']           = '4b id commerce';
$gl_caption ['c_lacaixa']                  = 'Carte Redsys';
$gl_caption ['c_lacaixa_simulador']        = 'Redsys en mode simulateur';
$gl_caption ['c_lacaixa_clave_sha2']       = 'Code SHA256';
$gl_caption ['c_lacaixa_merchant_code']    = 'Code du commerçe';

$gl_caption_product_public ['c_can_collect']    = 'Permetre recollida en botiga física';

$gl_caption_product_admin['c_is_public_tax_included']     = 'Tous les prix (PVP) sont TVA compris';
$gl_caption_product_admin['c_is_wholesaler_tax_included'] = 'Tous les prix pour distributeur (PVD) sont TVA compris';
$gl_caption_product_admin['c_group_12']                   = 'Impôts';
$gl_caption_product_admin['c_has_equivalencia_default']   = 'Équivalence marqué au temps d\'insérez distributeur';
$gl_caption_product_admin['c_stock_control']              = 'Contrôle automatique de stock';
$gl_caption_product_admin['c_allow_null_stock']           = 'Permettant la vente de produits avec un stock négatif';
$gl_caption_product_admin['c_group_10']                   = 'Stock';
$gl_caption_product_admin['c_group_11']                   = 'Tarifs d\'expéditiont';
$gl_caption_product_admin['c_send_rate_spain']            = 'Espagne';
$gl_caption_product_admin['c_send_rate_france']           = 'France';
$gl_caption_product_admin['c_send_rate_andorra']          = 'Andorre';
$gl_caption_product_admin['c_send_rate_world']            = 'Reste du monde';
$gl_caption_product_admin['c_allow_free_send']            = 'Casella activada, permetre enviament gratuit';
$gl_caption_product_admin['c_minimum_buy_free_send']      = 'Compra mínima de';
$gl_caption_product_admin['c_tax_included']               = 'TVA compris';
$gl_caption_product_admin['c_tax_not_included']           = 'TVA non comprise';


$gl_caption_page['c_ordre']               = 'Ordre';
$gl_caption_page['c_template']            = 'Modèle';
$gl_caption_page['c_link']                = 'Link';
$gl_caption_page_list['c_page_title']          = 'Titre au chercheurs';
$gl_caption_page['c_page']                = 'Nom ou Titre corps menu';
$gl_caption_page['c_title']               = 'Titre du corps';
$gl_caption_page['c_content']             = 'Teneur';
$gl_caption_page['c_prepare']             = 'En préparation';
$gl_caption_page['c_review']              = 'Pour donner avis';
$gl_caption_page['c_public']              = 'Public';
$gl_caption_page['c_archived']            = 'Classét';
$gl_caption_page['c_status']              = 'État';
$gl_caption_page['c_page_id']             = 'Id';
$gl_caption_page['c_parent_id']           = 'Parent';
$gl_caption_page['c_level']               = 'Niveau';
$gl_caption_page['c_menu']                = 'Menu';
$gl_caption_page['c_blocked']             = 'Block.';
$gl_caption_page['c_filter_parent_id']    = 'Parent';
$gl_caption_page['c_filter_level']        = 'Level';
$gl_caption_page['c_has_content']         = 'Content';
$gl_caption_page['c_has_content_0']       = 'non';
$gl_caption_page['c_has_content_1']       = 'Oui';
$gl_caption_page['c_edit_content_button'] = 'Modifier la page';
$gl_caption_page['c_edit_page_button']    = 'Modifier données';
$gl_caption_page['c_filter_menu2']        = 'Navigation';

/*banners*/
$gl_caption_banner['c_banner_id']          = '';
$gl_caption_banner['c_ordre']              = 'Ordre';
$gl_caption_banner['c_url1']               = 'Lien';
$gl_caption_banner['c_url1_name']          = 'Texte au lien';
$gl_caption_banner['c_url1_target']        = 'Destination';
$gl_caption_banner['c_url1_target__blank'] = 'Une nouvelle fenêtre';
$gl_caption_banner['c_url1_target__self']  = 'La même fenêtre';
$gl_caption_banner['c_file']               = 'Image banner';
$gl_caption_banner['c_banner']             = 'Banner';
$gl_caption_banner['c_place_id']           = 'Section';
$gl_caption_banner['c_banner_text']        = 'Texte';
$gl_caption_banner['c_banner_text2']        = 'Texte 2';
$gl_caption_banner['c_filter_place_id']    = 'Toutes les sections';
$gl_caption_banner['c_status']             = 'État';
$gl_caption_banner['c_filter_status']      = 'Tous les états';
$gl_caption_banner['c_prepare']            = 'En préparation';
$gl_caption_banner['c_review']             = 'Pour donner avis';
$gl_caption_banner['c_public']             = 'Public';
$gl_caption_banner['c_archived']           = 'Classét';


/* GUESTBOOK */
$gl_caption_guestbook['c_guestbook_id']      = '';
$gl_caption_guestbook['c_guest']             = 'Nom';
$gl_caption_guestbook['c_comment']           = 'Commentaire';
$gl_caption_guestbook['c_status']            = 'État';
$gl_caption_guestbook['c_prefered_language'] = 'Langue';
$gl_caption_guestbook['c_entered']           = 'Date';
$gl_caption_guestbook['c_prepare']           = 'En préparation';
$gl_caption_guestbook['c_review']            = 'Pour donner avis';
$gl_caption_guestbook['c_public']            = 'Public';
$gl_caption_guestbook['c_archived']          = 'Classé';

$gl_caption_guestbook['c_destacat']='Marqué';
$gl_caption_guestbook['c_destacat_0']='Marqué';
$gl_caption_guestbook['c_destacat_1']='Non marqué';
$gl_caption_guestbook['c_filter_destacat'] = 'Tous, marqué ou non';

/* GALLERY */
$gl_caption_gallery['c_prepare']         = 'En préparation';
$gl_caption_gallery['c_review']          = 'Pour donner avis';
$gl_caption_gallery['c_public']          = 'Public';
$gl_caption_gallery['c_archived']        = 'Classé';
$gl_caption_gallery['c_gallery']         = 'Galerie';
$gl_caption_gallery['c_subtitle']        = 'Sous-titre';
$gl_caption_gallery['c_content']         = 'Continu';
$gl_caption_gallery['c_entered']         = 'Jour d\'arrivée';
$gl_caption_gallery['c_modified']        = 'Date de modification';
$gl_caption_gallery['c_status']          = 'État';
$gl_caption_gallery['c_filter_status']   = 'Tous les états';
$gl_caption_gallery['c_filter_in_home']  = 'Tous, page d\'accueil ou non';
$gl_caption_gallery['c_filter_destacat'] = 'Tous, saillants ou non';
$gl_caption_gallery['c_ordre']           = 'Ordre';

$gl_caption_gallery['c_in_home']      = 'affiche sur la page d\'accueil';
$gl_caption_gallery['c_in_home_0']    = 'pas sur la page d\'accueil';
$gl_caption_gallery['c_in_home_1']    = 'affiche sur la page d\'accueil';
$gl_caption_gallery['c_content_list'] = 'Brève description à la liste';
$gl_caption_gallery['c_destacat']     = 'Galerie saillant';
$gl_caption_gallery['c_destacat_0']   = 'Non saillant';
$gl_caption_gallery['c_destacat_1']   = 'Saillant';

$gl_caption['c_gallery_file_name']     = $GLOBALS['gl_caption']['c_file_name'];
$gl_caption['c_gallery_old_file_name'] = $GLOBALS['gl_caption']['c_old_file_name'];

/* Utilitats */
$gl_caption_utility['c_group_1']                   = 'Valeurs par défaut d\'achat de propriété' ;
$gl_caption_utility['c_group_2']                   = 'Valeurs par défaut vente de propriété';
$gl_caption_utility['c_group_3']                   = 'Valeurs par défaut hypothèque';
$gl_caption_utility['c_agency_percent']            = 'Frais de gestion (%)';
$gl_caption_utility['c_efficiency']                = 'Certificat de performance énergétique (€)';
$gl_caption_utility['c_estate_commission_percent'] = 'Comission de l\'agence immobilière (%)';
$gl_caption_utility['c_habitability_card']         = 'Certificat d\'occupation (€)';
$gl_caption_utility['c_mortgage_cancel_percent']   = 'Commission d\'annulation(%)';
$gl_caption_utility['c_mortgage_initial_interest'] = 'Interérêts par defaut (%)';
$gl_caption_utility['c_mortgage_length']           = 'Durée hypothécaire (années)';
$gl_caption_utility['c_mortgage_notary_percent']   = 'Frais de notaire (%)';
$gl_caption_utility['c_mortgage_open_percent']     = 'Ouverture (%)';
$gl_caption_utility['c_mortgage_percent']          = 'Pourcentage prix hypothécaire (%)';
$gl_caption_utility['c_mortgage_register_percent'] = 'Enregistrement de l\'hypothèque (%)';
$gl_caption_utility['c_mortgage_valuation']        = 'Taxes (€)';
$gl_caption_utility['c_notary_percent']            = 'Frais de notaire (%)';
$gl_caption_utility['c_register_percent']          = 'Frais d\'inscription (%)';

/* Places*/
$gl_caption_place['c_place'] = 'Bureau';
$gl_caption_place['c_status'] = 'État';
$gl_caption_place['c_prepare'] = 'En préparation';
$gl_caption_place['c_review'] = 'Pour examiner';
$gl_caption_place['c_public'] = 'Public';
$gl_caption_place['c_archived'] = 'Filed';

$gl_caption_place['c_address'] = 'Adresse';
$gl_caption_place['c_adress'] = 'Rue/Avenue';
$gl_caption_place['c_numstreet'] = 'Numéro';
$gl_caption_place['c_block'] = 'Escalier';
$gl_caption_place['c_flat'] = 'Étage';
$gl_caption_place['c_door'] = 'Porte';
$gl_caption_place['c_zip'] = 'Code postal';

$gl_caption_place['c_zone_id'] = 'Zone';
$gl_caption_place['c_provincia_id'] = 'Province';
$gl_caption_place['c_comarca_id'] = 'Comarca';
$gl_caption_place['c_municipi_id'] = 'Commune';
$gl_caption_place['c_country_id'] = 'Pays';