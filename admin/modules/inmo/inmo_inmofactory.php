<?
/**
 * InmoHabitatsoft
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2011
 * @version $Id$
 * @access public
 */
 /*
FEATURES
	
	Nomes seveix per importar el primer cop, no actualitza
    Nomes s'ha fet per easybrava, per tant hi ha coses que no aniran amb tothom
  * 
  DEMANDES
  * Es de venta quan hi ha preu venta o no hi ha cap preu
  * Es de lloguer quan hi ha preu lloguer
  * Hi ha demandes sense municipis
  * Preu demanda agafo el més alt
  * Hi ha demandes sense preu  
  * Superfície, habitacions, banys, aseos, agafo la més baixa
  * Observacions -> observacions | notes

  CUSTOMERS
  * L'agent nomes posa el nom duplicat, per tant falla si hi ha 2 joans o 2 peps, etc...
  * El DNI, VAT CIF hi posa aquest text davant, el suprimeixo però pot haver altres texts
  * Observacions -> Obsevacions | Contacto 1.Teléfono | Contacto 1.Móvil
 
  PROPERTY
  * Observacions -> Observacions | Ubicacion llaves + Comentarios llaves | Portales publicados | Notas

PER FER
	
	property -> tipus -> sell?
  * demand -> estado -> activo
  * history - afagir estats
  * hores d'inici i fi?
  * al canviar zona de comarca, canviar en tots els inmobles
  * Joan Carles, Joan Anton, etc, tots els noms compostots fallen
  * Que es price 0 maxprice a demandes?
  * 
PER ELLS
	
  * 2 inmobles sense referencia
  * demandes sense municipi
  * clients repetits
  * comprovar inmobles en cada categoria
  * Posar nom i contrasenya per cada agent
  * Hi ha 3 clients buits, pero els deixo ja que queden relacionats amb demandes que no tenen nom de client
		
CANVIS EN BBDD
	
	
LETND NO TE
	

HABITATSOFT NO TE
	
  
  
  
VARS
	
*/
class InmoInmofactory extends Module{
	var $conta = 0;
	var $inmofactory_table = 0;
	var $letnd_id = 0;
	var $demand_id = 0;
	var $demand_tipus = '';
	var $propertys_added = array();
	var $errors = array();
	// el key es l'id a inmofactory, i el val l'id a letnd
	var	$inmofactory_categorys = array(
			'Casa|Adosada' => '22', //
			'Piso|Apartamento' => '12', //
			'Piso|Ático' => '14', //
			'Casa|Casa' => '3', // -> a casa urbana
			'Casa|Casa de pueblo' => '2', //
			'Casa|Casa rural' => '1', //
			'Casa|Casa rústica' => '2', 
			'Casa|Chalet/Torre' => '13', //
			'Piso|Dúplex' => '15', //
			'Parking|Individual' => '26', //
			'Casa|Masía' => '1', //
			'Casa|Pareada' => '23', //
			'Piso|Piso' => '4', //
			'Piso|Planta baja' => '25', //
			'Suelo|Residencial' => '5', // 
			'Casa|Unifamiliar' => '24', //
			'Casa|' => '3', //
			'Local|' => '7', //
			'Parking|' => '10', //
			'Piso|' => '4', //
			'Suelo|' => '5', //
			);
	var	$inmofactory_status = array(
			'Disponible' => 'onsale',
			'No disponible' => 'archived'
			);
	var	$inmofactory_facing = array(
			'Este' => 'e',
			'Oeste' => 'o',
			'Sur' => 's',
			'Sureste' => 'se',
			'Suroeste' => 'so',
			'Norte' => 'n',
			'Noroeste' => 'no',
			);
	var	$inmofactory_efficiency = array(
			'En trámite' => 'progress',
			'A' => 'a',
			'B' => 'b',
			'C' => 'c',
			'D' => 'd',
			'E' => 'e',
			'F' => 'f',
			'G' => 'g',
			);
	var	$inmofactory_marital = array(
			'Indefinido' => '',
			'Casado' => 'married',
			'Separado Judicialmente' => 'separated',
			);
	var	$inmofactory_languages = array(
			'Alemán' => 'deu',
			'Catalán' => 'cat',
			'Español' => 'spa',
			'Francés' => 'fra',
			'Inglés' => 'eng',
			);
	var	$inmofactory_countrys = array(
			'España' => 'ES',
			'Francia' => 'FR',
			'Andorra' => 'AD',
			);
	var	$inmofactory_hystory_tipus = array(
			'Atención oficina' => 'office',
			'Solicitud de información' => 'send_information',
			'Llamada Cliente' => 'to_call',
			'Llamada Propietario' => 'to_call',
			'Oferta económica' => 'offer',
			'Seguimiento' => 'various',
			'xxxxx' => 'waiting_call',
			'Reunión Cliente' => 'assembly',
			'Visita' => 'show_property',
			'Primera visita' => 'show_property',
			'Segunda visita' => 'show_property',
			'xxxxx' => 'like_property',
			'e-mail' => 'send_information',
			'Enviar e-mail' => 'send_information',
			'xxxxx' => 'demand_refused',
			'xxxxx' => 'demand_reseted',
			);
	var	$inmofactory_hystory_status = array(
			'Anulada' => 'canceled',
			'Pendiente' => 'sort_out',
			'Realizada' => 'finished',
			);
	function __construct(){
		parent::__construct();
		
		// inicio output
		echo "
		<html>
		<head>
		<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
		<style>
			.m1{
				font-size:20px;
				font-weight: bold;
				margin: 20px 0 10px 0;
			}
			.m2{				
				font-size:12px;
				margin:5px 0 0 20px;
				font-weight: bold;
				color:#484848;
			}
			.m2 i, .m1 i{	
				font-weight: normal;
				color:#706f6f;
			}
			.m3{				
				font-size:11px;
				margin:0 0 0 30px;
				color:#484848;
			}
			.m4{				
				font-size:11px;
				margin:0 0 0 40px;
				color:#484848;
			}
			.merror{				
				font-size:20px;
				margin:0 0 0 0;
				color:red;
			}
		</style>
		<script>
			var a = true;
			function moveWin(){
				window.scrollTo(0, document.body.scrollHeight);
				if (a) t = setTimeout('moveWin();',100);
			}			
		</script>
		</head>
		<body onload=''>
		<script>
			moveWin();		
		</script>";
		
		
		include (DOCUMENT_ROOT . 'common/includes/phpoffice/PHPExcel.php');
	}
	function show_end(){
		$this->p('Importació finalitzada  - <i>' . $this->conta . ' operacions en ' . $execution_time = Debug::get_execution_time() . ' segons </i>', 1);
		echo "
		<script>
			a = false;	
		</script></body></html>";
	}
	
	function import(){		
		set_time_limit(0);		//ini_set('implicit_flush', true);		//error_reporting(E_WARNING);
		$this->empty_tables(); // buido algunes les taules per insertar tot de nou
		//$this->import_field_names();
		
		$this->import_data();
		
		$this->add_languages(); // idiomes que falten
		//$this->insert_remove_keys();
		
		$this->show_end();	
		//send_mail_admintotal('Actualitzacio habitatsoft','Actualització finalitzada');
		die();
	}
	
	function empty_tables(){
		$this->p('Buidant taules fixes', 1);		
		
		$query = "
		TRUNCATE `inmo__property`;
		ALTER TABLE `inmo__property` AUTO_INCREMENT=1;

		TRUNCATE `inmo__property_file`;
		ALTER TABLE `inmo__property_file` AUTO_INCREMENT=1;

		TRUNCATE `inmo__property_language`;

		TRUNCATE `inmo__property_image`;
		ALTER TABLE `inmo__property_image` AUTO_INCREMENT=1;

		TRUNCATE `inmo__property_image_language`;

		TRUNCATE `inmo__zone`;
		ALTER TABLE `inmo__zone` AUTO_INCREMENT=1;


		TRUNCATE `custumer__custumer`;
		ALTER TABLE `custumer__custumer` AUTO_INCREMENT=1;

		TRUNCATE `custumer__custumer_file`;
		ALTER TABLE `custumer__custumer_file` AUTO_INCREMENT=1;

		TRUNCATE `custumer__custumer_to_property`;

		TRUNCATE `custumer__demand`;
		ALTER TABLE `custumer__demand` AUTO_INCREMENT=1;

		TRUNCATE `custumer__demand_to_property`;
		
		TRUNCATE `custumer__demand_to_category`;

		TRUNCATE `custumer__history`;
		ALTER TABLE `custumer__history` AUTO_INCREMENT=1;

		DELETE FROM user__user WHERE user_id > 3;
		ALTER TABLE `user__user` AUTO_INCREMENT=4;
		
		ALTER TABLE `inmo__property`
			CHANGE COLUMN `ref` `ref` VARCHAR(50) NOT NULL AFTER `property_id`;
		";
		
		$query = explode(';', $query);
		
		foreach ($query as $q){
			if (trim($q)) {
				Db::execute($q);
				$this->p($q, 2);
			}
		}
	
	}
	// nomes el primer cop per poder tindre els camps de l'excel en la bbdd
	function import_field_names(){
		
		return;//////////////////////////////////////////////////////////
		//////////////////////////////////////////////////////////////////
		
		$files = array('Action','Agents','Customers','Demand','Property');
		
		//$dir = dir(CLIENT_PATH . '/habitatsoft');	
		// Faig bucle per cada arxiu TXT, la primera linea son columnes, la resta son registres
		foreach ($files as $file)
		{
		
			$this->inmofactory_table = $file;
			// objecte excel
			$objPHPExcel = PHPExcel_IOFactory::load(CLIENT_PATH . '/inmo/import/'.$file.'.xlsx');
			$results = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
			
			
			$this->p('Processant taula: ' . $this->inmofactory_table, 1);

			$conta = 0;
			foreach ($results as $rs) {				
				
				foreach($rs as $k=>$v){
					$this->p('Processant camp: ' . $v, 2);
					
					$query = "INSERT INTO `letnd`.`export__inmofactory` 
							(`inmofactory_table`, `inmofactory_field`, `inmofactory_excelcol`) 
						VALUES 
							('$file', '$v', '$k');";
					
					Db::execute($query);
					
				}
				break;
				$conta++;
			}
			
			
			
			// inicialitzo variables de cada Taula (TXT)
			$column_names = array();
			$keys = array();					
		}
	}
	function import_data(){
	
		$files = array('Agents','Customers','Property','Demand','Action');
		
		//$dir = dir(CLIENT_PATH . '/habitatsoft');	
		// Faig bucle per cada arxiu TXT, la primera linea son columnes, la resta son registres
		foreach ($files as $file)
		{
		
			$this->inmofactory_table = $file;
			// objecte excel
			$objPHPExcel = PHPExcel_IOFactory::load(CLIENT_PATH . '/inmo/import/'.$file.'.xlsx');
			$results = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
			
			
			$this->p('Processant taula: ' . $this->inmofactory_table, 1);

			$conta = 0;
			foreach ($results as &$data) {				
				
				// noms columnes
				if ($conta==0){
					Db::connect_mother();
					foreach ($data as $key=>$field){
					
						// busco el nom de la taula i camp a la bbdd de relacions
						$query = "SELECT inmofactory_field, letnd_table, letnd_field, function
							FROM export__inmofactory
							WHERE letnd_table <>  '' AND
							inmofactory_table= '".$this->inmofactory_table."'
							AND inmofactory_field= '". $field ."'
							ORDER BY inmofactory_excelcol";
						$rs = Db::get_row($query);
						
						// nomès faig algo quan està definida 'letnd_table', sino passo del camp
						if ($rs) {
							$rs['key'] = $key;
							$column_names[$field]= $rs;
							
							// per relacionar noms columna ($column_names) amb registre ($row_inmofactory)
							$keys[$key] = $field;
						}
					}
					Db::reconnect();
					//debug::p($column_names);
				}
				// dades, les agrupo per letnd_table
				else{
					//Debug::p($column_names, '$column_names');
					//Debug::p($keys, '$keys');
					// inicialitzo variables per cada registre
					$this->p('Registre ' . $conta, 2);
					$row_inmofactory = array();
					$this->habitatsoft_id = 0;
					$this->letnd_id = 0;
					$this->demand_id = 0;
					
					foreach ($data as $key=>$field){
						// si tinc definit letnd_table i letnd_field pèr aquest camp el guardo, sino passo
						// un camp pot tindre una funcio definida, com ara get_boolean que canvia -1 per 1
						
						/*
						 * $key -> A, B , C , D
						 * $k -> nom a capçalera (Id de inmueble, Referencia, Tipo)
						 */
						
						$k = isset($keys[$key])?$keys[$key]:false; // 
						if ($k && $column_names[$k]['letnd_field']){
							
							if ($k=='Id de inmueble') {
								$this->letnd_id = $field;
							}
							
							//$this->p('Columna ' . $k, 3);
						
							if ($column_names[$k]['function']) 
								$field = $this->$column_names[$k]['function']($field,$data,$column_names,$row_inmofactory);
								//$field = call_user_func (array(&$this, $column_names[$k]['function']),$field);
							
							$row_inmofactory[$column_names[$k]['letnd_table']][$k]=$field;
						}
					}

					// En acabar de mirar totes les dades del registre faig els inserts que calguin per aquest registre
					// Aquí es crida una funció per tot el grup de camps que van a una taula mateixa taula de letnd, ej. tots els camps que van a inmo__property criden la funcio inmo__property
					foreach($row_inmofactory as $letnd_table=>$row){
						//call_user_func_array (array(&$this, $letnd_table),array(&$row,&$column_names));
						$this->$letnd_table($row,$column_names);
					}
					//debug::p($row_inmofactory, 'Rows_arr');
					// PROVES if ($conta==10) break;
					
				}
				$conta++;
			}
			
			
			
			// inicialitzo variables de cada Taula (TXT)
			$column_names = array();
			$keys = array();					
		}
	}
	// giro el row per poder insertar directament a bbdd
	function get_letnd_row(&$row, &$column_names){
		$new_row = array();
		foreach ($row as $key=>$value){
			$new_row[$column_names[$key]['letnd_field']] = $value;
		}
		return $new_row;
	}
	/*
	 * 
	 * 
	 *  FUNCIONS PER CAMP
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 */	
	
	// inmo__property -> get_category_id
	function get_category($val, &$data, &$column_names){
		
		// si no està dins l'array, retorno per defecte el primer
		
		$id_categoria = $column_names['Categoría']['key'];
		
		$key = $val . '|' . $data[$id_categoria];
		
		$var = 'inmofactory_categorys';
		if (isset($this->inmofactory_categorys[$key]))
			$ret = $this->inmofactory_categorys[$key];
		else{
			$ret = 0;
			$this->p("No s'ha trobat cap ".$var." per: " . $key, 'error');
		}
		
		return $ret;
	}
	function get_tipus2($val){
		// si no està dins l'array, retorno per defecte el primer
		return $val=='No'?'second':'new';
	}
	function get_status($val){
		
		$var = 'inmofactory_status';
		$v = &$this->inmofactory_status;
		if (isset($v[$val]))
			$ret = $v[$val];
		else{
			$ret = 'prepare';
			$this->p("No s'ha trobat cap ".$var." per: " . $val, 'error');
		}
		return $ret;
		
	}	
	function get_facing($val){
		
		$var = 'inmofactory_facing';
		$v = &$this->inmofactory_facing;
		if (isset($v[$val]))
			$ret = $v[$val];
		else{
			$ret = '';
			if ($val) $this->p("No s'ha trobat cap ".$var." per: " . $val, 'error');
		}
		return $ret;
		
	}	
	function get_efficiency($val){
		
		$var = 'inmofactory_efficiency';
		$v = &$this->inmofactory_efficiency;
		if (isset($v[$val]))
			$ret = $v[$val];
		else{
			$ret = 'progress';
			if ($val) $this->p("No s'ha trobat cap ".$var." per: " . $val, 'error');
		}
		return $ret;
	}	
	// inmo__property -> get_boolean
	function get_boolean($val){
		return $val=='No'?'0':'1';
	}
	// dates
	function format_datetime($val, &$data, &$column_names){
		// si no està dins l'array, retorno per defecte el primer
		$date =  format_datetime($val) . ':00';
		$date = unformat_datetime($date);
		return $date;
	}
	// dates
	function format_date($val){
		// si no està dins l'array, retorno per defecte el primer
		
		if (strpos($val, '-')===false){		
			$val = unformat_date($val,false);
		}
		else{
			$arr = explode('-',$val);
			$val = $arr[2] . '/' . $arr[0] . '/' . $arr[1];
		}
		return $val;
	}
	function get_municipi_id ($val, &$data, &$column_names) {		
		
		$val = trim($val);
		if (!$val) return;
		
		Db::connect_mother();
		
		$municipi = $val;
		$query = "SELECT municipi_id 
						FROM `letnd`.`inmo__municipi` 
						WHERE inmofactory_municipi = ".Db::qstr($municipi);		
		
		$municipi_id = Db::get_first($query);
		
		if (!$municipi_id){
			
			$query = "SELECT municipi_id 
						FROM `letnd`.`inmo__municipi` 
						WHERE municipi = ".Db::qstr($municipi);		
			
			$municipi_id = Db::get_first($query);
			
		}
		/*
		if ($municipi_id=='1'){
			$this->p('Error: ' . $municipi_id,'error');
			die($query );
		}
		 * 
		 */
		
		if (!$municipi_id){
			$this->p("No s'ha trobat cap municipi per: " . $val, 'error');		
		}
		
		Db::reconnect();
		
		return $municipi_id;
		
		
	}
	function get_municipi_demand ($val, &$data, &$column_names) {
		
		return $this->get_municipi_id ($val, $data, $column_names);
		
	}
	function get_municipi_register ($val, &$data, &$column_names) {
		
		return $this->get_municipi_id ($val, $data, $column_names);
		
	}
	function get_municipi ($val, &$data, &$column_names) {
		
		$col_name = $column_names['Población']['key'];
		$municipi = $data[$col_name];
		$municipi_id = $this->get_municipi_id ($municipi, $data, $column_names);		
		$data[$col_name] = $municipi_id;
		
		$val = trim($val);
		
		$zone_id = 0;
		
		if ($val){
			// zona
			$query = "SELECT zone_id 
							FROM inmo__zone 
							WHERE zone = ".Db::qstr($val);	
			$zone_id = Db::get_first($query);

			if (!$zone_id){

				$query = "INSERT 
							INTO inmo__zone (`municipi_id`, `zone`) 
							VALUES (".$municipi_id.", ".Db::qstr($val).")";

				Db::execute($query);
				$zone_id = Db::insert_id();

				$this->p("Zona afegida:" . $val, 4);

			}
		}		
		
		return $zone_id;
		
	}
	// passo any a data
	function get_construction_date($val){		
		return $val . '/01/01';
	}
	function get_user_id($val, &$data, &$column_names) {			
		
		if (!$val) return;
		
		$id = Db::get_first("
			SELECT user_id FROM user__user WHERE concat(name,' ',surname) = ".Db::qstr($val)				
		);
		
		if (!$id)
			$this->p("No s'ha trobat cap user amb aquest nom:" . $val,'error');
		
		return $id;
	}
	
	function get_custumer_id($val, &$data, &$column_names) {	
		
		$ret = Db::get_first("
			SELECT custumer_id FROM custumer__custumer WHERE concat(name,' ',surname1) = ".Db::qstr($val)				
		);
		
		if (!$ret)
			$this->p("No s'ha trobat cap client amb aquest nom:" . $val,'error');
		
		return $ret;
	}
	
	function get_property_observations ($val, &$data, &$column_names) {
		
		$observations = trim($val);
		
		$col_name = $column_names['Ubicación llaves + Comentarios llaves']['key'];
		$claus = $data[$col_name];
		
		$col_name = $column_names['Portales publicados']['key'];
		$portals = $data[$col_name];
		
		$col_name = $column_names['Notas']['key'];
		$notes = $data[$col_name];
				
		if ($notes){
			if ( $observations) $observations .="\n\n\n";
			$observations .= 
"Notas
--------------------

" . $notes;
		}
		
		if ($claus){
			if ( $observations) $observations .="\n\n\n";
			$observations .= 
"Ubicación llaves + Comentarios llaves
--------------------------------------

" . $claus;
		}
		
		if ($portals){
			if ( $observations) $observations .="\n\n\n";
			$observations .= 
"Portales publicados
--------------------

" . $portals;
		}
		
		
		
		return $observations;
		
	}
	
	// user__user
	function get_name($val, &$data, &$column_names) {	
		
		$pos = strpos($val,' ');
		
		if ($pos){
			$surname = substr($val, $pos+1);
			$val = substr($val, 0, $pos);
		}
		else
			$surname = '';
		
		
		$col_name = $column_names['Idioma']['key'];
		$data[$col_name] = $surname;
		
		return $val;
	}
	
	
	// customer_customer	
	
	function get_custumer_user_id($val, &$data, &$column_names) {	
		
		if (!$val) return;
		
		$val = explode(' ',$val);
		$val = $val[0];
		
		if (!$val){
			$this->p("Explode de custumer user no ha funcionat be",'error');
			return;
		}
		
		$id = Db::get_first("
			SELECT user_id FROM user__user WHERE name = ".Db::qstr($val)				
		);
		
		if (!$id)
			$this->p("No s'ha trobat cap user amb aquest nom:" . $val,'error');
		
		return $id;
	}
	
	function get_customer_name($val, &$data, &$column_names, &$row_inmofactory) {	
		
		$pos = strpos($val,' ');
		
		if ($pos){
			$surname = substr($val, $pos+1);
			$val = substr($val, 0, $pos);
		}
		else
			$surname = '';
		
		// columna nomes a letnd
		$row_inmofactory['custumer__custumer']['zz'] = $surname;
		$column_names['zz']['letnd_field'] = 'surname1';
		
		return $val;
	}
	function get_marital($val){
		
		//if (!$val) return;
		
		$var = 'inmofactory_marital';
		$v = &$this->inmofactory_marital;
		if (isset($v[$val]))
			$ret = $v[$val];
		else{
			$ret = '';
			if ($val) $this->p("No s'ha trobat cap ".$var." per: " . $val, 'error');
		}
		
		return $ret;
		
	}
	
	function get_vat($val){
		
		if($val=='Indefinido') $val = '';
		$val = str_replace('DNI', '', $val);
		$val = str_replace('NIF', '', $val);
		$val = str_replace('CIF', '', $val);
		$val = str_replace('Otros', '', $val);		
		
		return trim($val);
	}
	function get_language($val){
		
		$var = 'inmofactory_languages';
		$v = &$this->inmofactory_languages;
		if (isset($v[$val]))
			$ret = $v[$val];
		else{
			$ret = 'cat';
			if ($val) $this->p("No s'ha trobat cap ".$var." per: " . $val, 'error');
		}
		
		return $ret;
	}
	function get_country($val){
		
		$var = 'inmofactory_countrys';
		$v = &$this->inmofactory_countrys;
		if (isset($v[$val]))
			$ret = $v[$val];
		else{
			$ret = 'ES';
			if ($val) $this->p("No s'ha trobat cap ".$var." per: " . $val, 'error');
		}
		
		return $ret;
		
	}
	
	function get_customer_observations2($val, &$data, &$column_names) {	
		
		
		$nom = $val;
		
		$col_name = $column_names['Contacto 1.Teléfono']['key'];
		$tel = $data[$col_name];
		
		$col_name = $column_names['Contacto 1.Móvil']['key'];
		$tel2 = $data[$col_name];
		
		$val = '';
		if ($nom)
			$val .=$nom;
		
		if ($val && $tel)
			$val .=', ';
		
		if ($tel)
			$val .=$tel;
		
		if ($val && $tel2)
			$val .=', ';
		
		if ($tel2)
			$val .=$tel2;
			
		
		return $val;
	}
	
	// customer_demand	
	
	function get_demand_lower($val) {
		
		if ($val=='' || $val=='-') return '0';
		
		$arr = explode(' - ',$val);
		
		$arr[0] = unformat_decimal($arr[0], false);
		if (isset($arr[1])) {
			$arr[1] = unformat_decimal($arr[1], false);
			// el més petit
			$ret = $arr[1]>$arr[0]?$arr[0]:$arr[1];
		}
		else{
			$ret = $arr[0];
		}
		
		return $ret;
	}
	
	function get_demand_price($val, &$data, &$column_names) {
		
		$this->demand_tipus = 'sell';
		
		if($val=='-'){			
			
			$col_name = $column_names['Precio (Alquiler)']['key'];
			$val = $data[$col_name];
			
			$this->p('Precio alquiler:' . $val,3);
			
			$this->demand_tipus = 'rent';
		}
		
		if ($val=='' || $val=='-') {
			$this->demand_tipus = 'sell';
			// $this->p('No hi ha preu a demanda','error'); // no cal, alguns no tenen preu
			return '0';
		}
		//$this->p('PREU:' . $val,3);
		$arr = explode(' - ',$val);
		
		$arr[0] = unformat_decimal($arr[0], false);
		
		if (isset($arr[1])) {
			$arr[1] = unformat_decimal($arr[1], false);			
			// el més gran
			$ret = $arr[1]>$arr[0]?$arr[1]:$arr[0];
		}
		else{
			$ret = $arr[0];
		}
		
		//$this->p('PREU:' . $ret,3);
		
		return $ret;
		
	}
	
	
	function get_demand_observations ($val, &$data, &$column_names) {
		
		$observations = trim($val);
		
		
		$col_name = $column_names['Notas']['key'];
		$notes = $data[$col_name];
				
		if ($notes){
			if ( $observations) $observations .="\n\n\n";
			$observations .= 
"Notas
--------------------

" . $notes;
		}
		
		
		
		return $observations;
		
	}
	
	
	// customer_history	
	
	function get_history_tipus($val) {		
				
		
		$var = 'inmofactory_hystory_tipus';
		$v = &$this->inmofactory_hystory_tipus;
		if (isset($v[$val]))
			$ret = $v[$val];
		else{
			$ret = 'various';
			if ($val) $this->p("No s'ha trobat cap ".$var." per: " . $val, 'error');
		}
		
		return $ret;
		
	}
	
	function get_history_status($val) {		
				
		$var = 'inmofactory_hystory_status';
		$v = &$this->inmofactory_hystory_status;
		if (isset($v[$val]))
			$ret = $v[$val];
		else{
			$ret = 'sort_out';
			if ($val) $this->p("No s'ha trobat cap ".$var." per: " . $val, 'error');
		}
		
		return $ret;
		
	}
	
	function get_property_id($val) {		
		
		if (!$val) return;
		
		$ret = Db::get_first("SELECT property_id FROM inmo__property WHERE ref = " . Db::qstr($val));
		
		if (!$ret) $this->p('No es troba la referencia: ' . $val,'error');
		
		return $ret;
	}
	
	function format_history_date($val, &$data, &$column_names) {	
		
		$col_name = $column_names['Hora de inicio']['key'];
		$time = $data[$col_name];
		
		$val = $this->format_date($val);
		$time = unformat_time($time);
		
		$val = $val . ' ' . $time;
		
		return $val;
	}
	
	function format_history_finished_date($val, &$data, &$column_names) {	
		
		$col_name = $column_names['Hora de fin']['key'];
		$time = $data[$col_name];
		
		$val = $this->format_date($val);
		$time = unformat_time($time);
		
		$val = $val . ' ' . $time;
		
		return $val;
	}
	
	
	function get_history ($val, &$data, &$column_names) {
		
		$observations = trim($val);
		
		
		$col_name = $column_names['Notas']['key'];
		$notes = $data[$col_name];
				
		if ($notes){
			if ( $observations) $observations .="\n\n\n";
			$observations .= 
"Notas
--------------------

" . $notes;
		}
		
		
		
		return $observations;
		
	}
	
	
	/*
	 * 
	 * 
	 *  FUNCIONS PER TAULA
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 */	
	
	function inmo__property(&$row, &$column_names){
		$this->p('TAULA inmo__property ', 3);		
		
		$new_row = $this->get_letnd_row ($row, $column_names);
		// afegeixo camps nomès de letnd
		$new_row['land_units'] = 'm2';
		
		$this->get_comarca_id($new_row);
		
		
		
		// El primer cop que faig una exportacio, actualitza les propietats amb la 'ref' = 'Expediente', per mantindre les propietats que ja eren a la web ( si s'ha tingut en compte de posar les mateixes referencies
		$query = "SELECT property_id FROM inmo__property 
					WHERE property_id= " . $new_row['property_id']; 
		$existing_id = Db::get_first($query);		
		
		// ACTUALITZO O INSERTO
		// si existeix la ref, deixo el property_id actual de letnd per poder utilitzar els mateixos links
		if ($existing_id){
			$query = $this->get_update($new_row, 'inmo__property', 'property_id = ' . $existing_id);
			$this->letnd_id = $existing_id;
		}
		// si es un registre nou poso com a property_id el Expediente de habitatsoft ja que es un nombre únic 
		else{
			$this->letnd_id = $new_row['property_id'];
			$query = $this->get_insert($new_row, 'inmo__property');			
		}
		
		Db::execute($query);		
		$this->p($query, 4);
		
		if ($this->letnd_id) $this->propertys_added[] = $this->letnd_id; // compte global de les ids creades o updatejades
	}	
	
	function inmo__property_language(&$row, &$column_names){
		$language = 'spa';
		
		
		$col_name = $column_names['Id de Inmueble']['key'];
		
		$new_row['property_id'] = $this->letnd_id;
		
		$this->common_insert_all($row, $column_names, 'inmo__property_language', $new_row, $language);
	}
	
	function user__user (&$row, &$column_names) {
		
		$new_row['group_id'] = '2';
		$id = Db::get_first("SELECT user_id+1 FROM user__user ORDER BY user_id DESC LIMIT 1");
		$new_row['login'] = 'usuari' . $id;
		$new_row['passw'] = 'usuari' . $id;
		$this->common_insert_all($row, $column_names, 'user__user', $new_row);		
	}
	
	
	function custumer__custumer_to_property(&$row, &$column_names) {
		
		$new_row['property_id'] = $this->letnd_id;
		$this->common_insert_all($row, $column_names, 'custumer__custumer_to_property', $new_row);
		
		
	}
	
	
	function custumer__demand(&$row, &$column_names) {
		$this->demand_id =  $row['Identificador'];
		
		$new_row = $this->get_letnd_row($row, $column_names);
		$new_row['tipus'] = $this->demand_tipus;
		$this->get_comarca_id($new_row);
		
		$query = $this->get_insert($new_row, 'custumer__demand');	
		Db::execute($query );				
		$this->p($query, 4);
	}
	
	
	function custumer__demand_to_category(&$row, &$column_names) {
		
		$cat1 = $row['Tipo inmueble'];		
		$cat2 = $row['Categoría'];
			
		if ($cat2){

			$cat2 = substr($cat2, 0, -1);

			$cat2 = explode(',',$cat2);
			
			foreach ($cat2 as $c2){
				
				$category_id = $this->inmofactory_categorys[$cat1 . '|' . $c2];
				$query = 
						"INSERT INTO `custumer__demand_to_category` (`demand_id`, `category_id`) 
							VALUES ($this->demand_id, $category_id);";
				Db::execute($query );
				$this->p($query, 4);
			}
		
		}
		else{
			$category_id = $this->inmofactory_categorys[$cat1 . '|'];
			$query = 
					"INSERT INTO `custumer__demand_to_category` (`demand_id`, `category_id`) 
						VALUES ($this->demand_id, $category_id);";
			Db::execute($query );
			$this->p($query, 4);
		}
		
	}
	
	
	function custumer__custumer(&$row, &$column_names) {
		$new_row = array();
		$this->common_insert_all($row, $column_names, 'custumer__custumer', $new_row);	
		
	}
	
	
	function custumer__history(&$row, &$column_names) {
		$new_row = array();
		$this->common_insert_all($row, $column_names, 'custumer__history', $new_row);	
		
	}
	
	
	
	
	
	
	
	// funcio comú per registres que primer es borra tot i desprès s'inserta tot sense complicacions d'idiomes ni res
	function common_insert_all(&$row, &$column_names, $table, $other_rows = array(), $language = false){
		$this->p('TAULA ' . $table, 3);
		$new_row = $this->get_letnd_row($row, $column_names);
		
		if($other_rows) $new_row = array_merge ($new_row, $other_rows);
		
		//$new_row['property_id'] = $this->letnd_id;
		
		if ($language) $new_row['language']=$language; // per inmo__property_language		
		
		$query = $this->get_insert($new_row, $table);	
		Db::execute($query );				
		$this->p($query, 4);
	}
	
	
	
	
	
	
	
	
	
	
	
	/*
	 * 
	 * 
	 * 
	 * 
	 * ALTRES
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 */
	
	function get_comarca_id(&$new_row) {
		$municipi_id = $new_row['municipi_id'];
		
		if (!$municipi_id) return;
		
		$comarca_id = Db::get_first("SELECT comarca_id FROM inmo__municipi WHERE municipi_id = " . $municipi_id);
		$provincia_id = Db::get_first("SELECT provincia_id FROM inmo__comarca WHERE comarca_id = " . $comarca_id);
		
		$new_row['comarca_id'] = $comarca_id;
		$new_row['provincia_id'] = $provincia_id;
	}
	
	
	function add_languages(){
		
		// nomes easy brava
		// per funcionar hauriem de comprobar quins idiomes falten realment
		
		$this->p('Afegir idiomes restants', 1);	
		
		$query = " INSERT INTO inmo__property_language (property_id, language) SELECT DISTINCT property_id, 'cat' FROM inmo__property_language;";
		Db::execute($query );
		$this->p($query, 2);
		
		$query = " INSERT INTO inmo__property_language (property_id, language) SELECT DISTINCT property_id, 'eng' FROM inmo__property_language;";
		Db::execute($query );
		$this->p($query, 3);
		
		$query = " INSERT INTO inmo__property_language (property_id, language) SELECT DISTINCT property_id, 'fra' FROM inmo__property_language;";
		Db::execute($query );
		$this->p($query, 3);
		
		$query = " INSERT INTO inmo__property_language (property_id, language) SELECT DISTINCT property_id, 'deu' FROM inmo__property_language;";
		Db::execute($query );
		$this->p($query, 3);
		
	}
	function insert_remove_keys() {
		
		$table = 'custumer__custumer';
		$table_id = 'custumer_id';
		$q1 = "SELECT ". $table_id . " AS id FROM ". $table . " WHERE remove_key = '0'";
		$q2 = "SELECT count(*) FROM ". $table . " WHERE BINARY remove_key = ";
		$q3 = "UPDATE ". $table . " SET remove_key='%s' WHERE  ". $table_id . " = '%s'";
		
		$results = Db::get_rows($q1);
		
		$this->p('Remove keys a custumer', 1);	

		foreach ($results as $rs) {
			$updated = false; 
			while (!$updated){
				$pass = get_password(50);
				$q_exists = $q2 . "'".$pass."'";	

				// si troba regenero pass
				if(Db::get_first($q_exists)){
					$pass = get_password(50);					
					$this->p('Remove keys repetit', 'error');	
							
				}
				// poso el valor al registre
				else{				
					$q_update = sprintf($q3, $pass, $rs['id']);

					Db::execute($q_update);
					
					
					$this->p('Remove keys: ' . $pass, 2);					
					break;
				}
			}
		}
	}

	
	function get_insert(&$row, $table){
		$fields = '';
		$values = '';	
		
		foreach ($row as $key=>$r){
			$fields .= $key . ',';
			$values .= $this->get_str($r) . ',';
		}
		$fields = substr($fields,0,-1);
		$values = substr($values,0,-1);
		
		$query = "INSERT INTO ".$table." 
					(".$fields.") 
					VALUES (".$values.")";
		return $query;
	}
	
	function get_update(&$row, $table, $condition){
		$values = '';	
		
		foreach ($row as $key=>$r){
			$values .= $key . '=' . $this->get_str($r) . ',';
		}
		$values = substr($values,0,-1);
		
		$query = "UPDATE ".$table." 
			SET ".$values."
			WHERE " .
			$condition;
		return $query;
	}
	function get_str($str){
		return "'" . mysqli_real_escape_string( Db::cn(), $str ) . "'";
	}	
	function p ($txt, $level){
		
		if ($level==2) {
			$this->conta++;
			$txt =  $this->conta . ' ' . $txt . ' - <i>(' . $execution_time = Debug::get_execution_time() . ' s.) </i>';
		}
		if ($level == 'error'){
			$txt .= ' (error)';			
		}
		echo '<p class="m'.$level.'">'.$txt.'</p>';
		//@ob_flush();
		flush();
	}
	
}
?>