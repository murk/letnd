<?php

/* 29-3-13
 * 
 * Menu o bloc de categories
 * Nomes va amb USE_FRIENLY_URL
 *  
 */
//$show_images = isset($vars['show_images']) ? $vars['show_images'] : false;
$block_max_results = isset($vars['block_max_results']) ? $vars['block_max_results'] : false; // si no està definit els llista tots tipus menu
$add_dots_length = isset($vars['add_dots_length']) ? $vars['add_dots_length'] : false; // add_dots_length

$order = " ORDER BY ordre ASC, category ";
$limit = $block_max_results?"LIMIT " . $block_max_results:'';

$query = "SELECT category, portfoli__category.category_id as category_id
				FROM portfoli__category_language, portfoli__category
				WHERE portfoli__category_language.category_id = portfoli__category.category_id
				AND language = '" . LANGUAGE . "'
				" . $order .
				$limit;

$results = Db::get_rows($query);
$selected_found = false;
$vars['block_selected'] = (isset($_GET['category_id']))?1:0;

if ($results) {
    $conta = 0;
    $loop_count = count($results);
    $category_id = isset($_GET['category_id']) ? $_GET['category_id'] : false;
    
    foreach ($results as $rs) {

		$l = array();

		// CONTA PORTFOLIS, es mes rapid que posant la consulta com a subquery
		$portfoli_count = Db::get_first("SELECT count(*) 
							FROM portfoli__portfoli, portfoli__portfoli_to_category
							WHERE category_id = " . $rs['category_id'] . "
							AND portfoli__portfoli.portfoli_id = portfoli__portfoli_to_category.portfoli_id
							AND (status = 'public') 
							AND bin = 0
							LIMIT 1");
        
        
		if ($portfoli_count) {
			
			$l['link'] = 
				get_link( array(
						'tool' => 'portfoli', 
						'tool_section' => 'portfoli', 
						'category' => 'category_id',
						'category_id' => $rs['category_id'],
						'detect_page' => true
				));
			
			
			$l['category'] = $l['item'] = $rs['category'];
			$l['category_id'] = $l['id'] = $rs['category_id'];
			$l['class'] = '';
			$l['loop_count'] = $loop_count;
			$l['selected'] = $category_id == $rs["category_id"] && $vars['block_selected'] ? 1 : 0;
			
			if (isset($_GET['category_ids'])) {
				R::escape_array($_GET['category_ids']);
				//Debug::p($_GET['category_ids'], $rs["category_id"]);
				$l['selected'] = in_array($rs["category_id"], $_GET['category_ids']) ? 1 : 0;
				Debug::p($l['selected'], 'text');
			}
			/*
			if ($show_images)
				$l += UploadFiles::get_record_images($rs['category_id'], true, 'portfoli', 'category');
			 * 
			 */

			$l['conta'] = $conta++;

			$loop[] = $l;
		}
        
    }

    if ($loop) {
        $loop[0]['class'] = 'first';
        $loop[count($loop) - 1]['class'] = 'last';
    }
} else {
    $show_block = false;
    return;
}
?>