<?php
// menus eina
define( 'MISSATGE_MENU_NEW', 'Créer un message' );
define( 'MISSATGE_MENU_MISSATGE', 'Messages' );
define( 'MISSATGE_MENU_LIST', 'Parcourir les messages' );
define( 'MISSATGE_MENU_BIN', 'Corbeille' );

// captions seccio
$gl_caption_missatge['c_status']          = 'État';
$gl_caption_missatge['c_subject']         = 'Affaire';
$gl_caption_missatge['c_missatge']        = 'Message';
$gl_caption_missatge['c_blocked']         = '';
$gl_caption_missatge['c_bin']             = '';
$gl_caption_missatge['c_mls_from_id']     = 'De';
$gl_caption_missatge['c_mls_to_id']       = 'Pour';
$gl_caption_missatge['c_creator_user_id'] = 'De';

$gl_caption_missatge['c_status_prepare'] = 'En préparation';
$gl_caption_missatge['c_status_waiting'] = 'À lire';
$gl_caption_missatge['c_status_readed']  = 'Lu';
$gl_caption_missatge['c_status_send']    = 'Envoyé';

$gl_caption_missatge ['c_kind']         = 'Type';
$gl_caption_missatge ['c_kind_mls']     = 'Mls';
$gl_caption_missatge ['c_kind_general'] = 'Général';

$gl_caption_missatge ['c_entered']  = 'Date de création';
$gl_caption_missatge ['c_modified'] = 'Date de modificationn';
$gl_caption_missatge ['c_sent']     = 'Envoyé';

$gl_caption['c_user_id']       = "Ou utilisateur";
$gl_caption_list['c_user_id']  = "Utilisateurs";
$gl_caption['c_group_id']      = "Groupe";
$gl_caption_list['c_group_id'] = "Groupe";

$gl_caption['c_form_level1_title_group_id'] = "Envoyer à:";
$gl_caption['c_form_level1_title_subject']  = "Message";

$gl_caption['c_reply_button'] = $gl_caption['c_reply'] ;
?>
                  

