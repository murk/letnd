<?
/**
 * NewsNewHome
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2010
 * @version $Id$
 * @access public
 */
class NewsNewHome extends Module{

	function __construct(){
		parent::__construct();
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function get_records()
	{	    
		//$this->set_news_fields();
		$listing = new ListRecords($this);
		$listing->condition = "status = 'public'";
		$category_id = R::id('category_id');
		if ($category_id) $listing->condition .= " AND category_id = '" . $category_id  . "'";
		$listing->call_function['content'] = 'add_dots';
		$listing->order_by = 'entered DESC';
		$listing->template = 'news/new_home';
		return $listing->list_records();
	}
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{
	    $this->set_news_fields();
		$show = new ShowForm($this);
		$show->condition = "status = 'public'";
		$show->get_values();

		if ($show->has_results)
		{
			return $show->show_form();
		}
		else
		{
			$this->do_action('list_records');
		}	
	}
	function set_news_fields(){	
		global $gl_config;
		if (!$gl_config['show_entered_time'])
		{
			$this->set_field('entered', 'type', 'date');
		}
	}
}
?>