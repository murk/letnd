<?

/**
 * InvoicePdf
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2016
 * @version $Id$
 * @access public
 *
 *
 * Generadors Sepa
 *
 * https://github.com/congressus/sepa-direct-debit  <- Es aquest
 *
 *
 * https://github.com/AbcAeffchen/Sephpa
 *
 *
 * https://github.com/php-sepa-xml/php-sepa-xml
 *
 * http://www.infosepa.es/Miembros/conversor%20adeudos.aspx
 *
 *
 *
 * En el fichero de presentación de adeudos, ¿es necesario indicar en algún campo del fichero si el deudor es persona física o jurídica?  Ocultar información
 *
 * SÍ, debe indicarse en las etiquetas deudor.
 * ¿Qué formato de mandato se admite en SEPA?  Ocultar información
 *
 * El documento de mandato debe contener los datos obligatorios que se exigen para identificar la orden de domiciliación en SEPA, a saber: la referencia única del mandato, el nombre y domicilio del deudor, el IBAN, y en su caso el BIC, de la cuenta del deudor, el nombre del acreedor, el identificador del acreedor, el tipo de pago, la fecha de firma y la firma del deudor, e indicar claramente que se trata de un mandato de adeudo directo SEPA. El diseño de los mandatos es indiferente.
 *
 * En España se ha actualizado el folleto 50 de la serie de normas y procedimientos bancarios para diseñar una nueva orden de domiciliación adaptada a los requerimientos de los adeudos directos SEPA, ya sea en la versión básica como en la B2B, que recoge un formato abreviado de mandato con los elementos mínimos necesarios para emitir adeudos directos SEPA en cada una de las modalidades.
 * Caduca en 36 mesos
 *
 * ¿Qué debe consignarse en la secuencia de un adeudo?  Ocultar información
 *
 * Según el adeudo responda a una operación de pago único o recurrente, el cliente deberá indicar, en cada caso, el código que corresponda:
 *
 * FRST - Primer adeudo de una serie de adeudos recurrentes.
 * RCUR - Adeudo de una serie de adeudos recurrentes acogidos a un mismo mandato, cuando no se trate ni del primero ni del último.
 * FNAL - Último adeudo de una serie de adeudos recurrentes
 * OOFF - Adeudo correspondiente a una autorización con un único pago
 *
 * El incumplimiento de esta secuencia puede provocar el rechazo de las operaciones.
 *
 *
 */

require_once( DOCUMENT_ROOT . 'common/includes/sepa/sepa-direct-debit/SEPASDD.php' );

class InvoiceSepa {

	var $rs, $errors = [];

	function __construct( &$module ) {

		header( 'Content-type: text/html; charset=utf-8' ); // necessari pel flush
		@ini_set( 'zlib.output_compression', 0 );
		@ini_set( 'implicit_flush', 1 );
		@ob_end_clean();
		ob_implicit_flush( 1 );

		$this->module = &$module;

		$config = array(
			"name"        => "Marc Sanahuja Campanyà",
			"IBAN"        => "ES9300810004300006506660",
			"BIC"         => "BSABESBB",
			"batch"       => true,
			"creditor_id" => "47184738P",
			"version" => 3,
			"validate" => true,
			"currency"    => "EUR"
		);

		try {
			$this->sepa = new SEPASDD( $config );
		} catch ( Exception $e ) {
			echo $e->getMessage();
		}

	}

	// TODO-i Que no es pugui enviar cap de negativa
	private function get_mandate( &$rs ) {

		$custumer_id = $rs['custumer_id'];
		$invoice_id  = $rs['invoice_id'];
		$ret         = [];

		// Busco el que te invoice_id, la segona consulta se'l pot passar per això en faig 2
		$is_first = Db::get_row( "SELECT * FROM invoice__mandate WHERE custumer_id = $custumer_id AND invoice_id = $invoice_id" );
		// Comprobo si el client te un mandate, ordeno per id així obtinc l'últim del client
		$mandate = Db::get_row( "SELECT * FROM invoice__mandate WHERE custumer_id = $custumer_id ORDER BY mandate_id LIMIT 1" );

		if ( $is_first ) {

			$ret['type']        = 'FRST';
			$ret['mandate_ref'] = $is_first['mandate_ref'];
			$ret['mandate_date'] = $is_first['mandate_date'];

		} elseif ( $mandate ) {

			$mandate_id = $mandate['mandate_id'];
			// Si invoice_id, vol dir que no es el de la factura actual, posem recurrent
			if ( $mandate['invoice_id'] ) {
				$ret['type'] = 'RCUR';

			} // Si te mandate però no invoice_id, vol dir que no s'ha utilitzat mail
			else {
				$ret['type'] = 'FRST';

				Db::execute( "
					UPDATE invoice__mandate SET invoice_id = '$invoice_id' 
					WHERE mandate_id = $mandate_id;" );
			}
			$ret['mandate_ref'] = $mandate['mandate_ref'];
			$ret['mandate_date'] = $mandate['mandate_date'];

		} // sino en te el creo automàticament
		else {

			$mandate_date =  date('Y-m-d',strtotime("-1 days")); //poso al dia d'ahir, sino no es poden generar els rebuts el mateix dia, sempre ha de ser un dia després. Igualment la data es inventada, hauria de ser la que hi posi a la fulla firmada pel client
			$mandate_ref        = "LETND-$custumer_id-1";
			$ret['type']        = 'FRST';
			$ret['mandate_ref'] = $mandate_ref;
			$ret['mandate_date'] = $mandate_date;

			Db::execute( "
				INSERT INTO invoice__mandate (custumer_id, mandate_ref, mandate_date, invoice_id) 
				VALUES ($custumer_id, '$mandate_ref', '$mandate_date', $invoice_id)" );

		}

		return $ret;

	}

	private function add_error( $error ) {
		$this->errors [] = $error;
	}

	private function check_payment_error( $custumer_id, $company, &$rs, $mandate, $invoice_number ) {
		$link = "<a href=\"/?menu_id=5003&action=show_form_edit&custumer_id=$custumer_id\">";
		$link2 = "</a>";

		$error = false;

		if ( ! $company ) {
			$error = "$link$custumer_id$link2: no te nom";
		}
		if ( ! $rs['custumer_comta'] ) {
			$error = "$link$company$link2: no te <strong>número de compte</strong>";
		}
		// per si a cas es fes desde invoice, a batch ja no agafa el registre, així es poden fer factures a 0 però que no passi rebut
		if ( $rs['total_price'] == '0.00' ) {
			$error = "$link$company$link2: no te <strong>preu</strong>";
		}
		if ( $rs['bill_due'] == '0000-00-00' ) {
			$error = "$link$company$link2: no te data <strong>venciment</strong>";
		}
		if ( $mandate['mandate_ref'] == '' ) {
			$error = "$link$company$link2: no te <strong>referencia mandat</strong>";
		}
		if ( $rs['bill_date'] == '0000-00-00' ) {
			$error = "$link$company$link2: no te data de <strong>factura</strong>";
		}

		if ( $error ) {
			$this->add_error( "Factura $invoice_number: $error" );
		}
	}

	public function add_payment( $invoice_id ) {

		$this->rs    = $rs = Db::get_row( "SELECT * FROM invoice__invoice WHERE invoice_id = $invoice_id" );
		$custumer_id = $rs['custumer_id'];

		$mandate = $this->get_mandate( $rs );
		$invoice_number = $rs['invoice_serial'] . $rs['invoice_number'];

		if ( $rs['custumer_billing'] == 'citizen' ) {
			$company = $rs['custumer_name'] . ' ' . $rs['custumer_surname1'] . ' ' . $rs['custumer_surname2'];
		} else {
			$company = $rs['custumer_company'];
		}

		$this->check_payment_error( $custumer_id, $company, $rs, $mandate, $invoice_number );

		// TODO-i Si la data de venciment es menor a avui, posar data avui al vencioment
		$payment = array(
			"name"            => $company,
			"IBAN"            => $rs['custumer_comta'],
			// "BIC"             => "", -> es opcional
			"amount"          => $rs['total_price'] * 100,
			"type"            => $mandate['type'],
			"collection_date" => $rs['bill_due'],
			"mandate_id"      => $mandate['mandate_ref'],
			"mandate_date"    => $mandate['mandate_date'],
			"description"     => "FACTURA LETND " . $invoice_number,
			'end_to_end_id'   => 'LETND-' . $invoice_id
		);

		try {
			$endToEndId = $this->sepa->addPayment( $payment );
		} catch ( Exception $e ) {
			echo $e->getMessage();
		}

	}

	public function output( $file_name = false ) {

		if ( $this->errors ) {
			// TODO-i Quan es facvi el redirect, s'hauràn de borrar els errors si no n'hi ha
			print_javascript( 'top.$("#message_container").show();top.$("#message").html("");' );
			$this->output_js( "Hi ha errors en les factures<br><br>", 'resaltat' );

			foreach ( $this->errors as $error ) {
				$this->output_js( "$error<br><br>", 'vermell' );
			}


			return;
		}

		$rs   = &$this->rs;
		$path = CLIENT_PATH . 'invoice/sepas/';

		if ( ! $file_name ) {
			if ( $rs['custumer_billing'] == 'citizen' ) {
				$file_name = $rs['custumer_name'] . ' ' . $rs['custumer_surname1'] . ' ' . $rs['custumer_surname2'];
			} else {
				$file_name = $rs['custumer_company'];

			}
			$file_name = preg_replace( '/[^a-zA-Z0-9 _.-]/', '', $file_name ) . '_' . $rs['invoice_number'] . '.xml';
		} else {
			$file_name .= '.xml';
		}


		$file = '';
		try {
			$file = $this->sepa->save();
		} catch ( Exception $e ) {
			echo $e->getMessage();
		}

		if ( $file ) {

		} else {
			print_javascript( 'top.alert("No s\'ha pogut generar arxiu SEPA")' );
		}

		file_put_contents( $path . $file_name, $file );

		UploadFiles::serve_download( $path . $file_name, $file_name, true );


		die();
	}

	public function output_js( $message, $class, $encode = false ) {

		$message = json_encode( "<span class=\"$class\">$message</span>" );


		echo( '<SCRIPT LANGUAGE="JavaScript" TYPE="text/javascript">
			parent.Invoice.write_report(' . $message . ');
			</SCRIPT>' );

		// this is to make the buffer achieve the minimum size in order to flush data
		echo str_repeat( ' ', 1024 * 64 );
		ob_flush();
		flush();

	}


}

