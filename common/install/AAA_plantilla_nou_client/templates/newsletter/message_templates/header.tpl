<table id="letnd-header-holder" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td align="center" class="letnd-header-holder">

			<table id="letnd-header" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td class="letnd-header" align="left"><? if ( $preview ): ?><a href="<?= $url ?>">
							<img alt="<?= COMPANY_NAME ?>" src="/<?= CLIENT_DIR ?>/images/logo_newsletter.gif?v=1"/>
							</a><? endif // $preview?><? if ( ! $preview ): ?><a href="<?= $url ?>">
							<img alt="<?= COMPANY_NAME ?>" src="cid:logo"/></a><? endif // $preview?></td>

					<td valign="bottom" align="right">
						<table id="letnd-tel" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td class="letnd-tel">
									<a target="_blank" value="<?= str_replace( ' ', '', PAGE_PHONE ); ?>" href="tel:<?= PAGE_PHONE ?>"><?= PAGE_PHONE ?></a>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>


			<table id="letnd-slogan" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td class="letnd-slogan" align="left"><?= $page_slogan ?></td>
				</tr>
			</table>

		</td>
	</tr>
</table>