<?php
/**
 * Clase Block
 *
 * @author Joan Sanahuja <sanahuja@gmail.com>
 * @version 1.0
 */

/**
 * Classe per gestionar els blokc
 *
 * @author Joan Sanahuja <sanahuja@gmail.com>
 * @version 1.0
 */
class Blocks
{
    /**
     * Template del block
     *
     * @access public
     * @var object
     */
    var $tpl;
    var $title;
    var $script;
    var $vars_string;
    var $vars = array();
    var $place;
	var $active; // puc fer servir diferents blocks en un mateix site ( clumoto i clubmotoshop )
    var $loops=array();
    var $is_automatic = false;

    function __construct(&$tpl = false)
    {
        global $gl_language;
        $this->tpl = new phemplate(PATH_BLOCKS);

        if ($tpl)
        {
			$this->active = $GLOBALS['gl_config']['active_blocks'];
			$query = "SELECT script, vars, template, title, place, all__block.block_id AS block_id FROM all__block, all__block_language
				WHERE all__block.block_id = all__block_language.block_id
				AND language = '" . $gl_language . "'
				AND active = " . $this->active . "
				ORDER BY ordre ASC";

            $results = Db::get_rows($query);
            foreach($results as $rs)
            {
                $this->place = $rs['place'];
                $this->is_automatic = true;
                $this->tpl->set_file($rs['template'] . '_' . $this->place . '.tpl');
                $this->title = $rs['title'];

                $this->vars = array();
                $this->vars_string = $rs['vars'];
	            parse_str( $rs['vars'], $this->vars);

                $this->block_id = $rs['block_id'];
                $this->script = $rs['script'];
                $this->_get_block();
            }
            // posar tots a false per defecte pel error de 'undefined'
            $tpl->set_var('blocks_ht', array());
            $tpl->set_var('blocks_hb', array());
            $tpl->set_var('blocks_t', array());
            $tpl->set_var('blocks_l', array());
            $tpl->set_var('blocks_mt', array());
            $tpl->set_var('blocks_m', array());
            $tpl->set_var('blocks_mb', array());
            $tpl->set_var('blocks_r', array());
            $tpl->set_var('blocks_b', array());

            foreach($this->loops as $key => $loop)
            {
                $tpl->set_loop('blocks_' . $key, $loop);
            }
        }
    }

    /**
     * Blocks::_get_block()
     * Obté el bloc a partir de l'arxiu on hi ha el phop del block i el template on hi ha el disseny del block
     * Desde el php del block es poden fer servir 2 variables per anar al tpl:
     * vars -> totes les variables que vulguem inclure al tpl
     * loop -> el loop del tpl
     * o també fer servir la variable $this referent al block ej: $this->title, $this->tpl->set_var('a', $a);
     * i la variable:
     * $show_form per mostrar o no el form en determinades situacions
     * @return
     */
    function _get_block()
    {
        Debug::add($this->title . ' - ' . $this->block_id,2);
	    Debug::add( 'Block vars', $this->vars );

		global $gl_language, $gl_caption;
        $loop = array();


        $vars = $this->vars;

	    //parse_str($this->vars, $vars); // poso les variables del camp vars de la bbdd a $vars['var1], $vars['var2] ...
        $show_block = true; // per no incloure el bloc si ho poso a false;
        include (DOCUMENT_ROOT . 'public/blocks/' . $this->script . '.php');
        if ($show_block) 
		{
			$this->tpl->set_vars($vars, true);
			$this->tpl->set_loop('loop', $loop);
			$this->tpl->set_vars($gl_caption);
			$this->tpl->set_var('title', $this->title);
			$this->tpl->set_var('block_id', $this->block_id);
			$this->tpl->set_var('place', $this->place);
			$block = $this->tpl->process();
			$count_blocks = isset($this->loops[$this->place])?count($this->loops[$this->place]):0;
			if ($count_blocks==1){
				$this->loops[$this->place][0]['class'] = 'first';
			}
			else if ($count_blocks>1){
				$this->loops[$this->place][$count_blocks-1]['class'] = '';
			}
			$this->loops[$this->place][] = array('block' => $block,'class'=>'last');
		}
    }


	/**
	 * Funció per obtindre només un bloc
	 *
	 * @param string $block_id
	 * @param string $vars Sobre-escriu les variables que pugui haver a la bbdd o per poder disposar en la plantilla
	 *
	 * @param string $template Per utilitzar una plantilla diferent de la definida a la BBDD
	 *
	 * @return string
	 */
	static public function get_block( $block_id, $vars = '', $template = '' )
    {
        global $gl_language;

        $block = new Blocks($tpl);
        $block->tpl = new phemplate(PATH_BLOCKS);

        $query = "SELECT all__block.block_id as block_id, script, template, vars, title, place FROM all__block, all__block_language
				WHERE all__block.block_id = all__block_language.block_id
				AND language = '" . $gl_language . "'
				AND all__block.block_id = '" . $block_id . "'
				ORDER BY ordre ASC";

        $rs = Db::get_row($query);

        if (!$rs){
			Debug::throw_warning("No existeix el block: $block_id");
			return '';
        }

	    parse_str($rs['vars'], $block->vars);

        $block->vars_string = $rs['vars'];

	    if ($vars) {
		    $vars_arr = array();
		    parse_str($vars, $vars_arr);
		    $block->vars = array_merge( $block->vars, $vars_arr );

            $block->vars_string .= ' - ' . $vars;
	    }

	    $template = $template ?: $rs['template'];

        $block->place = $rs['place'];
        $block->tpl->set_file($template . '_' . $block->place . '.tpl');
        $block->title = $rs['title'];

        $block->block_id = $rs['block_id'];
        $block->script = $rs['script'];
        $block->_get_block();

        return isset($block->loops[$block->place][0]['block'])?$block->loops[$block->place][0]['block']:'';
    }
	
    /**
     * Blocks::get_block_link()
     * Enllaç a dins el block
	 * 
	 *		es diferent que desde el modul, dins el modul depen on som hi ha un enllaç o un altre, 
	 *		en el block han de ser els enllaços originals a la fitxa
     * 
     * $link_query per mostrar o no el form en determinades situacions
     * @return
     */
	static public function get_block_link($id, $file_name, $tool, $tool_section, $category, $category_id){
		
		
		$page_file_name = false;
		
		$link_query = '/' . $tool . '/' . $tool_section;
		
		$query = "SELECT  page_file_name FROM all__page INNER JOIN all__page_language USING(page_id) WHERE language='".LANGUAGE."' AND link = '" . $link_query . "'";
		$page_file_name = Db::get_first($query);
		
		// comprovo si existeix /news/new
		
		// sino comprovo si existeix /news/new/category_id/1
		$friendly_params = array();
		if (!$page_file_name && $category){
			$link_query .= '/'.$category.'/' . $category_id;	

			$query = "SELECT  page_file_name FROM all__page INNER JOIN all__page_language USING(page_id) WHERE language='".LANGUAGE."' AND link = '" . $link_query . "'";

			$page_file_name = Db::get_first($query);
			
			$friendly_params[$category] =$category_id;
		}	
		
		
		
		if (USE_FRIENDLY_URL){		
			
			$link = Page::get_link($tool, $tool_section,false,$id,$friendly_params,false,$file_name,false,$page_file_name);
			return $link;
		}
		else{
			$link = '/?tool=inmo&amp;tool_section=property&amp;action=show_record&amp;property_id=' . $id . '&amp;language=' . LANGUAGE;
			
			
			return $link;
		}
		
	}
}

// alias per la funció dins la clase Page
function get_block($block_id, $vars = '', $template = '')
{
    return Blocks::get_block($block_id, $vars, $template);
}
function get_block_vars(){
	if (isset($_GET['block']) && is_file(DOCUMENT_ROOT . 'public/blocks/' . $_GET['block'] . '.php')){
			return DOCUMENT_ROOT . 'public/blocks/' . $_GET['block'] . '_vars.php';
	}
	else {
		return false;
	}
}