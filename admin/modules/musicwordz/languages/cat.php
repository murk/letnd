<?php
// menus eina
define('MUSICWORDZ_MENU_SONG','Cançons');
define('MUSICWORDZ_MENU_PLACE','Sales, festivals');
define('MUSICWORDZ_MENU_PERFORMANCE_LIST','Llistar i editar actuacions');
define('MUSICWORDZ_MENU_PERFORMANCE_BIN','Paperera de reciclatge');
define('MUSICWORDZ_MENU_PERFORMANCE_NEW','Entrar nova actuació');
define('MUSICWORDZ_MENU_SONG_BIN','Paperera de reciclatge');
define('MUSICWORDZ_MENU_PERFORMANCE','Actuacions');
define('MUSICWORDZ_MENU_SONG_LIST','Llistar i editar cançons');
define('MUSICWORDZ_MENU_SONG_NEW','Entrar nova cançó');
define('MUSICWORDZ_MENU_PLACE_NEW','Entrar nova sala | festival');
define('MUSICWORDZ_MENU_PLACE_LIST','Llistar i editar sales | festivals');
define('MUSICWORDZ_MENU_PLACE_BIN','Paperera de reciclatge');

define('MUSICWORDZ_MENU_ONEPLACE', MUSICWORDZ_MENU_PLACE);
define('MUSICWORDZ_MENU_ONEPLACE_NEW', MUSICWORDZ_MENU_PLACE_NEW);
define('MUSICWORDZ_MENU_ONEPLACE_LIST', MUSICWORDZ_MENU_PLACE_LIST);
define('MUSICWORDZ_COMMENT_ONEPLACE', 'Només editar sales | festivals propis de l\'artista');

// captions seccio
$gl_caption_performance['c_performance'] = 'Actuació';
$gl_caption_performance['c_entered'] = 'Creada';
$gl_caption_performance['c_performance_date'] = 'Data actuació';
$gl_caption_performance['c_place_id'] = 'Sala | Festival';
$gl_caption['c_status'] = 'Estat';
$gl_caption['c_status_prepare'] = 'En preparació';
$gl_caption['c_status_review'] = 'Per tramitar';
$gl_caption_place['c_status_review'] = 'Per revisar';
$gl_caption['c_status_approved'] = 'Aprovada';
$gl_caption_song['c_status_approved'] = 'Tramitada';
$gl_caption_performance['c_status_approved'] = 'Declarada';
$gl_caption['c_entered'] = 'Data creació';
$gl_caption['c_modified'] = 'Modificat';

$gl_caption_place['c_place'] = 'Sala | Festival';
$gl_caption_place['c_address'] = 'Adreça';
$gl_caption_place['c_zip'] = 'Codi postal';
$gl_caption_place['c_phone1'] = 'Telèfon';
$gl_caption_place['c_mail'] = 'E-mail';
$gl_caption_place['c_country_id'] = 'País';
$gl_caption_place['c_provincia_id'] = 'Provincia';
$gl_caption_place['c_comarca_id'] = 'Comarca';
$gl_caption_place['c_municipi_id'] = 'Municipi';

$gl_caption['c_song_id'] = 'Cançó';
$gl_caption_song['c_song'] = 'Cançó';
$gl_caption_song['c_durada'] = 'Durada ( mm:ss )';
$gl_caption_song_list['c_durada'] = 'Durada';
$gl_caption['c_user_id'] = 'Artista';
$gl_caption['c_form_level1_title_entered'] =
$gl_caption['c_form_level1_title_performance_date'] = "Dates";


$gl_caption['c_filter_user_id'] = 'Tots els artistes';
$gl_caption['c_filter_status'] = 'Tots els estats';

$gl_caption['c_title_missatges_to_approve'] = "Estan pendents d'aprovar:";
$gl_caption['c_title_missatges_songs_to_approve'] = "Cançons:";
$gl_caption['c_title_missatges_performances_to_approve'] = "Actuacions:";
$gl_caption['c_title_missatges_places_to_approve'] = "Sales | Festivals:";
$gl_caption['c_title_missatges_to_approve_link'] = "Anar al llistat";

$gl_caption['c_artistic_name'] = "Nom artístic";

$gl_caption['c_song_subject'] = 'La cançó "%s" ha estat aprovada';
$gl_caption['c_see_songs'] = "Veure cançons";

$gl_caption['c_performance_subject'] = 'L\'actuació "%s" ha estat declarada';
$gl_caption['c_see_performances'] = "Veure actuacions";

?>