<?php

/**
 * 
 *
 * @version $Id$
 * @copyright 2003 
 **/
function get_calendar(){
global $month_names;
return <<< output_identifier

	<link rel="stylesheet" type="text/css" href="/common/jscripts/calendar/calendar.css">
<script LANGUAGE="JavaScript" src="/common/jscripts/calendar/calendar.js"></script>
<div id="CalendariBach" style="position:absolute; z-index:2; width: 120; visibility: hidden"><iframe width="120" height="150" src="about:blank" frameborder="0" id="calendariFrame"></iframe></div>
<div id="Calendari" style="position:absolute; z-index:3; width: 120; visibility: hidden; ">
  <table border="0" cellspacing="0" cellpadding="0" class="calendari">
  <tr>
    <td align="center">
      <!--calendari inici -->
      <table border="0" cellspacing="2" cellpadding="0" bgcolor="#FFFFFF">
        <tr> 
          <td align="center"> 
            <table border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF">
              <tr> 
                <td><img src="/common/jscripts/calendar/cal_de.gif" width="4" height="4"></td>
                <td background="/common/jscripts/calendar/fons1.gif"><img src="/common/jscripts/calendar/shim.gif" width="1" height="1"></td>
                <td><img src="/common/jscripts/calendar/cal_dd.gif" width="4" height="4"></td>
              </tr>
              <tr> 
                <td background="/common/jscripts/calendar/fons4.gif"><img src="/common/jscripts/calendar/shim.gif" width="1" height="1"></td>
                <td> 
                  <table cellspacing='0' cellpadding='0' border='0'>
                    <tr> 
                      <td> 
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td align="left"> 
                                <table border="0" cellspacing="2" cellpadding="0">
                                  <tr> 
                                    <td class="mes" width="70"> 
                                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr> 
                                          <td class="mes2"> 
                                            <div id="main2" style="position=relative;"> 
                                              December</div>
                                            <div align="left"> 
                                              <div id="MenuMes" style="position:absolute; z-index:1; visibility: hidden; width: 120"> 
                                                <table border="0" cellspacing="0" cellpadding="0">
                                                  <tr> 
                                                    <td><img src="/common/jscripts/calendar/shim.gif" width="1" height="2"></td>
                                                  </tr>
                                                </table>
                                                <table border="0" cellspacing="2" cellpadding="0" bgcolor="#F0FCFF" class="mesos">
                                                  <tr> 
                                                    <td class="mes2"> 
                                                      <div align="left"><a href="javascript:setMonth(1);" class="meslnk">$month_names[0]</a></div>
                                                    </td>
                                                    <td class="mes2"> 
                                                      <div align="left"><a href="javascript:setMonth(7);" class="meslnk">$month_names[6]</a></div>
                                                    </td>
                                                  </tr>
                                                  <tr> 
                                                    <td class="mes2"> 
                                                      <div align="left"><a href="javascript:setMonth(2);" class="meslnk">$month_names[1]</a></div>
                                                    </td>
                                                    <td class="mes2"> 
                                                      <div align="left"><a href="javascript:setMonth(8);" class="meslnk">$month_names[7]</a></div>
                                                    </td>
                                                  </tr>
                                                  <tr> 
                                                    <td class="mes2"> 
                                                      <div align="left"><a href="javascript:setMonth(3);" class="meslnk">$month_names[2]</a></div>
                                                    </td>
                                                    <td class="mes2"> 
                                                      <div align="left"><a href="javascript:setMonth(9);" class="meslnk">$month_names[8]</a></div>
                                                    </td>
                                                  </tr>
                                                  <tr> 
                                                    <td class="mes2"> 
                                                      <div align="left"><a href="javascript:setMonth(4);" class="meslnk">$month_names[3]</a></div>
                                                    </td>
                                                    <td class="mes2"> 
                                                      <div align="left"><a href="javascript:setMonth(10);" class="meslnk">$month_names[9]</a></div>
                                                    </td>
                                                  </tr>
                                                  <tr> 
                                                    <td class="mes2"> 
                                                      <div align="left"><a href="javascript:setMonth(5);" class="meslnk">$month_names[4]</a></div>
                                                    </td>
                                                    <td class="mes2"> 
                                                      <div align="left"><a href="javascript:setMonth(11);" class="meslnk">$month_names[10]</a></div>
                                                    </td>
                                                  </tr>
                                                  <tr> 
                                                    <td class="mes2"> 
                                                      <div align="left"><a href="javascript:setMonth(6);" class="meslnk">$month_names[5]</a></div>
                                                    </td>
                                                    <td class="mes2"> 
                                                      <div align="left"><a href="javascript:setMonth(12);" class="meslnk">$month_names[11]</a></div>
                                                    </td>
                                                  </tr>
                                                </table>
                                              </div>
                                            </div>
                                          </td>
                                          <td bgcolor="#ECE9D8" width="9" align="center" onClick="javascript:MesMenu()" style="cursor:hand;"><img src="/common/jscripts/calendar/fletxabaix.gif" width="7" height="4" border="0"></td>
                                        </tr>
                                      </table>
                                    </td>
                                    <td class="mes" width="11" align="center"> 
                                      <table border="0" cellspacing="0" cellpadding="0" height="12">
                                        <tr> 
                                          <td valign="middle" onClick="javascript:previMes()" style="cursor:hand;"><img src="/common/jscripts/calendar/fletxadalt.gif" width="7" height="4" border="0"></td>
                                        </tr>
                                        <tr> 
                                          <td valign="middle" onClick="javascript:proximMes()" style="cursor:hand;"><img src="/common/jscripts/calendar/fletxabaix.gif" width="7" height="4" border="0"></td>
                                        </tr>
                                      </table>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                              <td align="right"> 
                                <table border="0" cellspacing="2" cellpadding="0">
                                  <tr> 
                                    <td class="mes" width="30"> 
                                      <div id='main' style='position: relative'>1999</div>
                                    </td>
                                    <td class="mes" width="11" align="center"> 
                                      <table border="0" cellspacing="0" cellpadding="0" height="12">
                                        <tr> 
                                          <td valign="middle" onClick="javascript:previAny();" style="cursor:hand;"><img src="/common/jscripts/calendar/fletxadalt.gif" width="7" height="4" border="0"></td>
                                        </tr>
                                        <tr> 
                                          <td valign="middle" onClick="javascript:proximAny();" style="cursor:hand;"><img src="/common/jscripts/calendar/fletxabaix.gif" width="7" height="4" border="0"></td>
                                        </tr>
                                      </table>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                              <td align="right" width="12" valign="top"><a href="javascript:AmagaCalendari()"><img src="/common/jscripts/calendar/tanca.gif" width="9" height="9" border="0"></a></td>
                            </tr>
                          </table>
                      </td>
                    </tr>
                    <tr> 
                      <td valign="top"> 
                        <table border=0 cols=7 cellpadding="1" cellspacing="0" width="100%">
                          <tr class="fosc"> 
                            <td class="dies"><b>L</b></td>
                            <td class="dies"><b>M</b></td>
                            <td class="dies"><b>M</b></td>
                            <td class="dies"><b>J</b></td>
                            <td class="dies"><b>V</b></td>
                            <td class="dies"><b>S</b></td>
                            <td class="dies"><b>D</b></td>
                          </tr>
                        </table>
                        <table width="100%" border="0" cellspacing="1" cellpadding="0">
                          <tr> 
                            <td height="3"></td>
                          </tr>
                        </table>
                        <table border=0 cols=7 cellpadding="0" cellspacing="0">
                          <tr> 
                            <td id="set1pos0" class="cal">&nbsp;</td>
                            <td id="set1pos1" class="cal">&nbsp;</td>
                            <td id="set1pos2" class="cal">&nbsp;</td>
                            <td id="set1pos3" class="cal">&nbsp;</td>
                            <td id="set1pos4" class="cal">&nbsp;</td>
                            <td id="set1pos5" class="calR">&nbsp;</td>
                            <td id="set1pos6" class="calR">&nbsp;</td>
                          </tr>
                          <tr> 
                            <td id="set2pos0" class="cal">&nbsp;</td>
                            <td id="set2pos1" class="cal">&nbsp;</td>
                            <td id="set2pos2" class="cal">&nbsp;</td>
                            <td id="set2pos3" class="cal">&nbsp;</td>
                            <td id="set2pos4" class="cal">&nbsp;</td>
                            <td id="set2pos5" class="calR">&nbsp;</td>
                            <td id="set2pos6" class="calR">&nbsp;</td>
                          </tr>
                          <tr> 
                            <td id="set3pos0" class="cal">&nbsp;</td>
                            <td id="set3pos1" class="cal">&nbsp;</td>
                            <td id="set3pos2" class="cal">&nbsp;</td>
                            <td id="set3pos3" class="cal">&nbsp;</td>
                            <td id="set3pos4" class="cal">&nbsp;</td>
                            <td id="set3pos5" class="calR">&nbsp;</td>
                            <td id="set3pos6" class="calR">&nbsp;</td>
                          </tr>
                          <tr> 
                          <tr> 
                            <td id="set4pos0" class="cal">&nbsp;</td>
                            <td id="set4pos1" class="cal">&nbsp;</td>
                            <td id="set4pos2" class="cal">&nbsp;</td>
                            <td id="set4pos3" class="cal">&nbsp;</td>
                            <td id="set4pos4" class="cal">&nbsp;</td>
                            <td id="set4pos5" class="calR">&nbsp;</td>
                            <td id="set4pos6" class="calR">&nbsp;</td>
                          </tr>
                          <tr> 
                          <tr> 
                            <td id="set5pos0" class="cal">&nbsp;</td>
                            <td id="set5pos1" class="cal">&nbsp;</td>
                            <td id="set5pos2" class="cal">&nbsp;</td>
                            <td id="set5pos3" class="cal">&nbsp;</td>
                            <td id="set5pos4" class="cal">&nbsp;</td>
                            <td id="set5pos5" class="calR">&nbsp;</td>
                            <td id="set5pos6" class="calR">&nbsp;</td>
                          </tr>
                          <tr> 
                          <tr> 
                            <td id="set6pos0" class="cal">&nbsp;</td>
                            <td id="set6pos1" class="cal">&nbsp;</td>
                            <td id="set6pos2" class="cal">&nbsp;</td>
                            <td id="set6pos3" class="cal">&nbsp;</td>
                            <td id="set6pos4" class="cal">&nbsp;</td>
                            <td id="set6pos5" class="calR">&nbsp;</td>
                            <td id="set6pos6" class="calR">&nbsp;</td>
                          </tr>

                        </table>
                      </td>
                    </tr>
                  </table>
                </td>
                <td background="/common/jscripts/calendar/fons2.gif"><img src="/common/jscripts/calendar/shim.gif" width="1" height="1"></td>
              </tr>
              <tr> 
                <td><img src="/common/jscripts/calendar/cal_be.gif" width="4" height="4"></td>
                <td background="/common/jscripts/calendar/fons3.gif"><img src="/common/jscripts/calendar/shim.gif" width="1" height="1"></td>
                <td><img src="/common/jscripts/calendar/cal_bd.gif" width="4" height="4"></td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
      <script language="JavaScript" type="text/javascript">
		<!--
		
		  setCurrentMonth();
		//-->
		</script>
		      <!--calendari fi-->
		    </td>
		  </tr>
		</table>
		</div>
		
output_identifier;
}

?>