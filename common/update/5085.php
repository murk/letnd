<?
/*7-6-13
   Modificacions product - Afegir un altre nivell de subfamilies 
 
 */


if (is_table('product__product') && !is_field('product__product','subsubfamily_id')) {	
	
	$update_sql .= "
		ALTER TABLE `product__product`
			ADD COLUMN `subsubfamily_id` INT(6) NOT NULL DEFAULT '0' AFTER `subfamily_id`;";	
	
	
}


if (is_table('product__product') && !is_field('product__product','price_cost')) {	
	
	$update_sql .= "
		ALTER TABLE `product__product`
	ADD COLUMN `price_cost` DECIMAL(11,2) NOT NULL DEFAULT '0.00' AFTER `pvd5`;";	
	
	
}


?>