<?
/**
 * ProductOrder
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2010
 * @version $Id$
 * @access public
 */
class ProductPayment extends Module{

	var $processes, $is_shop_app = false;
	function __construct(){
		parent::__construct();
	}
	function on_load(){
		$GLOBALS['gl_page']->title = $this->caption['c_payment_title'];
		
		// NO CAL, JA QUE JA ES MIRA SEMPRE SI ESTÂ LOGGED EN AQUEST MODUL
		// si es privada no deixo fer res excepte entrar login i password
		/*if ($this->is_index && $this->config['is_private_shop'] && !$this->is_logged()) {
			redirect('/?tool=product&tool_section=customer&action=login&language='.LANGUAGE);
			die();
		}*/
	}
	// Part 1 process pagament
	function login(){
		if ($this->is_logged()) {
			$this->show_summary();
		}
		else{
			$module = Module::load('product','customer');
			$module->parent='ProductPayment';
			$content = $module->do_action('get_login');
			$GLOBALS['gl_content'] = $this->get_process() . $content;
		}
	}
	function register(){
		if ($this->is_logged()) {
			$this->show_summary();
		}
		else{
			$this->action = 'login'; // nomes es per agafar el process correcte en la barra de process de compra
			$module = Module::load('product','customer');
			$module->parent='ProductPayment';
			$content = $module->do_action('get_form_new');
			$GLOBALS['gl_content'] = $this->get_process() . $content;
		}
	}
	function edit_customer(){
		$module = Module::load('product','customer');
		$module->parent='ProductPayment';
		$content = $module->do_action('get_form_edit');
		$GLOBALS['gl_content'] = $this->get_process() . $content;
	}
	// Part 2 process pagament
	function show_summary(){

		// TODO-i No poder enviar a cap país a can_deliver

		if ($this->is_cart_empty()) return;
		
		$this->process = 'summary'; // nomes es per agafar el process correcte en la barra de process de compra
		$this->set_file('product/payment_summary.tpl', true);

		// lassdive
		if (!$this->is_shop_app) {
			$module   = Module::load( 'product', 'customer' );
			$customer = $module->do_action( 'get_record' );
			$this->set_var( 'customer', $customer );
		}
		// fi exclusió lassdive

		$module = Module::load('product','cart');
		$module->parent = 'ProductPayment';
		$cart = $module->do_action('get_records');
		$this->set_var('cart',$cart);

		$link_payment = $this->link . "&action=show_method&process=method";
		$this->set_var('link_payment',$link_payment);
		$this->set_vars($module->caption);

		$content = $this->process();
		$GLOBALS['gl_content'] = $this->get_process() . $content;
	}
	function get_cart_vars( $order_id ){

		$module = Module::load('product','cart');
		$module->order_id = $order_id;
		$cart_vars = $module->get_cart_vars();

		return $cart_vars;

	}
	// Part 3 process pagament
	function show_method(){
		if ($this->is_cart_empty() && !R::id('order_id')) return;

		// per pagar una comanda que no estava pagada
		$order_id = R::id('order_id');

		$_SESSION['product']['cart_vars']['collect'] = R::post( 'collect', '0' );
		$_SESSION['product']['cart_vars']['customer_comment'] = R::post( 'customer_comment', '' );
		$_SESSION['product']['cart_vars']['is_gift_card'] = R::post( 'is_gift_card', '' );

		$cart_vars = $this->get_cart_vars( $order_id );
		$totals = $cart_vars['totals'];

		$this->caption['c_method_title2'] =
			sprintf($this->caption['c_method_title2'],
				$totals['all_basetax'],
				$this->config['currency_name']);

		$this->set_var( 'rate_encrease_ondelivery_basetax', $totals['rate_encrease_ondelivery_basetax'] );
		$this->set_var( 'rate_basetax', $totals['rate_basetax'] );

		$this->set_vars_method();
		
		// loop per metodes de pagament
		$payment_methods = explode(',',$this->config['payment_methods']);
		$payment_loop = array();
		
		$vars = array(); // per poder fer els links sense un loop, un a un com anaven en principi
		
		if (USE_FRIENDLY_URL){
			$vars = array(
				'paypal_link' => Page::get_link('product', 'payment', 'show_paypal', false ,array('process'=>'method','order_id'=>$order_id), false),
				'4b_link' => Page::get_link('product', 'payment', 'show_4b', false ,array('process'=>'method','order_id'=>$order_id), false),
				'lacaixa_link' => Page::get_link('product', 'payment', 'show_lacaixa', false ,array('process'=>'method','order_id'=>$order_id), false),
				'account_link' => Page::get_link('product', 'payment', 'show_account', false ,array('process'=>'end','order_id'=>$order_id), false),
				'ondelivery_link' => Page::get_link('product', 'payment', 'show_ondelivery', false ,array('process'=>'end','order_id'=>$order_id), false),
				'directdebit_link' => Page::get_link('product', 'payment', 'show_directdebit', false ,array('process'=>'end','order_id'=>$order_id), false));
		}
		else{
			$vars = array(
				'paypal_link' => $this->link . '&action=show_paypal&process=method',
				'4b_link' => $this->link . '&action=show_4b&process=method',
				'lacaixa_link' => $this->link . '&action=show_lacaixa&process=method',
				'account_link' => $this->link . '&action=show_account&process=end',
				'ondelivery_link' => $this->link . '&action=show_ondelivery&process=end',
				'directdebit_link' => $this->link . '&action=show_directdebit&process=end');
		}
		
		foreach ($payment_methods as $method){		
			$payment_loop[] = array(
				'link' => $vars[$method . '_link'],
				'caption' => $this->caption['c_method_' . $method],
				'method' => $method);
		}
		$this->set_vars($vars);
		$this->set_loop('loop',$payment_loop);		
		
		// altres variables per mostrar
		$this->set_var('all_basetax',$totals['all_basetax']);

		$content = $this->process();
		$GLOBALS['gl_content'] = $this->get_process() . $content;
	}
	// Part 3b process pagament PASAREL·LA
	function show_paypal(){
		if ($this->is_cart_empty()) return;
		$this->set_vars_method();
		$order_vars = $this->save_order();

		include('product_payment_paypal.php');
		$payment = new ProductPaymentPaypal($this, $order_vars);
		$this->set_var('paypal_form', $payment->get_content());

		$content = $this->process();
		$GLOBALS['gl_content'] = $this->get_process() . $content;
		//Debug::add('Variables comanda',$order_vars);

	}
	// Part 3b process pagament PASAREL·LA 4b
	function show_4b(){
		if ($this->is_cart_empty()) return;
		$this->set_vars_method();
		$order_vars = $this->save_order();

		include('product_payment_4b.php');
		$payment = new ProductPayment4b($this, $order_vars);
		$this->set_var('c4b_form', $payment->get_content());

		$content = $this->process();
		$GLOBALS['gl_content'] = $this->get_process() . $content;
		//Debug::add('Variables comanda',$order_vars);

	}
	// Part 3b process pagament PASAREL·LA Lacaixa

	/**
	 * @param bool $order_vars Si es passen les variables de la comanda es que es la compra en un sol pas
	 */
	function show_lacaixa( $order_vars = false ){

		$this->set_vars_method();
		// Lassdive
		if ($order_vars){
			$this->set_var('show_lacaixa',true);
		}
		else {
		if ($this->is_cart_empty()) return;
			$order_vars = $this->save_order();
		}

		include('product_payment_lacaixa.php');
		$payment = new ProductPaymentLacaixa($this, $order_vars);
		$this->set_var('lacaixa_form', $payment->get_content());

		$content = $this->process();
		$GLOBALS['gl_content'] = $this->get_process() . $content;
		//Debug::add('Variables comanda',$order_vars);

	}
	function show_account(){
		if ($this->is_cart_empty()) return;
		$order_vars = $this->save_order();
		unset($_SESSION['product']['cart']);
		unset($_SESSION['product']['cart_vars']);

		$this->set_vars(array(
			'all_basetax' => $order_vars['totals']['all_basetax'],
			'deposit' => '',
			'banc_name' => $this->config['banc_name'],
			'account_number' => $this->config['account_number'],
			'concept' => $this->caption['c_order_id'] . ' ' . $order_vars['order_id']));

		// Lassdive
		if ( ! empty( $order_vars['totals']['pay_deposit'] ) ) {
			$this->set_var( 'deposit', $order_vars['totals']['deposit'] );
			$this->set_var( 'deposit_value', $order_vars['totals']['deposit_value'] );
		}

		// TODO-i Posar variable per http o https
		$this->caption['c_text_buyer2'] =
			sprintf($this->caption['c_text_buyer2'],$_SERVER['HTTP_HOST'], $order_vars['order_id'], $_SESSION['product']['customer_id'],'http://'.$_SERVER['HTTP_HOST'].'/?tool=product&tool_section=customer&action=show_login&language=' . LANGUAGE);
		$this->caption['c_text_buyer3'] = $this->caption['c_account_text_buyer'];

		$this->set_vars_method();
		
		$this->set_google_conversion();
		$content = $this->process();
		$GLOBALS['gl_content'] = $this->get_process() . $content;

		$this->send_mails($order_vars['order_id'], 'account');
	}
	function show_ondelivery(){
		if ($this->is_cart_empty()) return;
		$order_vars = $this->save_order();
		unset($_SESSION['product']['cart']);
		unset($_SESSION['product']['cart_vars']);

		$this->caption['c_text_buyer2'] =
			sprintf($this->caption['c_text_buyer2'],$_SERVER['HTTP_HOST'],$order_vars['order_id'], $_SESSION['product']['customer_id'],'http://'.$_SERVER['HTTP_HOST'].'/?tool=product&tool_section=customer&action=show_login&language=' . LANGUAGE);
		$this->caption['c_text_buyer3'] = $this->caption['c_ondelivery_text_buyer'];

		$this->set_vars_method();

		$this->set_google_conversion();
		$content = $this->process();
		$GLOBALS['gl_content'] = $this->get_process() . $content;

		$this->send_mails($order_vars['order_id'], 'ondelivery');
	}
	function show_directdebit(){
		if ($this->is_cart_empty()) return;
		$order_vars = $this->save_order();
		unset($_SESSION['product']['cart']);
		unset($_SESSION['product']['cart_vars']);

		$this->caption['c_text_buyer2'] =
			sprintf($this->caption['c_text_buyer2'],$_SERVER['HTTP_HOST'],$order_vars['order_id'], $_SESSION['product']['customer_id'],'http://'.$_SERVER['HTTP_HOST'].'/?tool=product&tool_section=customer&action=show_login&language=' . LANGUAGE);
		$this->caption['c_text_buyer3'] = $this->caption['c_directdebit_text_buyer'];

		$this->set_vars_method();
		
		$this->set_google_conversion();
		$content = $this->process();
		$GLOBALS['gl_content'] = $this->get_process() . $content;

		$this->send_mails($order_vars['order_id'], 'directdebit');
	}

	/**
	 *  Per fer compra en un sól pas, vinguent de show_summary, on ja s'ha triat mètode de pagament,  adresses, etc...
	 * De moment només està fet a lassdive per triar mètode de pagament
	 */
	function finish_order(){
		if ($this->is_cart_empty()) return;
		$order_vars = $this->save_order();
		unset($_SESSION['product']['cart']);
		unset($_SESSION['product']['cart_vars']);

		$this->set_vars_method();
		$this->set_vars( $order_vars );

		$payment_method  = R::escape( 'payment_method', false, '_POST' );

		if ( $payment_method == 'lacaixa' ) {
			$this->show_lacaixa( $order_vars );
		}
		elseif ( $payment_method == 'cash' ) {
			// Es gestiona a la funció save_order_vars de lass
			$this->set_var('show_cash',true);
			$content = $this->process();
			$GLOBALS['gl_content'] = $this->get_process() . $content;
		}
		elseif ( $payment_method == 'physicaltpv' ) {
			// Es gestiona a la funció save_order_vars de lass
			$this->set_var('show_physicaltpv',true);
			$content = $this->process();
			$GLOBALS['gl_content'] = $this->get_process() . $content;
		}


		// De moment només a lassdive
		/*$this->caption['c_text_buyer2'] =
			sprintf($this->caption['c_text_buyer2'],$_SERVER['HTTP_HOST'],$order_vars['order_id'], $_SESSION['product']['customer_id'],'http://'.$_SERVER['HTTP_HOST'].'/?tool=product&tool_section=customer&action=show_login&language=' . LANGUAGE);
		$this->caption['c_text_buyer3'] = $this->caption['c_directdebit_text_buyer'];

		$this->set_vars_method();

		$this->set_google_conversion();
		$content = $this->process();
		$GLOBALS['gl_content'] = $this->get_process() . $content;

		$this->send_mails($order_vars['order_id'], 'directdebit');*/
	}
	// Defineix variables comunes per: show_method show_paypal show_account show_ondelivery show_end
	function set_vars_method(){
		$this->set_file('product/payment_common.tpl');
		$this->set_vars(array('show_method'=>false,'show_paypal'=>false,'show_4b'=>false,'show_lacaixa'=>false,'show_account'=>false,'show_ondelivery'=>false,'show_cash'=>false,'show_physicaltpv'=>false,'show_end'=>false,'show_nocart'=>false));
		
		$this->set_var($this->action,true);
		
		$this->set_var('google_conversion',false);

		$this->set_vars($this->caption);
	}
	// Part 3c per obtindre diferents respostes ocultes del banc
	function get_order(){
		if ($this->process=='4b'){
			include('product_payment_4b.php');
			$payment = new ProductPayment4b($this);
		}
		$payment->get_order();
	}
	function set_notice(){
		/**/
		$this->set_vars_method();

		if ($this->process=='4b'){
			Debug::add('Post 4b',$_POST);
			include('product_payment_4b.php');
			$payment = new ProductPayment4b($this);		
		}
		elseif ($this->process=='lacaixa'){
			Debug::add('Post lacaixa',$_POST);
			include('product_payment_lacaixa.php');
			$payment = new ProductPaymentLacaixa($this);		
		}
		else{
			Debug::add('Post Paypal',$_POST);
			include('product_payment_paypal.php');
			$payment = new ProductPaymentPaypal($this);		
		}
		$is_valid = $payment->set_notice();
		
		if (!$is_valid) die();
		
		$this->set_customer($payment->order_id);
		
		$status_field = 'order_status';
		$transaction_field = 'transaction_id';

		// Lassdive
		$pay_deposit = R::number('pay_deposit');
		if ( $pay_deposit ) {
			// Només amb el trasaction_id per poder comprovar que es el primer contacte del banc
			// i el status en faig prou, la resta no cal, ja es pot trobar als logs o al banc			
			$status_field = 'deposit_status';
			$transaction_field = 'deposit_transaction_id';
		}

		// Comprobo si la notificació es la primera que rebo del banc
		$saved_order = Db::get_row("
			SELECT $transaction_field, $status_field
			FROM product__order
			WHERE order_id ='" . $payment->order_id . "' LIMIT 1");

		$saved_transaction = $saved_order[$transaction_field];
		$saved_order_status = $saved_order[$status_field];
		//$is_diferent = ($saved_transaction==$payment->transaction_id); // ja veurem que faig, s'hauria d'ocupar el modul paypal
		$is_first = ($saved_transaction=='');
		

		// Grabo el nou estat
		$sql = "UPDATE product__order
		SET $status_field = '" . $payment->order_status . "'
		,$transaction_field = '" . $payment->transaction_id . "'
		,approval_code = '" . $payment->approval_code . "'
		,error_code = '" . $payment->error_code . "'
		,error_description = '" . $payment->error_description . "'
		WHERE order_id ='" . $payment->order_id . "' LIMIT 1";
		Db::execute($sql);
		Debug::add('Actualització order',$sql);
		unset($_SESSION['product']['cart']);
		unset($_SESSION['product']['cart_vars']);
			
		// envio els mails un cop acabada la compra, o si paypal notifica la compra acabada
		if ($payment->order_status == 'completed')
			$message = $this->process . '_completed';
		else
			$message = $this->process . '_pending';

		// Si es el primer envia mail,
		// Si no es el primer però a canviat a completed, també
		if ($is_first
		    ||
		    ($payment->order_status == 'completed' && $saved_order_status != 'completed')
		) $this->send_mails($payment->order_id, $message);
		
		// poso factura
		include_once(DOCUMENT_ROOT . 'admin/modules/product/product_common.php'); // de moment nomes hi ha una funcio, aqui ja està be
		product_set_invoice_id($payment->order_id, $payment->order_status);
		/**/
		// TEST    ?tool=product&tool_section=payment&language=cat&action=set_notice
		// TEST $this->config['default_mail']='jsanahuja@teleline.es';$this->send_mails('77');
	}
	// Part 4 process pagament
	function show_end(){
		unset($_SESSION['product']['cart']);
		unset($_SESSION['product']['cart_vars']);
		
		$this->set_vars_method();
		
		$this->set_google_conversion();
		$id = '';
		
		// caption 1 2 3 de seller i buyer
		// agafo id última comanda client  -> millor agafar per GET -> treure agafar l'ultim de ondelivery i compte -> paypal ja poso order_id
		$customer_id = $_SESSION['product']['customer_id'];
		if ($customer_id){
			if (isset($_GET['order_id'])){
				$id = R::id('order_id');
			}
			else{
				$sql = "SELECT order_id FROM product__order WHERE customer_id = '".$customer_id."' ORDER BY order_id DESC";
				$id = $_GET['order_id'] = Db::get_first($sql);
				 
			}
			if ($id){
				$customer =  $this->_get_customer_name($id);
				
				if (USE_FRIENDLY_URL){
					$customer_url = '//'.$_SERVER['HTTP_HOST']. '/' . LANGUAGE . '/product/customer/show_login/';
				}
				else{
					$customer_url = '//'.$_SERVER['HTTP_HOST'].'/?tool=product&tool_section=customer&action=show_login&language=' . LANGUAGE;
				}
				$this->caption['c_text_buyer2'] =
					sprintf($this->caption['c_text_buyer2'],$_SERVER['HTTP_HOST'],$id,$customer['customer_id'], $customer_url);
					
				$module = Module::load('product','order');
				$module->is_logged = true;
				$order = $module->do_action('get_record');
				
				$this->set_var('order',$order);
			}
			else{
				$this->caption['c_text_buyer2'] = $this->caption['c_text_buyer_no_order'];
			}
		}
		else{
			$this->caption['c_text_buyer2'] = sprintf($this->caption['c_text_buyer_no_session'],$_SERVER['HTTP_HOST']);
		}		

		$this->set_var('order_id',$id);
		$this->set_vars($this->caption);

		$content = $this->process();
		$GLOBALS['gl_content'] = $this->get_process() . $content;
	}
	// REVISAR ,pensar i testejar si el sistema es bo, la seguretat del order_id ja es comproba desdel modul paypal que envia un mail a admintotal si algo no va be
	// poso el customer de la comanda en sessió, ja que quan es el paypal el que determina que s'ha pagat, el customer no està en sessió ni loggejat ja que no es ell qui entra a la pàgina sino paypal	
	function set_customer($order_id){	
		$rs = Db::get_row("SELECT customer_id, mail, is_wholesaler, name, surname FROM product__customer WHERE customer_id = (
								SELECT customer_id FROM product__order WHERE order_id = '".$order_id."')");
		
		$_SESSION['product']['customer_id'] = $rs['customer_id'];
		$_SESSION['product']['mail'] = $rs['mail'];
		$_SESSION['product']['is_wholesaler'] = $rs['is_wholesaler'];
		$_SESSION['product']['name'] = $rs['name'];
		$_SESSION['product']['surname'] = $rs['surname'];
	}
	// funcio de TEST per veure com queda el mail password
	function send_password_test(){
		$this->send_password(true);
	}
	// funcio de TEST per veure com queda el mail quan estem maquetant, nomès en local w.elquesigui, order_id i payment_method es opcional
	// TEST    ?tool=product&tool_section=payment&language=cat&action=mail_comanda
	// TEST    ?tool=product&tool_section=payment&language=cat&action=mail_comanda&order_id=1&payment_method=account&is_seller
	// TEST    ?tool=product&tool_section=payment&language=cat&action=send_password_test&mail=sanahuja@gmail.com
	function mail_comanda(){	
		if (!$GLOBALS['gl_is_local']) Main::error_404();
		$this->set_vars_method();		
		
		$order_id =R::id('order_id');
		if (!$order_id){
			$order_id = Db::get_first('SELECT order_id, payment_method FROM product__order LIMIT 1;');
			$payment_method = Db::get_first('SELECT payment_method FROM product__order LIMIT 1;');
		}
		else{			
			$payment_method = Db::get_first('SELECT payment_method FROM product__order WHERE order_id = \''.$order_id.'\' LIMIT 1;');
		}
		if (isset($_GET['payment_method'])){
			$payment_method = R::escape('payment_method');
		}
		
		$this->set_customer($order_id);
		
		$this->send_mails($order_id, $payment_method, true);
	}
	
	// Codi de conversió	
	
	// - En els moduls dels bancs quan es retorna a la pagina de la botiga 
	//				( no es del tot fiable perque potser que no tornin, faria falta un metode per comunicar a google via php )
	// 
	// - En els moduls de pagament diferit quan es tria el metode de pagament
	//				( show_directdebit, show_ondelivery, show_account )
	
	function set_google_conversion(){
		
		$analytics = $this->config['google_analytics_conversion'];
		if (!$analytics) return;
			
		$analytics = str_replace('"es"', '"' . LANGUAGE_CODE . '"', $analytics);
		$analytics = str_replace('"en"', '"' . LANGUAGE_CODE . '"', $analytics);
		
		$this->set_var('google_conversion',$analytics);
			
	}
	// Enviament de mails
	
	// - En els moduls dels bancs quan es notifica venta el primer cop 
	//				( els segons cops poden ser per actualitzar estat )
	// 
	// - En els moduls de pagament diferit quan es tria el metode de pagament
	//				( show_directdebit, show_ondelivery, show_account )
	
	function send_mails($id, $action, $is_test=false, $update_sells = false) {

		Main::load_class( 'product', 'common_order', 'admin');

		// Lassdive
		if ( $this->is_shop_app ) {
			$results = ProductCommonOrder::shop_app_update_sells( $id );
			if (!$is_test && $update_sells) $this->update_sells( $results );
			return;
		}

		$page = ProductCommonOrder::email_get_page($this->config, $is_test);
		// caption 1 2 3 de seller i buyer
		$customer =  $this->_get_customer_name($id);

		$t = array();

		$text_buyer = $this->caption['c_text_buyer'];
		// per pagar una comanda que no estava pagada
		if ( $order_id = R::id('order_id')) {
			$text_seller = $this->caption['c_text_seller_pay_now'];
		}
		else {
			$text_seller = $this->caption['c_text_seller'];
		}

		$t['c_text_buyer'] =
			sprintf($text_buyer,$this->caption['c_treatment_'.$customer['treatment']], $customer['name']);
		$t['c_text_buyer2'] =
			sprintf($this->caption['c_text_buyer2'],$_SERVER['HTTP_HOST'],$id,$customer['customer_id'], 'http://'.$_SERVER['HTTP_HOST'].'/?tool=product&tool_section=customer&action=show_login&language=' . LANGUAGE);
		$t['c_text_seller'] =
			sprintf($text_seller, $customer['name'],$_SERVER['HTTP_HOST']);
		$t['c_text_seller2'] = $t['c_text_buyer3'] = '';

		if ( $action != 'save_order') {
			$t['c_text_seller2'] = $this->caption[ 'c_' . $action . '_text_seller' ];
			$t['c_text_buyer3'] = $this->caption['c_' . $action . '_text_buyer'];
		}

		$module = ProductCommonOrder::email_get_order_module($id, $action, $t, $this->vars);

		// body buyer
		$module->set_vars(array(
			'is_buyer'=>true,
			'is_seller'=>false,
			'is_status_change' => false
			));

		$body_buyer = ProductCommonOrder::email_get_order_body( $module, $page );
		$subject_buyer = sprintf($this->caption['c_mail_subject_buyer'],$id,SHORT_HOST_URL);

		// body seller
		$GLOBALS['gl_caption']['c_footer_mail'] = ''; // trec el peu
		$module->set_vars(array(
			'is_buyer'=>false,
			'is_seller'=>true,
			'is_status_change' => false
			));

		$body_seller = ProductCommonOrder::email_get_order_body( $module, $page);
		$subject_seller = sprintf($this->caption['c_mail_subject_seller'],$id,$customer['customer_id']);

		 //Grabo nombre venuts
		if (!$is_test && $update_sells) $this->update_sells($module->results);
		//echo $body_seller;

		//si és només un e-mail per enviar informació conforma s'ha guardat la comanda envio un e-mail nomes al venedor i conforme només s'ha grabat una comanda
		if ($action=='save_order'){
			$body_seller = ProductCommonOrder::email_get_body( $page, $this->caption['c_mail_order_saved'] );
			$subject_seller = sprintf($this->caption['c_mail_order_subject_seller'],$id,$customer['customer_id']);
			$ds = array(
				'0' => array(
					'is_seller' => true,
					'to'        => $this->config['from_address'],
					'body'      => $body_seller,
					'subject'   => $subject_seller
				),
			);

		}
		//si es comanda definitva envio a tothom
		else {
			// dades mail venedor i comprador
			$ds = array(
				'0' => array(
					'is_seller' => false,
					'to'        => $customer['mail'],
					'body'      => $body_buyer,
					'subject'   => $subject_buyer
				),
				'1' => array(
					'is_seller' => true,
					'to'        => $this->config['from_address'],
					'body'      => $body_seller,
					'subject'   => $subject_seller
				),
			);
		}

		// envio tots els mails
		foreach ($ds as $d){
			ProductCommonOrder::send_email( $d, $is_test, $this->config );
		}
	}
	function update_sells(&$results){
		foreach ($results as $rs){
			//comprovo que el producte no sigui una variació
			if ($rs['product_variation_ids']=='0') {
				$sql = "UPDATE product__product SET 
					sells=sells+".$rs['quantity']."
					WHERE product_id = " . $rs['product_id'];
				Db::execute($sql);	

				// control stock
				if ($this->config['stock_control']){
					$sql = "UPDATE product__product SET 
						stock=stock-".$rs['quantity']." 
						WHERE product_id = " . $rs['product_id'];
					Db::execute($sql);
				
					// miro quan stock queda
					$query = "
						SELECT stock
						FROM product__product
						WHERE product_id = '" . $rs['product_id'] . "'";
					$stock = Db::get_first($query);
				}				
				
			}else{			
				$sql = "UPDATE product__product SET sells=sells+".$rs['quantity']." WHERE product_id = " . $rs['product_id'];
				Db::execute($sql);
				$sql = "UPDATE product__product_to_variation SET 
					variation_sells=variation_sells+".$rs['quantity']."
					WHERE product_variation_id = '" . $rs['product_variation_ids'] . "'
					AND product_id = " . $rs['product_id'];
				Db::execute($sql);
				
				// control stock
				if ($this->config['stock_control']){
				
					$sql = "UPDATE product__product_to_variation SET 
						variation_stock=variation_stock-".$rs['quantity']."
						WHERE product_variation_id = '" . $rs['product_variation_ids'] . "'
						AND product_id = " . $rs['product_id'];
					Db::execute($sql);
					
					// miro quan stock queda
					$query = "
						SELECT SUM(variation_stock) as stock
						FROM product__product_to_variation
						INNER JOIN product__variation USING(variation_id)
						WHERE product_id = '" . $rs['product_id'] . "'
						AND BIN = 0
						GROUP BY product_id";
					$stock = Db::get_first($query);
				}
				
			}
			
			// si hi stock_control i no permet negatiu
			// si no queda stock poso status a no-stock
			if ($this->config['stock_control'] && !$this->config['allow_null_stock'] && (intval($stock)<=0)){
				Db::execute("UPDATE product__product SET status='nostock' WHERE product_id='".$rs['product_id']."'");
			}
			
			
			//Debug::p($sql);	
		}
	}
	function _get_customer_name($id){
		$sql = "SELECT CONCAT(name,' ', surname) as name, mail, treatment, customer_id FROM product__customer JOIN product__order USING(customer_id) WHERE order_id = '" . $id . "'";
		return Db::get_row($sql);
	}
	function send_password($is_test=false){
		$module = Module::load('product','customer');
		$module->is_test = $is_test;
		$module->do_action('send_password');
	}
	// Grabar la comanda
	function save_order(){

		// per pagar una comanda que no estava pagada
		$order_id = R::id('order_id');


		$module = Module::load('product','order');
		
		$module->payment_method = substr($this->action, 5); // trec el show_ així queda paypal o ondelivery ...

		$ret = $module->save_order($order_id);

		//envio mail conforme comanda nova gravada
		$action='save_order';
		if (!$order_id) $this->send_mails($ret['order_id'], $action, false, true);

		$GLOBALS['gl_message'] = '';
		return $ret;
	}

	// Menu de process de la compra
	function show_process(){

		$content = $this->get_process();
		$GLOBALS['gl_page']->title = $this->processes[$this->action]['item'];
		$GLOBALS['gl_content'] = $content;
	}

	function get_process(){
		$tpl = new phemplate(PATH_TEMPLATES);
		$tpl->set_file('product/payment_process.tpl');
		$tpl->set_vars($this->caption);

		$processes = array(
			'login'=>array(
				'conta'=>0,
				'loop_count'=>4,
				'id'=>1,
				'item'=>$this->caption['c_payment_login'],
				'selected'=>$this->process=='identify'?1:0,
				'first_last'=>'first'
			),
			'show_summary'=>array(
				'conta'=>1,
				'loop_count'=>4,
				'id'=>2,
				'item'=>$this->caption['c_payment_summary'],
				'selected'=>$this->process=='summary'?1:0,
				'first_last'=>''
			),
			'show_method'=>array(
				'conta'=>2,
				'loop_count'=>4,
				'id'=>3,
				'item'=>$this->caption['c_payment_payment'],
				'selected'=>$this->process=='method'?1:0,
				'first_last'=>''
			),
			'show_end'=>array(
				'conta'=>3,
				'loop_count'=>4,
				'id'=>4,
				'item'=>$this->caption['c_payment_end'],
				'selected'=>$this->process=='end'?1:0,
				'first_last'=>'last'
			)
		);

		$tpl->set_loop('processes',$processes);
		$tpl->set_var('process',$this->process);

		return $tpl->process();
	}	
	
	/**
	 * ProductPayment::trace()
	 * Crea un arxiu de log desde el modul de pagament
	 * @return
	 */
	static public function trace($text, $name = '', $title = false, $file){
		
		$fp = fopen (CLIENT_PATH . '/temp/'.$file.'.log', "a+");
		
		$log = '';
		if ($title){
			$log .="\n\n\n================================================\n" . $title . ' - ' .date("Y-m-d H:i:s"). " - " . $_SERVER['REMOTE_ADDR'] . "\n================================================\n";
		}
		if ($name){
			$name .= ": ";
		}
		$log .= "\n" . $name . $text;
		
		fwrite($fp,$log);
		fclose($fp);
	}
	
	/**
	 * 
	 * Si no hi ha cistella, dono missatge d'error i no faig res mes
	 * @return
	 */
	function is_cart_empty(){
		if (!isset($_SESSION['product']['cart'])){
			
			$this->set_vars_method();
			$this->set_var('show_nocart',true);
			$this->set_var($this->action,false);
			$GLOBALS['gl_content'] = $this->process();
			return true;
		}
		return false;
	}
	
	/**
	 * ProductCustomer::is_logged()
	 * Saber si està logejat
	 * @return
	 */
	function is_logged(){

		// miro si hi ha les 2 sessions per més seguretat de que esta logged
		// lassdive
		if (empty($_SESSION['product']['is_shop_app'])) {
			return ( isset( $_SESSION['product']['customer_id'] ) && isset( $_SESSION['product']['mail'] ) );
		}
		else {
			$this->is_shop_app = true;
			return !empty($_SESSION['user_id']) && !empty($_SESSION['group_id']);
		}
	}

	/**
	 * ProductCustomer::do_action()
	 * Sobreescric la funció heredada
	 * Qualsevol acció d'aquesta clase l'haig de cridar per aquí,
	 * ja que és on controlo que l'usuari està loguejat
	 * @param string $action
	 * @return
	 */
	function do_action($action=''){
		// get_notice ve directament de la web del banc, per tant no hi ha cap sessio en marxa
		if ($this->action=='get_order' || $this->action=='set_notice' || $this->action=='mail_comanda') {
			parent::do_action();
			Debug::p_all();
			die();
		}

		// FA FALTA???
		// si la cistella es buida, es mostra el missatge i no fa cap acció
		//if (!isset($_SESSION['product']['cart'])) {
		//	$GLOBALS['gl_message'] = $this->caption['c_cart_is_empty'];
		//	return;
		//}

		$ac = $action?$action:$this->action;
		// es important no barrejar action amb this->action, per això faig servir una tercera variable $ac
		if ($ac=='login' || $ac=='register' || $ac=='send_password') {
			return parent::do_action($action);
		}
		else
		{
			if ($this->is_logged()) {
				return parent::do_action($action);
			}
			else
			{
				parent::do_action('login');
			}
		}
	}
}
?>