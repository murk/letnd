<article>
	<h2>Entrega de nuestros productos.</h2>
	<p>La entrega de los pedidos se realizará en un período de entre 3 y 14 días hábiles* a partir de la fecha de compra.</p>
	<p>Si se diera el caso, se le informaría mediante un e-mail de cualquier variación o problema que nos hubiéramos encontrado referente a la disponibilidad, precio, pago o entrega del producto</p>
	<p>En el caso de que se enviara un pedido y la dirección escrita por el usuario fuera incorrecta, los gastos suplementarios del transporte irían a cargo del comprador</p>
	<p>* Los días festivos y fines de semana se contemplarán como días no hábiles. 
    </p>
</article>