<?
if (!defined('NEWSLETTER_ADDRESSES_MESSAGE_ADDED')){
define('NEWSLETTER_ADDRESSES_MESSAGE_ADDED','Gràcies per inscriure\'t a la nostra llista');
define('NEWSLETTER_ADDRESSES_MESSAGE_ADD_EXISTS','Aquesta adreça de mail ja figura a la base de dades!');
}


$gl_caption['c_title_edit'] = 'Dades personals';
$gl_caption['c_title_register'] = 'Registre newsletter';
$gl_caption['c_title_login'] = 'Login newsletter';

$gl_caption ['c_register']='Registrar-se';

$gl_caption_customer ['c_mail_log']='Introduexi el seu e-mail';
$gl_caption_customer ['c_password_log']='Introduexi la seva contrasenya';

$gl_caption['c_password_forgotten'] = 'Ha oblidat la seva contrasenya?';
$gl_caption['c_password_insert_mail'] = 'Si us plau, introdueixi la seva adreça d\'email i li enviarem una contrasenya nova al seu compte de correu.';
$gl_caption['c_password_mail_error'] = 'L\'adreça d\'email proporcionada no existeix en la nostra base de dades, si us plau, escrigui l\'adreça correctament';
$gl_caption['c_password_sent'] = 'S\ha enviat una nova contrasenya a la seva adreça d\'email';
$gl_caption['c_password_mail_subject'] = 'La seva nova contrasenya';
$gl_caption['c_password_mail_text_1'] = 'La seva nova contrasenya és';
$gl_caption['c_password_mail_text_2'] = 'Pot canviar la contrasenya en la nostra pàgina web';

$gl_messages_customer['registered_succesful']="El registre s'ha efectuat correctament.<br><br>En breu li notificarem per email l'activació del seu compte";
$gl_caption_customer['c_subject'] = 'Nou client registrat';
$gl_caption_customer['c_body_1'] = 'S\'ha registrat un nou client a la newsletter';


$gl_caption_remove['c_admin_subject'] = 'Sol·licitud de baixa de la Newsletter';
$gl_caption_remove['c_admin_body'] = '<br />El client <strong>%s %s</strong> amb l\'adreça d\'email <strong>%s</strong> s\'ha donat de baixa de la newsletter.<br /><br />Es pot veure la llista de tots els clients donats de baixa a: <a href="%s/admin/?menu_id=17">%sadmin/?menu_id=17</a>';


$gl_caption_remove['c_remove_confirm_1'] = 'Es disposa a sol·licitar la baixa de la nostra llista per a la següent adreça de correu electrònic:';
$gl_caption_remove['c_remove_confirm_2'] = 'Recordi que aquesta acció no es pot desfer. Si voleu confirmar l’anulació de la vostra subscripció a la llista de publicitat, per favor cliqueu al botó:';
$gl_caption_remove['c_remove_confirm_button'] = 'Confirmar';

$gl_caption_remove['c_remove_confirmed_1'] = 'La seva subscripció a la nostra llista de correu electrònic ha estat anul·lada.';
$gl_caption_remove['c_remove_confirmed_2'] = 'Esperem reprendre el contacte amb vostè molt aviat.';


$gl_messages['added']=NEWSLETTER_ADDRESSES_MESSAGE_ADDED;
$gl_messages['add_exists']=NEWSLETTER_ADDRESSES_MESSAGE_ADD_EXISTS;
?>