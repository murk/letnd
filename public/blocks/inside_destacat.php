<?php
$module = Module::load('inside','tour');

$block_max_results = isset($vars['block_max_results']) ? $vars['block_max_results'] : $module->config['block_max_results'];


$module->parent = 'Block';

$module->limit = '0,' . $block_max_results;
$module->order_by = 'in_home DESC, ordre ASC, tour ASC, entered DESC';
$module->condition = 'AND in_home = 1';

$loop = $module->do_action('get_records');

$vars += $module->caption;

if ($loop)
{

}
else{
	$show_block = false;
}