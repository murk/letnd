#!/usr/bin/ruby
# **************************************************************
# *
# * jsrsServer.rb - Ruby JavaScript Remote Scripting server code
# *
# * Author: Ian Marsman, based on JSRS code developed by
# *         Brent Ashley [jsrs@megahuge.com]
# *
# * The JSRS is distributed under the terms of the GNU 
# * Genral Public License terms and conditions for copying,
# * distribution and modification.
# *
# **************************************************************
require 'cgi'
require 'rb2js'

# Function classes need to inherit from this. Just make a class
# that inherits from JSRSFunction, name it (case-sensitive) when
# calling from the client, and it will be found using Ruby's
# ObjectSpace methods.
class JSRSFunction
  def initialize(*params)
    # The input data (an array of or or more elements)
    @params = params
  end
  # This is implementation-specific
  # Note that this function can make use of @args data
  def data
    nil
  end
end

# This class handles the tasks of figuring out what "function"
# was called (in Ruby objects inheriting from JSRSFunction ar used),
# what parameters were called, and what the callback id is.
# Errors that occur (e.g. non-existant or invalid calls) are caught
# and turned into JSRS errors.
class JSRSServer
  attr_reader :result
  def initialize  
    cgi = CGI.new
    # Find the function being called.
    func = cgi['F'][0].to_s
    # Find the context object being called.
    callback = cgi['C'][0].to_s
    # Populate params list.
    params = []
    for i in 1..cgi.keys.length
      if cgi.key?('P'+i.to_s)
        params.push(cgi['P'+i.to_s][0].to_s)
      end
    end
    # The result will be JavaScript code (a string as far as Ruby is concerned).
    @result = ''
    begin
      # Get a shell instance of the called class (JSRS function).
      o = ObjectSpace.const_get(func)
      begin
        # Get an instance of the actual class being instantiated by calling
        # its new method.
        o = o.new(*params)
        # Handle case where an arbitrary, non-JSRS class was called (security risk).
        # Handle this as a JSRS Errror.
        unless o.kind_of?(JSRSFunction)
          @result = jsrs_err_str(callback, "\"#{func}\" not a JSRS function.")
        else
          # Get successfull call data and transform it into a JavaScript data string.
          @result = o.data
          @result = jsrs_ret_str(callback, @result.to_js)
        end
        # Invalid classes will have different new method arguments and will
        # trigger an ArgumentError. Handle this as a JSRS Errror.
      rescue ArgumentError
        @result = jsrs_err_str(callback, "\"#{func}\" not a JSRS function.")
      end
      # Handle case where non-existent class called.
    rescue NameError, Exception
      @result = jsrs_err_str(callback, "\"#{func}\" not a JSRS function.")
    end
  end
  # Escape dangerous characters for HTML (ones that would screw up a text box).
  def jsrs_escape(str)
    # Escape ampersands and forward slashes
    out_str = str.gsub('&', '&amp;').gsub(/<\s*\//, '<\\/')
  end   
  # Get data as a JSRS payload.
  def jsrs_ret_str(callback, str)
    outval = ''
    outval << "<html><head></head><body onload=\"p=document.layers?parentLayer:window.parent;p.jsrsLoaded('"
    outval <<  callback << "');\">jsrsPayload:<br>"
    outval <<  "<form name=\"jsrs_Form\"><textarea name=\"jsrs_Payload\" id=\"jsrs_Payload\">"
    outval <<  jsrs_escape(str) << "</textarea></form></body></html>"
    return outval
  end
  # Get a JSRS Error payload.
  def jsrs_err_str(callback, str)
    clean_str = str.gsub("'", "\\'").gsub(/([\\])/, '\1\1')
    outval = "jsrsError: " << clean_str
    outval << "<html><head></head><body "
    outval << "onload=\"p=document.layers?parentlayer:window.parent;p.jsrsError(\'" << callback
    outval << "','" << CGI::escape(str) << "\');\">" << clean_str << "</body></html>"
    return outval
  end
end
