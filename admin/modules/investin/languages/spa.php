<?php
if (!defined('INVESTIN_MENU_AGENCIA')){
	// menus eina

	define('INVESTIN_MENU_PAGINA','Edición de páginas');
	define('INVESTIN_MENU_PAGINA_NEW','Entrar nueva página');
	define('INVESTIN_MENU_PAGINA_NEW_SECOND','Entrar nueva página secundaria');
	define('INVESTIN_MENU_PAGINA_LIST','Listar páginas');
	define('INVESTIN_MENU_PAGINA_LIST_SECOND','Listar páginas secundarias');
	define('INVESTIN_MENU_PAGINA_BIN','Papelera de reciclage');

	define('WEB_MENU_BANNER','Banners');
	define('WEB_MENU_BANNER_NEW','Nuevo banner');
	define('WEB_MENU_BANNER_LIST','Listar banners');
	define('WEB_MENU_BANNER_LIST_BIN','Papelera');
}

/*--------------- nomès castellà i català, no traduit a la resta -------------------*/
 
/*pagines*/
$gl_caption_pagina['c_edit_level1'] = 'Datos de la pàgina';
$gl_caption_pagina['c_edit_level2'] = 'Datos de la pàgina';
$gl_caption_pagina['c_edit_level3'] = 'Datos de la pàgina';
$gl_caption_pagina['c_page_title_title'] = 'Datos de la página';
$gl_caption_pagina['c_column_title1'] = 'Primera columna';
$gl_caption_pagina['c_column_title2'] = 'Segunda columna';
$gl_caption_pagina['c_column_title3'] = 'Tercera columna';
$gl_caption_pagina['c_column_title4'] = 'Vídeo';
$gl_caption_pagina['c_column_title5'] = 'Google';
$gl_caption_pagina['c_column_title6'] = 'Más información';

$gl_caption_pagina['c_breadcrumb_edit_level1'] = '"editar pàgina"';
$gl_caption_pagina['c_breadcrumb_edit_level2'] = '"editar pàgina"';
$gl_caption_pagina['c_breadcrumb_edit_level3'] = '"editar pàgina"';
$gl_caption_pagina['c_breadcrumb_edit_list'] = '"llistat de subapartats"';

$gl_caption_pagina['c_subtitle_level2'] = 'Listar páginas';
$gl_caption_pagina['c_subtitle_level3'] = 'Listar páginas';

$gl_caption_pagina['c_filter_status'] = 'Todos los estados';
$gl_caption_pagina['c_filter_category_id'] = 'Todas las categorías';
$gl_caption_pagina['c_filter_paginatpl_id'] = 'Todas las plantillas';

$gl_caption_pagina['c_edit_content_button'] = 'Editar página';
$gl_caption_pagina['c_edit_pagina_button'] = 'Editar datos';



$gl_caption_pagina['c_ordre'] = 'Orden';
$gl_caption_pagina['c_entered'] = 'Creado';
$gl_caption_pagina['c_modified'] = 'Última Modificación';
$gl_caption_pagina['c_paginatpl_id'] = 'Plantilla ficha';
$gl_caption_pagina['c_listtpl_id'] = 'Plantilla listado';
$gl_caption_pagina['c_link'] = 'Link';
$gl_caption_pagina['c_pagina_file_name'] = $GLOBALS['gl_caption']['c_file_name'];
$gl_caption_pagina['c_body_supertitle'] = 'Supertítulo';
$gl_caption_pagina['c_body_supertitle2'] = 'Supertítulo';
$gl_caption_pagina['c_body_supertitle3'] = 'Supertítulo';
$gl_caption_pagina['c_title'] = 'Título';
$gl_caption_pagina['c_title2'] = 'Título';
$gl_caption_pagina['c_title3'] = 'Título';
$gl_caption_pagina['c_body_subtitle'] = 'Subtítulo';
$gl_caption_pagina['c_body_subtitle2'] = 'Subtítulo';
$gl_caption_pagina['c_body_subtitle3'] = 'Subtítulo';
$gl_caption_pagina['c_pagina'] = 'Nombre de la página en el menú';
$gl_caption_pagina['c_header_1'] = 'Frase cabecera 1';
$gl_caption_pagina['c_header_2'] = 'Frase cabecera 2';
$gl_caption_pagina['c_subpagina'] = 'Subapartats';
$gl_caption_pagina_list['c_pagina'] = 'Nombre';
$gl_caption_pagina['c_col_valign'] = $gl_caption_pagina['c_col_valign2'] = $gl_caption_pagina['c_col_valign3'] = 'Alineación vertical';
$gl_caption_pagina['c_col_valign_top'] = $gl_caption_pagina['c_col_valign2_top'] = $gl_caption_pagina['c_col_valign3_top'] = 'Arriba';
$gl_caption_pagina['c_col_valign_middle'] = $gl_caption_pagina['c_col_valign2_middle'] = $gl_caption_pagina['c_col_valign3_middle'] = 'Medio';
$gl_caption_pagina['c_col_valign_bottom'] = $gl_caption_pagina['c_col_valign2_bottom'] = $gl_caption_pagina['c_col_valign3_bottom'] = 'Abajo';
$gl_caption_pagina['c_body_content'] = 'Contenido';
$gl_caption_pagina['c_body_content2'] = 'Contenido';
$gl_caption_pagina['c_body_content3'] = 'Contenido';
$gl_caption_pagina['c_prepare'] = 'En preparación';
$gl_caption_pagina['c_review'] = 'Para revisar';
$gl_caption_pagina['c_public'] = 'Público';
$gl_caption_pagina['c_archived'] = 'Archivado';
$gl_caption_pagina['c_status'] = 'Estado';
$gl_caption_pagina['c_pagina_id'] = 'Id';
$gl_caption_pagina['c_parent_id'] = 'Pertenece a';
$gl_caption_pagina['c_custom_id'] = 'Galeria de personalitzación';
$gl_caption_pagina['c_level'] = '';
$gl_caption_pagina['c_blocked'] = 'Block.';

$gl_caption_pagina['c_add_pagina_button'] = 'Insertar nueva página';
$gl_caption_pagina['c_list_subpagines'] = 'Listar todos';
$gl_caption_pagina['c_add_subpagina'] = 'Añadir';
$gl_caption_pagina['c_add_subpagina2'] = 'Añadir subapartat';
$gl_caption_pagina['c_url1_name']='Nom URL';
$gl_caption_pagina['c_url1']='URL';
$gl_caption_pagina['c_url2_name']='Nom URL(2)';
$gl_caption_pagina['c_url2']='URL(2)';
$gl_caption_pagina['c_url3_name']='Nom URL(3)';
$gl_caption_pagina['c_url3']='URL(3)';
$gl_caption_pagina['c_videoframe']='Vídeo (youtube, metacafe...)<br />( 340 x 255 )';
$gl_caption_pagina['c_file']='Archivos';
$gl_caption_pagina['c_category_id']='Categoría';

$gl_caption['c_last_listing']='Listado anterior';


$gl_caption_category['c_ordre']='Ordre';
$gl_caption_category['c_category']='Categoría';
$gl_caption_category['c_is_clubone'] = 'Menú socios';

$gl_messages['c_restore_items'] = "Los elementos marcados no se han podido recuperar, ya que primero hay que recuperar la página superiror a la que pertenecen";




/*banners*/
$gl_caption_banner['c_banner_id']='';
$gl_caption_banner['c_ordre']='Orden';
$gl_caption_banner['c_url1']='Enlace';
$gl_caption_banner['c_url1_name']='Texto en el enlace';
$gl_caption_banner['c_url1_target']='Destíno';
$gl_caption_banner['c_url1_target__blank']='Una nueva ventana';
$gl_caption_banner['c_url1_target__self']='La misma ventana';
$gl_caption_banner['c_file']='Imagen banner <br>(960x92 - 248x122)';
$gl_caption_banner['c_banner']='Banner';

// mapes
$gl_caption['c_search_adress']='Buscar adreça';
$gl_caption['c_latitude']='latitud';
$gl_caption['c_longitude']='longitud';
$gl_caption['c_found_direction']='adreça trobada';
$gl_caption['c_edit_map'] = 'Mapa';
$gl_caption['c_save_map'] = 'Guardar ubicació';
$gl_caption['c_click_map'] = 'Fes un click en un punt o més del mapa per guardar la ubicació';
$gl_caption['c_form_map_title'] = 'Ubicació';
$gl_messages['point_saved'] = 'Nova ubicació guardada correctament';
$gl_messages['point_not_saved'] = 'No se ha podido guardar la ubicación';
$gl_caption['c_remove_polyline']='Borrar trazado';
?>