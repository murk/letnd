"use strict";

jQuery.validator.addMethod(
	"vdate",
	function (value, element) {
		return Date.parseExact(value, "dd/MM/yyyy");
	},
	$.validator.messages.date
);

var Sell = {
	is_others_visible: true,
	price_input: '',
	percent_inputs: [
		'mortgage_cancel',
		'estate_commission'
	],
	plusvalua_increment: 0,
	plusvalua_tribute_percent: 0,
	init: function () {

		this.price_input = '#price_' + sell_id;
		$(this.price_input).focus();
		// dp(this.price_input);
		this.init_masks();
		this.init_percents();
		this.calculate_percents();
		this.init_validation();
		this.write_report();
		this.init_all_keyups();
	},

	init_masks: function () {

		$("#date_bought_" + sell_id + ",#date_sold_" + sell_id).inputmask(
			"d/m/y",
			{
				"placeholder": "dd/mm/yyyy"
			}
		);

		var decimal_masks =
				Sell.price_input +
				',#price_bought_' + sell_id +
				',#mortgage_cancel_' + sell_id +
				',#mortgage_cancel_percent_' + sell_id +
				',#estate_commission_' + sell_id +
				',#estate_commission_percent' + sell_id +
				',#cadastral_price_' + sell_id +
				',#other_expenses_' + sell_id +
				',#habitability_card_' + sell_id +
				',#efficiency_' + sell_id
			;

		// Mascares decimals
		$(decimal_masks).inputmask(
			{
				'alias': 'decimal',
				'groupSeparator': '.',
				'radixPoint': ',',
				'autoGroup': true,
				'digits': 2,
				'digitsOptional': true,
				'placeholder': '',
				'decimalProtect': true
			}
		);

		// Mascares enters
		$('#year_' + sell_id).inputmask(
			{
				'alias': 'numeric',
				'groupSeparator': '.',
				radixPoint: ",",
				'autoGroup': true,
				'digits': 0,
				'min': 1,
				'max': new Date().getUTCFullYear()
			}
		);
	},

	init_validation: function () {

		// Accions del boto preu
		$(Sell.price_input)
			.keyup(function (e) {
				if (e.which == 13) {
					$(this).blur();
				}
				//else if ($(this).hasClass('valid')){
				else {
					//Sell.calculate_percents();
					//Sell.write_report();
				}
			})
			.blur(function () {

			});

		// Validacions
		var required_fields = [
			"price[" + sell_id + "]"
		];

		var rules = {};

		for (var f in required_fields) {
			rules[required_fields[f]] = {
				required: true
			}
		}
		$('#main_form')
			.validate({
				//rules: rules,
				//tooltip_options: {
				//	'_all_': {placement: 'right'}
				//},
				submitHandler: function (form) {
					// així no fa mai un submit
				},
				invalidHandler: function (form, validator) {
					setTimeout(
						function () {
							$(".tooltip").removeClass('in');
						}, 700);
				}
			});
		//$('#main_form').valid();
	},
	init_percents: function () {
		for (var key in this.percent_inputs) {
			var val = this.percent_inputs[key];
			this.add_percent(val, false);
		}
	},

	calculate_percents: function () {
		for (var key in this.percent_inputs) {
			var val = this.percent_inputs[key];
			this.calculate_percent(val, false);
		}
	},
	unmask_decimal: function (id) {
		var val = $(id).inputmask('unmaskedvalue');
		if (typeof val == 'undefined') return '';
		val = val.replace(',', '.');
		return Number(val);
	},

	calculate_percent: function (name, is_mortgage) {
		var currency_id = '#' + name + '_0',
			percent_id = '#' + name + '_percent_0',
			is_percent_active,
			current_id,
			other_id,
			new_val,
			price = is_mortgage ? this.unmask_decimal('#mortgage_' + sell_id) : this.unmask_decimal(Sell.price_input),
			value;

		if (!price) return;

		is_percent_active = $(percent_id).hasClass('active');


		// calculo percentatge
		if (is_percent_active) {
			current_id = percent_id;
			other_id = currency_id;
			value = this.unmask_decimal(current_id);
			new_val = value * price; // =/100 - després de round
			// dp(value, 'Es per cent');
			// dp(new_val);
		}
		// calculo import
		else {
			current_id = currency_id;
			other_id = percent_id;
			value = this.unmask_decimal(current_id);
			new_val = value / price * 10000; // = *100
			// dp(value, 'NO Es per cent');
		}
		// dp(current_id);
		//// dp(other_id);
		if ($.isNumeric(value)) {
			new_val = Math.round(new_val);
			new_val = new_val / 100;
			$(other_id).val(new_val);
		}
		else {
			$(other_id).val('');
		}
	},

	add_percent: function (name, is_mortgage) {
		var focus_val;
		$('#' + name + '_' + sell_id + ',#' + name + '_percent_' + sell_id)
			.focus(function () {
				focus_val = this.value;
				// dp('focus val', focus_val);

			})
			.keyup(function () {
				// dp('keyup val', this.value);
				if (this.value != focus_val) {
					var
						id = '#' + this.id,
						other_id =
							id == '#' + name + '_' + sell_id ?
							'#' + name + '_percent_' + sell_id :
							'#' + name + '_' + sell_id;

					$(other_id).removeClass('active');
					$(other_id).parent().removeClass('active');
					$(id).addClass('active');
					$(id).parent().addClass('active');
					// dp(id);
					// dp(other_id);
				}

				Sell.calculate_percent(name, is_mortgage);
			});
	},
	write_report: function () {

		dp('REPORT \n\n\n');


		// calculs
		var
			price = Sell.unmask_decimal(Sell.price_input),
			price_bought = Sell.unmask_decimal('#price_bought_' + sell_id),
			//mortgage_cancel_percent = Sell.unmask_decimal('#mortgage_cancel_percent_' + sell_id),
			mortgage_cancel = Sell.unmask_decimal('#mortgage_cancel_' + sell_id),
			//estate_commission_percent = Sell.unmask_decimal('#estate_commission_percent_' + sell_id),
			estate_commission = Sell.unmask_decimal('#estate_commission_' + sell_id),
			cadastral_price = Sell.unmask_decimal('#cadastral_price_' + sell_id),
			other_expenses = Sell.unmask_decimal('#other_expenses_' + sell_id),
			date_bought = $('#date_bought_' + sell_id).val(),
			date_sold = $('#date_sold_' + sell_id).val(),
			habitability_card = Sell.unmask_decimal('#habitability_card_' + sell_id),
			efficiency = Sell.unmask_decimal('#efficiency_' + sell_id),
			expenses_bought,
			expenses_sell,
			total_expenses,
			price_real,
			price_bought_real,
			total_days,
			plusvalua
			;


		if (!$('#has_habitability_card').is(":checked")) {
			habitability_card = 0;
		}
		if (!$('#has_efficiency').is(":checked")) {
			efficiency = 0;
		}

		// Dates
		date_bought = new Date(this.flip_date(date_bought));
		date_sold = new Date(this.flip_date(date_sold));

		// Dies
		total_days = this.get_total_days(date_sold, date_bought);

		// Plusvalua
		plusvalua = this.show_plusvalua(cadastral_price, total_days);

		// Beneficis
		expenses_sell = mortgage_cancel + estate_commission + plusvalua + habitability_card + efficiency;
		expenses_bought =  other_expenses;
		total_expenses = expenses_bought + expenses_sell;

		// Renda
		price_real = price - expenses_sell;
		price_bought_real = price_bought + expenses_bought;

		// resum
		this.set_report_val('price', '€', price);
		this.set_report_val('price_real', '€', price_real);
		this.set_report_val('price_bought', '€', -price_bought);
		this.set_report_val('price_bought_real', '€', -price_bought_real);

		// despeses fixes
		this.set_report_val('mortgage_cancel', '€', mortgage_cancel);
		this.set_report_val('estate_commission', '€', estate_commission);
		this.set_report_val('habitability_card', '€', habitability_card);
		this.set_report_val('efficiency', '€', efficiency);
		this.set_report_val('plusvalua', '€', plusvalua);
		this.set_report_val('other_expenses', '€', other_expenses);
		this.set_report_val('total_expenses', '€', total_expenses);

		this.show_renda(price_real, price_bought_real, date_sold, date_bought, total_days);

	},

	get_total_days: function (date_sold, date_bought) {

		var span = new TimeSpan(date_sold - date_bought)
			, total_days = span.days;

		if (isNaN(total_days) || total_days < 1) return false;

		else return total_days;

	},

	show_renda: function (price_real, price_bought_real, date_sold, date_bought, total_days) {

		if (!total_days) return;

		var earning = price_real - price_bought_real;

		// Dies fins 31/12/1996
		var date_1996 = new Date('1996-12-31');

		var years_before_1996 = moment(moment(date_1996)).diff(date_bought, 'years', true);

		dp(years_before_1996, 'anys 96');


		var span = new TimeSpan(date_1996 - date_bought),
		days_before_1996 = span.days;
		if ( days_before_1996 < 0 ) days_before_1996 = 0;

		//span = new TimeSpan(date_sold - date_1996);
		//var days_after_1996 = span.days;

		// Dies fins 19/01/2006
		var date_2006 = new Date('2006-01-19');
		var days_after_2006 = 0;

		span = new TimeSpan(date_2006 - date_bought);
		var days_before_2006 = span.days;

		if ( days_before_2006 < 0 ) {
			days_before_2006 = 0;
			span = new TimeSpan(date_sold - date_bought);
			days_after_2006 = span.days;
		}
		else
		{
			span = new TimeSpan(date_sold - date_2006);
			days_after_2006 = span.days;
		}


		// Guanys
		var daily_earning = earning / total_days
			, earning_before_2006 = days_before_2006 * daily_earning
			, earning_after_2006 = days_after_2006 * daily_earning
			, earning_before_2006_reductible = earning_before_2006

		// Càlcul quantitat a tributar abans 2006
			, coeficient_before_1996 = Math.ceil(years_before_1996 - 2) * 11.11
			, to_tribute_before_2006 = 0
			, to_tribute_before_2006_anyway = 0;

		// ajusto coeficient
		if (coeficient_before_1996 == '99.99') coeficient_before_1996 = 100;
		if (coeficient_before_1996 < 0) coeficient_before_1996 = 0;

		// Si es ven per més de 400.000
		if (price_real > 400000) {

			earning_before_2006_reductible = earning_before_2006 * 400000 / price_real;

			//dp(earning_before_2006,'earning_before_2006')
			//dp(price_real,'price_real')
			//dp(earning_before_2006_tmp,'earning_before_2006_tmp')

			to_tribute_before_2006_anyway = earning_before_2006 - earning_before_2006_reductible;

		}

		if (years_before_1996 < 0) {
			// data posterior al 1996 es tributa tot
			to_tribute_before_2006 = earning_before_2006_reductible;
			this.set_report_val('coeficient_before_1996', '%', false);
		}
		else if (coeficient_before_1996 < 100 && coeficient_before_1996 > 0) {
			// deixo de tributar un 11% cada any
			to_tribute_before_2006 = (100 - coeficient_before_1996) / 100 * earning_before_2006_reductible;
			this.set_report_val('coeficient_before_1996', '%', coeficient_before_1996);
		}
		else if (coeficient_before_1996 >= 100) {
			// no es tributa res - abans del 86
			this.set_report_val('coeficient_before_1996', '%', 100);
			to_tribute_before_2006 = 0
		}
		else if ( coeficient_before_1996 == 0) {
			this.set_report_val('coeficient_before_1996', '%', 0);
		}

		// Càlcul tributació
		var total_to_tribute = earning_after_2006 + to_tribute_before_2006 + to_tribute_before_2006_anyway
			, tribute, tribute_zone_1 = 0, tribute_zone_2 = 0, tribute_zone_3 = 0;

		if (total_to_tribute <= 6000) {
			tribute_zone_1 = total_to_tribute * 0.19;
		}
		else if (total_to_tribute > 6000 && total_to_tribute <= 50000) {
			tribute_zone_1 = 6000 * 0.19;
			tribute_zone_2 = (total_to_tribute - 6000 ) * 0.21;
		}
		else if (total_to_tribute > 50000) {
			tribute_zone_1 = 6000 * 0.19;
			tribute_zone_2 = 44000 * 0.21;
			tribute_zone_3 = (total_to_tribute - 50000 ) * 0.23;
		}

		tribute = tribute_zone_1 + tribute_zone_2 + tribute_zone_3;

		// Report
		this.set_report_val('earning', '€', earning);
		this.set_report_val('earning2', '€', earning);
		this.set_report_val('total_days', c_days, total_days);
		this.set_report_val('days_before_1996', c_days, days_before_1996);
		this.set_report_val('days_before_2006', c_days, days_before_2006);
		this.set_report_val('days_after_2006', c_days, days_after_2006);
		this.set_report_val('earning_before_2006', '€', earning_before_2006);
		this.set_report_val('earning_after_2006', '€', earning_after_2006);
		this.set_report_val('total_to_tribute', '€', total_to_tribute);
		this.set_report_val('to_tribute_before_2006', '€', to_tribute_before_2006);
		this.set_report_val('earning_before_2006_reductible', '€', to_tribute_before_2006?earning_before_2006_reductible:false);
		this.set_report_val('to_tribute_before_2006_anyway', '€', to_tribute_before_2006_anyway);
		this.set_report_val('to_tribute_after_2006', '€', to_tribute_before_2006 || to_tribute_before_2006_anyway?earning_after_2006:false);
		this.set_report_val('tribute_zone_1', '€', tribute_zone_1);
		this.set_report_val('tribute_zone_2', '€', tribute_zone_2);
		this.set_report_val('tribute_zone_3', '€', tribute_zone_3);
		this.set_report_val('tribute', '€', tribute);
	},

	show_plusvalua: function (cadastral_price, total_days) {

		if (!total_days || !cadastral_price || !this.plusvalua_increment) {
			return 0;
		}

		// calculo plusvalua
		var years = Math.floor(total_days / 365),
			plusvalua,
			increment = this.plusvalua_increment * years;

		// base imponible
		plusvalua = cadastral_price * increment / 100;

		// plusvalua
		plusvalua = plusvalua * this.plusvalua_tribute_percent / 100;
		return plusvalua;

	},

	set_plusvalua_municipi: function(suggestion) {

		var plusvalua = suggestion.data.split(',');
		plusvalua[0] = Number(plusvalua[0]);
		plusvalua[1] = Number(plusvalua[0]);

		if (!plusvalua[0]) {
			plusvalua[0] = 3;
		}
		if (!plusvalua[1]) {
			plusvalua[1] = 25;
		}

		Sell.plusvalua_increment = plusvalua[0];
		Sell.plusvalua_tribute_percent = plusvalua[1];
		Sell.write_report();

	},

	init_all_keyups: function () {

		$('#main_form .keyup').keyup(function () {
			delay(function () {
				Sell.calculate_percents();
				Sell.write_report();
			}, 200);
		});
		$('#main_form  input[type="checkbox"]').change(function () {
			Sell.calculate_percents();
			Sell.write_report();
		});


		$('#municipi_' + sell_id).autocomplete({
			serviceUrl: "/admin/?tool=utility&tool_section=sell&action=get_plusvalua_vars_ajax",
			minChars: 1,
			autoSelectFirst: true,
			onSelect: Sell.set_plusvalua_municipi
		});

	},
	format_number: function (num, is_decimal) {

		if (typeof is_decimal == 'undefined') is_decimal = true;
		if (isNaN(num)) num = 0;
		var num_decimals = 2, decimal;

		if (is_decimal) {
			num = Math.round(num * Math.pow(10, num_decimals)).toString();
			decimal = num.slice(num.length - 2, num.length);
			num = num.slice(0, num.length - 2);
		}
		else {
			num = num.toString();
		}

		num = num.replace(/(\d)(?=(\d{3})+$)/g, '$1.');

		if (is_decimal) {
			if (!num) num = 0;
			if (decimal == '0') decimal = '00';
			num = num + ',' + decimal;
		}

		return num;
	},

	set_report_val: function (name, sufix, val) {

		var holder = '#' + name + '_holder';
		val == 0 || val == '0,00' ? $(holder).hide() : $(holder).show();
		val = this.format_number(val, sufix=='€' || sufix == '%');

		$('#' + name).text(val + ' ' + sufix);

	},
	flip_date: function (dates) {

		dates = dates.split('/');
		dates = dates[2] + '-' + dates[1] + '-' + dates[0];
		return new Date(dates);

	},
	print: function () {
		print();
	}

};
$(function () {
	Sell.init();
});

var delay = (function () {
	var timer = 0;
	return function (callback, ms) {
		clearTimeout(timer);
		timer = setTimeout(callback, ms);
	};
})();