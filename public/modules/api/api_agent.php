<?
/**
 * ApiAgent
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2011
 * @version $Id$
 * @access public
 */
class ApiAgent extends Module{

	var $condition = '';
	var $q = false;
	
	var $group_id, $user_is_admin, $user_is_agent, $agencia_id;

	function __construct(){
		parent::__construct();
	}
	function list_records()
	{
		$content = $this->get_records();
		if ($this->q) {
			$GLOBALS['gl_page']->javascript .= "
				$('table.listRecord').highlight('".addslashes($this->q)."');			
			";
		}
		$GLOBALS['gl_content'] = $content;
	}
	function get_records()
	{
		
		$listing = new ListRecords($this);		
		
		$listing->set_var('search_value',$this->q?$this->q:$this->caption['c_search_value']);
		
		$listing->set_var('select_especialitats',$this->get_select_especialitats());
		$listing->set_var('select_municipis',$this->get_select_municipis());
		
		
		// search
		if ($this->condition) {
			$listing->condition = $this->condition . ' AND ';
		}
		$listing->always_parse_template = true;
		$listing->condition .= 'actiu = 1';
		$listing->call('records_walk','',true);
		
		$listing->order_by = 'agencia ASC';
		
	    return $listing->list_records();
	}
	function records_walk($module){
		$rs = &$module->rs;
		$ret = UploadFiles::get_record_images($rs['agencia_id'], true, 'api', 'agencia','agencia_');
		
		return $ret;
	}
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form($agent_id = false)
	{
	    $show = new ShowForm($this);
		
		if ($agent_id) $show->id = $agent_id;			
		
		$show->condition = 'actiu = 1';
		$show->get_values();
		
		if (!$show->has_results) Main::error_404();		
		
		$show->call('records_walk','',true);
		
		
		//if ($this->parent=='aixequem') return $show->id = $agent_id;
		
	    return $show->show_form();
	}
	function get_select_especialitats(){
		
		$speciality = R::escape('speciality');
		
		$results = Db::get_rows("SELECT speciality_id, speciality FROM api__speciality_language WHERE language='" . LANGUAGE . "' ORDER BY speciality");
				
		$html = '<select name="speciality">
					<option value="0">'. $this->caption['c_agent_tria_especialitat'] .'</option>';
		
		foreach ($results as $rs){
			$selected = ($speciality==$rs['speciality_id'])?' selected':'';
			$html .='<option value="' . $rs['speciality_id'] .'"'.$selected.'>' . $rs['speciality'] .'</option>';
		}
		
		$html .= '</select>';
		
		return $html;
	}
	function get_select_municipis(){
		
		$municipi_id = R::escape('municipi_id');
		
		$results = Db::get_rows("
			SELECT municipi_id, municipi 
			FROM inmo__municipi 		
			ORDER BY municipi");
				
		$html = '<select name="municipi_id">
					<option value="0">'. $this->caption['c_agent_tria_municipi'] .'</option>';
		
		foreach ($results as $rs){
			$query = "SELECT count(*) 
										FROM api__agent
										INNER JOIN api__agencia
										USING ( agencia_id )
										WHERE actiu = 1 
										AND api__agent.bin = 0 
										AND api__agencia.bin = 0 
										AND municipi_id = " . $rs['municipi_id'];
			$has_agents = Db::get_first( $query );
			if ($has_agents){
				$selected = ($municipi_id==$rs['municipi_id'])?' selected':'';
				$html .='<option value="' . $rs['municipi_id'] .'"'.$selected.'>' . $rs['municipi'] .'</option>';
			}
		}
		
		$html .= '</select>';
		
		return $html;
	}
	
	//
	// FUNCIONS BUSCADOR
	//
	function search(){

		$q = $this->q = R::escape('q');		
		$municipi_id = R::escape('municipi_id');
		
		$search_condition = '';

		if ($q)
		{
			// Sempre busco a tot arreu, abans si detectava que eren refrencies no hi  buscava, posaré checkbox de "buscar nomès referencies"

			$search_condition = "(
				nom like '%" . $q . "%'
				OR cognoms like '%" . $q . "%'
				OR CONCAT(nom, ' ', cognoms) like '%" . $q . "%'
				OR api_id like '%" . $q . "%'
				OR aicat_id like '%" . $q . "%'
				OR adresa like '%" . $q . "%'
				OR cp like '%" . $q . "%'
				OR telefon like '%" . $q . "%'
				OR telefon2 like '%" . $q . "%'
				OR fax like '%" . $q . "%'
				OR url1 like '%" . $q . "%'
				OR agent_description like '%" . $q . "%'
				" ;

			// busco a agencia
			$query = "SELECT agencia_id FROM api__agencia WHERE (
								 agencia like '%" . $q . "%'
									)" ;
			$results = Db::get_rows_array($query);
			$search_condition .= $this->get_ids_query($results, 'agencia_id');
			
			// busco municipi
			if (!$municipi_id) {
				$query = "SELECT municipi_id FROM inmo__municipi WHERE (
									 municipi like '%" . $q . "%'
										)" ;
				$results = Db::get_rows_array($query);
				$search_condition .= $this->get_ids_query($results, 'municipi_id');
			}

			$GLOBALS['gl_page']->title = TITLE_SEARCH . '<strong>&nbsp;&nbsp;"' . $q . '"</strong>';
		}		
			
		if ($search_condition) $search_condition .= ") " ;
		$search_condition = $this->get_ref_query('api_id',$q, $search_condition);
		
		
		$speciality = R::escape('speciality');
		
		if ($speciality) {
			// busco a especialitats
			$query = "SELECT agent_id FROM api__agent_to_speciality WHERE (
								 speciality_id = '" . $speciality . "'
									)" ;
									Debug::p($query);
			$results = Db::get_rows_array($query);
			$speciality_condition = $this->get_ids_query($results, 'agent_id', '');
			if (!$speciality_condition) $speciality_condition = 'agent_id IN (0)';
			if ($search_condition) $speciality_condition = ' AND' . $speciality_condition;
			$search_condition .= $speciality_condition;
		
		}
		
		
		if ($municipi_id) {
			// afegeixo query desplegable municipis	
			$municipi_condition .= "municipi_id = '" . $municipi_id . "'";
			if ($search_condition) $municipi_condition = " AND " . $municipi_condition;
			$search_condition .= $municipi_condition;		
		}
				
		
		$this->condition = $search_condition;
		Debug::add('Search condition', $this->condition);
		$this->do_action('list_records');
	}

	function get_ids_query(&$results, $field, $or_and = 'OR')
	{
		if (!$results) return '';
		$new_arr = array();
		foreach ($results as $rs){
			$new_arr [$rs[0]] = $rs[0];
		}
		return $or_and . " " . $field . " IN (" . implode(',', $new_arr) . ") ";
	}
	function get_ref_query($field, $q, $search_condition){
		//
		// busqueda automatica de referencies separades per espais o comes
		//
		$ref_array = explode (",", $q); // si l'array hem dona 1 probo de fer-ho amb espais, si es una sola referncia donarà el mateix resultat
		count($ref_array) == 1?$ref_array = explode (" ", $q):false;

		foreach($ref_array as $key => $value)
		{
			if (!is_numeric($value))
			{
				$ref_array = '';
				break;
			}
		}

		// consulta
		if ($ref_array)
		{
			$query_ref = "";
			foreach ($ref_array as $key)
			{
				$query_ref .= $field . " = '" . $key . "' OR ";
			}
			$query_ref = "(" . substr($query_ref, 0, -3) . ") ";

			return '( ' . $query_ref . ' OR ' . $search_condition . ')';
		}
		else{
			return $search_condition;
		}
	}

	function get_search_form(){
		$this->caption['c_by_ref']='';
		$this->set_vars($this->caption);
		$this->set_file('search.tpl');
		$this->set_var('menu_id',$GLOBALS['gl_menu_id']);
		return $this->process();
	}
}

?>