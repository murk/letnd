<?
define('USER_LOGIN_TITLE','Bienvenido a Letnd');
define('USER_LOGOUT_TITLE','Estás desconectado');
define('USER_LOGIN_TEXT','Introduce el login y password y pulsa aceptar');
define('USER_LOGIN_AGAIN_TEXT','La sesión ha caducado. Vuelve a introducir el login y password');
define('USER_LOGIN_INVALID','El nombre de usuario o password no són válidos');
define('USER_LOGOUT_TEXT','Para volver a conectar introduce el login y password');


$users_login_form['c_login'] = 'Login';
$users_login_form['c_password'] = 'Password';
$users_login_form['c_send'] = 'Aceptar';




// menus eina
define('USER_MENU_USER','Usuarios');
define('USER_MENU_USER_NEW','Crear Usuario');
define('USER_MENU_USER_LIST','Listar Usuarios');
define ('USER_MENU_USER_BIN','Papelera');
define('USER_MENU_CONCESSION','Privilegios');
define('USER_MENU_CONCESSION_LIST','Asignar Privilegios');
define ('USER_MENU_GROUP','Grupos de usuarios');
define('USER_MENU_GROUP_NEW','Crear grupo');
define('USER_MENU_GROUP_LIST','Listar grupos');
define ('USER_MENU_GROUP_BIN','Papelera');
define ('USER_MENU_LOG','Historial');
define ('USER_MENU_LOG_LIST','Listar historial');
define ('USER_LOGIN_BROWSER_INCORRECT','Para poder utilizar este problema es necesario al menos Internet Explorer 8, Firefox 3.6, Safari 5 o Google Chrome 7<br>Estás utilizando %s versión %s');



// captions seccio
$gl_caption_concession['c_tool'] = 'Módulo';
$gl_caption_concession['c_tool_section'] = 'Sección';
$gl_caption_concession['c_read1'] = 'Lectura';
$gl_caption_concession['c_write1'] = 'Escritura';
$gl_caption_concession['c_group_id'] = '';
$gl_caption_concession['c_bin'] = '';
$gl_caption_concession['c_search1'] = 'Elige grupo:';
$gl_caption_group['c_name']= "Nombre del grupo";
$gl_caption_user['c_group_id']="Nombre del grupo";
$gl_caption_user['c_name']= "Nombre";
$gl_caption_user['c_surname']= "Apellidos";
$gl_caption_user['c_login']= "Nombre de usuario";
$gl_caption_user['c_passw']= "Contraseña";
$gl_caption_user['c_passw_repeat']= "Repetir contraseña";
$gl_caption_user['c_email']= "E-mail";
$gl_caption_user['c_email_repeat']= "Repetir e-mail";
$gl_caption_log['c_ip']= "Ip del ordenador";
$gl_caption_log['c_user_id']= "Usuario";
$gl_caption_log['c_date']= "Fecha";
$gl_caption_log['c_login']= "Hora de entrada";
$gl_caption_log['c_logout']= "Hora de salida";


$gl_messages['Error_no_groups'] = "Error!, Antes de crear usuaros, se tienen que crear grupos.";
$gl_messages['user_exist_del']='Error!, El grupo: <b>"%s"</b> contiene usuarios activos, no se puede borrar.';
$gl_messages['user_admin_del']='Error!, El grupo: <b>"%s"</b> no se puede borrar.';
$gl_messages['add_exists']='Ya existe un registro con este nombre de usuario.';

$gl_caption_user['c_edit_map'] = 'Mapa';
$gl_caption_user['c_form_map_title'] = 'Ubicación del inmueble';
?>