<?

/* 24-1-18
 Vreasy


 */

if ( is_table( 'booking__book' ) && ! is_field( 'booking__book', 'vreasy_id' ) ) {

	$update_sql[] = "ALTER TABLE `booking__book`
	ADD COLUMN `vreasy_id` INT(11) NOT NULL DEFAULT '0' AFTER `approval_code_4`;";

	$update_sql[] = "ALTER TABLE `booking__book`
	ADD COLUMN `book_owner` ENUM('letnd','vreasy') NOT NULL DEFAULT 'letnd' AFTER `vreasy_id`;";

	$update_sql[] = "ALTER TABLE `booking__book`
	ADD COLUMN `book_synced` TINYINT NOT NULL DEFAULT '1' AFTER `book_owner`;";


}
else {
	if ( is_table( 'booking__book' ) ) {
		trigger_error( 'Ja existeixen el camp vreasy_id' );
	}
}

if ( is_table( 'inmo__property_to_portal' ) && ! is_field( 'inmo__property_to_portal', 'vreasy' ) ) {

	$update_sql[] = "ALTER TABLE `inmo__property_to_portal`
	ADD COLUMN `vreasy` TINYINT(1) NOT NULL DEFAULT '0' AFTER `ceigrup`;";

}
else {
	if ( is_table( 'inmo__property_to_portal' ) ) {
		trigger_error( 'Ja existeixen el camp vreasy' );
	}
}

if ( is_table( 'inmo__property' ) ) {

	if ( ! is_table( 'inmo__property_to_vreasy' ) ) {

		$update_sql [] = "CREATE TABLE `inmo__property_to_vreasy` (
								`property_vreasy_id` INT(11) NOT NULL AUTO_INCREMENT,
								`property_id` INT(11) NOT NULL DEFAULT '0',
								`vreasy_id` INT(11) NOT NULL DEFAULT '0',
								`synced` TINYINT(4) NOT NULL DEFAULT '1',
								PRIMARY KEY (`property_vreasy_id`),
								INDEX `property_id` (`property_id`),
								INDEX `vreasy_id` (`vreasy_id`),
								INDEX `synced` (`synced`)
							)
							COLLATE='utf8_spanish2_ci'
							ENGINE=MyISAM;";

	}
	else {
		trigger_error( 'Ja existeixen la taula inmo__property_to_vreasy' );
	}

}

?>