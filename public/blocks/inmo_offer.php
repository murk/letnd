<?

$module = Module::load('inmo','property');
$this->tpl->module = &$module;
$module->parent = 'Block';
include_once('inmo_block_functions.php');

$listing = $module->do_action('get_records');

// config
$random = isset($vars['random'])?$vars['random']:'0';
$offer_type = isset($vars['offer_type'])?$vars['offer_type']:false; // offer , home o none
$block_max_results = isset($vars['block_max_results']) ? $vars['block_max_results'] : $listing->config['block_max_results'];
$tipus = isset($vars['tipus'])?$vars['tipus']:false;
$category_id = isset($vars['category_id'])?$vars['category_id']:false;
$page_id = isset($vars['page_id']) ? $vars['page_id'] : false;
$status = isset($vars['status']) ? "status = '{$vars['status']}'" : "status = 'onsale' OR status = 'reserved'";

// enllacen directament a la categoria o /i tipus de l'inmoble
$link_category = isset($vars['link_category'])?$vars['link_category']:1; // true o false
$link_tipus = isset($vars['link_tipus'])?$vars['link_tipus']:1; // true o false
$link_tipus_values = isset($vars['link_tipus_values'])?$vars['link_tipus_values']:false;
$link_tipus_values = explode(',', $link_tipus_values);

if ($random) $listing->order_by  = 'RAND() ASC';

$listing->limit =  $block_max_results;

if ($offer_type && $offer_type!='none')
	$listing->condition .= " AND " . $offer_type . " = 1";

if ($category_id)
	$listing->condition .= " AND category_id = '" . $category_id . "'";

if ($status)
	$listing->condition .= " AND ($status)";

$listing->limit =  $block_max_results;

if ($tipus)
	$listing->condition .= inmo_block_get_tipus($tipus);


$listing->set_records();

$loop = $listing->loop;

if (!$loop) {
	$show_block = false;
	return;
}



$conta = 0;
$loop_count = count($loop);
foreach ($loop as $key=>$val){
	$rs = &$loop[$key];	

	
	/*
	if (USE_FRIENDLY_URL) {
		$friendly_params['tipus'] = $rs['tipus'];
		$file_name =$rs['property_file_name'];


		$l['link'] = Page::get_link('inmo','property', false, $rs['property_id'], $friendly_params, false, $file_name, $rs['category_id'], false);


	} else {
		$l['link'] = '/?tool=inmo&amp;tool_section=property&amp;action=show_record&amp;tipus=' . $rs['tipus'] . '&amp;property_id=' . $rs['property_id'] . '&amp;language=' . LANGUAGE;
	}
	 * 
	 */



	//$ref = get_ref($rs['ref'], $rs['category_id']);
	
	//$rs += UploadFiles::get_record_images($rs['property_id'], true, 'inmo', 'property');
	//$rs['ref'] = $gl_caption['c_ref'] . ' ' . $ref['ref'];
	//$l['price'] = format_int($rs['price']);	
	
	$link_tipus_value = $tipus?$tipus:$rs['tipus_value'];	

	if ($link_tipus_values) {
		foreach ($link_tipus_values as $v){
			if (strpos($v, $link_tipus_value)!== false) {
				$link_tipus_value = $v;
				break;
			}
		}
	}
		
	$params = array(
					'id' => $rs['property_id'],
					'file_name' => $rs['property_file_name'],
					'tool' => $listing->tool, 
					'tool_section' => $listing->tool_section, 
					'link_vars' => array('tipus'=>$link_tipus_value),
					'detect_page' => $page_id?false:true,
					'page_id' => $page_id
			);
	
	if ($link_category) {
		$params['category'] =  'category_id';
		$params['category_id'] =  $rs['category_id_value'];
	}
	
	if ($link_tipus) {
		$params['friendly_params'] =   array('tipus'=>$tipus==INMO_DEFAULT_TIPUS?'':($tipus?$tipus:$rs['tipus_value']));
	}
	
	$rs['link'] = $rs['show_record_link'] = get_link( $params );
	
	
	$rs['selected'] = '';
	$rs['conta'] = $conta;
	$rs['loop_count'] = $loop_count;
	$conta++;
	
}

$vars = $listing->caption;

?>