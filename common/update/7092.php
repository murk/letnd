<?
/*
28-10-2019

*/

if ( is_table( 'product__configpublic' ) && ! Db::get_first( "SELECT * FROM product__configpublic WHERE name='can_wholesaler_edit_company'" ) ) {
	$update_sql[] = "
		INSERT INTO product__configpublic (`name`, `value`) VALUES ('can_wholesaler_edit_company', '1');";

}
else {
	if ( is_table( 'product__configpublic' ) ) {
		trigger_error( 'Ja existeixen el camp can_wholesaler_edit_company' );
	}
}

if ( is_table( 'product__configpublic' ) && ! Db::get_first( "SELECT * FROM product__configpublic WHERE name='can_wholesaler_edit_dni'" ) ) {
	$update_sql[] = "
		INSERT INTO product__configpublic (`name`, `value`) VALUES ('can_wholesaler_edit_dni', '1');";

}
else {
	if ( is_table( 'product__configpublic' ) ) {
		trigger_error( 'Ja existeixen el camp can_wholesaler_edit_dni' );
	}
}