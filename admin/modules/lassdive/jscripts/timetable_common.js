class TimetableCommon {

	static format_date(date, make_object = true) {
		if (make_object) date = new Date(date);
		let month = date.getMonth() + 1;
		// if (month < 10) month = '0' + month;
		return date.getDate() + '/' + month + '/' + date.getFullYear();
	}

	static init_tips(show_edit, $view = 'week') {

		$('#timetable-list-records,#timetables').click(() => {
			let $tip = $('#timetable-tip');
			if ($tip.length) $tip.remove();
		});

		$('[data-order-id]').click(function (e) {


			// show_edit ve de llistat d'horaris i no de triar horaris
			if (!show_edit && reserva_time_table.start) {
				return;
			}

			e.stopPropagation();

			let $tip = $('#timetable-tip');
			if ($tip.length) $tip.remove();

			let order_id = $(this).data('order-id'),
				$data = $('#data-holder-' + order_id),
				order_status = $data.data('order-status'),
				x = e.pageX,
				payed = order_status == 'payed' ? LASSDIVE_PAYED : LASSDIVE_PENDING,
				users = $data.find('span').html(),
				total_width = $(window).width(),
				tip = `<div><h5>${LASSDIVE_RESERVA}: <strong>${order_id}</strong></h5>
							<h6>${payed}</h6>
							<p>${users}</p>							
						</div>`;

			if (!order_id) return;

			if (show_edit) {
				tip += `<div onclick="Timetable.edit_reserva(${order_id})" class="button-edit"></div>`;
			}
			tip = $(`<div id="timetable-tip">${tip}</div>`);

			let $this = $(this);
			if (x > total_width / 2) {
				// let result = $this.position().left + $this.width();
				let right = $this.css('right');
				tip.css('right', right)
			} else {
				let left = $this.css('left');
				tip.css('left', left)
			}
			if ($(this).prop("tagName") === 'SPAN') {
				$(this).parent().append(tip);
			}
			else {
				$(this).append(tip);
			}

			let offset = tip.offset();

			if (offset.top - $(window).scrollTop() < $('#timetable-list-records').offset().top){
				tip.css('top', 0);
				tip.css('bottom', 'auto');
			}

		});
	}

}


