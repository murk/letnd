<?
/**
 * BookingPropertyrange
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 7-2014
 * @version $Id$
 * @access public
 */
class BookingPropertyrange extends Module{
	
	var $is_editable = true, $is_mail = false, $property_id;
	
	function __construct(){
		parent::__construct();
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function get_records()
	{
	   	Main::load_class('booking', 'common');
		return BookingCommon::get_propertyrange_records($this, $this->property_id);		
	}
	function records_walk(&$listing) {
		
		return BookingCommon::propertyrange_records_walk($listing);
		
	}
	
}
?>