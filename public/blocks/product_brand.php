<?php
/* 29-3-13
 * 
 * Block de marques que contenen productes i els seus descomptes, segons siguin promoció o tinguin descompte
 * Nomes va amb USE_FRIENLY_URL
 *  
 */
$product_filter = isset($vars['product_filter'])?$vars['product_filter']:'all'; // all, discount, promotion, all_brands ( all_brands agafa totes les marques encara que no tinguinb productes
$get_max_discount = isset($vars['get_max_discount'])?$vars['get_max_discount']:'0'; // 0 , 1 - calcula el descompte del producte més alt

$vars['var_name'] = 'brand_id';

$module = Module::load('product','brand');
$module->parent = 'Block';
$module->product_filter = $product_filter;
$module->get_max_discount = $get_max_discount;

$loop = $module->do_action('get_records');


if ($product_filter == 'discount') {
	$vars['block_selected'] = (isset($_GET['brand_id']) && isset($_GET['discount']))?1:0;
	$vars['block_link'] = LANGUAGE . '/product/brand/discount/';
}
elseif ($product_filter == 'prominent') {
	$vars['block_selected'] = (isset($_GET['brand_id']) && isset($_GET['promotion']))?1:0;
	$vars['block_link'] = LANGUAGE . '/product/brand/promotion/';
}
else {
	$vars['block_selected'] = (isset($_GET['brand_id']) && !isset($_GET['promotion']) && !isset($_GET['discount']))?1:0;
	$vars['block_link'] = LANGUAGE . '/product/brand/';
}

if ( ! $vars['block_selected'] ) {
	$vars['block_selected'] = ( isset( $_GET['brand_ids'] ) ) ? 1 : 0;
}

if (!$loop) {
	$show_block = false;
	return;
}

$loop_count = count($loop);
$conta = 0;
$brand_id = isset( $_GET['brand_id'] ) ? $_GET['brand_id'] : false;
if ( isset( $_GET['brand_ids'] ) ) R::escape_array( $_GET['brand_ids'] );

foreach ($loop as $key=>$val){
	$rs = &$loop[$key];
	$rs['class'] = '';
	$rs['conta'] = $conta++;
	$rs['loop_count'] = $loop_count;
	$rs['selected'] = $brand_id == $rs["brand_id"] && $vars['block_selected']?1:0;

	if (!empty($_GET['brand_ids'])) {
		$rs['selected'] = in_array($rs["brand_id"], $_GET['brand_ids']) ? 1 : 0;
	}
}

$loop[0]['class'] = 'first';	
$loop[count($loop)-1]['class'] = 'last';