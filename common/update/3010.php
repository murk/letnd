<?php
// no es poden posar ; entre mig de la consulta ja que parteix les consultes per el, a no ser que fem servir $update_sql en array() ;
if (is_table('product__product')){
	
	$update_sql .="CREATE TABLE IF NOT EXISTS `product__product_download` (
	`download_id` INT(11) NOT NULL AUTO_INCREMENT,
	`product_id` INT(11) NOT NULL,
	`name` VARCHAR(100) NOT NULL COLLATE 'utf8_spanish2_ci',
	`size` INT(11) NOT NULL,
	`ordre` INT(11) NOT NULL,
	`main_file` INT(1) NOT NULL,
	PRIMARY KEY (`download_id`)
	)
	COLLATE='utf8_spanish2_ci'
	ENGINE=MyISAM
	ROW_FORMAT=DEFAULT;
	
	ALTER TABLE `product__variation_category` ENGINE = MYISAM;
	ALTER TABLE `product__variation_category_language` ENGINE = MYISAM;
	";
	
	// creo el directori per guardar els productes digitals
	$client_dir = DOCUMENT_ROOT . '/' . $_SESSION['client_dir'];
	$dir=$client_dir . '/product/product/downloads/';
	if (!is_dir($dir)){
		mkdir ($dir, 0776);
	}
	
	// afegir una variable a config de config
	$update_sql .="INSERT INTO `product__configpublic` (`name`, `value`) VALUES ('payment_methods', 'paypal,account,ondelivery');";
	// canviar tots els product_no_sell_text
	$update_sql .="
		UPDATE `product__configpublic` SET `name`='no_sell_text' WHERE `name`='product_no_sell_text';
		UPDATE `product__configpublic_language` SET `name`='no_sell_text' WHERE `name`='product_no_sell_text';";
}
if (is_table('news__new')){
	
	$update_sql .="	
	ALTER TABLE `news__new` ENGINE = MYISAM;
	ALTER TABLE `news__new_file` ENGINE = MYISAM;
	ALTER TABLE `news__new_image` ENGINE = MYISAM;
	ALTER TABLE `news__new_language` ENGINE = MYISAM;
	";
	
}
?>