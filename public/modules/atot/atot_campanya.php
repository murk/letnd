<?
/**
 * AtotCampanya
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 5/11/13
 * @version $Id$
 * @access public
 */
class AtotCampanya extends Module{

	function __construct(){
		parent::__construct();
	}
	function on_load(){
		
		$this->set_friendly_params(array(
			'municipi_id'=>R::id('municipi_id')
			));
		parent::on_load();
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function get_records()
	{
		$municipi_id =  R::id('municipi_id');
		if (!$municipi_id) Main::redirect ('/');
		
		
		$descripcio = Db::get_first("
			SELECT descripcio 
			FROM atot__municipi_language 
			WHERE municipi_id = " . $municipi_id . "
				AND language = '" . LANGUAGE . "'");
		
		$this->set_var('municipi_descripcio',$descripcio);
		
	    $poblacio = Db::get_first("SELECT municipi FROM atot__municipi WHERE municipi_id = " . $municipi_id);
		$poblacio = $this->caption['c_tab_2'] . ' - ' . $poblacio;
		Page::set_title($poblacio);
		
		$listing = new ListRecords($this);
		
		$listing->join = "JOIN atot__espectacle USING (espectacle_id)";
		$listing->condition = "
			status = 'public'
			AND (tipus_concert = 'escolar' OR tipus_concert = 'tots')
			AND atot__espectacle.bin=0
			AND municipi_id = " . R::id('municipi_id');
		
		
		$listing->group_fields = array('lloc');
		$listing->order_by = 'lloc ASC, curs_ordre ASC';
		
		$listing->call('records_walk','espectacle_id');
		
		$listing->set_var('is_home', '0');
		$listing->set_var('is_poblacio', true);
				
	    $ret = $listing->list_records();
		
		
	    return $ret;
	}
	function records_walk($espectacle_id) {		
		
		
		$query = "SELECT genere, aforament_maxim, espectacle, destacat
					FROM atot__espectacle, atot__espectacle_language 
					WHERE atot__espectacle.espectacle_id = " . $espectacle_id . "
					AND atot__espectacle.espectacle_id =  atot__espectacle_language.espectacle_id
					AND language = '" . LANGUAGE . "'
					AND status = 'public' AND bin = 0";
		$ret = Db::get_row($query);
		
		$ret['dumm'] = '';// sense això peta el $ret +
		$ret += UploadFiles::get_record_images($espectacle_id, false, 'atot', 'espectacle');
		
		$ret['espectacle_link'] = get_record_link('atot', 'espectacle', $espectacle_id, 11, array('municipi_id'=>R::id('municipi_id')));
		
		return $ret;
	}
}
?>