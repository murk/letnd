<table border="0" cellspacing="0" cellpadding="0" width="100%" class="listRecord">
    <tr>
      <td class="listCaption">&nbsp;</td>
      <td class="listCaption">&nbsp;
      <?=strtoupper($c_ref)?></td>
      <td class="listCaption">
          &nbsp;
          <?=$c_product_title?></td>
      <td class="listCaption">&nbsp;
        <?=$c_product_variations?></td>

	    <? if ( $has_order_date ): ?>

		    <td class="listCaption" align="right">
			    <?= $c_product_date ?>
			    &nbsp;</td>

	    <? endif // $has_order_date ?>

      <td class="listCaption" align="right">
          <?=$c_quantity?>
          &nbsp;</td>
      <td class="listCaption" align="right">
          <?=$c_product_base?>
          &nbsp;</td>
      <td class="listCaption" align="right">
          <?=$c_product_total_base?>
          &nbsp;</td>
    </tr>
  <? foreach($loop as $l): extract ($l)?>
    <tr class="listItem<?=$odd_even?>" onmousedown="change_bg_color(this, 'Down', '<?=$id?>')" id="list_row_<?=$id?>">
      <td valign="top" class="listImage" width="<?=round($im_admin_thumb_w/2)?>">	  
	  <?if ($image_name):?><div><a href="/admin/?action=show_form_edit&menu_id=20004&product_id=<?=$product_id?>"><img src="<?=$image_src_admin_thumb?>" width="<?=round($im_admin_thumb_w/2)?>"></a></div>
	  <?else: // image_name?><div class="noimage" style="min-height:<?=round($im_admin_thumb_h/2)?>px;"></div>
	  <?endif // image_name?>	  
	  </td>
      <td valign="top">
        &nbsp;
        <?=$ref?>
        &nbsp;</td>
      <td valign="top"><? if ($product_title):?>
          <a href="/admin/?action=show_form_edit&menu_id=20004&product_id=<?=$product_id?>">
          <?=$product_title?>
          </a>
        <? endif //product_title?>
        &nbsp;</td>
	    <td valign="top">
		    <? foreach ( $product_variation_loop as $l ): extract( $l ) ?>
			    <p><strong><?= $product_variation_category ?></strong>: <?= $product_variation ?></p>
		    <? endforeach //$product_variations_loop?>
	    </td>
	    <? if ( $has_order_date ): ?>

		    <td align="right" valign="top">
			    <?= $product_date ?>
			    &nbsp;</td>

	    <? endif // $has_order_date ?>
      <td align="right" valign="top">
          <?=$quantity?>
      &nbsp;</td>
      
      <td align="right" valign="top">
          <?=$product_base?>
          <?=CURRENCY_NAME?>
          &nbsp; </td>
      <td align="right" valign="top">
          <?=$product_total_base?> 
          <?=CURRENCY_NAME?>
         &nbsp; </td>
    </tr>
    <? endforeach //$loop?>
</table>
<table border="0" align="right" cellpadding="0" cellspacing="0" style="margin:10px 30px 0 0;line-height:16px;">
  <tr>
    <td style="padding-right:10px;"><?=$c_total_base?>
    <br /><?=$c_rate_basetax?><br /><?=$c_total_tax?><? if ($total_equivalencia):?><br /><?=$c_total_equivalencia?><? endif //total_equivalencia?><? if ($promcode_discount):?><br /><?=$c_promcode_discount?><? endif //promcode_discount?><br /><strong><?=$c_all_basetax?>
</strong></td>
    <td align="right"><?=$total_base?>&nbsp;<?=CURRENCY_NAME?>
    &nbsp;<br /><?=$rate_base?>&nbsp;<?=CURRENCY_NAME?>
    &nbsp;<br /><?=$all_tax?>&nbsp;<?=CURRENCY_NAME?>
    <? if ($total_equivalencia):?>&nbsp;<br /><?=$total_equivalencia?>&nbsp;<?=CURRENCY_NAME?><? endif //total_equivalencia?>
    <? if ($promcode_discount):?>&nbsp;<br />-<?=$promcode_discount?>&nbsp;<?=CURRENCY_NAME?><? endif //promcode_discount?>
    &nbsp;<br /><strong><?=$all_basetax?>&nbsp;<?=CURRENCY_NAME?>
    </strong>&nbsp;</td>
  </tr>
</table>