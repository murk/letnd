
const LASSDIVE_PRINTING = "Imprimint";
const LASSDIVE_NUM_RESERVA = 'Número de reserva: ';
const LASSDIVE_PAGAT = 'Pagat';
const LASSDIVE_PAGA_SENYAL = 'Paga i senyal: ';
const LASSDIVE_PENDENT = 'Pendent: ';
const LASSDIVE_RESERVA_ITEM_DATE = 'Dia';
const LASSDIVE_HORARI = 'Horari';
const LASSDIVE_CENTRE = 'Centre';
const LASSDIVE_USER_IDS = 'Instructor';

const LASSDIVE_HORA_MES_GRAN = 'The second selected hour must be greater than first';
const LASSDIVE_NO_DISPONIBLE = 'This period cannot be selected, there is no availability';
const LASSDIVE_HORA_DIFERENTS_CENTRES = 'You can not select times from diferent centers';

const LASSDIVE_NO_RESPONSE = ' No printer response';
const LASSDIVE_PRINT_SUCCESS = ' Ticket printed correctly';
const LASSDIVE_ASB_DRAWER_KICK = ' Status of the drawer kick number 3 connector pin= "H"';
const LASSDIVE_ASB_BATTERY_OFFLINE = ' Offline due to a weak battery';
const LASSDIVE_ASB_OFF_LINE = ' Offline status';
const LASSDIVE_ASB_COVER_OPEN = ' Cover is open';
const LASSDIVE_ASB_PAPER_FEED = ' Paper feed switch is feeding paper';
const LASSDIVE_ASB_WAIT_ON_LINE = ' Waiting for online recovery';
const LASSDIVE_ASB_PANEL_SWITCH = ' Panel switch is ON';
const LASSDIVE_ASB_MECHANICAL_ERR = ' Mechanical error generated';
const LASSDIVE_ASB_AUTOCUTTER_ERR = ' Auto cutter error generated';
const LASSDIVE_ASB_UNRECOVER_ERR = ' Unrecoverable error generated';
const LASSDIVE_ASB_AUTORECOVER_ERR = ' Auto recovery error generated';
const LASSDIVE_ASB_RECEIPT_NEAR_END = ' No paper in the roll paper near end detector';
const LASSDIVE_ASB_RECEIPT_END = ' No paper in the roll paper end detector';
const LASSDIVE_ASB_BUZZER = ' Sounding the buzzer';
const LASSDIVE_ASB_WAIT_REMOVE_LABEL = ' Waiting to remove label';
const LASSDIVE_ASB_SPOOLER_IS_STOPPED = ' Stop the spooler';

const LASSDIVE_ESCOLLIR_CENTRE = 'You must select a center';
const LASSDIVE_ESCOLLIR_PRODUCT_CENTRE = 'On week view you must choose product and center';

// TIPS
const LASSDIVE_RESERVA = 'Book';
const LASSDIVE_PAYED = 'Payed';
const LASSDIVE_PENDING = 'Pending';

const LASSDIVE_NO_DATE = 'No date';
const LASSDIVE_CONFIRMAR_ESBORRAR_HORARI = "The timetable, center and instructors assigned to this activity will be deleted are you sure you want to continue?";
const LASSDIVE_NO_ESBORRAT_HORARI = "The timetable could not be deleted";