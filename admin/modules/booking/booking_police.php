<?

/**
 * BookingPolice
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2014
 * @version $Id$
 * @access public
 */
class BookingPolice extends Module {

	function __construct() {
		parent::__construct();
	}

	function list_records() {
		$GLOBALS['gl_content'] = '<script type="text/javascript" src="/admin/modules/booking/jscripts/functions.js?v=' . $GLOBALS['gl_version'] . '"></script>' . $this->get_records();
	}

	function get_records() {

		$module = Module::load( 'booking', 'book', '', false );
		$module->unset_field( 'ref' );

		$listing = new ListRecords( $module );

		$date1 = unformat_date( R::escape( 'date1' ) );
		$date2 = unformat_date( R::escape( 'date2' ) );

		//Debug::p($date1, 'text');
		//Debug::p($date2, 'text');

		// Per defecte entre ahir i ahir, els clients que fa 24hores que han arribat
		if ( ! $date1 && ! $date2 ) {
			$date1 = date( "Y-m-d", time() - 60 * 60 * 24 );
			$date2 = date( "Y-m-d", time() - 60 * 60 * 24 );
		}

		// si no poso una data, busco nomes un dia
		if ( ! $date1 ) $date1 = $date2;
		if ( ! $date2 ) $date2 = $date1;

		$listing->condition = "
			date_in BETWEEN  '$date1' AND '$date2'
			AND is_owner = 0
			AND status IN ('booked','confirmed_email','confirmed_post','contract_signed')";
		$listing->add_button( 'edit_button', '', 'boto1', 'auto', 'booking.show_form_edit' );
		$listing->add_options_button( 'download_selected', '', $class = 'botoOpcions1', $place = 'top_before', 'bookpolice.download_selected' );

		$listing->set_options( 1, 1, 0, 1, 0, 0, 0, 0, 1, 1 );
		$listing->has_bin  = false;
		$listing->paginate = false;
		$listing->order_by = 'date_in ASC';

		$listing->call('records_walk',false, true, $this);

		return $this->get_search_form( $date1, $date2 ) . $listing->list_records();
	}


	function records_walk( &$listing ) {

		$id          = $listing->rs['book_id'];
		$price_total = $listing->rs['price_total'];


		/*
		  if (isset($listing->buttons['edit_book_button'])){
		  $listing->buttons['edit_book_button']['link_end'] ='&amp;property_id=' . $property_id . $listing->buttons['edit_book_button']['link_end'];
		  }

		 */


		$q           = New Query( 'booking__book_to_custumer' );
		$q->set_id_field( 'book_id' );
		$q->order_by = 'ordre ASC';
		$custumers   = $q->get_imploded( 'custumer_id', $id );

		$ret = array();


		// custumers
		$ret += $this->get_custumers( $custumers, $id );


		// $ret['price_total'] = '<div class="price-total">' . format_currency($price_total) . ' €</div>';
		$ret['price_total'] = format_currency( $price_total );


		return $ret;

	}

	function get_custumers( $ids, $book_id ) {


		if ( ! $ids ) return array();


		$module = Module::load( 'custumer', 'custumer', '', false );

		$listing = new ListRecords( $module );

		$listing->set_var( 'book_id', $book_id );
		$listing->name      = 'custumers';
		$listing->condition = 'custumer_id IN (' . $ids . ')';
		$listing->template  = 'booking/book_police_custumer_list';
		$listing->order_by  = 'FIELD(custumer_id,' . $ids . ')';
		$listing->set_records();

		foreach ($listing->loop as $key=>&$rs) {
			$nationality =
					Db::get_first( "SELECT country 
										FROM all__country 
										WHERE country_id = '" . $rs['nationality'] . "'" );
			$rs['nationality'] = $nationality;
			$treatment = $rs['treatment'];
			if ($treatment) $rs['treatment']   = $this->caption["c_treatment_$treatment"];
		}

		$custumers['custumers'] = $listing->process();

		return $custumers;
	}

	function download_selected() {
		$selected = $_POST['selected'];
		R::escape_array( $selected );

		$tipus_registre   = 1;
		$codi_establiment = $this->config['codi_establiment'];
		$nom_establiment  = mb_strtoupper( $this->config['nom_establiment'], 'utf-8' );
		$data_confeccio   = date( "Ymd", time() );
		$hora_confeccio   = date( "Hi", time() );
		$nombre_registres = 0;

		$text = '';


		foreach ( $selected as $book_id ) {

			$date_in = Db::get_first( "SELECT date_in FROM booking__book WHERE book_id = '" . $book_id . "'" );

			$custumers = Db::get_rows( "
				SELECT 2 AS type, vat,'' AS vat_foreigners, vat_type, vat_date, surname1, surname2, name, treatment, birthdate, nationality, '' AS date_in
					FROM custumer__custumer 
					WHERE custumer_id 
						IN (SELECT custumer_id 
								FROM booking__book_to_custumer 
								WHERE book_id = " . $book_id . ")
					ORDER BY book_main DESC" );
			foreach ( $custumers as $custumer ) {

				if ( $custumer['nationality'] != 'ES' ) {
					$custumer['vat_foreigners'] = $custumer['vat'];
					$custumer['vat']            = '';
				}

				$custumer['vat_date'] = str_replace( '-', '', $custumer['vat_date'] );
				if ( $custumer['vat_date'] == '00000000' ) $custumer['vat_date'] = '';

				$custumer['treatment']   = $custumer['treatment'] == 'sr' ? 'M' : 'F';
				$custumer['birthdate']   = str_replace( '-', '', $custumer['birthdate'] );
				$custumer['nationality'] =
					Db::get_first( "SELECT country 
										FROM all__country 
										WHERE country_id = '" . $custumer['nationality'] . "'" );
				$custumer['date_in']     = str_replace( '-', '', $date_in );

				// netejo caracters raros que no poden sortir
				foreach ( $custumer as &$c ) {
					$c = mb_strtoupper( $c, 'utf-8' );
					$c = str_replace( '|', '', $c );
				}

				$text .= "\r\n" . implode( '|', $custumer );
				$nombre_registres ++;
			}
		}

		if ( $text ) {
			$text = $tipus_registre . '|' . $codi_establiment . '|' . $nom_establiment . '|' . $data_confeccio . '|' . $hora_confeccio . '|' . $nombre_registres . $text;

			$file_name = $codi_establiment . '.001.txt';
			$text      = utf8_decode( $text );
			UploadFiles::serve_download( '', $file_name, 1, $text, 'text/plain' );
			die( $file_name );
		}
		else {
			$GLOBALS['gl_page']->show_message( 'No hi ha cap dada per exportar' );
		}

	}

	function get_search_form( $date1, $date2 ) {
		$this->caption['c_by_ref'] = '';
		$this->set_vars( $this->caption );
		$this->set_file( '/booking/police_search.tpl' );
		$this->set_var( 'menu_id', $GLOBALS['gl_menu_id'] );
		$this->set_var( 'date1', format_date_form( $date1 ) );
		$this->set_var( 'date2', format_date_form( $date2 ) );

		return $this->process();
	}

}

?>