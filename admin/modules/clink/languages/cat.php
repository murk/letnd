<?php
// menus eina
if (!defined('CLINK_MENU_NEW')){
	define('CLINK_MENU_NEW', 'Noticias');
	define('CLINK_MENU_NEW_NEW', 'Entrar nueva noticia');
	define('CLINK_MENU_NEW_LIST', 'Listar i editar noticias');
	define('CLINK_MENU_NEW_BIN', 'Papelera de reciclaje');

	define('CLINK_MENU_MEDIO', 'Medios');
	define('CLINK_MENU_MEDIO_NEW', 'Entrar nuevo medio');
	define('CLINK_MENU_MEDIO_LIST', 'Listar i editar medios');
	define('CLINK_MENU_MEDIO_BIN', 'Papelera de reciclaje');

	define('CLINK_MENU_ENCUENTRO', 'Encuentros');
	define('CLINK_MENU_ENCUENTRO_NEW', 'Entrar nuevo encuentro');
	define('CLINK_MENU_ENCUENTRO_LIST', 'Listar i editar  encuentros');
	define('CLINK_MENU_ENCUENTRO_BIN', 'Papelera de reciclaje');

	define('CLINK_MENU_CURRICULUM', 'Currículums');
	define('CLINK_MENU_CURRICULUM_NEW', 'Entrar nuevo currículum');
	define('CLINK_MENU_CURRICULUM_LIST', 'Listar i editar currículums');
	define('CLINK_MENU_CURRICULUM_BIN', 'Papelera de reciclaje');
}

$gl_messages_new['no_category']='Per poder insertar notícies hi ha d\'haver almenys una categoria';

// captions seccio
$gl_caption_new['c_prepare'] = 'En preparació';
$gl_caption_new['c_review'] = 'Per revisar';
$gl_caption_new['c_public'] = 'Públic';
$gl_caption_new['c_archived'] = 'Arxivat';
$gl_caption_new['c_new'] = 'Notícia';
$gl_caption_medio['c_new'] = 'Medio';
$gl_caption_encuentro['c_new'] = 'Encuentro';
$gl_caption_currículum['c_new'] = 'Currículum';
$gl_caption_new['c_title'] = 'Títol';
$gl_caption_new['c_subtitle'] = 'Subtítol';
$gl_caption_new['c_image_caption'] = 'Peu de foto';
$gl_caption_new['c_image_caption2'] = 'Peu de foto 2';
$gl_caption_new['c_content'] = 'Contingut';
$gl_caption_new['c_entered'] = 'Data publicació';
$gl_caption_new['c_expired'] = 'Data caducitat';
$gl_caption_new['c_status'] = 'Estat';
$gl_caption_new['c_category_id'] = 'Categoria';
$gl_caption_new['c_filter_category_id'] = 'Totes les categories';
$gl_caption_new['c_filter_status'] = 'Tots els estats';
$gl_caption_new['c_ordre'] = 'Ordre';
$gl_caption_new['c_source'] = 'Font';

$gl_caption_category['c_category'] = 'Categoria';
$gl_caption_category['c_new_button'] = 'Veure notícies';
$gl_caption_instalaciocategory['c_new_button'] = 'Veure instal·lacions';
$gl_caption_equipcategory['c_new_button'] = 'Veure equips';
$gl_caption_category['c_ordre'] = 'Ordre';
$gl_caption_image['c_name'] = 'Nom';
$gl_caption_new['c_url1_name']='Nom URL(1)';
$gl_caption_new['c_url1']='URL(1)';
$gl_caption_new['c_url2_name']='Nom URL(2)';
$gl_caption_new['c_url2']='URL(2)';
$gl_caption_new['c_videoframe']='Video (youtube, metacafe...)';
$gl_caption_new['c_file']='Arxius';
$gl_caption_new['c_in_home']='Surt a la home';
?>
