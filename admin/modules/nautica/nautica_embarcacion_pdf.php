<?
// iniciar pdf
//ini_set("memory_limit","40M"); // aumento memoria, haig de controlar l'error quan sobrepassa
		//define("K_TCPDF_EXTERNAL_CONFIG", true);
		require_once(DOCUMENT_ROOT . 'common/includes/tcpdf5/tcpdf.php');
		//require_once(DOCUMENT_ROOT . 'common/includes/tcpdf5/config/tcpdf_config_alt.php');
		
class PDF extends TCPDF {
	var $path, $destination='i', $is_admin; // destination = i->enviar al navegador o d->forçar descarrega
    function Header()
    {        
		if (!$this->is_admin) $this->Image($this->path.'header.jpg', 10, 0, 190, 28.45);    
		if (!$this->is_admin) $this->Image($this->path.'footer.jpg', 10, 164, 190, 133);
    }
	function show_pdf(&$show){
		//writeHTMLCell 	($w,$h,$x,$y,$html = '',$border = 0,$ln = 0,$fill = false,$reseth = true,$align = '',$autopadding = true) 	
		
		$this->SetDisplayMode('default');
		$this->SetMargins(19, 19);
		$this->SetAutoPageBreak(true, 10);
		
		$this->AddPage();
		extract($show->rs);
		extract($show->caption);
		$title = $model . '_' . unformat_int($dossier) . '_' . $clau;
		$this->SetTitle($title);	
		
		if ($clau){
		$html = '<span style="font-size:4mm;">'.$clau.'</span>';		
		$this->writeHTMLCell(178, 5, 13.5, 10, $html, 0, 0, 0, true, 'L', false);
		}
		
		$html = '<span style="font-size:4mm;">'.$dossier.'</span>';		
		$this->writeHTMLCell(180.5, 5, 16, 10, $html, 0, 1, 0, true, 'R', true);

		
		$this->Image(CLIENT_PATH . 'nautica/embarcacion/images/' . $image_name, 16, 28.45, 178, 133.5);		
		
		$html = '<span style="color:#d91018;font-size:5mm;font-weight:bold;height:5mm">'.$shipyard_id.' '.$model.'</span>';
					
		$this->writeHTMLCell(152, 16.8, 19, 167, $html, 0, 1, 0, true, 'L', true);
		
		$html = '<span style="font-size:4mm;height:4mm"><b>'.$c_engine.'</b>: '.$engine.'</span>';					
		$this->writeHTMLCell(152, 16.8, 19, 173, $html, 0, 1, 0, true, 'L', true);
		
		$html = '<b style="color:#d91018;font-size:5mm;">'.$price.' </b>';		
		$this->writeHTMLCell(50, 10, 141, 169.5, $html, 0, 1, 0, true, 'R', true);
		
		
		
		$this->SetXY (20.8, 187);
		$depth2 = $depth2!='0,00'?' - ' . $depth2 . ' m.':'';
		if ($hour_use=='0') $hour_use='';
		if ($length=='0,00') $length='';
		if ($breadth=='0,00') $breadth='';
		if ($weight=='0') $weight='';
		if ($depth=='0,00') $depth='';
		
		$tbl = '<table cellspacing="0" cellpadding="0" border="0" style="font-size:4mm;">';
		
		if ($length) $length .= ' m.';
		if ($breadth) $breadth .= ' m.';
		if ($weight) $weight .= ' t.';
		if ($depth) $depth .= ' m.';
		if ($depth2 && $depth) $depth .= $depth2;
		
		// tot per si hi ha un valor no omplert, al haver 2 columnes haig de redistribuir tot
		$camps = array('buildyear','length','fuel','breadth','hour_use','weight','flag_id','depth');
		$camps_amb_valor = array();
		
		$num = 1;
		$conta= 0;
		$conta_total = 0;
		foreach ($camps as $camp){
			//if ($$camp) {  // comento això per que volen que es mostri el camp igualment encara que no hi hagi valor
				$camps_amb_valor[$conta][$num] = $camp;
				$num=$num==1?2:1;				
				$conta_total++;
			//}
			if ($num==1) $conta++;
		}
		
		foreach ($camps_amb_valor as $key => $val){
			$val1 = ${$val[1]};
			$caption1 = ${'c_'.$val[1]};
			
			if (isset($val[2])){
				$val2 = ${$val[2]};
				$caption2 = ${'c_'.$val[2]};
			}
			else{
				$val2 = '';
				$caption2 = '';
			}
			
			$tbl .= $this->get_row1($caption1, $val1, $caption2, $val2);
		}
		
		$tbl .= '</table>';
		
		if ($conta_total>0) $this->Image($this->path.'punt.png', 20.8, 188, 5.16, 3.3);
		if ($conta_total>2) $this->Image($this->path.'punt.png', 20.8, 194.3, 5.16, 3.3);   
		if ($conta_total>4) $this->Image($this->path.'punt.png', 20.8, 200.6, 5.16, 3.3);   
		if ($conta_total>6) $this->Image($this->path.'punt.png', 20.8, 206.9, 5.16, 3.3); 
		
		if ($conta_total>1) $this->Image($this->path.'punt.png', 120, 188, 5.16, 3.3);
		if ($conta_total>3) $this->Image($this->path.'punt.png', 120, 194.3, 5.16, 3.3);   
		if ($conta_total>5) $this->Image($this->path.'punt.png', 120, 200.6, 5.16, 3.3);   
		if ($conta_total>7) $this->Image($this->path.'punt.png', 120, 206.9, 5.16, 3.3);   
		
		
		$this->writeHTML($tbl, true, false, false, false, '');
		
		$this->SetXY (20.8, 220);		
		
		$tbl = '<table cellspacing="0" cellpadding="0" border="0" style="font-size:3.5mm;">';
		
		$tbl .= $this->get_row2(strtoupper($c_equipament), $this->limit_description($equipament));
		$tbl .= $this->get_row2($c_characteristics_electronic_caps, implode_field($electronics,'c_detail',', '));
		$tbl .= $this->get_row2(strtoupper($c_characteristics_extra), implode_field($extras,'c_detail',', '));
		$tbl .= $this->get_row2(strtoupper($c_characteristics_sail), implode_field($sails,'c_detail',', '));
		
		$tbl .= '</table>';
		
		
		$this->writeHTML($tbl, true, false, false, false, '');
		
		
		
		$this->Output($title . '.pdf', $this->destination);
	}
	function get_row1($caption,$text,$caption2,$text2){
		
			return '								
			<tr>
				<td align="left" height="6.3mm" width="4.8mm" style="color:#d91018"></td>
				<td align="left" width="39.7mm"><b>'.$caption.'</b></td>
				<td align="left" width="55mm">'.$text.'</td>
				<td align="left" width="4.8mm" style="color:#d91018;"></td>
				<td align="left" width="26mm"><b>'.$caption2.'</b></td>
				<td align="left" width="130mm">'.$text2.'</td>
			</tr>';
	}
	function get_row2($caption,$text){
		if ($text) {
			return '								
			<tr>
				<td><b>'.$caption.'</b></td>
			</tr>
			<tr>			
				<td>'.$text.'</td>
			</tr>
			<tr >			
				<td height="4.4mm"></td>
			</tr>';
		}
		else return '';
	}
	function limit_description($value){
		strip_tags($value, '<p>');
		$length = 300;
		if (strlen($value) > $length)
		{			
			$value = strip_tags($value);
			$value = substr($value, 0, $length);
			$lastspace = strrpos($value, " ");
			$value = substr($value, 0, $lastspace) . " ...";
		}
		return $value;
	}
}
?>