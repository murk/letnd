<!DOCTYPE html>
<html lang="<?=LANGUAGE_CODE?>">
<head>

	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
	<meta name="format-detection" content="telephone=no"/>

	<link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,300,400italic,600,600italic,700,700italic' rel='stylesheet' type='text/css'>

	<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<? /* <script type="text/javascript" src="/common/jscripts/jquery/jquery.migrate.1.4.js"></script> */ ?>

	<?= load_scss() ?>
	<?= get_header( $this ) ?>

</head>

	<body class="clean">

		<header>

			<h1 title="<?=htmlspecialchars($title)?>"><?=$title?></h1>

			<?if($body_subtitle):?>
				<h2 title="<?=htmlspecialchars($body_subtitle)?>"><?=$body_subtitle?></h2>
			<?endif //body_subtitle?>


		</header>


		<main class="page">

				<?=$content?>

		</main>

		<iframe name="save_frame" id="save_frame" class="<?=$showiframe?>"></iframe>

		<?/* JSCRIPT */?>
		<script type="text/javascript">

			var a_vars = Array();var pagename='<?=addslashes($title)?>';var phpmyvisitesSite = 1;var phpmyvisitesURL = "<?=HOST_URL?>common/includes/phpmv2/phpmyvisites.php";

			var gl_language = '<?=$language?>';
			var gl_is_home = <?=$is_home?'true':'false'?>;
			var gl_tool = '<?=$tool?>';
			var gl_tool_section = '<?=$tool_section?>';
			var gl_action = '<?=$action?>';
			var gl_page_text_id = '<?=$page_text_id?>';
			var gl_dir_templates = '<?=DIR_TEMPLATES?>';
		    var gl_has_google_maps_key = <?= GOOGLE_MAPS_KEY?'true':'false' ?>;

			$(function(){
				<?=$javascript?>
			});
		</script>

		<?if(can_accept_cookies()):?>
			<?=GOOGLE_ANALYTICS?>
		<?else: //?>
			<?=get_cookies_message()?>
		<?endif //cookies?>

		<?= load_jscript( '
			/common/jscripts/lightgallery/js/lightgallery.min.js,
			/common/jscripts/lightgallery/js/lg-thumbnail.min.js,
			/common/jscripts/lightgallery/js/lg-zoom.min.js,
			/common/jscripts/jquery.cookie.js,
			/common/jscripts/slick/slick-1.8.0/slick.min.js,
			/common/jscripts/jquery.blockUI-2.70.js,
			/common/jscripts/jquery-ui-1.11.2/jquery-ui.min.js,
			/admin/jscripts/validacio.js,
			/common/jscripts/functions.js,' .
			//{{#is_inmo}}
			'/public/modules/booking/jscripts/functions.js,'.
			'/common/jscripts/multifile/jquery.MultiFile.js,'.
			//{{/is_inmo}}
			//{{#is_product}}
			'/public/modules/product/jscripts/functions.js,'.
			DIR_TEMPLATES.'jscripts/functions_product.js,'.
			//{{/is_product}}
            DIR_TEMPLATES . 'jscripts/functions.js'
		) ?>
		<script type="text/javascript" src="/common/jscripts/jquery-ui-1.11.2/languages/datepicker-<?= $language_code ?>.js"></script>
		<script type="text/javascript" src="/admin/languages/<?= $language ?>.js"></script>

		<? if ( GOOGLE_MAPS_KEY ): ?>
			<script type="text/javascript" src="//maps.google.com/maps/api/js?key=<?= GOOGLE_MAPS_KEY ?>"></script>
		<? endif // letnd:AIzaSyAaIGZEloZFoSY8TuTDyL27MDbuDvW5AoM ?>
		<? /* FI JSCRIPT */ ?>

		<?/* FI JSCRIPT */?>

	</body>
</html>