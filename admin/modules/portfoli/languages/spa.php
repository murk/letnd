<?php
// menus eina
if (!defined('PORTFOLI_MENU_PORTFOLI')){
	define('PORTFOLI_MENU_PORTFOLI', 'Portafoli');
	define('PORTFOLI_MENU_PORTFOLI_NEW', 'Nova entrada');
	define('PORTFOLI_MENU_PORTFOLI_LIST', 'Llistar entrades');
	define('PORTFOLI_MENU_PORTFOLI_BIN', 'Paperera reciclatge');
	define('PORTFOLI_MENU_CATEGORY', 'Categorías');
	define('PORTFOLI_MENU_CATEGORY_NEW', 'Entrar nueva categoría');
	define('PORTFOLI_MENU_CATEGORY_LIST', 'Listar categorías');
}

$gl_caption_portfoli['c_videoframe']='Vídeo (youtube, metacafe...)';
$gl_caption_portfoli['c_status'] = 'Estat';
$gl_caption_portfoli['c_ordre'] = 'Ordre';
$gl_caption_portfoli['c_prepare'] = 'En preparació';
$gl_caption_portfoli['c_review'] = 'Per revisar';
$gl_caption_portfoli['c_public'] = 'Públic';
$gl_caption_portfoli['c_archived'] = 'Arxivat';
$gl_caption_portfoli['c_title'] = 'Títol';
$gl_caption_portfoli['c_subtitle'] = 'Subtítol';
$gl_caption_portfoli['c_description'] = 'Descripció';
$gl_caption_portfoli['c_url1_name']='Nom URL(1)';
$gl_caption_portfoli['c_url1']='URL(1)';
$gl_caption_portfoli['c_url2_name']='Nom URL(2)';
$gl_caption_portfoli['c_url2']='URL(2)';
$gl_caption_portfoli['c_modified'] = 'Data modificació';
$gl_caption_portfoli['c_page_id'] = 'Pàgines';
$gl_caption_portfoli['c_destacat'] = 'Pàgina principal projectes';
$gl_caption_portfoli['c_destacat_not_page_id'] = 'Projecte destacat';
$gl_caption_portfoli['c_form_level1_title_destacat'] = 'Destacats';
$gl_caption_portfoli['c_form_level1_title_page_title'] = $GLOBALS['gl_caption']['c_seo'];
$gl_caption_portfoli['c_filter_destacat'] = 'Tots, destacats o no';
$gl_caption_portfoli['c_destacat_0']='No destacat';
$gl_caption_portfoli['c_destacat_1']='Destacat';
$gl_caption_portfoli['c_entered'] = 'Fecha creación';

$gl_caption['c_portfoli_file_name'] = $GLOBALS['gl_caption']['c_file_name'];
$gl_caption['c_portfoli_old_file_name'] = $GLOBALS['gl_caption']['c_old_file_name'];



$gl_caption_category['c_category'] = $gl_caption['c_category_id'] = 'Categoría';
$gl_caption_category['c_new_button'] = 'Ver proyectos';
$gl_caption_category['c_ordre'] = 'Orden';

?>