<?php
// no es poden posar ; entre mig de la consulta ja que parteix les consultes per el ;

$update_sql = "

ALTER TABLE `all__language` ADD `language_description` VARCHAR( 32 ) NOT NULL AFTER `language`;

INSERT INTO `all__language` (
`language` , `language_description` , `code` , `code2` , `admin` , `public` , `ordre` ) 
VALUES (
'Español', 'colombia', 'col', 'es', '0', '0', '3'
);

UPDATE `all__language` SET `ordre` = '4' WHERE `language_id` =3 LIMIT 1 ;
UPDATE `all__language` SET `ordre` = '5' WHERE `language_id` =4 LIMIT 1 ;
UPDATE `all__language` SET `ordre` = '6' WHERE `language_id` =5 LIMIT 1 ;
";
?>