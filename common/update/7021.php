<?php
/* 20-6-17
Mínim persones i ítems

*/


if ( is_table( 'product__product' ) && ! is_field( 'product__product', 'min_quantity' ) ) {

	$update_sql[] = "ALTER TABLE `product__product`
	ADD COLUMN `min_quantity` INT(11) NOT NULL DEFAULT 1 AFTER `certification`;";

} else {
	if ( is_table( 'product__product' ) ) {
		trigger_error( 'Ja existeixen el camp min_quantity' );
	}
}

?>