<?

function list_records()
{
	global $tpl, $gl_content, $gl_print_link;

    $template = 'admintotal/' . (isset($_GET['template_content'])?$_GET['template_content'] . '.tpl':'bbdd.tpl');
	$tpl->set_file($template);
	$tables = Db::get_rows('SHOW TABLES');
	$last_prefix = '';
	foreach($tables as $row){
		$table = current($row);
		$table_arr = array();
		// extreure el prefix
		$prefix = substr($table, 0, strpos($table,'__'));
		//$tpl->set_var('table_prefix',$prefix!=$last_prefix?$prefix:'');
		$table_arr['table_prefix'] = $prefix!=$last_prefix?$prefix:'';

		$fields = Db::get_rows('SHOW COLUMNS FROM ' . $table);
		$table_arr['table'] = $table;
		//$tpl->set_var('table',$table);
		foreach($fields as $field){
			$field_arr =  array();
			foreach($field as $key=>$value){
				$field_arr[$key] = $value;
			}
			$table_arr['loop2'][] = $field_arr;
		}
		$loop[] = $table_arr;
		$last_prefix = $prefix;

	}
	$tpl->set_loop('loop', $loop);
	$tpl->set_var('print_link', $gl_print_link);
	$gl_content = $tpl->process();
}


?>