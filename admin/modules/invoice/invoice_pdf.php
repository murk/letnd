<?

/**
 * InvoicePdf
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2017
 * @version $Id$
 * @access public
 *
 */
class InvoicePdf {
	var $pdf, $body_count = 0, $rs, $module;

	function __construct( &$module ) {

		$this->module = &$module;

		// create new PDF document
		$this->pdf = new PDF( 'P', 'mm', 'A4', true, 'UTF-8', false );
		$pdf       = &$this->pdf;

		$pdf->setCellHeightRatio( 1.5 );

		// set document information
		$pdf->SetCreator( 'LETND' );

		$pdf->SetAutoPageBreak( true, 25 );
		$pdf->SetDisplayMode( 'real' );
		$pdf->AddFont( 'opensansextrabold', '', 'opensansextrabold.php' );
		$pdf->AddFont( 'opensans-light', '', 'opensans-light.php' );
		$pdf->AddFont( 'opensansb', '', 'opensansb.php' );
		$pdf->AddFont( 'opensans', '', 'opensans.php' );

	}

	public function get() {

	}

	public function get_body( $invoice_id ) {
		$pdf = &$this->pdf;

		$pdf->AddPage();

		$this->rs = Db::get_row( "SELECT * FROM invoice__invoice WHERE invoice_id = $invoice_id" );

		$this->set_customer( $invoice_id );
		$this->set_invoice_date( $invoice_id );
		$this->set_concepts( $invoice_id );
		$this->set_prices();

		$this->body_count ++;
	}

	public function output( $file_name = false, $save_path = false ) {

		if ( $this->rs['invoice_number'] != 0 ) {
			$rs = &$this->rs;
		}
		else {
			$custumer_id = $this->rs['custumer_id'];
			$rs = Db::get_row( "SELECT * FROM custumer__custumer WHERE custumer_id = $custumer_id" );
		}

		if ( ! $file_name ) {
			if ( $rs['custumer_billing'] == 'citizen' ) {
				$file_name = $rs['custumer_name'] . ' ' . $rs['custumer_surname1'] . ' ' . $rs['custumer_surname2'];
			} else {
				$file_name = $rs['custumer_company'];

			}
			$file_name = preg_replace( '/[^a-zA-Z0-9 _.-]/', '', $file_name ) . '_' . $rs['invoice_number'] . '.pdf';
		}
		else {
			$file_name .= '.pdf';
		}

		if ($save_path){

			$this->pdf->Output( $save_path . $file_name, 'F' );
			return $save_path . $file_name;
		}
		else {
			$this->pdf->Output( $file_name, 'D' );
		}
	}

	public function set_customer( $invoice_id ) {

		$pdf = &$this->pdf;

		if ( $this->rs['invoice_number'] ) {
			$rs = &$this->rs;
		}
		else {
			$custumer_id = $this->rs['custumer_id'];
			$rs = Db::get_row( "SELECT * FROM custumer__custumer WHERE custumer_id = $custumer_id" );
		}


		if ( $rs['custumer_billing'] == 'citizen' ) {
			$company  = $rs['custumer_name'] . ' ' . $rs['custumer_surname1'] . ' ' . $rs['custumer_surname2'];
			$cif_type = 'NIF';
			$cif      = $rs['custumer_vat'];
		} else {
			$company  = $rs['custumer_company'];
			$cif_type = 'CIF';
			$cif      = $rs['custumer_cif'];
		}
		$company = mb_strtoupper( $company, 'UTF-8' );

		$adress = '';
		if ( $rs['custumer_adress'] ) {
			$adress .= $rs['custumer_adress'];
			if ( $rs['custumer_numstreet'] ) {
				$adress .= ', ' . $rs['custumer_numstreet'];
			}

			if ( $rs['custumer_block'] ) {
				$adress .= ' ' . $rs['custumer_block'];
			}
			if ( $rs['custumer_flat'] ) {
				$adress .= ' ' . $rs['custumer_flat'];
				if ( $rs['custumer_door'] ) {
					$adress .= '-' . $rs['custumer_door'];
				}
			}
		}

		$town     = $rs['custumer_town'];
		$province = mb_strtoupper( $rs['custumer_province'], 'UTF-8' );
		$zip      = $rs['custumer_zip'];

		$pdf->SetFont( 'opensansb', '', 11 );
		$html = "
			<table>
				<tr>
					<td>$company</td>
				</tr>
			</table>
		";
		$pdf->writeHTMLCell(
			87, // width
			0, // height
			112, // x
			22, // y
			$html, // html
			0, // border
			1 // ln
		);

		$y = $pdf->getY() + 2;

		$pdf->SetFont( 'opensans-light', '', 11 );
		$html = "
			<table>
				<tr>
					<td>$cif_type: $cif</td>
				</tr>
				<tr>
					<td>$adress</td>
				</tr>
				<tr>
					<td>$zip $town</td>
				</tr>
				<tr>
					<td>$province</td>
				</tr>
			</table>
		";

		$pdf->writeHTMLCell(
			87, // width
			0, // height
			112, // x
			$y, // y
			$html, // html
			0, // border
			1 // ln
		);
	}

	public function set_invoice_date( $invoice_id ) {
		$pdf = &$this->pdf;


		extract( $this->rs );

		// Data i Num
		$pdf->SetFont( 'opensans', '', 13 );

		$invoice_number = $invoice_serial . $invoice_number;
		$bill_date      = format_date_form( $bill_date );
		$bill_due       = format_date_form( $bill_due );


		$style = 'style="font-family:opensansb;font-size:10pt;color:rgb(140,140,140)"';
		$html  = "
			<table cellpadding=\"8\">
				<tr>
					<td width=\"33%\"><span $style>NUM. FACTURA</span>&nbsp;&nbsp;&nbsp;$invoice_number</td>
					<td width=\"33%\"><span $style>DATA</span>&nbsp;&nbsp;&nbsp;$bill_date</td>
					<td width=\"33%\"><span $style>VENCIMENT</span>&nbsp;&nbsp;&nbsp;$bill_due</td>
				</tr>
			</table>
		";
		$pdf->writeHTMLCell(
			180 + 8, // width + padding de la taula
			0, // height
			15 - 4, // x - padding de la taula/2
			68, // y
			$html, // html
			0, // border
			1 // ln
		);


		// Mètode
		$pdf->SetTextColor( 140, 140, 140 );
		$pdf->SetFont( 'opensans-light', '', 10 );
		$style = 'style="color:rgb(0,0,0);font-family:opensansb;"';

		if ( $billing_type == 'wire_transfer' ) {
			$html = "<div>Pagament per transferència bancària al compte: <span $style>ES28 0081 5267 7800 0107 9309</span></div>";
		} else {
			$billing_type = $this->module->caption[ 'c_billing_type_' . $billing_type ];
			$html         = "<div>Pagament per: <span $style>$billing_type</div></span>";
		}
		$pdf->writeHTMLCell(
			180, // width + padding de la taula
			0, // height
			14, // x - padding de la taula/2
			84, // y
			$html, // html
			0, // border
			1 // ln
		);
		$pdf->SetTextColor( 0, 0, 0 );

	}

	public function set_concepts( $invoice_id ) {
		$pdf = &$this->pdf;


		$results = Db::get_rows( "
			SELECT * 
			FROM invoice__invoiceconcept 
			WHERE invoice_id = $invoice_id 
			ORDER BY ordre ASC, concept ASC" );

		$pdf->SetFont( 'opensans', '', 9 );

		$html       = "<table style=\"background-color:rgb(247,247,247)\" cellpadding=\"8\">";
		$style_td   = 'style="background-color:rgb(255,255,255)"';
		$style_text = 'style="font-family:opensansb;font-size:9pt;color:rgb(140,140,140)"';


		$html .= "
			<tr>
				<td $style_td width=\"100mm\"><span $style_text>Concepte</span></td>
				<td $style_td width=\"26mm\" align=\"right\"><span $style_text>Quantitat</span></td>
				<td $style_td width=\"28mm\" align=\"right\"><span $style_text>Preu unitat</span></td>
				<td $style_td width=\"26mm\" align=\"right\"><span $style_text>Total</span></td>
			</tr>
		";

		foreach ( $results as $rs ) {
			extract( $rs );

			$total = $price * $quantity;

			$price = format_currency( $price );
			$total = format_currency( $total );


			$html .= "
				<tr>
					<td>$concept</td>
					<td align=\"right\">$quantity</td>
					<td align=\"right\">$price</td>
					<td align=\"right\">$total</td>
				</tr>
			";
		}
		$html .= "</table>";

		$pdf->writeHTMLCell(
			180, // width
			0, // height
			15 - 1, // x - El menys 1 es algo del padding, pero no se perque 1 enlloc de 2
			97, // y
			$html, // html
			0, // border
			1 // ln
		);


	}

	public function set_prices() {
		$pdf = &$this->pdf;

		extract( $this->rs );

		$total_price_no_vat = format_currency( $total_price_no_vat );
		$total_vat          = format_currency( $total_vat );
		$total_price        = format_currency( $total_price );

		$pdf->SetFont( 'opensansextrabold', '', 11 );

		$html = "<table cellpadding=\"6\">";

		$html .= "
			<tr>
				<td width=\"45mm\"><span style=\"font-family:opensans;font-size:10pt;\">Total base imponible</span></td>
				<td width=\"45mm\" align=\"right\">$total_price_no_vat</td>
			</tr>
			<tr>
				<td width=\"45mm\"><span style=\"font-family:opensans;font-size:10pt;\">IVA 21 %</span></td>
				<td width=\"45mm\" align=\"right\">$total_vat</td>
			</tr>
			<tr>
				<td width=\"45mm\"><span style=\"font-family:opensansb;font-size:11pt;\">TOTAL</span></td>
				<td width=\"45mm\" align=\"right\"><span style=\"font-size:13pt;\">$total_price</span></td>
			</tr>
		</table>
		";

		$y = $pdf->getY();

		$pdf->writeHTMLCell(
			90, // width
			0, // height
			15 + 90, // x
			$y + 10, // y
			$html, // html
			0, // border
			1 // ln
		);

	}

	static function draw_pdf_line( &$pdf, $left, $width ) {

		$pdf->SetDrawColor( 237, 237, 237 );
		$line_y = $pdf->getY() + 8;
		$pdf->Line( $left, $line_y, $left + $width, $line_y, array( 'width' => '0.4' ) );

		$pdf->setY( $pdf->getY() + 20 );
	}


	static public function set_header( &$pdf ) {
		$logo = CLIENT_PATH . '/images/logo_invoice.png';
		$pdf->Image( $logo, 15, 10, 54.02 );


		$pdf->SetFont( 'opensans', '', 9 );

		$html = '
			<table>
				<tr>
					<td height="5" class="font-family:opensansb;font-size:9pt">9Sistema Serveis Informàtics 2005 SL</td>
				</tr>
			</table>
		';

		$pdf->writeHTMLCell(
			80, // width
			0, // height
			15, // x
			37, // y
			$html, // html
			0, // border
			1 // ln
		);


		$pdf->SetFont( 'opensans-light', '', 9 );
		$html = '
			<table>
				<tr>
					<td>CIF: 47184738P</td>
				</tr>
				<tr>
					<td>Juli Garreta 25C - 2º - 2ª </td>
				</tr>
				<tr>
					<td>17002 - Girona - GIRONA</td>
				</tr>
				<tr>
					<td>Tel: 665 731 656</td>
				</tr>
				<tr>
					<td>admin@letnd.com - www.letnd.com</td>
				</tr>
			</table>
		';

		$pdf->writeHTMLCell(
			80, // width
			0, // height
			15, // x
			44, // y
			$html, // html
			0, // border
			1 // ln
		);

	}

	static public function set_footer( &$pdf ) {

		$pdf->SetTextColor( 140, 140, 140 );
		$pdf->SetFont( 'opensans', '', 5 );

		$html = '					
			<div>D\'acord amb el que estableix la Llei Orgànica 15/1999, de 13 de desembre, de protecció de dades de caràcter personal (LOPD), l\'informem que les seves dades estan incorporades en un fitxer del qual és titular LETND SERVEIS INFORMÀTICS amb la finalitat de realitzar la gestió administrativa, comptable i fiscal, així com enviar-li comunicacions comercials sobre els nostres productes i/o serveis.</div>
			
			<div>Així mateix, l\'informem de la possibilitat d\'exercir els drets d\'accés, rectificació, cancel·lació i oposició de les seves dades al domicili fiscal de LETND SERVEIS INFORMÀTICS situat al C / JULI GARRETA 25 - 17002 GIRONA.</div>
			
		';

		$pdf->writeHTMLCell(
			180, // width
			0, // height
			15, // x
			276, // y
			$html, // html
			0, // border
			1 // ln
		);

		$pdf->SetTextColor( 90, 90, 90 );
		$pdf->SetFont( 'opensans', '', 6 );

		$html = '					
			<div>Registro Mercantil de Girona, Tomo 2376, Folio 176, Hoja GI 39842, Inscripción 1</div>
			
		';

		$y = $pdf->getY();

		$pdf->writeHTMLCell(
			180, // width
			0, // height
			15, // x
			$y + 2, // y
			$html, // html
			0, // border
			1 // ln
		);

	}


}

require_once( DOCUMENT_ROOT . 'common/includes/tcpdf6/tcpdf.php' );

class PDF extends TCPDF {

	//Page header
	public function Header() {
		InvoicePdf::set_header( $this );
	}

	// Page footer
	public function Footer() {
		InvoicePdf::set_footer( $this );
	}
}