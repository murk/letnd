<?
/**
 * MuseumoliCategory
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2011
 * @version $Id$
 * @access public
 */
class MuseumoliCategory extends Module{
	function __construct(){
		parent::__construct();
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function get_records()
	{	
		return $this->_execute_module(str_replace('list_','get_',$this->action));
	}
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{
		return $this->_execute_module(str_replace('show_','get_',$this->action));
	}

	function save_rows()
	{
		$this->_execute_module();
	}

	function write_record()
	{
		$this->_execute_module();
	}

	function manage_images()
	{
		$this->_execute_module();
	}
	function _execute_module($action=''){
		$module = Module::load('news','category', $this);
		$action = $action?$action:$this->action;
		return $module->do_action($action);	
	}
}
?>