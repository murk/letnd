<?php
class ProductFamily extends Module{
	var $q='';
	var $form_tabs;
	function __construct(){
		parent::__construct();
	}
	function on_load() {

		$this->form_tabs = [];
		if ($this->config['has_family_calendar']) {
			$this->form_tabs = [
				'0' => [
					'tab_action'  => $this->action == 'show_calendar' ? false : 'show_calendar',
					'tab_caption' => $this->caption['c_edit_calendar']
				],
			];
		}

		if ( !$this->config['has_private_familys'] ) {
			$this->unset_field( 'is_family_private' );
			$this->unset_field( 'pass_code' );

		}

		parent::on_load();
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();

	}
	function get_records()
	{
	    $listing = new ListRecords($this);
		if (isset($_GET['parent_id'])){
			//$listing->condition = "parent_id = '" . $_GET['family_id'] . "'";
			//$listing->set_field('parent_id','list_admin','0');
			$listing->add_options_button ('add_subfamily_button', 'menu_id=20012&action=show_form_new&parent_id='.$_GET['parent_id']);
			$this->set_breadcrumb($_GET['parent_id']);
		}
		else{
			if ($this->process=='search'){
				$q = $this->q = R::escape('q');
				$listing->condition = "(
					family like'%" . $q . "%'
					OR family_description like'%" . $q . "%')";
				$GLOBALS['gl_page']->title = TITLE_SEARCH;
			}
		}

		$listing->add_filter('parent_id');
		$listing->add_button ('add_subfamily', 'action=show_form_new', 'boto2','after','','','parent_id');
		$listing->add_button ('view_subfamilys', '', 'boto2','after','','','parent_id');

		$listing->call('records_walk','parent_id',true);

		$listing->order_by = 'parent_id ASC, ordre ASC, family ASC';

		$list = $listing->list_records();

	    return $this->get_search_content() . $list;
	}
	function records_walk(&$listing, $parent_id){

		$ret = array();
		$family_id = $listing->rs['family_id'];

		$has_subfamilies = Db::get_first("
			SELECT count(*) FROM product__family WHERE parent_id = " . $family_id);

		$listing->buttons['view_subfamilys']['show'] = ($has_subfamilies);

		$level = $this->get_level(false, $listing->rs['parent_id']);

		$listing->buttons['add_subfamily']['show'] = $level!=3;
		//$listing->buttons['add_subfamily']['params'] = "&parent_id=".$parent_id;


		$parent_id = Db::get_first("
			SELECT family FROM product__family_language WHERE family_id = " . $parent_id . " AND language = '".LANGUAGE."'");

		$ret['parent_id'] = $parent_id;
		return $ret;
	}
	function get_level($family_id, $parent_id = false) {
		if ($parent_id===false)
			$parent_id = Db::get_first("
				SELECT parent_id FROM product__family WHERE family_id = " . $family_id);

		if (!$parent_id) return 1;
		else{
			$parent_parent_id = Db::get_first("
				SELECT parent_id FROM product__family WHERE family_id = " . $parent_id);
			if ($parent_parent_id) return 3;
			else return 2;
		}
	}
	function set_breadcrumb($parent_id, $last_listing=array(), $is_form=false, $title = false){

		$rs = Db::get_row(
				"SELECT family, product__family.family_id as family_id, parent_id
					FROM product__family, product__family_language 
					WHERE language = '".LANGUAGE."' 
					AND product__family.family_id = product__family_language.family_id 
					AND product__family.family_id = " . $parent_id . "");

		$family = $rs['family'];

		$page = &$GLOBALS['gl_page'];
		$last_menu = 0;
		// primer breadcrumb si encara hi ha una familia per sobre
		if ($rs['parent_id'] && $rs['parent_id']!='0'){
			$rs2 = Db::get_row(
				"SELECT family, product__family.family_id as family_id, parent_id
					FROM product__family, product__family_language 
					WHERE language = '".LANGUAGE."' 
					AND product__family.family_id = product__family_language.family_id 
					AND product__family.family_id = " . $rs['parent_id'] . "");

			$link = '?menu_id=20012&parent_id='.$rs2['family_id'];
			$page->add_breadcrumb( $rs2['family'], $link);
			$page->last_menu[$GLOBALS['gl_menu_id']][$last_menu] =
				array('name'=>$rs2['family'],'link'=>$link);

			$last_menu = 1;
		}

		if ($is_form){
			$link =   '?menu_id=20012&parent_id='.$rs['family_id'];

		}
		else{
			$link = '';
		}

		if ($family){
			$page->add_breadcrumb($family, $link);
			$page->last_menu[$GLOBALS['gl_menu_id']][$last_menu] =
				array('name'=>$family,'link'=>$link);

			$page->subtitle = $family;
		}


		if ($is_form && $this->action == 'show_form_new')
			$page->add_breadcrumb($title);


		//$page->title = $this->caption['c_subtitle_'.$this->process];
		//Page::add_breadcrumb ($family);



	}

    function get_search_content()
    {
		$tpl = new phemplate(PATH_TEMPLATES);
        $template = 'product/family_search.tpl';
        $tpl->set_file($template);
        $tpl->set_var('menu_id', $GLOBALS['gl_menu_id']);
		$tpl->set_var('q',  htmlspecialchars(R::get('q')));
        $tpl->set_vars($this->caption);

		if ($this->q) {
			$GLOBALS['gl_page']->javascript .= "
				$('table.listRecord').highlight('".addslashes(R::get('q'))."');			
			";
		}

        return $tpl->process();
    }

	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{
	    $show = new ShowForm($this);
		$show->form_tabs = $this->form_tabs;
		$parent_id = false;

		$show->get_values();

		if (isset($_GET['parent_id'])){
			$parent_id = $_GET['parent_id'];
			$this->set_breadcrumb($_GET['parent_id'], $show->last_listing, true, $this->caption['c_add_subfamily_button']);
		}

		elseif (isset($_GET['family_id']) && $this->action=='show_form_new'){
			$parent_id = $_GET['family_id'];
			$this->set_breadcrumb($parent_id, $show->last_listing, true, $this->caption['c_form_title']);
		}

		elseif (isset($_GET['family_id']) ){
			$parent_id = $show->rs['parent_id'];
			$this->set_breadcrumb($show->rs['parent_id'], $show->last_listing, true, $this->caption['c_form_title']);
		}

		$show->rs['parent_id'] =  $this->get_parent_select($show->id, $parent_id);



	    return $show->show_form();
	}


	function get_parent_select($sel_family_id, $parent_id){

		$ret = '<select name="parent_id['.$sel_family_id.']" id="parent_id_'.$sel_family_id.'">
					<option value="null">- ' . $this->caption['c_check_parent_id'] . ' -</option>';


		$families = Db::get_rows("
				SELECT family, product__family.family_id as family_id 
					FROM product__family, product__family_language WHERE parent_id = 0 
					AND language = '".LANGUAGE."'  
					AND product__family.family_id = product__family_language.family_id 
					AND bin=0");

		foreach ($families as $rs){

			$family_id = $rs['family_id'];
			$family = $rs['family'];

			$selected = $parent_id==$family_id?' selected="selected"':'';
			$ret .= '<option value="'.$family_id.'"'.$selected.'>'.$family.'</option>';

			$subfamilies = Db::get_rows("
				SELECT family, product__family.family_id as family_id 
					FROM product__family, product__family_language 
					WHERE parent_id = $family_id AND language = '".LANGUAGE."'  
						AND product__family.family_id = product__family_language.family_id 
						AND bin=0");




			foreach ($subfamilies as $rs){
				$family_id = $rs['family_id'];
				$family = $rs['family'];

				$selected = $parent_id==$family_id?' selected="selected"':'';
				$ret .= '<option value="'.$family_id.'"'.$selected.'>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp'.$family.'</option>';

			}

		}
		$ret .= '</select><input name="old_parent_id['.$sel_family_id.']" type="hidden">';
		return $ret;
	}

	function save_rows()
	{
	    $save_rows = new SaveRows($this);
	    $save_rows->save();
	}

	function write_record()
	{
	    $writerec = new SaveRows($this);
	    $writerec->save();

		if ($this->action == 'save_record') {
			$this->relocate_products($writerec->id);
		}
	}
	function relocate_products($family_id){

		$current_id = $family_id;

		$subfamily_id = $subsubfamily_id = false;

		$parent = Db::get_first(
				"SELECT parent_id
					FROM product__family
					WHERE family_id = " . $family_id . "");

		if ($parent){
			$subfamily_id = $family_id;
			$family_id = $parent;

			$parent = Db::get_first(
				"SELECT parent_id
					FROM product__family
					WHERE family_id = " . $family_id . "");

			if ($parent){
				$subsubfamily_id = $subfamily_id;
				$subfamily_id = $family_id;
				$family_id = $parent;
			}
		}

		Debug::p($family_id, '$family_id');
		Debug::p($subfamily_id, '$subfamily_id');
		Debug::p($subsubfamily_id, '$subsubfamily_id');


		/* TODO-i PER FER URGENT

			
		 * No permetre que una familia acabi al nivell 4
		 *		ej. si es al nivell 2 amb una subfamilia no es pot passar al nivell 3 ja que la sub quedaria al 4
		 *		donar missatge de primer truere les subfamilies
		 * Averiguar f-s-ss abans de guardar i la nova f-s-ss
		 * Buscar productes que tenien vella f-s-ss i posar la nova f-s-ss
		 * Si un porducte estava a f i tenia una s, i ara està a f-s, s'han de canviar tots els productes que eren a s, però també els que eren a f-s o a f-s-ss
		 * 
		 * Poder el més fàcil es començar pel nivell més baix de families que pengen de la current_id i refer l'estructura per cada producte que conté
			

		 */

		/*
		 * $products = "SELECT product_id 
			FROM product__product 
			WHERE family_id = ";
		 */

	}

	function manage_images()
	{
	    $image_manager = new ImageManager($this);
		$image_manager->form_tabs = $this->form_tabs;
	    $image_manager->execute();

		if (isset($_GET['family_id']) && $this->action == 'list_records_images'){
			$parent_id = Db::get_first("SELECT parent_id FROM product__family WHERE family_id = " . $_GET['family_id']);
			$this->set_breadcrumb($parent_id, $image_manager->last_listing, true, $this->caption['c_edit_images']);
		}
	}

	public function show_calendar() {

	    $show = new ShowForm($this);
	    $show->id = R::id('family_id');
		$show->action = 'get_record';
	    $show->set_vars_form();


		$module               = Module::load( 'product', 'familybusy' );
		$module->parent       = 'ProductFamily';

		if ( $GLOBALS['gl_menu_id'] == '2205' ) {
			$data_link  = false;
			$image_link = false;

		} else {
			$data_link  = $show->rs['data_link'];
			$image_link = $show->rs['image_link'];
		}

		$module->tpl->set_vars( [
			'data_link'          => $data_link,
			'image_link'         => $image_link,
			'show_previous_link' => $show->get_var( 'show_previous_link' ),
			'show_next_link'     => $show->get_var( 'show_next_link' ),
			'back_button'     => $show->get_var( 'back_button' ),
		] );

		$module->form_tabs = $this->form_tabs;
		$module->do_action( 'show_family_calendar' );
	}

	public function save_has_calendar() {
		$has_calendar = R::number( 'has_calendar', false, '_POST' );
		$is_calendar_public = R::number( 'is_calendar_public', false, '_POST' );
		$family_id = R::id( 'family_id', false, '_POST' );

		$query = "UPDATE `product__family` SET `has_calendar`='$has_calendar', `is_calendar_public`='$is_calendar_public' WHERE  `family_id`=$family_id;";

		Db::execute( $query );

		$ret['saved'] = true;

		die (json_encode($ret));

	}
}