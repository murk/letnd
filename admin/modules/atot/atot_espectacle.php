<?
/**
 * AtotEspectacle
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 5/11/13
 * @version $Id$
 * @access public
 */
class AtotEspectacle extends Module{

	function __construct(){
		parent::__construct();
		$this->order_by = 'espectacle ASC, entered DESC';
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function get_records()
	{
	    $listing = new ListRecords($this);
		$listing->call('records_walk','content', true);
		
		$listing->add_filter('tipus_concert');
		$listing->add_filter('status');
		$listing->add_filter('in_home');
		$listing->add_filter('destacat');
		$listing->add_filter('only_campanya');
		
		if (isset($_GET['in_home']) && $_GET['in_home']!='null') {
			$listing->set_field('in_home','list_admin','input');
		}
		if (isset($_GET['destacat']) && $_GET['destacat']!='null') {
			$listing->set_field('destacat','list_admin','input');
		}
		if (isset($_GET['only_campanya']) && $_GET['only_campanya']!='null') {
			$listing->set_field('only_campanya','list_admin','input');
		}
		
		$listing->order_by = $this->order_by;
		
	    return $listing->list_records();
	}
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{
	    $show = new ShowForm($this);
		$show->order_by = $this->order_by;
		
		$this->set_field('entered','default_value',now());
		if ($this->action == 'show_form_new') $this->set_field('modified','type','hidden');
		
	    return $show->show_form();
	}
	function records_walk(&$listing, $content=false){			
		
		if ($content!==false){
			$ret['content']=add_dots('content', $content);
		}
		return $ret;
	}

	function save_rows()
	{
	    $save_rows = new SaveRows($this);
	    $save_rows->save();
	}

	function write_record()
	{
	    $writerec = new SaveRows($this);
		$writerec->set_field('modified','override_save_value','now()');		
	    $writerec->save();
	}

	function manage_images()
	{
	    $image_manager = new ImageManager($this);
	    $image_manager->execute();
	}
}
?>