<?php
// menus eina
define('NAUTICA_MENU_EMBARCACION_LIST', 'Llistat i editar embarcacions');
define('NAUTICA_MENU_EMBARCACION', 'Embarcacions');
define('NAUTICA_MENU_EMBARCACION_NEW', 'Nova embarcació');
define('NAUTICA_MENU_EMBARCACION_BIN', 'Paperera de reciclatge');
define('NAUTICA_MENU_TIPUS', 'Tipus');
define('NAUTICA_MENU_TIPUS_LIST', 'Llistat de tipus');
define('NAUTICA_MENU_TIPUS_NEW', 'Nou tipus');
define('NAUTICA_MENU_SHIPYARD', 'Astillers');
define('NAUTICA_MENU_SHIPYARD_LIST', 'Llistat d\'astillers');
define('NAUTICA_MENU_SHIPYARD_NEW', 'Nou astiller');
define('NAUTICA_MENU_FLAG', 'Banderes');
define('NAUTICA_MENU_FLAG_LIST', 'Llistat de banderes');
define('NAUTICA_MENU_FLAG_NEW', 'Nova bandera');

// captions seccio
$gl_caption_embarcacion['c_status'] = 'Estat';
$gl_caption_embarcacion['c_clau_dossier'] = 'Dades Internes';
$gl_caption_embarcacion['c_clau'] = 'Clau';
$gl_caption_embarcacion['c_dossier'] = 'Expedient';
$gl_caption_embarcacion['c_entered'] = 'Entrada';
$gl_caption_embarcacion['c_entered_form'] = 'Data d\'entrada';
$gl_caption_embarcacion['c_prepare'] = 'En preparació';
$gl_caption_embarcacion['c_review'] = 'Revisar';
$gl_caption_embarcacion['c_onsale'] = 'Per vendre';
$gl_caption_embarcacion['c_sold'] = 'Venuda';
$gl_caption_embarcacion['c_archived'] = 'Arxivada';
$gl_caption_embarcacion['c_reserved'] = 'Pending';
$gl_caption_embarcacion['c_tipus_id'] = 'Tipus d\'embarcació';
$gl_caption['c_shipyard'] = 'Astiller';
$gl_caption_embarcacion['c_shipyard_id'] = 'Astiller';
$gl_caption_embarcacion['c_model'] = 'Model';
$gl_caption_embarcacion['c_buildyear'] = 'Any';
$gl_caption_embarcacion['c_buildyear_form'] = 'Any de construcció';
$gl_caption_embarcacion['c_length'] = 'Eslora';
$gl_caption_embarcacion['c_breadth'] = 'Manga';
$gl_caption_embarcacion['c_weight'] = 'Pes';
$gl_caption_embarcacion['c_depth'] = 'Calat';
$gl_caption_embarcacion['c_depth2'] = 'Calat';
$gl_caption_embarcacion['c_engine'] = 'Motor';
$gl_caption_embarcacion['c_fuel'] = 'Carburant';
$gl_caption_embarcacion['c_fuel_gasoline'] = 'Gasolina';
$gl_caption_embarcacion['c_fuel_diesel'] = 'Diesel';
$gl_caption['c_flag'] = 'Bandera';
$gl_caption_embarcacion['c_flag_id'] = 'Bandera';
$gl_caption_embarcacion['c_hour_use'] = 'Hores';
$gl_caption_embarcacion['c_price'] = 'Preu';
$gl_caption_embarcacion['c_price_pounds'] = 'Preu (&)';
$gl_caption_embarcacion['c_changeable'] = 'Accepto embarcació a canvi';

$gl_caption_embarcacion['c_vhf'] = 'VHF';
$gl_caption_embarcacion['c_depthsounder'] = 'Sonda';
$gl_caption_embarcacion['c_speedlog'] = 'Corredora';
$gl_caption_embarcacion['c_radar'] = 'Radar';
$gl_caption_embarcacion['c_gps'] = 'GPS';
$gl_caption_embarcacion['c_plotter'] = 'Plotter';
$gl_caption_embarcacion['c_autopilot'] = 'Pilot automàtic';
$gl_caption_embarcacion['c_trimmtabs'] = 'Flaps Hidràulics';
$gl_caption_embarcacion['c_searchlight'] = 'Far pirata';
$gl_caption_embarcacion['c_electrical_capstan'] = 'Molinet elèctric';
$gl_caption_embarcacion['c_gangway'] = 'Pasarela';
$gl_caption_embarcacion['c_liferaft'] = 'Bot salvavides';
$gl_caption_embarcacion['c_tender'] = 'Embarcació auxiliar';
$gl_caption_embarcacion['c_offshore_connexion'] = 'Conexió 220V';
$gl_caption_embarcacion['c_battery_charger'] = 'Carregador de bateries';
$gl_caption_embarcacion['c_generator'] = 'Generador';
$gl_caption_embarcacion['c_cover'] = 'Lona de fondeig';
$gl_caption_embarcacion['c_camping_cover'] = 'Capota';
$gl_caption_embarcacion['c_bimini_top'] = 'Toldo pel sol';
$gl_caption_embarcacion['c_bowthruster'] = 'Helix de proa';
$gl_caption_embarcacion['c_hot_water'] = 'Aigua Calenta';
$gl_caption_embarcacion['c_davit'] = 'Pescant';
$gl_caption_embarcacion['c_anemometer'] = 'Equip de vent';
$gl_caption_embarcacion['c_mainsail'] = 'Vela Major';
$gl_caption_embarcacion['c_genoa'] = 'Gènova';
$gl_caption_embarcacion['c_mizzen'] = 'Messana';
$gl_caption_embarcacion['c_jib'] = 'Floc';
$gl_caption_embarcacion['c_stormsail'] = 'Tormentín';
$gl_caption_embarcacion['c_forsail2'] = 'Trinqueta';
$gl_caption_embarcacion['c_spinaker'] = 'Spinaker';
$gl_caption_embarcacion['c_spi_boom'] = 'Tangó';
$gl_caption_embarcacion['c_roller_genoa'] = 'Enrollador de Génova';

$gl_caption_embarcacion['c_music_equipment'] = 'Equip de música';
$gl_caption_embarcacion['c_television'] = 'Televisió';
$gl_caption_embarcacion['c_attached_engine'] = 'Motor Annexe';
$gl_caption_embarcacion['c_fridge'] = 'Nevera elèctrica';
$gl_caption_embarcacion['c_heating'] = 'Calefacció';
$gl_caption_embarcacion['c_air_conditioning'] = 'Aire condicionat';
$gl_caption_embarcacion['c_teka'] = 'Cuberta de Teka';
$gl_caption_embarcacion['c_wc'] = 'WC';
$gl_caption_embarcacion['c_black_water'] = 'Dipòsit d\'Aigües Negres';
$gl_caption_embarcacion['c_roller_mainsail'] = 'Enrollador de Major';




$gl_caption_embarcacion['c_equipament'] = 'Equipament';

$gl_caption_embarcacion['c_data'] = 'Dades de l\'embarcació';
$gl_caption_embarcacion['c_ref'] = 'Referència';
$gl_caption_embarcacion['c_general_data'] = 'Dades generals';
$gl_caption_embarcacion['c_measures_weight'] = 'Mides - Pes';
$gl_caption_embarcacion['c_measures'] = 'Mides';
$gl_caption_embarcacion['c_weight_units'] = 'kg.';
$gl_caption_embarcacion['c_characteristics_electronic'] = 'Electrònica';
$gl_caption_embarcacion['c_characteristics_electronic_caps'] = 'ELECTRÒNICA';
$gl_caption_embarcacion['c_characteristics_extra'] = 'Extres';
$gl_caption_embarcacion['c_characteristics_sail'] = 'Vela';
$gl_caption_embarcacion['c_characteristics'] = 'Característiques';

$gl_caption_embarcacion['c_filter_shipyard_id'] = 'Tots els astillers';
$gl_caption_embarcacion['c_filter_tipus_id'] = 'Tots els tipus';
$gl_caption_embarcacion['c_filter_status'] = 'Tots els estats';
$gl_caption_embarcacion['c_filter_length'] = 'Tots les eslores';
$gl_caption_embarcacion['c_filter_price'] = 'Tots els preus';
$gl_caption_embarcacion['c_print_pdf'] = 'Imprimir pdf';

$gl_caption_tipus['c_ordre']='Ordre';
$gl_caption_tipus['c_tipus']='Tipus';

$gl_caption['c_embarcacion_file_name'] = $GLOBALS['gl_caption']['c_file_name'];
$gl_caption['c_embarcacion_old_file_name'] = $GLOBALS['gl_caption']['c_old_file_name'];


$gl_caption_embarcacion['c_engine_type'] = 'Tipus motor';
$gl_caption_embarcacion['c_engine_type_fora'] = 'Foraborda';
$gl_caption_embarcacion['c_engine_type_intra'] = 'Intraborda';
$gl_caption_embarcacion['c_fuel_tank'] = 'Dipòsit combustible';
$gl_caption_embarcacion['c_water_tank'] = 'Dipòsit aigua';
$gl_caption_embarcacion['c_compass'] = 'Compàs';
$gl_caption_embarcacion['c_bath_stair'] = 'Escala de bany';
$gl_caption_embarcacion['c_trim'] = 'Trim';
$gl_caption_embarcacion['c_kitchen'] = 'Cuina';
$gl_caption_embarcacion['c_microwave'] = 'Microones';
$gl_caption_embarcacion['c_shower'] = 'Dutxa';
?>