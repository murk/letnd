<table border="0" cellspacing="0" cellpadding="0" width="100%" class="subListRecord" <?= $is_reserva_form?"id='reservaitem_list_row_list_table'":'' ?>>
	<thead>
	<tr>
		<td class="listCaption">
			<?= $c_ref ?>
		</td>
		<td class="listCaption">
			<?= $c_product_title ?>
		</td>
		<td class="listCaption">
			<?= $c_lassdive_product_date ?>
		</td>
		<td class="listCaption">
			<?= $c_reservaitem_date ?>
		</td>
		<td class="listCaption">
			<?= $c_start_time ?>
		</td>
		<td class="listCaption">
			<?= $c_end_time ?>
		</td>
		<td class="listCaption">
			<?= $c_centre_id ?>
		</td>
		<td class="listCaption">
			<?= $c_user_id ?>
		</td>
		<td class="listCaption">
			<?= $c_is_done ?>
		</td>
		<td class="listCaption align-right">
			<?= $c_product_base ?>
		</td>
		<td class="listCaption"></td>
	</tr>
	</thead>
	<? foreach ( $loop as $l ): extract( $l ) ?>

		<tr class="listItem<?= $odd_even ?> <?= $tr_class ?>" onmousedown="change_bg_color(this, 'Down', '<?= $reservaitem_id ?>')" id="reservaitem_list_row_<?= $reservaitem_id ?>">

			<? include ('reserva_list_common.tpl') ?>

		</tr>
	<? endforeach //$loop?>
</table>
<? if ( $is_reserva_form ): ?>
	<input name="<?= $inputs_prefix ?>[delete_record_ids]" type="hidden" value="">
	<input id="inputs_prefix" type="hidden" value="<?= $inputs_prefix ?>">
<? endif // $is_reserva_form ?>