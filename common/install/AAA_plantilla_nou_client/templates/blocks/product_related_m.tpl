<section class="product-related product list-records">

	<h6><?= $title ?></h6>

	<div id="product-related-slider">
		<? foreach($loop as $l): extract( $l ) ?>

			<? if($conta % 3 == 0): ?>
				<div class="row<? if($loop_count - $conta <= 3) echo " last" ?><? if($conta == 0) echo " first" ?>">
			<? endif //conta?>

			<article class="item">

				<a href="<?= $show_record_link ?>" title="<?= htmlspecialchars( $product_title ) ?>">
					<figure>
						<? if($image_name): ?>
							<img <?= get_image_attrs( $image_src_thumb, $image_alt?$image_alt:$product_title, 150 ) // 300x200 ?> />
						<? endif //images?>
					</figure>
				</a>

				<? if($can_buy): ?>
					<div class="cart-holder">
						<a class="view" href="<?= $show_record_link ?>" title="<?= htmlspecialchars( $product_title ) ?>"></a>

						<a onclick="javascript:cart_add_item(<?= $id ?>,1,1,event);" class="cart-button" title="<?= htmlspecialchars( $c_add_to_cart ) ?>"></a>
					</div>
				<? endif //$can_buy ?>


				<div class="text-holder">
					<? if($ref): ?>
						<a href="<?= $show_record_link ?>" title="<?= htmlspecialchars( $ref ) ?>"><em><?= $c_ref ?>
								<?= $ref ?>
							</em></a>
					<? endif ?>

					<? if($product_title): ?>
						<a href="<?= $show_record_link ?>" title="<?= htmlspecialchars( $product_title ) ?>">
							<h3>
								<?= $product_title ?>
							</h3></a>
					<? endif ?>

					<? if($can_buy): ?>
						<a href="<?= $show_record_link ?>" title="<?= htmlspecialchars( $product_title ) ?>">
							<div class="price">
								<? if($old_price): ?>

									<span class="old-price"><?= $old_price ?> <?= CURRENCY_NAME ?></span>

								<? endif //old_price?>

								<?= $pvp ?> <?= CURRENCY_NAME ?>

							</div>
						</a>

					<? else: //?>
						<div class="price"></div>
					<? endif ?>
				</div>

			</article>
			<? if($conta % 3 == 2): ?>
				</div>
			<? endif //conta?>

		<? endforeach //$loop?>
		<? if($conta % 3 != 2) echo "</di" . "v>" ?>
	</div>

	<? if($more): ?>
		<div class="center"><a title="<?= htmlspecialchars( $more ) ?>" href="<?= $more_link ?>" class="more-info"><?= $more ?></a>
		</div>
	<? endif //$more?>


</section>