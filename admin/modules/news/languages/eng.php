<?php
// menus eina
if (!defined('NEWS_MENU_NEW')){
	define('NEWS_MENU_NEW', 'Noticias');
	define('NEWS_MENU_NEW_NEW', 'Entrar nueva noticia');
	define('NEWS_MENU_NEW_LIST', 'Listar i editar noticias');
	define('NEWS_MENU_NEW_BIN', 'Papelera de reciclaje');
	define('NEWS_MENU_CATEGORY', 'Categorías');
	define('NEWS_MENU_CATEGORY_NEW', 'Entrar nueva categoría');
	define('NEWS_MENU_CATEGORY_LIST', 'Listar categorías');
}

$gl_messages_new['no_category']='Para poder insertar noticias tiene que haber almenos una categoría';

// captions seccio
$gl_caption_new['c_prepare'] = 'En preparación';
$gl_caption_new['c_review'] = 'Para revisar';
$gl_caption_new['c_public'] = 'Pública';
$gl_caption_new['c_archived'] = 'Archivada';
$gl_caption_new['c_new'] = 'New';
$gl_caption_new['c_title'] = 'Title';
$gl_caption_new['c_subtitle'] = 'Subtitle';
$gl_caption_new['c_content'] = 'Content';
$gl_caption_new['c_entered'] = 'Date';
$gl_caption_new['c_expired'] = 'Fecha caducidad';
$gl_caption_new['c_modified'] = 'Modified date';
$gl_caption_new['c_status'] = 'Estado';
$gl_caption_new['c_category_id'] = 'Categoría';
$gl_caption_new['c_filter_category_id'] = 'Todas las categorías';
$gl_caption_new['c_filter_status'] = 'Todas los estados';
$gl_caption_new['c_ordre'] = 'Order';

$gl_caption_category['c_category'] = 'Category';
$gl_caption_category['c_new_button'] = 'View news';
$gl_caption_category['c_ordre'] = 'Order';
$gl_caption_image['c_name'] = 'Name';
$gl_caption_new['c_url1_name']='URL Name(1)';
$gl_caption_new['c_url1']='URL(1)';
$gl_caption_new['c_url2_name']='URL Name(2)';
$gl_caption_new['c_url2']='URL(2)';
$gl_caption_new['c_url3_name']='URL Name(3)';
$gl_caption_new['c_url3']='URL(3)';
$gl_caption_new['c_videoframe']='Video (youtube, metacafe...)';
$gl_caption_new['c_file']='Files';
$gl_caption_new['c_content_list']='Short description on listing';

$gl_caption['c_new_file_name'] = $GLOBALS['gl_caption']['c_file_name'];
$gl_caption['c_new_old_file_name'] = $GLOBALS['gl_caption']['c_old_file_name'];
$gl_caption_new['c_form_level1_title_page_title'] = $GLOBALS['gl_caption']['c_seo'];

$gl_caption['c_listtpl_id'] = 'Listing template';
$gl_caption['c_formtpl_id'] = 'Template';

$gl_caption['c_next_new'] = "Next new";
$gl_caption['c_previous_new'] = "Previous new";
?>
