<?

/* 27-10-16

	PRODUCT - Afegir comentari en la comanda


 */

if ( is_table( 'product__order' ) && ! is_field( 'product__order', 'customer_comment' ) ) {

	$update_sql[] = "ALTER TABLE `product__order`
	ADD COLUMN `customer_comment` TEXT NOT NULL DEFAULT '' AFTER `invoice_refund_date`;
";

} else {
	if ( is_table( 'product__order' ) ) {
		trigger_error( 'Ja existeixen el camp customer_comment' );
	}
}


?>