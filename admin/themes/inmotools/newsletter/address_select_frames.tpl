<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<?=load_admin_css()?>
	<link href="/common/jscripts/jquery-ui-1.8.20/css/ui-lightness/jquery-ui-1.8.20.custom.css" rel="stylesheet" type="text/css"/>
	<script type="text/javascript" src="/common/jscripts/jquery/jquery-1.7.2.min.js"></script>


	<? if ( ! DEBUG ): ?>
		<script type="text/javascript" src="/common/jscripts/jquery.onerror.js?v=<?= $version ?>"></script>
	<? endif //DEBUG?>
	<script type="text/javascript" src="/common/jscripts/dropzone/dropzone.js"></script>
	<script type="text/javascript" src="/common/jscripts/jquery-ui-1.8.20/js/jquery-ui-1.8.20.custom.min.js?v=<?= $version ?>"></script>
	<script type="text/javascript" src="/common/jscripts/jquery-ui-1.8.20/languages/jquery.ui.datepicker-<?= $language_code ?>.js?v=<?= $version ?>"></script>
	<script type="text/javascript" src="/common/jscripts/jquery.highlight-4.js?v=<?= $version ?>"></script>
	<script type="text/javascript" src="/common/jscripts/jquery.dump.js?v=<?= $version ?>"></script>
	<script type="text/javascript" src="/common/jscripts/jquery.mousewheel.js?v=<?= $version ?>"></script>
	<script type="text/javascript" src="/common/jscripts/jquery.timepicker.js?v=<?= $version ?>"></script>
	<script type="text/javascript" src="/common/jscripts/jquery.cookie.js?v=<?= $version ?>"></script>
	<script type="text/javascript" src="/common/jscripts/jquery.blockUI.js?v=<?= $version ?>"></script>
	<script type="text/javascript" src="/common/jscripts/jquery.dropshadow.js?v=<?= $version ?>"></script>
	<script type='text/javascript' src='/common/jscripts/jquery.autocomplete.min.js'></script>
	<script language="JavaScript" src="/common/jscripts/multifile/jquery.MultiFile.js?v=<?= $version ?>"></script>
	<script type="text/javascript" src="/admin/jscripts/main.js?v=<?= $version ?>"></script>
	<script type="text/javascript" src="/admin/languages/<?= $language ?>.js?v=<?= $version ?>"></script>

	<? /* Validacions  */ ?>
	<script type="text/javascript" src="/common/jscripts/jquery-validation/jquery.validate.js"></script>
	<script type="text/javascript" src="/common/jscripts/jquery-validation/localization/messages_<?= $language_code ?>.min.js"></script>
	<script type="text/javascript" src="/common/jscripts/jquery-validation/jquery-validate.bootstrap-tooltip.min.js"></script>
	<script type="text/javascript" src="/common/jscripts/bootstrap-js/tooltip.js"></script>
	<script type="text/javascript" src="/common/jscripts/jquery.inputmask/jquery.inputmask.bundle.min.js"></script>
	<script type="text/javascript" src="/common/jscripts/datejs/date.min.js"></script>
	<script type="text/javascript" src="/common/jscripts/datejs/date-<?= $language_code ?>.min.js"></script>
	<? /* Fi validacions */ ?>

	<? /* CUSTOM PER SELECT FRAMES */ ?>

	<script language="JavaScript" src="/admin/jscripts/assign.js?v=<?= $version ?>"></script>
	<script language="JavaScript" src="/admin/modules/newsletter/jscripts/mail.js?v=<?= $version ?>"></script>
	<SCRIPT LANGUAGE="JavaScript" TYPE="text/javascript">
		<!--
		var addreses_selected = new Array();
		//-->
	</SCRIPT>
</head>
<body class="select-frame">
<div class="search">
	<form action="/admin/?action=list_records_search_select_addresses&menu_id=11" method="post" name="listAddresses" target="listAddresses">
	<table border="0" cellpadding="0" cellspacing="0" class="search_select_frame">   
		<tr> 
		  <td><?=$c_search?></td>
		  <td><input type="text" name="search" class="search">
		  </td><td><input type="submit" name="Submit" value="ok" class="boto-search"></td>
		  <td><strong>
		  &nbsp;&nbsp; <?=$c_groups?></strong><?=$group_id?></td>
		</tr>  
	</table></form>
</div>
<table width="100%" height="1" border="0" cellpadding="0" cellspacing="0" bgcolor="#999999">
  <tr>
    <td></td>
  </tr>
</table>
<table border="0" cellspacing="0" cellpadding="0" height="448">
  <tr>
    <td valign="top" width="710"><iframe name="listAddresses" id="listAddresses" scrolling="yes" marginwidth="0" marginheight="0" frameborder="0" vspace="0" hspace="0" style="overflow:visible; width:710px; height:448px;"  src="/admin/?action=list_records_select_list&menu_id=11"></iframe></td>
    <td width="240" class="assign_right" valign="top">
		<h5><?=$c_selected?></h5>
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		  <tr>
			<td id="write_cell"></td>
		  </tr>
		</table>
<SCRIPT LANGUAGE="JavaScript" TYPE="text/javascript">
<!--
populate_table();
//-->
</SCRIPT></td>
  </tr>
</table>
<form action="" method="post" name="theForm" target="save_frame"><table width="100%" height="1" border="0" cellpadding="0" cellspacing="0" bgcolor="#999999">
  <tr>
    <td height="30" align="center">
      <div class="centerFloat1"><div class="centerFloat2">
	  <input class="btn_save_select_frame" type="button" name="Submit" value="<?=$c_ok?>" onClick="pass_selected_addresses()">
	  <input class="btn_select_frame" type="button" name="Submit" value="<?=$c_cancel?>" onClick="top.hidePopWin(false);"></div></div>
      <input name="selected_addresses_ids" type="hidden" value="">
      <input name="selected_addresses_names" type="hidden" value="">
    </td>
  </tr>
</table></form>
</body>
</html>