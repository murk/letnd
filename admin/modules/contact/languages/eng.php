<?php
// menus eina
define('CONTACT_MENU_CONTACT','Contact us');
define('CONTACT_MENU_LIST','Contact e-mails');

// captions seccio
$gl_caption_contact ['c_company'] = 'Company';
$gl_caption_contact ['c_name'] = 'Name';
$gl_caption_contact ['c_surnames'] = 'Surname(s)';
$gl_caption_contact ['c_mail'] = 'E-mail';
$gl_caption_contact ['c_to_address'] = 'To';
$gl_caption_contact ['c_phone'] = 'Telephone';
$gl_caption_contact ['c_matter'] = 'Subject';
$gl_caption_contact ['c_comment'] = 'Comments';
$gl_caption_contact ['c_accept_info'] = 'Allow send information';
$gl_caption_contact_list ['c_accept_info'] = 'Allow info';
$gl_caption ['c_sendmail'] = 'Send e-mail';
$gl_messages ['added'] = 'Form successfully sent.';
$gl_caption ['c_sendok'] = $gl_messages ['added'];
$gl_messages ['not_saved '] = 'It has not been possible to send the e-mail.';

$gl_caption_contact['c_nif']='ID/VAT number.';
$gl_caption_contact['c_company']='Company';

$gl_caption_contact['c_contact_language']='Contact page language';
// NO TRADUIT

$gl_caption_contact['c_entered']= 'Sent.';

$gl_caption_contact['c_export_button']= 'Exportar';

?>