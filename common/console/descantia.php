<?
/*
 *
Configuració MsDos
-----------------------
C:\server\aliases  -> hi ha tots els .bat per cridar directament cada comando com un executable

per bash

alias letnd="php letnd"
alias client="php letnd client"
alias fotocasa="php letnd fotocasa"
alias vreasy="php letnd vreasy"
alias

Configurar per vagrant
-----------------------
chmod +x letnd
sudo ln -s /letnd/letnd /usr/local/bin/letnd

Accions
--------

	client finquesgarrotxa regen
	client finquesgarrotxa config inmo admin name val
		Si nomes es posa name, es fa el get, si hi ha $val el set

	letnd new update-file <- per cear un nou arxiu de config i obrir-lo directament a phpstorm

	fotocasa finquesgarrotxa api-key
	fotocasa finquesgarrotxa api-key $5saraasrfa98as44l!dee$jkae
	fotocasa finquesgarrotxa propertys

TODO-i

	http://getopt-php.github.io/getopt-php/syntax.html -> per subcommands
	https://github.com/c9s/CLIFramework
	https://github.com/RobSis/zsh-completion-generator

	client finquesgarrotxa download
	client finquesgarrotxa download-all
	client finquesgarrotxa upload
	client finquesgarrotxa upload-all
	client finquesgarrotxa debug




 */


require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/../cli_init.php';
require __DIR__ . '/../console/util.php';
require __DIR__ . '/../console/exporta.php';
require __DIR__ . '/../console/fotocasa.php';
require __DIR__ . '/../console/habitaclia.php';
require __DIR__ . '/../console/vreasy.php';
require __DIR__ . '/../console/client.php';
require __DIR__ . '/../console/create.php';
require __DIR__ . '/../console/test.php';

include_once( DOCUMENT_ROOT . 'common/includes/rest/Unirest.php' );

use Symfony\Component\Console\Application;
use Symfony\Component\Console\Output\ConsoleOutput;

class Letnd {

	public function __construct() {

		// Create the Application
		$application = new Application( '<info>Letnd</info> versió <comment>1.0</comment> 16/3/2018' );

		$input = new ConsoleOutput();
		// $this->print_title($input);

		// Register all Commands
		$application->add(new Console\Command\Exporta);
		$application->add(new Console\Command\Fotocasa);
		$application->add(new Console\Command\Habitaclia);
		$application->add(new Console\Command\Vreasy);
		$application->add(new Console\Command\Client);
		$application->add(new Console\Command\Create);
		$application->add(new Console\Command\Test);
		$application->setAutoExit( false );

		// Run it
		$application->run();

		// $this->print_end($input);


		/*$args = $_SERVER['argv'];
		$args = array_slice( $args, 1 ); // trec el "cmd"

		if ( ! $args ) {
			die( "No has posat cap comando" );
		}

		// El primer argument crida un comando, i la resta són variables. Poso underscore ja que no puc fer una funció que es digui "list" per exemple, es una paraula reservada pel php
		$cmd  = '_' . $args ['0'];
		$args = array_slice( $args, 1 );

		if ( method_exists( $this, $cmd ) ) {
			$this->$cmd( $args );
		}*/

	}

	/*

	/*
	private function _list( $args ) {


				$list = [
					'list' => "Mostra ajuda de totes les variables"
				];

				$out = '';

				foreach ( $list as $k => $v ) {
					$out .= "<td style='color:blue'>$k</td><td>$v</td>";
				}

				$out = "<table><tr>$out</tr></table>";

				echo $out;
	}
	*/

	private function print_title($input){


		$input->writeln ( <<< EOT
<fg=yellow> ___  ___ ___  ___   _   _  _ _____ ___   _   
|   \| __/ __|/ __| /_\ | \| |_   _|_ _| /_\  
| |) | _|\__ \ (__ / _ \| .` | | |  | | / _ \ 
|___/|___|___/\___/_/ \_\_|\_| |_| |___/_/ \_\     
</>
EOT
		);

	}

	private function print_end($input){
		$input->writeln ( <<< EOT
<fg=yellow>---</>
EOT
);

	}

}