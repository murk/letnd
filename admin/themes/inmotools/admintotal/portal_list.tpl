<script src="/common/jscripts/codemirror/lib/codemirror.js"></script>
<script src="/common/jscripts/codemirror/mode/xml/xml.js"></script>
<link rel="stylesheet" href="/common/jscripts/codemirror/lib/codemirror.css">
<link rel="stylesheet" href="/common/jscripts/codemirror/theme/paraiso-dark.css">

<link href="/admin/themes/inmotools/styles/utility.css" rel="stylesheet"
      type="text/css">

<style>
	textarea{
		display: none;
		height: auto;
	}
	.CodeMirror {
		border: none;
		height: auto;
		line-height: 1.6;
		font-size: 15px;
	}
</style>

<script language="JavaScript">

	$(function () {
		CodeMirror.fromTextArea(document.getElementById("xml"), {
			lineNumbers: true,
			mode: "xml",
			indentUnit: 8,
			indentWithTabs: true,
			theme: "paraiso-dark",
			matchBrackets: true,
			viewportMargin: Infinity,
            readOnly: true
		});
	});

</script>


<table width="100%">
	<tr>
		<td class="formCaptionTitle">
			<? if ( $action == 'list_logs' ): ?>
				Logs
			<? else: //   ?>
				Exportacions a portals
			<? endif //$ ?>
		</td>
	</tr>
</table>
<br/>
<? /* <div id='opcions_holder'>
 	<table width="100%" border="0" cellspacing="0" cellpadding="0" id="opcions">
 		<tr>
 			<td class="opcionsTitol"><strong>Opcions</strong></td>
 		</tr>
 		<tr>
 			<td class="opcions">&nbsp;<? foreach ( $languages as $lang ): ?><?= $lang ?>
 					<input name="checkbox" type="checkbox" id="checkbox" checked="checked" onclick="hide('<?= $lang ?>')"/>
 				<? endforeach //$s?>
 				&nbsp;&nbsp;&nbsp;selects
 				<input name="checkbox" type="checkbox" id="checkbox" checked="checked" onclick="hide('resaltatSmall')"/>&nbsp;&nbsp;&nbsp;nums
 				<input name="checkbox" type="checkbox" id="checkbox" checked="checked" onclick="hide('nums')"/></td>
 		</tr>
 	</table>
 </div> */ ?>
<? if ( $action == 'export' ): ?>
	<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
		<tr>
			<td class="listDetails">
				<table width="100%" cellpadding="0" cellspacing="0">
					<tr>
						<td class="listItem"><h6>Apicat - Ghestia</h6></td>
						<td class="listItem">
							<a target="export_frame" href="/admin/?menu_id=1082&action=export_apicat" class="boto1">Exportar</a>
						</td>
					</tr>
					<tr>
						<td class="listItem"><h6>Apicat - Ghestia</h6></td>
						<td class="listItem">
							<a target="export_frame" href="/admin/?menu_id=1082&action=import_apicat" class="boto1">Importar</a>
						</td>
					</tr>
					<tr>
						<td class="listItem"><h6>Ceigrup ( per web nomes funciona per el mateix client, ja que peta el open_basedir )</h6></td>
						<td class="listItem">
							<a target="export_frame" href="/admin/?menu_id=1082&action=export_ceigrup" class="boto1">Exportar</a>
						</td>
					</tr>
					<tr>
						<td class="listItem"><h6>Fotocasa</h6></td>
						<td class="listItem">
							<a target="export_frame" href="/admin/?menu_id=1082&action=export_fotocasa" class="boto1">Exportar</a>
						</td>
					</tr>
					<tr>
						<td class="listItem"><h6>Habitaclia</h6></td>
						<td class="listItem">
							<a target="export_frame" href="/admin/?menu_id=1082&action=export_habitaclia" class="boto1">Exportar</a>
						</td>
					</tr>
					<tr>
						<td class="listItem"><h6>Mls</h6></td>
						<td class="listItem">
							<a target="export_frame" href="/admin/?menu_id=1082&action=export_mls" class="boto1">Forçar MLS</a>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<div id="report">
		<iframe name="export_frame" id="export_frame" scrolling="yes" marginwidth="0" marginheight="0" frameborder="0"
		        style="overflow:visible; width:100%; height: 700px;"></iframe>
	</div>

<? endif //$export ?>

<? if ( $logs ): ?>

	<div id="other_fields">
		<table width="100%" cellspacing="0" cellpadding="4" class="formstbl">
			<tbody>
			<? foreach ( $logs as $log ): ?>
			<tr>
				<td width="150" valign="top" class="formsCaption">
					<h2><a href="/admin/?menu_id=1083&log=<?= $log ?>"><?= $log ?></a></h2>
				</td>
				<? endforeach //$logs?>
			</tr>

			</tbody>
		</table>
	</div>
<? endif // $logs ?>

<? if ( $client_logs ): ?>
	<table width="100%">
		<tr>
			<td class="formCaptionTitle">
				Logs clients
			</td>
		</tr>
	</table>
	<br/>

	<div id="other_fields">
		<table width="100%" cellspacing="0" cellpadding="4" class="formstbl">
			<tbody>
			<? foreach ( $client_logs as $log ): ?>
			<tr>
				<td width="150" valign="top" class="formsCaption">
					<h2><a href="/admin/?menu_id=1083&log=clients/<?= $log ?>"><?= $log ?></a></h2>
				</td>
				<? endforeach //$client_logs?>
			</tr>

			</tbody>
		</table>
	</div>
<? endif // $client_logs ?>

<? if ( $xmls ): ?>
	<table width="100%">
		<tr>
			<td class="formCaptionTitle">
				Xmls
			</td>
		</tr>
	</table>
	<br/>

	<div id="other_fields">
		<table width="100%" cellspacing="0" cellpadding="4" class="formstbl">
			<tbody>
			<? foreach ( $xmls as $x ): ?>
			<tr>
				<td width="150" valign="top" class="formsCaption">
					<h2><a href="/admin/?menu_id=1083&xml=<?= $x ?>"><?= $x ?></a></h2>
				</td>
				<? endforeach //$xmls?>
			</tr>

			</tbody>
		</table>
	</div>
<? endif // $xmls ?>

<? if ( $report ): ?>
	<? if ( $log_file ): ?>
		<table width="100%" cellspacing="0" cellpadding="4">
			<tbody>
			<tr>
				<td class="formCaptionTitle"><strong>
						<?= $log_file ?></strong></td>
			</tr>
			</tbody>
		</table>
	<? endif // $log ?>
	<div id="report">
		<?= $report ?>
	</div>
<? endif // $report ?>


<? if ( $xml ): ?>
	<? if ( $xml_file ): ?>
		<table width="100%" cellspacing="0" cellpadding="4">
			<tbody>
			<tr>
				<td class="formCaptionTitle"><strong>
						<?= $xml_file ?></strong></td>
			</tr>
			</tbody>
		</table>
	<? endif // $xml ?>
	<textarea id="xml">
		<?= $xml ?>
	</textarea>
<? endif // $report ?>
