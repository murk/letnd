<?
/**
 * LestanyApartat
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2011
 * @version $Id$
 * @access public
 */
class LestanyApartat extends Module{
	var $form_tabs;

	function __construct(){
		parent::__construct();
	}

	function on_load(){
		$this->form_tabs = array('0' => array('tab_action'=>'show_form_map','tab_caption'=>$this->caption['c_edit_map']));

		parent::on_load();
	}

	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function get_records()
	{
	    $listing = new ListRecords($this);

		$listing->add_button ('edit_map', 'action=show_form_map','boto1','after');

		$listing->has_bin = false;
		$listing->set_options( 0, 0, 1, 0, 0, 0, 1, 1, 1, 0 );
	    return $listing->list_records();
	}
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{
	    $show = new ShowForm($this);
		$show->form_tabs = $this->form_tabs;

		$show->has_bin = false;
	    return $show->show_form();
	}

	function save_rows()
	{
	    $save_rows = new SaveRows($this);
	    $save_rows->save();
	}

	function write_record()
	{
	    $writerec = new SaveRows($this);
	    $writerec->save();
	}

	function manage_images()
	{
	    $image_manager = new ImageManager($this);
	    $image_manager->execute();
	}





/*
 * MAPES
 */
	function show_form_map (){ //esenyo el napa
		global $gl_news;

		$rs = Db::get_row('SELECT * FROM lestany__apartat WHERE apartat_id = ' . $_GET['apartat_id']);
		$rs['title'] = Db::get_first('SELECT apartat FROM lestany__apartat_language  WHERE apartat_id = ' . $_GET['apartat_id'] . ' AND language = LANGUAGE');
		$rs['address'] = "C/ Doctor Vilardell, 1, 08148 L’Estany";
		$rs['is_default_point'] = ($rs['latitude']!=0)?0:1;
		$rs['latitude1']= substr($rs['latitude'],0,strpos($rs['latitude'],';'));
		$rs['longitude1']=substr($rs['longitude'],0,strpos($rs['longitude'],';'));
		$rs['center_latitude']=$rs['center_latitude']!=0?$rs['center_latitude']:GOOGLE_CENTER_LATITUDE;
		$rs['center_longitude']=$rs['center_longitude']!=0?$rs['center_longitude']:GOOGLE_CENTER_LONGITUDE;
		$rs['zoom']=$rs['zoom']?$rs['zoom']:GOOGLE_ZOOM;
		$rs['menu_id'] =  $_GET['menu_id'];
		$rs['id'] =  $rs[$this->id_field];
		$rs['id_field'] =  $this->id_field;
		$this->set_vars($rs);//passo els resultats del array
		$this->set_file('lestany/apartat_map.tpl'); //crido el arxiu
		$this->set_vars($this->caption); //passo tots els captions
		$gl_news = $this->caption['c_click_map'];
		$content = $this->process();//processo
		$GLOBALS['gl_content'] = $content;
	}
	function save_record_map() {
		global $gl_page;
		extract( $_POST );
		//Debug::p( $_POST );

		if ( $latitude && $longitude ) {

			$query = "UPDATE `lestany__apartat` SET
				`latitude` = '$latitude',
				`longitude` = '$longitude',
				`center_latitude` = '$center_latitude',
				`center_longitude` = '$center_longitude',
				`zoom` = '$zoom'
				WHERE `apartat_id` =$apartat_id
				LIMIT 1";
			//Debug::p( $query );
			Db::execute ($query) ;
			$ret ['message'] = $this->messages['point_saved'];
		}
		else {
			$ret ['message'] = $this->messages['point_not_saved'];
		}

		Debug::p_all();
		die (json_encode($ret));

	}
	function save_rows_map_points(){
		global $gl_page;
		$apartat_id = $_GET['apartat_id'];
		$points = trim($_POST['points']);
		$points = explode(';',$points);

		$latitudes = array();
		$longitudes = array();
		$error = false;

		foreach ($points as $point){
			if ($point){
				$p = explode(',',trim($point));

				$la =trim($p[0]);
				$lo = trim($p[1]);

				if (is_numeric($la) && is_numeric($lo)
				    && $la!='' && $lo!=''
				    && count($p)==2) {
					$latitudes[] =$la;
					$longitudes[] = $lo;
				}
				else{
					$error=true;
					break;
				}
			}
		}
		if ($error){
			$this->map_points_error();
		}

		$center_latitude = $latitudes[0]; // centro en el primer punt
		$center_longitude = $longitudes[0]; // centro en el primer punt

		$latitudes = implode (';',$latitudes);
		$longitudes = implode (';',$longitudes);

		debug::p($latitudes);
		debug::p($longitudes);

		if ($latitudes && $longitudes){
			Db::execute ("UPDATE `lestany__apartat` SET
				`latitude` = '$latitudes',
				`longitude` = '$longitudes',
				`center_latitude` = '$center_latitude',
				`center_longitude` = '$center_longitude'
				WHERE `apartat_id` =$apartat_id
				LIMIT 1") ;

			//$gl_page->show_message($this->messages['point_saved']);
			$_SESSION['message'] = $this->messages['point_saved'];
			Debug::p_all();
			print_javascript('parent.window.location.replace(parent.window.location);');
			die();
		}
		else{
			$this->map_points_error();
		}
	}
	function map_points_error(){
		global $gl_page;
		$gl_page->show_message('Hi ha un error en l\'entrada de coordenades, si us plau comproveu que estigui ben formada');
		Debug::p_all();
		die();
	}
}
?>