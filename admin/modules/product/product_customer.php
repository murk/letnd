<?
/**
 * ProductCustomer
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2010
 * @version $Id$
 * @access public
 */
class ProductCustomer extends Module{
	var $condition = '', $q='', $is_excel;
	function __construct(){
		parent::__construct();
	}
	function on_load(){

		if (is_file(CLIENT_PATH . '/product_customer_config.php')){
			include (CLIENT_PATH . '/product_customer_config.php');
			if ($fields) $this->fields = array_merge ($this->fields, $fields);
		}


	}
	function list_records()
	{
		$GLOBALS['gl_content'] = '<script type="text/javascript" language="JavaScript" src="/admin/modules/product/jscripts/functions.js"></script>' . $this->get_search_form() . $this->get_records();
	}
	function get_records()
	{
		$listing = new ListRecords($this);

		if ($this->process=='wholesaler'){
			$condition = 'is_wholesaler = 1';
			$listing->set_field('has_equivalencia', 'checkbox_show_text', true);
		}
		else{
			$condition = 'is_wholesaler = 0';
			$listing->unset_field('has_equivalencia');
		}
		// search
		if ($this->condition) {
			$listing->condition = $this->condition . ' AND ' . $condition;
		}
		else{
			$listing->condition = $condition;
		}
		if ($this->parent == 'NewsletterCustumer') {
			return $listing;
		}

		$listing->add_options_button('export_button', 'action=export', 'botoOpcions1 excel', 'top_after');
		$listing->add_filter( 'activated' );
		$listing->add_button('list_orders','action=list_orders', 'boto1', 'after');
		$listing->add_button('activate_button','', 'boto2', 'after', 'activate_customer');
		$listing->add_button('deactivate_button','', 'boto1', 'after', 'deactivate_customer');
		$listing->add_button('set_wholesaler_button','', 'boto1', 'after', 'set_wholesaler');
		$listing->add_button('unset_wholesaler_button','', 'boto1', 'after', 'unset_wholesaler');
		$listing->call('records_walk','activated,is_wholesaler',true);
		$listing->order_by = 'activated ASC,name ASC,surname ASC';


		if ( $this->is_excel ) {
			return $listing;
		}

	    return $listing->list_records();
	}
	function records_walk(&$listing,$activated,$is_wholesaler){
		$b = &$listing->buttons;
		$b['activate_button']['show'] = !$activated;
		$b['deactivate_button']['show'] = !$b['activate_button']['show'];
		$b['set_wholesaler_button']['show'] = !$is_wholesaler;
		$b['unset_wholesaler_button']['show'] = !$b['set_wholesaler_button']['show'];
	}
	// accio del boto de activar o desactivar
	function activate_customer(){
		$activated = $_GET['activated'];
		if ($this->_send_activated($activated))
		{
			$customer_id = $_GET['customer_id'];
			$message = $activated=='1'?$this->messages['activated']:$this->messages['deactivated'];
			Db::execute('UPDATE `product__customer` SET `activated`='.$activated.' WHERE `customer_id`=' . $customer_id);
			$_SESSION['message'] = $message;
			print_javascript('parent.window.location.replace(parent.window.location);');
		}
		else{
			$GLOBALS['gl_page']->show_message ($this->messages['not_activated']);
		}
		die();
		Debug::p_all();
	}
	// accio del boto passar a distribuidor
	function set_wholesaler(){
		$customer_id = $_GET['customer_id'];
		$is_wholesaler = $_GET['is_wholesaler'];
		$message = $is_wholesaler?$this->messages['set_wholesaler']:$this->messages['unset_wholesaler'];
		Db::execute('UPDATE `product__customer` SET `is_wholesaler`='.$is_wholesaler.' WHERE `customer_id`=' . $customer_id);
		$_SESSION['message'] = $message;
		print_javascript('parent.window.location.replace(parent.window.location);');
	}
	function list_orders(){
		$module = Module::load('product','order');
		//$module->do_action('list_records');
		$GLOBALS['gl_content'] = $this->get_search_form() . $module->do_action('get_records');
	}
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_search_form() . $this->get_form();
	}
	function get_form()
	{
		$this->merge_module('product', 'address');

		$show = new ShowForm($this);
		
		// nomes es obligatori a public, a admin que facin el que vulguin
		$show->set_field('surname','required','0');
		$show->set_field('birthdate','required','0');
		$show->set_field('mail','required','0');
		$show->set_field('password','required','0');
		$show->set_field('telephone','required','0');
		$show->set_field('dni','required','0');		
		$show->set_field('address','required','0');		
		$show->set_field('zip','required','0');		
		$show->set_field('city','required','0');		
		
		
		if ($this->parent == 'NewsletterCustumer') {
			$show->set_field('group_id','form_admin','input');
			$show->set_field('group_id','select_caption',$this->caption['c_group_caption']);
		}
		else{
			$show->set_var('group_id','');
		}
		$show->before_table = 'product__address, ';
		$show->condition = 'product__customer.customer_id = product__address.customer_id AND invoice=\'1\'';

		$show->get_values();
		
		// m'asseguro que no es pugui canviar al guardar un formulari client a distribuidor
		$is_wholesaler = $this->action=='show_form_new'?$this->process=='wholesaler':$show->rs['is_wholesaler'];
		$show->set_var('is_wholesaler',$is_wholesaler); // a form new no ho agafa sino
		
		if ($this->action == 'show_form_new' || $this->action == 'get_form_new') {
			// desplegable country per la segona adreça
			$this->set_var (
			'country_id2',
			get_select('country_id2', '', 'ES',$this->fields['country_id']));
			$this->set_var (
				'provincia_id2',
				get_select('provincia_id2', '', '',$this->fields['provincia_id']));
		}
		// al form no surt nom i cognom de l'adreça de facturació, es la mateixa que posem al customer
		if ($this->action!='get_record') {
			$show->set_field('a_name','form_admin','no');
			$show->set_field('a_surname','form_admin','no');
		}

		// si editem
		if ($this->action=='show_form_edit'){
			$module = Module::load('product', 'address');
			$module->caption['c_form_title'] = '';
			$module->customer_id = $show->rs['customer_id'];
			$module->delivery_id = $show->rs['delivery_id'];
			$addresses  = $module->do_action('get_records');
			$show->set_var('addresses',$addresses);
		}
		// faig password no obligatori amb distribuidor
		if ($is_wholesaler){
			$show->set_field('password','required','');	
			$show->set_field('has_equivalencia','default_value',$this->config['has_equivalencia_default']);		
		}
		else{
			$show->unset_field('has_equivalencia');			
			$show->rs['has_equivalencia'] = false;
			
		}
		return $show->show_form();
	}

	function save_rows()
	{
		$save_rows = new SaveRows($this);
		$save_rows->field_unique = "mail";
	    $save_rows->save();
	}

	function write_record()
	{
		$pass_sent = true;
		$message = '';
		
		if ($this->action=='add_record'){
			$this->add_field('is_wholesaler');
			$this->set_field('is_wholesaler','type','int');
			$this->set_field('is_wholesaler','override_save_value',$this->process=='wholesaler'?'1':'0');
		}
		else{
			$this->unset_field('is_wholesaler');		
		}
		
		$writerec = new SaveRows($this);
		$writerec->field_unique = "mail";

		$country = $writerec->get_value( 'country_id' );
		if ($country != 'ES') $writerec->set_value( 'provincia_id', 0 );
		
		if (($this->process=='wholesaler') && $this->action=='add_record'){
			if (!$writerec->get_value('password')) $writerec->set_value('password',get_password());
			
			$pass_sent = $this->_send_password(
				$writerec->get_value('mail'),
				$writerec->get_value('password'), 
				$writerec->get_value('treatment'), 
				$writerec->get_value('name'), 
				$writerec->get_value('surname'));
			$message = $this->messages['password_sent'];
			//$message = 
		}
		
		
		$writerec->save();	
		
		// poso el remove key automaticament
		if ($this->action=='add_record' && $GLOBALS['gl_insert_id'])
			$this->newsletter_add_remove_key('product__customer', $GLOBALS['gl_insert_id']);

		// nomes a save_rows
		$message = isset($GLOBALS['gl_errors']['add_exists'])?$GLOBALS['gl_errors']['add_exists']:$message;

		if (!$writerec->saved) return; // si no ha grabat (ej. add_exists) no faig res més

		// primera adreça
		// canvio l'id del post per guardar automatic amb save_records
		if ($this->action=='save_record')
		{
			// definir l'address_id;
			$address_id = current($_POST['address_id']);
			$unset_id = current($_POST['customer_id']);
			$customer_id = $unset_id;
			foreach ($_POST as $key=>$val){
				if (is_array($val) && array_key_exists($unset_id,$val)){
				//if (isset($val[$unset_id])){
					unset($_POST[$key][$unset_id]);
					$_POST[$key][$address_id]=$val[$unset_id];
				}
			}
		}
		$module = Module::load('product','address');
		$module->parent = 'ProductCustomer';
		if ($this->action=='add_record') {
			$customer_id = $GLOBALS['gl_insert_id'];
			$module->customer_id = $customer_id;
			$module->set_field('invoice','override_save_value','1'); // aquesta adreça es la de facturació
		}
		$module->do_action($this->action);

		// segona adreça, nomes es crea nova al crear nou usuari, sino es crea nova desde una finestra al seu modul corresponent
		if (!isset($_POST['same_address']) && ($this->action=='add_record')) {
			$query = "INSERT INTO product__address SET
			customer_id = " . Db::qstr($customer_id) . ",
			a_name = " . Db::qstr($_POST['a_name2']) . ",
			a_surname = " . Db::qstr($_POST['a_surname2']) . ",
			address = " . Db::qstr($_POST['address2']) . ",
			zip = " . Db::qstr($_POST['zip2']) . ",
			city =" . Db::qstr($_POST['city2']) . ",
			invoice = 0";

			$country_id2 = R::text_id( 'country_id2', '', '_POST' );
			$provincia_id2 = R::text_id( 'provincia_id2', '', '_POST' );

			if ( $country_id2 ) {
				$query .= ",
							country_id = " . Db::qstr($country_id2);
			}

			if ( $country_id2 == 'ES' && $provincia_id2 ) {
				$query .= ",
							provincia_id = " . Db::qstr($provincia_id2);
			}

			Db::execute($query);

			// relaciono el customer amb aquesta adreça per saber quina es la de entrega
			$query = 'UPDATE product__customer SET delivery_id = ' . Db::insert_id() .
				' WHERE customer_id = ' . $customer_id;
			Db::execute($query);
		}
		if ($this->action=='save_record') {
			// canvio el delivery_id segons convingui

		}
		if ($message) $GLOBALS['gl_message'] = $message;

		// si no s'ha enviat mail vaig al formulari del customer per poder guardar un altre cop
		
		if (!$pass_sent){
			$_SESSION['message'] = $this->messages['password_not_sent'];
			print_javascript("top.window.location.href = '/admin/?menu_id=20124&action=show_form_edit&customer_id=" . $customer_id . "'");
			die();
		}
	}

	function manage_images()
	{
	    $image_manager = new ImageManager($this);
	    $image_manager->execute();
	}

	//
	// FUNCIONS BUSCADOR
	//
	function search( $get = false ){

		$q = $this->q = R::escape('q');
		$search_condition = '';

		if ($q)
		{
			// Sempre busco a tot arreu, abans si detectava que eren refrencies no hi  buscava, posaré checkbox de "buscar nomès referencies"

			$search_condition = "(
				name like '%" . $q . "%'
				OR surname like '%" . $q . "%'
				OR concat(name,' ',surname) like '%" . $q . "%'
				OR mail like '%" . $q . "%'
				OR dni like '%" . $q . "%'
				OR telephone like '%" . $q . "%'
				OR telephone_mobile like '%" . $q . "%'
				OR comment like '%" . $q . "%'
				" ;


			// busco a address
			$query = "SELECT customer_id FROM product__address WHERE (
								 a_name like '%" . $q . "%' OR
								 a_surname like '%" . $q . "%' OR
								 address like '%" . $q . "%' OR
								 zip like '%" . $q . "%' OR
								 city like '%" . $q . "%' OR
								 concat(a_name,' ',a_surname) like '%" . $q . "%'
									)" ;
			$results = Db::get_rows_array($query);
			//$results = array_merge($results, Db::get_rows_array($query));

			$search_condition .= $this->get_ids_query($results, 'product__customer.customer_id');
			$search_condition .= ") " ;
			//$search_condition = $this->get_ref_query('order_id',$q, $search_condition);


			$GLOBALS['gl_page']->title = TITLE_SEARCH . '<strong>&nbsp;&nbsp;"' . $q . '"</strong>';
		}
		if ($this->process=='wholesaler'){
			$search_condition .=$q?' AND is_wholesaler=1':'is_wholesaler=1';
		}
		$this->condition = $search_condition;
		Debug::add('Search condition', $this->condition);

		if ($get) return $this->get_records();
		else $this->do_action('list_records');
	}

	function get_ids_query(&$results, $field)
	{
		if (!$results) return '';
		$new_arr = array();
		foreach ($results as $rs){
			$new_arr [$rs[0]] = $rs[0];
		}
		return "OR " . $field . " IN (" . implode(',', $new_arr) . ") ";
	}
	function get_ref_query($field, $q, $search_condition){
		//
		// busqueda automatica de referencies separades per espais o comes
		//
		$ref_array = explode (",", $q); // si l'array hem dona 1 probo de fer-ho amb espais, si es una sola referncia donarà el mateix resultat
		count($ref_array) == 1?$ref_array = explode (" ", $q):false;

		foreach($ref_array as $key => $value)
		{
			if (!is_numeric($value))
			{
				$ref_array = '';
				break;
			}
		}

		// consulta
		if ($ref_array)
		{
			$query_ref = "";
			foreach ($ref_array as $key)
			{
				$query_ref .= $field . " = '" . $key . "' OR ";
			}
			$query_ref = "(" . substr($query_ref, 0, -3) . ") ";

			return '( ' . $query_ref . ' OR ' . $search_condition . ')';
		}
		else{
			return $search_condition;
		}
	}

	function get_search_form(){
		$tpl = new phemplate(PATH_TEMPLATES);
		$this->caption['c_by_ref']='';
		$tpl->set_vars($this->caption);
		$tpl->set_file('search.tpl');
		$tpl->set_var('menu_id',$GLOBALS['gl_menu_id']);
		$tpl->set_var('q',  htmlspecialchars(R::get('q')));		
		
		if ($this->q) {
			$GLOBALS['gl_page']->javascript .= "
				$('table.listRecord').highlight('".addslashes(R::get('q'))."');			
			";
		}
		
		return $tpl->process();
	}

	function list_records_walk($customer_id, $name, $surname, $mail)
	{
		$name=  $name . ' ' . $surname . ' (' . $mail . ')';
		$name= addslashes($name); // cambio ' per \'
		$name= htmlspecialchars($name); // cambio nomès cometes dobles per  &quot;
		$ret['tr_jscript'] = 'onClick="parent.insert_address(' . $customer_id . ',\'' . $name . '\')"';
		return $ret;
	}
	function send_password_kaskote(){
		$results = Db::get_rows("SELECT customer_id, treatment, name, surname, mail, password
			FROM product__customer
			WHERE bin = '0'
			AND activated=1
			AND kaskote_sent=0"); 
		foreach($results as $rs){			
			extract($rs);
			
			$pass_sent = $this->_send_password($mail,$password, $treatment, $name, $surname);
			
			if ($pass_sent){
				Db::execute('UPDATE `product__customer` SET `kaskote_sent`=1 WHERE `customer_id`='.$customer_id);	
				echo("<br>$customer_id - $name $surname - $mail <br>");
			}
			else{
				echo("<span style='color:red'><br>$customer_id - $name $surname - $mail <br></span>");
			}
			
			
		}
		die();
	}
	function send_password(){
		$rs = Db::get_row("SELECT treatment, name, surname, mail, password 
			FROM product__customer 
			WHERE customer_id = '" . $_GET['customer_id'] . "'"); 
		extract($rs);
		$pass_sent = $this->_send_password($mail,$password, $treatment, $name, $surname);
		if ($pass_sent){
			$GLOBALS['gl_message'] = $this->messages['password_sent'];
		}
		else{
			$GLOBALS['gl_message'] = $this->messages['password_not_sent'];		
		}
		$GLOBALS['gl_page']->is_frame = true;
	}
	
	
	function _send_password($mail,$password, $treatment, $name, $surname){	

		// preparo mail	
		$tpl_message = new phemplate(PATH_TEMPLATES_PUBLIC, 'product/mail_password.tpl');		
		$tpl_message->set_var('password', $password);
		$tpl_message->set_var('treatment', $this->caption['c_treatment_'.$treatment]);
		$tpl_message->set_var('name', $name);
		$tpl_message->set_var('surname', $surname);
		$tpl_message->set_var('mail', $mail);
		$tpl_message->set_vars($this->caption);

		$tpl = new phemplate(PATH_TEMPLATES_PUBLIC, 'newsletter/message_templates/message.tpl');
		$tpl->set_var('pictures',array());
		$tpl->set_var('preview',false);
		$tpl->set_var("url", HOST_URL);
		$tpl->set_var ('message',$tpl_message->process());
		$tpl->set_var ('page_address',$this->config['page_address']);		
		
		$body = $tpl->process();
		
		$subject = $this->caption['c_password_mail_subject'];

		// envio mail
		return $this->_send_mail_customer($body, $subject, $mail);
	}
	
	
	function _send_activated($activated){	
		if ( $activated=='0' ) return true; // quan es desactiva no s'envia mail, però retorno true per que guardi a la bbdd
		$rs = Db::get_row("SELECT treatment, name, surname, mail, password 
			FROM product__customer 
			WHERE customer_id = '" . $_GET['customer_id'] . "'"); 
		extract($rs);
		
		// preparo mail	
		$tpl = new phemplate(PATH_TEMPLATES_PUBLIC, 'mail.tpl');
		
		$tpl->set_var("url", HOST_URL);
		$tpl->set_var('short_host_url', substr(HOST_URL,7,-1));
		$tpl->set_var('preview',false);
		$tpl->set_var('remove_text','');
		
		$tpl->set_vars($this->config);
		$tpl->set_vars($this->caption);
		
		$url_name = $this->config['company_name']?$this->config['company_name']:HOST_URL;
		$message = $this->caption['c_activated_mail_message'];
		$tpl->set_var ('content',
			sprintf($message,
						'<a href="'.HOST_URL.'">'.$url_name.'</a>'));
		$tpl->set_var ('page_address',$this->config['page_address']);		
		
		$body = $tpl->process();
	
		if (PRODUCT_MAIL_AUTO_INLINE_CSS) {
			$instyler = new \Pelago\Emogrifier($body);
			$body = $instyler->emogrify();
		}
		
		$subject = $this->caption['c_activated_mail_subject'];
		
		// envio mail
		return $this->_send_mail_customer($body, $subject, $mail);
	}
	function _send_mail_customer($body, $subject, $mail){
		// envio mail
		$mailer = get_mailer($this->config);
		if (is_file(CLIENT_PATH . 'images/logo_newsletter.jpg'))
			$mailer->AddEmbeddedImage(CLIENT_PATH . 'images/logo_newsletter.jpg', 'logo', 'logo.jpg');
		elseif(is_file(CLIENT_PATH . 'images/logo_newsletter.gif')) 
			$mailer->AddEmbeddedImage(CLIENT_PATH . 'images/logo_newsletter.gif', 'logo', 'logo.gif');
		
		$mailer->Body = $body;
		$mailer->Subject = $subject;
		$mailer->IsHTML(true);
		$mailer->AddAddress ($mail);
		Debug::add('mailer', $mailer);
		return send_mail($mailer, false);
	}
	function newsletter_add_remove_key($table, $id) {	

			$table_id = strpos($table, "customer")!==false?'customer_id':'custumer_id';

			$q2 = "SELECT count(*) FROM ". $table . " WHERE BINARY remove_key = ";
			$q3 = "UPDATE ". $table . " SET remove_key='%s' WHERE  ". $table_id . " = '%s'";

			$updated = false; 
			while (!$updated){
				$pass = get_password(50);
				$q_exists = $q2 . "'".$pass."'";	

				// si troba regenero pass
				if(Db::get_first($q_exists)){
					$pass = get_password(50);							
				}
				// poso el valor al registre
				else{				
					$q_update = sprintf($q3, $pass, $id);

					Db::execute($q_update);
					break;
				}
			}
	}
	function export()
	{
		ini_set('max_execution_time',120);
		ini_set('memory_limit', '1024M');

		$this->is_excel = true;

	    Main::load_class('excel');


		$column_formats = array (
			'telephone' => 'FORMAT_TEXT',
			'telephone_mobile' => 'FORMAT_TEXT',
			'dni' => 'FORMAT_TEXT',
		);


		$q = $this->q = R::escape('q');

		$listing = $q?$this->search(true):$this->get_records();

	    $this->merge_export_addresses_config($listing);

	    $listing->send_field_bottom( 'enteredc' );
	    $listing->send_field_bottom( 'prefered_language' );

		$listing->limit = 5000;

		$listing->before_table = 'product__address, ';
		$and = $listing->condition ? ' AND' : '';
		$listing->condition .= "$and product__customer.customer_id = product__address.customer_id AND invoice='1'";


		$listing->list_records( false );

		$this->get_export_delivery_address($listing->results);

		$excel = new Excel( $listing );
		$excel->set_visible_fields( [], ['list_admin', 'form_admin'] )
			->show_fields( 'customer_id' )
			->column_formats( $column_formats )
			->file_name( 'customers' )
			->make( $listing->results );
	}
	private function get_export_delivery_address(&$results){

		if (!$results) return;

		$module = Module::load( 'product', 'address', '', false );
		$module->action = 'show_record';

		foreach ( $results as $key => $val ) {
			$rs          = &$results[ $key ];
			$customer_id = $rs['customer_id'];
			$address_id = $rs['delivery_id'];
			$name = $rs['name'];
			$surname = $rs['surname'];
			$is_same_address = false;

			$show            = new ShowForm( $module );
			$show->unset_field( 'customer_id' );
			$show->has_bin   = false;

			if ($address_id == 0){
				$is_same_address = true;
				$query     = "SELECT address_id FROM product__address WHERE customer_id = $customer_id AND invoice = 1";
				$address_id = Db::get_first( $query );
			}

			$show->id = $address_id;


			$show->set_record();

			if ($show->rs) {
				foreach ( $show->rs as $k=>$val ) {
					$rs["delivery_$k"] = $val;
				}
				if ($is_same_address) {
					$rs['delivery_a_name'] = $name;
					$rs['delivery_a_surname'] = $surname;
				}
			}
		}
	}
	private function merge_export_addresses_config( &$listing ) {

		$gl_db_classes_fields = [];
		include( DOCUMENT_ROOT . 'admin/modules/product/product_address_config.php' );

		unset($gl_db_classes_fields['address_id']);
		unset($gl_db_classes_fields['customer_id']);

		$fields_delivery = [];
		foreach ( $gl_db_classes_fields as $key => $field ) {
			$field['list_admin'] = 'out';
			$field['type'] = 'none';
			$fields_delivery["delivery_$key"] = $field;
		}

		unset($gl_db_classes_fields['a_name']);
		unset($gl_db_classes_fields['a_surname']);

		$fields_invoice = [];
		foreach ( $gl_db_classes_fields as $key => $field ) {
			$fields_invoice["$key"] = $field;
		}
		$listing->fields = array_merge( $listing->fields, $fields_invoice );
		$listing->fields = array_merge( $listing->fields, $fields_delivery );

		$gl_caption = [];
		$gl_caption_address = [];
		$gl_caption_customer = [];

		include( DOCUMENT_ROOT . 'admin/modules/product/languages/'. LANGUAGE . '.php' );

		$gl_caption = array_merge( $gl_caption, $gl_caption_address );

		// TODO-i  posar nomes els de els fields
		foreach ( $gl_caption as $key => $val ) {
			$key = substr( $key, 2 );

			// Poso el text d'adreça d'entrega
			if ($key == 'a_name') $val = $gl_caption_customer ['c_form_title4'] . ' - ' . $val;

			$listing->caption["c_$key"] = $val;
			$listing->caption["c_delivery_$key"] = $val;
		}

		$listing->caption["c_customer_id"] = 'Id';
	}
}