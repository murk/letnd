<script language="JavaScript" src="/admin/modules/inmo/jscripts/functions.js?v=<?=$version?>"></script>
<?if (!$results):?>
<?if ($options_filters):?>
<div id='opcions_holder'><table width="100%" border="0" cellspacing="0" cellpadding="0" id="opcions">
    <tr>
      <td class="opcionsTitol"><strong><?=$c_options_bar?></strong></td>
    </tr>
    <tr>
      <td class="opcions"><table class="filters">
        <tr valign="middle">
		<?foreach($options_filters as $options_filter):?>
          <td><?=$options_filter?></td>
		  <?endforeach //$options_filters?>
          <?if ($save_button):?>
          <?endif //save_button?>
          <?if ($select_button):?>
          <?endif //select_button?>
          <?if ($delete_button):?>
          <?if ($has_bin):?>
          <?if ($is_bin):?>
          <?else:?>
          <?endif // is_bin?>
          <?else:?>
          <!--<td>
              <td>&nbsp;</td><strong><a class="botoOpcions" href="/admin?menu_id=<?=$menu_id?>&sort_by=0&sort_order=<?=$sort_order0?>">
                <?=$c_move_to?>
               
            <input type="submit" name="bin_selected" value="<?=$c_delete_selected?>" class="boto1">
              </a></strong></td>-->
          <?endif // has_bin?>
          <?endif //delete_button?>
          <?if ($print_button):?>
          <?endif //delete_button?>
        </tr>
      </table></td>
    </tr>
</table></div>
<?endif //options_filters?>
<?endif //!$results?>
<?if ($results):?>
<div id='split_holder'><table width="100%" border="0" cellpadding="0" cellspacing="0" id="split">
  <tr valign="middle">
    <td width="20%" align="left" nowrap class="pageResults"><?=$split_results_count?></td>
    <td align="right" nowrap class="pageResults"><?=$split_results?></td>
  </tr>
</table></div>
<?if ($options_bar):?>
  <div id='opcions_holder'><table width="100%" border="0" cellspacing="0" cellpadding="0" id="opcions">
    <tr>
      <td class="opcionsTitol"><strong>
        <?=$c_options_bar?>
        </strong></td>
    </tr>
    <tr>
      <td class="opcions"><div id="opcions_fixed_bar"><div class="more"></div></div><table class="filters">
        <tr valign="middle">
		<?foreach($options_move_tos as $options_move_to):?>
          <td><?=$options_move_to?></td>
		  <?endforeach //$options_move_tos?>
          <?foreach($options_filters as $options_filter):?><form>
          <td valign="top">
            <?=$options_filter?>
          </td>
          </form>
          <?endforeach //$options_filters?>
        </tr>
      </table><?if ($options_filters):?><div class="sep"></div><?endif //options_filters?>
      <table border="0" cellpadding="2" cellspacing="0">
          <tr valign="middle">
            <?if ($save_button):?>
              <td><strong><a class="botoOpcionsGuardar" href="javascript:submit_form_save('save_rows_save_records<?=$action_add?>')">
                <?=$c_send_edit?>
                </a></strong></td>
              <?endif //save_button?>
            <?if ($select_button):?>
              <td><?=$c_select?>
                &nbsp;<strong><a class="opcions" href="javascript:select_all(true, document.list)">
                <?=$c_all?>
                </a>&nbsp;-&nbsp;<a class="opcions" href="javascript:select_all(false, document.list)">
                <?=$c_nothing?>
                </a></strong></td>
              <?endif //select_button?>
            <?if ($delete_button):?>
              <?if ($has_bin):?>
                <?if ($is_bin):?>
                  <td><strong><a class="botoOpcions" href="javascript:submit_list('save_rows_delete_selected<?=$action_add?>', '<?=$c_confirm_deleted?>', true)">
                    <?=$c_delete_selected?>
                    </a></strong></td>
                  <td><strong><a class="botoOpcions" href="javascript:submit_list('save_rows_restore_selected<?=$action_add?>')">
                    <?=$c_restore_selected?>
                    </a></strong></td>
                  <?else:?>
                  <td><strong><a class="botoOpcionsBorrar" href="javascript:submit_list('save_rows_bin_selected<?=$action_add?>')">
                    <?=$c_delete_selected?>
                    </a></strong></td>
                  <?endif // is_bin?>
                <?else:?>
                <td><strong><a class="botoOpcionsBorrar" href="javascript:submit_list('save_rows_delete_selected<?=$action_add?>', '<?=$c_confirm_deleted?>', true)">
                  <?=$c_delete_selected?>
                  </a></strong></td>
                <!--<td>
              <td>&nbsp;</td><strong><a class="botoOpcions" href="/admin?menu_id=<?=$menu_id?>&sort_by=0&sort_order=<?=$sort_order0?>">
                <?=$c_move_to?>
               
            <input type="submit" name="bin_selected" value="<?=$c_delete_selected?>" class="boto1">
              </a></strong></td>-->
                <?endif // has_bin?>
              <?endif //delete_button?>
            <?if ($print_button):?>
              <td><strong><a class="botoOpcions" href="<?=$print_link?>">
                <?=$c_print_all?>
                </a></strong></td>
              <?endif //delete_button?>
          </tr>
        </table></td>
    </tr>
  </table></div>
  <?endif //$options_bar?>
<?if ($order_bar):?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td height="10"></td>
  </tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="listRecord">
<tr><td class="listCaption">
<table border="0" cellspacing="0" cellpadding="0" >
  <tr>
    <td valign="top" nowrap="nowrap"><strong>
      &nbsp;<?=$c_order_by?>&nbsp;&nbsp;
      </strong></td>
      <td valign="top">
    <?foreach($fields as $l): extract ($l)?><strong><a class="opcions" href="<?=$order_link?>">
      <?=$field?>
    </a></strong>
        <?if ($order_image):?>
          <img src="/admin/themes/inmotools/images/<?=$order_image?>.gif" width="9" height="9" border="0" align="middle">
          <?endif //order_image?>
        &nbsp;<?endforeach //$field?></td>
  </tr>
</table>
</td>
</tr>
</table>
<?else: //$order_bar?>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="listRecord">
  <tr>
    <td></td>
  </tr>
</table>
<?endif //$order_bar?>
<form method="post" action="<?=$form_link?>" name="list" onsubmit="return check_selected(this)">
<input name="action" type="hidden" value="save_rows"><?=$out_loop_hidden?>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="listRecord">
    <?foreach($loop as $l): extract ($l)?>
      <tr class="listItem<?=$odd_even?>" onmousedown="change_bg_color(this, 'Down', '<?=$id?>')" id="list_row_<?=$id?>">
        <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td><table border="0" cellspacing="0" cellpadding="0">
                  <tr><?if ($options_checkboxes):?> 
                    <td><input name="selected[<?=$conta?>]" type="checkbox" value="<?=$id?>" onclick="return false;"<?=$selected?>/></td><?endif // options_checkboxes?>
                    <td class="listTitle">&nbsp;<strong>                      
                      <input name="property_id[<?=$contract_id?>]" type="hidden" value="<?=$contract_id?>" /><?=$c_contract_title_list?>: </strong><?=$c_contract_id?> <?=$contract_id?></td>
                    </tr>
                </table></td>
              </tr>
            <tr>
              <td align="center"><table width="95%" border="0" cellpadding="0" cellspacing="0">
                  <tr>
                    <td width="30%" valign="top"><table border="0" cellpadding="0" cellspacing="0" class="listDetails">
                      <tr>
                        <td valign="top"><strong class="listItem">
                          <?=$c_signed?>
                          :</strong>&nbsp;</td>
                        <td valign="top"><?=$signed?></td>
                      <tr>
                        <td valign="top"><strong class="listItem">
                          <?=$c_kind_id?>
                          :</strong>&nbsp;</td>
                        <td valign="top"><?=$kind_id?></td>
                        </tr><?if ($registered):?>
                      <tr>
                        <td valign="top" nowrap="nowrap">
<strong class="listItem">
                          <?=$c_registered2?>
                          </strong></td>
                        <td valign="top"><?=$registered?></td>
                        </tr><?endif //registered?>
                      <tr>
                        <td valign="top"><strong class="listItem">
                          <?=$c_duration?>
                          :</strong>&nbsp;</td>
                        <td valign="top"><?=$duration?>
                          <span class="forms">
                          <?=$c_months?>
                          </span></td>
                      </tr>
                      <tr>
                        <td valign="top"><strong class="listItem">
                          <?=$c_state?>
                          :</strong>&nbsp;</td>
                        <td valign="top"><?=$state?></td>
                      </tr><?if ($rescinded):?>
                      <tr>
                        <td valign="top" nowrap="nowrap">
<strong class="listItem">
                          <?=$c_rescinded?>
                          </strong></td>
                        <td valign="top"><?=$rescinded?></td>
                        </tr><?endif //rescinded?>
                      <tr>
                        <td valign="top"><strong class="listItem">
                          <?=$c_expires3?>
                          :</strong>&nbsp;</td>
                        <td valign="top"><strong class="<?=$expires_color?>"><?=$expires?></strong>
                          <?=$c_days?></td>
                      </tr>
                      <tr>
                        <td valign="top"><strong class="listItem">
                          <?=$c_ipc4?>
                          :</strong>&nbsp;</td>
                        <td valign="top"><strong class="<?=$ipc_color?>"><?=$ipc?></strong>&nbsp;<?=$c_days?></td>
                        </tr>
                    </table>
                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td class="listDescription"><?=$contract_observations?></td>
                        </tr>
                      </table></td>
                    <td width="70%" valign="top" ><?if ($property_id):?><table width="100%" border="0" cellspacing="0" cellpadding="0" class="listDetails2">
                        <tr>
                          <td valign="top"><strong class="resaltatSmall"><?=$c_property_id?>
                            </strong>
                            <table width="100%" border="0" cellpadding="0" cellspacing="0" style="padding-left:20px;">
                              <tr>
                                <td valign="top">
                                    <table border="0" cellpadding="0" cellspacing="0" class="listDetails">
                                      <tr>
                                        <td valign="top"><strong class="listItem">
                                          <?=$c_ref?>
                                          :</strong>&nbsp;</td>
                                        <td valign="top"><?=$ref?></td>
                                      </tr>
                                      <tr>
                                        <td valign="top"><strong class="listItem">
                                          <?=$c_property_private?>
                                          :</strong>&nbsp;</td>
                                        <td valign="top"><?=$property_private?></td>
                                      </tr>
                                      <tr>
                                        <td valign="top"><strong class="listItem">
                                          <?=$c_tipus?>
                                          :</strong>&nbsp;</td>
                                        <td valign="top"><?=$tipus?></td>
                                      </tr>
                                      <tr>
                                        <td valign="top"><strong class="listItem">
                                          <?=$c_category_id?>
                                          :</strong>&nbsp;</td>
                                        <td valign="top"><?=$category_id?></td>
                                      </tr>
                                      <tr>
                                        <td valign="top"><strong class="listItem">
                                          <?=$c_tipus2?>
                                          :</strong>&nbsp;</td>
                                        <td valign="top"><?=$tipus2?></td>
                                      </tr>
                                      <tr>
                                        <td valign="top"><strong class="listItem">
                                          <?=$c_price?>
                                          :</strong>&nbsp;</td>
                                        <td valign="top"><span class="resaltatSmall">
                                          <?if ($price):?><?=$price?><?endif // price?>
                                        </span></td>
                                      </tr>
                                      <tr>
                                        <td valign="top">&nbsp;</td>
                                        <td valign="top">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td valign="top">&nbsp;</td>
                                        <td valign="top">&nbsp;</td>
                                      </tr>
                                  </table></td>
                                <?if ($image_name):?>
                                <td valign="top" class="listImage">	  
								  <div><img src="<?=$image_src_admin_thumb?>"></div>
								  </td>
                                <?endif // image_name?>
                              </tr>
                            </table>
                            <?if ($owners):?>
<strong class="resaltatSmall">
                            <?=$c_owner_title?>
                            </strong>
                              <table border="0" cellpadding="0" cellspacing="0" class="listDetails" style="padding-left:20px;">
                                    <?foreach($owners as $o):extract ($o)?>
                                <tr>
                                  <td valign="top"><?php if ( $name || $surname1 || $surname2) : ?><strong><?=$c_custumer_id?>:</strong> <?=$name?>
                    <?=$surname1?> 
                    <?=$surname2?><?php endif //name?></td><td valign="top">
					<?php if ( $phone1 || $phone2 || $phone3) : ?><strong><?=$c_phone?>:</strong><?php endif //phone1?>
                    <?=$phone1?><?php if ( $phone1 && $phone2 ) : ?> | <?php endif //phone1?>
                    <?=$phone2?><?php if ( $phone2 && $phone3 ) :?> | <?php endif //phone1?>
                    <?=$phone3?><br /></td>
                                  </tr><?endforeach //$owners?>
                            </table><?endif //$owners?></td>
                        </tr>
                      </table><?endif // property_id?><?if ($buyers):?><table width="100%" border="0" cellspacing="0" cellpadding="0" class="listDetails2">
                        <tr>
                          <td valign="top"><strong class="resaltatSmall">
                            <?=$c_buyers?>
                            </strong>
                              <table border="0" cellpadding="0" cellspacing="0" class="listDetails" style="padding-left:20px;">
                                    <?foreach($buyers as $o):extract ($o)?>
                                <tr>
                                  <td valign="top"><?php if ( $name || $surname1 || $surname2) : ?><strong><?=$c_custumer_id?>:</strong> <?=$name?>
                    <?=$surname1?> 
                    <?=$surname2?><?php endif //name?></td><td valign="top">
					<?php if ( $phone1 || $phone2 || $phone3) : ?><strong><?=$c_phone?>:</strong><?php endif //phone1?>
                    <?=$phone1?><?php if ( $phone1 && $phone2 ) : ?> | <?php endif //phone1?>
                    <?=$phone2?><?php if ( $phone2 && $phone3 ) :?> | <?php endif //phone1?>
                    <?=$phone3?><br /></td>
                                  </tr><?endforeach //$buyers?>
                            </table></td>
                        </tr>
                      </table><?endif //$buyers?></td>
                  </tr>
                </table>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td align="right"><?foreach($buttons as $button):?><?=$button?><?endforeach //$buttons?></td>
                  </tr>
                  <tr>
                    <td><img src="/admin/themes/inmotools/images/spacer.gif" width="1" height="10"></td>
                  </tr>
                </table></td>
              </tr>
          </table></td>
        </tr>
      <?endforeach //$loop?>
</table>
</form>
<?if ($order_bar):?>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="listRecord">
  <tr>
    <td class="listCaption"><table border="0" cellspacing="0" cellpadding="0" >
  <tr>
    <td valign="top" nowrap="nowrap"><strong>
      &nbsp;<?=$c_order_by?>&nbsp;&nbsp;
      </strong></td>
      <td valign="top">
    <?foreach($fields as $l): extract ($l)?><strong><a class="opcions" href="<?=$order_link?>">
      <?=$field?>
    </a></strong>
        <?if ($order_image):?>
          <img src="/admin/themes/inmotools/images/<?=$order_image?>.gif" width="9" height="9" border="0" align="middle">
          <?endif //order_image?>
        &nbsp;<?endforeach //$field?></td>
  </tr>
</table></td>
  </tr>
</table>
<?endif //$order_bar?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td height="10"></td>
  </tr>
</table>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr valign="middle">
    <td width="20%" align="left" nowrap class="pageResults"><?=$split_results_count?></td>
    <td align="right" nowrap class="pageResults"><?=$split_results?></td>
  </tr>
</table>
<?endif //results?>