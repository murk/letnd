<?
/*13-5-13
  Reserves inmo
 
 */

if (is_table("inmo__property")){
	
	$read_write = Db::get_row("SELECT read1,write1 FROM all__menu WHERE menu_id = 101");
	$read = $read_write['read1'];
	$write = $read_write['write1'];
	
	
	$update_sql .="INSERT INTO `all__menu`
			(`menu_id`, `menu_group`, `tool`, `tool_section`, `action`, `parent_id`, `toolmode_id`, `variable`, `ordre`, `link`, `process`, `read1`, `write1`) VALUES

			('150', 'inmo', 'booking', 'book', '', '100', '1', 'BOOKING_MENU_BOOK', '1', '', '', '$read', '$write'),
			('151', 'inmo', 'booking', 'book', 'list_records', '150', '1', 'BOOKING_MENU_BOOK_LIST', '1', '', '', '$read', '$write'),
			('152', 'inmo', 'booking', 'book', 'list_records', '150', '1', 'BOOKING_MENU_BOOK_LIST_PAST', '2', '', 'past_bookings', '$read', '$write'),
			('153', 'inmo', 'booking', 'book', 'list_records', '150', '1', 'BOOKING_MENU_BOOK_LIST_BIN', '3', '', '', '$read', '$write'),

			('160', 'inmo', 'booking', 'extra', '', '100', '1', 'BOOKING_MENU_EXTRA', '2', '', '', '$read', '$write'),
			('161', 'inmo', 'booking', 'extra', 'show_form_new', '160', '1', 'BOOKING_MENU_EXTRA_NEW', '1', '', '', '$read', '$write'),
			('162', 'inmo', 'booking', 'extra', 'list_records', '160', '1', 'BOOKING_MENU_EXTRA_LIST', '2', '', '', '$read', '$write'),
			('163', 'inmo', 'booking', 'extra', 'list_records', '160', '1', 'BOOKING_MENU_EXTRA_LIST_BIN', '3', '', '', '$read', '$write'),

			('170', 'inmo', 'booking', 'season', '', '100', '1', 'BOOKING_MENU_SEASON', '3', '', '', '$read', '$write'),
			('171', 'inmo', 'booking', 'season', 'show_form_new', '170', '1', 'BOOKING_MENU_SEASON_NEW', '1', '', '', '$read', '$write'),
			('172', 'inmo', 'booking', 'season', 'list_records', '170', '1', 'BOOKING_MENU_SEASON_LIST', '1', '', '', '$read', '$write');";
	
	$update_sql .="UPDATE all__menu SET ordre = '4' WHERE menu_id = 106;
				UPDATE all__menu SET ordre = '5' WHERE menu_id = 120;
				UPDATE all__menu SET ordre = '6' WHERE menu_id = 114;
				UPDATE all__menu SET ordre = '7' WHERE menu_id = 140;";
}

if (is_table("web__banner_language")){
	
	$update_sql .="ALTER TABLE `web__banner_language`
	CHANGE COLUMN `banner_text` `banner_text` TEXT NOT NULL DEFAULT '' COLLATE 'utf8_spanish2_ci' AFTER `url1_name`;";
	
}


?>