<?
/**
 * * Gestiona privilegis de l'usuari
 *
 *
 *
 */

class User {
    /**
	 * AIXO HAURIA D?ANAR PER CADA GRUP UN DEFAULT_MENU
     * Static function
     * Serveix per obtenir el pròxim DEFAULT_MENU_ID per defecte si el que som ara no tenim permisos
     *
     * @access public
     */
    static private function _set_next_default_menu (){
		// si es menu per defecte
			// primer miro si hi ha algun altre DEFAULT_MENU_ID definit al config
			$ids = explode(',',DEFAULT_MENU_ID);
			unset($ids[0]); // aquest ja sabem que no te permis

			foreach ($ids as $id){
				 $query = "SELECT menu_id
								FROM all__menu
								WHERE menu_id=" . $id . " 
								AND toolmode_id<>0
								AND read1 LIKE '%," . $_SESSION['group_id'] . ",%'
								ORDER BY ordre";
				$found_id = Db::get_first($query);
				if ($found_id) {
					$_SESSION['message'] = $GLOBALS['gl_message'];
					User::next_id($found_id);
					return;
				}
			}
	}
    /**
     * Static function
     * Serveix per saber si l'usuari te privilegis de lectura almenys,
     * i si no busca la primera secció dins el mateix mòdul on tingui acces
	 * i si no mira els següents de la llista a DEFAULT_MENU_ID
	 * i si no mostra missatge que no te permisos
     *
     * @access public
     */
    static public function is_not_allowed ()
    {
        global $gl_read, $gl_tool_top_id, $gl_menu_id;
		
        if (!$gl_read) {
		
			if (!isset($_GET['menu_id']) && !isset($_POST['menu_id']))  {
				User::_set_next_default_menu ();
			}
			
			// sino busco el pròxim amb permís en el mateix tool
            $query = "SELECT menu_id
              FROM all__menu
              WHERE parent_id=" . $gl_tool_top_id . " AND toolmode_id<>0
              AND read1 LIKE '%," . $_SESSION['group_id'] . ",%'
              ORDER BY ordre";
            $results = Db::get_rows($query);
			Debug::add('Resultats menus amb permisos nivell 1',$results);
			Debug::add('Query Resultats menus amb permisos nivell 1',$query);
            foreach ($results as $rs) {
                $query = "SELECT menu_id
              FROM all__menu
              WHERE parent_id=" . $results[0]['menu_id'] . " AND toolmode_id<>0
              AND read1 LIKE '%," . $_SESSION['group_id'] . ",%'
              ORDER BY ordre LIMIT 1";
              // canviat a MUSEUS ---> AND action = 'list_records'"; // agafava el primer fill amb list_records
			  // el problema es que feiem premisos per tot el grup de submenus, (tool_section) de la mateixa eina, però vull fer permisos per un tot sol d'una eina en concret
			  // ej. -> a la fitxa de museu vull mostrar nomès menu d'editar fitxa i prou per els museus ( nomes ho faig servir aquí), però hi ha 3 menus mes que son tool_section = museu
                $results = Db::get_rows($query);
                if ($results){
					Debug::add('Resultats menus amb permisos nivell 2', $results);
					// $gl_menu_id = $results[0]['menu_id'];
					User::next_id($results[0]['menu_id']);
					break;
				}
            }
        }
        return !$gl_read;
    }
    /**
     * Static function
     * Redirigeix al proxima secció trobada
     * Si no en troba cap dona un missatge conforme l'usuari no te privilegis en el mòdul
     *
     * @access public
     */
    static public function goto_readable()
    {
        if (User::next_id()) {
        	// si hi ha una altra secció amb permisos, mostra la següent secció
            print_javascript('window.location.href = "' . HOST_URL . 'admin/?menu_id=' . User::next_id() . '"');
            die();
        }else {
        	// si no hi ha cap mes secció amb permisos, mostra missatge de no te permis
            global $gl_message, $gl_messages, $gl_page;
            $gl_message = $gl_messages['concession_no_acces'];
			
			$gl_module = Module::init(); // inicio modul general o modul v3 creats amb classes
			$gl_module->set_tpl();// defineixo tpl
			$gl_module->set_language(); // carrego arxiu variables idioma per mostrar el títol de la secció
			
        	$gl_page->show();
			Debug::p_all();
        	die();
        }
	}

	/**
	 * Comproba si l'usuari actual té permisos d'escritura per un determinat grup de menús
	 * @param $menu_group
	 *
	 * @return boolean
	 */
	static function has_write_permission ($menu_group) {

		$group_id = $_SESSION['group_id'];
		$query = "
				SELECT 1
				FROM all__menu
				WHERE write1
				LIKE '%," . $group_id . ",%'
				AND menu_group = '" . $menu_group . "'
				LIMIT 1";

		return Db::get_first( $query );
	}
	/**
	 * Static function
	 * Mostra el formulari si no està logged
	 *
	 *
	 * @access public
	 */
	static public function check_is_logged(){
		if (!isset($_SESSION['login']) || isset($_GET['logout'])) {
			global $gl_page, $users_login_form, $tpl;
			include (DOCUMENT_ROOT . 'admin/modules/user/languages/' . LANGUAGE . '.php');
			include (DOCUMENT_ROOT . 'admin/modules/user/login.php');
			if ($GLOBALS['gl_is_admin']) {
				$gl_page->show();
				die();
			}
		}
	}
	/**
	 * Static function
	 * Crida l'arxiu on_login just desprès de fer el login
	 *
	 *
	 * @access public
	 */
	static public function on_login(){
		if (isset($_SESSION['on_login'])) include (DOCUMENT_ROOT . 'admin/modules/user/on_login.php');
	}
    /**
     * Static var
     * Id de la pròxima secció amb permís
     * Es la manera de proporcionar una propietat statica en php4
     * @access protected
     */
    static public function next_id($val = '')
    {
        static $next_id = '';
        if ($val) {
            $next_id = $val;
        }else {
            return $next_id;
        }
    }
	// per saber si l'usuari de admin està logejat
	static public function is_logged(){
		return  (isset($_SESSION['login']) && $_SESSION['user_id']);
	}
}

// Funcions directes per saber si s'està logejat en un modul desde qualsevol lloc 
// ( per no haver de carregar tot el modul nomès per això )

function is_product_logged(){
	return (isset($_SESSION['product']['customer_id']) && isset($_SESSION['product']['mail']));
}
function is_product_wholesaler(){	
	return (isset($_SESSION['product']['customer_id']) && isset($_SESSION['product']['mail']) && isset($_SESSION['product']['is_wholesaler']) && $_SESSION['product']['is_wholesaler']);
}
function get_product_logged(){
	$ret['logged_name'] = $_SESSION['product']['name'];
	$ret['logged_surname'] = $_SESSION['product']['surname'];
	return $ret;
}
function is_newsletter_logged(){
	return (isset($_SESSION['newsletter']['customer_id']) && isset($_SESSION['newsletter']['mail']));
}
function is_custumer_logged(){
	return (isset($_SESSION['custumer']['custumer_id']) && isset($_SESSION['custumer']['mail']));
}
function get_custumer_logged(){
	$ret['logged_name'] = $_SESSION['custumer']['name'];
	$ret['logged_surname'] = $_SESSION['custumer']['surname'];
	return $ret;
}
?>