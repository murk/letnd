<?

/*12-11-13
  Factures a productes
	
 Poso totes les comandes que haurien de ser factura a -1 perque no es tornin factura al guardar. Nomes es podrà fer factura de les comandes a partir d'avui
 */

if (is_table('product__configadmin') && !Db::get_first("SELECT * FROM product__configadmin WHERE name='invoice_prefix'" )) {	
	
	$update_sql[] = "
		INSERT INTO `product__configadmin` (`name`,`value`) VALUES ('invoice_prefix','');";
	
}

if (is_table('product__configadmin') && !Db::get_first("SELECT * FROM product__configadmin WHERE name='has_invoice'" )) {	
	
	$update_sql[] = "
		INSERT INTO `product__configadmin` (`name`,`value`) VALUES ('has_invoice','0');";
	
}

if (is_table('product__order')) {	
	
	$update_sql[] = "
		ALTER TABLE `product__order`
			ADD COLUMN `invoice_id` INT(11) NOT NULL DEFAULT '0' AFTER `error_description`,
			ADD COLUMN `invoice_date` DATE NOT NULL AFTER `invoice_id`,
			ADD COLUMN `invoice_refund_id` INT(11) NOT NULL DEFAULT '0' AFTER `invoice_date`,
			ADD COLUMN `invoice_refund_date` DATE NOT NULL AFTER `invoice_refund_id`;";
	
	$update_sql[] = "
		UPDATE `product__order`
			SET invoice_id=-1 WHERE order_status =  'completed' OR order_status =  'refunded' OR order_status =  'preparing' OR order_status =  'shipped' OR order_status =  'delivered' ;";
	
}



?>