<?php
$vars['var_name'] = 'comarca_id';
$tipus = isset($vars['tipus'])?$vars['tipus']:false;
$page_id = isset($vars['page_id']) ? $vars['page_id'] : false;
include_once('inmo_block_functions.php');

$condition = '';
$condition .= inmo_block_get_tipus($tipus);

$query = "SELECT Distinct comarca, inmo__comarca.comarca_id
				FROM inmo__comarca, inmo__property
				WHERE inmo__comarca.comarca_id = inmo__property.comarca_id
				AND bin = 0 AND (status = 'onsale' OR status = 'sold' OR status = 'reserved')
					" . $condition . "
				ORDER BY comarca";

$results = Db::get_rows($query);


$comarca_id = R::number('comarca_id');
$vars['block_selected'] = $comarca_id?1:0;

if ($results) {
    foreach($results as $rs) {
        ($comarca_id == $rs["comarca_id"])?$selected = 1:$selected = 0;

		/*
		  if (USE_FRIENDLY_URL) {
		  $friendly_params['comarca_id'] = $rs['comarca_id'];
		  if ($tipus) $friendly_params['tipus'] = $tipus;
		  $l['link'] = Page::get_link('inmo','property',false,false,$friendly_params,false,'',false,false);
		  } else {
		  $l['link'] = '/?tool=inmo&amp;tool_section=property&amp;comarca_id=' . $rs['comarca_id'] . '&amp;language=' . LANGUAGE;
		  }

		 */
				
		$l['link'] = get_link(array(
				'tool' => 'inmo', 
				'tool_section' => 'property', 
				'friendly_params' => array('tipus'=>$tipus==INMO_DEFAULT_TIPUS?'':$tipus, 'comarca_id'=> $rs['comarca_id']),
				'link_vars' => array('tipus'=>$tipus),
				'detect_page' => $page_id?false:true,
				'page_id' => $page_id
			));
		
        $l['selected'] = $selected;
        $l['value'] = $rs['comarca_id'];
        $l['item'] = $rs['comarca'];
		$l['class'] = '';
        $loop2[] = $l;
    }
	
	
$loop2[0]['class'] = 'first';	
$loop2[count($loop2)-1]['class'] = 'last';
	
$loop[] = array ('loop' => $loop2,
    'item' => '');
}


?>