<?

/* 10-10-13 
  Modificacions immo, certificat d’eficiencia energetica

 */

if (is_table('inmo__configadmin')) {	
	
	$update_sql = "
		INSERT INTO `inmo__configadmin` (`name`,`value`) VALUES ('default_zone','0');
		INSERT INTO `inmo__configadmin` (`name`,`value`) VALUES ('default_tipus','sell');
		INSERT INTO `inmo__configadmin` (`name`,`value`) VALUES ('is_mls_on','1');
		ALTER TABLE `inmo__property`
	CHANGE COLUMN `tipus` `tipus` ENUM('sell','rent','temp','moblat','selloption') NOT NULL DEFAULT 'sell';
	ALTER TABLE `inmo__property`
	ADD COLUMN `efficiency` ENUM('a','b','c','d','e','f','g') NOT NULL AFTER `status`";
	
}


?>