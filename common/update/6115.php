<?

/* 28-2-2017
	Enviar missatge a tots els clients sobre canvi de servidor
 */


Main::add_missatge(
	array(
			'status'          => 'send',
			'kind'            => 'letnd',
			'subject'         => 'Important canvi de servidors Letnd',
			'missatge'        => '<p><strong>Benvolgut client,</strong></p>
<p>Els pr&ograve;xims dies 1 i 2 de mar&ccedil;, Letnd substituir&agrave; els servidors on t&eacute; vost&egrave; allotjada la p&agrave;gina web i els correus electr&ograve;nics per uns servidors m&eacute;s moderns i potents. L&rsquo;objectiu d&rsquo;aquest canvi &eacute;s millorar les prestacions que ofereix aquest servidor en termes de seguretat, capacitat i velocitat de c&agrave;rrega.</p>
<p>Procurarem en tot moment que aquest canvi no afecti en cap cas el funcionament ordinari de la seva p&agrave;gina i els seus correus, per&ograve; &eacute;s possible que es produeixin errors de c&agrave;rrega o de tr&agrave;nsit en els correus durant aquest canvi. L&rsquo;informem que en cap cas es perdr&agrave; cap tipus d&rsquo;informaci&oacute;. &Eacute;s possible que, en ordinadors que no tinguin les &uacute;ltimes versions o versions actualitzades dels gestors de correu Outlook o Windows Live Mail, els correus es rebin per duplicat. Per aquest motiu, s&iacute; que recomanem que es buidi la carpeta del correu &ldquo;Bandeja de Entrada&rdquo; i els correus es guardin sota un altre nom.</p>
<p>Per a qualsevol informaci&oacute; addicional o per notificar qualsevol incid&egrave;ncia, tot l&rsquo;equip Letnd resta a la seva completa disposici&oacute;.<br /><br /><br /></p>
<p><strong>Apreciado cliente,</strong></p>
<p>Los pr&oacute;ximos d&iacute;as 1 y 2 de marzo, Letnd sustituir&aacute; los servidores donde tiene usted alojada la p&aacute;gina web y los correos electr&oacute;nicos por unos servidores m&aacute;s modernos y potentes. El objetivo de este cambio es mejorar las prestaciones que ofrece este servidor en t&eacute;rminos de seguridad, capacidad y velocidad de carga.</p>
<p>Procuraremos en todo momento que este cambio no afecte en ning&uacute;n caso al funcionamiento ordinario de su p&aacute;gina y de sus correos, pero es posible que se produzcan errores de carga o de tr&aacute;nsito en los correos durante este cambio. Le informamos que en ning&uacute;n caso se perder&aacute; ning&uacute;n tipo de informaci&oacute;n. Es posible que, en ordenadores que no tengan las &uacute;ltimas versiones o versiones actualizadas de los gestores de correo Outlook o Windows Live Mail, los correos se reciban por duplicado. Por este motivo, s&iacute; que recomendamos que se vac&iacute;e la carpeta del correo &ldquo;Bandeja de Entrada&rdquo; y los correos se guarden bajo otro nombre.</p>
<p>Para cualquier informaci&oacute;n adicional o para notificar cualquier incidencia, todo el equipo Letnd resta a su completa disposici&oacute;n.</p>
<p>Cordialment,</p>
<p>Joan Monj&eacute; Cano<br />Responsable de Posicionament Web Letnd</p>',
			'link'            => '',
			'link_text'       => '',
		));

?>