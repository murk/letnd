<?
/**
 * InmoImportxml
 *
 * http://w.finquescatalonia/admin/?tool=inmo&tool_section=importxml&action=import
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2011
 * @version $Id$
 * @access public
 */
/*
FEATURES

   Nomes seveix per importar el primer cop, no actualitza
   Nomes s'ha fet per finques catalonia
 *


 PROPERTY


PER FER


CANVIS EN BBDD

*/

class InmoImportxml extends Module {
	var $conta = 0;
	var $inmofactory_table = 0;
	var $letnd_id = 0;
	var $demand_id = 0;
	var $demand_tipus = '';
	var $propertys_added = array();
	var $errors = array();
	// el key es l'id a inmofactory, i el val l'id a letnd
	var $import_categorys = array(); // nomes quan es programa per poder veure totes les categories que s'importen
	var $view_results = array(); // nomes quan es programa per poder veure el diferents valors dels camps
	var $view_results_2 = array(); // nomes quan es programa per poder veure el diferents valors dels camps repetits inclus
	var $categorys = Array
	(
		'Piso'                => 4,
		'Local comercial'     => 7,
		'Adosada'             => 22,
		'Casa'                => 3,
		'Garaje'              => 26,
		'Atico'               => 14,
		'Apartamento'         => 12,
		'Duplex'              => 15,
		'Estudio'             => 12,
		'Parking'             => 10,
		'Terreno urbanizable' => 5,
		'Terreno urbano'      => 5,
		'Casa con terreno'    => 24,
		'Atico Duplex'        => 15,
		'Planta baja'         => 25,
		'Masia'               => 1,
		'Chalet'              => 13,
		'Piso Tipo Duplex'    => 15,
		'Triplex'             => 15,
	);
	var $inmofactory_status = array(
		'Disponible'    => 'onsale',
		'No disponible' => 'archived'
	);

	var $tipus = array(
		'Vender'             => 'sell',
		'Alquilar'           => 'rent',
		'Temporada Alquiler' => 'temp',
		'Traspasar'          => 'rent',
		'Vender o Alquilar'  => 'sell'
	);

	var $facing = array(
		'Norte'      => 'n',
		'Sur'        => 's',
		'Este'       => 'e',
		'Oeste'      => 'o',
		'Noroeste'   => 'no',
		'Suroeste'   => 'so',
		'Este Oeste' => 'e',
		'Sureste'    => 'se',
		'Norte Sur'  => 's',
		'Noreste'    => 'ne'
	);
	var $inmofactory_efficiency = array(
		'En trámite' => 'progress',
		'A'          => 'a',
		'B'          => 'b',
		'C'          => 'c',
		'D'          => 'd',
		'E'          => 'e',
		'F'          => 'f',
		'G'          => 'g',
	);

	function __construct() {
		parent::__construct();

		// inicio output
		echo "
		<html>
		<head>
		<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
		<style>
			.m1{
				font-size:20px;
				font-weight: bold;
				margin: 20px 0 10px 0;
			}
			.m2{				
				font-size:12px;
				margin:5px 0 0 20px;
				font-weight: bold;
				color:#484848;
			}
			.m2 i, .m1 i{	
				font-weight: normal;
				color:#706f6f;
			}
			.m3{				
				font-size:11px;
				margin:0 0 0 30px;
				color:#484848;
			}
			.m4{				
				font-size:11px;
				margin:0 0 0 40px;
				color:#484848;
			}
			.merror{				
				font-size:20px;
				margin:0 0 0 0;
				color:red;
			}
		</style>
		<script>
			/*	
			var a = true;
			function moveWin(){
				window.scrollTo(0, document.body.scrollHeight);
				if (a) t = setTimeout('moveWin();',100);
			}*/			
		</script>
		</head>
		<body onload=''>
		<script>
			moveWin();		
		</script>";

	}

	function show_end() {
		$this->p( 'Importació finalitzada  - <i>' . $this->conta . ' operacions en ' . $execution_time = Debug::get_execution_time() . ' segons </i>', 1 );
		echo "
		<script>
			a = false;	
		</script></body></html>";
	}

	function import() {
		set_time_limit( 0 );        //ini_set('implicit_flush', true);		//error_reporting(E_WARNING);
		// $this->import_field_names();

		$this->import_data();

		$this->add_languages(); // idiomes que falten
		//$this->insert_remove_keys();

		$this->show_end();
		//send_mail_admintotal('Actualitzacio habitatsoft','Actualització finalitzada');
		die();
	}

	// nomes el primer cop per poder tindre els camps de l'excel en la bbdd
	function import_field_names() {

		$z    = new XMLReader;
		$file = "E:\\Dropbox\\Projectes web\\Finques Catalonia\\material\\importar.xml";

		$z->open( $file );

		$doc = new DOMDocument;

		// move to the first <product /> node
		while ( $z->read() && $z->name !== 'propiedad' ) {
			;
		}

		while ( $z->name === 'propiedad' ) {
			$node = simplexml_import_dom( $doc->importNode( $z->expand(), true ) );
			$this->p( '', 2 );

			foreach ( $node as $k => $v ) {
				$this->p( $k, 3 );
				$this->p( $v, 4 );


				$query = "INSERT INTO `letnd`.`export__importxml` 
						(`importxml_field`) 
					VALUES 
						('$k');";

				Db::execute( $query );


			}
			break;
			$z->next( 'propiedad' );
		}


	}

	function import_data() {

		$z    = new XMLReader;
		$file = "E:\\Dropbox\\Projectes web\\Finques Catalonia\\material\\importar.xml";

		$z->open( $file );

		$doc = new DOMDocument;

		// move to the first <product /> node
		while ( $z->read() && $z->name !== 'propiedad' ) {
			;
		}

		$this->p( 'Processant arxiu: ' . $file, 1 );

		$conta = 0;
		while ( $z->name === 'propiedad' ) {

			$node = simplexml_import_dom( $doc->importNode( $z->expand(), true ) );

			//Debug::p($column_names, '$column_names');
			//Debug::p($keys, '$keys');
			// inicialitzo variables per cada registre
			$this->p( 'Registre ' . $conta, 2 );
			$row_imported      = array();
			$this->letnd_id   = 0;

			// noms columnes
			if ($conta==0){
				Db::connect_mother();
				$node2 = $node;
				foreach ($node2 as $field=>$key){

					// busco el nom de la taula i camp a la bbdd de relacions
					$query = "SELECT importxml_field, letnd_table, letnd_field, function
						FROM export__importxml
						WHERE letnd_table <>  ''
						AND importxml_field= '". $field ."'";
					$rs = Db::get_row($query);

					// nomès faig algo quan està definida 'letnd_table', sino passo del camp
					if ($rs) {
						$column_names[$field]= $rs;
					}
				}
				Db::reconnect();
				// debug::p($column_names);
			}

			foreach ( $node as $k => $field ) {


				$field = (string) $field;

				// si tinc definit letnd_table i letnd_field pèr aquest camp el guardo, sino passo
				// un camp pot tindre una funcio definida, com ara get_boolean que canvia -1 per 1

				if ( isset( $column_names[ $k ] ) ) {
					if ( $column_names[ $k ]['function'] ) {

						$field = $this->$column_names[$k]['function']($field,$node,$column_names,$row_imported);


						// $field = call_user_func( array( &$this, $column_names[ $k ]['function'] ), $field );
					}

					$row_imported[ $column_names[ $k ]['letnd_table'] ][ $k ] = $field;
				}

			}

			//  Debug::p( $row_imported, 'row imported' );

			// En acabar de mirar totes les dades del registre faig els inserts que calguin per aquest registre
			// Aquí es crida una funció per tot el grup de camps que van a una taula mateixa taula de letnd, ej. tots els camps que van a inmo__property criden la funcio inmo__property
			foreach ( $row_imported as $letnd_table => $row ) {
				//call_user_func_array (array(&$this, $letnd_table),array(&$row,&$column_names));
				$this->$letnd_table( $row, $column_names );
			}
			//debug::p($row_imported, 'Rows_arr');
			// PROVES if ($conta==10) break;


			$conta ++;
			$z->next( 'propiedad' );
		}

		// print_r( $this->import_categorys );

		if ($this->view_results) {
			Debug::p( $this->view_results, 'View results' );
			Debug::p( $this->view_results2, 'View results 2' );
		}

		// inicialitzo variables de cada Taula (TXT)
		$column_names = array();
		$keys         = array();
	}

	// giro el row per poder insertar directament a bbdd
	function get_letnd_row( &$row, &$column_names ) {
		$new_row = array();
		foreach ( $row as $key => $value ) {
			$new_row[ $column_names[ $key ]['letnd_field'] ] = $value;
		}

		return $new_row;
	}
	/*
	 * 
	 * 
	 *  FUNCIONS PER CAMP
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 */

	// nomes quan es programa per veure el diferents valors dels camps
	function view_results($val, &$data, &$column_names, &$row_imported) {
		if ($val) {
			$this->view_results[ $val ] = $val;
			$this->view_results2[] = $val;
		}
	}
	function get_land($val, &$data, &$column_names, &$row_imported) {

		if ($data->tipo_ofer == 'Terreno urbanizable'){
			$row_imported['inmo__property']['m_cons'] = $val;
		}

		return $val;
	}
	function get_price($val, &$data, &$column_names, &$row_imported) {

		$preu_venta = (int) $data->precioinmo;

		// si te 2 agafo preu venta
		if ($preu_venta) $val = $preu_venta;

		return $val;
	}
	function get_piscina($val, &$data, &$column_names, &$row_imported) {

		$piscina2 = (int) $data->piscina_prop;

		// si te 2 agafo preu venta
		if ($piscina2 == '1') $val = $piscina2;

		return $val;
	}
	function get_room($val, &$data, &$column_names, &$row_imported) {

		$total = (int) $data->habdobles + (int) $val;
		// echo( $data->habdobles . ' - ' . $val . ' - ' . $data->habitaciones . ' - Total: ' . $total);

		return $total;
	}
	function get_latitude($val, &$data, &$column_names, &$row_imported) {

		// columna nomes a letnd
		$row_imported['inmo__property']['center_latitude'] = $val;
		$column_names['center_latitude']['letnd_field'] = 'center_latitude';

		return $val;
	}
	function get_longitude($val, &$data, &$column_names, &$row_imported) {

		// columna nomes a letnd
		$row_imported['inmo__property']['center_longitude'] = $val;
		$column_names['center_longitude']['letnd_field'] = 'center_longitude';

		return $val;
	}
	function get_efficiency($val, &$data, &$column_names, &$row_imported) {

		// columna nomes a letnd
		$row_imported['inmo__property']['efficiency2'] = $val;
		$column_names['efficiency2']['letnd_field'] = 'efficiency2';

		return $val;
	}
	function get_tipus2($val){
		// si no està dins l'array, retorno per defecte el primer
		return $val=='Obra Nueva'?'new':'second';
	}
	function get_text($val){
		// si no està dins l'array, retorno per defecte el primer
		return trim ($val);
	}

	// inmo__property -> get_category_id
	function get_category( $val ) {

		// si no està dins l'array, retorno per defecte el primer
		$var = 'categoria';
		if ( isset( $this->categorys[ $val ] ) ) {
			$ret = $this->categorys[ $val ];
		} else {
			$ret =  reset ($this->categorys);
			$this->p( "No s'ha trobat cap " . $var . " per: " . $val, 'error' );
		}

		$this->import_categorys [$val]= $val;

		return $ret;
	}
	function get_tipus( $val ) {

		// si no està dins l'array, retorno per defecte el primer
		$var = 'tipus';
		if ( isset( $this->tipus[ $val ] ) ) {
			$ret = $this->tipus[ $val ];
		} else {
			$ret =  reset ($this->tipus);
			$this->p( "No s'ha trobat cap " . $var . " per: " . $val, 'error' );
		}

		return $ret;
	}

	function get_facing( $val ) {

		$var = 'facing';
		$v   = &$this->facing;
		if ( isset( $v[ $val ] ) ) {
			$ret = $v[ $val ];
		} else {
			$ret = '';
			if ( $val ) {
				$this->p( "No s'ha trobat cap " . $var . " per: " . $val, 'error' );
			}
		}

		return $ret;

	}

	// dates
	function format_datetime( $val, &$data, &$column_names ) {
		// si no està dins l'array, retorno per defecte el primer
		$date = format_datetime( $val ) . ':00';
		$date = unformat_datetime( $date );

		return $date;
	}

	// dates
	function format_date( $val ) {
		// si no està dins l'array, retorno per defecte el primer

		if ( strpos( $val, '-' ) === false ) {
			$val = unformat_date( $val, false );
		} else {
			$arr = explode( '-', $val );
			$val = $arr[2] . '/' . $arr[0] . '/' . $arr[1];
		}

		return $val;
	}

	function get_municipi_id( $val, &$data, &$column_names ) {

		$val = trim( $val );
		if ( ! $val ) {
			return;
		}

		// Manualment
		if ($val == 'Platja d`Aro')
			$val = 'Platja d\'Aro';
		if ($val == 'S`Agaró Vell')
			$val = 'Agaró, s\'';
		if ($val == 'Castell-Platja d`Aro y S`Agaró')
			$val = 'Castell - Platja d\'Aro';


		$query = "SELECT municipi_id 
						FROM `inmo__municipi` 
						WHERE municipi = " . Db::qstr( $val );

		$municipi_id = Db::get_first( $query );

		/*
		if ($municipi_id=='1'){
			$this->p('Error: ' . $municipi_id,'error');
			die($query );
		}
		 * 
		 */

		if ( ! $municipi_id ) {
			$this->p( "No s'ha trobat cap municipi per: " . $val, 'error' );
			$municipi_id = 0;
		}

		return $municipi_id;


	}

	/*
	 * 
	 * 
	 *  FUNCIONS PER TAULA
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 */

	function inmo__property( &$row, &$column_names ) {
		$this->p( 'TAULA inmo__property ', 3 );

		$new_row = $this->get_letnd_row( $row, $column_names );
		// afegeixo camps nomès de letnd
		$new_row['land_units'] = 'm2';
		$new_row['status'] = 'onsale';

		$this->get_comarca_id( $new_row );


		// El primer cop que faig una exportacio, actualitza les propietats amb la 'ref' = 'Expediente', per mantindre les propietats que ja eren a la web ( si s'ha tingut en compte de posar les mateixes referencies
		$query       = "SELECT property_id FROM inmo__property 
					WHERE property_id= " . $new_row['property_id'];
		$existing_id = Db::get_first( $query );

		// ACTUALITZO O INSERTO
		// si existeix la ref, deixo el property_id actual de letnd per poder utilitzar els mateixos links
		if ( $existing_id ) {
			$query              = $this->get_update( $new_row, 'inmo__property', 'property_id = ' . $existing_id );
			$this->letnd_id = $existing_id;
		} // si es un registre nou poso com a property_id el Expediente de habitatsoft ja que es un nombre únic
		else {
			$this->letnd_id = $new_row['property_id'];
			$query              = $this->get_insert( $new_row, 'inmo__property' );
		}

		Db::execute( $query );
		$this->p( $query, 4 );

		if ( $this->letnd_id ) {
			$this->propertys_added[] = $this->letnd_id;
		} // compte global de les ids creades o updatejades
	}

	function inmo__property_language( &$row, &$column_names ) {
		$language = 'spa';


		// $col_name = $column_names['Id de Inmueble']['key'];

		$new_row['property_id'] = $this->letnd_id;

		$this->common_insert_all( $row, $column_names, 'inmo__property_language', $new_row, $language );
	}


	// funció comú per registres que primer es borra tot i desprès s'inserta tot sense complicacions d'idiomes ni res
	function common_insert_all( &$row, &$column_names, $table, $other_rows = array(), $language = false ) {
		$this->p( 'TAULA ' . $table, 3 );
		$new_row = $this->get_letnd_row( $row, $column_names );

		if ( $other_rows ) {
			$new_row = array_merge( $new_row, $other_rows );
		}

		//$new_row['property_id'] = $this->letnd_id;

		if ( $language ) {
			$new_row['language'] = $language;
		} // per inmo__property_language

		$query = $this->get_insert( $new_row, $table );
		Db::execute( $query );
		$this->p( $query, 4 );
	}


	/*
	 * 
	 * 
	 * 
	 * 
	 * ALTRES
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 */

	function get_comarca_id( &$new_row ) {

		$municipi_id = $new_row['municipi_id'];

		if ( ! $municipi_id ) {
			return;
		}

		$comarca_id   = Db::get_first( "SELECT comarca_id FROM inmo__municipi WHERE municipi_id = " . $municipi_id );
		$provincia_id = Db::get_first( "SELECT provincia_id FROM inmo__comarca WHERE comarca_id = " . $comarca_id );

		$new_row['comarca_id']   = $comarca_id;
		$new_row['provincia_id'] = $provincia_id;
	}


	function add_languages() {

		// nomes easy brava
		// per funcionar hauriem de comprobar quins idiomes falten realment
		//  WHERE property_id > 100 nomes finques catalonia, els importats son tots majors de 100

		$this->p( 'Afegir idiomes restants', 1 );

		$query = " INSERT INTO inmo__property_language (property_id, language) SELECT DISTINCT property_id, 'cat' FROM inmo__property_language WHERE property_id > 100;";
		Db::execute( $query );
		$this->p( $query, 2 );

		$query = " INSERT INTO inmo__property_language (property_id, language) SELECT DISTINCT property_id, 'eng' FROM inmo__property_language WHERE property_id > 100;";
		Db::execute( $query );
		$this->p( $query, 3 );

		$query = " INSERT INTO inmo__property_language (property_id, language) SELECT DISTINCT property_id, 'fra' FROM inmo__property_language WHERE property_id > 100;";
		Db::execute( $query );
		$this->p( $query, 3 );

		/*
		$query = " INSERT INTO inmo__property_language (property_id, language) SELECT DISTINCT property_id, 'deu' FROM inmo__property_language;";
		Db::execute( $query );
		$this->p( $query, 3 );
		*/

	}


	function get_insert( &$row, $table ) {
		$fields = '';
		$values = '';

		foreach ( $row as $key => $r ) {
			$fields .= $key . '`,`';
			$values .= $this->get_str( $r ) . ',';
		}
		$fields = substr( $fields, 0, - 2 );
		$values = substr( $values, 0, - 1 );

		$query = "INSERT INTO " . $table . " 
					(`" . $fields . ") 
					VALUES (" . $values . ")";

		return $query;
	}

	function get_update( &$row, $table, $condition ) {
		$values = '';

		foreach ( $row as $key => $r ) {
			$values .= $key . '=' . $this->get_str( $r ) . ',';
		}
		$values = substr( $values, 0, - 1 );

		$query = "UPDATE " . $table . " 
			SET " . $values . "
			WHERE " .
		         $condition;

		return $query;
	}

	function get_str( $str ) {
		return "'" . mysqli_real_escape_string( Db::cn(), $str ) . "'";
	}

	function p( $txt, $level ) {

		if ( $level == 2 ) {
			$this->conta ++;
			$txt = $this->conta . ' ' . $txt . ' - <i>(' . $execution_time = Debug::get_execution_time() . ' s.) </i>';
		}
		if ( $level == 'error' ) {
			$txt .= ' (error)';
		}
		echo '<p class="m' . $level . '">' . $txt . '</p>';
		//@ob_flush();
		flush();
	}

}

?>