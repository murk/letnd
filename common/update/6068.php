<?

/* 1-3-16

	INMO
	- Marcar inmobles nous
 */

if ( is_table( 'inmo__configpublic' ) && ! Db::get_first( "SELECT * FROM inmo__configpublic WHERE name='recent_time'" ) ) {
	$update_sql[] = "
		INSERT INTO inmo__configpublic (`name`, `value`) VALUES ('recent_time', '30');";

} else {
	if ( is_table( 'inmo__configpublic' ) ) {
		trigger_error( 'Ja existeixen el camp recent_time' );
	}
}


?>