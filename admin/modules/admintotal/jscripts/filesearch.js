var filesearch = {
	auto_complete_results: [],
	editor: false,
	default_server_dir_message: '',
	server_dir: false,
	local_file: false,
	file_relative_path: false,
	init: function () {
		$('#client_add').autocomplete({
			lookup: filesearch.auto_complete_results,
			minLength: 0,
			onSelect: function (suggestion) {
				$('#client_id').val(suggestion.data);
				$('#client').val(suggestion.value);
			}
		});
		filesearch.default_server_dir_message = $('#server_dir').html();
	},
	load_file: function (file_path, domain, file_relative_path, client_id, obj) {

		$('#help').hide();
		filesearch.server_dir = false;
		filesearch.local_file = false;
		filesearch.file_relative_path = file_relative_path;
		$('.file_name').removeClass('active1');
		$(obj).addClass('active1');

		$('#file_holder').show();
		$('#file_path').html(domain + ' - <span>' + file_path + '</span>');

		if (filesearch.editor) {
			//filesearch.editor.setSize('0', '100%');
			filesearch.editor.setValue('');
		}

		filesearch.get_file_ajax(file_path, domain, client_id);
		// filesearch.check_server_dir(client_id);
	},
	/*	check_server_dir: function (client_id) {

	 var server_dir_tag = $('#server_dir');

	 server_dir_tag.html(filesearch.default_server_dir_message);
	 server_dir_tag.removeClass('error');

	 $.ajax({
	 url: '/admin/?action=check_server_dir&menu_id=1035',
	 data: 'client_id=' + client_id,
	 dataType: 'json',
	 type: 'POST'
	 })
	 .done(function (data) {
	 if (data.error) {
	 server_dir_tag.html('<span>' + data.error + ' - </span>' + data.server_dir);
	 server_dir_tag.addClass('error');
	 }
	 else {
	 server_dir_tag.html('<span>' + data.message + ' - </span>' + data.server_dir);
	 filesearch.server_dir = data.server_dir;
	 }
	 });
	 },*/
	get_file_ajax: function (file_path, domain, client_id) {

		var server_dir_tag = $('#server_dir');
		server_dir_tag.html(filesearch.default_server_dir_message);
		server_dir_tag.removeClass('error');

		filesearch.local_file = file_path;
		$.ajax({
			url: '/admin/?action=get_file&menu_id=1035',
			data: {
				client_id: client_id,
				domain: domain,
				local_file: filesearch.local_file,
				file_relative_path: filesearch.file_relative_path
			},
			dataType: 'json',
			type: 'POST'
		})
			.done(function (data) {

				if (data.error) {
					server_dir_tag.html('<div>' + data.error + ' - ' + data.server_dir + '</div>');
					server_dir_tag.addClass('error');
					return;
				}
				else {
					server_dir_tag.html( data.message + '<div>' + data.server_dir_message + ' - <strong>' + data.server_dir + '</strong></div>');
					filesearch.server_dir = data.server_dir;
				}
				if (data.html !== false) {

					if (!filesearch.editor) {

						filesearch.editor = CodeMirror(document.getElementById("file"), {
							/*matchBrackets: true,*/
							lineNumbers: true,
							styleActiveLine: true, /* Addon */
                     mode: data.mime,
							indentUnit: 4,
							indentWithTabs: true,
							lineWrapping: true,
							enterMode: 'keep',
							keyMap: 'sublime',
							theme: 'solarized light',
							tabMode: 'shift',
							viewportMargin: Infinity,
                     matchTags: {bothTags: true},
                     foldGutter: true,
                     extraKeys: {"Ctrl-J": "toMatchingTag","Ctrl-.": function(cm){ cm.foldCode(cm.getCursor());}},
							gutters: ["CodeMirror-lint-markers", "CodeMirror-linenumbers", "CodeMirror-foldgutter"],
							onCursorActivity: function () {
								filesearch.editor.addLineClass(hlLine, null);
								hlLine = filesearch.editor.addLineClass(filesearch.editor.getCursor().line, "CodeMirror-activeline-background");
							}
						});
						CodeMirror.commands.save = function (e) {
							filesearch.save_file(domain);
							return false;
						};
						emmetCodeMirror(filesearch.editor, {
							'Ctrl-Alt-W': 'emmet.wrap_with_abbreviation',
							'Tab': 'emmet.expand_abbreviation_with_tab'
						});

						Mousetrap.bind('ctrl+s', function (e) {
							filesearch.save_file(domain);
							return false;
						});

					}
					filesearch.editor.setValue(data.html);
					filesearch.editor.focus();
					filesearch.editor.setCursor({line: 3});
					filesearch.editor.setSize('100%', '100%');

				}
			});
	},
	save_file: function (domain) {

		if (!filesearch.server_dir || !filesearch.local_file) {
			var message = 'Encara no es pot guardar, encara s\'està descarregant l\'arxiu del servidor';
			showPopWin("", 300, 100, null, true, false, message);
			return;
		}

		showPopWin("", 300, 100, null, false, false, 'Guardant <i class="fa fa-spinner fa-pulse fa-fw"></i>');

		$.ajax({
			url: '/admin/?action=save_file&menu_id=1035',
			data: {
				file_content: filesearch.editor.getValue(),
				domain: domain,
				local_file: filesearch.local_file,
				server_dir: filesearch.server_dir,
				file_relative_path: filesearch.file_relative_path
			},
			dataType: 'json',
			type: 'POST'
		})
			.done(function (data) {
				if (data.message) {
					hidePopWin();
					// showPopWin("", 300, 100, null, true, false, data.message);
					var server_dir_tag = $('#server_dir');
					server_dir_tag.append(data.message);
				}
			});
	}
};