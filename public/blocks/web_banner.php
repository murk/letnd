<?php
$place_id = isset($vars['place_id'])?$vars['place_id']:'1'; // l'agafa de la taula all__block -> vars 
$limit = isset($vars['limit'])?$vars['limit']:0; 
$random = isset($vars['random'])?$vars['random']:0; 
$minimum_banners = isset($vars['minimum_banners'])?$vars['minimum_banners']:0; // l'agafa de la taula all__block -> vars 
$get_all_files = isset($vars['get_all_files'])?$vars['get_all_files']:false; // l'agafa de la taula all__block -> vars

// Banners
		$module = Module::load('web','banner', '', false); // així no necessito el web_banner.php

		$module->set_field( 'banner_text', 'type', $module->config['banner_text_type'] );
		$module->set_field( 'banner_text2', 'type', $module->config['banner_text2_type'] );

		if ( $module->config['show_banner_text2'] == '0' ) {
			$module->unset_field( 'banner_text2' );
		}


		$listing = new ListRecords($module);
		$listing->set_field('url1_target','type','text');
		$listing->use_no_template = true;
		
		$listing->order_by = 'ordre ASC, banner_id';
		if ($random) $listing->order_by = 'RAND()';
		if ($limit) $listing->limit = $limit;
		
		$listing->name = 'Block';
		$listing->paginate = false;
		$listing->condition = "place_id = '" . $place_id . "' AND status='public'";
		
		$listing->set_records();
		if ($GLOBALS['gl_is_local']) $query = $listing->get_query()['q'];

		$conta = 0;
		$loop_count = count($listing->loop);
		
		foreach ($listing->loop as $key=>$val){
			$rs = &$listing->loop[$key];
			if ($rs['files']){
			debug::add('banners',$rs['files'] );
				$rs['banner_src'] = $rs['files'][0]['file_src'];
				$size = getimagesize(DOCUMENT_ROOT . $rs['files'][0]['file_src_real']);
				$rs['banner_src_real']= $rs['files'][0]['file_src_real'];
				$rs['banner_width'] = $size[0];
				$rs['banner_height'] = $size[1];
			}
			else{
				$rs['banner_src'] = '';
			}			
			if ($get_all_files) $rs['banner_files'] = $rs['files'];
			unset ($rs['files']);
			$rs['class'] = '';
			$rs['conta'] = $conta++;
			$rs['loop_count'] = $loop_count;
		}
		$loop = $listing->loop;
		if ($loop){
			$loop[0]['class'] = 'first';	
			$loop[count($loop)-1]['class'] = 'last';
		}


if (!$loop){
	$show_block = false;
}
// acabo d'emplenar el loop per que quadrin els banner, sempre amb multiples -> 2x, 3x, 4x fins que tingui el número correcte
// hi ha sliders com a clubmotoshop que necessiten un mínim de 6 imatges
if ($loop_count<=$minimum_banners && $loop_count!=0){
	$initial_loop_count = $loop_count;
	$initial_loop = $loop;
	while ($loop_count<$minimum_banners){
		$loop_count = $loop_count+$initial_loop_count;
		
		// canvio el loop count del que ja tenia dins el loop
		foreach($loop as $key=>$val){
			// canvio el loop count
			$loop[$key]['loop_count'] = $loop_count;
		}
		// sumo un loop inicial al loop
		foreach($initial_loop as $key=>$val){
			// canvio el loop count
			$loop[$conta] = $loop[$key];
			$loop[$conta]['conta'] = $conta++;
		}
	}
}

?>