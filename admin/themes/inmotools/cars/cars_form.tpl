<script language="JavaScript" src="/admin/jscripts/validacio.js?v=<?=$version?>"></script>
<script language="JavaScript" src="/admin/jscripts/euro_calc.js?v=<?=$version?>"></script>
<script type="text/javascript">
$(function() {
	set_focus(document.theForm);
});
</script>
<form name="form_delete" method="post" action="<?=$link_action_delete?>" target="save_frame">
<input name="<?=$id_field?>" type="hidden" value="<?=$id?>">
</form><form name="theForm" method="post" action="<?=$link_action?>" onsubmit="<?=$js_string?>" target="save_frame">
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="form"> 
  <tr>
    <td>
        <?if ($form_new):?>
        <div id="form_buttons_holder"><table id="form_buttons">
          <tr> 
            <td class="buttons_top"><div class="right"><input type="submit" name="Submit" value="<?=$c_send_new?>" class="btn_form1"></div></td>
          </tr>
        </table></div>
        <?endif //form_new?><?if ($form_edit):?>
            <div id="form_buttons_holder"><table id="form_buttons">
              <tr>
                <td valign="bottom"><div class="menu"><div><?=$c_edit_data?></div><?if ($has_images):?>
            	<a href="<?=$image_link?>"><?=$c_edit_images?></a>
         <?endif //has_images?></div>					 
					 </td>
                <td align="right" class="buttons_top"><div class="right"><input type="submit" name="Submit" value="<?=$c_send_edit?>" class="btn_form1" /><a class="btn_form_delete" href="javascript:submit_form('<?=$c_confirm_form_bin?>', true);">
                  <?=$c_delete?>
                  </a><a class="btn_form1" href="<?=$preview_link?>" target="_blank">
                  <?=$c_preview?>
                  </a></div></td>
              </tr>
          </table></div>
          <?endif //form_edit?> 
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr> 
            <td><table width="100%" cellpadding="4" cellspacing="0">
                <tr> 
                  <td class="formCaptionTitle"><strong>
                  <?=$c_data?>&nbsp;</strong></td>
                </tr>
              </table>
              <table width="100%" border="0" cellpadding="0" cellspacing="0" class="formstbl">
                <tr>
                  <td><table width="100%" cellpadding="4" cellspacing="0">
                    <tr>
                      <td width="150" class="formsCaption"><strong>
                        <?=$c_ref?>:</strong></td>
                      <td class="forms"><?=$ref?></td>
                      <td width="150" class="formsCaption"><strong>
                        <?=$c_ref_supplier?>:</strong></td>
                      <td class="forms"><?=$ref_supplier?></td>
                    </tr>
                  </table>
<table width="100%" cellpadding="4" cellspacing="0">
                    <tr>
                      <td width="150" class="formsCaption"><strong>
                        <?=$c_product_private?>
                      </strong></td>
                      <td class="forms"><input name="cars_id[<?=$cars_id?>]" type="hidden" value="<?=$cars_id?>" />
                          <input name="cars_id_javascript" type="hidden" value="<?=$cars_id?>" />
                          <?=$cars_private?></td>
                    </tr>
                    <tr>
                      <td valign="top" class="formsCaption"><strong>
                        <?=$c_observations?>: </strong></td>
                      <td class="forms"><?=$observations?></td>
                    </tr>
                  </table>
                    <table width="100%" cellpadding="0" cellspacing="0">
                      <tr>
                        <td height="1" class="formsCaptionHor"><img src="/admin/themes/default/images/spacer.gif" width="1" height="1" /></td>
                      </tr>
                      <tr> </tr>
                  </table>
                    <table width="100%" cellpadding="4" cellspacing="0">
                      <tr>
                        <td width="150" height="1" valign="top" class="formsCaptionHor"><?=$c_data_product?>: </td>
                        <td height="1" class="formsCaptionHor"><img src="/admin/themes/default/images/spacer.gif" width="1" height="1" /></td>
                      </tr>
                      <tr>
                        <td valign="top" class="formsCaption"><strong>
                          <?=$c_family_id?>:</strong></td>
                        <td class="forms"><?=$family_id?></td>
                      </tr>
                      <tr>
                        <td class="formsCaption"><strong>
                          <?=$c_pvp?>:</strong></td>
                        <td class="forms"><?=$pvp?>
                          &nbsp;&euro;&nbsp;&nbsp;
                          <?=$price_consult?>
                          &nbsp;
                          <?=$c_price_consult?></td>
                      </tr>
                      <tr>
                        <td class="formsCaption"><strong>
                          <?=$c_pvd?>:</strong></td>
                        <td class="forms"><?=$pvd?>
                          &nbsp;&euro;</td>
                      </tr>
                      <tr>
                        <td class="formsCaption"><strong>
                          <?=$c_tax_id?>:</strong></td>
                        <td class="forms"><?=$tax_id?></td>
                      </tr>
                      <tr>
                        <td class="formsCaption"><strong><strong>
                          <?=$c_brand_id?>
                        </strong> </strong>:</td>
                        <td class="forms"><?=$brand_id?></td>
                      </tr>
                      <tr>
                        <td class="formsCaption"><strong><strong>
                          <?=$c_model_id?>
                        </strong> </strong>:</td>
                        <td class="forms"><?=$model_id?></td>
                      </tr>
                      <tr>
                        <td class="formsCaption"><strong><strong>
                          <?=$c_guarantee?>
                        </strong> </strong>:</td>
                        <td class="forms"><?=$guarantee?>
                            <?=$guarantee_period?></td>
                      </tr>
                      <tr>
                        <td class="formsCaption"><strong><strong>
                          <?=$c_status?>
                        </strong> </strong>:</td>
                        <td class="forms"><?=$status?></td>
                      </tr>
                    </table>
<table width="100%" cellpadding="4" cellspacing="0">
              <tr>
                <td width="150" height="1" valign="top" class="formsCaptionHor"><?=$c_properties_product?>: </td>
                <td height="1" class="formsCaptionHor"><img src="/admin/themes/default/images/spacer.gif" width="1" height="1" /></td>
              </tr>
              <tr>
                <td valign="top" class="formsCaption"><strong>
                  <?=$c_fuel?>:</strong></td>
                <td class="forms"><?=$fuel?></td>
              </tr>
              <tr>
                <td class="formsCaption"><strong>
                  <?=$c_km?>:</strong></td>
                <td class="forms"><?=$km?></td>
              </tr>
              <tr>
                <td class="formsCaption"><strong>
                  <?=$c_kv?>:</strong></td>
                <td class="forms"><?=$kv?></td>
              </tr>
              <tr>
                <td class="formsCaption"><strong>
                  <?=$c_cc?>:</strong></td>
                <td class="forms"><?=$cc?></td>
              </tr>
              <tr>
                <td class="formsCaption"><strong><strong>
                  <?=$c_color?>
                  </strong> </strong>:</td>
                <td class="forms"><?=$color?></td>
              </tr>
              <tr>
                <td class="formsCaption"><strong><strong>
                  <?=$c_year?>
                  </strong> </strong>:</td>
                <td class="forms"><?=$year?></td>
              </tr>
            </table></td>
        </tr>
      </table>
</td>
          </tr>
      </table>
<table width="100%" cellpadding="4" cellspacing="0">
                <tr>
                  <td class="formCaptionTitle"><strong>
                    <?=$c_data_web?>
                  </strong></td>
                </tr>
      </table>
              <table width="100%" cellpadding="4" cellspacing="0" class="formstbl">
                <tr>
                  <td width="150" class="formsCaption"><strong>
                    <?=$c_discount?>:</strong></td>
                  <td class="forms"><?=$discount?></td>
                  <td width="150" class="formsCaption"><strong><?=$c_free_send?>:</strong></td>
                  <td class="forms"><?=$free_send?></td>
                </tr>
              </table>
              <table width="100%" cellpadding="4" cellspacing="0">
                <tr> 
                  <td class="formCaptionTitle"><strong><?=$c_descriptions?></strong></td>
                </tr>
              </table>
              <table width="100%" cellpadding="0" cellspacing="0" class="formstbl">
                <tr> 
                  <td><table cellpadding="4" cellspacing="0">
                      <tr> 
                        <td width="150" valign="top" class="formsCaption"><strong>
                        <?=$c_cars_title?>:</strong></td>
                        <td><table cellpadding="4" cellspacing="0"><?foreach($cars_title as $l): extract ($l)?>
                      <tr> 
                        <td class="forms"><?=$lang?></td><td>
                          <?=$cars_title?> 
                        </td>
                      </tr>
                        <?endforeach //$cars_title?>
                    </table></td>
                      </tr>
                    </table>
                    <table width="100%" cellpadding="0" cellspacing="0">
                      <tr> 
                        <td height="1" class="formsCaptionHor"><img src="/admin/themes/default/images/spacer.gif" width="1" height="1"></td>
                      <tr> 
                    </table>
                    <table width="100%" cellpadding="4" cellspacing="0">
                      <td class="formsCaptionHor"><strong><?=$c_description?> 
                        </strong></td>
                      </tr>
                      <?foreach($cars_description as $l): extract ($l)?>
                      <tr> 
                        <td class="formsHor"><?=$lang?><br><?=$cars_description?>
                        </td>
                      </tr>
                      <?endforeach //$pcar_description?>
                    </table></td>
                </tr>
              </table>
              <?if ($form_new):?>
              <table width="100%" cellpadding="4" cellspacing="2" class="formsButtonstbl">
                <tr> 
                  <td align="center" class="formsButtons"> <input type="submit" name="Submit" value="<?=$c_send_new?>" class="btn_form1"> 
                  </td>
                </tr>
              </table>
              <?endif //form_new?><?if ($form_edit):?>
              <table width="100%" cellpadding="4" cellspacing="2" class="formsButtonstbl">
                <tr> 
                  <td align="center" class="formsButtons"><input type="submit" name="Submit" value="<?=$c_send_edit?>" class="btn_form1">&nbsp;&nbsp;<a class="btn_form_delete" href="javascript:submit_form('<?=$c_confirm_form_bin?>', true);">
                      <?=$c_delete?>
                      </a>&nbsp;<a class="btn_form1" href="<?=$preview_link?>" target="_blank">
                <?=$c_preview?>
                </a>&nbsp;</td>
                </tr>
              </table>
              <?endif //form_edit?></td>
          </tr>
        </table></td>
  </tr>      
</table>
</form>
