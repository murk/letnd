<?

/* 6-4-18

Dipost a product


 */

if( is_table( 'product__order' ) && !is_field( 'product__order', 'deposit' ) ){

	$update_sql[]="ALTER TABLE `product__order`
	ADD COLUMN `deposit` DECIMAL(11,2) NOT NULL AFTER `promcode_discount`;";

}else{
	if( is_table( 'product__order' ) ){
		trigger_error( 'Ja existeixen el camp deposit' );
	}
}

if( is_table( 'product__order' )){

	$update_sql[]="ALTER TABLE `product__order`
	CHANGE COLUMN `order_status` `order_status` ENUM('paying','failed','deposit','pending','denied','voided','refunded','completed','preparing','shipped','delivered') NOT NULL COLLATE 'utf8_spanish2_ci';";

}