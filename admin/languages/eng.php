<?
define("MENU_HOME_PUBLIC","Web");
define("MENU_HOME","Homepage");
define("MENU_LOGOUT","Log out");


// configurable
define("USER_TEXT","user");
define('USER_LOGIN_INVALID','The user name or password are not valid');
define('USER_LOGIN_VALID','Wellcome to ' . HOST_URL);

// la linea de sota no es pot tocar, que te dit que no la toquis!!!!!!!
// menus
define('EXPERIENCIA_MENU_ROOT','Experience');
define('NEWSLETTER_MENU_ROOT','Newsletter');
define('TRANSLATION_MENU_ROOT','Translations');
define('FACTURACIO_MENU_ROOT','Invoicing');
define('STATISTICS_MENU_ROOT','Statistics');
define('USER_MENU_ROOT','Users');
define('ADMINTOTAL_MENU_ROOT','Admin total');
define('WEB_MENU_ROOT','Letnd management');
define('INMO_MENU_ROOT','Properties');
define('OWNER_MENU_ROOT','Owners');
define('REPORT_MENU_ROOT','Documents');
define('CONFIG_MENU_ROOT','Configuration');
define('NEWS_MENU_ROOT','News');
define('GALERY_MENU_ROOT','Image gallery');
define('RENOVATS_MENU_ROOT','Gift postcards');
define('SHOPWINDOW_MENU_ROOT','Shop windows');
define('UTILITY_MENU_ROOT','Utilities');
define ('MLS_MENU_ROOT','MLS');
define('CUSTUMER_MENU_ROOT','Customers');
define('WORKS_MENU_ROOT','Works');
define('PRODUCT_MENU_ROOT','Products');
define('RATE_MENU_ROOT','Rates');
define('CARS_MENU_ROOT','Cotxes');
define('NEW_MENU_ROOT','Noticies');
define('PORTFOLI_MENU_ROOT','Portfoli');
define('CONTACT_MENU_ROOT','Contacte');
define('PRINT_MENU_ROOT','Print');
define('BOOKING_MENU_ROOT','Bookings');
define('MISSATGE_MENU_ROOT','Messages');
define('CUSTOMER_MENU_ROOT','Clients and orders');
define('DIRECTORY_MENU_ROOT','Files');
define('SELLPOINT_MENU_ROOT','Sell points');



define('DB_SPLIT_TITLE_FIRST_PAGE', 'Beginning');
define('DB_SPLIT_TITLE_PREVIOUS_PAGE', 'Previous page');
define('DB_SPLIT_TITLE_NEXT_PAGE', 'Next page');
define('DB_SPLIT_TITLE_LAST_PAGE', 'End');
define('DB_SPLIT_TITLE_PAGE_NO', 'Page %d');
define('DB_SPLIT_TITLE_PREV_SET_OF_NO_PAGE', 'Previous %d pages');
define('DB_SPLIT_TITLE_NEXT_SET_OF_NO_PAGE', 'Next %d pages');
define('DB_SPLIT_BUTTON_FIRST', '&lt;&lt;BEGINNING');
define('DB_SPLIT_BUTTON_PREV', '&lt;&lt;&nbsp;Previous');
define('DB_SPLIT_BUTTON_NEXT', 'Next&nbsp;&gt;&gt;');
define('DB_SPLIT_BUTTON_LAST', 'FINAL&gt;&gt;');
define('DB_SPLIT_DISPLAY_NUMBER_OF_RESULTS', 'Showing <b>%d</b> to <b>%d</b> (of <b>%d</b>)');
define ('TITLE_SEARCH','Search results');

define('DB_SPLIT_PAGE_TITLE', 'Page');
define('DB_SPLIT_PAGE_DESCRIPTION', 'Results %d to %d of %d');

$month_names = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
$week_names = array("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday");


// Caption default
$gl_caption['c_form_title'] = 'Edit data';
$gl_caption['c_print_title'] = 'Data';
$gl_caption['c_config_title'] = 'Edit configuration';
$gl_caption['c_edit'] = 'Edit details';


$gl_caption['c_edit_button'] = 'Edit data';
$gl_caption['c_view_button'] = 'View data';
$gl_caption['c_edit_images_button'] = 'Edit images';
$gl_caption['c_add_images_button'] = 'Add images';



$gl_caption['c_edit_data'] = 'Edit data';
$gl_caption['c_edit_images'] = 'Edit images';
$gl_caption['c_add_images'] = 'Add images';
$gl_caption['c_search_form'] = 'Search';
$gl_caption['c_send_new'] = 'Save';
$gl_caption['c_send_edit'] = 'Save changes';
$gl_caption['c_delete'] = 'Delete';
$gl_caption['c_action'] = 'Action';
$gl_caption['c_delete_selected'] = 'Delete selected';
$gl_caption['c_restore_selected'] = 'Restore selected';
$gl_caption['c_confirm_deleted'] = 'The selected elements will be completely deleted from the database \\n\\nDo you wish to continue?';
$gl_caption['c_confirm_images_deleted'] = 'The selected images will be completely deleted from the database \\n\\nDo you wish to continue?';
$gl_caption['c_confirm_form_bin'] = 'The record will be sent to the recycle bin \\n\\nDo you wish to continue?';
$gl_caption['c_confirm_form_deleted'] = 'The record will be completely deleted from the database \\n\\nDo you wish to continue?';
$gl_caption['c_move_to'] = 'Move to:';
$gl_caption['c_print_all'] = 'Print all';
$gl_caption['c_print'] = 'Print';
$gl_caption['c_order_by'] = 'Sort by:';
$gl_caption['c_select'] = 'Select';
$gl_caption['c_multislect_selectable'] = 'Fields';
$gl_caption['c_multislect_selected'] = 'Selected fields';
$gl_caption['c_all'] = 'all';
$gl_caption['c_nothing'] = 'nothing';
$gl_caption['c_preview'] = 'Preview';
$gl_caption['c_yes'] = 'Yes';
$gl_caption['c_no'] = 'No';
$gl_caption['c_options_bar'] = 'Options';
$gl_caption['c_file_maximum'] = 'maximum %s files at a time';
$gl_caption['c_file_maximum_1'] = 'maximum 1 file at a time';

// buscadors
$gl_caption['c_search_title'] = 'Search';
$gl_caption['c_by_ref'] = '(or reference nos. separated by spaces, e.g. "456 245 45")';
$gl_caption['c_by_words'] = 'By words';

// sobreescrits caption images
$gl_caption_image['c_edit'] = 'Images from';
$gl_caption_image['c_insert'] = 'Add images from your computer';
$gl_caption_image['c_insert_advanced'] = 'Advanced (java)';
$gl_caption_image['c_delete_selected'] = 'Delete selected images';
$gl_caption_image['c_confirm_deleted'] = 'The images with be completely deleted.\\n\\n\\nDo you wish to continue?';
$gl_caption_image['c_select_all'] = 'Select all';
$gl_caption_image['c_deselect_all'] = 'Deselect all';

// nous images
$gl_caption_image['c_stop'] = 'Stop';
$gl_caption_image['c_refresh'] = 'Refresh';
$gl_caption_image['c_add'] = 'Add images';
$gl_caption_image['c_thumbnails'] = 'Images';
$gl_caption_image['c_icons'] = 'Icons';
$gl_caption_image['c_list'] = 'List';
$gl_caption_image['c_details'] = 'Details';
$gl_caption_image['c_progress_dialog_title_text'] = 'Adding images';

// camps images
$gl_caption_image['c_ordre'] = 'order';
$gl_caption_image['c_main_image'] = 'main';
$gl_caption_image['c_name'] = 'Name';
$gl_caption_image['c_imagecat_id'] = 'Category';
$gl_caption_image['c_image_alt'] = 'Alt';
$gl_caption_image['c_image_title'] = 'Title';
$gl_caption_image['c_edit_titles_images'] = 'Edit image titles';
$gl_caption_image['c_hide_titles_images'] = 'Hide image titles';

// sobreescrits caption traduccions i traduccions_news.tpl
$gl_caption_translation['c_send_edit'] = 'Submit for translation';
$gl_caption_translation['c_page_words_title'] = 'Total no. of words for translation on this page';
$gl_caption_translation['c_all_page_words_title'] = 'Total no. of words for translation on all the pages';
$gl_caption_translation['c_total'] = 'Total';

// sobreescrits end


// Message default
$gl_messages['title_edit']='Edit';
$gl_messages['title_copy']='Copy details';
$gl_messages['added']='The record has been successfully added.';
$gl_messages['add_exists']='A record with this %1$s already exists: %2$s';
$gl_messages['saved']='The record has been modified successfully';
$gl_messages['not_saved']='The record has not been changed';
$gl_messages['no_records']='There is no record in this listing';
$gl_messages['no_records_bin']='There is no record in the recycle bin';
$gl_messages['list_saved']='The changes to the records have been carried out successfully';
$gl_messages['list_not_saved']='No record has been changed. No change has been detected';
$gl_messages['list_deleted']='The records have been deleted';
$gl_messages['list_bined']='The records have been moved to the recycling bin';
$gl_messages['list_restored']='The records have been successfully restored';
$gl_messages['concession_no_acces']='You do not have permission for this section';
$gl_messages['concession_no_write']='You do not have write permission for this section';
$gl_messages['select_item']='Click on a record in the list to select';
$gl_messages['related_not_deleted']='The marked records cannot be deleted, because they have related records';



// sobreescrits message image
$gl_messages_image['list_saved']='The changes to the images have been carried out successfully.';
$gl_messages_image['list_not_saved']='No record has been changed. No change has been detected';
$gl_messages_image['list_deleted']='The images have been deleted';
// sobreescrits end


// NO TRADUIT
$gl_messages['deleted']='The record has been deleted';
$gl_messages['bined']='The record has been sent to the recycle bin';
$gl_messages['restored']='The record has been successfully restored';
$gl_caption['c_ok'] = 'Accept';
$gl_caption['c_cancel'] = 'Cancel';
$gl_caption ['c_close']='Close';
$gl_messages['post_max_size']='El pes del formulari (contingut + arxius adjunts) excedeix el límit de ';
$gl_messages['post_file_max_size']='El pes dels arxius adjunts excedeix el límit de ';
$gl_messages['assigned']='Registres sel·leccionats';
$gl_caption['c_black']='Negre';
$gl_caption['c_white']='Blanc';
$gl_caption['c_silver']='Plata';
$gl_caption['c_grey']='Verd';
$gl_caption['c_blue']='Blau';
$gl_caption['c_red']='Vermell';
$gl_caption['c_orange']='Taronja';
$gl_caption['c_green']='Verd';
$gl_caption['c_purple']='Lila';
$gl_caption['c_pink']='Rosa';
$gl_caption['c_golden']='Daurat';
$gl_caption['c_yellow']='Groc';
$gl_caption['c_maroon']='Grana';
$gl_caption['c_brown']='Marró';
//creo un array global per els mesos
$gl_caption_months = array("Gener", "Febrer", "Març", "Abril", "Maig", "Juny", "Juliol", "Agost", "Setembre", "Octubre", "Novembre", "Desembre");

$gl_caption ['c_2']='dos';
$gl_caption ['c_3']='tres';
$gl_caption ['c_4']='quatre';
$gl_caption ['c_5']='cinc';
$gl_caption ['c_6']='sis';
$gl_caption ['c_7']='set';
$gl_caption ['c_8']='vuit';
$gl_caption ['c_9']='nou';
$gl_caption['c_10']='deu';
$gl_caption['c_11']='onze';
$gl_caption['c_12']='dotze';
$gl_caption['c_13']='tretze';
$gl_caption['c_14']='catorze';
$gl_caption['c_15']='quinze';
$gl_caption['c_16']='setze';
$gl_caption['c_17']='diset';
$gl_caption['c_18']='divuit';
$gl_caption['c_19']='dinou';
$gl_caption['c_20']='vint';
$gl_caption['c_30']='trenta';
$gl_caption['c_40']='quaranta';
$gl_caption['c_50']='cinquanta';
$gl_caption['c_60']='seixanta';
$gl_caption['c_70']='setanta';
$gl_caption['c_80']='vuitanta';
$gl_caption['c_90']='noaranta';
$gl_caption['c_1000']='mil';


$gl_caption['c_add_button'] = 'Afegir registre';

$gl_caption['c_login'] = 'Login';
$gl_caption['c_password'] = 'Password';
$gl_caption['c_mail'] = 'E-mail';
$gl_caption['c_mail_repeat'] = 'Repeat E-mail';
$gl_caption['c_password_repeat'] = 'Repeat password';
$gl_caption['c_noscript'] = '<p><strong>El seu navegador no soporta o té desactivat Javascript. </strong></p><p>Ha d\'<a href="//www.google.com/support/websearch/bin/answer.py?answer=23852">habilitar Javascript</a> per poder utilitzar Letnd</p>';
$gl_caption['c_cookie'] = '<p><strong>El seu navegador no soporta o té desactivades les Cookies. </strong></p><p>Ha d\'<a href="//www.google.com/support/websearch/bin/answer.py?answer=35851">habilitar les Cookies</a> per poder utilitzar Letnd</p>';

$gl_messages_image['images_uploaded']='Imatges guardades';
$gl_messages['form_sent']='El formulari ha estat enviat correctament';
$gl_messages['form_not_sent']='El formulari no s\'ha pogut enviar';


$gl_caption['c_search_adress'] = 'Buscar adreça';
$gl_caption['c_map_frame_1'] = 'S\'ha trobat coincidència per';
$gl_caption['c_map_frame_2'] = 'Si l\'ubicació és correcta';
$gl_caption['c_map_frame_3'] = 'clica aquí per guardar';
$gl_caption['c_map_frame_4'] = 'No s\'ha trobat cap coincidència';

$gl_caption['c_list_view_1'] = 'Normal';
$gl_caption['c_list_view_2'] = 'Big icons';
$gl_caption['c_list_view_3'] = 'List';

$gl_caption['c_edit_list_button_1'] = 'No editar llistat';
$gl_caption['c_edit_list_button_'] = 'Editar llistat';
$gl_caption['c_add_dots'] = 'Retallar text';

// SEO
$gl_caption['c_seo']='Eines SEO ( Posicionament web )';
$gl_caption['c_page_title']='Títol de la pàgina. max. 66-69 caràcters. Òptim < 65 car:';
$gl_caption['c_page_description']='Descripció de la pàgina. max. 156-158 caràcters:';
$gl_caption['c_page_keywords']='Paràules clau:';
$gl_caption['c_file_name']='Nom de la pàgina en URL. Ex. www.lamevaweb.com/<b>nom-de-la-pagina</b>.htm max. 90 caràcters:';
$gl_caption['c_file_name_excel']='Nom pàgina';
$gl_caption['c_old_file_name']='Nom vell de la pàgina en URL.';



// SOCIAL
$gl_caption['c_edit_social'] = 'Social networks';
$gl_caption['c_edit_social_title'] = 'Publish';
$gl_caption['c_social_language'] = 'Idioma';
$gl_caption['c_facebook_message'] = 'Missatge';
$gl_caption['c_facebook_picture'] = 'Imatge';
$gl_caption['c_facebook_link'] = 'Enllaç';
$gl_caption['c_facebook_link_explanation'] = 'Agafa els texts directament de la pàgina';
$gl_caption['c_facebook_name'] = 'Descripció 1<sup>a</sup> línia';
$gl_caption['c_facebook_caption'] = 'Descripció 2<sup>a</sup> línia';
$gl_caption['c_facebook_image_title'] = 'Imatge';
$gl_caption['c_facebook_description'] = 'Descripció';
$gl_caption['c_facebook_save'] = 'Publicar a facebook';
$gl_caption['c_facebook_publish_as'] = 'Publicar com a';
$gl_caption['c_facebook_publish_as_image'] = 'Imatge';
$gl_caption['c_facebook_publish_as_link'] = 'Enllaç';
$gl_caption['c_facebook_publish_save_default']='Predeterminar aquesta opció';
$gl_caption['c_facebook_publish_user']='Mur propi';
$gl_caption['c_facebook_publish_pagina']='Pàgina';
$gl_caption['c_facebook_publish_id']='Publicar a';
$gl_caption['c_facebook_login']='Iniciar sessió a Facebook';
$gl_caption['c_facebook_delete_text']='<p>Els missatges publicats a Facebook només es poden modificar des de Facebook.</p><p>Si vols tornar a publicar des de Letnd primer s\'ha d\'esborrar la publicació anterior</p>';
$gl_caption['c_facebook_delete']='Esborrar publicació a Facebook';
$gl_caption['c_facebook_logout']='Logout';
$gl_caption['c_facebook_cannot_delete']='No es pot esborrar la publicació ja que es va crear amb un altre usuari de Facebook corresponent a';
$gl_caption['c_facebook_activate']='El preu d\'activació d\'aquest servei es de 60€ para poder donar d\'alta a Facebook';

$gl_caption['c_twitter_message'] = 'Missatge';
$gl_caption['c_twitter_save'] = 'Publicar a twitter';
$gl_caption['c_twitter_login']='Iniciar sessió a Twitter';
$gl_caption['c_twitter_delete_text']='<p>Els missatges publicats a Twitter només es poden modificar des de Twitter.</p><p>Si vols tornar a publicar des de Letnd primer s\'ha d\'esborrar la publicació anterior</p>';
$gl_caption['c_twitter_delete']='Esborrar publicació a Twitter';
$gl_caption['c_twitter_cannot_delete']='No es pot esborrar la publicació ja que es va crear amb un altre usuari de Twitter corresponent a';

$gl_caption['c_button_form_twitter_published']=
$gl_caption['c_button_form_facebook_published']=
$gl_caption['c_button_form_twitter']=
$gl_caption['c_button_form_facebook']=	'';
$gl_caption['c_chars']=	'Caràcters';

$gl_messages['social_facebook_no_user']='No estàs loguejat, si us plau torna a probar-ho';
$gl_messages['social_facebook_no_page']='No hi ha cap pàgina amb aquest id';
$gl_messages['social_facebook_published']='El missatge s\'ha publicat correctament a Facebook';
$gl_messages['social_facebook_not_init']='No s\'ha pogut connectar a Facebook';
$gl_messages['social_facebook_error']='Hi ha hagut un error a Facebook: ';
$gl_messages['social_facebook_deleted']='El missatge s\'ha esborrat a Facebook';
$gl_messages['social_facebook_not_deleted']='No hi ha cap missatge per borrar';

$gl_messages['social_twitter_empty']='El missatge està buit, no es pot publicar a Twitter';
$gl_messages['social_twitter_no_user']='No estàs loguejat, si us plau torna a probar-ho';
$gl_messages['social_twitter_published']='El missatge s\'ha publicat correctament a Twitter';
$gl_messages['social_twitter_not_init']='No s\'ha pogut conectar a Twitter';
$gl_messages['social_twitter_error']='Hi ha hagut un error a Twitter: ';
$gl_messages['social_twitter_deleted']='El missatge s\'ha esborrat a Twitter';
$gl_messages['social_twitter_not_deleted']='No hi ha cap missatge per borrar';

// FI NO TRADUIT


$gl_caption['c_file_manager']=	'File manager';
$gl_messages['no_records_search']='No match found for this search';
$gl_caption['c_back'] = 'Back';

$gl_caption['c_asc'] = 'asc';
$gl_caption['c_desc'] = 'desc';

$gl_caption['c_reply'] = 'Reply';

$gl_caption['c_file_file']='File';
$gl_caption['c_file_title']='Title';
$gl_caption['c_file_show_home']='In list';
$gl_caption['c_file_ordre']='Order';
$gl_caption['c_file_public']='Public';
$gl_caption['c_file_filecat_id']='Cat.';

$gl_caption['c_export_button']= 'Export';

$gl_caption['c_entered'] = 'Created';
$gl_caption['c_modified'] = 'Modified';