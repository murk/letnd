<?php
Main::load_class( 'file' );
// carrego moduls que necessito nomès a admintotal
global $gl_db_classes_table, $gl_db_classes_id_field;

$GLOBALS['gl_order_by'] = "tool ASC, tool_section";

$gl_db_classes_table    = 'all__menu';
$gl_db_classes_id_field = 'menu_id';
// /////////////////////////////////////////////////////////////////////////////////////
// GUARDA CANVIS EN SECCIONS: DIRS, PROTECT, CREA TAULA, ETC
// /////////////////////////////////////////////////////////////////////////////////////
function save_rows() {
	global $gl_action, $gl_message;
	switch ( $gl_action ) {
		case 'save_rows_unprotect':
			$config_file = DOCUMENT_ROOT . 'admin/modules/' . $_GET['tool2'] . '/' . $_GET['tool2'] . '_' . $_GET['tool_section2'] . "_config.php";
			$size        = filesize( $config_file );
			$source_file = fopen( $config_file, "r+" );

			$contents    = fread( $source_file, $size );
			$contents    = str_replace( '$gl_file_protected = true;', '$gl_file_protected = false;', $contents );
			$source_file = fopen( $config_file, "w" );
			fwrite( $source_file, $contents );
			fclose( $source_file );
			$gl_message           = "Arxiu desprotegit: $config_file";
			$GLOBALS['gl_reload'] = true;
			break;
		case 'save_rows_protect':
			$config_file = DOCUMENT_ROOT . 'admin/modules/' . $_GET['tool2'] . '/' . $_GET['tool2'] . '_' . $_GET['tool_section2'] . "_config.php";
			$size        = filesize( $config_file );
			$source_file = fopen( $config_file, "r+" );

			$contents    = fread( $source_file, $size );
			$contents    = str_replace( '$gl_file_protected = false;', '$gl_file_protected = true;', $contents );
			$source_file = fopen( $config_file, "w" );
			fwrite( $source_file, $contents );
			fclose( $source_file );
			$gl_message           = "Arxiu protegit: $config_file";
			$GLOBALS['gl_reload'] = true;
			break;
		case 'save_rows_table':
			$table = $_GET['tool2'] . '__' . $_GET['tool_section2'];
			$id    = $_GET['tool_section2'] . '_id';

			$query = "CREATE TABLE `" . $table . "` (
  				`" . $id . "` INT(11) NOT NULL AUTO_INCREMENT,
  				PRIMARY KEY  (`" . $id . "`))";
			Db::execute( $query );
			$gl_message           = "Taula creada: <br>CREATE TABLE `" . $table . "` (
  				<br>`" . $id . "` int(11) NOT NULL auto_increment,
					<br>PRIMARY KEY  (`" . $id . "`))";
			$GLOBALS['gl_reload'] = true;
			break;
		case 'save_rows_languages':
			// TODO que no guardi arxiu general si no s'ha canviat res
			$tool_dir = DOCUMENT_ROOT . 'admin/modules/' . $_GET['tool2'] . '/';
			// pas a pas, de moment nomès va amb el català
			$tool_language_dir  = $tool_dir . 'languages/';
			$tool_language_file = $tool_language_dir . 'cat.php';
			if ( isset( $_POST['root_cat_php'] ) ) {
				$text = trim( $_POST['cat_php'] );
				//write_file(DOCUMENT_ROOT . 'admin/languages/cat.php', $text);
				$gl_message .= "<tr><td><b>Idioma general no guardat, això esta desactivat</b></td></tr>";
			}
			if ( isset( $_POST['cat_php'] ) ) {
				$text = trim( $_POST['cat_php'] );
				write_file( $tool_language_file, $text );
				$gl_message .= "<tr><td>Idioma modul guardat</td></tr>";
			}
			$gl_message = "<table>" . $gl_message . "</table>";
			break;
		case 'save_rows_dir':

			$tool_dir          = DOCUMENT_ROOT . 'admin/modules/' . $_GET['tool2'] . '/';
			$tool_section_file = $tool_dir . $_GET['tool2'] . '_' . $_GET['tool_section2'] . '.php';
			// pas a pas, de moment nomès va amb el català
			$tool_language_dir  = $tool_dir . 'languages/';
			$tool_language_file = $tool_language_dir . 'cat.php';

			$php_template_dir = DOCUMENT_ROOT . 'admin/modules/admintotal/php_templates/';

			if ( ! is_dir( $tool_dir ) ) {
				// creo directori eina
				if ( mkdir( $tool_dir, 0776 ) ) {
					$gl_message .= "<tr><td>Directori creat:</td><td>'$tool_dir'</td></tr>";
					// creo directori idioma
					if ( mkdir( $tool_language_dir, 0776 ) ) {
						$gl_message .= "<tr><td>Directori idioma creat:</td><td>'$tool_language_dir'</td></tr>";
						// copia arxiu idioma
						if ( copy( $php_template_dir . '/languages/cat.php', $tool_language_file ) ) {
							$gl_message .= "<tr><td>Arxiu idiomes copiat:</td><td>'$tool_language_file'</td></tr>";
						}
					}
				}
			}
			if ( ! is_file( $tool_section_file ) ) {
				// copio arxiu seccio
				if ( copy( $php_template_dir . 'tool_section.php', $tool_section_file ) ) {
					$file          = new File( $tool_section_file );
					$file->content = str_replace( '__Tool', ucfirst( $_GET['tool2'] ), $file->content );
					$file->content = str_replace( '__Section', snake_to_camel( $_GET['tool_section2'], true ), $file->content );
					$file->content = str_replace( '__tool', $_GET['tool2'], $file->content );
					$file->content = str_replace( '__section', $_GET['tool_section2'], $file->content );
					$file->write();
					$gl_message .= "<tr><td>Arxiu secció creat:</td><td>'$tool_section_file'</td></tr>";
				}
			}
			$gl_message = "<table>" . $gl_message . "</table>";
			//$GLOBALS['gl_page']->show_message($gl_message);
			$GLOBALS['gl_reload'] = true;
			break;
	}
}

// /////////////////////////////////////////////////////////////////////////////////////
// FORMULARI PER ESCRIURE ELS ARXIUS D'IDIOMES
// /////////////////////////////////////////////////////////////////////////////////////
function list_records_languages() {
	global $tpl, $gl_content;
	// agafo la taula i la seccio del nom de la taula per si no coincideix amb la tool_section i el tool
	$table_array        = explode( '__', $_GET['table'] );
	$table_tool         = $table_array[0];
	$table_tool_section = $table_array[1];
	$table              = $_GET['table'];

	$tool_dir = DOCUMENT_ROOT . 'admin/modules/' . $_GET['tool2'] . '/';
	// pas a pas, de moment nomès va amb el català
	$tool_language_dir     = $tool_dir . 'languages/';
	$tool_language_file    = $tool_language_dir . 'cat.php';
	$tool_language_content = read_file( $tool_language_file );
	$root_language_content = read_file( DOCUMENT_ROOT . 'admin/languages/cat.php' );

	include( $tool_language_file );

	$loop                    = array();
	$loop_caption            = array();
	$loop_constant_edit      = array();
	$loop_root_constant_edit = array();
	foreach ( get_defined_constants() as $key => $val ) {
		if ( ( strstr( $tool_language_content, "define('" . $key ) ) || ( strstr( $tool_language_content, "define(\"" . $key ) ) ) {
			$loop_constant_edit[] = array( 'var_name' => $key, 'var_value' => $val );
		}
		if ( ( strstr( $root_language_content, "define('" . $key ) ) || ( strstr( $root_language_content, "define(\"" . $key ) ) ) {
			$loop_root_constant_edit[] = array( 'var_name' => $key, 'var_value' => $val );
		}
	}
	$query   = "SELECT variable FROM all__menu WHERE tool = '" . $_GET['tool2'] . "'";
	$results = Db::get_rows( $query );
	// constants del menu
	foreach ( $results as $rs ) {
		if ( $rs['variable'] == strtoupper( $_GET['tool2'] ) . '_MENU_ROOT' ) {
			if ( ! defined( $rs['variable'] ) ) {
				$tpl->set_var( 'menu_root', $rs['variable'] );
			} else {
				$tpl->set_var( 'menu_root', '' );
			}
		} elseif ( ! defined( $rs['variable'] ) ) {
			$loop[] = array( 'var_name' => $rs['variable'], 'var_value' => '' );
		}
	}
	// $gl_caption dels camps de la BBDD
	if ( is_table( $table ) ) {
		$fields = get_fields( $table );
		foreach ( $fields as $key => $value ) {
			$key = strtolower( $key );
			if ( $key != $table_tool_section . '_id' ) {
				$var_value      = isset( $gl_caption[ 'c_' . $key ] ) ? $gl_caption[ 'c_' . $key ] : '';
				$var_value      = isset( ${'gl_caption_' . $_GET['tool_section2']}[ 'c_' . $key ] ) ? ${'gl_caption_' . $_GET['tool_section2']}[ 'c_' . $key ] : $var_value;
				$loop_caption[] = array( 'var_name' => 'c_' . $key, 'var_value' => $var_value );
			}
		}
	}
	// $gl_caption dels camps de la BBDD d'idiomes
	$table_languages = $table . '_language';
	if ( is_table( $table_languages ) ) {
		$fields = get_fields( $table_languages );
		foreach ( $fields as $key => $value ) {
			$key = strtolower( $key );
			if ( $key != $table_tool_section . '_id' && $key != 'language' ) {
				$var_value      = isset( $gl_caption[ 'c_' . $key ] ) ? $gl_caption[ 'c_' . $key ] : '';
				$var_value      = isset( ${'gl_caption_' . $_GET['tool_section2']}[ 'c_' . $key ] ) ? ${'gl_caption_' . $_GET['tool_section2']}[ 'c_' . $key ] : $var_value;
				$loop_caption[] = array( 'var_name' => 'c_' . $key, 'var_value' => $var_value );
			}
		}
	}

	$tpl->set_var( 'tool', $_GET['tool2'] );
	$tpl->set_var( 'tool_section', $_GET['tool_section2'] );
	$tpl->set_var( 'root_cat_php', $root_language_content );

	$tpl->set_var( 'cat_php', $tool_language_content );
	$tpl->set_file( 'admintotal/modules_list_languages.tpl' );
	$tpl->set_loop( 'loop', $loop );
	$tpl->set_loop( 'loop_caption', $loop_caption );
	$tpl->set_loop( 'loop_root_constant_edit', $loop_root_constant_edit );
	$tpl->set_loop( 'loop_constant_edit', $loop_constant_edit );
	$gl_content = $tpl->process();
}

// /////////////////////////////////////////////////////////////////////////////////////
// LLISTAT DE SECCIONS AMB OPCIONS DE MODIFICAR CONFIG, CREAR TAULA ETC...
// /////////////////////////////////////////////////////////////////////////////////////
function list_records() {
	global $gl_action, $gl_db_max_results;
	$gl_db_max_results = 2000;
	// m'he trobat quan faig servir el $gl_action dins de la funció list_records (admintotal/admintotal_modules) que quan vinc
	// de guardar resultats i redirigeixo l'aplicacio al list records un altre cop, haig de posar el gl_action = a list_records, ja que sino es = a save_rows i no em fa res, ja que espero que sigui o list_records o list_records_xxxxx
	// $gl_action = $action;
	// solucio : posar sempre el action_previous_list
	switch ( $gl_action ) {
		case 'list_records':
			$listing               = new ListRecords;
			$listing->group_fields = array( 'tool' );
			$listing->group_by     = "tool, tool_section";
			$listing->order_by     = $GLOBALS['gl_order_by'];
			$listing->call( 'list_records_walk', 'tool,tool_section' );
			$listing->condition = "tool_section <> ''";
			$listing->list_records();
			break;
		case 'list_records_languages':
			list_records_languages();
			break;
	}
}

function list_records_walk( $tool, $tool_section ) {
	$tool_dir          = DOCUMENT_ROOT . 'admin/modules/' . $tool . '/';
	$tool_config_file  = $tool_dir . $tool . '_' . $tool_section . '_config.php';
	$tool_section_file = $tool_dir . $tool . '_' . $tool_section . '.php';
	// pas a pas, de moment nomès va amb el català
	$tool_language_dir  = $tool_dir . 'languages/';
	$tool_language_file = $tool_language_dir . 'cat.php';

	$r['protected']     = false;
	$r['unprotected']   = false;
	$r['modify_config'] = '';
	$r['create_config'] = '';

	if ( file_exists( $tool_config_file ) ) {
		$r['is_config_file'] = true;
		include_once( $tool_config_file );
		if ( isset( $gl_file_protected ) && $gl_file_protected ) {
			$r['protected'] = true;
		} else {
			$r['unprotected']   = true;
			$r['modify_config'] = 'modificar configuració';
		}
	} else {
		$r['is_config_file'] = false;
		$r['create_config']  = 'crear configuració';
	}
	// comprova si la taula existeix
	if ( ! is_table( $tool . '__' . $tool_section ) ) {
		$r['create_config'] = '';
		$r['protected']     = false;
		$r['unprotected']   = false;
		//$r['unprotect'] = '';
	}
	// comproba si existeix l'arxiu del modul per copiar-hi les plantilles per iniciar-lo
	$r['is_file']          = is_file( $tool_section_file );
	$r['is_language_file'] = is_file( $tool_language_file );
	// comproba si existeix la taula del modul
	$r['is_table'] = is_table( $tool . '__' . $tool_section );

	return $r;
}

// /////////////////////////////////////////////////////////////////////////////////////
// FORMULARI DE CONFIGURACIÓ DE CAMPS
// /////////////////////////////////////////////////////////////////////////////////////
function show_form() {
	global $inmo_form, $tpl, $gl_action, $gl_menu_id, $gl_message, $gl_db_classes_id_field;
	$config_file = DOCUMENT_ROOT . 'admin/modules/' . $_GET['tool2'] . '/' . $_GET['tool2'] . '_' . $_GET['tool_section2'] . '_config.php';
	// agafo la taula i la seccio del nom de la taula per si no coincideix amb la tool_section i el tool
	$table_array        = explode( '__', $_GET['table'] );
	$table_tool         = $table_array[0];
	$table_tool_section = $table_array[1];
	$table              = $_GET['table'];

	$is_config = $table_tool_section == 'configadmin' || $table_tool_section == 'configpublic';
	// valors per defecte generals del llistat i el formulari
	$gl_db_classes_list = array(
		'cols' => '1',
	);
	$gl_db_classes_form = array(
		'cols'         => '1',
		'images_title' => '',
	);
	// fi valors per defecte
	if ( ! is_table( $table ) ) {
		echo $table;
		$gl_message = 'La taula no existeix';

		return;
	}
	// TAULES NORMALS O CONFIGS
	// agafo els fields de la BBDD o els valors dels 'name' en una taula de configuració
	$fields = get_fields( $table );

	$language_fields = array();
	if ( is_table( $table . '_language' ) ) {
		if ( $is_config ) {
			global $gl_languages;
			$language = current( $gl_languages['public'] );
			$q        = "SELECT * FROM " . $table . "_language
			WHERE language = '" . $language . "'
			ORDER BY " . $table_tool_section . "_id";
			$results  = Db::get_rows( $q );
			foreach ( $results as $rs ) {
				$language_fields[] = strtoupper( $rs['name'] );
			}
		} else {
			$fields_lang = meta_columns( $table_tool . '__' . $table_tool_section . '_language' );
			unset( $fields_lang[ strtoupper( $table_tool_section . '_id' ) ] );
			unset( $fields_lang['LANGUAGE'] );
			$fields += $fields_lang;
			$language_fields = array_keys( $fields_lang );
		}
	}

	Debug::add( 'Camps taula i taula LANGUAGE', $language_fields );
	$is_file = file_exists( $config_file );
	if ( $is_file ) {
		include_once( $config_file );
	}
	// print_r ($gl_db_classes_fields);
	foreach ( $fields as $name => $field ) {
		$fields_bbdd = '';
		foreach ( $field as $key => $value ) {
			$fields_bbdd[ $key ] = $value;
		}
		if ( in_array( strtoupper( $name ), $language_fields ) ) {
			$fields_bbdd['language'] = '1';
		}

		if ( $is_file && isset( $gl_db_classes_fields[ strtolower( $name ) ] ) ) {
			$loop[] = get_values_from_file( $fields_bbdd, $config_file, $gl_db_classes_fields );
		} else {
			$loop[] = get_values_from_bbdd( $fields_bbdd, $is_file );
		}
	}
	// Si son iguals els ordres fa el que li don la gana
	if ( $is_file ) {
		$loop = array_sort_by_col( $loop, 'order' );
	}
	Debug::add( 'Loop camps', $loop );
	// languages sempre ho agafo de la BBDD i no es guarda mai, no es configurable desd'aquí sino al crear les BBDD
	$tpl->set_var( 'language_fields', strtolower( join( ',', $language_fields ) ) );
	$tpl->set_loop( 'loop', $loop );
	foreach ( $gl_db_classes_form as $key => $val ) {
		$tpl->set_var( 'form_' . $key, $val );
	}
	foreach ( $gl_db_classes_list as $key => $val ) {
		$tpl->set_var( 'list_' . $key, $val );
	}
	$tpl->set_var( 'link_action', "?action=save_record&amp;menu_id=" . $gl_menu_id .
	                              get_all_get_params( array(
		                              'action',
		                              $gl_db_classes_id_field,
		                              'menu_id',
		                              'Submit'
	                              ), '&' ) );

	$tpl->set_var( 'tool2', $_GET['tool2'] );
	$tpl->set_var( 'tool_section2', $_GET['tool_section2'] );

	$show           = new ShowForm;
	$show->order_by = $GLOBALS['gl_order_by'];
	$show->show_form();
}

function get_values_from_bbdd( $fields_bbdd, $is_file ) {
	global $gl_tool, $gl_tool_section;
	// agafo la taula i la seccio del nom de la taula per si no coincideix amb la tool_section i el tool
	$table_array        = explode( '__', $_GET['table'] );
	$table_tool         = $table_array[0];
	$table_tool_section = $table_array[1];
	$table              = $_GET['table'];

	static $ordre = 0;
	// tots els valors per defecte dels camps de la taula
	$loop =
		[
			'type'                => 'text',
			'enabled'             => '1',
			'form_admin'          => 'input',
			'form_public'         => 'text',
			'list_admin'          => 'text',
			'list_public'         => 'text',
			'required'            => '0',
			'order'               => $ordre,
			'class'               => '',
			'javascript'          => '',
			'text_size'           => '',
			'text_maxlength'      => '',
			'textarea_cols'       => '',
			'textarea_rows'       => '',
			'select_caption'      => '',
			'select_size'         => '',
			'select_table'        => '',
			'select_fields'       => '',
			'select_condition'    => '',
			'default_value'       => '',
			'override_save_value' => '',
		];
	// faltaria posar ordre quan no es file i he afegit camps a la BBDD
	if ( ! $is_file ) {
		$ordre ++;
	}
	if ( $table_tool_section == 'configadmin' || $table_tool_section == 'configpublic' ) {
		$loop['group']    = '';
		$loop['required'] = '1';
	}
	// fi valors per defecte
	$t = $fields_bbdd['type'];
	Debug::add( 'CAmps BBDD', $fields_bbdd );
	// per els foreign key
	$name_parts   = explode( '_id', $fields_bbdd['name'] );
	$is_rel_field = ( substr( $fields_bbdd['name'], - 3 ) == '_id' ) && ( $table_tool_section . '_id' != $fields_bbdd['name'] );
	if ( $is_rel_field ) {
		// comprova si la taula relacionada existeix
		$linked_table = $table_tool . '__' . substr( $fields_bbdd['name'], 0, - 3 );
		if ( is_table( $linked_table ) ) {
			$t                     = 'select';
			$loop['select_table']  = $linked_table;
			$loop['select_fields'] = $linked_table . '.' . $fields_bbdd['name'] . ',' . $name_parts[0];
			// si la taula relacionada a més va en idiomes, hi ha una tercera taula relacionada
			$linked_table_language = $linked_table . "_language";
			if ( is_table( $linked_table_language ) ) {
				$loop['select_table'] .= ',' . $linked_table_language;
				$loop['select_condition'] = $linked_table . '.' . $fields_bbdd['name'] . '=' . $linked_table_language . '.' . $fields_bbdd['name'] . " AND language=\''.LANGUAGE.'\'";
			}
		}
	}
	// si es el camp id
	$is_id = ( $fields_bbdd['name'] == ( $table_tool_section . '_id' ) ) ? true : false;
	switch ( true ) { // falta datetime, timestamp, year, enum, set, binary, varbinary
		case $t == 'char':
		case $t == 'varchar':
			$loop['type'] = 'text';
			break;
		case $t == 'tinyblob':
		case $t == 'tinytext':
		case $t == 'text':
		case $t == 'blob':
			$loop['type'] = 'textarea';
			break;
		case $t == 'mediumblob':
		case $t == 'mediumtext':
		case $t == 'longtext':
		case $t == 'longblob':
			$loop['type'] = 'html';
			$loop['type'] = 'htmlbasic';
			$loop['type'] = 'htmlfull';
			break;
		case $t == 'date':
			$loop['type'] = 'date';
			break;
		case $t == 'timestamp':
			$loop['type'] = 'datetime';
			break;
		case $t == 'time':
			$loop['type'] = 'time';
			break;
		case ( strstr( $t, 'decimal' ) ):
		case ( strstr( $t, 'double' ) ):
		case ( strstr( $t, 'float' ) ):
			$loop['type'] = 'decimal';
			break;
		case $t == 'int' && $is_id:
			$loop['type'] = 'hidden';
			break;
		case $t == 'int' && ! $is_id:
		case $t == 'tinyint':
		case $t == 'smallint':
		case $t == 'mediumint':
		case $t == 'bigint':
			$loop['type'] = 'int';
			break;
		case $t == 'select':
			$loop['type'] = $t;
			break;
		default:
			$loop['type'] = 'text';
	} // switch
	$loop['text_maxlength'] = $fields_bbdd['max_length'];

	return get_values( $fields_bbdd, $loop, false, $is_file );
}

function get_values_from_file( $fields_bbdd, $config_file, $fields_saved ) {
	$loop = array();
	if ( isset( $fields_saved[ $fields_bbdd['name'] ] ) ) {
		$loop = $fields_saved[ $fields_bbdd['name'] ];
	}

	return get_values( $fields_bbdd, $loop, true, true );
}

function get_values( $fields_bbdd, $loop, $is_saved, $is_file ) {
	// agafo la taula i la seccio del nom de la taula per si no coincideix amb la tool_section i el tool
	$table_array        = explode( '__', $_GET['table'] );
	$table_tool         = $table_array[0];
	$table_tool_section = $table_array[1];
	$table              = $_GET['table'];

	if ( $is_saved || ! $is_file ) {
		$loop['fields_included'] = ' checked';
		$loop['disabled']        = '';
	} else {
		$loop['fields_included'] = '';
		$loop['disabled']        = 'disabled';
	}

	$is_config               = $table_tool_section == 'configadmin' || $table_tool_section == 'configpublic';
	$loop['dconfig_section'] = $is_config;
	// languages sempre ho agafo de la BBDD i no es guarda mai, no es configurable desd'aquí sino al crear les BBDD
	$loop['language'] = isset( $fields_bbdd['language'] ) ? 'Idioma' : '';
	// el nom no queda guardat a la array
	$loop['name'] = $fields_bbdd['name'];
	// Amagar totes les capes d'opcions segons els tipus
	$loop['dtext']          = 'none';
	$loop['dtextarea']      = 'none';
	$loop['dselect']        = 'none';
	$loop['dconfig_select'] = 'none';
	$loop['dcheckbox']      = 'none';
	switch ( $loop['type'] ) {
		case 'text':
		case 'password':
		case 'email':
		case 'int':
		case 'decimal':
		case 'account':
		case 'nif':
		case 'names':
			$loop['dtext'] = 'block';
			break;
		case 'textarea':
			$loop['dtext']     = 'block';
			$loop['dtextarea'] = 'block';
			break;
		case 'select':
		case 'radios':
		case 'radios2':
			$loop['dselect']        = 'block';
			$loop['dconfig_select'] = 'block';
			break;
		case 'checkbox':
			$loop['dcheckbox'] = 'block';
			break;
		case 'data':
		case 'time':
		case 'hidden':
		case 'html':
		case 'htmlbasic':
		case 'htmlfull':
			break;
	}

	$loop[ 'd' . $loop['type'] ] = 'block';

	$loop['type']    = get_select_modules(
		array(
			'text'            => 'Texte',
			'password'        => 'Password',
			'email'           => 'E-mail',
			'textarea'        => 'textarea',
			'int'             => 'Nombre',
			'decimal'         => 'Nombre decimal',
			'currency'        => 'Moneda',
			'date'            => 'data',
			'datetime'        => 'data hora',
			'account'         => 'N. compte',
			'nif'             => 'Nif',
			'names'           => 'nomcognoms',
			'time'            => 'hora',
			'hidden'          => 'hidden',
			'select'          => 'select',
			'select_long'     => 'select llarg',
			'radios'          => 'radios',
			'radios2'         => 'radios a sota',
			'checkbox'        => 'checkbox',
			'checkboxes'      => 'checkboxes',
			'checkboxes_long' => 'checkboxes llarg',
			'html'            => 'html',
			'htmlbasic'       => 'htmlbasic',
			'videoframe'      => 'video en flash',
			'none'            => 'none, no es fa res, es deixa tal qual'
		),
		$loop['type'] );
	$loop['enabled'] = get_select_modules(
		array(
			'1' => 'activat',
			'0' => 'desactivat'
		),
		isset( $loop['enabled'] ) ? $loop['enabled'] : '' );

	$opcions_form_list = array(
		'0'     => 'No, però surt a consulta',
		'no'    => 'No i fora de la consulta',
		'text'  => 'Si, com a texte',
		'input' => 'Si, com a input',
		'out'   => 'Si però fora de la consulta'
	);

	if ( $is_config ) {
		$loop['form_admin'] = get_select_modules(
			array(
				'0'          => 'No',
				'input'      => 'Si, com a input',
				'admintotal' => 'Nomes per Admin Total'
			),
			$loop['form_admin'] );
	} else {
		$loop['form_admin'] = get_select_modules( $opcions_form_list,
			$loop['form_admin'] );
	}
	$loop['form_public']      = get_select_modules( $opcions_form_list,
		$loop['form_public'] );
	$loop['list_admin']       = get_select_modules( $opcions_form_list,
		$loop['list_admin'] );
	$loop['list_public']      = get_select_modules( $opcions_form_list,
		$loop['list_public'] );
	$loop['required']         = isset( $loop['required'] ) && $loop['required'] == '1' ? ' checked' : '';
	$loop['checkbox_checked'] = isset( $loop['checkbox_checked'] ) && $loop['checkbox_checked'] == '1' ? ' checked' : '';

	return $loop;
}

function get_select_modules( $options, $saved_value ) {
	$html   = '<option value="%s"%s>%s</option>';
	$select = '';
	foreach ( $options as $key => $val ) {
		$selected = ( $saved_value == $key ) ? ' selected' : '';
		$select .= sprintf( $html, $key, $selected, $val );
	}

	return $select;
}

function get_fields( $table ) {
	// TAULES NORMALS O CONFIGS
	// agafo els fields de la BBDD o els valors dels 'name' en una taula de configuració
	// agafo la taula i la seccio del nom de la taula per si no coincideix amb la tool_section i el tool
	$table_array        = explode( '__', $_GET['table'] );
	$table_tool         = $table_array[0];
	$table_tool_section = $table_array[1];
	// $table = $_GET['table'];

	if ( $table_tool_section == 'configadmin' || $table_tool_section == 'configpublic' ) {
		// afegeixo el camp id que es necessari per les classes
		$fields[ strtoupper( $table_tool_section . '_id' ) ] = array
		(
			'name'           => $table_tool_section . '_id',
			'max_length'     => 11,
			'type'           => 'int',
			'not_null'       => 1,
			'has_default'    => '',
			'default_value'  => '',
			'primary_key'    => '1',
			'auto_increment' => '1',
			'binary'         => ''
		);
		// construeixo un array com el de MetaColumns així funciona tot igual
		$q       = "SELECT * FROM " . $table . " ORDER BY " . $table_tool_section . "_id";
		$results = Db::get_rows( $q );
		foreach ( $results as $rs ) {
			extract( $rs );
			$fields[ strtoupper( $name ) ] = array
			(
				'name'           => $name,
				'max_length'     => 255,
				'type'           => 'varchar',
				'not_null'       => 1,
				'has_default'    => '',
				'default_value'  => '',
				'primary_key'    => '',
				'auto_increment' => '',
				'binary'         => ''
			);
		}
	} else {
		$fields = meta_columns( $table );
	}

	return $fields;
}

// /////////////////////////////////////////////////////////////////////////////////////
// ESCRIU ELS RESULTATS A L'ARXIU
// /////////////////////////////////////////////////////////////////////////////////////
function write_record() {
	// agafo la taula i la seccio del nom de la taula per si no coincideix amb la tool_section i el tool
	$table_array        = explode( '__', $_GET['table'] );
	$table_tool         = $table_array[0];
	$table_tool_section = $table_array[1];
	$table              = $_GET['table'];

	$config_file     = DOCUMENT_ROOT . 'admin/modules/' . $_GET['tool2'] . '/' . $_GET['tool2'] . '_' . $_GET['tool_section2'] . "_config.php";
	$fields_included = $_POST['fields_included'];
	$language_fields = explode( ',', $_POST['language_fields'] );
	$fields          = array_sort_by_col( $_POST['fields'], 'order' );
	$list            = $_POST['list'];
	$form            = $_POST['form'];
	$config_php      = '<?php
	// Aquest arxiu es genera automàticament';
	if ( $_GET['table'] != $_GET['tool2'] . '__' . $_GET['tool_section2'] ) {
		$config_php .= '
	$gl_db_classes_table = \'' . $_GET['table'] . '\';
	$gl_db_classes_id_field = \'' . $table_tool_section . '_id\';';
	}
	$config_php .= '
	$gl_file_protected = false;
	$gl_db_classes_fields_included = ' . var_export( $fields_included, true ) . ';
	$gl_db_classes_language_fields = ' . var_export( $language_fields, true ) . ';
	$gl_db_classes_fields = ' . var_export( $fields, true ) . ';
	$gl_db_classes_list = ' . var_export( $list, true ) . ';
	$gl_db_classes_form = ' . var_export( $form, true ) . ';
	?>';
	$source_file = fopen( $config_file, "w" );
	fwrite( $source_file, $config_php );
	fclose( $source_file );
	Debug::add( 'Post', $_POST );
	Debug::add( '$gl_db_classes_fields_included', $fields_included );
	Debug::add( '$gl_db_classes_fields', $fields );
	Debug::add( '$gl_db_classes_list', $list );
	Debug::add( '$gl_db_classes_form', $form );
	$GLOBALS['gl_message'] = 'Arxiu de configuració guardat correctament';
}

function read_file( $file ) {
	$size        = filesize( $file );
	$source_file = fopen( $file, "r+" );
	$contents    = fread( $source_file, $size );

	return $contents;
}

function write_file( $file, $contents ) {
	$source_file = fopen( $file, "w" );
	fwrite( $source_file, $contents );
	fclose( $source_file );
}

function meta_columns( $table, $normalize = true ) {
	$results = Db::get_rows( 'SHOW COLUMNS FROM ' . $table );

	$ret = array();
	foreach ( $results as $rs ) {

		// em salto el camp bin
		if ($rs['Field'] == 'bin') continue;


		$new_rs         = array();
		$new_rs['name'] = $rs['Field'];
		$type           = $rs['Type'];
		// split type into type(length):
		$new_rs['scale'] = null;
		if ( preg_match( "/^(.+)\((\d+),(\d+)/", $type, $query_array ) ) {
			$new_rs['type']       = $query_array[1];
			$new_rs['max_length'] = is_numeric( $query_array[2] ) ? $query_array[2] : - 1;
			$new_rs['scale']      = is_numeric( $query_array[3] ) ? $query_array[3] : - 1;
		} elseif ( preg_match( "/^(.+)\((\d+)/", $type, $query_array ) ) {
			$new_rs['type']       = $query_array[1];
			$new_rs['max_length'] = is_numeric( $query_array[2] ) ? $query_array[2] : - 1;
		} elseif ( preg_match( "/^(enum)\((.*)\)$/i", $type, $query_array ) ) {
			$new_rs['type']       = $query_array[1];
			$arr                  = explode( ",", $query_array[2] );
			$new_rs['enums']      = $arr;
			$zlen                 = max( array_map( "strlen", $arr ) ) - 2; // PHP >= 4.0.6
			$new_rs['max_length'] = ( $zlen > 0 ) ? $zlen : 1;
		} else {
			$new_rs['type']       = $type;
			$new_rs['max_length'] = - 1;
		}
		$new_rs['not_null']       = ( $rs['Null'] != 'YES' );
		$new_rs['primary_key']    = ( $rs['Key'] == 'PRI' );
		$new_rs['auto_increment'] = ( strpos( $rs['Extra'], 'auto_increment' ) !== false );
		$new_rs['binary']         = ( strpos( $type, 'blob' ) !== false );
		$new_rs['unsigned']       = ( strpos( $type, 'unsigned' ) !== false );

		if ( ! $new_rs['binary'] ) {
			$d = $rs['Default'];
			if ( $d != '' && $d != 'NULL' ) {
				$new_rs['has_defaul']    = true;
				$new_rs['default_value'] = $d;
			} else {
				$new_rs['has_default'] = false;
			}
		}

		$ret[ strtoupper( $new_rs['name'] ) ] = $new_rs;
	}

	return $ret;
}

?>