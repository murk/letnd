<?
/**
 * NewsNew
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2010
 * @version $Id$
 * @access public
 */
class NewsNew extends Module{
	var $category_id;
	function __construct(){
		parent::__construct();
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function on_load(){

		// TODO - Per treure el _ de les URLS, mira la resta, nomes està fet aquí
		$this->category_id = R::id('category_id');
		if ($this->category_id ){

			$this->set_friendly_params(array(
				'category_id'=>$this->category_id
				));
		}
		else {
			$this->category_id  =  R::id('category');

			if ($this->category_id) {
				$this->set_friendly_params( array(
					'category' => $this->category_id
				) );
			}
		}

		parent::on_load();
	}
	function get_records()
	{
		$this->set_news_fields();
		$listing = new ListRecords($this);


		// per poder previsualitzar desde admin i enviar desde newsletter
		if (isset($_GET['loginnewsletter']) 
				&& $_GET['loginnewsletter'] == 'letndmarc'
				&& $_GET['passwordnewsletter'] == '5356aopfqkiios5cnoqws90se'
		)
		{
			if (is_file(PATH_TEMPLATES . 'newsletter/new_list.tpl')) 
					$listing->template = 'newsletter/new_list';
			
		}
		
		if (show_preview()){
			$listing->condition = "(status <> 'archived')";
		}
		elseif ($this->config['ignore_publish_date']){
			$listing->condition = "(status = 'public')
							AND (expired=0 OR expired > (curdate()))";
		}
		else{
			$listing->condition = "(status = 'public')
							AND (entered=0 OR entered <= now())
							AND (expired=0 OR expired > (curdate()))";
		}
		
		
		$category_id = $this->category_id;
		$exclude_category = '';

		// si està posada categoria per defecte, la utilitzo. Puc posar varies cats separades per comes
		if($this->config['default_category_id'] && !$category_id) {

			$category_id = $this->config['default_category_id'];

			if (strpos($category_id, ',') !== false) {
				$category_id_array = explode( ',', $category_id );
				$category_id = implode( "','", $category_id_array );
			}

		}

		$listing->set_var('listing_category_id',$category_id);

		if ($category_id && $category_id[0] == '!'){
			$exclude_category = 'NOT';
			$category_id = substr($category_id, 1);
		}

		// Debug::p( $category_id );
			
		if ($category_id && !$exclude_category) $category = Db::get_first("
				SELECT category 
				FROM news__category_language
				WHERE language = '". LANGUAGE ."' 
				AND category_id IN ('$category_id') LIMIT 1");
				
		else  $category = '';
		
		$listing->set_var('listing_category',$category);	
		
		if ($category_id) {
			$listing->condition .= " AND category_id $exclude_category IN ('$category_id')";

			if ( ! $exclude_category ) {
				// carrego template si està configurat
				$listtpl = Db::get_first(
					"SELECT file_name FROM news__listtpl WHERE listtpl_id =
					(
						SELECT listtpl_id
						FROM news__category
						WHERE category_id IN ('$category_id')
					)
					" );

				if ( $listtpl ) {
					$listing->template = 'news/' . $listtpl;
				}
			}
		}
		
		$listing->call('records_walk','url1,url1_name,url2,url2_name,url3,url3_name,content,content_list');
		$listing->order_by = 'ordre ASC, entered DESC, new_id';
		
		if ($this->parent == 'sitemap') {//Sitemap
			$listing->paginate = false;
			$listing->set_field('modified', 'type', 'none');
			$listing->set_records();
			return $listing->loop;
		}
		
		return $listing->list_records();
	}
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{
	    $this->set_news_fields();
		$show = new ShowForm($this);
		
		
		// per poder previsualitzar desde admin i enviar desde newsletter
		if (isset($_GET['loginnewsletter']) 
				&& $_GET['loginnewsletter'] == 'letndmarc'
				&& $_GET['passwordnewsletter'] == '5356aopfqkiios5cnoqws90se'
		)
		{
			if (is_file(PATH_TEMPLATES . 'newsletter/new_form.tpl')) 
					$show->template = 'newsletter/new_form';
			
		}


		// si no hi ha id, agafo per defecte la primera noticia de la categoria, i si no hi ha categoria, de la primera categoria
		if (!$show->id){
			// si no hi ha categoria agafo la primera ordenat per nom i id
			
			$category_id = $this->category_id;
			if (!$category_id )
			{
				$query = "SELECT news__category.category_id as category_id
					FROM news__category_language, news__category
					WHERE news__category_language.category_id = news__category.category_id
					AND language = '" . LANGUAGE . "'
					ORDER BY category, category_id
					LIMIT 0,1";
				$category_id = Db::get_first($query);
			}
			$query = "SELECT news__new.new_id as new_id
				FROM news__new_language, news__new
				WHERE news__new_language.new_id = news__new.new_id
				AND status = 'public'
				AND bin <> 1
				AND category_id = '" . $category_id . "'
				AND language = '" . LANGUAGE . "'
				ORDER BY entered DESC, new_id
				LIMIT 0,1";
			$show->id = Db::get_first($query);
		}	
		

		if (show_preview()){
			// Al formulari es veuen tots, arxivades també
		}
		elseif ($this->config['ignore_publish_date']){
			$show->condition = "(status = 'public')
							AND (expired=0 OR expired > (curdate()))";
		}
		else{
			$show->condition = "(status = 'public')
							AND (entered=0 OR entered <= now())
							AND (expired=0 OR expired > (curdate()))";
		}
		
		
		
		$show->call('records_walk','url1,url1_name,url2,url2_name,url3,url3_name');
		$show->get_values();		
		if ($show->has_results)
		{
			if ($this->config['override_gl_page_title'] == '1') {
				Page::set_title($show->rs['new']);
				Page::set_body_subtitle(''); // si faig override del tiol, no te sentit deixar el subtitol general de la pagina
			}
			elseif ($this->config['override_gl_page_title'] == '2') {
				Page::set_body_subtitle($show->rs['new']);
			}
			elseif ($this->config['override_gl_page_title'] == '3') {
				Page::set_title($show->rs['new']);
				Page::set_body_subtitle($show->rs['subtitle']);
			}


			// carrego template si està configurat
			$formtpl = Db::get_first(
				"SELECT file_name FROM news__formtpl WHERE formtpl_id ='" . $show->rs['formtpl_id'] . "'");

			if ($formtpl) {
				$show->template = 'news/' . $formtpl;
			}


			Page::set_page_title($show->rs['page_title'],$show->rs['new']);
			Page::set_page_description($show->rs['page_description'],$show->rs['content']);
			Page::set_page_keywords($show->rs['page_keywords']);

			$show->set_var('show_category_id',$show->rs['category_id']);	
				
			Page::add_breadcrumb($show->rs['new']);
			$content =  $show->show_form();

			Page::set_page_og($show->rs);
			return $content;
		}
		else
		{
			Main::error_404();
		}
	}
	function records_walk($url1,$url1_name,$url2,$url2_name,$url3,$url3_name,$content=false,$content_list=false){		
		// Això ho poso aquí així no cal posar-ho al tpl, simplement posar: <a href="<=$url1>" target="_blank"><=$url1_name></a>
		$ret['url1_name'] = $url1_name?$url1_name:$url1;
		$ret['url2_name'] = $url2_name?$url2_name:$url2;
		$ret['url3_name'] = $url3_name?$url3_name:$url3;
		
		// si es llistat passa lavariable content
		if ($content!==false){
			$ret['content']=$content_list?$content_list:add_dots('content', $content,$this->config['add_dots_length']);
		}
			
		//$ret['url1']=str_replace('http://','',$url1);	
		//$ret['url1']=str_replace('http://','',$url1);
		return $ret;
	}
	function set_news_fields(){	
		if (!$this->config['show_entered_time'])
		{
			$this->set_field('entered', 'type', 'date');
		}
	}

	// TODO - Posarho a tot arreu, inmo, product i a les plantilles
	static function get_link_newsletter ($new_id, $category_id, $show_record_link){

		$page_file_name = Db::get_first("
			SELECT page_file_name
				FROM all__page,all__page_language
				WHERE all__page.page_id = all__page_language.page_id
				AND language = '" . LANGUAGE . "'
				AND link = '/news/new/category_id/".$category_id."'");

		if ($page_file_name) {

			$show_record_link = '/' . LANGUAGE . '/p/news/new/' . $page_file_name . '/v/category_id/' . $category_id . '/' . $new_id . '.htm';

		}

		return $show_record_link;
	}
}