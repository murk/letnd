<?
/**
 * ProductCustomer
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2010
 * @version $Id$
 * @access public
 */
class ProductCustomer extends Module{
	var $menus, $menu_id, $never_mind_logged = false, $is_test = false, $is_shop_app = false; // per sobreescriure el session_id quan no n'hi ha ( distribuidors )
	function __construct(){
		parent::__construct();
	}
	function on_load(){
		// si es privada no deixo fer res excepte entrar login i password
		// està tot controlat a do_action , aquí nomès es pot entrar login, registrar-se i enviar pass i enviar mail registrat sense estar logejat, sigui is_private_shop o no
		/*if ($this->action!='login' && $this->action!='show_form_new' && $this->action!='add_record' && $this->action!='send_password' && $this->is_index && $this->config['is_private_shop'] && !$this->is_logged()) {
			redirect('/?tool=product&tool_section=customer&action=login&language='.LANGUAGE);
			die();
		}*/

		if (is_file(CLIENT_PATH . '/product_customer_config.php')){
			include (CLIENT_PATH . '/product_customer_config.php');
			if (isset($fields)) $this->fields = array_merge ($this->fields, $fields);
		}


	}
	function list_records()
	{
		
		$this->show_login(); // accio per defecte en el modul
		//$GLOBALS['gl_content'] = $this->get_records();
	}
	function get_records()
	{
	    //$listing = new ListRecords($this);
	    //return $listing->list_records();
	}
	function show_form()
	{
		// Nomes poso el menu de customer quan aquest modul es index ( o sigui no el crido desde cap altre modul)
		if ($this->is_logged() && $this->is_index) {
			$GLOBALS['gl_content'] = $this->get_menu_customer() . $this->get_form();
			$GLOBALS['gl_page']->title = $this->caption['c_custumer_menu_personal_data'];
		}
		else{
			$GLOBALS['gl_content'] = $this->get_form();
			$GLOBALS['gl_page']->title = $this->caption['c_title_register'];
		}
	}
	function get_form()
	{
		if ($this->process=='identify') {
			$this->caption['c_send_edit'] = $this->caption['c_next_payment_step'];
		}

		$this->merge_module('product', 'address');

		$show = new ShowForm($this);
		$show->before_table = 'product__address, ';
		$show->condition = 'product__customer.customer_id = product__address.customer_id AND invoice=\'1\'';

		$show->set_var('is_summary',false);
		$show->set_var('is_payment',$this->parent=='ProductPayment');
		//$show->set_var('is_edit_payment',$this->parent=='ProductPayment' && $this->action=='get_form_edit');

		$show->get_values();
		if ($this->action == 'show_form_new' || $this->action == 'get_form_new') {
			// desplegable country per la segona adreça
			$this->set_var (
			'country_id2',
			get_select('country_id2', '', 'ES',$this->fields['country_id']));
			$this->set_var (
			'provincia_id2',
			get_select('provincia_id2', '', '',$this->fields['provincia_id']));
		}

		// al form no surt nom i cognom de l'adreça de facturació, es la mateixa que posem al customer
		if ($this->action!='get_record') {
			$show->set_field('a_name','form_public','no');
			$show->set_field('a_surname','form_public','no');
		}



		// si ve de summary
		if ($this->action=='get_record'){
			$show->set_var('edit_link','/?tool=product&tool_section=payment&action=edit_customer&process=identify&language='.$GLOBALS['gl_language']);
			$show->set_var('is_summary',true);

			// obtinc adresses
			$module = Module::load('product', 'address');
			$module->invoice_id = $show->rs['address_id'];
			$module->delivery_id = $show->rs['delivery_id'];
			$show->set_vars($module->do_action('get_addresses'));

			// Obtinc Collect
			$collect_vars = $this->get_collect_vars();
			$show->set_vars($collect_vars);

		}

		// si editem tant de payment com de customer
		if ($this->action=='show_form_edit'  || $this->action=='get_form_edit'){
			$module = Module::load('product', 'address');
			$module->caption['c_form_title'] = '';
			$module->customer_id = $show->rs['customer_id'];
			$module->delivery_id = $show->rs['delivery_id'];
			$addresses  = $module->do_action('get_records');
			$show->set_var('addresses',$addresses);

			$this->set_wholesaler_vars( $show );
		}
	    return $show->show_form();
	}

	private function set_wholesaler_vars( &$show ) {
		$is_wholesaler = $show->rs['is_wholesaler'];
		if ( ! $is_wholesaler ) return;

		if (!$this->config['can_wholesaler_edit_company']) $show->set_field( 'company', 'form_public', 'text' );
		if (!$this->config['can_wholesaler_edit_dni']) $show->set_field( 'dni', 'form_public', 'text' );


	}
	function get_collect_vars(){
		// en el summary poso un checkbox de recollida en botiga
		// $collect = '';
		// if ( $this->parent == 'ProductPayment' )
			$collect = !empty($_SESSION['product']['cart_vars']['collect'])?$_SESSION['product']['cart_vars']['collect']:'0';

			$checked = $collect == '1'?' checked':'';

			$collect = "<input onclick='payment_set_collect();' $checked value='1' type=\"checkbox\" id='collect' name=\"collect\">";


		return [
			'can_collect' => $this->config['can_collect'] == 1,
			'collect'     => $collect
		];
	}
	function list_order()
	{
		$module = Module::load('product','order', $this);
		$module->is_logged = true;
		$orders = $module->do_action('get_records');

		$GLOBALS['gl_content'] = $this->get_menu_customer() . $orders;
		$GLOBALS['gl_page']->title = $this->caption['c_custumer_menu_orders'];
	}
	function show_order()
	{
		$module = Module::load('product','order');
		$module->is_logged = true;
		$order = $module->do_action('get_record');

		$GLOBALS['gl_content'] = $this->get_menu_customer() . $order;
		$GLOBALS['gl_page']->title = $this->caption['c_custumer_menu_orders'];
	}
	function show_invoice()
	{
		$module = Module::load('product','order');
		$module->is_logged = true;
		$module->is_invoice = R::text_id('invoice');		
		$order = $module->do_action('get_record');		

		$GLOBALS['gl_content'] = $order;
	}

	function save_rows()
	{
	    //$save_rows = new SaveRows($this);
	    //$save_rows->save();
	}

	function write_record()
	{
	    $writerec = new SaveRows($this);

		// no valida els del from d'adreça: Adreça, CP, Població ( de moment es igual )
		if ( !$writerec->has_requireds()) return;

		$writerec->field_unique = "mail";

		$country = $writerec->get_value( 'country_id' );
		if ($country != 'ES') $writerec->set_value( 'provincia_id', 0 );
		
		$is_wholesaler = $writerec->get_value('is_wholesaler');
		
		if ($this->action=='add_record') {
			// si es botiga privada, deixo registrar-se però no deixo entar fins que no s'hagi validat el client desde la intranet
			if ($this->config['activate_customer_by_default'])
			// Faig el login per que m'acabo de registrar
			// més segur amb 2 comprovacions
			{
				// en una botiga on s'activa automaticament ( la majoria ), si es distribuidor, no s'activa
				// i el clients registrats son actius automaticament, ja que han d'efectuar una compra inmediata que es el que interessa
								
				if ($is_wholesaler) {					
					$writerec->set_field('activated','override_save_value','0');
				}
				else{					
					$writerec->set_field('activated','override_save_value','1');					
				}
			}
			else
			{				
				// en una on no s'activa el client automaticament, el que es registra pot triar ser distribuidor o client normal, ja que sempre s'ha de validar desde la intranet per activar el client
				$writerec->set_field('activated','override_save_value','0');
			}
			// si no hi ha el select de prefered language en el formulari, poso l'idioma en que es mira
			if (!$writerec->get_value('prefered_language')) $writerec->set_field('prefered_language','override_save_value',"'" . LANGUAGE . "'");
		}
		
		
		
	    $writerec->save();
		
		// poso el remove key automaticament
		if ($this->action=='add_record' && $GLOBALS['gl_insert_id'])
			$this->newsletter_add_remove_key('product__customer', $GLOBALS['gl_insert_id']);

		$message = isset($GLOBALS['gl_errors']['add_exists'])?$GLOBALS['gl_errors']['add_exists']:'';

		if (!$writerec->saved) return; // si no ha grabat (ej. add_exists) no faig res més

		$name = current($_POST['name']);
		$surname = current($_POST['surname']);
		
		// primera adreça
		// canvio l'id del post per guardar automatic amb save_records
		if ($this->action=='save_record')
		{
			// definir l'address_id;
			$address_id = current($_POST['address_id']);
			$unset_id = current($_POST['customer_id']);
			foreach ($_POST as $key=>$val){
				if (is_array($val) && array_key_exists($unset_id,$val)){
					unset($_POST[$key][$unset_id]);
					$_POST[$key][$address_id]=$val[$unset_id];
				}
			}
		}
		$module = Module::load('product','address');
		$module->parent = 'ProductCustomer';
		if ($this->action=='add_record') {
			$customer_id = $GLOBALS['gl_insert_id'];
			$module->customer_id = $customer_id;
			if ($this->config['activate_customer_by_default'] && !$is_wholesaler)
			// Faig el login per que m'acabo de registrar
			// més segur amb 2 comprovacions
			{
				$_SESSION['product']['customer_id'] = $customer_id;
				$_SESSION['product']['mail'] = current($_POST['mail']);
				$_SESSION['product']['is_wholesaler'] = false; // no es pot crear un distribuidor desde el site public
				$_SESSION['product']['name'] = current($_POST['name']);
				$_SESSION['product']['surname'] = current($_POST['surname']);
			}
			$module->set_field('invoice','override_save_value','1'); // aquesta adreça es la de facturació
		}
		$module->do_action($this->action);

		// segona adreça, nomes es crea nova al crear nou usuari, sino es crea nova desde una finestra al seu modul corresponent
		if (!isset($_POST['same_address']) && ($this->action=='add_record')) {
			$query = "INSERT INTO product__address SET
			customer_id = " . Db::qstr($customer_id) . ",
			a_name = " . Db::qstr($_POST['a_name2']) . ",
			a_surname = " . Db::qstr($_POST['a_surname2']) . ",
			address = " . Db::qstr($_POST['address2']) . ",
			zip = " . Db::qstr($_POST['zip2']) . ",
			city =" . Db::qstr($_POST['city2']) . ",
			invoice = 0";

			$country_id2 = R::text_id( 'country_id2', '', '_POST' );
			$provincia_id2 = R::text_id( 'provincia_id2', '', '_POST' );

			if ( $country_id2 ) {
				$query .= ",
							country_id = " . Db::qstr($country_id2);
			}

			if ( $country_id2 == 'ES' && $provincia_id2 ) {
				$query .= ",
							provincia_id = " . Db::qstr($provincia_id2);
			}
			Debug::p( $query );
			Db::execute($query);

			// relaciono el customer amb aquesta adreça per saber quina es la de entrega
			$query = 'UPDATE product__customer SET delivery_id = ' . Db::insert_id() .
				' WHERE customer_id = ' . $customer_id;
			Db::execute($query);
		}

		if ($this->action=='save_record') {
			// canvio el delivery_id segons convingui

		}
		if ($message) $GLOBALS['gl_message'] = $message;

		// si estic a payment desprès de guardar vaig al summary
		if ($this->process == 'identify') {
			print_javascript("top.window.location.href = '/?tool=product&tool_section=payment&process=identify&action=login&language=" . $GLOBALS['gl_language'] . "'");
		}
		// si es nou registre redirigeixo al menu de custormer
		elseif ($this->action=='add_record' && $this->config['activate_customer_by_default'] && !$is_wholesaler) {
			$this->send_mail_registered($customer_id, true);
			print_javascript("top.window.location.href = '".str_replace('&amp;','&',$this->link)."&action=show_login'");
		}
		// si es nou registre i no s'ha activat mostro missatge i redirigeixo a mostrar missatge
		elseif ($this->action=='add_record') {	
			
			$_SESSION['message'] = $this->messages['registered_succesful'];	
			$this->send_mail_registered($customer_id, false);
			print_javascript("top.window.location.href = '".str_replace('&amp;','&',$this->link)."&action=show_message'");
			Debug::p_all();
			die();
		}

	}
	function send_mail_registered($customer_id, $activated){

		// if ($GLOBALS['gl_is_local']) return;

		$this->never_mind_logged = true;
		$this->action = 'get_record';
		
		// Agafo formulari del customer, però amb el form.tpl per defecte
		$show = new ShowForm($this);
		$show->id = $customer_id;
		$show->set_field('password','form_public','no');
		$show->use_default_template = true;
		$customer = $show->show_form();
		
		$page = clone $GLOBALS['gl_page'];
		$page->template = 'mail.tpl';
		$page->set_var('preview','');
		$page->set_var('remove_text','');
		$page->content = $this->caption['c_body_1'] . ': <br>' . $customer;
			
		// afegeixo missatge d'activar client quan la activació no es automatica
		if (!$activated){
		$page->content .= '<br><br><br>' . 
			sprintf($this->caption['c_body_2'],'<a href="/admin/?menu_id=20124">','</a>');
		
		}
		$body = $page->get();
		
		// envio mail
		$mailer = get_mailer($this->config);
		if (is_file(CLIENT_PATH . 'images/logo_newsletter.jpg'))
			$mailer->AddEmbeddedImage(CLIENT_PATH . 'images/logo_newsletter.jpg', 'logo', 'logo.jpg');
		elseif(is_file(CLIENT_PATH . 'images/logo_newsletter.gif')) 
			$mailer->AddEmbeddedImage(CLIENT_PATH . 'images/logo_newsletter.gif', 'logo', 'logo.gif');
		
		$mailer->Body = $body;
		$mailer->Subject = $this->caption['c_subject'];
		$mailer->IsHTML(true);
		$mailer->AddAddress ($this->config['default_mail']);
		
		send_mail($mailer, false);
	}
	function show_message(){
		// res, nomes es per mostrar el missatge i ja es fa automaticament a "message"
	}

	function manage_images()
	{
	    $image_manager = new ImageManager($this);
	    $image_manager->execute();
	}
	function show_login(){
		if ($this->is_logged()) {
			$this->show_menu_customer();
			if ($this->config['show_first_customer_menu']){
				$_GET['menu_id']=2;
				$this->do_action('list_order');
			}
		}
		else{
			$content = $this->get_login();
			$GLOBALS['gl_page']->title = $this->caption['c_title_login'];
			$GLOBALS['gl_content'] = $content;
		}
	}
	function get_login(){
		$this->set_file('product/customer_login.tpl');
		$this->set_vars($this->caption);
		$link_action = $this->link . "&action=login&process=".$this->process;
		$link_register = $this->process=='identify'?
			'/?tool=product&tool_section=payment&action=register&process=identify&language='. $GLOBALS['gl_language']:
			$this->link . '&action=show_form_new';
		$link_send_password = $this->process=='identify'?
			'/?tool=product&tool_section=payment&action=send_password&process=identify&language='. $GLOBALS['gl_language']:
			$this->link . '&action=send_password';

		$this->set_var('js_string',"return validar(document.theForm,'mail','".addslashes($this->caption['c_mail'])."','email1','password','".addslashes($this->caption['c_password'])."','text1');");
		$this->set_vars(array(
			'link_action'=>$link_action,
			'link_register'=>$link_register,
			'link_send_password'=>$link_send_password
			));
		return $this->process();
	}
	function show_menu_customer(){
		$this->menu_id = isset($_GET['menu_id'])?$_GET['menu_id']:0;
		$content = $this->get_menu_customer();
		$GLOBALS['gl_page']->title = $this->menus[$this->menu_id]['item'];
		$GLOBALS['gl_content'] = $content;

	}
	function get_menu_customer(){

		$this->menu_id = isset($_GET['menu_id'])?$_GET['menu_id']:0; // haig de repetir, no sempre vinc de show
		$tpl = new phemplate(PATH_TEMPLATES);
		$tpl->set_file('product/customer_menu.tpl');
		$tpl->set_vars($this->caption);
		$tpl->set_var('is_index', $this->menu_id==0?1:0);
		$tpl->set_var('link_home', $this->link. '&action=show_menu_customer');

		$this->menus = array(
			0=>array(
				'id'=>'2',
				'item'=>$this->caption['c_custumer_menu_orders'],
				'link'=>$this->link . "&action=list_order&menu_id=2",
				'conta'=>'1',
				'class'=>'first',
				'selected'=>$this->menu_id==2?1:0
			),
			1=>array(
				'id'=>'1',
				'item'=>$this->caption['c_custumer_menu_personal_data'],
				'link'=>$this->link . "&action=show_form_edit&menu_id=1",
				'conta'=>'0',
				'class'=>'',
				'selected'=>$this->menu_id==1?1:0,
			),
			2=>array(
				'id'=>'3',
				'item'=>$this->caption['c_custumer_menu_logout'],
				'link'=>$this->link . "&action=logout&menu_id=3",
				'conta'=>'2',
				'class'=>'last',
				'selected'=>$this->menu_id==3?1:0
			)
		);

		$tpl->set_loop('menus',$this->menus);

		return $tpl->process();
	}
	function login()
	{
		/* TODO-i Comprovar automàticament si hi ha la provincia entrada i ino redirigir al formulari obligant a entrar-la - Nomes pels que vaig actualitzar les provincies, està a l'arxiu de l'update corresponent */
		global $gl_message, $gl_page;

		if (isset($_POST['mail']) && $_POST['mail']!='' && $_POST['password']!='')
		{	// nomès pot haber un login igual
			$query = "SELECT customer_id, mail, password, name, surname, is_wholesaler
						FROM product__customer
						WHERE mail = " . Db::qstr($_POST['mail']) . "
						AND  password = " . Db::qstr($_POST['password']) . "
						AND bin=0
						AND activated=1";
			$rs = Db::get_row($query);
			Debug::add('Query client',$query);
			Debug::add('Results client',$rs);
			// més segur amb 2 comprovacions
			if (($rs['mail'] == $_POST['mail'])
				&& ($rs['password'] == $_POST['password']))
			{
				$_SESSION['product']['customer_id'] = $rs['customer_id'];
				$_SESSION['product']['mail'] = $rs['mail'];
				$_SESSION['product']['is_wholesaler'] = $rs['is_wholesaler'];
				$_SESSION['product']['name'] = $rs['name'];
				$_SESSION['product']['surname'] = $rs['surname'];

				if ( $this->config['show_javascript_message_on_login'] ) {
					$js = $gl_page->get_javascript_show_message(
						USER_LOGIN_VALID,
						'logged',
						[
							'name' => $rs['name'],
							'surname' => $rs['surname']
						]
					);
					print_javascript($js);
				}
				else {
					// així no DONA ERROR AMB LOGOUT, (recarregava el top i feia logout i mai conseguiem fer login)
					if ( strpos( $_SERVER['HTTP_REFERER'], 'action=logout' ) || strpos( $_SERVER['HTTP_REFERER'], 'customer/logout' ) ) {

						if ( $this->config['show_first_customer_menu'] ) {
							print_javascript( 'top.window.location.href = "/?tool=product&tool_section=customer&language=' . LANGUAGE . '&action=list_order&menu_id=2";' );
							die();
						}
						else {
							print_javascript( 'top.window.location.href = "/?tool=product&tool_section=customer&language=' . LANGUAGE . '&action=show_menu_customer";' );
						}
					}
					else {
						if ( isset( $_POST['redirect'] ) ) {
							print_javascript( 'top.window.location.href = "' . addslashes( $_POST['redirect'] ) . '";' );
						}
						else {
							print_javascript( 'top.window.location.href=top.window.location.href' );
						}
					}
				}
			}
			// mostra missatge d'error
			else
			{
				$gl_page->show_message(USER_LOGIN_INVALID);
			}
			Debug::p_all();
			die();
		}
		// mostra formulari de login, si no s'ha entrat les dades bé o si s'entra per primer cop
		else
		{
			// guardo post en sessió per poder guardar al entrar login i password correctes
			if ($_POST) {
				$_SESSION['post'] = $_POST;
			}
			$this->show_login();
		}
	}
	function logout()
	{
		global $gl_message;

		unset($_SESSION['product']);

		$this->show_login();
		// anar a pagina inici
	}
	function send_password(){
		$new_password = get_password();
		if ($this->is_test){
			$mail = $_GET['mail'];
		}
		else {
			$mail = R::escape('mail',false,'_POST');
			$mailer = $GLOBALS['gl_current_mailer'] = get_mailer($this->config);
			$sql = "UPDATE product__customer SET password = '".$new_password."' WHERE mail = '" . $mail . "'";
			Debug::add('Consulta nou password', $sql);
			Db::Execute($sql);
		}
		// si o hi ha l'adreça mostro missatge d'error
		if (!$this->is_test && mysqli_affected_rows(Db::cn())==0) {
			print_javascript('
					if (typeof top.hidePopWin == "function") top.hidePopWin(false);
					top.alert("' . stripslashes($this->caption['c_password_mail_error']) . '");');
		}
		// si l'adreça es bona envio mail amb la nova contrasenya
		else{
			if (!$this->is_test)
				print_javascript('
					if (typeof top.hidePopWin == "function") top.hidePopWin(false);
					top.alert("' . stripslashes($this->caption['c_password_sent']) . '");
					top.customer_show_login();');

			// preparo mail
			$page = $GLOBALS['gl_page'];
			$page->template = 'mail.tpl';
			$page->set_var('preview','');
			$page->set_var('remove_text','');
			$this->set_file('/product/mail_password.tpl');
			$rs = $this->_get_customer_name($mail);
			$this->set_vars($rs);
			$this->set_var('password', $new_password);
			$this->set_var('treatment', $this->caption['c_treatment_'.$rs['treatment']]);
			$this->set_vars($this->caption);

			$page->content = $this->process();
			$body = $page->get();
			$subject = $this->caption['c_password_mail_subject'];
			
			if ($this->is_test)
				echo $body;
			
			else{			
				// envio mail
				if (is_file(CLIENT_PATH . 'images/logo_newsletter.jpg'))
					$mailer->AddEmbeddedImage(CLIENT_PATH . 'images/logo_newsletter.jpg', 'logo', 'logo.jpg');
				elseif(is_file(CLIENT_PATH . 'images/logo_newsletter.gif')) 
					$mailer->AddEmbeddedImage(CLIENT_PATH . 'images/logo_newsletter.gif', 'logo', 'logo.gif');
				
				$mailer->Body = $body;
				$mailer->Subject = $subject;
				$mailer->IsHTML(true);
				$mailer->AddAddress ($mail);
				Debug::add('mailer', $mailer);
				send_mail($mailer, false);
			}
		}
		Debug::p_all();
		die();
	}
	function newsletter_add_remove_key($table, $id) {	

			$table_id = strpos($table, "customer")!==false?'customer_id':'custumer_id';

			$q2 = "SELECT count(*) FROM ". $table . " WHERE BINARY remove_key = ";
			$q3 = "UPDATE ". $table . " SET remove_key='%s' WHERE  ". $table_id . " = '%s'";

			$updated = false; 
			while (!$updated){
				$pass = get_password(50);
				$q_exists = $q2 . "'".$pass."'";	

				// si troba regenero pass
				if(Db::get_first($q_exists)){
					$pass = get_password(50);							
				}
				// poso el valor al registre
				else{				
					$q_update = sprintf($q3, $pass, $id);

					Db::execute($q_update);
					break;
				}
			}
	}

	function _get_customer_name($mail){
		$sql = "SELECT CONCAT(name,' ', surname) as name, treatment FROM product__customer WHERE mail = '" . $mail . "'";
		return Db::get_row($sql);
	}
	function get_download(){
		// comprovo que l'id de la comanda sigui de una pagada
		// si no hi ha id també surto
		$order_id = R::escape('order_id');
		if (!$order_id) return;
		
		$query = "SELECT count(*) 
			FROM product__order 
			WHERE order_id = '$order_id'
			AND order_status IN ('completed','preparing','shipped','delivered','collect')";
		
		if (!Db::get_first($query)) return;
		
		$download_id = R::escape('download_id');
		$query = "
			SELECT download_id, name
			FROM product__product_download
			WHERE download_id = '". $download_id. "'
			ORDER BY ordre, download_id";
		
		$rs = Db::get_row($query);
		
		$file = $rs['download_id'] . strrchr(substr($rs['name'], -5, 5), '.');
		
		
		$file = CLIENT_PATH . '/product/product/downloads/' . $file;
		header("Content-type: application/octet-stream");
		header("Content-Disposition: attachment; filename=\"".$file."\"\n");
		$fp=fopen($file, "r");
		fpassthru($fp);
		die();
	}

	/**
	 * ProductCustomer::is_logged()
	 * Saber si està loguejat
	 * @return
	 */
	function is_logged(){
		// miro si hi ha les 2 sessions per més seguretat de que esta logged
		if ($this->never_mind_logged) return true; // per poder cridar desde dins la clase alguna acció encara que no estigui logejat, com ara mail a admin  quan s'haregistrat nou usuari

		// lassdive
		if (empty($_SESSION['product']['is_shop_app'])) {

			// per quan s'està logejat a la intranet
			if (isset($_SESSION['user_id']) && isset($_SESSION['user_id'])){
				// Per poder veure les factures des de la intranet
				if (R::text_id('invoice')) return true;
			}

			return ( isset( $_SESSION['product']['customer_id'] ) && isset( $_SESSION['product']['mail'] ) );
		}
		else {
			$this->is_shop_app = true;
			return !empty($_SESSION['user_id']) && !empty($_SESSION['group_id']);
		}
	}
	/**
	 * ProductCustomer::do_action()
	 * Sobreescric la funció heredada
	 * Qualsevol acció d'aquesta clase l'haig de cridar per aquí,
	 * ja que és on controlo que l'usuari està loguejat
	 * @param string $action
	 * @return
	 */
	function do_action($action=''){

		// si estic logejat es que ja he emplenat un formulari,
		// per tant no en puc crear cap més, nomès editar el meu
		if ($this->is_logged()){
			if ($this->action == 'show_form_new') $this->action = 'show_form_edit';
			if ($action == 'show_form_new') $action = 'show_form_edit';
			if ($this->action == 'get_form_new') $this->action = 'get_form_edit';
			if ($action == 'get_form_new') $action = 'get_form_edit';
		}


		$ac = $action?$action:$this->action;
		// es important no barrejar action amb this->action, per això faig servir una tercera variable $ac

		// aquests action no necessiten estar loguejats
		if (
				   $ac=='show_form_new'
				|| $ac=='get_form_new'
				|| $ac=='login'
				|| $ac=='get_login'
				|| $ac=='show_login'
				|| $ac=='show_message'
				|| $ac=='send_password'
			) {
			return parent::do_action($action);
		}
		else
		{
			// miro si hi ha les 2 sessions per més seguretat de que esta logged
			if ($this->is_logged()) {
				// m'asseguro que només s'editin les dades del customer_id de la sessió
				$id = $_SESSION['product']['customer_id'];
				$_GET['customer_id'] = $id;
				if(isset($_POST['customer_id']) &&(
					!isset($_POST['customer_id'][$id]) ||
					$_POST['customer_id'][$id]!=$id)){
					Main::error_404('algu intenta entrar amb un form propi o passa algo raro'); // algu intenta entrar amb un form propi o passa algo raro
				}
				if ($ac == 'add_record') die(); // si està loggejat, no pot afegir un nou customer, un client nomès es pot fer el seu formulari i prou
				return parent::do_action($action);
			}
			elseif ($ac == 'add_record')
			{
				// add record no deixo fer si està logejat i si, si no ho està ( per això no està posat al grup de dalt )
				parent::do_action('add_record');
			}
			else
			{
				parent::do_action('show_login');
			}
		}
	}
}
?>