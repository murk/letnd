<?

class LassdiveIncoming {
	static $has_payment_method;
	static public function get_incoming( &$module ) {

		$user_id = $_SESSION['user_id'];
		if (
			$module->search->is_excel ||
			$module->search->view_format != 'orderitems' ||
			! in_array( $user_id, [ '1', '2', '3' ] ) // Nomes en Miquel

		) return '';

		$tpl = new phemplate( PATH_TEMPLATES );
		$tpl->set_vars( $module->module_caption );
		$tpl->set_file( 'lassdive/reserva_earnings.tpl' );

		$condition          = $condition_totals_1 = $module->condition;
		$condition_totals_2 = '';

		$payment_method = R::escape( 'payment_method', 'null' );
		if ( $payment_method == 'null' ) $payment_method = false;
		self::$has_payment_method = $payment_method;

		if ( $payment_method ) {
			$condition_totals_1 = $condition . " AND order_id IN ( SELECT order_id FROM product__order WHERE payment_method = '$payment_method')";
			$condition_totals_2 = $condition . " AND order_id IN ( SELECT order_id FROM product__order WHERE deposit_payment_method = '$payment_method')";
		}

		// Totals
		$rs_totals = self::get_totals( $condition_totals_1 );
		if ( $payment_method ) {
			$rs_2      = self::get_totals( $condition_totals_2, true );
			$rs_totals = self::sum_arrays( $rs_totals, $rs_2 );
		}
		$totals = self::format_rs( $rs_totals );


		$tpl->set_vars( $totals );


		// Per activitat
		$rs_products = self::get_products( $condition );

		foreach ( $rs_products as &$product ) {

			$product_id = $product['product_id'];

			$rs_product_totals = self::get_product_totals( $condition_totals_1, $product_id );
			if ( $payment_method ) {
				$rs_2              = self::get_product_totals( $condition_totals_2, $product_id, true );
				$rs_product_totals = self::sum_arrays( $rs_product_totals, $rs_2 );
			}

			$product += self::format_rs( $rs_product_totals );

		}

		$tpl->set_loop( 'products', $rs_products );


		// Per instructor
		$rs_users = self::get_users( $condition );

		foreach ( $rs_users as &$user ) {

			$user_id = $user['user_id'];

			$rs_user_totals = self::get_user_totals( $condition_totals_1, $user_id );
			if ( $payment_method ) {
				$rs_2           = self::get_user_totals( $condition_totals_2, $user_id, true );
				$rs_user_totals = self::sum_arrays( $rs_user_totals, $rs_2 );
			}

			$user += self::format_rs( $rs_user_totals );
		}

		$tpl->set_loop( 'users', $rs_users );


		$ret = $tpl->process();

		return $ret;
	}

	static private function get_totals( $condition, $is_deposit = false ){
		$query_discounted = self::get_query_basetax_discounted( $is_deposit );
		$query_basetax = self::get_query_basetax( $is_deposit );

		$query     = "
			SELECT 
				sum( $query_discounted ) as all_basetax_discounted,					 
				sum( $query_basetax ) as all_basetax
			FROM product__orderitem
			INNER JOIN lassdive__orderitem USING (orderitem_id)
			WHERE $condition";

		$rs = Db::get_row( $query );
		return $rs;
	}
	static private function get_product_totals( $condition, $product_id, $is_deposit = false ){
		$query_discounted = self::get_query_basetax_discounted( $is_deposit );
		$query_basetax = self::get_query_basetax( $is_deposit );

		$query = "
			SELECT 
				sum( $query_discounted ) as all_basetax_discounted, 
				sum( $query_basetax ) as all_basetax 
			FROM product__orderitem
			INNER JOIN lassdive__orderitem USING (orderitem_id)
			WHERE $condition
			AND product_id = $product_id";

			$rs = Db::get_row( $query );
			return $rs;
	}
	static private function get_user_totals( $condition, $user_id, $is_deposit = false ){
		$query_discounted = self::get_query_basetax_discounted( $is_deposit );
		$query_basetax = self::get_query_basetax( $is_deposit );

		$query = "
			SELECT 
				sum(				
				$query_discounted
				/ 
					(SELECT count(*) FROM lassdive__reservaitem_to_user 
					WHERE lassdive__reservaitem_to_user.reservaitem_id = lassdive__orderitem.reservaitem_id )
				
				) as all_basetax_discounted,
				sum(
				
				($query_basetax) 
				/ 
				(SELECT count(*) FROM lassdive__reservaitem_to_user 
				WHERE lassdive__reservaitem_to_user.reservaitem_id = lassdive__orderitem.reservaitem_id )
				
				) as all_basetax 
			FROM product__orderitem
			INNER JOIN lassdive__orderitem USING (orderitem_id)
			WHERE $condition
			AND reservaitem_id IN (
				SELECT reservaitem_id 
				FROM lassdive__reservaitem_to_user 
				WHERE user_id = $user_id)";

		$rs = Db::get_row( $query );
		return $rs;
	}

	static function get_query_basetax_discounted ( $is_deposit) {

		// tots 2 es calculen amb la relació amb el preu final

		$query_rest = self::$has_payment_method ?
			" (all_basetax - deposit) / (all_base + all_tax)"
			:
			"all_basetax / (all_base + all_tax)";

		$query_discounted = $is_deposit ?
			"
					product_basetax * (
							SELECT deposit / (all_base + all_tax)
							FROM product__order 
							WHERE product__order.order_id = product__orderitem.order_id
							)"
			:
			"
					product_basetax * 
						(
							SELECT $query_rest
							FROM product__order 
							WHERE product__order.order_id = product__orderitem.order_id
						)";
		return $query_discounted;
	}
	static function get_query_basetax ( $is_deposit ) {

		$query = $is_deposit ?
			"product_basetax * (
							SELECT deposit / (all_basetax)
							FROM product__order 
							WHERE product__order.order_id = product__orderitem.order_id
							)"
			:
			"product_basetax";

		return $query;
	}

	static private function sum_arrays( $a, $b){
		$c = [];
		foreach ( $a as $k => $v ) {
			$c[$k] = $a[$k] + $b[$k];
		}
		return $c;
	}

	static private function format_rs( $rs ){

		$ret['all_discount'] = format_currency( $rs['all_basetax'] - $rs['all_basetax_discounted'] );

		$ret['all_basetax_discounted'] = format_currency( $rs['all_basetax_discounted'] );

		$ret['all_basetax'] =
			$rs['all_basetax'] == $rs['all_basetax_discounted'] ?
				'' :
				format_currency( $rs['all_basetax'] );
		return $ret;
	}

	static private function get_products( $condition ) {

		$language       = LANGUAGE;
		$query_products = "
			SELECT product_id 
			FROM product__orderitem
			INNER JOIN lassdive__orderitem USING (orderitem_id)
			WHERE $condition";
		$query          = "
			SELECT product_id, product_title
				FROM product__product_language 
				WHERE language = '$language'
				AND product_id IN
				($query_products)
				ORDER BY product_title;			
			";

		return Db::get_rows( $query );
	}

	static private function get_users( $condition ) {

		$query_reserva_items = "
			SELECT reservaitem_id 
			FROM product__orderitem
			INNER JOIN lassdive__orderitem USING (orderitem_id)
			WHERE $condition";

		$query = "			
			SELECT user_id, CONCAT(name, ' ', surname) as name
				FROM user__user 
				WHERE user__user.user_id IN (
					SELECT DISTINCT user_id
					FROM lassdive__reservaitem_to_user 
					WHERE reservaitem_id 
					IN ($query_reserva_items)
				)
			ORDER BY name ASC";

		return Db::get_rows( $query );
	}
}