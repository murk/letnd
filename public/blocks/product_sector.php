<?php

/* 29-3-13
 * 
 * Menu o bloc de sectors
 * Nomes va amb USE_FRIENLY_URL
 *  
 */
$show_images = isset($vars['show_images']) ? $vars['show_images'] : false;
$block_max_results = isset($vars['block_max_results']) ? $vars['block_max_results'] : false; // si no està definit els llista tots tipus menu
$add_dots_length = isset($vars['add_dots_length']) ? $vars['add_dots_length'] : false; // add_dots_length

$order = " ORDER BY ordre ASC, sector ";
$limit = $block_max_results?"LIMIT " . $block_max_results:'';

$query = "SELECT sector, product__sector.sector_id as sector_id, sector_description
				FROM product__sector_language, product__sector
				WHERE product__sector_language.sector_id = product__sector.sector_id
				AND language = '" . LANGUAGE . "'
				AND bin = 0
				" . $order .
				$limit;

$results = Db::get_rows($query);
$selected_found = false;

$vars['var_name'] = 'sector_id';

$vars['block_selected'] = (R::number('sector_id') || R::number('sector'))?1:0;

if ( ! $vars['block_selected'] ) {
	$vars['block_selected'] = ( isset( $_GET['sector_ids'] ) ) ? 1 : 0;
}

if ($results) {
    $conta = 0;
    $loop_count = count($results);
    $sector_id = $sector_id = R::number('sector_id')?R::number('sector_id'):R::number('sector');
	R::escape_array($_GET['sector_ids']);
    
    foreach ($results as $rs) {

		$l = array();

		// CONTA PRODUCTES, es mes rapid que posant la consulta com a subquery
		$product_count = Db::get_first("SELECT count(*) 
							FROM product__product_to_sector, product__product
							WHERE sector_id = " . $rs['sector_id'] . "
							AND product__product.product_id = product__product_to_sector.product_id
							AND (status = 'onsale' OR status = 'nostock' OR status = 'nocatalog') 
							AND product__product.bin = 0
							LIMIT 1");
        
        
		if ($product_count) {
			$friendly_params['sector_id'] = $rs['sector_id'];
			$l['link'] = Page::get_link('product','product',false,false,$friendly_params,false,'',false,false);
			$l['sector_description'] = add_dots('sector_description', nl2br(htmlspecialchars($rs['sector_description'])), $add_dots_length); // per la home nomès
			$l['sector'] = $rs['sector'];
			$l['sector_id'] = $l['id'] = $rs['sector_id'];
			$l['class'] = '';
			$l['loop_count'] = $loop_count;
			$l['selected'] = $sector_id == $rs["sector_id"] && $vars['block_selected'] ? 1 : 0;
			
			if (isset($_GET['sector_ids'])) {
				$l['selected'] = in_array($rs["sector_id"], $_GET['sector_ids']) ? 1 : 0;
			}

			if ($show_images)
				$l += UploadFiles::get_record_images($rs['sector_id'], true, 'product', 'sector');

			$l['conta'] = $conta++;

			$loop[] = $l;
		}
        
    }

    if ($loop) {
        $loop[0]['class'] = 'first';
        $loop[count($loop) - 1]['class'] = 'last';
    }
} else {
    $show_block = false;
    return;
}
?>