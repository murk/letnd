<?php

if (defined('PRODUCT_HAS_PRIVATE_FAMILYS')) {
	$has_private_familys = PRODUCT_HAS_PRIVATE_FAMILYS;
}
else {
	$cf = get_config_array('admin', 'product');
	$has_private_familys = $cf['has_private_familys'];
}

$show_images = isset($vars['show_images']) ? $vars['show_images'] : false;
$sector_id =  isset($vars['sector_id']) ? $vars['sector_id'] : false;
$vars['sector_id'] = $sector_id;

$vars['block_selected'] = 0;

// Query families
$pass_code = isset($_SESSION['product']['family_pass_code'])?$_SESSION['product']['family_pass_code']:false;
$q_family_condition = "product__family_language.family_id = product__family.family_id
				AND language = %s
				AND product__family.parent_id= %s				
				AND bin = 0";

if (isset($has_private_familys) && $pass_code) {
	$q_family_condition .= " AND (is_family_private = 0 || pass_code = %s)";
}
else {
	$q_family_condition .= " AND is_family_private = 0";
}


$q_family = new Query( 'product__family_language, product__family' );
$q_family->cache_get('family, product__family.family_id, family_description, family_alt', $q_family_condition)
	->set_order_by("ordre ASC, family");
if (isset($home_max_familys)) {
	$q_family->set_limit("0, $home_max_familys");
}
$results = $q_family->get_c(LANGUAGE, 0, $pass_code);


$family_id = R::get( 'family_id' );
if (!$family_id) $family_id = R::get( 'family' );
if ($family_id)	$vars['block_selected'] = 1;

// filtre
$family_ids = R::get( 'family_ids' );
if ($family_ids)	$vars['block_selected'] = 1;

$conta = 0;
$loop_count = count($results);
$loop = array();


$friendly_params =  $sector_id?
	['sector' => $sector_id]:
	[];

// Query contar productes, el query val per tots, es suposa que no estarà un producte al primer nivell si es del segon, o al revés
$sector_condition = $sector_id?
	"AND product_id IN (SELECT product_id FROM product__product_to_sector WHERE sector_id = $sector_id)":
	"";
$product_condition = "AND (status = 'onsale' OR status = 'nostock' OR status = 'nocatalog')";
$query_count = new Query( 'product__product' );
$query_count->cache_get('count(*)',
						"(product__product.family_id = %s
						|| product__product.subfamily_id = %s
						|| product__product.subsubfamily_id = %s )
						$product_condition
						$sector_condition
						AND product__product.bin = 0");

foreach ($results as $rs) {

	$l = array();

	// CONTA PRODUCTES, es mes rapid que posant la consulta com a subquery
	$product_count = $query_count->get_first_c($rs['family_id'], $rs['family_id'], $rs['family_id']);

	if ($product_count) {

		$l['link'] =
			get_link( array(
					'tool' => 'product',
					'tool_section' => 'product',
					'category' => 'family',
					'category_id' => $rs['family_id'],
					'detect_page' => true,
					'friendly_params' => $friendly_params
			));

		$l['id'] = $rs['family_id'];
		$l['family_description'] = nl2br(htmlspecialchars($rs['family_description'])); // per la home nomès
		$l['family_alt'] = htmlspecialchars($rs['family_alt']);
		$l['item'] = $rs['family'];
		$l['class'] = '';
		$l['conta'] = $conta++;
		$l['loop_count'] = $loop_count;

		if ($l['id'] == $family_id)
			$selected = 1;
		else
			$selected = 0;

		// pel filtre
		if ( $family_ids &&
		     in_array( $l['id'], $family_ids ) ) {
			$selected = 1;
		}

		$l['selected'] = $selected;

		if ($show_images)
			$l += UploadFiles::get_record_images($rs['family_id'], true, 'product', 'family');


		$results2 = $q_family->get_c(LANGUAGE, $l['id'], $pass_code);

		$show = false;


		/* dietetica - he activat les subfamilies */
		$loop2 = array(); // segon loop dins de loop
		$conta2 = 0;
		$loop_count2 = count($results2);

		if ($results2) {


			foreach ($results2 as $rs2) {

				$l2 = array();

				$sub_product_count = $query_count->get_first_c($rs2['family_id'], $rs2['family_id'], $rs2['family_id']);

				if ($sub_product_count) {

					$l2['link'] =
						get_link( array(
								'tool' => 'product',
								'tool_section' => 'product',
								'category' => 'family',
								'category_id' => $rs2['family_id'],
								'detect_page' => true,
								'friendly_params' => $friendly_params
						));


					$l2['id'] = $rs2['family_id'];
					$l2['item'] = $rs2['family'];
					$l2['family_description'] = $rs2['family_description'];
					$l2['family_alt'] = htmlspecialchars($rs2['family_alt']);
					$l2['class'] = '';
					$l2['conta'] = $conta2++;
					$l2['loop_count'] = $loop_count2;


					if ($l2['id'] == $family_id)
						$selected = 1;
					else
						$selected = 0;

					// pel filtre
					if ( $family_ids &&
					  in_array( $l2['id'], $family_ids) ){
						$selected = 1;
					  }

					$l2['selected'] = $selected;

					if ($selected) $l['selected'] = $selected;


					if ($show_images)
						$l2 += UploadFiles::get_record_images($rs2['family_id'], true, 'product', 'family');

					$loop2[] = $l2;
				}
			}
		}

		if ($loop2) {
			$loop2[0]['class'] = 'first';
			$loop2[count($loop2) - 1]['class'] = 'last';
			
			// poso be el loop_count, pot ser que alguna familia no tingui productes i llavors falla
			if (count($loop2) != $loop_count2) {
				foreach ($loop2 as &$val) {
					$val['loop_count'] = count($loop2);
				}
			}
		}

		$l['loop'] = $loop2;
		/*  fi  dietetica */

		$loop[] = $l;
	}
}
if ($loop) {
	$loop[0]['class'] = 'first';
	$loop[count($loop) - 1]['class'] = 'last';
	// poso be el loop_count, pot ser que alguna familia no tingui productes i llavors falla
	if (count($loop) != $loop_count) {
		foreach ($loop as &$val) {
			$val['loop_count'] = count($loop);
		}
	}
	
} else {
	/* si no hi ha cap resultat no mostro el block */
	$show_block = false;
	return;
}