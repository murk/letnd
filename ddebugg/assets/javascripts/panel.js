Clockwork.controller('PanelController', function ($scope, $http, requests, updateNotification)
{
	$scope.requests = []
	$scope.request = null

	$scope.timelineLegend = []

	$scope.loadingMoreRequests = false
	$scope.preserveLog = true
	$scope.requestsListCollapsed = false
	$scope.showIncomingRequests = true

	$scope.expandedEvents = []

	$scope.init = function () {
		key('⌘+k, ctrl+l', () => $scope.$apply(() => $scope.clear()))

		if (Extension.runningAsExtension()) {
			$scope.$integration = new Extension($scope, requests, updateNotification)
		} else {
			$scope.$integration = new Standalone($scope, $http, requests)
		}

		$scope.$integration.init()
	}

	$scope.clear = function () {
		requests.clear()

		$scope.requests = []
		$scope.request = null

		$scope.timelineLegend = []

		$scope.showIncomingRequests = true

		$scope.expandedEvents = []
	}

	$scope.refreshRequests = function (activeRequest) {
		$scope.requests = requests.all()

		if (! $scope.preserveLog) {
			$scope.showRequest($scope.requests[0].id)
		} else if ($scope.showIncomingRequests && $scope.requests.length) {
			let id = activeRequest ? activeRequest.id : $scope.requests[$scope.requests.length - 1].id;
			$scope.showRequest(id);
			$scope.showIncomingRequests = true
		}
	}

	$scope.showRequest = function (id) {

		let repeated_id = $scope.request && $scope.request.id == id;

		if (repeated_id) return;

		$scope.regenerate_data_tables();
		console.log('Carregant', id);

		$scope.request = requests.findId(id)

		$scope.updateNotification = updateNotification.show(requests.remoteUrl)
		$scope.timelineLegend = $scope.generateTimelineLegend()

		$scope.showIncomingRequests = (id == $scope.requests[$scope.requests.length - 1].id)
	}

	$scope.getRequestClass = function (id) {
		return $scope.request && $scope.request.id == id ? 'selected' : ''
	}

	$scope.showDatabaseConnectionColumn = function () {
		if (! $scope.request) return

		let connnections = $scope.request.databaseQueries
			.map(query => query.connection)
			.filter((connection, i, connections) => connections.indexOf(connection) == i)

		return connnections.length > 1
	}

	$scope.showCacheTab = function () {
		let cacheProps = [ 'cacheReads', 'cacheHits', 'cacheWrites', 'cacheDeletes', 'cacheTime' ]

		if (! $scope.request) return

		return cacheProps.some(prop => $scope.request[prop]) || $scope.request.cacheQueries.length
	}

	$scope.showCacheQueriesConnectionColumn = function () {
		return $scope.request && $scope.request.cacheQueries.some(query => query.connection)
	}

	$scope.showCacheQueriesDurationColumn = function () {
		return $scope.request && $scope.request.cacheQueries.some(query => query.duration)
	}

	$scope.generateTimelineLegend = function () {
		if (! $scope.request) return []

		let items = []
		let maxWidth = $scope.getTimelineWidth()
		let labelCount = Math.floor(maxWidth / 80)
		let step = $scope.request.responseDuration / (maxWidth - 20)
		let j

		for (j = 2; j < labelCount + 1; j++) {
			items.push({
				left: (j * 80 - 40).toString(),
				time: Math.round(j * 80 * step).toString()
			})
		}

		if (maxWidth - ((j - 1) * 80) > 45) {
			items.push({
				left: (maxWidth - 35).toString(),
				time: Math.round(maxWidth * step).toString()
			});
		}

		return items
	}

	$scope.getTimelineWidth = function () {
		let timelineShown = $('[tab-content="timeline"]').css('display') !== 'none'

		if (! timelineShown) $('[tab-content="timeline"]').show()

		let width = $('.timeline-graph').width()

		if (! timelineShown) $('[tab-content="timeline"]').hide()

		return width
	}

	$scope.loadMoreRequests = function () {
		$scope.loadingMoreRequests = true

		requests.loadPrevious(10).then(() => {
			$scope.$apply(() => {
				$scope.requests = requests.all()
				$scope.loadingMoreRequests = false
			})
		})
	}

	$scope.toggleRequestsList = function () {
		$scope.requestsListCollapsed = ! $scope.requestsListCollapsed
	}

	$scope.togglePreserveLog = function () {
		$scope.preserveLog = ! $scope.preserveLog
	}

	$scope.isEventExpanded = function (event) {
		return $scope.expandedEvents.indexOf(event) !== -1
	}

	$scope.toggleEvent = function (event) {
		if ($scope.isEventExpanded(event)) {
			$scope.expandedEvents = $scope.expandedEvents.filter(item => item != event)
		} else {
			$scope.expandedEvents.push(event)
		}
	}

	$scope.closeUpdateNotification = function () {
		$scope.updateNotification = null

		updateNotification.ignoreUpdate(requests.remoteUrl)
	}

	$scope.open_in_editor_letnd = function (file, line) {

		let link = "http://localhost:63342/api/file?file=" + file + "&line=" + line
		console.log(link)
		let xhttp = new XMLHttpRequest()
		xhttp.open("GET", link, true)
		xhttp.send()


	}

	$scope.query_table = false;
	$scope.log_table = false;

	$scope.regenerate_data_tables = function (){

		if ($scope.query_table) {
			console.log('Destruir query-table ...');
			$scope.query_table.destroy();
			$scope.query_table = false;
		}

		if ($scope.log_table) {
			console.log('Destruir log-table ...');
			$scope.log_table.destroy();
			$scope.log_table = false;
		}

	}
	$scope.create_data_tables = function () {
		$scope.create_query_table();
		$scope.create_log_table();
	}

	$scope.create_query_table = function () {


		if ($scope.query_table) {
			console.log('Ja està creada query-table, no faig res ...');
			return;
		}


		console.log('Creant query-table ...');

		// Datatables
		$scope.query_table = $('#query-table').DataTable(
			{
				dom: 'Bfrtip',
				paging: false,
				fixedHeader: {
					header: true,
					footer: true
				},
				initComplete: function () {
					this.api().columns(2).every(function () {
						var column = this;
						var select = $('<select><option value=""></option></select>')
							.appendTo('.dt-buttons')
							.on('change', function () {
								var val = $.fn.dataTable.util.escapeRegex(
									$(this).val()
								);

								column
									.search(val ? '^' + val + '$' : '', true, false)
									.draw();
							});

						column.data().unique().sort().each(function (d, j) {
							select.append('<option value="' + d + '">' + d + '</option>')
						});
					});
				},
				buttons: [
					{
						text: 'Select',
						action: function (e, dt, node, config) {
							$scope.query_table.search('Select').draw();
                    $('.dt-buttons button.active').removeClass('active');
							node.addClass('active');
						}
					},
					{
						text: 'Update',
						action: function (e, dt, node, config) {
							$scope.query_table.search('Update').draw();
                    $('.dt-buttons button.active').removeClass('active');
							node.addClass('active');
						}
					},
					{
						text: 'Insert',
						action: function (e, dt, node, config) {
							$scope.query_table.search('Insert').draw();
                    $('.dt-buttons button.active').removeClass('active');
							node.addClass('active');
						}
					},
					{
						text: 'Delete',
						action: function (e, dt, node, config) {
							$scope.query_table.search('Delete').draw();
                    $('.dt-buttons button.active').removeClass('active');
							node.addClass('active');
						}
					},
					{
						text: 'Show',
						action: function (e, dt, node, config) {
							$scope.query_table.search('Show').draw();
                    $('.dt-buttons button.active').removeClass('active');
							node.addClass('active');
						}
					}
				]

			}
		);
		// $scope.query_table.state.clear();
	};

	$scope.create_log_table = function () {


		if ($scope.log_table) {
			return;
		}

		$scope.log_table = $('#log-table').DataTable(
			{
				dom: 'Bfrtip',
				paging: false,
				buttons: [
					{
						text: 'debug',
						action: function (e, dt, node, config) {
							$scope.log_table.search('debug').draw();
                    $('.dt-buttons button.active').removeClass('active');
							node.addClass('active');
						}
					},
					{
						text: 'error',
						action: function (e, dt, node, config) {
							$scope.log_table.search('error').draw();
                    $('.dt-buttons button.active').removeClass('active');
							node.addClass('active');
						}
					},
					{
						text: 'info',
						action: function (e, dt, node, config) {
							$scope.log_table.search('info').draw();
                    $('.dt-buttons button.active').removeClass('active');
							node.addClass('active');
						}
					},
					{
						text: 'notice',
						action: function (e, dt, node, config) {
							$scope.log_table.search('notice').draw();
                    $('.dt-buttons button.active').removeClass('active');
							node.addClass('active');
						}
					},
					{
						text: 'warning',
						action: function (e, dt, node, config) {
							$scope.log_table.search('warning').draw();
                    $('.dt-buttons button.active').removeClass('active');
							node.addClass('active');
						}
					},
					{
						text: 'critical',
						action: function (e, dt, node, config) {
							$scope.log_table.search('critical').draw();
                    $('.dt-buttons button.active').removeClass('active');
							node.addClass('active');
						}
					},
					{
						text: 'plantilla',
						action: function (e, dt, node, config) {
							$scope.log_table.search('plantilla').draw();
                    $('.dt-buttons button.active').removeClass('active');
							node.addClass('active');
						}
					}
				]

			}
		);

	};

	$scope.$on('ready', function () {

		console.log('App ready');

	});

	angular.element(window).bind('resize', () => {
		$scope.$apply(() => $scope.timelineLegend = $scope.generateTimelineLegend())
    })
})
