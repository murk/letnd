<?php
set_tool('inmo', 'property', 'inmo__property', 'property_id');
Module::get_config_values();
set_language_vars('custumer', 'custumer', 'property');
set_config('inmo/inmo_property_config.php');

function save_rows()
{
    $save_rows = new SaveRows;
    $save_rows->save();
}

function list_records()
{
    global $gl_action;
	switch ($gl_action)
    {
		case 'list_records_select_frames_property'://obre el frame,per assignar propietats list_records_select_frames_property
			assign_properties();
			return;
            break;
		case 'list_records_select_property': //es la funcio del iframe per assignar clients que llista els clients list_records_select_property
			list_records_to_select();
			return;
            break;
		case 'list_records_select_search_words_property': //busca propietats per referencia al iframe
			list_records_to_select();
			return;
			break;

            // default:
    } // switch
	$listing = new ListRecords;
    $listing->set_options(1, 1, 1, 1, 0, 0, 0, 0); // set_options ($options_bar = 1, $options_checkboxes = 1, $save_button = 1, $select_button = 1, $delete_button = 1, $print_button = 1, $edit_buttons = 1, $image_buttons = 1)
    // referncia automatica
    if (INMO_PROPERTY_AUTO_REF == 1) {
        $listing->call('get_ref', 'ref,category_id');
    }
    $listing->list_records();
}

function show_form()
{
    show_form_property();
}

function write_record()
{
	global $gl_action, $gl_insert_id, $gl_tool_section, $gl_saved, $gl_message;

    write_record_property();

	if (isset($_GET['custumer_id'])) {
	//guardo el propert_id a  custumer__custumer_to_property
		$query = "INSERT INTO custumer__custumer_to_property ( custumer_id, property_id ) values ('" . $_GET['custumer_id'] . "', '" .  $gl_insert_id . "')";
		Db::execute($query);
	}
	set_inner_html('propertys',list_property_of_custumer($_GET['custumer_id'], '', true));

}

function manage_images()
{
    global $gl_action, $tpl;
	$tpl->set_var('show_tabs',false);
	$image_manager = new ImageManager;
    $image_manager->reload=false;
    $image_manager->execute();
	if (($gl_action=='save_rows_save_records_images_property') || ($gl_action=='save_rows_delete_selected_images_property')) {
		set_inner_html('propertys',list_property_of_custumer($_GET['custumer_id'], '', true));
	}
}

function assign_properties () {

    global $tpl, $gl_action, $gl_page, $gl_content, $gl_caption, $gl_db_classes_fields;


    $gl_page->menu_tool = false;
    $gl_page->menu_all_tools = false;
    $gl_page->template = '';

    $tpl->set_file('custumer/custumer_select_frames.tpl');
    $tpl->set_vars($gl_caption);	
	
	$tpl->set_var('custumer_id',R::number('custumer_id')); // r:number pot ser = '' R::id no
	$tpl->set_var('language_code', $GLOBALS['gl_language_code']);
	$tpl->set_var('language', $GLOBALS['gl_language']);
    $gl_content = $tpl->process();
}

function list_records_to_select () {
	set_config('custumer/custumer_custumer_property_assign_config.php');

	 global $tpl, $gl_action, $gl_page, $gl_content, $gl_caption, $gl_messages, $gl_message;

	$gl_page->menu_tool = false;
    $gl_page->menu_all_tools = false;
    $gl_page->template = 'clean.tpl';
    $gl_page->title = '';
    $gl_message = $gl_messages['select_item'];

	$listing = new ListRecords;

	if ($gl_action == "list_records_select_search_words_property")
    {	if (R::get('q') != "") {
		include(DOCUMENT_ROOT . '/admin/modules/inmo/inmo_search_functions.php');
		$listing->condition = search_search_condition($listing);
    	}
	}
	$listing->use_default_template = true;
	$listing->call('list_records_walker', 'property_id,ref,property_private');
	$listing->set_options(1, 0, 0, 0, 0, 0, 0, 0);
	$listing->order_by = 'ref DESC';
    $listing->order_by = '';
	
	$listing->add_filter('category_id');
	$listing->add_filter('tipus');
	
	$gl_caption['c_room_0'] = $gl_caption['c_filter_room_0'];
	
	$listing->add_filter('room');
	$listing->add_filter('status');
	
    $listing->list_records();

}

function list_records_walker($property_id, $ref , $property_private)
{
    //passo els valors a traves de la funcio insert_adress
    $name = $ref . ' - ' . $property_private;
    $name= addslashes($name); // cambio ' per \'
    $name= htmlspecialchars($name); // cambio nomès cometes dobles per  &quot;
	$ret['tr_jscript'] = 'onClick="parent.insert_address(' . $property_id . ',\'' . $name . '\')"';
    return $ret;
}

?>