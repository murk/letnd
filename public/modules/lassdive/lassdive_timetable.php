<?

/**
 * LassdiveReserva
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2019
 * @version $Id$
 * @access public
 */
class LassdiveTimetable extends Module {

	private $timetable;
	public function __construct() {

		parent::__construct();
		Main::load_class( 'lassdive', 'timetable_main', 'admin' );
		$this->timetable            = new LassdiveTimetableMain( $this );
		$this->timetable->caption = $this->caption;
		$this->timetable->is_public = true;
	}


	public function list_records() {

		$this->month();

	}

	public function month() {
		$this->timetable->view = 'month';
		$this->timetable->get_main_frame();
	}

	public function week() {
		$this->timetable->view = 'week';
		$this->timetable->get_main_frame();
	}

	public function get_month() {
		$this->timetable->view = 'month';
		$this->timetable->get_timetable();
	}

	public function get_week() {
		$this->timetable->view = 'week';
		$this->timetable->get_timetable();
	}

	public function get_select_centre() {
		$this->timetable->get_select_centre();
	}
}