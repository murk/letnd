<?

/* 5-5-16

	INMO
	- Configuració dels camps obligatoris
 */


if ( is_table( 'custumer__configpublic' ) && ! Db::get_first( "SELECT * FROM custumer__configpublic WHERE name='required_add'" ) ) {
	$update_sql[] = "
		INSERT INTO custumer__configpublic (`name`, `value`) VALUES ('required_add', '');";

} else {
	if ( is_table( 'custumer__configpublic' ) ) {
		trigger_error( 'Ja existeixen el camp required_add' );
	}
}

if ( is_table( 'custumer__configpublic' ) && ! Db::get_first( "SELECT * FROM custumer__configpublic WHERE name='required_remove'" ) ) {
	$update_sql[] = "
		INSERT INTO custumer__configpublic (`name`, `value`) VALUES ('required_remove', '');";

} else {
	if ( is_table( 'custumer__configpublic' ) ) {
		trigger_error( 'Ja existeixen el camp required_remove' );
	}
}


?>