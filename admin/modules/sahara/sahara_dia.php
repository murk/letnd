<?
/**
 * SaharaDia
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c)  30/11/2013
 * @version $Id$
 * @access public
 */
class SaharaDia extends Module{
	var $circuit_id;
	function __construct(){
		parent::__construct();
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function get_records()
	{
	    if ($this->parent == 'SaharaCircuit'){
		
			$listing = new ListRecords($this);
			$listing->has_bin = false;
			$listing->condition = 'circuit_id = ' . $this->circuit_id;
			$listing->order_by = 'ordre asc, dia ASC';			
			$listing->set_options(0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
			
			$listing->add_button('edit_dia_button', '', 'boto1', 'before','show_form_edit_dia');
			
			$listing->show_langs = true;
			
			return $listing->list_records();
		}
	}
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{
		if ($this->process=='circuit'){
			$circuit_id = R::id('circuit_id');
			$show = new ShowForm($this);
			
			//$dia_num = Db::get_first("SELECT MAX(dia_num)+1 FROM sahara__dia WHERE circuit_id = " . $circuit_id);
			
			//$show->set_field('dia_num', 'default_value', $dia_num);
			$show->set_field('circuit_id', 'default_value', $circuit_id);
			$show->has_bin = false;
			return $show->show_form();
		}
	}

	function save_rows()
	{
	    $save_rows = new SaveRows($this);
	    $save_rows->save();
	}

	function write_record()
	{		
	    $writerec = new SaveRows($this);
	    $writerec->save();
		
		
		if ($this->process=='circuit'){
			
			$this->circuit_id = R::id('circuit_id');
			
			if ($this->action=='add_record'){
			$dia_id = $GLOBALS['gl_insert_id'];

			Db::execute("
				UPDATE sahara__dia 
				SET circuit_id=".$this->circuit_id."
				WHERE  dia_id=".$dia_id.";");
			}
			
			$this->parent='SaharaCircuit';
			$dias = $this->do_action('get_records');
			set_inner_html('dias', $dias);
			
			
			$GLOBALS['gl_page']->show_message($GLOBALS['gl_message']);
			Debug::p_all();
			die();
							
		}
	}

	function manage_images()
	{
	    $image_manager = new ImageManager($this);
	    $image_manager->execute();
	}
}
?>