<?php
global $gl_caption_admin, $gl_caption_public, $gl_caption_newsletter, $gl_caption_inmo_admin, $gl_caption_inmo_public, $gl_caption_translation, $gl_caption_custumer, $gl_caption_page, $gl_caption_save, $gl_caption_product_admin, $gl_caption_product_public;
// menus eina
define('WEB_MENU_TITLE','Titulo web'); //Nom apareix dalt internet explorer
define('WEB_MENU_ROBOTS','Tags de la web'); //Paraules dels tags
define('WEB_MENU_SAVE','Copias de seguridad'); //Paraules dels tags
define('WEB_MENU_SAVE_LIST','Realizar copia'); //Paraules dels tags
define('WEB_MENU_TRANSLATION','Traducciones');
define('WEB_MENU_TRANSLATION_LIST','Listar todas las traducciones');
define('WEB_MENU_PAGE_TRANSLATION_LIST','Listar apartados a traducir');
define('WEB_MENU_BACKOFFICE_SEARCH','Búsquedas de clients');


define('WEB_MENU_CONFIG', 'Configuración');
define('WEB_MENU_CONFIGADMIN_LIST', 'General parte privada');
define('WEB_MENU_CONFIGPUBLIC_LIST', 'General parte pública');
define('WEB_MENU_CONFIGINMOADMIN_LIST', 'Propiedades parte privada');
define('WEB_MENU_CONFIGINMOPUBLIC_LIST', 'Propiedades parte pública');
define('WEB_MENU_CONFIGCUSTUMER_LIST', 'Clientes');
define('WEB_MENU_CONFIGNEWSLETTER_LIST', 'Newsletter');
define('WEB_MENU_CONFIGUTILITY_LIST', 'Utilitades');
define('WEB_MENU_CONFIGMLS_LIST','MLS');
define('WEB_MENU_CONFIGCONTACT_PUBLIC_LIST','Contacto parte pública');
define('WEB_MENU_CONFIGCONTACT_ADMIN_LIST','Contacto parte privada');

define('WEB_MENU_CONFIGPRODUCT_ADMIN_LIST','Productos parte privada');
define('WEB_MENU_CONFIGPRODUCT_PUBLIC_LIST','Productos parte pública');
/*--------------- no traduit -------------------*/
$gl_caption_config['c_others1']='Característica 1';
$gl_caption_config['c_others2']='Característica 2';
$gl_caption_config['c_others3']='Característica 3';
$gl_caption_config['c_others4']='Característica 4';


$gl_caption_config['c_default_home_tool'] = 'Herraminta predeterminada';
$gl_caption_config['c_default_home_tool_section'] = 'Sección predeterminada';
$gl_caption_config['c_language'] = 'Idioma predeterminado';
$gl_caption_config['c_home_max_results'] = 'Nombre de resultados en la página de inicio';
$gl_caption_config['c_page_slogan'] = 'Frase corporativa';
$gl_caption_config['c_page_phone'] = 'Teléfono';
$gl_caption_config['c_page_address'] = 'Calle';
$gl_caption_config['c_page_title'] = 'Título Intranet';
$gl_caption_config['c_default_menu_id'] = 'Menú por defecto';
$gl_caption_config['c_theme'] = 'Tema';
$gl_caption_config['c_language'] = 'Idioma';
$gl_caption_config['c_max_results'] = 'Número de resultados por página';
$gl_caption_config['c_max_page_links'] = 'Número de páginas a enseñar';
$gl_caption_config['c_add_dots_length'] = 'Máximo de palabras en las descripciones';
$gl_caption_config['c_date_created'] = 'Fecha de creación de la web';
$gl_caption_admin['c_group_1'] = 'Opciones de los listados';
$gl_caption_public['c_group_1'] = $gl_caption_admin['c_group_1'];
$gl_caption_config['c_templatehtml_id'] = 'Plantilla en html';
$gl_caption_config['c_templatetext_id'] = 'Plantilla de texto';
$gl_caption_config['c_group_1'] = 'De';
$gl_caption_config['c_group_2'] = 'Responder a';
$gl_caption_config['c_group_3'] = 'Datos SMTP';
$gl_caption_config['c_m_username'] = 'Nombre de usuaro';
$gl_caption_config['c_m_password'] = 'Contraseña';
$gl_caption_config['c_m_password_repeat'] = 'Repetir contraseña';
$gl_caption_config['c_m_host'] = 'Servidor';
$gl_caption['c_custumer_table'] = 'Tabla de direcciones';
$gl_caption['c_newsletter__custumer'] = 'newsletter__custumer';
$gl_caption['c_custumer__custumer'] = 'custumer__custumer';
$gl_caption['c_contact__contact'] = 'contact__contact';
$gl_caption['c_product__customer'] = 'product__customer';
$gl_caption_newsletter['c_content_extra'] = 'Contenido inculuido';
$gl_caption_config['c_only_test'] = 'Mode test';
$gl_caption_config['c_only_test_description'] = 'Si activas esta casilla, no se enviará ningún mail, solo se simulará el envio';

$gl_caption_config['c_text'] = 'texto';
$gl_caption_config['c_provincies'] = 'Provincias habituales';
$gl_caption_config['c_default_provincia'] = 'Provincia por defecto';
$gl_caption_config['c_default_comarca'] = 'Comarca por defecto';
$gl_caption_config['c_default_municipi'] = 'Municipio por defecto';
$gl_caption_config['c_units'] = 'Unidades';

$gl_caption_inmo_admin['c_group_1'] = 'Thumbnail admin';
$gl_caption_config['c_im_admin_thumb_w'] = 'anchura';
$gl_caption_config['c_im_admin_thumb_h'] = 'altura';
$gl_caption_config['c_im_admin_thumb_q'] = 'calidad';

$gl_caption_inmo_admin['c_group_2'] = 'Thumbnail público';
$gl_caption_inmo_public['c_group_2'] = $gl_caption_inmo_admin['c_group_2'];
$gl_caption_config['c_im_thumb_w'] = 'anchura';
$gl_caption_config['c_im_thumb_h'] = 'altura';
$gl_caption_config['c_im_thumb_q'] = 'calidad';

$gl_caption_inmo_admin['c_group_3'] = 'Imagen ficha pública';
$gl_caption_inmo_public['c_group_3'] = $gl_caption_inmo_admin['c_group_3'];
$gl_caption_config['c_im_details_w'] = 'anchura';
$gl_caption_config['c_im_details_h'] = 'altura';
$gl_caption_config['c_im_details_q'] = 'calidad';

$gl_caption_inmo_admin['c_group_4'] = 'Imagen grande público';
$gl_caption_inmo_public['c_group_4'] = $gl_caption_inmo_admin['c_group_4'];
$gl_caption_config['c_im_medium_w'] = 'anchura';
$gl_caption_config['c_im_medium_h'] = 'altura';
$gl_caption_config['c_im_medium_q'] = 'calidad';


$gl_caption_inmo_admin['c_group_5'] = 'Imagen impresión grande';
$gl_caption_config['c_im_big_w'] = 'anchura';
$gl_caption_config['c_im_big_h'] = 'altura';

$gl_caption_config['c_im_list_cols'] = 'Columnas imagen';
$gl_caption_config['c_property_auto_ref'] = 'Referéncia automàtica';
$gl_caption_config['c_menu_price'] = 'Valores del menú de precio';
$gl_caption_config['c_home_max_results'] = 'Número de destacados';
$gl_caption_config['c_home_title'] = 'Título de propiedades destacadas';
$gl_caption_config['c_html'] = 'html';
$gl_caption_config['c_from_address'] = 'E-mail';
$gl_caption_config['c_from_address_repeat'] = 'Repetir e-mail';
$gl_caption_config['c_from_name'] = 'Nombre';
$gl_caption_config['c_reply_to'] = 'E-mail';
$gl_caption_config['c_reply_to_repeat'] = 'Repetir e-mail';
$gl_caption_config['c_reply_to_name'] = 'Nombre';
$gl_caption_config['c_subject'] = 'Asunto';
$gl_caption_config['c_format'] = 'Formato';
$gl_caption_config['c_send_method'] = 'Forma de envio';
$gl_caption_config['c_CC'] = 'Copia en cada dirección (CC)';
$gl_caption_config['c_BCC'] = 'Copia oculta en cada dirección (BCC)';
$gl_caption_config['c_one_by_one'] = 'Un mensaje diferente para cada dirección';
$gl_caption['c_template_caption']='[Ninguna plantilla]';

$gl_caption_translation['c_2'] = 'En traducción';
$gl_caption_translation['c_3'] = 'Finalizado';
$gl_caption_translation['c_1'] = 'Pendiente';
$gl_caption_translation['c_4'] = 'Error';
$gl_caption['c_description'] = 'Descripción de la web';
$gl_caption['c_mail'] = 'Correo predeterminado';
$gl_caption['c_title'] = 'Título de la web';
$gl_caption['c_robots'] = 'Tags de la web';
$gl_caption_config['c_interest'] = 'Interés predeterminado';
$gl_caption_custumer['c_group_1'] = 'Búsqueda proximativa';
$gl_caption_custumer['c_group_2'] = 'MLS';
$gl_caption_config['c_search_percent'] = 'Desnivel en las búsquedas cliente y MLS (%)';
$gl_caption_config['c_search_level'] = 'Nivel de proximación en la busqueda';
$gl_caption_translation['c_join_date'] = 'Fecha de envió';
$gl_caption_translation['c_state'] = 'Estado actual:';
$gl_caption_translation['c_numwords'] = 'Número de palabras:';
$gl_caption_translation['c_originaltext'] = 'Texto original:';

$gl_caption['c_search_level_1'] = 'Restrictivo';
$gl_caption['c_search_level_2'] = 'Normal';
$gl_caption['c_search_level_3'] = 'Poco restrictivo';

$gl_caption_config ['c_mls_all'] = 'Marcar por defecto la casilla MLS de los inmuebles';
$gl_caption_config ['c_mls_comission'] = 'Comissión por defecto de las propiedades MLS (%)';
$gl_caption_config ['c_mls_search_always'] = 'Búsqueda MLS activada';
$gl_caption_config ['c_mls_allow_listing'] = 'Permitir listar inmuebles';

$gl_caption_config ['c_0'] = 'No';
$gl_caption_config ['c_1'] = 'Si';
$gl_caption_config ['c_default_mail']='E-mail predeterminado';

$gl_caption_save ['c_save_1'] = 'Clique aquí';
$gl_caption_save ['c_save_2'] = 'para descargar la Copia de seguridad';


$gl_caption_config ['c_google_center_longitude'] = 'Ubicación';
$gl_caption_inmo_admin['c_google_center_longitude'] = 'Posición inicial del mapa';
$gl_caption_config ['c_google_latitude'] = 'Latitud';
$gl_caption_config ['c_google_longitude'] = 'Longitud';
$gl_caption_config ['c_google_center_latitude'] = 'Latitud centro mapa';
$gl_caption_config ['c_center_longitude'] = 'Longitud centro mapa';
$gl_caption_config ['c_google_zoom']='';

// productes
$gl_caption_product_admin ['c_default_sell']='Casella activada, producto en venda';
$gl_caption_product_admin ['c_pvp_required']='P.V.P obligatorio';
$gl_caption_product_admin ['c_home_title']='Titulo de los productos destacados';
$gl_caption_product_admin ['c_group_1'] = $gl_caption_product_public ['c_group_1'] = $gl_caption_inmo_admin['c_group_1'];
$gl_caption_product_admin ['c_group_2'] = $gl_caption_product_public ['c_group_2'] = $gl_caption_inmo_admin['c_group_2'];
$gl_caption_product_admin ['c_group_3'] = $gl_caption_product_public ['c_group_3'] = $gl_caption_inmo_admin['c_group_3'];
$gl_caption_product_admin ['c_group_4'] = $gl_caption_product_public ['c_group_4'] = $gl_caption_inmo_admin['c_group_4'];
$gl_caption_product_admin ['c_group_5'] = $gl_caption_product_public ['c_group_5'] = $gl_caption_inmo_admin['c_group_5'];

$gl_caption_product_public ['c_group_5'] = 'Opciones del módulo de pago';
$gl_caption_product_public ['c_group_6'] = 'Pàgina de inicio';
$gl_caption_product_public ['c_no_sell_text']='Descripción producto no en venda';
$gl_caption_product_public ['c_paypal_sandbox']='Paypal en modo sandbox';
$gl_caption_product_public ['c_banc_name']='Nombre del banco';
$gl_caption_product_public ['c_account_number']='Número de cuenta';
$gl_caption_product_public ['c_paypal_mail']='E-mail de la cuenta PayPal';
$gl_caption_product_public ['c_paypal_mail_repeat'] = 'Repetir e-mail';

$gl_caption_product_admin['c_is_public_tax_included']='Todos los precios (PVP) son I.V.A incluido';
$gl_caption_product_admin['c_is_wholesaler_tax_included']='Todos los precios distribuidor (PVD) son I.V.A incluido';
$gl_caption_product_admin['c_equivalencia']='Recargo de equivalencia (%)';
$gl_caption_product_admin['c_pvp_tax_default']='Precio I.V.A incluido';
$gl_caption_product_admin['c_pvd_tax_default']='Precio distribuidor I.V.A incluido';
$gl_caption_product_admin['c_stock_control']='Control automático de stock';
$gl_caption_product_admin['c_allow_null_stock']='Permitir venda productos con stock negativo';
$gl_caption_product_admin['c_group_10']='Stock';
$gl_caption_product_admin['c_group_11']='Tarifas de envio';
$gl_caption_product_admin['c_send_rate_spain']='España';
$gl_caption_product_admin['c_send_rate_france']='Francia';
$gl_caption_product_admin['c_send_rate_andorra']='Andorra';
$gl_caption_product_admin['c_send_rate_world']='Resta del món';
$gl_caption_product_admin['c_allow_free_send']='Casilla activada, permitir envio gratuito';
$gl_caption_product_admin['c_minimum_buy_free_send']='Compra mínima de';
$gl_caption_product_admin['c_tax_included'] = 'IVA incluído';
$gl_caption_product_admin['c_tax_not_included'] = 'IVA no incluído';