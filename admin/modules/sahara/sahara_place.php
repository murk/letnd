<?
/**
 * SaharaPlace
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c)  30/11/2013
 * @version $Id$
 * @access public
 */
class SaharaPlace extends Module{

	function __construct(){
		parent::__construct();
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function get_records()
	{
	    $listing = new ListRecords($this);
		$listing->has_bin = false;
		$listing->order_by = 'ordre asc, place asc';
		$listing->add_button ('circuit_button', 'action=list_records&menu_id=2503&selected_menu_id=2507', 'boto1', 'after');
		$listing->show_langs = true;
		$listing->add_filter('in_home');
	    return $listing->list_records();
	}
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{
	    $show = new ShowForm($this);
		$show->has_bin = false;
	    return $show->show_form();
	}

	function save_rows()
	{
	    $save_rows = new SaveRows($this);
	    $save_rows->save();
	}

	function write_record()
	{
	    $writerec = new SaveRows($this);
	    $writerec->save();
	}

	function manage_images()
	{
	    $image_manager = new ImageManager($this);
	    $image_manager->execute();
	}
}
?>