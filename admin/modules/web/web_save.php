<?php
function backup_save ()
{
	// TODO-i Ocupa massa memòria, fer el que vaig fer per Toti Ferer i els arxius
	include (DOCUMENT_ROOT . '/common/includes/backup/backup.php');
	ini_set('max_execution_time',120);
	ini_set('memory_limit', '1024M');
	
	$backup_obj = new MySQL_Backup(); 
	$backup_obj->server = $_SESSION['db_host'];
	$backup_obj->username =  $_SESSION['db_user_name'];
	$backup_obj->password = $_SESSION['db_password'];
	$backup_obj->database = $_SESSION['db_name'];
	$backup_obj->struct_only = true;

	$savar_estructura = utf8_encode($backup_obj->Execute());

	//borrem tots els documents antics que hi puguin haver-hi de la copia anterior.
	$ruta=DOCUMENT_ROOT.CLIENT_DIR.'/temp/';
	exist_dir($ruta); //comprovo que existeixi el directori
	delete_files($ruta);


	$backup_obj->struct_only = false;
	$backup_obj->data_only = true;
	//un cop tinc la variable $savar_estructura amb la estructura de la bd i la variable $salvar_dades, creo un arxiu .sql on posso els 2 arxius.
	$salvardades=utf8_encode($backup_obj->Execute());	
	//creo els arxius
	$data = date("dmY");
	
	$arxiu1 = $_SESSION['db_name']."_estructura_".$data.".sql";
	$arxiu2 = $_SESSION['db_name']."_dades_".$data.".sql";


	//  if (!$fp=fopen (DOCUMENT_ROOT.'/'.CLIENT_DIR.'/temp/'.$arxiu1,'a')) {
	if (!$fp=fopen ($ruta.$arxiu1,'a')) {
		echo "Error opening files. Please contact with 9 Sistema.<br/>";
	}
	else {
		fwrite($fp,$savar_estructura );
		fclose($fp);
	}
	if (!$fp2=fopen ($ruta.$arxiu2,'a')) {
		echo "Error opening files. Please contact with 9 Sistema";
	}
	else {
		fwrite($fp2,$salvardades );
		fclose($fp2);
	}
	zip_file($ruta,$arxiu1,$arxiu2,$_SESSION['db_name']."_".$data);
}

function list_records()
{
	global $gl_action, $gl_news, $tpl, $gl_process, $gl_content, $gl_caption;

	switch ($gl_process)
    {
        case 'get_zip':
		// ara esta a 128M per defecte // ini_set("memory_limit","60M"); // aumento memoria, haig de controlar l'error quan sobrepassa
		backup_save();
		//zip_flash();
	}
	$gl_news = '
	<b><a target="save_frame" href="/admin/' . get_all_get_params(array('process'),'?','&') . 'process=get_zip">' . $gl_caption ['c_save_1'] . '</a></b> ' . $gl_caption ['c_save_2'] . '';

}
function zip_file($ruta,$arxiu1,$arxiu2,$nom_arxiu)
{
	global $gl_caption;

    include(DOCUMENT_ROOT . '/common/includes/zip/zipfile.php');
    $zipfile = new zipfile();
	$zipfile->addFile(implode("", file($ruta.$arxiu1)), $arxiu1);
	$zipfile->addFile(implode("", file($ruta.$arxiu2)), $arxiu2);
	//Si volgues descarregar-ho directa
	//header("Content-type: application/force-download"); Aquesta línie es interesant per si dona errors de header
	//$zipfile->output(false,"holaaaaaaaaaaaaa");
	//header("Content-type: application/octet-stream");
	//header("Content-disposition: attachment; filename=test.zip");
	//echo $zipfile -> file();
	$zipfile->output(false, $nom_arxiu);
}
function delete_files ($ruta) {
//borro primer tots els arxius que hi han a la carpeta de la última vegada que ha fet la copia
//recorro el directori i borro tots els arxius que hi hagiin
$directorio = opendir($ruta);
while ($archivo = readdir($directorio)){
if( $archivo !='.' && $archivo !='..' ){
//si es un directorio, volvemos a llamar a la función para que elimine el contenido del mismo
if ( is_dir( $ruta.$archivo ) ) eliminar_recursivo_contenido_de_directorio( $ruta.$archivo );
//si no es un directorio, lo borramos
unlink($ruta.$archivo);
}
}
closedir($directorio);
}
function exist_dir ($ruta) {
if(!opendir($ruta))  //si no el pot obrir el creo amb tots els permisos
{ mkdir ($ruta,0777);
}
else {
return;
}
}


?>