<?

/**
 *
 * Funcions comuns per Lassdive
 *
 * <h2>Lassdive - Notes</h2>
 * <ul>
 * <li>
 * Quan es selecciona un producte, els nous items seleccionats es passen per ajax post {@link LassdiveReservaTimetable::count_busy_hours_new}, per poder
 * comparar amb els que están a la BBDD i els que estàn al corro de la compra i marcar així les hores ocupades
 * </li>
 * </p>
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2011
 * @version $Id$
 * @access public
 */
class LassdiveCommon {

	/*
	 *
	 *
	 * TODO-i Treure de admin que es puguin modificar el sorderitems_id
	 * TODO-i Qui veu el llistat de reserves
	 * TODO-i Llistar beneficis per instructor,  a l'hora de llistar el benefici per instructor s'ha de repartir en els instructors que hi hagi ja que poden ser més d'un per activitat
	 *
	 * TODO-i Qui pot veure què al llistat de reserves
	 *
	 *
	 * TODO-i Borrar relacionats des de productes i desde lassdive. La taula lassdive__orderitem va per triggers
	 *
	 * TODO-i Vista general de tots els plannings per centre
	 *
	 * TODO-i No posar els excels a la carpeta temp del client
	 *
	 * TODO-i Posar a cada grup, si es poden donar permisos o no ( perque no surti mail el grup a assignar privilegis
	 * TODO-i Els productes que tenen data, hi hauria d'haver un checkbox per si volem posar "horaris a consultar amb el centre"

	Per després d'entregar
	TODO-i El missatge de quantitat màxima per producte, s'haurà de treure, ja que ja es controla per el timetable
	TODO-i Missatges en popup i missatges quan es fa un ajax
	TODO-i Repasar excel que quadri la llista de orderitems, els preus sobretot, nomes preu unitat
		TODO-i Comprovar que desde public no es pugui fer excel i altres coses
	TODO-i Poder anular un orderitem de una reserva, si nomes n'hi ha un, s'ha de pregntar si vol esborrar la reserva
	TODO-i Web lassdive, posar SSL i posar jquery-ui i appearance
	TODO-i Comprovar coses rares que fa l'ipad quan es fa el scroll i mostra teclat alhora
	 *
	 * SEGONA FASE
	 * Desactivar la gestió d'usuaris general, i posar-ne una pròpia de Lassdive que es puguin triar els administradors, i usuaris d'aquell administrador ( per la segona fase )
	 *
	 *
	 */

	const GRUP_ADMINISTRADORS = 2;
	const GRUP_GESTORS_WEB = 4;
	const GRUP_ADMINISTRADORS_FRANQUICIA = 5;
	const GRUP_RESERVES = 6;
	const GRUP_RESERVES_FRANQUICIA = 7;
	const GRUP_INSTRUCTORS = 8;
	const GRUP_INSTRUCTORS_FRANQUICIA = 9;

	static public function add_page_files( &$module ) {

		if ( $module->is_index ) {
			$version = 4;
			Page::add_javascript_file( "/admin/modules/lassdive/jscripts/" . LANGUAGE . ".js?v=$version" );
			Page::add_javascript_file( "/admin/modules/lassdive/jscripts/reserva_object.js?v=$version" );
			Page::add_javascript_file( "/admin/modules/lassdive/jscripts/timetable_common.js?v=$version" );
			Page::add_javascript_file( "/admin/modules/lassdive/jscripts/reserva_search.js?v=$version" );
			Page::add_javascript_file( "/admin/modules/lassdive/jscripts/reserva_timetable.js?v=$version" );
			Page::add_javascript_file( "/admin/modules/lassdive/jscripts/reserva_print.js?v=$version" );
			Page::add_javascript_file( "/admin/modules/lassdive/jscripts/epos-print-4.1.0.js?v=$version" );
			Page::add_javascript_file( "/admin/modules/lassdive/jscripts/reserva_list.js?v=$version" );
			Page::add_javascript_file( "/admin/modules/lassdive/jscripts/reserva_form.js?v=$version" );
			Page::add_css_file( "/admin/themes/inmotools/lassdive/styles.css?v=$version" );
			Page::add_css_file( "/admin/themes/inmotools/lassdive/timetable.css?v=$version" );
		}

	}

	static public function get_query_centre_users() {
		return "(
			group_id = " . self::GRUP_GESTORS_WEB . " OR 
			group_id = " . self::GRUP_RESERVES . " OR 
			group_id = " . self::GRUP_RESERVES_FRANQUICIA . " OR 
			group_id = " . self::GRUP_INSTRUCTORS . " OR 
			group_id = " . self::GRUP_INSTRUCTORS_FRANQUICIA . "
			) 
			AND bin = 0";
	}

	static public function get_query_centre_admin() {
		return "(
			group_id = " . LassdiveCommon::GRUP_ADMINISTRADORS_FRANQUICIA . " OR
			group_id = " . LassdiveCommon::GRUP_ADMINISTRADORS . "
			)
			AND bin = 0";
	}

	static public function get_query_all_users() {
		return "group_id <> 1" . "
			AND bin = 0";
	}

	static public function set_locale() {
		switch ( strtolower( LANGUAGE_CODE ) ):
			case "es":
				$locale_code = "es_ES";
				break;
			case "ca":
				$locale_code = "ca_ES";
				break;
			case "en":
				$locale_code = "en_EN";
				break;
			case "fr":
				$locale_code = "fr_FR";
				break;
			case "deu":
				$locale_code = "de_DE";
				break;
			default:
				$locale_code = "es_ES";
				break;
		endswitch;

		$locale = setlocale( LC_TIME, $locale_code );
	}

	static public function is_miquel() {
		$user_id = $_SESSION['user_id'];
		return in_array( $user_id, [ '1', '2', '3' ] );
	}

}