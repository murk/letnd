<?
/*
16-08-2019

*/


if ( is_table( 'product__configadmin' ) && ! Db::get_first( "SELECT * FROM product__configadmin WHERE name='is_pvp_on'" ) ) {
	$update_sql[] = "
		INSERT INTO product__configadmin (`name`, `value`) VALUES ('is_pvp_on', '1');";

} else {
	if ( is_table( 'product__configadmin' ) ) {
		trigger_error( 'Ja existeixen el camp is_pvp_on' );
	}
}

if ( is_table( 'product__configadmin' ) && ! Db::get_first( "SELECT * FROM product__configadmin WHERE name='is_pvd_on'" ) ) {
	$update_sql[] = "
		INSERT INTO product__configadmin (`name`, `value`) VALUES ('is_pvd_on', '1');";

} else {
	if ( is_table( 'product__configadmin' ) ) {
		trigger_error( 'Ja existeixen el camp is_pvd_on' );
	}
}

if ( is_table( 'product__configadmin' ) && ! Db::get_first( "SELECT * FROM product__configadmin WHERE name='is_characteristics_on'" ) ) {
	$update_sql[] = "
		INSERT INTO product__configadmin (`name`, `value`) VALUES ('is_characteristics_on', '1');";

} else {
	if ( is_table( 'product__configadmin' ) ) {
		trigger_error( 'Ja existeixen el camp is_characteristics_on' );
	}
}

if ( is_table( 'product__configadmin' ) && ! Db::get_first( "SELECT * FROM product__configadmin WHERE name='is_ref_required'" ) ) {
	$update_sql[] = "
		INSERT INTO product__configadmin (`name`, `value`) VALUES ('is_ref_required', '1');";

} else {
	if ( is_table( 'product__configadmin' ) ) {
		trigger_error( 'Ja existeixen el camp is_ref_required' );
	}
}