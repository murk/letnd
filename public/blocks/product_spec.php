<?php
// Crea filtre segons una specificació

// Variables
$spec_id = isset( $vars['spec_id'] ) ? $vars['spec_id'] : '';

if ( ! $spec_id ) {
	$show_block = false;

	return;
};

// Si és un enllaç
$vars['block_selected'] = ( isset( $_GET['spec_id'] ) ) ? 1 : 0;
$is_filter              = false;

// Si és un filtre
if ( ! $vars['block_selected'] ) {
	$vars['block_selected'] = ( isset( $_GET['spec_ids'] ) ) ? 1 : 0;
	$is_filter              = true;
}

$query_spec = "SELECT spec FROM product__spec_language WHERE spec_id = '" . $spec_id . "' AND language = '" . LANGUAGE . "'";
$spec_name       = Db::get_first( $query_spec );

$query_spec   = "SELECT spec_type, spec_unit FROM product__spec WHERE spec_id = '" . $spec_id . "'";
$spec_results = Db::get_row( $query_spec );
$spec_type    = $spec_results ['spec_type'];
$spec_unit    = $spec_results ['spec_unit'];


if ( $spec_type == 'text' ) {
	$spec_table      = 'product__product_to_spec_language';
	$spec_tables     = 'product__product_to_spec_language, product__product_to_spec';
	$spec_conditions = "
		AND product__product_to_spec_language.product_spec_id = product__product_to_spec.product_spec_id
        AND language = '" . LANGUAGE . "'";
} else {
	$spec_table      = 'product__product_to_spec';
	$spec_tables     = 'product__product_to_spec';
	$spec_conditions = "";
}

// Afegeixo condicions per els productes públics
$spec_conditions .= "
		AND product__product_to_spec.product_id IN
		 (SELECT product_id FROM product__product WHERE bin = 0  AND
		      (status = 'onsale' OR status = 'nostock' OR status = 'nocatalog') AND product__product.product_id = product__product_to_spec.product_id )";

$query = "SELECT DISTINCT (" . $spec_table . ".spec)
				FROM " . $spec_tables . "
				WHERE product__product_to_spec.spec_id = '" . $spec_id . "' " .
         $spec_conditions .
         " ORDER BY spec ASC;";

$results = Db::get_rows( $query );

$vars['spec_name']       = $spec_name;
$vars['spec_unit']       = $spec_unit;
$vars['spec_id']       = $spec_id;

$conta = 0;
$loop_count = count( $results );
foreach ( $results as $rs ) {

	$l = array();

	//$l['link'] = Page::get_link( 'product', 'product', false, false,  );


	$friendly_params = array(
		urlencode('spec_ids[]') => $spec_id,
		urlencode('spec_id_' . $spec_id . '[]') => $rs['spec']
	);
	$l['link'] =
		get_link( array(
				'tool' => 'product',
				'tool_section' => 'product',
				'detect_page' => true
		)) ;
	$l['link'] .= '?' . urlencode('spec_ids[]') . '=' . $spec_id . '&' . urlencode('spec_ids_' . $spec_id . '[]'). '=' . $rs['spec'];


	// no hi ha id, es busca pel valor tal com és
	$l['item']       = $rs['spec'];
	$l['spec']       = $rs['spec'];
	$l['class']      = '';
	$l['conta']      = $conta ++;
	$l['loop_count'] = $loop_count;

	$selected      = (
		( isset( $_GET[ 'spec_ids_' . $spec_id ] ) &&
		  in_array( $l['spec'], $_GET[ 'spec_ids_' . $spec_id ] ) )
		||
		$spec_id == $l['spec'] ) ? 1 : 0;
	$l['selected'] = $selected;


	$loop[] = $l;
}

if ( $loop ) {
	$loop[0]['class']                    = 'first';
	$loop[ count( $loop ) - 1 ]['class'] = 'last';
	// poso be el loop_count, pot ser que alguna familia no tingui productes i llavors falla
	if ( count( $loop ) != $loop_count ) {
		foreach ( $loop as &$val ) {
			$val['loop_count'] = count( $loop );
		}
	}

} else {
	/* si no hi ha cap resultat no mostro el block */
	$show_block = false;
	return;
}
?>