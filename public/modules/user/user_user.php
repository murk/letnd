<?

/**
 * UserUser
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2011
 * @version $Id$
 * @access public
 */
class UserUser extends Module {

	var $user_id = false, $template = false;
	function __construct() {
		parent::__construct();
	}

	/* funció que es crida per fer una acció del modul automatica o desde un altre modul ( list_records, show_form ) etc...*/
	function do_action( $action = '' ) {
		switch ( $this->parent ) {
			case 'InmoAgent':
			case 'GetUser':
				$this->unset_field ('group_id');
				$this->unset_field ('login');
				$this->unset_field ('passw');
				break;
			default:
				Main::error_404();
		}
		if ( $this->action != 'list_records' && $this->action != 'show_record' ) {
			Main::error_404();
		}

		return parent::do_action( $action );
	}

	function list_records() {
		$GLOBALS['gl_content'] = $this->get_records();
	}

	function get_records() {
		$listing            = new ListRecords( $this );
		$listing->condition = "(status = 'public' AND group_id > 1)";

		return $listing->list_records();
	}

	function show_form() {
		$GLOBALS['gl_content'] = $this->get_form();
	}

	function get_form() {
		$show = new ShowForm( $this );

		if ($this->parent == 'GetUser') {
			$show->id = $this->user_id;
			if ($this->template) $show->template = $this->template;
		}
		$this->set_var('is_get_contact',$this->parent == 'GetContact');

		$show->condition = "(status = 'public' AND group_id > 1)";
		$show->get_values();

		if ($show->has_results) {
			return $show->show_form();
		} elseif ($this->parent == 'GetUser') {
			return '';
		} else {
			Main::redirect('/');
		}

		// per l'error phpstorm
		return '';
	}
}

?>