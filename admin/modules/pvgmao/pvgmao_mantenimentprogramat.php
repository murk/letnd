<?
/**
 * PvgmaoMantenimentprogramat
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2011
 * @version $Id$
 * @access public
 *
 * INFO
 * No es pot entrar un de nou, es generen automàticament a partir del manteniment periòdic de la màquina
 *
 * Per tant tampoc es pot editar: màquina, manteniment, data prevista
 *
 *
 */
class PvgmaoMantenimentprogramat extends Module{

	var $needs_client_id = true;
	var $group_id, $user_is_admin, $user_is_client, $client_id;

	function __construct(){
		parent::__construct();
	}

	function on_load() {
		parent::on_load();
		$this->set_field( 'maquina_id', 'select_condition', 'client_id=' . $this->client_id . ' ORDER BY maquina');
		$this->set_field( 'manteniment_id', 'select_condition', 'client_id=' . $this->client_id . ' ORDER BY manteniment' );
		$this->set_field( 'operari_id', 'select_condition', 'client_id=' . $this->client_id . ' ORDER BY nom, cognoms' );

	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function get_records()
	{
	    $listing = new ListRecords($this);
		$listing->has_bin = false;

		$listing->set_options( 1, 0, 0, 1, 0 );

		// TODO-i Activar editar llistat, s'ha de posar la data_realitzat com a not null sempre
		// $listing->set_options( 1, 0, 1, 1, 0 );

		$listing->add_filter( 'maquina_id' );
		$listing->add_filter( 'manteniment_id' );
		$listing->add_filter( 'operari_id' );

		$listing->condition = "client_id = " . $this->client_id;
		$listing->condition .= " AND maquina_id IN ( SELECT maquina_id FROM pvgmao__maquina WHERE bin = 0)";

		if ( $this->process == 'finished_manteniments' ) {
			// $listing->condition .= ' AND data_previst < now()';
			$listing->condition .= " AND data_realitzat IS NOT NULL AND data_realitzat <> '0000-00-00'";
			$listing->order_by = 'data_realitzat DESC, data_previst DESC';
		}
		elseif ( $this->process == 'pending_manteniments' ) {
			$listing->condition .= " AND (data_realitzat IS NULL || data_realitzat = '0000-00-00')";
			$listing->order_by = 'data_previst ASC, data_realitzat ASC';
		}
		else {
			$listing->order_by = 'data_previst ASC, data_realitzat ASC';
		}

		// Cerca per dates
		$date1 =	unformat_date( R::escape('date1'));
		$date2 = unformat_date(R::escape('date2'));

		// si no poso una data de les dates, busco nomes un dia
		if (!$date1) $date1 = $date2;
		if (!$date2) $date2 = $date1;

		if ($date1 && $date2) {
			$listing->condition .= " AND data_previst BETWEEN  '" . $date1 . "' AND '" . $date2 . "'";
		}

		return $this->get_search_form( $date1, $date2 ) . $listing->list_records();
	}
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{
		if ( $this->action == 'show_form_new' ) {
			Main::error_404();
		}

	    $show = new ShowForm($this);
		$show->condition = "client_id = " . $this->client_id;
		$show->set_options( 1, 0, 0, 0 );
		$show->has_bin = false;
	    return $show->show_form();
	}

	function save_rows()
	{
	    $save_rows = new SaveRows($this);
	    $save_rows->save();
	}

	function write_record()
	{
	    $writerec = new SaveRows($this);
	    $writerec->save();
	}

	function manage_images()
	{
	    $image_manager = new ImageManager($this);
	    $image_manager->execute();
	}



	//
	// FUNCIONS BUSCADOR
	//

	function get_search_form( $date1, $date2 ) {
		$tpl = new phemplate( PATH_TEMPLATES );
		$tpl->set_vars( $this->caption );
		$tpl->set_file( '/pvgmao/mantenimentprogramat_search.tpl' );
		$tpl->set_var( 'menu_id', $GLOBALS['gl_menu_id'] );
		$tpl->set_var( 'client_id', $this->client_id );
		$tpl->set_var( 'date1', format_date_form( $date1 ) );
		$tpl->set_var( 'date2', format_date_form( $date2 ) );
		$ret = $tpl->process();

		return $ret;
	}
}