<form name="theForm" method="post"  action="<?= $link_action ?>" onsubmit="<?= $js_string ?>" enctype="multipart/form-data" target="save_frame">
	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="form">
		<tr>
			<td>
				<div id="form_buttons_holder">
					<table id="form_buttons">
						<tr>
							<td valign="bottom">
								<div class="menu"></div>
							</td>
							<td class="buttons_top">
								<div class="right"></div>
							</td>
						</tr>
					</table>
				</div>

				<? /*  HIDDEN */ ?>

	          <input name="batch_id[<?=$batch_id?>]" type="hidden" value="<?=$batch_id?>" />
	          <input name="batch_id_javascript" type="hidden" value="<?=$batch_id?>" />



				<? ////////////   TITOL  ////////////////?>


				<table width="100%" cellpadding="0" cellspacing="0">
					<tr>
						<td class="formCaptionTitle"><strong>
								<? if ( $form_new ): ?>
									<?= $c_create_batch ?>
								<? else: // ?>
									<?= $c_edit_batch ?>
								<? endif // $action == 'show_form_new' ?>
								&nbsp;&nbsp;</strong></td>
					</tr>
				</table>


				<? ////////////   CAMPS  ////////////////?>

					<table width="100%" cellpadding="0" cellspacing="0" class="formstbl">

						<tr>
							<td valign="top" class="formsCaption">
								<?= $c_batch ?>:
							</td>
							<td valign="top" class="forms">
								<?= $batch ?>
							</td>
						</tr>

					</table>

				<table width="100%" cellpadding="4" cellspacing="2" class="formsButtonstbl">
					<tr>
						<td align="center" class="formsButtons">
							<div class="centerFloat1">
								<div class="centerFloat2">
									<? if ( $form_new ): ?>
										<input type="submit" name="Submit"
										        value="<?= htmlspecialchars_decode($c_create_batch) ?>"
										        class="btn_form1">
									<? else: // ?>
										<input type="submit" name="Submit"
										        value="<?= htmlspecialchars($c_send_edit) ?>"
										        class="btn_form1">
									<? endif // $ ?>
								</div>
							</div>
						</td>
					</tr>
				</table>


			</td>
		</tr>
	</table>
</form>