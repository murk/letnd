<?php
/* 2-3-17
Migració Apache 2.4 PHP5.6
*/

if ( is_table( 'all__configadmin' ) && ! Db::get_first( "SELECT * FROM all__configadmin WHERE name='can_edit_pages'" ) ) {
	$update_sql[] = "
		INSERT INTO all__configadmin (`name`, `value`) VALUES ('can_edit_pages', '0');";

} else {
	if ( is_table( 'all__configadmin' ) ) {
		trigger_error( 'Ja existeixen el camp can_edit_pages' );
	}
}

if ( is_table( 'all__configadmin_page' ) && ! Db::get_first( "SELECT * FROM all__configadmin_page WHERE name='show_body_subtitle'" ) ) {
	$update_sql[] = "
		INSERT INTO all__configadmin_page (`name`, `value`) VALUES ('show_body_subtitle', '1');";

} else {
	if ( is_table( 'all__configadmin_page' ) ) {
		trigger_error( 'Ja existeixen el camp show_body_subtitle' );
	}
}


if ( is_table( 'all__configadmin_page' ) && ! Db::get_first( "SELECT * FROM all__configadmin_page WHERE name='show_body_description'" ) ) {
	$update_sql[] = "
		INSERT INTO all__configadmin_page (`name`, `value`) VALUES ('show_body_description', '1');";

} else {
	if ( is_table( 'all__configadmin_page' ) ) {
		trigger_error( 'Ja existeixen el camp show_body_description' );
	}
}


?>