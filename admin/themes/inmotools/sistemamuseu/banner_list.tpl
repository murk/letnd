<link href="/admin/themes/inmotools/styles/image_drag.css" rel="stylesheet" type="text/css">
<!--[if IE 6]>

<![endif]-->
<!--[if IE 7]>
<style>
#sort
{
	clear: both;
	width: 100%;
	height: 100%;
}

li.sortableitemPlaceholder
{
	height: 0px;
}
</style>
<![endif]-->
<style>
.sortableitemHandle {
	width:200px;
	height:70px;
}
.sortableitem {
	
}
input.wide {
	width:350px;
}
</style>
<script language="JavaScript" src="/admin/jscripts/image_drag.js?v=<?=$version?>"></script>
<form method="post" action="<?=$form_link?>" name="list" target="save_frame">
  <input name="action" type="hidden" value="save_rows_save_records_images<?=$action_add?>">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0" class="formstbl">
          <tr>
            <td id="ajax_list"><br />
              <?if (!$results):?>
                <div class="separador"></div>
                <?endif //results?>
              <ul id="sort" class="sort">
                  <? foreach($loop as $l): extract ($l)?>
                    <? if ($image_src_admin_thumb):?>
                      <li id="<?=$id?>" class="sortableitem">
                        <div id="im_container_<?=$conta?>" class="selectableitem_out">
                          <table width="100%" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                              <td width="230" valign="top"><div id="im_bg_<?=$conta?>">
                                  <div class="sortableitemHandle"><img src="<?=$image_src_admin_thumb?>" width="200" height="70" id="im_<?=$conta?>"></div>
                                  <input name="selected[<?=$conta?>]" type="checkbox" value="<?=$id?>" onclick="checkbox_onclick(this);">
                                  <?=$c_delete?>
                                </div></td>
                              <td valign="top"><div id="select_bg_<?=$conta?>">
                                  <input name="<?=$id_field?>[<?=$id?>]" type="hidden" value="<?=$id?>">
                                  <?=$ordre?>
                                  <input name="image_id_<?=$conta?>" type="hidden" value="<?=$id?>">
                                  <table border="0" cellpadding="2" cellspacing="0">
                                    <tr>
                                      <td><?=$c_url1?></td>
                                      <td><?=$url1?></td>
                                    </tr>
                                    <tr>
                                      <td valign="top"><?=$c_url1_name?>
                                      	&nbsp;&nbsp;&nbsp;</td>
                                      <td><?foreach($url1_name as $l): extract ($l)?><span class="formsCaption"><?=$lang?>
                                      </span><br /><?=$url1_name?><br /><?endforeach //$loop?></td>
                                    </tr>
                                    <tr>
                                      <td><?=$c_url1_target?></td>
                                      <td><?=$url1_target?></td>
                                    </tr>
                                    <tr>
                                      <td><?=$c_status?></td>
                                      <td><?=$status?></td>
                                    </tr>
                                  </table></div><div class="right"><input name="button" type="button" class="btn_form1" id="button" onclick="document.location.href='?action=show_form_edit&action_previous_list=list_records&menu_id=30102&banner_id=<?=$id?>'" value="<?=$c_edit_data?>" /></div>
                              </td>
                            </tr>
                          </table>
                        </div>
                      </li>
                      <? endif //image_src_admin_thumb?>
                    <? endforeach //$loop?>
                <div class="cls"></div>
              </ul></td>
          </tr>
        </table>
        <table width="100%" cellpadding="4" cellspacing="2" class="formsButtonstbl">
          <tr>
            <td align="center" class="formsButtons"><table border="0" cellspacing="0" cellpadding="0">
                <tr valign="middle">
                  <td><div class="centerFloat1">
                      <div class="centerFloat2">
                        <a class="btn_form1" href="javascript:submit_form_save('save_rows_save_records<?=$action_add?>')">
                <?=$c_send_edit?>
              </a>
                        <div class="btn_sep"></div>
                        <a class="btn_form1" href="javascript:submit_list('save_rows_delete_selected', '<?=$c_confirm_deleted?>', true)">
                <?=$c_delete_selected?>
              </a> </div>
                    </div></td>
                </tr>
              </table></td>
          </tr>
        </table></td>
    </tr>
  </table>
</form>
