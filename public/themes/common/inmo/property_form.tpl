<html>
<head>
<link href="/admin/themes/inmotools/styles/general.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="/common/jscripts/jquery/jquery-1.7.2.min.js"></script>
<? if (!DEBUG):?>
<script type="text/javascript" src="/common/jscripts/jquery.onerror.js?v=<?=$version?>"></script>
<? endif //DEBUG?>
<script type="text/javascript" src="/common/jscripts/jquery.dump.js?v=<?=$version?>"></script>
<script type="text/javascript" src="/common/jscripts/jquery.mousewheel.js?v=<?=$version?>"></script>
<script type="text/javascript" src="/common/jscripts/jquery.timepicker.js?v=<?=$version?>"></script>
<script type="text/javascript" src="/common/jscripts/jquery.cookie.js?v=<?=$version?>"></script>
<script type="text/javascript" src="/common/jscripts/jquery.blockUI.js?v=<?=$version?>"></script>
<script type="text/javascript" src="/common/jscripts/jquery.dropshadow.js?v=<?=$version?>"></script>
<script type="text/javascript" src="/admin/jscripts/main.js?v=<?=$version?>"></script>
<script type="text/javascript" src="/admin/languages/<?=$language?>.js?v=<?=$version?>"></script>

<script language="JavaScript">
$(document).ready(				
	function ()
		{
			//$('#content').height($("#popupFrame", top.document).height());
		}
)
</script>
</head>
<body id="mls-form" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<div id="content" style="overflow: auto;"><div id="inner_content">
<?if ($results):?>
<table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-bottom:1px solid #666666;">
              <tr>
                <td><table border="0" cellspacing="6" cellpadding="6">
                    <tr>
                      <td id="details_selected"><strong>
                        <?=$c_data?>
                        </strong></td>
                      <td id="images_click"><a href="#" onClick="return hide_details()"><strong>
                      <?=$c_images?>
                      </strong></a></td>
                      <td id="details_click"><a href="#" onClick="return hide_images()"><strong>
                        <?=$c_data?>
                      </strong></a></td>
                      <td id="images_selected"><strong>
                        <?=$c_images?>
                      </strong></td>
                    </tr>
                </table></td>
              </tr>
 </table>
  <table width="100%" border="0" cellspacing="10" cellpadding="0" class="formstbl" id="details">
    <tr>
      <td align="left" valign="top" >
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td class="listTitle"><strong><?=$c_ref?><?=$ref?></strong><?if ($property):?>&nbsp;<?=$property?><br><br><?endif //property?></td>
  </tr>
</table>
        <?if ($property_title):?>
        <span>
        <?=$property_title?>
        <br />
        <br />
        </span>
        <?endif //property_title?> 
        <?if ($description):?>
        <table border="0" cellspacing="2" cellpadding="0">
            <tr>
              <td style="padding-bottom:3px"><?=$description?></td>
          </tr>
        </table>
        <?endif //description?>
        <table border="0" cellspacing="0" cellpadding="2" class="listDetails">
            <tr>
              <td valign="top"><strong><?=$c_tipus?>:</strong></td>
              <td><?=$tipus?></td>
          </tr>
            <tr>
              <td valign="top"><strong><?=$c_tipus2?>:</strong></td>
              <td><?=$tipus2?></td>
          </tr>
          <?if ($floor_space):?>
            <tr>
              <td valign="top"><strong><?=$c_floor_space?>:</strong></td>
              <td><?=$floor_space?> m2</td>
          </tr>
            <?endif //floor_space?>
          <?if ($land):?> 
            <tr>
              <td valign="top"><strong><?=$c_land?>:</strong></td>
              <td><?=$land?> <?=$land_units?></td>
            </tr>
          <?endif //land?> 
          <?if ($category_id):?>
          <tr>
            <td valign="top"><strong><?=$c_category_id?>:</strong></td>
            <td><?=$category_id?></td>
          </tr>
          <?endif //category_id?>
          <?if ($comarca_id):?>
            <tr>
              <td valign="top"><strong><?=$c_zone?>:</strong>&nbsp;</td>
              <td><?=$comarca_id?><br>
                <?=$provincia_id?> (<?=$country_id?>)</td>
            </tr>
          <?endif //comarca_id?>
              </table>
        <?if ($price):?>
        <br />
        &nbsp;<strong><?=$c_price?>: </strong><strong class="resaltatSmall"><?=$price?>
        <?=CURRENCY_NAME?>&nbsp;</strong>
        <?endif //price?>
          <?if ($price_consult):?>
        <br />
        <span class="price"><strong> <?=$price_consult?></strong>
        &nbsp;</span>
        <?endif //price_consult?>
          <?if ($status):?>
        <br />
        <br />
        <span class="sold"><?=$status?></span>
        <br /> <br />
        <?endif //status?></td>
    </tr>
    <tr>
      <td align="center"><?if ($details):?>
    <table width="96%" cellpadding="4" cellspacing="0" class="listDetails2">
      <?$con = 0?><?foreach($details as $d):?><?if ($$d):?>
                   <?if ($con==0):?><tr><?endif //$conn?>
                      <td align="left" nowrap="nowrap" class="detailsCaption"><strong><?=${'c_'.$d}?>:</strong></td>
                     <td width="50%" align="left"><?=$$d?></td>
                   <?if ($con==0):$con=1?><td class="detailsMiddle" width="1"></td><?else:$con=0?></tr><?endif //$con?><?endif //$$d?><?endforeach //details?>
    </table>
    <?endif //$details?></td>
    </tr>
  </table>
<?endif //results?>
<?if (!$results):?>
  <table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td height="100%" align="center" valign="middle" id="missatge"><?=$c_no_available?></td>
      </tr>
    </table>
<?endif //results?>
<script type="text/javascript">

$(document).ready(				
	function ()
		{		
			//open_flash(gl_default_imatge);
			$('#images_selected').hide();
			$('#images_click').show();
			$('#details_selected').show();
			$('#details_click').hide();
		
			$('#all_images').hide();
			$('#images').show();
			$('#details').show();
		}
)
function hide_details(){
	$('#images_selected').show();
	$('#images_click').hide();
	$('#details_selected').hide();
	$('#details_click').show();

	$('#all_images').show();
	$('#images').hide();
	$('#details').hide();
	return false;	 
}

function hide_images(imatge){
	$('#images_selected').hide();
	$('#images_click').show();
	$('#details_selected').show();
	$('#details_click').hide();

	$('#all_images').hide();
	$('#images').show();
	$('#details').show();
	return false;	 
}
</script>
<table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#666666" id="images">
    <tr><?$conta = 0;?>
   <? foreach($images as $l): extract ($l)?><?$conta++;?>
        <td align="center"><?if ($image_name_thumb):?>
        <img onClick="return open_viewer(this, '<?= $image_conta ?>')" src="<?= $image_src_admin_thumb ?>" vspace="10" border="0"/>
        <?endif //$image_name_details?></td>
        <?if ($conta == 2) break;?>
    <? endforeach //$images?>
    </tr>
</table>
<div id="all_images" style="background-color:#666666;padding:2px;"><div>
   <? foreach($images as $l): extract ($l)?><div style="float:left;padding:2px">
       <img onClick="return open_viewer(this, '<?= $image_conta ?>')" src="<?= $image_src_admin_thumb ?>" vspace="10" border="0"/></div>
    <? endforeach //$images?>
<div class="cls"></div></div>
</div>
</body>
</html>