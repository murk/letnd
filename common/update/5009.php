<?

/* 12-3-13 
  Afegir sectors a productes



 */
if (is_table("product__product") && !is_table('product__product_to_sector')) {

	$update_sql[] = "
					CREATE TABLE `product__product_to_sector` (
						`product_id` INT(11) NOT NULL DEFAULT '0',
						`sector_id` INT(11) NOT NULL DEFAULT '0'
					)
					COLLATE='utf8_spanish2_ci'
					ENGINE=MyISAM;
					";

	$update_sql[] = "
					CREATE TABLE `product__sector` (
						`sector_id` INT(11) NOT NULL AUTO_INCREMENT,
						`ordre` INT(11) NOT NULL DEFAULT '0',
						`bin` TINYINT(1) NOT NULL DEFAULT '0',
						PRIMARY KEY (`sector_id`)
					)
					COLLATE='utf8_spanish2_ci'
					ENGINE=MyISAM;
					";

	$update_sql[] = "
					CREATE TABLE `product__sector_language` (
						`sector_id` INT(11) NOT NULL DEFAULT '0',
						`language` CHAR(3) NOT NULL DEFAULT '' COLLATE 'utf8_spanish2_ci',
						`sector` VARCHAR(100) NOT NULL DEFAULT '' COLLATE 'utf8_spanish2_ci',
						`sector_description` TEXT NOT NULL COLLATE 'utf8_spanish2_ci'
					)
					COLLATE='utf8_spanish2_ci'
					ENGINE=MyISAM;
					";

	$update_sql[] = "
					INSERT INTO `product__configadmin` (`name`, `value`) VALUES ('is_sector_on', '0');
					";
	$update_sql[] = "
					INSERT INTO `product__configadmin` (`name`, `value`) VALUES ('is_sell_on', '1');
					";
	$update_sql[] = "
					INSERT INTO `product__configadmin` (`name`, `value`) VALUES ('is_digital_product_on', '1');
					";
	$update_sql[] = "
					INSERT INTO `all__menu`
						(`menu_id`, `menu_group`, `tool`, `tool_section`, `action`, `parent_id`, `toolmode_id`, `variable`, `ordre`, `link`, `process`, `read1`, `write1`) VALUES
						('20080', 'product', 'product', 'sector', '', '20000', '1', 'PRODUCT_MENU_SECTOR', '8', '', '', ',1,', ',1,'),
						('20081', 'product', 'product', 'sector', 'show_form_new', '20080', '1', 'PRODUCT_MENU_SECTOR_NEW', '1', '', '', ',1,', ',1,'),
						('20082', 'product', 'product', 'sector', 'list_records', '20080', '1', 'PRODUCT_MENU_SECTOR_LIST', '2', '', '', ',1,', ',1,'),
						('20083', 'product', 'product', 'sector', 'list_records_bin', '20080', '1', 'PRODUCT_MENU_SECTOR_LIST_BIN', '3', '', '', ',1,', ',1,');
					";
}
?>