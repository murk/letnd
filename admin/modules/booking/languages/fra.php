<?
// menus eina
if (!defined('INMO_MENU_PROPERTY')){
	define('INMO_MENU_PROPERTY', 'Biens');
	define('INMO_MENU_PROPERTY_NEW', 'Saisir nouveau bien');
	define('INMO_MENU_PROPERTY_LIST', 'Établir la liste des biens');
	define('INMO_MENU_SEARCH_FORM', 'Rechercher et modifier les biens');
	define('INMO_MENU_PROPERTY_BIN', 'Corbeille');
	define('INMO_MENU_CATEGORY', 'Type de bien');
	define('INMO_MENU_CATEGORY_NEW', 'Saisir nouveau type');
	define('INMO_MENU_CATEGORY_LIST', 'Établir la liste des types');
	define('INMO_MENU_TRANSLATION', 'Traductions');
	define('INMO_MENU_PROPERTY_TRANSLATION_LIST', 'Établir la liste des biens à traduire');
	define('INMO_MENU_CATEGORY_TRANSLATION_LIST', 'Établir la liste des types à traduire');
	define('INMO_MENU_CONFIG', 'Configuration Biens');
	define('INMO_MENU_CONFIGADMIN_LIST', 'Configuration section privée');
	define('INMO_MENU_CONFIGPUBLIC_LIST', 'Configuration section publique');
	define('INMO_MENU_CONFIGADMIN', 'Configuration section privée');
	define('INMO_MENU_CONFIGPUBLIC', 'Configuration section publique');
	define('INMO_MENU_ZONE', 'Zones');
	define('INMO_MENU_ZONE_NEW', 'Saisir nouvelle zone');
	define('INMO_MENU_ZONE_LIST', 'Établir la liste des zones');
	define('INMO_MENU_ZONE_BIN', 'Corbeille');
	define('INMO_MENU_TV', 'Vitrines virtuelles');
	define('INMO_MENU_TV_LIST', 'Créer une vitrine');
	define('INMO_MENU_TV_LIST_SELECTED', 'Immobiliers dans le placard');
	define('INMO_MENU_TV_LIST_DOWNLOAD', 'Download le placard');
	define('INMO_MENU_PROPERTY_MAP_LIST', 'Chercher par carte');
	define('INMO_MENU_ADJECTIVE_NEW', 'Entrar nou adjectiu');
	define('INMO_MENU_ADJECTIVE_LIST', 'Listar adjectius');

	define('BOOKING_MENU_BOOK','Reserves');
	define('BOOKING_MENU_BOOK_NEW','Entrar nova reserva');
	define('BOOKING_MENU_BOOK_LIST','Llistar reserves');
	define('BOOKING_MENU_BOOK_LIST_PAST','Reserves finalitzades');
	define('BOOKING_MENU_BOOK_SEARCH','Disponibilitat');
	
	define('BOOKING_MENU_EXTRA','Serveis');
	define('BOOKING_MENU_EXTRA_NEW','Entrar nou servei');
	define('BOOKING_MENU_EXTRA_LIST','Llistar serveis');
	define('BOOKING_MENU_EXTRA_LIST_BIN','Paperera de reciclatge');
	
	define('BOOKING_MENU_SEASON','Temporades');
	define('BOOKING_MENU_SEASON_NEW','Entrar nova temporada');
	define('BOOKING_MENU_SEASON_LIST','Llistar temporades');

	define ('TITLE_COMARCA_ADMIN','Per zona: ');
	define('PROPERTY_AUTO_REF', 'Automàtica');

	define('BOOKING_MENU_PROMCODE','Codi promocional');
	define('BOOKING_MENU_PROMCODE_NEW','Crear codi');
	define('BOOKING_MENU_PROMCODE_LIST','Llistar codi');
	define('BOOKING_MENU_PROMCODE_LIST_BIN','Paperera');

	define('BOOKING_MENU_POLICE','Registre Mossos');
	define('BOOKING_MENU_POLICE_LIST','Llistar reserves');
}

// captions seccio
$gl_caption_book['c_payment'] = 'Payment';
$gl_caption_book['c_payed'] = 'Payed';
$gl_caption_book['c_payment_num'] = 'Núm.';
$gl_caption_book['c_payment_1'] = 'Payment 1';
$gl_caption_book['c_payment_2'] = 'Payment 2';
$gl_caption_book['c_payment_3'] = 'Payment 3';
$gl_caption_book['c_payment_4'] = 'Payment 4';
$gl_caption_book['c_payment_done_1'] =
$gl_caption_book['c_payment_done_2'] =
$gl_caption_book['c_payment_done_3'] =
$gl_caption_book['c_payment_done_4'] = 'Payed';
$gl_caption_book['c_payment_entered_1'] =
$gl_caption_book['c_payment_entered_2'] =
$gl_caption_book['c_payment_entered_3'] =
$gl_caption_book['c_payment_entered_4'] = 'Date';
$gl_caption_book['c_payment_rest'] = 'Pending paymnent';
$gl_caption_book['c_payment_uncheck_message'] = '¿Are you sure you want to uncheck the payment?';
$gl_caption_book['c_payment_send_mail'] = 'send&nbsp;e-mail';
$gl_caption_book['c_payment_send_mail_subject'] = 'Avis de réception du versement pour la réservation: %1$s';
$gl_caption_book['c_payment_send_mail_body'] = 'Chère Madame, Cher Monsieur %1$s, <p>Nous avons le plaisir de vous confirmer la réception de votre versement de %2$s € pour la réservation numéro %3$s en date du %4$s</p>';
$gl_caption_book['c_payment_send_mail_body_rest'] = '<p>Le solde pendant est de %1$s €</p>';
$gl_caption_book['c_payment_send_mail_body2'] = '<p>Cordialement</p>';
$gl_caption_book['c_payment_mail_sent'] = 'The e-mail has been sent to:';
$gl_caption_book['c_payment_mail_not_sent'] = 'The e-mail could not be sent to:';
$gl_caption_book['c_payment_mail_not_sent_no_mail'] = 'The customer has no email address defined';
$gl_caption_book['c_is_owner'] = 'Propiétaire';
$gl_caption_book['c_caption_is_owner'] = 'Tous';
$gl_caption_book['c_is_owner_0'] = 'Non';
$gl_caption_book['c_is_owner_1'] = 'Oui';

$gl_caption_book['c_property_title'] = 'Immeuble';
$gl_caption_book['c_names'] = 'Clients';
$gl_caption_book['c_dates'] = 'Date de la réservation';
$gl_caption['c_dates_from'] = 'Del';
$gl_caption['c_dates_to'] = 'al';
$gl_caption_book['c_date_in'] = 'Date d\'arrivée';
$gl_caption_book_list['c_date_in'] = 'Entrée';
$gl_caption_book['c_date_out'] = 'Date de départ';
$gl_caption_book_list['c_date_out'] = 'Sortie';
$gl_caption_book['c_book_entered'] = 'Réservé';
$gl_caption_book['c_capacity'] = 'Capacité';
$gl_caption_book['c_person'] = 'Personnes';
$gl_caption_book['c_adult'] = 'Adultes';
$gl_caption_book['c_child'] = 'Enfants';
$gl_caption_book['c_baby'] = 'Bébés';
$gl_caption_book['c_book_id'] = 'Réservation';
$gl_caption_book['c_property_id'] = 'Immeuble';
$gl_caption_book['c_choose_first'] = "Il faut d'abord choisir l'adulte numéro 1";
$gl_caption['c_status'] = "State";
$gl_caption['c_status_group_free'] = "Free";
$gl_caption['c_status_group_not_free'] = "Occupied";
$gl_caption['c_status_pre_booked'] = "Pre booked";
$gl_caption['c_status_cancelled'] = "Cancelled";
$gl_caption['c_status_booked'] = "Booked";
$gl_caption['c_status_confirmed_email'] = "Confirmed by email";
$gl_caption['c_status_confirmed_post'] = "Confirmed by post";
$gl_caption['c_status_contract_signed'] = "Contract signed";
$gl_caption_book['c_caption_filter_status'] = 'All';

for ($i=0;$i<=25;$i++){	
	
	$gl_caption_book['c_adult_'.$i] = $i;
	$gl_caption_book['c_child_'.$i] = $i;
	$gl_caption_book['c_baby_'.$i] = $i;
	
}

$gl_caption_book ['c_maximum'] = 'Maximum';
$gl_caption_book ['c_name'] = 'Prénom';
$gl_caption_book ['c_surnames'] = 'Nom';
$gl_caption_book ['c_mail'] = 'Courriel';
$gl_caption_book ['c_phone'] = 'Téléphone';
$gl_caption_book ['c_comment'] = 'Commentaires';
$gl_caption_book ['c_comment_private'] = 'Commentaires privés';
//$gl_caption_book ['c_property_id'] = '';
$gl_caption_book ['c_send_new'] = 'Réserver';
$gl_caption_book ['c_custumers'] = 'Clients';
$gl_caption_book ['c_price_extras'] = 'Extras';
$gl_caption_book ['c_price_extras_search'] = 'Prix extras';
$gl_caption_book ['c_price_total'] = 'Prix total';
$gl_caption_book ['c_price_book'] = 'Prix location';
$gl_caption_book ['c_price'] = 'Prix';
$gl_caption_book ['c_edit_book_button'] = 'Editer la réservation';
$gl_caption_book ['c_download_contract'] = 'Contracte';
$gl_caption_book ['c_custumer_search_group'] = 'Clients';
$gl_caption_book ['c_extra_search_group'] = 'Extras';
$gl_caption_book ['c_recalculate_rate'] = 'Recalculate prix';
$gl_caption_book ['c_is_manual_price'] = 'Prix manuel';

$gl_caption_book['c_form_title'] = 'Réservation';

$gl_messages_book['added'] = "Réservation réalisée entre les dates: %s et %s";
$gl_messages_book['added2'] = "Réservation réalisée par \"%s\" entre les dates: %s et %s";
$gl_messages_book['saved'] = "Réservation actualisée entre les date: %s et %s";
$gl_messages_book['saved2'] = "Réservation actualisée par \"%s\" entre les dates: %s et %s";
$gl_messages_book['deleted'] = "Réservation supprimée entre les dates: %s et %s";
$gl_messages_book['deleted2'] = "Réservation supprimée par %s entre les dates: %s et %s";
$gl_messages_book['full'] = "Occupée entre les dates: %s et %s";
$gl_messages_book['full2'] = 'Le logement "%s" est occupé entre les dates:  %s et %s';
$gl_messages_book['full3'] = 'Réservations en conflit';
$gl_messages_book['has_no_range'] = 'Le logement ne se loue pas à ces dates';
$gl_messages_book['too_many_person'] = $gl_caption['c_too_many_person_error'] = 'Le nombre de personnes excède le maximum pour ce logement';
$gl_messages_book['exceeds_31_days'] = 'Els habitatges de lloguer de temporada no es poden llogar per un periode superior a 31 dies, per qualsevol dubte, si us plau contacti la nostra oficina';


// custumers
$gl_caption['c_add_new_custumer'] = "Ajouter un nouveau client";
$gl_caption['c_edit_custumer'] = "Editer le client";
$gl_caption['c_add_custumer'] = "Sélectionner le client";

$gl_caption['c_adult_singular'] = "Adulte";
$gl_caption['c_child_singular'] = "Enfant";
$gl_caption['c_baby_singular'] = "Bébé";


$gl_caption_extra ['c_extra_book_selected'] = 'Sélectionner';
$gl_caption_extra ['c_extra_book_input'] = '';

// serveis
$gl_caption['c_extra_type'] = "Type";
$gl_caption['c_extra_type_required'] = "Obligatoire";
$gl_caption['c_extra_type_optional'] = "Additionnel";
$gl_caption['c_extra_type_included'] = "Inclus";
$gl_caption['c_extra'] = "Extras";
$gl_caption['c_has_quantity'] = "Quantité sélectionnable";
$gl_caption_list['c_has_quantity'] = "Quantité";
$gl_caption['c_extra_quantity_search'] = "Quantité";
$gl_caption['c_price'] = "Prix";
$gl_caption['c_extra_price'] = "Prix";
$gl_caption['c_comment'] = "Commentaire";
$gl_caption['c_extras'] = "Extras";
$gl_caption['c_concept'] = "Concept";
$gl_caption['c_concept_normal'] = "Extra";
$gl_caption['c_concept_deposit'] = "Caution";
$gl_caption['c_concept_cleaning'] = "Nettoyage";

$gl_caption['c_extra_type_required_search'] = "Extras obligatoires";
$gl_caption['c_extra_type_optional_search'] = "Extras additionnels";
$gl_caption['c_extra_type_included_search'] = "Extras inclus";
$gl_caption['c_extra_total_price_search'] = "Prix total extra";
$gl_caption['c_extra_quantity_search'] = "Quantité extra";
$gl_caption['c_extra_type_search'] = "Type extra";

// temporades
$gl_caption['c_season'] = $gl_caption['c_season_id'] = "Saison";
$gl_caption['c_seasons'] = "Saisons";
$gl_caption['c_ordre'] = "Ordre";
$gl_caption['c_date_start'] = "Début";
$gl_caption['c_date_end'] = "Final";
$gl_caption['c_minimum_nights'] = "Minimum";
$gl_caption['c_minimum_nights_1'] = "1 nuit";
$gl_caption['c_minimum_nights_2'] = "2 nuits";
$gl_caption['c_minimum_nights_3'] = "3 nuits";
$gl_caption['c_minimum_nights_4'] = "4 nuits";
$gl_caption['c_minimum_nights_5'] = "5 nuits";
$gl_caption['c_minimum_nights_6'] = "6 nuits";
$gl_caption['c_minimum_nights_7'] = "1 semaine";
$gl_caption['c_minimum_nights_8'] = "8 nuits";
$gl_caption['c_minimum_nights_9'] = "9 nuits";
$gl_caption['c_minimum_nights_10'] = "10 nuits";
$gl_caption['c_minimum_nights_11'] = "11 nuits";
$gl_caption['c_minimum_nights_12'] = "12 nuits";
$gl_caption['c_minimum_nights_13'] = "13 nuits";
$gl_caption['c_minimum_nights_14'] = "2 semaines";
$gl_caption['c_price_night'] = "Prix par nuit";
$gl_caption_list['c_price_night'] = "Nuit";
$gl_caption['c_price_weekend'] = "Prix par nuit le week-end";
$gl_caption_list['c_price_weekend'] = "Week-end";
$gl_caption['c_price_more_week'] = "Prix + de 7 nuits";
$gl_caption['c_price_week'] = "Prix semaine";
$gl_caption_list['c_price_week'] = "Semaine";
$gl_caption['c_price_week_type'] = "";
$gl_caption['c_price_week_type_week'] = "Par semaine";
$gl_caption['c_price_week_type_night'] = "Par nuit";
$gl_caption['c_color_id'] = $gl_caption['c_color'] = "Couleur";
$gl_caption['c_already_used'] = "couleur déjà attribuée";

$gl_caption['c_detail_season'] = "Saison";
$gl_caption['c_detail_day'] = "Jour";
$gl_caption['c_detail_type'] = "Tarif";
$gl_caption['c_detail_price'] = "Prix";
$gl_caption['c_detail_price_night'] = "Nuit";
$gl_caption['c_detail_price_weekend'] = "Nuit de week-end";
$gl_caption['c_detail_price_week'] = "Semaine prix nuit";
$gl_caption['c_detail_whole_week'] = "Semaine prix semaine";


$gl_caption['c_whole_week'] = "Semaines entières";
$gl_caption_list['c_whole_week_0'] = "";
$gl_caption_list['c_whole_week_1'] = "Oui / Si";

$gl_caption['c_start_day'] = "Premier jour";
$gl_caption['c_start_day_0'] = "N'importe lequel";
$gl_caption_list['c_start_day_0'] = "";
$gl_caption['c_start_day_1'] = "Lu";
$gl_caption['c_start_day_2'] = "Ma";
$gl_caption['c_start_day_3'] = "Me";
$gl_caption['c_start_day_4'] = "Je";
$gl_caption['c_start_day_5'] = "Ve";
$gl_caption['c_start_day_6'] = "Sa";
$gl_caption['c_start_day_7'] = "Di";

$gl_caption_propertyrange['c_form_title'] = "Prix période réservation";
$gl_messages_propertyrange['full2'] = "Il y a déjà une saison définit entre les dates:  %s et %s pour le logement \"%s\"";

$gl_caption['c_minimum_nights_error'] = "Il n'est pas possible de réserver moins de %s nuits pour la période entre le %s et le %s";
$gl_caption['c_whole_week_error'] = "Vous devez réserver des semaines entières pour la période située entre le %s et le %s";
$gl_caption['c_start_day_error'] = "Vous devez réserver des semaines entières de %s à %s pour la période située entre le %s et le %s";
$gl_caption['c_equal_dates_error'] = "Les deux dates ne peuvent être les même";
$gl_caption['c_no_date_selected'] = "Vous n'avez sélectionné aucune date";



$gl_caption['c_promcode'] = 'Code promotionnel';
$gl_caption_promcode['c_name'] = 'Nom';
$gl_caption_promcode['c_discount_pvp'] = 'Rabais PVP';
$gl_caption_promcode['c_discount_type'] = 'Type de rabais';
$gl_caption_promcode['c_discount_type_fixed'] = 'Montant fixe';
$gl_caption_promcode['c_discount_type_percent'] = 'Pourcentage';
$gl_caption_promcode['c_minimum_amount'] = 'Dépense minimum';
$gl_caption_promcode['c_start_date'] = 'Date de début';
$gl_caption_promcode['c_end_date'] = 'Date de fin';
$gl_caption_book['c_promcode_discount'] = 'Dte. codi promocional';
$gl_caption['c_promcode_button'] = '';

$gl_caption_promcode['c_custumer_id'] = 'Client';
$gl_caption['c_custumer_caption'] = 'Tous';
$gl_caption_promcode['c_custumer_id_format'] = '%s %s %s';
$gl_caption_promcode['c_one_time_only'] = 'Aplicar sólo una vez por cliente';
$gl_caption_promcode_list['c_one_time_only'] = 'Sólo una vez';


$gl_caption_police['c_police_search_title'] = 'Reservas entre las fechas';
$gl_caption['c_treatment_police'] = 'Sexe';
$gl_caption_police['c_treatment_sr'] = 'Masculí';
$gl_caption_police['c_treatment_sra'] = 'Femení';
$gl_caption['c_download_selected'] = 'Descarregar arxiu';


$gl_caption['c_tt'] =  $gl_caption['c_price_tt'] = 'Taxe touristique';
$gl_caption['c_promcode_button'] = 'Voir';

$gl_caption['c_promcode_err_not_valid'] = 'Le code promotionnel n\'est pas valable';
$gl_caption['c_promcode_err_minimum'] = 'Pour appliquer le rabais, le montant de la réservation doit être supérieur à %s';
$gl_caption['c_promcode_err_expired'] = 'Le code promotionnel a expiré';
$gl_caption['c_promcode_err_used'] = 'Ce code a déjà été utilisé une fois';
$gl_caption['c_promcode_err_not_logged'] = 'Per aplicar descompte s\'ha d\'estar registrat';

$gl_caption ['c_promcode']='Code promotionnel';

// buscador
$gl_caption['c_by_ref'] = '( <strong>Ref</strong>:</strong> "10 23 45" . <strong>Date:</strong> "12/2015" , "2015" , "22/12/2015" )';
$gl_caption['c_fields'] = "Champs";
$gl_caption['c_nights'] = "Nuits";
$gl_caption['c_excel_name'] = "reservas";
$gl_caption['c_exact_date'] = "Date exacte";

$gl_messages ['has_no_owner']="Attention, l'immeuble n'a pas aucun propriétaire assigné";


// Pagament
$gl_caption['c_order_status'] = 'État paiement';
$gl_caption_list['c_order_status'] = 'État';
$gl_caption['c_order_status_paying'] = 'Attendant paiement';
$gl_caption['c_order_status_failed'] = 'Paiement échoué';
$gl_caption['c_order_status_pending'] = 'Paiement manquant d\'ètre acceptée';
$gl_caption['c_order_status_denied'] = 'Paiement refusé';
$gl_caption['c_order_status_voided'] = 'Paiement annulé';
$gl_caption['c_order_status_refunded'] = 'paiement retourné';
$gl_caption['c_order_status_completed'] = 'Payé';

$gl_caption['c_payment_method'] = 'Paiement méthode';
$gl_caption['c_payment_method_paypal'] = 'Paypal';
$gl_caption['c_payment_method_account'] = 'Virement bancaire';
$gl_caption['c_payment_method_directdebit'] = 'Reçu de la Banque';
$gl_caption['c_payment_method_lacaixa'] = 'Carte de crédit / paiement';
$gl_caption['c_payment_method_none'] = '';
$gl_caption['c_payment_method_cash'] = 'Cash';
$gl_caption['c_payment_method_physicaltpv'] = 'TPV physique';

for ( $i = 1; $i <= 4; $i ++ ) {
	$gl_caption[ 'c_order_status_' . $i ]                    = $gl_caption['c_order_status'];
	$gl_caption_list[ 'c_order_status_' . $i ]               = $gl_caption_list['c_order_status'];
	$gl_caption[ 'c_order_status_' . $i . '_paying' ]        = $gl_caption['c_order_status_paying'];
	$gl_caption[ 'c_order_status_' . $i . '_failed' ]        = $gl_caption['c_order_status_failed'];
	$gl_caption[ 'c_order_status_' . $i . '_pending' ]       = $gl_caption['c_order_status_pending'];
	$gl_caption[ 'c_order_status_' . $i . '_denied' ]        = $gl_caption['c_order_status_denied'];
	$gl_caption[ 'c_order_status_' . $i . '_voided' ]        = $gl_caption['c_order_status_voided'];
	$gl_caption[ 'c_order_status_' . $i . '_refunded' ]      = $gl_caption['c_order_status_refunded'];
	$gl_caption[ 'c_order_status_' . $i . '_completed' ]     = $gl_caption['c_order_status_completed'];
	$gl_caption[ 'c_payment_method_' . $i ]                  = $gl_caption['c_payment_method'];
	$gl_caption[ 'c_payment_method_' . $i . '_paypal' ]      = $gl_caption['c_payment_method_paypal'];
	$gl_caption[ 'c_payment_method_' . $i . '_account' ]     = $gl_caption['c_payment_method_account'];
	$gl_caption[ 'c_payment_method_' . $i . '_directdebit' ] = $gl_caption['c_payment_method_directdebit'];
	$gl_caption[ 'c_payment_method_' . $i . '_lacaixa' ]     = $gl_caption['c_payment_method_lacaixa'];
	$gl_caption[ 'c_payment_method_' . $i . '_none' ]        = $gl_caption['c_payment_method_none'];
	$gl_caption[ 'c_payment_method_' . $i . '_cash' ]        = $gl_caption['c_payment_method_cash'];
	$gl_caption[ 'c_payment_method_' . $i . '_physicaltpv' ] = $gl_caption['c_payment_method_physicaltpv'];
}

$gl_caption ['c_books_not_deleted_not_owner'] = "The marked booking have not been deleted because they where created by an external application";

$gl_caption['c_book_owner'] = "Reservée par";