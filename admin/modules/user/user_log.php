<?php
function save_rows()
{
    $save_rows = new SaveRows;
    $save_rows->save();
}

function list_records()
{ 
    $listing = new ListRecords;
    $listing->join = 'INNER JOIN user__user USING (user_id)';
	$listing->condition = "group_id >= " . $_SESSION['group_id'];
	$listing->has_bin = false;
	$listing->set_options(0, 0, 0, 0, 0, 0, 0, 0, 1, 0);
	$listing->order_by = 'date DESC, login DESC';
    $listing->list_records();
}

function show_form()
{
    $show = new ShowForm;
    $show->show_form();
}

function write_record()
{
    $writerec = new SaveRows;
    $writerec->save();
}

function manage_images()
{
    $image_manager = new ImageManager;
    $image_manager->execute();
}
?>