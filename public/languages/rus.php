<?
// blocks

define('INMO_PRICE_FIRST','До %s&nbsp;'.CURRENCY_NAME);
define('INMO_PRICE_MIDDLE','От %s и до %s&nbsp;'.CURRENCY_NAME);
define('INMO_PRICE_LAST','Более %s&nbsp;'.CURRENCY_NAME);
define('INMO_FLOOR_SPACE_FIRST','Fins a %s m<sup>2</sup>');
define('INMO_FLOOR_SPACE_MIDDLE','De %s a %s m<sup>2</sup>');
define('INMO_FLOOR_SPACE_LAST','Més de %s m<sup>2</sup>');
define('INMO_PERSON_FIRST','Fins a %s m<sup>2</sup>');
define('INMO_PERSON_MIDDLE','De %s a %s m<sup>2</sup>');
define('INMO_PERSON_LAST','Més de %s m<sup>2</sup>');
define ('LETND','Letnd');

$gl_caption['c_offers'] = 'Предложения';


// captions

$gl_caption['c_send_mail'] = 'отправить E-mail';
$gl_caption['c_more_info'] = 'Допольнитеньная информация';
$gl_caption['c_read_more'] = 'Прочитайте больше';
$gl_caption['c_title_search'] = 'искать';
$gl_caption['c_yes'] = 'да';
$gl_caption['c_no'] = 'нет';
$gl_caption['c_send'] = 'отправить ';
$gl_caption['c_clear'] = 'Стереть';
$gl_caption['c_send_to_friend']='отослать другу';
$gl_caption['c_print']='распечатать';
$gl_caption['c_zoom_link']='увеличить изображение';
$gl_caption['c_contact_link']='связаться';

// bloc inmo_categories
$gl_caption['c_sell'] = 'Продажа';
$gl_caption['c_rent'] = 'Сдача внаём';
$gl_caption['c_temp'] = 'Сдача внаём посезонно';
$gl_caption['c_moblat'] = 'Сдача внаём с мебелью';
$gl_caption['c_selloption'] = 'Miete Kaufoption';

// seo
$gl_caption['c_seo_verd_1']='<a href="http://www.letnd.com" target="_blank" title="Diseño páginas web en Girona">Diseño y programación web:<img src="/common/images/logoletnd.png" width="95" height="16" alt="Letnd: Diseño web, posicionamiento web, páginas web en Girona" style="vertical-align:text-bottom" /></a>';
$gl_caption['c_seo_verd_2']='<a href="http://www.letnd.com" target="_blank" title="Web site design in Girona">Web programming:<img src="/common/images/logoletnd.png" width="95" height="16" alt="Letnd: Web design, web position, web pages girona" style="vertical-align:text-bottom" /></a>';
$gl_caption['c_seo_verd_3']='<a href="http://www.letnd.com" target="_blank" title="Diseño páginas web en Girona">Creado por:<img src="/common/images/logoletnd.png" width="95" height="16" alt="Letnd: Diseño web, posicionamiento web, páginas web en Girona" style="vertical-align:text-bottom" /></a>';
$gl_caption['c_seo_verd_4']='<a href="http://www.letnd.com" target="_blank" title="Diseño páginas web en Girona">Diseño web:<img src="/common/images/logoletnd.png" width="95" height="16" alt="Letnd: Diseño web, posicionamiento web, páginas web en Girona" style="vertical-align:text-bottom" /></a>';
$gl_caption['c_seo_blanc_1']='<a href="http://www.letnd.com" target="_blank" title="Diseño páginas web en Girona">Diseño y programación web:<img src="/common/images/logoblanc.png" width="95" height="16" alt="Letnd: Diseño web, posicionamiento web, páginas web en Girona" style="vertical-align:text-bottom" /></a>';
$gl_caption['c_seo_blanc_2']='<a href="http://www.letnd.com" target="_blank" title="Diseño páginas web en Girona">Programación web:<img src="/common/images/logoblanc.png" width="95" height="16" alt="Letnd: Diseño web, posicionamiento web, páginas web en Girona" style="vertical-align:text-bottom" /></a>';
$gl_caption['c_seo_blanc_3']='<a href="http://www.letnd.com" target="_blank" title="Diseño páginas web en Girona">Creado por:<img src="/common/images/logoblanc.png" width="95" height="16" alt="Letnd: Diseño web, posicionamiento web, páginas web en Girona" style="vertical-align:text-bottom" /></a>';
$gl_caption['c_seo_blanc_4']='<a href="http://www.letnd.com" target="_blank" title="Diseño páginas web en Girona">Diseño web:<img src="/common/images/logoblanc.png" width="95" height="16" alt="Letnd: Diseño web, posicionamiento web, páginas web en Girona" style="vertical-align:text-bottom" /></a>';

$gl_caption['c_seo_custom_2']='<a href="http://www.letnd.com" target="_blank" title="Diseño páginas web en Girona">Programación web:<img src="/'.CLIENT_DIR.'/templates/images/letnd.png" alt="Letnd: Diseño web, posicionamiento web, páginas web en Girona" /></a>';
$gl_caption['c_seo_custom_4']='<a href="http://www.letnd.com" target="_blank" title="Diseño páginas web en Girona">Diseño web:<img src="/'.CLIENT_DIR.'/templates/images/letnd.png" alt="Letnd: Diseño web, posicionamiento web, páginas web en Girona" /></a>';

$gl_caption['c_tercersetena'] = 'Diseño <a class="tercersetena" target="_blank" href="http://www.tercersetena.com">Tercersetena.com</a>';

// NO TRADUIT
$gl_caption['c_tipus_temp'] = 'Сдача внаём посезонно';
$gl_caption['c_tipus_moblat'] = 'Сдача внаём с мебелью';
// FI NO TRADUIT

// TAMBÉ A ADMIN
// Caption default
$gl_caption['c_form_title'] = 'Pедактировать данные';
$gl_caption['c_config_title'] = 'Показать конфигурацию';
$gl_caption['c_edit'] = 'Редактировать';


$gl_caption['c_edit_button'] = 'Pедактировать  данные';
$gl_caption['c_view_button'] = 'Показать данные';
$gl_caption['c_edit_images_button'] = 'Показать изображения';
$gl_caption['c_add_images_button'] = 'Afegir imatges';



$gl_caption['c_edit_data'] = 'Editar dades';
$gl_caption['c_edit_images'] = 'Editar imatges';
$gl_caption['c_add_images'] = 'Afegir imatges';
$gl_caption['c_search_form'] = 'Искать';
$gl_caption['c_send_new'] = 'Сохранить';
$gl_caption['c_send_edit'] = 'Сохранить изменения';
$gl_caption['c_delete'] = 'Удалить';
$gl_caption['c_action'] = 'Операция';
$gl_caption['c_delete_selected'] = 'Esborrar seleccionats';
$gl_caption['c_restore_selected'] = 'Recuperar seleccionats';
$gl_caption['c_confirm_deleted'] = 'Els elements seleccionats s\\\'esborraran definitivament de la base de dades \\n\\nDesitges continuar?';
$gl_caption['c_confirm_images_deleted'] = 'Les imatges seleccionats s\\\'esborraran definitivament\\n\\nDesitges continuar?';
$gl_caption['c_confirm_form_bin'] = 'El registre s\\\'enviarà a la paperera de reciclatge \\n\\nDesitges continuar?';
$gl_caption['c_confirm_form_deleted'] = 'El registre s\\\'esborrarà definitivament de la base de dades \\n\\nDesitges continuar?';
$gl_caption['c_move_to'] = ' Перевести в:';
$gl_caption['c_print_all'] = 'Распечатать всё';
$gl_caption['c_print'] = 'Pаспечатать';
$gl_caption['c_order_by'] = ' Организовать по:';
$gl_caption['c_select'] = 'Выбрать';
$gl_caption['c_all'] = 'всё';
$gl_caption['c_nothing'] = 'ничего';
$gl_caption['c_preview'] = 'Показать';
$gl_caption['c_yes'] = 'Да';
$gl_caption['c_no'] = 'Нет';
$gl_caption['c_options_bar'] = 'Варианты';
$gl_caption['c_file_maximum'] = 'màxim %s arxius alhora';
$gl_caption['c_file_maximum_1'] = 'màxim 1 arxiu alhora';


// Message default
$gl_messages['title_edit']='Редактировать';
$gl_messages['title_copy']='Скопировать данные';
$gl_messages['added']='Регистр добавлен правильно';
$gl_messages['add_exists']=' Уже существует регистр с "%s"';
$gl_messages['saved']='Регистр изменён правильно';
$gl_messages['not_saved']='Регистр не изменён';
$gl_messages['no_records']='В этом списке нет регистра';
$gl_messages['no_records_bin']='Корзина пуста';
$gl_messages['list_saved']='Изменения в регистрах осуществлены правильно';
$gl_messages['list_not_saved']='Не изменён ни один регистр, изменений не найдено';
$gl_messages['list_deleted']='Регистр удален';
$gl_messages['list_bined']='Регистры отправлены в корзину';
$gl_messages['list_restored']='Регистры восстановлены правильно';
$gl_messages['concession_no_acces']='У Вас нет доступа в этот раздел';
$gl_messages['concession_no_write']='У Вас нет доступа для написания   в этом разделе';
$gl_messages['select_item']='Чтобы выбрать, нажми  на регистр со списка';
$gl_messages['related_not_deleted']='Отмеченные регистры нельзя стереть, так как  они имеют отношеения к другим регистрам';


$gl_messages['deleted']='Регистр удалён';
$gl_messages['bined']='Регистр отправлен в корзину';
$gl_messages['restored']='Регистр восстановлен правильно';
$gl_caption['c_ok'] = 'Принять';
$gl_caption['c_cancel'] = 'Закрыть';
$gl_messages['post_max_size']='Объём формуляра (содержимое + приложенные файлы) превышает допустимый лимит ';
$gl_messages['post_file_max_size']=' Объем  приложенных файлов  превышает допустимый лимит ';
$gl_messages['assigned']='Выбранные регистры';

$gl_caption['c_form_sent'] = 'Спасибо, что установили с нами контакт. На ваш запрос ответим в ближайшее время';
$gl_caption['c_form_not_sent'] = 'Формуляр не удалось переслать, пожалуйста, попытайтесь еще раз';

$gl_caption['c_cookies_message'] = 'Aquesta web utilitza cookies pròpies i de tercers per a millorar l\'experiència de navegació així com per a tasques d\'anàlisi. En continuar navegant, entenem que accepta l\'';
$gl_caption['c_cookies_message_more'] = 'ús de cookies';
$gl_caption['c_cookies_message_accept'] = 'Accepto';

$gl_caption['c_cookies_text'] = '

<p class="accept-cookies-title">Què són les cookies?</p>
<p>Una cookie es un petit arxiu que s’emmagatzema a l’ordinador de l’usuari i ens permet reconèixer-lo. El conjunt de cookies ens ajuda a millorar la qualitat del nostre web, permetent-nos controlar quines pàgines troben els nostres usuaris útils i quines no.</p>
<p>Les cookies són essencials per al funcionament d’Internet, aportant innumerables avantatges en la prestació de serveis interactius, facilitant-nos la navegació i usabilitat del nostre web. Tingueu en compte que les cookies no poden fer malbé el vostre equip i que, a canvi, si són activades ens ajuden a identificar i resoldre els errors.</p>
<p class="accept-cookies-title">Quin tipus de cookies utilitza aquesta web?</p>
<p>Cookies pròpies: són aquelles que s’envien a l’equip de l’usuari des d’un equip o domini gestionat per l’editor i des del que es presta el servei sol•licitat per l’usuari.</p>
<p>Cookies de tercers: són aquelles que s’envien a l’equip de l’usuari des d’un equip o domini no gestionat per l’editor sinó per una altra entitat que analitza les dades obtingudes.</p>
<p>Cookies persistents:són un tipus de cookies en el qual les dades segueixen emmagatzemades en el terminal i poden ser accedides i tractades pel responsable de la cookie.</p>
<p>Cookies d\'analítica web: a través de l\'analítica web s\'obté informació relativa al nombre d\'usuaris que accedeixen al web, el nombre de pàgines vistes, la freqüència i repetició de les visites, la seva durada, el navegador utilitzat, l\'operador que presta el servei, l\'idioma , el terminal que utilitza, o la ciutat a la qual està assignada la seva adreça IP. Informació que possibilita un millor i més apropiat servei per part d\'aquesta web.</p>
<p class="accept-cookies-title">Acceptació de l\'ús de cookies.</p>
<p>Assumim que vostè accepta l’ús de cookies. No obstant, vostè pot restringir, bloquejar o borrar les cookies d’aquesta web o qualsevol altra pàgina a partir de la configuración del seu navegador.</p>
<p>A continuación li mostrem les pàgines d’ajuda dels navegadors principals:</p>
<ul>
<li>Internet Explorer: <a href="http://windows.microsoft.com/es-xl/internet-explorer/delete-manage-cookies#ie="ie-10"" target="_blank">windows.microsoft.com/es-xl/internet-explorer/delete-manage-cookies#ie="ie-10"</a></li>
<li>FireFox: <a href="http://support.mozilla.org/es/kb/Borrar%20cookies" target="_blank">support.mozilla.org/es/kb/Borrar%20cookies</a></li>
<li>Chrome: <a href="http://support.google.com/chrome/answer/95647?hl="es"" target="_blank">support.google.com/chrome/answer/95647?hl="es"</a></li>
<li>Safari: <a href="http://www.apple.com/es/privacy/use-of-cookies/" target="_blank">www.apple.com/es/privacy/use-of-cookies/</a></li>
</ul>
';

// contrasenyes
$gl_caption['c_password_forgotten'] = 'Have you forgotten the password?';
$gl_caption['c_password_insert_mail'] = 'Please, enter your e-mail, and we\'ll send you a new one to your mail account';
$gl_caption['c_password_insert_mail_exists'] = 'If you have forgotten your password, please click accept and we\'ll send you a new one to your mail account';
$gl_caption['c_password_mail_error'] = 'The e- mail address entered, doesn´t exist, please enter the correct';
$gl_caption['c_password_sent'] = 'A new password have been sent to your e-mail';
$gl_caption['c_password_mail_subject'] = 'The new password';
$gl_caption['c_password_mail_text_1'] = 'The new password is';
$gl_caption['c_password_mail_text_2'] = 'You can change the password in our website';
$gl_caption['c_welcome'] = 'Wellcome';

$gl_caption['c_new_custumer_exists'] = 'You have already introduced a person with this name, full name or date of birth. You can:';
$gl_caption['c_new_custumer_exists_change_name'] = 'Correct the name or date of birth
';
$gl_caption['c_new_custumer_exists_return'] = 'Return to the reservation and select a person on the list';

// captcha
$gl_caption['c_recaptcha_error'] = 'Please validate the "CAPTCHA" correctly';
?>