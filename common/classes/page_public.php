<?php
// Variables globals
class Page {
    var $tpl;
	var $page_title;
	var $page_og_title;
    var $page_description;
    var $page_og_description;
    var $page_og;
    var $page_keywords;
    var $title;
    var $body_subtitle;
    var $body_description;
    var $top_title = '';
    var $top_page_id = 0;
    var $top_body_subtitle = '';
    var $message;
    var $content;
    var $tool_content;
    var $user_name;
    var $user_surname;
    var $template;
    var $html;
    var $page_id;
    var $page_text_id;
    var $page_file_name;
    var $page_template;
    var $parent_id;
    var $level;
    var $menu;
    var $is_home;
    var $is_tool_home;
	var $javascript;
    var $is_frame = false;
    var $is_page_home;
	var $vars = array(); // per passar variables al tpl desde qualsevol lloc, ej: desde un modul
	var $breadcrumb = array(); // array que guarda nous items definits pels moduls pel breadcumb a part dels que corresponen a la pàgina on som
	
    function __construct()
    {
        global $gl_is_home, $gl_is_tool_home;
        $this->is_home = $gl_is_home;
        $this->is_tool_home = $gl_is_tool_home;

        if (isset($_POST['is_frame']) || isset($_GET['is_frame']))$this->is_page = true;
        $this->set_page_vars();

        $this->html = isset($_GET["html"])?$_GET["html"]:'';
		$this->get_page_row();
        
        // $this->template = $this->page_id?'general.tpl':''; // si el poso primer el page_id també pot ser la home NO TE SENTIT POSAR-HO
        $this->template = $this->is_home?'home.tpl':'general.tpl';
        if (isset($_GET['template'])) {
            $this->template = $_GET['template']?$_GET['template'] . '.tpl':''; // si vull un template del get, també puc posar una cadena buida per no tindre cap template
        }
		
        $this->tpl = new phemplate(PATH_TEMPLATES);
		
        Debug::p( [
					[ 'Funció especial', 'page_on_init' ],
				], 'general');
        
		// funció que es pot posar en un client concret
		if (function_exists('page_on_init')) page_on_init($this);
    }

    function load_template()
    {
        $this->tpl->set_vars($this->vars);
        $this->tpl->set_file($this->template);
    }

    function set_page_vars()
    {
        global
        $gl_tool,
        $gl_tool_section,
        $gl_action,
        $gl_parent_id,
        $gl_db_classes_table,
        $gl_table_overrided,
        $gl_db_classes_id_field;
        // accio per defecte
        if (!$gl_action) $gl_action = 'list_records';

        if (!$gl_table_overrided) $gl_db_classes_table = $gl_tool . '__' . $gl_tool_section;
        if (!$gl_table_overrided) $gl_db_classes_id_field = $gl_tool_section . '_id';		
		
        $this->page_title = PAGE_TITLE;
        $this->page_og_title = PAGE_TITLE;
        $this->page_description = PAGE_DESCRIPTION;
        $this->page_og_description = PAGE_DESCRIPTION;
        $this->page_og = [];
        $this->page_keywords = PAGE_KEYWORDS;
        $this->title = '';
        $this->body_subtitle = '';
        $this->body_description = '';
		$this->set_var('images',false);
    }
    // Assignar totes les variables sencilles de la pàgina, titol, menus fixes, etc...
    function assign_variables()
    {
        global $gl_caption, $gl_calendar, $gl_language, $gl_action, $gl_tool, $gl_tool_section,$gl_language_code;
		
		$top_page_text_id = $this->top_page_id?Db::get_first("SELECT page_text_id FROM all__page WHERE page_id = " . $this->top_page_id):'';
			
        $this->tpl->set_var('action', $gl_action);
        $this->tpl->set_var('tool', $gl_tool);
        $this->tpl->set_var('tool_section', $gl_tool_section);
        $this->tpl->set_var('menu', $this->menu);
        $this->tpl->set_var('page_id', $this->page_id);
        $this->tpl->set_var('page_text_id', $this->page_text_id);
        $this->tpl->set_var('top_page_id', $this->top_page_id);
        $this->tpl->set_var('top_page_text_id', $top_page_text_id);
        $this->tpl->set_var('language', $gl_language);
        $this->tpl->set_var('language_code', $gl_language_code);
        $this->tpl->set_var('client_dir', CLIENT_DIR);
        $this->tpl->set_var('page_title', $this->page_title);
        $this->tpl->set_var('page_og_title', $this->page_og_title);
        $this->tpl->set_var('page_description', $this->page_description);
        $this->tpl->set_var('page_og_description', $this->page_og_description);
        $this->tpl->set_var('page_og', $this->page_og);
        $this->tpl->set_var('page_keywords', $this->page_keywords);
        $this->tpl->set_var('page_address', nl2br(PAGE_ADDRESS));
        $this->tpl->set_var('page_slogan',  nl2br(PAGE_SLOGAN));
        $this->tpl->set_var('page_template', $this->page_template!='');
		$this->tpl->set_var('title', $this->title);
		$this->tpl->set_var('body_subtitle', $this->body_subtitle);
		$this->tpl->set_var('body_description', $this->body_description);
		$this->tpl->set_var('top_title', $this->top_title);
		$this->tpl->set_var('top_body_subtitle', $this->top_body_subtitle);
		$this->tpl->set_var('javascript', $this->javascript);
        $this->tpl->set_var('message', $this->message);
        $this->tpl->set_var('message_class', $this->message?'':'hidden');
        $this->tpl->set_var('content', $this->content);
        $this->tpl->set_var('tool_content', $this->page_template?$this->tool_content:''); // quan no hi ha page_template, també assigno tool_content a $content, i llavors sortiria repetit
        $this->tpl->set_var('default_mail', DEFAULT_MAIL);
        $this->tpl->set_var('date', date('j-n-Y', time()));
        $this->tpl->set_vars($gl_caption);
		$this->tpl->set_var('calendar', $gl_calendar);
		$this->tpl->set_var('showiframe', DEBUG?'visible':'none');
		$this->tpl->set_var('is_home', $this->is_home);
		$this->tpl->set_var('is_tool_home', $this->is_tool_home);
		$this->tpl->set_var('is_page_home', $this->is_page_home);
		$this->tpl->set_var('is_only_page', $this->page_text_id && !$gl_tool_section);
		$this->tpl->set_var('short_host_url', SHORT_HOST_URL);
        $this->tpl->set_var("url", HOST_URL);
		
		$viewsite = $GLOBALS['gl_is_mobile']?'desktop':'mobile';

		$uri = $_SERVER["REQUEST_URI"];
		if (strpos($uri,'viewsite')!==false){
			
			$viewsite_url = str_replace('mobile', $viewsite, $uri);
			$viewsite_url = str_replace('desktop', $viewsite, $viewsite_url);
		}
		else{
		
			$viewsite_url =strpos($uri,'?')===false?'?viewsite='.$viewsite:'&viewsite='.$viewsite;
			$viewsite_url = $uri . $viewsite_url;
		}
		
		$this->tpl->set_var('view_site',  $viewsite_url);
		$this->tpl->set_var('is_mobile',$GLOBALS['gl_is_mobile']);	
				
		
		// classes atributs per el body o main
		$css_selector_class = array();
		$css_selector_id = '';
		$css_selector_home_id = '';
		
		if ($this->is_home){
			$css_selector_home_id= ' id="home"';
			$css_selector_id= 'home';
			$css_selector_class[]= 'home';
		}
		else{
			$css_selector_home_id= ' id="inner"';
			if ($gl_tool_section){
				$action = str_replace('_','-',$gl_action);
				$css_selector_class[]= $gl_tool;
				$css_selector_class[]= $gl_tool_section;
				
				//if (!empty($GLOBALS['gl_module'])){
					
				$action = str_replace('_','-',$GLOBALS['gl_module']->action);
				// excepcions, no puc saber quina accio faig servir al final, 
				// per tant haig d'anar fent excepcions com a action=search, el modul fill d'aquest fa un list_records
				if ($action == 'search') $action = 'list-records';
				
				$css_selector_class[]= $action;			
				$css_selector_id = $gl_tool_section . '-' . $action;
				//}

				if ($this->is_tool_home) {
					$css_selector_class[]= 'tool-home';
				}

			}
			if ($this->page_text_id){
				$css_selector_class[]= 'page-' .$this->page_text_id;
				
				// nomes poso page quan son pagines soles
				if (!$gl_tool_section){
					$css_selector_class[]= 'page';
					$css_selector_id = 'page-' .$this->page_text_id;
				}
				// Aquest es per identificar un page amb tool
				else
					$css_selector_class[]= 'page-tool';

				// I aquests qualsevol page
				if ($this->is_page_home) {
					$css_selector_class[]= 'page-home';
				} else {
					$css_selector_class[]= 'page-inner';
				}
			}
		}
		$css_selector_class = implode(' ', $css_selector_class);
		
		$this->tpl->set_var('css_selector_class',  $css_selector_class);
		$this->tpl->set_var('css_selector_id',  $css_selector_id);
		$this->tpl->set_var('css_selector_home_id',  $css_selector_home_id);
    }
    // Mostrar la pagina un cop acabat
    function show()
    {
        global $gl_message, $gl_content, $gl_tool;
		
		// ES AL CONFIG
		//if (isset($_SESSION['message'])) {
		//	$this->javascript .=$this->show_message($_SESSION['message'],'saved',true,true);
		//	unset($_SESSION['message']);
		//}
		
        $this->message = $gl_message;
        $this->content = $this->html?$gl_content . $this->show_html():$gl_content;
		
		// si està configurat per separar contingut de la page del de la eina
		// millor així__halt_compiler
		if ($GLOBALS['gl_config']['separate_tool_content']){
			//$this->tool_content = $gl_content;
			$this->tool_content = $gl_content;
			$this->content = $this->show_page($gl_content);
		}		
		// a la home mostro primer la pagina i despres el content i a les altres al revès (TODO -> s'ha de poder configurar quin vull primer)
        elseif ($this->is_home && $this->page_id) {
            $this->title = $this->title?$this->title:constant(strtoupper($gl_tool) . '_HOME_TITLE');
            // a la home hi sumo el contingut, així es pot mostrar continut especific del all_page + propietats destacades o altres similars
            $this->content = $this->show_page() . $gl_content;
        }
		elseif($this->page_id){			
			$this->content = $gl_content . $this->show_page();
		}

        if ($this->template) {
            $this->load_template();
            new Blocks($this->tpl);
            $this->assign_variables();
            $html = $this->tpl->process();

            if (show_preview()){
            	$html .= $this->get_preview_html();
            }

            echo $html;

        }else {
            echo $this->content;
        }

    }
    function get_preview_html(){

    	$preview_visible_js = R::cookie('is_letnd_preview_visible', 'false');
    	
    	$html = "
			<aside class='letnd-preview-mode'>
				<img src=\"/admin/themes/inmotools/images/logo_preview.png\" alt=\"Letnd\" width=\"113\" height=\"20\" border=\"0\">
				<span> Mode preview</span>
				<a onclick=\"letnd_preview_close();\"></a>
			</aside>
    	    <style scoped>
    	        html.letnd-preview-initialized:not(.letnd-preview-hidden) {
    	            margin-top: 40px !important;
    	        }
    	        .letnd-preview-mode {
    	            position: fixed;
    	            top: 0;
    	            right: 0;
    	            left: 0;
    	            background-color:#666;
    	            color: #ffffff;
    	            border-bottom: 4px solid #a7bf1a;
    	            border-right: 0;
    	            border-top: 0;
    	            padding: 5px 5px 5px 30px; 
    	            font-size: 15px;
    	            line-height: 26px;
    	            z-index: 100000;
    	            display: grid;
    	            grid-template-columns: 1fr auto auto;
    	            grid-column-gap: 20px;
    	            align-items: center;      
    	        }
    	        .letnd-preview-mode img {
    	        
    	        }
    	        .letnd-preview-mode a {
    	            width: 24px;
    	            height: 24px;
    	            line-height: 24px;
    	            cursor: pointer;
    	            background-color:#000;
    	            text-align:center;
    	            font-size: 13px;
    	        }
    	        .letnd-preview-mode a:hover {
    	            background-color:#797979;
    	        }
    	        html:not(.letnd-preview-hidden) .letnd-preview-mode a:before {
    	            content: 'x';
    	            display: block;
    	        }
    	        html.letnd-preview-hidden .letnd-preview-mode a:before {
    	            content: '+';
    	            display: block;
    	        }
    	        .letnd-preview-hidden .letnd-preview-mode {
    	            left: auto;
    	            width: 28px;
    	            border-left: 4px solid #a7bf1a;
    	            padding: 0;
    	            
    	        }
    	        .letnd-preview-hidden .letnd-preview-mode img,
    	        .letnd-preview-hidden .letnd-preview-mode span {
    	            display: none;
    	        }
    	        @media only screen and (max-width: 700px) {	
	                .letnd-preview-mode img {
	                    display: none;
	                }
				}
			</style>
			<script>
				var is_letnd_preview_visible = '$preview_visible_js';
				$('html').addClass('letnd-preview-initialized');
				if(is_letnd_preview_visible == 'true') letnd_preview_close();
				
				function letnd_preview_close() {
					$('html').toggleClass('letnd-preview-hidden');
					var cookie = $('html').hasClass('letnd-preview-hidden');
					$.cookie('is_letnd_preview_visible',cookie, { path: '/' });
				}
			</script>
    	";


    	return $html;
    }

	// obtindre la pagina manualment
	// el content li assigno jo
	// i el missatge també
	function get(){
		$this->load_template();
		$this->assign_variables();
		return $this->tpl->process();
	}
    function show_html()
    {
        global $gl_language;
        $filename = PATH_TEMPLATES . $gl_language . '/' . $this->html . '.htm';
        $filesize = filesize($filename);
        if ($filesize) {
            $tmp = fread($fp = fopen($filename, 'r'), $filesize);
            fclose($fp);
        }
        return $tmp;
    }
	// obtinc registre de la page si n'hi ha, al contruir la clase
	function get_page_row(){
		$this->page_id = R::id('page_id'); //isset($_GET["page_id"])?$_GET["page_id"]:'';		
		
		// si no hi ha page id o no es home no estic fent servir cap 'page'
		if (!$this->page_id && !$this->is_home && !isset($_GET['page_file_name'])) return; 
		
		global $gl_language;

        $query = "SELECT template, link, page, page_title, page_description, page_keywords, title, body_subtitle, body_description,  all__page.page_id as page_id, page_text_id, parent_id, level, menu, has_content, page_file_name
				FROM all__page_language, all__page
				WHERE all__page_language.page_id = all__page.page_id
				AND status = 'public'
				AND bin <> 1
				AND language = '" . $gl_language . "'";

        if ($this->is_home) { // a la home no hi ha el page_id, per tant busco el link = "/" que vol dir que es home
                $query .= " AND link = '/'";
        }
		// use_friendly_url -> agafo id a partir del nom de l'arxiu i idioma
		elseif (isset($_GET['page_file_name'])){
		
			$page_file_name = R::file_name();
			
            $query .= " AND all__page.page_id = (SELECT page_id FROM all__page_language WHERE all__page_language.page_file_name = '" . $page_file_name . "' AND language='".$gl_language."' LIMIT 1)";
		}
		else {
            $query .= " AND all__page.page_id = " . Db::qstr($this->page_id);
        }
		
        $rs = Db::get_row($query);				
		$base_link = $_SERVER["REQUEST_URI"];
		
        if ($rs) {
			if (!$rs['has_content']) 
				// si m'he equivocat a la taula, i poso que no te content, però tampoc hi ha cap fill, deixo el rs
				$rs=Page::get_first_child($rs['page_id'])?Page::get_first_child($rs['page_id']):$rs;
						
            
            $this->page_id = $rs['page_id'];
            $this->page_text_id = $rs['page_text_id'];
            $this->page_file_name = $rs['page_file_name'];
            $this->page_template = $rs['template'];
            $this->parent_id = $rs['parent_id'];
            $this->level = $rs['level'];
            $this->menu = $rs['menu'];	
			
			// comprovo si l'adreça de la pagina és correcta
			Main::check_redirect_case_seven($this->page_id, $rs['link'], $this->page_file_name, $rs['has_content']);
			
			$this->page_title = $this->page_og_title = $rs['page_title']?$rs['page_title']:$this->page_title;
			$this->page_description = $this->page_og_description = $rs['page_description']?$rs['page_description']:$this->page_description;
			$this->page_keywords = $rs['page_keywords']?$rs['page_keywords']:$this->page_keywords;
			$this->title = $rs['title']?$rs['title']:$rs['page']; // si no hi ha title, el titol es el page ( com era al principi )
			$this->body_subtitle = $rs['body_subtitle'];
			$this->body_description = $rs['body_description'];
			
			if (is_table('all__page_image')) {
				$this->set_vars(UploadFiles::get_record_images($this->page_id, true, 'all','page'));
			}
			
			$this->is_page_home = $base_link=='/' . LANGUAGE . $rs['link'] . '/' .$rs['page_file_name'] . '.html'?true:false;
        }
		// si ja no existeix la pagina, fem un 404
		else{
			// si es user friendly, comprovo si la pagina és a page_old_file_name
			if (isset($_GET['page_file_name'])){
				$query = str_replace('all__page_language.page_file_name =','all__page_language.page_old_file_name =',$query);
				$rs = Db::get_row($query);
				if ($rs){	
					if ($rs['link'] == 'file:' || $rs['link'] == 'link:'){
						$new_url = $rs['page_file_name'];
					}
					else{
						$new_url = str_replace($_GET['page_file_name'].'.html',$rs['page_file_name'].'.html',$base_link);
					}
					Main::redirect($new_url);
				}
				// si no trobem amb old_file_name
				else{
					Main::error_404();
				}
			}
			
			// mostrem 404 si aquell page_id o page_file_name ja no existeix
			// ( el que no cumpleix aquesta condicio nomes es una homepage sense una all__page ( com a tiendascopporo )
			if (isset($_GET['page_file_name']) || $this->page_id){
				Main::error_404();
			}
		}
	}
	static public function get_first_child($page_id){
		global $gl_language;
		
		$query = "SELECT template, link, page, page_title, page_description, page_keywords, title, body_subtitle, body_description, all__page.page_id as page_id, parent_id, level, menu, has_content, page_file_name
				FROM all__page_language, all__page
				WHERE all__page_language.page_id = all__page.page_id
				AND status = 'public'
				AND parent_id = " . Db::qstr($page_id) . "				
				AND bin <> 1
				AND language = '" . $gl_language . "'
				ORDER BY ordre ASC, page_id
				LIMIT 1";
		$rs = Db::get_row($query);	
		if ($rs) {		
			// si m'he equivocat a la taula, i poso que no te content, però tampoc hi ha cap fill, deixo el rs
			if (!$rs['has_content']) 
				return Page::get_first_child($rs['page_id'])?Page::get_first_child($rs['page_id']):$rs;
			else
				return $rs;
		}
		else {
			return false;
		}
	}

    function show_page($tool_content='')
    {        
        if ($this->page_id) {
            if ($this->page_template) {				
                return $this->get_page_content($this->page_template,$tool_content);
            }else {
                return $tool_content; // retorno $tool_content, així posa el contingut de l'eina a content encara que no hi hagi page; ho faig així per quan vull posar el tool_content dins de la page_template
            }
        }else {
            return $tool_content;
        }
	}
	function show_message($message, $code = false, $replace=true, $get = false)
	{
		// Els code són:
		//  save
		//  not_saved
		//  logged      ( moduls usuaris )
		//  not_logged  ( moduls usuaris )
		if (!$code) {
			$code = $GLOBALS['gl_saved']?'saved':'not_saved';
		}
		
		// crido funcio javascript amb el missatge d'error o succes, i la variable succes= true or false
		$js = 'if (top.document.getElementById("message")){
			top.document.getElementById("message_container").style.display="block";
			top.document.getElementById("message").innerHTML = '.json_encode($message).';
		}';
		$js .= $this->get_javascript_show_message($message,$code);
		$js .= '
		else {
			if (typeof top.hidePopWin == "function") top.window.setTimeout("hidePopWin(false);", 1000);
			if (typeof top.showPopWin == "function") top.showPopWin("", 300, 100, null, false, false, '.json_encode($message).');
		}';

		if (!DEBUG && $replace) $js .='window.location.replace("/admin/themes/inmotools/blank.htm");';
		
		if ($get) return $js;
		
		print_javascript($js);
	}
	// crido un objecte jscript al guardar, al fer un login, etc, per poder tindre control en cada web sobre que fa l'aplicació quan es fa servir iframe 
	// 
	/* ej.
		var contact = {
			'show_message': function (tool_section, action, message, code){		
				// codi	
			}
		};
	*/
	function get_javascript_show_message( $message, $code, $vars = [] ) {
		$tool         = $GLOBALS['gl_tool'];
		$tool_section = $GLOBALS['gl_tool_section'];
		$action       = $GLOBALS['gl_action'];
		$message      = json_encode( $message );
		$vars         = $vars ? ', ' . json_encode( $vars ) : '';

		$js = "
			if (typeof top.$tool != 'undefined' && typeof top.$tool.show_message == 'function') {
				top.$tool.show_message('$tool_section', '$action', $message, '$code'$vars);
			}";

		return $js;
	}
    function get_page_content($template,$tool_content='')
    {
        global $gl_caption, $gl_language;
        $tpl = new phemplate(PATH_TEMPLATES_PAGE . 'page/');
        $tpl->set_file($template . '_' . $gl_language . '.tpl');
        $tpl->set_vars($gl_caption);
		
		// poso el tool content tant al page com a plantilla general
        $tpl->set_var('tool_content',$tool_content);
        $tpl->set_var('page_text_id', $this->page_text_id);
		//
		
        return $tpl->process();
    }
    static public function get_page($page_id, $content = false, $get_file_name=false) // lo de content no està fet, es per si necessitessim un content de la pagina
    {
        global $gl_language, $gl_is_home;
        $query = "SELECT all__page.page_id as page_id, link, page, title, page_file_name, has_content
				FROM all__page_language, all__page
				WHERE all__page_language.page_id = all__page.page_id
				AND status = 'public'
				AND bin <> 1
				AND language = '" . $gl_language . "'
				AND (all__page.page_id = '" . $page_id . "' OR all__page.page_text_id = '" . $page_id . "')";
        $results = Db::get_rows($query);
	    if ($results) {
            extract (current($results));
			if ($get_file_name) return Page::get_page_link($page_id, $link, $page_file_name, $has_content, $get_file_name);
            $r['p_link'] = Page::get_page_link($page_id, $link, $page_file_name, $has_content);
            $r['p_item'] = $page;
			$r['p_title'] = $title?$title:$page;
			
			$g_id = R::id('page_id');
			$g_page_file_name = R::text_id('page_file_name');
			
            ($g_id == $page_id || $g_page_file_name === $page_file_name)?$r['p_selected'] = 1:$r['p_selected'] = 0;
            if (($link == '/') && ($gl_is_home)) $r['p_selected'] = 1;
            return $r;
        }else {
            return array();
        }
    }
    // la faig servir al block all_page i a la funció get_page
    static public function get_page_link($page_id, &$link, $page_file_name, $has_content, $get_file_name=false, $language = LANGUAGE)
    {
		// si la pàgina no te contingut, agafo el link i tot del primer fill que es la pàgina que es mostra, així el link es exactamnet el mateix que el fill
		if (!$has_content){
			$rs=Page::get_first_child($page_id);
			if ($rs){
				$link = $rs['link'];
				$page_id = $rs['page_id'];
				$page_file_name = $rs['page_file_name'];
			}
		}
		if ($get_file_name) return  $page_file_name?$page_file_name . '.html':'';
		/*
		BASE_URL_PUBLIC  ---> serveix per fer probes si no podem fer servir el index.php a public, no està fet per fer anar sempre la web així, per exemple no detectaria la homepage
		
		*/
		// JAVASCRIPT
        if (strpos($link, 'javascript:') === 0) {
            $ret = $link;
        } 
		// ENLLAÇ, s'ha de posar al camp link = link: , i agafa l'enllaç del camp page_file_name
		// ARXIU, s'ha de posar al camp link = file: , i agafa l'enllaç del camp page_file_name
        elseif ($link == 'file:' || $link == 'link:') {
            $ret = $page_file_name;
        } 
		// LINK A UN ALTRE MODUL
		elseif ($link)
        {
            if (USE_FRIENDLY_URL){
				$ret = '/' . $language;
				if ($link == '/') // la home page no porta el page_id
				{
					$ret .= '/';
				}
				else{
					$ret .= $link . '/' . $page_file_name . '.html';				
				}
			}
			else{
				$sep = strstr($link, '?')?'&amp;':'?';
				if (BASE_URL_PUBLIC){				
				$ret = '/'.BASE_URL_PUBLIC . '?' . $link . '&language=' . $language;
				}
				else{
				$ret = $link . $sep . 'language=' . $language;
				}
				if ($link != '/') // la home page no porta el page_id
				{
					$ret .= '&amp;page_id=' . $page_id;
				}
			}
        }
		// LINK A LA PAGINA
        else
        {
            if (USE_FRIENDLY_URL){
				$ret = '/' . $language . '/' . $page_file_name . '.html';
			}
			else{
				$ret = '/' . BASE_URL_PUBLIC . '?page_id=' . $page_id . '&amp;language=' . $language;
			}
        }
		// EVITAR DUPLICATS
		// si es la home, trec el link del menu d'idiomes
		if ($GLOBALS['gl_default_language']==$language && $link == '/') $ret = '/';
		
		return $ret;
		
    }

	// AQUESTES FUNCIONS SON PER COMPTABILITAT V2
	// Son les mateixes funcions del phemplate, serà més fàcil cambiar-ho per extends phemplate quan ja no hi hagi la variable global $tpl
	function set_var($name, $value)
    {
        $this->vars[$name] = $value;
    }
    function set_vars($vars, $clear = false)
    {
        if ($clear)
        {
            $this->vars = $vars;
        }
        else
        {
            if (is_array($vars)) $this->vars = array_merge($this->vars, $vars);
        }
    }
	// afegir items al breadcumb
	static public function add_breadcrumb($item,$link=false){
		$GLOBALS['gl_page']->breadcrumb []= array('item'=>$item,'link'=>$link,'selected'=>0);
	}
	// FUNCIÓ PER CONSTRUIR EL LINK SEGONS SIGUI UNA FRIENDLY_URL O NO
	// TOTS ELS LINKS DE TOTS ELS MÒDULS PÚBLICS HAURAN DE PASSAR PER AQUÍ
	//
	// DE MOMENT: 
	//      a news forço V1 per fer-ho més endavant quan canvii tot a V2
	//		està fet a newsletter, list_records per view i show_record_link
	// 		action a friendly no es fa servir ( list_records i show_record )
	// 		Si exclude_params = false no propago parametres
	//      Els keys de  friendly_params no es propaguen, sino que queden a la url integrats (no cal posar-lo a exclude_params, ja queda exclòs )
	// TODO: Links
	//      nom a les categories
	//      important fer-ho al block d'idiomes
	//
	// S'ESTA COMPLICANT MASSA, HAURE DE FER UNA CLASE
	// Si es link a registre, sempre ha d'haber el $id per saber que es un show_record
	static public function get_link_short(&$module, $action=false){
			if (USE_FRIENDLY_URL){
				return Page::get_link($module->tool, $module->tool_section, $action, $id=false ,array(), false, false, false);
			}
			else{
				return '/?tool='.$module->tool.'&tool_section='.$module->tool_section.'&action='.$action.'&language='.LANGUAGE;
			}
	
	}
	static public function get_link($tool, $tool_section, $action=false, $id=false ,$friendly_params=array(), $exclude_params = array(), $file_name = false, $category_file_name = false, $page_file_name = false, $language = LANGUAGE){	
			//$page_file_name = $GLOBALS['gl_page']->page_file_name; // s'ha d'agafar de la bbdd, no sempre es la pagina actual
			$base_tool = ($tool!=DEFAULT_TOOL || $tool_section!=DEFAULT_TOOL_SECTION)?$tool . '/' . $tool_section . '/':'';
			$amp = false;

		// forço a noticies v1 ( es temporal, no tinc temps de fer els redirects )
			//if ($tool == 'news' && $tool_section == 'new' && !$file_name) $file_name=false;
	
				$ret = '/' . $language . '/';

				// .htaccess V1
				if ($file_name===false){
					$id = $id?$id.'/':'';
					$ret .= $base_tool . $id;	
					if ($action) $ret .= $action . '/';
					
								
					if ($friendly_params){
						foreach($friendly_params as $k=>$v){
							if ($v!==false && $v!='') $ret .=  $k . '/' . $v . '/';
							if ($exclude_params!==false) $exclude_params[] = $k;
						}
					}
					
					if ($page_file_name) $ret .= $page_file_name . '.html';				
				}
				// .htaccess V2
				elseif ($page_file_name){
					$ret .= 'p/' . $base_tool . $page_file_name . '/';
					if ($category_file_name) $ret .= $category_file_name . '/';
				}
				elseif ($category_file_name){
					$ret .= 'c/' . $base_tool . $category_file_name . '/';				
				}
				// lang/tool/section/nom-arxiu.htm
				// lang/tool/section/
				// lang/nom-arxiu.htm
				elseif ($file_name!==false){
					$ret .= $base_tool;				
				}
				
				// .htaccess tots els v2
				if($file_name!==false){
					$f_params_friendly = '';
					$f_params_normal = '';
					// més de 2 parametres van a fora   /p1/val/p2/val/?p3=3&p4=4&p5=5
					// més de 1 parametres amb pagina i categoria van a fora   
					//				/p1/val/?p3=3&p4=4&p5=5 hi ha un limit de 10 al htaccess
					// o show_record també va fora
					if ($friendly_params){
						
						// no hi ha action a V2, haig de fer de parametre
						if ($action) {
							$friendly_temp2 = $friendly_params;
							$friendly_temp1 = array('action'=>$action);
							$friendly_params = array_merge($friendly_temp1, $friendly_temp2);
						}
						
						
						$conta = 1;
						if ($category_file_name && $page_file_name) $conta = 2;
						foreach($friendly_params as $k=>$v){
							if ($v!==false && $v!='') {
								if ($conta<=2){
									$f_params_friendly .=  $k . '/' . $v . '/';
								}
								else {
									$amp = $amp?'&':'?';
									$f_params_normal .= $amp . $k . '=' . $v;
								}
								$conta++;
							}
							if ($exclude_params!==false) $exclude_params[] = $k;
						}
						if ($f_params_friendly) $f_params_friendly = 'v/' . $f_params_friendly;
					}					
				
					// show_record
					if ($id){
						$file_name =  $file_name?$file_name:$id;
						$ret .= $f_params_friendly;
						$ret .= $file_name . '.htm';
						$ret .= $f_params_normal;
					}
					else{					
						$ret .= $f_params_friendly . $f_params_normal;
					}
					
					// que faig amb action aquí?					
					//if ($action) $ret .= $action . '/';
				}
				
				// Si exclude_params = false no propago parametres
				// si exclude_params = array() propago parametres
				// sumo els parametres que vull excloure del que s'han d'excloure sempre per defecte
				if ($exclude_params!==false) {
					$exclude_params = array_merge (array('language','tool','tool_section','orderby','page_file_name','filecat_file_name','file_name'),$exclude_params);
					$ret .= get_all_get_params($exclude_params,$amp?'&':'?');
				}
			return $ret;
			
			/*if (USE_FRIENDLY_URL){
				$ret = '/' . LANGUAGE . '/' . $module->tool . '/' . $module->tool_section . '/';
				if ($action) $ret .= $action . '/';
				if ($this->page_file_name) $ret .= $this->page_file_name . '.html';
				
				// els params ho exclou tambés dels parametres ( no se com s'ha de fer encara ), nomès es a list_records per view
				if ($params) {
					$params = array_merge (array('language','tool','tool_section','orderby','page_file_name'),$params);
					$ret .= get_all_get_params($params,'?');
				}
			}
			else{
				// s'ha de ferrrrrrrrrrrr
			}
			return $ret;*/
	}
	
	// TITOL de la pagina al cos
	static public function set_title($text1){
		$GLOBALS['gl_page']->title = $text1;
	}
	static public function get_title(){
		return $GLOBALS['gl_page']->title;
	}
	static public function set_body_subtitle($text1){
		$GLOBALS['gl_page']->body_subtitle = $text1;
	}
	static public function get_body_subtitle(){
		return $GLOBALS['gl_page']->body_subtitle;
	}
	static public function set_body_description($text1){
		$GLOBALS['gl_page']->body_description = $text1;
	}
	static public function get_body_description(){
		return $GLOBALS['gl_page']->body_description;
	}
	
	static public function get_tool_content(){
		return $GLOBALS['gl_page']->tool_content;
	}
	
	// META TAG title
	// Sempre ha d'haver algun page_title, si els dos textes son '', 
	// llavors agafa el general que s'assigna al crear la classe
	static public function set_page_title($text1,$text2=''){
		if ($text1 || $text2){
			$GLOBALS['gl_page']->page_title = $text1?$text1:$text2;
			$GLOBALS['gl_page']->page_og_title = $text2; // utilitzo el text posat a la fitxa en concret, no el de SEO
		}
	}
	
	// META TAG description
	// El text2 sol ser automàtic de la descripció de la familia o sector, etc.., per tant s'ha de retallar
	static public function set_page_description($text1,$text2=''){
		if ($text1 || $text2){
			$GLOBALS['gl_page']->page_description = $text1?$text1:add_dots_meta_description($text2);
			$GLOBALS['gl_page']->page_og_description = add_dots_meta_description($text2, 250);// utilitzo el text posat a la fitxa en concret, no el de SEO
		}
	}

	// META TAG image per facebook
	static public function set_page_og(&$rs){
		if (isset($rs['image_src_details'])){
			$GLOBALS['gl_page']->page_og['image'] = $rs['image_src_details'];
			$GLOBALS['gl_page']->page_og['type'] = 'article';
		}
	}
	
	// META TAG keywords
	static public function set_page_keywords($text1,$text2=''){
		if ($text1 || $text2){
			$GLOBALS['gl_page']->page_keywords = $text1?$text1:$text2;
		}
	}
}


/**
 * FUNCIÓ Definitiva per crear un link
 *
 *
 *
 * @param array $parameters Array amb diferents paràmetres per nomes posar els que calguin
 *
 *
 * @param integer $parameters ['id'] Id de l'element
 * @param string $parameters ['file_name'] = '' Nom de la pagina
 *                False fa V1
 *                Cadena buida V2 ( funciona amb mes de 2 friendlies )
 * @param string $parameters ['tool'] = ''
 *        Tool
 * @param string $parameters ['tool_section'] = ''
 *        Section
 * @param string $parameters ['action'] = false
 *        Acció
 * @param string $parameters ['category'] = ''
 *        nom del que fa de categoria 'kind', 'tipus','category' el que sigui
 * @param integer $parameters ['category_id'] = ''
 *        Id de la categoria
 * @param array $parameters ['link_vars'] = array()
 *        Array de variables possibles al link de la pagina (si 'detect_page')
 *        nomes es posen si es troba una pagina amb elles
 *        Si vull que a més es posin s'han de posar també a friendly_params
 * @param boolean $parameters ['detect_page'] = false
 *        Si ha de detectar la pagina automàticament segons els pages
 *        Si hi ha page_id o page_file_name NO autodetecta
 * @param integer $parameters ['page_id'] = false
 *        Id de la pagina
 * @param string $parameters ['page_file_name'] = false
 *        Nom de la pagina
 * @param array $parameters ['friendly_params'] = array()
 *        Paràmetres que poden anar: /param/val/param/val *
 * @param array|boolean $parameters ['exclude_params'] = false
 *        Si exclude_params = false no propago parametres
 *      Si es array excloc els parametres de l'array de propagarse
 *
 *
 * Falta fer el category_file_name
 * Falta fer per page_id o page
 *
 * @return string
 */

function get_link ($parameters)
{
    static $cache;
	$default = array(
					'id' => false,
					'file_name' => '', 
					'tool' => '', 
					'tool_section' => '', 
					'action' => false, 
					'category' => '',
					'category_id' => '',
					'link_vars' => array(),
					'detect_page' => false,
					'page_id' => false,
					'page_file_name' => false,
					'friendly_params' => array(),
					'exclude_params' => false,
			);
	
	$parameters = array_merge($default, $parameters);	
	$link = '';
	
	extract($parameters);
			
	// poso category el primer dels friendly
	if ($category){ 
		// array_add($friendly_params, array ($category=>$category_id));
	}
	

	// si em dona l'id busco el page_file_name
	if ( $page_id && ! $page_file_name ) {

		$query = "
				SELECT  page_file_name 
					FROM all__page_language 
					WHERE language='" . LANGUAGE . "'
					AND page_id = '" . $page_id . "'";

		$page_file_name = ! empty( $cache[ 'page_file_name-' . $page_id ] ) ? $cache[ 'page_file_name-' . $page_id ] : Db::get_first( $query );

		if ( $page_file_name ) {
			$cache[ 'page_file_name-' . $page_id ] = $page_file_name;
		}


	}	

	// Si tinc file_name però no page_id, busco el page_id per poder posar les variables de més
	// TODO No està testejat amb page_file_name, de moment no es crida d'enlloc
	if ($page_file_name && !$page_id){

		$query = "
				SELECT  page_id
					FROM all__page_language
					WHERE language='" . LANGUAGE . "'
					AND page_file_name = '" . $page_file_name . "'";

		$page_id = ! empty( $cache[ 'page_id-' . $page_file_name ] ) ? $cache[ 'page_id-' . $page_file_name ] : Db::get_first( $query );

		if ( $page_id ) {
			$cache[ 'page_id-' . $page_file_name ] = $page_id;
		}

	}

	// Que faig, poso les variables que trobo en el 'link' de la BBDD o les variables de 'link_vars'
	//
	// si he posat un page_id vull que m'ho relacioni amb el page
	// per tant necessito saber les variables que hi ha en aquell page
	// els 2 primers parametres sempre son tool i tool_section ej: /inmo/property/place/2
	if ( $page_id ) {


		$query = "
				SELECT  link
					FROM all__page
					WHERE page_id = '" . $page_id . "'";

		$link_field = ! empty( $cache[ 'link_field-' . $page_id ] ) ? $cache[ 'link_field-' . $page_id ] : Db::get_first( $query );

		if ( $link_field ) {
			$cache[ 'link_field-' . $page_id ] = $link_field;
		}

		$link_field = explode( '/', $link_field );
		Debug::p($link_field  );
		for ( $i = 3; $i < count( $link_field ); $i = $i + 2 ) {
			$friendly_params[ $link_field[ $i ] ] = isset( $link_field[ $i + 1 ] ) ? $link_field[ $i + 1 ] : '';
		}

		// Debug::p( $friendly_params );
	}

	if ($detect_page && !$page_file_name){
			
		
		// 1- comprovo si existeix /news/new/category_id/1
		// 1b - també pot ser /inmo/property/tipus/temp , que no es la categoria
		
		$link_vars [$category] = $category_id;

		foreach ($link_vars as $k=>$v){
			if ($v) {
//				Debug::p( $tool_section );
//				Debug::p( $tool );
//				Debug::p( $v,'v' );
				$link_query = USE_FRIENDLY_URL ?
						'/' . $tool
						. '/' . $tool_section
						. '/' . $k
						. '/' . $v :
						'tool=' . $tool . '&tool_section=' . $tool_section . '&' . $k . '=' . $v;

				$query = "
				SELECT  page_file_name 
					FROM all__page 
					INNER JOIN all__page_language 
					USING(page_id) 
					WHERE language='" . LANGUAGE . "' 
					AND link = '" . $link_query . "'
					AND bin = 0
					AND status = 'public'";

				$page_file_name = !empty($cache[$link_query]) ? $cache[$link_query] : (Db::get_first($query));


				if ($page_file_name) {

					array_add($friendly_params, array($k => $v));
					$cache[$link_query] = $page_file_name;
					break;
				}
			}
			
		}
		
		// 2 - comprovo si existeix /news/new		
		if (!$page_file_name) {
			$link_query = USE_FRIENDLY_URL?
								'/' . $tool . '/' . $tool_section:
								'tool=' . $tool . '&tool_section=' . $tool_section;

			$query = "
				SELECT  page_file_name 
					FROM all__page 
					INNER JOIN all__page_language 
					USING(page_id) 
					WHERE language='" . LANGUAGE . "' 
					AND link = '" . $link_query . "'
					AND bin = 0
					AND status = 'public'";
			
			$page_file_name = !empty($cache[$link_query])?$cache[$link_query]:Db::get_first($query);
			
			if ($page_file_name) {
				$cache[$link_query] = $page_file_name;
			}
		}
	}	
	
	if (USE_FRIENDLY_URL){
		
		// forço V1 per quan nomes hi ha page i category perque es com van les pages
		// TODO-i revisar si !$page_file_name es correcte, ho he posat pel block de family
		// TODO-i Poder s'hauria de fer al reves, autodetect page no hauria d'anar a l'enllaç, i detectar la pagina al carregar l'eina
		if (!$action && !$id && !$friendly_params && !$file_name){
			$file_name = false;

			// poso category el primer dels friendly només al forçar a v1, així surt a la url com a paràmetres normals
			if ($category) {
				array_add($friendly_params, array ($category=>$category_id));
			}
		}
		// Debug::p( $page_file_name );

		$link = Page::get_link(
				$tool, $tool_section,
				$action,$id,$friendly_params,$exclude_params,$file_name,$category_id,$page_file_name);
			
	}	
		
	else{	
		$link .= '/?language=' . LANGUAGE;
		if ($tool) $link .= '&amp;tool=' .$tool;
		if ($tool_section) $link .= '&amp;tool_section=' .$tool_section;
		if ($action) $link .= '&amp;action=' .$action;
		if ($id && !$action) $link .= '&amp;action=show_record';
		if ($id) $link .= '&amp;'.$tool_section.'_id=' .$id;
		if ($page_id) $link .= '&amp;page_id=' .$page_id;
		
		// aqui inclou category
		foreach ($friendly_params as $k=>$v){
			if ($v) {
				$link .= '&amp;'.$k.'=' .$v;
			}
		}
		//if ($link) $link = '/?' . substr ($link, 5);		
	}
	
	return $link;
}
?>