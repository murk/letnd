<table border="0" cellspacing="0" cellpadding="0">
  <tr>
	<?if ($page_results):?>
    <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td><strong><?=$c_page_words_title?>:</strong></td>
      </tr>
    </table>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td height="3"></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
        <?foreach($loop as $l): extract ($l)?>
        <tr>
          <td height="25"><?=$language?></td>
          <td align="right"><?=$word_count?>
            &nbsp;</td>
          <td>x
            <?=$language_price?>
            <?=CURRENCY_NAME?></td>
          <td>=</td>
          <td align="right"><?=$price?>
            <?=CURRENCY_NAME?></td>
        </tr>
        <?endforeach //$loop?>
        <tr>
          <td height="1" class="lineaSuma">&nbsp;</td>
          <td class="lineaSuma">&nbsp;</td>
          <td class="lineaSuma">&nbsp;</td>
          <td class="lineaSuma">&nbsp;</td>
          <td class="lineaSuma">&nbsp;</td>
        </tr>
        <tr>
          <td height="25"><strong><?=$c_total?>:</strong></td>
          <td align="right"><?=$word_count_total?>
            &nbsp;</td>
          <td>&nbsp;</td>
          <td>=</td>
          <td align="right"><?=$price_total?>
            <?=CURRENCY_NAME?></td>
        </tr>
      </table></td>
    <td width="30" valign="top">&nbsp;</td>
	<?endif //$page_results?>
	<?if ($loop2):?>	
    <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td><strong><?=$c_all_page_words_title?>:</strong></td>
      </tr>
    </table>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td height="3"></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
        <?foreach($loop2 as $l): extract ($l)?>
        <tr>
          <td height="25"><?=$language?></td>
          <td align="right"><?=$word_count?>
            &nbsp;</td>
          <td>x
            <?=$language_price?> 
            <?=CURRENCY_NAME?> </td>
          <td>=</td>
          <td align="right"><?=$price?>
            <?=CURRENCY_NAME?></td>
        </tr>
        <?endforeach //$loop2?>
        <tr>
          <td height="1" class="lineaSuma">&nbsp;</td>
          <td class="lineaSuma">&nbsp;</td>
          <td class="lineaSuma">&nbsp;</td>
          <td class="lineaSuma">&nbsp;</td>
          <td class="lineaSuma">&nbsp;</td>
        </tr>
        <tr>
          <td height="25"><strong><?=$c_total?>:</strong></td>
          <td align="right"><?=$word_count_all_pages?>
            &nbsp;</td>
          <td>&nbsp;</td>
          <td>=</td>
          <td align="right"><?=$price_all_pages?> <?=CURRENCY_NAME?></td>
        </tr>
      </table></td><?endif //$loop2?>
  </tr>
</table>
