<?php
	// Aquest arxiu es genera automaticament
	// Qualsevol canvi que es faci es podrà sobrescriure involuntariament desde l'eina de gestió
	// Achtung!
global $gl_language;
$gl_file_protected = true;
	$gl_db_classes_fields_included = array (
  'agenda_id' => '1',
  'url1' => '1',
  'url2' => '1',
  'videoframe'=>'1',
  'status' => '1',
  'in_home'=>'1',  
  'entered' => '1',
  'agenda' => '1',
  'subtitle' => '1',
  'content' => '1',
  'url1_name'=>'1',
  'url2_name'=>'1',
  'file'=>'1',
);
	$gl_db_classes_language_fields = array (
  0 => 'agenda',
  1 => 'subtitle',
  2 => 'content_list',
  3 => 'content',
  4 => 'url1_name',
  5 => 'url2_name',
  6 => 'date_text',
);
	$gl_db_classes_fields = array (
  'agenda_id' =>
  array (
    'type' => 'hidden',
    'enabled' => '1',
    'form_admin' => 'text',
    'form_public' => 'text',
    'list_admin' => 'text',
    'list_public' => 'text',
    'order' => '0',
    'default_value' => '',
    'override_save_value' => '',
    'class' => '',
    'javascript' => '',
    'text_size' => '',
    'text_maxlength' => '',
    'textarea_cols' => '',
    'textarea_rows' => '',
    'select_caption' => '',
    'select_size' => '',
    'select_table' => '',
    'select_fields' => '',
    'select_condition' => '',
  ),
  'ordre' =>
  array (
    'type' => 'int',
    'enabled' => '1',
    'form_admin' => 'input',
    'form_public' => '0',
    'list_admin' => 'input',
    'list_public' => '0',
    'order' => '0',
    'default_value' => '',
    'override_save_value' => '',
    'class' => '',
    'javascript' => '',
    'text_size' => '2',
    'text_maxlength' => '11',
    'textarea_cols' => '',
    'textarea_rows' => '',
    'select_caption' => '',
    'select_size' => '',
    'select_table' => '',
    'select_fields' => '',
    'select_condition' => '',
  ),
  'category_id' =>
  array (
    'type' => 'select',
    'enabled' => '1',
    'form_admin' => 'input',
    'form_public' => 'text',
    'list_admin' => 'text',
    'list_public' => 'text',
    'order' => '5',
    'default_value' => '',
    'override_save_value' => '',
    'class' => '',
    'javascript' => '',
    'text_size' => '',
    'text_maxlength' => '',
    'textarea_cols' => '',
    'textarea_rows' => '',
    'select_caption' => '',
    'select_size' => '',
    'select_table' => 'llemena__agendacategory,llemena__agendacategory_language',
    'select_fields' => 'llemena__agendacategory.agendacategory_id,agendacategory',
    'select_condition' => 'llemena__agendacategory.agendacategory_id=llemena__agendacategory_language.agendacategory_id AND language=\''.$gl_language.'\' ORDER BY ordre ASC, agendacategory ASC',
  ),
  'start_date' =>
  array (
    'type' => 'date',
    'enabled' => '1',
    'form_admin' => 'input',
    'form_public' => 'text',
    'list_admin' => 'text',
    'list_public' => 'text',
    'order' => '7',
    'default_value' => '',
    'override_save_value' => '',
    'class' => '',
    'javascript' => '',
    'text_size' => '',
    'text_maxlength' => '-1',
    'textarea_cols' => '',
    'textarea_rows' => '',
    'select_caption' => '',
    'select_size' => '',
    'select_table' => '',
    'select_fields' => '',
    'select_condition' => '',
    'required' => '1',
  ),
  'end_date' =>
  array (
    'type' => 'date',
    'enabled' => '1',
    'form_admin' => 'input',
    'form_public' => 'text',
    'list_admin' => 'text',
    'list_public' => 'text',
    'order' => '7',
    'default_value' => '',
    'override_save_value' => '',
    'class' => '',
    'javascript' => '',
    'text_size' => '',
    'text_maxlength' => '-1',
    'textarea_cols' => '',
    'textarea_rows' => '',
    'select_caption' => '',
    'select_size' => '',
    'select_table' => '',
    'select_fields' => '',
    'select_condition' => '',
  ),
  'date_text' =>
  array (
    'type' => 'textarea',
    'enabled' => '1',
    'form_admin' => 'input',
    'form_public' => 'text',
    'list_admin' => '0',
    'list_public' => 'text',
    'order' => '2',
    'required' => '',
    'default_value' => '',
    'override_save_value' => '',
    'class' => 'wide',
    'javascript' => '',
    'text_size' => '',
    'text_maxlength' => '255',
    'textarea_cols' => '',
    'textarea_rows' => '3',
    'select_caption' => '',
    'select_size' => '',
    'select_table' => '',
    'select_fields' => '',
    'select_condition' => '',
  ),
  'status' =>
  array (
    'type' => 'select',
    'enabled' => '1',
    'form_admin' => 'input',
    'form_public' => 'text',
    'list_admin' => 'input',
    'list_public' => 'text',
    'order' => '0',
    'default_value' => '',
    'override_save_value' => '',
    'class' => '',
    'javascript' => '',
    'text_size' => '',
    'text_maxlength' => '',
    'textarea_cols' => '',
    'textarea_rows' => '',
    'select_caption' => '',
    'select_size' => '',
    'select_table' => '',
    'select_fields' => 'prepare,review,public,archived',
    'select_condition' => '',
  ),
  'in_home'=>
   array (
   'type' => 'checkbox',
    'enabled' => '1',
    'form_admin' => 'input',
    'form_public' => 'text',
    'list_admin' => '0',
    'list_public' => 'text',
    'order' => '12',
    'default_value' => '1',
    'override_save_value' => '',
    'class' => '',
    'javascript' => '',
    'text_size' => '',
    'text_maxlength' => '1',
    'textarea_cols' => '',
    'textarea_rows' => '',
    'select_caption' => '',
    'select_size' => '',
    'select_table' => '',
    'select_fields' => '',
    'select_condition' => '',
  ),
  'agenda' =>
  array (
    'type' => 'text',
    'enabled' => '1',
    'form_admin' => 'input',
    'form_public' => 'text',
    'list_admin' => 'text',
    'list_public' => 'text',
    'order' => '2',
    'required' => '',
    'default_value' => '',
    'override_save_value' => '',
    'class' => 'wide',
    'javascript' => '',
    'text_size' => '',
    'text_maxlength' => '255',
    'textarea_cols' => '',
    'textarea_rows' => '',
    'select_caption' => '',
    'select_size' => '',
    'select_table' => '',
    'select_fields' => '',
    'select_condition' => '',
    'required' => '1',
  ),
  'subtitle' =>
  array (
    'type' => 'text',
    'enabled' => '1',
    'form_admin' => 'input',
    'form_public' => 'text',
    'list_admin' => 'no',
    'list_public' => 'text',
    'order' => '3',
    'default_value' => '',
    'override_save_value' => '',
    'class' => 'wide',
    'javascript' => '',
    'text_size' => '',
    'text_maxlength' => '255',
    'textarea_cols' => '',
    'textarea_rows' => '',
    'select_caption' => '',
    'select_size' => '',
    'select_table' => '',
    'select_fields' => '',
    'select_condition' => '',
  ),
   'content_list' =>
  array (
    'type' => 'html',
    'enabled' => '1',
    'form_admin' => 'input',
    'form_public' => 'text',
    'list_admin' => '0',
    'list_public' => 'text',
    'order' => '4',
    'default_value' => '',
    'override_save_value' => '',
    'class' => '',
    'javascript' => '',
    'text_size' => '',
    'text_maxlength' => '',
    'textarea_cols' => '',
    'textarea_rows' => '20',
    'select_caption' => '',
    'select_size' => '',
    'select_table' => '',
    'select_fields' => '',
    'select_condition' => '',
  ),
  'content' =>
  array (
    'type' => 'html',
    'enabled' => '1',
    'form_admin' => 'input',
    'form_public' => 'text',
    'list_admin' => 'no',
    'list_public' => 'text',
    'order' => '4',
    'default_value' => '',
    'override_save_value' => '',
    'class' => '',
    'javascript' => '',
    'text_size' => '',
    'text_maxlength' => '',
    'textarea_cols' => '',
    'textarea_rows' => '',
    'select_caption' => '',
    'select_size' => '',
    'select_table' => '',
    'select_fields' => '',
    'select_condition' => '',
  ),
  'url1_name' =>
  array (
    'type' => 'text',
    'enabled' => '1',
    'form_admin' => 'input',
    'form_public' => 'text',
    'list_admin' => '0',
    'list_public' => 'text',
    'order' => '2',
    'required' => '',
    'default_value' => '',
    'override_save_value' => '',
    'class' => 'wide',
    'javascript' => '',
    'text_size' => '',
    'text_maxlength' => '100',
    'textarea_cols' => '',
    'textarea_rows' => '',
    'select_caption' => '',
    'select_size' => '',
    'select_table' => '',
    'select_fields' => '',
    'select_condition' => '',
  ),
  'url1' =>
  array (
    'type' => 'text',
    'enabled' => '1',
    'form_admin' => 'input',
    'form_public' => 'text',
    'list_admin' => '0',
    'list_public' => 'text',
    'order' => '2',
    'required' => '',
    'default_value' => '',
    'override_save_value' => '',
    'class' => 'wide',
    'javascript' => '',
    'text_size' => '',
    'text_maxlength' => '250',
    'textarea_cols' => '',
    'textarea_rows' => '',
    'select_caption' => '',
    'select_size' => '',
    'select_table' => '',
    'select_fields' => '',
    'select_condition' => '',
  ),
  'url2_name' =>
  array (
    'type' => 'text',
    'enabled' => '1',
    'form_admin' => 'input',
    'form_public' => 'text',
    'list_admin' => '0',
    'list_public' => 'text',
    'order' => '2',
    'required' => '',
    'default_value' => '',
    'override_save_value' => '',
    'class' => 'wide',
    'javascript' => '',
    'text_size' => '',
    'text_maxlength' => '100',
    'textarea_cols' => '',
    'textarea_rows' => '',
    'select_caption' => '',
    'select_size' => '',
    'select_table' => '',
    'select_fields' => '',
    'select_condition' => '',
  ),
    'url2' =>
  array (
    'type' => 'text',
    'enabled' => '1',
    'form_admin' => 'input',
    'form_public' => 'text',
    'list_admin' => '0',
    'list_public' => 'text',
    'order' => '2',
    'required' => '',
    'default_value' => '',
    'override_save_value' => '',
    'class' => 'wide',
    'javascript' => '',
    'text_size' => '',
    'text_maxlength' => '250',
    'textarea_cols' => '',
    'textarea_rows' => '',
    'select_caption' => '',
    'select_size' => '',
    'select_table' => '',
    'select_fields' => '',
    'select_condition' => '',
  ),
   'videoframe' =>
  array (
    'type' => 'videoframe',
    'enabled' => '1',
    'form_admin' => 'input',
    'form_public' => 'text',
    'list_admin' => 'no',
    'list_public' => 'text',
    'order' => '2',
    'required' => '',
    'default_value' => '',
    'override_save_value' => '',
    'class' => 'wide',
    'javascript' => '',
    'text_size' => '',
    'text_maxlength' => '',
    'textarea_cols' => '',
    'textarea_rows' => '6',
    'select_caption' => '',
    'select_size' => '',
    'select_table' => '',
    'select_fields' => '',
    'select_condition' => '',
  ),  
  'file' =>
  array (
    'type' => 'file',
    'enabled' => '1',
    'form_admin' => 'input',
    'form_public' => '0',
    'list_admin' => '0',
    'list_public' => '0',
    'order' => '0',
    'default_value' => '',
    'override_save_value' => '',
    'class' => '',
    'javascript' => '',
    'text_size' => '',
    'text_maxlength' => '5',
    'textarea_cols' => '',
    'textarea_rows' => '',
    'select_caption' => '',
    'select_size' => '',
    'select_table' => 'llemena__agenda_file',
    'select_fields' => 'file_id, name',
    'select_condition' => '',
    'accept' => 'pdf|doc',
  ),
);
	$gl_db_classes_list = array (
  'cols' => '1',
);
	$gl_db_classes_form = array (
  'cols' => '1',
  'images_title' => 'agenda',
);
$gl_db_classes_uploads = array (
	'file' => '',
);
	?>