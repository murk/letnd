<?php
set_tool('inmo', 'property', 'inmo__property', 'property_id');
set_config('inmo/inmo_property_config.php');

include_once('inmo_common.php');

function save_rows()
{
    // el camp showwindow_id es per estar preparat per poder fer categories d'aparadors
    global $gl_message, $gl_messages_tv;
    // query per borrar noms els registres d'aquesta paginaci, abans d'insertar els seleccionats
    $query_delete = "DELETE FROM inmo__property_to_tv
							WHERE property_id
							IN (" . implode(',', $_POST["property_id"]) . ")";
    $query = "INSERT INTO inmo__property_to_tv (
					`property_id` ,
					`tv_id`
					)
					VALUES ('" .
    				implode("', '1'),('", $_POST["selected"]) . "
					','1');";
    Db::execute($query_delete);
    Db::execute($query);
    $gl_message = $gl_messages_tv['list_saved'];
}

function list_records()
{
    include (DOCUMENT_ROOT . 'admin/modules/inmo/inmo_property_functions.php');
	include (DOCUMENT_ROOT . 'admin/modules/inmo/inmo_search_functions.php');
    set_fields_to_text();
    merge_caption('tv');

    global $gl_action, $gl_news, $tpl, $gl_process, $gl_content;
    switch ($gl_process) {
	    case 'get_flash':
	    case 'get_flash_data':

		    set_field( 'description', 'type', 'text' );
		    $listing            = new ListRecords();
		    $listing->condition = 'property_id IN (' . implode( ',', get_tv_ids() ) . ')';
		    list_records_property( $listing );;
		    // escriure arxiu de text amb totes les variables pel flash
		    $json              = new Services_JSON();
		    $tpl_flash['vars'] = $tpl->vars;

	    // Limito imatges
		    if ( INMO_TV_NUM_IMAGES ) {

			    foreach ( $tpl->loops['loop'] as &$l ) {
				    $l['images'] = array_slice( $l['images'], 0, INMO_TV_NUM_IMAGES );
			    }

		    }

		    $tpl_flash['loop'] = $tpl->loops['loop'];
		    //array_walk ($tpl_flash['vars'], 'utf8_encode_array');
		    //array_walk ($tpl_flash['loop'], 'utf8_encode_array');
		    $val        = print_r( $tpl_flash, true );
		    $val        = $json->encode( $tpl_flash );
		    $val        = urlencode( ( $json->encode( $tpl_flash ) ) );
		    $flash_data = 'tpl=' . $val;
		    if ( $gl_process == 'get_flash' ) {
			    zip_flash( $flash_data );
		    } else {
			    echo( $flash_data );
			    die();
		    }

		    break;

	    case 'get_property':
		    global $gl_upload;
		    $images = $gl_upload['image']->get_record_images_i( $_GET['property_id'] );


		    if ( INMO_TV_NUM_IMAGES ) {
			    $images = array_slice( $images, 0, INMO_TV_NUM_IMAGES );
		    }


		    zip_property( $images );
		    break;

	    case 'tv_download':

		    $gl_news = '<b><a target="_blank" href="/admin/' . get_all_get_params( array( 'process' ), '?', '&' ) . 'process=show_flash">clica aquí per veure el flash online</a></b><br><br>
                    <b><a target="save_frame" href="/admin/' . get_all_get_params( array( 'process' ), '?', '&' ) . 'process=get_flash">clica aquí</a></b> per descarregar l\'arxiu principal';

		    set_field( 'description', 'list_admin', 'no' );
		    set_field( 'land', 'list_admin', 'no' );
		    set_field( 'floor_space', 'list_admin', 'no' );
		    set_field( 'entered', 'list_admin', 'no' );
		    set_field( 'download', 'list_admin', 'out' );
		    set_field( 'wc', 'list_admin', 'no' );
		    set_field( 'bathroom', 'list_admin', 'no' );
		    set_field( 'courtyard', 'list_admin', 'no' );
		    set_field( 'garden', 'list_admin', 'no' );

		    $listing                       = new ListRecords();
		    $listing->use_default_template = true;
		    $listing->condition            = 'property_id IN (' . implode( ',', get_tv_ids() ) . ')';
		    $listing->set_options( 0, 0, 0, 0, 0, 0, 0, 0, 1 );
		    list_records_property( $listing );
		    Debug::add( 'Tpl', $tpl );
		    break;

	    case 'show_flash':
		    $flash_file = file_exists( CLIENT_PATH . 'templates/inmo/tv_16-9.swf' ) ? '/' . CLIENT_DIR . '/templates/inmo/tv_16-9.swf' : '/admin/modules/inmo/tv_16-9.swf';

		    echo( '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
			<html xmlns="http://www.w3.org/1999/xhtml">
			<head>
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			<title>' . INMO_MENU_TV . '</title><style>
			html, body {
			height: 100%;
			margin:0px;
			}
			</style>
			</head>
			<body>
			<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,28,0" width="100%" height="100%">
			  <param name="swfversion" value="9,0,28,0" />
			  <param name="movie" value="' . $flash_file . '" />
			  <param name="quality" value="best" />
			  <param name="allowFullScreen" value="true" />
		 	  <param name="FlashVars" value="online=true" />
			  <embed src="' . $flash_file . '" width="100%" height="100%" quality="best" pluginspage="http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash" type="application/x-shockwave-flash" allowfullscreen="true" flashvars="online=true"></embed>
			</body>
			</html>' );
		    html_end();
		    break;

	    default:
		    $listing = new ListRecords;
		    if ( $gl_process == 'tv_selected' ) {
			    $listing->condition = 'property_id IN (' . implode( ',', get_tv_ids() ) . ')';
		    }
		    $listing->selected_ids = get_tv_ids();
		    $listing->set_options( 1, 1, 1, 1, 0, 0, 0, 0, 1 );
		    list_records_property( $listing );
    } // switch
}
function get_download_links($property_id)
{
    return '<b><a target="save_frame" href="/admin/' . get_all_get_params(array('process'),'?','&') . 'process=get_property&amp;property_id=' . $property_id . '">descarregar arxius</a></b>';
}

function zip_flash($flash_data)
{
	global $gl_caption;
    include(DOCUMENT_ROOT . '/common/includes/zip/zipfile.php');
    $zipfile = new zipfile();

    $zipfile->addFile($flash_data, "flash_data.txt");

	$flash_file = file_exists(CLIENT_PATH . 'templates/inmo/tv_16-9.swf')?CLIENT_PATH . 'templates/inmo/tv_16-9.swf':'modules/inmo/tv_16-9.swf';

    $zipfile->addFile(implode("", file($flash_file)), "tv_16-9.swf");
	$zipfile->output(false,$gl_caption['c_main_files']);

}
function zip_property($images){
	global $gl_caption;
    include(DOCUMENT_ROOT . '/common/includes/zip/zipfile.php');
	// ara esta a 128M per defecte // ini_set("memory_limit","50M"); // aumento memoria, haig de controlar l'error quan sobrepassa
    $zipfile = new zipfile();
	foreach ($images['images'] as $image){
		$zipfile->addFile(implode("", file(DOCUMENT_ROOT . $image['image_src'])), $image['image_name']);
	}
	$zipfile->output(false, $gl_caption['c_property_file'].$_GET['property_id']);
}

function get_tv_ids()
{

    $query = "SELECT property_id FROM inmo__property_to_tv";
    $results = Db::get_rows($query);
    $ret = array();
    foreach ($results as $rs)
    {
        $ret[] = $rs['property_id'];
    }
	if (!$ret) $ret[] = 0;
    return $ret;
}