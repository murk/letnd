<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<?=load_admin_css()?>
<link href="/common/jscripts/jquery-ui-1.8.20/css/ui-lightness/jquery-ui-1.8.20.custom.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/common/jscripts/jquery/jquery-1.7.2.min.js"></script>
<? if (!DEBUG):?>
<script type="text/javascript" src="/common/jscripts/jquery.onerror.js?v=<?=$version?>"></script>
<? endif //DEBUG?>
<script type="text/javascript" src="/common/jscripts/jquery-ui-1.8.20/js/jquery-ui-1.8.20.custom.min.js?v=<?=$version?>"></script>
<script type="text/javascript" src="/common/jscripts/jquery-ui-1.8.20/languages/jquery.ui.datepicker-<?=$language_code?>.js?v=<?=$version?>"></script>
<script type="text/javascript" src="/common/jscripts/jquery.dump.js?v=<?=$version?>"></script>
<script type="text/javascript" src="/common/jscripts/jquery.mousewheel.js?v=<?=$version?>"></script>
<script type="text/javascript" src="/common/jscripts/jquery.timepicker.js?v=<?=$version?>"></script>
<script type="text/javascript" src="/common/jscripts/jquery.cookie.js?v=<?=$version?>"></script>
<script type="text/javascript" src="/common/jscripts/jquery.blockUI.js?v=<?=$version?>"></script>
<script type="text/javascript" src="/common/jscripts/jquery.dropshadow.js?v=<?=$version?>"></script>
<script type="text/javascript" src="/admin/jscripts/main.js?v=<?=$version?>"></script>
<script type="text/javascript" src="/admin/languages/<?=$language?>.js?v=<?=$version?>"></script>


<script language="JavaScript" src="/admin/modules/inmo/jscripts/custumer.js?v=<?=$version?>"></script>
<script language="JavaScript" src="/admin/jscripts/assign.js?v=<?=$version?>"></script>
<SCRIPT LANGUAGE="JavaScript" TYPE="text/javascript">
<!--
var addreses_selected = new Array();
//-->
</SCRIPT>
</head>
<body class="select-frame">
<div class="search">
	<form name="search" method="get" action="/admin/" target="listAddresses">
	<table border="0" cellpadding="0" cellspacing="0" class="search_select_frame">  
		<tr valign="middle">
				<td><?=$c_by_words?></td>
				<td>
					<input name="action" type="hidden" value="list_records_search_words_custumer">
					<input type="hidden" name="property_id" value="<?=$property_id?>">
					<input type="hidden" name="menu_id" value="<?=$menu_id?>">
					<?if($is_booking):?>
						<input type="hidden" name="book_id" value="<?=$book_id?>">
						<input type="hidden" name="main_custumer" value="<?=$main_custumer?>">
						<input type="hidden" name="exclude_custumer_ids" value="<?=$exclude_custumer_ids?>">
						<input type="hidden" name="ordre" value="<?=$ordre?>">

					<?endif //is_booking?>
					<input name="words" type="text" id="text" class="search"></td><td><input type="submit" name="Submit" value="ok" class="boto-search"></td>
			</tr>
	</table>
	</form>
</div>
<table width="100%" height="1" border="0" cellpadding="0" cellspacing="0" bgcolor="#999999">
  <tr>
    <td></td>
  </tr>
</table>

<table border="0" cellspacing="0" cellpadding="0" height="448" <?if($is_booking) echo ' width="100%"'?>>
  <tr>
    <td valign="top" <?=($is_booking)?'':' width="610"'?>>
		
		<iframe name="listAddresses" scrolling="yes" marginwidth="0" marginheight="0" frameborder="0" vspace="0" hspace="0" style="overflow:visible; width:<?=$is_booking?'100%':'610px'?>; height:448px;"  src="/admin/?action=list_records_select_custumer&menu_id=<?=$menu_id?><?if($is_booking):?>&book_id=<?=$book_id?>&main_custumer=<?=$main_custumer?>&exclude_custumer_ids=<?=$exclude_custumer_ids?>&ordre=<?=$ordre?><?endif ?>"></iframe>
	</td>
	
	<?if(!$is_booking):?>
    <td width="240" class="assign_right" valign="top">
		<h5><?=$c_selected_customers?></h5>
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		  <tr>
			<td id="write_cell"></td>
		  </tr>
		</table></td>
	<?endif ?>
	
  </tr>
</table>
	<?if(!$is_booking):?>
	<SCRIPT LANGUAGE="JavaScript" TYPE="text/javascript">
	<!--
	populate_table();
	//-->
	</SCRIPT>
	<table width="100%" height="1" border="0" cellpadding="0" cellspacing="0" bgcolor="#999999">
	  <tr>
		<td height="30" align="center">	
		  <form action="<?=$submit_link?>" method="post" name="theForm" target="save_frame">
		 <input name="property_id" type="hidden" value="<?=$property_id?>">
		 <input name="contract_id" type="hidden" value="<?=$contract_id?>">
		  <div class="centerFloat1"><div class="centerFloat2">
		  <input class="btn_save_select_frame" type="button" name="Submit" value="<?=$c_ok?>" onClick="pass_selected_addresses()">
		  <input class="btn_select_frame" type="button" name="Submit" value="<?=$c_cancel?>" onClick="top.hidePopWin(false);"></div></div>
		  <input name="selected_addresses_ids" type="hidden" value="">
		  <input name="selected_addresses_names" type="hidden" value="">
		</form>  	  
		</td>
	  </tr>
	</table>
	<?endif //is_booking?>
</body>
</html>