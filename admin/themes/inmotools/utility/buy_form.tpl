<script language="JavaScript" src="/common/jscripts/mortgage-calculator.js"></script>
<script language="JavaScript" src="/admin/modules/utility/jscripts/functions_buy.js?v=<?= $version ?>"></script>
<link href="/admin/themes/inmotools/styles/utility.css" rel="stylesheet"
      type="text/css">

<form id="main_form" name="theFormNO">
	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="form">
		<tr>
			<td><? if($form_new): ?><? if($save_button): ?>
					<div id="form_buttons_holder">
						<table id="form_buttons">
							<tr>
								<td class="buttons_top">
									<div class="right">
										<? /* <input type="submit" name="Submit" value="<?= $c_send_new ?>" class="btn_form1"/> */ ?>

										<a class="btn_form1" onclick="Buy.print()">
											<?= $c_print ?>
										</a>
									</div>
								</td>
							</tr>
						</table>
					</div>
				<? endif //save_button?>
				<? endif //form_new?>
				<? /* <? if($form_edit): ?>
 					<div id="form_buttons_holder">
 						<table id="form_buttons">
 							<tr>
 								<td valign="bottom">
 									<div class="menu">
 										<div><?= $c_edit_data ?></div><? if($has_images): ?>
 											<a href="<?= $image_link ?>"><?= $c_edit_images ?></a>
 										<? endif //has_images?><? foreach($form_tabs as $r): extract( $r ) ?><a
 											href="?action=<?= $tab_action ?>&<?= $id_field ?>=<?= $id ?>&menu_id=<?= $menu_id ?>"><?= $tab_caption ?></a><? endforeach //$loop?>
 									</div>
 								</td>
 								<td class="buttons_top">
 									<div class="right">
 										<? if($back_button): ?><a class="btn_form_back"
 										                          href="<?= $back_button ?>"></a><? endif //back_button?>
 										<? if($save_button): ?><input type="submit" name="Submit"
 										                              value="<?= $c_send_edit ?>"
 										                              class="btn_form1"><? endif //save_button?><? if($delete_button): ?>
 										<a class="btn_form_delete"
 										   href="javascript:submit_form('<? if($has_bin): ?><?= $c_confirm_form_bin ?><? else: ?><?= $c_confirm_form_deleted ?><? endif //has_bin?>', true);">
 											<?= $c_delete ?>
 											</a><? endif //delete_button?><? if($preview_button): ?><a class="btn_form1"
 										                                                               href="<?= $preview_link ?>"
 										                                                               target="_blank">
 											<?= $c_preview ?>
 											</a><? endif //preview_button?><? if($print_button): ?><a class="btn_form1 btn_print"
 										                                                              href="<?= $print_link ?>">
 											<?= $c_print ?>
 											</a> <? endif //print_button?></div>
 								</td>
 							</tr>
 						</table>
 					</div>
 				<? endif //form_edit?> */ ?>
				<table width="100%" border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td><? if($c_form_title): ?>
								<table width="100%" cellpadding="4" cellspacing="0">
									<tr>
										<td class="formCaptionTitle"><strong>
												<?= $c_form_title ?> <? if($form_subtitle): ?>
													<span><?= $form_subtitle ?></span><? endif //form_subtitle?>
											</strong></td>
									</tr>
								</table>
							<? endif //c_form_title?>

							<? /*
							////////////////////////////////////////
							Camps principals
							////////////////////////////////////////
							*/ ?>
							<div id="price_div_holder">
								<table width="100%" cellpadding="4" cellspacing="0" class="formstbl">

									<tr>
										<td width="170" valign="top" class="formsCaption">
											<?= $c_price ?>
										</td>
										<td valign="top" class="forms">
											<label class="sufix active"><?= $price ?><span> €</span></label>
										</td>
									</tr>
									<tr>
										<td width="170" valign="top" class="formsCaption">
											<?= $c_mortgage ?>
										</td>
										<td valign="top" class="forms">
											<label class="sufix active chain"><?= $mortgage_percent ?><span> %</span></label>
											<label class="sufix"><?= $mortgage ?><span> €</span></label>
										</td>
									</tr>
									<tr class="mortgage_holder">
										<td width="150" valign="top" class="formsCaption">
											<?= $c_mortgage_length ?>
										</td>
										<td valign="top" class="forms">
											<label class="sufix active"><?= $mortgage_length ?><span> <?= $c_mortgage_years ?></span></label>
										</td>
									</tr>
									<tr class="mortgage_holder">
										<td width="150" valign="top" class="formsCaption">
											<?= $c_mortgage_initial_interest ?>
										</td>
										<td valign="top" class="forms">
											<label class="sufix active"><?= $mortgage_initial_interest ?><span> %</span></label>
										</td>
									</tr>

								</table>
							</div>

							<? /*
							////////////////////////////////////////
							Altres camps
							////////////////////////////////////////
							*/ ?>
							<div id="other_fields">
								<table width="100%" cellpadding="4" cellspacing="0" class="formstbl">
									<? /* <? if($form_level1_title): ?>
							 										<tr>
							 											<td colspan="2" class="formCaptionTitle"><?= $form_level1_title ?></td>
							 										</tr>
							 									<? endif //form_level1_title?>

							 									<? if($form_level2_title): ?>
							 										<tr>
							 											<td colspan="2" class="formsCaptionHor"><?= $form_level2_title ?></td>
							 										</tr>
							 									<? endif //form_level2_title?> */ ?>
									<tr>
										<td width="150" valign="top" class="formsCaption">
											<?= $c_taxtype_id ?>
										</td>
										<td valign="top" class="forms">
											<?= $taxtype_id ?>
										</td>
									</tr>
									<tr>
										<td width="150" valign="top" class="formsCaption">
											<?= $c_notary ?>
										</td>
										<td valign="top" class="forms">
											<label class="sufix active chain"><?= $notary_percent ?><span> %</span></label>
											<label class="sufix"><?= $notary ?><span> €</span></label>
										</td>
									</tr>
									<tr>
										<td width="150" valign="top" class="formsCaption">
											<?= $c_register ?>
										</td>
										<td valign="top" class="forms">
											<label class="sufix active chain"><?= $register_percent ?><span> %</span></label>
											<label class="sufix"><?= $register ?><span> €</span></label>
										</td>
									</tr>
									<tr>
										<td width="150" valign="top" class="formsCaption">
											<?= $c_agency ?>
										</td>
										<td valign="top" class="forms">
											<label class="sufix active chain"><?= $agency_percent ?><span>  %</span></label>
											<label class="sufix"><?= $agency ?><span> €</span></label>
										</td>
									</tr>
									<tr class="mortgage_holder">
										<td width="150" valign="top" class="formsCaption">
											<?= $c_mortgage ?>
										</td>
										<td valign="top" class="forms">

										</td>
									</tr>
									<tr class="mortgage_holder">
										<td width="150" valign="top" class="indent formsCaption">
											<?= $c_mortgage_open ?>
										</td>
										<td valign="top" class="indent forms">
											<label class="sufix active chain"><?= $mortgage_open_percent ?><span> %</span></label>
											<label class="sufix"><?= $mortgage_open ?><span> €</span></label>
										</td>
									</tr>
									<tr class="mortgage_holder">
										<td width="150" valign="top" class="indent formsCaption">
											<?= $c_mortgage_notary ?>
										</td>
										<td valign="top" class="indent forms">
											<label class="sufix active chain"><?= $mortgage_notary_percent ?><span> %</span></label>
											<label class="sufix"><?= $mortgage_notary ?><span> €</span></label>
										</td>
									</tr>
									<tr class="mortgage_holder">
										<td width="150" valign="top" class="indent formsCaption">
											<?= $c_mortgage_register ?>
										</td>
										<td valign="top" class="indent forms">
											<label class="sufix active chain"><?= $mortgage_register_percent ?><span> %</span></label>
											<label class="sufix"><?= $mortgage_register ?><span> €</span></label>
										</td>
									</tr>
									<tr class="mortgage_holder">
										<td width="150" valign="top" class="indent formsCaption">
											<?= $c_mortgage_valuation ?>
										</td>
										<td valign="top" class="indent forms">
											<label class="sufix active"><?= $mortgage_valuation ?><span> €</span></label>
										</td>
									</tr>
								</table>
							</div>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</form>


<? /*
////////////////////////////////////////
Report
////////////////////////////////////////
*/ ?>
<div id="report">
	<h5><?= $c_report_title ?></h5>
	<table id="report_title">
		<tr>
			<th><?= $c_price ?>:</th>
			<td><span id="price"></span></td>
		</tr>
		<tr class="mortgage_holder">
			<th><?= $c_mortgage ?>:</th>
			<td><span id="mortgage2"></span></td>
		</tr>
		<tr class="mortgage_holder">
			<th><?= $c_mortgage_length ?>:</th>
			<td><span id="mortgage_length2"></span></td>
		</tr>
		<tr class="mortgage_holder">
			<th><?= $c_monthly_payment ?>:</th>
			<td><span id="monthly_payment2"></span></td>
		</tr>
	</table>
	<div class="report_sep"></div>

	<h6><?= $c_expense_not_financed ?></h6>
	<table>
		<tr>
			<th><?= $c_down_payment ?>:</th>
			<td id="down_payment"></td>
		</tr>
		<tr class="sep">
			<td colspan="2"></td>
		</tr>
		<tr>
			<th><?= $c_tax ?> <span id="taxtype"></span> <span id="tax_val"></span>:</th>
			<td id="tax"></td>
		</tr>
		<tr>
			<th><?= $c_notary ?>:</th>
			<td id="notary"></td>
		</tr>
		<tr>
			<th><?= $c_register ?>:</th>
			<td id="register"></td>
		</tr>
		<tr id="agency_holder">
			<th><?= $c_agency ?>:</th>
			<td id="agency"></td>
		</tr>
		<tr id="judicial_acts_holder">
			<th><?= $c_judicial_acts ?> <span >1,5 %</span>:</th>
			<td id="judicial_acts"></td>
		</tr>

		<tr class="sep">
			<td colspan="2"></td>
		</tr>

		<tr class="mortgage_holder">
			<th><?= $c_mortgage ?>:</th>
			<td></td>
		</tr>
		<tr id="mortgage_notary_holder" class="indent mortgage_holder">
			<th><?= $c_mortgage_notary ?>:</th>
			<td id="mortgage_notary"></td>
		</tr>
		<tr id="mortgage_register_holder" class="indent mortgage_holder">
			<th><?= $c_mortgage_register ?>:</th>
			<td id="mortgage_register"></td>
		</tr>
		<tr id="mortgage_valuation_holder" class="indent mortgage_holder">
			<th><?= $c_mortgage_valuation ?>:</th>
			<td id="mortgage_valuation"></td>
		</tr>
		<tr id="mortgage_open_holder" class="indent mortgage_holder">
			<th><?= $c_mortgage_open ?>:</th>
			<td id="mortgage_open"></td>
		</tr>
		<tr id="judicial_acts_mortgage_holder" class="indent mortgage_holder">
			<th><?= $c_judicial_acts_mortgage ?>:</th>
			<td id="judicial_acts_mortgage"></td>
		</tr>
		<tr class="sep">
			<td colspan="2"></td>
		</tr>
		<tr class="total expenses">
			<th><?= $c_total_expenses ?>:</th>
			<td id="total_expenses"></td>
		</tr>
		<tr class="total">
			<th><?= $c_total ?>:</th>
			<td id="total_no_financed"></td>
		</tr>
	</table>
	<div class="indent report_sep mortgage_holder"></div>


	<div class="mortgage_holder page_before"><h6><?= $c_mortgage_title ?></h6>
		<table>
			<tr>
				<th><?= $c_mortgage ?>:</th>
				<td id="mortgage"></td>
			</tr>
			<tr>
				<th><?= $c_mortgage_length ?>:</th>
				<td id="mortgage_length"></td>
			</tr>
			<tr>
				<th><?= $c_mortgage_initial_interest ?>:</th>
				<td id="mortgage_initial_interest"></td>
			</tr>
			<tr>
				<th><?= $c_monthly_payment ?>:</th>
				<td id="monthly_payment"></td>
			</tr>
		</table>

		<div id="mortgage_table"></div>

	</div>

	<div class="vinculant"><?= $c_no_vinculant ?></div>
</div>

<script type="text/javascript">
	var buy_id = <?= $id ?>
		, c_mortgage_years = '<?= addslashes( strtolower($c_mortgage_years) )?>'
		, c_table_quote = '<?= addslashes($c_table_quote)?>'
		, c_table_year = '<?= addslashes($c_table_year)?>'
		, c_table_payment = '<?= addslashes($c_table_payment)?>'
		, c_table_interest = '<?= addslashes($c_table_interest)?>'
		, c_table_balance = '<?= addslashes($c_table_balance)?>'
		, c_table_total_returned = '<?= addslashes($c_table_total_returned)?>'
		, c_table_total_paid = '<?= addslashes($c_table_total_paid)?>'
</script>
