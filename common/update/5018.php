<?

/* 9-5-13 
  Modificacions eina nàutica

 */

if (is_table('nautica__configadmin')) {	
	
	$update_sql = "
		INSERT INTO `nautica__configadmin` (`name`,`value`) VALUES ('auto_ref','0');
		INSERT INTO `nautica__configadmin` (`name`,`value`) VALUES ('has_clau','1');
		INSERT INTO `nautica__configadmin` (`name`,`value`) VALUES ('has_pdf','1');
		INSERT INTO `nautica__configadmin` (`name`,`value`) VALUES ('show_electronics','1');
		INSERT INTO `nautica__configadmin` (`name`,`value`) VALUES ('show_extras','1');
		INSERT INTO `nautica__configadmin` (`name`,`value`) VALUES ('show_sails','1');
		";
	$update_sql .="
		ALTER TABLE `nautica__embarcacion_language`
			ADD COLUMN `page_title` VARCHAR(255) NOT NULL DEFAULT '' AFTER `equipament`,
			ADD COLUMN `page_description` VARCHAR(255) NOT NULL DEFAULT '' AFTER `page_title`,
			ADD COLUMN `page_keywords` VARCHAR(255) NOT NULL DEFAULT '' AFTER `page_description`,
			ADD COLUMN `embarcacion_file_name` VARCHAR(100) NOT NULL DEFAULT '' AFTER `page_keywords`,
			ADD COLUMN `embarcacion_old_file_name` VARCHAR(100) NOT NULL DEFAULT '' AFTER `embarcacion_file_name`;";	
	$update_sql .="
		ALTER TABLE `nautica__embarcacion`
			ADD COLUMN `changeable` TINYINT(1) NOT NULL DEFAULT '0' AFTER `price`;
		INSERT INTO `nautica__configadmin` (`name`, `value`) VALUES ('list_cols', '4');";	
	$update_sql .="
		ALTER TABLE `nautica__embarcacion`
			ADD COLUMN `engine_type` ENUM('fora','intra') NOT NULL DEFAULT 'intra' AFTER `engine`,
			ADD COLUMN `fuel_tank` INT NOT NULL DEFAULT '0' AFTER `fuel`,
			ADD COLUMN `water_tank` INT NOT NULL DEFAULT '0' AFTER `fuel_tank`,
			ADD COLUMN `compass` TINYINT(3) NOT NULL DEFAULT '0' AFTER `roller_mainsail`,
			ADD COLUMN `bath_stair` TINYINT(3) NOT NULL DEFAULT '0' AFTER `compass`,
			ADD COLUMN `trim` TINYINT(3) NOT NULL DEFAULT '0' AFTER `bath_stair`,
			ADD COLUMN `kitchen` TINYINT(3) NOT NULL DEFAULT '0' AFTER `trim`,
			ADD COLUMN `microwave` TINYINT(3) NOT NULL DEFAULT '0' AFTER `kitchen`,
			ADD COLUMN `shower` TINYINT(3) NOT NULL DEFAULT '0' AFTER `microwave`;
		;";	
	
}


?>