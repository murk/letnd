<?if($rest_of_custumers):?>

	<table>
		<?foreach($rest_of_custumers as $l): extract ($l)?>
			<tr>
				<td class="person-num">
					<?=$person?> <?=$num?>
				</td>
				<td>
					<?if($num==1):?>
						<span class="adult-one"><?=$custumer_select?></span>
					<?else:?>
						<?=$custumer_select?> 
					<?endif ?>
				</td>
			</tr>
			<tr>
				<td colspan="2" class="custumer-buttons">
					<? /* Si te link ( client >2 o si només hi ha client 1  */ ?>
					<?if($add_new_custumer_link):?>
						<a href="<?=$add_new_custumer_link?>"><?=$c_add_new_custumer?></a> 
						-
						<?if($selected_id):?><a href="javascript:bookingform.edit_custumer(<?=$selected_id?>,<?=$id?>)"><?=$c_edit_custumer?></a> - <?endif?>
						
						<a href="javascript:bookingform.select_custumer(<?=$ordre?>,<?=$id?>)"><?=$c_add_custumer?></a>

					<? /* Missatge que primer s'ha d'escollir client 1  */ ?>
					<?elseif ($num<>1): //?>
						<span class="choose-first"><?=$c_choose_first?></span>

					<? /* Client 1 ja està triat, per tant només es pot editar  */ ?>
					<?elseif ($num==1): //?><?if($selected_id):?><a href="javascript:bookingform.edit_custumer(<?=$selected_id?>,<?=$id?>)"><?=$c_edit_custumer?></a><?endif?>
					<?endif // add_new_custumer_link?>
				</td>
			</tr>
		<?endforeach //$rest_of_custumers?>
	</table>
	
<?endif //rest_of_custumers?>