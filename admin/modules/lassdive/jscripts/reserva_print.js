class ReservaPrint {

	constructor() {

		this.text = {
			num_reserva: LASSDIVE_NUM_RESERVA,
			pagat: LASSDIVE_PAGAT,
			paga_senyal: LASSDIVE_PAGA_SENYAL,
			pendent_pagament: LASSDIVE_PENDENT,
			reservaitem_date: LASSDIVE_RESERVA_ITEM_DATE,
			timetable: LASSDIVE_HORARI,
			centre_id: LASSDIVE_CENTRE,
			user_ids: LASSDIVE_USER_IDS
		};
		this.is_loaded = false;
		let _this = this;
		$(function () {

			$('body').append('<div style="display:none;"><canvas id="canvas" width="800" height="480"></canvas><img id="logo-print" src="/admin/themes/inmotools/lassdive/images/logo-print.png?v=2" alt=""/></div>');
			_this.canvas = $('#canvas').get(0);
			_this.is_loaded = true;

		});

	}

	print(order_id) {

		if (!this.is_loaded) return;

		this.order_id = order_id;
		this.get_vars(order_id);

	}

	start_printing() {

		let builder = new epson.ePOSBuilder();

		builder.addLayout(builder.LAYOUT_RECEIPT, 700);
		builder.addTextLang('es').addTextSmooth(true);

		this.draw_logo(builder);
		this.draw_title(builder);
		this.draw_customer(builder);
		this.draw_today_date(builder);
		this.draw_pay_details(builder);
		this.draw_order_items(builder);

		builder.addFeedUnit(50);
		builder.addCut();

		this.end_printing(builder);
	}

	draw_logo(builder) {

		// draw image (for raster image)
		let canvas = $('#canvas').get(0);
		let context = canvas.getContext('2d');
		context.drawImage($('#logo-print').get(0), 0, 0, 364, 110);

		// append raster image
		builder.addTextAlign(builder.ALIGN_LEFT);
		builder.addImage(context, 0, 0, 364, 110);
		builder.addFeedLine(1);

	}

	draw_title(builder) {

		builder.addFeedUnit(12); // fa pasar el paper X dots
		builder.addTextAlign(builder.ALIGN_LEFT);

		builder.addTextStyle(0, 0, 1);
		builder.addTextSize(1, 2).addText(this.text.num_reserva);
		builder.addTextStyle(0, 0, 1);

		builder.addTextSize(4, 2).addText(' ' + this.order_id);

	}

	draw_customer(builder) {
		builder.addFeedUnit(100);
		builder.addTextSize(1, 2).addTextStyle(0, 1, 0).addText(this.vars.customer);
	}

	draw_today_date(builder) {
		builder.addFeedUnit(80);
		builder.addTextSize(1, 1).addTextStyle(0, 0, 1).addText(this.vars.today);
	}

	draw_pay_details(builder) {

		let column_width = 200;
		builder.addFeedUnit(70);

		builder.addTextSize(1, 2).addTextStyle(0, 0, 0);

		if (this.vars.order_status == 'pending') {

			builder.addTextPosition(0);
			builder.addTextSize(1, 1).addText(this.text.paga_senyal);

			builder.addTextPosition(column_width);
			builder.addTextAlign(builder.ALIGN_RIGHT);
			builder.addTextSize(2, 1).addText(this.vars.deposit);

			builder.addFeedUnit(40);

			builder.addTextAlign(builder.ALIGN_LEFT);
			builder.addTextPosition(0);
			builder.addTextSize(1, 1).addText(this.text.pendent_pagament);

			builder.addTextPosition(column_width);
			builder.addTextAlign(builder.ALIGN_RIGHT);
			builder.addTextStyle(0, 0, 1);
			builder.addTextSize(2, 1).addText(this.vars.total_pending);
		}
		else {
			builder.addTextSize(2, 1).addText(this.text.pagat);
			builder.addTextStyle(0, 0, 0);

			builder.addTextAlign(builder.ALIGN_RIGHT);
			builder.addTextPosition(column_width);
			builder.addTextStyle(0, 0, 1);
			builder.addTextSize(2, 1).addText(this.vars.all_basetax);
		}

	}

	draw_order_items(builder) {

		let column_width = 200;
		this.draw_line(builder);


		for (let order_item of this.vars.order_items) {

			builder.addTextPosition(0);
			builder.addTextSize(1, 1).addTextStyle(0, 0, 1).addText(order_item.product_title);
			builder.addTextStyle(0, 0, 0);

			if (order_item.reservaitem_date) {

				builder.addFeedUnit(50);

				builder.addTextAlign(builder.ALIGN_LEFT);
				builder.addTextPosition(0);
				builder.addText(this.text.reservaitem_date);
				builder.addTextPosition(column_width);
				builder.addText(order_item.reservaitem_date);
			}

			if (order_item.start_time) {

				builder.addFeedUnit(36);

				builder.addTextPosition(0);
				builder.addText(this.text.timetable);
				builder.addTextPosition(column_width);
				builder.addText('De ' + order_item.start_time + ' a ' + order_item.end_time);
			}

			if (order_item.centre_id) {

				builder.addFeedUnit(36);

				builder.addTextPosition(0);
				builder.addText(this.text.centre_id);
				builder.addTextPosition(column_width);
				builder.addText(order_item.centre_id);
			}


			let users_length = order_item.user_id.length;
			if (users_length) {

				builder.addFeedUnit(36);

				builder.addTextPosition(0);
				builder.addText(this.text.user_ids);

				for (let n = 0; n < users_length; n++) {
					let user_id = order_item.user_id[n];
					let sep = users_length == n + 1 ? '' : '\n';
					builder.addTextPosition(column_width);
					builder.addText(user_id + sep);
				}
			}
			this.draw_line(builder);

		}

	}

	// He fet la funció perque això no va ->	builder.addHLine(400, 500, builder.LINE_THIN_DOUBLE);
	draw_line(builder) {
		builder.addText("\n");
		builder.addFeedUnit(26);

		builder.addTextAlign(builder.ALIGN_LEFT);
		let context = this.canvas.getContext('2d');
		context.fillStyle = 'rgba(25, 25, 25, 1)';
		context.fillRect(0, 0, 700, 1);
		builder.addImage(context, 0, 0, 700, 1);
		builder.addFeedUnit(30);
	}

	end_printing(builder) {

		let ipaddr = '172.20.10.10';
		let devid = 'local_printer';
		let timeout = '60000';

		let url = `http://${ipaddr}/cgi-bin/epos/service.cgi?devid=${devid}&timeout=${timeout}`;
		let epos = new epson.ePOSPrint(url);

		let _this = this;

		epos.onreceive = function (res) {

			let message = _this.getStatusText(epos, res.status);

			if (!res.success){
				message = `<div class="error"><h5>ERROR</h5> <br> ${message}</div>`;
			}

			_this.show_message(message, res.success?1500:3000);
		};

		epos.onerror = function (err) {
			let message = `<div class="error"><h5>ERROR</h5> <br> ${err.responseText}</div>`;
			_this.show_message(message, 3000);

		};

		showPopWin('', 300, 200, null, false, false, LASSDIVE_PRINTING + '<img src="/common/images/wait.png" style="margin-left:10px;vertical-align: middle;" width="32" height="32" />');

		// send
		epos.send(builder.toString());
	}

	show_message(message, timeout = 2000) {

		top.window.setTimeout("hidePopWin(false);", timeout);
		top.showPopWin("", 300, 200, null, false, false, message);
	}

	get_vars(order_id) {

		let _this = this;
		let link = `/${gl_language}/lassdive/print/get_vars/?order_id=${order_id}`;

		$.ajax({
			url: link
		})
			.done(function (data) {

				_this.vars = data.vars;
				_this.start_printing();

			});
	}

	// get status text
	getStatusText(e, status) {
		let s = '';
		if (status & e.ASB_NO_RESPONSE) {
			s += ` ${LASSDIVE_NO_RESPONSE} <br>`;
		}
		if (status & e.ASB_PRINT_SUCCESS) {
			s += ` ${LASSDIVE_PRINT_SUCCESS} <br>`;
		}
		if (status & e.ASB_DRAWER_KICK) {
			s += ` ${LASSDIVE_ASB_DRAWER_KICK} <br>`;
		}
		if (status & e.ASB_BATTERY_OFFLINE) {
			s += ` ${LASSDIVE_ASB_BATTERY_OFFLINE} <br>`;
		}
		if (status & e.ASB_OFF_LINE) {
			s += ` ${LASSDIVE_ASB_OFF_LINE} <br>`;
		}
		if (status & e.ASB_COVER_OPEN) {
			s += ` ${LASSDIVE_ASB_COVER_OPEN} <br>`;
		}
		if (status & e.ASB_PAPER_FEED) {
			s += ` ${LASSDIVE_ASB_PAPER_FEED} <br>`;
		}
		if (status & e.ASB_WAIT_ON_LINE) {
			s += ` ${LASSDIVE_ASB_WAIT_ON_LINE} <br>`;
		}
		if (status & e.ASB_PANEL_SWITCH) {
			s += ` ${LASSDIVE_ASB_PANEL_SWITCH} <br>`;
		}
		if (status & e.ASB_MECHANICAL_ERR) {
			s += ` ${LASSDIVE_ASB_MECHANICAL_ERR} <br>`;
		}
		if (status & e.ASB_AUTOCUTTER_ERR) {
			s += ` ${LASSDIVE_ASB_AUTOCUTTER_ERR} <br>`;
		}
		if (status & e.ASB_UNRECOVER_ERR) {
			s += ` ${LASSDIVE_ASB_UNRECOVER_ERR} <br>`;
		}
		if (status & e.ASB_AUTORECOVER_ERR) {
			s += ` ${LASSDIVE_ASB_AUTORECOVER_ERR} <br>`;
		}
		if (status & e.ASB_RECEIPT_NEAR_END) {
			s += ` ${LASSDIVE_ASB_RECEIPT_NEAR_END} <br>`;
		}
		if (status & e.ASB_RECEIPT_END) {
			s += ` ${LASSDIVE_ASB_RECEIPT_END} <br>`;
		}
		if (status & e.ASB_BUZZER) {
			s += ` ${LASSDIVE_ASB_BUZZER} <br>`;
		}
		if (status & e.ASB_WAIT_REMOVE_LABEL) {
			s += ` ${LASSDIVE_ASB_WAIT_REMOVE_LABEL} <br>`;
		}
		if (status & e.ASB_NO_LABEL) {
			// s += ' No paper in the label peeling detector <br>';
		}
		if (status & e.ASB_SPOOLER_IS_STOPPED) {
			s += ` ${LASSDIVE_ASB_SPOOLER_IS_STOPPED} <br>`;
		}
		return s;
	}

}

let gl_reserva_print = new ReservaPrint();