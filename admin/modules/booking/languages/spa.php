<?
// menus eina
if (!defined('INMO_MENU_PROPERTY')){
	define('INMO_MENU_PROPERTY', 'Inmuebles');
	define('INMO_MENU_PROPERTY_NEW', 'Entrar nueva finca');
	define('INMO_MENU_PROPERTY_LIST', 'Listar inmuebles');
	define('INMO_MENU_SEARCH_FORM', 'Buscar y editar inmuebles');
	define('INMO_MENU_PROPERTY_BIN', 'Papelera de reciclaje');
	define('INMO_MENU_CATEGORY', 'Tipos de inmuebles');
	define('INMO_MENU_CATEGORY_NEW', 'Entrar nuevo tipo');
	define('INMO_MENU_CATEGORY_LIST', 'Listar tipos');
	define('INMO_MENU_TRANSLATION', 'Traducciones');
	define('INMO_MENU_PROPERTY_TRANSLATION_LIST', 'Inmuebles para traducir');
	define('INMO_MENU_CATEGORY_TRANSLATION_LIST', 'Categorías para traducir');
	define('INMO_MENU_CONFIG', 'Configuración inmuebles');
	define('INMO_MENU_CONFIGADMIN_LIST', 'Configuración parte privada');
	define('INMO_MENU_CONFIGPUBLIC_LIST', 'Configuración parte pública');
	define('INMO_MENU_CONFIGADMIN', 'Configuración parte privada');
	define('INMO_MENU_CONFIGPUBLIC', 'Configuración parte pública');
	define('INMO_MENU_ZONE', 'Zonas');
	define('INMO_MENU_ZONE_NEW', 'Entrar nueva zona');
	define('INMO_MENU_ZONE_LIST', 'Listar zonas');
	define('INMO_MENU_ZONE_BIN', 'Papelera de reciclaje');
	define('INMO_MENU_TV', 'Escaparates virtuales');
	define('INMO_MENU_TV_LIST_DOWNLOAD', 'Descargar escaparate');
	define('INMO_MENU_TV_LIST', 'Seleccionar Inmuebles');
	define('INMO_MENU_TV_LIST_SELECTED', 'Inmuebles en el escaparate');
	define('INMO_MENU_PROPERTY_MAP_LIST', 'Buscar por mapa');
	define('INMO_MENU_ADJECTIVE_NEW', 'Entrar nuevo adjetivo');
	define('INMO_MENU_ADJECTIVE_LIST', 'Listar adjetivos');

	define('BOOKING_MENU_BOOK','Reservas');
	define('BOOKING_MENU_BOOK_NEW','Entrar nueva reserva');
	define('BOOKING_MENU_BOOK_LIST','Listar reservas');
	define('BOOKING_MENU_BOOK_LIST_PAST','Reservas finalitzadas');
	define('BOOKING_MENU_BOOK_SEARCH','Disponibilidad');

	define('BOOKING_MENU_EXTRA','Servicios');
	define('BOOKING_MENU_EXTRA_NEW','Entrar nuevo servicio');
	define('BOOKING_MENU_EXTRA_LIST','Listar servicios');
	define('BOOKING_MENU_EXTRA_LIST_BIN','Papelera de reciclaje');

	define('BOOKING_MENU_SEASON','Temporadas');
	define('BOOKING_MENU_SEASON_NEW','Entrar nueva temporada');
	define('BOOKING_MENU_SEASON_LIST','Listar temporadas');

	define ('TITLE_COMARCA_ADMIN','Por zona: ');
	define('PROPERTY_AUTO_REF', 'Automática');

	define('BOOKING_MENU_PROMCODE','Código promocional');
	define('BOOKING_MENU_PROMCODE_NEW','Crear código');
	define('BOOKING_MENU_PROMCODE_LIST','Listar código');
	define('BOOKING_MENU_PROMCODE_LIST_BIN','Papelera');

	define('BOOKING_MENU_POLICE','Registro Mossos');
	define('BOOKING_MENU_POLICE_LIST','Listar reservas');
}

// captions seccio
$gl_caption_book['c_payment'] = 'Pago';
$gl_caption_book['c_payed'] = 'Pagado';
$gl_caption_book['c_payment_num'] = 'Núm.';
$gl_caption_book['c_payment_1'] = 'Pago 1';
$gl_caption_book['c_payment_2'] = 'Pago 2';
$gl_caption_book['c_payment_3'] = 'Pago 3';
$gl_caption_book['c_payment_4'] = 'Pago 4';
$gl_caption_book['c_payment_done_1'] =
$gl_caption_book['c_payment_done_2'] =
$gl_caption_book['c_payment_done_3'] =
$gl_caption_book['c_payment_done_4'] = 'Pagado';
$gl_caption_book['c_payment_entered_1'] =
$gl_caption_book['c_payment_entered_2'] =
$gl_caption_book['c_payment_entered_3'] =
$gl_caption_book['c_payment_entered_4'] = 'Fecha';
$gl_caption_book['c_payment_rest'] = 'Pago pendiente';
$gl_caption_book['c_payment_uncheck_message'] = '¿Seguro que quieres desmarcar el pago?';
$gl_caption_book['c_payment_send_mail'] = 'enviar&nbsp;mail';
$gl_caption_book['c_payment_send_mail_subject'] = 'Notificación de pago de la reserva: %1$s';
$gl_caption_book['c_payment_send_mail_body'] = 'Apreciado %1$s, <p>Le notificamos que hemos recibido el pago por importe de %2$s € de la reserva núm. %3$s en fecha de %4$s</p>';
$gl_caption_book['c_payment_send_mail_body_rest'] = '<p>Queda pendiente de pago el importe de %1$s €</p>';
$gl_caption_book['c_payment_send_mail_body2'] = '<p>Estamos a su disposición</p>';
$gl_caption_book['c_payment_mail_sent'] = 'El E-mail se ha enviado correctamente a:';
$gl_caption_book['c_payment_mail_not_sent'] = 'No se pudo enviar el E-mail a:';
$gl_caption_book['c_payment_mail_not_sent_no_mail'] = 'El client no tiene ninguna dirección de email entrada';
$gl_caption_book['c_is_owner'] = 'Propietario';
$gl_caption_book['c_caption_is_owner'] = 'Todos';
$gl_caption_book['c_is_owner_0'] = 'No';
$gl_caption_book['c_is_owner_1'] = 'Sí';

$gl_caption_book['c_property_title'] = 'Inmueble';
$gl_caption_book['c_names'] = 'Clientes';
$gl_caption_book['c_dates'] = 'Fechas reserva';
$gl_caption['c_dates_from'] = 'Del';
$gl_caption['c_dates_to'] = 'al';
$gl_caption_book['c_date_in'] = 'Fecha de entrada';
$gl_caption_book_list['c_date_in'] = 'Entrada';
$gl_caption_book['c_date_out'] = 'Fecha de salida';
$gl_caption_book_list['c_date_out'] = 'Salida';
$gl_caption_book['c_book_entered'] = 'Reservado';
$gl_caption_book['c_capacity'] = 'Capacidad';
$gl_caption_book['c_person'] = 'Personas';
$gl_caption_book['c_adult'] = 'Adultos';
$gl_caption_book['c_child'] = 'Niños';
$gl_caption_book['c_baby'] = 'Bebés';
$gl_caption_book['c_book_id'] = 'Reserva';
$gl_caption_book['c_property_id'] = 'Inmueble';
$gl_caption_book['c_choose_first'] = "Primero se tiene que escoger el Adulto 1";
$gl_caption['c_status'] = "Estado";
$gl_caption['c_status_group_free'] = "Libre";
$gl_caption['c_status_group_not_free'] = "Ocupado";
$gl_caption['c_status_pre_booked'] = "Pre reserva";
$gl_caption['c_status_cancelled'] = "Cancelada";
$gl_caption['c_status_booked'] = "Reservado";
$gl_caption['c_status_confirmed_email'] = "Confirmada por email";
$gl_caption['c_status_confirmed_post'] = "Confirmada por correo";
$gl_caption['c_status_contract_signed'] = "Contracto firmado";
$gl_caption_book['c_caption_filter_status'] = 'Todos';

for ($i=0;$i<=25;$i++){	
	
	$gl_caption_book['c_adult_'.$i] = $i;
	$gl_caption_book['c_child_'.$i] = $i;
	$gl_caption_book['c_baby_'.$i] = $i;
	
}

$gl_caption_book ['c_maximum'] = 'Màxim';
$gl_caption_book ['c_name'] = 'Nombre';
$gl_caption_book ['c_surnames'] = 'Apellidos';
$gl_caption_book ['c_mail'] = 'E-mail';
$gl_caption_book ['c_phone'] = 'Teléfono';
$gl_caption_book ['c_comment'] = 'Observaciones';
$gl_caption_book ['c_comment_private'] = 'Observaciones privadas';
//$gl_caption_book ['c_property_id'] = '';
$gl_caption_book ['c_send_new'] = 'Reservar';
$gl_caption_book ['c_custumers'] = 'Clientes';
$gl_caption_book ['c_price_extras'] = 'Extras';
$gl_caption_book ['c_price_extras_search'] = 'Precio extras';
$gl_caption_book ['c_price_total'] = 'Precio total';
$gl_caption_book ['c_price_book'] = 'Precio reserva';
$gl_caption_book ['c_price'] = 'Precio';
$gl_caption_book ['c_edit_book_button'] = 'Editar reserva';
$gl_caption_book ['c_download_contract'] = 'Contrato';
$gl_caption_book ['c_custumer_search_group'] = 'Clientes';
$gl_caption_book ['c_extra_search_group'] = 'Extras';
$gl_caption_book ['c_recalculate_rate'] = 'Recalcular importe';
$gl_caption_book ['c_is_manual_price'] = 'Precio manual';

$gl_caption_book['c_form_title'] = 'Reserva';

$gl_messages_book['added'] = "Reserva realizada entre las fechas: %s y %s";
$gl_messages_book['added2'] = "Reserva realizada para \"%s\" entre las fechas: %s y %s";
$gl_messages_book['saved'] = "Reserva actualizada entre las fechas: %s y %s";
$gl_messages_book['saved2'] = "Reserva actualizada para \"%s\" entre las fechas: %s y %s";
$gl_messages_book['deleted'] = "Reserva borrada entre las fechas: %s y %s";
$gl_messages_book['deleted2'] = "Reserva borrada para %s entre las fechas: %s y %s";
$gl_messages_book['full'] = "Está ocupado entre las fechas: %s y %s";
$gl_messages_book['full2'] = 'El "inmueble" %s está ocupado entre las fechas: %s y %s';
$gl_messages_book['full3'] = 'Reservas en conflicto:';
$gl_messages_book['has_no_range'] = 'El inmueble no se alquila en estas fechas';
$gl_messages_book['too_many_person'] = $gl_caption['c_too_many_person_error'] = 'El número de personas excede el màximo para este inmueble';
$gl_messages_book['exceeds_31_days'] = 'Las viviendas de alquiler de temporada no se pueden alquilar por un periodo superior a 31 días, para cualquier duda, por favor contacte con nuestra oficina';


// custumers
$gl_caption['c_add_new_custumer'] = "Añadir cliente nuevo";
$gl_caption['c_edit_custumer'] = "Editar cliente";
$gl_caption['c_add_custumer'] = "Seleccionar cliente";

$gl_caption['c_adult_singular'] = "Adulto";
$gl_caption['c_child_singular'] = "Niño";
$gl_caption['c_baby_singular'] = "Bebé";


$gl_caption_extra ['c_extra_book_selected'] = 'Seleccionar';
$gl_caption_extra ['c_extra_book_input'] = '';

// serveis
$gl_caption['c_extra_type'] = "Tipo";
$gl_caption['c_extra_type_required'] = "Obligatorio";
$gl_caption['c_extra_type_optional'] = "Adicional";
$gl_caption['c_extra_type_included'] = "Incluido";
$gl_caption['c_extra'] = "Extra";
$gl_caption['c_has_quantity'] = "Cantidad seleccionable";
$gl_caption_list['c_has_quantity'] = "Cantidad";
$gl_caption['c_extra_quantity_search'] = "Cantidad";
$gl_caption['c_price'] = "Precio";
$gl_caption['c_extra_price'] = "Precio";
$gl_caption['c_comment'] = "Comentario";
$gl_caption['c_extras'] = "Extras";
$gl_caption['c_concept'] = "Concepto";
$gl_caption['c_concept_normal'] = "Extra";
$gl_caption['c_concept_deposit'] = "Fianza";
$gl_caption['c_concept_cleaning'] = "Limpieza";

$gl_caption['c_extra_type_required_search'] = "Extras obligatorios";
$gl_caption['c_extra_type_optional_search'] = "Extras adicionales";
$gl_caption['c_extra_type_included_search'] = "Extras incluidos";
$gl_caption['c_extra_total_price_search'] = "Precio total extra";
$gl_caption['c_extra_quantity_search'] = "Cantidad extra";
$gl_caption['c_extra_type_search'] = "Tipo extra";

// temporades
$gl_caption['c_season'] = $gl_caption['c_season_id'] = "Temporada";
$gl_caption['c_seasons'] = "Temporadas";
$gl_caption['c_ordre'] = "Orden";
$gl_caption['c_date_start'] = "Inicio";
$gl_caption['c_date_end'] = "Final";
$gl_caption['c_minimum_nights'] = "Mínimo";
$gl_caption['c_minimum_nights_1'] = "1 noche";
$gl_caption['c_minimum_nights_2'] = "2 noches";
$gl_caption['c_minimum_nights_3'] = "3 noches";
$gl_caption['c_minimum_nights_4'] = "4 noches";
$gl_caption['c_minimum_nights_5'] = "5 noches";
$gl_caption['c_minimum_nights_6'] = "6 noches";
$gl_caption['c_minimum_nights_7'] = "1 semana";
$gl_caption['c_minimum_nights_8'] = "8 noches";
$gl_caption['c_minimum_nights_9'] = "9 noches";
$gl_caption['c_minimum_nights_10'] = "10 noches";
$gl_caption['c_minimum_nights_11'] = "11 noches";
$gl_caption['c_minimum_nights_12'] = "12 noches";
$gl_caption['c_minimum_nights_13'] = "13 noches";
$gl_caption['c_minimum_nights_14'] = "2 semanas";
$gl_caption['c_price_night'] = "Precio por noche";
$gl_caption_list['c_price_night'] = "Noche";
$gl_caption['c_price_weekend'] = "Precio noche fin de semana";
$gl_caption_list['c_price_weekend'] = "Fin semana";
$gl_caption['c_price_more_week'] = "Precio + de 7 noches";
$gl_caption['c_price_week'] = "Precio semana";
$gl_caption_list['c_price_week'] = "Semana";
$gl_caption['c_price_week_type'] = "";
$gl_caption['c_price_week_type_week'] = "Por semana";
$gl_caption['c_price_week_type_night'] = "Por noche";
$gl_caption['c_color_id'] = $gl_caption['c_color']  = "Color";
$gl_caption['c_already_used'] = "Colores ya asignados";

$gl_caption['c_detail_season'] = "Temporada";
$gl_caption['c_detail_day'] = "Día";
$gl_caption['c_detail_type'] = "Tarifa";
$gl_caption['c_detail_price'] = "Precio";
$gl_caption['c_detail_price_night'] = "Noche";
$gl_caption['c_detail_price_weekend'] = "Noche fin de semana";
$gl_caption['c_detail_price_week'] = "Semana precio noche";
$gl_caption['c_detail_whole_week'] = "Semana precio semana";


$gl_caption['c_whole_week'] = "Semanas enteras";
$gl_caption_list['c_whole_week_0'] = "";
$gl_caption_list['c_whole_week_1'] = "Sí";

$gl_caption['c_start_day'] = "Primer dia";
$gl_caption['c_start_day_0'] = "Cualquiera";
$gl_caption_list['c_start_day_0'] = "";
$gl_caption['c_start_day_1'] = "L";
$gl_caption['c_start_day_2'] = "M";
$gl_caption['c_start_day_3'] = "X";
$gl_caption['c_start_day_4'] = "J";
$gl_caption['c_start_day_5'] = "V";
$gl_caption['c_start_day_6'] = "S";
$gl_caption['c_start_day_7'] = "D";

$gl_caption_propertyrange['c_form_title'] = "Precio periodo reserva";
$gl_messages_propertyrange['full2'] = "Ya hay definido una temporada entre las fechas: %s y %s para el inmueble \"%s\"";

$gl_caption['c_minimum_nights_error'] = "No se pueden reservar menos de %s noches en el periodo entre el %s y el %s";
$gl_caption['c_whole_week_error'] = "Se tiene que reservar semanas enteras en el periodo entre el %s i el %s";
$gl_caption['c_start_day_error'] = "Se tiene que reservar semanas enteras de %s a %s en el periodo entre el %s y el %s";
$gl_caption['c_equal_dates_error'] = "Las dos fechas no pueden ser iguales";
$gl_caption['c_no_date_selected'] = "No s'ha seleccionat cap data";



$gl_caption['c_promcode'] = 'Código promocional';
$gl_caption_promcode['c_name'] = 'Nombre';
$gl_caption_promcode['c_discount_pvp'] = 'Descuento PVP';
$gl_caption_promcode['c_discount_type'] = 'Tipo de descuento';
$gl_caption_promcode['c_discount_type_fixed'] = 'Importe fijo';
$gl_caption_promcode['c_discount_type_percent'] = 'Porcentaje';
$gl_caption_promcode['c_minimum_amount'] = 'Gasto mínimo';
$gl_caption_promcode['c_start_date'] = 'Fecha inicial';
$gl_caption_promcode['c_end_date'] = 'Fecha final';
$gl_caption_book['c_promcode_discount'] = 'Dto. código promocional';
$gl_caption['c_promcode_button'] = '';

$gl_caption_promcode['c_custumer_id'] = 'Cliente';
$gl_caption['c_custumer_caption'] = 'Todos';
$gl_caption_promcode['c_custumer_id_format'] = '%s %s %s';
$gl_caption_promcode['c_one_time_only'] = 'Aplicar sólo una vez por cliente';
$gl_caption_promcode_list['c_one_time_only'] = 'Sólo una vez';


$gl_caption_police['c_police_search_title'] = 'Reserves entre les dates';
$gl_caption['c_treatment_police'] = 'Sexo';
$gl_caption_police['c_treatment_sr'] = 'Masculino';
$gl_caption_police['c_treatment_sra'] = 'Femenino';
$gl_caption['c_download_selected'] = 'Descaregar archivo';


$gl_caption['c_tt'] =  $gl_caption['c_price_tt'] = 'Tasa turística';
$gl_caption['c_promcode_button'] = 'Ver';

$gl_caption['c_promcode_err_not_valid'] = 'El código promocional no es válido';
$gl_caption['c_promcode_err_minimum'] = 'Para aplicar el descuento la compra tiene que ser superior a %s';
$gl_caption['c_promcode_err_expired'] = 'El código promocional ha expirado';
$gl_caption['c_promcode_err_used'] = 'Este código ya se ha utilizado una vez';
$gl_caption['c_promcode_err_not_logged'] = 'Para aplicar descuento debe estar registrado';
$gl_caption['c_promcode_err_not_main_custumer'] = 'Para aplicar descuento seleccionar "Adulto 1"';

$gl_caption ['c_promcode']='Descuento código';

// buscador
$gl_caption['c_by_ref'] = '( <strong>Ref</strong>:</strong> "10 23 45" . <strong>Fecha:</strong> "12/2015" , "2015" , "22/12/2015" )';
$gl_caption['c_fields'] = "Campos";
$gl_caption['c_nights'] = "Noches";
$gl_caption['c_excel_name'] = "reservas";
$gl_caption['c_exact_date'] = "Fecha exacta";

$gl_messages ['has_no_owner']="Atención, el inmueble no tiene ningún propietario asignado";


// Pagament
$gl_caption['c_order_status'] = 'Estado de pago';
$gl_caption_list['c_order_status'] = 'Estado';
$gl_caption['c_order_status_paying'] = 'Pendiente de pago';
$gl_caption['c_order_status_failed'] = 'Pago fallido';
$gl_caption['c_order_status_pending'] = 'Pago pendiente de aceptación';
$gl_caption['c_order_status_denied'] = 'Pago rechazado';
$gl_caption['c_order_status_voided'] = 'Pago anulado';
$gl_caption['c_order_status_refunded'] = 'Pago devuelto';
$gl_caption['c_order_status_completed'] = 'Pagado';

$gl_caption['c_payment_method'] = 'Método de pago';
$gl_caption['c_payment_method_paypal'] = 'Paypal';
$gl_caption['c_payment_method_account'] = 'Transferencia bancaria';
$gl_caption['c_payment_method_directdebit'] = 'Recibo bancario';
$gl_caption['c_payment_method_lacaixa'] = 'Tarjeta de crédito';
$gl_caption['c_payment_method_none'] = '';
$gl_caption['c_payment_method_cash'] = 'Efectivo';
$gl_caption['c_payment_method_physicaltpv'] = 'TPV físico';

for ( $i = 1; $i <= 4; $i ++ ) {
	$gl_caption[ 'c_order_status_' . $i ]                    = $gl_caption['c_order_status'];
	$gl_caption_list[ 'c_order_status_' . $i ]               = $gl_caption_list['c_order_status'];
	$gl_caption[ 'c_order_status_' . $i . '_paying' ]        = $gl_caption['c_order_status_paying'];
	$gl_caption[ 'c_order_status_' . $i . '_failed' ]        = $gl_caption['c_order_status_failed'];
	$gl_caption[ 'c_order_status_' . $i . '_pending' ]       = $gl_caption['c_order_status_pending'];
	$gl_caption[ 'c_order_status_' . $i . '_denied' ]        = $gl_caption['c_order_status_denied'];
	$gl_caption[ 'c_order_status_' . $i . '_voided' ]        = $gl_caption['c_order_status_voided'];
	$gl_caption[ 'c_order_status_' . $i . '_refunded' ]      = $gl_caption['c_order_status_refunded'];
	$gl_caption[ 'c_order_status_' . $i . '_completed' ]     = $gl_caption['c_order_status_completed'];
	$gl_caption[ 'c_payment_method_' . $i ]                  = $gl_caption['c_payment_method'];
	$gl_caption[ 'c_payment_method_' . $i . '_paypal' ]      = $gl_caption['c_payment_method_paypal'];
	$gl_caption[ 'c_payment_method_' . $i . '_account' ]     = $gl_caption['c_payment_method_account'];
	$gl_caption[ 'c_payment_method_' . $i . '_directdebit' ] = $gl_caption['c_payment_method_directdebit'];
	$gl_caption[ 'c_payment_method_' . $i . '_lacaixa' ]     = $gl_caption['c_payment_method_lacaixa'];
	$gl_caption[ 'c_payment_method_' . $i . '_none' ]        = $gl_caption['c_payment_method_none'];
	$gl_caption[ 'c_payment_method_' . $i . '_cash' ]        = $gl_caption['c_payment_method_cash'];
	$gl_caption[ 'c_payment_method_' . $i . '_physicaltpv' ] = $gl_caption['c_payment_method_physicaltpv'];
}

$gl_caption ['c_books_not_deleted_not_owner'] = "Las reservas marcadas no se han borrado ya que se han efectuado desde un portal o aplicación externa";

$gl_caption['c_book_owner'] = "Reservado por";