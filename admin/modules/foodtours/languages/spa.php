<?php
// menus eina
if (!defined('FOODTOURS_MENU_TOUR')){
	define('FOODTOURS_MENU_TOUR','Tours');
	define('FOODTOURS_MENU_TOUR_NEW','Nou tours');
	define('FOODTOURS_MENU_TOUR_LIST','Llistar tours');
	define('FOODTOURS_MENU_TOUR_BIN','Paperera de reciclatge');
	define('FOODTOURS_MENU_PLACE','Llocs');
	define('FOODTOURS_MENU_PLACE_NEW','Nou lloc');
	define('FOODTOURS_MENU_PLACE_LIST','Llistar llocs');
	define('FOODTOURS_MENU_ELEMENT','Inclosos / no inclosos');
	define('FOODTOURS_MENU_ELEMENT_NEW','Nou ítem');
	define('FOODTOURS_MENU_ELEMENT_LIST','Llistar ítems');
}

// captions seccio
$gl_caption['c_subtitle'] = "Subtítol";

$gl_caption_tour['c_prepare'] = 'En preparación';
$gl_caption_tour['c_review'] = 'Para revisar';
$gl_caption_tour['c_public'] = 'Público';
$gl_caption_tour['c_archived'] = 'Archivado';
$gl_caption_tour['c_tours'] = 'Tour';
$gl_caption_tour['c_content'] = 'Descripción';
$gl_caption_tour['c_entered'] = 'Fecha creación';
$gl_caption_tour['c_modified'] = 'Fecha modificación';
$gl_caption_tour['c_status'] = 'Estado';
$gl_caption_tour['c_filter_category_id'] = 'Todas las categorias';
$gl_caption_tour['c_filter_status'] = 'Todos los estados';
$gl_caption_tour['c_filter_in_home'] = 'Todas, home o no';
$gl_caption_tour['c_ordre'] = 'Orden';
$gl_caption_tour['c_url1_name']='Nombre URL(1)';
$gl_caption_tour['c_url1']='URL(1)';
$gl_caption_tour['c_url2_name']='Nombre URL(2)';
$gl_caption_tour['c_url2']='URL(2)';
$gl_caption_tour['c_url3_name']='Nombre URL(3)';
$gl_caption_tour['c_url3']='URL(3)';
$gl_caption_tour['c_videoframe']='Vídeo (youtube, metacafe...)';
$gl_caption_tour['c_file']='Archivos';
$gl_caption_tour['c_in_home']='Sale en la Home';
$gl_caption_tour['c_in_home_0']='Sale en la Home';
$gl_caption_tour['c_in_home_1']='Sale en la Home';
$gl_caption_tour['c_content_list']='Descripció curta al llistat';
$gl_caption_tour['c_price']='Precio';
$gl_caption_tour['c_price_children']='Precio niños';
$gl_caption_tour['c_price_text']='Comentario precio';

$gl_caption_tour['c_form_level1_title_page_title']='Eines SEO';

$gl_caption_tour['c_form_level1_title_includes'] = "Inclou - exclou";
$gl_caption_tour['c_form_level1_title_bites'] = "Bites & Tastings";
$gl_caption_tour['c_form_level1_title_duration']='Tour info';
$gl_caption_tour['c_form_level1_title_price']='Preus';
$gl_caption_tour['c_form_level1_title_tours']='Descripcions';
$gl_caption_tour['c_form_level1_title_url1_name']='Enllaços i arxius adjunts';
$gl_caption_tour['c_includes'] = 'Inclou';
$gl_caption_tour['c_excludes'] = 'Exclou';
$gl_caption_tour['c_edit_dies'] = 'Dies';

$gl_caption_element['c_element'] = 'Ítem';

$gl_caption['c_ordre'] = 'Ordre';
$gl_caption['c_tour_file_name'] = $GLOBALS['gl_caption']['c_file_name'];
$gl_caption['c_tour_old_file_name'] = $GLOBALS['gl_caption']['c_old_file_name'];


// TRADUIR
$gl_caption['c_highlights'] = "Tour highlights";
$gl_caption['c_bites'] = "Bites & Tastings";
$gl_caption['c_spoken_language'] = "Idioma";
$gl_caption['c_spoken_language_eng'] = "Anglès";
$gl_caption['c_spoken_language_cat'] = "Català";
$gl_caption['c_spoken_language_spa'] = "Castellà";
$gl_caption['c_spoken_language_fra'] = "Francès";
$gl_caption['c_spoken_language_dut'] = "Holandès";
$gl_caption['c_duration'] = "Duration";
$gl_caption['c_place'] = "Lloc";
$gl_caption['c_timetable'] = "Horari";
$gl_caption['c_people'] = "Persones";

?>