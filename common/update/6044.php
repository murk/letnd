<?php

/* 6-06-15
  Canvi portfoli, afegir camps per cerca de text  HTML a TEXT

UP-DATE
Modificacions en el mòdul de portfoli per poder indexar el camp de descrpció en format html

 */

if (is_table('portfoli__portfoli_language') && !is_field('portfoli__portfoli_language','description_text')) {

	$update_sql[]="
		ALTER TABLE `portfoli__portfoli_language`
			ADD COLUMN `description_text` TEXT NOT NULL AFTER `description`;";
	$update_sql[]="
		ALTER TABLE `portfoli__portfoli_language`
			ADD INDEX `portfoli_id` (`portfoli_id`),
			ADD INDEX `language` (`language`);";


	$results = Db::get_rows("SELECT description, portfoli_id, language  FROM portfoli__portfoli_language");
	foreach ($results as $rs){
		$description_text = addslashes( rip_tags( $rs['description'] ) );
		$update_sql[]="
					UPDATE portfoli__portfoli_language
						SET description_text='" . $description_text . "'
						WHERE portfoli_id= ".$rs['portfoli_id']."
						AND language='".$rs['language']."' LIMIT 1;";
	}


}
else {
	if (is_table('portfoli__portfoli_language')) {
		trigger_error('Ja existeixen el camp description_text');
	}
}

if ( is_table( 'directory__configadmin' ) && ! Db::get_first( "SELECT * FROM directory__configadmin WHERE name='preview_all'" ) ) {
	$update_sql[]= "
		INSERT INTO directory__configadmin (`name`, `value`) VALUES ('preview_all', '0');";

} else {
	if ( is_table( 'directory__configadmin' ) ) {
		trigger_error( 'Ja existeixen el camp preview_all' );
	}
}
	
?>