<?php
$module = Module::load( 'web', 'guestbook' );

$block_max_results = isset( $vars['block_max_results'] ) ? $vars['block_max_results'] : false;
$destacat_type   = isset( $vars['destacat_type'] ) ? $vars['destacat_type'] : 'all'; // destacat, all, destacat-all


if ( $destacat_type == 'all' ) {
	$order_by  = 'entered DESC';
	$condition = "";
}
elseif ( $destacat_type == 'destacat-all' ) {
	$order_by  = 'destacat DESC, entered DESC';
	$condition = "";
}
else {
	$order_by  = 'entered DESC';
	$condition = " AND destacat = 1";
}

$module->parent = 'Block';

if ( $block_max_results ) $module->limit = '0,' . $block_max_results;
$module->order_by  = $order_by;
$module->condition = $condition;

$loop = $module->do_action( 'get_records' );

$vars += $module->caption;

if ( $loop ) {

}
else {
	$show_block = false;
}