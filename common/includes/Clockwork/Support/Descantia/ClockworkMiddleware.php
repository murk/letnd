<?php namespace Clockwork\Support\Letnd;

use Clockwork\Clockwork;
use Clockwork\DataSource\LetndDataSource;
use Clockwork\DataSource\PhpDataSource;
use Clockwork\Request\Timeline;
use Clockwork\Storage\FileStorage;
use Clockwork\Storage\SqlStorage;

class ClockworkMiddleware {
	/**
	 * Create a new middleware instance.
	 */
	public function __construct() {

	}

	static function get_clock() {

		include( DOCUMENT_ROOT . 'common/includes/Clockwork/clockwork_load.php' );


		$clockwork = new Clockwork;

		$timeline = new Timeline();
		$clockwork->addDataSource( new PhpDataSource )
		          ->addDataSource( new LetndDataSource( $timeline ) );

		if ( $GLOBALS['gl_is_letnd'] ) {
			$clockwork->setStorage( new SqlStorage(
					"mysql:dbname=$db_name;host=$db_host",
					'admintotal__clockwork',
					$db_user_name,
					$db_password )
			);
		}
		else {
			include( DOCUMENT_ROOT . 'common/includes/Clockwork/Storage/FileStorage.php' );
			$clockwork->setStorage( new FileStorage( DOCUMENT_ROOT . 'common/includes/Clockwork/temp-clockwork-data' ) );
		}
		$clockwork->setTimeline( $timeline )
		          ->startEvent( 'initialisation', 'Inici', 'start' );

		header( 'X-Clockwork-Path:' . '/common/includes/Clockwork/clockwork_letnd.php?id=' );
		header( 'X-Clockwork-Id: ' . $clockwork->getRequest()->id );
		header( 'X-Clockwork-Version: ' . Clockwork::VERSION );

		$clockwork->endEvent( 'initialisation' );

		return $clockwork;

	}
}