<?php
// no es poden posar ; entre mig de la consulta ja que parteix les consultes per el ; a no ser que fem servir $update_sql en array()

if (is_table("all__page") && !is_field('all__page','level')){		
	// Afegir subcategories de pàgines
	$update_sql .="
		ALTER TABLE `all__page` ADD `level` TINYINT( 1 ) NOT NULL DEFAULT '1' AFTER `page_id`;
		ALTER TABLE `all__page` ADD `parent_id` INT( 11 ) NOT NULL DEFAULT '0' AFTER `page_id`;
		ALTER TABLE `all__page` ADD `has_content` TINYINT( 1 ) NOT NULL DEFAULT '1' AFTER `ordre`;
		INSERT INTO `all__menu` (`menu_id`, `menu_group`, `tool`, `tool_section`, `action`, `parent_id`, `toolmode_id`, `variable`, `ordre`, `link`, `process`, `bin`, `read1`, `write1`) VALUES ('1050', 'admintotal', 'admintotal', 'page', '', '1000', '1', 'ADMINTOTAL_MENU_PAGE', '5', '', '', '0', ',1,', ',1,');
		INSERT INTO `all__menu` (`menu_id`, `menu_group`, `tool`, `tool_section`, `action`, `parent_id`, `toolmode_id`, `variable`, `ordre`, `link`, `process`, `bin`, `read1`, `write1`) VALUES ('1051', 'admintotal', 'admintotal', 'page', 'show_form_new', '1050', '1', 'ADMINTOTAL_MENU_PAGE_NEW', '1', '', '', '0', ',1,', ',1,');
		INSERT INTO `all__menu` (`menu_id`, `menu_group`, `tool`, `tool_section`, `action`, `parent_id`, `toolmode_id`, `variable`, `ordre`, `link`, `process`, `bin`, `read1`, `write1`) VALUES ('1052', 'admintotal', 'admintotal', 'page', 'list_records', '1050', '1', 'ADMINTOTAL_MENU_PAGE_LIST', '2', '', '', '0', ',1,', ',1,');
		INSERT INTO `all__menu` (`menu_id`, `menu_group`, `tool`, `tool_section`, `action`, `parent_id`, `toolmode_id`, `variable`, `ordre`, `link`, `process`, `bin`, `read1`, `write1`) VALUES ('1053', 'admintotal', 'admintotal', 'page', 'list_records_bin', '1050', '1', 'ADMINTOTAL_MENU_PAGE_BIN', '3', '', '', '0', ',1,', ',1,');";
}
if (is_table("all__block") && !is_field('all__block','vars')){		
	// Afegir subcategories de pàgines
	$update_sql .="
		ALTER TABLE `all__block` ADD `vars` VARCHAR(50) DEFAULT '' AFTER `script` ;";
}
?>