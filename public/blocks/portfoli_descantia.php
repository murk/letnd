<?php

global $gl_page;

$page_id = $gl_page->page_id;

if (!$page_id) return false;


$query = "SELECT portfoli__portfoli.portfoli_id as portfoli_id, 
				title, 
				subtitle
				FROM portfoli__portfoli_language, portfoli__portfoli
				WHERE portfoli__portfoli_language.portfoli_id = portfoli__portfoli.portfoli_id
				AND (status = 'public')
				AND bin <> 1
				AND language = '" . LANGUAGE . "'
				AND portfoli__portfoli.portfoli_id IN (SELECT portfoli__portfoli_to_page.portfoli_id FROM portfoli__portfoli_to_page WHERE page_id = ". $page_id.")
				ORDER BY ordre ASC, title";

$results = Db::get_rows($query);

if ($results) {

	$conta = 0;
	$loop_count = count($results);
	foreach ($results as $rs) {
		$l = array();


		
		$l['link'] = $l['show_record_link'] = '/' . LANGUAGE . '/portfoli/portfoli/' . $rs['portfoli_id'] . '/';			
		
		
		$query = "SELECT page_file_name FROM all__page INNER JOIN all__page_language USING(page_id) WHERE language='".LANGUAGE."' AND page_text_id = 'portfoli'";
		
		$page_name = Db::get_first($query);
		if ($page_name) $l['link'] .= $page_name . '.html';			
		
		$l['title'] = $rs['title'];
		$l['subtitle'] = $rs['subtitle'];
		$l['category_id'] = 'Categoria';
		$l['id'] = $rs['portfoli_id'];
		$l['conta'] = $conta++;
		$l['class'] = '';
		$l += UploadFiles::get_record_images($rs['portfoli_id'], true, 'portfoli', 'portfoli');
		$l['loop_count'] = $loop_count;
		
		$loop[] = $l;
	}
	$loop[0]['class'] = 'first';
	$loop[count($loop) - 1]['class'] = 'last';
	
}
else {
	/* si no hi ha cap resultat no mostro el block */
	$show_block = false;
	return;
}
?>