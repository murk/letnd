<?if (!defined('PRODUCT_DISCOUNT_MAX_RESULTS')) Module::get_config_values('product');?>
<article>
  <h2>Pour acheter à <?=COMPANY_NAME?>,vous pouvez acheter à travers:
  </h2>
  <p><strong>Paiement par Paypal:</strong><br />
  Payer avec la carte de crédito u par virement bancaire sur votre compte Paypal, est plus rapide et plus élargie</p>
  
  <p><strong>Paiement par carte de crédit ou de paiement:</strong><br />
  Vous pouvez utiliser votre carte de crédit ou de paiement, Visa ou Mastercard</p>
  
  <p><strong>Paiement à la livraison: </strong><br />
  Vous payez au moment de la réception de votre commande. 
  </p>
  
  <p><strong>Par virement bancaire:</strong><br />
  Vous effectuez un virement bancaire à: <?=PRODUCT_BANC_NAME?>, numéro de compte: <?=PRODUCT_ACCOUNT_NUMBER?>, indiqué au total, et une fois confirmé son transfert <?=COMPANY_NAME?> enverra son commande.
  </p>
</article>