<?

/* 02-11-15

	Actualitzaciˇ historial de PREUS inmo

 */
if ( is_table( 'custumer__history' ) ) {

	$update_sql [] = "ALTER TABLE `custumer__history`
	CHANGE COLUMN `tipus` `tipus` ENUM('various','to_call','waiting_call','assembly','show_property','like_property','send_information','demand_refused','demand_reseted','office','offer','price_changed') NOT NULL COLLATE 'utf8_spanish2_ci' AFTER `finished_date`;";

	$update_sql [] = "ALTER TABLE `custumer__history`
		CHANGE COLUMN `custumer_id` `custumer_id` INT(11) NOT NULL DEFAULT '0' AFTER `history_id`,
		CHANGE COLUMN `history_title` `history_title` VARCHAR(255) NOT NULL DEFAULT '' COLLATE 'utf8_spanish2_ci' AFTER `tipus`,
		CHANGE COLUMN `bin` `bin` INT(11) NOT NULL DEFAULT '0' AFTER `client_id`;";

	$update_sql [] = "ALTER TABLE `custumer__history`
							ADD INDEX `custumer_id` (`custumer_id`),
							ADD INDEX `property_id` (`property_id`),
							ADD INDEX `message_id` (`message_id`),
							ADD INDEX `demand_id` (`demand_id`),
							ADD INDEX `client_id` (`client_id`);";

	$update_sql [] = "INSERT INTO custumer__history (
				`date`, `status`, `finished_date`, `tipus`, `history_title`, `property_id`)
				SELECT entered, 'finished', entered, 'price_changed', price, property_id FROM inmo__property WHERE price !=0";

}

?>