<div class="intro">
	<div class="introleft">
    <p>Des del dia 2 de gener ha obert les portes <strong>MIRONAUTO</strong> ,un nou taller de reparacions de turismes, furgonetes, vehicles comercials i camions (fins a 5.000 kg de tara),  amb un equip professional i preparat per a les noves tecnologies, on rebreu un tracte personalitzat digne de les vostres expectatives.<br />
      Les nostres instal&middot;lacions s&oacute;n modernes i respectuoses amb el medi ambient. Fem qualsevol tipus de reparaci&oacute;, ja sigui mec&agrave;nica en general, planxa i pintura, electricitat (diagnosi amb ordinador, comprovacions de circuit de c&agrave;rrega de bateries, servei de diagnosi i c&agrave;rrega d&rsquo;aire acondicionat...)</p>    
  </div>
	<div class="introtextv">
<h2>NO SÓM UN ALTRE TALLER, SOM EL TEU TALLER.</h2>
</div>
</div>
