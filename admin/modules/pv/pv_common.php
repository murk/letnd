<?

/**
 * PvCommon
 *
 * Funcions comuns per PV
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2011
 * @version $Id$
 * @access public
 */
class PvCommon {

	static public function set_security( &$module ) {

		$module->group_id      = $_SESSION['group_id'];
		$module->user_is_admin = $module->group_id < 3; // grups 2 i 1
		// els usuaris es el grup 3, no son ni admin ni client
		$module->user_is_client = $module->group_id == 4; // grups 4

		if ( $module->user_is_client ) {
			$module->client_id = Db::get_first( 'SELECT client_id FROM user__user WHERE user_id = ' . $_SESSION['user_id'] );
		}

	}

	static public function check_client_id( &$module ) {

		// A imatges no s'ha de posar client, sempre va relacionat amb una taula que ja te client_id

		$call   = R::text_id( 'call' );
		$action = $module->action;

		if (
			$call
			|| $action == 'list_records_images'
			|| $action == 'save_rows_get_images'
			|| $action == 'save_rows_save_records_images'
			|| $action == 'save_rows_delete_selected_images'
			|| $action == 'show_uploader_images'
		) {
			return;
		}

		// Si es client no cal, ja el tinc no haig de triar res
		if ( ! empty ( $module->needs_client_id ) ) {


			// Miro el client_id
			if ( $module->user_is_client ) {
				$client_id = $module->client_id;
			} else {

				$client_id = R::id( 'client_id' );
				if ( ! $client_id ) {
					$client_id = R::id( 'client_id', false, '_POST' );
				}

			}

			// Si hi ha client_id per get o post o user_is_client, configuro el mòdul
			if ( $client_id ) {
				$module->client_id = $client_id;

				$client = Db::get_first( "SELECT client FROM pv__client WHERE client_id = '$client_id';" );

				$no_client_uri = $_SERVER["REQUEST_URI"];
				$no_client_uri = str_replace( '&client_id=' . $client_id, '', $no_client_uri );
				$no_client_uri = str_replace( '?client_id=' . $client_id, '', $no_client_uri );

				if ( $module->user_is_client ) {
					$change_client_link = '';
				} else {
					$change_client_link = '<a class="botoCanviarClient" href="' . $no_client_uri . '">' . $module->caption ['c_canviar_client'] . '</a>';

					// afegeixo client a tots el menus ( de moment va a tots, si no hauria de canviar el page_admin.php
					Page::add_menu_params( 'client_id=' . $client_id );
				}


				$GLOBALS ['gl_page']->subtitle = $client . $change_client_link;


				// poso que es guardi automàticament al client
				if ( $module->action == 'add_record' ) {
					// $module->add_field( 'client_id' );
					// $module->set_field( 'type', 'hidden', $client_id );
					$module->set_field( 'client_id', 'override_save_value', $client_id );
				}

				$GLOBALS ['gl_page']->javascript .= "pv.client_id = $client_id;";
			}

			// Sino mostro pantalla per triar
			// I si estic en un save_frame?
			else {

				$module->action = false;

				$module_client         = Module::load( 'pv', 'client' );
				$GLOBALS['gl_content'] = $module_client->select_client();

			}

		}
	}

	static function add_page_files( &$module ) {

		if ($module->is_index) {
			Page::add_javascript_file( '/admin/modules/pv/jscripts/functions.js' );
			Page::add_css_file( '/admin/themes/inmotools/pv/styles.css' );
		}

		if ( $GLOBALS['gl_tool'] == 'pv' && $GLOBALS['gl_tool_section'] == 'doc' ) {
			Page::add_css_file( '/clients/consultoriapvgestio/templates/styles/editor.css' );
		}

		if ( $GLOBALS['gl_tool'] == 'pvgmao' ){
			$GLOBALS['gl_page']->javascript .= "gl_pvgmao_see_estat_history = \"" . addslashes( $module->caption['c_pvgmao_see_estat_history'] ) . "\";";
		}

	}


	static public function set_client_menus( &$module ) {

		if ( ! $module->user_is_client ) {
			return;
		}

		$client = Db::get_row( 'SELECT has_seguiment,has_adr, has_gmao FROM pv__client WHERE client_id = ' . $module->client_id );

		$page = &$GLOBALS['gl_page'];

		if ( ! $client['has_seguiment'] ) {
			$page->delete_menu( 2300 );
		}
		if ( ! $client['has_adr'] ) {
			$page->delete_menu( 2600 );
		}
		if ( ! $client['has_gmao'] ) {
			$page->delete_menu( 2700 );
		}

		$menu_id = $GLOBALS['gl_menu_id'];
		do {
			$query = "SELECT menu_id, parent_id FROM all__menu WHERE menu_id = $menu_id";
			$rs    = Db::get_row( $query );

			$last_menu_id = $menu_id;
			$menu_id      = $rs['parent_id'];
		} while ( $menu_id != 0 );


		$menu_id       = $last_menu_id;
		$redirect_menu = 0;

		if ( ! $client['has_seguiment'] && ! $client['has_adr'] && ! $client['has_gmao'] ) {
			$GLOBALS['gl_content'] = '<div id="message_container"><div id="message"> ' . $module->messages['concession_no_acces'] . '</div></div>';
			$module->action        = false;

			return;
		}

		// Vaig per ordre, primer es pot redirigir a seguiment 2300, després a adr 2600 i per últim a gmao 2700

		$posible_parent = 0;
		if ($client['has_gmao']) $posible_parent = 2700;
		if ($client['has_adr']) $posible_parent = 2600;
		if ($client['has_seguiment']) $posible_parent = 2300;


		if ( ! $client['has_seguiment'] && $menu_id == 2300 ) {

			$redirect_menu = Db::get_first( "SELECT link FROM all__menu WHERE menu_id = $posible_parent" );
		}
		if ( ! $client['has_adr'] && $menu_id == 2600 ) {

			$redirect_menu = Db::get_first( "SELECT link FROM all__menu WHERE menu_id = $posible_parent" );
		}
		if ( ! $client['has_gmao'] && $menu_id == 2700 ) {

			$redirect_menu = Db::get_first( "SELECT link FROM all__menu WHERE menu_id = $posible_parent" );
		}

		if ( $redirect_menu ) {
			Main::redirect( "/admin/?menu_id=$redirect_menu" );
		}

	}


	/*
	 * Generar pròxim manteniment a partir de la data de realització
	 * Si no s'ha posat cap data de realització, ja no genero cap més
	 */
	static public function generate_manteniments( &$module ) {

		if ($module->tool_section != 'mantenimentprogramat') return;

		// Miro per periodes de DIES
		$results = Db::get_rows( "
			SELECT maquinaitem_id, maquina_id, manteniment_id, cada, periode, data_inici, operari_id 
				FROM pvgmao__maquinaitem 
				ORDER BY maquina_id ASC, data_inici ASC
			" );


		foreach ( $results as &$rs ) {

			$operari_id = $rs['operari_id'];
			$data_inici     = $rs['data_inici'];
			$cada           = $rs['cada'];
			$periode        = $rs['periode'];
			$maquina_id     = $rs['maquina_id'];
			$manteniment_id = $rs['manteniment_id'];
			$maquinaitem_id = $rs['maquinaitem_id'];

			$next_date = true;

			// Amb el while poso manteniments passats que s'haurien d'haver fet abans ( va be pel primer cop que faig servir l'eina, després no se si val la pena tindre-ho
			while ( $next_date ) {

				$next_date = self::get_next_manteniment_dates( $data_inici, $cada, $periode, $maquinaitem_id );
				$rs['next_date'] = $next_date;
				$client_id = Db::get_first( "SELECT client_id FROM pvgmao__maquina WHERE maquina_id = $maquina_id" );
				// Ara ho  agafo del maquinaitem $operari_id = Db::get_first( "SELECT operari_id FROM pvgmao__maquina WHERE maquina_id = $maquina_id" );

				if ( $next_date ) {
					$sql_insert = "
					INSERT INTO pvgmao__mantenimentprogramat
					 (maquina_id, maquinaitem_id, manteniment_id, operari_id, data_previst, client_id) 
					VALUES 
					($maquina_id, $maquinaitem_id, $manteniment_id, $operari_id, '$next_date', $client_id);";
					Db::execute( $sql_insert );
				}

				// $next_date = false; // desconecto el while, un cop està tot rodat
			}

		}

	}

	// S'ha de fer un cronjob perque no quedi desfassat
	static private function get_next_manteniment_dates( $data_inici, $cada, $periode, $maquinaitem_id ) {

		// Comprovo si s'ha generat mai
		$query = "
				SELECT count(*)
				FROM pvgmao__mantenimentprogramat
				WHERE maquinaitem_id = $maquinaitem_id
				LIMIT 1";
		$its_virgin = ! Db::get_first( $query );


		// Si no s'ha generat mai, genero amb data_inici
		// No tinc en compte si es inferior a data d'avui, si he posat una data anterior poder ho he fet expressament per generar un manteniment passat. Per defecte al guardar màquina, es posa dia d'avui, i llavors ja es genera correctament
		if ( $its_virgin ) return $data_inici;

		// busco l'últim manteniment pendent
		$query = "
				SELECT count(*)
				FROM pvgmao__mantenimentprogramat
				WHERE maquinaitem_id = $maquinaitem_id	
				AND (data_realitzat IS NULL || data_realitzat = '0000-00-00')		
				ORDER BY data_previst DESC, mantenimentprogramat_id DESC
				LIMIT 1";
		$has_pending    = Db::get_first( $query );

		// Si hi ha algún pendent, no genero
		if ( $has_pending ) return false;

		// busco l'últim manteniment pendent
		$query = "
				SELECT data_previst, data_realitzat
				FROM pvgmao__mantenimentprogramat
				WHERE maquinaitem_id = $maquinaitem_id	
				ORDER BY data_realitzat DESC, mantenimentprogramat_id DESC
				LIMIT 1";
		$rs    = Db::get_row( $query );

		$data_realitzat = $rs['data_realitzat'];

		$data_realitzat_dt = new DateTime($data_realitzat);
		// Genero la pròxima feina segons la data_realitzat
		if ( $periode == 'dia' ) {
			$data_realitzat_dt->add(new DateInterval("P${cada}D"));
		} elseif ( $periode == 'mes' ) {
			$data_realitzat_dt->add(new DateInterval("P${cada}M"));
		}
		$next_date = date_format($data_realitzat_dt, 'Y/m/d');

		return $next_date;
	}

	static public function get_consultor(){
		return empty( $GLOBALS['gl_pv_consultor'] ) ? 'consultoriapvgestio' : $GLOBALS['gl_pv_consultor'];
	}

}