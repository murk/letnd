function write_report(message, reset) {

	if (typeof(reset) === 'undefined') reset = false;

	var report = document.getElementById('report');

	if (reset) report.innerHTML = "";

	report.innerHTML = report.innerHTML + '<br>' + message;

	report.scrollTop = report.scrollHeight;
}

function show_not_sent(message_id, e){
	e = window.event || e;
	e.stopPropagation ? e.stopPropagation() : (e.cancelBubble=true);
	
	showPopWin('/admin/?template=window&menu_id=11&action=list_records_mail_not_sent&message_id='+message_id, 950, 530, 'newsletter_reload_page', true, false, '');	
}

function delete_not_sent_address (id, message_id){
	if (confirm(MESSAGE_CONFIRM_DELETE_NOT_SENT))
		window.save_frame.location.href = '/admin/?action=write_record&menu_id=11&process=delete_not_sent&custumer_id='+id + '&message_id='+message_id;
}
function newsletter_reload_page(){
	top.window.location.reload();
}