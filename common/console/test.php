<?

namespace Console\Command;

use Console;
use Query;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;


final class Test extends Command {

	private $output;
	private $input;

	protected function configure() {
		$this->setName( 'test' )
		     ->setDescription( 'Executar proves' );
	}

	protected function execute( InputInterface $input, OutputInterface $output ) {
		$this->input  = $input;
		$this->output = $output;
		$io           = new SymfonyStyle( $input, $output );


		$this->prova( $io );

		// return value is important when using CI
		// to fail the build when the command fails
		// 0 = success, other values = fail

		return 0;
	}

	private function prova( SymfonyStyle $io ) {
		$q = new Query ( 'inmo__property_image' );
		$io->block( $q->id_field );
		setcookie('first_time', 'si');
	}
}