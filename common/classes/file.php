<?php
/**
 * File
 * // Nomes llegir
 * $content = File::read($file);
 * // Nomes escriure
 * File::write($file, $content);
 *
 * // Llegir i escriure
 * $file = new File($file);
 * $file->content =  $file->content . $add_content;
 * $file->write();
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2010
 * @version $Id$
 * @access public
 */
class File{
	var $content='';
	var $file;
	var $size;
	function __construct($file=''){
		if ($file) $this->read($file);
	}
	function read($file)
	{
		if (!isset($this))
		{
			return file_get_contents($file);
		}
		else
		{
			if (!is_file($file)){
				file_put_contents($file, '');
			}
			$this->size = filesize($file);
			$this->file = $file;
			$this->content = file_get_contents($file);
			return $this->content;
		}
	}

	function write($content=false, $content_file=false)
	{
		// si hi ha el segon argument, intercambio les variables
		// així puc cridar la funció: write($content) o write($file,$content)
		if (!isset($this)) $t = new File(); else $t = &$this;
		if ($content_file) {
			$t->file = $content;
			$content = $content_file;
		}
		$source_file = fopen($t->file, "w");
		fwrite ($source_file, $content?$content:$t->content);
		fclose($source_file);
	}
}
?>