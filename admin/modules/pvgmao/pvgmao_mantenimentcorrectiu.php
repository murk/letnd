<?
/**
 * PvgmaoMantenimentcorrectiu
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2011
 * @version $Id$
 * @access public
 */
class PvgmaoMantenimentcorrectiu extends Module{

	var $needs_client_id = true;
	var $group_id, $user_is_admin, $user_is_client, $client_id;

	public function __construct(){
		parent::__construct();
	}

	function on_load() {
		parent::on_load();
		$this->set_field( 'maquina_id', 'select_condition', 'client_id=' . $this->client_id . ' ORDER BY maquina' );
		$this->set_field( 'operari_id', 'select_condition', 'client_id=' . $this->client_id . ' ORDER BY nom, cognoms' );

	}
	public function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	public function get_records()
	{
	    $listing = new ListRecords($this);

		$listing->add_filter( 'maquina_id' );
		$listing->add_filter( 'operari_id' );

		$listing->condition = "client_id = " . $this->client_id;

		$listing->order_by = 'data_realitzat DESC';

	    return $listing->list_records();
	}
	public function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	public function get_form()
	{
	    $show = new ShowForm($this);

		$show->condition = "client_id = " . $this->client_id;

	    return $show->show_form();
	}

	public function save_rows()
	{
	    $save_rows = new SaveRows($this);
	    $save_rows->save();
	}

	public function write_record()
	{
	    $writerec = new SaveRows($this);
	    $writerec->save();
	}

	public function manage_images()
	{
	    $image_manager = new ImageManager($this);
	    $image_manager->execute();
	}
}