$(document).ready(
	function () {
		/*$('select')
		 .wrap(document.createElement("span"))
		 .bind("focus", function(e){
		 $(this).parent().addClass('focus');
		 })
		 .bind("blur", function(e){
		 $(this).parent().removeClass('focus');
		 });*/

		$('#opcions select').customSelect();
		if ($.cookie('is_menu_collapsed') == 'true') collapse_menu_onload();
		if (document.theForm) {
			form_init_validations();
			form_on_change();
		}

		$(window).resize(function () {
			fixa_menus(true);
		});
		$(window).on("load", function () {
			fixa_menus(true);
		});


	}
);

function form_init_validations() {

	jQuery.validator.addMethod(
		"vdate",
		function (value, element) {
			return  this.optional(element) || Date.parseExact(value, "dd/MM/yyyy");
		},
		$.validator.messages.date
	);
	jQuery.validator.addMethod(
		"vtime",
		function(value, element) {
			return this.optional(element) || /^([01]\d|2[0-3])(:[0-5]\d){1,2}$/.test(value);
		},
		$.validator.messages.time
	);
	jQuery.validator.addMethod(
		"vminutes",
		function(value, element) {
			return this.optional(element) || /^([0-5]\d)(:[0-5]\d){1,2}$/.test(value);
		},
		$.validator.messages.time
	);

	$('form[name="theForm"]')
		.validate({
			//rules: rules,
			//tooltip_options: {
			//	'_all_': {placement: 'right'}
			//},
			//submitHandler: function (form) {
				// així no fa mai un submit
			//},

			invalidHandler: function (form, validator) {
				setTimeout(
					function () {
						$(".tooltip").removeClass('in');
					}, 700);
			}
		});

}
var gl_theform_serialized;
function form_on_change(){
	//dp('es form');

	gl_theform_serialized = $('form[name="theForm"]').serialize();
	
	$('form[name="theForm"]').submit(function(){form_set_serialized();});
	
	$('a:not([href^="javascript:"][target!="_self"])').click(function(){
		if (this.href && !this.target && $('form[name="theForm"]').length) {

			return check_form_change();

		}
		
		return true;//confirm(this.target);
	})
}
function check_form_change () {

	if (gl_theform_serialized!= $('form[name="theForm"]').serialize()) {
		var save =  confirm('No has guardat els canvis, desitges guardar-los?');
		if (save) {
			$('form[name="theForm"]').submit();
			form_set_serialized();
			return false;
		}
	}
	return true;
}
function form_set_serialized(){
	gl_theform_serialized = $('form[name="theForm"]').serialize();	
}
function fixa_menus(set_scroll){

			// Fixa menus
						
			var msie8 = $.browser.msie && $.browser.version < 9;
			var top_opcions = false;
			var top_menu = false;
			var top_buttons = false;
			var w_width = $(window).width();
			
			if (typeof(gl_action)=='undefined') gl_action = '';
			var is_form = gl_action.indexOf('show_form')!=-1;
			// nomès per plantilla general
			if (!msie8 && ($('#menu').length)) {
			
				var menu_width = ($('#menu_holder').outerWidth());
				
				// OPCIONS - Filtres i botons
				if ($('#opcions_holder').length){
					top_opcions = $('#opcions_holder').offset().top;
					var options_h = $('#opcions').height();
					var split_h = $('#split_holder').height();
					var fixed_bar_position = $.cookie('fixed_bar_position')?$.cookie('fixed_bar_position'):'middle';

					if (!is_form){
					
						$('#opcions').css('left',($('#menu_holder').outerWidth())+'px');
						$('#opcions').css('width',(w_width-menu_width)+'px');
						$('#opcions_holder').height($('#opcions').innerHeight());
						
						$('#split_holder').height($('#split').innerHeight());
						$('#split').css('left',menu_width+'px');
						$('#split').css('width',(w_width-menu_width)+'px');
						
					}
					
					
					
					// ORDRE copia
					if ($('.listRecord thead').length && !is_form){	
					
						$('.listRecord thead tr td').css('width','auto');
						$('.listRecord thead tr td').each(function() {
							$(this).css('width',($(this).outerWidth()));
						});
						
						var show = $('#opcions_order_fixed').length?$('#opcions_order_fixed').css('display'):'none';
						$('#opcions_order_fixed').remove();
						var table = $('<table id="opcions_order_fixed" width="100%" cellspacing="0" cellpadding="0" border="0"><thead></thead></table>');
						$('body').append(table);
						table.css('display',show);
						table.width($('.listRecord').width());
						$('#opcions_order_fixed thead').html($('.listRecord thead').html());
						table.css('left',($('#menu_holder').outerWidth())+'px');
					}
					
					
					var fixed_bar_bottom = options_h-15;//$('#opcions_fixed_bar .more').height();
					
					if (fixed_bar_position=='top') {
						top_opcions -= split_h;
						$('#opcions').css('top',split_h +'px');								
						$('#opcions_fixed_bar .more').addClass('down');			
						if (table) table.css('top',(options_h+split_h) +'px');	
					}
					else if (fixed_bar_position=='middle'){
						$('#split').css('top','-' + split_h +'px');			
						if (table) table.css('top',options_h +'px');			
					}
					else if (fixed_bar_position=='bottom'){
						top_opcions += fixed_bar_bottom;
						$('#opcions').css('top','-' + fixed_bar_bottom +'px');	
						$('#split').css('top','-' + split_h +'px');			
						if (table) table.css('top',(options_h-fixed_bar_bottom) +'px');		
					}
					
					// boto opcions
					$('#opcions_fixed_bar').click(function(){
					
						if (fixed_bar_position=='top'){
							fixed_bar_position = 'middle';
							
							$('#opcions').animate({top:'0'}, 300);
							$('#split').animate({top:'-' + split_h+'px'}, 300);						
							table.animate({top:options_h+'px'}, 300);
							
							$('#opcions_fixed_bar .more').removeClass('down');
							
							top_opcions += split_h;	
						}
						else if (fixed_bar_position=='middle'){
							fixed_bar_position = 'bottom';
							
							$('#opcions').animate({top:'-'+fixed_bar_bottom+'px'}, 300);					
							table.animate({top:(options_h-fixed_bar_bottom)+'px'}, 300);
							
							top_opcions += fixed_bar_bottom;
						}
						else if (fixed_bar_position=='bottom'){
							fixed_bar_position = 'top';
							
							$('#opcions').animate({top:split_h+'px'}, 300);
							$('#split').animate({top:'0'}, 300);					
							table.animate({top:(options_h+split_h)+'px'}, 300);							
							$('#opcions_fixed_bar .more').addClass('down');
							
							top_opcions -= split_h+fixed_bar_bottom;
						}
						
						$.cookie('fixed_bar_position',fixed_bar_position);
					});
					
				}
				
				// MENU DRETA
				if ($('#menu').length){
					$('#menu, #menu_holder').width($('#menu').innerWidth());
					top_menu = $('#menu_holder').offset().top;
					var top_footer_bottom = $('#footer_bottom').offset().top;
					var top_menu_diff = 0;
					if ($(window).height() < $('#menu').height()){
						top_menu_diff =  $('#menu').height()-$(window).height();
						top_menu = top_menu + top_menu_diff;
						$('#menu').css('top','-'+top_menu_diff+'px');
					}
					
					//dp('top_menu_diff' , top_menu_diff);
					//dp('Top menu' , top_menu);
					$('#menu_holder').width($('#menu_holder').width()); // fixo l'ample
					$('#menu_holder').height($('#menu_holder').innerHeight());
					$('#menu_holder2').height($('#menu_holder').innerHeight());
					$('#menu').width($('#menu_holder').innerWidth()); // fixo l'ample	
				}				
				
				// OPCIONS - Formularis
				if ($('#form_buttons_holder').length){				
					top_buttons =  $('#form_buttons_holder').offset().top;

					$('#form_buttons_holder').css('height',$('#form_buttons_holder').css('height')); 
					
					$('#form_buttons').width((w_width-menu_width)+'px'); // +1 bug ie

					//dp('Top buttons' , top_buttons);
					//dp('Top buttons width' , $('#form_buttons').width());
				}
				
				if (!set_scroll) return;
				
				// FIXAR SCROLL
				$(window).scroll(function (event) {
					var y = $(this).scrollTop();
					var x = $(this).scrollLeft();
					
					if (y >= top_opcions && top_opcions!==false) {
						$('#opcions, #split').addClass('fixed');
						$('#opcions_fixed_bar').show();
					} else {
						$('#opcions, #split').removeClass('fixed');
						$('#opcions_fixed_bar').hide();
					}
					if (y >= (top_opcions+20) && top_opcions!==false) {
						$('#opcions_order_fixed').show();
					} else {
						$('#opcions_order_fixed').hide();
					}
					
					if (y >= top_menu) {					
						$('#menu').addClass('fixed');						
						
						var bottom_diff = y+$('#menu').height()-top_menu_diff-top_footer_bottom;
						if (bottom_diff>0) {
							$('#menu').addClass('fixed-bottom');
							$('#menu').removeClass('fixed-left');
							$('#menu').css('top','auto');
						}
						else{
							$('#menu').removeClass('fixed-bottom')
							if (x){
								$('#menu').css('top', (y - top_menu) + 'px');	
								$('#menu').addClass('fixed-left');
							}
							else{
								$('#menu').removeClass('fixed-left');
								$('#menu').css('top','-'+top_menu_diff+'px');
							}
						}
						
						
					} else {
						$('#menu').removeClass('fixed');
					}
					
					
					
					if (y >= top_buttons && top_buttons!==false) {
						$('#form_buttons').addClass('fixed');
					} else {
						$('#form_buttons').removeClass('fixed');
					}
				});
			}
}
function collapse_menu(){
	if (gl_is_menu_collapsed){
		$('#menu_arrow').removeClass('m_arrow_down');
		$('#menu_arrow').addClass('m_arrow');
		$('#menu, #menu_holder, #menu_holder2').width(213);
		$('#menu_content').show();
		//$('#opcions, #split, #opcions_holder, #split_holder, #opcions_order_fixed').width($(window).width()-214);
		//$('#opcions, #split').css('left','214px');
	}
	else{
		$('#menu_content').hide();
		$('#menu_arrow').removeClass('m_arrow');
		$('#menu_arrow').addClass('m_arrow_down');
		$('#menu_holder').css('min-width','15px');
		$('#menu, #menu_holder, #menu_holder2').width(15);
		//$('#opcions, #split, #opcions_holder, #split_holder, #opcions_order_fixed').width($(window).width()-16);
		//$('#opcions, #split').css('left','16px');
	}
	fixa_menus(false);
	
	$.cookie('is_menu_collapsed',!gl_is_menu_collapsed);	
	gl_is_menu_collapsed = !gl_is_menu_collapsed;
	/*$('#menu').animate({
    width: '10px'
	}, 5000, function() {
    // Animation complete.
	});*/
}
function collapse_menu_onload(){
	$('#menu_content').hide();
	$('#menu_arrow').removeClass('m_arrow');
	$('#menu_arrow').addClass('m_arrow_down');
	$('#menu_holder').css('min-width','15px');
	$('#menu, #menu_holder, #menu_holder2').width(15);
	gl_is_menu_collapsed = true;
}
gl_is_menu_collapsed = false;

// selects
/*
 * This is based on ideas from a technique described by Alen Grakalic in
 * http://cssglobe.com/post/8802/custom-styling-of-the-select-elements
 */
(function($) {
	$.fn.customSelect = function(settings) {
		var config = {
			customSelectClass: 'custom-select', // Class name of the (outer) inserted span element
			activeClass: 'active', // Class name assigned to the fake select when the real select is in hover/focus state
			wrapperElement: '<div class="custom-select-container" />' // Element that wraps the select to enable positioning
		};
		if (settings) {
			$.extend(config, settings);
		}
		this.each(function() {
			var select = $(this);
			select.wrap(config.wrapperElement);
			var update = function() {
				val = $('option:selected', this).text();
				span.find('span span').text(val);
			};
			// Update the fake select when the real select value changes
			select.change(update);
			/* Gecko browsers don't trigger onchange until the select closes, so
			 * changes made by using the arrow keys aren't reflected in the fake select.
			 * See https://bugzilla.mozilla.org/show_bug.cgi?id=126379.
			 * IE normally triggers onchange when you use the arrow keys to change the selected
			 * option of a closed select menu. Unfortunately jQuery doesnt seem able to bind to this.
			 * As a workaround the text is also updated when any key is pressed and then released
			 * in all browsers, not just in Firefox.
			 */
			select.keyup(update);
			/* Create and insert the spans that will be styled as the fake select
			 * To prevent (modern) screen readers from announcing the fake select in addition to the real one,
			 * aria-hidden is used to hide it.
			 */
			// Three nested spans? The only way I could get text-overflow:ellipsis to work in IE7.
			var span = $('<span class="' + config.customSelectClass + '" aria-hidden="true"><span><span>' + $('option:selected', this).text() + '</span></span></span>');
			select.after(span);
			// Change class names to enable styling of hover/focus states
			select.bind({
				mouseenter: function() {
					span.addClass(config.activeClass);
				},
				mouseleave: function() {
					span.removeClass(config.activeClass);
				},
				focus: function() {
					span.addClass(config.activeClass);
				},
				blur: function() {
					span.removeClass(config.activeClass);
				}
			});
		});
	};
})(jQuery);



function ewe_onload(){}

function submit_theForm()
{
}
var ewe = new Array();
var ew = new Array();
var editorPath = '/admin/jscripts/wysiwyg/';
//document.write ('<script language="JScript" src="jscripts/wysiwyg/source/ewe.js"><\/script>');

function showPopWin(sUrl, nW, nH, returnFunc, showCloseBox, bTitleBar, sMessage, bFrameScroll, sContingut, sWUnits, sHUnits)
{
	$("#imagesFrame").css("visibility","hidden"); // applet
	if (sWUnits == '%'){
		nW = $(window).width()*nW/100;
	}
	
	if (sHUnits == '%'){
		nH = $(window).height()*nH/100;
	}
	if (returnFunc)
		returnFunc = "'" + returnFunc + "'";
	else
		returnFunc = 'false';
	
	bFrameScroll = bFrameScroll?'yes':'no';
	sFrame = '<iframe id="popupFrame" src="'+sUrl+'" scrolling="'+bFrameScroll+'" frameborder="0" name="popupFrame" style="width: '+nW+'px; height: '+nH+'px; background-color: #fff;" hspace="0"></iframe>';
	sContent = sUrl?sFrame:'<table class="popupMessage" border="0" cellspacing="0" cellpadding="0" width="'+nW+'" height="'+nH+'"><tr><td valign="middle" align="center" class="modalMessage">' + sMessage + '</td></tr></table>';
	sContent += showCloseBox?'<a class="modalCloseImg" title="'+MODALBOX_CLOSE+'" onclick="hidePopWin('+ returnFunc+');">'+MODALBOX_CLOSE+'</a>':'';
	if (sMessage){
		nFadeIn=0;
		$.blockUI.defaults.fadeOut = 200;
	}
	else{
		nFadeIn=500;
		$.blockUI.defaults.fadeOut = 200;
	}


	$.blockUI({
		message: sContent,
		fadeIn: nFadeIn,
		css: {
			top:  ($(window).height() - nH) /2 + 'px',
			left: ($(window).width() - nW) /2 + 'px',
			padding: '0',
			margin: '0',
			width:nW+'px',
			height:nH+'px',
			backgroundColor:"#fff"
		},
		overlayCSS:  {
				backgroundColor: '#000',
				opacity:         0.3
			}

	});

	$('.modalMessage').css ({
				'padding':'6px',
				'font-size': '20px',
				'color': '#535353'
			});
	$('.modalErrors').css ({
				'font-size': '12px',
				'color': '#535353'
			});

}
function hidePopWin(callReturnFunc) {	
	$("#imagesFrame").css("visibility","visible"); // applet
	$.unblockUI();
	if (callReturnFunc != false && window[callReturnFunc]) {	
		window[callReturnFunc]();
	}
}
function open_viewer(obj, image_num){

	dp('image_num', image_num);

	var gallery = $(obj).lightGallery({
			mode: 'lg-fade',
			dynamic: true,
         dynamicEl: gl_gallery_images,
			thumbnail: true,
			zoom: true,
			overlayOpacity: 0.8,
			download: true,
			index: image_num
		});

	$(obj).on('onCloseAfter.lg', function (event) {

		$("#imagesFrame").css("visibility", "visible"); // applet
	});

	$("#imagesFrame").css("visibility","hidden"); // applet

}
function submit_list(action, message, do_confirm, formu)
{
  formu=formu?formu:document.list;
  rows_selected = check_selected(formu);
  confirmed = rows_selected && do_confirm?confirm(message):true;
  if (confirmed && rows_selected)
  {
    document.getElementById('message_container').style.display='none';
    formu.target = 'save_frame';
	old_action = formu.action.value;
    formu.action.value = action;
    formu.submit();
    formu.action.value = old_action; // per quan es fa en ajax, aixi torna el valor per defecte per el submit normal
  }
}
function submit_list_move_to(obj, move_field, action, formu)
{
  formu=formu?formu:document.list;
   if (!check_selected(formu)) {
	   obj.selectedIndex=0;
	   return;
   }
  //showPopWin('', 300, 100, null, false, false, MODALBOX_SAVE);
  formu.move_field.value = move_field;
  formu.elements[move_field].value = obj.options[obj.selectedIndex].value;
  submit_list(action);
}
function submit_list_move_to_button( move_field, val)
{
	var formu = document.list;
	if (!check_selected(formu)) {
	   return;
	}

	if (!$('input[name="move_field"]').length) {
		$(formu).append( '<input name="move_field" type="hidden" value="" />');
		$(formu).append( '<input name="' + move_field + '" type="hidden" value="" />');
	}
	$('input[name="move_field"]').val(move_field);
	$('input[name="'+ move_field + '"]').val(val);
	submit_list('save_rows_selected_to_value');
}
function submit_form(message, do_confirm, formu)
{
  formu=formu?formu:document.form_delete;
  formu.target = 'save_frame';
  confirmed = do_confirm?confirm(message):true;
  if (confirmed)
  {
    formu.submit();
  }
}

function submit_form_save(action, formu)
{
  formu=formu?formu:document.list;
  showPopWin('', 300, 100, null, false, false, MODALBOX_SAVE);
  document.getElementById('message').innerHTML = '';
  formu.target = 'save_frame';
  formu.action.value = action;
  formu.submit();
}


function select_all(state,formu)
{
	even_odd = 'Odd';
	for (x=0;x<formu.elements.length; x++)
	{
	 if ((formu.elements[x].type == 'checkbox') && (formu.elements[x].name.indexOf('selected[')!=-1))
	 {
		 formu.elements[x].checked = state;
		 even_odd = even_odd=='Even'?'Odd':'Even';
		 obj = document.getElementById('list_row_'+formu.elements[x].value);
		 if (state) {obj.className = 'listItemDown'} else {obj.className = 'listItem' + even_odd};
	 }
	 // quan hi ha més d'un checkbox per fila
	 else if((formu.elements[x].type == 'checkbox')){
		 formu.elements[x].checked = state;
	 }
	}
}


// quan hi ha més d'un checkbox per fila
function select_all_checkboxes(state,formu)
{
	for (x=0;x<formu.elements.length; x++)
	{
	 if (formu.elements[x].type == 'checkbox')
	 {
	 formu.elements[x].checked = state;
	 }
	}
}

function submit_select_list(obj,formu, action)
{
  if (obj.selectedIndex != 0 && check_selected(formu))
  {
    formu.select_action.value = action;
    formu.submit();
  }
  else
  {
    obj.selectedIndex = 0;
  }
}

function go_url_select(obj,formu,sUrl,sTarget)
{
    obj_page = (sTarget)?frames[sTarget]:window;
    val_selected = obj.options[obj.selectedIndex].value;
    obj_page.location.href = sUrl + val_selected;
}

function check_selected(formu)
{
 b=is_any_selected(formu);
 if (!b && b_check_selected) alert (NONE_SELECTED);
 return !b_check_selected || b;
}

function is_any_selected(formu)
{
	for (x=0;x<formu.elements.length; x++)
	{
	 if ((formu.elements[x].checked)&&(formu.elements[x].name.indexOf('selected')!=-1))  return true;
	 if ((formu.elements[x].checked)&&(formu.elements[x].name.indexOf('download_selected')!=-1))  return true;
	}
  return false;
}

var b_check_selected = true;


function m_over(src,clr) {
 	if (!src.contains(event.fromElement)) {
	 src.style.cursor = 'hand';
	 src.bgColor = clr;
	}
}
function m_out(src,clr) {
	if (!src.contains(event.toElement)) {
	 src.style.cursor = 'default';
	 src.bgColor = clr;
	}
}
function mClk(src) {
	if(event.srcElement.tagName=='TD'){
	 src.children.tags('A')[0].click();
	}
}

function open_print_window(url){
	if (gl_list_print_language) url += '&language=' + gl_list_print_language;
	window.open(url,'printWindow','toolbar=no,location=no,directories=no,status=no,menubar=yes,scrollbars=yes,resizable=yes,copyhistory=no,width='+ ($(window).width()-100) +',height='+ ($(window).height()-100) +',screenX=50,screenY=50,top=50,left=50')
}
gl_list_print_language = '';
function set_print_language(language, language_code){
	$('#list_print_button').find('strong').html(language_code);
	gl_list_print_language = language;
}

function change_read(escritura, lectura){
	if (!document.getElementById(lectura).checked) {
		document.getElementById(escritura).checked = false;
	}
}
function change_write(lectura, escritura){
	if (document.getElementById(escritura).checked) {
	document.getElementById(lectura).checked = document.getElementById(escritura).checked;
	}
}
function change_bg_color(obj, estil, conta)
{	

	// llistats amb checkbox	
	if (document.list && document.list.elements) {
	
		list_checkbox = $('input[name^="selected["][type=checkbox][value="' + conta + '"]');
		if (list_checkbox.length) {
			var seleccionat = list_checkbox.attr('checked')=='checked';

			if (estil=='Down') {
				list_checkbox.attr('checked',!seleccionat);
			}

			// A down nomes poso i trec la classe que toca, així no substitueix altres classes
			if (seleccionat) {
				if (estil == 'Over') obj.className = 'listItem' + estil;
				if (estil == 'Odd' || estil == 'Even') obj.className = 'listItemDown';
				if (estil == 'Down') {
					var add_class = (conta % 2) ? 'Even' : 'Odd';
					$(obj).removeClass('listItemDown');
					$(obj).addClass('listItem' + add_class);
				}
			} else {
				if (estil == 'Down') {
					var remove_class = (conta % 2) ? 'Odd' : 'Even';
					$(obj).removeClass(remove_class);
					$(obj).addClass('listItemDown');
				}
				else {
					obj.className = 'listItem' + estil;
				}
			}

		}		
	}
	// llistats sense checkbox
	else
	{
		//if ($(obj).css('cursor')=='pointer') obj.className = 'listItem' + estil;
		if (obj.onclick) obj.className = 'listItem' + estil;
	}
}

function set_focus(obj)
{

	for (x=0;x<obj.elements.length; x++)
	{
	 if ( (obj.elements[x].type.indexOf('select')!=-1)||(obj.elements[x].type == 'text')||(obj.elements[x].type == 'textarea')||(obj.elements[x].type == 'radio') )
	 {
	 //obj.elements[x].focus();
	break;
	 }
	}
}

function setDebug() {
	if (window.console) {
		window.dp = window.console.log.bind(window.console);
	} else {
		window.dp = function() {};
	}
}
setDebug();

/*function dp(error_text, error){
	if (typeof(error)=='undefined') error = error_text;
	else error = error_text+": " + error;
	if(window.console) {
		console.log(error);
	}
}*/

function form_object_file_delete_row(id){
	if ($('#' + id).hasClass('disabled')) {
		$('#' + id).removeClass('disabled');
		$('#' + id + ' input').prop('disabled', false);
		$('#' + id + '_hidden').prop('checked', true);
	}
	else {
		$('#' + id).addClass('disabled');
		$('#' + id + ' input').prop('disabled', true);
		$('#' + id + '_hidden').prop('checked', false);
	}
}

function form_object_file_replace_on_change (obj) {

	var filename = $(obj)[0].files.length ? $(obj)[0].files[0].name : "";
	var id_span = obj.id + '_span';
	$('#' + id_span).html(filename);

}


function add_file_dropzone(tool, tool_section, name, id, input_id,  acceptedFiles){

	Dropzone.autoDiscover = false;
	var url_base = "?tool="+tool+"&tool_section="+tool_section + "&name="+name+"&call=drop_upload_file&gl_is_ajax=1&id="+id;
	var options = {
		url: url_base,
		acceptedFiles: acceptedFiles,
		addRemoveLinks:true,
		thumbnailWidth: 200,
		thumbnailHeight: 133,
		dictDefaultMessage: DROP_FILE_DEFAULT_MESSAGE,
		dictInvalidFileType: DROP_INVALID_FILE_TYPE,
		dictCancelUploadConfirmation: DROP_CANCEL_UPLOAD_CONFIRMATION,
		//forceFallback: true,
		paramName: name
	}

	var myDropzone = new Dropzone('div#dropzone'+input_id, options);

	myDropzone.on("success", function(file,response) {
		dp(file, 'file');
		dp('response', response);

	if  (response['files']) {
			$('#' + input_id + '_holder' ).html(response['files']);
		}
	});

}

function add_dropzones(tool, tool_section, id_field, thumb_w, thumb_h){
	Dropzone.autoDiscover = false;
	var url_base = "?menu_id="+gl_menu_id+"&tool="+tool+"&tool_section="+tool_section+"&gl_is_ajax=1&call=drop_upload_image&id=";
	var options = {
						url: '',
						acceptedFiles: "image/*",
						thumbnailWidth: thumb_w,
						thumbnailHeight: thumb_h,
						addRemoveLinks:true,
						dictDefaultMessage: DROP_DEFAULT_MESSAGE,
						dictInvalidFileType: DROP_INVALID_FILE_TYPE,
						dictCancelUploadConfirmation: DROP_CANCEL_UPLOAD_CONFIRMATION,
						//forceFallback: true,
						paramName: "imageDrop"
				  }
	
	$('input[type=hidden][name^="'+id_field+'["]').each( function (index, obj){
		var id = $(obj).val();
		
		if ( $('#list_row_' + id + ' td.listImage .no-dropzone').length) return;

		var div_cont;
		if ( $('#list_row_' + id + ' td.listImage .noimage').length)
			div_cont  = '#list_row_' + id + ' td.listImage .noimage';
		else
			div_cont  = '#list_row_' + id + ' td.listImage';
			
		var $newdiv1 = $(div_cont).append('<span class="dropzone" id="dropzone'+id+'"/>');
		
		options['url'] = url_base + id;
		
		//$('#dropzone'+id).dropzone( options );
		var myDropzone = new Dropzone('span#dropzone'+id, options);
		
		
	});
	
}
function add_form_dropzone (id, thumb_w, thumb_h){

	var tool = gl_child_tool ? gl_child_tool : gl_tool;
	var tool_section = gl_child_tool_section ? gl_child_tool_section : gl_tool_section;

	Dropzone.autoDiscover = false;
	var url_base = "?menu_id="+gl_menu_id+"&tool="+tool+"&tool_section="+tool_section+"&gl_is_ajax=1&is_dropzone_form=1&call=drop_upload_image&id=" + id;
	var options = {
						url: url_base,
						acceptedFiles: "image/*",
						thumbnailWidth: thumb_w,
						thumbnailHeight: thumb_h,
						addRemoveLinks: true,
						dictDefaultMessage: DROP_DEFAULT_MESSAGE,
						dictInvalidFileType: DROP_INVALID_FILE_TYPE,
						dictCancelUploadConfirmation: DROP_CANCEL_UPLOAD_CONFIRMATION,
						//forceFallback: true,
						paramName: "imageDrop"
				  };
	dp('span#dropzone'+id);
	var myDropzone = new Dropzone('span#dropzone'+id, options);


	myDropzone.on("success", function(file,response) {

		if  (response['images_html']) {
			$('#ajax_list' ).html(response['images_html']);
			start_images_drag();
			top.gl_gallery_images = response['gallery_images'];
		}
	});
}


function add_select_long (tool, tool_section, menu_id, params, field, form_id, is_v3, action) {

	if (typeof (action) == 'undefined') {
		action = 'get_select_long_values';
		if (is_v3 == '') action = 'list_records_get_select_long_values';
	}

	var html = '';

	var base_jq = '#' + field;
	var service_url = "/admin/?action=" + action + "&tool=" + tool + '&tool_section=' + tool_section + '&menu_id=' + menu_id + '&field=' + field + params;

	if (form_id !== false) {
		base_jq += '_' + form_id;
		service_url += '&id=' + form_id;
	}

	var last_valid_data  = {};
	last_valid_data [field] = {'data': $(base_jq).val(), 'value': $(base_jq + '_add').val() };

	// recullo el primer resultat
	$.ajax({
		url: service_url,
		data: {
			q: ''
		},
		dataType: 'json',
		type: 'GET'
	})
		.done(function (data) {

			if (typeof data.is_complete == 'undefined') data.is_complete = false;
			if (typeof data.has_custom == 'undefined') data.has_custom = false;

			$(base_jq + '_add').show();

			$(base_jq + '_add').autocomplete({
				serviceUrl: service_url,
				minChars: 0,
				noCache: false,
				showNoSuggestionNotice: true,
				noSuggestionNotice: NO_RESULTS,
				autoSelectFirst: true,
				onSelect: function (suggestion) {
					id = suggestion['data'];
					value = suggestion['value'];

					if (id) {
						$(base_jq).val(id);
						last_valid_data [field]['data'] = id;
						last_valid_data [field]['value'] = value;
					}

				}
			}).blur(function () {
				// Permetre valor buit
				var val = $(base_jq + '_add').val();

				if (val) {
					$(base_jq + '_add').val(last_valid_data [field]['value']);
					$(base_jq).val(last_valid_data [field]['data']);
				}
				else {
					$(base_jq).val('');
				}
			});

			if (data.is_complete) {
				$(base_jq + '_add').autocomplete().setOptions({
					lookup: data.suggestions
				});
			}
			// Si no es complert, ho poso en cache per tindre els primers resultats automàticament
			else {
				$(base_jq + '_add').autocomplete().cachedResponse[service_url + '?query='] = data;
			}

			if (data.has_custom) {
				$(base_jq + '_add').autocomplete().setOptions({
					formatResult: function (suggestion, currentValue) {
						return suggestion.custom;
					}
				});
			}

		});

}


function add_text_autocomplete (tool, tool_section, menu_id, params, field, form_id, is_v3, action) {

	if (typeof (action) == 'undefined') {
		action = 'get_text_autocomplete_values';
		if (is_v3 == '') action = 'list_records_get_text_autocomplete_values';
	}

	var base_jq = '#' + field;
	var service_url = "/admin/?action=" + action + "&tool=" + tool + '&tool_section=' + tool_section + '&menu_id=' + menu_id + '&field=' + field + params;

	if (form_id !== false) {
		base_jq += '_' + form_id;
		service_url += '&id=' + form_id;
	}


	$(base_jq).autocomplete({
		serviceUrl: service_url,
		minChars: 0,
		noCache: false,
		autoSelectFirst: true,
		onSelect: function(suggestion){
		}
	});

}
function sublist_reset_all( ) {
	for (var i in checkboxes_long_vars) {
		var sublist = checkboxes_long_vars[i];
		sublist.selected_ids = [];
		sublist.new_record_id = 0;
		$(sublist.id).autocomplete().setOptions({
				params: {'selected_ids': []}
		});
	}

	$('.sublist-new').empty().hide();
}
function sublist_delete( id, field, inputs_prefix, related_field, action) {


	if (action == 'new') {
		var related_id = $('#' + inputs_prefix + '_' + related_field + '_' + id).val();

		var selected_ids = checkboxes_long_vars[ field ].selected_ids.filter(function (elem) {
			return elem != related_id;
		});

		checkboxes_long_vars[ field ].selected_ids = selected_ids;

		$(checkboxes_long_vars[ field ].id).autocomplete().setOptions({
				params: {'selected_ids': checkboxes_long_vars[ field ].selected_ids}
			});
	}
	else {
		var input = $('input[name="' + inputs_prefix + '[delete_record_ids]"]');

		dp('id', id);
		dp('field', field);
		dp('inputs_prefix', inputs_prefix);
		dp('related_field', related_field);

		dp('input', input);
		

		var delete_ids = input.val();

		if (delete_ids) {
			delete_ids = delete_ids.split(',');
		}
		else {
			delete_ids = [];
		}
		delete_ids.push(id);
		input.val(delete_ids.join(','));
	}

	$('#' + inputs_prefix + '_list_row_' + id).remove();

	if (!$('#' + inputs_prefix + '_list_table > tbody > tr').length) {
		if (action == 'new') {
			$('#' + inputs_prefix + '_list_table').hide();
		}
		else {
			$('#' + inputs_prefix + '_list_table').remove();
		}
	}
}
var checkboxes_long_vars = {};
function add_checkboxes_long (tool, tool_section, menu_id, params, field, form_id, is_v3, is_sublist) {

	var action = 'get_checkboxes_long_values';
	if (is_v3 == '') action = 'list_records_get_checkboxes_long_values';

	var base_jq = '#' + field + '_' + form_id;
	var service_url = "/admin/?action=" + action + "&tool=" + tool + '&tool_section=' + tool_section + '&menu_id=' + menu_id + '&field=' + field + '&id=' + form_id + params;


	checkboxes_long_vars[field] = {};
	checkboxes_long_vars[field].id = base_jq + '_add';
	checkboxes_long_vars[field].new_record_id = 0;
	checkboxes_long_vars[field].selected_ids = [];


	// recullo el primer resultat
	$.ajax({
		url: service_url,
		data: {
			selected_ids: '',
			q: ''
		},
		dataType: 'json',
		type: 'GET'
	})
		.done(function (data) {

			if (typeof data.is_complete == 'undefined') data.is_complete = false;
			if (typeof data.has_custom == 'undefined') data.has_custom = false;

			$(base_jq + '_add').show();


			$(base_jq + '_add').autocomplete({
				serviceUrl: service_url,
				minChars: 0,
				noCache: false,
				showNoSuggestionNotice: true,
				noSuggestionNotice: NO_RESULTS,
				autoSelectFirst: true,
				onSelect: function (suggestion) {
						add_checkboxes_long_on_select (base_jq, suggestion, field, is_sublist, tool, tool_section, menu_id, params, form_id);
					}
			});


			if (data.is_complete) {
				$(base_jq + '_add').autocomplete().setOptions({
					lookup: data.suggestions
				});
			}
			// Si no es complert, ho poso en cache per tindre els primers resultats automàticament
			else {
				$(base_jq + '_add').autocomplete().cachedResponse[service_url + '?query='] = data;
			}

			if (data.has_custom) {
				$(base_jq + '_add').autocomplete().setOptions({
					formatResult: function (suggestion, currentValue) {
						return suggestion.custom;
					}
				});
			}

		});
}
function add_checkboxes_long_on_select (base_jq, suggestion, field, is_sublist, tool, tool_section, menu_id, params, form_id) {


			$(base_jq + '_add').val('');

			var html = '';
			var selected_id = suggestion['data'];
			var selected_text = suggestion['value'];
			checkboxes_long_vars[field].selected_ids.push(selected_id);

			$(base_jq + '_add').autocomplete().setOptions({
				params: {'selected_ids': checkboxes_long_vars[field].selected_ids}
			});

			if (is_sublist) {

				var table_id = '#' + is_sublist;
				if (params) params = '/?' + params.substring(1);

				if (checkboxes_long_vars[field].selected_ids.length == 1) $(table_id).show();

				checkboxes_long_vars[field].new_record_id++;

				$.ajax({
					url: '/admin' + params,
					data: {
						action: 'get_sublist_item',
						tool: tool,
						tool_section: tool_section,
						menu_id: menu_id,
						field: field,
						id: form_id,
						new_record_id: checkboxes_long_vars[field].new_record_id,
						selected_id: selected_id
					},
					dataType: 'json',
					type: 'GET'
				})
					.done(function (data) {

						var table = $(table_id + ' > tbody').length ? $(table_id + ' > tbody') : $(table_id);
						table.append(data.html);

					});


			}
			else {

				html = '<td><input type="checkbox" id="' + field + '_' + form_id + '_' + selected_id + '" name="' + field + '[' + form_id + '][' + selected_id + ']" checked="checked" value="' + selected_id + '"></td>';
				html += '<td align="left"><label for="' + field + '_' + form_id + '_' + selected_id + '">' + selected_text + '&nbsp;&nbsp;&nbsp;&nbsp;</label></td>';

				$(base_jq + '_table').append(html);
			}


}

function form_object_select_edit(select_input, link, name) {
	var edit_id = $('#' + select_input).val();

	if (edit_id != 'null' && edit_id) {
		showPopWin(link + '&' + name + '=' + edit_id, 700, 480, false, true);
	}
	else {
		alert(NO_OPTION_SELECTED);
	}
}

function form_object_init_password(id){
	$('#' + id + '_masked').remove();
	$('#' + id).prop("type", "password"); // TODO jquery 1.7 attr
	$('#' + id).attr('autocomplete', 'new-password');
	document.getElementById(id).readOnly = true;
	setTimeout(function () {
		document.getElementById(id).readOnly = false
	}, 150);
}

function form_object_toggle_password(id) {
	$input = $('#' + id);

	$(this).toggleClass("fa-eye fa-eye-slash");

	if ($input.attr("type") == "password") {
		$input.prop("type", "text");
	} else {
		$input.prop("type", "password");
	}

}

function show_global_message (message) {
	if (top.document.getElementById("message")) {
		top.document.getElementById("message_container").style.display = "block";
		top.document.getElementById("message").innerHTML = message;
	}
	if (typeof top.hidePopWin == "function") top.window.setTimeout("hidePopWin(false);", 800);
	if (typeof top.showPopWin == "function") top.showPopWin("", 300, 100, null, false, false, message);
}

// Substituir aquestes per les de booking
function unformat_currency_js(currency){
	currency = currency.replace('.', '');
	currency = currency.replace(',', '.');

	currency = parseFloat (currency);

	if (isNaN(currency)) currency = 0;

	return currency;
}
function format_currency_js(currency){

	if (isNaN(currency)) return '0,00';

	currency = currency * 100;
	currency = Math.round(currency);

	var is_negative = currency<0;
	currency = Math.abs(currency);

	var ret_curr = currency.toString();
	var punt_index;

	if (currency>99999){
		var coma_index = ret_curr.indexOf(',');
		if (coma_index==-1) coma_index = ret_curr.length;
		punt_index = coma_index-5;
		ret_curr = ret_curr.substr(0,punt_index) + '.' + ret_curr.substr(punt_index)
	}
	if (currency>99){
		punt_index = ret_curr.length-2;
		ret_curr = ret_curr.substr(0,punt_index) + ',' + ret_curr.substr(punt_index);
	}
	else if (currency>9){
		ret_curr = '0,' + ret_curr;
	}
	else{
		ret_curr = '0,0' + ret_curr;
	}

	if (is_negative) ret_curr = '-' + ret_curr;

	return ret_curr;
}