<?

/* 14-09-15

  Actualitzaciˇ recaptcha de google

 */

if ( is_table( 'all__configpublic' ) && ! Db::get_first( "SELECT * FROM all__configpublic WHERE name='recaptcha_key'" ) ) {
	$update_sql[] = "
		INSERT INTO all__configpublic (`name`, `value`) VALUES ('recaptcha_key', '');";

} else {
	if ( is_table( 'all__configpublic' ) ) {
		trigger_error( 'Ja existeixen el camp recaptcha_key' );
	}
}
if ( is_table( 'all__configpublic' ) && ! Db::get_first( "SELECT * FROM all__configpublic WHERE name='recaptcha_secret'" ) ) {
	$update_sql[] = "
		INSERT INTO all__configpublic (`name`, `value`) VALUES ('recaptcha_secret', '');";

} else {
	if ( is_table( 'all__configpublic' ) ) {
		trigger_error( 'Ja existeixen el camp recaptcha_secret' );
	}
}
if ( is_table( 'all__configpublic' ) && ! Db::get_first( "SELECT * FROM all__configpublic WHERE name='use_recaptcha'" ) ) {
	$update_sql[] = "
		INSERT INTO all__configpublic (`name`, `value`) VALUES ('use_recaptcha', '0');";

} else {
	if ( is_table( 'all__configpublic' ) ) {
		trigger_error( 'Ja existeixen el camp recaptcha_secret' );
	}
}

?>