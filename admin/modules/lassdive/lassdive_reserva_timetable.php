<?
Main::load_class( 'lassdive', 'timetable_common', 'admin' );

/**
 * LassdiveReservaTimetable
 *
 * Funcions per crear taula d'horaris de reserva per una activitat
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2018
 * @version 1
 * @access public
 */
class LassdiveReservaTimetable {

	var $is_admin = false, $is_public = false, $caption, $centres, $positions = [], $current_centre = 0, $error, $is_miquel;

	public function __construct( &$caption ) {
		$this->caption = $caption;
		$user_id = $_SESSION['user_id'];
		$this->is_miquel = in_array( $user_id, [ '1', '2', '3' ] );
	}

	public function get_table( $reservaitem_id ) {

		$error = $html = $header = '';
		if ( ! $reservaitem_id ) return [ 'html' => '', 'error' => '', 'header' => '' ];

		list( $product_id, $reservaitem_date, $is_new, $start_time, $end_time, $centre_id, $centre, $order_id )
			= $this->get_orderitem_vars( $reservaitem_id );

		$product_vars = $this->get_product_vars( $product_id );
		$timetables   = $this->get_timetables_array( $product_id, $reservaitem_id, $reservaitem_date );

		if ( ! $timetables['timetables'] ) $error = $this->error;

		$tpl = new phemplate( PATH_TEMPLATES, 'lassdive/reserva_timetable.tpl' );
		$tpl->set_vars( $this->caption );
		$tpl->set_vars( [
			'order_id'              => $order_id,
			'reservaitem_id'        => $reservaitem_id,
			'reservaitem_date'      => format_date_form( $reservaitem_date ),
			'reservaitem_date_js'   => $reservaitem_date,
			'can_select_lower_date' => $this->is_miquel ? 'true' : 'false',
			'is_new'                => $is_new,
			'centres_json'          => $this->get_centres_json(),
			'is_user_popup'         => false,
			'is_header'             => false,
			'has_current_center'    => $this->current_centre > 1 ? 'has-current-center' : '',
		] );
		$tpl->set_vars( $product_vars );

		if ( $error ) {
			$tpl->set_var( 'timetables', true ); // per que no doni error a reserva_time_table.start , reserva_time_table.end del tpl
		}
		else  {
			$tpl->set_loop( 'timetables', $timetables['timetables'] );
			$tpl->set_loop( 'no_time_reservas', $timetables['no_time_reservas'] );
			$tpl->set_loop( 'centres', $this->centres );

			$html = $tpl->process();
		}
		if ( R::escape( 'is_init' ) ) {
			$tpl->set_var( 'is_header', true );
			$header = $tpl->process();
		}

		return [ 'html' => $html, 'error' => $error, 'header' => $header ];
	}

	public function get_user_ids_popup( $reservaitem_id ) {

		$error = $html = $header = '';
		if ( ! $reservaitem_id ) return [ 'html' => '', 'error' => '', 'header' => '' ];

		list( $product_id, $reservaitem_date, $is_new, $start_time, $end_time, $centre_id, $centre, $order_id )
			= $this->get_orderitem_vars( $reservaitem_id );

		$product_vars = $this->get_product_vars( $product_id );


		$tpl = new phemplate( PATH_TEMPLATES, 'lassdive/reserva_timetable.tpl' );
		$tpl->set_vars( $this->caption );
		$tpl->set_vars( [
			'order_id'              => $order_id,
			'reservaitem_id'        => $reservaitem_id,
			'reservaitem_date'      => format_date_form( $reservaitem_date ),
			'reservaitem_date_js'   => $reservaitem_date,
			'start_time'            => $start_time,
			'end_time'              => $end_time,
			'selected_centre_id'    => $centre_id,
			'selected_centre'       => $centre,
			'is_new'                => $is_new,
			'centres_json'          => '',
			'is_user_popup'         => true,
			'is_header'             => false,
			'can_select_lower_date' => $this->is_miquel ? 'true' : 'false',
			'has_current_center'    => '',
		] );
		$tpl->set_vars( $product_vars );

		if ( ! $error ) {
			$tpl->set_loop( 'timetables', false );
			$tpl->set_loop( 'centres', $this->centres );

			$html = $tpl->process();
		}
		if ( R::escape( 'is_init' ) ) {
			$tpl->set_var( 'is_header', true );
			$header = $tpl->process();
		}

		return [ 'html' => $html, 'error' => $error, 'header' => $header ];
	}

	public function get_timetables_array( $product_id, $reservaitem_id, $reservaitem_date ) {

		if ($this->is_day_busy( $product_id, $reservaitem_date )) {
			$this->error = $this->caption['c_day_is_busy'];
			return false;
		}

		$centres = $this->centres = $this->get_centres( $product_id );

		if ( ! $centres ) {
			$this->error = $this->caption['c_no_center_with_activity'];
			return false;
		}

		$timetable_common = new LassdiveTimetableCommon();

		$hours      = $timetable_common->get_hours();
		$timetables = $timetable_common->get_busy_hours( $hours, $centres, $product_id, $reservaitem_id, $reservaitem_date );


		return $timetables;
	}

	function is_day_busy($product_id, $reservaitem_date){
		$query = "
			SELECT count(*) 
				FROM product__familybusy
				WHERE family_id = ( SELECT family_id FROM product__product WHERE product_id = $product_id)
				AND date_busy = '$reservaitem_date'";
		return Db::get_first( $query );
	}

	/**
	 * @param $product_id
	 *
	 *
	 * @return array Centres que tenen el producte i quantitat de producte i que estan públics
	 */
	private function get_centres( $product_id ) {

		$current_centre = $this->current_centre = empty($_SESSION['centre_id'])?1:$_SESSION['centre_id'];

		$query   = "
			SELECT *,
			(SELECT centre_max_quantity 
				FROM lassdive__centreproduct AS t2
				WHERE product_id = $product_id
				AND t1.centre_id = t2.centre_id) AS centre_max_quantity
			FROM lassdive__centre AS t1
			WHERE status = 'public'
			AND centre_id IN ( 
				SELECT centre_id 
				FROM lassdive__centreproduct
				WHERE product_id = $product_id
				AND centre_max_quantity > 0
			) ";

		if ($this->is_public){
			$query   .= "ORDER BY centre_id=$current_centre DESC, centre ASC";
		}
		else{
			$query   .= "ORDER BY centre ASC";
		}
		$results = Db::get_rows( $query );

		return $results;
	}

	private function get_centres_json() {
		$c = [];
		foreach ( $this->centres as $centre ) {
			$c[ $centre ['centre_id'] ] = [ 'centre_max_quantity' => $centre['centre_max_quantity'] ];
		}

		return json_encode( $c );
	}

	private function get_product_vars( $product_id ) {

		$ret   = [];
		$query = "
			SELECT product_title 
			FROM product__product_language 
			WHERE product_id = $product_id";

		$ret ['product'] = Db::get_first( $query );

		$query                   = "
			SELECT quantityconcept 
			FROM product__quantityconcept_language 
			INNER JOIN product__product 
			USING (quantityconcept_id) 
			WHERE product__product.product_id = $product_id";

		$ret ['quantityconcept'] = Db::get_first( $query );
		$ret ['product_id'] = $product_id;

		return $ret;

	}

	private function get_orderitem_vars( $reservaitem_id ) {

		$is_new = R::id( 'is_new' );
		$is_init = R::id( 'is_init' );

		if ($is_new) {
			$orderitem_id     = 0;
			$order_id         = 0;
			$reservaitem_date = unformat_date( R::escape( 'reservaitem_date', false, '_POST' ) );
			$product_date     = 0;
			$product_id       = R::escape( 'product_id', false, '_POST' );
			$start_time       = '';
			$end_time         = '';
			$centre_id        = '';
			$centre           = '';

			// Per reservaitem que no s'ha triat data
			if (!$reservaitem_date) {
				$reservaitem_date = date("Y/n/j");
			}
		}

		else {
			$query            = "
			SELECT orderitem_id, reservaitem_date, start_time, end_time, centre_id 
			FROM lassdive__orderitem 
			WHERE reservaitem_id = $reservaitem_id";
			$rs = Db::get_row( $query );

			$orderitem_id     = $rs['orderitem_id'];
			$reservaitem_date = $rs['reservaitem_date'];
			$start_time       = format_time( $rs['start_time'] );
			$end_time         = format_time( $rs['end_time'] );
			$centre_id        = $rs['centre_id'];

			$query        = "
			SELECT product_id, product_date, order_id 
			FROM product__orderitem 
			WHERE orderitem_id = $orderitem_id";
			$rs           = Db::get_row( $query );
			$product_id   = $rs['product_id'];
			$product_date = $rs['product_date'];
			$order_id         = $rs['order_id'];

			// Les comandes que no tenen cap dia assignat no mostrava el calendari al timetable
			// per tant li assigno el dia d'avui per poder mostrar el calendari
			if ($is_init && $product_date == '0000-00-00') $product_date = date("Y/n/j");


			$query  = "SELECT centre FROM lassdive__centre WHERE centre_id = $centre_id";
			$centre = Db::get_first( $query );
		}

		$date_wanted      = R::get( 'day' );
		$reservaitem_date = $reservaitem_date == '0000-00-00' ? $product_date : $reservaitem_date;
		$reservaitem_date = $date_wanted ? $date_wanted : $reservaitem_date;

		/*		$today = new DateTime();
		if ( $today > new DateTime( $reservaitem_date ) ) {
			$reservaitem_date = $today->format( "Y/n/j" ); // sobretot el mes sense 0 a l'inici, que es com es guarda al cart
		}*/


		return [
			$product_id,
			$reservaitem_date,
			$is_new,
			$start_time,
			$end_time,
			$centre_id,
			$centre,
			$order_id
		];
	}

	static public function save_timetable_ajax( &$caption ) {

		$ret['error']   = $caption['c_error_guardar'];
		$ret['success'] = true;

		$reservaitem_id   = R::id( 'reservaitem_id' );
		$reservaitem_date = R::escape( 'reservaitem_date' );
		$start_time       = R::escape( 'start_time' );
		$end_time         = R::escape( 'end_time' );
		$user_ids         = R::get( 'user_ids' );
		R::escape_array( $user_ids );
		$centre_id = R::escape( 'centre_id' );

		$rows_to_update = self::get_rows_to_copy_selection( $reservaitem_id, $centre_id );

		foreach ( $rows_to_update['results'] as $rs ) {
			$update_reservaitem_id = $rs['reservaitem_id'];
			self::save_timetable_ajax_update( $update_reservaitem_id, $reservaitem_date, $start_time, $end_time, $user_ids, $centre_id );
		}
		$ret['rows'] = $rows_to_update['rows'];

		$ret['error'] = '';
		// Proves - http_response_code( 401 );
		json_end( $ret );
	}

	static public function save_timetable_ajax_update( $reservaitem_id, $reservaitem_date, $start_time, $end_time, $user_ids, $centre_id ) {
		$query = "
			UPDATE lassdive__orderitem 
			SET 
				reservaitem_date = '$reservaitem_date', 
				start_time = '$start_time', 
				end_time = '$end_time',
				centre_id = '$centre_id'
				WHERE reservaitem_id = $reservaitem_id;
			DELETE FROM lassdive__reservaitem_to_user
			WHERE reservaitem_id = $reservaitem_id;";

		if ( $user_ids ) {
			foreach ( $user_ids as $user_id ) {
				$query .= "
			INSERT INTO lassdive__reservaitem_to_user 
				(user_id, reservaitem_id)
			VALUES
				('$user_id', $reservaitem_id);
			";
			}
		}
		lassdive_log_sql( $query );
		Db::multi_query( $query );
	}

	// Si no s'ha posat cap start_time, end_time, centre_id i user_id el canvio tots a l'hora
	static private function get_rows_to_copy_selection( $reservaitem_id, $centre_id ) {

		// Aquí es suposa que tots estan en el mateix orderitem, poder s'hauria de fer buscant el order_id, però llavors s'ha de controlar que tingui el mateix product_id
		$orderitem_id_query = "
				SELECT orderitem_id 
				FROM lassdive__orderitem 
				WHERE reservaitem_id = $reservaitem_id";
		$orderitem_id       = Db::get_first( $orderitem_id_query );

		$query = "
			SELECT reservaitem_id FROM lassdive__orderitem 
			WHERE 
				(
					start_time <> ' 00:00:00' 
					OR end_time <> ' 00:00:00' 
					OR centre_id <> '0'
					OR reservaitem_id IN ( 
						SELECT reservaitem_id 
						FROM lassdive__reservaitem_to_user
						WHERE lassdive__orderitem.reservaitem_id = lassdive__reservaitem_to_user.reservaitem_id
						 )
				)
				AND orderitem_id = $orderitem_id;";


		$results = Db::get( $query );
		$ret     = [];

		// Si no en troba cap, vol dir que es el primer cop que s'assigna horari, per tant els poso tots iguals
		if ( ! $results ) {

			$query      = "SELECT product_id FROM product__orderitem WHERE orderitem_id = $orderitem_id";
			$product_id = Db::get_first( $query );


			$query        = "SELECT centre_max_quantity FROM lassdive__centreproduct WHERE centre_id = '$centre_id' AND product_id = $product_id";
			$max_quantity = Db::get_first( $query );

			$row_conta = 0;

			// Poso el mateix ordre que quan les llisto, per això necessito inner join
			$query = "
			SELECT reservaitem_id
			FROM product__orderitem
			INNER JOIN lassdive__orderitem USING (orderitem_id)
			WHERE orderitem_id = $orderitem_id
			ORDER BY orderitem_id ASC, start_time ASC, end_time ASC, product_title, reservaitem_id;";

			$results     = Db::get( $query );
			$rows        = '';
			$new_results = [];

			foreach ( $results as $rs ) {

				$new_results[] = $rs;

				$reservaitem_id = $rs['reservaitem_id'];
				$rows           .= "#reservaitem_list_row_{$reservaitem_id},";

				$row_conta ++;

				if ( $max_quantity == $row_conta ) break;
			}
			$ret['rows']    = substr( $rows, 0, - 1 );
			$ret['results'] = $new_results;
		}
		else {
			$ret['rows']    = false;
			$ret['results'] = [ 0 => [ 'reservaitem_id' => $reservaitem_id ] ];
		}

		return $ret;

	}

	/**
	 * Es crida des de la plantilla  cart_list.tpl
	 * @param $loop
	 */
	static public function get_cart_timetables(&$loop){

		$new_loop = [];
		foreach ( $loop as &$rs ) {
			$product_id = $rs['product_id'];
			$cart_key = $rs['cart_key']?$rs['cart_key']:false; // undefined quan ve de product_common_order.php:199 shop_app_update_sells

			$extra_vars = self::get_cart_item_extra_vars($product_id, $cart_key);
			foreach ( $extra_vars as $rs_extra ) {

				$rs_extra['reservaitem_date'] = self::get_cart_reservaitem_date($rs_extra);


				$rs_extra['user_ids'] = empty($rs_extra['user_ids'])?'':self::get_cart_users($rs_extra['user_ids']);
				$rs_extra['centre_id'] =  empty($rs_extra['centre_id'])?'':self::get_cart_centre($rs_extra['centre_id']);

				$new_rs = array_merge( $rs, $rs_extra );
				$new_loop []= $new_rs;
			}
		}

		$loop = $new_loop;

	}

	static function get_cart_item_extra_vars($product_id, $cart_key){


		if (empty($_SESSION['product']['cart_vars'][ $product_id ]['extra_vars']))
			return [[
			'reservaitem_id' => '',
			'product_date' => '',
			'reservaitem_date' => '',
			'start_time' => '',
			'end_time' => '',
			'centre_id' => '',
			'user_ids' => '',
			]];

		$extra_vars = $_SESSION['product']['cart_vars'][ $product_id ]['extra_vars'];
		$ret = [];


		foreach ( $extra_vars as $rs) {
			$extra_cart_key = $rs['cart_key'];
			if ($extra_cart_key == $cart_key){
				$ret []= $rs;
			}
		}
		return $ret;
	}

	static function get_cart_reservaitem_date( $rs) {
		$reservaitem_date = format_date_list($rs['reservaitem_date']);
		if (!$rs['start_time']) return $reservaitem_date;

		return "$reservaitem_date<br>De {$rs['start_time']} a {$rs['end_time']}";
	}
	static function get_cart_users( $user_ids){

		if (!$user_ids) return '';

		$users = '';
		$sep = '<br>';
		$user_ids = implode ("','", $user_ids);

		$query   = "
				SELECT CONCAT (name, ' ', surname) AS item
					FROM user__user 
					WHERE user_id IN  ('$user_ids')";

		$results = Db::get_rows( $query );

		foreach ( $results as $rs ) {
			$users .= $rs['item'] . $sep;
		}

		return $users;
	}
	static function get_cart_centre($centre_id){

		if (!$centre_id) return '';

		$query = "SELECT centre FROM lassdive__centre WHERE centre_id = '$centre_id'";
		$centre     = Db::get_first( $query );

		return $centre;


	}

	static public function product_orderitem_on_insert( $orderitem_id, &$rs ) {

		$product_id = $rs['product_id'];
		$product_variation_ids = $rs['product_variation_ids'];
		$product_date = $rs['product_date'];

		$reserva_items = self::get_cart_item_extra_vars($product_id, $rs['cart_key']);

		// El trigger ja ha creat els registres
		$query   = "SELECT reservaitem_id FROM lassdive__orderitem WHERE orderitem_id = $orderitem_id";
		$results = Db::get_rows( $query );
		$query = '';


		foreach ( $results as $rs_orderitem ) {

			$reservaitem_id = $rs_orderitem['reservaitem_id'];

			$reserva_item = current($reserva_items);

			// per si no hi ha reservaitem_date, poso el product_date ( teòricament no cal )
			$reservaitem_date = $reserva_item['reservaitem_date']?$reserva_item['reservaitem_date']:$product_date;
			$reservaitem_date = Db::qstr($reservaitem_date);
			$start_time = Db::qstr($reserva_item['start_time']);
			$end_time = Db::qstr($reserva_item['end_time']);
			$centre_id = Db::qstr($reserva_item['centre_id']);
			$user_ids = isset($reserva_item['user_ids'])?$reserva_item['user_ids']:false;

			$query .= "
				UPDATE lassdive__orderitem SET
						orderitem_id = $orderitem_id, 
						reservaitem_date = $reservaitem_date,
						start_time = $start_time, 
						end_time = $end_time,
						centre_id = $centre_id
					WHERE reservaitem_id = $reservaitem_id;
					  ";

			if ($user_ids) {
				foreach ( $user_ids as $user_id ) {
					$user_id = Db::qstr( $user_id );
					$query   .= "
					INSERT INTO lassdive__reservaitem_to_user 
					(reservaitem_id, user_id) 
					VALUES 
					( $reservaitem_id, $user_id);";
				}
			}
			next($reserva_items);

		}
		lassdive_log_sql( $query, $rs, $reserva_items );
		Db::multi_query($query);
	}

}



	function lassdive_log_sql( $sql, $rs = false, $reserva_items = false ){

		$mysql = Db::qstr($sql);
		$user_id = Db::qstr($_SESSION['user_id']);
		$session = Db::qstr(json_encode($_SESSION));
		$cookie = Db::qstr(json_encode($_COOKIE));

		$script_name = Db::qstr( $_SERVER["SCRIPT_NAME"]);
		$host = Db::qstr( $_SERVER["HTTP_HOST"]);
		$uri = Db::qstr( $_SERVER["REQUEST_URI"]);
		$referer = Db::qstr( $_SERVER["HTTP_REFERER"]);
		$query_string = Db::qstr( $_SERVER["QUERY_STRING"]);
		$user_agent = Db::qstr( $_SERVER['HTTP_USER_AGENT']);
		if ($rs) {
			$vars =  [ 'rs'=>$rs,  'reserva_items'=>$reserva_items,  ];
			$vars = Db::qstr( json_encode( $vars ) );
		}
		else {
			$vars = "''";
		}

		$backtrace =  Db::qstr( Debug::get_error_backtrace());

		$query = "
		INSERT INTO `a__log` 
			(
				`mysql`,
			    `user_id`, 
			    `session`,
			    `cookie`,
				`script_name`,
				`host`,
				`uri`,
				`referer`,
				`query_string`,
				`user_agent`,
				`backtrace`,
				`vars`
			) 
			VALUES (
				$mysql,
				$user_id,
				$session,
				$cookie,
				$script_name,
				$host,
				$uri ,
				$referer,
				$query_string,
				$user_agent,
				$backtrace,
				$vars
			);";
		Db::execute( $query );

	}