<?

/* 4-2-17

	NEWS LLISTAR TOT A LA HOME, per defecte sempre llistarem tot, crec que no hi ha cap client que li calgui el contrari


 */

if ( is_table( 'news__configpublic' ) && ! Db::get_first( "SELECT * FROM news__configpublic WHERE name='home_list_all'" ) ) {
	$update_sql[] = "
		INSERT INTO news__configpublic (`name`, `value`) VALUES ('home_list_all', '1');";

} else {
	if ( is_table( 'news__configpublic' ) ) {
		trigger_error( 'Ja existeixen el camp home_list_all' );
	}
}


?>