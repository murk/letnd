<?php
/*
 * 
 * S'ha posat per els usuaris d'api que nomes es puguin editar ells mateixos
 *  No visualitzen cap més grup, ni cap més usuari
 * 
 * A INMO o altres eines, també serveix per editar nomes els inmobles 
 * i els propietaris d'el propi usuari - a API de la mateixa agencia_id
 * 
 * 
 * 
 */
if (!is_field('user__group','edit_only_itself')){
		
	// Afegir i canviar camps de news
		$update_sql .="
		ALTER TABLE `user__group`  ADD COLUMN `edit_only_itself` TINYINT(1) NOT NULL DEFAULT '0' AFTER `ordre`;
		";
}			
?>