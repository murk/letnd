<?
/**
 * RacingRally
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2017
 * @version $Id$
 * @access public
 */
class RacingRally extends Module{
	// var $category_id;
	function __construct(){
		parent::__construct();
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	/* Category
	function on_load(){

		$this->category_id = R::id('category_id');
		if ($this->category_id ){

			$this->set_friendly_params(array(
				'category_id'=>$this->category_id
				));
		}
		else {
			$this->category_id  =  R::id('category');

			if ($this->category_id) {
				$this->set_friendly_params( array(
					'category' => $this->category_id
				) );
			}
		}

		parent::on_load();
	}
	*/
	function get_records()
	{
		$this->set_racing_fields();

		// Array dels anys passats
		$query = "SELECT DISTINCT YEAR(entered) as past_year FROM racing__rally ORDER BY entered DESC";
		$past_years = Db::get_rows( $query );


		$params = [
				'tool' => 'racing',
				'tool_section' => 'rally',
				'friendly_params' => [],
				'exclude_params' => false,
				'detect_page' => true
		];


		$selected_rally_text = '';
		$selected_past_year = R::text_id( 'year' );
		foreach ( $past_years as &$rs ) {


			$past_year = $rs['past_year'];

			$selected = 0;
			$rally_text = $this->caption['c_blunik_rallies_any'] . " $past_year";
			
			if ( $past_year == $selected_past_year ) {
				$selected_rally_text = $rally_text;
			}

			$rs['selected'] = $selected;

			$rs['past_year_text'] = $rally_text;

			$params['friendly_params']['year'] = $past_year;
			$rs['rallies_link'] = get_link($params);

		}

		$params['friendly_params'] = false;

		$selected_next = $selected_past_year?0:1;
		$rally_text = $this->caption['c_blunik_next_rallies'];

		if ( !$selected_past_year ) {
			$selected_rally_text = $rally_text;
		}

		$next_rallies = [
			'past_year' => '',
			'selected'  => $selected_next,
			'past_year_text'  => $rally_text,
			'rallies_link'  => get_link($params),
			'detect_page' => true
		];

		array_unshift( $past_years, $next_rallies );


		$this->set_var( 'past_years', $past_years );
		$this->set_var( 'selected_rally_text', $selected_rally_text );


		$listing = new ListRecords($this);

		$listing->condition = "
					(status = 'public')";

		if ( $selected_past_year ) {
			$listing->condition .= " AND YEAR(entered) = '$selected_past_year'";

			Page::set_page_title($selected_rally_text);
		}
		else {
			$listing->condition .= " AND (entered > now())
					AND (expired=0 OR expired > (now()))";
		}

		
		/* Category
		$category_id = $this->category_id;

		// si està posada categoria per defecte, la utilitzo
		if($this->config['default_category_id'] && !$category_id)
			$category_id = $this->config['default_category_id'];
		Debug::p( $category_id );

		$listing->set_var('listing_category_id',$category_id);	
			
		if ($category_id) $category = Db::get_first("
				SELECT category 
				FROM racing__category_language
				WHERE language = '". LANGUAGE ."' 
				AND category_id = '" . $category_id ."'");
				
		else  $category = '';
		
		$listing->set_var('listing_category',$category);	
		
		if ($category_id) {
			$listing->condition .= " AND category_id = '" . $category_id . "'";

		}
		*/
		$listing->call('records_walk','url1,url1_name,url2,url2_name,url3,url3_name,entered,content,content_list');
		$listing->order_by = 'entered ASC, rally, rally_id';

		if ($this->parent == 'sitemap') {//Sitemap
			$listing->paginate = false;
			$listing->set_field('modified', 'type', 'none');
			$listing->set_records();
			return $listing->loop;
		}
		
		return $listing->list_records();
	}
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{
	    $this->set_racing_fields();
		$show = new ShowForm($this);

		/* Category
		// si no hi ha id, agafo per defecte la primera noticia de la categoria, i si no hi ha categoria, de la primera categoria
		if (!$show->id){
			// si no hi ha categoria agafo la primera ordenat per nom i id
			
			$category_id = $this->category_id;
			if (!$category_id )
			{
				$query = "SELECT racing__category.category_id as category_id
					FROM racing__category_language, racing__category
					WHERE racing__category_language.category_id = racing__category.category_id
					AND language = '" . LANGUAGE . "'
					ORDER BY category, category_id
					LIMIT 0,1";
				$category_id = Db::get_first($query);
			}
			$query = "SELECT racing__rally.rally_id as rally_id
				FROM racing__rally_language, racing__rally
				WHERE racing__rally_language.rally_id = racing__rally.rally_id
				AND status = 'public'
				AND bin <> 1
				AND category_id = '" . $category_id . "'
				AND language = '" . LANGUAGE . "'
				ORDER BY entered DESC, rally_id
				LIMIT 0,1";
			$show->id = Db::get_first($query);
		}	
		*/

		$show->condition = "(status = 'public')";

		
		
		
		$show->call('records_walk','url1,url1_name,url2,url2_name,url3,url3_name,entered');
		$show->get_values();		
		if ($show->has_results)
		{
			if ($this->config['override_gl_page_title'] == '1') {
				Page::set_title($show->rs['rally']);
				Page::set_body_subtitle(''); // si faig override del tiol, no te sentit deixar el subtitol general de la pagina
			}
			elseif ($this->config['override_gl_page_title'] == '2') {
				Page::set_body_subtitle($show->rs['rally']);
			}
			elseif ($this->config['override_gl_page_title'] == '3') {
				Page::set_title($show->rs['rally']);
				Page::set_body_subtitle($show->rs['subtitle']);
			}


			Page::set_page_title($show->rs['page_title'],$show->rs['rally']);
			Page::set_page_description($show->rs['page_description'],$show->rs['content']);
			Page::set_page_keywords($show->rs['page_keywords']);
			
			// $show->set_var('show_category_id',$show->rs['category_id']);
				
			Page::add_breadcrumb($show->rs['rally']);

			$content =  $show->show_form();

			Page::set_page_og($show->rs);
			return $content;
		}
		else
		{
			Main::error_404();
		}
	}
	function records_walk($url1,$url1_name,$url2,$url2_name,$url3,$url3_name,$entered,$content=false,$content_list=false) {
		// Això ho poso aquí així no cal posar-ho al tpl, simplement posar: <a href="<=$url1>" target="_blank"><=$url1_name></a>
		$ret['url1_name'] = $url1_name ? $url1_name : $url1;
		$ret['url2_name'] = $url2_name ? $url2_name : $url2;
		$ret['url3_name'] = $url3_name ? $url3_name : $url3;

		// si es llistat passa lavariable content
		if ( $content !== false ) {
			// $ret['content']=$content_list?$content_list:add_dots('content', $content,$this->config['add_dots_length']);
			$ret['content'] = $content_list;
		}

		$entered_dt     = new DateTime( $entered );
		$now_dt         = new DateTime();
		$ret['is_past'] = $now_dt > $entered_dt;
			
		//$ret['url1']=str_replace('http://','',$url1);	
		//$ret['url1']=str_replace('http://','',$url1);
		return $ret;
	}
	function set_racing_fields(){
		if (!$this->config['show_entered_time'])
		{
			$this->set_field('entered', 'type', 'date');
		}
	}
}
?>