<script language="JavaScript" src="/admin/jscripts/validacio.js?v=<?=$version?>"></script>
<script language="JavaScript" src="/admin/modules/admintotal/jscripts/functions.js?v=<?=$version?>"></script>
<form name="theForm" method="post" action="<?=$link_action?>" target='save_frame'>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td><table width="100%" cellpadding="4" cellspacing="0">
                <tr>
                  <td class="formCaptionTitle">
<strong>Opcions adicionals:</strong>
<br />
change_field_name - required - accept - checked_table - checked_fields - checked_condition - not_sortable - checked - group - td_class<br />
<br />
<br /><strong>Taula &quot;<?=$tool2?>__<?=$tool_section2?>&quot;</strong>
                  &nbsp;&nbsp;<?=Debug::open_link("admin/modules/$tool2/${tool2}_${tool_section2}_config.php","config") ?>
                  &nbsp;&nbsp;<?=Debug::open_link("admin/modules/$tool2/languages/cat.php",'cat') ?>
                  &nbsp;&nbsp;<?=Debug::open_link("public/modules/$tool2/languages/cat.php",'cat public') ?>
                  &nbsp;&nbsp;<?=Debug::open_link("admin/modules/$tool2/languages/spa.php",'spa') ?>
                  &nbsp;&nbsp;<?=Debug::open_link("public/modules/$tool2/languages/spa.php",'spa public') ?>
                  &nbsp;&nbsp;<?=Debug::open_link("admin/modules/$tool2/languages/eng.php",'eng') ?>
                  &nbsp;&nbsp;<?=Debug::open_link("public/modules/$tool2/languages/eng.php",'eng public') ?>
                  &nbsp;&nbsp;<?=Debug::open_link("admin/modules/$tool2/languages/fra.php",'fra') ?>
                  &nbsp;&nbsp;<?=Debug::open_link("public/modules/$tool2/languages/fra.php",'fra public') ?>
                  &nbsp;&nbsp;<?=Debug::open_link("admin/modules/$tool2/languages/deu.php",'deu') ?>
                  &nbsp;&nbsp;<?=Debug::open_link("public/modules/$tool2/languages/deu.php",'deu public') ?>
                  </td>
                </tr>
              </table>
              <table width="100%" cellpadding="0" cellspacing="0" class="listRecord">
                <?foreach($loop as $l): extract ($l)?>
                <tr>
                  <td width="150" valign="top" class="formsCaption"><span class="formsHor">
                    <input name="fields_included[<?=$name?>]" type="checkbox" onClick="disable_inputs(document.theForm,this,'fields[<?=$name?>]')" value="1"<?=$fields_included?>>
                    <strong class="formsCaption">
                    <?=$name?>
                    </strong><strong class="resaltat"> <br>
                    <?=$language?>
                    </strong></span></td>
                  <td class="forms"><table width="100%" border="0" cellpadding="2" cellspacing="1">
                      <tr>
                        <td class="listItem"><strong>Tipus</strong></td>
                        <td class="listItem"><strong>Activat?</strong></td>
                        <td class="listItem"><strong>Surt al formulari?</strong></td><?if (!$dconfig_section):?>
                        <td class="listItem"><strong>Surt al llistat?</strong></td><?endif //dconfig_section?>
                        <td class="listItem"><strong>Obligatori?</strong></td>
                        <td class="listItem"><strong>Ordre</strong></td>
                        <td class="listItem"><strong>Valor per defecte</strong></td>
                        <td class="listItem"><strong>Sobrescriure al guardar </strong></td><?if ($dconfig_section):?>
                        <td class="listItem"><strong>Grup del config </strong></td><?endif //dconfig_section?>
					  </tr>
                      <tr>
                        <td valign="top"><select name="fields[<?=$name?>][type]" onChange="show(this.options[selectedIndex].value,'<?=$name?>');"<?=$disabled?>>
                            <?=$type?>
                          </select>
                          <input name="field[<?=$name?>]" type="hidden" value="<?=$name?>"></td>
                        <td valign="top"><select name="fields[<?=$name?>][enabled]"<?=$disabled?>>
                            <?=$enabled?>
                          </select></td>
                        <td valign="top" nowrap="nowrap">admin:
                          <select name="fields[<?=$name?>][form_admin]"<?=$disabled?>>
                            <?=$form_admin?>
                          </select>
                          <br /><?if (!$dconfig_section):?>
                          public:&nbsp;
                          <select name="fields[<?=$name?>][form_public]"<?=$disabled?>>
                            <?=$form_public?>
                          </select><?endif //dconfig_section?></td><?if (!$dconfig_section):?>
                        <td valign="top" nowrap="nowrap">admin:
                          <select name="fields[<?=$name?>][list_admin]"<?=$disabled?>>
                            <?=$list_admin?>
                          </select>
                          <br />
                          public:&nbsp;
                          <select name="fields[<?=$name?>][list_public]"<?=$disabled?>>
                            <?=$list_public?>
                          </select></td><?endif //dconfig_section?>
                        <td valign="top"><input type="checkbox" name="fields[<?=$name?>][required]" value="1"<?=$required?><?=$disabled?>></td>
                        <td valign="top"><input name="fields[<?=$name?>][order]" type="text" value="<?=$order?>" size="2" maxlength="3"<?=$disabled?>></td>
                        <td valign="top"><input name="fields[<?=$name?>][default_value]" type="text" value="<?=$default_value?>" size="10" maxlength="100"<?=$disabled?> /></td>
                        <td valign="top"><input name="fields[<?=$name?>][override_save_value]" type="text" value="<?=$override_save_value?>" size="10" maxlength="100"<?=$disabled?> /></td><?if ($dconfig_section):?>
                        <td valign="top"><input name="fields[<?=$name?>][group]" type="text" value="<?=$group?>" size="10" maxlength="100"<?=$disabled?> /></td><?endif //dconfig_section?>
                      </tr>
                    </table></td>
                </tr>
                <tr class="listItemOdd">
                  <td valign="top" class="formsCaption"></td>
                  <td class="forms"><table border="0" cellpadding="0" cellspacing="0">
                      <tr>
                        <td valign="top"><table border="0" cellspacing="0" cellpadding="0">
                            <tr>
                              <td class="formsCaption">class&nbsp;</td>
                              <td><input name="fields[<?=$name?>][class]" type="text" value="<?=$class?>"<?=$disabled?> /></td>
                              <td class="formsCaption">javascript&nbsp;</td>
                              <td><input name="fields[<?=$name?>][javascript]" type="text" value="<?=$javascript?>"<?=$disabled?> /></td>
                            </tr>
                          </table></td>
                        <td valign="top"><table border="0" cellpadding="0" cellspacing="0">
                            <tr id="<?=$name?>text" style="display:<?=$dtext?>;">
                              <td><table border="0" cellpadding="0" cellspacing="0">
                                  <tr>
                                    <td class="formsCaption">size&nbsp;</td>
                                    <td><input name="fields[<?=$name?>][text_size]" type="text" value="<?=$text_size?>"<?=$disabled?> /></td> <td class="formsCaption">maxlength&nbsp;</td><td><input name="fields[<?=$name?>][text_maxlength]" type="text" value="<?=$text_maxlength?>"<?=$disabled?> /></td>
                                  </tr>
                                </table></td>
                            </tr>
                            <tr id="<?=$name?>textarea" style="display:<?=$dtextarea?>;">
                              <td><table border="0" cellspacing="0" cellpadding="0">
                                  <tr>
                                    <td class="formsCaption">cols&nbsp;</td>
                                    <td><input name="fields[<?=$name?>][textarea_cols]" type="text" value="<?=$textarea_cols?>"<?=$disabled?> /></td>
                                    <td class="formsCaption">rows&nbsp;</td>
                                    <td><input name="fields[<?=$name?>][textarea_rows]" type="text" value="<?=$textarea_rows?>"<?=$disabled?> /></td>
                                  </tr>
                                </table></td>
                            </tr>
                            <tr id="<?=$name?>checkbox" style="display:<?=$dcheckbox?>;">
                              <td><table border="0" cellspacing="0" cellpadding="0">
                                  <tr>
                                    <td class="formsCaption">checked</td>
                                    <td><input type="checkbox" name="fields[<?=$name?>][checkbox_checked]" value="1"<?=$checkbox_checked?><?=$disabled?> /></td>
                                  </tr>
                                </table></td>
                            </tr>
                            <tr id="<?=$name?>select" style="display:<?=$dselect?>;">
                              <td><table border="0" cellspacing="0" cellpadding="0">
                                  <tr>
                                    <td class="formsCaption">caption&nbsp;</td>
                                    <td><input name="fields[<?=$name?>][select_caption]" type="text" value="<?=$select_caption?>"<?=$disabled?> /></td>
                                    <td class="formsCaption">size&nbsp;</td>
                                    <td><input name="fields[<?=$name?>][select_size]" type="text" value="<?=$select_size?>"<?=$disabled?> /></td>
                                  </tr>
                                </table></td>
                            </tr>
                          </table></td>
                      </tr>
                    </table></td>
                </tr>
                <tr id="<?=$name?>config_select" style="display:<?=$dconfig_select?>;">
                  <td valign="top" class="formsCaption">&nbsp;</td>
                  <td class="forms"><table border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td class="formsCaption">taula relacionada&nbsp;</td>
                        <td><input name="fields[<?=$name?>][select_table]" type="text" value="<?=$select_table?>" size="100"<?=$disabled?>></td> </tr>
                            <tr>
                        <td class="formsCaption"><p>camps<br />
                          enum: camp1,camp2,camp3<br />
                          taula rel: inmo__category.category_id,category
                          &nbsp;</p></td>
                        <td valign="top"><input name="fields[<?=$name?>][select_fields]" type="text" value="<?=$select_fields?>" size="100"<?=$disabled?>></td></tr>
                            <tr>
                        <td class="formsCaption">condicio&nbsp;</td>
                        <td><input name="fields[<?=$name?>][select_condition]" type="text" value="<?=$select_condition?>" size="100"<?=$disabled?>></td>
                      </tr>
                    </table></td>
                </tr>
                <?endforeach //$loop?>
              </table><div class="sep_dark"></div>
              <table width="100%" cellpadding="4" cellspacing="0">
                <tr>
                  <td class="formCaptionTitle"><strong>Llistats</strong></td>
                </tr>
              </table>
              <table width="100%" cellpadding="4" cellspacing="0" class="formstbl">
                <tr>
                  <td width="150" class="formsCaption"><strong>Nombre columnes </strong></td>
                  <td class="forms"> <input name="list[cols]" type="text" value="<?=$list_cols?>" />
                    <input name="language_fields" type="hidden" value="<?=$language_fields?>">
                  </td>
                </tr>
              </table><div class="sep_dark"></div>
              <table width="100%" cellpadding="4" cellspacing="0">
                <tr>
                  <td class="formCaptionTitle"><strong>Formularis</strong></td>
                </tr>
              </table>
              <table width="100%" cellpadding="4" cellspacing="0" class="formstbl">
                <tr>
                  <td width="150" class="formsCaption"><strong>Nombre columnes </strong></td>
                  <td class="forms"> <input name="form[cols]" type="text" value="<?=$form_cols?>" />
                  </td>
                </tr>              
                <tr>
                  <td width="150" class="formsCaption"><strong>Camps pel titol del formulari d'imatges </strong></td>
                  <td class="forms">
                    <input name="form[images_title]" type="text" value="<?=$form_images_title?>" />                  </td>
                </tr></table>
              <table width="100%" cellpadding="4" cellspacing="2" class="formsButtonstbl">
                <tr>
                  <td align="center" class="formsButtons"><strong><a class="botoOpcions" href="javascript:document.theForm.submit();">Guardar</a></strong> </td>
                </tr>
              </table></td>
          </tr>
        </table></td>
    </tr>
</table>
  </form>
<p style="padding-left:30px;"><strong>form_admin',
'form_public',
'list_admin' ,
'list_public</strong><br />
  '0' =&gt; 'No, per&ograve; surt a consulta',<br />
  'no' =&gt; 'No i fora de la consulta',<br />
  'text' =&gt; 'Si, com a texte',<br />
  'input' =&gt; 'Si, com a input',<br />
'out' =&gt; 'Si per&ograve; fora de la consulta'</p>
<p style="padding-left:30px;"> <strong>Tipus</strong><br />
  'text' =&gt; 'Texte',<br />
'password' =&gt; 'Password',<br />
'email' =&gt; 'E-mail',<br />
'textarea' =&gt; 'textarea',<br />
'int' =&gt; 'Nombre',<br />
'decimal' =&gt; 'Nombre decimal',<br />
'currency' =&gt; 'Moneda',<br />
'date' =&gt; 'data',<br />
'datetime' =&gt; 'data hora',<br />
'account' =&gt; 'N. compte',<br />
'nif' =&gt; 'Nif',<br />
'names' =&gt; 'nomcognoms',<br />
'time' =&gt; 'hora',<br />
'hidden' =&gt; 'hidden',<br />
'select' =&gt; 'select',<br />
'radios' =&gt; 'radios',<br />
'radios2' =&gt; 'radios a sota',<br />
'checkbox' =&gt; 'checkbox',<br />
'checkboxes' =&gt; 'checkboxes',<br />
'html' =&gt; 'html',<br />
'videoframe' =&gt; 'video en flash',<br />
'none' =&gt; 'none')</p>
