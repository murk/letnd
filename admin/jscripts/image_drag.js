$(document).ready(
	function () {
		start_images_drag();
		if ($.cookie('is_image_alt_visible')=='true') {
			toggle_image_alt();
			$.cookie('is_image_alt_visible','true');
		}
		init_image_uploader ();
	}
);

var order_numbers;
var select_numbers;

function start_images_drag(){
		order_numbers = new Array();
		select_numbers = new Array();
		if (document.getElementById('im_container_0')){	// si hi ha una imatge					
			//if (document.getElementById('im_container_1')){ // si hi ha més d'una imatge ( ho trec per que vagi el clic en una sola imatge
			$('#sort').sortable(
				{
					forcePlaceholderSize: true,
					helper: 'clone',
					placeholder: 	'sortableitemPlaceholder',
					opacity: 		0.5,
					revert:			200,
					tolerance:		'pointer',
					handle:         'div.sortableitemHandle',
					items: 'li',
					scroll: true,
					start : function()
					{
						if (document.imagesFrame) document.imagesFrame.document.getElementById('image_upload_div').style.display='none';
						start_order_images();
					},
					stop : function()
					{
						stop_order_images();
					}
				}
			)
			//}
			$('#sort').selectable(
				{
					autoRefresh:true,
					filter : 'li.sortableitem',
					stop:     function(obj, ui){ change_select_images(); }
				}
			);
			init_images();
		}
}

function init_images()
{
	var i = 0;
	while (document.getElementById('im_bg_' + i)) {
		var id_current = $('input[name=image_id_' + i + ']').val();
		order_numbers[i] = i;// li poso l'ordre de nou, així encara que estigui mal guardat es guardarà amb l'ordre que es veu
		select_numbers[id_current] = i;
		// iniciar mouse actions
		$('#im_bg_' + i)
			.bind("mouseover", {n: i}, function (event) {
				image_hover(document.getElementById('im_container_' + event.data.n), 'over');
			})
			.bind("mouseout", {n: i}, function (event) {
				image_hover(document.getElementById('im_container_' + event.data.n), 'out');
			})
			.bind("click", {n: i}, function (event) {
				open_viewer(this, event.data.n);
			});
		$('#select_bg_' + i)
			.bind("mouseover", {n: i}, function (event) {
				select_hover(document.getElementById('im_container_' + event.data.n), 'over');
			})
			.bind("mouseout", {n: i}, function (event) {
				select_hover(document.getElementById('im_container_' + event.data.n), 'out');
			});
		i++;
	}
	//$.dump(select_numbers);
}

function image_hover(obj, action){
	obj.className = 'sortableitem_' + action;
}
function select_hover(obj, action){
	obj.className = 'selectableitem_' + action;
}

var inputs_vals = new Array();
function start_order_images(){
	// guardar els valors del checbox i principal abans d'arrastrar ja que els reseteja	 
	i = 0;
	temp = new Array();
	inputs_vals['main_image'] = $('input[name="main_image"]:checked"').val();
	while(document.getElementById('im_container_'+i))
	{
		id_current = $('input[name=image_id_' + i + ']').val();
		temp[i] = $('input[name="selected[' + i + ']"]').attr('checked');
		i++;
	}
	inputs_vals['selected'] = temp;
	//$.dump(inputs_vals);
}
function stop_order_images(){
	ob = $('#sort').sortable('toArray');
	
	// passo ordre
	i = 0;
	$.each(ob, function(key, val) {	
		if (val){
		$('input[name="ordre[' + val + ']"]').val(order_numbers[i]);
		i++;	
		}
	});

	 // torna imatge a selectableitem_out, despres de moure-la no fa el onmouseout
	 $.each(order_numbers, function(key, val) {
		image_hover(document.getElementById('im_container_' + key), 'out');
	 });
	 // tornar el checbox i principal els valors abans de arrastrar
	 $.each(inputs_vals['selected'], function(key, val) {
		$('input[name="selected[' + key + ']"]').attr('checked',val);
		
		if ($('input[name=main_image]:eq('+key+')').val() == inputs_vals['main_image']) {$('input[name=main_image]:eq('+key+')').attr('checked',true)}
	 });
}
function change_select_images(){
	 dp('stopped');
	 $.each(order_numbers, function(key, val) {
		$('input[name="selected[' + key + ']"]').removeAttr('checked');
		dp('id',key);
	 });
	 
	 $(".ui-selected").each(function(key, val){
		$('input[name="selected[' + select_numbers[val.id] + ']"]').attr('checked',true);
		dp('id2',select_numbers[val.id]);
	});
	selecting = true;
}
// amb la clase selectables si faig click directament al checkbox no queda seleccionat
selecting = false;
function checkbox_onclick(obj){
	return false
	obj.checked = false;
	document.getElementById(obj.value).click();
	if (selecting)
	{
		//obj.checked = true;
		//selecting = false;
	}
}
function delete_selected_images(){	
	// s'ha de fer un array d'imatges amb ids i treure totes les 'i'
	/*i = 0;
	 while(document.getElementById('im_container_'+i))
	{
		id_current = $('input[name=image_id_' + i + ']').val();
			$('#im_bg_'+i).unbind();
			$('#select_bg_'+i).unbind();
		if ($('input[name="selected[' + i + ']"]').attr('checked')){
			$('#' + id_current).remove();
			//order_numbers.splice(i);
			//delete select_numbers[id_current];
		}
		i++;
	}
	order_numbers = new Array();
	select_numbers = new Array();
	init_images();
	*/
}
function toggle_image_alt(){
	$('#ajax_list').toggleClass('captions');
	$('.veure-titols').toggle();
	$('.amagar-titols').toggle();
	$.cookie('is_image_alt_visible',$.cookie('is_image_alt_visible') != 'true');
}

function init_image_uploader() {

	if ($.cookie('use_java_uploader') == 'true') {
		show_java_uploader(true)
	}
	else if ($.cookie('use_java_uploader') == 'false') {
		hide_java_uploader(true);
	}
	else if ($.cookie('use_java_uploader') === null) {
		if (navigator.javaEnabled() && $.cookie('browser')!='Chrome') {
			show_java_uploader(true);
			$.cookie('use_java_uploader', true);
		}
		else {
			hide_java_uploader(true);
			$.cookie('use_java_uploader', false);
		}
	}

	$('#show_java_uploader').change(function () {
		if ($(this).is(":checked")) {
			show_java_uploader(false);
			$.cookie('use_java_uploader', true);
		} else {
			hide_java_uploader(false);
			$.cookie('use_java_uploader', false);
		}
	});

	add_form_dropzone(parent_id, im_admin_thumb_w, im_admin_thumb_h);

}
function show_java_uploader(change_checkbox) {
	dp('show');
	$('#imagesFrame').show();
	$('.dropzone').hide();

	if (change_checkbox) $('#show_java_uploader').prop('checked', true);
}
function hide_java_uploader(change_checkbox) {
	dp('hide');
	$('#imagesFrame').hide();
	$('.dropzone').css('display','block');

	if (change_checkbox) $('#show_java_uploader').prop('checked', false);
}