<?php
// menus eina
if (!defined('LASSDIVE_MENU_ACTIVITAT')){
	define('LASSDIVE_MENU_ACTIVITAT', 'Activitats');
	define('LASSDIVE_MENU_ACTIVITAT_NEW', 'Entrar nova activitat');
	define('LASSDIVE_MENU_ACTIVITAT_LIST', 'Llistar i editar activitats');
	define('LASSDIVE_MENU_ACTIVITAT_LIST_PACKS', 'Llistar especials');
	define('LASSDIVE_MENU_ACTIVITAT_BIN', 'Paperera de reciclatge');
	define('LASSDIVE_MENU_IDIOMA', 'Idioma');
	define('LASSDIVE_MENU_IDIOMA_NEW', 'Entrar nou idioma');
	define('LASSDIVE_MENU_IDIOMA_LIST', 'Llistar idiomes');

	define('LASSDIVE_MENU_LEVEL', 'Nivells');
	define('LASSDIVE_MENU_LEVEL_NEW', 'Entrar nou nivell');
	define('LASSDIVE_MENU_LEVEL_LIST', 'Llistar nivells');

	define('LASSDIVE_MENU_MODALITY', 'Modalitats');
	define('LASSDIVE_MENU_MODALITY_NEW', 'Entrar nova modalitat');
	define('LASSDIVE_MENU_MODALITY_LIST', 'Llistar modalitats');

	define('LASSDIVE_MENU_ELEMENT','Inclòs / no inclòs');
	define('LASSDIVE_MENU_ELEMENT_NEW','Nou ítem');
	define('LASSDIVE_MENU_ELEMENT_LIST','Llistar ítems');

	define('LASSDIVE_MENU_PRICE', 'Preus');
	define('LASSDIVE_MENU_PRICE_NEW', 'Entrar nou preu');
	define('LASSDIVE_MENU_PRICE_LIST', 'Llistar preus');
	define('LASSDIVE_MENU_PRICE_BIN', 'Paperera de reciclatge');

	define('LASSDIVE_MENU_CATEGORY', 'Categories');
	define('LASSDIVE_MENU_CATEGORY_NEW', 'Entrar nova categoria');
	define('LASSDIVE_MENU_CATEGORY_LIST', 'Llistar categories');

	define('LASSDIVE_MENU_FAMILY', 'Calendari famílies');

	define('LASSDIVE_MENU_RESERVA', 'Reserves');
	define('LASSDIVE_MENU_RESERVA_LIST', 'Llistar reserves');
	define('LASSDIVE_MENU_RESERVA_BIN', 'Paperera de reciclatge');

	define('LASSDIVE_MENU_CENTRE', 'Centres');
	define('LASSDIVE_MENU_CENTRE_NEW', 'Entrar nou centre');
	define('LASSDIVE_MENU_CENTRE_LIST', 'Llistar centres');
	define('LASSDIVE_MENU_ASSIGNCENTRE', 'Assignar productes');

	define('LASSDIVE_MENU_OPERATOR', 'Operador');
	define('LASSDIVE_MENU_OPERATOR_NEW', 'Entrar nou operador');
	define('LASSDIVE_MENU_OPERATOR_LIST', 'Llistar operadors');
}

$gl_messages_activitat['no_category']='Per poder insertar notícies hi ha d\'haver almenys una categoria';

// captions seccio
$gl_caption_activitat['c_see_activities'] = 'Listar actividades';
$gl_caption_activitat['c_edit_calendar'] = 'Editar calendario';
$gl_caption_activitat['c_see_products'] = 'Listar productos';
$gl_caption_activitat['c_activitat_title'] = 'Títol';
$gl_caption_activitat['c_parent_id'] = 'Pertenece a';
$gl_caption_activitat['c_prepare'] = 'En preparación';
$gl_caption_activitat['c_review'] = 'Para revisar';
$gl_caption_activitat['c_public'] = 'Público';
$gl_caption_activitat['c_archived'] = 'Archivado';
$gl_caption_activitat['c_title'] = 'Título';
$gl_caption_activitat['c_subtitle'] = 'Subtítulo';
$gl_caption_activitat['c_content'] = 'Descripción';
$gl_caption_activitat['c_entered'] = 'Creada';
$gl_caption_activitat['c_modified'] = 'Modificada';
$gl_caption_activitat['c_start_date'] = 'Fecha inicio';
$gl_caption_activitat['c_end_date'] = 'Fecha final';
$gl_caption_activitat['c_minimum_age'] = 'Edad mínima';
$gl_caption_activitat['c_maximum_person'] = 'Máximo personas';
$gl_caption_activitat['c_status'] = 'Estado';
$gl_caption_activitat['c_category_id'] = 'Categoría';
$gl_caption_activitat['c_filter_category_id'] = 'Todas les categorías';
$gl_caption_activitat['c_filter_status'] = 'Todos los estados';
$gl_caption_activitat['c_filter_in_home'] = 'Todos, home o no';
$gl_caption_activitat['c_filter_parent_id'] = 'Todas las categorías';
$gl_caption_activitat['c_ordre'] = 'Orden';
$gl_caption_activitat['c_weekday_id'] = 'Días';
$gl_caption_activitat['c_price'] = 'Precio';
$gl_caption_activitat['c_price2'] = 'Precio 2';
$gl_caption_activitat['c_price3'] = 'Precio 3';
$gl_caption_activitat['c_price4'] = 'Precio 4';
$gl_caption_activitat['c_price5'] = 'Precio 5';
$gl_caption_activitat['c_price6'] = 'Precio 6';
$gl_caption_activitat['c_price7'] = 'Precio 7';
$gl_caption_activitat['c_price8'] = 'Precio 8';
$gl_caption_activitat['c_price9'] = 'Precio 9';
$gl_caption_activitat['c_price10'] = 'Precio 10';
$gl_caption_activitat['c_price_text'] = 'Precio text';
$gl_caption_activitat['c_price_text2'] = 'Precio text 2';
$gl_caption_activitat['c_price_text3'] = 'Precio text 3';
$gl_caption_activitat['c_price_text4'] = 'Precio text 4';
$gl_caption_activitat['c_price_text5'] = 'Precio text 5';
$gl_caption_activitat['c_price_text6'] = 'Precio text 6';
$gl_caption_activitat['c_price_text7'] = 'Precio text 7';
$gl_caption_activitat['c_price_text8'] = 'Precio text 8';
$gl_caption_activitat['c_price_text9'] = 'Precio text 9';
$gl_caption_activitat['c_price_text10'] = 'Precio text 10';
$gl_caption_activitat['c_and'] = 'y';
$gl_caption_activitat['c_or'] = 'o';

$gl_caption_category['c_category'] = 'Categoría';
$gl_caption_category['c_activitat_button'] = 'Ver actividades';
$gl_caption_category['c_ordre'] = 'Orden';
$gl_caption_image['c_name'] = 'Nombre';
$gl_caption['c_idioma'] = $gl_caption['c_idioma_id'] = 'Idioma';
$gl_caption_activitat['c_url1_name']='Nombre URL(1)';
$gl_caption_activitat['c_url1']='URL(1)';
$gl_caption_activitat['c_url2_name']='Nombre URL(2)';
$gl_caption_activitat['c_url2']='URL(2)';
$gl_caption_activitat['c_url3_name']='Nombre URL(3)';
$gl_caption_activitat['c_url3']='URL(3)';
$gl_caption_activitat['c_videoframe']='Vídeo (youtube, metacafe...)';
$gl_caption_activitat['c_file']='Archivos';
$gl_caption_activitat['c_in_home']='Surt a la Home';
$gl_caption_activitat['c_in_home_0']='No surt a la Home';
$gl_caption_activitat['c_in_home_1']='Surt a la Home';
$gl_caption_activitat_list['c_in_home_0']='';
$gl_caption_activitat_list['c_in_home_1']='Si';
$gl_caption_activitat['c_destacat']='Notícia destacada';
$gl_caption_activitat['c_destacat_0']='No destacada';
$gl_caption_activitat['c_destacat_1']='Destacada';

$gl_caption_activitat['c_timetable']='Horario';
global $week_names;
$gl_caption_activitat['c_timetable1']=$week_names[0];
$gl_caption_activitat['c_timetable2']=$week_names[1];
$gl_caption_activitat['c_timetable3']=$week_names[2];
$gl_caption_activitat['c_timetable4']=$week_names[3];
$gl_caption_activitat['c_timetable5']=$week_names[4];
$gl_caption_activitat['c_timetable6']=$week_names[5];
$gl_caption_activitat['c_timetable7']=$week_names[6];
$gl_caption['c_level_id'] = $gl_caption['c_level'] = 'Nivel';
$gl_caption['c_modality_id'] = $gl_caption['c_modality'] = 'Modalidad';
$gl_caption['c_needed_material'] = 'Material necesario';
$gl_caption['c_includes'] = 'Incluye';
$gl_caption['c_excludes'] = 'No incluye';
$gl_caption_element['c_element'] = 'Ítem';

$gl_caption_activitat['c_form_level1_title_minimum_age']='Características';
$gl_caption_activitat['c_form_level1_title_activitat']='Descripciones';
$gl_caption_activitat['c_form_level1_title_timetable']='Horarios';
$gl_caption_activitat['c_form_level1_title_price']='Precios';
$gl_caption_activitat['c_form_level1_title_needed_material']='Material';
$gl_caption_activitat['c_form_level1_title_file']='Archivos adjuntos';
$gl_caption_activitat['c_form_level1_title_page_title']='SEO';
$gl_caption_activitat['c_form_level2_title_entered']='Fechas';


$gl_caption['c_activitat_file_name'] = $GLOBALS['gl_caption']['c_file_name'];
$gl_caption['c_activitat_old_file_name'] = $GLOBALS['gl_caption']['c_old_file_name'];

$gl_caption_activitat['c_selected']='Actividades seleccionadas';

$gl_caption['c_ordre']='Orden';

// public
$gl_caption['c_activitat'] = $gl_caption['c_activitat_id'] = 'Actividad';
$gl_caption['c_activitat'] = 'Activitat ( Menú i llistat )';
$gl_caption_list['c_activitat'] = 'Activitat';
$gl_caption['c_price_id']='Precios';
$gl_caption['c_price_month']='Precio mensual';
$gl_caption['c_price_quarter']='Precio trimestral';


/* Centres*/
$gl_caption['c_centre']    = 'Centro';
$gl_caption_centre['c_status']   = 'Estado';
$gl_caption_centre['c_prepare']  = 'En preparación';
$gl_caption_centre['c_public']   = 'Público';
$gl_caption_centre['c_archived'] = 'Archivado';

$gl_caption['c_address']   = 'Dirección';
$gl_caption['c_poblacio']   = 'Población';
$gl_caption['c_zip']       = 'Código postal';
$gl_caption_centre['c_admin_id']       = 'Administradores';
$gl_caption_centre['c_user_id']       = 'Usuarios';

/* Operadors */
$gl_caption['c_operator'] = $gl_caption['c_operator_id'] = 'Operador';
$gl_caption_list['c_operator_id'] = 'Op.';
$gl_caption['c_telephone'] = 'Teléfono';
$gl_caption['c_operator_select_caption'] = "Todos";
$gl_caption['c_operator_select_operator'] = "Con operador";
$gl_caption['c_operator_select_no_operator'] = "Sin operador";

$gl_caption['c_centreproduct'] = "Productos";
$gl_caption['c_image'] = "Imagen";
$gl_caption['c_family_id'] = "Familia";
$gl_caption['c_subfamily_id'] = "Subfamilia";
$gl_caption['c_product_title'] = "Producto";
$gl_caption['c_product_subtitle'] = "Subtítulo";
$gl_caption['c_ref'] = "Referencia";
$gl_caption['c_status'] = "Estado";
$gl_caption['c_prepare'] = 'En preparación';
$gl_caption['c_review'] = 'Para revisar';
$gl_caption['c_onsale'] = 'Para vender';
$gl_caption['c_nostock'] = 'Fuera de stock';
$gl_caption['c_nocatalog'] = 'Descatalogado';
$gl_caption['c_archived'] = 'Archivado';
$gl_caption['c_max_quantity'] = "Cantidad máxima";
$gl_caption['c_quantityconcept_id'] = "Concepto";

$gl_caption['c_form_level1_title_admin_id'] = "Usuarios";
$gl_caption['c_form_level1_title_centreproduct'] = "Actividades";

$gl_caption_assigncentre['c_centres'] = "Centros";
$gl_caption['c_centre_max_quantity'] = "Max";

/* Reserves */
$gl_caption['c_reservaitem_id'] = "";
$gl_caption['c_lassdive_product_date'] = "Dia solicitado";
$gl_caption['c_reservaitem_date'] = "Dia asignado";
$gl_caption['c_view_format_order'] = "Reservas";
$gl_caption['c_view_format_orderitems'] = "Actividades";
$gl_caption['c_product_id'] = "Actividad";
$gl_caption['c_lassdive_product_basetax'] = "Precio";

$gl_caption['c_has_no_timetable'] = "Horarios";
$gl_caption['c_has_no_timetable_'] = "Todos";
$gl_caption['c_has_no_timetable_no_time'] = "No horario";
$gl_caption['c_has_no_timetable_no_complement'] = "No compl.";

$gl_caption['c_date_search_caption'] = "Buscar fecha a";
$gl_caption['c_date_search'] = "Fecha";
$gl_caption['c_timetable'] = "Horario";
$gl_caption['c_date_search_order'] = "Reserva";
$gl_caption['c_date_search_orderitems'] = "Actividad";

$gl_caption['c_is_done_caption'] = "Hecho / Por hacer";
$gl_caption['c_is_done'] = "Hecho";
$gl_caption['c_is_done_'] = "Todos";
$gl_caption['c_is_done_search_0'] = "No";
$gl_caption['c_is_done_search_1'] = "Hecho";
$gl_caption['c_is_done_0'] = "No";
$gl_caption['c_is_done_1'] = "Sí";
$gl_caption['c_orderitems'] = "Actividades";

$gl_caption['c_start_time'] = "Empieza";
$gl_caption['c_end_time'] = "Acaba";
$gl_caption['c_centre_id'] = "Centro";
$gl_caption['c_user_id'] = "Instructor";
$gl_caption['c_is_done'] = "Hecho";

$gl_caption['c_source_id'] = "Procedencia";
$gl_caption['c_source_'] = "Todas";
$gl_caption['c_source_caption'] = "Procedencia";
$gl_caption['c_source_web'] = "Web";
$gl_caption['c_source_centre'] = "Centro";

$gl_caption['c_no_center_with_activity'] = "No hay ningún centro que ofrezca esta actividad";
$gl_caption['c_day_is_busy'] = "No se pueden hacer reservas para este día";
$gl_caption['c_selected_timetables'] = "horario seleccionado";
// $gl_caption['c_all_timetables_already_selected'] = "Ja s'han seleccionat tots els horaris";
$gl_caption['c_select_ok'] = "Hecho!";
$gl_caption['c_select_canviar'] = "Canviar horario";
$gl_caption['c_error_guardar'] = "Ha habido un error al guardar";
$gl_caption['c_button_time'] = "";
$gl_caption['c_selecciona'] = "selecciona...";
$gl_caption['c_lassdive_login'] = "Usuario";
$gl_caption['c_lassdive_logon'] = "Entrar";

$gl_caption['c_reserva_form_activitats'] = "Actividades";
$gl_caption['c_reserva_discount'] = "Descuento";
$gl_caption['c_reserva_deposit'] = 'Paga y señal';

$gl_caption['c_total'] = "Total";

$gl_caption['c_product_original_title'] = "Prod. original";

// Calendari
$gl_caption['c_view_week'] = "Semana";
$gl_caption['c_view_month'] = "Mes";


// LOGS
$gl_caption['c_mysql'] = "Sql";
$gl_caption['c_user_id'] = "Usuari";
$gl_caption['c_entered'] = "Data";
$gl_caption['c_session'] = "Sessió";
$gl_caption['c_cookie'] = "Cookie";
$gl_caption['c_uri'] = "Adreça";
$gl_caption['c_referer'] = "Provinent";
$gl_caption['c_query_string'] = "Get";
$gl_caption['c_backtrace'] = "Backtrace";
$gl_caption['c_vars'] = "Variables";