<?php
global $gl_caption_admin,
       $gl_caption_public,
       $gl_caption_newsletter,
       $gl_caption_contact_public,
       $gl_caption_inmo_admin,
       $gl_caption_inmo_public,
       $gl_caption_booking_admin,
       $gl_caption_booking_public,
       $gl_caption_translation,
       $gl_caption_custumer,
       $gl_caption_page,
       $gl_caption_save,
       $gl_caption_product_admin,
       $gl_caption_product_public,
       $gl_caption_utility;
// menus eina
if ( ! defined( 'WEB_MENU_TITLE' ) ) {
	define( 'WEB_MENU_TITLE', 'Titulo web' ); //Nom apareix dalt internet explorer
	define( 'WEB_MENU_ROBOTS', 'Tags de la web' ); //Paraules dels tags
	define( 'WEB_MENU_SAVE', 'Copias de seguridad' ); //Paraules dels tags
	define( 'WEB_MENU_SAVE_LIST', 'Realizar copia' ); //Paraules dels tags
	define( 'WEB_MENU_TRANSLATION', 'Traducciones' );
	define( 'WEB_MENU_TRANSLATION_LIST', 'Listar todas las traducciones' );
	define( 'WEB_MENU_PAGE_TRANSLATION_LIST', 'Listar apartados a traducir' );
	define( 'WEB_MENU_BACKOFFICE_SEARCH', 'Búsquedas de clients' );


	define( 'WEB_MENU_CONFIG', 'Configuración' );
	define( 'WEB_MENU_CONFIGADMIN_LIST', 'General parte privada' );
	define( 'WEB_MENU_CONFIGPUBLIC_LIST', 'General parte pública' );
	define( 'WEB_MENU_CONFIGINMOADMIN_LIST', 'Propiedades parte privada' );
	define( 'WEB_MENU_CONFIGINMOPUBLIC_LIST', 'Propiedades parte pública' );
	define( 'WEB_MENU_CONFIGCUSTUMER_LIST', 'Clientes' );
	define( 'WEB_MENU_CONFIGNEWSLETTER_LIST', 'Newsletter' );
	define( 'WEB_MENU_CONFIGUTILITY_LIST', 'Utilitades' );
	define( 'WEB_MENU_CONFIGMLS_LIST', 'MLS' );
	define( 'WEB_MENU_CONFIGCONTACT_PUBLIC_LIST', 'Contacto parte pública' );
	define( 'WEB_MENU_CONFIGCONTACT_ADMIN_LIST', 'Contacto parte privada' );

	define( 'WEB_MENU_CONFIGBOOKING_ADMIN_LIST', 'Reservas parte privada' );
	define( 'WEB_MENU_CONFIGBOOKING_PUBLIC_LIST', 'Reservas parte pública' );

	define( 'WEB_MENU_CONFIGPRODUCT_ADMIN_LIST', 'Productos parte privada' );
	define( 'WEB_MENU_CONFIGPRODUCT_PUBLIC_LIST', 'Productos parte pública' );

	define( 'WEB_MENU_CONFIGCLIENT_LIST', 'Frases parte pública' );

	if ( ! defined( 'WEB_MENU_BANNER' ) ) {
		define( 'WEB_MENU_BANNER', 'Banners' );
		define( 'WEB_MENU_BANNER_NEW', 'Nuevo banner' );
		define( 'WEB_MENU_BANNER_LIST', 'Listar banners' );
		define( 'WEB_MENU_BANNER_LIST_BIN', 'Papelera' );
	}

	define( 'WEB_MENU_PAGE', 'Edición de páginas' );
	define( 'WEB_MENU_PAGE_NEW', 'Nueva página' );
	define( 'WEB_MENU_PAGE_LIST', 'Listar páginas' );
	define( 'WEB_MENU_PAGE_LIST_BIN', 'Papelera' );

	define( 'WEB_MENU_GUESTBOOK', 'Libro de visitas' );
	define( 'WEB_MENU_GUESTBOOK_NEW', 'Nueva visita' );
	define( 'WEB_MENU_GUESTBOOK_LIST', 'Listar visitas' );
	define( 'WEB_MENU_GUESTBOOK_LIST_BIN', 'Papelera' );


	define( 'WEB_MENU_GALLERY', 'Galeries de imágenes' );
	define( 'WEB_MENU_GALLERY_NEW', 'Nueva galería' );
	define( 'WEB_MENU_GALLERY_LIST', 'Listar galerías' );
	define( 'WEB_MENU_GALLERY_LIST_BIN', 'Papelera' );


	define( 'WEB_MENU_PLACE', 'Oficinas' );
	define( 'WEB_MENU_PLACE_NEW', 'Nueva oficina' );
	define( 'WEB_MENU_PLACE_LIST', 'Listar oficinas' );
}

if ( ! defined( 'API_MENU_PAGINA' ) ) {
	// per poder banners fer desde api// per poder banners fer desde api
	define( 'API_MENU_PAGINA', 'Edición de páginas' );
	define( 'API_MENU_PAGINA_NEW', 'Entrar nueva página' );
	define( 'API_MENU_PAGINA_NEW_SECOND', 'Entrar nueva página secundaria' );
	define( 'API_MENU_PAGINA_LIST', 'Listar páginas' );
	define( 'API_MENU_PAGINA_LIST_SECOND', 'Listar páginas secundarias' );
	define( 'API_MENU_PAGINA_BIN', 'Papelera de reciclage' );
}

/*--------------- no traduit -------------------*/

$gl_caption_product_admin['c_group_15']     = 'Característiques configurables';
for ($i = 1; $i <= 20; $i++) {
	$gl_caption_config['c_others' . $i] = 'Característica ' . $i;
}
for ($i = 1; $i <= 20; $i++) {
	$gl_caption_config['c_others' . $i . '_type'] = 'Tipo';
}


$gl_caption_config['c_default_home_tool']         = 'Herramienta predeterminada';
$gl_caption_config['c_default_home_tool_section'] = 'Sección predeterminada';
$gl_caption_config['c_language']                  = 'Idioma predeterminado';
$gl_caption_config['c_home_max_results']          = 'Nombre de resultados en la página de inicio';
$gl_caption_config['c_page_slogan']               = 'Frase corporativa';
$gl_caption_public['c_page_title']                = 'Título de la web';
$gl_caption_config['c_page_phone']                = 'Teléfono';
$gl_caption_config['c_page_mobile']               = 'Móvil';
$gl_caption_config['c_page_fax']                  = 'Fax';
$gl_caption_config['c_page_address']              = 'Calle';
$gl_caption_config['c_page_numstreet']            = 'Número';
$gl_caption_config['c_page_flat']                 = 'Piso';
$gl_caption_config['c_page_door']                 = 'Puerta';
$gl_caption_config['c_page_postcode']             = 'Código postal';
$gl_caption_config['c_page_municipi']             = 'Población';
$gl_caption_config['c_page_provincia']            = 'Provincia';
$gl_caption_config['c_page_country']              = 'País';
$gl_caption_config['c_page_title']                = 'Título Intranet';
$gl_caption_config['c_default_menu_id']           = 'Menú por defecto';
$gl_caption_config['c_theme']                     = 'Tema';
$gl_caption_config['c_language']                  = 'Idioma';
$gl_caption_config['c_max_results']               = 'Número de resultados por página';
$gl_caption_config['c_max_page_links']            = 'Número de páginas a enseñar';
$gl_caption_config['c_add_dots_length']           = 'Máximo de palabras en las descripciones';
$gl_caption_config['c_date_created']              = 'Fecha de creación de la web';
$gl_caption_admin['c_group_1']                    = 'Opciones de los listados';
$gl_caption_public['c_group_2']                   = $gl_caption_admin['c_group_1'];
$gl_caption_config['c_templatehtml_id']           = 'Plantilla en html';
$gl_caption_config['c_templatetext_id']           = 'Plantilla de texto';
$gl_caption_config['c_group_1']                   = 'De';
$gl_caption_config['c_group_2']                   = 'Responder a';
$gl_caption_config['c_group_3']                   = 'Datos SMTP';
$gl_caption_config['c_group_4']                   = 'Dirección';
$gl_caption_config['c_group_5']                   = 'Datos empresa';
$gl_caption_config['c_company_name']              = 'Nombre empresa';
$gl_caption_config['c_company_cif']               = 'CIF / NIF';
$gl_caption_config['c_m_username']                = 'Nombre de usuaro';
$gl_caption_config['c_m_password']                = 'Contraseña';
$gl_caption_config['c_m_password_repeat']         = 'Repetir contraseña';
$gl_caption_config['c_m_host']                    = 'Servidor';
$gl_caption['c_custumer_table']                   = 'Tabla de direcciones';
$gl_caption['c_newsletter__custumer']             = 'newsletter__custumer';
$gl_caption['c_custumer__custumer']               = 'custumer__custumer';
$gl_caption['c_contact__contact']                 = 'contact__contact';
$gl_caption['c_product__customer']                = 'product__customer';
$gl_caption_newsletter['c_content_extra']         = 'Contenido inculuido';
$gl_caption_config['c_only_test']                 = 'Mode test';
$gl_caption_config['c_only_test_description']     = 'Si activas esta casilla, no se enviará ningún mail, solo se simulará el envio';

$gl_caption_config['c_text']              = 'texto';
$gl_caption_config['c_provincies']        = 'Provincias habituales';
$gl_caption_config['c_default_provincia'] = 'Provincia por defecto';
$gl_caption_config['c_default_comarca']   = 'Comarca por defecto';
$gl_caption_config['c_default_municipi']  = 'Municipio por defecto';
$gl_caption_config['c_units']             = 'Unidades';

$gl_caption_inmo_admin['c_group_1']      = 'Thumbnail admin';
$gl_caption_config['c_im_admin_thumb_w'] = 'anchura';
$gl_caption_config['c_im_admin_thumb_h'] = 'altura';
$gl_caption_config['c_im_admin_thumb_q'] = 'calidad';

$gl_caption_inmo_admin['c_group_2']  = 'Thumbnail público';
$gl_caption_inmo_public['c_group_2'] = $gl_caption_inmo_admin['c_group_2'];
$gl_caption_config['c_im_thumb_w']   = 'anchura';
$gl_caption_config['c_im_thumb_h']   = 'altura';
$gl_caption_config['c_im_thumb_q']   = 'calidad';

$gl_caption_inmo_admin['c_group_3']  = 'Imagen ficha pública';
$gl_caption_inmo_public['c_group_3'] = $gl_caption_inmo_admin['c_group_3'];
$gl_caption_config['c_im_details_w'] = 'anchura';
$gl_caption_config['c_im_details_h'] = 'altura';
$gl_caption_config['c_im_details_q'] = 'calidad';

$gl_caption_inmo_admin['c_group_4']  = 'Imagen grande público';
$gl_caption_inmo_public['c_group_4'] = $gl_caption_inmo_admin['c_group_4'];
$gl_caption_config['c_im_medium_w']  = 'anchura';
$gl_caption_config['c_im_medium_h']  = 'altura';
$gl_caption_config['c_im_medium_q']  = 'calidad';


$gl_caption_inmo_admin['c_group_5'] = 'Imagen impresión grande';
$gl_caption_config['c_im_big_w']    = 'anchura';
$gl_caption_config['c_im_big_h']    = 'altura';

$gl_caption_config['c_im_list_cols']        = 'Columnas imagen';
$gl_caption_config['c_property_auto_ref']   = 'Referéncia automàtica';
$gl_caption_config['c_menu_price']          = 'Valores del menú de precio';
$gl_caption_config['c_menu_price_rent']     = 'Valores del menú de precio alquiler';
$gl_caption_config['c_recent_time']         = 'Días para mostrar etiqueta de "nuevo immueble"';
$gl_caption_config['c_home_max_results']    = 'Número de destacados';
$gl_caption_config['c_home_title']          = 'Título de propiedades destacadas';
$gl_caption_config['c_html']                = 'html';
$gl_caption_config['c_from_address']        = 'E-mail';
$gl_caption_config['c_from_address_repeat'] = 'Repetir e-mail';
$gl_caption_config['c_from_name']           = 'Nombre';
$gl_caption_config['c_reply_to']            = 'E-mail';
$gl_caption_config['c_reply_to_repeat']     = 'Repetir e-mail';
$gl_caption_config['c_reply_to_name']       = 'Nombre';
$gl_caption_config['c_subject']             = 'Asunto';
$gl_caption_config['c_format']              = 'Formato';
$gl_caption_config['c_send_method']         = 'Forma de envio';
$gl_caption_config['c_CC']                  = 'Copia en cada dirección (CC)';
$gl_caption_config['c_BCC']                 = 'Copia oculta en cada dirección (BCC)';
$gl_caption_config['c_one_by_one']          = 'Un mensaje diferente para cada dirección';
$gl_caption['c_template_caption']           = '[Ninguna plantilla]';

$gl_caption_translation['c_2']            = 'En traducción';
$gl_caption_translation['c_3']            = 'Finalizado';
$gl_caption_translation['c_1']            = 'Pendiente';
$gl_caption_translation['c_4']            = 'Error';
$gl_caption['c_description']              = 'Descripción de la web';
$gl_caption['c_mail']                     = 'Correo predeterminado';
$gl_caption['c_title']                    = 'Título de la web';
$gl_caption['c_robots']                   = 'Tags de la web';
$gl_caption_custumer['c_group_1']         = 'Búsqueda proximativa';
$gl_caption_custumer['c_group_2']         = 'MLS';
$gl_caption_config['c_search_percent']    = 'Desnivel en las búsquedas cliente y MLS (%)';
$gl_caption_config['c_search_level']      = 'Nivel de proximación en la busqueda';
$gl_caption_translation['c_join_date']    = 'Fecha de envió';
$gl_caption_translation['c_state']        = 'Estado actual:';
$gl_caption_translation['c_numwords']     = 'Número de palabras:';
$gl_caption_translation['c_originaltext'] = 'Texto original:';

$gl_caption['c_search_level_1'] = 'Restrictivo';
$gl_caption['c_search_level_2'] = 'Normal';
$gl_caption['c_search_level_3'] = 'Poco restrictivo';

$gl_caption_config ['c_mls_all']           = 'Marcar por defecto la casilla MLS de los inmuebles';
$gl_caption_config ['c_mls_comission']     = 'Comissión por defecto de las propiedades MLS (%)';
$gl_caption_config ['c_mls_search_always'] = 'Búsqueda MLS activada';
$gl_caption_config ['c_mls_allow_listing'] = 'Permitir listar inmuebles';

$gl_caption_config ['c_0']            = 'No';
$gl_caption_config ['c_1']            = 'Si';
$gl_caption_config ['c_default_mail'] = 'E-mail predeterminado';
$gl_caption_config ['c_use_custom_foot'] = 'Pie intranet';

$gl_caption_save ['c_save_1'] = 'Clique aquí';
$gl_caption_save ['c_save_2'] = 'para descargar la Copia de seguridad';


$gl_caption_config ['c_google_center_longitude']    = 'Ubicación';
$gl_caption_inmo_admin['c_google_center_longitude'] = 'Posición inicial del mapa';
$gl_caption_config ['c_google_latitude']            = 'Latitud';
$gl_caption_config ['c_google_longitude']           = 'Longitud';
$gl_caption_config ['c_google_center_latitude']     = 'Latitud centro mapa';
$gl_caption_config ['c_center_longitude']           = 'Longitud centro mapa';
$gl_caption_config ['c_google_zoom']                = '';
$gl_caption_config ['c_allow_seo']                  = 'Activar SEO';

// contact
$gl_caption_contact_public['c_group_4']  = 'Opciones';
$gl_caption_contact_public['c_send_mail_copy']  = 'Enviar copia al cliente';
$gl_caption_contact_public['c_use_mail_template']  = 'Utilizar plantilla';

// inmo
$gl_caption_inmo_admin['c_custom1_name']   = 'Característica 1';
$gl_caption_inmo_admin['c_custom2_name']   = 'Característica 2';
$gl_caption_inmo_admin['c_custom3_name']   = 'Característica 3';
$gl_caption_inmo_admin['c_custom4_name']   = 'Característica 4';
$gl_caption_inmo_admin['c_custom5_name']   = 'Característica 5';
$gl_caption_inmo_admin['c_custom6_name']   = 'Característica 6';
$gl_caption_inmo_admin['c_custom7_name']   = 'Característica 7';
$gl_caption_inmo_admin['c_custom8_name']   = 'Característica 8';
$gl_caption_inmo_admin['c_custom1_type']   =
$gl_caption_inmo_admin['c_custom2_type'] =
$gl_caption_inmo_admin['c_custom3_type'] =
$gl_caption_inmo_admin['c_custom4_type'] =
$gl_caption_inmo_admin['c_custom5_type'] =
$gl_caption_inmo_admin['c_custom6_type'] =
$gl_caption_inmo_admin['c_custom7_type'] =
$gl_caption_inmo_admin['c_custom8_type'] = 'Tipo';
$gl_caption_inmo_admin['c_custom1_filter'] =
$gl_caption_inmo_admin['c_custom2_filter'] =
$gl_caption_inmo_admin['c_custom3_filter'] =
$gl_caption_inmo_admin['c_custom4_filter'] =
$gl_caption_inmo_admin['c_custom5_filter'] =
$gl_caption_inmo_admin['c_custom6_filter'] =
$gl_caption_inmo_admin['c_custom7_filter'] =
$gl_caption_inmo_admin['c_custom8_filter'] = 'Filtro';

$gl_caption_config['c_checkbox']     = 'Casilla verificación';
$gl_caption_config['c_int']          = 'Número';
$gl_caption_config['c_text']         = 'Texto en idiomas';
$gl_caption_config['c_text_no_lang'] = 'Texto';
$gl_caption_config['c_textarea']     = 'Texto varias líneas';
$gl_caption_config['c_select']       = 'Desplegable';

// booking
$gl_caption_booking_admin ['c_group_1'] = "Reservas";
$gl_caption_booking_admin ['c_group_2'] = "Datos Mossos";
$gl_caption_booking_admin ['c_group_3'] = "Contratos";

$gl_caption['c_ignore_outside_ue']           = "Ignorar fuera de la UE";
$gl_caption['c_default_status']              = "Estado por defecto al reservar desde admin";
$gl_caption['c_public_default_status']       = "Estado por defecto al reservar desde la web";
$gl_caption['c_booked']                      = "Reservado";
$gl_caption['c_pre_booked']                  = "Pre reserva";
$gl_caption['c_has_payment']                 = "Módulo de pago";
$gl_caption['c_nom_establiment']             = "Nombre establecimiento";
$gl_caption['c_codi_establiment']            = "Código establecimiento";
$gl_caption['c_has_contracts']               = "Contratos";
$gl_caption['c_contract_default_language']   = "Idioma por defecto";
$gl_caption['c_contract_format']             = "Formato";
$gl_caption['c_google_analytics_conversion'] = "Código conversión google analytics";


// productes
$gl_caption_product_admin ['c_default_sell'] = 'Casella activada, producto en venda';
$gl_caption_product_admin ['c_pvp_required'] = 'P.V.P obligatorio';
$gl_caption_product_admin ['c_home_title']   = 'Titulo de los productos destacados';
$gl_caption_product_admin ['c_group_1']      = $gl_caption_product_public ['c_group_1'] = $gl_caption_inmo_admin['c_group_1'];
$gl_caption_product_admin ['c_group_2']      = $gl_caption_product_public ['c_group_2'] = $gl_caption_inmo_admin['c_group_2'];
$gl_caption_product_admin ['c_group_3']      = $gl_caption_product_public ['c_group_3'] = $gl_caption_inmo_admin['c_group_3'];
$gl_caption_product_admin ['c_group_4']      = $gl_caption_product_public ['c_group_4'] = $gl_caption_inmo_admin['c_group_4'];
$gl_caption_product_admin ['c_group_5']      = $gl_caption_product_public ['c_group_5'] = $gl_caption_inmo_admin['c_group_5'];

$gl_caption_product_public ['c_group_5']      = $gl_caption_booking_public ['c_group_5'] = 'Opciones del módulo de pago';
$gl_caption_product_public ['c_group_6']      = 'Pàgina de inicio';
$gl_caption_product_public ['c_group_7']      = $gl_caption_booking_public ['c_group_7'] = 'Transferencia bancaria';
$gl_caption_product_public ['c_group_8']      = $gl_caption_booking_public ['c_group_8'] = 'Paypal';
$gl_caption_product_public ['c_group_9']      = $gl_caption_booking_public ['c_group_9'] = 'Tarjeta Redsys';
$gl_caption_product_public ['c_group_10']     = 'Tarjeta 4b';
$gl_caption_product_public ['c_no_sell_text'] = 'Descripción producto no en venda';

$gl_caption ['c_paypal_sandbox']           = 'Paypal en modo sandbox';
$gl_caption ['c_banc_name']                = 'Nombre del banco';
$gl_caption ['c_account_number']           = 'Número de cuenta';
$gl_caption ['c_paypal_mail']              = 'E-mail de la cuenta PayPal';
$gl_caption ['c_paypal_mail_repeat']       = 'Repetir e-mail';
$gl_caption_product_public ['c_home_show_marked']         = 'Muestra destacados';
$gl_caption_product_public ['c_home_show_familys']        = 'Muestra familias i subfamilias';
$gl_caption_product_public ['c_home_max_familys']         = 'Número máximo de familias';
$gl_caption_product_public ['c_home_max_family_products'] = 'Número máximo de productos de cada familia';
$gl_caption ['c_payment_methods']          = 'Módulos de pago activos';
$gl_caption ['c_paypal']                   = 'PayPal';
$gl_caption ['c_account']                  = 'Transferencia bancaria';
$gl_caption ['c_ondelivery']               = 'Contrareembolso';
$gl_caption ['c_directdebit']              = 'Recibo bancario';
$gl_caption ['c_4b']                       = 'Tarjeta 4b';
$gl_caption_product_public['c_home_title']                = 'Título de la página inicial';
$gl_caption_product_public ['c_4b_simulador']             = '4b en modo simulador';
$gl_caption_product_public ['c_4b_id_comercio']           = '4b id comercio';
$gl_caption ['c_lacaixa']                  = 'Tarjeta Redsys';
$gl_caption ['c_lacaixa_simulador']        = 'Redsys en modo simulador';
$gl_caption ['c_lacaixa_clave_sha2']       = 'Clave SHA256';
$gl_caption ['c_lacaixa_merchant_code']    = 'Código de comerçio';

$gl_caption_product_public ['c_can_collect']    = 'Permitir recogida en tienda física';

$gl_caption_product_admin['c_is_public_tax_included']     = 'Todos los precios (PVP) son IVA incluido';
$gl_caption_product_admin['c_is_wholesaler_tax_included'] = 'Todos los precios distribuidor (PVD) son IVA incluido';
$gl_caption_product_admin['c_group_12']                   = 'Impuestos';
$gl_caption_product_admin['c_has_equivalencia_default']   = 'Equivalencia marcada al insertar distribuidor';
$gl_caption_product_admin['c_stock_control']              = 'Control automático de stock';
$gl_caption_product_admin['c_allow_null_stock']           = 'Permitir venda productos con stock negativo';
$gl_caption_product_admin['c_group_10']                   = 'Stock';
$gl_caption_product_admin['c_group_11']                   = 'Tarifas de envio';
$gl_caption_product_admin['c_send_rate_spain']            = 'España';
$gl_caption_product_admin['c_send_rate_france']           = 'Francia';
$gl_caption_product_admin['c_send_rate_andorra']          = 'Andorra';
$gl_caption_product_admin['c_send_rate_world']            = 'Resto del mundo';
$gl_caption_product_admin['c_allow_free_send']            = 'Casilla activada, permitir envio gratuito';
$gl_caption_product_admin['c_minimum_buy_free_send']      = 'Compra mínima de';
$gl_caption_product_admin['c_tax_included']               = 'IVA incluído';
$gl_caption_product_admin['c_tax_not_included']           = 'IVA no incluído';


$gl_caption_page['c_ordre']               = 'Orden';
$gl_caption_page['c_template']            = 'Plantilla';
$gl_caption_page['c_link']                = 'Link';
$gl_caption_page_list['c_page_title']          = 'Título en los buscadores';
$gl_caption_page['c_page']                = 'Nombre';
$gl_caption_page['c_title']               = 'Título en el cuerpo';
$gl_caption_page['c_content']             = 'Contenido';
$gl_caption_page['c_prepare']             = 'En preparación';
$gl_caption_page['c_review']              = 'Para revisar';
$gl_caption_page['c_public']              = 'Público';
$gl_caption_page['c_archived']            = 'Archivado';
$gl_caption_page['c_status']              = 'Estado';
$gl_caption_page['c_page_id']             = 'Id';
$gl_caption_page['c_parent_id']           = 'Parent';
$gl_caption_page['c_level']               = 'Nivel';
$gl_caption_page['c_menu']                = 'Menú';
$gl_caption_page['c_blocked']             = 'Block.';
$gl_caption_page['c_filter_parent_id']    = 'Parent';
$gl_caption_page['c_filter_level']        = 'Level';
$gl_caption_page['c_has_content']         = 'Content';
$gl_caption_page['c_has_content_0']       = 'no';
$gl_caption_page['c_has_content_1']       = 'si';
$gl_caption_page['c_edit_content_button'] = 'Editar página';
$gl_caption_page['c_edit_page_button']    = 'Editar datos';
$gl_caption_page['c_filter_menu2']        = 'Menú de navegación';

/*banners*/
$gl_caption_banner['c_banner_id']          = '';
$gl_caption_banner['c_ordre']              = 'Orden';
$gl_caption_banner['c_url1']               = 'Enlace';
$gl_caption_banner['c_url1_name']          = 'Texto en el enlace';
$gl_caption_banner['c_url1_target']        = 'Destino';
$gl_caption_banner['c_url1_target__blank'] = 'Una nueva ventana';
$gl_caption_banner['c_url1_target__self']  = 'La misma ventana';
$gl_caption_banner['c_file']               = 'Imagen banner';
$gl_caption_banner['c_banner']             = 'Banner';
$gl_caption_banner['c_place_id']           = 'Sección';
$gl_caption_banner['c_banner_text']        = 'Texto';
$gl_caption_banner['c_banner_text2']        = 'Texto 2';
$gl_caption_banner['c_filter_place_id']    = 'Todas las secciones';
$gl_caption_banner['c_status']             = 'Estado';
$gl_caption_banner['c_filter_status']      = 'Todos los estados';
$gl_caption_banner['c_prepare']            = 'En preparación';
$gl_caption_banner['c_review']             = 'Para revisar';
$gl_caption_banner['c_public']             = 'Pública';
$gl_caption_banner['c_archived']           = 'Archivada';


/* GUESTBOOK */
$gl_caption_guestbook['c_guestbook_id']      = '';
$gl_caption_guestbook['c_guest']             = 'Nombre';
$gl_caption_guestbook['c_comment']           = 'Comentario';
$gl_caption_guestbook['c_status']            = 'Estado';
$gl_caption_guestbook['c_prefered_language'] = 'Idioma';
$gl_caption_guestbook['c_entered']           = 'Fecha';
$gl_caption_guestbook['c_prepare']           = 'En preparación';
$gl_caption_guestbook['c_review']            = 'Para revisar';
$gl_caption_guestbook['c_public']            = 'Pública';
$gl_caption_guestbook['c_archived']          = 'Archivada';

$gl_caption_guestbook['c_destacat']='Destacado';
$gl_caption_guestbook['c_destacat_0']='No destacado';
$gl_caption_guestbook['c_destacat_1']='Destacado';
$gl_caption_guestbook['c_filter_destacat'] = 'Todos, destacados o no';

/* GALLERY */
$gl_caption_gallery['c_prepare']         = 'En preparació';
$gl_caption_gallery['c_review']          = 'Per revisar';
$gl_caption_gallery['c_public']          = 'Públic';
$gl_caption_gallery['c_archived']        = 'Arxivat';
$gl_caption_gallery['c_gallery']         = 'Galería';
$gl_caption_gallery['c_subtitle']        = 'Subtítulo';
$gl_caption_gallery['c_content']         = 'Contenido';
$gl_caption_gallery['c_entered']         = 'Data entrada';
$gl_caption_gallery['c_modified']        = 'Data modificació';
$gl_caption_gallery['c_status']          = 'Estat';
$gl_caption_gallery['c_filter_status']   = 'Tots els estats';
$gl_caption_gallery['c_filter_in_home']  = 'Totes, home o no';
$gl_caption_gallery['c_filter_destacat'] = 'Totes, destacades o no';
$gl_caption_gallery['c_ordre']           = 'Ordre';

$gl_caption_gallery['c_in_home']      = 'Surt a la Home';
$gl_caption_gallery['c_in_home_0']    = 'No surt a la Home';
$gl_caption_gallery['c_in_home_1']    = 'Surt a la Home';
$gl_caption_gallery['c_content_list'] = 'Descripció curta al llistat';
$gl_caption_gallery['c_destacat']     = 'Galeria destacada';
$gl_caption_gallery['c_destacat_0']   = 'No destacada';
$gl_caption_gallery['c_destacat_1']   = 'Destacada';

$gl_caption['c_gallery_file_name']     = $GLOBALS['gl_caption']['c_file_name'];
$gl_caption['c_gallery_old_file_name'] = $GLOBALS['gl_caption']['c_old_file_name'];

/* Utilitats */
$gl_caption_utility['c_group_1']                   = 'Valores predeterminados compra inmueble';
$gl_caption_utility['c_group_2']                   = 'Valores predeterminados venta inmueble';
$gl_caption_utility['c_group_3']                   = 'Valores predeterminados hipoteca';
$gl_caption_utility['c_agency_percent']            = 'Gastos gestión (%)';
$gl_caption_utility['c_efficiency']                = 'Certificado de eficiencia energética (€)';
$gl_caption_utility['c_estate_commission_percent'] = 'Comisión agencia inmobiliaria (%)';
$gl_caption_utility['c_habitability_card']         = 'Cédula de habitabilidad (€)';
$gl_caption_utility['c_mortgage_cancel_percent']   = 'Comisión de cancelación (%)';
$gl_caption_utility['c_mortgage_initial_interest'] = 'Interés predeterminado (%)';
$gl_caption_utility['c_mortgage_length']           = 'Duración hipoteca (años)';
$gl_caption_utility['c_mortgage_notary_percent']   = 'Gastos notario (%)';
$gl_caption_utility['c_mortgage_open_percent']     = 'Apertura (%)';
$gl_caption_utility['c_mortgage_percent']          = 'Porcentaje hipoteca sobre el precio (%)';
$gl_caption_utility['c_mortgage_register_percent'] = 'Registro hipoteca (%)';
$gl_caption_utility['c_mortgage_valuation']        = 'Tasación (€)';
$gl_caption_utility['c_notary_percent']            = 'Gastos notario (%)';
$gl_caption_utility['c_register_percent']          = 'Gastos registro (%)';

/* Places*/
$gl_caption_place['c_place'] = 'Oficina';
$gl_caption_place['c_status'] = 'Estat';
$gl_caption_place['c_prepare'] = 'En preparación';
$gl_caption_place['c_review'] = 'Para revisar';
$gl_caption_place['c_public'] = 'Pública';
$gl_caption_place['c_archived'] = 'Archivada';

$gl_caption_place['c_address'] = 'Dirección';
$gl_caption_place['c_adress'] = 'Calle';
$gl_caption_place['c_numstreet'] = 'Número';
$gl_caption_place['c_block'] = 'Bloque';
$gl_caption_place['c_flat'] = 'Piso';
$gl_caption_place['c_door'] = 'Puerta';
$gl_caption_place['c_zip'] = 'Código postal';

$gl_caption_place['c_zone_id'] = 'Zona';
$gl_caption_place['c_provincia_id'] = 'Provincia';
$gl_caption_place['c_comarca_id'] = 'Comarca';
$gl_caption_place['c_municipi_id'] = 'Municipio';
$gl_caption_place['c_country_id'] = 'País';