<?

/* 1-12-15

	Actualització inmo booking, mostar reserves amb un contracte concret, així es pot canviar el contracte i mostrar les reserves ja fetes amb el contracte vell

 */
if ( is_table( 'booking__book' ) ) {
	$update_sql[] = "
		ALTER TABLE `booking__book`
	ADD COLUMN `contract_id` TINYINT(11) NOT NULL DEFAULT '1' AFTER `is_owner`;";

}

?>