<?php

/* 18-11-14 
  
 * Immo: Hora entrada i hora sortida

 */

	
	
if (is_table('inmo__property') && !(is_field('inmo__property','hut'))){
	
	$update_sql = "
	ALTER TABLE `inmo__property`
		ADD COLUMN `hut` int(11) NOT NULL DEFAULT '0' AFTER `user_id`,
		ADD COLUMN `level_count` int(11) NOT NULL DEFAULT '0' AFTER `user_id`,
		ADD COLUMN `marble` TINYINT(1) NOT NULL AFTER `gres`;
	";	
	
}
	
if (is_table('booking__extra_to_book')){
	
	$update_sql = "
	ALTER TABLE `booking__extra_to_book`
		CHANGE COLUMN `quantity` `quantity` INT(11) NOT NULL DEFAULT '1' AFTER `extra_price`;
	";	
	
}
?>