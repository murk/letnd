<?
/**
 * RacingSuport - Blunik
 *
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2017
 * @version $Id$
 * @access public
 */
class RacingSuport extends Module{

	function __construct(){
		parent::__construct();
	}
	function on_load(){

		// social
		// Main::load_class( 'social' );
		/*
		$this->form_tabs =
			array(
				'0' =>
					array(
						'tab_action'  => 'show_form_social&amp;menu_id=' . $GLOBALS['gl_menu_id'],
						'tab_caption' => $this->caption['c_edit_social']
					)
			);
		*/

	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function get_records()
	{
	    $listing = new ListRecords($this);
		
		
		// $listing->call('records_walk','', true);
		//$listing->add_button ('edit_content_button', 'action=show_form_copy', 'boto1');

		if (isset($_GET['selected_menu_id']))
		{
			$listing->condition = 'category_id = ' . $_GET['category_id'];
		}
		else {
			$listing->add_filter( 'category_id' );
		}
		$listing->add_filter('status');
		$listing->add_filter('tema_id');

		// Social::init_listing($listing);
		
		$listing->order_by = 'ordre ASC, suport, suport_id';
	    return $listing->list_records();
	}
	function records_walk(&$listing){

	}
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{
	    // si no hi ha cap categoria faig un redirect a entrar categoria nova
		if (!Db::get_first("SELECT count(*) FROM racing__category LIMIT 0,1")){
			$_SESSION['message'] = $this->messages['no_category'];
			redirect('/admin/?menu_id=3006');
		}

		$this->set_field('modified','default_value',now(true));

		$show = new ShowForm($this);
		// $show->form_tabs = $this->form_tabs;
		$show->set_form_level1_titles('page_title');

		//activo o desactivo el seo
		if (!$_SESSION['show_seo']) {
			$show->unset_field('page_title');	
			$show->unset_field('page_description');
			$show->unset_field('page_keywords');
			$show->unset_field('new_file_name');		    
			$show->unset_field('new_old_file_name');
		}
	    return $show->show_form();
	}

	function save_rows()
	{
	    $save_rows = new SaveRows($this);
	    $save_rows->save();
	}

	function write_record()
	{
	    $writerec = new SaveRows($this);
		$writerec->set_field('modified','override_save_value','now()');			
		$writerec->field_unique = "new_file_name";
		$writerec->save();
	}

	function manage_images()
	{
	    $image_manager = new ImageManager($this);
		// $image_manager->form_tabs = $this->form_tabs;
	    $image_manager->execute();
	}
	
	/////////////
	// SOCIAL
	///////////////
	function show_form_social() {
		$id = R::id($this->id_field);
		$language = Social::get_language();

		$query = "
			SELECT 
				suport, 
				tema, 
				content,
				suport_file_name as file_name
			FROM " . $this->table . "_language 
			WHERE " . $this->id_field . "='" . $id . "'
			AND language = '" . $language . "'";
		$rs = Db::get_row($query);
		
		if (!$rs) return;

		$link = HOST_URL . $language . '/blunik/suport/' . $id . '.htm';
		
		$facebook_params = array(
			'message' => '',
			'name' => htmlspecialchars($rs['suport']),
			'caption' => htmlspecialchars($rs['tema']),
			'description' => htmlspecialchars( p2nl($rs['content']))
		);
		$twitter_params = array(
			'status' => htmlspecialchars($rs['suport'])
		);
		
		$social = new Social($this, $id, $facebook_params, $twitter_params, $link);
		$GLOBALS['gl_content'] = $social->get_form();
	}
	function write_record_social() {
		$social = new Social($this);		
		$social->publish();
	}
	function set_buttons_social(&$listing) {
		
		Social::set_buttons_listing($listing);
		
	}

}
?>