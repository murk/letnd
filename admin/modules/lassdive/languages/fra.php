<?php
// menus eina
if (!defined('LASSDIVE_MENU_ACTIVITAT')){
	define('LASSDIVE_MENU_ACTIVITAT', 'Activitats');
	define('LASSDIVE_MENU_ACTIVITAT_NEW', 'Entrar nova activitat');
	define('LASSDIVE_MENU_ACTIVITAT_LIST', 'Llistar i editar activitats');
	define('LASSDIVE_MENU_ACTIVITAT_LIST_PACKS', 'Llistar especials');
	define('LASSDIVE_MENU_ACTIVITAT_BIN', 'Paperera de reciclatge');
	define('LASSDIVE_MENU_IDIOMA', 'Idioma');
	define('LASSDIVE_MENU_IDIOMA_NEW', 'Entrar nou idioma');
	define('LASSDIVE_MENU_IDIOMA_LIST', 'Llistar idiomes');

	define('LASSDIVE_MENU_LEVEL', 'Nivells');
	define('LASSDIVE_MENU_LEVEL_NEW', 'Entrar nou nivell');
	define('LASSDIVE_MENU_LEVEL_LIST', 'Llistar nivells');

	define('LASSDIVE_MENU_MODALITY', 'Modalitats');
	define('LASSDIVE_MENU_MODALITY_NEW', 'Entrar nova modalitat');
	define('LASSDIVE_MENU_MODALITY_LIST', 'Llistar modalitats');

	define('LASSDIVE_MENU_ELEMENT','Inclòs / no inclòs');
	define('LASSDIVE_MENU_ELEMENT_NEW','Nou ítem');
	define('LASSDIVE_MENU_ELEMENT_LIST','Llistar ítems');
	
	define('LASSDIVE_MENU_PRICE', 'Preus');
	define('LASSDIVE_MENU_PRICE_NEW', 'Entrar nou preu');
	define('LASSDIVE_MENU_PRICE_LIST', 'Llistar preus');
	define('LASSDIVE_MENU_PRICE_BIN', 'Paperera de reciclatge');

	define('LASSDIVE_MENU_CATEGORY', 'Categories');
	define('LASSDIVE_MENU_CATEGORY_NEW', 'Entrar nova categoria');
	define('LASSDIVE_MENU_CATEGORY_LIST', 'Llistar categories');

	define('LASSDIVE_MENU_FAMILY', 'Calendari famílies');

	define('LASSDIVE_MENU_RESERVA', 'Reserves');
	define('LASSDIVE_MENU_RESERVA_LIST', 'Llistar reserves');
	define('LASSDIVE_MENU_RESERVA_BIN', 'Paperera de reciclatge');

	define('LASSDIVE_MENU_CENTRE', 'Centres');
	define('LASSDIVE_MENU_CENTRE_NEW', 'Entrar nou centre');
	define('LASSDIVE_MENU_CENTRE_LIST', 'Llistar centres');
	define('LASSDIVE_MENU_ASSIGNCENTRE', 'Assignar productes');

	define('LASSDIVE_MENU_OPERATOR', 'Operador');
	define('LASSDIVE_MENU_OPERATOR_NEW', 'Entrar nou operador');
	define('LASSDIVE_MENU_OPERATOR_LIST', 'Llistar operadors');
}

$gl_messages_activitat['no_category']='Per poder insertar notícies hi ha d\'haver almenys una categoria';

// captions seccio
$gl_caption_activitat['c_see_activities'] = 'Llistar activitats';
$gl_caption_activitat['c_edit_calendar'] = 'Editar calendari';
$gl_caption_activitat['c_see_products'] = 'Llistar productes';
$gl_caption_activitat['c_activitat_title'] = 'Títol';
$gl_caption_activitat['c_parent_id'] = 'Pertany a';
$gl_caption_activitat['c_prepare'] = 'En preparació';
$gl_caption_activitat['c_review'] = 'Per revisar';
$gl_caption_activitat['c_public'] = 'Públic';
$gl_caption_activitat['c_archived'] = 'Arxivat';
$gl_caption_activitat['c_title'] = 'Títol';
$gl_caption_activitat['c_subtitle'] = 'Subtítol';
$gl_caption_activitat['c_content'] = 'Descripció';
$gl_caption_activitat['c_entered'] = 'Creada';
$gl_caption_activitat['c_modified'] = 'Modificada';
$gl_caption_activitat['c_start_date'] = 'Data inici';
$gl_caption_activitat['c_end_date'] = 'Data final';
$gl_caption_activitat['c_minimum_age'] = 'Edat mínima';
$gl_caption_activitat['c_maximum_person'] = 'Màxim persones';
$gl_caption_activitat['c_status'] = 'Estat';
$gl_caption_activitat['c_category_id'] = 'Categoria';
$gl_caption_activitat['c_filter_category_id'] = 'Totes les categories';
$gl_caption_activitat['c_filter_status'] = 'Tots els estats';
$gl_caption_activitat['c_filter_in_home'] = 'Totes, home o no';
$gl_caption_activitat['c_filter_parent_id'] = 'Totes les categories';
$gl_caption_activitat['c_ordre'] = 'Ordre';
$gl_caption_activitat['c_weekday_id'] = 'Dies';
$gl_caption_activitat['c_price'] = 'Preu';
$gl_caption_activitat['c_price2'] = 'Preu 2';
$gl_caption_activitat['c_price3'] = 'Preu 3';
$gl_caption_activitat['c_price4'] = 'Preu 4';
$gl_caption_activitat['c_price5'] = 'Preu 5';
$gl_caption_activitat['c_price6'] = 'Preu 6';
$gl_caption_activitat['c_price7'] = 'Preu 7';
$gl_caption_activitat['c_price8'] = 'Preu 8';
$gl_caption_activitat['c_price9'] = 'Preu 9';
$gl_caption_activitat['c_price10'] = 'Preu 10';
$gl_caption_activitat['c_price_text'] = 'Preu text';
$gl_caption_activitat['c_price_text2'] = 'Preu text 2';
$gl_caption_activitat['c_price_text3'] = 'Preu text 3';
$gl_caption_activitat['c_price_text4'] = 'Preu text 4';
$gl_caption_activitat['c_price_text5'] = 'Preu text 5';
$gl_caption_activitat['c_price_text6'] = 'Preu text 6';
$gl_caption_activitat['c_price_text7'] = 'Preu text 7';
$gl_caption_activitat['c_price_text8'] = 'Preu text 8';
$gl_caption_activitat['c_price_text9'] = 'Preu text 9';
$gl_caption_activitat['c_price_text10'] = 'Preu text 10';
$gl_caption_activitat['c_and'] = 'et';
$gl_caption_activitat['c_or'] = 'ou';

$gl_caption_category['c_category'] = 'Categoria';
$gl_caption_category['c_activitat_button'] = 'Veure activitats';
$gl_caption_category['c_ordre'] = 'Ordre';
$gl_caption_image['c_name'] = 'Nom';
$gl_caption['c_idioma'] = $gl_caption['c_idioma_id'] = 'Idioma';
$gl_caption_activitat['c_url1_name']='Nom URL(1)';
$gl_caption_activitat['c_url1']='URL(1)';
$gl_caption_activitat['c_url2_name']='Nom URL(2)';
$gl_caption_activitat['c_url2']='URL(2)';
$gl_caption_activitat['c_url3_name']='Nom URL(3)';
$gl_caption_activitat['c_url3']='URL(3)';
$gl_caption_activitat['c_videoframe']='Video (youtube, metacafe...)';
$gl_caption_activitat['c_file']='Arxius';
$gl_caption_activitat['c_in_home']='Surt a la Home';
$gl_caption_activitat['c_in_home_0']='No surt a la Home';
$gl_caption_activitat['c_in_home_1']='Surt a la Home';
$gl_caption_activitat_list['c_in_home_0']='';
$gl_caption_activitat_list['c_in_home_1']='Oui';
$gl_caption_activitat['c_destacat']='Notícia destacada';
$gl_caption_activitat['c_destacat_0']='No destacada';
$gl_caption_activitat['c_destacat_1']='Destacada';

$gl_caption_activitat['c_timetable']='Horari';
global $week_names;
$gl_caption_activitat['c_timetable1']=$week_names[0];
$gl_caption_activitat['c_timetable2']=$week_names[1];
$gl_caption_activitat['c_timetable3']=$week_names[2];
$gl_caption_activitat['c_timetable4']=$week_names[3];
$gl_caption_activitat['c_timetable5']=$week_names[4];
$gl_caption_activitat['c_timetable6']=$week_names[5];
$gl_caption_activitat['c_timetable7']=$week_names[6];
$gl_caption['c_level_id'] = $gl_caption['c_level'] = 'Nivell';
$gl_caption['c_modality_id'] = $gl_caption['c_modality'] = 'Modalitat';
$gl_caption['c_needed_material'] = 'Material necessari';
$gl_caption['c_includes'] = 'Inclou';
$gl_caption['c_excludes'] = 'No inclou';
$gl_caption_element['c_element'] = 'Ítem';

$gl_caption_activitat['c_form_level1_title_minimum_age']='Característiques';
$gl_caption_activitat['c_form_level1_title_activitat']='Descripcions';
$gl_caption_activitat['c_form_level1_title_timetable']='Horaris';
$gl_caption_activitat['c_form_level1_title_price']='Preus';
$gl_caption_activitat['c_form_level1_title_needed_material']='Material';
$gl_caption_activitat['c_form_level1_title_file']='Arxius adjunts';
$gl_caption_activitat['c_form_level1_title_page_title']='SEO';
$gl_caption_activitat['c_form_level2_title_entered']='Dates';


$gl_caption['c_activitat_file_name'] = $GLOBALS['gl_caption']['c_file_name'];
$gl_caption['c_activitat_old_file_name'] = $GLOBALS['gl_caption']['c_old_file_name'];

$gl_caption_activitat['c_selected']='Activitats seleccionades';

$gl_caption['c_ordre']='Ordre';

// public
$gl_caption['c_activitat'] = $gl_caption['c_activitat_id'] = 'Activitats';
$gl_caption['c_activitat'] = 'Activitat ( Menú i llistat )';
$gl_caption_list['c_activitat'] = 'Activitat';
$gl_caption['c_price_id']='Preus';
$gl_caption['c_price_month']='Preu mensual';
$gl_caption['c_price_quarter']='Preu trimestral';


/* Centres*/
$gl_caption['c_centre']    = 'Centre';
$gl_caption_centre['c_status']   = 'Estat';
$gl_caption_centre['c_prepare']  = 'En preparació';
$gl_caption_centre['c_public']   = 'Públic';
$gl_caption_centre['c_archived'] = 'Arxivat';

$gl_caption['c_address']   = 'Adreça';
$gl_caption['c_poblacio']   = 'Població';
$gl_caption['c_zip']       = 'Codi postal';
$gl_caption_centre['c_admin_id']       = 'Administradors';
$gl_caption_centre['c_user_id']       = 'Usuaris';

/* Operadors */
$gl_caption['c_operator'] = $gl_caption['c_operator_id'] = 'Operador';
$gl_caption_list['c_operator_id'] = 'Op.';
$gl_caption['c_telephone'] = 'Telèfon';
$gl_caption['c_operator_select_caption'] = "All";
$gl_caption['c_operator_select_operator'] = "With operator";
$gl_caption['c_operator_select_no_operator'] = "Without operator";

$gl_caption['c_centreproduct'] = "Productes";
$gl_caption['c_image'] = "Imatge";
$gl_caption['c_family_id'] = "Família";
$gl_caption['c_subfamily_id'] = "Subfamília";
$gl_caption['c_product_title'] = "Producte";
$gl_caption['c_product_subtitle'] = "Subtítol";
$gl_caption['c_ref'] = "Referència";
$gl_caption['c_status'] = "Estat";
$gl_caption['c_prepare'] = 'En preparació';
$gl_caption['c_review'] = 'Per revisar';
$gl_caption['c_onsale'] = 'Per vendre';
$gl_caption['c_nostock'] = 'Fora de stock';
$gl_caption['c_nocatalog'] = 'Descatalogat';
$gl_caption['c_archived'] = 'Arxivat';
$gl_caption['c_max_quantity'] = "Quantitat màxima";
$gl_caption['c_quantityconcept_id'] = "Concepte";

$gl_caption['c_form_level1_title_admin_id'] = "Usuaris";
$gl_caption['c_form_level1_title_centreproduct'] = "Activitats";

$gl_caption_assigncentre['c_centres'] = "Centres";
$gl_caption['c_centre_max_quantity'] = "Max";

/* Reserves */
$gl_caption['c_reservaitem_id'] = "";
$gl_caption['c_lassdive_product_date'] = "Dia sol·licitat";
$gl_caption['c_reservaitem_date'] = "Dia assignat";
$gl_caption['c_view_format_order'] = "Reserves";
$gl_caption['c_view_format_orderitems'] = "Activitats";
$gl_caption['c_product_id'] = "Activitat";
$gl_caption['c_lassdive_product_basetax'] = "Preu";

$gl_caption['c_has_no_timetable'] = "Horaris";
$gl_caption['c_has_no_timetable_'] = "Tots";
$gl_caption['c_has_no_timetable_no_time'] = "No horari";
$gl_caption['c_has_no_timetable_no_complement'] = "No compl.";

$gl_caption['c_date_search_caption'] = "Buscar data a";
$gl_caption['c_date_search'] = "Data";
$gl_caption['c_timetable'] = "Horari";
$gl_caption['c_date_search_order'] = "Reserva";
$gl_caption['c_date_search_orderitems'] = "Activitat";

$gl_caption['c_is_done_caption'] = "Fet / per fer";
$gl_caption['c_is_done'] = "Fet";
$gl_caption['c_is_done_'] = "Tots";
$gl_caption['c_is_done_search_0'] = "No";
$gl_caption['c_is_done_search_1'] = "Fet";
$gl_caption['c_is_done_0'] = "No";
$gl_caption['c_is_done_1'] = "Sí";
$gl_caption['c_orderitems'] = "Activitats";

$gl_caption['c_start_time'] = "Comença";
$gl_caption['c_end_time'] = "Acaba";
$gl_caption['c_centre_id'] = "Centre";
$gl_caption['c_user_id'] = "Instructor";
$gl_caption['c_is_done'] = "Fet";

$gl_caption['c_source_id'] = "Procedència";
$gl_caption['c_source_'] = "Totes";
$gl_caption['c_source_caption'] = "Procedència";
$gl_caption['c_source_web'] = "Web";
$gl_caption['c_source_centre'] = "Centre";

$gl_caption['c_no_center_with_activity'] = "No hi ha cap centre que ofereixi aquesta activitat";
$gl_caption['c_day_is_busy'] = "No es poden fer reserves per aquest dia";
$gl_caption['c_selected_timetables'] = "horari seleccionat";
// $gl_caption['c_all_timetables_already_selected'] = "Ja s'han seleccionat tots els horaris";
$gl_caption['c_select_ok'] = "Fet!";
$gl_caption['c_select_canviar'] = "Canviar horari";
$gl_caption['c_error_guardar'] = "Hi ha hagut un error al guardar";
$gl_caption['c_button_time'] = "";
$gl_caption['c_selecciona'] = "selecciona...";
$gl_caption['c_lassdive_login'] = "Usuari";
$gl_caption['c_lassdive_logon'] = "Entrar";

$gl_caption['c_reserva_form_activitats'] = "Activitats";
$gl_caption['c_reserva_discount'] = "Descompte";
$gl_caption['c_reserva_deposit'] = 'Paga i senyal';

$gl_caption['c_total'] = "Total";

$gl_caption['c_product_original_title'] = "Prod. original";

// Calendari
$gl_caption['c_view_week'] = "Setmana";
$gl_caption['c_view_month'] = "Mes";


// LOGS
$gl_caption['c_mysql'] = "Sql";
$gl_caption['c_user_id'] = "Usuari";
$gl_caption['c_entered'] = "Data";
$gl_caption['c_session'] = "Sessió";
$gl_caption['c_cookie'] = "Cookie";
$gl_caption['c_uri'] = "Adreça";
$gl_caption['c_referer'] = "Provinent";
$gl_caption['c_query_string'] = "Get";
$gl_caption['c_backtrace'] = "Backtrace";
$gl_caption['c_vars'] = "Variables";