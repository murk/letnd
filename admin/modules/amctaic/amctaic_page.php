<?
/**
 * AmctaicPage
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2010
 * @version $Id$
 * @access public
 */
class AmctaicPage extends Module{
	function __construct(){
		parent::__construct();
		$this->table = 'all_page';
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function get_records()
	{		
		$module = Module::load('web','page');
		$module->parent = 'amctaic__page';
		return $module->do_action(str_replace('list_','get_',$this->action));
	}
	
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	
	function get_form()
	{	
		$module = Module::load('web','page');
		$module->set_field('page_title','form_admin','no');
		$module->parent = 'amctaic__page';
		return $module->do_action(str_replace('show_','get_',$this->action));
	}
	function records_walk($page_id){
	}
	
	function save_rows()
	{
	    $save_rows = new SaveRows($this);
	    $save_rows->save();
	}

	function write_record()
	{
		$module = Module::load('web','page');
		$module->parent = 'amctaic__page';
		return $module->do_action($this->action);
	}

	function manage_images()
	{
	    $image_manager = new ImageManager($this);
	    $image_manager->execute();
	}
}
?>