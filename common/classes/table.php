<?php
/**
 * HTML Table
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2013
 * @version $Id$
 * @access public
 */
class Table{
	var $cols;
	var $current_col = 1;
	var $html = '';
	var $empty_class = 'empty';
	var $row_class = false;
	var $col = array();
	
	function __construct($cols=1, $class=false){
		$this->cols = $cols;
		$class =$class?' class="'. $class.'"':'';
		$this->html = '<table' . $class . '>';
		
		for ($i = 0; $i < $cols; $i++) {
			$this->col[$i+1] = array(
				'tag' => 'td',
				'class' => ''
				);
		}
	}
	
	/**
	 * Canvia configuracio de la columna
	 * @param int $num_col La columna que volem canviar
	 * @param string $tag Pot se th o td
	 * @param string $class  classe de la columna
	 */
	function set_col($num_col,$tag,$class='') {
		$col = &$this->col[$num_col];
		$col['tag'] = $tag;
		$col['class'] = $class;
	}
	
	function ths() {
		
		for ($i = 0; $i < $this->cols; $i++) {
			$this->col[$i+1]['tag'] = 'th';
		}
		
	}
	function tds() {
		
		for ($i = 0; $i < $this->cols; $i++) {
			$this->col[$i+1]['tag'] = 'td';
		}
		
	}
	
	/**
	 * Canvia la clase del tr
	 * @param string $class  classe de la fila
	 */
	function set_row_class($class){
		$this->row_class = $class;
	}
	
	/**
	 * Afegeix una cela
	 * @param string $html
	 * @param string $class
	 * @param string $is_th  aquesta es interna, serveix per quan crido add_header
	 */
	function add_cell($html, $class=false)
	{
		$col = &$this->col[$this->current_col];
		
		if ($class) $col['class'] = ' ' . $class;		
		
		$class =$col['class']?' class="'. trim($col['class']).'"':'';
		
		$tag = $col['tag'];
		
		if ($this->current_col == 1) {
			$this->html .="<tr";
			if ($this->row_class) {
				$this->html .=' class ="' . $this->row_class . '"';
				$this->row_class = ''; // resetejo automaticament, s'ha de definir la classe per cada row
			}
			$this->html .=">";
		}
		
		$this->html .= '<' . $tag . $class . '>' . $html . '</' . $tag . '>';
		if ($this->current_col == $this->cols) $this->html .="</tr>";
		
		$this->current_col ++;
		if ($this->current_col>$this->cols) $this->current_col = 1;
		
	}

	function get_table()
	{
		// afegeixo les columnes que falten fins el final
		
		if ($this->current_col!=1) {		
			for ($i = $this->current_col; $i <= $this->cols; $i++) {
				$this->add_cell('',$this->empty_class);
			}
		}
		
		
		$this->html .='</table>';
		return $this->html;
	}
}
?>