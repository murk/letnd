<?
/**
 * BookingOrder
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2010
 * @version $Id$
 * @access public
 *
 * Metode de Paypal IPN
 *
 */
class BookingPaymentPaypal extends ModuleBase {
    var $vars = array();
    var $config;
	var $url;
	var $book_id;

	var $status = '';
	var $transaction_id;
    function __construct(&$module, $book_vars='')
    {
        $this->config = &$module->config;
		$this->url = $this->config['paypal_sandbox']?'www.sandbox.paypal.com':'www.paypal.com';
    	$paypal_mail = $this->config['paypal_sandbox']?'sanahuja@gmail.com':$this->config['paypal_mail'];

        if ($book_vars) {
			$products = $book_vars['results'];
			$book = $book_vars['totals'];
			$payment_number    = $book_vars['payment_number'];

			$book_id = $book_vars['book_id'];

			$this->vars += $this->get_customer($book_vars['custumer_id']);

			$count = 1;
			// formo les reserves de la cistella de paypal
			foreach ($products as $product) {
				$this->vars['item_name_' . $count] = $product['title'];
				$this->vars['item_number_' . $count] = $product['book_id'];
				$this->vars['quantity_' . $count] = $product['quantity'];
				$this->vars['amount_' . $count] = $product['price_total'];
				$count++;
			}
			
			// SI DESCOMPTE MAJOR A SUMA BOOKINGS, paypal no ho conta, per tant no pot sortir mai un import inferior a almenys les despeses d'enviament ( ja va bé, ja que normalment es posarà un import mínim , sino es perdria calers si no cobris ni les despeses d'enviament )
			
			
			// http://demo.letnd.com/?tool=booking&tool_section=payment&language=spa&action=set_notice&process=paypal

			$return_url = HOST_URL . LANGUAGE . '/booking/book/add-custumers/book_id/' . $book_id . '/';

			$notify_url = HOST_URL . '?tool=booking&tool_section=payment&language=' . LANGUAGE . '&action=set_notice&process=paypal&payment_number=' . $payment_number;

			// Forço sempre http ja que resys no soporta SNI
	        $notify_url = str_replace( 'https://', 'http://', $notify_url );
			
			// variables del paypal
			$this->vars += array(
				'upload' => '1',
				'address_override' => '1',
				'custom' => $book_id,
				//'amount' => $this->get_number($book['total_basetax']), // això no feia res, crec que es per si es fa un item individual
				'discount_amount_cart' => $book['promcode_discount'],
				'handling_cart' => 0,
				'business' => $paypal_mail, // id de paypal o adreça de l'account
				'cmd' => '_cart',
				'charset' => 'utf-8',
				'currency_code' => 'EUR',
				'return' => $return_url, // s'ha fet correctament
				'cancel_return' => HOST_URL . $_SERVER["REQUEST_URI"], // s'ha cancelat la compra
				'notify_url' => $notify_url,
				'bn' => 'LETND_WPS',
				'rm' => '2', // retorna un post
				// 'cbt' => 'Tornar a Tiendascopporo.com', // caption del boto de tornar a la botiga
				'lc' => LANGUAGE_CODE
			);
			//Debug::p($this->vars);

	        Debug::p( [
	        			[ 'vars', $this->vars ],
	        		], 'paypal');
        }

    }
    // obté variables de adreça i dades usuari
    function get_customer($custumer_id)
    {
        $sql = "SELECT custumer__custumer.custumer_id AS payer_id, name AS first_name, CONCAT(surname1, ' ', surname2) AS last_name, mail AS email, adress AS address1, town AS city, zip, country FROM custumer__custumer WHERE custumer_id = " . $custumer_id;
        $ret = Db::get_row($sql);
        //Debug::p($ret);
        return $ret;
    }
    // obté formulari html
    function get_content()
    {
        if (BOOKING_PAYPAL_SANDBOX) {
            $this->vars['business'] = 'sanahuja-facilitator@letnd.com';
            $this->vars['email'] = 'marc@letnd.com';
        }
        $ret = '<form action="https://'.$this->url.'/cgi-bin/webscr" method="post" id="paypal_form">';
        foreach ($this->vars as $key => $val) {
            $ret .= '<input type="hidden" name="' . $key . '" value="' . $val . '" />
			';
        }
        $ret .= '<input class="paypal boto" type="submit">';
        $ret .= '</form>';
        if (!BOOKING_PAYPAL_SANDBOX) $ret .= get_javascript('$("#paypal_form").submit();');
        return $ret;

    }
    function set_notice()
    {
		//Debug::add('Paypal set_notice','Si');
		$this->trace("Variables POST:\n---------------\n" . print_r($_POST,true) . "\n\n", '', 'set_notice');
		
        $header = "";
        $emailtext = "";
        // Read the post from PayPal and add 'cmd'
        $req = 'cmd=_notify-validate';

        foreach ($_POST as $key => $value)
        {
            $value = urlencode($value);
            $req .= "&$key=$value";
        }
		
		
		$ch = curl_init();
		curl_setopt($ch,CURLOPT_URL,'https://'.$this->url.'/cgi-bin/webscr');
		curl_setopt($ch,CURLOPT_POST,true);
		curl_setopt($ch,CURLOPT_POSTFIELDS,$req);
		curl_setopt($ch,CURLOPT_FOLLOWLOCATION,false);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
		$result = curl_exec($ch);
		curl_close($ch);

	    $this->trace($result, 'Result, resposta de paypal');

		$ch = curl_init('https://www.howsmyssl.com/a/check');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    $data = curl_exec($ch);
	    $json = json_decode($data);
	    $this->trace($json->tls_version, 'TLS Version:');
		$curl_info = curl_version();
	    $this->trace($curl_info['version'], 'Curl Version:');
	    $this->trace($curl_info['ssl_version'], 'SSL Version:');




		switch($result){
			case "VERIFIED":
				
				$this->transaction_id = $_POST['txn_id'];
				$this->book_id = $_POST['custom'];
				// verified payment
				$this->trace($_POST['payment_status'], 'Payment Status');
				
				switch($_POST['payment_status']){
					case 'Failed':
						$this->status = 'failed';
						break;
					case 'Pending':
						$this->status = 'pending';
						break;
					case 'Voided':
						$this->status = 'voided';
						break;
					case 'Refunded':
						$this->status = 'voided';
						break;
					case 'Denied':
					case 'Expired':
						$this->status = 'denied';
						break;
					case 'Completed':
					case 'Processed':
						$this->status = 'completed';
						break;
				}
				break;
			case "INVALID":
				
				$this->transaction_id = $_POST['txn_id'];
				$this->book_id = $_POST['custom'];
				
				// invalid/fake payment 
				$emailtext .= "INVALID \n\n";
				foreach ($_POST as $key => $value) {
					$emailtext .= $key . " = " . $value . "\n\n";
				}
				$emailtext .= "\n\nReferer = " . $_SERVER['HTTP_REFERER'];					

				$this->trace('error', 'INVALID');

				send_mail_admintotal('Intent de pagament INVALID a ' . HOST_URL,$emailtext);
				// envio mail notificant l'intent invalid i no faig res mès
				
				return false;
				break;
			default:
				// any other case (such as no response, connection timeout...)
				$emailtext .= "Any other case (such as no response, connection timeout...) \n\n";
				foreach ($_POST as $key => $value) {
					$emailtext .= $key . " = " . $value . "\n\n";
				}
				$emailtext .= "\n\nReferer = " . $_SERVER['HTTP_REFERER'];				

				$this->trace('error', 'Any other case');
			
				send_mail_admintotal('Intent de pagament invàlid per HTTP ERROR amb paypal a ' . HOST_URL,$emailtext);
		}
		
		return true;

    }
	
	function trace($text, $name = '', $title = false){
		BookingPayment::trace($text, $name, $title, 'paypal');
	}
}
    ?>