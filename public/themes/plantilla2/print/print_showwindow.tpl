<?php
// variables
include (DOCUMENT_ROOT . 'admin/modules/print/print_showwindow_tpl_start.php');

// poso la funció aquesta per obtindre els parametres que depenen del disseny, i així posar la logica al print_showwindow_tpl_start.php
// faig extract d'aquestes variables, cuidado no posar el mateix nom a una altra
function pdf_get_config(){
	return array(
		'text_fields_increment_1' => 30, // distancia entre camps quan pocs camps
		'text_fields_increment_2' => 24, // distancia entre camps quan hi ha mes camps
		'text_fields_increment_3' => 17, // distancia entre camps quan hi ha mes camps
		'max_fields_showwindow' => 10,   // maxim camps per aparador
		'ref_size_album' => 16,   // mida del ref a album
		'ref_size_showwindow' => 20,   // mida del ref a album
		'bold_color' => array(53, 74, 91),   // color ref i negretes
		'property_color' => array(93, 130, 160),   // color ref i negretes
		'text_color' => array(0, 0, 0),   // color texte general
		'bg_color' => array(250, 186, 60),   // color de fons dels quadres
		'line_color' => array(253, 228, 279),   // color de les linees
		'image1_x' => 30.5,   // imatge 1
		'image1_y' => 89,
		'image1_w' => 0,
		'image1_h' => 356,
		'image2_x' => 30.5,  // imatge 2
		'image2_y' => 450,
		'image2_w' => 0,
		'image2_h' => 356,
	);
}


function set_pdf_header(&$this)
    {
		extract(pdf_get_config());
		// ratlles grises
        $this->SetDrawColorArray($line_color);
        //$this->Rect(15, 14, 565, 50, 'D');
        //$this->Rect(15, 75, 565, 751.5, 'D');
        $this->Rect(15, 69.5, 565, 749.5, 'D');
        
        // quadres verds
        $this->SetFillColorArray($bg_color);//quadre de dalt i de baix
        $this->RoundedRect(15, 13.5, 565, 56, 6, '1001', 'F');
        $this->RoundedRect(15, 818.5, 565, 8, 6, '0110', 'F');
        // referencia        
		$this->SetFont('Verdanab', '', $this->ref_size);
		$this->SetTextColorArray($bold_color);
        $this->Text(26, 53.5, $this->c_ref . ' ' . $this->ref);
        // logo
        $this->Image($this->logo, 466, 22,0,38,'','','T',false,300,'R'); //el primer valor es la posició del logo
    }


$pdf->SetMargins(27, 84);
$pdf->SetAutoPageBreak(true, 30);
$pdf->SetDisplayMode('real');
$pdf->AddFont('verdanab', '', 'verdanab.php');
// ////////////////////////////////////////////////////////////
// ////////                         ///////////////////////////
// /                 ALBUM                       /////////////
// ///////                          /////////////////////////
// /////////////////////////////////////////////////////////
if ($gl_is_album)
{
    // si hi ha imatges a la primera pagina hi va text+foto i les altres 2 fotos
    for ($image_number = 0;$image_number < count($images);$image_number = $image_number++)
    {
        $pdf->AddPage();
        $image1 = isset($images[$image_number])?DOCUMENT_ROOT . $images[$image_number]['image_src']:false;
        $image2 = isset($images[$image_number + 1])?DOCUMENT_ROOT . $images[$image_number + 1]['image_src']:false;
        // $image = file_exists(DOCUMENT_ROOT . $image_src_original)?DOCUMENT_ROOT . $image_src_original:DOCUMENT_ROOT . $image_src;
        // primera imatge
        if ($image1) $pdf->Image($image1, $image1_x, $image1_y , $image1_w , $image1_h);

//print_r($pdf->images[$image1]);die;
        if ($image_number == 0)
        {
            // quadre verd clar
        	$pdf->SetFillColorArray($bg_color);
			$pdf->SetTextColorArray($bold_color);
            $pdf->Rect(30.5, 450, 534, 18, 'F');
            // Titol mig
            $pdf->SetFont('helvetica', 'B', 14);
            $pdf->Text(36.5, 464, $property);
            // Property_title
            $pdf->SetFont('helvetica', 'I', 14);
			$pdf->SetTextColor(93, 130, 161);
            $pdf->SetY(480);
            if ($property_title)
            {
                $pdf->MultiCell(0, 16, $property_title, 0, 'L');
                // així controlo l'espai entre titol i descripcio
                $pdf->Cell(100, 10, '', 0);
                $pdf->Ln();
            }
            // Descripció
            $pdf->SetFont('helvetica', '', 12);
			$pdf->SetTextColor(93, 130, 161);
            $pdf->MultiCell(0, 16, $description);
            // detalls
            $i = 0;
            // alçada pagina - marge inferior - alçada celes fields - alçada 2 celes contorns - Preu
            $fields_y = 841.88-30 - (ceil(count($fields_pdf) / 2) * 16)-10-26;
            // si cap a la primera pagina dibuixo el quadre  abaix de tot, sino a continuació del text
            if ($pdf->GetY() > $fields_y-5)
            {
				// posa el quadre tot sencer a l'altre pàgina
                $pdf->AddPage();
            }
			else
			{
				//	sino afegeix 1 salts de linea
                $pdf->Ln(10);
			}
            //if ($pdf->PageNo() == 1) $pdf->SetY($fields_y);
            // contorn superior
            $pdf->SetDrawColor(253, 228, 279);
            $pdf->Cell(542, 6, '', 'TRL');
            $pdf->Ln();
            foreach($fields_pdf as $key => $value)
            {
                // contorn esquerra i dret
                $L = $i == 0?'L':0;
                $R = $i == 1?'R':0;
                // cel.les
                $pdf->SetTextColor(53, 74, 91);
                $pdf->Cell(100, 16, ' ' . $key, $L);
                $pdf->SetTextColor(93, 130, 161);
                $pdf->Cell(171, 16, $value, $R);
                // cada 4 celes un salt de linea
                $i == 1?$pdf->Ln():false;
                $i = $i == 0?1:0;
            }
            // si es impar dibuixo cela buida per acabar el contorn
            if ($i == 1)
            {
                $pdf->Cell(100, 16, '', 0);
                $pdf->Cell(171, 16, '', 'R');
                $pdf->Ln();
            }
            // contorn inferior
            $pdf->Cell(542, 4, '', 'BRL');
            $pdf->Ln();
            // Preu
            if ($price){
				$pdf->Cell(100, 11, '', 0);
				$pdf->Ln();
				$pdf->SetFont('helvetica', 'B', 15);
				$pdf->SetTextColor(53, 74, 91);
				$pdf->Cell(100, 15, ' ' . $c_price, 0);
				$pdf->SetTextColor(93, 130, 160);
				$pdf->Cell(171, 15, $price, 0);
			}
        }
        else
        {
            // segona imatge
            if ($image2) $pdf->Image($image2, $image2_x, $image2_y , $image2_w , $image2_h);
            $image_number++;
        }
        $image_number++;       
        if ($pdf->PageNo()>=$pages) break;
    }
}
// ////////////////////////////////////////////////////////////
// ////////                         ///////////////////////////
// /              APARADOR                       /////////////
// ///////                          /////////////////////////
// /////////////////////////////////////////////////////////
else
{
    // si hi ha imatges a la primera pagina hi va text+foto i les altres 2 fotos
    for ($image_number = 0;$image_number < count($images);$image_number = $image_number++)
    {
        $pdf->AddPage();
        $image1 = isset($images[$image_number])?DOCUMENT_ROOT . $images[$image_number]['image_src']:false;
        $image2 = isset($images[$image_number + 1])?DOCUMENT_ROOT . $images[$image_number + 1]['image_src']:false;
        // $image = file_exists(DOCUMENT_ROOT . $image_src_original)?DOCUMENT_ROOT . $image_src_original:DOCUMENT_ROOT . $image_src;
        // primera imatge
        if ($image1) $pdf->Image($image1, $image1_x, $image1_y , $image1_w , $image1_h);

        if ($image_number == 0)
        {
            // quadre verd clar
            $pdf->SetFillColorArray($bg_color);
            //$pdf->Rect(30.5, 440, 534, 10, 'F');
            // ratlla verda vertical
            $pdf->SetFillColor(253, 228, 179);
            $pdf->Rect(188.5, 506, 1, 286, 'F');
            // Titol mig
            $pdf->SetFont('helvetica', '', 25);
			$pdf->SetTextColorArray($property_color);
            $pdf->Text(30.5, 485, $property);
            $i = 0;
            $conta = 0;
            foreach($fields_pdf as $key => $value)
            {
                $pdf->SetFont('helvetica', 'B', 19);
                $pdf->SetTextColorArray($bold_color);
                $pdf->Text(30.5, 520 + $i, $key);
                $pdf->SetFont('helvetica', '', 19);
                $pdf->SetTextColorArray($text_color);
                $pdf->Text(215, 520 + $i, $value);
                $i += $text_fields_increment;
                $conta++;
            }
        }
        else
        {
            // segona imatge
            if ($image2) $pdf->Image($image2, $image2_x, $image2_y , $image2_w , $image2_h);
            $image_number++;
        }

        $image_number++;       
        if ($pdf->PageNo()>=$pages) break;
    }
}

include (DOCUMENT_ROOT . 'admin/modules/print/print_showwindow_tpl_end.php');
?>