<?Header("content-type: application/x-javascript");
extract($_GET);
$c_send_edit = addslashes($c_send_edit);
?>

// html_editors = <?=$html_editors?>;

<?
$common_config = '
		theme : "modern",		
		skin: "letnd",
		width : "635px",
		language : "' . $language_code. '",
		relative_urls: false,
		plugins: [
			 "advlist autolink link image lists charmap print preview hr anchor pagebreak",
			 "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
			 "table contextmenu directionality emoticons paste textcolor responsivefilemanager"
		],
		
		// File manager	
		external_filemanager_path:"/common/jscripts/filemanager/",
		filemanager_title:"' . $c_file_manager . '" ,
		external_plugins: { "filemanager" : "/common/jscripts/filemanager/plugin.min.js"},

		// CSS
		content_css : "/' . $client_dir . '/templates/styles/editor.css",
		
		// Configadmin
		indentation : "' . $tinymce_indent . 'px",
		
		setup: function (theEditor) {
			theEditor.on("focus", function () {
				//$(this.contentAreaContainer.parentElement).find("div.mce-toolbar-grp,div.mce-menubar,div.mce-flow-layout").show();
			});
			theEditor.on("blur", function () {
				//$(this.contentAreaContainer.parentElement).find("div.mce-toolbar-grp,div.mce-menubar,div.mce-flow-layout").hide();
			});
			theEditor.on("init", function() {
				//$(this.contentAreaContainer.parentElement).find("div.mce-toolbar-grp,div.mce-menubar,div.mce-flow-layout").hide();
			});
		},
'

?>
// tooltip = "<?=$c_send_edit?>";
if (html_editors["htmlbasic"]){	

	tinymce.init({
		<?=$common_config?>
		selector : "#" + html_editors["htmlbasic"].join(",textarea#"),
	 });
	
}

if (html_editors["htmlnewsletter"]){	

	tinymce.init({
		<?=$common_config?>
		selector : "#" + html_editors["htmlnewsletter"].join(",textarea#"),
	 });
	
}

if (html_editors["html"]){	

	tinymce.init({
		<?=$common_config?>
		selector : "#" + html_editors["html"].join(",textarea#"),
	 });
	
}

if (html_editors["htmlfull"]){	

	tinymce.init({
		<?=$common_config?>
		selector : "#" + html_editors["htmlfull"].join(",textarea#"),
	 });
		
}


<? /* TOTES LES  OPCIONS
        // Theme options
        theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect,fontsizeselect",
        theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
        theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
        theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,spellchecker,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,blockquote,pagebreak,|,insertfile,insertimage",
        theme_advanced_toolbar_location : "top",
        theme_advanced_toolbar_align : "left",
        theme_advanced_statusbar_location : "bottom",
        theme_advanced_resizing : true,
	*/