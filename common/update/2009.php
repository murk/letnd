<?php
// no es poden posar ; entre mig de la consulta ja que parteix les consultes per el ;

// així esquivo on ja ho ha posat en Marc manualment
if (!is_table('contact__configpublic')){


	$rs = Db::get_row('SELECT read1, write1 FROM `all__menu` WHERE menu_id = 4001');
	$read = $rs['read1'];
	$write = $rs['write1'];

	// update bernat contact
	$update_sql =
	"
		CREATE TABLE IF NOT EXISTS `contact__configpublic` (
		  `name` varchar(50) NOT NULL,
		  `value` varchar(255) NOT NULL,
		  `configpublic_id` int(11) NOT NULL auto_increment,
		  `language` varchar(3) NOT NULL,
		  PRIMARY KEY  (`configpublic_id`)
		) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

		--
		-- Volcar la base de datos para la tabla `contact__configpublic`
		--
		INSERT INTO `contact__configpublic` (`name`, `value`, `configpublic_id`, `language`) VALUES
		('from_address', 'info@letnd.cat', 1, ''),
		('m_host', 'mail.letnd.com', 2, ''),
		('m_username', 'info@letnd.cat', 3, ''),
		('m_password', '', 4, '');


		CREATE TABLE IF NOT EXISTS `contact__configadmin` (
		  `name` varchar(50) NOT NULL,
		  `value` varchar(255) NOT NULL,
		  `configadmin_id` int(11) NOT NULL auto_increment,
		  `language` varchar(3) NOT NULL,
		  PRIMARY KEY  (`configadmin_id`)
		) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;
		INSERT INTO `contact__configadmin` (`name`, `value`, `configadmin_id`, `language`) VALUES
		('custumer_table', 'custumer__custumer', 1, '');



		INSERT INTO `all__menu` (`menu_id`, `menu_group`, `tool`, `tool_section`, `action`, `parent_id`, `toolmode_id`, `variable`, `ordre`, `link`, `process`, `bin`, `read1`, `write1`) VALUES
		(4106, 'web', 'web', 'config', 'list_records', 4001, 1, 'WEB_MENU_CONFIGCONTACT_PUBLIC_LIST', 11, '', 'contact_public', 0, '$read', '$write'),
		 (4107, 'web', 'web', 'config', 'list_records', 4001, 1, 'WEB_MENU_CONFIGCONTACT_ADMIN_LIST', 10, '', 'contact_admin', 0, '$read', '$write');
	";
}
?>