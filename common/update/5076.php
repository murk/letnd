<?

/* 17-04-14
  Modificacions inmo - Descartar inmobles resultants de les demandes

 */

if (is_table('custumer__custumer') && !is_table('custumer__demand_to_property')) {	
	
	$update_sql .= "
		CREATE TABLE `custumer__demand_to_property` (
			`demand_property_id` INT(11) NOT NULL AUTO_INCREMENT,
			`demand_id` INT(11) NULL DEFAULT '0',
			`property_id` INT(11) NULL DEFAULT '0',
			`client_id` INT(11) NULL DEFAULT '0',
			`liked` TINYINT(1) NULL DEFAULT '0',
			`mailed` TINYINT(1) NULL DEFAULT '0',
			`refused` TINYINT(1) NULL DEFAULT '0',
			PRIMARY KEY (`demand_property_id`)
		)
		COLLATE='utf8_spanish2_ci'
		ENGINE=MyISAM;
		";	
	
	
}

if (is_table('custumer__history')) {		
	
	
	$update_sql .="
		ALTER TABLE `custumer__history`
			CHANGE COLUMN `tipus` `tipus` ENUM('various','to_call','waiting_call','assembly','show_property','like_property','send_information','demand_refused','demand_allowed') NOT NULL COLLATE 'utf8_spanish2_ci' AFTER `finished_date`,
			ADD COLUMN `demand_id` INT(11) NOT NULL DEFAULT '0' AFTER `message_id`;
			";
	
}


?>