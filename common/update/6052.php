<?

/* 19-09-15

  Actualitzaci� clients clients actius inmo

 */

if ( is_table( 'custumer__custumer' ) && ! is_field( 'custumer__custumer', 'activated' ) ) {

	$update_sql[] = "ALTER TABLE `custumer__custumer`
	ADD COLUMN `activated` TINYINT(1) NOT NULL DEFAULT '1' AFTER `book_main`;";

} else {
	if ( is_table( 'custumer__custumer' ) ) {
		trigger_error( 'Ja existeixen el camp activated' );
	}
}

if (!Db::get_first('SELECT count(*) FROM all__menu WHERE `menu_id` =5004')){

	$rs = Db::get_row('SELECT read1, write1 FROM `all__menu` WHERE menu_id = 5001');
	$read = $rs['read1'];
	$write = $rs['write1'];

	$update_sql[] =
		"
	INSERT INTO `all__menu` ( `menu_id` , `menu_group` , `tool` , `tool_section` , `action` , `parent_id` , `toolmode_id` , `variable` , `ordre` , `link` , `process` , `bin` , `read1` , `write1` )
	VALUES (
	'5004', 'custumer', 'custumer', 'custumer', 'list_records_inactive', '5001', '1', 'CUSTUMER_MENU_LIST_INACTIVE', '3', '', '', '0', '$read', '$write'
	);";
}

else {
	trigger_error('Ja existeix all__menu WHERE `menu_id` =5004');
}

?>