<?
/**
 * PvNorma
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2011
 * @version $Id$
 * @access public
 */
class PvNorma extends Module{
	var $parent_id, $q, $condition, $is_search = false;
	function __construct(){
		parent::__construct();
		
	}
	function on_load(){	
		
		$this->last_menu = &$GLOBALS['gl_page']->last_menu[$GLOBALS['gl_menu_id']][0];
		$this->last_menu_sub = &$GLOBALS['gl_page']->last_menu[$GLOBALS['gl_menu_id']][1];	
		
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_search_form() . $this->get_records();
	}
	function get_records()
	{
	    $this->parent_id = R::id('norma_id');
		
		$this->set_field('norma','class','');
		$this->set_field('codi_lloc','text_size','2');
		$this->set_field('codi_lloc','type','select');
		$this->set_field('numero','text_size','2');
		
		$listing = new ListRecords($this);
		$listing->add_dots_filter();
		
		$this->set_submenus();
		
		// en la cerca hi ha evolucions i normes
		if ($this->is_search) {
			
			$listing->set_field('evolucio','list_admin','input');
			
			$listing->condition = $this->condition;
			
			$listing->order_by =  'evolucio ASC';
			
		}
		// evolucions
		elseif ($this->parent_id){
			$listing->condition = "parent_id='" . $this->parent_id . "'";
			
			$listing->unset_field('codi_lloc');
			$listing->unset_field('categoria_id');
			$listing->unset_field('numero');
			$listing->unset_field('norma');
			$listing->unset_field('evolucions');
			$listing->set_field('evolucio','list_admin','input');
			
			$this->send_field_top('evolucio');
			
			$listing->order_by =  'evolucio ASC';
		}
		
		// normes
		else{
			$listing->condition = "parent_id='0'";
			
			$listing->add_filter ('categoria_id');
			
			$listing->group_fields = array('categoria_id');

			$listing->group_fields_function = 'format_groupfield';
			
			//$listing->add_button ('veure_evolucions_button', '', 'boto1');
			//$listing->add_button ('add_evolucio_button', 'action=show_form_new', 'boto2');
			
			$listing->order_by =  'categoria_id ASC';
		}		
		
		$listing->add_swap_edit();	
		$listing->call('records_walk',false,true);	
		
		
		
	    return $listing->list_records();
	}
	function records_walk($module){
		
		$ret = array();		
		$norma_id = $module->rs['norma_id'];
		$parent_id = $module->rs['parent_id'];
		
		
		// EVOLUCIONS nomes per normes
		if ($parent_id==0){
		
			$ret['evolucions'] = '';
			$results = Db::get_rows("
				SELECT evolucio, norma_id 
				FROM pv__norma 
				WHERE bin = 0
				AND parent_id = '".$norma_id."' 
				ORDER BY evolucio ASC");
			
			
			if ($results){
			
				$add_new_menu = '<a href="?menu_id=2403&amp;action=show_form_new&amp;parent_id='.$norma_id.'" class="bigger">'.$this->caption['c_add_evolucio_button'].'</a>';
				
				$ret['evolucions'] .= '<p style="white-space:nowrap;margin:0 12px 8px 0;"><a href="?menu_id=2403&amp;norma_id='.$norma_id.'" class="bigger">'.$this->caption['c_list_evolucions_button'].'</a> - ' . $add_new_menu . '</p>';
				
				foreach ($results as $rs) {
					$ret['evolucions'] .= '<p>- <a href="?menu_id=2403&amp;action=show_form_edit&amp;norma_id='.$rs['norma_id'].'">'.$this->caption['c_evolucio'] . ' ' . $rs['evolucio'].'</a></p>';
				}
			}
			else{
			
				$add_new_menu = '<a href="?menu_id=2403&amp;action=show_form_new&amp;parent_id='.$norma_id.'" class="bigger">'.$this->caption['c_add_evolucio_button'].'</a>';
				
				$ret['evolucions'] .= '<p style="white-space:nowrap;margin:0 12px 8px 0;">' . $add_new_menu . '</p>';
			}
		}
		
		// REQUISITS
		
		$top_id = $this->parent_id?$this->parent_id:$norma_id;
		
		$ret['requisits'] = '';
		
		if ($this->parent_id){
			$results = Db::get_rows("
				SELECT requisit_id, num_requisit, classificacio 
				FROM pv__requisit 
				WHERE bin = 0
				AND norma_id = '".$norma_id."' 
				ORDER BY num_requisit ASC");
			
			$last_norma = $norma_id;
		}
		else{
			$results = Db::get_rows("
				SELECT requisit_id, num_requisit, classificacio 
				FROM pv__requisit 
				WHERE bin = 0
				AND top_id = '".$top_id."' 
				ORDER BY num_requisit ASC");
			
			$last_norma = Db::get_first ('
					SELECT norma_id 
					FROM pv__norma 
					WHERE parent_id = ' . $top_id . '
					OR norma_id = ' . $top_id . '
					ORDER BY evolucio DESC LIMIT 1');
		
		}
		
		
		if ($results){
		
			$add_new_menu = '<a href="?menu_id=2403&amp;action=show_form_new&amp;tool_section=requisit&amp;norma_id='.$last_norma.'" class="bigger">'.$this->caption['c_add_requisit_button'].'</a>';
			
			$ret['requisits'] .= '<p style="white-space:nowrap;margin:0 0 8px 0;"><a href="?menu_id=2403&amp;tool_section=requisit&amp;top_id='.$top_id.'" class="bigger">'.$this->caption['c_list_requisits_button'].'</a> - ' . $add_new_menu . '</p>';
			
			foreach ($results as $rs) {
				$ret['requisits'] .= '<p>- <a href="?menu_id=2403&amp;action=show_form_edit&amp;tool_section=requisit&amp;requisit_id='.$rs['requisit_id'].'">' . $this->caption['c_requisit'] . ' ' . $rs['num_requisit'] . ' (' . $rs['classificacio'].')</a></p>';
			}
		}
		else{
		
			$add_new_menu = '<a href="?menu_id=2403&amp;action=show_form_new&amp;tool_section=requisit&amp;norma_id='.$last_norma.'" class="bigger">'.$this->caption['c_add_requisit_button'].'</a>';
			
			$ret['requisits'] .= '<p style="white-space:nowrap;margin:0 0 8px 0;">' . $add_new_menu . '</p>';
		}
		

		return $ret;
	}
	function set_submenus($parent_id=false, $norma_id=false){	
				
		$page = &$GLOBALS['gl_page'];	
		
		if ($parent_id){ 
			$link = '?process=apartat&menu_id=2403&norma_id=' . $parent_id;
		}
		else{
			$link = '';
		}	
		
		$norma_id = $parent_id?$parent_id:R::id('norma_id'); // hi ha parent id quan es form u un segon nivell
		
		if ($norma_id){
			$norma = Db::get_first("SELECT norma FROM pv__norma WHERE norma_id = " . $norma_id);	
			//$page->title = $norma;
			$page->subtitle = $norma;
		
			$page->add_breadcrumb ($norma, $link);
			//$page->add_breadcrumb ('', $link2);
			
			if ($parent_id) Page::add_breadcrumb ($this->caption['c_edit_data']);
			
			$this->last_menu = array('name'=>$norma,'link'=>$link);	
		}	
	}
	function format_groupfield(&$rs, $group_field, $val){		
		
		$rs = Db::get_row("SELECT categoria, codi_categoria, norma_id FROM pv__categoria,pv__norma WHERE pv__categoria.categoria_id = pv__norma.categoria_id AND norma_id = '" . $rs['norma_id'] . "'");
		
		return $rs['categoria'] . ' - ' . $rs['codi_categoria'];
	}
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{
		$this->parent_id = R::id('parent_id');
		
		
	    $show = new ShowForm($this);
		
		
		// nomes hi ha _GET parent id a add_record desde una norma
		if (!$this->parent_id)  {
			if ($this->action == 'show_form_new'){
				$this->parent_id = 0;
			}
			else
			{
				$show->get_values();
				$this->parent_id = $show->rs['parent_id'];
			}
		}
		$this->set_submenus($this->parent_id);
		
		// és una evolucio
		if ($this->parent_id){
			$show->unset_field('codi_lloc');
			$show->unset_field('categoria_id');
			$show->unset_field('numero');
			$show->unset_field('norma');
			
			$show->set_field('parent_id','form_admin','input');
			$show->set_field('parent_id','default_value',$this->parent_id);
			
			$show->set_field('nivell','form_admin','input');
			$show->set_field('nivell','default_value','2');
			
			$next_evolucio = Db::get_first ('
				SELECT evolucio 
				FROM pv__norma 
				WHERE parent_id = ' . $this->parent_id . '
				ORDER BY evolucio DESC LIMIT 1');
						
			
			if ($next_evolucio){
				$next_evolucio = chr(ord($next_evolucio)+1);
			}
			else {
				$next_evolucio = 'A';
			}
			
			$show->set_field('evolucio','form_admin','input');
			$show->set_field('evolucio','default_value',$next_evolucio);
			
			$parent_norma = Db::get_first ('SELECT norma FROM pv__norma WHERE norma_id = ' . $this->parent_id);
			
			$rs = Db::get_row ('SELECT norma,codi_lloc, 
			(SELECT codi_categoria 
				FROM pv__categoria 
				WHERE pv__categoria.categoria_id = pv__norma.categoria_id ) 
				as codi_categoria,
				numero
			FROM pv__norma 
			WHERE norma_id = ' . $this->parent_id);
			
			$parent_norma = $rs['norma'] . ' - <strong>' . strtoupper($rs['codi_lloc']) . ' ' . $rs['codi_categoria'] . ' ' . $rs['numero'] . '</strong>';
			
			$show->add_field('parent_norma');
			$show->set_field('parent_norma','type','none');
			$show->set_field('parent_norma','form_admin','out');
			
			$show->send_field_top('parent_norma');
		}
		
		
		if ($this->parent_id)
			$show->rs['parent_norma'] = $parent_norma;
		
		if ($this->action=='show_form_new'){
			$show->set_field('entered','default_value',now());
		}
		
	    return $show->show_form();
	}

	function save_rows()
	{
	    $save_rows = new SaveRows($this);
	    $save_rows->save();
	}

	function write_record()
	{
	    $writerec = new SaveRows($this);
	    $writerec->save();
		
		if ($this->action=='add_record'){
			$GLOBALS['gl_do_next'] = false;
			
			$next_evolucio = chr(ord($writerec->get_value('evolucio'))+1);
			
			print_javascript('
				if (parent.document.theForm) 
					parent.document.theForm.reset();				
				
				top.$("#evolucio_0").val("'. $next_evolucio .'");
				
				if (typeof parent.form_set_serialized == "function")
					parent.form_set_serialized();'
				);
		}
		
	}

	function manage_images()
	{
	    $image_manager = new ImageManager($this);
	    $image_manager->execute();
	}
	
	
	
	//
	// FUNCIONS BUSCADOR
	//
	function search(){

		$q = $this->q = R::escape('q');
		
		$search_condition = '';
		
		$this->is_search = true;

		if ($q)
		{	
			$search_condition = " (
				titol_norma like '%" . $q . "%'
				OR descripcio_norma like '%" . $q . "%'
				OR norma like '%" . strtolower($q) . "%'
				OR entered like '%" . $q . "%'
				" ;
			if(strpos($q,'/')){
				
				$date_arr = explode('/',$q);
				$date = '';
				if (count($date_arr)<=3){
					$date_arr = array_reverse($date_arr);
					$date = implode('-', $date_arr);
					$search_condition .= "
					OR entered like '%" . $date . "%'
					" ;
				}
			}
			
			$search_condition .= ") " ;			
			

			$GLOBALS['gl_page']->title = TITLE_SEARCH . '<strong>&nbsp;&nbsp;"' . $q . '"</strong>';
		}
		// per fer un reset de una cerca, busco cadena buida
		else{
			Main::redirect('/admin/?menu_id=' . $GLOBALS['gl_menu_id']);
		
		}
		$this->condition = $search_condition;
		Debug::add('Search condition', $this->condition);
		$this->do_action('list_records');
	}

	function get_search_form(){
		$tpl = new phemplate(PATH_TEMPLATES);
		$tpl->set_vars($this->caption);
		$tpl->set_file('search.tpl');
		$tpl->set_var('menu_id',$GLOBALS['gl_menu_id']);
		$tpl->set_var('q',  htmlspecialchars(R::get('q')));
		$ret = $tpl->process();		
		
		if ($this->q) {
			$GLOBALS['gl_page']->javascript .= "
				$('table.listRecord').highlight('".addslashes(R::get('q'))."');			
			";
		}
		
		return $ret;
	}
}
?>