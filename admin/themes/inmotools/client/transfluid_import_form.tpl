<style type="text/css">
	#import div.import {
		padding: 20px;
		border-top: 1px solid #D3D3D3;
	}
	#import table td {
		padding: 2px 5px;
	}
	#import table th {
		padding: 2px 5px;
		text-align: left;
	}
	h6 {
		font-size: 20px;
		margin: 10px 5px;
	}
	#confirm {
		display: none;
		padding: 25px;
		overflow: hidden;
		width: 80%;
	}
	#group_exists {
		display: none;
		padding: 25px;
		font-size: 16px;
	}
	.done {
		display: none;
		padding: 25px;
		font-size: 20px;
	}
	td.gris {
		color: #666666;
	}
	tr.vermell td {
		COLOR: #CB2903;
	}
	tr.blau td {
		COLOR: #12a6ee;
	}
	td.vermell {
		COLOR: #CB2903 !important;
		font-weight: 700;
	}
	.llegenda {
		display: flex;
		align-items: center;
		padding-top: 40px;
		padding-bottom: 40px;
	}
	.llegenda > div {
		display: flex;
		align-items: center;
		padding-right: 40px;
	}
	.llegenda span {
		display: block;
		width: 50px;
		height: 20px;
		margin-right: 20px;
	}
	.llegenda span.gris {
		background-color: #666666;
	}
	.llegenda span.vermell {
		background-color: #CB2903;
	}
	.llegenda span.blau {
		background-color: #12a6ee;
	}
</style>
<script type="text/javascript" src="/common/jscripts/notify/pnotify.custom.min.js"></script>
<link href="/common/jscripts/notify/pnotify.custom.css?v=<?= $version ?>" rel="stylesheet" type="text/css"/>
<script type="text/javascript">

	function notify_import_success() {

		top.$("#confirm").hide();
		top.$(".done").show();

		top.PNotify.removeAll();

		new PNotify({
			title: 'Importació finalitzada',
			icon: false,
			type: 'success',
			hide: false
		});
	}

	function notify_preview_success() {

		top.$("#confirm").show();
		top.$(".done").hide();

		top.PNotify.removeAll();

		new PNotify({
			title: 'Previsualització finalitzada',
			icon: false,
			type: 'success',
			hide: false
		});
	}

	$(function () {
		$('#excel').change(function () {

			var val = $('#excel').val();

			var ext_arr = val.split('.');
			var ext = ext_arr[ext_arr.length - 1];

			if (ext != 'xlsx' && ext != 'xls') {
				alert('L\'arxiu seleccionat no és un arxiu d\'excel correcte');
				$('#excel').val('');
				return;
			}
		});

		$("input[name=import]").click(function () {

			var val = $('#excel').val();

			if (!val) {
				new PNotify({
					title: 'S\'ha de triar un arxiu',
					icon: false,
					type: 'error'
				});
				return false;
			}

			new PNotify({
				title: 'Importació',
				text: 'Obtinguent registres...',
				icon: false,
				type: 'info',
				hide: false
			});

		});
		$("input[name=preview]").click(function () {

			var val = $('#excel').val();

			if (!val) {
				new PNotify({
					title: 'S\'ha de triar un arxiu',
					icon: false,
					type: 'error'
				});
				return false;
			}

			new PNotify({
				title: 'Previsualització',
				text: 'Obtinguent registres...',
				icon: false,
				type: 'info',
				hide: false
			});

		});
	});

</script>
<form name="theForm" method="post" action="/admin/?menu_id=2932&action=add_record" enctype="multipart/form-data" target="save_frame">
	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="form">
		<tr>
			<td>
				<div id="form_buttons_holder">
					<table id="form_buttons">
						<tr>
							<td valign="bottom">
								<div class="menu"></div>
							</td>
							<td class="buttons_top">
								<div class="right"></div>
							</td>
						</tr>
					</table>
				</div>


				<? ////////////   TITOL  ////////////////?>


				<table width="100%" cellpadding="0" cellspacing="0">
					<tr>
						<td class="formCaptionTitle"><strong>
								<?= $c_import_title ?>
								&nbsp;&nbsp;</strong></td>
					</tr>
				</table>


				<? ////////////   CAMPS  ////////////////?>

				<table width="100%" cellpadding="0" cellspacing="0" class="formstbl">

					<tr>
						<td valign="top" class="formsCaption">
							<?= $c_group ?>:
						</td>
						<td valign="top" class="forms">
							<input size="50" type="input" id="group" name="group" value="Empreses"/>
						</td>
					</tr>


					<tr>
						<td valign="top" class="formsCaption">
							<?= $c_select_file ?>:
						</td>
						<td valign="top" class="forms">
							<input class="file" type="file" id="excel" name="excel"/>
						</td>
					</tr>

				</table>

				<table width="100%" cellpadding="4" cellspacing="2" class="formsButtonstbl">
					<tr>
						<td align="center" class="formsButtons">
							<div class="centerFloat1">
								<div class="centerFloat2">
									<input type="submit" name="preview" value="<?= $c_import_preview ?>" class="btn_form1">
									<input type="submit" name="import" value="<?= $c_import ?>" class="btn_form1">
								</div>
							</div>
						</td>
					</tr>
				</table>

				<div class="done">
					Importació finalitzada
				</div>

				<div id="import">
				</div>
				<div id="confirm">
					<input type="submit" name="import" value="<?= $c_import ?>" class="btn_form1">
				</div>
				<div class="done">
					Importació finalitzada
				</div>

			</td>
		</tr>
	</table>
</form>