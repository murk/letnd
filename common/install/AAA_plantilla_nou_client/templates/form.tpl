
	<form name="theForm" method="post" action="<?= $link_action ?>" onsubmit="<?= $js_string ?>" encType="multipart/form-data">
		<? if ( $c_form_title ): ?>
			<h3><?= $c_form_title ?></h3>
		<? endif//c_form_title?>
		<table class="form">
			<? foreach ( $fields as $l ): extract( $l ) ?>
				<tr id="<?= $field_name . "_holder" ?>">
					<th><?= $key ?>:</th>
					<td><? foreach ( $val as $l ): extract( $l ) ?>
							<?= $val ?>
						<? endforeach //$val?></td>
				</tr>
			<? endforeach //$field?>
		</table>
		<? if ( $has_images ): ?>
			<div class="listImage">
				<? if ( $image_name ): ?>
					<img src="<?= $image_src_details ?>">
				<? endif // image_name?>
			</div>
		<? endif // has_images?>
		<? if ( $form_new ): ?>
			<? if ( $save_button ): ?>
				<div class="buttons"><input class="boto" type="submit" name="Submit2" value="<?= $c_send_new ?>"/></div>
			<? endif //save_button?>
		<? endif //form_new?>
		<? if ( $form_edit ): ?>
			<? if ( $save_button ): ?>
				<div class="buttons"><input class="boto" type="submit" name="Submit" value="<?= $c_send_edit ?>"/></div>
			<? endif //save_button?>
		<? endif //form_edit?>
	</form>
	<script type="text/javascript">
		$(function () {
			set_focus(document.theForm);
		});
	</script>
