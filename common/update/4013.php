<?php
// no es poden posar ; entre mig de la consulta ja que parteix les consultes per el ; a no ser que fem servir $update_sql en array()
if (is_table("product__order")){
	
	$update_sql[]="ALTER TABLE `product__order`
						ADD COLUMN `total_equivalencia` DECIMAL(11,2) NOT NULL AFTER `total_tax`,
						ADD COLUMN `all_equivalencia` DECIMAL(11,2) NOT NULL AFTER `all_tax`;";	
						
	$update_sql[]="ALTER TABLE `product__orderitem`
						ADD COLUMN `product_equivalencia` DECIMAL(11,2) NOT NULL AFTER `product_tax`,
						ADD COLUMN `product_total_equivalencia` DECIMAL(11,2) NOT NULL AFTER `product_total_tax`;";
}	
if (is_table("product__configadmin")){
	
	$update_sql[]="INSERT INTO `product__configadmin` (`name`, `value`) VALUES ('equivalencia', '0');";	
}	
?>