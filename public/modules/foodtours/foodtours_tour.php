<?
/**
 * FoodtoursTour
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 3/5/2016
 * @version $Id$
 * @access public
 */
class FoodtoursTour extends Module{
	function __construct(){
		parent::__construct();
	}
	function on_load(){
		
		//$this->set_friendly_params(array(
		//	'place_id'=>R::text_id('place_id')
		//	));
		parent::on_load();
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function get_records()
	{
	   
		
		$listing = new ListRecords($this);
		$listing->call('records_walk','content,content_list', true);
		
		$listing->condition .= "status = 'public'";
			
		$listing->order_by = 'ordre ASC, tour ASC, entered DESC';
		
		
		if ($this->parent == 'Block') {
			$listing->name = 'Block';
			$listing->paginate = false;
			$listing->order_by = 'in_home DESC, ordre ASC, tour ASC, entered DESC';
			$listing->limit = '4';
			$listing->set_records();
			return $listing->loop;
		}
		
	    return $listing->list_records();
	}
	function records_walk(&$listing, $content=false,$content_list=false){		
		
		$ret = array();
		$ret['content']= add_dots('content', $content);
		return $ret;
	}
	
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{
		
	    $show = new ShowForm($this);
		$show->condition .= "status = 'public'";
		
		$show->get_values();
		if ($show->has_results)
		{
			
			Page::set_page_title($show->rs['page_title'],$show->rs['tour']);
			Page::set_page_description($show->rs['page_description'],$show->rs['content']);
			Page::set_page_keywords($show->rs['page_keywords']);

			$html =  $show->show_form();

			Page::set_page_og($show->rs);
			return $html;
			
		}
		else
		{
			Main::redirect('/');
		}
		
	}
}
?>