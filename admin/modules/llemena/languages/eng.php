<?php
// menus eina
if (!defined('LLEMENA_MENU_PAGINA')){
define('LLEMENA_MENU_PAGINA','Edició pàgines'); 
define('LLEMENA_MENU_PAGINA_NEW','Entrar nova pàgina');
define('LLEMENA_MENU_PAGINA_LIST','Pàgines Capcelera'); 
define('LLEMENA_MENU_PAGINA_LIST2','Pàgines Lateral Esquerra'); 
define('LLEMENA_MENU_PAGINA_LIST3','Pàgines "Més coses" Esquerra');
define('LLEMENA_MENU_PAGINA_LIST4','Pàgines Més coses" Dreta');
define('LLEMENA_MENU_PAGINA_BIN','Paperera de reciclatge'); 
define('LLEMENA_MENU_CATEGORY','Tipus'); 
define('LLEMENA_MENU_CATEGORY_NEW','Entrar nou tipus'); 
define('LLEMENA_MENU_CATEGORY_LIST','Llistar tipus'); 

define('LLEMENA_MENU_RESTAURANT','Restaurants'); 
define('LLEMENA_MENU_RESTAURANT_NEW','Entrar nou restaurant'); 
define('LLEMENA_MENU_RESTAURANT_LIST','Llistar restaurants'); 
define('LLEMENA_MENU_RESTAURANT_BIN','Paperera de reciclatge'); 

define('LLEMENA_MENU_ALLOTJAMENT','Allotjaments');  
define('LLEMENA_MENU_ALLOTJAMENT_NEW','Entrar nou allotjament'); 
define('LLEMENA_MENU_ALLOTJAMENT_LIST','Llistar allotjament'); 
define('LLEMENA_MENU_ALLOTJAMENT_BIN','Paperera de reciclatge');

define('LLEMENA_MENU_ALLOTJAMENTCATEGORY','Categorías'); 
define('LLEMENA_MENU_ALLOTJAMENTCATEGORY_NEW','Entrar nova categoria'); 
define('LLEMENA_MENU_ALLOTJAMENTCATEGORY_LIST','Llistar categories'); 

define('LLEMENA_MENU_AGENDA','Agenda');  
define('LLEMENA_MENU_AGENDA_NEW','Entrar nova activitat'); 
define('LLEMENA_MENU_AGENDA_LIST','Llistar activitats'); 
define('LLEMENA_MENU_AGENDA_BIN','Paperera de reciclatge'); 

define('LLEMENA_MENU_AGENDACATEGORY','Categories'); 
define('LLEMENA_MENU_AGENDACATEGORY_NEW','Entrar nova categoria'); 
define('LLEMENA_MENU_AGENDACATEGORY_LIST','Llistar categorías'); 
}

/*--------------- nomès castellà i català, no traduit a la resta -------------------*/

$gl_caption_pagina['c_menu_1']='Capcelera';
$gl_caption_pagina['c_menu_2']='Lateral Esquerra';
$gl_caption_pagina['c_menu_3']='Més coses Esquerra';
$gl_caption_pagina['c_menu_4']='Més coses Dreta';
$gl_caption_pagina['c_ordre'] = 'Orden';
$gl_caption_pagina['c_template'] = 'Plantilla';
$gl_caption_pagina['c_paginatpl_id'] = 'Plantilla';
$gl_caption_pagina['c_link'] = 'Link';
$gl_caption_pagina['c_page_title'] = 'Título en Google';
$gl_caption_pagina['c_title'] = 'Título dentro la pàgina';
$gl_caption_pagina['c_body_subtitle'] = 'Subtítulo';
$gl_caption_pagina['c_pagina'] = 'Nombre de la pàgina en el menú';
$gl_caption_pagina['c_content'] = 'Contenido';
$gl_caption_pagina['c_body_content_list'] = 'Descripción corta al llistat';
$gl_caption_pagina['c_body_content'] = 'Contingut';
$gl_caption_pagina['c_prepare'] = 'En preparación';
$gl_caption_pagina['c_review'] = 'Para revisar';
$gl_caption_pagina['c_public'] = 'Público';
$gl_caption_pagina['c_archived'] = 'Archivado';
$gl_caption_pagina['c_status'] = 'Estado';
$gl_caption_pagina['c_pagina_id'] = 'Id';
$gl_caption_pagina['c_parent_id'] = 'Parent';
$gl_caption_pagina['c_level'] = 'Nivel';
$gl_caption_pagina['c_menu'] = 'Menú';
$gl_caption_pagina['c_blocked'] = 'Block.';
$gl_caption_pagina['c_filter_parent_id'] = 'Parent';
//$gl_caption_pagina['c_filter_top_parent'] = 'Totes';
$gl_caption_pagina['c_filter_top_parent2'] = 'Mostrar subpàgines de';
$gl_caption_pagina['c_filter_level'] = 'Level';
$gl_caption_pagina['c_has_content'] = 'Content';
$gl_caption_pagina['c_has_content_0'] = 'no';
$gl_caption_pagina['c_has_content_1'] = 'si';
$gl_caption_pagina['c_edit_content_button'] = 'Editar página';
$gl_caption_pagina['c_edit_pagina_button'] = 'Editar datos';
$gl_caption_pagina['c_url1_name']='Nombre URL(1)';
$gl_caption_pagina['c_url1']='URL(1)';
$gl_caption_pagina['c_url2_name']='Nombre URL(2)';
$gl_caption_pagina['c_url2']='URL(2)';
$gl_caption_pagina['c_url3_name']='Nombre URL(3)';
$gl_caption_pagina['c_url3']='URL(3)';
$gl_caption_pagina['c_address']='Dirección';
$gl_caption_pagina['c_visit']='Visita';
$gl_caption_pagina['c_videoframe']='Vídeo (youtube, metacafe...)';
$gl_caption_pagina['c_file']='Archivos';
$gl_caption_pagina['c_category_id']='Tipus';
$gl_caption_pagina['c_category_id_caption'] = 'Cap tipus';
$gl_caption['c_municipi']='Town';
$gl_caption['c_municipi_caption'] = 'Cap municipi';
$gl_caption['c_municipi_santgregori']='Sant Gregori';
$gl_caption['c_municipi_canetadri']='Canet d\'Adri';
$gl_caption['c_municipi_santaniolfinestres']='Sant Aniol de Finestres';
$gl_caption['c_municipi_santmartillemena']='Sant Martí de Llémena';
$gl_caption['c_remove_polyline']='Borrar trazado';

$gl_caption['c_ordre']='Orden';
$gl_caption['c_category']='Categoría';

// mapes
$gl_caption['c_search_adress']='Buscar adreça';
$gl_caption['c_latitude']='latitud';
$gl_caption['c_longitude']='longitud';
$gl_caption['c_found_direction']='adreça trobada';
$gl_caption['c_edit_map'] = 'Mapa';
$gl_caption['c_save_map'] = 'Guardar ubicació';
$gl_caption['c_click_map'] = 'Fes un click en un punt o més del mapa per guardar la ubicació';
$gl_caption['c_form_map_title'] = 'Ubicació';
$gl_messages['point_saved'] = 'Nova ubicació guardada correctament';

// restaurant

$gl_caption['c_home']='Destacado';
$gl_caption['c_telephone'] = 'Phone';
$gl_caption['c_telephone_mobile'] = 'Cell Phone';
$gl_caption['c_fax'] = 'Fax';
$gl_caption['c_mail'] = 'E-mail';
$gl_caption['c_url'] = 'Url';
$gl_caption['c_adress'] = 'Direction';
$gl_caption['c_price_id'] = 'Price rank';
$gl_caption['c_customer_description'] = 'Description';
$gl_caption['c_data']='Datos del cliente';
$gl_caption['c_general_data']='Datos generales';
$gl_caption['c_descriptions']='Descripciones';
$gl_caption['c_status'] = 'Estado';
$gl_caption['c_prepare'] = 'En preparación';
$gl_caption['c_review'] = 'Para revisar';
$gl_caption['c_public'] = 'Público';
$gl_caption['c_archived'] = 'Archivado';
$gl_caption['c_owner_name'] = 'Nombre del gerente (privado)';
$gl_caption['c_owner_surnames'] = 'Apellidos del gerente (privado)';
$gl_caption['c_owner_phone'] = 'Teléfono del gerente (privado)';
$gl_caption['c_file']='Archivos';
$gl_caption['c_edit_map'] = 'Mapa';
$gl_caption['c_edit_data'] = 'Editar Cliente';
$gl_caption['c_save_map'] = 'Guardar ubicación';
$gl_caption['c_click_map'] = 'Haz un clic en un punto del mapa para establecer la situación';
$gl_caption['c_form_map_title'] = 'Ubicación del cliente';
$gl_caption['c_country_id']='País';
$gl_caption['c_municipi_id']='Town';
$gl_caption['c_provincia_id']='Provincia';
$gl_caption['c_comarca_id']='Comarca';
$gl_caption['c_schedule']='Timetable';
$gl_caption['c_observations']='Observaciones (privado)';

$gl_caption_restaurant['c_description']='Descripción';
$gl_caption_restaurant['c_customer_title'] = 'Nombre del restaurante';
$gl_caption_restaurant['c_preu_carta']='Menu price';
$gl_caption_restaurant['c_preu_menu']='Daily Menu price';
$gl_caption_restaurant['c_servei_adicional']='Additional services';
$gl_caption_restaurant['c_festa_semanal']='Weekly holiday';
$gl_caption_restaurant['c_is_cuina']='Cocina de "les Carboneres"';
$gl_caption_restaurant['c_is_cuina_list']='C. Carboneres"';
$gl_caption_restaurant['c_yes']='Sí';
$gl_caption_restaurant['c_no']='No';

$gl_caption_allotjament['c_customer_title'] = 'Nombre del alojamiento';
$gl_caption_allotjament['c_customer_description'] = 'Características';
$gl_caption_allotjament['c_presentacio'] = 'Presentación';
$gl_caption_allotjament['c_servei_interior']='Interior services';
$gl_caption_allotjament['c_servei_exterior']='Exterior services';
$gl_caption_allotjament['c_preu']='Price';
$gl_caption_allotjament['c_allotjamentcategory_id']='Categoría';

$gl_caption['c_ordre']='Ordre';
$gl_caption['c_allotjamentcategory']='Categoría';
$gl_caption['c_view_button']='Ver alojamientos';

$gl_caption_agendacategory['c_agendacategory_id']='Categoría';
$gl_caption_agendacategory['c_agendacategory']='Categoría';
$gl_caption_agendacategory['c_new_button']='Ver actividades';


$gl_caption_agenda['c_agenda'] = 'Actividad';
$gl_caption_agenda['c_title'] = 'Título';
$gl_caption_agenda['c_subtitle'] = 'Subtítulo';
$gl_caption_agenda['c_content'] = 'Contenido';
$gl_caption_agenda['c_entered'] = 'Fecha';
$gl_caption_agenda['c_status'] = 'Estado';
$gl_caption_agenda['c_category_id'] = 'Categoría';
$gl_caption_agenda['c_filter_category_id'] = 'Todas las categorías';
$gl_caption_agenda['c_filter_status'] = 'Todas los estados';
$gl_caption_agenda['c_ordre'] = 'Orden';
$gl_caption_agenda['c_url1_name']='Nombre URL(1)';
$gl_caption_agenda['c_url1']='URL(1)';
$gl_caption_agenda['c_url2_name']='Nombre URL(2)';
$gl_caption_agenda['c_url2']='URL(2)';
$gl_caption_agenda['c_videoframe']='Vídeo (youtube, metacafe...)';
$gl_caption_agenda['c_file']='Archivos';
$gl_caption_agenda['c_in_home']='Sale en la home';
$gl_caption_agenda['c_content_list']='Descripción corta al listado';
?>