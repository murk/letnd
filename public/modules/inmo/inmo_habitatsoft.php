<?
/**
 * InmoHabitatsoft
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2011
 * @version $Id$
 * @access public
 */
 /*
FEATURES
	Adreça -> "/inmo/habitatsoft/import/"
	o
	Adreça -> "/?tool=inmo&tool_section=habitatsoft&action=import" ( no va per culpa HS que posen un interrogant per les seves vars
	
	Vars HS
	?LicenciaVH=500000&PasswordVH=50000
	
	Adreça resultant habitatsoft
	/inmo/habitatsoft/import/?LicenciaVH=500000&PasswordVH=50000
	www.agenciaportselva.com/inmo/habitatsoft/import/?LicenciaVH=500133&PasswordVH=54750
	
	El primer cop actualitza les propietats amb la mateixa 'ref'
		- Per poder conservar els ids de letnd el primer cop que es fa la importacio
	Tots els altres cops ( si hi ha habitat_soft_id ) actualitza les propietats amb el mateix habitat_soft_id
		- S'ha pogut canviar el ref desde el habitatsoft i no coincidir ja amb el de letnd
	Borra totes les propietats que no s'han pujat
		-Per no fer-ho hauria de comunicar-me amb habitatsoft per dirme quines borrar i contestar quines s'han borrat
	Borra totes les imatges que no s'han pujat
		-Per no fer-ho hauria de comunicar-me amb habitatsoft per dirme quines borrar i contestar quines s'han borrat
	Imatges, quan s'ha fet tota l'importació de dades, mou i redimensiona totes les imatges noves
	Imatges si la data de modificació es igual no fa res
	Videos i Files, nomès borra en els registres sobrants ( registres que no existeixen a habitatsoft )
	Tipus inmoble, situacio, provincia, poblacio:   borra tots abans d'insertar de nou
	Tipus inmoble,  si no està dins l'array, retorno per defecte Lloguer
	Descripció, si hi ha una descripcio portales, substitueixo la que fa habitatsoft automàtica
		
PREGUNTES A HABITATSOFT
	es pot saber quines propietats s'han borrat ( si sempre es pujen totes es facil saber-ho, però quan nomes es publica les selecionades no puc borrar tota la resta )
	es pot passar una contrasenya per post?
	com puc detectar amb PHP si es habitatsoft que va a la pàgina?
	habitatsoft com sab si tot ha anat be?
	idPiso i idSituacion es 674408600.11, que vol dir 11? tipus de camp?
	noms arxius sempre amb majuscules?
	com exporta el programa, tot sempre, o els registres seleccionats. I les imatges sempre totes?
	si es borra un registre com se sab?
	FechaImagen1 es la data que s'ha insertat l'imatge a HABITATSOFT
	FechaHoraModificado, si s'han canviat imatges canvia aquesta data?
	NombreTipoInmuebleDefectoCS o NombreTipoInmuebleCS
	Qhe es IdMAEPoblacion?
	Parquing
	Hab dobles inclou suites?
	
	Quan faig un reset d'imatges hauria d'enviar alguna variable al publicar -> ara haig de trampejar amb UPDATE `inmo__property_image` SET `entered`='2011-10-10 19:03:00'
	
	Sistema de seguretat tipus Paypal, retornar una resposta a habitatsoft, i habitatsoft tornar resposta a letnd

PER FER
	comprobar error de mes de 10000 caracters ( que faig? conto numero de camps i ajunto un registre amb l'altre fins que sumi be?
	posar traspas a tipus a letnd
	insertar codi postatl a letnd
	
		
CANVIS EN BBDD
	ALTER TABLE `inmo__property`
		ADD COLUMN `habitatsoft_id` INT(11) NOT NULL DEFAULT '0' AFTER `property_id`;
	ALTER TABLE `inmo__property_image`
		ADD COLUMN `entered` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP AFTER `main_image`;
	INSERT INTO `inmo__configpublic` (`name`, `value`) VALUES ('allow_habitatsoft', '1');
	INSERT INTO `inmo__configpublic` (`name`, `value`) VALUES ('habitatsoft_pass', 'pass');
	INSERT INTO `inmo__configpublic` (`name`, `value`) VALUES ('habitatsoft_client_ref', '');
	
LETND NO TE
	Habitacions dobles, suites i individuals
	Cuina
	Any

HABITATSOFT NO TE
	Pati
	Metros garage
	Metros 
VARS
	$conta - conta les operacions que fa el programa p(text, level= 2)
	$habitatsoft_client_ref - referencia del client a habitatsoft, s'agafa de $_GET['LicenciaVH']
	$habitatsoft_id - Id del registre actual que estem entrant
	$habitatsoft_table - Taula del registre actual que estem entrant
	$letnd_id - Id del registre actual que estem entrant de letnd
	$habitatsoft_expediente - numero d'expedient a habitatsoft que equival a property_id
	$propertys_added - array que guarda totes les propietats que s'han actualitzat o agegit
	$images_added - array que guarda totes les imatges per poder posar-les al seu lloc al finalitzar
	$images_delete - array que guarda totes les imatges per s'han de borrar
	$habitat_languages - array que guarda la relacio idiomes habitatsoft a idiomes letnd
	$habitat_tipus - array que guarda la relacio tipus habitatsoft a tipus letnd
	$habitat_tipus2 - array que guarda la relacio tipus habitatsoft a tipus2 letnd ( nou o segonama )

$COLUMN_NAMES ===== CONTE ESTRUCTURA DE LA TAULA ( CADA ARXIU TXT )
-Els registres que falten són camps que no importo i per tant no tinc definits a la taula letnd.export__habitatsoft

$column_names =
array (
  'IdPiso' => 
  array (
	'habitatsoft_field' => 'IdPiso',
	'letnd_table' => 'inmo__property',
	'letnd_field' => 'property_id',
	'function' => '',
  ),
  'FechaHoraRegistro' => 
  array (
	'habitatsoft_field' => 'FechaHoraRegistro',
	'letnd_table' => 'inmo__property',
	'letnd_field' => 'entered',
	'function' => '',
  ),
  'IdTipoInmueble' => 
  array (
	'habitatsoft_field' => 'IdTipoInmueble',
	'letnd_table' => 'inmo__property',
	'letnd_field' => 'category_id',
	'function' => '',
  ),
)

$ROW_HABITATSOFT ===== CONTE REGISTRE TAL QUAL EL CSV ( coincideixen keys amb la de dalt )
$row_habitatsoft =
  'inmo__property_image' => 
		array (
		  'IdPiso' => '674408600.11',
		  'FechaHoraRegistro' => '02/10/2001 17:38:51',
		  'IdTipoInmueble' => '4',	
		)
$ROW ====== CONTE REGISTRE PER UNA TAULA A LETND
	$row = $row_habitatsoft['inmo__property_image'] =
		array (
		  'IdPiso' => '674408600.11',
		  'FechaHoraRegistro' => '02/10/2001 17:38:51',
		  'IdTipoInmueble' => '4',	
		)

GET_INSERT i GET_UPDATE  ======== SE LI PASSA REGISTRE AIXÍ:	
	  array (
		'name' => '1100_000002_foto_1_674408600.jpg',
		'ordre' => '1',
		'entered' => '05/06/2002 11:36:54',
		...
	  )
	
*/
class InmoHabitatsoft extends Module{
	var $conta = 0;
	var $habitatsoft_client_ref = '';
	var $habitatsoft_id = 0;
	var $habitatsoft_table = '';
	var $letnd_id = 0;
	var $habitatsoft_expediente = 0;
	var $propertys_added = array();
	var $images_added = array();
	var $images_delete = array();	
	var	$habitat_languages = array(
			'CS' => 'spa',
			'CT' => 'cat',
			'FR' => 'fra',
			'IN' => 'eng',
			'AL' => 'deu'
			);	
	var	$habitat_tipus = array(
			'3' => 'rent',
			'4' => 'sell',
			'5' => 'rent' // traspas de moment no tenim
			);
	var	$habitat_tipus2 = array(
			'0' => 'second',
			'1' => 'new'
			);
	// el key es l'id a habitatsoft, i el val l'id a letnd
	var	$habitat_categorys = array(
			'3' => '4', // pis
			'4' => '3', //casa
			'5' => '5', // terreny
			'6' => '17', // nau
			'7' => '7', // local comercial
			'8' => '10', // parking
			'9' => '21' // inmueble singular
			);
	function __construct(){
		parent::__construct();
		$this->habitatsoft_client_ref = $this->config['habitatsoft_client_ref'];
		// m'assseguro que nomès es faci al clients habilitats
		if (
				!isset($this->config['allow_habitatsoft']) || 
				!$this->config['allow_habitatsoft'] || 
				!isset($_GET['PasswordVH']) ||
				($this->config['habitatsoft_pass']!=$_GET['PasswordVH']) || 
				!isset($_GET['LicenciaVH']) ||
				($this->config['habitatsoft_client_ref']!=$_GET['LicenciaVH'])
			){
				Header( "HTTP/1.1 301 Moved Permanently" ); 
				Header( "Location: /" );
		}
		// inicio output
		echo "
		<html>
		<head>
		<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
		<style>
			.m1{
				font-size:20px;
				font-weight: bold;
				margin: 20px 0 10px 0;
			}
			.m2{				
				font-size:12px;
				margin:5px 0 0 20px;
				font-weight: bold;
				color:#484848;
			}
			.m2 i, .m1 i{	
				font-weight: normal;
				color:#706f6f;
			}
			.m3{				
				font-size:11px;
				margin:0 0 0 30px;
				color:#484848;
			}
			.m4{				
				font-size:11px;
				margin:0 0 0 40px;
				color:#484848;
			}
		</style>
		<script>
			var a = true;
			function moveWin(){
				window.scrollTo(0, document.body.scrollHeight);
				if (a) t = setTimeout('moveWin();',100);
			}			
		</script>
		</head>
		<body onload=''>
		<script>
			moveWin();		
		</script>";
	}
	function show_end(){
		$this->p('Importació finalitzada  - <i>' . $this->conta . ' operacions en ' . $execution_time = Debug::get_execution_time() . ' segons </i>', 1);
		echo "
		<script>
			a = false;	
		</script></body></html>";
	}
	
	function import(){		
		set_time_limit(0);		//ini_set('implicit_flush', true);		//error_reporting(E_WARNING);
		$this->empty_tables(); // buido algunes les taules per insertar tot de nou
		$this->import_data();
		$this->change_ids(); // canvio els ids de les taules relacionades
		$this->change_municipi_ids(); // canvio els ids dels municipis i comarca_id
		$this->delete_files(); // borro tots els videos i arxius dels registres que no figuren a habitatsoft
		$this->delete_propertys(); // borro tots els registres de propietats que no figuren a habitatsoft
		$this->delete_images();	 // borro totes i cadascuna de les imatges
		$this->move_images();	 // moc i redimensiono tots les imatges pujades
		$this->show_end();	 // moc i redimensiono tots les imatges pujades		//Debug::p_all();
		send_mail_admintotal('Actualitzacio habitatsoft','Actualització finalitzada');
		die();
	}
	
	function empty_tables(){
		$this->p('Buidant taules fixes', 1);			
		// borro anteriors
		$query = "DELETE FROM inmo__category";
		Db::execute($query );		
		$this->p($query, 3);
		
		$query = "DELETE FROM inmo__category_language";
		Db::execute($query );		
		$this->p($query, 3);
		
		//$query = "DELETE FROM inmo__provincia"; FAIG SERVIR ELS IDS DE LETND
		//Db::execute($query );		
		//$this->p($query, 3);
		
		//$query = "DELETE FROM inmo__municipi";
		//Db::execute($query );		
		//$this->p($query, 3);
		
		$query = "DELETE FROM inmo__zone";
		Db::execute($query );
		$this->p($query, 3);
	
	}
	function utf8_fopen_read($fileName) {
		$fc = utf8_encode(file_get_contents($fileName));
		$handle=fopen("php://memory", "rw");
		fwrite($handle, $fc);
		fseek($handle, 0);
		return $handle;
	} 
	function import_data(){
	
		$files = array('UN',/*'TipoOperacion','FamiliaTipoInmueble',*/ 'TipoInmueble','Situacion',/*'Provincia','Promocion','Poblacion',*/'IN','FR','CT','CS','AL');
		
		//$dir = dir(CLIENT_PATH . '/habitatsoft');	
		// Faig bucle per cada arxiu TXT, la primera linea son columnes, la resta son registres
		foreach ($files as $file)
		{
			// inicialitzo variables de cada Taula (TXT)
			$column_names = array();
			$keys = array();
		
			$this->habitatsoft_table = $file;
			//debug::p($this->habitatsoft_table,'nom taula');
			$file_handle = $this->utf8_fopen_read(CLIENT_PATH . '/habitatsoft/' . $this->habitatsoft_client_ref . '_' . $file . '.TXT');
			$this->p('Processant taula: ' . $this->habitatsoft_table, 1);

			$conta = 0;
			while (($data = fgetcsv($file_handle, 0, ";")) !== FALSE) {					
				
				// noms columnes
				if ($conta==0){
					Db::connect_mother();
					foreach ($data as $key=>$field){
					
						// busco el nom de la taula i camp a la bbdd de relacions
						$query = "SELECT habitatsoft_field, letnd_table, letnd_field, function
							FROM export__habitatsoft
							WHERE letnd_table <>  '' AND
							habitatsoft_table= '".$this->habitatsoft_table."'
							AND habitatsoft_field= '". $field ."'";
						$rs = Db::get_row($query);
						
						// nomès faig algo quan està definida 'letnd_table', sino passo del camp
						if ($rs) {
							$column_names[$field]= $rs;
							
							// per relacionar noms columna ($column_names) amb registre ($row_habitatsoft)
							$keys[$key] = $field;
						}
						/********************************************
						// script inicial que vaig fer servir per posar tots els resultats a la taula letnd.export__habitatsoft
						
						$query = "INSERT INTO letnd.export__habitatsoft
						(`habitatsoft_table`, `habitatsoft_field`) 
						VALUES 
						('".$this->habitatsoft_table."', '". str_replace('\\"', '"', $field) ."');";
						Db::execute($query);
						**********************************************/	
					}
					Db::reconnect();
					//debug::p($column_names);
				}
				// dades, les agrupo per letnd_table
				else{
					// inicialitzo variables per cada registre
					$this->p('Registre ' . $conta, 2);
					$row_habitatsoft = array();
					$this->habitatsoft_id = 0;
					$this->letnd_id = 0;
					
					foreach ($data as $key=>$field){
						// si tinc definit letnd_table pèr aquest camp el guardo, sino passo
						// un camp pot tindre una funcio definida, com ara get_boolean que canvia -1 per 1
						
						if (isset($keys[$key])){
							$k = $keys[$key];
							if ($column_names[$k]['function']) 
								$field = call_user_func (array(&$this, $column_names[$k]['function']),$field);
								
							$row_habitatsoft[$column_names[$k]['letnd_table']][$k]=$field;
						}
					}

					// En acabar de mirar totes les dades del registre faig els inserts que calguin per aquest registre
					// Aquí es crida una funció per tot el grup de camps que van a una taula mateixa taula de letnd, ej. tots els camps que van a inmo__property criden la funcio inmo__property
					foreach($row_habitatsoft as $letnd_table=>$row){
						call_user_func_array (array(&$this, $letnd_table),array(&$row,&$column_names));
					}
					//debug::p($row_habitatsoft, 'Rows_arr');
				}
				$conta++;
			} // END WHILE						
		}
	}
	// giro el row per poder insertar directament a bbdd
	function get_letnd_row(&$row, &$column_names){
		$new_row = array();
		foreach ($row as $key=>$value){
			$new_row[$column_names[$key]['letnd_field']] = $value;
		}
		return $new_row;
	}
	// inmo__property -> get_tipus
	function get_tipus($val){
		// si no està dins l'array, retorno per defecte el primer
		return isset($this->habitat_tipus[$val])?$this->habitat_tipus[$val]:$this->habitat_tipus[3];
	}
	// inmo__property -> get_tipus
	function get_tipus2($val){
		// si no està dins l'array, retorno per defecte el primer
		return isset($this->habitat_tipus2[$val])?$this->habitat_tipus2[$val]:$this->habitat_tipus2[0];
	}	
	// inmo__property -> get_boolean
	function get_boolean($val){
		return -(int)$val;
	}
	// dates
	function format_datetime($val){
		// si no està dins l'array, retorno per defecte el primer
		$date =  format_datetime($val) . ':00';
		$date = str_replace("/", "-", $date);
		return $date;
	}
	function clean_habitat_id($field){
		if (strpos($field,'.')!==false){
			return substr($field,0,strpos($field,'.'));
		}
		else{
			return $field;
		}
	}
	function inmo__property(&$row, &$column_names){
		$this->p('TAULA inmo__property ', 3);
		
		$this->habitatsoft_id =	$row['IdPiso'] = $this->clean_habitat_id($row['IdPiso']);
		
		$this->habitatsoft_expediente = $row['Expediente'] = (int)$row['Expediente'];
		
		$new_row = $this->get_letnd_row ($row, $column_names);
		// afegeixo camps nomès de letnd
		$new_row['status'] = 'onsale';
		$new_row['land_units'] = 'm2';
		
		$new_row['category_id'] = $this->clean_habitat_id($new_row['category_id']);
		if (isset($this->habitat_categorys[$new_row['category_id']])) $new_row['category_id'] = $this->habitat_categorys[$new_row['category_id']];
		
		// sumo habitacions
		$new_row['room'] = $new_row['room1']+$new_row['room2']+$new_row['room3'];
		unset($new_row['room1']); 
		unset($new_row['room2']); 
		unset($new_row['room3']); 
		
		// El primer cop que faig una exportacio, actualitza les propietats amb la 'ref' = 'Expediente', per mantindre les propietats que ja eren a la web ( si s'ha tingut en compte de posar les mateixes referencies
		$query = "SELECT property_id FROM inmo__property 
					WHERE habitatsoft_id='' AND ref = " . $this->habitatsoft_expediente; 
		$existing_id = Db::get_first($query);
		
		// Tots els altres cops actualitza segons habitat_soft_id; si hi ha habitat_soft_id, vol dir que ja tenim establerta una relació directa d'habitatsoft a Letnd
		if (!$existing_id){
		$query = "SELECT property_id FROM inmo__property WHERE habitatsoft_id='".$this->habitatsoft_id."'";
		$existing_id = Db::get_first($query);
		}
		
		// ACTUALITZO O INSERTO
		// si existeix la ref, deixo el property_id actual de letnd per poder utilitzar els mateixos links
		if ($existing_id){
			$query = $this->get_update($new_row, 'inmo__property', 'property_id = ' . $existing_id);	
		}
		// si es un registre nou poso com a property_id el Expediente de habitatsoft ja que es un nombre únic 
		else{
			$existing_id = $this->habitatsoft_expediente;
			$query = $this->get_insert($new_row, 'inmo__property');			
		}
		
		Db::execute($query );		
		$this->p($query, 4);
		
		$this->letnd_id = $existing_id;
		if ($this->letnd_id) $this->propertys_added[] = $this->letnd_id; // compte global de les ids creades o updatejades
	}
	
	function inmo__property_image(&$row, &$column_names){
		$this->p('TAULA inmo__property_image ', 3);		
		$new_row = array();
		
		foreach ($row as $key=>$r){
		
			$habitat_fld = $key;
			$letnd_fld = $column_names[$key]['letnd_field'];
			
			// Trobo el numero de foto ( Foto1, FechaImagen1, ComentarioImagen1 .... )
			$habitat_fld = str_replace('Foto', '', $habitat_fld);
			$habitat_fld = str_replace('FechaImagen', '', $habitat_fld);
			$image_number = str_replace('ComentarioImagen', '', $habitat_fld);
			
			// afegeixo camps nomès de letnd
			$new_row[$image_number]['property_id'] = $this->letnd_id;
			$new_row[$image_number]['ordre'] = $image_number;
			$new_row[$image_number]['main_image'] = $image_number==1?1:0;
			$new_row[$image_number][$letnd_fld] = $r;
		}	
		
		/****** BORRAVA TOT EL REGISTRE I TOTES LES IMATGES SEMPRE
		// per nom imatge no em puc basar per saber si ja tinc o no la imatge, per tant el més fàcil és borrar i tornar a insertar
		// si la data es fiable, llavors si que nomès caldrà actualitzar les noves
		$query = "SELECT image_id, name FROM inmo__property_image WHERE property_id = " . $this->letnd_id;
		$this->images_delete += Db::get_rows($query);
		
		$query = "DELETE FROM inmo__property_image WHERE property_id = " . $this->letnd_id;
		Db::execute($query);
		$this->p($query, 4);	
		$this->p('&nbsp;', 4);	
		******************************************/
		
		// inserto imatges	
		$final_ids = array();	
		foreach ($new_row as $r){
			// habitatsoft envia a la taula tots els camps de 20 imatges, sino te nom no hi ha imatge
			if ($r['name']){
				// comprobo si la imatge ja es a la bbdd
				$letnd_rs = Db::get_row("SELECT image_id, name, entered 
										FROM inmo__property_image 
										WHERE name = '" . $r['name'] . "'");
				
				// si la imatge es a la bbdd i la data es la mateixa no actualitzo
				if ($letnd_rs && strtotime($letnd_rs['entered'])==strtotime($r['entered'])){
					$this->p('La Imatge és la mateixa' , 4);
					$final_ids []= $letnd_rs['image_id'];
				}
				// si ha canviat la data, borro imatges i torno a copiar, i actualitzo camp
				elseif ($letnd_rs){
					$this->images_delete += $letnd_rs;
					$query = $this->get_update($r, 'inmo__property_image', 'image_id = ' . $letnd_rs['image_id']);
					Db::execute($query );
					$this->p($query, 4);	
					$this->images_added[]= array('image_id' => $letnd_rs['image_id'], 'name' => $r['name']);
					$final_ids []= $letnd_rs['image_id'];
				}
				// si no he trobat registre, en creo un de nou
				else {
					$query = $this->get_insert($r, 'inmo__property_image');
					Db::execute($query );
					$insert_id = Db::insert_id();
					// entro captions buits
					foreach ($this->habitat_languages as $key=>$lang){
						Db::execute("
							INSERT INTO `inmo__property_image_language` (`image_id`, `language`) 
							VALUES (".$insert_id.", '".$lang."');");
					}
					
					
					$this->p($query, 4);	
					$this->images_added[]= array('image_id' => $insert_id, 'name' => $r['name']);
					$final_ids []= $insert_id;
				}
			}
		}
		// Borro totes les imatges que sobren de la BBDD, les que s0han borrat a habitatsoft que son les que no porten nom
		
		if ($final_ids){
			$letnd_results = Db::get_rows("SELECT image_id, name, entered 
											FROM inmo__property_image 
											WHERE property_id = " . $this->letnd_id . "
											AND image_id NOT IN (" . implode($final_ids,',') . ")");
			if ($letnd_results) $this->images_delete = array_merge($this->images_delete,$letnd_results);	
			
			$query = "DELETE
											FROM inmo__property_image 
											WHERE property_id = " . $this->letnd_id . "
											AND image_id NOT IN (" . implode($final_ids,',') . ")";
			Db::execute($query);
			$this->p($query, 4);
		}
										
	}
	function inmo__category_language(&$row, &$column_names){
		$this->p('TAULA inmo__category_language ', 3);		
		
		$new_row = array();
		$row_assoc = array();		
		
		// ho poso en array associativa, em serveix pel lang
		foreach ($row as $key=>$r){	
			$row_assoc[$key]= $row[$key];				
		}
		$row_assoc['IdTipoInmueble'] = $this->clean_habitat_id($row_assoc['IdTipoInmueble']);
		if (isset($this->habitat_categorys[$row_assoc['IdTipoInmueble']])) $row_assoc['IdTipoInmueble'] = $this->habitat_categorys[$row_assoc['IdTipoInmueble']];
		
		// primer inserto categoria
			$query = "INSERT INTO inmo__category (category_id) VALUES ('".$row_assoc['IdTipoInmueble']."')";
			Db::execute($query );
			$this->p($query, 4);
			
		// poso be idiomes
		foreach ($this->habitat_languages as $key=>$lang){
			$new_row[$lang]['category_id'] = $row_assoc['IdTipoInmueble'];
			$new_row[$lang]['language'] = $lang;
			$new_row[$lang]['category'] = $row_assoc['NombreTipoInmueble' . $key];
		}
		// inserto		
		foreach ($new_row as $r){
			$query = $this->get_insert($r, 'inmo__category_language');
			Db::execute($query );
			$this->p($query, 4);
		}	
	
	}
	/*
	function inmo__provincia(&$row, &$column_names){
		$this->common_insert_all(&$row, $column_names, 'inmo__provincia');
	}
	function inmo__municipi(&$row, &$column_names){
		$this->common_insert_all(&$row, $column_names, 'inmo__municipi');
	}
	*/
	function inmo__zone(&$row, &$column_names){
		$this->common_insert_all($row, $column_names, 'inmo__zone');
	}
	function inmo__property_language(&$row, &$column_names){
		$language = $this->habitat_languages[substr($this->habitatsoft_table,-2)];
		
		// si hi ha una descripcio portales, substitueixo la que fa habitatsoft automatica per la entrada manualment
		if ($row['DescripcionPortales']) $row['Descripcion'] = $row['DescripcionPortales'];
		unset($row['DescripcionPortales']);
		
		$this->common_insert_all($row, $column_names, 'inmo__property_language', $language);
	}
	// funcio comú per registres que primer es borra tot i desprès s'inserta tot sense complicacions d'idiomes ni res
	function common_insert_all(&$row, &$column_names, $table, $language = false){
		$this->p('TAULA ' . $table, 3);
		$new_row = $this->get_letnd_row($row, $column_names);
		
		if ($language) $new_row['language']=$language; // per inmo__property_language
		
		if (isset($new_row['property_id'])) 
			$new_row['property_id'] = $this->clean_habitat_id($new_row['property_id']);
		
		$query = $this->get_insert($new_row, $table);	
		Db::execute($query );				
		$this->p($query, 4);
	}
	function delete_files(){
		$this->p('_____________________________________________________________________________', 1);
		$ids = implode(',', $this->propertys_added);
		if (!$ids){$this->p('No s\'ha insertat cap registre', 1);return;} // no hauria de pasar mai, però per si acas
		
		$this->p('Borrant arxius de propietats sobrants', 1);	
		$query = "SELECT file_id as id, name FROM inmo__property_file WHERE property_id NOT IN (" . $ids . ")";
		$rs = Db::get_rows($query);
		$this->p($query, 3);
		
		foreach ($rs as $r){
			$file = $r['id'] . strrchr(substr($r['id'], -5, 5), '.');			
			$this->p('Borrat ' . $file, 4);
		}
		
		$this->p('Borrant videos de propietats sobrants', 1);	
		$query = "SELECT video_id as id, name FROM inmo__property_video WHERE property_id NOT IN (" . $ids . ")";
		$rs = Db::get_rows($query);
		$this->p($query, 3);
		
		foreach ($rs as $r){
			$file = $r['id'] . strrchr(substr($r['id'], -5, 5), '.');			
			$this->p('Borrat ' . $file, 4);
		}
		
		
		
	}
	function change_ids(){		
		$this->p('Canviant ids d\'Habitatsoft a Letnd ', 1);		
		
		$query = "UPDATE inmo__property_language SET property_id=(SELECT property_id FROM inmo__property WHERE habitatsoft_id=inmo__property_language.property_id)";
		Db::execute($query );
		$this->p($query, 3);	
	}
	function change_municipi_ids(){		
		$this->p('Canviant ids comarca i municipis d\'Habitatsoft a Letnd ', 1);		
		//Db::connect_mother();
		
		// Aquest query es el que hauria de ser, però hauria de conectar amb usuari root per tindre acces a les 2 bbdds
		/*
		"UPDATE agenciaportselva.inmo__municipi,letnd.inmo__municipi 
		SET 
			agenciaportselva.inmo__municipi.municipi_id= letnd.inmo__municipi.municipi_id, 
			agenciaportselva.inmo__municipi.comarca_id= letnd.inmo__municipi.comarca_id
		WHERE letnd.inmo__municipi.habitatsoft_id = agenciaportselva.inmo__municipi.municipi_id;"
		*/
		
		
		//Db::reconnect();
		
		$query = "UPDATE inmo__property, inmo__municipi 
					SET inmo__property.municipi_id = inmo__municipi.municipi_id
					WHERE inmo__property.municipi_id = inmo__municipi.habitatsoft_id";
		Db::execute($query );		
		$this->p($query, 3);	
		
		$query = "UPDATE inmo__zone, inmo__municipi 
					SET inmo__zone.municipi_id = inmo__municipi.municipi_id
					WHERE inmo__zone.municipi_id = inmo__municipi.habitatsoft_id";
		Db::execute($query );		
		$this->p($query, 3);	
	}
	function delete_propertys(){
		$ids = implode(',', $this->propertys_added);
		if (!$ids){$this->p('No s\'ha insertat cap registre', 1);return;} // no hauria de pasar mai, però per si acas
		
		$this->p('Borrant propietats sobrants', 1);			
		// Afegeixo a l'array les imatges de les propietats que s'han de borrar
		// si la data es fiable, llavors si que nomès caldrà actualitzar les noves
		$query = "SELECT image_id, name FROM inmo__property_image WHERE property_id NOT IN (" . $ids . ")";
		$this->images_delete += Db::get_rows($query);
		
		$query = "DELETE FROM inmo__property WHERE property_id NOT IN (" . $ids . ")";
		Db::execute($query );
		$this->p($query, 3);
		
		$query = "DELETE FROM booking__booking WHERE property_id NOT IN (" . $ids . ")";
		Db::execute($query );
		$this->p($query, 3);
		
		$query = "DELETE FROM custumer__custumer_to_property WHERE property_id NOT IN (" . $ids . ")";
		Db::execute($query );
		$this->p($query, 3);
		
		$query = "DELETE FROM inmo__property_file WHERE property_id NOT IN (" . $ids . ")";
		Db::execute($query );
		$this->p($query, 3);
		
		$query = "DELETE FROM inmo__property_image WHERE property_id NOT IN (" . $ids . ")";
		Db::execute($query );
		$this->p($query, 3);
		
		$query = "DELETE FROM inmo__property_language WHERE property_id NOT IN (" . $ids . ")";
		Db::execute($query );
		$this->p($query, 3);
		
		$query = "DELETE FROM inmo__property_video WHERE property_id NOT IN (" . $ids . ")";
		Db::execute($query );
		$this->p($query, 3);
		
	}
	function delete_images(){
		$this->p('Borrant imatges anteriors', 1);
		foreach ($this->images_delete as $image){
			$rs = ImageManager::get_file_name($image['image_id'], $image['name'],'inmo','property');
			
			$this->p('Borrant imatges id: ' . $image['image_id'], 2);
			
			$this->p($rs['image_src'], 3);
			unlink(DOCUMENT_ROOT . $rs['image_src']);
			
			$this->p($rs['image_src_medium'], 3);
			unlink(DOCUMENT_ROOT . $rs['image_src_medium']);
			
            $this->p($rs['image_src_details'], 3);
			unlink(DOCUMENT_ROOT . $rs['image_src_details']);
			
            $this->p($rs['image_src_thumb'], 3);
			unlink(DOCUMENT_ROOT . $rs['image_src_thumb']);
			
            $this->p($rs['image_src_admin_thumb'], 3);
			unlink(DOCUMENT_ROOT . $rs['image_src_admin_thumb']);
		}
		// falta borrar imatges al disc dur
	}
	function move_images(){
		$this->p('Movent i Redimensionant imatges', 1);
		
		define('IMAGES_DIR', CLIENT_PATH . 'inmo/property/images/'); // la defineixo per que es defineix a upload_files
			
		foreach ($this->images_added as $image){
			$rs = ImageManager::get_file_name($image['image_id'], $image['name'],'inmo','property');
			
			// renombro imatge principal
			$ext = strrchr(substr($image['name'], -5, 5), '.');
			
			$this->p('Moure imatge principal:', 2);
			$this->p('/habitatsoft/' . $image['name'] . '->' . $rs['image_src'], 3);
			
			// Haig de conseguir borrar la imatge original
			//$this->chmod_ftp_files();
			/*rename(CLIENT_PATH . '/habitatsoft/' . $image['name'],
					DOCUMENT_ROOT . $rs['image_src']);*/
			copy(CLIENT_PATH . '/habitatsoft/' . $image['name'],
					DOCUMENT_ROOT . $rs['image_src']);
			// faig imatges thumbnails
			$cg = $this->get_config('admin','inmo__configadmin');
			unset($cg['watermark']); // de moment res de watermarks
			
			
			ImageManager::resize_images($image['image_id'], $ext, $cg);
			
			$this->p('Creant imatges id: ' . $image['image_id'], 2);
			$this->p($rs['image_src'], 3);
			$this->p($rs['image_src_medium'], 3);
            $this->p($rs['image_src_details'], 3);
            $this->p($rs['image_src_thumb'], 3);
            $this->p($rs['image_src_admin_thumb'], 3);
		}
	}
	
	function get_insert(&$row, $table){
		$fields = '';
		$values = '';	
		
		foreach ($row as $key=>$r){
			$fields .= $key . ',';
			$values .= $this->get_str($r) . ',';
		}
		$fields = substr($fields,0,-1);
		$values = substr($values,0,-1);
		
		// si es un inmoble nou poso com a property_id el Expediente de habitatsoft ja que es un nombre únic 
		if ($table=='inmo__property'){		
			$query = "INSERT INTO ".$table." 
						(property_id,".$fields.") 
						VALUES (".$this->habitatsoft_expediente.",".$values.")";
		}
		else
		{
			$query = "INSERT INTO ".$table." 
						(".$fields.") 
						VALUES (".$values.")";
		}
		return $query;
	}
	
	function get_update(&$row, $table, $condition){
		$values = '';	
		
		foreach ($row as $key=>$r){
			$values .= $key . '=' . $this->get_str($r) . ',';
		}
		$values = substr($values,0,-1);
		
		$query = "UPDATE ".$table." 
			SET ".$values."
			WHERE " .
			$condition;
		return $query;
	}
	function get_str($str){
		return "'" . mysqli_real_escape_string( Db::cn(), $str ) . "'";
	}	
	function p ($txt, $level){
		$this->conta++;
		if ($level==2) $txt =  $this->conta . ' ' . $txt . ' - <i>(' . $execution_time = Debug::get_execution_time() . ' s.) </i>';
		echo '<p class="m'.$level.'">'.$txt.'</p>';
		//@ob_flush();
		flush();
	}
	function chmod_ftp_files(){
		
		// no funciona la conexio ftp
		// haig de poder canviar els permisos per borrar les imatges que s'han pujat
		
		$ftpUserName = 'apselva';
		$ftpUserPass = 'PderSeEeeR';
		$ftpServer = '81.25.126.213';

		  $ftpConn = ftp_connect($ftpServer);
		 
		  if (!$ftpConn) {
			die("Unable to connect to $ftpServer");
		  }

		if (@ftp_login($conn_id, $ftpUserName, $ftpUserPass)) {
		   echo "Connected as $ftpUserName @ $ftpServer";
		}
		else {
		   echo "Couldn't connect as $ftpUserName";
		   ftp_close($ftpConn);
		   die("Closed connection to $ftpServer");
		}

		//Now change permissions to 666 or whatever you need

		echo ftp_chmod($ftpConn, 0755, $ftpFilename) ? "CHMOD successful!" : 'Error';


		// Close the connection
		ftp_close($conn_id);
	}
}
?>