
<table class="form addresses">
	<tr>
		<? if ($results):?>
		<th>
			<input name="delivery_id[<?=$customer_id?>]" type="radio" id="delivery_0" value="0" <?=$same_address?> />
		</th>
		<? endif //has_results?>
		<td class="label">
			<label for="delivery_0"><?=$c_same_address?></label>
		</td>
	</tr>

<? foreach($loop as $l): extract ($l)?>
	<tr>
		<th>
			<input name="delivery_id[<?=$customer_id?>]" type="radio" id="delivery_<?=$address_id?>" value="<?=$address_id?>" <?=$checked?> />
		</th>
		<td class="label">
			<label for="delivery_<?= $address_id ?>">
				<a class="lnk-right" href="/<?= LANGUAGE ?>/product/address/show_form_edit/address_id/<?= $address_id ?>/?template=clean" data-iframe="true"><?= $c_edit_address ?></a>
				<?= $a_name ?> <?= $a_surname ?><br>
				<?= $address ?><br>
				<?= $city ?> <?= $zip ?><br>
				<? if ( $provincia_id ): ?>
					<?= $provincia_id ?><br>
				<? endif //$ ?>
				<?= strtoupper( $country_id ) ?>
			</label>
		</td>
	</tr>
<? endforeach //$loop?>
</table>