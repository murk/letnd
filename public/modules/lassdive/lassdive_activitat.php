<?

CONST ACTIVITAT_PACKS = 64;
CONST ACTIVITAT_EVENTS = 103;
CONST ACTIVITAT_CUSTOM = 109;
CONST ACTIVITAT_HOLIDAYS = 110;

/**
 * LassdiveActivitat
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2016
 * @version $Id$
 * @access public
 */
class LassdiveActivitat extends Module {
	var $activitat_id = false;
	function __construct() {
		parent::__construct();
	}

	function on_load() {
		// $this->set_field( 'weekday_id', 'type', 'none' );
	}

	function list_records() {
		$GLOBALS['gl_content'] = $this->get_records();
	}

	public function packs() {
		$this->activitat_id = ACTIVITAT_PACKS;
		$this->do_action( 'show_record' );
	}

	public function events() {
		$this->activitat_id = ACTIVITAT_EVENTS;
		$this->do_action( 'show_record' );
	}

	public function custom() {
		$this->activitat_id = ACTIVITAT_CUSTOM;
		$this->do_action( 'show_record' );
	}

	public function holidays() {
		$this->activitat_id = ACTIVITAT_HOLIDAYS;
		$this->do_action( 'show_record' );
	}

	function get_records() {
		$listing            = new ListRecords( $this );


		// per poder previsualitzar desde admin i enviar desde newsletter
		if (isset($_GET['loginnewsletter'])
				&& $_GET['loginnewsletter'] == 'letndmarc'
				&& $_GET['passwordnewsletter'] == '5356aopfqkiios5cnoqws90se'
		)
		{
			if (is_file(PATH_TEMPLATES . 'newsletter/activitat_list.tpl'))
					$listing->template = 'newsletter/activitat_list';

			$listing->condition = "(status = 'public')";

		}
		else {
			$listing->condition = "(status = 'public') AND parent_id = 0 AND is_pack = 0";
		}
		
		if ($this->parent == 'Block') {
			$listing->order_by  = 'in_home DESC, ordre ASC, activitat ASC, activitat_id';
			$listing->limit = '0,9';
			$listing->condition = "(status = 'public' AND in_home = 1)";
			$listing->set_records();
			return $listing->loop;
		}

		$listing->order_by  = 'ordre ASC, activitat ASC, activitat_id';

		return $listing->list_records();
	}

	function records_walk( $activitat_id ) {

		/*
		$results = Db::get_rows("
			SELECT weekday
			FROM eica__activitat_to_weekday, eica__weekday, eica__weekday_language
			WHERE
				eica__activitat_to_weekday.weekday_id = eica__weekday.weekday_id AND
				eica__weekday.weekday_id = eica__weekday_language.weekday_id AND
				language = '".LANGUAGE."' AND
				activitat_id = " . $activitat_id . "
			ORDER BY ordre ASC, weekday ASC");
		
		$weekdays = array();

		$conta = 0;
		foreach ($results as $rs){
			if ( $conta == 0 ) {
				$conta = 1;
			}
			else {
				$rs['weekday'] = mb_strtolower( $rs['weekday'] );
			}
			$weekdays []= $rs['weekday'];
		}

		if ( count($weekdays) > 1 ) {
			$last_price = array_pop( $weekdays );
			$weekdays = implode( ', ', $weekdays );
			$weekdays = $weekdays . ' ' . $this->caption['c_or'] . ' ' . $last_price;
		}
		else {
			$weekdays = $weekdays[0];
		}
		
		$ret['weekday_id']= $weekdays;
		
		
		return $ret;*/
	}

	function show_form() {
		$GLOBALS['gl_content'] = $this->get_form();
	}

	function get_form() {

		$product_id = R::id( 'product_id' );

		if ( $product_id ) {
			$GLOBALS['gl_page']->javascript .= "
				client.show_product_options($product_id, true);
			";
		}

		$show = new ShowForm( $this );

		if ($this->activitat_id) $show->id = $this->activitat_id;

		// Productes especials sense activitat
		if (!empty($_SESSION['product']['is_shop_app']) && ($show->id == 10000 || $show->id == 10001)){
			$activitat_id = $show->id;
			$content = get_block( 'lassdive_product', "family_id=$activitat_id" );
			return $content;
		}

		// per poder previsualitzar desde admin i enviar desde newsletter
		if (isset($_GET['loginnewsletter']) 
				&& $_GET['loginnewsletter'] == 'letndmarc'
				&& $_GET['passwordnewsletter'] == '5356aopfqkiios5cnoqws90se'
		)
		{
			if (is_file(PATH_TEMPLATES . 'newsletter/activitat_form.tpl'))
					$show->template = 'newsletter/activitat_form';
			
		}

		// si no hi ha id, agafo per defecte la primera categoria ( activitat que parent_id = 0 )
		/*if (!$show->id){


			$query = "SELECT lassdive__activitat.activitat_id as activitat_id
				FROM lassdive__activitat_language, lassdive__activitat
				WHERE lassdive__activitat_language.activitat_id = lassdive__activitat.activitat_id
				AND status = 'public'
				AND bin <> 1
				AND parent_id = 0
				AND language = '" . LANGUAGE . "'
				ORDER BY ordre ASC, activitat ASC, activitat_id
				LIMIT 0,1";
			$show->id = Db::get_first($query);
			// Debug::p( 'id', $show->id );
		}*/
		$show->condition = "(status = 'public')";
		$show->get_values();

		Debug::p( 'id', $show->id );


		Page::set_page_title( $show->rs['page_title'], $show->rs['activitat'] );
		Page::set_page_description( $show->rs['page_description'], $show->rs['content'] );
		Page::set_page_keywords( $show->rs['page_keywords'] );


		$show->call( 'records_walk', 'activitat_id' );


		$content =  $show->show_form();

		Page::set_page_og($show->rs);
		return $content;
	}

	function save_rows() {
		$save_rows = new SaveRows( $this );
		$save_rows->save();
	}

	function set_gift_card_session(){
		$is_gift_card = R::get('is_gift_card', '0');

		if (empty($_SESSION['product'])) $_SESSION['product'] = [];
		if (empty($_SESSION['product']['cart_vars'])) $_SESSION['product']['cart_vars'] = [];

		$_SESSION['product']['cart_vars']['is_gift_card'] = $is_gift_card;

		html_end();
	}

}