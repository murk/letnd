<?

/* 12-7-16

	Afegeixo camp a banner
 */

if ( is_table( 'web__banner_language' ) && ! is_field( 'web__banner_language', 'banner_text2' ) ) {

	$update_sql[] = "ALTER TABLE `web__banner_language`
	ADD COLUMN `banner_text2` TEXT NOT NULL AFTER `banner_text`;";

} else {
	if ( is_table( 'web__banner_language' ) ) {
		trigger_error( 'Ja existeixen el camp banner_text2' );
	}
}

if ( is_table( 'web__configadmin_banner' ) && ! Db::get_first( "SELECT * FROM web__configadmin_banner WHERE name='show_banner_text2'" ) ) {
	$update_sql[] = "
		INSERT INTO web__configadmin_banner (`name`, `value`) VALUES ('show_banner_text2', '0');";

} else {
	if ( is_table( 'web__configadmin_banner' ) ) {
		trigger_error( 'Ja existeixen el camp show_banner_text2' );
	}
}

?>