<?php
/**
 * UserUser
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2013
 * @version $Id$
 * @access public
 */
class UserGroup extends Module{

	var $condition = '';
	var $q = false;

	function __construct(){
		parent::__construct();
	}

	// els groyups amb edit_only_itself no poden editar groups
	function has_permission(){
		global $gl_message, $gl_messages;
		$query = "SELECT edit_only_itself FROM user__group WHERE group_id = " .$_SESSION['group_id'];
		if (Db::get_first($query)) {
			$gl_message = $gl_messages['concession_no_acces'];
			return false;
		}
		else{
			return true;
		}
	}

	function get_group_condition()
	{
	// no es veu el propi grup, així no es pot borrar el propi grup
		$group_condition = $_SESSION['group_id'] > 2?3:$_SESSION['group_id'];
		$group_condition = 'group_id >' . $group_condition;
		return $group_condition;
	}



	function save_rows()
	{
		if (!$this->has_permission()) return;
		if ($this->are_user_before_del())
		{
			return;
		}

		$save_rows = new SaveRows($this);
		$save_rows->save();
	}
	
	function list_records()
	{
		$content = $this->get_records();
		$GLOBALS['gl_content'] = $this->get_search_form() . $content;
	}

	function get_records()
	{	
		if (!$this->has_permission()) return;
		$listing = new ListRecords($this);
		if ($_SESSION['group_id']!=1){
			$listing->unset_field('show_html_editor');
			$listing->unset_field('show_seo');
		}
		
		$listing->add_button ('view_users', 'menu_id=7009&tool=user&tool_section=user', 'boto2','before','','',false,false);

		$listing->call( 'records_walk', '', $this );

		$listing->order_by = 'ordre ASC, name ASC';
		$listing->has_bin = false;
		$listing->condition = $this->get_group_condition();	
		
		// search
		if ($this->condition) {
			$listing->condition .= ' AND ' . $this->condition;
		}		

		return $listing->list_records();
	}

	function records_walk( $listing ) {

		$rs =  &$listing->rs;
		$is_editable = $rs['is_editable'];

		$ret['is_row_editable'] = $is_editable;

		return $ret;

	}

	function are_user_before_del()
	{
		global $gl_action, $gl_messages, $gl_message;
		if ($gl_action == 'save_rows_bin_selected')
		{
			// le formulari, passa els valors amb un array
			foreach ($_POST["selected"] as $key => $value)
			{
				if ($value > 2)
				{
					$query = "SELECT user__user.group_id, user__group.name
							FROM user__user, user__group
							WHERE user__user.group_id = user__group.group_id
							AND user__user.group_id = " . $value;
					$results = Db::get_rows($query);

					if ($results)
					{
						// si no te resultats l'elimino del array
						unset ($_POST["selected"] [$key]);
						$gl_message .= "<tr><td>" . sprintf($gl_messages['user_exist_del'], $results[0]['name']) . "</td></tr>";
					}
				}
				// grups d'administrador no es poden borrar, simplement agafem el nom de la BBDD
				else
				{
					$query = "SELECT name
							FROM user__group
							WHERE group_id = " . $value;
					$results = Db::get_rows($query);

					if ($results)
					{
						unset ($_POST["selected"] [$key]);
						$gl_message .= "<tr><td>" . sprintf($gl_messages['user_admin_del'], $results[0]['name']) . "</td></tr>";
					}
				}
			}
			$gl_message = "<table>" . $gl_message . "</table>";
			// si el array esta buit ens el careguem
			if (count($_POST["selected"]) == 0)
			{
				return true;
			}
			else
			{
				return false;
			}
			//print_r ($_POST["selected"]);
		}
	}
	
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}

	function get_form()
	{
		if (!$this->has_permission()) return;
		$show = new ShowForm($this);
		$show->has_bin = false;
		if ($_SESSION['group_id']!=1){
			$show->unset_field('show_html_editor');
			$show->unset_field('show_seo');
		}
		$show->get_values();

		$is_editable = $show->rs['is_editable'];

		if (!$is_editable) $show->set_editable( false );

		return $show->show_form();
	}

	function write_record()
	{
		if (!$this->has_permission()) return;
		$writerec = new SaveRows($this);
		$writerec->save();
	}

	function manage_images()
	{
		if (!$this->has_permission()) return;
		$image_manager = new ImageManager($this);
		$image_manager->execute();
	}


	//
	// FUNCIONS BUSCADOR
	//
	function search(){

		$q = $this->q = R::escape('q');
		
		$search_condition = '';

		if ($q)
		{
			// Sempre busco a tot arreu, abans si detectava que eren refrencies no hi  buscava, posaré checkbox de "buscar nomès referencies"

			$search_condition = "(
				name like '%" . $q . "%'
				" ;

			$search_condition .= ") " ;

			$GLOBALS['gl_page']->title = TITLE_SEARCH . '<strong>&nbsp;&nbsp;"' . $q . '"</strong>';
		}
		$this->condition = $search_condition;
		Debug::add('Search condition', $this->condition);
		$this->do_action('list_records');
	}

	function get_ids_query(&$results, $field)
	{
		if (!$results) return '';
		$new_arr = array();
		foreach ($results as $rs){
			$new_arr [$rs[0]] = $rs[0];
		}
		return "OR " . $field . " IN (" . implode(',', $new_arr) . ") ";
	}

	function get_search_form(){
		$tpl = new phemplate(PATH_TEMPLATES);
		$this->caption['c_by_ref']='';
		$tpl->set_vars($this->caption);
		$tpl->set_file('search.tpl');
		$tpl->set_var('menu_id',$GLOBALS['gl_menu_id']);
		$tpl->set_var('q',  htmlspecialchars(R::get('q')));		
		
		if ($this->q) {
			$GLOBALS['gl_page']->javascript .= "
				$('table.listRecord').highlight('".addslashes(R::get('q'))."');			
			";
		}
		
		return $tpl->process();
	}
}
?>