<?php

class SplitPageResults {
	/*
	  NOTE: the constructor (also) builds an sql query that counts the total records from $sql_query..
	  this value is then saved as $query_num_rows (which does not have to be set before
	  creating an instance of this class.
	  Please note the function references (&$variable) - please read up on this in the PHP documentation.
	 */

	var $query_num_rows, $current_page_number, $num_pages, $page_name, $do_split = true, $use_friendly_url, $parameters, $parameters_friendly;

	/* class constructor */

	function __construct(&$sql, $change_sql_id_field, $list_name, $split) {
		if ($split) {
			$this->do_split = false;
			return;
		}
		$sql_query = $sql['split'];
		global $gl_db_max_results, $PHP_SELF, $gl_variable;
		$this->page_name = 'page' . $list_name;
		$this->current_page_number = R::id($this->page_name);
		if (empty($this->current_page_number))
			$this->current_page_number = 1;

		$offset = ($gl_db_max_results * ($this->current_page_number - 1));
		$sql['q'] .= " LIMIT " . $offset . ", " . $gl_db_max_results;

		$count_field = $change_sql_id_field ? $change_sql_id_field : "*"; // revisar si s'ha de posar millor el gl_id_field

		$count_query = "SELECT count(" . $count_field . ") as total FROM " . $sql_query;
		$results = Db::get_rows_array( $count_query );

		$count_num_rows = count( $results );

		// Amb "group by" el count es comporta diferent, compta el numero de repeticions de cada grup
		$this->query_num_rows =	$count_num_rows == 1? $results[0][0] : $count_num_rows;


		Debug::add('Consulta split count', $count_query);
		Debug::add('Consulta split', $sql['q']);

	}

	function display_links(&$listing, $use_friendly_url) {
		$ret = array();
		$output = $ret['split_previous_link'] = $ret['split_next_link'] = $ret['split_previous_title'] = $ret['split_next_title'] =  $ret['split_previous_set_link'] = $ret['split_next_set_link'] = $ret['split_previous_set_title'] = $ret['split_next_set_title'] = $ret['split_results'] = '';

		$add_nbsp = $listing->config['split_results_add_nbsp'] && !$GLOBALS['gl_is_admin'];

		if (!$this->do_split)
			return $ret;

		global $PHP_SELF, $gl_db_max_results, $gl_db_max_page_links;
		$this->num_pages = $num_pages = ceil($this->query_num_rows / $gl_db_max_results);
		if ($num_pages == 1)
			return $ret;

		$parameters = $this->parameters = get_all_get_params(array($this->page_name), '?', '&');
		$this->parameters_friendly = get_all_get_params(array('language', 'tool', 'tool_section', 'action', 'page', 'page_file_name'), '?');
		$this->use_friendly_url = $use_friendly_url;

		// check if num_pages > $gl_db_max_page_links
		$cur_window_num = ceil($this->current_page_number / $gl_db_max_page_links);
		$max_window_num = ceil($num_pages / $gl_db_max_page_links);

		// (NO HO FAIG, poder un altre dia) FIRST - not displayed on first page
		// if ($this->current_page_number > 1) $output .= $this->get_link('page=1', $listing, DB_SPLIT_BUTTON_FIRST, DB_SPLIT_TITLE_FIRST_PAGE) . '&nbsp;';

		// PREVIOUS SET of pages
		if ($cur_window_num > 1){
			$title = sprintf(DB_SPLIT_TITLE_PREV_SET_OF_NO_PAGE, $gl_db_max_page_links);
			$output .= $this->get_link(($cur_window_num - 1) * $gl_db_max_page_links, $listing, '...', $title, 'pageResultsPreviousSet');

			if ($add_nbsp)
				$output .= '&nbsp;';

			$ret['split_previous_set_title'] = htmlspecialchars( DB_SPLIT_TITLE_PREV_SET_OF_NO_PAGE );
			$ret['split_previous_set_link'] = $this->get_link($this->current_page_number - 1, $listing);
		}
		
		// PREVIOUS - No es mostra a la primera pàgina
		if ($this->current_page_number > 1) {
			$output .= $this->get_link($this->current_page_number - 1, $listing, DB_SPLIT_BUTTON_PREV, DB_SPLIT_TITLE_PREVIOUS_PAGE, 'pageResultsPrevious');
			if ($add_nbsp)
				$output .= '&nbsp;&nbsp;';

			$ret['split_previous_title'] = htmlspecialchars( DB_SPLIT_TITLE_PREVIOUS_PAGE );
			$ret['split_previous_link'] = $this->get_link($this->current_page_number - 1, $listing);
		}

		// PAGE NUMBERS
		for ($jump_to_page = 1 + (($cur_window_num - 1) * $gl_db_max_page_links); ($jump_to_page <= ($cur_window_num * $gl_db_max_page_links)) && ($jump_to_page <= $num_pages); $jump_to_page++) {
			if ($jump_to_page == $this->current_page_number) {
				$output .= '<b class="pageResults">' . $jump_to_page . '</b>';
				if ($add_nbsp)
					$output = '&nbsp;' . $output . '&nbsp;';
			}
			else {
				$output .= $this->get_link($jump_to_page, $listing, $jump_to_page, sprintf(DB_SPLIT_TITLE_PAGE_NO, $jump_to_page));
				if ($add_nbsp)
					$output = '&nbsp;' . $output . '&nbsp;';
			}
		}

		// NEXT
		if (($this->current_page_number < $num_pages) && ($num_pages != 1)) {
			$output .= $this->get_link($this->current_page_number + 1, $listing, DB_SPLIT_BUTTON_NEXT, DB_SPLIT_TITLE_NEXT_PAGE, 'pageResultsNext');
			if ($add_nbsp)
				$output = '&nbsp;&nbsp;' . $output;

			$ret['split_next_title'] = htmlspecialchars(DB_SPLIT_TITLE_NEXT_PAGE);
			$ret['split_next_link'] = $this->get_link($this->current_page_number + 1, $listing);
		}

		// NEXT SET of pages
		if ($cur_window_num < $max_window_num) {
			$title = sprintf(DB_SPLIT_TITLE_NEXT_SET_OF_NO_PAGE, $gl_db_max_page_links);
			$output .= $this->get_link(($cur_window_num) * $gl_db_max_page_links + 1, $listing, '...', $title, 'pageResultsNextSet');

			$ret['split_next_set_title'] = htmlspecialchars( $title );
			$ret['split_next_set_link'] = $this->get_link(($cur_window_num) * $gl_db_max_page_links + 1, $listing);
		}

		// (NO HO FAIG, poder un altre dia) LAST
		// if ($this->current_page_number > 1) $output .= '&nbsp;' . $this->get_link($num_pages, $listing, DB_SPLIT_BUTTON_LAST, DB_SPLIT_TITLE_LAST_PAGE);
		$ret['split_results'] = $output;
		return $ret;
	}

	function get_link($page, &$listing, $name = '', $title = '', $class = '') {

		$class = $class ? 'class="pageResults ' . $class . '"' : 'class="pageResults"';
		
		
		$action = str_replace('get_', 'list_', $listing->action); // get nomes es per cridar de dins un modul a l'altre, mai per url
		//
		// per quan l'accio global es diferent de la del modul; ej. search d'api
		if ($listing->is_index) {
			$action = $GLOBALS['gl_action'];
		}
		
		if ($this->use_friendly_url) {
			
			// accion pèr defecte 'list_records' no surt mai
			if ($action == 'list_records') $action = '';
			
			$file_name = isset($listing->fields[$listing->tool_section . '_file_name']) ? '' : false; // per saber si es V2 o V1

			$tool = $listing->parent_tool ? $listing->parent_tool : $listing->tool;
			$tool_section = $listing->parent_tool_section ? $listing->parent_tool_section : $listing->tool_section;

			// forço a noticies v1 ( es temporal, no tinc temps de fer els redirects )
			//if ($tool == 'news' && $tool_section == 'new' && !$file_name)
				//$file_name = false;

			$category_file_name = R::file_name('filecat_file_name');

			$params = array();
			$params[] = 'action';
//			if ($file_name !== false) {
//				$action = false;
//			}
			
			// a page guardo el parametre de view per que alcanviar de pagina em quedi a la mateixa vista
			$listing->friendly_params['view'] = R::text_id('view');
			$listing->friendly_params['orderby'] = R::text_id('orderby');

			// EVITAR DUPLICATS
			if ($page == 1) {
				//$link = '/' . LANGUAGE . '/' . $listing->base_link_friendly . '/';
				$listing->friendly_params[$this->page_name] = false;
				$link = Page::get_link($tool, $tool_section, $action, false, $listing->friendly_params, $params, $file_name, $category_file_name, $listing->page_file_name);
			} else {
				//$link = '/' . LANGUAGE . '/' . $listing->base_link_friendly . '/' . $action . '/'. $this->page_name .'/' . $page . '/';	
				$listing->friendly_params[$this->page_name] = $page;
				//Debug::p($listing->friendly_params, 'frindly params');
				$link = Page::get_link($listing->parent_tool ? $listing->parent_tool : $listing->tool, $listing->parent_tool_section ? $listing->parent_tool_section : $listing->tool_section, $action, false, $listing->friendly_params, $params, $file_name, $category_file_name, $listing->page_file_name);

			}
			//if (isset(GET['page_file_name'])) $link .=  GET['page_file_name'] . '.html';
			//if ($this->parameters_friendly) $link .= $this->parameters_friendly;
		} else {
			// EVITAR DUPLICATS
			if ($page == 1) {
				$link = substr($this->parameters, 0, -5);
			} else {
				$link = $this->parameters . $this->page_name . '=' . $page;
			}
		}

		if ($name) {
			$link = '<a href="' . $link . '" ' . $class . ' title=" ' . $title . ' ">' . $name . '</a>';
		}
		return $link;
	}

	function display_count(&$listing) {
		if (!$this->do_split) {
			//return '';
			$this->query_num_rows = $listing->loop_count;
			$ret['split_results_count'] = sprintf(DB_SPLIT_DISPLAY_NUMBER_OF_RESULTS, ($this->query_num_rows ? 1 : 0), $this->query_num_rows, $this->query_num_rows);
			$ret['split_results_total_count'] = $this->query_num_rows;

			return $ret;
		}

		global $gl_db_max_results;
		$to_num = ($gl_db_max_results * $this->current_page_number);
		if ($to_num > $this->query_num_rows)
			$to_num = $this->query_num_rows;
		$from_num = ($gl_db_max_results * ($this->current_page_number - 1));
		if ($to_num == 0) {
			$from_num = 0;
		} else {
			$from_num++;
		}
		// EVITAR DUPLICATS
		if ($this->current_page_number != 1 && !$GLOBALS['gl_is_admin']) {
			$GLOBALS['gl_page']->page_title = $GLOBALS['gl_page']->page_title . ' - ' . DB_SPLIT_PAGE_TITLE . ' ' . $this->current_page_number;
			$GLOBALS['gl_page']->page_description = $GLOBALS['gl_page']->page_description . ' - ' . sprintf(DB_SPLIT_PAGE_DESCRIPTION, $from_num, $to_num, $this->query_num_rows);
		}
		$results_text = isset($listing->caption['c_split_results_count']) ? $listing->caption['c_split_results_count'] : DB_SPLIT_DISPLAY_NUMBER_OF_RESULTS;
		$ret['split_results_count'] = sprintf($results_text, $from_num, $to_num, $this->query_num_rows);
		$ret['split_from_product'] = $from_num;
		$ret['split_to_product'] = $to_num;
		$ret['split_max_results'] = $gl_db_max_results;
		$ret['split_page'] = $this->current_page_number;
		$ret['split_page_count'] = $this->num_pages;
		$ret['split_results_total_count'] = $this->query_num_rows;

		return $ret;
	}

}

?>