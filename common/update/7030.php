<?

/* 24-8-17
 Afegir opció a newsletter de enviar un màxim de missatges


 */

if ( is_table( 'newsletter__configadmin' ) && ! Db::get_first( "SELECT * FROM newsletter__configadmin WHERE name='max_sends_per_message'" ) ) {
	$update_sql[] = "
		INSERT INTO newsletter__configadmin (`name`, `value`) VALUES ('max_sends_per_message', '1500');";

} else {
	if ( is_table( 'newsletter__configadmin' ) ) {
		trigger_error( 'Ja existeixen el camp max_sends_per_message' );
	}
}


?>