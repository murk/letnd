<?php
// menus eina
if (!defined('PORTAL_MENU_PORTAL')){
define('PORTAL_MENU_CUSTOMER','Clientes');
define('PORTAL_MENU_NEW','Nuevo cliente');
define('PORTAL_MENU_LIST','Listar clientes');
define('PORTAL_MENU_LIST_BIN','Papelera');
define('PORTAL_MENU_FAMILY','Famílias');
define('PORTAL_MENU_FAMILY_NEW','Crear família');
define('PORTAL_MENU_FAMILY_LIST','Listar famílas');
define('PORTAL_MENU_FAMILY_LIST_BIN','Papelera');
define('PORTAL_MENU_CATEGORY','Categorias');
define('PORTAL_MENU_CATEGORY_NEW','Nueva categoría');
define('PORTAL_MENU_CATEGORY_LIST','Listar categorías');
define('PORTAL_MENU_CATEGORY_LIST_BIN','Paperera');
}

$gl_caption_customer['c_customer_private']='Nombre privado';
$gl_caption_customer['c_home']='Destacado';
$gl_caption_customer['c_family_id'] = 'Família';
$gl_caption_customer['c_subfamily_id'] = 'Subfamília';
$gl_caption_customer['c_telephone'] = 'Teléfono';
$gl_caption_customer['c_telephone_mobile'] = 'Teléfono Móbil';
$gl_caption_customer['c_fax'] = 'Fax';
$gl_caption_customer['c_mail'] = 'E-mail';
$gl_caption_customer['c_url'] = 'Url';
$gl_caption_customer['c_adress'] = 'Dirección';
$gl_caption_customer['c_price_id'] = 'Rango de precio';
$gl_caption_customer['c_customer_title'] = 'Nombre del Cliente';
$gl_caption_customer['c_customer_description'] = 'Descripción';
$gl_caption_customer['c_data']='Datos del cliente';
$gl_caption_customer['c_general_data']='Datos generales';
$gl_caption_customer['c_descriptions']='Descripciones';
$gl_caption_customer['c_description']='Descripción';
$gl_caption_customer['c_status'] = 'Estado';
$gl_caption_customer['c_prepare'] = 'En preparación';
$gl_caption_customer['c_review'] = 'Para revisar';
$gl_caption_customer['c_onsale'] = 'Para vender';
$gl_caption_customer['c_archived'] = 'Arxivado';
$gl_caption_customer['c_owner_name'] = 'Nombre del gerente';
$gl_caption_customer['c_owner_surnames'] = 'Apellidos del gerente';
$gl_caption_customer['c_owner_phone'] = 'Teléfono del gerente';
$gl_caption_customer['c_file']='Archivos';
$gl_caption_customer['c_edit_map'] = 'Mapa';
$gl_caption_customer['c_edit_data'] = 'Editar Cliente';
$gl_caption_customer['c_save_map'] = 'Guardar ubicación';
$gl_caption_customer['c_click_map'] = 'Haz un clic en un punto del mapa para establecer la situación';
$gl_caption_customer['c_form_map_title'] = 'Ubicación del cliente';
$gl_caption_customer['c_country_id']='País';
$gl_caption_customer['c_municipi_id']='Municipio';
$gl_caption_customer['c_provincia_id']='Provincia';
$gl_caption_customer['c_comarca_id']='Comarca';
$gl_caption_customer['c_schedule']='Horarios';


$gl_caption_customer['c_data_customer']='Datos públicos del cliente';
$gl_caption_customer['c_search_adress']='Buscar dirección';
$gl_caption_customer['c_latitude']='latitud';
$gl_caption_customer['c_longitude']='longitud';
$gl_caption_customer['c_found_direction']='dirección encontrada';

$gl_messages['point_saved'] = 'Nova ubicació guardada correctament';
//Categories
$gl_caption_customer['c_category_id']=$gl_caption_category['c_category_id']='Categoría';
$gl_caption_category['c_category_name']='Nombre de la categoría';


//captions comuns
$gl_caption['c_observations'] = 'Observaciones';
$gl_caption['c_family_description']='Descripción de la família';
$gl_caption['c_ordre'] = 'Orden';
$gl_caption['c_check_parent_id']='Escoge família';
$gl_caption['c_subfamily_id_format']='#%s#%s';

//apartat families
$gl_caption_family['c_parent_id']='Familia a la que pertenece';
$gl_caption_family['c_family']='Nombre de la familia';

?>