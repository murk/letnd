<?php
// config
// El tipus select es per fer el rang de preus amb 2 desplegables
// El tipus links utiltza rangs Ej: De 100 a 200, de 200 a 300, etc... per tant també pot ser un desplegable o un llistat de links
$block_type = isset($vars['block_type'])?$vars['block_type']:'links'; // links - select - range

if ( isset( $vars['tipus'] ) ) {
	$tipus = $vars['tipus'];
} else {
	$tipus = false;
	$vars['tipus'] = '';
}

$menu_price = isset($vars['menu_price'])?$vars['menu_price']:false;
$page_id = isset($vars['page_id']) ? $vars['page_id'] : false;
include_once('inmo_block_functions.php');

$condition = '';
$condition .= inmo_block_get_tipus($tipus);

$vars['var_name'] = 'prices';

$caption = &$GLOBALS['gl_caption'];
if(!isset($caption['c_filter_price_min'])){
	include(DOCUMENT_ROOT . 'public/modules/inmo/languages/' . LANGUAGE . '.php');
	$caption = $gl_caption;
}

if ($menu_price) {
	$prices = explode(',', $menu_price);
}
else{
	if (!defined('INMO_MENU_PRICE'))
		Module::get_config_values('inmo');
	
	$prices = explode(',', INMO_MENU_PRICE);

	if ($tipus && strpos($tipus, 'rent')!==false)
		$prices = explode(',', INMO_MENU_PRICE_RENT);

	if ($tipus && strpos($tipus, 'temp')!==false)
		$prices = explode(',', INMO_MENU_PRICE_RENT);
}

$c_inmo_price_first = isset( $caption['c_inmo_price_first'] ) ?
	$caption['c_inmo_price_first'] :
	INMO_PRICE_FIRST;

$c_inmo_price_middle = isset( $caption['c_inmo_price_middle'] ) ?
	$caption['c_inmo_price_middle'] :
	INMO_PRICE_MIDDLE;

$c_inmo_price_last = isset( $caption['c_inmo_price_last'] ) ?
	$caption['c_inmo_price_last'] :
	INMO_PRICE_LAST;

// Link
$friendly_params = array();
if ($tipus) $friendly_params['tipus'] = $tipus==INMO_DEFAULT_TIPUS?'':$tipus;

if (USE_FRIENDLY_URL) {
	$friendly_params['prices'] = '%s-%s';
}
else{
	$friendly_params['price1'] = '%s';
	$friendly_params['price2'] = '%s';
}

$link = get_link(array(
		'tool' => 'inmo',
		'tool_section' => 'property',
		'friendly_params' => $friendly_params,
		'link_vars' => array('tipus'=>$tipus),
		'detect_page' => $page_id?false:true,
		'page_id' => $page_id
	));
// Link
	

if ($prices) {
	
	if ($tipus == 'temp') {
		$c_filter_price_min = $caption['c_filter_price_min_temp'];
		$c_filter_price_max = $caption['c_filter_price_max_temp'];
	}
	else {
		$c_filter_price_min = $caption['c_filter_price_min'];
		$c_filter_price_max = $caption['c_filter_price_max'];
	}
	
    $conta = 0;
    $previous_price = 0;
    $prices_count = count($prices);
	
	$prices_request = R::text_id('prices');
	$selected_price_1 = '';
	$selected_price_2 =  '';
	if ($prices_request){
		$prices_request = explode('-',$prices_request);
		$selected_price_1 = $prices_request[0];
		$selected_price_2 = $prices_request[1]; // nomes per select
	}
	else {
		$selected_price_1 = R::text_id('price1',0);
		$selected_price_2 =  R::text_id('price2');
	}

	// Si tenim un get del tipus, llavors només s'ha de posar els preus seleccionats si coincideixen els tipus
	$tipus_request = R::text_id('tipus');
	if ( $tipus && $tipus != 'auto' && $tipus_request ) {
		if ( $tipus != $tipus_request ) {
			$selected_price_1 = '';
			$selected_price_2 =  '';
		}
	}

	$vars['block_selected'] = $selected_price_1?1:0;
	
	$select_1 = '';
	$select_2 = '';


	if ($block_type == 'range') {

		$vars['min_price'] = 0;
		$condition = "
				SELECT max(price) 
				FROM inmo__property 
				WHERE bin=0 
					AND (status = 'onsale' OR status = 'sold' OR status = 'reserved')
					AND price_consult = 0";

		if ($tipus)
			$condition .= inmo_block_get_tipus($tipus);

		$vars['max_price'] = Db::get_first($condition);

		// A temp miro també a la taula price_tmp
		if ( $tipus == 'temp' ) {
			$condition = "
				SELECT max(price_tmp) 
				FROM inmo__property 
				WHERE bin=0 
					AND (status = 'onsale' OR status = 'sold' OR status = 'reserved')
					AND price_consult = 0";

			if ( $tipus ) {
				$condition .= inmo_block_get_tipus( $tipus );
			}
			$vars['max_price'] = ceil (max($vars['max_price'],Db::get_first($condition)));
		}

		if (!$vars['max_price']) $vars['max_price'] = 0; // per que no peti si no hi ha immoble

		$vars['selected_price_1'] = $selected_price_1?$selected_price_1:'0';
		$vars['selected_price_2'] = $selected_price_2?$selected_price_2:$vars['max_price'];

		// Quan té els valors per defecte ho poso a 0 així no ho inclou en la cerca ( sino sempre busca preus diferents de 0 i llavors deixa de funcionar per si busquem només referencia
		if ($vars['selected_price_1'] == $vars['min_price'] && $vars['selected_price_2'] == $vars['max_price']){
			$vars['selected_price_hidden'] = '';
		}
		else{
			$vars['selected_price_hidden'] = $vars['selected_price_1'] . '-' . $vars['selected_price_2'];

		}

		$price_dif = $vars['max_price']-$vars['min_price'];

		$vars['price_step'] = 10;
		if ( $price_dif > 10000 ) {
			$vars['price_step'] = 100;
		}
		if ( $price_dif > 50000 ) {
			$vars['price_step'] = 200;
		}
		if ( $price_dif > 100000 ) {
			$vars['price_step'] = 1000;
		}
		if ( $price_dif > 500000 ) {
			$vars['price_step'] = 2000;
		}
	}

	else {
		foreach ( $prices as $price ) {
			if ( $block_type == 'links' ) {
				if ( $conta == 1 ) {
					$l['item'] = sprintf( $c_inmo_price_first, format_int( $price ) );
				}
				if ( $conta > 1 ) {
					$l['item'] = sprintf( $c_inmo_price_middle, format_int( $previous_price ), format_int( $price ) );
				}
				// Em salto el primer
				if ( $conta > 0 ) {

					$l['link'] = sprintf( $link, $previous_price, $price );

					// Si el segon preu coincideix, ja em serveix per rangs i per preus "fins a"
					$l['selected'] = $selected_price_2 === $price ? 1 : 0;

					$l['value'] = $previous_price . '-' . $price;
					$l['class'] = '';

					$l['price_1'] = $previous_price;
					$l['price_2'] = $price;

					$loop2[] = $l;
				}

				$previous_price = $price;
			} elseif ( $block_type == 'select' ) {
				$selected_1 = $selected_price_1 === $price ? ' selected' : '';
				$selected_2 = $selected_price_2 === $price ? ' selected' : '';
				$select_1 .= '<option value="' . $price . '"' . $selected_1 . '>' . format_int( $price ) . ' ' . INMO_CURRENCY_NAME . '</option>';

				if ( $conta > 0 ) {
					$select_2 .= '<option value="' . $price . '"' . $selected_2 . '>' . format_int( $price ) . ' ' . INMO_CURRENCY_NAME . '</option>';
				}
			}

			$conta ++;
		}
	}
	
	if ($block_type == 'select'){	

		$vars['price1'] = '<select name="price1">
							<option value="">' . $c_filter_price_min . '</option>' . $select_1 . 
						'</select>';
		$vars['price2'] = '<select name="price2">
							<option value="">' . $c_filter_price_max . '</option>' . $select_2 . 
						'	<option value="">+ ' . format_int($price) . ' '  . INMO_CURRENCY_NAME . '</option>
						</select>';
	}

	if ($block_type=='links'){
		
		$l['link'] = sprintf($link, $price, '');

		$l['item'] = sprintf($c_inmo_price_last, format_int($price));
		$l['selected'] = $selected_price_1 == $previous_price?1:0;
		$l['value'] =  $previous_price . '-';

		$l['price_1'] = $previous_price;
		$l['price_2'] = '';

		$loop2[] = $l;

		$loop2[0]['class'] = 'first';	
		$loop2[count($loop2)-1]['class'] = 'last';

		$loop[] = array ('loop' => $loop2,
			'item' => '');
	}
}
?>