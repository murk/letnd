<?
/*
23-09-2019

*/


if ( is_table( 'booking__book' ) && ! is_field( 'booking__book', 'is_manual_price' ) ) {

	$update_sql[] = "ALTER TABLE `booking__book`
				ADD COLUMN `is_manual_price` TINYINT(1) NOT NULL DEFAULT 0 AFTER `price_total`;";

}
else {
	if ( is_table( 'booking__book' ) ) {
		trigger_error( 'Ja existeixen el camp is_manual_price' );
	}
}