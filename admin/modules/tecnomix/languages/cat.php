<?php
// menus eina
if (!defined('TECNOMIX_MENU_NEW')){
	define('TECNOMIX_MENU_NEW', 'Notícies');
	define('TECNOMIX_MENU_NEW_NEW', 'Entrar nova notícia');
	define('TECNOMIX_MENU_NEW_LIST', 'Llistar i editar notícies');
	define('TECNOMIX_MENU_NEW_BIN', 'Paperera de reciclatge');
	define('TECNOMIX_MENU_CATEGORY', 'Categories');
	define('TECNOMIX_MENU_CATEGORY_NEW', 'Entrar nova categoria');
	define('TECNOMIX_MENU_CATEGORY_LIST', 'Llistar categories');

	define('TECNOMIX_MENU_INSTALACIO', 'Instalacions');
	define('TECNOMIX_MENU_INSTALACIO_NEW', 'Entrar nova instalació');
	define('TECNOMIX_MENU_INSTALACIO_LIST', 'Llistar i editar instalacions');
	define('TECNOMIX_MENU_INSTALACIO_BIN', 'Paperera de reciclatge');
	define('TECNOMIX_MENU_INSTALACIOCATEGORY', 'Categories d\'instalacions');
	define('TECNOMIX_MENU_INSTALACIOCATEGORY_NEW', 'Entrar nova categoria');
	define('TECNOMIX_MENU_INSTALACIOCATEGORY_LIST', 'Llistar categories');

	define('TECNOMIX_MENU_EQUIP', 'Equips');
	define('TECNOMIX_MENU_EQUIP_NEW', 'Entrar nou equip');
	define('TECNOMIX_MENU_EQUIP_LIST', 'Llistar i editar equips');
	define('TECNOMIX_MENU_EQUIP_BIN', 'Paperera de reciclatge');
	define('TECNOMIX_MENU_EQUIPCATEGORY', 'Categories d\'equips');
	define('TECNOMIX_MENU_EQUIPCATEGORY_NEW', 'Entrar nova categoria');
	define('TECNOMIX_MENU_EQUIPCATEGORY_LIST', 'Llistar categories');

	define('TECNOMIX_MENU_PROJECTE', 'Projectes');
	define('TECNOMIX_MENU_PROJECTE_NEW', 'Entrar nou projecte');
	define('TECNOMIX_MENU_PROJECTE_LIST', 'Llistar i editar projectes');
	define('TECNOMIX_MENU_PROJECTE_BIN', 'Paperera de reciclatge');
}

$gl_messages_new['no_category']='Per poder insertar notícies hi ha d\'haver almenys una categoria';

// captions seccio
$gl_caption_new['c_prepare'] = 'En preparació';
$gl_caption_new['c_review'] = 'Per revisar';
$gl_caption_new['c_public'] = 'Públic';
$gl_caption_new['c_archived'] = 'Arxivat';
$gl_caption_new['c_new'] = 'Notícia';
$gl_caption_instalacio['c_new'] = 'Instalació';
$gl_caption_equip['c_new'] = 'Equip';
$gl_caption_projecte['c_new'] = 'Projecte';
$gl_caption_new['c_title'] = 'Títol';
$gl_caption_new['c_subtitle'] = 'Subtítol';
$gl_caption_new['c_content'] = 'Contingut';
$gl_caption_new['c_content_list']='Descripció curta al llistat';
$gl_caption_new['c_entered'] = 'Data publicació';
$gl_caption_new['c_expired'] = 'Data caducitat';
$gl_caption_new['c_status'] = 'Estat';
$gl_caption_new['c_category_id'] = 'Categoria';
$gl_caption_new['c_filter_category_id'] = 'Totes les categories';
$gl_caption_new['c_filter_status'] = 'Tots els estats';
$gl_caption_new['c_ordre'] = 'Ordre';

$gl_caption_category['c_category'] = 'Categoria';
$gl_caption_category['c_new_button'] = 'Veure notícies';
$gl_caption_instalaciocategory['c_new_button'] = 'Veure instal·lacions';
$gl_caption_equipcategory['c_new_button'] = 'Veure equips';
$gl_caption_category['c_ordre'] = 'Ordre';
$gl_caption_image['c_name'] = 'Nom';
$gl_caption_new['c_url1_name']='Nom URL(1)';
$gl_caption_new['c_url1']='URL(1)';
$gl_caption_new['c_url2_name']='Nom URL(2)';
$gl_caption_new['c_url2']='URL(2)';
$gl_caption_new['c_videoframe']='Video (youtube, metacafe...)';
$gl_caption_new['c_file']='Arxius';
$gl_caption_new['c_in_home']='Surt a la home';
$gl_caption_new['c_new_file_name']=$GLOBALS['gl_caption']['c_file_name'];
$gl_caption_new['c_new_old_file_name']=$GLOBALS['gl_caption']['c_old_file_name'];
?>
