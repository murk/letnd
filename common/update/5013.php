<?

/* 29-3-13 
  Actualitzacio imatges a sectors



 */
if (is_table("product__sector") && !is_table('product__sector_image')){	
	$update_sql .="
            CREATE TABLE `product__sector_image` (
                    `image_id` INT(11) NOT NULL AUTO_INCREMENT,
                    `sector_id` INT(11) NOT NULL DEFAULT '0',
                    `name` VARCHAR(100) NOT NULL DEFAULT '' COLLATE 'utf8_spanish2_ci',
                    `size` INT(11) NOT NULL DEFAULT '0',
                    `ordre` INT(11) NOT NULL DEFAULT '0',
                    `main_image` INT(1) NOT NULL DEFAULT '0',
                    PRIMARY KEY (`image_id`)
            )
            COLLATE='utf8_spanish2_ci'
            ENGINE=MyISAM;";
	$update_sql .="
            CREATE TABLE `product__sector_image_language` (
                    `image_id` INT(11) NOT NULL DEFAULT '0',
                    `language` CHAR(3) NOT NULL COLLATE 'utf8_spanish2_ci',
                    `image_alt` VARCHAR(150) NOT NULL COLLATE 'utf8_spanish2_ci',
                    `image_title` VARCHAR(255) NOT NULL COLLATE 'utf8_spanish2_ci'
            )
            COLLATE='utf8_spanish2_ci'
            ENGINE=MyISAM;
            ";

}

?>