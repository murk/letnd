<?
/**
 * BookingPayment
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2017
 * @version $Id$
 * @access public
 */
class BookingPayment extends Module{

	var $processes;
	function __construct(){
		parent::__construct();
	}
	function on_load(){
		$GLOBALS['gl_page']->title = $this->caption['c_payment_title'];
	}

	function login() {
		$module = Module::load('custumer', 'custumer', $this);
		$module->do_action('show_login');
	}
	// Pagament
	function show_method() {
		if ( ! R::id( 'book_id' ) ) {
			return;
		}

		$book_id        = R::id( 'book_id' );
		$payment_number = R::id( 'payment_number', '1' );

		BookingPayment::trace( $book_id, '', 'Show Method', 'payment' );

		$book_rs     = Db::get_row( "SELECT price_total, payment_$payment_number AS payment, (price_total - payment_done_1 - payment_done_2 - payment_done_3 - payment_done_4) AS rest FROM booking__book WHERE book_id = $book_id" );
		$price_total = $book_rs['price_total'];
		$payment     = $book_rs['payment'];
		$rest     = $book_rs['rest'];

		if ( $price_total == $payment ) {

			$this->caption['c_method_title2'] =
				sprintf( $this->caption['c_method_title2'],
					$payment,
					$this->config['currency_name'] );
		}
		elseif ($payment_number == '1') {

			$this->caption['c_method_title2'] =
				sprintf( $this->caption['c_method_title_initial'],
					$payment,
					$this->config['currency_name'],
					$rest,
					$this->config['currency_name']
					);
		}
		else {

			$this->caption['c_method_title2'] =
				sprintf( $this->caption['c_method_title_rest'],
					$this->caption["c_method_title_number_$payment_number"],
					$payment,
					$this->config['currency_name']
					);

		}

		$this->set_vars_method();
		
		// loop per mètodes de pagament
		$payment_methods = explode(',',$this->config['payment_methods']);
		$payment_loop = [];

		$payment_number_url = $payment_number=='1'?'':"?payment_number=$payment_number";

		if ($payment_number_url){
			$account_link = Page::get_link('booking', 'payment', 'show-account', false ,array('process'=>'end','book_id'=>$book_id), false) . $payment_number_url;
		}
		else {
			$account_link = Page::get_link('booking', 'book', 'add-custumers', false ,array('show_account'=>'1','book_id'=>$book_id), false);
		}


		$vars = [
				'paypal_link' => Page::get_link('booking', 'payment', 'show-paypal', false, [ 'process' =>'method', 'book_id' =>$book_id ], false) . $payment_number_url,
				'lacaixa_link' => Page::get_link('booking', 'payment', 'show-lacaixa', false, [ 'process' =>'method', 'book_id' =>$book_id ], false) . $payment_number_url,
				'account_link' => $account_link
		];

		
		foreach ($payment_methods as $method){		
			$payment_loop[] = [
				'link' => $vars[$method . '_link'],
				'caption' => $this->caption['c_method_' . $method],
				'method' => $method
			];
		}
		$this->set_vars($vars);
		$this->set_loop('loop',$payment_loop);

		$content = $this->process();
		$GLOBALS['gl_content'] = $this->get_process() . $content;
	}
	// Pagament PASAREL·LA
	function show_paypal(){
		$this->set_vars_method();
		$order_vars = $this->get_order_vars();

		include(DOCUMENT_ROOT . '/public/modules/booking/booking_payment_paypal.php');
		$payment = new BookingPaymentPaypal($this, $order_vars);
		$this->set_var('paypal_form', $payment->get_content());

		$content = $this->process();
		$GLOBALS['gl_content'] = $this->get_process() . $content;

		Debug::p( [
					[ 'order_vars', $order_vars ],
				], 'bookingPayment');

	}
	// Pagament PASAREL·LA REDSYS
	function show_lacaixa(){
		$this->set_vars_method();
		$order_vars = $this->get_order_vars();

		include(DOCUMENT_ROOT . '/public/modules/booking/booking_payment_lacaixa.php');
		$payment = new BookingPaymentLacaixa($this, $order_vars);
		$this->set_var('lacaixa_form', $payment->get_content());

		$content = $this->process();
		$GLOBALS['gl_content'] = $this->get_process() . $content;

		Debug::p( [
					[ 'order_vars', $order_vars ],
				], 'bookingPayment');

	}
	function show_account(){

		$order_vars = $this->get_order_vars();


		$payment_number =  R::id( 'payment_number', '1' );
		$this->set_payment_method( 'account', $payment_number, $order_vars['book_id'] );

		$this->set_vars( array(
			'price_total'       => $order_vars['totals']['price_total'],
			'promcode_discount' => $order_vars['totals']['promcode_discount'],
			'banc_name'         => $this->config['banc_name'],
			'account_number'    => $this->config['account_number'],
			'concept'           => sprintf( $this->caption['c_book_form_title'], $order_vars['book_id'] ),
		) );

		$this->set_vars_method();

		$this->send_mails($order_vars['book_id'], 'account');

		// $this->set_google_conversion(); No està fet a inmo

		$content = $this->process();

		$ret =  $this->get_process() . $content;

		if ($payment_number != '1'){
			$GLOBALS['gl_content'] = $ret;
		}
		else {
			return $ret;
		}

	}
	function set_payment_method($method, $payment_number, $book_id){

		$sql = "UPDATE booking__book
		SET payment_method_$payment_number = '$method'
		WHERE book_id ='$book_id'";

		Db::execute( $sql );

	}
	// Defineix variables comunes per: show_method show_paypal show_account show_ondelivery show_end
	function set_vars_method(){
		$this->set_file('booking/payment_common.tpl');
		$this->set_vars(array('show_method'=>false,'show_paypal'=>false,'show_lacaixa'=>false,'show_account'=>false,'show_end'=>false));
		
		$this->set_var($this->action,true);
		
		$this->set_var('google_conversion',false);

		$this->set_vars($this->caption);
	}
	function set_notice(){
		/**/
		$this->set_vars_method();
		$payment_number = R::id( 'payment_number', '1' );

		Debug::p( [
					[ 'Post ' . $this->process, $_POST ],
				], 'bookingPayment');

		if ($this->process=='lacaixa'){
			include(DOCUMENT_ROOT . '/public/modules/booking/booking_payment_lacaixa.php');
			$payment = new BookingPaymentLacaixa($this);
		}
		else{
			include(DOCUMENT_ROOT . '/public/modules/booking/booking_payment_paypal.php');
			$payment = new BookingPaymentPaypal($this);
		}
		$is_valid = $payment->set_notice();
		
		if (!$is_valid) die();
		
		$this->set_customer($payment->book_id);

		// Comprobo si la notificació es la primera que rebo del banc
		$saved_order = Db::get_row("
			SELECT transaction_id_$payment_number AS transaction_id, order_status_$payment_number AS order_status
			FROM booking__book
			WHERE book_id ='" . $payment->book_id . "' LIMIT 1");

		$saved_transaction = $saved_order['transaction_id'];
		$saved_status = $saved_order['orderstatus'];
		//$is_diferent = ($saved_transaction==$payment->transaction_id); // ja veurem que faig, s'hauria d'ocupar el modul paypal
		$is_first = ($saved_transaction=='');


		$payment_number =  R::id( 'payment_number', '1' );

		$status = $payment->status == 'completed'?",status = 'booked'":"";

		// Grabo el nou estat
		$sql = "UPDATE booking__book
		SET order_status_$payment_number = '" . $payment->status . "'
		,payment_method_$payment_number = '" . $this->process . "'
		,transaction_id_$payment_number = '" . $payment->transaction_id . "'
		,approval_code_$payment_number = '" . $payment->approval_code . "'
		,error_code_$payment_number = '" . $payment->error_code . "'
		,error_description_$payment_number = '" . $payment->error_description . "'
		,payment_done_$payment_number = payment_$payment_number
		,payment_entered_$payment_number = now()
		$status
		WHERE book_id ='" . $payment->book_id . "' LIMIT 1";

		BookingPayment::trace( $sql, '', false, 'payment' );

		Db::execute($sql);

		Debug::p( [
					[ 'Actualització book - set_notice', $sql ],
				], 'bookingPayment');
			
		// envio els mails un cop acabada la compra, o si paypal notifica la compra acabada
		if ($payment->status == 'completed')
			$message = $this->process . '_completed';
		else
			$message = $this->process . '_pending';

		// Si es el primer envia mail,
		// Si no es el primer però a canviat a completed, també
		if ($is_first
		    ||
		    ($payment->status == 'completed' && $saved_status != 'completed')
		) $this->send_mails($payment->book_id, $message);


		/**/
		// TEST    ?tool=booking&tool_section=payment&language=cat&action=set_notice
	}

	// REVISAR ,pensar i testejar si el sistema es bo, la seguretat del book_id ja es comproba desdel modul paypal que envia un mail a admintotal si algo no va be
	// poso el customer de la comanda en sessió, ja que quan es el paypal el que determina que s'ha pagat, el customer no està en sessió ni loggejat ja que no es ell qui entra a la pàgina sino paypal	
	function set_customer($book_id){
		$rs = Db::get_row("SELECT custumer_id, mail FROM custumer__custumer WHERE custumer_id = (
								SELECT custumer_id FROM booking__book_to_custumer WHERE ordre = 1 AND book_id = '$book_id')");
		
		$_SESSION['custumer']['custumer_id'] = $rs['custumer_id'];
		$_SESSION['custumer']['mail'] = $rs['mail'];
	}


	// funcio de TEST per veure com queda el mail quan estem maquetant, nomès en local w.elquesigui, order_id i payment_method es opcional
	// TEST    ?tool=booking&tool_section=payment&language=cat&action=mail_comanda
	// TEST    ?tool=booking&tool_section=payment&language=cat&action=mail_comanda&order_id=1&payment_method=account&is_seller
	// TEST    ?tool=booking&tool_section=payment&language=cat&action=send_password_test&mail=sanahuja@gmail.com
	function mail_comanda(){
		// if (!$GLOBALS['gl_is_local']) Main::error_404();
		$this->set_vars_method();

		$book_id =R::id('book_id');
		if (!$book_id){
			$book_id = Db::get_first('SELECT book_id FROM booking__book LIMIT 1;');
			$payment_method = Db::get_first('SELECT payment_method_1 FROM booking__book LIMIT 1;');
		}
		else{
			$payment_method = Db::get_first('SELECT payment_method_1 FROM booking__book WHERE book_id = \''.$book_id.'\' LIMIT 1;');
		}
		if (isset($_GET['payment_method'])){
			$payment_method = R::escape('payment_method');
		}

		$this->set_customer($book_id);

		$this->send_mails($book_id, $payment_method, true);
	}
	
	// Codi de conversió	
	
	// - En els moduls dels bancs quan es retorna a la pagina de la botiga 
	//				( no es del tot fiable perque potser que no tornin, faria falta un metode per comunicar a google via php )
	// 
	// - En els moduls de pagament diferit quan es tria el metode de pagament
	//				( show_directdebit, show_ondelivery, show_account )
	
	function set_google_conversion(){
		
		$analytics = $this->config['google_analytics_conversion'];
		if (!$analytics) return;
			
		$analytics = str_replace('"es"', '"' . LANGUAGE_CODE . '"', $analytics);
		$analytics = str_replace('"en"', '"' . LANGUAGE_CODE . '"', $analytics);
		
		$this->set_var('google_conversion',$analytics);
			
	}
	// Enviament de mails
	
	// - En els moduls dels bancs quan es notifica venta el primer cop 
	//				( els segons cops poden ser per actualitzar estat )
	// 
	// - En els moduls de pagament diferit quan es tria el metode de pagament
	//				( show_directdebit, show_ondelivery, show_account )

	// TODO-i Probar online al pujar mysqli  http://w2.ronisol/?tool=booking&tool_section=payment&language=cat&action=mail_comanda
	function send_mails($book_id, $action, $is_test=false){

		Main::load_class( 'booking', 'book', 'public');

		$customer =  $this->_get_customer($book_id);
		$custumer_id = $customer['custumer_id'];
		$property_id = Db::get_first( "SELECT property_id FROM booking__book WHERE book_id = $book_id" );


		$t = [];

		/*
		NO SE SI CAL

		*$t['c_text_buyer'] =
			sprintf($this->caption['c_text_buyer'],$this->caption['c_treatment_'.$customer['treatment']], $customer['name']);
		$t['c_text_buyer2'] =
			sprintf($this->caption['c_text_buyer2'],$_SERVER['HTTP_HOST'],$book_id,$customer['custumer_id'], 'http://'.$_SERVER['HTTP_HOST'].'/?tool=booking&tool_section=custumer&action=show_login&language=' . LANGUAGE);
		$t['c_text_buyer3'] = $this->caption['c_' . $action . '_text_buyer'];
		$t['c_text_seller'] =
		sprintf($this->caption['c_text_seller'], $customer['name'],$_SERVER['HTTP_HOST']);
		$t['c_text_seller2'] = $this->caption['c_' . $action . '_text_seller'];*/


		$booking_module = Module::load( 'booking', 'book', $this );
		$booking_module->property_id = $property_id;

		// Preus
		$prices = $booking_module->price_module->get_price(
						'',
						'',
						$book_id,
						$property_id
					);

		$booking_module->send_mail_booked( $book_id, $custumer_id, $prices, $t, $is_test );


	}
	function _get_customer($id){

		// El principal sempre es ordre 1
		$sql = "SELECT CONCAT(name,' ', surname1) as name, mail, treatment, custumer_id FROM custumer__custumer WHERE custumer_id = (
								SELECT custumer_id FROM booking__book_to_custumer WHERE ordre = 1 AND book_id = '$id')";
		return Db::get_row($sql);
	}
	// Grabar la comanda
	function get_order_vars (){

		$book_id = R::id( 'book_id' );
		$payment_number =  R::id( 'payment_number', '1' );


		$rs = Db::get_row( "SELECT book_id, payment_$payment_number AS price_total, promcode_discount FROM booking__book WHERE book_id = $book_id" );

		$book_id = $rs['book_id'];
		$price_total = $rs['price_total'];
		$promcode_discount = $rs['promcode_discount'];

		$custumer_id =$_SESSION['custumer']['custumer_id'];

		$ret = [
			'book_id'     => $book_id,
			'payment_number'     => $payment_number,
			'custumer_id' => $custumer_id,
			'results'     => [
				0 => [
					'title'       => $this->caption['c_subject_booked_owner'] . "$book_id",
					'book_id'     => $book_id,
					'quantity'    => 1,
					'price_total' => $price_total
				]
			],
			'totals'      => [
				'price_total' => $price_total,
				'promcode_discount' => $promcode_discount
			]
		];

		return $ret;
	}

	// Menu de process de la compra
	function show_process(){

		$content = $this->get_process();
		$GLOBALS['gl_page']->title = $this->processes[$this->action]['item'];
		$GLOBALS['gl_content'] = $content;
	}

	function get_process(){

		// Això està a product, aquí de moment no, però ho deixo per si es posa més endavant

		return;


		/*
		$tpl = new phemplate(PATH_TEMPLATES);
		$tpl->set_file('booking/payment_process.tpl');
		$tpl->set_vars($this->caption);

		$processes = array(
			'show_method'=>array(
				'conta'=>0,
				'loop_count'=>2,
				'id'=>1,
				'item'=>$this->caption['c_payment_payment'],
				'selected'=>$this->process=='method'?1:0,
				'first_last'=>''
			),
			'show_end'=>array(
				'conta'=>1,
				'loop_count'=>2,
				'id'=>2,
				'item'=>$this->caption['c_payment_end'],
				'selected'=>$this->process=='end'?1:0,
				'first_last'=>'last'
			)
		);

		$tpl->set_loop('processes',$processes);
		$tpl->set_var('process',$this->process);

		return $tpl->process();*/
	}
	
	/**
	 * BookingPayment::trace()
	 * Crea un arxiu de log desde el modul de pagament
	 * @return
	 */
	static public function trace($text, $name = '', $title = false, $file){
		
		$fp = fopen (CLIENT_PATH . '/temp/'.$file.'.log', "a+");
		Debug::p ( CLIENT_PATH . '/temp/'.$file.'.log', 'Trace booking' );
		Debug::p ( $name, 'name' );
		Debug::p ( $title, 'title' );
		Debug::p ( $text, 'text' );
		
		$log = '';
		if ($title){
			$log .="\n\n\n================================================\n" . $title . ' - ' .date("Y-m-d H:i:s"). " - " . $_SERVER['REMOTE_ADDR'] . "\n================================================\n";
		}
		if ($name){
			$name .= ": ";
		}
		$log .= "\n" . $name . $text;
		
		fwrite($fp,$log);
		fclose($fp);
	}
	
	/**
	 * BookingCustomer::is_logged()
	 * Saber si està logejat
	 * @return
	 */
	function is_logged(){
		return (isset($_SESSION['custumer']['custumer_id']) && isset($_SESSION['custumer']['mail']));
	}

	/**
	 * BookingCustomer::do_action()
	 * Sobreescric la funció heredada
	 * Qualsevol acció d'aquesta clase l'haig de cridar per aquí,
	 * ja que és on controlo que l'usuari està loguejat
	 * @param string $action
	 * @return
	 */
	function do_action($action=''){
		// get_notice ve directament de la web del banc, per tant no hi ha cap sessio en marxa
		if ($this->action=='get_order' || $this->action=='set_notice' || $this->action=='mail_comanda') {
			parent::do_action();
			Debug::p_all();
			die();
		}

		// FA FALTA???
		// si la cistella es buida, es mostra el missatge i no fa cap acció
		//if (!isset($_SESSION['product']['cart'])) {
		//	$GLOBALS['gl_message'] = $this->caption['c_cart_is_empty'];
		//	return;
		//}

		$ac = $action?$action:$this->action;
		// es important no barrejar action amb this->action, per això faig servir una tercera variable $ac
		if ($ac=='login') {
			return parent::do_action($action);
		}
		else
		{
			if ($this->is_logged()) {
				return parent::do_action($action);
			}
			else
			{
				parent::do_action('login');
			}
		}
	}
}