<?
/**
 * LlemenaAgenda
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2011
 * @version $Id$
 * @access public
 */
class LlemenaAgenda extends Module{

	function __construct(){
		parent::__construct();
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function get_records()
	{
		$listing = new ListRecords($this);
		
		
		// per poder previsualitzar desde admin i enviar desde newsletter
		if (isset($_GET['loginnewsletter']) 
				&& $_GET['loginnewsletter'] == 'letndmarc'
				&& $_GET['passwordnewsletter'] == '5356aopfqkiios5cnoqws90se'
		)
		{
			if (is_file(PATH_TEMPLATES . 'newsletter/agenda_list.tpl')) 
					$listing->template = 'newsletter/agenda_list';
			
		}
		
		$listing->condition = "(status = 'public')
							AND (end_date=0 OR end_date > (curdate()))";
		
		$agendacategory_id = R::id('agendacategory_id');
		if ($agendacategory_id) $listing->condition .= " AND agendacategory_id = '" . $agendacategory_id . "'";
		
		$listing->call('records_walk','url1,url1_name,url2,url2_name,start_date,end_date,date_text,content,content_list');
		$listing->order_by = 'ordre ASC, start_date ASC, end_date ASC';
		if ($this->parent=='home'){
			$listing->limit = '0,4';
			$listing->set_records();
			return $listing->loop;
		}
		else{
			return $listing->list_records();
		}
	}
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{
		$show = new ShowForm($this);
		
		
		// per poder previsualitzar desde admin i enviar desde newsletter
		if (isset($_GET['loginnewsletter']) 
				&& $_GET['loginnewsletter'] == 'letndmarc'
				&& $_GET['passwordnewsletter'] == '5356aopfqkiios5cnoqws90se'
		)
		{
			if (is_file(PATH_TEMPLATES . 'newsletter/agenda_form.tpl')) 
					$show->template = 'newsletter/agenda_form';
			
		}		
		$show->condition = "(status = 'public')
							AND (end_date=0 OR end_date > (curdate()))";
		$show->call('records_walk','url1,url1_name,url2,url2_name,start_date,end_date,date_text');
		$show->get_values();		
		if ($show->has_results)
		{
			$GLOBALS['gl_page']->title = $show->rs['agenda'];
			$GLOBALS['gl_page']->page_title = $show->rs['agenda'];
			$GLOBALS['gl_page']->page_description = add_dots_meta_description($show->rs['content']);
			$GLOBALS['gl_breadcrumb'] = $show->rs['agenda'];
			return $show->show_form();
		}
		else
		{
			Main::redirect('/');
		}
	}
	function records_walk($url1,$url1_name,$url2,$url2_name,$start_date,$end_date,$date_text,$content=false,$content_list=false){		
		// Això ho poso aquí així no cal posar-ho al tpl, simplement posar: <a href="<=$url1>" target="_blank"><=$url1_name></a>
		$ret['url1_name'] = $url1_name?$url1_name:$url1;
		$ret['url2_name'] = $url2_name?$url2_name:$url2;
		
		// Així es pot posar l'adreça amb http o sense ( a la plantilla o poso sempre sense ja que pot ser https
		if (strpos($url1,'://')===false && $url1) $ret['url1']= 'http://' . $url1;
		if (strpos($url2,'://')===false && $url2) $ret['url2']= 'http://' . $url2;
		
		// Si hi ha data en text, la mostro, sino mostro les dates
		if (!$date_text) {
			$start_date = format_date_form($start_date);
			$end_date = format_date_form($end_date);
			if ($start_date) $ret['date_text']= $start_date;
			if ($end_date) $ret['date_text'] .= ' - ' . $end_date;
		}
		
		// si es llistat passa lavariable content
		if ($content!==false){
			$ret['content']=$content_list?$content_list:add_dots('content', $content);
		}
		return $ret;
	}
}
?>