<?
/**
 * InvestinEspai
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2015
 * @version $Id$
 * @access public
 */
class InvestinEspai extends Module {

	function __construct() {
		parent::__construct();
	}
	function on_load (){
		$this->set_field( 'zone_id', 'type', 'text' );
		parent::on_load();
	}

	function list_records() {
		$GLOBALS['gl_content'] = $this->get_records();
	}

	function get_records() {


		$comarca_id  = R::number( 'comarca_id', 20 ); // per defecte Girona

		if ($comarca_id == 20) $comarca = 'Girona';
		if ($comarca_id == 2) $comarca = 'Figueres';

		if ( !$GLOBALS['gl_is_tool_home'])
			Page::set_title(Page::get_title() . ' - ' . $comarca);

		$listing = new ListRecords( $this );

		$listing->set_var('comarca_id', $comarca_id);

		$listing->order_by = 'zone ASC';

		$municipi_id = R::number('municipi_id');

		if ( ! $municipi_id ) {
			// Agafa el primer municipi automàticament
			// Copia del query que hi ha al block, però aquest s'executa primer
			$query = "SELECT DISTINCT inmo__municipi.municipi_id
				FROM inmo__municipi
				WHERE municipi_id IN (
					SELECT DISTINCT municipi_id
					FROM investin__espai
					WHERE status='public'
					AND investin__espai.bin = 0
				)
				AND inmo__municipi.comarca_id = '" . $comarca_id . "'
				ORDER BY municipi
				LIMIT 1";
			// fi còpia
			$municipi_id = $_GET['municipi_id'] = Db::get_first( $query );
		}

		$listing->condition = "status = 'public'";

		if ($municipi_id){
			$listing->condition .= " AND municipi_id = '" . $municipi_id . "'";
		}

		return $listing->list_records();
	}

	function show_form() {
		$GLOBALS['gl_content'] = $this->get_form();
	}

	function get_form() {

		$comarca_id  = R::number( 'comarca_id', 20 ); // per defecte Girona

		$show = new ShowForm( $this );

		$show->get_values();

		$show->set_var('comarca_id', $comarca_id);

		if ( ! $show->has_results )
			Main::error_404();

		Page::set_title($show->rs['zone']);
		Page::set_page_title($show->rs['zone']);
		Page::set_page_description($show->rs['altres']);
		// Page::set_page_description($show->rs['page_description'],$show->rs['altres']);

		$content = $show->show_form();

		return $content;
	}
}

?>