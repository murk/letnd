<?php
// menus eina
if (!defined('RACING_MENU_RALLY')){
	define('RACING_MENU_RALLY', 'Rallis');
	define('RACING_MENU_RALLY_NEW', 'Entrar nou ralli');
	define('RACING_MENU_RALLY_LIST', 'Llistar i editar rallis');
	define('RACING_MENU_RALLY_BIN', 'Paperera de reciclatge');

	define('RACING_MENU_CATEGORY', 'Categories');
	define('RACING_MENU_CATEGORY_NEW', 'Entrar nova categoria');
	define('RACING_MENU_CATEGORY_LIST', 'Llistar categories');

	define('RACING_MENU_SUPORT', 'Suport');
	define('RACING_MENU_SUPORT_NEW', 'Entrar nou suport');
	define('RACING_MENU_SUPORT_LIST', 'Llistar i editar suports');
	define('RACING_MENU_SUPORT_BIN', 'Paperera de reciclatge');

	define('RACING_MENU_VIDEO', 'Vídeos');
	define('RACING_MENU_VIDEO_NEW', 'Entrar nou vídeo');
	define('RACING_MENU_VIDEO_LIST', 'Llistar i editar vídeos');
	define('RACING_MENU_VIDEO_BIN', 'Paperera de reciclatge');
}

$gl_messages['no_category']='Pour insérer nouvelles doit avoir au moins une catégorie';

// captions seccio
$gl_caption['c_prepare'] = 'En préparation';
$gl_caption['c_review'] = 'Pour examiner';
$gl_caption['c_public'] = 'Public';
$gl_caption['c_archived'] = 'Filed';
$gl_caption['c_rally'] = 'Ralli';
$gl_caption['c_suport'] = 'Nom';
$gl_caption['c_video'] = 'Nom';
$gl_caption['c_title'] = 'Titre';
$gl_caption['c_subtitle'] = 'Sous-titre';
$gl_caption['c_tema'] = $gl_caption['c_tema_id'] = 'Tema';
$gl_caption['c_content'] = 'Contenu';
$gl_caption['c_entered'] = 'Date';
$gl_caption['c_expired'] = 'Expire';
$gl_caption_rally['c_entered'] = 'Data del Rally';
$gl_caption['c_modified'] = 'Date de modification';
$gl_caption['c_status'] = 'État';
$gl_caption['c_category_id'] = 'Catégorie';
$gl_caption['c_filter_category_id'] = 'Toutes les catégories';
$gl_caption['c_filter_status'] = 'Tous les États';
$gl_caption['c_filter_in_home'] = 'Tout dans la Home';
$gl_caption['c_filter_destacat'] = 'Tous les soulignés';
$gl_caption['c_ordre'] = 'Ordre';
$gl_caption['c_filter_tema_id'] = 'Tots els temes';
$gl_caption['c_tema_select_caption'] = 'Cap tema';
$gl_caption['c_manual_id'] = 'Manual relacionat';
$gl_caption['c_manual_select_caption'] = 'Cap manual';

$gl_caption_category['c_category'] = 'Catégorie';
$gl_caption_category['c_racing_button'] = 'Voir suports';
$gl_caption['c_tema_button'] = 'Veure FAQs';
$gl_caption_category['c_ordre'] = 'Ordre';
$gl_caption_image['c_name'] = 'Nom';
$gl_caption['c_url1_name']='Nom URL(1)';
$gl_caption['c_url1']='URL(1)';
$gl_caption['c_url2_name']='Nom URL(2)';
$gl_caption['c_url2']='URL(2)';
$gl_caption['c_url3_name']='Nom URL(3)';
$gl_caption['c_url3']='URL(3)';
$gl_caption['c_videoframe']='Vidéo (youtube, metacafe...)';
$gl_caption['c_file']='Archives';
$gl_caption['c_in_home']='À la home';
$gl_caption['c_in_home_0']='Non à la Home';
$gl_caption['c_in_home_1']='À la home';
$gl_caption['c_content_list']='Brève description dans la liste';
$gl_caption['c_destacat']='Noticia souligné';
$gl_caption['c_destacat_0']='Non souligné';
$gl_caption['c_destacat_1']='Souligné';

$gl_caption['c_rally_file_name'] = $gl_caption['c_suport_file_name'] = $gl_caption['c_video_file_name'] = $GLOBALS['gl_caption']['c_file_name'];
$gl_caption['c_rally_old_file_name'] = $gl_caption['c_suport_old_file_name'] = $gl_caption['c_video_old_file_name'] = $GLOBALS['gl_caption']['c_old_file_name'];
$gl_caption['c_form_level1_title_page_title'] = $GLOBALS['gl_caption']['c_seo'];

$gl_caption['c_selected']='Nouvelles sélectionné';
?>
