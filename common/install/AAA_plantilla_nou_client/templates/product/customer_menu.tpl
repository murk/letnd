<ul id="customer_menu">
	<? foreach($menus as $m): extract ($m)?>
		<li class="sel<?=$selected?> <?=$class?>">
				<a href="<?=$link?>" title="<?=htmlspecialchars($item)?>"><?=$item?></a>
		</li>
	<? endforeach //$loop?>
</ul>