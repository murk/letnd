<?php
// menus eina
if (!defined('LLEMENA_MENU_PAGINA')){
define('LLEMENA_MENU_PAGINA','Edició pàgines'); 
define('LLEMENA_MENU_PAGINA_NEW','Entrar nova pàgina');
define('LLEMENA_MENU_PAGINA_LIST','Pàgines Capcelera'); 
define('LLEMENA_MENU_PAGINA_LIST2','Pàgines Lateral Esquerra'); 
define('LLEMENA_MENU_PAGINA_LIST3','Pàgines "Més coses" Esquerra');
define('LLEMENA_MENU_PAGINA_LIST4','Pàgines "Més coses" Dreta');
define('LLEMENA_MENU_PAGINA_BIN','Paperera de reciclatge'); 
define('LLEMENA_MENU_CATEGORY','Tipus'); 
define('LLEMENA_MENU_CATEGORY_NEW','Entrar nou tipus'); 
define('LLEMENA_MENU_CATEGORY_LIST','Llistar tipus'); 

define('LLEMENA_MENU_RESTAURANT','Restaurants'); 
define('LLEMENA_MENU_RESTAURANT_NEW','Entrar nou restaurant'); 
define('LLEMENA_MENU_RESTAURANT_LIST','Llistar restaurants'); 
define('LLEMENA_MENU_RESTAURANT_BIN','Paperera de reciclatge'); 

define('LLEMENA_MENU_ALLOTJAMENT','Allotjaments');  
define('LLEMENA_MENU_ALLOTJAMENT_NEW','Entrar nou allotjament'); 
define('LLEMENA_MENU_ALLOTJAMENT_LIST','Llistar allotjament'); 
define('LLEMENA_MENU_ALLOTJAMENT_BIN','Paperera de reciclatge'); 

define('LLEMENA_MENU_ALLOTJAMENTCATEGORY','Categories'); 
define('LLEMENA_MENU_ALLOTJAMENTCATEGORY_NEW','Entrar nova categoria'); 
define('LLEMENA_MENU_ALLOTJAMENTCATEGORY_LIST','Llistar categories'); 

define('LLEMENA_MENU_AGENDA','Agenda');  
define('LLEMENA_MENU_AGENDA_NEW','Entrar nova activitat'); 
define('LLEMENA_MENU_AGENDA_LIST','Llistar activitats'); 
define('LLEMENA_MENU_AGENDA_BIN','Paperera de reciclatge'); 

define('LLEMENA_MENU_AGENDACATEGORY','Categories'); 
define('LLEMENA_MENU_AGENDACATEGORY_NEW','Entrar nova categoria'); 
define('LLEMENA_MENU_AGENDACATEGORY_LIST','Llistar categories'); 
}

/*--------------- nomès castellà i català, no traduit a la resta -------------------*/

$gl_caption_pagina['c_menu_1']='Capcelera';
$gl_caption_pagina['c_menu_2']='Lateral Esquerra';
$gl_caption_pagina['c_menu_3']='Més coses Esquerra';
$gl_caption_pagina['c_menu_4']='Més coses Dreta';
$gl_caption_pagina['c_ordre'] = 'Ordre';
$gl_caption_pagina['c_template'] = 'Plantilla';
$gl_caption_pagina['c_paginatpl_id'] = 'Plantilla';
$gl_caption_pagina['c_link'] = 'Link';
$gl_caption_pagina['c_page_title'] = 'Títol a Google';
$gl_caption_pagina['c_title'] = 'Títol dins la pàgina';
$gl_caption_pagina['c_body_subtitle'] = 'Subtítol';
$gl_caption_pagina['c_pagina'] = 'Nom de la pàgina en el menú';
$gl_caption_pagina['c_content'] = 'Contingut';
$gl_caption_pagina['c_body_content_list'] = 'Descripció curta al llistat';
$gl_caption_pagina['c_body_content'] = 'Contingut';
$gl_caption_pagina['c_prepare'] = 'En preparació';
$gl_caption_pagina['c_review'] = 'Per revisar';
$gl_caption_pagina['c_public'] = 'Públic';
$gl_caption_pagina['c_archived'] = 'Arxivat';
$gl_caption_pagina['c_status'] = 'Estat';
$gl_caption_pagina['c_pagina_id'] = 'Id';
$gl_caption_pagina['c_parent_id'] = 'Pertany a';
$gl_caption_pagina['c_level'] = 'Nivell';
$gl_caption_pagina['c_menu'] = 'Menú';
$gl_caption_pagina['c_blocked'] = 'Block.';
$gl_caption_pagina['c_filter_parent_id'] = 'Parent';
//$gl_caption_pagina['c_filter_top_parent'] = 'Totes';
$gl_caption_pagina['c_filter_top_parent2'] = 'Mostrar subpàgines de';
$gl_caption_pagina['c_filter_level'] = 'Level';
$gl_caption_pagina['c_has_content'] = 'Content';
$gl_caption_pagina['c_has_content_0'] = 'no';
$gl_caption_pagina['c_has_content_1'] = 'si';
$gl_caption_pagina['c_edit_content_button'] = 'Editar pàgina';
$gl_caption_pagina['c_edit_pagina_button'] = 'Editar dades';
$gl_caption_pagina['c_add_subpagina_button'] = 'Nova subpàgina';
$gl_caption_pagina['c_url1_name']='Nom URL(1)';
$gl_caption_pagina['c_url1']='URL(1)';
$gl_caption_pagina['c_url2_name']='Nom URL(2)';
$gl_caption_pagina['c_url2']='URL(2)';
$gl_caption_pagina['c_url3_name']='Nom URL(3)';
$gl_caption_pagina['c_url3']='URL(3)';
$gl_caption_pagina['c_address']='Adreça';
$gl_caption_pagina['c_visit']='Visita i horari';
$gl_caption_pagina['c_videoframe']='Vídeo (youtube, metacafe...)';
$gl_caption_pagina['c_file']='Arxius';
$gl_caption_pagina['c_category_id']='Tipus';
$gl_caption_pagina['c_category_id_caption'] = 'Cap tipus';
$gl_caption['c_municipi']='Municipi';
$gl_caption['c_municipi_caption'] = 'Cap municipi';
$gl_caption['c_municipi_santgregori']='Sant Gregori';
$gl_caption['c_municipi_canetadri']='Canet d\'Adri';
$gl_caption['c_municipi_santaniolfinestres']='Sant Aniol de Finestres';
$gl_caption['c_municipi_santmartillemena']='Sant Martí de Llémena';
$gl_caption['c_remove_polyline']='Borrar traçat';

$gl_caption['c_ordre']='Ordre';
$gl_caption['c_category']='Categoria';

// mapes
$gl_caption['c_search_adress']='Buscar adreça';
$gl_caption['c_latitude']='latitud';
$gl_caption['c_longitude']='longitud';
$gl_caption['c_found_direction']='adreça trobada';
$gl_caption['c_edit_map'] = 'Mapa';
$gl_caption['c_save_map'] = 'Guardar ubicació';
$gl_caption['c_click_map'] = 'Fes un click en un punt o més del mapa per guardar la ubicació';
$gl_caption['c_form_map_title'] = 'Ubicació';
$gl_messages['point_saved'] = 'Nova ubicació guardada correctament';
$gl_messages['point_not_saved'] = 'No s\'ha pogut guardar l\'ubicació';

// restaurant

$gl_caption['c_home']='Destacat';
$gl_caption['c_telephone'] = 'Telèfon';
$gl_caption['c_telephone_mobile'] = 'Telèfon Mòbil';
$gl_caption['c_fax'] = 'Fax';
$gl_caption['c_mail'] = 'E-mail';
$gl_caption['c_url'] = 'Url';
$gl_caption['c_adress'] = 'Adreça';
$gl_caption['c_price_id'] = 'Rango de preu';
$gl_caption['c_data']='Dades del client';
$gl_caption['c_general_data']='Dades generals';
$gl_caption['c_descriptions']='Descripcions';
$gl_caption['c_description']='Descripció';
$gl_caption['c_status'] = 'Estat';
$gl_caption['c_prepare'] = 'En preparació';
$gl_caption['c_review'] = 'Per revisar';
$gl_caption['c_public'] = 'Públic';
$gl_caption['c_archived'] = 'Arxivat';
$gl_caption['c_owner_name'] = 'Nom del gerent (privat)';
$gl_caption['c_owner_surnames'] = 'Cognoms del gerent (privat)';
$gl_caption['c_owner_phone'] = 'Teléfon del gerent (privat)';
$gl_caption['c_file']='Arxius';
$gl_caption['c_edit_map'] = 'Mapa';
$gl_caption['c_edit_data'] = 'Editar Client';
$gl_caption['c_save_map'] = 'Guardar ubicació';
$gl_caption['c_click_map'] = 'Fes click a un punt del mapa per establir la situació';
$gl_caption['c_form_map_title'] = 'Ubicació del client';
$gl_caption['c_country_id']='País';
$gl_caption['c_municipi_id']='Municipi';
$gl_caption['c_provincia_id']='Provincia';
$gl_caption['c_comarca_id']='Comarca';
$gl_caption['c_schedule']='Horaris';
$gl_caption['c_observations']='Observacions (privat)';
$gl_caption['c_zoom']='';

$gl_caption_restaurant['c_customer_description'] = 'Descripció';
$gl_caption_restaurant['c_customer_title'] = 'Nom del restaurant';
$gl_caption_restaurant['c_preu_carta']='Preu carta';
$gl_caption_restaurant['c_preu_menu']='Preu menú';
$gl_caption_restaurant['c_servei_adicional']='Servei adicional';
$gl_caption_restaurant['c_festa_semanal']='Festa setmanal';
$gl_caption_restaurant['c_is_cuina']='Cuina de les Carboneres';
$gl_caption_restaurant['c_is_cuina_list']='C. Carboneres';
$gl_caption_restaurant['c_yes']='Sí';
$gl_caption_restaurant['c_no']='No';

$gl_caption['c_filter_municipi']='Tots els municipi';
$gl_caption['c_filter_status']='Tots els estats';
$gl_caption['c_filter_allotjamentcategory_id']='Totes les categories';

$gl_caption_allotjament['c_customer_title'] = 'Nom de l\'allotjament';
$gl_caption_allotjament['c_customer_description'] = 'Característiques';
$gl_caption_allotjament['c_presentacio'] = 'Presentació';
$gl_caption_allotjament['c_servei_interior']='Serveis Interior';
$gl_caption_allotjament['c_servei_exterior']='Serveis exterior';
$gl_caption_allotjament['c_preu']='Preu';
$gl_caption_allotjament['c_allotjamentcategory_id']='Categoria';

$gl_caption['c_ordre']='Ordre';
$gl_caption['c_allotjamentcategory']='Categoria';
$gl_caption['c_view_button']='Veure allotjaments';

$gl_caption_agendacategory['c_agendacategory_id']='Categoria';
$gl_caption_agendacategory['c_agendacategory']='Categoria';
$gl_caption_agendacategory['c_new_button']='Veure activitats';


$gl_caption_agenda['c_agenda'] = 'Activitat';
$gl_caption_agenda['c_title'] = 'Títol';
$gl_caption_agenda['c_subtitle'] = 'Subtítol';
$gl_caption_agenda['c_content'] = 'Contingut';
$gl_caption_agenda['c_entered'] = 'Data publicació';
$gl_caption_agenda['c_expired'] = 'Data caducitat';
$gl_caption_agenda['c_start_date'] = 'Data Inicial';
$gl_caption_agenda['c_end_date'] = 'Data Final';
$gl_caption_agenda['c_date_text'] = 'Data de l\'activitat en text';
$gl_caption_agenda['c_status'] = 'Estat';
$gl_caption_agenda['c_category_id'] = 'Categoria';
$gl_caption_agenda['c_filter_category_id'] = 'Totes les categories';
$gl_caption_agenda['c_filter_status'] = 'Tots els estats';
$gl_caption_agenda['c_ordre'] = 'Ordre';
$gl_caption_agenda['c_url1_name']='Nom URL(1)';
$gl_caption_agenda['c_url1']='URL(1)';
$gl_caption_agenda['c_url2_name']='Nom URL(2)';
$gl_caption_agenda['c_url2']='URL(2)';
$gl_caption_agenda['c_videoframe']='Video (youtube, metacafe...)';
$gl_caption_agenda['c_file']='Arxius';
$gl_caption_agenda['c_in_home']='Surt a la home';
$gl_caption_agenda['c_content_list']='Descripció curta al llistat';

$gl_caption_agenda['c_selected']='Activitats seleccionades';
?>