class Timetable {

	constructor(current_date, can_select_lower_date) {

		this.current_date = new Date(current_date);
		this.can_select_lower_date = can_select_lower_date;
		this.view = 'month';
		this.is_admin = typeof gl_is_admin === 'undefined' ? false : gl_is_admin;
		this.ajax_url = this.is_admin
			? `/admin/?menu_id=2303&tool=lassdive&tool_section=timetable&action=`
			: `/?language=${gl_language}&tool=lassdive&tool_section=timetable&action=`;
		this.is_initialized = false;

	}

	init() {
		this.init_date_nav();
		this.init_search();
		this.init_reset();
		this.init_view();
		this.init_pull();
	}

	init_search() {
		let _this = this;
		let last_product_id = $('#product_id').val();
		$('#product_id').change(function () {
				let product_id = this.value;

				if (last_product_id != product_id) {
					last_product_id = product_id;
					_this.get_centre_id(product_id);
				}
			}
		);
	}

	init_reset() {
		$('#reset-search-timetable').click(function () {
			$('#centre_id_holder').html('');
			$('#product_id_add').val('');
			$('#product_id').val('');
			$('#timetables-list').html('');
		});
	}

	init_view() {
		let _this = this;
		$('#view').change(function () {
			_this.view = $(this).val();
			_this.show_content(true, true);
		});
	}

	init_pull() {
		$('#pull').click(function () {
			if (!$(this).hasClass('initialized')) {
				$(this).addClass('initialized');
			}
			$('body').toggleClass('search-initialized');
		});
	}

	get_centre_id(product_id) {

		if (!product_id) {
			$('#centre_id_holder').html('');
			return;
		}
		this.show_message_sending();
		let
			_this = this,
			url = this.ajax_url,
			data = {
				'action': 'get_select_centre',
				'product_id': product_id,
			};

		$.ajax({
			url: url,
			data: data,
			dataType: 'json'
		})
			.done((data) => {
				if (data.error) {
					_this.show_message(data.error);
					$('#centre_id_holder').html('');
				}
				else if (data.html) {
					if (_this.view == 'month') _this.show_content(); // carerga el producte per tots els centres
					$('#centre_id_holder').html(data.html).change(function () {
						_this.show_content();
					});
					jcf.replace('#centre_id');
					top.hidePopWin();
				}
			})
			.fail((jqXHR, textStatus, errorThrown) => {
				_this.show_message(`${textStatus}: ${errorThrown}`);
				$('#centre_id_holder').html('');
			})
		;
	}


	init_date_nav() {

		this.set_date_nav(this.current_date);
		let _this = this;

		let options = {
			changeMonth: true,
			changeYear: true,
			onClose: function (selectedDate) {
				let date = $(this).datepicker('getDate');
				_this.get_day_html(0, date);
			}
		};

		// if (!this.can_select_lower_date) options.minDate = new Date();

		$('#timetable-date').datepicker(options);

	}

	set_date_nav(date) {
		let today = new Date();
		date = new Date(date);
		let $prev = $('#prev-date');

		// this.areSameDate(today, date) && !this.can_select_lower_date ? $prev.hide() : $prev.show();

	}

	next_date() {
		if (this.view === 'week') {
			this.get_day_html(7);
		}
		else {
			this.current_date.setMonth(this.current_date.getMonth() + 1);
			this.get_day_html(0, this.current_date);
		}
	}

	prev_date() {
		if (this.view === 'week') {
			this.get_day_html(-7);
		}
		else {
			this.current_date.setMonth(this.current_date.getMonth() - 1);
			this.get_day_html(0, this.current_date);
		}
	}

	get_day_html(n, given_date = false) {

		if (given_date) {
			this.current_date = given_date;
		}
		else {
			this.current_date.setDate(this.current_date.getDate() + n);
		}

		let date = this.current_date;
		let new_date = date.getFullYear() + '/' + (date.getMonth() + 1) + '/' + date.getDate();

		this.set_date_nav(new_date);

		$('#timetable-date').val(TimetableCommon.format_date(date, false));

		this.show_content();

	}

	show_content(show_message = true, is_view_change = false) {

		let timetable_date = $('#timetable-date').val();

		let [product_id, user_id, centre_id] = this.get_search_vars();
		let
			message = '',
			_this = this,
			url = this.ajax_url,
			data = {
				'action': `get_${this.view}`,
				'timetable_date': timetable_date,
				'product_id': product_id,
				'user_id': user_id,
				'centre_id': centre_id
			};

		if (this.view === 'week') {
			/* Nomes mostro missatge quan falta el centre
			if (!product_id) {
				message += LASSDIVE_ESCOLLIR_PRODUCTE + '\n';
			}*/
			if (product_id && !centre_id) {
				message += LASSDIVE_ESCOLLIR_CENTRE + '\n';
			}
			if (is_view_change && (!product_id || !centre_id)) {
				message += LASSDIVE_ESCOLLIR_PRODUCT_CENTRE + '\n';
			}
			if (message) {
				// Quan es plantilla inicial no mostra missatge
				if (show_message) this.show_message(message.trim());
				return;
			}
			if (!product_id) {
				return;
			}
		}

		this.show_message_sending();

		$.ajax({
			url: url,
			data: data,
			dataType: 'json'
		})
			.done((data) => {
				if (data.error) {
					_this.show_message(data.error);
				}
				else if (data.html) {
					$('#timetables-list').html(data.html);
					TimetableCommon.init_tips(true, _this.view);

					if (!_this.is_initialized) {
						_this.is_initialized = true;
						$('body').addClass('search-initialized');
					}

					top.hidePopWin();

				}

			})
			.fail((jqXHR, textStatus, errorThrown) => {
				_this.show_message(`${textStatus}: ${errorThrown}`);
			})
		;
	}

	static edit_reserva(order_id) {
		ReservaList.show_popup('timetables-holder');
		Timetable.get_popup_content('timetables-holder', order_id, false);
	}

	static get_popup_content(item, order_id) {

		$('.off-canvas-menu-inner').addClass('iframe');
		$('#timetables-holder-header, #timetables-holder').addClass('timetable-overlay');

		let is_admin = typeof gl_is_admin === 'undefined' ? false : gl_is_admin;
		let base_url = is_admin ? '/admin/?menu_id=2303&action=show_form_edit&order_id=${order_id}' : `/${gl_language}/lassdive/reserva/v/action/show-form-edit/order_id/${order_id}/?is_frame=1`;

		let html = `<iframe src='${base_url}'></iframe>`;

		let header = `
			<div id="timetables-header">
				<div class="text">
					<h5>${LASSDIVE_RESERVA}: ${order_id}</h5>
				</div>
				<div id="timetables-close" onclick="ReservaList.hide_popup('timetables-holder')"></div>
			</div>
		`;

		$(`#${item}-inner`).html(html);
		$(`#${item}-header`).html(header);

		hidePopWin(false);
		$('#timetables-holder-header, #timetables-holder').removeClass('timetable-overlay');


	}

	get_search_vars() {
		let product_id = $('#product_id').val();
		let user_id = $('#user_id').val();
		let centre_id = $('#centre_id').val();
		return [product_id, user_id, centre_id];
	}


	show_message(message) {
		showPopWin('', 300, 100, null, false, false, message);
		window.setTimeout(() => {
			hidePopWin()
		}, 1000);
	}

	show_message_sending() {
		showPopWin('', 300, 100, null, false, false, '<img src="/common/images/wait.png" style="margin-left:10px;vertical-align: middle;" width="32" height="32" />');
	}
}


