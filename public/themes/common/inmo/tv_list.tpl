<!DOCTYPE html>
<html lang="<?= LANGUAGE_CODE ?>">
<head>

	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
	<meta name="format-detection" content="telephone=no"/>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

	<link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,300,400italic,600,600italic,700,700italic' rel='stylesheet' type='text/css'>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

	<script type="text/javascript" src="<?= $js_src ?>"></script>
	<script type="text/javascript" src="/admin/languages/<?= $language ?>.js"></script>

	<link rel="stylesheet" type="text/css" href="<?= $css_src ?>"/>

	<? if ( $custom_css_src ): ?>
		<link rel="stylesheet" type="text/css" href="<?= $custom_css_src ?>"/>
	<? endif // $custom_css_src ?>

</head>

<body class="">
	<main id="main">

		<? foreach( $loop as $l ): extract( $l ) ?>

			<article id="article_<?= $conta ?>">

				<? if( $images ): ?>

					<figure>
							<style scoped>
								#article_<?= $conta ?> figure {
									background-image: url(<?=$image_src?>);
								}
							</style>
					</figure>


				<? endif // images?>

				<? if( $status ): ?>
					<div class="flag sold">
						<span><?= $status ?></span>
					</div>
				<? elseif( $offer ): ?>
					<div class="flag offer">
						<span><?= $c_offer ?></span>
					</div>
				<? elseif( $recent ): ?>
					<div class="flag recent"><span><?= $c_recent ?></span></div>
				<? endif // $status ?>

				<div class="text-holder">

					<? if( $property ): ?>
						<h1>
							<?= $property ?>
						</h1>
					<? endif //property?>

					<? if( $property_title ): ?>
						<h2>
							<?= $property_title ?>
						</h2>
					<? endif //$property_title?>


					<? $details=
						get_property_details(
							'floor_space,room,bathroom,municipi_id', $this, $l
						)
					?>
					<? if( $details ): ?>
						<div class="details-holder">
							<div class="line"></div>
							<?= $details ?>
						</div>
					<? endif // $details ?>

				</div>


				<? if( $ref || $hut ): ?>

					<div class="ref-holder">

						<span class="ref">
						<?= $c_ref ?> <?= $ref ?>
						</span>

						<? if( $logo ): ?>
							<img class="logo" src="<?= $logo ?>"/>
						<? endif // $logo ?>

						<? if( $hut ): ?>
							<span class="hut">
							<?= $c_hut ?>. <?= $hut ?>
						</span>
						<? endif //$hut ?></div>
				<? endif // $ref ?>


				<? if( $price || ( $old_price && $price_change_percent_value < 0 && $tipus_value != 'temp' ) ): ?>

					<div class="price-holder">
						<? if( $old_price && $price_change_percent_value < 0 && $tipus_value != 'temp' ): ?>
							<span class="old-price"><?= $old_price ?> <?= CURRENCY_NAME ?></span>
						<? endif //old_price?>
						<? /* Per maquetar <span class="old-price">100.000 <?= CURRENCY_NAME ?></span> */ ?>


						<? if( $price ): ?>
							<span class="price"><?= $price ?> <?= CURRENCY_NAME ?><? if( $tipus_value == 'temp' && $total_nights == 1 ): ?>/n<? endif //$ ?></span>
						<? endif //price?>

					</div>

				<? endif // $old_price || $price || $price_consult || $price_change_percent ?>


			</article>

		<? endforeach //$loop?>
	</main>
</body>
<? /* Per poder disenyar en el mateix navegador
 <script src="https://julian.com/research/velocity/vmd.min.js"></script>
  */ ?>

</html>