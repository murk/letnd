<?php

if (!defined('TITLE_COMARCA')){
	define('TITLE_COMARCA', 'Properties in: ');
	define('TITLE_PRICE', 'Properties for up to %s '.CURRENCY_NAME);
	define('TITLE_PRICE_2', 'Properties for between %s and %s '.CURRENCY_NAME);
	define('TITLE_PRICE_3', 'Properties for more than %s '.CURRENCY_NAME);

	define('TITLE_FLOORSPACE', 'Properties for up to %s m<sup>2</sup>');
	define('TITLE_FLOORSPACE_2', 'Properties for between %s and %s m<sup>2</sup>');
	define('TITLE_FLOORSPACE_3', 'Properties for more than %s m<sup>2</sup>');

	define('TITLE_PERSON', 'Up to %s people');
	define('TITLE_PERSON_2', 'Between %s and %s people');
	define('TITLE_PERSON_3', 'More than %s people');
}

$gl_caption['c_filter_price_min'] = 'Prijs min.';
$gl_caption['c_filter_price_max'] = 'Prijs max.';
$gl_caption['c_filter_price_min_temp'] = 'Prijs min. / nacht';
$gl_caption['c_filter_price_max_temp'] = 'Prijs max. / nacht';

$gl_caption['c_recent'] = 'Neu';

// NO TRADUIT
$gl_caption['c_property_list__view_all'] = 'Alle bezochte eigendommen zien';
$gl_caption['c_characteristics'] = 'Característiques';
// FI NO TRADUIT

$gl_caption['c_booking_close_results_message'] = 'There are no properties available on the given dates, the following properties can be booked on similar dates';
?>