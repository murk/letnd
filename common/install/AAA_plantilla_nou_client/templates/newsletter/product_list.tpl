
<table border="0" cellspacing="0" cellpadding="0" class="letnd-list">
	<? foreach($loop as $l): extract ($l)?>	
	<tr>
		<td class="letnd-list-image <?=$first_last?>" valign="top">
			<?if($images):?>
				<a href="<?= HTTP ?><?= SHORT_HOST_URL ?><?=$show_record_link?>" title="<?=htmlspecialchars($image_title?$image_title:$product_title)?>"><img <?=get_image_attrs($image_src_thumb, $image_alt?$image_alt:$product_title, 180, 180, false)?> /></a>
			<?endif //image?>								
		</td>
		<td class="letnd-list-details <?=$first_last?>" valign="top">			
				<? if ($product_title):?>
					<h2>	
						<a href="<?= HTTP ?><?= SHORT_HOST_URL ?><?=$show_record_link?>" title="<?htmlspecialchars($product_title)?>"><?=$product_title?></a>
					</h2>
				<?endif?>
				
				<? if ($product_description):?>
					<p><?=$product_description?></p>
				<?endif?>
				
				<p class="letnd-info"><a href="<?= HTTP ?><?= SHORT_HOST_URL ?><?=$show_record_link?>"><?=$c_more_info?></a></p>
		</td>
	</tr>		
	<? endforeach //$loop?>
</table>