<?
/**
 * BonetPagina
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) Novembre 2018
 * @version $Id$
 * @access public 
 
 */
class BonetPagina extends Module{
	var $pagina_id, $list_template, $parent_has_only_title = false;
	function __construct(){
		parent::__construct();
	}
	function get_records()
	{
		
		$listing = new ListRecords($this);
		$listing->paginate = false;
		$listing->template = $this->list_template;

		$listing->set_var( 'has_only_title', $this->parent_has_only_title );
		
		$listing->always_parse_template = true;	
		$listing->order_by = 'ordre ASC, pagina, pagina_id';
		$listing->extra_fields = '(SELECT file_name FROM bonet__paginatpl WHERE bonet__paginatpl.paginatpl_id = bonet__pagina.paginatpl_id) as file_name';
		$listing->condition = "parent_id = " . $this->pagina_id . $GLOBALS['gl_bonet_status'];
		$listing->call('records_walk','pagina_id',true);
			
	    $ret = $listing->list_records();
	    return $ret;
	}
	
	function records_walk(&$listing,$pagina_id){
		$rs = &$listing->rs;
		
		if ($listing->list_show_type=='list'){
			$ret['body_content']= add_dots('body_content', $rs['body_content']);
		}
		
		// plantilles
		/* Desconectat
		 * $ret['show_video']= (isset($rs['images'][1]) || $rs['videoframe']);
		if (!$rs['videoframe'] && isset($rs['images'][1])){
			$ret['image2'] = $rs['images'][1];
		}
		*/
		
		// comú
		// $ret['show_info']= ($rs['url1']||$rs['url2']||$rs['url3']||$rs['files']);
		$ret['show_info']= false; // Està desconectat

		return $ret;
	}
	function show_form()
	{
		
		// Faig un show form per les dades de la pàgina
		$show = new ShowForm($this);
		$show->condition = substr($GLOBALS['gl_bonet_status'],5);
		
		$show->get_values();
		
		if (!$show->has_results) Main::error_404();
		
		$show->call('records_walk','pagina_id',true);
		
		
		$this->increment_counter();
		
		// defineixo les variables de la pàgina
		Page::set_title($show->rs['pagina'] );

		Page::set_page_title( $show->rs['page_title'], $show->rs['pagina'] );
		Page::set_page_description( $show->rs['page_description'], $show->rs['body_subtitle2'] );
		Page::set_page_keywords( $show->rs['page_keywords'] );

		if ($show->rs ['template']) {
			$template = $show->rs ['template'];
			$language = LANGUAGE;
			$show->template = "bonet/${template}_$language";
		}
		else {
			$show->template = 'bonet/' . Db::get_first(
					"SELECT file_name FROM bonet__paginatpl WHERE paginatpl_id = " . $show->rs['paginatpl_id'] );
		}


		$body_content = $show->rs['body_content'];
		$body_content2 = $show->rs['body_content2'];
		$body_content3 = $show->rs['body_content3'];
		$paragraph_title = $show->rs['paragraph_title'];
		$paragraph_title2 = $show->rs['paragraph_title2'];
		$paragraph_title3 = $show->rs['paragraph_title3'];
		$has_only_title =
			! $body_content && ! $body_content2 && ! $body_content3 && ! $paragraph_title && ! $paragraph_title2 && ! $paragraph_title3;
		
		// carrego subapartats
		$module = Module::load('bonet','pagina');
		$module->pagina_id =  $show->id;
		$module->parent_has_only_title = $has_only_title;
		$module->list_template = 'bonet/' . Db::get_first(
			"SELECT file_name FROM bonet__listtpl WHERE listtpl_id = " . $show->rs['listtpl_id']);
		$list_content = $module->do_action('get_records');
		// fi subapartats
		
		$show->set_var('list_content',$list_content);


		$show->set_var( 'has_only_title', $has_only_title );
		$content = $show->show_form();
		
		$GLOBALS['gl_content'] = $content;

		$page = &$GLOBALS['gl_page'];

		$pagina_text_id = $show->rs['pagina_text_id'];
		$level = $show->rs['level'];
		$parent_id = $show->rs['parent_id'];
		$page->tpl->set_var('pagina_id',$show->id);
		$page->tpl->set_var('pagina_text_id',$pagina_text_id);
		$page->tpl->set_var('pagina_level_2_id',$this->get_level_2_id($parent_id, $level, $pagina_text_id));

	}
	function get_level_2_id($parent_id, $level, $pagina_text_id) {
		if ($level == '2') return $pagina_text_id;

		$query          = "SELECT pagina_text_id FROM bonet__pagina WHERE pagina_id = $parent_id";
		$pagina_text_id = Db::get_first( $query );
		return $pagina_text_id;
	}
	function increment_counter(){
		$pagina_id = $this->pagina_id;
		include (DOCUMENT_ROOT . 'common/includes/browser/browser.php');
		$browser = new Browser();
		$name = $browser->getBrowser();
		
		
		$is_ok=( 
				$name != Browser::BROWSER_GOOGLEBOT &&
			$name != Browser::BROWSER_SLURP &&
			$name != Browser::BROWSER_W3CVALIDATOR &&
			$name != Browser::BROWSER_MSNBOT &&
			$name != Browser::BROWSER_UNKNOWN
				);		
		
		if ($is_ok){
			// si no està en sessió la conto, nomès un cop per usuari
			if (!isset($_SESSION['bonet']['paginas'][$pagina_id])){
				$_SESSION['bonet']['paginas'][$pagina_id] = true;
				Db::execute("UPDATE bonet__pagina SET counter=(counter+1) WHERE  pagina_id='" .$pagina_id . "'");
			}			
		}		
	}
}
?>