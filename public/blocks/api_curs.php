<?php
$query = "SELECT api__curs.curs_id as curs_id, curs, curs_file_name
				FROM api__curs_language, api__curs
				WHERE api__curs_language.curs_id = api__curs.curs_id
				AND (status = 'public' || status = 'open' || status = 'full' || status = 'finished')
				AND ( end_date >= now() )
				AND bin <> 1
				AND language = '" . LANGUAGE . "'
				ORDER BY ordre ASC, curs ASC";

$results = Db::get_rows($query);

// Busco pagina seleccionada
$file_name = R::file_name( 'file_name' );
$curs_id = 0;

if ( $file_name ) {
	$curs_id = Db::get_first( "SELECT curs_id 
					FROM api__curs_language 
					WHERE language= '" . LANGUAGE . "'
					AND curs_file_name = '" . $file_name . "'" );
	if ( ! $curs_id ) {
		$curs_id = $file_name;
	}
}


$selected_found = false;
if ($results)
{
	$conta = 0;
	$loop_count = count($results);
    foreach($results as $rs)
    {
		$l = array();
		if ($curs_id == $rs["curs_id"])
		{
			$selected = 1;
			$selected_found = true;
		}
		else
		{
			$selected = 0;
		}

		$l['link'] = '/' . LANGUAGE . '/api/curs/' . $rs['curs_id'];



		$l['link'] = get_link(array(
				'id' => $rs['curs_id'],
				'file_name' => $rs['curs_file_name'],
				'tool' => 'api',
				'tool_section' => 'curs',
				'detect_page' => false));


        $l['selected'] = $selected;
        $l['curs'] = $rs['curs'];
		$l['id'] = $rs['curs_id'];
		$l['conta'] = $conta++;
		$l['class'] = '';
		$l['loop_count'] = $loop_count;
        $loop[] = $l;
    }
	$loop[0]['class'] = 'first';	
	$loop[count($loop)-1]['class'] = 'last';
	if (!$selected_found) $loop[0]['selected'] = 1;
}



$query = "SELECT api__curs.curs_id as curs_id, curs, curs_file_name
				FROM api__curs_language, api__curs
				WHERE api__curs_language.curs_id = api__curs.curs_id
				AND (status = 'public' || status = 'open' || status = 'full' || status = 'finished')
				AND ( end_date < now() )
				AND bin <> 1
				AND language = '" . LANGUAGE . "'
				ORDER BY ordre ASC, curs ASC";


$results = Db::get_rows($query);

if ( $results ) {
	$vars['list_past_curs'] = '/' . LANGUAGE . '/api/curs/v/action/list-past/';
}
else {
	$vars['list_past_curs'] = false;
}

?>