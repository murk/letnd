<?
/**
 * LlemenaAgenda
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2010
 * @version $Id$
 * @access public
 */
class LlemenaAgenda extends Module{
	var $is_newsletter = false, $condition, $is_search = false, $q;

	function __construct(){
		parent::__construct();
	}
	function list_records()
	{
		$content =  $this->get_records();
		$GLOBALS['gl_content'] = $this->get_search_form() . $content;
	}
	function get_records()
	{
	    $listing = new ListRecords($this);
		$listing->call('records_walk','content,content_list',true);

        switch ($this->process) { // process-> propietat invetada que passo pel post del tpl
            case 'select_agendas': // llistat dins iframe per seleccionar productes
                $page = &$GLOBALS['gl_page'];
                $page->template = 'clean.tpl';
                $page->title = '';
				$this->is_newsletter = true;
				$this->set_field('content', 'list_admin', '0');
                $listing->set_options(0, 0, 0, 0, 0, 0, 0, 0, 1);				
				$listing->condition = "(status = 'public')
							AND (end_date=0 OR end_date > (curdate()))";
                $GLOBALS['gl_message'] = $GLOBALS['gl_messages']['select_item'];

                break;
        	default:
        } // switch*/
	
		if (isset($_GET['selected_menu_id']))
		{
			$listing->condition = 'category_id = ' . $_GET['category_id'];
		}
		else
		{
		$listing->add_filter('category_id');
		}
		$listing->add_filter('status');
		$listing->order_by = 'ordre ASC, start_date ASC, end_date ASC';


		// search
		if ($this->condition) {
			$and = $listing->condition?' AND ':'';
			$listing->condition .= $and . $this->condition;
		}

	    return $listing->list_records();
	}
	function records_walk(&$listing, $content=false,$content_list=false){		

		$ret = [];

		if ($this->is_newsletter){
			$ret = $this->list_records_walk_select ($listing->rs['agenda_id'], $listing->rs['agenda']);
		}	
		
		// si es llistat passa la variable content
		/*if ($content!==false){
			$ret['content']=$content_list?$content_list:add_dots('content', $content);
		}*/
		return $ret;
	}
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{
	    // si no hi ha cap categoria faig un redirect a entrar categoria nova
		if (!Db::get_first("SELECT count(*) FROM llemena__category LIMIT 0,1")){
			$_SESSION['message'] = $this->messages['no_category'];
			redirect('/admin/?menu_id=2306');
		}
		$this->set_field('start_date','default_value',now());
		$show = new ShowForm($this);
	    return $show->show_form();
	}

	function save_rows()
	{
	    $save_rows = new SaveRows($this);
	    $save_rows->save();
	}

	function write_record()
	{
	    $writerec = new SaveRows($this);
		$writerec->save();
	}

	function manage_images()
	{
	    $image_manager = new ImageManager($this);
	    $image_manager->execute();
	}
	
	/////////////
	// NEWSLETTER
	///////////////
    // Per poder escollir productes desde una finestra amb frame, de moment desde newsletter
    function select_frames()
    {
        $page = &$GLOBALS['gl_page'];

        $page->menu_tool = false;
        $page->menu_all_tools = false;
        $page->template = '';

        $this->set_file('llemena/agenda_select_frames.tpl');
        $this->set_vars($this->caption);
        $this->set_var('menu_id', $GLOBALS['gl_menu_id']);
        // $this->set_var('custumer_id',$_GET['custumer_id']);
        $GLOBALS['gl_content'] = $this->process();
    }
	// per posar el onclick al llistat
	function list_records_walk_select ($product_id, $product_title)
	{
		$product_title= addslashes($product_title); // cambio ' per \'
		$product_title= htmlspecialchars($product_title); // cambio nomès cometes dobles per  &quot;
		$ret['tr_jscript'] = 'onClick="parent.insert_address(' . $product_id . ',\'' . $product_title . '\')"';
		return $ret;
	}




	//
	// FUNCIONS BUSCADOR
	//
	function search(){

		$q = $this->q = R::escape('q');

		$search_condition = '';

		$this->is_search = true;

		if ($q)
		{
			// Busco en tots els idiomes
			$search_condition = "
			 agenda_id IN (
			    SELECT agenda_id FROM llemena__agenda_language
			        WHERE agenda like '%" . $q . "%'
					OR subtitle like '%" . $q . "%'
					OR date_text like '%" . $q . "%'
					OR content_list like '%" . $q . "%'
					OR content like '%" . $q . "%'
					OR url1_name like '%" . $q . "%'
					OR url2_name like '%" . $q . "%'
				)" ;


			$GLOBALS['gl_page']->title = TITLE_SEARCH . '<strong>&nbsp;&nbsp;"' . $q . '"</strong>';
		}
		// per fer un reset de una cerca, busco cadena buida
		else{
			Main::redirect('/admin/?menu_id=' . $GLOBALS['gl_menu_id']);

		}
		$this->condition = $search_condition;
		Debug::add('Search condition', $this->condition);
		$this->do_action('list_records');
	}

	function get_search_form(){
		$tpl = new phemplate(PATH_TEMPLATES);
		$this->caption['c_by_ref']='';
		$tpl->set_vars($this->caption);
		$tpl->set_file('search.tpl');
		$tpl->set_var('menu_id',$GLOBALS['gl_menu_id']);
		$tpl->set_var('q',  htmlspecialchars(R::get('q')));

		if ($this->q) {
			$GLOBALS['gl_page']->javascript .= "
				$('table.listRecord').highlight('".addslashes(R::get('q'))."');			
			";
		}

		return $tpl->process();
	}
}
?>