# Veure producte i order_id de cada orderitem
SELECT

	reservaitem_id,
	orderitem_id,
	order_id,
	product_id,
	ref,

	(
		SELECT (
			SELECT product_title
			FROM product__product_language
			WHERE product__product_language.product_id = product__orderitem.product_id AND language = 'spa')
		FROM product__orderitem
		WHERE product__orderitem.orderitem_id = lassdive__orderitem.orderitem_id
	) AS product,

	reservaitem_date,
	start_time,
	end_time,
	is_done,
	centre_id
FROM lassdive__orderitem
INNER JOIN product__orderitem USING (orderitem_id)
WHERE
	(
		SELECT BIN
		FROM product__order
		WHERE product__order.order_id = product__orderitem.order_id) = 0
ORDER BY `reservaitem_date` DESC;

# Actualització pagament deposit web lassdive
CREATE TABLE IF NOT EXISTS `client__lassdive__configpublic` (
  `configpublic_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `value` varchar(768) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`configpublic_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

INSERT INTO `client__lassdive__configpublic` (`configpublic_id`, `name`, `value`) VALUES
	(1, 'has_client_tool_config', '1');

CREATE TABLE IF NOT EXISTS `client__lassdive__configpublic_product` (
  `name` varchar(50) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `value` varchar(255) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

INSERT INTO `client__lassdive__configpublic_product` (`name`, `value`) VALUES
	('deposit_payment_on', '1'),
	('deposit_payment_percent', '20'),
	('deposit_payment_round', '0'),
	('deposit_rest_round', '5');


UPDATE `product__family` SET `accepts_deposit_payment`='0' WHERE  `family_id`=109;

# Passar de "deposit" a deposit_status
UPDATE product__order SET deposit_status = 'completed', order_status = 'paying' WHERE order_status = 'deposit';
UPDATE product__order SET deposit_status = 'completed' WHERE order_status IN ('completed', 'shipped', 'delivered');
# UPDATE product__order SET deposit_status = 'completed' WHERE order_status IN ('completed', 'shipped', 'delivered') AND deposit <> 0;




