<?
/**
 * WebPage
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2010
 * @version $Id$
 * @access public
 */
class AdmintotalPage extends Module{
	var $page_ids = array(), $menus = 1, $levels_count = array(), $dir, $delete_button=0;

	function __construct(){
		parent::__construct();
		$this->table = 'all__page';
		$this->dir = CLIENT_PATH.'/templates/page/';
		$this->order_by = 'page_id ASC';
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	
	function get_records()
	{
		$this->menus = R::text_id('menu','1');
		
		$_GET['menu'] = $this->menus;
		
	    $listing = new ListRecords($this);
		if ($this->action=='list_records_bin') return $listing->list_records();// la paperera la llisto tal qual
		
		$listing->paginate = false;
		//$listing->set_options(1, 1, 1, 1, 1, 1, 0, 1, 1, 1);
		$listing->add_field('marge');
		$listing->swap_fields('status', 'marge');
		$listing->set_field('marge','type','none');
		$listing->set_field('marge','list_admin','out');
		$listing->add_filter('menu');
		$listing->add_filter('parent_id');
		$listing->show_langs = true;
		
		$parent_id = R::text_id('parent_id');
		
		// si menus igual a 2 però te com a parent la home que es menu 1, llavors peta, per això només ho faig pels menus=1
		if ($parent_id!==false && $this->menus==1){ 
			$this->get_page_ids(0,1);
			$page_ids = implode($this->page_ids,',');
			$listing->condition = 'page_id IN ('.$page_ids.')';
			$listing->condition .= "ORDER BY FIELD(page_id, " . $page_ids . ")";
		}
		
		//$listing->add_button ('edit_page_button', 'action=show_form_edit', 'boto1');
		$listing->call('list_records_walk','page_id,page,level,parent_id,has_content',true);
		
	    return $listing->list_records();
	}
	
	function get_records_xxxx()
	{
	    $listing = new ListRecords($this);
		$listing->show_langs = true;
		$listing->order_by = $this->order_by;
		$listing->add_filter('parent_id');
		$listing->add_filter('level');
	    return $listing->list_records();
	}
	function list_records_walk(&$listing,$page_id,$page,$level,$parent_id,$has_content){
		$style = '';
		if ($level == 1) $style = "font-weight:bold;";
		if ($level > 2) $style = "color:#5e6901;";
		$key = $parent_id . '_' . $level;
		if (!isset($this->levels_count[$key])) {
			$this->levels_count[$key] = 0;
		}
		$this->levels_count[$key]++;
		$ret['marge'] = '<div style="margin-left:'.(30*($level-1)).'px;'.$style.'">'.$this->levels_count[$key].' - ' .$page.'</div>';
		
		//$listing->buttons['edit_page_button']['show'] = ($has_content==1);
		
		return $ret;
	}
	function get_page_ids($parent_id, $level){
		$loop = array();
		$query = "SELECT page_id
						FROM all__page
						WHERE parent_id = " . $parent_id . "
						AND bin <> 1
						AND menu IN (" . $this->menus . ")
						ORDER BY menu ASC, ordre ASC, page_id";
		//Debug::add($query,'Query get_page_recursive');	
		$results = Db::get_rows($query);
		
		if ($results)
		{
			foreach($results as $rs)
			{
				$this->page_ids[]=$rs['page_id'];
				$this->get_page_ids($rs['page_id'], $level+1);
			}
		}
	}
	
	
	
	
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{
	    $show = new ShowForm($this);
		$show->order_by = $this->order_by;
	    return $show->show_form();
	}

	function save_rows()
	{
	    $save_rows = new SaveRows($this);
	    $save_rows->save();
	}

	function write_record()
	{
	    $writerec = new SaveRows($this);
	    $writerec->save();
	}

	function manage_images()
	{
	    $image_manager = new ImageManager($this);
	    $image_manager->execute();
	}
}
?>