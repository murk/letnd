<?php
//
// FALTA
// Posar els missatges d'error en un array global per poder llistar-los
// Marcar l'input de add_record_exists quan estigui repetit
//


class SaveRows Extends ModuleBase
{
    var
    	$set_values_field,
    	$message_deleted,
    	$message_bined,
    	$message_restored,
    	$message_saved,
    	$message_not_saved,
	    $message_list_deleted,
	    $message_list_bined,
	    $message_list_restored,
    	$message_list_saved,
    	$message_list_not_saved,
    	$message_added,
    	$message_add_exists,
    	$field_unique,
    	$call_function,
    	$function_name,
		$function_parameters,
		$condition,
		$id,
		$saved = false,
        $notify_ids = array(),
		$querys = '',
		$post = array();
    // on_save
	private $on_save_function;
	private $delete_results_fields;
	private $deleted_results = [];

	private $inputs_prefix = '';

    function __construct(&$module=false, $inputs_prefix = '')
	{
		$this->link_module($module);

		$this->message_list_deleted = $this->messages['list_deleted'];
        $this->message_deleted = $this->messages['deleted'];
        $this->message_list_bined = $this->messages['list_bined'];
        $this->message_bined = $this->messages['bined'];
        $this->message_list_restored = $this->messages['list_restored'];
        $this->message_restored = $this->messages['restored'];
        $this->message_saved = $this->messages['saved'];
        $this->message_not_saved = $this->messages['not_saved'];
        $this->message_list_saved = $this->messages['list_saved'];
        $this->message_list_not_saved = $this->messages['list_not_saved'];
        $this->message_added = $this->messages['added'];
        $this->message_add_exists = ''; // $this->messages['add_exists'];
		$this->inputs_prefix = $inputs_prefix;

		// guardo el post per quan caduca la sessió poder guardar els canvis al fer el login
        if (isset($_SESSION['post'])) {
        	$_POST = $_SESSION['post'];
	        $this->post =  $_SESSION['post'];
        	unset($_SESSION['post']);
        }
        else {
	        $this->post = $_POST;
        }
		Debug::p( $this->post, "save post" );

	    if ( $this->inputs_prefix ) {
		    $this->post = $this->post[ $this->inputs_prefix ];
	    }


		$this->set_id();


    }

	/**
	    // 1 - array ids seleccionats a -> save_rows_delete_selected, save_rows_bin_selected, save_rows_restore_selected
		// 2 - array ids a plural -> save_rows_save_config, save_rows_save_records
		// 3 - no hi ha post, puc fer un convert_rs més tard com al modul de order
		// 4 - id a singular -> save_record, add_record, copy_record,
		// 5 - A bin_record, delete_record	no és cap array
		// 6 - A add_record id es 0 fins que es guarda i llavors passa a ser el nou id
	 *
	 */
	public function set_id (){

		if (substr($this->action,-9)== '_selected'){
			$this->id = $this->post['selected'];
		}
		elseif (substr($this->action,0,10)== 'save_rows_'){
			$this->id = $this->post[$this->id_field];
		}
		elseif (!isset($this->post[$this->id_field])){
			$this->id = 0;
		}
		elseif (is_array($this->post[$this->id_field])){
			$this->id = current($this->post[$this->id_field]);
		}
		else{
			$this->id = $this->post[$this->id_field]; // a delete bin_record, delete_record no es cap array
		}

		debug::add('$save_rows->id',$this->id);

    }
    function save()
    {
        global $gl_message, $gl_saved;

        switch ($this->action)
        {
            case strstr($this->action, 'save_rows_delete_selected'):
                $this->delete_selected();
                $gl_message = $this->message_list_deleted;
                break;

            case strstr($this->action, 'save_rows_bin_selected'):
                $this->bin_selected();
                $gl_message .= $this->message_list_bined;
                break;

            case strstr($this->action, 'save_rows_restore_selected'):

                $this->set_values_field = "bin";
                $this->set_values_to('0');
                $gl_message = $this->message_list_restored;
                break;

            case strstr($this->action, 'save_rows_selected_to_value'):
                (!$this->set_values_field)?$this->set_values_field = $this->post['move_field']:false;
                $this->set_values_to($this->post[$this->set_values_field]);
                $gl_message = $this->message_list_saved;
                break;

            case strstr($this->action, 'save_rows_save_config'):
                $b = $this->save_records_config();
                $gl_message = $b?$this->message_list_saved:$this->message_list_not_saved;
                $gl_saved = $this->saved = true;
                break;
            case strstr($this->action, 'save_rows_save_records'):
                $b = $this->save_records();
                $gl_message = $b?$this->message_list_saved:$this->message_list_not_saved;

                // Notify_ids de moment nomes es fa servir per quan messaege_exists
                if ( $this->message_add_exists ) {
                    $gl_message = $this->message_add_exists;
                    if ( $this->notify_ids ) {
                        $this->notify( $this->notify_ids, $this->message_add_exists, false );
                    }
                }
                // de moment nomès no es guarda si no hi ha cap camvi, per tant saved=true per no agafar els posts
                $gl_saved = $this->saved = true;
                break;
            case strstr($this->action, 'save_record'):
                $b = $this->save_records();
                $gl_message = $b?$this->message_saved:$this->message_not_saved;
                UploadFiles::gl_upload_get_files($this);

                $this->set_caller_select_edit();

                // de moment nomès no es guarda si no hi ha cap camvi, per tant saved=true per no agafar els posts
                $gl_saved = $this->saved = true;
                break;
            case strstr($this->action, 'add_record'):
            case strstr($this->action, 'copy_record'):
                $b = $this->add_record();
                if ($this->saved) UploadFiles::gl_upload_get_files($this);
                break;
            case strstr($this->action, 'bin_record'):
                $b = $this->bin_record();
                $gl_message = $this->message_bined;
                break;
            case strstr($this->action, 'delete_record'):
                $b = $this->delete_record();
                $gl_message = $this->message_deleted;
                break;
        }

         $this->fire_on_save();

		Debug::add('Save rows querys', $this->querys, 2);
    }
    // guarda canvis en cada resultat del llistat de la columna que hagem posat com editable, no borra res
    function save_records()
    {
        global $gl_saved;
        Debug::add('Post', $this->post);


        $gl_saved = $this->saved = false;
        // bucle cada fila, faig bucle amb el primer de la llista, encara que hi hagi varis camps per guardar, el primer ja em seveix per el bucle
        // faig el bucle amb el old_ , ja que aquests inputs son hidden, per tant em fa el bucle complert,
        // ja que si son checkboxes, nomès fa bucle per els seleccionats, i es salta els que no estan seleccionats
        $rows = $this->post[$this->id_field];

        foreach(array_keys($rows) as $row)
        {
            $row = R::text_id($row,false,false);
			$str = '';
            $str_language = array();
            $query = '';
            if ($this->field_unique && $this->record_exists($row))
            {
                $this->message_list_not_saved = $this->message_add_exists;
                $this->message_not_saved = $this->message_add_exists;
				$GLOBALS['gl_errors']['add_exists'] = $this->message_add_exists;
            }
            else
            {
                // bucle cada camp de la fila i obtindre els seus valors
                foreach ($this->fields as $field => $value)
                {
                    $post = isset($this->post[$field])?$this->post[$field]:null;
                    // per els checkbox, si es null vol dir que no esta seleccionat un checkbox
                    if (!isset($post[$row]))
                    {
                        $post[$row] = '0';
                    }
                    $post_old = isset($this->post["old_" . $field])?$this->post["old_" . $field]:'';
                    // El radio te un valor per el $this->post[$field], ja que no es cap array de botons,
                    // tots tenen el mateix nom i donen de valor el id de la fila seleccionada
                    if (
	                    ( isset( $post_old[ $row ] ) || ( $value['override_save_value'] != '' ) ) &&
	                    ( $value['type'] != 'checkboxes' ) &&
	                    ( $value['type'] != 'checkboxes_long' ) &&
	                    ( $value['type'] != 'sublist' ) &&
	                    ( $value['type'] != 'file' )
	                    // de moment trec lo del _old, que guardi sempre i a pendre pel cul
	                    // es que al no recarregar el formulari lo de old peta (amb el sistema de guardar amb frame)
	                    // && ($post[$row]) != ($post_old[$row])
                    )
                    {
                        // subllistat del camp en tots els idiomes
                        // REVISAR - s'ha de probar amb 2 camps si funciona, podria fallar l'ordre dels idiomes de cada un
                        if (in_array($field, $this->language_fields))
                        {
                            foreach($post[$row] as $language => $val)
                            {
                                if (!isset($str_language[$language])) $str_language[$language] = ''; // per no donar error undefined
                                $str_language[$language] .= $field . " = " . Db::qstr($val) . ", ";
                            }
                        }
                        else
                        {
                            if ($value['override_save_value']!='')
                            {
                                $insert_value = $value['override_save_value'];
                            } elseif ($this->call_function[$field])
                            {
                                $insert_value = call_user_func($this->call_function[$field], $row, $post[$row]);
                            }
                            else
                            {
                                // revisar el tema de treure el format dates, nums, etc...
                                if (function_exists('unformat_' . $value['type']))
                                {
                                    $insert_value = call_user_func ('unformat_' . $value['type'], $post[$row], true, $field, $row,  $value);
                                }
                                else
                                {
                                    $insert_value = Db::qstr($post[$row]);
                                }
                            }
							// per els casos on poso: camp AS camp2
							$field_name = isset($value['change_field_name'])?current(explode(' AS ',$value['change_field_name'])):$field;
							
                            $str .= $field_name . " = " . $insert_value . ", ";
                        }
                    } elseif (
	                    isset( $post_old[ $row ] ) &&
	                    (
		                    $value['type'] == 'checkboxes' ||
		                    $value['type'] == 'checkboxes_long'
	                    ) &&
	                    ( $value['checked_table'] != '' )
                    ) {
	                    $this->insert_checkboxes( $post, $row, $field, $value );
                    } elseif
                    ( isset( $post_old[ $row ] ) && ( $value['type'] == 'sublist' ) ) {
	                    NewRow::save_records( $row, $field, $value, $this->id_field, $this->action, $this->module );
                    }
                }
                if ($str)
                {
                    $str = substr($str, 0, -2);
                    $query = "UPDATE " . $this->table . " SET " . $str . "
	            			WHERE " . $this->id_field . " = '$row'";
                    Db::execute($query);
					$this->querys .='<br>' . $query . ';';
                    $gl_saved = $this->saved = true;
                }
                // subllistat del camp en tots els idiomes
                foreach($str_language as $language => $str_lang)
                {
                    if ($str_lang)
                    {
                        $str_lang = substr($str_lang, 0, -2);
                        $query = "UPDATE " . $this->table . "_language SET $str_lang
                  			WHERE " . $this->id_field . " = '$row' AND language = '$language'";
                        Db::execute($query);
						$this->querys .='<br>' . $query . ';';
                        $gl_saved = $this->saved = true;
                    }
                }
            }
        }
        return $gl_saved;
    }
    function insert_checkboxes(&$post, $row, $field, $value)
    {
        // inserta els registres a la taula de molts a molts
        global $gl_insert_id;		
		
		$checked_id_field = isset($value['checked_id_field'])?$value['checked_id_field']:$this->id_field; // per haver escrit malament custumer
		
		$field = isset($value['checked_id_field2'])?$value['checked_id_field2']:$field; // aixi puc canviar el nom del camp com a sahara
		
        if ($row != '0')
        {
            $id = $row;
            $query = "DELETE FROM " . $value['checked_table'] . "
						WHERE " . $checked_id_field . "=" . $row;
            Db::execute($query);
			$this->querys .='<br>' . $query . ';';
        }
        else
        {
            // si es add_record necessitem l'insert_id
            $id = $gl_insert_id;
        }
        $query = "";
        if ($post[$row])
        {
            foreach($post[$row] as $checked_val)
            {
                $query .= "
						(" . R::id($checked_val,false,false) . "," . $id . "),";
            }
            $query = "
						INSERT INTO " . $value['checked_table'] . "
						(" . $field . ", " . $checked_id_field . ")
						VALUES " . substr($query, 0, -1) . ";";
            Db::execute($query);
			$this->querys .='<br>' . $query . ';';
        }
    }

    function add_record()
    {
        global $gl_message, $gl_insert_id, $gl_saved;
        Debug::add('Post', $this->post);
        if ($this->field_unique && $this->record_exists('0'))
        {
            $gl_message = $this->message_add_exists;
            $gl_saved = $this->saved = false;
        }
        else
        {
            $values = array();
            $fields = array();
            $this->get_values_fields($values, $fields);
            $query = "INSERT INTO " . $this->table . " (
        			" . implode(", ", $fields) . "
        			) values ("
             . implode(", ", $values) . ")";
            Db::execute($query);
			$this->querys .='<br>' . $query . ';';
            $gl_insert_id = $this->id = Db::insert_id();
            // un cop creat el registre, insertem la taula relacionada de checkboxes
            foreach($this->fields as $key => $cg)
            {
	            if (
		            isset( $this->post[ 'old_' . $key ][0] ) &&
		            ( $cg['type'] == 'checkboxes' ||
		              $cg['type'] == 'checkboxes_long' ) &&
		            ( $cg['checked_table'] != '' )
	            )
                {
                    $this->insert_checkboxes($this->post[$key], 0, $key, $cg);
                }
                elseif (isset($this->post['old_' . $key][0]) && ($cg['type'] == 'sublist'))
                {
                    NewRow::save_records($gl_insert_id, $key, $cg, $this->id_field, $this->action, $this->module);
                }
            }
            // un cop creat el registre, inserim a la taula relacionada d'idiomes
            if ($this->language_fields[0])
            {
                $this->add_record_languages();
            }
            $gl_message = $this->message_added;
            $gl_saved = $this->saved = true;

            $this->set_caller_select_add();

        }
    }

    private function set_caller_select_add() {
	    $caller_select_id = R::text_id( 'caller_select_id' );

	    if ( $caller_select_id === false )
		    return;

	    $config = $_SESSION ['caller_select_config'][ $this->id_field  ];

	    if ( ! $config )
		    return;

	    unset ( $config['add_new_link']);
	    // Als formularis nous forço a mostrar l'oopció que s'acaba d'entrar
	    $config['default_value'] = $this->id;
	    $id = $caller_select_id == '0'?'':$this->id;

	    $input  = new FormObject( $config, $id, 'form', $caller_select_id, $this );
	    $select = $input->get_input();

	    $javascript = "
            top.$('#" . $this->id_field . "_" . $caller_select_id . "').replaceWith ( '" . $select['val'] . "' ); 
            top.$('#old_" . $this->id_field . "_" . $caller_select_id . "').replaceWith ( '' );
        ";

	    print_javascript( $javascript );

    }

    private function set_caller_select_edit() {

    	if (!$this->saved) return;

	    $caller_select_id = R::text_id( 'caller_select_id' );

	    if ( $caller_select_id === false )
		    return;

	    $config = $_SESSION ['caller_select_config'][ $this->id_field  ];

	    if ( ! $config )
		    return;

	    $table = $config['select_table'];
	    $fields = $config['select_fields'];
	    $condition = $config['select_condition'];

	    $query = "SELECT $fields FROM $table WHERE " . $this->table . "." . $this->id_field . " = '" . $this->id . "'";

	    if ( $condition ) {
		    $query .= " AND $condition";
	    }

	    $rs = Db::get_row( $query );

	    $html = '';
	    array_shift($rs);

	    // S'haurà de definir el caption, tant en el section on hi ha el select com en el section on s'edita
        if (isset($this->caption['c_' . $this->name_no_prefix . '_format']))
        {
            $html .= vsprintf ($this->caption['c_' . $this->name_no_prefix . '_format'], $rs);
        }
        else
        {
            // si no en defineixo cap els camps els separo amb un guió i prou
            $html .= implode(" - ", $rs);
        }

	    $javascript = "
            top.$('#" . $this->id_field . "_" . $caller_select_id . " option:selected').text ( '" . $html . "' ); 
        ";

	    print_javascript( $javascript );

    }

    /**
     *
     * @access public
     * @return void
     */
    function add_record_languages()
    {
        global $gl_insert_id, $gl_languages;
        // haig de fer un insert per cada idioma
        $query = '';
        foreach ($gl_languages['public'] as $language)
        {
            $values = array();
            $fields = array();
            $this->get_values_fields_languages($values, $fields, $language);
            $query .= "("
             . implode(", ", $values) . ", '" . $gl_insert_id . "','" . $language . "'),";
        }
        $query = "INSERT INTO " . $this->table . "_language (
	     " . implode(", ", $fields) . ", " . $this->id_field . ", language
	     ) values " . substr($query, 0, -1);
        Db::execute($query);
		$this->querys .='<br>' . $query . ';';
        // $gl_insert_id = Db::insert_id();
        // $gl_message = $this->message_add;
    }
	
	// es fa servir desde add_record
    function get_values_fields(&$values, &$fields, $other_config = array())
    {

    	$fields_config =  $other_config?$other_config:$this->fields;

        // faig servir el old_key per descartar els fields que no son del tipus input
        // o sigui que per guardar el valor d'un camp ha de ser sempre un input, si interessa guardar-lo però que no es vegi pot ser un input i type=hidden
        foreach($fields_config as $key => $cg)
        {

        	if (($cg['override_save_value']!='') && !isset($cg['type'])) $cg['type']=''; // per guardar un valor nomès fent servir el override_save_value i res al _config
	        if (
		        ( $key != $this->id_field ) &&
		        ! in_array( $key, $this->language_fields ) &&
		        ( $cg['type'] != 'checkboxes' ) &&
		        ( $cg['type'] != 'checkboxes_long' ) &&
		        ( $cg['type'] != 'sublist' ) &&
		        ( isset( $this->post[ 'old_' . $key ][0] ) || $cg['override_save_value'] != '' ) &&
		        ( $cg['type'] != 'file' )
	        )
            {
                // per els checkbox, si no està definit vol dir que no esta seleccionat un checkbox
                if (($cg['type'] == 'checkbox') && (!isset($this->post[$key][0])))
                {
                    $this->post[$key][0] = '0';
                }
                if ($cg['override_save_value']!='')
                {
                    $values[] = $cg['override_save_value'];
                } elseif (($this->call_function[$key]))
                {
                    $values[] = call_user_func($this->call_function[$key], 0, 0);
                } elseif (function_exists('unformat_' . $cg['type']))
                {
                    $values[] = call_user_func ('unformat_' . $cg['type'], $this->post[$key][0], true, $key, 0, $cg);
                }
                else
                {
                    $values[] = Db::qstr($this->post[$key][0]);
                }
                $fields[] = $key;
            }
        }
    }

    function get_values_fields_languages(&$values, &$fields, $language, $other_language_fields = array())
    {

    	$language_fields =  $other_language_fields?$other_language_fields:$this->language_fields;

        global $gl_languages;
        foreach ($language_fields as $language_field)
        {
            if ($this->fields[$language_field]['type'] != 'file'){
				$post_language = isset($this->post[$language_field][0][$language])?$this->post[$language_field][0][$language]:$this->post[$language_field][0];
				$values[] = Db::qstr($post_language);
				$fields[] = $language_field;
			}
        }
    }
    function call($name, $parameters)
    {
        $this->function_name = $name;
        $this->function_parameters = explode(',', $parameters);
    }
    function get_call_rs(&$rs)
    {
        if ($this->function_name)
        {
            $parameters = array();
            foreach ($this->function_parameters as $param)
            {
                $parameters[] = $rs[$param];
            }
            $ret = call_user_func_array($this->function_name, $parameters);
            if (is_array($ret)) $rs = array_merge($rs, $ret);
        }
    }

    // on_save event
	public function on_save( Closure $return_function, $delete_results_fields = '' ) {
		$this->on_save_function      = $return_function;
		$this->delete_results_fields = $delete_results_fields;
	}

	private function fire_on_save() {

		if ( ! $this->on_save_function ) return;

		call_user_func_array($this->on_save_function, [$this->id, $this->action, $this->deleted_results, &$this]);

	}

	private function set_deleted_results( $query_ids ) {
		// TODO Falten els idiomes
		if ( ! $this->delete_results_fields ) return;

		$fields = $this->delete_results_fields;
		$ids    = implode( ',', $query_ids );
		$table = $this->table;
		$id_field = $this->id_field;

		$query = "SELECT $id_field,$fields FROM $table WHERE $id_field IN ($ids)";

		$this->deleted_results = Db::get_rows( $query );
	}
	// fi on save event

    function record_exists($id){

        $field_uniques = is_array ($this->field_unique)?$this->field_unique:array( '0' => $this->field_unique);


        $record_exists = false;
        foreach ( $field_uniques as $field_unique ) {
            $ret = $this->record_exists_field( $id, $field_unique );
            if ($ret) $record_exists = true;
        }

        return $record_exists;

    }
    function record_exists_field($id, $field_unique)
    {
        $field_unique_value = isset($this->post[$field_unique][$id])?$this->post[$field_unique][$id]:false;
	    $found_value = '';
	    $rs = [];
		$table = $this->table;
		
		// si el camp únic no és obligatori, si no té cap valor no comprovem si existeix i permetem que es pugui guardar
		if ( (!isset($this->fields[$field_unique]['required']) || $this->fields[$field_unique]['required']=='') && ($field_unique_value == '' || $field_unique_value == '0')) return false;

		Debug::add('Field unique value',$field_unique_value);
        // si hi ha un $field_unique, comprovem que no existeix ja un registre amb aquest valor
        if ($field_unique_value!==false)
        {
            if (in_array($field_unique, $this->language_fields)){
				// poden ser iguals el del mateix id
				$escaped_id = Db::qstr($id);
				foreach ($field_unique_value as $key=>$val){
					// si no te valor, no poso a la consulta, si es required ja no deixarà passar sense valor
					if ($val) {

						$val = Db::qstr($val);
						$query = "
							SELECT count(*) FROM ${table}_language 
							WHERE $field_unique = $val AND $this->id_field <> $escaped_id";

                        $rs = Db::get_first($query);
						Debug::add('Field unique $query',$query);
                        if ($rs) {
	                        $found_value .= $val . ', ';
                        }
					}
				}
                if ($found_value) $found_value = substr($found_value, 0 , -2);
			}
			else{
			
				$query = "SELECT count(*) FROM $table WHERE $field_unique = " . Db::qstr($field_unique_value) . " AND " . $this->id_field . " <> " . Db::qstr($id);
                $rs = Db::get_first($query);
				if ($rs) $found_value = $field_unique_value;
				Debug::add('Field unique $query',$query);
			}

            if ( $found_value ) {
                $this->message_add_exists .= '<div>' . sprintf( $this->messages['add_exists'], $this->caption[ 'c_' . $field_unique ] , $found_value ) . '</div>';

                if ( strpos( $this->action, 'save_rows_save_records' ) !== false ) {
                    $this->notify_ids [] = $id;
                }
            }



        }
        return !empty($found_value);
    }
    // guarda canvis en llistat de configuracio
    function save_records_config()
    {
        global $gl_saved;
        Debug::add('Post', $_POST);
        $gl_saved = $this->saved = false;
        // bucle cada fila, faig bucle amb el primer de la llista, encara que hi hagi varis camps per guardar, el primer ja em seveix per el bucle
        // faig el bucle amb el old_ , ja que aquests inputs son hidden, per tant em fa el bucle complert,
        // ja que si son checkboxes, nomès fa bucle per els seleccionats, i es salta els que no estan seleccionats

	    $row = key( $_POST[ $this->id_field ] );
        $str = '';
        $str_language = array();
        $query = '';
        // bucle cada camp de la fila i obtindre els seus valors
        foreach ($this->fields as $field => $value)
        {
            $post = isset($_POST[$field])?$_POST[$field]:null;
            // per els checkbox, si es null vol dir que no esta seleccionat un checkbox
            if (!isset($post[$row]))
            {
                $post[$row] = '0';
            }

            $post_old = isset($_POST["old_" . $field])?$_POST["old_" . $field]:'';
	        if (
		        isset( $post_old[ $row ] ) &&
		        ( $value['type'] != 'checkboxes' ) &&
		        ( $value['type'] != 'checkboxes_long' ) &&
		        ( $value['type'] != 'sublist' )
		        // de moment trec lo del _old, que guardi sempre i a pendre pel cul
		        // es que al no recarregar el formulari lo de old peta (amb el sistema de guardar amb frame)
		        // && ($post[$row]) != ($post_old[$row])
	        )
            {
                // subllistat del camp en tots els idiomes
                // REVISAR - s'ha de probar amb 2 camps si funciona, podria fallar l'ordre dels idiomes de cada un
                if (in_array($field, $this->language_fields))
                {
                    foreach($post[$row] as $language => $val)
                    {
                        if (!isset($str_language[$language])) $str_language[$language] = ''; // per no donar error undefined                        
                        $insert_value = Db::qstr($val);
                        $query = "UPDATE " . $this->table . "_language SET value = " . $insert_value . "
	            			WHERE name = '" . $field . "' AND language = '" . $language . "'";
                        Db::execute($query);
						$this->querys .='<br>' . $query . ';';
                    }
                }
                else
                {
                    if (isset($this->call_function[$field]))
                    {
                        $insert_value = call_user_func($this->call_function[$field], $row, $post[$row]);
                    }
                    else
                    {
                        if (function_exists('unformat_' . $value['type']))
                        {
                            $insert_value = call_user_func ('unformat_' . $value['type'], $post[$row], true, $field, $row, $value);
                        }
                        else
                        {
                            $insert_value = Db::qstr($post[$row]);
                        }
                    }
                    $query = "UPDATE " . $this->table . " SET value = " . $insert_value . "
	            			WHERE name = '" . $field . "'";
                    Db::execute($query);
					$this->querys .='<br>' . $query . ';';
                }
            }
        }
        $gl_saved = $this->saved = true;
        return $gl_saved;
    }

    function set_values_to($value, $function = false)
    {
        $query = "UPDATE " . $this->table . " SET " . $this->set_values_field . ' = ' . $value . " WHERE ";
        foreach ($_POST["selected"] as $key)
        {
            $id = R::id($key,false,false);
			$query .= $this->id_field . " = " . Db::qstr($id) . " OR ";
        }
        $query = substr($query, 0, -4);
        Db::execute($query);
		$this->querys .='<br>' . $query . ';';
    }

    function bin_selected()
    {
        $query_ids = array();
        foreach ($this->post["selected"] as $key)
        {
            $query_ids[$key] = R::id($key,false,false);
        }
        $this->bin_ids($query_ids);
    }
	/**
	 * @param $query_ids
	 */
	function bin_ids($query_ids) {

		$this->check_related_tables( $query_ids, 'bin' );

	    if ($query_ids)
        {
            // bin els registres
	        $query = "UPDATE " . $this->table . " 
	                        SET bin = 1
	                        WHERE " . $this->id_field . "
							IN (" . implode(',', $query_ids) . ")";
            Db::execute($query);
			$this->querys .='<br>' . $query . ';';
        }

		$this->notify( $this->notify_ids );

	}

    function delete_selected()
    {
        $query_ids = array();
        foreach ($this->post["selected"] as $key)
        {
            $query_ids[$key] = R::id($key,false,false);
        }
        $this->delete_ids($query_ids);
    }

    function delete_record()
    {
        $query_ids = array();

	    $id = R::id( $this->post[ $this->id_field ], false, false );
        $query_ids[$id] = $id;
		
        $this->delete_ids($query_ids);
    }

	/**
	 * @param $query_ids
	 */
	function delete_ids($query_ids)
    {

	    $this->check_related_tables( $query_ids, 'delete' );


	    if ($query_ids)
        {
            $this->set_deleted_results($query_ids);

        	// borro tots els registres de imatges relacionats amb l'apartat i les imatges
            if ($this->has_images)
            {
                $this->uploads['image']->delete_all_files();
                if ($this->uploads['image']->deleted_files)
                {
                    foreach ($this->uploads['image']->deleted_files as $image_id => $file)
                    {
                        $image_ext = strrchr(substr($file['store_name'], -5, 5), '.');
                        unlink(IMAGES_DIR . $image_id . "_medium" . $image_ext);
                        unlink(IMAGES_DIR . $image_id . "_details" . $image_ext);
                        unlink(IMAGES_DIR . $image_id . "_thumb" . $image_ext);
                        unlink(IMAGES_DIR . $image_id . "_admin_thumb" . $image_ext);
                    }
                }
            }
            // borro tots els arxius relacionats amb l'apartat
            UploadFiles::gl_upload_delete_all_files($this);
            // borro tots els registres de idiomes relacionats amb l'apartat
            if ($this->language_fields[0])
            {
                $query = 'DELETE FROM ' . $this->table . '_language
							WHERE ' . $this->id_field . '
							IN (' . implode(',', $query_ids) . ')';
                Db::execute($query);
				$this->querys .='<br>' . $query . ';';
            }
            // borro els registres
            $query = 'DELETE FROM ' . $this->table . '
							WHERE ' . $this->id_field . '
							IN (' . implode(',', $query_ids) . ')';
			// això fa falta per les imatges, no se perque un dia les va borrar totes, així m'asseguro que nomès borra les del mateix registre ($parent_id)
			if ($this->condition) {
				$query = $query . ' AND ' . $this->condition;
			}
            Db::execute($query);
			$this->querys .='<br>' . $query . ';';
        }

	    $this->notify( $this->notify_ids );

    }

	/**
	 * Comprova els registres de les taules relacionades
	 * @param $query_ids
	 * @param $from string Des d'on es crida, si quan s'envia a la paperera o quan es borra definitivament
	 */
	function check_related_tables( &$query_ids, $from ) {

		if (!$query_ids) return;

		$notify_ids = &$this->notify_ids;
		// borro o no tots els registres d'altres taules relacionats amb l'apartat
		// miro també quins ids no puc borrar, quan action = notify
		if ( $this->related_tables ) {


			// Api, volen poder esborrar igualment
			foreach ( $this->related_tables as $rel ) {
				if ( $rel['action'] == 'notify_on_bin_only' && $from == 'delete' ) {
					$query_related = 'SELECT DISTINCT ' . $this->id_field . '
							FROM ' . $rel['table'] . '
							WHERE ' . $this->id_field . ' IN (' . implode( ',', $query_ids ) . ')';
					$results       = Db::get_rows( $query_related );
					foreach ( $results as $rs ) {
						unset( $query_ids[ $rs[ $this->id_field ] ] );
						$notify_ids[] = $rs[ $this->id_field ];
					}
				}
			}

			// primer faig el bucle pel notify així descarto els ids que no puc borrar
			foreach ( $this->related_tables as $rel ) {
				if ( $rel['action'] == 'notify' ) {
					$query_related = 'SELECT DISTINCT ' . $this->id_field . '
							FROM ' . $rel['table'] . '
							WHERE ' . $this->id_field . ' IN (' . implode( ',', $query_ids ) . ')';
					$results       = Db::get_rows( $query_related );
					foreach ( $results as $rs ) {
						unset( $query_ids[ $rs[ $this->id_field ] ] );
						$notify_ids[] = $rs[ $this->id_field ];
					}
				}
			}

			// només ho faig quan s'elimina definitivament
			if ( $from == 'delete' ) {
				// faig el bucle pel delete
				foreach ( $this->related_tables as $rel ) {
					if ( $rel['action'] == 'delete' ) {
						$query = 'DELETE FROM ' . $rel['table'] . '
							WHERE ' . $this->id_field . '
							IN (' . implode( ',', $query_ids ) . ')';
						Db::execute( $query );
						$this->querys .= '<br>' . $query . ';';
					}
				}
			}
		}
	}

	/**
	 * @param $notify_ids
	 * @param string $message
	 * @param bool $reload_page
	 * Notifica si hi ha cap registre relacionat
	 */
	function notify($notify_ids, $message='', $reload_page = true){

		if ($notify_ids)
			{
                if ( $reload_page ) {
                    $url_query = parse_url( $_SERVER["HTTP_REFERER"] );
                    $url_query = $url_query['query'];
                    $_GET      = array();
                    parse_str( $url_query, $_GET );
                    $js                  = "top.window.location = '?marked_ids=" . implode( ',', $notify_ids ) . get_all_get_params( array( 'marked_ids' ), '&', '', true ) . "';";
                    $_SESSION['message'] = $message ? $message : $this->messages['related_not_deleted'];
                    print_javascript( $js );
                    die();
                }
                else {

                    $js = 'top.$("#contingut .listItemMarked").removeClass("listItemMarked");';
                    foreach ( $notify_ids as $notify_id ) {
                        $js .= 'top.$("#list_row_" + ' . $notify_id . ').addClass("listItemMarked");';
                    }
                    print_javascript( $js );

                }
			}
	}

    function bin_record()
    {
        $query_ids = array();

	    $id = R::id( $this->post[ $this->id_field ], false, false );
        $query_ids[$id] = $id;

        $this->bin_ids($query_ids);
	    /*



        $query = "UPDATE " . $this->table . " SET bin=1
					WHERE " . $this->id_field . " = " . Db::qstr($id);
        Db::execute($query);
		$this->querys .='<br>' . $query . ';';*/
    }
    // Comprobo de no passar el límit del post, per que sino hem peten totes les variables del POST i es un putu desastre
    // es pot cridar:  SaveRows::is_post_too_big()
    static public function is_post_too_big()
    {
        if (!isset($_SERVER['CONTENT_LENGTH'])) return false;
        global $gl_message, $gl_messages;

        $post_max_size = ini_get('post_max_size');
        $mul = substr($post_max_size, -1);
        $mul = ($mul == 'M' ? 1048576 : ($mul == 'K' ? 1024 : ($mul == 'G' ? 1073741824 : 1)));
        if ($_SERVER['CONTENT_LENGTH'] > $mul * (int)$post_max_size)
        {
            $gl_message = $gl_messages['post_max_size'] . ($mul * (int)$post_max_size / 1048576) . ' Mb';
            return true;
        }
        else
        {
            return false;
        }
    }
	// paso un results a post per guardar
	function convert_results($results){
		foreach ($results as $rs){
			convert_rs($rs);
		}
	}
	// paso un results a post per guardar
	function convert_rs($rs){
		$id = isset($rs[$this->id_field])?$rs[$this->id_field]:0;
		foreach ($rs as $key=>$val){
			$this->post[$key][$id] = $val;
			$this->post['old_' .$key][$id] = $val;
		}
	}
	// es fa servir per canviar el valor d'un camp abans de guardar
	function set_value($field,$val,$lang='',$all_langs=false){
		
		if ($all_langs){
			foreach ($GLOBALS['gl_languages']['public'] as $language) {
				$this->post[$field][$this->id][$language] = $val;
			}
			return;
		}
		elseif ($lang)	{
			$this->post[$field][$this->id][$lang] = $val;
		}
		else {
			$this->post[$field][$this->id] = $val;
		}
	}
	// es fa servir per obtindre el valor d'un camp
	/**
	 * @param $field
	 * @param string $lang
	 *
	 * @return mixed
	 */
	function get_value($field,$lang='',$id = 0){

		// A add_record per que funcioni un cop guarda, id ha de ser sempre 0
		// TODO-i  A copy_record haurà de ser el id anterior
        if (!$id) $id = $this->action == 'add_record'?0:$this->id;

		if (!$lang){
			$ret = isset($this->post[$field][$id])?$this->post[$field][$id]:false;

			// comprovo si es un checkbox o checkboxes que hi hagi el old,
			// així puc diferenciar entre isset o false
			if ($ret === false && isset($this->post['old_' . $field][$id]))
				$ret = 0;

			return $ret;
		}
		else{
			return isset($this->post[$field][$id][$lang])?$this->post[$field][$id][$lang]:false;
		}
	}

	/**
	 * @param array $fields Opcional per poder fer amb diferents camps que els del config, per contact es necessari ja que hi han molts formularis que s'han fet a mida amb javascripts que no corresponen al _config
	 * @param bool|false $require_only_one Per poder fer que almenys hi hagi un dels valaors per poder passa, també per contact
	 *
	 * @return bool
	 */
	function has_requireds($fields = array(), $require_only_one = false) {

		if ( ! $fields ) {
			$fields = array();
			foreach ( $this->fields as $field => $val ) {
				if ( isset( $this->fields[ $field ]['required'] ) && $this->fields[ $field ]['required'] == '1' ) {
					$fields [] = $field;
				}
			}

		}

		$val = '';

		Debug::p( [
			'Camps requerits', $fields,
		], 'shopCustumer');

		$at_least_one_missing = false;
		foreach ( $fields as $field ) {
			$val = $this->get_value( $field );

			// Si val es fals es que no està posat al formulari, per tant ignoro que sigui required
			if ( $val !== false ) {
				$val = trim( $val );
				// Si es require_only_one, nomes que 1 tingui valor es retorna true
				if ( $val && $require_only_one ) {
					return true;
				}

				// Si NO es require_only_one, nomes que 1 no tingui valor es retorna false
				if ( ! $val && ! $require_only_one ) {
					$at_least_one_missing = true;
					break;
				}
			}
		}
		Debug::p( [
			[ 'Requered only one', $require_only_one ],
			[ 'At least one missing', $at_least_one_missing ]
		], 'shopCustumer');

		// Si require_only_one arriba aquí es que no ha trobat cap $val, per tant es false
		if ( $require_only_one ) {
			return false;
		}



		// Si one_val_missing retorno false
		if ( $at_least_one_missing ) {
			// TODO-i Fer que es mostri un javascript per notificar que no s'ha guardat
			global $gl_message, $gl_messages;
			$gl_message = $gl_messages['form_not_sent'];

			Debug::p( [
				'Missatge requireds no enviat', $gl_message
			], 'shopCustumer');

			return false;
		} // Sino es que tots els valors tenen algún valor
		else {
			return true;
		}
	}
}