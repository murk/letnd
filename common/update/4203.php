<?php
// no es poden posar ; entre mig de la consulta ja que parteix les consultes per el ; a no ser que fem servir $update_sql en array()
	
if (is_table("product__family") && !is_table("product__family_image")){	
	$update_sql[]="CREATE TABLE `product__family_image` (
		`image_id` INT(11) NOT NULL AUTO_INCREMENT,
		`family_id` INT(11) NOT NULL DEFAULT '0',
		`name` VARCHAR(100) NOT NULL DEFAULT '0',
		`size` INT(11) NOT NULL DEFAULT '0',
		`ordre` INT(11) NOT NULL DEFAULT '0',
		`main_image` INT(1) NOT NULL DEFAULT '0',
		PRIMARY KEY (`image_id`)
	)
	COLLATE='utf8_spanish2_ci'
	ENGINE=InnoDB;";	
	$update_sql[]="INSERT INTO `all__menu` (`menu_id`, `menu_group`, `tool`, `tool_section`, `action`, `parent_id`, `toolmode_id`, `variable`, `ordre`) VALUES (20014, 'product', 'product', 'family', 'list_records_images', 20011, 1, 'PRODUCT_MENU_FAMILY_NEW', 1);";
	
	// creo el directori per guardar les imatges
	$client_dir = DOCUMENT_ROOT . '/' . $_SESSION['client_dir'];
	$dir=$client_dir . '/product/family/';
	if (!is_dir($dir)){
		mkdir ($dir, 0776);
	}
	$dir=$client_dir . '/product/family/images/';
	if (!is_dir($dir)){
		mkdir ($dir, 0776);
	}
}
?>