<?php
/* 25-4-18
Afegir camp is_calendar_public a les families

*/


if ( is_table( 'product__family' ) && ! is_field( 'product__family', 'is_calendar_public' ) ) {

	$update_sql[] = "ALTER TABLE `product__family`
	ADD COLUMN `is_calendar_public` TINYINT(1) NOT NULL DEFAULT '1' AFTER `destacat`;
";

} else {
	if ( is_table( 'product__family' ) ) {
		trigger_error( 'Ja existeixen el camp is_calendar_public' );
	}
}

?>