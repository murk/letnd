<!-- period selection -->

<p class="periodevisu" align="center">
	{'menu_periode'|translate:"<strong> $date_litteral </strong>"} 
	&nbsp;
	
</p>

<p class="choixperiode">
	(<a href="{$url_period}&amp;period=1&menu_id={$menu_id}">{'menu_jour'|translate}</a> |
	<a href="{$url_period}&amp;period=2&menu_id={$menu_id}">{'menu_semaine'|translate}</a> |
	<a href="{$url_period}&amp;period=3&menu_id={$menu_id}">{'menu_mois'|translate}</a> |
	<a href="{$url_period}&amp;period=4&menu_id={$menu_id}">{'menu_annee'|translate}</a>
	)
</p>
