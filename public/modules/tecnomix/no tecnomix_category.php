<?
/**
 * TecnomixCategory
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2010
 * @version $Id$
 * @access public
 */
class TecnomixCategory extends Module{
	var $new_link;
	function __construct(){
		parent::__construct();
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	/* funció que es crida per fer una acció del modul automatica o desde un altre modul ( list_records, show_form ) etc...*/
	function do_action($action = ''){
		switch ($this->parent){		
			case 'TecnomixNewcategory':				
			break;	
			case 'TecnomixInstalaciocategory':
				$this->new_link = '/instalacio/new/category_id/';
			break;	
			case 'TecnomixEquipcategory':
				$this->new_link = '/equip/new/category_id/';
			break;	
			case 'TecnomixProjectecategory':	
			break;
		}
		return parent::do_action($action);
	}
	function get_records()
	{
		$listing = new ListRecords($this);
		$listing->condition = "parent_module = '" . $this->parent . "'";
		$listing->has_bin = false;
		$listing->order_by = 'ordre asc, category asc';
		$listing->paginate = false;
		$listing->list_records(false);
		foreach ($listing->results as $key => $val){
			$rs = &$listing->results[$key];
			$rs['new_link'] = '/' . $GLOBALS['gl_language'] . $this->new_link . $rs['category_id'];
		}
		return $listing->parse_template();
	}
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{
	    $show = new ShowForm($this);
		$show->has_bin = false;
	    return $show->show_form();
	}
}
?>