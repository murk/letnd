<?php

$family_id =  isset($vars['family_id'])?$vars['family_id']:0;
$parent_id =  isset($vars['parent_id'])?$vars['parent_id']:0;

$product_id = isset($vars['product_id'])?$vars['product_id']:'0';
$show_whole_family = isset($vars['show_whole_family'])?$vars['show_whole_family']:'0';

$vars['is_lassdive_complement'] = $family_id == '10000';
$vars['is_related_products'] = $show_whole_family ? true : false;


$module = Module::load('product','product');
$module->parent = 'Block';

if ( $show_whole_family ) {

	$query = "SELECT family_id, subfamily_id, subsubfamily_id FROM product__product WHERE product_id = '$product_id'";
	$rs    = Db::get_row( $query );

	$family_id = $rs['family_id'];
	$subfamily_id = $rs['subfamily_id'];
	$subsubfamily_id = $rs['subsubfamily_id'];


	$module->condition =
		"AND product__product.product_id IN (
	
		SELECT product2_id FROM product__product_related WHERE product_id IN
		 ( 
				SELECT product_id 
				FROM product__product 
				WHERE  family_id = '$family_id'
				AND subfamily_id = '$subfamily_id'
				AND subsubfamily_id = '$subsubfamily_id'
		)
		ORDER BY ordre
		
	)";

}
else {
	$family_condition = $parent_id ? 'subfamily_id' : 'subfamily_id= 0 AND family_id';
	$module->condition =
		"AND $family_condition = '$family_id'";
}


$loop = $module->do_action('get_records');

foreach ( $loop as &$rs ) {
	$product_id = $rs['product_id'];
	if (isset($_SESSION['product']['cart_vars'][$product_id]['extra_vars'])) {
		$rs['reserva_items_index'] = count( $_SESSION['product']['cart_vars'][$product_id]['extra_vars'] ) + 1;
	}
	else {
		$rs['reserva_items_index'] = 1;
	}

	if (!empty($_SESSION['product']['is_shop_app'])) {
		$rs_calendar = Db::get_row( "SELECT has_calendar FROM product__family WHERE family_id = " . $rs['real_family_id'] );

		$rs['family_has_calendar'] = $rs_calendar['has_calendar'] == 1;
	}

}

$vars += $module->caption;

if(!$loop){
	$show_block = false;
}

?>