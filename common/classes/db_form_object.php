<?php
/**
 * Clase FormObject
 *
 * @author Joan Sanahuja <sanahuja@gmail.com>
 * @version 1.0
 */

/**
 * Classe per crear objectes de formulari tipus desplegable
 * (agafa els valors del desplegable de una taula relacionada)<br>
 * Aquesta clase queda integrada dins la clase ShowForm a partir de la funció create_select<br><br>
 * <b>Exemple</b><br>
 * $show->create_select("category_id");<br>
 * $show->selects["category_id"]->select_table = "inmo__category, inmo__category_language";<br>
 * $show->selects["category_id"]->select_fields = "category, inmo__category.category_id";<br>
 * $show->selects["category_id"]->condition = "inmo__category.category_id=inmo__category_language.category_id AND language='" . $gl_language . "' ORDER BY ordre ASC";
 *
 * @author Joan Sanahuja <sanahuja@gmail.com>
 * @version 1.0
 */
class FormObject Extends ModuleBase
{
    /**
     * Id de registre pel qyue creem el input, si es un formulari nou es igual a 0
     *
     * @access public
     * @var int
     */
    var $id;
    /**
     * Nom del camp de la BBDD
     *
     * @access public
     * @var string
     */
    var $name;
    /**
     * Nom del camp de la BBDD sense afegir mai prefix, tots els captions van amb aquesta, sino en un sublist hauria de fer tots els captions duplicats amb el prefix
     *
     * @access public
     * @var string
     */
    var $name_no_prefix;
    /**
     * Nom del objecte del formulari (el mateix nom del camp de la BBDD)
     *
     * @access public
     * @var string
     */
    var $input_name;
    /**
     * Idioma de l'objecte del formulari quan es un input de multiples idioomes
     *
     * @access public
     * @var string
     */
    var $input_language;
    /**
     * Guarda el valor del camp
     *
     * @access private
     * @var array
     */
    var $value;
    /**
     * Guarda el valor del camp
     *
     * @access private
     * @var array
     */
    var $original_value;
    /**
     * Per desabilitar el desplegable (a vegades el desplegable ha d'estar desabilitat fins que l'habilitem des d'una funció javascript
     *
     * @access public
     * @var boolean
     */
    var $disabled;
    /**
     * Guarda tota la configuració del input
     *
     * @access private
     * @var boolean
     */
    var $is_input;
    /**
     * Serveix per saber la classe que crida aquesta
     * Te el valor 'form' si estem fent servir la clase show_form
     * Te el valor 'list' si estem fent servir la clase list_records
     *
     * @access private
     * @var boolean
     */
    var $list_show_type;
    /**
     * Mida de l'input text
     *
     * @access public
     * @var string
     */
    var $text_size;
    /**
     * Màxim caracters de l'input text
     *
     * @access public
     * @var string
     */
    var $text_maxlength;

    /**
     * Columnes textarea
     *
     * @access public
     * @var string
     */

    var $textarea_cols;
    /**
     * Ample textarea
     *
     * @access public
     * @var string
     */
    var $textarea_rows;
    /**
     * Text que es mostra en un select ej. 'escull una opció del desplegable'
     *
     * @access public
     * @var string
     */
    var $select_caption;
    /**
     * Mida select per tipus llista
     *
     * @access public
     * @var string
     */
    var $select_size;
    /**
     * Multiples seleccions per select tipus llista
     *
     * @access public
     * @var string
     */
    var $select_multiple;
    /**
     * Per un checkbox si està seleccionat
     *
     * @access public
     * @var string
     */
    // var $checkbox_checked;
    /**
     * Serveix per deixar marcats tots els checkboxs (per exemple quan hi han arxius adjunts, queden tots marcats i el que demarquem al enviar el formulari es borra)
     *
     * @access public
     * @var boolean
     */
    var $checked;
    /**
     * Taula que guarda quins elements del checkbox estan marcats
     *
     * @access public
     * @var boolean
     */
    var $checked_table;
    /**
     * Camps de la consulta separats per comes (crec que ha de ser el mateix que $fields, per tant poder es pot suprimir aquesta variable)
     *
     * @access public
     * @var string
     */
    var $checked_fields;
    /**
     * Condició dels elements del checkbox estan marcats
     *
     * @access public
     * @var string
     */
    var $checked_condition;
    /**  per haver escrit malament custumer
     *  per haver escrit malament custumer
     *  per haver escrit malament custumer
     * Condició dels elements del checkbox estan marcats
     *
     * @access public
     * @var string
     */
    var $checked_id_field;
    var $checked_id_field2;
    /**
     * Taula de la BBDD on es troben el llistat d'elements del desplegable
     *
     * @access public
     * @var string
     */
    /**
     * Taula de la BBDD on es troben el llistat d'elements del desplegable
     *
     * @access public
     * @var string
     */
    var $select_table;
    /**
     * Camps de la Taula de la BBDD on es troben el llistat d'elements del desplegable
     *
     * @access public
     * @var string
     */
    var $select_fields;
    /**
     * Condició per crear la consulta
     *
     * @access public
     * @var array
     */
    var $condition;
    /**
     * Ordre per crear la consulta
     *
     * @access public
     * @var array
     */
    var $order_by;
    /**
     * Funció de javascript executada per l'event 'onchange'
     *
     * @access public
     * @var string
     */
    var $onchange;
    /**
     * tipus de input
     * 'text', 'textarea', 'int', 'decimal', 'decimal_null', 'date', 'time', 'minutes', 'select', 'radio', 'checkbox', 'html', 'htmlbasic'
     *
     * @access public
     * @var string
     */
    var $type;
    /**
     * Clase de l'input
     *
     * @access public
     * @var string
     */
    var $class;
    /**
     * Javascript de l'input Ex. 'onclick="call(a,b);"
     *
     * @access public
     * @var string
     */
    var $javascript;
    /**
     * Altres Atributs html de l'input
     *
     * @access public
     * @var string
     */
    var $attributes;
    /**
     * Tipus d'arxius que es poden pujar en un file
     *
     * @access public
     * @var string
     */
    var $accept;
    /**
     * Posa automaticament els add_dots a tots el inputs text i html
     *
     * @access public
     * @var string
     */
    var $add_dots;
    /**
     * Al tipus files mostra o no els arxius per cada idioma
     *
     * @access public
     * @var string
     */
    var $show_langs;
    /**
     * Tota la configuració de l'objecte
     *
     * @access public
     * @var array
     */
    var $object_cf;

    function __construct($cg = '', $value = '', $list_show_type = false, $id = '', &$module = false)
	{
		if ($module) $this->link_module($module); // Nomes es crida des d'aquí (dels helpers, i newsletter 1) sense mòdul, per tant no cal que estigui linkat al mòdul quen es fa serveir per crear un select o radio sol
		global $gl_site_part, $gl_write, $gl_page;

        if ($cg)
        {
            $this->id = $id;
            $this->object_cf = $cg;
            $this->name = $cg['name'];
	        $this->name_no_prefix = ! empty ( $cg['name_no_prefix'] ) ? $cg['name_no_prefix'] : $this->name;
            $this->input_name = $cg['input_name'];
            $this->input_language = $cg['input_language'];
			
			$input_id = strstr($this->action, "show_form_copy")?0:$this->id;
		
            $this->input_id = $cg['name'] . '_' . $input_id . ($cg['input_language']?'_'.$cg['input_language']:'');
            $this->value = $this->original_value = $value;
            $this->default_value = $cg['default_value'];
            $this->disabled = $cg['enabled'] == '0';
            // es un input si:
            // - així ho diu la configuració
            // - si l'accio no es show_record (show_record serveix per mostrar el registre, sense editar, perque un registre sigui editable ha de ser show_form_new o show_form_edit
            // - si te permisos d'escritura o es public ( quan es public no existeixen els permisos, no està fet )
            $this->is_input = ($cg[$list_show_type . '_' . $gl_site_part] == 'input') &&
								(!strstr($this->action, "show_record")) &&
								($this->action!="get_record") &&  // a V3 no existirà el strstr ja que no hi ha action_add
								($gl_write || $gl_site_part == 'public') &&
								($gl_page->template != 'print.tpl');
            $this->list_show_type = $list_show_type;
            $this->is_record = $this->type = $cg['type'];

            $this->text_size = $cg['text_size'];
            $this->text_maxlength = $cg['text_maxlength']; // la V. $cg, es el config, l'array que es genera
            $this->textarea_cols = $cg['textarea_cols'];
            $this->textarea_rows = $cg['textarea_rows'];

            $this->select_caption = $cg['select_caption'];
            $this->select_size = $cg['select_size'];
            $this->checked = isset($cg['checked'])?$cg['checked']:false;
            $this->checked_table = isset($cg['checked_table'])?$cg['checked_table']:false;
            $this->checked_fields = isset($cg['checked_fields'])?$cg['checked_fields']:false;
            $this->checked_condition = isset($cg['checked_condition'])?$cg['checked_condition']:false;
            $this->checked_id_field = isset($cg['checked_id_field'])?$cg['checked_id_field']:$this->id_field;
            $this->checked_id_field2 = isset($cg['checked_id_field2'])?$cg['checked_id_field2']:$this->name_no_prefix;
            $this->select_table = $cg['select_table'];
            $this->select_fields = $cg['select_fields'];
            $this->condition = $cg['select_condition'];
            $this->attributes = isset($cg['attributes'])?$cg['attributes']:false;
            $this->accept = isset($cg['accept'])?$cg['accept']:false;

            $this->class = $cg['class'];
            $this->javascript = $cg['javascript'];
			
            $this->show_langs = isset($cg['show_langs'])?$cg['show_langs']:true; // per defecte true
        }
		if ($module){
			$this->add_dots = $module->add_dots;
		}
    }

    /**
     * Obté els items de l'objecte de formulari de la BBDD
     *
     * @access private
     * @return array Resultats per l'objecte del formulari en un array
     */
    function get_results($table = '', $fields = '', $condition = '', $input_language='')
    {
        //if (!$this->id) return array();//  dona error al crear per exemple nova noticia, no surten les categories llistades. HO VAIG POSAR PER QUE DONAVA ERROR EN EL FORM DE UN FILE ( HE POSAT L'IF AL CRIDAR LA FUNCIO DESDE GET_FILE
		$table = $table?$table:$this->select_table;
        $fields = $fields?$fields:$this->select_fields;
        $condition = $condition?$condition:$this->condition;
        $input_language = $input_language?$input_language:$this->input_language;

        $query = "SELECT " . $fields . " FROM " . $table;

        if ($condition) $query .= " WHERE " . $condition;
        if ($input_language) $query .= ($condition?" AND":" WHERE") . " language='" . $input_language . "'";
	    if ($this->order_by) $query .= $this->order_by;

        Debug::add('Query form_object ' . $this->name , $query);
		$results = Db::get_rows_array($query);
        return $results;
    }
    /**
     *
     * @access public
     * @return void
     */
    function get_result()
    {
        if (!$this->value) return ''; // per form new no hi ha cap valor per id

        $name_parts = explode(',', $this->select_fields);
        $id_field = array_shift($name_parts);
        $query = 'SELECT ' . implode(',', $name_parts) . '
				FROM ' . $this->select_table . '
				WHERE ' . $id_field . "='" . $this->value . "'";
        if ($this->condition) $query .= " AND " . $this->condition;
		debug::add('Query get_result', $query);
        $rs = Db::get_row($query);
		
		if (!$rs) return '';

        if (isset($this->caption['c_' . $this->name_no_prefix . '_format']))
        {
            $html = vsprintf ($this->caption['c_' . $this->name_no_prefix . '_format'], $rs);
        }
        else
        {
            $html = implode(" - ", $rs);
        }
        return $html;
    }

    /**
     * Crea un delsplegable
     *
     * @access public
     * @return string Desplegable complert en HTML
     */
    function get_select()
    {
        $html = '';
        if ($this->select_table)
        {
            if ($this->is_input)
            {
                $results = $this->get_results();
            }
            else
            {
                return $this->get_result();
            }
        }
        else
        {
            if ($this->is_input)
            {
				// per crear un select a partir d'un array directe de bbdd
				// normalment per select creat a partir de la funcio get_select
				if (is_array($this->select_fields)) {
					
					$results = $this->select_fields;
					
				}
				else{
					$fields = explode(',', $this->select_fields);
					foreach($fields as $key => $value)
					{
						$results[$key][0] = $value;
						// primer poso el caption tipus "c_room_1" si no "c_1",
						// així si room te els valors 0,1,2,3 i elevator te els valors 0,1
						// puc posar captions diferents
						$results[$key][1] = isset($this->caption['c_' . $this->name_no_prefix . '_' . $value])?$this->caption['c_' . $this->name_no_prefix . '_' . $value]:(
						isset($this->caption['c_' . $value])?$this->caption['c_' . $value]:$value
						);
					}
				}
            }
            else
            {	
	            $ret = isset($this->caption['c_' . $this->name_no_prefix . '_' . $this->value])?
					$this->caption['c_' . $this->name_no_prefix . '_' . $this->value]:
					(isset($this->caption['c_' . $this->value])?
						$this->caption['c_' . $this->value]:
						$this->value);
				return isset($this->value)?$ret:'';
            }
        }
		debug::add('Select name',$this->name);
		debug::add('Select results',$results);
        if ($this->select_caption) $html .= '<option value="null">' . $this->select_caption . '</option>';
        if ($results)
        {
            $conta = 0;
            foreach ($results as $rs)
            {
                $selected = $this->value === $rs[0]?' selected="selected"':'';
                // si no hi ha cap valor passo de tot i selecciono sempre el primer (per els tipus list)
                if ($conta == 0 && $this->value == '' && !$this->select_caption) $selected = ' selected="selected"';

                $html .= '<option value="' . $rs[0] . '"' . $selected . '>';
                // **************** Desplegables mostrant 2 camps o més
                // si hem definit un caption per el option del select li sumem al final del valor
                // nomès miro que estigui definit el primer, per tant tots els captions dels camps que vull que surtin al desplegable han d'estar definits, si no en defineixo cap els camps els separo amb un guió i prou
                // ej. quan vull posar el regal i el preu del regal a continuació:
                // faig un caption que sigui c_regal_option ' (' , i un altre que sigui c_preu_option = ')',
                // això hem mostrarà: regal exemple (130)
                $rubish = array_shift($rs);
                if (isset($this->caption['c_' . $this->name_no_prefix . '_format']))
                {
                    $html .= vsprintf ($this->caption['c_' . $this->name_no_prefix . '_format'], $rs);
                }
                else
                {
                    // si no en defineixo cap els camps els separo amb un guió i prou
                    $html .= implode(" - ", $rs);
                }
                $html .= '</option>';
                $conta++;
            }
        }
        $onchange = '';
        $disabled = '';
        $class = $this->class?(' class="' . $this->class . '"'):'';
        $attributes = $this->attributes?(' ' . $this->class):'';
        if ($this->onchange) $onchange = ' onChange="' . $this->onchange . '"';
        if ($this->disabled) $disabled = ' disabled';
        $javascript = $this->javascript?' ' . $this->javascript:'';
        $select_size = $this->select_size?' size="' . $this->select_size . '"':'';
        $html = '<select id="'.$this->input_id.'" name="' . $this->input_name . '"' . $javascript . $select_size . $onchange . $disabled . $class. $attributes . '>' . $html . '</select>';


	    if ( ! empty( $this->object_cf['add_new_link'] ) ) {

		    $caption = $this->list_show_type == 'form' ? $this->object_cf['add_new_link_caption'] : '+';

		    $html .= '<a title="' . $this->object_cf['add_new_link_caption'] . '" class="add_new_link" href="javascript:showPopWin(\'' . $this->object_cf['add_new_link'] . '&template=window&caller_select_id=' . $this->id . '\',700,480,false,true);">' . $caption . '</a>';

		    $_SESSION ['caller_select_config'][ $this->name_no_prefix ] = $this->object_cf;

		    // Ha d'haver add_new_link per poder editar
		    // només edito als formularis, per editar al llistat haig de fer una icona sino seria massa llarg
		    if ( ! empty( $this->object_cf['add_edit_link'] ) && $this->list_show_type == 'form' ) {

			    // $caption = $this->list_show_type == 'form' ? $this->object_cf['add_new_link_caption'] : '+';
			    $caption = $this->caption['c_edit'];

			    $html .= '<a title="' . $this->caption['c_edit'] . '" class="add_new_link" href="javascript:form_object_select_edit(\'' . $this->input_id . '\', \'' . $this->object_cf['add_edit_link'] . '&action=show_form_edit&template=window&caller_select_id=' . $this->id . '\', \'' . $this->name_no_prefix . '\');">' . $caption . '</a>';

			    $_SESSION ['caller_select_config'][ $this->name_no_prefix ] = $this->object_cf;
		    }
	    }



        return $html;
    }

	/**
	 * Crea un input text que actua com un desplegable, és per quan és massa llarg
	 *
	 * @access public
	 * @return string Desplegable complert en HTML
	 */
	function get_select_long()
	{
		$html = '';
		$caption = $this->select_caption;
		if ($this->select_table)
		{

			$caption  = trim ($this->get_result());

			if ($this->is_input)
			{
				$html = '<input id="' . $this->input_id .'" name="' . $this->input_name . '" type="hidden" value="' . htmlentities($this->value) . '" />';
			}
			else
			{
				return $html;
			}
		}
		else {
			trigger_error('Falta definir select_table', E_USER_ERROR);
		}
		// Debug::p( $html );
		// propago paràmetres igual que faig amb show_record_link a db_base_list_show, si no no funcionaria quan no hi hagi menu_id
		$params = get_all_get_params(array('action', $this->id_field, 'page'.$this->module->name, 'tool', 'tool_section', 'menu_id', 'id'),'&','',true);

		$html .= '<label class="active sufix select_long"><input id="' . $this->input_id . '_add" type="text" value="' . $caption . '"><span>...</span></label>';
		$GLOBALS['gl_page']->javascript .=
			"
			add_select_long ('". $this->tool ."', '". $this->tool_section ."', '". $GLOBALS['gl_menu_id'] ."', '". $params . "', '". $this->name_no_prefix . "', '". $this->id ."', '". $this->is_v3 ."');
			";

		return $html;
	}

	/**
	 * Crea un input text lliure però busca suggerències
	 *
	 * @access public
	 * @return string Desplegable complert en HTML
	 */
	function get_text_autocomplete()
	{
		$this->class .= ($this->class ? ' ' : '') . 'text_autocomplete';
		$html = $this->get_text();

		// propago paràmetres igual que faig amb show_record_link a db_base_list_show, si no no funcionaria quan no hi hagi menu_id
		$params = get_all_get_params(array('action', $this->id_field, 'page'.$this->module->name, 'tool', 'tool_section', 'menu_id', 'id'),'&','',true);

		$GLOBALS['gl_page']->javascript .=
			"
			add_text_autocomplete ('". $this->tool ."', '". $this->tool_section ."', '". $GLOBALS['gl_menu_id'] ."', '". $params."', '". $this->name_no_prefix."', '". $this->id ."', '". $this->is_v3 ."');
			";

		return $html;
	}

    /**
     * Crea un array de radios
     *
     * @access public
     * @return string Array radios complert en HTML
     */
    function get_radios()
    {
        $html = '';
        $fields = explode(',', $this->select_fields);
        if ($this->select_table)
        {
            if ($this->is_input)
            {
                $results = $this->get_results();
            }
            else
            {
                return $this->get_result();
            }
        }
        else
        {
            if ($this->is_input)
            {
                foreach($fields as $key => $value)
                {
                    $results[$key][0] = $value;
                    $results[$key][1] = isset($this->caption['c_' . $this->name_no_prefix . '_' . $value])?
						$this->caption['c_' . $this->name_no_prefix . '_' . $value]:
						$this->caption['c_' . $value];
                }
            }
            else
            {
                return $this->caption['c_' . $this->value];
            }
        }

        $class = $this->class?(' class="' . $this->class . '"'):'';
        $attributes = $this->attributes?(' ' . $this->attributes):'';
        $javascript = $this->javascript?' ' . $this->javascript:'';

        if ($results)
        {
            foreach ($results as $rs)
            {
                $checked = $this->value == $rs[0]?' checked="checked"':'';
				$html2 = '';
                $html2 .= '<input name="' . $this->input_name . '" id="' . $this->input_id . '_'. $rs[0] . '" type="radio" value="' . $rs[0] . '"' . $checked . $class . $attributes . $javascript . ' />';				
				
				
                if ($this->type != 'radios3') {
					$html2 .= '</td><td align="left">';				
					$html2 .= '<label for="' . $this->input_id . '_' . $rs[0] .'">' . $rs[1] . '&nbsp;&nbsp;</label>';
				}
		
                if ($this->type != 'radios3') $html2 = '<td>' . $html2 . '</td>';
				
                if ($this->type == 'radios2') $html2 = '<tr>' . $html2 . '</tr>';
				
                if ($this->type == 'radios3') $html2 = '<label>' . $html2 . '<span>' . $rs[1] . '</span></label>';
				
				$html .=$html2;
            }
            if ($this->type == 'radios') $html = '<tr align="center">' . $html . '</tr>';
            if ($this->type != 'radios3') $html = '<table class="' . $this->type . '-object" border="0" cellspacing="0" cellpadding="0">' . $html . '</table>';
        }
		
        return $html;
    }

    /**
     * Crea una matriu de checkboxes
     *
     * @access public
     * @return string Checkboxes en HTML
     */
    function get_checkboxes()
    {
        $html = '';
		$checked_results = array();
        $disabled = $this->disabled?' disabled':'';
        $checked = $this->checked?' checked':'';
        $all_selected = !$this->checked_table;
        $is_new = strstr($this->action, 'show_form_new') || strstr($this->action, 'get_form_new');
		Debug::add('canviat a get_, var is_new',$is_new);
        $is_input = $this->is_input;
        // Hi han 2 tipus de checked
        // 1 - Tots marcats (com el cas de les imatges que corresponen a un registre)
        // 2 - Marcats els relacionats (com a newsletter que es mostren tots els grups i es marquen nomès els que s'han triat)
        // 1. 1 Es input o text en un formulari nou -> No es mostra res - Cap consulta
        // 1. 2 Si es un input -> tots marcats - Consulta de tots
        // 1. 3 Si es TEXT  -> Tots en text - Consulta tots
        // 2. 1 Si es un input i formulari nou -> tots desmarcats - Consulta de tots
        // 2. 2 Si es un input i no formulari nou -> Marcar els seleccionats - Consulta de tots
        // - Consulta de marcats
        // 2. 3 Si es text i formulari nou -> No es mostra res - Cap consulta
        // 2. 4 Si es text i no formulari nou -> Mostra nomes marcats en text - Consulta 2 modificada
        // $this->condition en el _config no es pot fer servir en aquest tipus
        // (almenys de moment no sembla necessari)
        switch (true)
        {
            // all_selected
            case $all_selected && $is_new:
                return '';
                break;
            case $all_selected && $is_input:
            case $all_selected && !$is_input:
                $this->condition = $this->id_field . ' = ' . $this->id ;
                $results = $this->get_results();
                break;
            // not all_selected
            case !$all_selected && $is_input && $is_new:
                $results = $this->get_results();
                break;
            case !$all_selected && $is_input && !$is_new:
                $results = $this->get_results();
				
				// per haver escrit malament custumer				
                $checked_results = $this->get_results($this->checked_table, $this->checked_fields, $this->checked_id_field . ' = ' . $this->id);
				
                break;
            case !$all_selected && !$is_input && $is_new:
				// retorno valor per defecte si hi son				
				//if ($this->default_value){// hauria de fer el mateix que a sota
				return '';
                break;
	        case !$all_selected && !$is_input && !$is_new:
	            // Retorna texte dels seleccionats posats en un <ul>
				$checked_results = $this->get_results($this->checked_table, $this->checked_fields, $this->checked_id_field . ' = ' . $this->id);
				if (!$checked_results) return '';
				$field   = explode(',',$this->select_fields);
				$field = $field[0];
				$this->condition = $this->condition?' AND ' . $this->condition:'';
				$this->condition = $field.' IN (' . implode_field($checked_results) . ')' . $this->condition;
                $results = $this->get_results();
				$html = '';
				foreach ($results as $rs){
					$html .= '<li>' . $rs[1] . '</li>';
				}
                return '<ul>' . $html . '</ul>';
                break;
        } // switch
        if ($GLOBALS['gl_site_part'] == 'public') return $this->get_checkboxes_public($results,$is_input,$html,$checked,$disabled,$is_new,$all_selected,$checked_results);

	    if ( $results ) {
		    $i = 1;
		    foreach ( $results as $rs ) {
			    if ( $is_input ) {
				    $value = array_shift( $rs );
				    $text  = implode( " - ", $rs );
				    $html .= '<td><input type="checkbox" value="' . $value . '" name="' . $this->input_name . "[" . $value . ']" id="' . $this->input_id . "_" . $value . '"' . $disabled . $checked . ' /></td><td align="left"><label for="' . $this->input_id . "_" . $value . '">' . $text . '&nbsp;&nbsp;&nbsp;&nbsp;</label></td>';
			    } else {
				    $html .= '<td>' . $rs[1] . '</td>';
			    }
			    if ( $i == $this->textarea_cols ) {
				    $html = '<tr>' . $html . '</tr>';
				    $i    = 0;
			    }
			    $i ++;
		    }
	    }
        // seleccionar les caselles marcades
        if (!$all_selected && $is_input && !$is_new)
        {
            foreach ($checked_results as $rs)
            {
                $html = str_replace('<input type="checkbox" value="' . $rs[0] . '"',
                    '<input type="checkbox" value="' . $rs[0] . '" checked="checked"',
                    $html);
            }
        }
        return '<table border="0" cellspacing="0" cellpadding="0">' . $html . '</table>';
    }
	// a public ho poso en li i ul per poder formatejar millos
	function get_checkboxes_public(&$results,$is_input,$html,$checked,$disabled,$is_new,$all_selected,$checked_results)
    {
		if ($results)
        {
            foreach ($results as $rs)
            {
                if ($is_input)
                {
                    $html .= '<label><input type="checkbox" value="' . $rs[0] . '" name="' . $this->input_name . "[" . $rs[0] . ']" id="' . $this->input_id . "_" . $rs[0] . '"' . $disabled . $checked . ' />' . $rs[1] . '</label>';
                }
                else
                {
                    $html .= '<td>' . $rs[1] . '</td>';
                }
            }
	        
        }
		
        // seleccionar les caselles marcades
        if (!$all_selected && $is_input && !$is_new)
        {
            foreach ($checked_results as $rs)
            {
                $html = str_replace('<input type="checkbox" value="' . $rs[0] . '"',
                    '<input type="checkbox" value="' . $rs[0] . '" checked="checked"',
                    $html);
            }
        }
		
		return $html;
	}

	function get_sublist() {

		$is_input = $this->is_input;
	    $is_admin = $GLOBALS['gl_is_admin'];
		$html = '';

		if ( $is_input ) {
			// propago paràmetres igual que faig amb show_record_link a db_base_list_show, si no no funcionaria quan no hi hagi menu_id
			$params = get_all_get_params(array('action', $this->id_field, 'page'.$this->module->name, 'tool', 'tool_section', 'menu_id', 'id'),'&','',true);

			$class = $this->object_cf['class']?' ' . $this->object_cf['class']:'';
			$table_id = $this->object_cf['sublist_tool'] . '__' . $this->object_cf['sublist_tool_section'] . '_new_list_table';
			$html .= '<label class="active prefix sublist"><span>+</span><input class="'. $class .'" id="' . $this->input_id . '_add" type="text"></label><table id="' . $table_id  . '" class="listRecord sublist sublist-new" border="0" cellspacing="0" cellpadding="0"></table>';
			$GLOBALS['gl_page']->javascript .=
				"
			add_checkboxes_long ('" . $this->tool . "', '" . $this->tool_section . "', '" . $GLOBALS['gl_menu_id'] . "', '". $params ."', '" . $this->name_no_prefix
				. "', '" . $this->id . "', '" . $this->is_v3 . "', '$table_id');
			";


            $is_new = strstr($this->action, 'show_form_new') || strstr($this->action, 'get_form_new');
			if (!$is_new) {
				$html .= $this->module->module->get_sublist_html( $this->id, $this->name_no_prefix, $is_input );
			}

		}
		else {
			// falta fer-ho a públic
			if ($is_admin)
				$html = $this->module->module->get_sublist_html( $this->id, $this->name_no_prefix, $is_input );
		}

		return $html;
	}

	function get_checkboxes_long() {

		$html = '';
		$checked_results = array();
		$disabled = $this->disabled?' disabled':'';
		$checked = $this->checked?' checked':'';
		$is_new = strstr($this->action, 'show_form_new') || strstr($this->action, 'get_form_new');

		$is_input = $this->is_input;

		// propago paràmetres igual que faig amb show_record_link a db_base_list_show, si no no funcionaria quan no hi hagi menu_id
		$params = get_all_get_params(array('action', $this->id_field, 'page'.$this->module->name, 'tool', 'tool_section', 'menu_id', 'id'),'&','',true);

		// TODO - Comprovar amb camp tot automàtic, nomès està a solicituds i es semiautomatic
		// 2 - Marcats els relacionats
		// 2. 1 Si es un input i formulari nou -> tots desmarcats - Cap consulta
		// 2. 2 Si es un input i no formulari nou -> Consulta de marcats
		// 2. 3 Si es text i formulari nou -> No es mostra res - Cap consulta
		// 2. 4 Si es text i no formulari nou -> Mostra nomes marcats en text - Consulta 2 modificada

		switch (true)
		{
			case $is_input && $is_new:
				break;
			case $is_input && !$is_new:

				$table = $this->select_table . ',' . $this->checked_table;
				$fields = $this->select_fields;


				$checked_table   = explode(',',$this->checked_table);
				$checked_table = $checked_table[0];

				$select_table   = explode(',',$this->select_table);
				$select_table = $select_table[0];

				$condition = $this->checked_id_field . ' = ' . $this->id .
				             " AND " . $checked_table . '.' . $this->checked_id_field2 . "
								= " . $select_table . '.' . $this->checked_id_field2;

				if ($this->condition) $condition = $condition . ' AND ' . $this->condition;

				$checked_results = $this->get_results($table, $fields, $condition);

				break;
			case !$is_input && $is_new:
				return '';
				break;
			case !$is_input && !$is_new:
				// Retorna texte dels seleccionats posats en un <ul>
				$checked_results = $this->get_results($this->checked_table, $this->checked_fields, $this->checked_id_field . ' = ' . $this->id);
				if (!$checked_results) return '';
				$field   = explode(',',$this->select_fields);
				$field = $field[0];
				$this->condition = $this->condition?' AND ' . $this->condition:'';
				$this->condition = $field.' IN (' . implode_field($checked_results) . ')' . $this->condition;
				$results = $this->get_results();
				$html = '';
				foreach ($results as $rs){
					$html .= '<li>' . $rs[1] . '</li>';
				}
				return '<ul>' . $html . '</ul>';
				break;
		} // switch

		if ( $checked_results ) {
			$i = 1;
			foreach ( $checked_results as $rs ) {
				if ( $is_input ) {
					$value = array_shift( $rs );
					$text  = implode( " - ", $rs );
					$html .= '<td><input type="checkbox" value="' . $value . '" checked="checked" name="' . $this->input_name . "[" . $value . ']" id="' . $this->input_id . "_" . $value . '"' . $disabled . ' /></td><td align="left"><label for="' . $this->input_id . "_" . $value . '">' . $text . '&nbsp;&nbsp;&nbsp;&nbsp;</label></td>';
				} else {
					$html .= '<td>' . $rs[1] . '</td>';
				}
				if ( $i == $this->textarea_cols ) {
					$html = '<tr>' . $html . '</tr>';
					$i    = 0;
				}
				$i ++;
			}
		}


		$html = '<table border="0" cellspacing="0" cellpadding="0">' . $html . '</table>';

		if ($is_input) {
			$html .= '<table class="checkboxes_long" id="' . $this->input_id . '_table" border="0" cellspacing="0" cellpadding="0"></table><label class="active prefix checkboxes_long"><span>+</span><input id="' . $this->input_id . '_add" type="text"></label>';
			$GLOBALS['gl_page']->javascript .=
				"
				add_checkboxes_long ('". $this->tool ."', '". $this->tool_section ."', '". $GLOBALS['gl_menu_id'] ."', '". $params ."', '". $this->name_no_prefix
				."', '". $this->id ."', '". $this->is_v3 ."', false);
				";
		}

		return $html;

	}

    /**
     * Crea un array per valorar de l'1 al 10
     *
     * @access public
     * @return string Array radios complert en HTML
     */
    function get_rate()
    {
        $html = '';
        $num = 10;
		for ($i = 1; $i <= $num; $i++)
		{
			$results[$i][0] = $i;
			$results[$i][1] = $i;
		}

        $class = $this->class?(' class="' . $this->class . '"'):'';
        $attributes = $this->attributes?(' ' . $this->attributes):'';
        $javascript = $this->javascript?' ' . $this->javascript:'';

        if ($results)
        {
            foreach ($results as $rs)
            {
                $checked = $this->value == $rs[0]?' checked="checked"':'';

                $html .= '<td style="padding:2px;"><label for="' . $this->input_id . '_' . $rs[0] .'">' . $rs[1] . '&nbsp;&nbsp;</label></td>';               
            }
           $html = '<tr align="center">' . $html . '</tr>';
            foreach ($results as $rs)
            {
                $checked = $this->value == $rs[0]?' checked="checked"':'';

                $html .= '<td style="padding:2px;"><input name="' . $this->input_name . '" id="' . $this->input_id . '_'. $rs[0] . '" type="radio" value="' . $rs[0] . '"' . $checked . $class . $attributes . $javascript . ' /></td>';               
            }
           $html = '<tr align="center">' . $html . '<td class="' . $this->input_name . '_error"></td></tr>';
        }
        return '<table class="rate-object" border="0" cellspacing="0" cellpadding="0">' . $html . '</table>';
    }

    /**
     * Crea una resposta si o no
     *
     * @access public
     * @return string Array radios complert en HTML
     */
    function get_yesno()
    {
        $html = '';
		
		
		
		$results[1][0] = '1';
		$results[1][1] = $this->caption['c_yes'];
		
		$results[0][0] = '0';
		$results[0][1] = $this->caption['c_no'];
		
		if (!$this->is_input) return $results[$this->value][1];

        $class = $this->class?(' class="' . $this->class . '"'):'';
        $attributes = $this->attributes?(' ' . $this->attributes):'';
        $javascript = $this->javascript?' ' . $this->javascript:'';

        if ($results)
        {
            foreach ($results as $rs)
            {
                $checked = $this->value == $rs[0]?' checked="checked"':'';

                $html .= '<td style="padding:2px;"><label for="' . $this->input_id . '_' . $rs[0] .'">' . $rs[1] . '&nbsp;&nbsp;</label></td>';
				
                $html .= '<td style="padding:2px;"><input name="' . $this->input_name . '" id="' . $this->input_id . '_'. $rs[0] . '" type="radio" value="' . $rs[0] . '"' . $checked . $class . $attributes . $javascript . ' /></td>';                
            }
           $html = '<tr align="center">' . $html . '</tr>';
        }
        return '<table class="yesno-object" border="0" cellspacing="0" cellpadding="0">' . $html . '</table>';
    }

	private function get_filecat(&$file_categorys, $selected_id, $name) {

		$html = '';

		if ( !empty ($this->config['filecat_has_category_zero']) ) {
			$selected = $selected_id == 0 ? ' selected' : '';
			$html     .= "<option value='0' $selected></option>";
		}



		foreach ( $file_categorys as $category ) {
			$filecat_id = $category['filecat_id'];
			$selected = $category['filecat_id'] == $selected_id ? ' selected' : '';
			$filecat = strip_tags ($category['filecat']);
			$html .= "<option value='$filecat_id' $selected>$filecat</option>";
		}

		$html ="<select name='$name'>$html</select>";

		return $html;
	}

	private function get_file_status($selected_status, $name) {

		$html = '';

		$status_array = array('prepare', 'public');

		foreach ( $status_array as $status ) {
			$selected = $status == $selected_status ? ' selected' : '';
			$status_text = strip_tags( $this->caption [ 'c_' . $status ] );
			$html .= "<option value='$status' $selected>$status_text</option>";
		}

		$html ="<select name='$name'>$html</select>";

		return $html;
	}
    /**
     * Crea una matriu de checkboxes
     *
     * @access public
     * @return string Checkboxes en HTML
     */
    function get_file()
    {
        $html = '';
        $disabled = $this->disabled?' disabled':'';
        $checked = $this->checked?' checked':'';
        $all_selected = !$this->checked_table;
        $is_new = strstr($this->action, 'show_form_new') || strstr($this->action, 'get_form_new');
		Debug::add('canviat a get_, var is_new',$is_new);
        $is_input = $this->is_input;
	    $is_admin = $GLOBALS['gl_is_admin'];
	    $accept = '.' . str_replace( '|', ',.', $this->accept );

	    $has_filecat_id = is_table( $this->select_table . 'cat' );


	    if ( ! $is_new ) {

	    	$has_name_original = $this->config['files_show_original_name'];
	        $has_title = is_field($this->select_table, 'title');
	        $has_ordre = is_field($this->select_table, 'ordre');
	        $has_public = is_field($this->select_table, 'public');
	        $has_show_home = !empty ($this->object_cf['has_show_home']) && $this->object_cf['has_show_home']=='1';
		    $rs_num = 2;

		    if ( $has_name_original ) {
			    $this->select_fields .= ',name_original';
			    $name_original_index = $rs_num;
			    $rs_num ++;
		    }
		    if ( $has_filecat_id ) {
			    $this->select_fields .= ',filecat_id';
			    $filecat_id_index = $rs_num;
			    $rs_num ++;
		    }
		    if ( $has_title ) {
			    $this->select_fields .= ',title';
			    $title_index = $rs_num;
			    $rs_num ++;
		    }
		    if ( $has_ordre ) {
			    $this->select_fields .= ',ordre';
			    $ordre_index = $rs_num;
			    $rs_num ++;
		    }
		    if ( $has_public ) {
			    $this->select_fields .= ',public';
			    $public_index = $rs_num;
			    $rs_num ++;
		    }
		    if ( $has_show_home ) {
			    $this->select_fields .= ',show_home';
			    $show_home_index = $rs_num;
			    $rs_num ++;
		    }
	    }



        // Hi han 2 tipus de checked
        // 1 - Tots marcats (com el cas de les imatges que corresponen a un registre)

        // 1. 1 Es input o text en un formulari nou -> No es mostra res - Cap consulta
        // 1. 2 Si es un input -> tots marcats - Consulta de tots
        // 1. 3 Si es TEXT  -> Tots en text - Consulta tots
        // 1. 3b Si es TEXT i show_langs=false -> Nomes es mostra el de l'idioma

        // $this->condition en el _config no es pot fer servir en aquest tipus
        // (almenys de moment no sembla necessari)
        switch (true)
        {
            // all_selected
            case $all_selected && $is_new:
				$results = false;
                break;
			case $all_selected && !$this->show_langs:
                $this->condition = $this->id_field . ' = ' . $this->id . " AND language = '" . LANGUAGE . "'" ;
				if ( $has_ordre ) {
					if (empty ($this->config['file_order_desc'])){
						$this->order_by .= ' ORDER BY ordre';
					}
					else {
						$this->order_by .= ' ORDER BY ordre DESC';
					}
				}
				/*				if ($has_ordre || $has_filecat_id){
					$this->condition .= ' ORDER BY ordre';
				}*/
                $results = $this->id?$this->get_results():array();
                break;
            case $all_selected && $is_input:
            case $all_selected && !$is_input:

                $this->condition = $this->id_field . ' = ' . $this->id ;
		        if ( $has_ordre ) {
					if (empty ($this->config['file_order_desc'])){
						$this->order_by .= ' ORDER BY ordre';
					}
					else {
						$this->order_by .= ' ORDER BY ordre DESC';
					}
				}
                $results = $this->id?$this->get_results():array();
                break;
        } // switch
		

        if ($results)
        {
		    if ( $has_filecat_id ) {
			    $file_categorys = Db::get_rows( "SELECT * FROM " . $this->select_table . "cat WHERE language = '" . LANGUAGE . "' ORDER BY filecat ASC" );
		    }
            foreach ($results as $rs)
            {
			// NOMES ES PER ADMIN, A PUBLIC ESTÀ A L'ARRAY FILES
				/*
				 * 
				 *
				vaig comprobar amb tecnomix i no feia falta
				w.tecnomix/admin/?menu_id=3603&action=show_form_edit&tool=tecnomix&tool_section=instalacio&new_id=6
				parent_tool_section  seria instalacio i els arxius els guarda a tecnomix/news
				
				$tool = $this->parent_tool?$this->parent_tool:$this->tool;
				$tool = $this->parent_tool?$this->parent_tool:$this->tool;
				$tool_section = $this->parent_tool_section?$this->parent_tool_section:$this->tool_section;

				 * 
				 * 
				 * 				 */
				
				$file_ext = strrchr(substr($rs[1], -5, 5), '.');
				$download_link = UploadFiles::get_download_link($this->tool, $this->tool_section, $this->name_no_prefix, $rs[0], $file_ext);
				
						
				if ($is_input)
				{
					$file_input_id = $this->input_id . '_' . $rs[0];
					$html .= '<tr id="' . $file_input_id . '"><td><a onclick="form_object_file_delete_row(\'' . $file_input_id . '\')" class="fa fa-trash fa-lg"></a><input class="hidden-checkbox" type="checkbox" value="' . $rs[0] . '" id="' . $file_input_id . '_hidden" name="' . $this->input_name . '[' . $rs[0] . ']"' . $disabled . ' checked /></td>';

					// A admin editarem el nom també
					if ( $is_admin ) {

						$file_ext        = strrchr( substr( $rs[1], - 5, 5 ), '.' );
						$file_name_short = str_replace( $file_ext, '', $rs[1] );


						if ( $has_name_original ) {
		                    $name_original = '<div class="name-original">' . $rs[ $name_original_index ] . '</div>';
	                    }
	                    else {
							 $name_original = '';
	                    }
						$html .= '<td>
									<input type="text" value="' . htmlspecialchars( $file_name_short ) . '" name="' . $this->input_name . "[file_name_short][" . $rs[0] . ']" />
									<input type="hidden" value="' . htmlspecialchars( $file_ext ) . '" name="' . $this->input_name . "[file_ext][" . $rs[0] . ']" />
									 ' . $file_ext .
						            $name_original .
					            '</td>';

					} else {

						$html .= '<td><a href="' . $download_link . '">' . $rs[1] . '</a></td>';

					}


					// Els camps extres només són per admin, desde públic només poden pujar l'arxiu
                    if ($is_admin){
	                    // Categoria
	                    if ( $has_filecat_id ) {
		                    $filecat_id     = $rs[ $filecat_id_index ];
		                    $filecat_select = $this->get_filecat( $file_categorys, $filecat_id, $this->input_name . "[filecat_id][" . $rs[0] . ']' );
		                    $html .= "<td>$filecat_select</td>";
	                    };

	                    // Títol
	                    // Arrays tipus file[466][show_home][2473], així no interfereix amb el checkbox file[466][2473]
	                    if ( $has_title ) {
		                    $title = $rs[ $title_index ];
		                    $html .= '<td><input type="text" value="' . htmlspecialchars( $title ) . '" name="' . $this->input_name . "[title][" . $rs[0] . ']" /></td>';
	                    };
	                    // Ordre
	                    if ( $has_ordre ) {
		                    $ordre = $rs[ $ordre_index ];
		                    $html .= '<td><input class="int" type="text" value="' . htmlspecialchars( $ordre ) . '" name="' . $this->input_name . "[ordre][" . $rs[0] . ']" /></td>';
	                    };

	                    // Public
	                    if ( $has_public ) {
		                    $public_checked = $rs[ $public_index ] ? ' checked' : '';
		                    $html .= '<td><input type="checkbox" value="' . $rs[0] . '" name="' . $this->input_name . "[public][" . $rs[0] . ']"' . $public_checked . ' /></td>';
	                    };

	                    // checkbox per mostrar a la home
	                    if ( $has_show_home ) {
		                    $show_home_checked = $rs[ $show_home_index ] ? ' checked' : '';
		                    $html .= '<td><input type="checkbox" value="' . $rs[0] . '" name="' . $this->input_name . "[show_home][" . $rs[0] . ']"' . $show_home_checked . ' /></td>';
	                    };


	                    // Download del file segons config
	                    if ($this->config['files_show_download']) {
		                    $html .= '<td><a target="_blank" class="fa fa-download fa-lg" href="' . $download_link . '"></a></td>';
	                    }

						// Canviar l'arxiu
	                    $input_replace = '<input onchange="form_object_file_replace_on_change(this)" class="hidden-file" accept="' . $accept . '" type="file" id="replace_' . $file_input_id . '" name="replace_' . $this->input_name . "[" . $rs[0] . ']" />';

	                    $html .= '<td class="hidden-file-holder"><div><a class="fa fa-upload fa-lg"></a>' . $input_replace . '</div><span id="replace_' . $file_input_id . '_span"></span></td>';


                    }
					$html .= '</tr>';
                }
				else
				{
					
					$html .= '<div><a href="' . $download_link . '">' . $rs[1] . '</a></div>';
				}
            }
			if ($is_input)
			{
				$table_header = "<tr><td></td><td>" . $this->caption['c_file_file'] . "</td>";
				if ($is_admin){
					if ($has_filecat_id)
						$table_header .= "<td>" . $this->caption['c_file_filecat_id'] . "</td>";
					if ($has_title)
						$table_header .= "<td>" . $this->caption['c_file_title'] . "</td>";
					if ($has_ordre)
						$table_header .= "<td>" . $this->caption['c_file_ordre'] . "</td>";
					if ($has_public)
						$table_header .= "<td>" . $this->caption['c_file_public'] . "</td>";
					if ($has_show_home)
						$table_header .= "<td>" . $this->caption['c_file_show_home'] . "</td>";

					// Les icones de descarregar i carregar
					if ( $this->config['files_show_download'] ) {
						$table_header .= "<td></td><td></td>";
					}
					$table_header .= "<td></td>";

				}
				$html = "<table> $table_header $html </table>";
			}
			$html  = '<div id="' . $this->input_id . '_holder" class="multifiles">' . $html . '</div>';
        }

	    $is_long = isset($this->object_cf['is_long']) && $this->object_cf['is_long'];

	    // Gestor avançat per pujar tants arxius com es vulgui
	    // Nomes quan s'edita ( ja que va per ajax i necessitem el registre ja creat ) i a admin
		if ($is_input && $is_long && !$is_new && $GLOBALS['gl_is_admin']){

            // poso el div buit per poder emplenar amb ajax
            if (!$results)
                $html  = '<div id="' . $this->input_id . '_holder" class="multifiles"></div>';

			// poso javascript per insertar nou file
			$html ='<div class="dropzone" id="dropzone'.$this->input_id.'"></div>' . $html;

			$this->accept = '.' . str_replace( '|', ',.', $this->accept );

			$GLOBALS['gl_page']->javascript .=
				"
				add_file_dropzone (
					'$this->tool',
					'$this->tool_section',
					'$this->name_no_prefix',
					'$this->id',
					'$this->input_id',
					'$this->accept'
				);
            ";

		}
		elseif ($is_input){
			$max = ($this->text_maxlength==1)?$this->caption['c_file_maximum_1']:sprintf ($this->caption['c_file_maximum'],$this->text_maxlength);


			// poso javascript per insertar nou file
			 $html .='<input accept="' . $accept . '" class="file" type="file" id="'.$this->input_id.'" multiple name="' . $this->input_name . '[]" /><span class="file-max"> ( '. $max .') </span>';
			 //$html .='<input class="file" type="file" id="'.$this->input_id.'" multiple name="' . $this->input_name . '[]" /><span class="file-max"> ( '. $max .') </span>
			 //<div id="'.$this->input_id.'_list"></div>';

            $GLOBALS['gl_page']->javascript .=
            "
              $('#$this->input_id').MultiFile({
                accept:'$this->accept',
                max:$this->text_maxlength,
                STRING: {
                     remove: 'x'
                }
              });
            ";

		}
        return $html;
    }

    function get_text()
    {
        global $gl_saved;
        $html0 = '';
        $html2 = '';

        $text_size = $this->text_size?(' size="' . $this->text_size . '"'):'';
        $text_maxlength = $this->text_maxlength?(' maxlength="' . $this->text_maxlength . '"'):'';
        $class = $this->class?(' class="' . $this->class . '"'):'';
        $attributes = $this->attributes?(' ' . $this->attributes):'';
        $javascript = $this->javascript?(' ' . $this->javascript):'';
        // miro si es un formulari poso el valor per defecte
        $this->value = ($this->id != '0' || !$gl_saved)?htmlspecialchars(strip_tags($this->value)):htmlspecialchars(strip_tags($this->default_value));

        switch ($this->type)
        {
            case 'password':
				$type = 'password' ;
				$asterisks = '';
				$password_view = empty($this->object_cf['password_view'])?false:$this->object_cf['password_view'];

					// TODO $this->name comprobar si va amb un sublist
					$repeat_input = "<input id=\"{$this->input_id}_repeat\" name=\"{$this->name}_repeat[{$this->id}]\" type=\"{$type}\" value=\"{$this->value}\"{$text_size}{$text_maxlength}{$class}{$attributes}{$javascript} />";
				// Creo un nou camp que es diu nom del input password + _repeat
	            if ( $GLOBALS['gl_is_admin'] ) {
		            if ( $password_view ) {
			            $repeat_input .= "
						<span onclick=\"form_object_toggle_password('{$this->input_id}_repeat')\" class=\"fa fa-fw fa-eye\"></span>
						";
		            }


		            $GLOBALS['gl_page']->javascript .= "
						form_object_init_password( '{$this->input_id}_repeat' ); 
					";

	            }

				$this->module->rs[$this->name.'_repeat'] = $repeat_input;
				if ($this->module->use_default_template){
					$v[0] = array('lang' => '', 'val' => $repeat_input);
					$this->module->rs['fields'][] = array('key' => $this->module->caption['c_' . $this->name_no_prefix], 'val' => $v);
					// giro el caption per els automatics ja que surt primer el repeat, els inputs tant li fa l'ordre, de fet els valors han de ser exactament iguals
					$this->module->caption['c_' . $this->name_no_prefix] = $this->module->caption['c_' . $this->name_no_prefix . '_repeat'];
				}


				 if ( $GLOBALS['gl_is_admin'] ) {

					 if ( $password_view ) {
						 $html2 = "
						<span onclick=\"form_object_toggle_password( '{$this->input_id}' )\" class=\"fa fa-fw fa-eye\"></span>
						";
					 }


					$GLOBALS['gl_page']->javascript .= "
						form_object_init_password( '{$this->input_id}' ); 
					";

	            }

				break;
	        case 'email':
		        $type = 'text';

				// afegeixo els attributes perque no faci autocomplete, no m'interessa que ho faci mai ja que són formularis sempre diferents i no soles ser les dades del que emplena al formulari
	            if ( $GLOBALS['gl_is_admin'] ) {
	                $attributes = ' autocomplete="off"' . $attributes;
	            }

		        if ( $this->list_show_type == 'form' && empty($this->object_cf['no_repeat']) ) {
			        // Creo un nou camp que es diu nom del input mail + _repeat
					// TODO $this->name comprobar si va amb un sublist
			        $val                                         = '<input name="' . $this->name . '_repeat[' . $this->id . ']" type="' . $type . '" value="' . $this->value . '"' . $text_size . $text_maxlength . $class . $attributes . ' />';
			        $this->module->rs[ $this->name . '_repeat' ] = $val;
			        if ( $this->module->use_default_template ) {
				        $v[0]                         = array( 'lang' => '', 'val' => $val );
				        $this->module->rs['fields'][] = array(
					        'key' => $this->module->caption[ 'c_' . $this->name_no_prefix ],
					        'val' => $v
				        );
				        // giro el caption per els automatics ja que surt primer el repeat, els inputs tant li fa l'ordre, de fet els valors han de ser exactament iguals
				        $this->module->caption[ 'c_' . $this->name_no_prefix ] = $this->module->caption[ 'c_' . $this->name_no_prefix . '_repeat' ];
			        }
		        }

		        break;
            case 'int':$type = 'text';
                break;
            case 'zero_fill_int':$type = 'text';
	            // TODO $this->name comprobar si va amb un sublist
				$GLOBALS['gl_page']->javascript .= '

					if ($.fn.inputmask){
						$("#'.$this->name.'_'.$this->id.'").inputmask(
							{
								mask: "9999",
								placeholder: "",
								digits: ' . $text_maxlength . ',
								digitsOptional: 0,
								// ,rightAlign: !0
							}
						);
					}';
                break;
            case 'datetime':$type = 'text';

				// TODO $this->name comprobar si va amb un sublist
				$datetime = explode(' ',$this->value);
	            $this->value = $datetime[0];
				$time = isset($datetime[1])?$datetime[1]:'';
				$GLOBALS['gl_page']->javascript .= '
					if ($.fn.inputmask){
						$("#'.$this->name.'_time_'.$this->id.'").inputmask(
							"h:s",
							{
								"placeholder": "hh:mm",
								 hourFormat: "24"
							}
						);
					}';
					$html2 = '&nbsp;<input id="' . $this->name . '_time_' . $this->id . '" name="' . $this->name . '_time[' . $this->id . ']" type="text" size="4" value="' . $time . '" class="vtime" />';

            case 'date':$type = 'text';
				$GLOBALS['gl_page']->javascript .= '
					$("#'.$this->input_id.'").datepicker({
						changeMonth: true,
						changeYear: true,
						yearRange: \''.(date("Y")-100).':'.(date("Y")+50).'\'
					});

					if ($.fn.inputmask){
						$("#'.$this->input_id.'").inputmask(
							"d/m/y",
							{
								"placeholder": "dd/mm/yyyy"
						});
					}';
	            $attributes = ' autocomplete="off"' . $attributes;
				break;
            case 'time':$type = 'text';
	            // TODO $this->name comprobar si va amb un sublist
				$time = $this->value;
				$GLOBALS['gl_page']->javascript .= '

					if ($.fn.inputmask){
						$("#'.$this->name.'_'.$this->id.'").inputmask(
							"h:s",
							{
								"placeholder": "hh:mm",
								 hourFormat: "24"
							}
						);
					}';
	            $attributes = ' autocomplete="off"' . $attributes;
                break;
            case 'minutes':$type = 'text';
	            // TODO $this->name comprobar si va amb un sublist
				$time = $this->value;
				$GLOBALS['gl_page']->javascript .= '

					if ($.fn.inputmask){
						$("#'.$this->name.'_'.$this->id.'").inputmask(
							"s:s",
							{
								"placeholder": "mm:ss"
							}
						);
					}';
                break;
            case 'creditcard':$type = 'text';
                break;
            case 'creditdate':$type = 'text';
                break;
            case 'text':$type = 'text';
                break;
            case 'decimal':$type = 'text';
                break;
            case 'decimal_null':$type = 'text';
                break;
            case 'currency':$type = 'text';
                break;
            case 'names': $type = 'text';
                break;
            case 'filename': $type = 'text';
                break;
            case 'account':$type = 'text';
                break;
            case 'nif':$type = 'text';
                break;
            case 'text_autocomplete':$type = 'text';
                break;
            default: ;
        } // switch
        $html = '<input id="' . $this->input_id . '" name="' . $this->input_name . '" type="' . $type . '" value="' . $this->value . '"' . $text_size . $text_maxlength . $class . $attributes . $javascript . ' />';
        return $html0 . $html . $html2;
    }

    function get_textarea($is_type_html='')
    {
		if ($this->type == 'textarea') $this->value = htmlspecialchars($this->value);

        $textarea_cols = $this->textarea_cols?$this->textarea_cols:'90';
        $textarea_rows = $this->textarea_rows?$this->textarea_rows:'20';
        $class = $this->class?(' class="' . $this->class . '"'):'';
        $javascript = $this->javascript?(' "' . $this->javascript . '"'):'';
        $attributes = $this->attributes?(' "' . $this->attributes . '"'):'';
        $text_maxlength = ($this->text_maxlength && $this->text_maxlength!='-1')?(' onkeyup="return textarea_maxlength(this, ' . $this->text_maxlength . ')"'):'';
		
        $html = '<textarea '.$is_type_html.'name="' . $this->input_name . '" id="' . $this->input_id . '" cols="' . $textarea_cols . '" rows="' . $textarea_rows . '"' . $class . $attributes . $javascript . $text_maxlength . '>' . $this->value . '</textarea>';
        return $html;
    }

    function get_checkbox()
    {
			$checked = ($this->value == 1)?' checked':'';
			$class = $this->class?(' class="' . $this->class . '"'):'';
            $disabled =($this->disabled)? ' disabled':'';
			$javascript = $this->javascript?(' "' . $this->javascript . '"'):'';
			$html = '<input name="' . $this->input_name . '" id="' . $this->input_id . '" type="checkbox" value="1"' . $class . $javascript . $checked . $disabled . ' />';
			return $html;
    }

    function get_radio()
    {
    	// TODO $this->name comprobar si va amb un sublist
        $checked = ($this->value == 1)?' checked':'';
        $class = $this->class?' class="' . $this->class . '"':'';
        $attributes = $this->attributes?(' ' . $this->attributes . ''):'';
        $javascript = $this->javascript?(' ' . $this->javascript . ''):'';
        $html = '<input name="' . $this->name . '" id="' . $this->input_id . '" type="radio" value="' . $this->id . '"' . $class . $attributes . $javascript . $checked . ' />';
        return $html;
    }

    function get_hidden()
    {
        $html = '<input id="' . $this->input_id .'" name="' . $this->input_name . '" type="hidden" value="' . htmlentities($this->value) . '" />';
        return $html;
    }

	function get_html() {
		//static $n = 0;
		$GLOBALS['gl_page']->html_editors[ $this->type ][] = "#{$this->input_id}";

		$this->value         = htmlspecialchars( $this->value );
		$this->textarea_rows = $this->textarea_rows ?: ($this->type == 'htmlbasic' ? '20' : '35');
		$language_code = $GLOBALS['gl_languages']['codes2'][$this->input_language];
        $this->attributes .= " data-language = '{$language_code}'";
		$ret                 = $this->get_textarea( 'style="visibility:hidden;" ' );

		// Per posar com a div i fer inline: $ret = "<div id=\"{$this->input_id}\">{$this->value}</div>";

		if ( DEBUG ) $ret .= '<p><a class="btn" onclick="getElementById(\'' . $this->input_id . '\').style.visibility=\'visible\';tinymce.execCommand(\'mceToggleEditor\',false,\'' . $this->input_id . '\');"><span>( HTML, nomès en debug )</span></a></p>';

		return $ret;
	}

	// Si el id del formulari es 0 i hi ha value, vol dir que es el default_value,
    // EL force el faig servir a web_config perque vull que sempre forci la normalitzacio
	function normalize_date() {

        if ( ($this->id != '0' || ! $this->value) && empty($this->object_cf['force']) )
			return;

        @list( $date, $time ) = explode( ' ', $this->value );


		if ( $date ) {
			list( $day, $month, $year ) = explode( '/', $date );

			if ( strlen( $day ) < 2 )
				$day = '0' . $day;
			if ( strlen( $month ) < 2 )
				$month = '0' . $month;
			$this->value = $this->default_value = $day . '/' . $month . '/' . $year;
		}
		if ( $time ) {
			list( $hour, $minute ) = explode( ':', $time );
			if ( strlen( $hour ) < 2 )
				$hour = '0' . $hour;
			if ( strlen( $minute ) < 2 )
				$minute = '0' . $minute;
			$this->value .= ' ' . $hour . ":" . $minute;
			$this->default_value = $this->value;
		}


	}

	/**
	 * Formatejar el valor (text, int, decimal, date, time
	 * textarea s'haurà de formatejar per substituir lineas per <br> nomès quan es mostra com a text
	 * checkbox, radio, select, html no es formateja mai, ja són valors predefinits
	 *
	 * Al fer un canvi aquí, també fer-lo a {@link Export::process_results}
	 *
	 * @param bool $return_array
	 * @param bool $is_excel
	 *
	 * @return array|string
	 */
	function get_input($return_array=true, $is_excel = false)
    {
        // !!!!!!!!!!!!!!!!!!!!!!!!!!  també s'haurien de poder fomatejar els selects i radios !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

	    if ($is_excel && function_exists('format_' . $this->type . '_excel'))
        {
            $this->value = call_user_func ('format_' . $this->type . '_excel', $this->value);
        }
        elseif (function_exists('format_' . $this->type . '_' . $this->list_show_type))
        {
            $this->value = call_user_func ('format_' . $this->type . '_' . $this->list_show_type, $this->value);
        }
        elseif (function_exists('format_' . $this->type))
        {
            $this->value = call_user_func ('format_' . $this->type, $this->value);
        }
		// el valor original ha de quedar tal qual, aqui li feia la funcio de formatejar
       // $this->value = $this->original_value = ($this->id != '0')?$this->value:$this->default_value;
        $this->value = ($this->id != '0')?$this->value:$this->default_value;
		$original_val = false;

        // es mostra un input, o sigui que es editable
        if ($this->is_input)
        {
            switch ($this->type)
            {
	            case 'datetime':
	            case 'date':
					// Classes definides a main.js a form_init_validations
		            $this->class .= ( $this->class ? ' ' : '') . 'vdate';
					$this->normalize_date();
		            $r = $this->get_text();
	                break;
	            case 'time':
		            $this->class .= ( $this->class ? ' ' : '') . 'vtime';
		            $r = $this->get_text();
		            break;
	            case 'minutes':
		            $this->class .= ( $this->class ? ' ' : '') . 'vminutes';
		            $r = $this->get_text();
		            break;
                case 'text':
                case 'password':
                case 'email':
                case 'int':
                case 'zero_fill_int':
                case 'decimal':
                case 'decimal_null':
                case 'currency':
                case 'account':
                case 'nif':
                case 'names':
                case 'filename':
                case 'creditcard':
                case 'creditdate':
                    $r = $this->get_text();
					$original_val = $this->original_value; // TODO-i També hauria d'anar a get_text_autocomplete, get_hidden ..
                    break;
                case 'text_autocomplete':
                    $r = $this->get_text_autocomplete();
                    break;
                case 'textarea':
                case 'videoframe':
                    $r = $this->get_textarea();
                    break;
                case 'checkbox':
                    $r = $this->get_checkbox();
					$original_val = $this->original_value;
                    break;
                case 'radio':
                    $r = $this->get_radio();
					$original_val = $this->original_value;
                    break;
                case 'select':
                    $r = $this->get_select();
					$original_val = $this->original_value;
                    break;
                case 'select_long':
                    $r = $this->get_select_long();
					$original_val = $this->original_value;
                    break;
                case 'radios':
                case 'radios2':
                case 'radios3':
                    $r = $this->get_radios();
					$original_val = $this->original_value;
                    break;
                case 'html':
                case 'htmlbasic':
                case 'htmlfull':
                case 'htmlnewsletter':
                    $r = $this->get_html();
                    break;
                case 'hidden':
                    $r = $this->get_hidden();
                    break;
                case 'checkboxes':
                    $r = $this->get_checkboxes();
                    break;
                case 'checkboxes_long':
                    $r = $this->get_checkboxes_long();
                    break;
                case 'sublist':
                    $r = $this->get_sublist();
                    break;
                case 'file':
                    $r = $this->get_file();
                    break;
                case 'rate':
                    $r = $this->get_rate();
                    break;
                case 'yesno':
                    $r = $this->get_yesno();
					$original_val = $this->original_value;
                    break;
            } // switch}
            $r .= '<input name="old_' . $this->input_name . '" type="hidden" value="' . htmlentities($this->value) . '" />';
        }
        // es mostra nomès text, no es editable
        else
        {
            switch ($this->type)
            {
                case 'password':
                    $r = "********";
                    break;
                case 'text':
                case 'email':
                case 'int':
                case 'zero_fill_int':
                case 'names':
                case 'filename':
                case 'decimal':
                case 'decimal_null':
                case 'currency':
                case 'account':
                case 'nif':
                case 'datetime':
                case 'date':
                case 'time':
                case 'minutes':
                case 'creditcard':
                case 'creditdate':
                case 'text_autocomplete':
                    $r = htmlspecialchars($this->value);
					$original_val = $this->original_value;
                    break;
                case 'html':
                case 'htmlbasic':
                case 'htmlfull':
                case 'htmlnewsletter':
					if ($this->add_dots)
						$this->value = add_dots ($this->name, $this->value);
                    $r = $this->value;
                    break;
                case 'radio':
                case 'select':
                case 'select_long':
                case 'radios':
                case 'radios2':
                case 'radios3':
                    $r = $this->get_select();
					$original_val = $this->original_value;
                    break;
                case 'textarea':
					if ($this->add_dots)
						$this->value = add_dots ($this->name, $this->value);
                    $r = nl2br(htmlspecialchars($this->value));
                    break;
                case 'checkbox':
                	if (empty($this->object_cf['checkbox_show_text'])) {
		                $r = $this->value;
	                } else {
		                $r = $this->value == '1' ? $this->caption['c_yes'] : $this->caption['c_no'];
	                }
					$original_val = $this->original_value;
                    break;
                case 'hidden':
                    $r = $this->get_hidden(); // sempre surt hidden, tant es 'is_input'
                    break;
	            case 'checkboxes':
                    $r = $this->get_checkboxes();
                    break;
                case 'checkboxes_long':
                    $r = $this->get_checkboxes_long();
                    break;
                case 'sublist':
                    $r = $this->get_sublist();
                    break;
                case 'file':
                    $r = $this->get_file();
                    break;
                case 'videoframe':
                    $r = $this->value;
                    break;
                case 'rate':
                    $r = $this->value;
                    break;
                case 'yesno':
                    $r = $this->get_yesno();
					$original_val = $this->original_value;
                    break;
            } // switch}
        }
		if ($return_array)
			return array('val'=>$r, 'original_val'=>$original_val);
		else
			return $r;
    }
}

/**
 *
 * @access public
 * @return string
 */
function format_decimal($number)
{
    settype($number, "float");

    $n = explode('.' , $number);

    $n[0] = number_format($n[0], 0, ',', '.');
    $n[1] = isset($n[1])? ',' . $n[1]:'';
    $number = $n[0] . $n[1];
    return $number;
}
function unformat_decimal($number, $quotes=false, $field = '', $row = 0, $config = array())
{
    $number = str_replace('.', '', $number);
    $number = str_replace(',', '.', $number);
    if ($quotes) $number = Db::qstr($number);
    return $number;
}
function format_decimal_null($number) {
	if ($number) return format_decimal($number);
	else return $number;
}
function unformat_decimal_null($number) {
	if ($number != '') return unformat_decimal($number);
	else return 'NULL';
}
/**
 *
 * @access public
 * @return string
 */
function format_currency($number, $format_zero=true)
{
	if (!$GLOBALS['gl_is_admin'] && $number==0) return 0;
    settype($number, "float");
	
    if (!$format_zero && ($number== 0) ) return 0;
	$number = number_format($number, 2, ',','.');
    return $number;
}

function format_currency_excel($number)
{
    // tal qual ve de la BBDD va be per l'excel
    return $number;
}
function unformat_currency($number, $quotes=false, $field = '', $row = 0, $config = array())
{
    $number = str_replace('.', '', $number);
    $number = str_replace(',', '.', $number);
    if ($quotes) $number = Db::qstr($number);
    return $number;
}

/**
 *
 * @access public
 * @return string
 */
function format_int($number)
{
    // s'haurà de mirar per locales
    $number = number_format((int)$number, 0, ',', '.');
    return $number;
}
function format_int_excel($number)
{
    return $number;
}
function unformat_int($number, $quotes=false, $field = '', $row = 0, $config = array())
{
    // s'haurà de mirar per locales
    $number = str_replace('.', '', $number);
    $number = str_replace(',', '.', $number);
    if ($quotes) $number = Db::qstr($number);
    return $number;
}

function format_datetime($value)
{
	if ($value)
    {
	    $value = explode (' ', $value);
	    $date = format_date_form($value[0]);
	    $ret = $date . ' ' .  format_time($value[1]);
	    if ($ret == ' ') $ret = '';
        return $ret;
        //return $date . ' ' . date("G", strtotime ($value)) . ':' . date("i", strtotime ($value));
    }
    else
    {
        return '';
    }
}
function unformat_datetime($value, $quotes=false, $field = '', $row = 0, $config = array())
{
    if ($value == 'now()') return $value; // es pot posar aquesta funcio SQL directament al valor ja que es molt útil

	// Formulari amb hora en input a part
    if (isset($_POST[$field . '_time'][$row])) {
	    $time = $_POST[ $field . '_time' ][ $row ];
    }
    else {
	    $date_arr = explode( ' ', $value );
	    if ( isset( $date_arr[1] ) ) {
		    $time = $date_arr[1];
		    $value = $date_arr[0];
	    }
	    else {
		    $time = '';
	    }
    }

    $date = unformat_date($value);

	$time = unformat_time($time);
	$datetime = $date . ' ' . $time;

    // Aquest paràmetre només està des de save_rows
    if ($config && $datetime == ' ')
	    $datetime = '0000-00-00';

    if ($quotes) $datetime = Db::qstr($datetime);

    return $datetime;
}

function format_date_list($value)
{
    if ((int)$value != '0' && $value)
    {
        //if ($_ENV['OS']!='Windows_NT'){
		//	return date("d-m-Y",strtotime($value));
		//}
		//else{
			$data = explode(' ', $value);
			$data = $data[0];
			$data_arr = explode('-', $data);
			if (!isset($data_arr[1])) $data_arr = explode('/', $data);
			return $data_arr[2] . '/' . $data_arr[1]. '/' . $data_arr[0];
		//}
    }
    else
    {
        return '';
    }
}

function format_date_form($value)
{
	if ((int)$value != '0' && $value)
    {
        //if ($_ENV['OS']!='Windows_NT'){ ORDINADORS WINDOWS PETEN DATES ANTERIORS A 1900 no se quan
		//	return date("d/m/Y",strtotime($value));
		//}
		//else{
			$data = explode(' ', $value);
			$data = $data[0];
			$data_arr = explode('-', $data);
			if (!isset($data_arr[1])) $data_arr = explode('/', $data);
			return $data_arr[2] . '/' . $data_arr[1]. '/' . $data_arr[0];
		//}
    }
    else
    {
        return '';
    }
}
function unformat_date($value, $quotes=false, $field = '', $row = 0, $config = array())
{
    if ($value == 'now()') return $value; // es pot posar aquesta funcio SQL directament al valor ja que es molt útil
    $date_arr = explode("/", $value);
    if (count($date_arr) != 3) $date_arr = explode("-", $value);
    if (count($date_arr) == 3)
    {
        $date = ($date_arr[2] . "/" . $date_arr[1] . "/" . $date_arr[0]);
    }
    else
    {
        $date = '';
    }

    // Aquest paràmetre només està des de save_rows
    if ($config && !$date)
	    $date = '0000-00-00';

    if ($quotes) $date = Db::qstr($date);
    return $date;
}

function format_time($value)
{
    // TODO-i No s pot posar 24:00, s'hauria de poder
	if ($value != '00:00:00' && $value){
		$output = date("H", strtotime ($value)) . ':' . date("i", strtotime ($value));
		return $output;
    }
    else
    {
        return '';
    }
}

function unformat_time($value, $quotes=false, $field = '', $row = 0, $config = array()) {

	$output = '';
	if ( $value != '' ) {
		$output = date( "H", strtotime( $value ) ) . ':' . date( "i", strtotime( $value ) ) . ':00';
	}

    if ($quotes) $output = Db::qstr($output);
    return $output;
}

function format_minutes($value)
{
	if ($value != '00:00:00' && $value){
		$output = date("i", strtotime ($value)) . ':' . date("s", strtotime ($value));
		return $output;
    }
    else
    {
        return '';
    }
}

function unformat_minutes($value, $quotes=false, $field = '', $row = 0, $config = array()) {

	$output = '';
	if ( $value != '' ) {
		$output = '00:' .$value;
	}

    if ($quotes) $output = Db::qstr($output);
    return $output;
}

function unformat_videoframe( $value, $quotes=false, $field = '', $row = 0, $config = array())
{
	// haig d'afegir <param name="wmode" value="transparent" /> i wmode="transparent"> per que no es solapi amb el visor d'imatges o qualsevol altra capa
	if (strpos($value,'wmode="transparent"')===false) 
		$value = str_replace('></embed>',' wmode="transparent"></embed>',$value);
		
	if (strpos($value,'<param name="wmode" value="transparent"')===false)
		$value = str_replace('<embed src="','<param name="wmode" value="transparent" /><embed src="',$value);
		
	// TODO posar una mida especifica
	
	if ($quotes) $value = Db::qstr($value);
	return $value;
}
function unformat_zero_fill_int( $value, $quotes=false, $field = '', $row = 0, $config = array())
{
	$text_maxlength = $config['text_maxlength'];
	$value = str_pad((string)$value, $text_maxlength, "0", STR_PAD_LEFT);

	if ($quotes) $value = Db::qstr($value);
	return $value;
}

// una manera fàcil de crear un select
// amb el $cg_override puc acabar de posar tots els parametres del config adicionals
//    'select_table' => '',
//    'select_fields' => '',
//    'select_condition' => '',
//		etc....
function get_select ($name, $values, $selected = '', $cg_override=array(), &$caption = ''){

	global $gl_caption;
	$caption=$caption?$caption:$gl_caption;
	
    $select_caption = isset($caption['c_caption_' . $name])?$caption['c_caption_' . $name]:'';
	$cg = array (
	'name' => $name,
	'input_name' => $name,
	'input_language' => '',
    'type' => 'select',
    'enabled' => '1',
    'form_admin' => 'input',
    'form_public' => 'input',
    'list_admin' => 'input',
    'list_public' => 'input',
    'order' => '',
    'default_value' => '',
    'override_save_value' => '',
    'class' => '',
    'javascript' => '',
    'text_size' => '',
    'text_maxlength' => '',
    'textarea_cols' => '',
    'textarea_rows' => '',
	'select_caption' => $select_caption,
    'select_size' => '',
    'select_table' => '',
   	'select_fields' => $values,
    'select_condition' => '',
  	);
	$cg = array_merge($cg, $cg_override);

	$input = new FormObject($cg, $selected, 'form','999');
	$input->caption = $caption;
	$input->is_input = true;
 	return $input->get_input(false);
}

// una manera fàcil de crear un grup de radios
function get_radios ($name, $values, $selected, $cg_override=array(), &$caption = ''){
	global $gl_caption;
	$caption=$caption?$caption:$gl_caption;

    $cg = array (
	'name' => $name,
	'input_name' => $name,
	'input_language' => '',
    'type' => 'radios',
    'enabled' => '1',
    'form_admin' => 'input',
    'form_public' => 'input',
    'list_admin' => 'input',
    'list_public' => 'input',
    'order' => '',
    'default_value' => '',
    'override_save_value' => '',
    'class' => '',
    'javascript' => '',
    'text_size' => '',
    'text_maxlength' => '',
    'textarea_cols' => '',
    'textarea_rows' => '',
	'select_caption' => '',
    'select_size' => '',
    'select_table' => '',
   	'select_fields' => $values,
    'select_condition' => '',
  	);
	$cg = array_merge($cg, $cg_override);

	$input = new FormObject($cg, $selected, 'form','999');
	$input->caption = $caption;
 	return $input->get_input(false);
}

// una manera fàcil de crear un grup de radios
function get_checkbox ($name, $checked, $cg_override=array()){

    $cg = array (
	'name' => $name,
	'input_name' => $name,
	'input_language' => '',
    'type' => 'checkbox',
    'enabled' => '1',
    'form_admin' => 'input',
    'form_public' => 'input',
    'list_admin' => 'input',
    'list_public' => 'input',
    'order' => '',
    'default_value' => '',
    'override_save_value' => '',
    'class' => '',
    'javascript' => '',
    'text_size' => '',
    'text_maxlength' => '',
    'textarea_cols' => '',
    'textarea_rows' => '',
	'select_caption' => '',
    'select_size' => '',
    'select_table' => '',
   	'select_fields' => '',
    'select_condition' => '',
  	);
	$cg = array_merge($cg, $cg_override);

	$input = new FormObject($cg, $checked, 'form','999');
 	return $input->get_input(false);
}