<?
/**
 * ProductCalendar
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) Abril 2017
 * @version $Id$
 * @access public
 *
 *
 * Funcions del calendari per reutilitzar també a public
 *
 */
class ProductCalendar {

	var $products, $one_product, $today_form_date, $tomorrow_form_date, $message_type, $process, $family_id = false, $month_seasons, $today_not_formatted, $is_family_calendar = false;

	public function __construct($process='', $one_product = true, $family_id = false){
		$this->process = $process;
		$this->products = $this->get_family($family_id);
		$this->one_product = $one_product;
	}

	// Mostrar calendari
	public function get_calendar_array( $product_id = false ) {
		global $gl_message;

		$number_months = 3;

		$date = time();

		$today_day = date( 'd', $date );
		$month     = date( 'n', $date );
		$year      = date( 'Y', $date );

		$date     = mktime( 0, 0, 0, $month, 1, $year );
		$week_day = date( 'w', $date );


		if ( $this->message_type != 'error' ) {
			$this->today_form_date    = "$year-$month-" . ( $today_day );
			$this->tomorrow_form_date = "$year-$month-" . ( $today_day + 1 );
		}


		return array(
			'calendar'           => $this->get_calendar( $month, $year, $week_day, $number_months, $product_id ),
			'products'           => $this->products,
			'today_form_date'    => $this->today_form_date,
			'tomorrow_form_date' => $this->tomorrow_form_date,
			'message'            => $gl_message,
			'message_type'       => $this->message_type
		);


	}

	// obtindre mes per ajax
	/**
	 *
	 */
	public function get_month(){
		$month = R::id('month');
		$year = R::id('year');
		$product_id = R::id( 'product_id' );

		$date = mktime(0, 0, 0, $month, 1, $year);
		$week_day = date('w', $date);

		$this->one_product = true;
		$this->products = $this->get_family();

		Debug::p_all();
		die($this->get_calendar($month,$year,$week_day, 1, $product_id));
	}

	// comprovo dies ocupats
	private function get_month_reserves($month,$year,$family_id){

		return Db::get_rows("SELECT familybusy_id as calendar_id, family_id, date_busy
							FROM product__familybusy
							WHERE (
								'$year-$month-1' <= date_busy
								AND LAST_DAY('$year-$month-1') >= date_busy
								)
							AND family_id = $family_id
							ORDER BY date_busy");

	}
	// construeixo el calendari
	private function get_calendar($month,$year,$week_day, $number_months = 3, $product_id = false){

		global $month_names;

		$this->today_not_formatted = $today_not_formatted = date( 'Y/m/d' );


		$max_weeks = 0;
		$calendar = array();

		// bucle mesos
		for ($i = 1; $i <= $number_months; $i++)
		{
			// ho moc a dins bucle -> $reserves = $this->get_month_reserves($month,$year);
			//debug::p($reserves,'reserves');
			$days_in_month = date('t', mktime(0, 0, 0, $month, 1, $year));
			$calendar[$month]['month'] = $month_names[$month-1];
			$calendar[$month]['month_value'] = $month;
			$calendar[$month]['year'] = $year;
			$conta = 1;
			$week = 1;
			foreach ($this->products as $pr)
			{
				$reserves = $this->get_month_reserves($month,$year,$pr['family_id']);
				$p = &$calendar[$month]['products'][$pr['family_id']];
				$p = $pr;
				$week_day_pr = $week_day;
				// empleno començament mes
				if ($this->one_product)
				{
					$week_day_empty = $week_day_pr;
					if ($week_day_empty == 0)
						$week_day_empty = 7;
					while ($conta < $week_day_empty && $week_day_empty != 1)
					{
						$this->one_product?$d = &$p['weeks'][$week]['days'][$conta]:$d = &$p['days'][$conta];
						$d['class1'] = 'empty';
						$d['day'] = $d['onclick1'] = $d['id1'] = '';
						$conta++;
					}
				}
				for ($day = 1; $day <= $days_in_month; $day++, $conta++)
				{
					$this->one_product?$d = &$p['weeks'][$week]['days'][$conta]:$d = &$p['days'][$conta];
					$d['day'] = $day;
					$d['id1'] = '';
					$d['is_weekend'] = ($week_day_pr == 0 || $week_day_pr == 6) ? ' weekend' : '';

					$d['is_occupied'] = $this->get_reservat($reserves, $day, $month, $year,
						$d);

					$class1 = "day" . $d['is_weekend'] . $d['is_occupied'];

					$onclick_action = $product_id ? "calendar[$product_id].do_action" : 'calendar.do_action';
					$set_day_click = "onclick=\"$onclick_action(" . $day . ',' . $month . ',' . $year;


					$d['onclick1'] = $set_day_click;
					$d['onclick1'] .= ");\"";

					if ($d['is_occupied'] == ' occupied')
					{
						$d['class1'] = "day" . $d['is_weekend'] . $d['is_occupied'];
						$d['id1'] .= $d['calendar_id1'];
						if (!$this->is_family_calendar) $d['onclick1'] = ""; // A públic no es fa cap acció quan està ocupat
					}
					else {
						$d['class1'] = $class1;
					}

					$week_day_pr++;
					if ($week_day_pr == 7)
						$week_day_pr = 0;
					if ($week_day_pr == 1)
						$week++;
				}
				// empleno final mes
				if ($this->one_product)
				{
					$week_day_empty = $week_day_pr;
					if ($week_day_empty == 0)
						$week_day_empty = 7;

					while ($week_day_empty < 8)
					{
						$this->one_product?$d = &$p['weeks'][$week]['days'][$conta]:$d = &$p['days'][$conta];
						$d['class1'] = 'empty';
						$d['day'] = $d['onclick1'] = $d['id1'] = '';
						$conta++;
						$week_day_empty++;
						// assumiré que sempre hi ha sis setmanes (almenys els pròxims 4 anys es cert)
						if (($week_day_empty == 8) && ($week < 6))
						{
							$week_day_empty = 1;
							$week++;
						}
					}
					$max_weeks = $max_weeks > ($conta-1) / 7?$max_weeks:($conta-1) / 7;
				}
			}
			$week_day = $week_day_pr; // així el pròxim més continua en aquest dia de la setmana
			$month++;
			if ($month == 13)
			{
				$month = 1;
				$year++;
			} ;
		}

		$tpl = new phemplate(PATH_TEMPLATES, 'product/product_form_calendar.tpl');
		$tpl->set_loop('calendar',$calendar);
		return $tpl->process();

	}
	// Comprobo quines reserves te un dia concret
	//
	// Pot ser:
	//		un dia del mig
	//		el primer dia d'una reserva
	//      l'ultim dia d'una reserva
	//      el primer d'una reserva i l'ultim de l'anterior reserva
	//
	private function get_reservat($reserves, $day, $month, $year, &$d )
	{
		static $count = 0;

		$data = strtotime("$year-$month-$day");

		$d['calendar_id'] = '';

		foreach ($reserves as $v)
		{
			if (strtotime($v['date_busy']) == $data)
			{
				$d['calendar_id1'] = $v['calendar_id'];
				return ' occupied';
			}
		}

		// A públic posa com ocupat, tots els dies anteriors a avui
		if ( ! $GLOBALS['gl_is_admin'] ) {
			$data_not_formatted = "$year/$month/$day";
			if ( is_date_bigger( $this->today_not_formatted, $data_not_formatted ) ) {
				$d['calendar_id1'] = false;
				return ' occupied';
			}
			Debug::p( [
						[ 'today_not_formatted', $this->today_not_formatted ],
						[ 'data_not_formated', $data_not_formatted ],
					], 'calendari');
		}

		return ' free';
	}



	// Això era per poder fer varis, però al fianl només és un tot sol
	private function get_family($family_id = false)
	{
		global $gl_db_max_results;

		if (!$family_id) $family_id = R::id('family_id');

		$query = "SELECT family_id, '' AS checked FROM product__family";
		if ($family_id)
		{
			$query .= " WHERE family_id = '$family_id'";
		}
		// ha d'haver sempre un family_id
		else {
			die();
		}
		$products = Db::get_rows($query);
		$products[0]['checked'] = ' checked';

		Debug::p ( $products, 'products' );

		return $products;
	}



	/*
	 * AQUESTES ESTAVEN A BOOKING_COMMON
	 *
	 *
	 *
	 * Variables comuns a admin i common
	 *
	 *
	 */
	static function set_calendar_vars( &$module, $family_id, $product_id = false ) {
		static $is_initialized = false;

		if ( $product_id ) {

			if ( ! $is_initialized ) {
				$is_initialized = true;

				$GLOBALS['gl_page']->javascript .= "
				gl_message_choose_date = '" . addslashes($module->caption['c_choose_date']) . "';
				calendar = [];
			";
			}

			$GLOBALS['gl_page']->javascript .= "
				calendar[$product_id] = new Calendar($family_id, $product_id);
			";
		}
		else {
			$GLOBALS['gl_page']->javascript .= "
				calendar = new Calendar($family_id);
			";
		}


	}

}