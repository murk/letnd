<?

/**
 * ProductOrderSearch
 *
 * @property array search_vars
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2011
 * @version $Id$
 * @access public
 */
class ProductOrderSearch {

	var $search_vars = array(), $search_fields, $caption, $q = '', $is_excel, $view_format, $action, $fields;

	public function __construct( &$module ) {
		global $gl_caption, $gl_caption_order;
		include( DOCUMENT_ROOT . 'admin/modules/product/languages/' . LANGUAGE . '.php' );
		$this->caption = array_merge( $gl_caption, $gl_caption_order, $module->caption );
		$this->action  = $module->action;
		$this->fields  = $module->fields;
	}

	function get_search_form() {

		$tpl = new phemplate( PATH_TEMPLATES );
		$tpl->set_vars( $this->caption );
		$tpl->set_file( 'product/order_search.tpl' );
		$tpl->set_var( 'menu_id', isset( $GLOBALS['gl_menu_id'] ) ? $GLOBALS['gl_menu_id'] : '' );
		$tpl->set_var( 'action', $this->action == 'list_records_bin' ? 'list_records_bin' : 'list_records' );
		$tpl->set_var( 'is_bin', $this->action == 'list_records_bin' );
		$tpl->set_var( 'q', htmlspecialchars( R::get( 'q' ) ) );

		$tpl->set_vars( $this->search_vars );

		$ret = $tpl->process();

		if ( $this->q ) {
			$GLOBALS['gl_page']->javascript .= "
				$('table.listRecord').highlight('" . addslashes( R::get( 'q' ) ) . "');
			";
		}

		return $ret;
	}


	function init_search_advanced() {

		$this->is_excel    = R::get( 'excel' );

		$view_format = R::escape( 'view_format' );

		// La vista la guardo en cookie
		if ( !$view_format && isset( $_COOKIE['product']['order_view_format'] ) ) {
			$view_format = $_COOKIE['product']['order_view_format'];
		}
		else if (!$view_format) {
			$view_format = 'all';
		}
		setcookie( "product[order_view_format]", $view_format, time() + ( 10 * 365 * 24 * 60 * 60 ), '/' );

		$this->view_format = $view_format;

		$this->set_search_advanced_vars();
	}


	function set_search_advanced_vars() {

		$c = &$this->caption;

		// Variables formulari
		$date_from      = R::escape( 'date_from' );
		$date_to        = R::escape( 'date_to' );
		$order_status   = R::escape( 'order_status', 'null' );
		$send_date      = R::escape( 'send_date', 'null' );
		$payment_method = R::escape( 'payment_method', 'null' );
		$view_format    = $this->view_format;
		$product_id     = R::escape( 'product_id' ); // Utilitzo product_id perque no puc fer servir orderitem_id
		$product_id_add = htmlspecialchars( R::get( 'product_id_add' ) ); // Per l'autocomplete


		// Si he triat mostrar product_ids, només es pot buscar la data a product_id
		$this->$view_format = $view_format;

		$order_status_select = $this->get_filter_status( $order_status );
		$send_date_select = $this->get_filter_send_date( $send_date );

		$c['c_caption_payment_method'] = ' ';
		$payment_method_fields         = $this->fields['payment_method']['select_fields'];
		$payment_method_select         = get_select( 'payment_method', $payment_method_fields, $payment_method, [ 'class' => 'alone' ], $c );


		$vars = array(
			'date_from'             => $date_from,
			'date_to'               => $date_to,
			'view_format'           => $view_format,
			'order_status'          => $order_status,
			'order_status_select'   => $order_status_select,
			'send_date'             => $send_date,
			'send_date_select'      => $send_date_select,
			'payment_method'        => $payment_method,
			'payment_method_select' => $payment_method_select,
			'product_id'            => $product_id,
			'product_id_add'        => $product_id_add,
		);

		$this->search_vars = $vars;

	}

	private function get_filter_status( $status_value = '', $caption = false ) {

		$status = '
			<select class="alone" name="order_status">';

		$status .= '
				<option value="null">' . $caption . '</option>
				<option class="outlined" value="grup-not-payed">' . $this->caption['c_order_status_group_not_payed'] . '</option>
				<option class="outlined" value="grup-payed">' . $this->caption['c_order_status_group_payed'] . '</option>
				<optgroup label="' . $this->caption['c_order_status_group_not_payed'] . '">
				<option value="paying">' . $this->caption['c_order_status_paying'] . '</option>
				<option value="failed">' . $this->caption['c_order_status_failed'] . '</option>
				<option value="pending">' . $this->caption['c_order_status_pending'] . '</option>
				<option value="voided">' . $this->caption['c_order_status_voided'] . '</option>
				<option value="refunded">' . $this->caption['c_order_status_refunded'] . '</option>
				<optgroup label="' . $this->caption['c_order_status_group_payed'] . '">
				<option value="completed">' . $this->caption['c_order_status_completed'] . '</option>
				<option value="preparing">' . $this->caption['c_order_status_preparing'] . '</option>
				<option value="shipped">' . $this->caption['c_order_status_shipped'] . '</option>
				<option value="delivered">' . $this->caption['c_order_status_delivered'] . '</option>
				<option value="collect">' . $this->caption['c_order_status_collect'] . '</option>
			</select>';

		if ( $status_value )
			$status = str_replace( 'value="' . $status_value . '"', 'value="' . $status_value . '" selected', $status );

		return $status;
	}

	private function get_filter_send_date( $send_date_value ) {

		$ret = '
			<select class="alone" name="send_date">';

		$ret .= '
				<option value="null"></option>
				<option value="shipped">' . $this->caption['c_send_date_shipped'] . '</option>
				<option value="delivered">' . $this->caption['c_send_date_delivered'] . '</option>
				<option value="no-date-shipped">' . $this->caption['c_send_date_no_date_shipped'] . '</option>
				<option value="no-date-delivered">' . $this->caption['c_send_date_no_date_delivered'] . '</option>
			</select>';

		if ( $send_date_value )
			$ret = str_replace( 'value="' . $send_date_value . '"', 'value="' . $send_date_value . '" selected', $ret );

		return $ret;
	}

	public function get_search_condition() {

		$search_condition = '';
		$and              = '';

		$search_condition .= $this->get_search_condition_product_id( $and );
		$search_condition .= $this->get_search_condition_order_status( $and );
		$search_condition .= $this->get_search_condition_payment_method( $and );
		$search_condition .= $this->get_search_condition_send_date( $and );

		$search_condition .= $this->get_search_condition_dates( $and );

		$search_condition .= $this->get_search_words_condition( $and );

		if ( $this->view_format == 'orderitems' ) {
			$search_condition .= "
                $and (SELECT product__order.bin FROM product__order WHERE product__order.order_id = product__orderitem.order_id) = 0";
		}

		return $search_condition;
	}

	private function get_search_condition_dates( &$and ) {

		$date_from   = $this->search_vars['date_from'];
		$date_to     = $this->search_vars['date_to'];

		if ( ! $date_from && ! $date_to ) return '';

		$condition = '';
		$field     = 'entered_order';

		if ( $date_from && $date_to ) {

			$condition .= $and . "
					(
						(
							date($field) >= '" . unformat_date( $date_from ) . "' AND
							date($field) <= '" . unformat_date( $date_to ) . "'
						)
					)";
			$and       = ' AND ';

		}
		elseif ( $date_from ) {

			$condition .= $and . "
				(
						date($field) >= '" . unformat_date( $date_from ) . "'
				 )";
			$and       = ' AND ';
		}

		elseif ( $date_to ) {

			$condition .= $and . "
				(
						date($field) <= '" . unformat_date( $date_to ) . "'
						AND 
						UNIX_TIMESTAMP($field) > 0
				 ) ";
			$and       = ' AND ';
		}

		return $condition;

	}

	private function get_search_condition_order_status( &$and ) {

		$order_status = $this->search_vars['order_status'];
		if ( $order_status == 'null' ) return '';

		$old_and = $and;
		if ( $this->view_format == 'orderitems' ) $and = '';

		$condition = '';

		if ( $order_status == 'grup-payed' ) {
			$condition .= $and . "order_status IN ('completed', 'preparing', 'shipped', 'delivered', 'collect')";
		}
		elseif ( $order_status == 'grup-not-payed' ) {
			$condition .= $and . "order_status IN ('paying', 'failed', 'pending', 'denied', 'voided', 'refunded')";

		}
		else {
			$condition .= $and . "order_status = '$order_status'";
		}
		$and = ' AND ';

		if ( $this->view_format == 'orderitems' ) {
			$and       = $old_and;
			$condition = "$and order_id IN ( SELECT order_id FROM product__order WHERE $condition )";
			$and       = ' AND ';
		}


		return $condition;

	}

	private function get_search_condition_payment_method( &$and ) {

		$payment_method = $this->search_vars['payment_method'];
		if ( $payment_method == 'null' ) return '';

		$old_and = $and;
		if ( $this->view_format == 'orderitems' ) $and = '';

		$condition = '';

		$condition .= $and . "payment_method = '$payment_method'";

		$and = ' AND ';

		if ( $this->view_format == 'orderitems' ) {
			$and       = $old_and;
			$condition = "$and order_id IN ( SELECT order_id FROM product__order WHERE $condition )";
			$and       = ' AND ';
		}


		return $condition;

	}

	private function get_search_condition_send_date( &$and ) {

		$send_date = $this->search_vars['send_date'];
		if ( $send_date == 'null' ) return '';

		$old_and = $and;
		if ( $this->view_format == 'orderitems' ) $and = '';

		$condition = '';

		if ( $send_date == 'shipped' ) {
			$condition .= $and . "UNIX_TIMESTAMP(shipped) > 0";
		}
		elseif ( $send_date == 'delivered' ) {
			$condition .= $and . "UNIX_TIMESTAMP(delivered) > 0";
		}
		elseif ( $send_date == 'no-date-shipped' ) {
			$condition .= $and . "UNIX_TIMESTAMP(shipped) = 0";
		}
		elseif ( $send_date == 'no-date-delivered' ) {
			$condition .= $and . "UNIX_TIMESTAMP(delivered) = 0";
		}

		$and = ' AND ';

		if ( $this->view_format == 'orderitems' ) {
			$and       = $old_and;
			$condition = "$and order_id IN ( SELECT order_id FROM product__order WHERE $condition )";
			$and       = ' AND ';
		}


		return $condition;

	}

	private function get_search_condition_product_id( &$and ) {

		$product_id = $this->search_vars['product_id'];
		if ( ! $product_id ) return '';

		$condition = "product_id = '$product_id'";

		if ( $this->view_format != 'orderitems' ) {
			$condition = "order_id IN ( SELECT order_id FROM product__orderitem WHERE $condition )";
		}

		$condition = "$and $condition";

		$and = ' AND ';

		return $condition;

	}

	/*	private function get_search_condition_in_order( &$and, $field ) {

			$value = $this->search_vars[ $field ];
			if ( $value == '' || $value === false ) return '';


			$condition = "$field = '$value'";

			if ( $this->view_format == 'orderitems' ) {
				$condition = "order_id IN ( SELECT order_id FROM product__order WHERE $condition )";
			}

			$condition = "$and $condition";

			$and = ' AND ';


			return $condition;

		}*/


	//
	// BUSCADOR PER PARAULES
	//
	function get_search_words_condition( &$and ) {
		/*

		ORDER
		0 - comment, entered_order, customer_comment
		1 - customer_id
		payment_method ha de ser un desplegable
		2 -source_id
		3- id

		*/
		$is_orderitem = $this->view_format == 'orderitems';

		$q = R::escape( 'q' );
		if ( ! $q ) return '';

		$condition = "
				total_base like '%" . $q . "%'
				OR total_basetax like '%" . $q . "%'
				OR transaction_id like '%" . $q . "%'
				OR comment like '%" . $q . "%'
				OR customer_comment like '%" . $q . "%'
				OR entered_order like '%" . $q . "%'
				";


		// busco a clients
		$query   = "
					SELECT order_id
						FROM product__customer
						INNER JOIN product__order USING (customer_id)
						WHERE (
							name LIKE '%" . $q . "%'
							OR surname LIKE '%" . $q . "%'
							OR CONCAT(TRIM(name), ' ', TRIM(surname)) LIKE '%" . $q . "%'
							OR mail LIKE '%" . $q . "%'
					)
					AND (
						product__customer.bin = 0
					)
			";
		$results = Db::get_rows_array( $query );

		// busco a address
		$query   = "
					SELECT order_id
						FROM product__address
						INNER JOIN product__order USING (customer_id)
						WHERE (
							 a_name LIKE '%" . $q . "%' OR
							 a_surname LIKE '%" . $q . "%' OR
							 address LIKE '%" . $q . "%' OR
							 zip LIKE '%" . $q . "%' OR
							 city LIKE '%" . $q . "%' OR
							 concat(a_name,' ',a_surname) LIKE '%" . $q . "%'
								)";
		$results = array_merge( $results, Db::get_rows_array( $query ) );

		$condition .= $this->get_ids_query( $results, 'order_id' );


		// Per referencia
		$condition = $this->get_ref_query( 'order_id', $q, $condition );

		if ( $is_orderitem ) {
			$condition = "order_id IN (SELECT order_id FROM product__order WHERE $condition)";
		}


		/*

		ORDERITEM
		*/
		$condition .= $this->get_search_words_condition_orderitem( $is_orderitem, $q );


		$condition = "$and ($condition)";
		$and       = ' AND ';

		return $condition;

	}

	private function get_search_words_condition_orderitem( $is_orderitem, $q ) {


		$family_subquery = '
			SELECT product__family_language.family_id FROM product__family_language 
			WHERE family';


		$condition = "
			SELECT product__product.product_id as product_id
			FROM product__product, product__product_language
			WHERE
				        (
				            product__product.product_id = product__product_language.product_id
				            AND 
				            bin = 0
			            ) 
				        AND
				        (
				            product_title LIKE '%$q%'
							OR product_subtitle LIKE '%$q%'
							OR ref LIKE '%$q%'
							OR family_id IN ($family_subquery LIKE '%$q%')
							OR subfamily_id IN ($family_subquery LIKE '%$q%')
						)
		";

		$condition = "
			product_id IN ( $condition )
			OR product_variations LIKE '%$q%'
			OR product_variation_categorys LIKE '%$q%'
			";
		if ( ! $is_orderitem ) {
			$condition = "order_id IN (SELECT order_id FROM product__orderitem WHERE $condition)";
		}

		$condition = "OR $condition";

		return $condition;
	}

	function get_ids_query( &$results, $field ) {
		if ( ! $results ) return '';
		$new_arr = array();
		foreach ( $results as $rs ) {
			$new_arr [ $rs[0] ] = $rs[0];
		}

		return "OR " . $field . " IN (" . implode( ',', $new_arr ) . ") ";
	}

	function get_ref_query( $field, $q, $condition ) {
		//
		// busqueda automatica de referencies separades per espais o comes
		//
		$ref_array = explode( ",", $q ); // si l'array hem dona 1 probo de fer-ho amb espais, si es una sola referència donarà el mateix resultat
		count( $ref_array ) == 1 ? $ref_array = explode( " ", $q ) : false;

		foreach ( $ref_array as $key => $value ) {
			if ( ! is_numeric( $value ) ) {
				$ref_array = '';
				break;
			}
		}

		// consulta
		if ( $ref_array ) {
			$query_ref = "";
			foreach ( $ref_array as $key ) {
				$query_ref .= $field . " = '" . $key . "' OR ";
			}
			$query_ref = "(" . substr( $query_ref, 0, - 3 ) . ") ";

			return '( ' . $query_ref . ' OR ' . $condition . ')';
		}
		else {
			return $condition;
		}
	}

}