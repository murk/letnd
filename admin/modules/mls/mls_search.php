<?php

function save_rows()
{
    $save_rows = new SaveRows;
    $save_rows->save();
}

include(DOCUMENT_ROOT . '/admin/modules/custumer/custumer_funcions.php');
include_once (DOCUMENT_ROOT . '/admin/modules/inmo/inmo_property_functions.php');

// sobreescric taula, idiomes i config
set_table('custumer__demand', 'demand_id');
set_language_vars('inmo', 'property', 'demand');
set_config('custumer/custumer_demand_config.php');
//set_inmo_provinces();



function list_records()
{
    global $gl_action, $gl_content, $listing, $gl_page, $gl_content, $tpl;
	
    //echo "el gl_action és: " . $gl_action . "</br>";
	$mls = true;
    switch ($gl_action) {
        case "list_records_exact": 
		check_exacts_demands($mls);
		case "list_records_prox":
		check_proximity_demands ($mls);
		}
	
	//$listing = new ListRecords;
  	//$listing->list_records();
}

function show_form()
{
    $show = new ShowForm;
    $show->show_form();
}

function write_record()
{
    $writerec = new SaveRows;
    $writerec->save();
}

function manage_images()
{
    $image_manager = new ImageManager;
    $image_manager->execute();
}
?>