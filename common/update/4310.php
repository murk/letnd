<?php
/* 3-9-12 */
// Sitemap, poso el camp de data de modificació a news i page

if (is_table("news__new")){
	$update_sql[]="ALTER TABLE `news__new`
	ADD COLUMN `modified` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00' AFTER `entered`;";
	
	$update_sql[]="UPDATE `news__new` SET `modified`=now()";
}	
if (is_table("all__page")){
	$update_sql[]="ALTER TABLE `all__page`
	ADD COLUMN `modified` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00' AFTER `blocked`;";
	
	$update_sql[]="UPDATE `all__page` SET `modified`=now()";
}		
?>