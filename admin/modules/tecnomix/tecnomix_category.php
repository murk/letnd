<?
/**
 * TecnomixCategory
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2010
 * @version $Id$
 * @access public
 */
class TecnomixCategory extends Module{
	var $list_menu_id, $category_menu_id;
	function __construct(){
		parent::__construct();
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	/* funció que es crida per fer una acció del modul automatica o desde un altre modul ( list_records, show_form ) etc...*/
	function do_action($action = ''){
		switch ($this->parent){		
			case 'TecnomixNewcategory':				
			break;	
			case 'TecnomixInstalaciocategory':
				$this->list_menu_id = 3603;
				$this->category_menu_id = 3607;
			break;	
			case 'TecnomixEquipcategory':
				$this->list_menu_id = 3703;
				$this->category_menu_id = 3707;
			break;	
			case 'TecnomixProjectecategory':	
			break;
		}
		// el camp parent_section nomès el tinc quan creo un registre nou
		// un cop creat el registre no es modifica mai aquest camp, el registre pertany sempre a aquella secció
	    if ($this->action=='add_record') {
			$this->add_field('parent_module'); 
			$this->set_field ('parent_module','override_save_value',"'" . $this->parent . "'"); // sempre sobreescric el parent_module
		}
		return parent::do_action($action);
	}
	function get_records()
	{
		$listing = new ListRecords($this);
		$listing->condition = "parent_module = '" . $this->parent . "'";
		$listing->has_bin = false;
		$listing->order_by = 'ordre asc, category asc';
		$listing->set_options (1, 1, 1, 1, 1, 1, 1, 1, 1); // $options_bar = 1, $options_checkboxes = 1, $save_button = 1, $select_button = 1, $delete_button = 1, $print_button = 1, $edit_buttons = 1, $image_buttons = 1, $split_count = 1
		$listing->add_button ('new_button', 'action=list_records&menu_id='.$this->list_menu_id.'&selected_menu_id='.$this->category_menu_id.'', 'boto1', 'after');
		return $listing->list_records();
	}
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{
	    $show = new ShowForm($this);
		$show->has_bin = false;
	    return $show->show_form();
	}

	function save_rows()
	{
	    $save_rows = new SaveRows($this);
	    $save_rows->save();
	}

	function write_record()
	{
	    $writerec = new SaveRows($this);
	    $writerec->save();
	}

	function manage_images()
	{
	    $image_manager = new ImageManager($this);
	    $image_manager->execute();
	}
}
?>