<?php
// menus eina
if (!defined('PRODUCT_MENU_PRODUCT')){
define('PRODUCT_MENU_PRODUCT','Products');
define('PRODUCT_MENU_NEW','Create product');
define('PRODUCT_MENU_LIST','List products');
define('PRODUCT_MENU_LIST_BIN','Recycling bin');
define('PRODUCT_MENU_FAMILY','Families');
define('PRODUCT_MENU_FAMILY_NEW','Create family');
define('PRODUCT_MENU_FAMILY_LIST','List famlies');
define('PRODUCT_MENU_FAMILY_LIST_BIN','Recycling bin');
define('PRODUCT_MENU_BRAND','Brands');
define('PRODUCT_MENU_BRAND_NEW','Create brand');
define('PRODUCT_MENU_BRAND_LIST','List brands');
define('PRODUCT_MENU_BRAND_LIST_BIN','Recycling bin');
define('PRODUCT_MENU_TAX','Taxes');
define('PRODUCT_MENU_TAX_NEW','Create tax');
define('PRODUCT_MENU_TAX_LIST','List tax');
define('PRODUCT_MENU_TAX_LIST_BIN','Recycling bin');
//--------------- a traduir -----------------
define('PRODUCT_MENU_COLOR','Colors');
define('PRODUCT_MENU_COLOR_NEW','Create color');
define('PRODUCT_MENU_COLOR_LIST','List colors');
define('PRODUCT_MENU_COLOR_LIST_BIN','Recycling bin');
define('PRODUCT_MENU_MATERIAL','Materials');
define('PRODUCT_MENU_MATERIAL_NEW','Create material');
define('PRODUCT_MENU_MATERIAL_LIST','List materials');
define('PRODUCT_MENU_MATERIAL_LIST_BIN','Recycling bin');
define('PRODUCT_MENU_VARIATION','Price variations');
define('PRODUCT_MENU_VARIATION_NEW','Create variation');
define('PRODUCT_MENU_VARIATION_LIST','List variations');
define('PRODUCT_MENU_VARIATION_DEFAULT','Default variations');
define('PRODUCT_MENU_VARIATION_CATEGORY','Variations categories');
define('PRODUCT_MENU_VARIATION_CATEGORY_NEW','Create Category');
define('PRODUCT_MENU_VARIATION_CATEGORY_LIST','List Categories');
define('PRODUCT_MENU_VARIATION_LIST_BIN','Recycling bin');

define('PRODUCT_MENU_PROMCODE','Promotional codes');
define('PRODUCT_MENU_PROMCODE_NEW','Create code');
define('PRODUCT_MENU_PROMCODE_LIST','List codes');
define('PRODUCT_MENU_PROMCODE_LIST_BIN','Recycling bin');

define('PRODUCT_MENU_SECTOR','Sectores');
define('PRODUCT_MENU_SECTOR_NEW','Crear sector');
define('PRODUCT_MENU_SECTOR_LIST','Listar sectors');
define('PRODUCT_MENU_SECTOR_LIST_BIN','Papelera');

define('SELLPOINT_MENU_SELLPOINT','Puntos de venta');
define('SELLPOINT_MENU_SELLPOINT_NEW','Nuevo punto');
define('SELLPOINT_MENU_SELLPOINT_LIST','Listar puntos');
define('SELLPOINT_MENU_SELLPOINT_BIN','Papelera de reciclage');

define('PRODUCT_MENU_SPEC','Specifications');
define('PRODUCT_MENU_SPEC_NEW','Create specification');
define('PRODUCT_MENU_SPEC_LIST','List specification');
define('PRODUCT_MENU_SPEC_CATEGORY','Categories specifications');
define('PRODUCT_MENU_SPEC_CATEGORY_NEW','Create category');
define('PRODUCT_MENU_SPEC_CATEGORY_LIST','List categories');
define('PRODUCT_MENU_SPEC_LIST_BIN','Recycling bin');

define('PRODUCT_MENU_QUANTITYCONCEPT','Concepts quantity');
define('PRODUCT_MENU_QUANTITYCONCEPT_NEW','New concept');
define('PRODUCT_MENU_QUANTITYCONCEPT_LIST','List concepts');
}



$gl_caption['c_images_variation_title']='Variation';
$gl_caption_color['c_color_name']='Color name';
$gl_caption['c_color_id']='Color';
$gl_caption['c_material_id']='Material';
$gl_caption['c_choose']='Choose';
$gl_caption_product['c_temperature_unit']='';
$gl_caption_product['c_util_life_unit']='';
$gl_caption_product['c_others1']='';
$gl_caption_product['c_others2']='';
$gl_caption_product['c_others3']='';
$gl_caption_product['c_others4']='';
$gl_caption_product['c_auto_ref']='Automatic';

$gl_caption_product['c_format']='Format';
$gl_caption_product['c_weight']='Weight';
$gl_caption_product['c_gram']='gram';
$gl_caption_product['c_kilo']='kilo/s';
$gl_caption_product['c_weight_unit']='';
$gl_caption_product['c_height_unit']='';
$gl_caption_product['c_height']='Height';
$gl_caption_product['c_mm']='mm';
$gl_caption_product['c_cm']='cm';
$gl_caption_product['c_mt']='mt';
$gl_caption_product['c_km']='km';
$gl_caption_product['c_feet']='feet';
$gl_caption_product['c_temperature']='Temperature';
$gl_caption_product['c_celcius']='celcius';
$gl_caption_product['c_fahrenheit']='fahrenheit';
$gl_caption_product['c_util_life']='Util life';
$gl_caption_product['c_hours']='hours';
$gl_caption_product['c_days']='days';
$gl_caption_product['c_years']='years';
$gl_caption_product['c_meters']='meters';
$gl_caption_product['c_km']='kilometers';
$gl_caption_product['c_months']='months';
$gl_caption_product['c_caracteristics']='Characteristics';
$gl_caption_product['c_other_caracteristics']='Others characteristics - You can configurate the name of this characteristics at the configuration menu -';
$gl_caption_product['c_power']='Power';
$gl_caption_product['c_alimentacion']='Power supply';
$gl_caption_product['c_certification']='Certification/s';
$gl_caption_material['c_material']='Material name';


//-----------------------------------
$gl_caption['c_ref'] = 'Ref';
$gl_caption_product['c_home']='Highlight';
$gl_caption_product['c_ref_number'] = 'Automatic code';
$gl_caption_product['c_ref_supplier'] = 'Supplier Code';
$gl_caption_product['c_product_private'] = 'Private name';
$gl_caption['c_family_id'] = 'Family';
$gl_caption_product['c_subfamily_id'] = 'Subfamily';
$gl_caption['c_family_content_list'] = 'Short description on listing';
$gl_caption['c_family_description']='Family description';
$gl_caption_product['c_subfamily_id_format']='#%s#%s';
$gl_caption_product['c_subsubfamily_id'] = 'Subfamily';
$gl_caption_product['c_subsubfamily_id_format']='#%s#%s';
$gl_caption_product['c_pvp'] = 'PVP';
$gl_caption_product['c_price_cost'] = 'Cost price';
$gl_caption['c_price'] = 'Price';
$gl_caption_product['c_destacat']='Featured';
$gl_caption_product['c_pvd'] = 'PVD';
$gl_caption_product['c_pvd1'] = 'Rate 1';
$gl_caption_product['c_pvd2'] = 'Rate 2';
$gl_caption_product['c_pvd3'] = 'Rate 3';
$gl_caption_product['c_pvd4'] = 'Rate 4';
$gl_caption_product['c_pvd5'] = 'Rate 5';
$gl_caption_product['c_price_consult'] = 'Price on application';
$gl_caption_product['c_price_consult_1'] = 'Price on application';
$gl_caption_product['c_brand_id'] = $gl_caption_product['c_brand'] = 'Brand';
$gl_caption_product['c_sell'] = 'Web sell?';
$gl_caption['c_guarantee']='Warranty';
$gl_caption_product['c_guarantee_period']='Warranty period';
$gl_caption_product['c_da']='days';
$gl_caption_product['c_mo']='months';
$gl_caption_product['c_yr']='years';
$gl_caption['c_status'] = 'Status';
$gl_caption_product['c_free_send'] = 'Allow free send';
$gl_caption_product['c_ordre'] = 'Order';
$gl_caption['c_product_title'] = 'Product name';
$gl_caption['c_product_subtitle'] = 'Subtitle';
$gl_caption_product['c_product_description'] = 'Product description';
$gl_caption['c_prepare'] = 'Being prepared';
$gl_caption['c_review'] = 'Review';
$gl_caption['c_onsale'] = 'Public';
$gl_caption['c_nostock'] = 'no stock';
$gl_caption['c_nocatalog'] = 'Discontinued';
$gl_caption['c_archived'] = 'Archived';
$gl_caption['c_tax_id'] = 'TAX (%)';
$gl_caption['c_tax_included'] = 'TAX included';
$gl_caption['c_tax_not_included'] = 'TAX not included';
$gl_caption_product['c_offer'] = 'Offer';
$gl_caption_product['c_discount'] = 'Discount (%)';
$gl_caption_product['c_discount_wholesaler'] = 'Discount wholesaler(%)';
$gl_caption_product['c_margin'] = 'Margin (%)';
$gl_caption_product['c_ean'] = 'EAN';
$gl_caption_product['c_price_unit'] = 'Price by';
$gl_caption_product['c_unit'] = 'Unit';
$gl_caption_product['c_ten'] = 'Ten';
$gl_caption_product['c_hundred'] = 'Hundred';
$gl_caption_product['c_thousand'] = 'Thousand';
$gl_caption_product['c_couple'] = 'Couple';
$gl_caption_product['c_dozen'] = 'Doxen';
$gl_caption_product['c_m2'] = 'm2';
$gl_caption_product['c_data']='Product information';
$gl_caption_product['c_general_data']='General information';
$gl_caption_product['c_descriptions']='Description';
$gl_caption_product['c_description']='Description';
$gl_caption_product['c_data_product']='Product information';
$gl_caption_product['c_data_web']='Web sell';
$gl_caption_product['c_stock']='Stock';
$gl_caption_product['c_sells']='Units sell';
$gl_caption_product['c_file']='Public Files';
$gl_caption_product['c_discount_fixed_price']='Discount by fixed price';
$gl_caption_product['c_discount_fixed_price_wholesaler']='Discount by fixed price wholesaler';
$gl_caption_product['c_selected']='Selected products';
$gl_caption_product['c_videoframe']='Video (youtube, metacafe...)';


$gl_caption['c_observations'] = 'Comments';
$gl_caption['c_check_parent_id']='Choose family';
$gl_caption['c_check_brand_id']='Choose brand';




$gl_caption['c_brand_name']='Brand name';
$gl_caption_family['c_parent_id']='Family it belongs to';
$gl_caption_family['c_family']='Family name';
$gl_caption_family['c_family_alt']='Family "title/alt"';
$gl_caption_family['c_family_subtitle'] = 'Subtitle';
$gl_caption_family['c_view_subfamilys']='view families';
$gl_caption_family['c_add_subfamily_button']=$gl_caption_family['c_add_subfamily']='Add subfamily';
$gl_caption_family['c_filter_parent_id'] = 'Any family';
$gl_caption_family['c_search_title'] = 'Search families';




$gl_caption_tax['c_tax_name']='Tax name';
$gl_caption_tax['c_tax_value']='Value (%)';
$gl_caption_tax['c_equivalencia']='Equivalencia (%)';




$gl_caption_product['c_search_title']= 'Product search';
$gl_caption_product['c_by_nif']= 'By code';
$gl_caption_product['c_by_words']= 'By words';


$gl_caption['c_observations'] = 'Comments';
$gl_caption['c_ordre']='Order';
$gl_caption['c_withinstall']='Price with installation';

$gl_caption ['c_noproduct']= $gl_messages_product ['no_records']= 'There are no products available right now.';
$gl_caption_product['c_model']='Model';
$gl_caption['c_year']='Year';
$gl_caption['c_ishandmade']='Handmade';



// No traduit
if(!defined( 'CUSTOMER_MENU_CUSTOMER' )){
	define( 'CUSTOMER_MENU_CUSTOMER', 'Clients' );
	define( 'PRODUCT_MENU_CUSTOMER', 'Clients' );
	define( 'CUSTOMER_MENU_CUSTOMER_NEW', 'Insert client' );
	define( 'CUSTOMER_MENU_CUSTOMER_LIST', 'List clients' );
	define( 'CUSTOMER_MENU_CUSTOMER_BIN', 'Recycling bin' );

	define( 'CUSTOMER_MENU_ORDER', 'Orders' );
	define( 'PRODUCT_MENU_ORDER', 'Orders' );
	define( 'CUSTOMER_MENU_ORDER_NEW', 'New order' );
	define( 'CUSTOMER_MENU_ORDER_LIST', 'List orders' );
	define( 'CUSTOMER_MENU_ORDER_LIST_BIN', 'Recycling bin' );

	define( 'CUSTOMER_MENU_WHOLESALER', 'Retailer' );
	define( 'PRODUCT_MENU_WHOLESALER', 'Retailers' );
	define( 'CUSTOMER_MENU_WHOLESALER_NEW', 'Insert retailer' );
	define( 'CUSTOMER_MENU_WHOLESALER_LIST', 'List retailer' );
	define( 'CUSTOMER_MENU_WHOLESALER_BIN', 'Recycling bin' );

	define( 'CUSTOMER_MENU_RATE', 'Tarifas envio' );
	define( 'PRODUCT_MENU_RATE', 'Tarifas envio' );
	define( 'CUSTOMER_MENU_RATE_NEW', 'Add country' );
	define( 'CUSTOMER_MENU_RATE_PROVINCIA_NEW', 'Add province' );
	define( 'CUSTOMER_MENU_RATE_LIST', 'Listar tarifas' );
	define( 'PRODUCT_MENU_COUNTRY', 'Country selection' );
	define( 'CUSTOMER_MENU_COUNTRY_LIST', 'Country selection' );

	define( 'CUSTOMER_MENU_COURIER', 'Messenger' );
	define( 'PRODUCT_MENU_COURIER', 'Messengers' );
	define( 'CUSTOMER_MENU_COURIER_NEW', 'Create messenger' );
	define( 'CUSTOMER_MENU_COURIER_LIST', 'List messengers' );
	define( 'CUSTOMER_MENU_COURIER_LIST_BIN', 'Bin' );
}

$gl_caption ['c_quantity']='Amount';
$gl_caption ['c_product_title']='Product';
$gl_caption ['c_product_tax']='V.A.T.'; // això ha d'anar a la taula de productes
$gl_caption ['c_product_basetax']='Unit/price';
$gl_caption ['c_product_base']='Unit/price';
$gl_caption ['c_product_total_base']='Total';
$gl_caption ['c_product_total_tax']='V.A.T.'; // això ha d'anar a la taula de productes
$gl_caption ['c_total_equivalencia']='Equalization tax';
$gl_caption ['c_product_total_basetax']='Total';
$gl_caption ['c_total']='Total';
$gl_caption ['c_total_base']='Subtotal';
$gl_caption ['c_total_tax']='I.V.A'; // això ha d'anar a la taula de productes
$gl_caption ['c_total_basetax']='Total';
$gl_caption ['c_rate_basetax']='Shipping cost';
$gl_caption ['c_rate_base']='';
$gl_caption ['c_rate_tax']='';
$gl_caption ['c_deposit']='Deposit';
$gl_caption ['c_deposit_rest']='Remaining amount';
$gl_caption ['c_all_basetax']='Total';
$gl_caption ['c_all_base'] = 'Tax Base';
$gl_caption ['c_all_tax']='V.A.T.';
$gl_caption ['c_all_equivalencia']='Surcharge equivalence';
$gl_caption ['c_promcode_basetax']='Promotional code discount';
$gl_caption['c_product_old_base'] = "";
$gl_caption['c_product_old_basetax'] = "";
$gl_caption['c_product_discount'] = "Discount";
$gl_caption['c_product_discount_fixed'] = "";

$gl_caption_customer['c_prefered_language'] = 'Language';
$gl_caption_customer['c_form_title'] = 'Customer data';
$gl_caption_customer['c_customer_id'] = 'Num.';
$gl_caption_customer['c_customer_id_title'] = 'Number';
$gl_caption_customer['c_treatment'] = 'Treatment';
$gl_caption_customer['c_name'] = 'Name';
$gl_caption_customer['c_company']='Company';
$gl_caption_customer['c_surname'] = 'Surname';
$gl_caption_customer['c_birthdate'] = 'Birth date';
$gl_caption_customer['c_mail'] = "E-mail";
$gl_caption_customer['c_password'] = 'Password';
$gl_caption_customer['c_password_repeat'] = 'Repeat password';
$gl_caption['c_telephone'] = 'Phone';
$gl_caption_customer['c_telephone_mobile'] = 'Cellphone';
$gl_caption['c_has_equivalencia'] = 'Surcharge equivalence';
$gl_caption['c_has_no_vat'] = 'Do not add VAT';
$gl_caption_customer['c_wholesaler_rate'] = 'Rate';
$gl_caption_customer['c_wholesaler_rate_1'] = 'Rate 1';
$gl_caption_customer['c_wholesaler_rate_2'] = 'Rate 2';
$gl_caption_customer['c_wholesaler_rate_3'] = 'Rate 3';
$gl_caption_customer['c_wholesaler_rate_4'] = 'Rate 4';
$gl_caption_customer['c_wholesaler_rate_5'] = 'Rate 5';
$gl_caption['c_dni'] = 'ID card';
$gl_caption_customer['c_comment'] = 'Comments';
$gl_caption_customer['c_bin'] = '';
$gl_caption_customer['c_enteredc'] = "Entry date";
$gl_caption['c_treatment_sra'] = "Mrs.";
$gl_caption['c_treatment_sr'] = "Mr.";
$gl_caption ['c_same_address']='Same billing direction';
$gl_caption_customer['c_list_orders'] = 'See orders';
$gl_caption_customer ['c_form_title2']='Login data';
$gl_caption_customer ['c_form_title3']='Billing data';
$gl_caption_customer ['c_form_title3_list']='Billing data';
$gl_caption_customer ['c_form_title_wholesaler']='Distribuidor';
$gl_caption_customer ['c_form_title4']='Delivery address';
$gl_caption_customer['c_add_button'] = 'Add direction';

$gl_caption['c_order_id'] = $gl_caption['c_order'] = 'Order';
$gl_caption_order['c_customer_id'] = 'Customer';
$gl_caption_order['c_total_base'] = 'Tax Base';
$gl_caption_order['c_total_tax'] = 'I.V.A.';
$gl_caption_order['c_total_basetax'] = 'Total';
$gl_caption_order['c_entered_order'] = 'Order date';
$gl_caption_order['c_entered_order_short'] = 'Date';
$gl_caption_order['c_customer_id'] = 'Customer';
$gl_caption_order['c_product_id'] = 'Product';
$gl_caption_order['c_shipped'] = 'Shipping date';
$gl_caption_order['c_delivered'] = 'Delivery date';
$gl_caption_order['c_order_status'] = 'Status';
$gl_caption_order['c_deposit_payment_method'] = 'Payment method deposit';
$gl_caption_order_list['c_deposit_payment_method'] = 'Payment m. deposit';
$gl_caption_order['c_payment_method'] = 'Payment method';
$gl_caption_order_list['c_payment_method'] = 'Payment m.';
$gl_caption_order['c_comment'] = 'Private comments';
$gl_caption['c_customer_comment'] = 'Customer comments';
$gl_caption['c_is_gift_card'] = 'Gift card';
$gl_caption_order['c_address_invoice_id'] = '';
$gl_caption_order['c_filter_order_status'] = 'All status';
$gl_caption_order['c_filter_payment_method'] = 'All method';
$gl_caption_order['c_customer_id_format'] = '%s %s';
$gl_caption_order['c_file'] = 'Public files';
$gl_caption_order['c_button-edit-admin'] = '';


$gl_caption_order['c_order_status_paying'] = 'Outstanding';
$gl_caption_order['c_order_status_failed'] = 'Payment failed';
$gl_caption_order['c_deposit_payed'] = 'Deposit payed';
$gl_caption_order['c_order_status_pending'] = 'Pending payment to be accepted';
$gl_caption_order['c_order_status_denied'] = 'Pagament rebutjat';
$gl_caption_order['c_order_status_voided'] = 'Payment canceled';
$gl_caption_order['c_order_status_refunded'] = 'Payment canceled';
$gl_caption_order['c_order_status_completed'] = 'Completed payment';
$gl_caption_order['c_order_status_preparing'] = 'Being prepared';
$gl_caption_order['c_order_status_shipped'] = 'Shipped';
$gl_caption_order['c_order_status_delivered'] = 'Delivered';
$gl_caption_order['c_order_status_collect'] = 'Pending pick up';

$gl_caption_order['c_deposit_status'] = 'Status deposit';

$gl_caption_order['c_deposit_status_paying'] = 'Outstanding';
$gl_caption_order['c_deposit_status_failed'] = 'Payment failed';
$gl_caption_order['c_deposit_status_deposit'] = 'Deposit payed';
$gl_caption_order['c_deposit_status_pending'] = 'Pending payment to be accepted';
$gl_caption_order['c_deposit_status_denied'] = 'Pagament rebutjat';
$gl_caption_order['c_deposit_status_voided'] = 'Payment canceled';
$gl_caption_order['c_deposit_status_refunded'] = 'Payment canceled';
$gl_caption_order['c_deposit_status_completed'] = 'Completed payment';

$gl_caption_order['c_deposit_rest'] = 'Rest';

// Enviament
$gl_caption['c_send_date'] = "Deliver";
$gl_caption['c_send_date_shipped'] = $gl_caption_order['c_shipped'];
$gl_caption['c_send_date_delivered'] = $gl_caption_order['c_delivered'];
$gl_caption['c_send_date_no_date_shipped'] = 'No shipped date';
$gl_caption['c_send_date_no_date_delivered'] = 'No delivered date';

$gl_caption['c_payment_method_none'] = 'None';
$gl_caption['c_payment_method_paypal'] = 'Paypal';
$gl_caption['c_payment_method_account'] = 'Bank transfer';
$gl_caption['c_payment_method_ondelivery'] = 'On delivery';
$gl_caption['c_payment_method_directdebit'] = 'Direct debit';
$gl_caption['c_payment_method_4b'] = 'Credit card (4B)';
$gl_caption['c_payment_method_lacaixa'] = 'Credit card';
$gl_caption['c_payment_method_cash'] = 'Cash';
$gl_caption['c_payment_method_physicaltpv'] = 'Physical TPV';

$gl_caption['c_deposit_payment_method_none'] = 'None';
$gl_caption['c_deposit_payment_method_paypal'] = $gl_caption['c_payment_method_paypal'];
$gl_caption['c_deposit_payment_method_account'] = $gl_caption['c_payment_method_account'];
$gl_caption['c_deposit_payment_method_ondelivery'] = $gl_caption['c_payment_method_ondelivery'];
$gl_caption['c_deposit_payment_method_directdebit'] = $gl_caption['c_payment_method_directdebit'];
$gl_caption['c_deposit_payment_method_4b'] = $gl_caption['c_payment_method_4b'];
$gl_caption['c_deposit_payment_method_lacaixa'] = $gl_caption['c_payment_method_lacaixa'];
$gl_caption['c_deposit_payment_method_cash'] = $gl_caption['c_payment_method_cash'];
$gl_caption['c_deposit_payment_method_physicaltpv'] = $gl_caption['c_payment_method_physicaltpv'];


$gl_caption['c_collect'] = 'Collect on physical shop';
$gl_caption_list['c_collect'] = 'Collect';
$gl_caption_order['c_edit_button'] = 'Editr order';
$gl_caption_order['c_show_record'] = 'See order';
$gl_caption_order['c_show_invoice'] = 'See bill';
$gl_caption_order['c_show_invoice_refund'] = 'See refund bill';
$gl_caption_order['c_pay_now'] = 'Pay now';
$gl_caption_order['c_no_shipped'] = 'Order no shipped';
$gl_caption_order['c_no_delivered'] = 'Order no delivered';


$gl_caption_order ['c_order_title_admin']='Order information';
$gl_caption_order ['c_order_title']='Order information';
$gl_caption_order ['c_order_title2']='Products';
$gl_caption_order ['c_order_title3']='Billing adress';
$gl_caption_order ['c_order_title4']='Delivery adress';
$gl_caption_order ['c_order_title5']='Payment method';
$gl_caption_order ['c_order_title_invoice_customer']='Customer information';
$gl_caption_order ['c_order_num']='num. ';
$gl_caption ['c_product_variation_ids']='';
$gl_caption ['c_product_variations']='Variations';
$gl_caption ['c_product_variation_categorys']='';

$gl_caption_order['c_invoice_id'] = 'Bill';
$gl_caption_order['c_invoice_refund_id'] = 'Refund Bill';
$gl_caption_order['c_invoice_date'] = 'Date';
$gl_caption_order['c_invoice_refund_date'] = 'Date';
$gl_caption_order['c_view_invoice'] = 'View bill';
$gl_caption_order['c_view_invoice_refund'] = 'View refund bill';
$gl_caption_order ['c_order_title_invoice_customer']='Customer data';

$gl_caption_orderitem = $gl_caption_order;
$gl_caption_orderitem_list = $gl_caption_order_list;

$gl_caption ['c_address_id']='';
$gl_caption ['c_delivery_id']='';
$gl_caption_address ['c_customer_id']='';
$gl_caption_address['c_a_name'] = 'Name';
$gl_caption_address['c_a_surname'] = 'Surname';
$gl_caption_address['c_address'] = 'Address';
$gl_caption_address['c_zip'] = 'Zip';
$gl_caption_address['c_city'] = 'City';
$gl_caption_address['c_country_id'] = 'Country';
$gl_caption_address['c_invoice'] = '';
$gl_caption_address['c_edit_address'] = 'Edit address';
$gl_caption_address['c_new_address'] = 'Add address';

$gl_caption_variation['c_variation'] = 'Variation';
$gl_caption_variation['c_variation_subtitle'] = 'Subtitle';
$gl_caption_variation['c_variation_description'] = 'Description';
$gl_caption_variation['c_list_pick_default'] = 'Default';
$gl_caption_variation['c_variation_category_id'] = 'Category';
$gl_caption_variation['c_variation_status'] = 'Status';
$gl_caption['c_disabled'] = 'Disabled';
$gl_caption_variation['c_variation_pvp'] = 'Increase PVP';
$gl_caption_variation['c_variation_pvd'] = 'Increase PVD 1';
$gl_caption_variation['c_variation_pvd2'] = 'Increase PVD 2';
$gl_caption_variation['c_variation_pvd3'] = 'Increase PVD 3';
$gl_caption_variation['c_variation_pvd4'] = 'Increase PVD 4';
$gl_caption_variation['c_variation_pvd5'] = 'Increase PVD 5';
$gl_caption_variation_category['c_variation_category'] = 'Category';
$gl_caption_variation_category['c_variation_description'] = 'Description';
$gl_caption_product['c_edit_variations'] = $gl_caption_product['c_edit_variations_button'] = 'Variations';
$gl_caption_product['c_edit_specs'] = $gl_caption_product['c_edit_specs_button'] = 'Specs';
$gl_caption_product['c_edit_relateds'] = $gl_caption_product['c_edit_relateds_button'] = 'Related';
$gl_caption['c_no_assigned'] = 'There is no defined variation';
$gl_caption_product['c_data_variations'] = 'Variations of product';
$gl_caption_product['c_price_base'] = 'Base price of product';
$gl_caption['c_data_digital'] = 'Digital Product';
$gl_caption_product['c_download'] = 'Downloadable files';
$gl_caption_product['c_filter_family_id'] = 'All families';
$gl_caption_product['c_filter_subfamily_id'] = 'All subfamilies';
$gl_caption_product['c_filter_subsubfamily_id'] = 'All subfamilies';
$gl_caption_product['c_filter_status'] = 'All states';
$gl_caption_product['c_filter_destacat'] = 'Destacados / No destacados';
$gl_caption_product['c_destacat_0'] = 'No destacats';
$gl_caption_product['c_destacat_1'] = 'Destacats';
$gl_caption_product['c_filter_prominent'] = 'En Promoción / No promoción';
$gl_caption_product['c_prominent_0'] = 'No promoción';
$gl_caption_product['c_prominent_1'] = 'En promoción';
$gl_caption_product['c_stockcontrol']='Control de stock';
$gl_caption_product['c_last_stock_units_public']='Aviso últimas unidades público';
$gl_caption_product['c_last_stock_units_admin']='Aviso últimas unidades admin';
$gl_caption_brand['c_brand_description']='Descripción';
$gl_caption['c_prominent']='Promoted';
$gl_caption_list['c_prominent']='Prom.';
$gl_caption_product['c_is_new_product']='Producto nuevo';
$gl_caption_product_list['c_is_new_product']='Nuevo';
$gl_caption_variation['c_variation_stock']='Stock';
$gl_caption_variation['c_variation_last_stock_units_public']='Aviso público';
$gl_caption_variation['c_variation_last_stock_units_admin']='Aviso admin';
$gl_caption_variation['c_ref']='Ref';
$gl_caption_variation['c_ref_supplier']='Ref Proveedor';
$gl_caption_variation['c_variation_ref']='Ref';
$gl_caption_variation['c_variation_ref_number']='Ref. Automática';
$gl_caption_variation['c_variation_ref_supplier']='Ref Proveedor';

$gl_caption['c_product_file_name'] = $GLOBALS['gl_caption']['c_file_name'];
$gl_caption['c_product_old_file_name'] = $GLOBALS['gl_caption']['c_old_file_name'];

$gl_caption_variation['c_variation_sells']='Unidades vendidas';
$gl_caption['c_add_variations']='Añadir variaciones';
$gl_caption_variation['c_selected']='Variaciones para añadir';
$gl_caption['c_new_variations']='Variaciones añadidas';
$gl_caption['c_variation_category_select']='Todas las categorias';
$gl_caption_variation['c_filter_variation_category_id']='Todas las categorias';


$gl_caption['c_send_password'] = 'Send password by email';
$gl_caption['c_password_sent'] = 'It has sent a new password to your email address';
$gl_caption['c_password_mail_subject'] = 'Your new password';
$gl_caption['c_password_mail_text_1'] = 'Your new password to acces our shop is';
$gl_caption['c_password_mail_text_2'] = 'You can change your password throught "My account" from our website';
$gl_messages_customer['password_sent'] = 'The password has been sent to the customer';
$gl_messages_customer['password_not_sent'] = 'The password couldn\'t be sent, plis click "send password by email"';

$gl_caption_customer['c_activate_button'] = 'Activate customer';
$gl_caption_customer['c_deactivate_button'] = 'Desactivate customer';
$gl_caption_customer['c_set_wholesaler_button'] = 'Change to wholesaler';
$gl_caption_customer['c_unset_wholesaler_button'] = 'Treure distribuïdor';
$gl_caption_customer['c_activated_mail_message'] = 'Your account at %s has been activated, now you can acces our shop.';
$gl_caption_customer['c_activated_mail_subject'] = 'Your account has been activated';
$gl_caption_customer['c_filter_activated'] = 'Activated / Deactivated';
$gl_caption_customer['c_activated_0'] = 'Deactivated';
$gl_caption_customer['c_activated_1'] = 'Activated';

$gl_messages_customer['activated'] = 'The client has been activated';
$gl_messages_customer['deactivated'] = 'The client has been deactivaed';
$gl_messages_customer['not_activated'] = 'The client could not be activated, please try again';
$gl_messages_customer['set_wholesaler'] = 'The client is now retailer';
$gl_messages_customer['unset_wholesaler'] = 'The client is no longer retailer';

$gl_caption_family['c_destacat']='Destacada';

$gl_caption['c_promcode'] = 'Promotional code';
$gl_caption_promcode['c_discount_pvp'] = 'Discount PVP';
$gl_caption_promcode['c_discount_pvd'] = 'Discount fare 1';
$gl_caption_promcode['c_discount_pvd2'] = 'Discount fare 2';
$gl_caption_promcode['c_discount_pvd3'] = 'Discount fare 3';
$gl_caption_promcode['c_discount_pvd4'] = 'Discount fare 4';
$gl_caption_promcode['c_discount_pvd5'] = 'Discount fare 5';
$gl_caption_promcode['c_discount_type'] = 'Discount type';
$gl_caption_promcode['c_discount_type_fixed'] = 'Fixed ampount';
$gl_caption_promcode['c_discount_type_percent'] = 'Percentage';
$gl_caption_promcode['c_minimum_amount'] = 'Minimum amount';
$gl_caption_promcode['c_start_date'] = 'Initial date';
$gl_caption_promcode['c_end_date'] = 'Final date';
$gl_caption['c_promcode_discount'] = 'Discount promotional code';

$gl_caption['c_sector']='Sector';
$gl_caption['c_sector_id']='Sector';
$gl_caption_sector['c_sector_description']='Description';


$gl_caption_brand['c_send_rate_spain']='Spain';
$gl_caption_brand['c_send_rate_france']='France';
$gl_caption_brand['c_send_rate_andorra']='Andorra';
$gl_caption_brand['c_send_rate_world']='Rest of the world';
$gl_caption['c_minimum_buy_free_send']='Minimum purchase';

/*  SELLPOINT  */
/*--------------- nomès castellà i català, no traduit a la resta -------------------*/


$gl_caption_sellpoint['c_sellpoint'] = 'Title';
$gl_caption_sellpoint['c_sellpoint_description'] = 'Description';
$gl_caption_sellpoint['c_empresa'] = 'Company';
$gl_caption_sellpoint['c_adress'] = 'Address';
$gl_caption_sellpoint['c_comunitat_id'] = 'Region';
$gl_caption_sellpoint['c_provincia_id'] = 'Province';
$gl_caption_sellpoint['c_provincia_id_caption'] = 'No province';
$gl_caption_sellpoint['c_poblacio'] = 'City';
$gl_caption_sellpoint['c_cp'] = 'Postal code';
$gl_caption_sellpoint['c_telefon'] = 'Telephone';
$gl_caption_sellpoint['c_telefon2'] = 'Telephone 2';
$gl_caption_sellpoint['c_fax'] = 'Fax';
$gl_caption_sellpoint['c_correu'] = 'E-mail';
$gl_caption_sellpoint['c_correu2'] = 'E-mail 2';
$gl_caption_sellpoint['c_url1'] = 'Web';
$gl_caption_sellpoint['c_url1_name'] = 'Name of the web';



$gl_caption_sellpoint['c_filter_comarca_id'] = 'All counties';
$gl_caption_sellpoint['c_filter_municipi_id'] = 'All towns';
$gl_caption_sellpoint['c_filter_seller_kind'] = 'All types';


// mapes
$gl_caption_sellpoint['c_edit_map'] = 'Location';
$gl_caption_sellpoint['c_search_adress']='Seek address';
$gl_caption_sellpoint['c_latitude']='latitude';
$gl_caption_sellpoint['c_longitude']='longitude';
$gl_caption_sellpoint['c_found_direction']='address found';
$gl_caption_sellpoint['c_save_map'] = 'Save location';
$gl_caption_sellpoint['c_click_map'] = 'Click within the map to define location';
$gl_caption_sellpoint['c_form_map_title'] = 'Location';
$gl_messages['point_saved'] = 'New location correctly saved';

$gl_caption_sellpoint['c_prepare'] = 'Being prepared';
$gl_caption_sellpoint['c_review'] = 'To review';
$gl_caption_sellpoint['c_public'] = 'Public';
$gl_caption_sellpoint['c_archived'] = 'Archived';
$gl_caption_sellpoint['c_status'] = 'State';

$gl_caption_sellpoint['c_international'] = 'International';
$gl_caption_sellpoint['c_country_id'] = 'Country';
$gl_caption_sellpoint['c_seller_kind'] = 'Type';
$gl_caption_sellpoint['c_seller_kind_shop'] = 'Shop';
$gl_caption_sellpoint['c_seller_kind_shops'] = 'Shops';
$gl_caption_sellpoint['c_seller_kind_dealer'] = 'Dealer';
$gl_caption_sellpoint['c_seller_kind_dealers'] = 'Dealers';

$gl_caption_sellpoint['c_sellpoint_file_name'] = $GLOBALS['gl_caption']['c_file_name'];
$gl_caption_sellpoint['c_sellpoint_old_file_name'] = $GLOBALS['gl_caption']['c_old_file_name'];

$gl_caption_sellpoint['c_filter_provincia_id'] = 'All provinces';

// tarifes
$gl_caption['c_country_id'] = $gl_caption['c_country'] = 'Country';
$gl_caption['c_provincia_id'] = $gl_caption['c_provincia'] = 'Province';
$gl_caption_rate['c_rate'] = 'Delivery rate';
$gl_caption_rate['c_send_rate_world']='Rest of countries';
$gl_caption_rate['c_rate_title']='Rates';
$gl_caption_rate['c_tax_included'] = 'VAT included';
$gl_caption_rate['c_tax_not_included'] = 'VAT not included';
$gl_caption_rate['c_rate_encrease_ondelivery'] = $gl_caption_rate['c_send_rate_encrease_ondelivery_world'] = 'On delivery increase';

$gl_caption['c_can_deliver'] = "Permetre enviament";

// Especificacions
$gl_caption['c_spec_category_id'] = $gl_caption['c_spec_category'] =  'Category';
$gl_caption['c_spec_type'] = 'Type';
$gl_caption['c_ordre'] = 'Order';
$gl_caption['c_spec'] = 'Specification';
$gl_caption['c_spec_unit'] = 'Unites';
$gl_caption['c_filter_spec_category_id'] = 'All categories';


$gl_caption['c_spec_type_checkbox']     = 'Checkbox';
$gl_caption['c_spec_type_int']          = 'Number';
$gl_caption['c_spec_type_text']         = 'Text in languages';
$gl_caption['c_spec_type_text_no_lang'] = 'Text';
$gl_caption['c_spec_type_textarea']     = 'Text in lines';
$gl_caption['c_spec_type_select']       = 'Select box';

$gl_caption['c_data_specs']       = 'Product specifications';

// Couriers
$gl_caption['c_courier'] = $gl_caption['c_courier_id'] = "Messenger";
$gl_caption['c_courier_url'] = "Web";
$gl_caption['c_courier_follow_code'] = "Codi de seguiment";
$gl_caption['c_default_courier'] = "Establish as predefined";
$gl_caption['c_default_courier_0'] = "";
$gl_caption['c_default_courier_1'] = "Default";

// Enviar emails segons/  estat
// admin
$gl_caption['c_prepared_emailed'] =
$gl_caption['c_delivered_emailed'] =
$gl_caption['c_shipped_emailed'] = '';
$gl_caption['c_send_mail_prepared'] = " Send mail order prepared";
$gl_caption['c_send_mail_shipped'] = "Send mail order sent";
$gl_caption['c_send_mail_delivered'] = "Send mail order delivered";
$gl_caption['c_resend_mail'] = "Resend mail";
$gl_caption['c_mail_sent'] = "Mail send on";
$gl_caption['c_mail_send_error'] = "Message has not been sent, please try again";
$gl_caption['c_sending_email'] = "Sending mail...";
$gl_caption['c_email_sended_correctly'] = "Mail correctly sent";

// públic
$gl_caption['c_mail_subject_buyer_status_prepared'] = 'Your order at %1$s is prepared';
$gl_caption['c_mail_subject_buyer_status_shipped'] = 'Your order at %1$s has been sent';
$gl_caption['c_mail_subject_buyer_status_delivered'] = 'Your order at %1$s has been delivered';
$gl_caption['c_mail_subject_seller_status_prepared'] = 'Copy of prepared order';
$gl_caption['c_mail_subject_seller_status_shipped'] = 'Copy of sent order';
$gl_caption['c_mail_subject_seller_status_delivered'] = 'Copy of delivered order';

$gl_caption['c_mail_body_prepared'] = '
	<p>Dear %1$s,</p>
	<p>We are thankful for your confidence in us for your purchases.</p>
	<p>We are preparing your order with the reference: <strong>%2$s</strong> and soon we will make the corresponding shipment. Please check the following information and contact us if you notice any errors.</p>
	<p>At any time, you can check your order by accessing <a href="%3$s">your account</a> on our website. You can also contact us via the following email:</p>
	<p><a href="mailto:%4$s">%4$s</a></p>
	<p>Thanks for shopping at %5$s!</p>';

$gl_caption['c_mail_body_shipped'] = '
	<p>Dear %1$s,</p>
	<p>We inform you that your order <strong>%2$s</strong> has already been sent.</p>';

$gl_caption['c_mail_body_shipped_2'] = '
	<p>The shipping company responsible for sending your order is <a href="%3$s"><strong>%4$s</strong></a> and the tracking number is
		<strong>%5$s</strong>.</p >';

$gl_caption['c_mail_body_shipped_3'] = '
	<p>You can check your order in "orders" at "<a href="%6$s">my account</a>".</p >
	<p>Thanks for shopping at %7$s!</p >';

$gl_caption['c_mail_body_delivered'] = '
	<p>Dear %1$s,</p>
	<p>Your order <strong>%2$s</strong> has already been delivered at the address you indicated us.</p>
	<p>Hopefully, you are completely satisfied with your purchase. If for some reason this is not the case, we would be grateful if you would give us the opportunity to resolve your concerns by replying directly to this email:</p>
	<p><a href="mailto:%3$s">%3$s</a></p>
	<p>Thanks for shopping at %4$s!</p>';


// Relacionats
$gl_caption['c_data_related']       = 'Related products';
$gl_caption['c_product_relateds'] = "Related products";
$gl_caption['c_select_related_products'] = "Search and select related products";
$gl_caption_product_related['c_image'] = "";

// Quantitat màxima
$gl_caption['c_max_quantity'] = "Maximum amount per order";
$gl_caption_list['c_max_quantity'] = "Max";
$gl_caption['c_max_quantity_abbr'] = "Max.";
$gl_caption['c_min_quantity'] = "Minimum amount per order";
$gl_caption_list['c_min_quantity'] = "Min";
$gl_caption['c_min_quantity_abbr'] = "Min.";
$gl_caption['c_quantityconcept_id'] = "Concept";
$gl_caption['c_quantityconcept'] = "Concept";

// Calendari
$gl_caption['c_edit_calendar'] = "Calendar";
$gl_caption['c_has_calendar'] = "Activate calendar";
$gl_caption['c_is_calendar_public'] = "Públic";
$gl_caption['c_familybusy_id'] = "";
$gl_caption['c_date_busy'] = "Date";
$gl_caption['c_familybusy_entered'] = "Creation date";
$gl_caption['c_form_calendar_title'] = "Dates full";
$gl_caption['c_calendar_not_saved_error'] = "date has not been saved, please try again";
$gl_caption['c_calendar_saved_occupied_message'] = "Day has been filled";
$gl_caption['c_calendar_saved_freed_message'] = "Day has been freed";

$gl_caption['c_choose_date'] = "You must choose a date";

// Famílies privades
$gl_caption['c_is_family_private'] = "Privada";
$gl_caption['c_pass_code'] = "Codi d'accés";

$gl_caption['c_pass_code_error'] = "The entered code is wrong";
$gl_caption['c_pass_code_success'] = "The code is correct";

// Cart
$gl_caption['c_product_date'] = "Date";

// Cercador comandes
$gl_caption['c_dates_from'] = 'From';
$gl_caption['c_dates_to'] = 'to';
$gl_caption['c_caption_filter_order_status'] = 'Status';
$gl_caption['c_order_status'] = 'Status';
$gl_caption['c_order_status_group_payed'] = 'Payed';
$gl_caption['c_order_status_group_not_payed'] = 'To pay';
$gl_caption['c_order_status_group_payed_deposit'] = 'Deposit payed';
$gl_caption['c_order_status_group_not_payed_deposit'] = 'Deposit to pay';
$gl_caption['c_view_format'] = "Show";
$gl_caption['c_view_format_all'] = "All";

$gl_caption['c_view_format_order'] = "Orders";
$gl_caption['c_view_format_orderitems'] = "Products";

$gl_caption['c_view_format_title'] = "Type of listing";