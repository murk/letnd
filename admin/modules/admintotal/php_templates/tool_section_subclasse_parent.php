<?
/**
 * __Tool__Section
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2011
 * @version $Id$
 * @access public
 */
class __Tool__Section extends Module{

	public function __construct(){
		parent::__construct();
	}
	public function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	public function get_records()
	{	
		return $this->_execute_module(str_replace('list_','get_',$this->action));
	}
	public function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	public function get_form()
	{
		return $this->_execute_module(str_replace('show_','get_',$this->action));
	}

	public function save_rows()
	{
		$this->_execute_module();
	}

	public function write_record()
	{
		$this->_execute_module();
	}

	public function manage_images()
	{
		$this->_execute_module();
	}
	public function _execute_module($action=''){
		// $this->name = 'instalacio'; // si vull canviar el nom del parent
		$module = Module::load('_ToolModule','__SectionModule', $this);
		$action = $action?$action:$this->action;
		return $module->do_action($action);	
	}
}