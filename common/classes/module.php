<?
/**
 * * Gestiona moduls, serà la clase base per tots els moduls
 *
 *
 *
 */

class Module extends ModuleBase {
	var $section_base_name, $section_base_name_public;
	var $parent=''; // referencia al modul principal que ha cridat aquest modul
	var $name=''; // Nom del modul

	function __construct($is_v3=true){
		global $gl_action, $gl_process, $gl_config, $gl_tool, $gl_tool_section;
		
		$this->is_v3 = $is_v3; // per saber si no estem en v3
		$this->tool = $gl_tool;
		$this->tool_section = $gl_tool_section;
		
		$this->base_link = 'tool=' . $this->tool . '&amp;tool_section=' . $this->tool_section;
		$this->base_link_friendly = $this->tool . '/' . $this->tool_section;
		$this->link = $GLOBALS['gl_is_admin']?
		'/admin/?' . $this->base_link:
		'/?' . $this->base_link . '&amp;language='.$GLOBALS['gl_language'];

		$this->set_default_friendly_params();		
		
		//$this->link = $GLOBALS['gl_is_admin']?
		//	'/admin/?tool=' . $gl_tool . '&tool_section='.$gl_tool_section:
		//	'/?tool=' . $gl_tool . '&tool_section='.$gl_tool_section . '&language='.$GLOBALS['gl_language'];

		if ($this->is_index) {
			$this->action = $gl_action;
			$this->process = $gl_process;
			$this->config = $gl_config; // treure a V3
		}
	}
	/**
	 * Module::load()
	 * el modul principal el carrega automaticament amb vars globals
	 * per carregar un modul manualment es crida Module::load(tool,tool_section)
	 * @param string $tool
	 * @param string $tool_section
	 * @param module $parent_module  si li poso un parent_module puc saber d'on ve i on haig de tornar quan faig editar, guardar, pujar imatges, etc....
	 * @return
	 */
	static public function load($tool='',$tool_section='', $parent_module='', $subclass=true, $config_file=''){
		global $gl_tool, $gl_tool_section, $gl_is_home, $gl_is_tool_home, $gl_is_admin;

		$is_index = $tool=='';

		$tool = $tool?$tool:$gl_tool;
		$tool_section = $tool_section?$tool_section:$gl_tool_section;

		$section_base_name = DOCUMENT_ROOT . 'admin/modules/' . $tool . '/' . $tool . '_' . $tool_section;
		$section_base_name_public = DOCUMENT_ROOT . 'public/modules/' . $tool . '/' . $tool . '_' . $tool_section;

		$class = ucfirst($tool) . snake_to_camel($tool_section, true);

		// per fer servir el modul sense carregar arxius de la subclasse posar $subclass = false;
		// si la clase ja existeix vol dir que ja l'haviem carregat, no cal tornar-hi
		// TODO - Els errors 404 dona problemes amb http://www.biologik.es/cat/directory/document/file/2/force/1/
		if ($subclass && !class_exists($class)) {
			// admin
			if ( $gl_is_admin ) {
				include( $section_base_name . '.php' );
//				file_exists( $section_base_name . '.php' ) ?
//					include( $section_base_name . '.php' ) :
//					Main::error_404( 'No hi ha arxius del mòdul:' . $section_base_name . '.php' );
			} // public
			elseif ( ! $gl_is_admin && $gl_is_tool_home && $is_index && file_exists( $section_base_name_public . '_home.php' ) ) {
				$class .= 'Home';
				include( $section_base_name_public . '_home.php' );
			} elseif ( ! $gl_is_admin ) {
				include( $section_base_name_public . '.php' );
//				file_exists( $section_base_name_public . '.php' ) ?
//					include( $section_base_name_public . '.php' ) :
//					Main::error_404( 'No hi ha arxius del mòdul: ' . $section_base_name_public . '.php' );
			}
		}

		// si no es l'index.php, es a dir: cridem el modul: $module = Module::load(tool,tool_section);
		if (!$is_index) {
			$module =$subclass?new $class():new Module();
			$module->name = $class;
			$link_debug = $GLOBALS['gl_site_part'] . '/modules/' . $tool . '/' . $tool . '_' . $tool_section . '.php';
			Debug::add($subclass?'SUBCLASS ':'SUBCLASS CONTAINER', Debug::get_clock_link($link_debug, 1,$class));

			$module->is_index = false;
			$module->section_base_name = $section_base_name;
			$module->section_base_name_public = $section_base_name_public;
			$module->tool = $tool;
			$module->tool_section = $tool_section;
			$module->base_link = 'tool=' . $tool . '&amp;tool_section=' . $tool_section;
			$module->base_link_friendly = $tool . '/' . $tool_section;
			$module->link = $GLOBALS['gl_is_admin']?
			'/admin/?' . $module->base_link:
			'/?' . $module->base_link . '&amp;language='.$GLOBALS['gl_language'];
			//$module->link = $GLOBALS['gl_is_admin']?
			//'/admin/?tool=' . $tool . '&tool_section='.$tool_section:
			//'/?tool=' . $tool . '&tool_section='.$tool_section . '&language='.$GLOBALS['gl_language'];
			
			// si li poso un parent_module puc saber d'on ve i on haig de tornar quan faig editar, guardar, pujar imatges, etc....
			if ($parent_module){
				$module->parent_tool = $parent_module->tool;
				$module->parent_tool_section = $parent_module->tool_section;
				$module->parent = $parent_module->name;				
				$module->base_link = 'tool=' . $parent_module->tool . '&amp;tool_section=' . $parent_module->tool_section;
				$module->base_link_friendly = $parent_module->tool . '/' . $parent_module->tool_section;
				$module->link = $GLOBALS['gl_is_admin']?
				'/admin/?' . $module->base_link:
				'/?' . $module->base_link . '&amp;language='.$GLOBALS['gl_language'];
				
				$parent_module->child_tool = $tool;
				$parent_module->child_tool_section = $tool_section;
			}

			Module::get_config_values($module->tool, $module->tool_section, $module); // variables de configuracio de la taula modul__config
			$module->set_tpl(); // defineixo tpl
			$module->set_language($module->parent_tool_section); // carrego arxiu variables idioma
			$module->set_config($config_file); // carrego arxiu config del modul
			
			if ($parent_module){
				// on va el next_form_id
				$GLOBALS['gl_do_next_id_field'] = $module->id_field;
			}
			
			$module->set_functions(); // carrego arxiu de funcions per el modul
			$module->on_load(); // crido la funció on_load quan he carregat tot el modul
			UploadFiles::initialize($module);
			return $module;
		}
	}
	function on_load(){

		$tool_function = $this->tool . "_on_load";
		$tool_index_function = $this->tool . "_index_on_load";

		Debug::p( [
					[ 'Funció especial', 'client_on_load' ],
					[ 'Funció especial', $tool_function ],
					[ 'Funció especial ( nomes quan el mòdul es index )', $tool_index_function ],
					[ 'Funció especial', 'client_on_start_action' ],
					[ 'Funció especial', 'client_on_end' ],
				], 'general');

		// funcio que es pot posar en un client concret
		if (function_exists('client_on_load')) client_on_load($this);
		if (function_exists($tool_function)) $tool_function($this);
		if ($this->is_index && function_exists($tool_index_function)) $tool_index_function($this);
	}
	// Just abans de començar l'accio
	function on_start_action($action){
		// funcio que es pot posar en un client concret
		if (function_exists('client_on_start_action')) client_on_start_action($action, $this);		
		
	} 
	// Al acabar l'accio del modul ( list_records, show_form, etc )
	function on_end($action){
		// funcio que es pot posar en un client concret
		if (function_exists('client_on_end')) client_on_end($action, $this);		
		
	} 
	
	
	/**
	 * Module::get_config_values()
	 * Static o No function
	 * Obtinc els valors de configuracio de la taula tool__config i tool__config_language
	 * Y ho poso en forma de constant TOOL_NAME i $gl_config['name'] = $value;
	 *
	 * V3 Agafo tant els valors 'public' com 'admin' i es sobreescriuen els valors iguals segons on soc
	 * ej. si soc a admin primer agafo valors public i despres admin sobreescriu valors public iguals
	 *
	 * Quan crido desde les plantilles de public, nomes agafa el tool per que no hi ha section de public
	 *
	 * @param boolean $tool
	 * @access public
	 * @return void
	 */
	static public function get_config_values($tool=false, $tool_section=false, $obj=false)
	{
		global $gl_tool, $gl_tool_section, $gl_config, $gl_language;
		
		if (!$tool)	$tool = $gl_tool;
		if (!$tool_section)	$tool_section = $gl_tool_section;
		
		($obj)?$config = &$obj->config:$config = &$gl_config;

		$cg['admin'] = Module::get_config_array('admin', $tool);
		$cg['public'] = Module::get_config_array('public', $tool);
		
		// config de secció només en poso a admin, si algun dia fa falta també farem el de public
		$cg['section'] = Module::get_config_array('admin', $tool,$tool_section);

		$site_part = $GLOBALS['gl_site_part'];
		$other_part = $site_part=='public'?'admin':'public';

		// sobreescric amb preferencia de la part on som
		$config = array_merge($config, $cg[$other_part], $cg[$site_part], $cg['section']);

		if (!empty ($config['has_client_tool_config'])){
			$cg['client'] = Module::get_config_array('public', "client__" . CLIENT_DIR_NAME, $tool);
			if ($cg['client']) $config = array_merge($config, $cg['client']);
		}
		
		//Debug::add('Config values',$tool, 2);
		//Debug::add('Admin', $cg['admin']);
		//Debug::add('Public', $cg['public']);
		//Debug::add('Config ' . $site_part, $config);

		foreach ($config as $key => $val)
		{
			if (!defined(strtoupper($tool . '_' . $key)))
				define(strtoupper($tool . '_' . $key), $val);
		}
	}
	static public function get_config_array($site_part, $tool, $tool_section = false){
		$table = $tool . '__config' . $site_part;
		if ($tool_section) $table .= '_' . $tool_section;
		$config = array();
		if (!is_table($table)) return $config;;
		$table_lang = $tool . '__config' . $site_part . '_language';

		if (is_table($table_lang))
		{
			$query = "SELECT  " . $table . ".name,
		" . $table . ".value as value,  " . $table_lang . ".value as valuelang
		FROM " . $table . "
		LEFT OUTER JOIN  " . $table_lang . "
		USING (name)
		WHERE language = '" . $GLOBALS['gl_language'] . "' OR isnull(language)";
		} else
		{
			$query = "SELECT name, value
				FROM " . $table;
		}
		$results = Db::get_rows($query);
		foreach ($results as $rs)
		{
			// revisar, treure les constants, va millor amb un array
			$valuelang = isset($rs['valuelang'])?$rs['valuelang']:'';
			$value = $valuelang?$valuelang:$rs['value']; // te preferencia l'idioma
			$config[$rs['name']] = $value;
		}
		return $config;
	}
	/**
	 * Module::get_config()
	 * Obtinc els valors de configuracio de la taula tool__config i tool__config_language
	 *
	 * @param string $site_part ('public' o 'admin')
	 * @param string $table (taula de configuració)
	 * @param string $table_lang (taula d'idioma de configuració)
	 * @access public
	 * @return array() $config['name'] = $value;
	 */
	function get_config($site_part, $table='', $table_lang=''){
		if (!$table) $table = $this->tool . '__config' . $site_part;
		if (!$table_lang) $table_lang = $this->tool . '__config' . $site_part . '_language';
		if ($site_part == 'public' && is_table($table_lang))
		{
			$query = "SELECT  " . $table . ".name,
		" . $table . ".value as value,  " . $table_lang . ".value as valuelang
		FROM " . $table . "
		LEFT OUTER JOIN  " . $table_lang . "
		USING (name)
		WHERE language = '" . $GLOBALS['gl_language'] . "' OR isnull(language)";
		} else
		{
			$query = "SELECT name, value
				FROM " . $table;
		}
		$results = Db::get_rows($query);
		$config = array();

		foreach ($results as $rs)
		{
			$valuelang = isset($rs['valuelang'])?$rs['valuelang']:'';
			$value = $valuelang?$valuelang:$rs['value']; // te preferencia l'idioma
			$config[$rs['name']] = $value;
		}
		return $config;
	}
	/**
	 * Module::init()
	 * Static function
	 * nomès es crida de l'index ja que no sabem si es V2 o V3,
	 * Es fa servir $module = Module::init()
	 * @return
	 */
	static public function init(){
		global $gl_tool, $gl_tool_section,  $gl_is_home, $gl_is_tool_home;
		$link_debug = $GLOBALS['gl_site_part'] . '/modules/' . $gl_tool . '/' . $gl_tool . '_' . $gl_tool_section . '.php';

		// Això em permet tindre p.e. un tool= admintotal i tool_section=guestia_import, que carrega l'arxiu admintotal_ghestia_import.php, però que cridi la clase en CamelCase: AdmintotalGhestiaImport
		$class = ucfirst($gl_tool) . snake_to_camel($gl_tool_section, true);

		// si no hi ha un arxiu de "_home" agafo la clase normal, així no fa falta posar a tots els is_tool_home un arxiu per la seva home (ej: a products nomes fa falta la home de product, la home de cart no fa falta)
		$class .= (($gl_is_home || $gl_is_tool_home) && class_exists ($class.'Home'))?'Home':'';
		if (class_exists ($class)) {
			$module = new $class();
			$module->name = $class;
			if (isset($_GET['globals'])){
				Debug::add('INICI ', 'Globals',1);
				foreach ($GLOBALS as $k=>$v){
					if ($k!='gl_debug_messages') Debug::add($k, $v);
				}
			}
			Debug::add('MAIN CLASS ', Debug::get_clock_link($link_debug, 1, $class));
			return $module;
		}
		else
			Debug::add('NO CLASS ', Debug::get_clock_link($link_debug, 1, $class));
			return new Module(false);
	}
	/**
	 * Module::set_tpl()
	 * Defineixo el tpl per utilitzar globalment.
	 * I defineixo variables globals dins el tpl
	 *
	 * @return void
	 */
	function set_tpl(){
		if (!$this->is_v3) {
			global $tpl;
			$this->tpl = &$tpl;
		}
		$this->tpl = new phemplate(PATH_TEMPLATES);
		$this->tpl->module = &$this;
		
		// poso variables comunes
		$this->set_var('version',$GLOBALS['gl_version']);
		$this->set_var('is_mobile',$GLOBALS['gl_is_mobile']);
        if (property_exists($GLOBALS['gl_page'],'page_text_id')) 
			$this->set_var('page_text_id', $GLOBALS['gl_page']->page_text_id);
		
		// per poder extendre tpl més endavant
		$this->vars = &$this->tpl->vars; /// Holds all the template variables
		$this->loops = &$this->tpl->loops;
		$this->path = &$this->tpl->path; /// path to templates
		$this->file = &$this->tpl->file; /// template
	}
	/**
	 * Module::set_language()
	 * Carrego arxiu d'idiomes i sobreescric les variables
	 * @return
	 */
	function set_language($parent_section=''){
		global
		$gl_language_vars_overrided ,$gl_is_admin;	
		
		// Si es el modul principal, puc sobreescriure les variables globals al carregar l'arxiu del modul
		if ($this->is_index) {
			global $gl_caption, $gl_caption_list, $gl_caption_image, $gl_messages;			
		}
		// Si no es el modul principal, faig una copia de gl_caption, així no el modifico al carregar arxiu idioma del modul
		else
		{
			$gl_caption = $GLOBALS['gl_caption'];
			$gl_caption_list = $GLOBALS['gl_caption_list'];
			$gl_caption_image = $GLOBALS['gl_caption_image'];
			$gl_messages = $GLOBALS['gl_messages'];
		}
		// la variable caption ha de ser global, ja que son els captions generals de l'eina, però no s'ha de sobreescriure com abans, si no que s'ha de guardar amb el merge del caption_section, i els messages igual
		if (!$this->is_v3) {
			global

			$gl_messages, ${'gl_caption_' . $this->tool_section},
			${'gl_messages_' . $this->tool_section},
			$gl_is_admin;

			$this->caption = &$gl_caption;
			$this->caption_image = &$gl_caption_image;
			// no hi poso el caption list ja que es v3
			$this->messages = &$gl_messages;
		}
		
		


		if (!$gl_language_vars_overrided || $this->is_v3) {


			Debug::p( [
						[ 'Arxiu idiomes per sobreescriure admin client', CLIENT_PATH . 'languages/' . LANGUAGE . '_before_admin.php' ],
						[ 'Arxiu idiomes client', CLIENT_PATH . 'languages/' . LANGUAGE . '.php' ],
					], 'general');

			// es pot carregar un arxiu d'admin del client abans que el d'admin
			// així es pot sobreescriure un define
			if (
				$gl_is_admin &&
				file_exists(CLIENT_PATH . 'languages/' . LANGUAGE . '_before_admin.php')
			)
				include (CLIENT_PATH . 'languages/' . LANGUAGE . '_before_admin.php');


			include (DOCUMENT_ROOT . 'admin/modules/' . $this->tool . '/languages/' . LANGUAGE . '.php');

			// public
			if (!$gl_is_admin) {
				if (file_exists(DOCUMENT_ROOT . 'public/modules/' . $this->tool . '/languages/' . LANGUAGE . '.php'))
					include (DOCUMENT_ROOT . 'public/modules/' . $this->tool . '/languages/' . LANGUAGE . '.php');
				
				// l'include es aqui perque hem de sobreescriuree tambe 'gl_caption_' . $this->tool_section etc...
				if (is_file(CLIENT_PATH . 'languages/' . LANGUAGE . '.php'))
					include (CLIENT_PATH . 'languages/' . LANGUAGE . '.php');				
				
			}
			else{
				if (file_exists(CLIENT_PATH . 'languages/' . LANGUAGE . '_admin.php'))
					include (CLIENT_PATH . 'languages/' . LANGUAGE . '_admin.php');
			}
			if ($this->is_v3) {

				$this->caption = $gl_caption;
				$this->caption_image = $gl_caption_image;
				$this->caption_list = $gl_caption_list;
				$this->messages = $gl_messages;
			}
			// CAPTIONS I MESSAGES
			// fusionem variables de idiomes per sobrescriure valors per defecte per valors segons tool_section o tool
			// El gl_caption l'agafa al primer cat i el $gl_tool_section  l'agafa a la funcio set_page vars de la classe page a page_admin
			
			if (isset(${'gl_caption_' . $this->tool_section}))
			$this->caption = array_merge($this->caption, ${'gl_caption_' . $this->tool_section});
			
			if (isset(${'gl_caption_' . $this->tool_section . '_image'}))
			$this->caption_image = array_merge($this->caption_image, ${'gl_caption_' . $this->tool_section . '_image'});
			
			if (isset(${'gl_caption_' . $this->tool_section . '_list'}))
			$this->caption_list = array_merge($this->caption_list, ${'gl_caption_' . $this->tool_section . '_list'});
			
			if (isset(${'gl_messages_' . $this->tool_section}))
			$this->messages = array_merge($this->messages, ${'gl_messages_' . $this->tool_section});
			
			// si hi ha parent module sobreescrivim els del parent module
			if ($parent_section){
				if (isset(${'gl_caption_' . $parent_section}))
				$this->caption = array_merge($this->caption, ${'gl_caption_' . $parent_section});
				if (isset(${'gl_caption_' . $parent_section . '_list'}))
				$this->caption_list = array_merge($this->caption_list, ${'gl_caption_' . $parent_section . '_list'});
				if (isset(${'gl_messages_' . $parent_section}))
				$this->messages = array_merge($this->messages, ${'gl_messages_' . $parent_section});
			}
			
			// els captions dels clients a la BBDD sobreescriuen tot
			if (!$gl_is_admin)
				Module::set_client_language($this->caption, $this->is_index);
		}
	}
	/**
	 * Module::set_language()
	 * Carrego arxiu d'idiomes i sobreescric les variables
	 * @return
	 */
	static function set_client_language(&$caption, $is_index){		
		
		if ($is_index) $gl_caption = &$GLOBALS['gl_caption'];
		
		// variables client de la bbdd, les passo en  prefix:  c_
		$caption_client = Module::get_config_array('public', 'client');
		foreach ($caption_client as $key=>$value){
			$caption['c_' . $key] = $value;
			if ($is_index) $gl_caption['c_' . $key] = $value;
		}
	}
	/**
	 * Module::set_config()
	 * Carrego arxiu de config dels camps de la bbdd
	 * @return
	 */
	function set_config($config_file=''){
		global
			$gl_config_overrided, $gl_section_base_name, $gl_section_base_name_public;
		// això anirà fora quan es  pugui cambiar l'ordre a l0 index de Module::get_config_values() i Module::load(); v3
		if ($this->is_index && !$gl_section_base_name) {
			$this->section_base_name = DOCUMENT_ROOT . 'admin/modules/' . $this->tool . '/' . $this->tool . '_' . $this->tool_section;
			$this->section_base_name_public = DOCUMENT_ROOT . 'public/modules/' . $this->tool . '/' . $this->tool . '_' . $this->tool_section;
		}
		// a v2 era una constant, per tant quedava definida amb l'eina anterior a set_tool o change_tool, allà defineixo aquestes variables globals
		elseif ($this->is_index && $gl_section_base_name){
			$this->section_base_name = $gl_section_base_name;
			$this->section_base_name_public = $gl_section_base_name_public;
		}

		if (!$this->is_v3) {
			global
				$gl_db_classes_id_field,
				$gl_db_classes_language_fields,
				$gl_db_classes_fields,
				$gl_db_classes_list,
				$gl_db_classes_form,
				$gl_db_classes_related_tables,
				$gl_db_classes_uploads;

			$this->id_field = &$gl_db_classes_id_field;
			$this->table = &$gl_db_classes_table;
			$this->language_fields = &$gl_db_classes_language_fields;
			$this->fields = &$gl_db_classes_fields;
			$this->list_settings = &$gl_db_classes_list;
			$this->form_settings = &$gl_db_classes_form;
			$this->related_tables = &$gl_db_classes_related_tables;
			$this->upload_settings = &$gl_db_classes_uploads;
		}
		$config_file_path = '';
		// per carregar un arxiu config diferent del que carrega automaticament
		if($config_file){
			$config_file_path = DOCUMENT_ROOT . 'admin/modules/' . $config_file;
		}
		// a v3 no existirà la variable gl_config_overrided, ja que carrego tot amb la clase modul
		elseif (!$gl_config_overrided || $this->is_v3) {
			$config_file_path = $this->section_base_name . '_config.php';

		}
		// sempre intento include quan $config_file, per que vull que dongui error si no la troba
		// quan es automatic no vull que dongui error ja que pot ser un modul sense arxiu config
		if ($config_file_path && ($config_file || file_exists($config_file_path))) {
			include ($config_file_path);

			if ($this->is_v3) {
				$this->id_field = $this->tool_section . '_id';
				if (!$this->table) $this->table = $this->tool . '__' . $this->tool_section;
				$this->language_fields = $gl_db_classes_language_fields;
				$this->fields = $gl_db_classes_fields;
				$this->list_settings = $gl_db_classes_list;
				$this->form_settings = $gl_db_classes_form;
				$this->related_tables = isset($gl_db_classes_related_tables)?$gl_db_classes_related_tables:'';
				$this->upload_settings = isset($gl_db_classes_uploads)?$gl_db_classes_uploads:'';
			}
		}
	}
	/**
	* EXPERIMENTAL !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	 * Module::merge_module()
	 * Sumo arxiu de config dels camps de la bbdd per poder posar 2 formularis o més en un
	 * Sumo captions
	 * @return
	 */
	function merge_module($tool,$tool_section, $config_file=''){
		
		
		// carrego idiomes
		include (DOCUMENT_ROOT . 'admin/modules/' . $tool . '/languages/' . LANGUAGE . '.php');

		// public
		if (!$GLOBALS['gl_is_admin']) {
			include (DOCUMENT_ROOT . 'public/modules/' . $tool . '/languages/' . LANGUAGE . '.php');
			if (file_exists(CLIENT_PATH . 'languages/' . LANGUAGE . '.php'))
				include (CLIENT_PATH . 'languages/' . LANGUAGE . '.php');
		}

		// CAPTIONS I MESSAGES
		//$gl_caption = $gl_messages = array();
		if (isset(${'gl_caption_' . $tool_section}))
		$gl_caption = array_merge($gl_caption, ${'gl_caption_' . $tool_section});
		if (isset(${'gl_messages_' . $tool_section}))
		$gl_messages = array_merge($gl_messages, ${'gl_messages_' . $tool_section});

		if ($gl_caption) $this->caption += $gl_caption;
		if ($gl_messages) $this->messages += $gl_messages;
		
		if ($config_file === false) return;
		
		
		// carrego arxiu config a sumar al actual

		if($config_file){
			$config_file_path = DOCUMENT_ROOT . 'admin/modules/' . $config_file;
		}
		else {
			$config_file_path = DOCUMENT_ROOT . 'admin/modules/' . $tool . '/' . $tool . '_' . $tool_section . '_config.php';
		}
		include ($config_file_path);

		// NO SE QUINS S'HAURIEN DE SUMAR, de moment només fields
		//$this->id_field = $this->tool_section . '_id';;
		//$this->table = $this->table . $this->tool . '__' . $this->tool_section;
		//$this->language_fields = $gl_db_classes_language_fields;
		$this->fields += $gl_db_classes_fields;
		//$this->list_settings = $gl_db_classes_list;
		//$this->form_settings = $gl_db_classes_form;
		//$this->related_tables = isset($gl_db_classes_related_tables)?$gl_db_classes_related_tables:'';
		//$this->upload_settings = isset($gl_db_classes_uploads)?$gl_db_classes_uploads:'';

		


		return;
		///////////////////////////// VELL //////////////////////////////////////
		// carrego idiomes
		include (DOCUMENT_ROOT . 'admin/modules/' . $this->tool . '/languages/' . LANGUAGE . '.php');

		// public
		if (!$GLOBALS['gl_is_admin']) {
			include (DOCUMENT_ROOT . 'public/modules/' . $this->tool . '/languages/' . LANGUAGE . '.php');
			if (file_exists(CLIENT_PATH . 'languages/' . LANGUAGE . '.php'))
				include (CLIENT_PATH . 'languages/' . LANGUAGE . '.php');
		}

		// CAPTIONS I MESSAGES
		$gl_caption = $gl_messages = array();
		if (isset(${'gl_caption_' . $tool_section}))
		$gl_caption = array_merge($gl_caption, ${'gl_caption_' . $tool_section});
		if (isset(${'gl_messages_' . $tool_section}))
		$gl_messages = array_merge($gl_messages, ${'gl_messages_' . $tool_section});

		if ($gl_caption) $this->caption += $gl_caption;
		if ($gl_messages) $this->messages += $gl_messages;
	}
	/**
	 * Module::set_functions()
	 * Carrego arxius de funcions especifiques d'aquest modul
	 * @return
	 */
	function set_functions(){
		global $gl_is_admin;
		if ($gl_is_admin && file_exists(DOCUMENT_ROOT . 'admin/modules/' . $this->tool . '/' . $this->tool . '_functions.php')) {
			include_once (DOCUMENT_ROOT . 'admin/modules/' . $this->tool . '/' . $this->tool . '_functions.php');
		}
		
		if ($gl_is_admin && file_exists($this->section_base_name . '_functions.php')) {
			include_once ($this->section_base_name . '_functions.php');
		}
		elseif (!$gl_is_admin && file_exists($this->section_base_name_public . '_functions.php'))
		{
			include_once ($this->section_base_name_public . '_functions.php');
		}
	}
	/**
	 * Module::do_action()
	 * Crida metode segons l'action de la pàgina
	 * Els més comuns estan agrupats en un sol métode:
	 *      show_form,list_records,save_rows,write_record,manage_images
	 * La resta crida un metode amb el mateix nom de l'action
	 * 		ej: /admin?menu_id=10&action=get_anything  cridarà el metode $module->get_anything();
	 * @return
	 */
	function do_action($action = ''){
		global $gl_page;
		
		$this->on_start_action($action?$action:$this->action); // crido la funció on_start_action al començar totes les accions del modul
		
		// nomès les que venen automatiques son ilegals,
		// es pot cridar una funcio privada desd'un altre modul així:
		// $module->do_action('_privada');  pero no desde la url: index.php?action=_privada
		if ($this->action[0]=='_') {
			$m1 = ' No s\'executarà cap acció que comenci per _';
			$m2 = 'Acció ilegal!!!!!!!!!!!!!!!! -----> "'.$this->action.'"';
			Debug::p($m1,$m2);Debug::add($m2,$m1);
			return;
		}

		// si ve com parametre, despres reestableixo el action vell
		if ($action){
			$old_action = $action;
			$this->action = $action;
		}

		// Si no hi ha acció no faig res
		if (!$this->action) return;

		Debug::add('ACTION' , $this->action, 2);

		// ************
		// CRIDAR FUNCIO SEGONS ACTION

		if (!$this->is_v3) {
			$this->deprecated_call_function();
		}
		else{
			switch ($this->action) {
				case 'show_form_new':
				case 'show_form_edit':
				case 'show_form_copy':
				case 'show_record':
					$this->show_form();
					break;
				case 'get_form_new':
				case 'get_form_edit':
				case 'get_form_copy':
				case 'get_record':
					return $this->get_form();
					break;
				case 'list_records':
				case 'list_records_bin':
					$this->list_records();
					break;
				case 'get_records':
				case 'get_records_bin':
					return $this->get_records();
					break;
				case 'save_rows_delete_selected':
				case 'save_rows_bin_selected':
				case 'save_rows_restore_selected':
				case 'save_rows_selected_to_value':
				case 'save_rows_save_records':
				case 'save_rows_save_config':
					if ($this->is_index) $gl_page->is_frame = true;
					if ($this->can_write()){
						$this->save_rows();
						if ($this->is_index) Main::do_next($this->action);
					}
					break;
				case 'save_record':
				case 'add_record':
				case 'bin_record':
				case 'delete_record':
					if ($this->is_index) $gl_page->is_frame = true;
					if ($this->can_write()){
						$this->write_record();
						if ($this->is_index) Main::do_next($this->action);
					}
					break;
				case 'list_records_images':
				case 'save_rows_get_images':
				case 'save_rows_save_records_images':
				case 'save_rows_delete_selected_images':
				case 'show_uploader_images':
					$this->manage_images();
					break;
				case 'write_record_social':
					if ($this->is_index) $gl_page->is_frame = true;
					if ($this->can_write()){
						$this->write_record_social();
						if ($this->is_index) Main::do_next($this->action);
					}
					break;
				default:
					return call_user_func (array(&$this, $this->action));
					break;

			} // switch
			if ($action) $this->action = $old_action;
		}
			
		$this->on_end($action?$action:$this->action); // crido la funció on_end al acabar totes les accions del modul

	}
	function can_write(){
		if (SaveRows::is_post_too_big()){
			return false;
		}elseif (!$GLOBALS['gl_write'] && $GLOBALS['gl_is_admin']) {
			$GLOBALS['gl_message'] = $GLOBALS['gl_messages']['concession_no_write'];
			return false;
		}
		else{
			return true;
		}
	}
	/**
	 * Module::deprecated_call_function()
	 * Substituir per moduls, serveix per tots els moduls vells que no utilitzen la classe module si no que van en funcinos
	 * @return
	 */
	function deprecated_call_function(){
		global $gl_action, $gl_write, $gl_message, $gl_messages, $gl_saved, $gl_page, $gl_is_admin;
		// ================
		// ACTIONS
		// Control del fluxe de l'aplicació
		// ================
		switch (true) {
			case strstr($gl_action, "_images"):
				function_exists('manage_images') ? manage_images() : Main::error_404('No existeix manage_images');
				break;
			case strstr($gl_action, "list_records"): // a public era "list"
				function_exists('list_records') ? list_records() : Main::error_404('No existeix list_records');
				break;
			case $gl_action == "show_search_form":
				function_exists('show_search_form') ? show_search_form() : Main::error_404('No existeix show_search_form');
				break;
			case strstr($gl_action, "show_"): // a public era "show"
				function_exists('show_form') ? show_form() : Main::error_404('No existeix show_form');
				break;
			case strstr($gl_action, "save_rows"): // a public era "save"
				if ($this->is_index) $gl_page->is_frame = true;
				if ($this->can_write()){
					function_exists('save_rows') ? save_rows() : Main::error_404('No existeix save_rows');
					if ($this->is_index) Main::do_next($gl_action);
				}
				break;
			case strstr($gl_action, "record"):
				if ($this->is_index) $gl_page->is_frame = true;
				if ($this->can_write()){
					function_exists('write_record') ? write_record() : Main::error_404('No existeix write_record');
					if ($this->is_index) Main::do_next($gl_action);
				}
				break;
		} // switch
	}
}