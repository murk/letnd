<?php
// path of your TTF font directory
$fontdir = "c:/windows/fonts/";
$pdf = pdf_new();

if (!pdf_open_file($pdf, "")) {
    echo error;
    exit;
};

pdf_set_parameter($pdf, "warning", "true");
pdf_set_info($pdf, "Creator", "pdf_clock.php");
pdf_set_info($pdf, "Author", "Uwe Steinmann");
pdf_set_info($pdf, "Title", "Analog Clock");
pdf_begin_page($pdf, 592, 840);
pdf_set_parameter($pdf, "FontOutline", "Verdana=$fontdir/verdanaz.TTF");
$font = pdf_findfont($pdf, "Verdana", "winansi", 1);
pdf_setfont($pdf, $font, 20);




pdf_add_outline($pdf, "Pagina 1");

// Open few .TTF (true type font)
// be sure that your font file contains enough character for your language


pdf_show_xy($pdf, "Ref. U425", 24.5, 786.5);
$image = pdf_open_image_file($pdf, "jpeg", "1.jpg", "empty", 'image1');


pdf_set_value($pdf, "imagewidth", '10');
pdf_set_value($pdf, "imageheight", '10');

pdf_place_image($pdf, $image, 0, 0, 0.1);


pdf_end_page($pdf);


pdf_close($pdf);

$buf = pdf_get_buffer($pdf);
$len = strlen($buf);

header("Content-type: application/pdf");
header("Content-Length: $len");
header("Content-Disposition: inline; filename=foo.pdf");
echo $buf;

pdf_delete($pdf);
?>
