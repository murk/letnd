<?if ($is_owner):?>
<input name="selected_owner_ids" type="hidden" value="<?=$selected_owner_ids?>" />
<input name="selected_owner_names" type="hidden" value="<?=$selected_owner_names?>" />
<?endif //is_owner?>
<table width="90%" border="0" cellspacing="0" cellpadding="0" class="subListRecordInForm">
<?foreach($loop as $l): extract ($l)?>
  <tr>
    <td class="padding-top-2"><table width="100%" cellpadding="4" cellspacing="0">
        <tr>
          <td class="formsCaptionHor">
              <table border="0" cellpadding="0" cellspacing="0">
                <tr>
                  <td><strong>
                    <?=$c_name?>
                    :</strong></td>
                  <td>&nbsp;
                      <?=$name?>
                    &nbsp;</td>
                  <td><strong>&nbsp;
                        <?=$c_surnames?>
                    :&nbsp;</strong></td>
                  <td>&nbsp;
                      <?=$surname1?>
                    &nbsp;</td>
                  <td>&nbsp;
                  <?=$surname2?></td>
                </tr>
            </table></td>
        </tr>
        </table>
      <table class="padding-top-1" width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td align="left"><table width="100%" cellpadding="4" cellspacing="0">
              <tr>
                <td width="150" valign="top" class="formsCaption"><strong>
                  <?=$c_phone?>
                :</strong></td>
                <td class="forms"><table border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td><?=$c_phone1?></td>
                    <td><?=$c_phone2;?></td>
                    <td><?=$c_phone3;?></td>
                  </tr>
                  <tr>
                    <td><?=$phone1?>
                      <input name="custumer_id[<?=$custumer_id?>]" type="hidden" value="<?=$custumer_id?>" />                      &nbsp;&nbsp;</td>
                    <td><?=$phone2;?>                      &nbsp;&nbsp;</td>
                    <td><?=$phone3;?>                      &nbsp;&nbsp;</td>
                  </tr>
                </table></td>
              </tr>
              <tr>
                <td valign="top" class="formsCaption"><strong>
                  <?=$c_address?>
                : </strong></td>
                <td class="forms"><table border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td><?=$c_adress?></td>
                      <td><?=$c_numstreet;?></td>
                      <td><?=$c_block;?></td>
                      <td><?=$c_flat;?></td>
                      <td><?=$c_door;?></td>
                    </tr>
                    <tr>
                      <td><?=$adress?>
                      &nbsp;&nbsp;</td>
                      <td><?=$numstreet;?>
                      &nbsp;&nbsp;</td>
                      <td><?=$block;?>
                      &nbsp;&nbsp;</td>
                      <td><?=$flat;?>
                      &nbsp;&nbsp;</td>
                      <td><?=$door;?></td>
                    </tr>
                </table>                </td>
              </tr>
              <tr>
                <td valign="top" class="formsCaption"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;
                        <?=$c_town;?>
                      :</td>
                  </tr>
                </table></td>
                <td class="forms"><table border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td><?=$town;?></td>
                  </tr>

                </table></td>
              </tr>
              <tr>
                <td valign="top" class="formsCaption"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;
                        <?=$c_province;?>
                      :</td>
                  </tr>
                </table></td>
                <td class="forms"><table border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td><?=$province;?></td>
                  </tr>

                </table></td>
              </tr>
              <tr>
                <td valign="top" class="formsCaption"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;
                        <?=$c_zip;?>
                      :</td>
                  </tr>
                </table></td>
                <td class="forms"><table border="0" cellspacing="0" cellpadding="0">

                  <tr>
                    <td><?=$zip;?></td>
                  </tr>

                </table></td>
              </tr>
              <tr>
                <td valign="top" class="formsCaption"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;
                        <?=$c_country;?>
                      :</td>
                  </tr>
                </table></td>
                <td class="forms"><table border="0" cellspacing="0" cellpadding="0">

                  <tr>
                    <td><?=$country;?></td>
                  </tr>

                </table></td>
              </tr>
              <tr>
                <td height="50" valign="top" class="forms"></td>
                <td class="forms"><div class="right"><?foreach($buttons as $button):?><?=$button?><?endforeach //$buttons?></div></td>
              </tr>
            </table>          </td>
        </tr>
      </table>      </td>
  </tr>
<?endforeach //$loop?>
</table>
<br />