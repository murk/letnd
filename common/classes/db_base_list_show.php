<?php
/**
 * Clase BaseListShow
 *
 * @author Joan Sanahuja <sanahuja@gmail.com>
 * @version 1.0
 */

/**
 * Classe base per crear formularis i llistats
 *
 * @author Joan Sanahuja <sanahuja@gmail.com>
 * @version 1.0
 * @uses FormObject
 */
class BaseListShow Extends ModuleBase
{
    /**
     * Nom d'aquest llistat, pot servir per distingir si es fa més d'un llistat alhora
     * inclus per guardar llistats anteriors, poder haurà de ser automatic si no es posa
     * @access public
     * @var string
     */
    var $name;
    /**
     * Plantilla HTML utilitzada per el formulari o llistat <br>
     * Nomès es posa el nom complert del arxiu i directori de l'eina, el directori ja ho sab l'aplicació  on és
     *
     * @access public
     * @var string
     */
    var $template;
    /**
     * Per canviar la Plantilla HTML automatica del formulari o llistat<br>
	 * Les plantilles automatiques son list.tpl, form.tpl, amb aquesta variable puc crear una plantilla automàtica diferent, ej. list_options.tpl
     * Nomès es posa el nom complert del arxiu, el directori ja ho sab l'aplicació  on és
     *
     * @access public
     * @var string
     */
    var $default_template;
    /**
     * Si nomès vull recollir les variables, no faig servir plantilla ni variables automatiques
	 * 
     * 
     *
     * @access public
     * @var string
     */
    var $use_no_template;
    /**
     * El titol de la pàgina l'agafa directament de la BBDD, amb aquesta propietat es pot sobreescriure per el que volguem
     *
     * @access public
     * @var string
     */
    var $title_modify;
    // - Crida una funció que volem executar per que calculi el valor d'un camp
    // this->call_function["Nom del Camp"] = "Nom de la Funció"
    // - Crida la funció "Nom de la Funció" pasant el nom del camp i el valor del camp com a parametres i aquesta funció que crida a de retornar un nou valor per "Nom del Camp"
    // - Ej. <br> $listing->call_function["entered"] = "format_date_list";
    // - Per el camp entered, que es un camp tipus data, crida la funció "format_date_list" que retorna la data formatejada en l'estil espanyol: DD-MM-YYYY
    //  @access public
    //  @var array
    var $call_function;
    /**
     * Afegeix una nova condició per fer la consulta amb un AND desprès de la condició que ja fa la clase per si sola <br>
     * La clase sempre posa per si sola la condició per trobar el registre que estem editant a la BBDD (o sigui "WHERE taula_id = $_GET[id]")<br><br>
     * Ej. <br>
     * $show->selects["category_id"]->condition = "language='" . $gl_language . "' ORDER BY ordre ASC";
     *
     * @access public
     * @var string
     */
    var $condition, $original_condition;
    /**
     * Afegeix una left outer join a la consulta
     * Ej. <br>
     * $show->join = "LEFT OUTER JOIN inmo_property_image using (property_id)"
     *
     * @access public
     * @var string
     */
    var $join;
    /**
     * Nom de la funció especificada a "this->call($name, $parameters)"
     *
     * @access private
     * @var string
     */
    var $function_name;
    /**
     * Parametres per la funció especificada a "this->call($name, $parameters)"
     *
     * @access private
     * @var string
     */
    var $function_parameters;
    /**
     * Si passa "&this" com a parametre a "this->call($name, $parameters, $pass_this)"
     *
     * @access private
     * @var string
     */
    var $function_pass_this;
    /**
     * Es passa l'objecte especificat com a paràmetre a "this->call($name, $parameters, $pass_this, &$obj)"
     *
     * @access private
     * @var string
     */
    var $function_object;
    /**
     * Nom base plantilla
     *
     * @access private
     * @var string
     */
    var $template_base;
    /**
     * Canvia id principal a la consulta, per no trobar ambigus en el left outer join;
     *
     * @access public
     * @var string
     */
    var $change_sql_id_field;
    /**
     * Cadena de validacio del javascript;
     *
     * @access public
     * @var string
     */
    var $js_string;

    var $order_by, $order_col, $is_order_asc, $key_values_list, $group_by, $list_show_type,
        $is_site_public, $site_part, $use_default_template, $is_home, $hidden, $out_loop_hidden,
        $has_results, $is_bin, $has_bin, $automatic_condition, $options_filters = array(),
        $options_move_tos = array(), $action_add, $buttons, $buttons_tmp, $options_buttons, $options_buttons_after, $options, $show_langs, $before_table =
		'', $extra_fields, $limit, $page_file_name='', $last_listing, $table_class='', $override_order_by_field = '', $is_print_page = false;
	/**
	 * Si volem que fagi paginació o no;
	 *
	 * @access public
	 * @var boolean
	 */
	var $paginate = true;
	/**
	 * Si volem que es pugui ordenar per columna;
	 *
	 * @access public
	 * @var boolean
	 */
	var $is_sortable = true;

    /**
     * Guarda totes les variables del formulari que s'han agafat de la BBDD o que s'han calculat a partir d'aquestes
     * O sigui, totes les variables que tenen a veure amb el registre de la BBDD que estem visualitzant
     * A list_records guarda el RS actual que s'està processant en el loop de results
     * @access private
     * @var array
     */
    var $rs = array();
    /**
     * $data serveix simplement per guardar variables ( així es pot aprofitar per ejemple en la funcio call
	 * 
     * @access public
     * @return void
     */
	var $propagate_params = true;
	var $form_level1_titles = array(), $form_level2_titles = array(), $data, $inputs_prefix = '', $is_editable = true, $is_excel = false;

    function __construct(&$module)
	{
        $this->link_module($module);
		
	    global $gl_site_part, $gl_is_home, $gl_page;
		
        // passem aqui els valors de validacio de javascript desde la clase db_form_object.

		$this->js_string = '';
        $this->has_bin = true;

    	// globals necessaries
		$this->is_home = $gl_is_home;
        $this->is_site_public = ($gl_site_part == 'public');
        $this->site_part = $gl_site_part;
        $this->is_print_page = $gl_page->template == 'print.tpl';

        $this->template_base = PATH_TEMPLATES . $this->tool . '/' . $this->tool_section . '_';

        $this->order_by = '';
        $this->buttons_tmp = array('auto'=>array(),'before'=>array(),'after'=>array());
		
		if ($GLOBALS['gl_is_admin']){
			if ( ! isset( $_SESSION['last_list'][ $GLOBALS['gl_menu_id'] ] ) ) {
				$_SESSION['last_list'][ $GLOBALS['gl_menu_id'] ] = array( 'name'     => '',
				                                                          'link'     => '',
				                                                          'edit'     => false,
				                                                          'add_dots' => true
				);
			}
			$this->last_listing = &$_SESSION['last_list'][ $GLOBALS['gl_menu_id'] ];
		}
		else {
			if ( $GLOBALS['gl_tool'] && $GLOBALS['gl_tool_section'] ) {
				if ( ! isset( $_SESSION['last_list'][ $GLOBALS['gl_tool'] ][ $GLOBALS['gl_tool_section'] ][ LANGUAGE ] ) ) {
					$_SESSION['last_list'][ $GLOBALS['gl_tool'] ][ $GLOBALS['gl_tool_section'] ][ LANGUAGE ] = array(
						'name' => '',
						'link' => ''
					);
				}
				$this->last_listing = &$_SESSION['last_list'][ $GLOBALS['gl_tool'] ][ $GLOBALS['gl_tool_section'] ][ LANGUAGE ];
			}
		}
		// Debug::p( $_SESSION );
	}
    function list_show()
    {
        $this->buttons = $this->buttons_tmp['before'] + $this->buttons_tmp['auto'] + $this->buttons_tmp['after'];

        // si existeix template a directori eina o si ja s'ha definit un de diferent o si som a la home i existeix l'agafa, si no agafa el generic		
		
		if ($this->use_no_template) // quan no vull passar cap plantilla però que no me l'agafi automàticament

		{
			$this->use_default_template = false;
		} elseif ($this->use_default_template) // quan vull forçar a que me l'agafi automàticament

		{
			$this->template = $this->list_show_type . '.tpl';
			$this->use_default_template = true;
		} elseif ($this->default_template) // quan vull posar un generic diferent a l'automàtic (ej. a variacions de preus de product)

        {
			$this->template = $this->default_template . '.tpl';
            $this->use_default_template = true;
        } elseif ($this->template)
        {
			// si faig més d'un cop show_form em va sumant .tpl al final, per tant nomès sumo .tpl si no hi es
			if (substr($this->template,-4)!='.tpl') $this->template .= '.tpl';
            $this->use_default_template = false;
        /*} elseif ($this->is_home && file_exists($this->template_base . 'home.tpl'))
        {
            $this->template = $this->tool . '/' . $this->tool_section . '_home.tpl';
            $this->use_default_template = false;*/
        } elseif (file_exists($this->template_base . $this->list_show_type . '.tpl'))
        {
            $this->template = $this->tool . '/' . $this->tool_section . '_' . $this->
                list_show_type . '.tpl';
            $this->use_default_template = false;
        } else 
        {
            $this->template = $this->list_show_type . '.tpl';
            $this->use_default_template = true;
        }
		
		// NOM PAGINA SITE PUBLIC
		
		// EVITAR DUPLICATS
		// haig de mirar si un modul està posat en un page
		if ($this->is_site_public){
		
			$this->page_file_name = R::file_name();
			$page_name = '';
			
			if (!$this->page_file_name) {
				// TODO-i Arreglar l'autedetect de les pàgines
				// primer busco els altres, amb el friendly_params
				foreach ( $this->friendly_params as $param => $val ) {

					$query = "SELECT  page_file_name FROM all__page INNER JOIN all__page_language USING(page_id) WHERE language='".LANGUAGE."' AND link LIKE '/".$this->tool.'/'.$this->tool_section. "/$param/%'AND bin=0 AND status='public'";
					$page_name = Db::get_first($query);

					if ($page_name) break;
				}

				if (!$page_name) {
					$query     = "SELECT  page_file_name FROM all__page INNER JOIN all__page_language USING(page_id) WHERE language='" . LANGUAGE . "' AND link = '/" . $this->tool . '/' . $this->tool_section . "'AND bin=0 AND status='public'";
					$page_name = Db::get_first( $query );
				}

				if ($page_name) $this->page_file_name = $page_name;
			}				
		}
    }

    /**
     * variables de fora el bucle, es crida al cridar la funció list_records o show_form
     *
     * @access public
     * @return void
     */
    function set_vars_common()
    {
        global $tpl, $gl_menu_id, $gl_language;

        $this->tpl->set_vars($this->caption);
        $this->tpl->set_vars($this->config);
        $this->tpl->set_var('menu_id', $gl_menu_id);
        $this->tpl->set_var('tool', $this->tool);
        $this->tpl->set_var('tool_section', $this->tool_section);
        $this->tpl->set_var('parent_tool', $this->parent_tool);
        $this->tpl->set_var('parent_tool_section', $this->parent_tool_section);
        $this->tpl->set_var('language', $gl_language);
        $this->tpl->set_var('has_images', $this->has_images);
        $this->tpl->set_var('base_link', $this->base_link);
        $this->tpl->set_var('options_filters', $this->options_filters);
        $this->tpl->set_var('options_move_tos', $this->options_move_tos);
        $this->tpl->set_var('options_buttons', $this->options_buttons);
        $this->tpl->set_var('options_buttons_after', $this->options_buttons_after);
        if ($this->options_move_tos)
            $this->out_loop_hidden .= '<input name="move_field" type="hidden" value="" />';
        $this->tpl->set_var('out_loop_hidden', $this->out_loop_hidden);
        $this->tpl->set_var('table_class', $this->table_class);
        $this->tpl->set_var('inputs_prefix', $this->inputs_prefix);
        $this->tpl->set_var('tr_inputs_prefix', $this->inputs_prefix?$this->inputs_prefix.'_':'');
        $this->tpl->set_var('is_print_page', $this->is_print_page);
        // ordre per defecte o ordre indicat al clicar la columna
        // comprovar que la cadena order_by que s'obtè de la URL sigui correcta
		// order_by es la forma vella order_by=price asc, la forma nova es orderby=price-asc
		
		$orderby = R::escape('orderby');
		$order_by = R::escape('order_by',$orderby);
		
        if ($order_by)
        {
			$order_arr = $orderby?explode('-', $order_by):explode(' ', $order_by);
			
            if (($order_arr[1] != 'asc') && ($order_arr[1] != 'desc'))
            {
                $order_arr[1] = 'asc';
            }
            if (array_key_exists($order_arr[0], $this->fields))
            {
                $this->order_by = $order_arr[0] . ' ' . $order_arr[1];
				if ($this->override_order_by_field) $this->order_by = $this->override_order_by_field . ' ' . $order_arr[1];
                $this->order_col = $order_arr[0];
                $this->is_order_asc = $order_arr[1] == 'asc';
            } else
            {
                //unset($_GET['order_by']);
                //unset($_GET['orderby']); ---> no se si haig de fer uneset, pero quan utilitzo un module::load em fa unset al order principal
				$order_by = false;
            }
        }
        // ordre per defecte donat per la propietat ->order_by
        if (!$order_by)
        {
            if ($this->order_by)
            {
                $order_arr2 = explode(',', $this->order_by);
                $order_arr = explode(' ', $order_arr2[0]);
				
				// perque funcioni tambe per exemple:
				//	FIELD(booking__extra.extra_type,'optional','required','included')
				//	i booking__extra.extra_type ASC
				$point_pos  = strpos($order_arr[0],'.');
				if($point_pos !== false ){
				
					$order_arr[0] = substr($order_arr[0], $point_pos+1);
				
				}
				
                $this->order_col = $order_arr[0];
                $this->is_order_asc = isset($order_arr[1])?$order_arr[1] == 'asc':false;
            } else
            {
                $this->order_col = '';
                $this->is_order_asc = true;
            }
        }
        // poso el nom de la columna, el link de l'ordre i la imatge al tpl
        $fields = array();
        foreach ($this->fields as $key => $value)
        {
	        if (
		        $value['type'] != 'hidden' &&
		        $value[ $this->list_show_type . '_' . $this->site_part ] != '0' &&
		        $value[ $this->list_show_type . '_' . $this->site_part ] != 'no' &&
		        $value[ $this->list_show_type . '_' . $this->site_part ] != 'hidden'
	        )
            {
                // en els llistats automatics segueixo posant el nom de la columna, però sense link
				// en el llistat manual, nomès faig un array de els camps ordenables
				if (($this->list_show_type == 'list'))
                {
                    if (!isset($value['not_sortable']) || $this->use_default_template){
						$field = array('field' => $this->caption['c_' . $key]);
	                    if (!isset($value['not_sortable']) && $this->is_sortable ) $field += $this->set_sorting($key);
						else $field += array('order_link'=>'', 'order_image'=>'');
	                    $fields[$key] = $field;
					}
                }
            }
        }
		
        reset($this->fields);
        $this->tpl->set_var('fields', $fields);
        $this->tpl->set_var('action_add', $this->action_add);
    }

    function process_field($field_config, $key, &$rs) // print_r($rs);

    {
        global $gl_languages, $gl_language;
        // variable comuns a forms i llistat per el rs
        $field_config['name'] = $key;
        $input_id = strstr($this->action, "show_form_copy")?0:$rs[$this->id_field];

        // TODO-i def
        //$field_config['input_name'] = $key . '[' . $input_id . ']';
	    $this->get_input_name ($field_config, $input_id);

		$field_config['input_language'] = '';
        // si estic a un llistat, per defecte nomès mostra un idioma, llavors el nom de l'input ha d'anar l'idioma que he posat automaticament a la consulta, que es el $gl_language
        

        if ($field_config[$this->list_show_type . '_' . $this->site_part] == 'input' &&
            $key != $this->id_field && $key != 'bin' && $field_config['type']!='hidden')
        {
            $required = empty($field_config['required'])?'':$field_config['required'];
            // REVISAR, el js nomès controla un idioma que sigui obligatori emplenar, i aquest idioma es l'idioma que es fa servir a la intranet
            $js_name = $field_config['input_name'];
			
			if (in_array($key, $this->language_fields)){
				foreach ($gl_languages['public'] as $language)
				{ 
					$lang_required = $required && $language==$gl_language;
					$js_name = $field_config['input_name'] . '[' . $language . ']';
					
					$this->js_string .= ",'" . $js_name . "','" .
					addslashes(htmlspecialchars($this->caption['c_' . $key])) .
					"','" . $field_config['type'] .
					 $lang_required . "'";
				}
			}
			else{
            $this->js_string .= ",'" . $js_name . "','" .
            	addslashes(htmlspecialchars($this->caption['c_' . $key])) .
                "','" . $field_config['type'] .
                 $required . "'";
			}
        }
		
		// NO SE SI POUC TREURE-HO , MIRAR call_function si al gun mirar el nom idiomitxzat o no
		// al canviar lo de $js_string, vaig passar això a sota per que s'utilitza el nom al cridar una funció, abans de treure m'haig d'assegurar que no fa petar res més. Buscar tots els ->call_function i que cap sigui de un camp d'idioma
		if (in_array($key, $this->language_fields))
            $field_config['input_name'] .= '[' . $gl_language . ']';
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	    $is_lang_input = in_array($key, $this->language_fields) && $this->show_langs;

        switch (true) // falta datetime, timestamp, year, enum, set, binary, varbinary

        {
            case $key == $this->id_field:
                $val = $rs[$key];
                // nomès per el hidden, si cambio el $val de dalt no m'agafa la resta de fields be, ja que necessita el id real del registre per agafar els valors
                // a ShowForm::show_form() cambio els rs[id] i rs[id_field]
                // $val_h = strstr($this->action, "show_form_copy")?0:$rs[$key];

                $this->hidden .= '<input name="' . $field_config['input_name'] .
                    '" type="hidden" value="' . $input_id . '" />';

				$input_name_copy = str_replace(  $key ,  $key . '_copy', $field_config['input_name']);
                $this->hidden .= '<input name="' . $input_name_copy . '" type="hidden" value="' . $val . '" />';
	            // $this->hidden .= '<input name="' . $field_config['name'] . '_copy[' . $input_id . ']' .                    '" type="hidden" value="' . $val . '" />';
                break;
                // tots els hidden posar en una mateixa variable 'hidden'i fora del array 'fields' per a llistats i forms automatics
                // assigno igualment el hidden a $val per llistats i forms no automatics
                // se suposa que els hidden no son ni en idiomes ni se li assigna una funció
            case $field_config['type'] == 'hidden':
                if (!isset($rs[$key]))
                    $rs[$key] = ''; // per a formularis nous no està definit
                $val = $this->get_input($field_config, $rs[$key], $rs[$this->id_field]);
                $this->hidden .= $val;
                // if ($key=='ordre') echo $val . "  <br> ";
                break;
                // subllistat del camp en tots els idiomes, si ja té un valor vol dir que ja s'ha inclos a la consulta principal
            case $is_lang_input:
                $val_arr = $this->sublist_languages($field_config, $rs[$this->id_field], isset($rs['blocked'])?
                    $rs['blocked']:'');
				$val = $val_arr['val'];
				$rs[$key . '_value'] = $val_arr['original_val'];
                break;
                // cridar funcio per obtenir valor final
            case isset($this->call_function[$key]):
                $rs[$key] = ($this->list_show_type == 'form' && $this->id == 0)?$this->fields[$key]['default_value']:
                $rs[$key]; // agafa valor per defecte nomès als formularis nous
                $val = call_user_func($this->call_function[$key], $field_config['input_name'], $rs[$key]);
                break;
				// cridar funcio per obtenir valor final en tota la resta menys el camp id que es queda com està
			case $field_config['type'] == 'none':
				$val = isset($rs[$key])?$rs[$key]:'';// per a formularis nous no està definit
				break;
            case $key != $this->id_field:
                if (!isset($rs[$key]))
                    $rs[$key] = ''; // per a formularis nous no està definit
                $val_arr = $this->get_input($field_config, $rs[$key], $rs[$this->id_field], true);
				$val = $val_arr['val'];
				if ($val_arr['original_val']!==false) $rs[$key . '_value'] = $val_arr['original_val'];
                break;
        } // switch
        if ($this->use_default_template)
        {
            // unset($rs[$key]);
            if ($field_config['type'] != 'hidden')
            {
                // per camps sense idioma definir 'lang' per no donar 'undefined' als tpl
                // i posar val en 2 nivells més endins a l'array
                if (!is_array($val))
                {
                    $v[0] = array('lang' => '', 'val' => $val);
                } else
                {
                    $v = $val;
                }
                $rs['fields'][] = array(
					'field_name' => $key,
					'key' => $this->caption['c_' . $key],
					'val' => $v,
	                'is_lang_input' => $is_lang_input,
					'td_class' => isset($field_config['td_class'])?' class="'.$field_config['td_class'].'"':'',
					// titol, nivell 1 i 2 d'elements del formulari
					'form_level1_title' => (in_array($key,$this->form_level1_titles))?$this->caption['c_form_level1_title_' . $key]:false,	
					'form_level2_title' => (in_array($key,$this->form_level2_titles))?$this->caption['c_form_level2_title_' . $key]:false
				);	
				
				
                unset($v);
            }
        }
        // else
        // {
        // millor utilitzar sempre el format per plantilla a mida encara que s'utilitzi el use_default_template,
        // així es pot fer un tipus de plantilla com el dels inmobles amb 2 camps com a títol
        $rs[$key] = $val;
        // }
    }
    // ----------------------
    // Afegeix filtres al menu opcions, els filtres es complementen uns als altres
	// filter_relation_table = taula intermija el filtre en ralacio molts a molts
    // ----------------------
    function add_filter($field, $filter_relation_table = '', $select_size = '', $type = 'select', $is_automatic_filter = true)
    {
        $this->fields[$field]['filter_relation_table'] = $filter_relation_table;
        $this->fields[$field]['is_automatic_filter'] = $is_automatic_filter;
		$cg = $this->fields[$field] + array('name' => $field, 'input_name' => $field, 'input_language' => '');
        // sobreescric aquest valors ja que sempre vull un input al crear un filtre
        $cg[$this->list_show_type . '_' . $this->site_part] = 'input';
        $cg['type'] = $type;
        $cg['class'] = 'filter';
        $cg['select_size'] = $select_size;
        $cg['javascript'] = 'onChange="javascript:window.location=\'' .
            get_all_get_params(array($field, 'page' . $this->name),'?','&',true) . $field . '=\'+this.options[this.selectedIndex].value"';
        if (isset($this->caption['c_filter_' . $field]))
        {
            $cg['select_caption'] = $this->caption['c_filter_' . $field];
        }
        $caption2 = isset($this->caption['c_filter_' . $field . '2'])?$this->caption['c_filter_' .
            $field . '2'] . ': ':'';
        $this->always_parse_template = true;
		$value = R::escape($field,'null');
        $this->options_filters[$field] = $caption2 . $this->get_input($cg, $value,1);
    }

	/**
	 * Funció per crear un filtre i posar-lo a la linea de filtres però sense necessitat de estar relacionat amb cap field
	 * Per posar la primera opció com a enunciat, posar el valor '' ( buit ) <br><br>
	 * Exemple:
	 * <code>
	    $listing->add_custom_filter(
			'excluded',
			['','fotocasa','apicat']
		);
	 * </code>
	 *
	 * @param string $name Nom del filtre
	 * @param array $values <ol><li>Pot ser associativa amb value i text del option </li> <li>sequencial i agafar el text automàticament dels captions ( c_ + Nom del filtre + _ + valor del item de l'array )</li></ol>
	 */
	function add_custom_filter($name, $values){

    	$is_associative = key($values) != 0;
    	$filter = '';
    	$selected_value = R::get($name);

    	foreach ( $values as $k => $v ) {
		    if ( $is_associative ) {
		    	$value = $k;
			    $text = $v;
		    }
		    else {
		    	$value = $v;
			    $text = $this->caption["c_${name}_$v"];
		    }
		    if ($value == '') $value = 'null';

		    $selected = $value == $selected_value ? 'selected' : '';
		    $filter .= "<option value='$value' $selected>$text</option>";
    	}

    	$on_change = 'onChange="javascript:window.location=\'' .
            get_all_get_params(array($name, 'page' . $this->name),'?','&',true) . $name . '=\'+this.options[this.selectedIndex].value"';

    	$this->options_filters[$name] = "<select name='$name' class='filter' $on_change>$filter</select>";
    }

    // Poso els inputs en un array de nom "input_prefix" per al post.
	// Sense prefix: nom_camp[id]
	// Amb prefix: tool__tool_section[nom_camp][id]
	function get_input_name( &$field_config, $id ) {
		$f_name           = $field_config['name'];
		$f_input_language = isset($field_config['input_language'])?$field_config['input_language']:false;
		$field_config['name_no_prefix']       = $f_name;

		if ( ( $this->inputs_prefix ) ) {
			$prefix = $this->inputs_prefix;

			$field_config['input_name'] = "${prefix}[${f_name}][$id]";
			$field_config['name']       = "${prefix}_${f_name}";

		} else {
			$field_config['input_name'] = "${f_name}[$id]";
		}
		if ( $f_input_language ) {
			$field_config['input_name'] .= "[$f_input_language]";
		}
	}

    // ----------------------
    // Tipus de valors, inputs, desplegables, etc
    // ----------------------
    function get_input($field_config, $value, $id, $return_original_val=false) {

	    $input = new FormObject( $field_config, $value, $this->list_show_type, $id, $this );
	    $r     = $input->get_input( true, $this->is_excel );
	    if ( $return_original_val ) {
		    return $r;
	    } else {
		    return $r['val'];
	    }
    }
    /**
     * Funció a cridar en cada registre
     *
     * @param string $name Nom del la funció que es crida
     * @param string $parameters Valors dels camps enviats com a parametres a la funcio
     *
     * 	Ej. call("a","menu_id, tool") es cridarà la funció a($rs['menu_id'],$rs['tool'])
     * @access public
     * @return void
     */
    function call($name, $parameters, $pass_this = false, &$object = false)
    {
        $this->function_pass_this = $pass_this;
        if ($object) $this->function_object = &$object;
        $this->function_name = $name;
        $this->function_parameters = explode(',', $parameters);
    }
    function get_call_rs(&$rs)
    {
        if ($this->function_name)
        {
            $parameters = array();
            if ($this->function_pass_this)
                $parameters[] = &$this;
            foreach ($this->function_parameters as $param)
            {
                $parameters[] = isset($rs[$param])?$rs[$param]:false;
            }

            if ($this->function_object) {
	            $function_name = array( &$this->function_object, $this->function_name );
            }
            else {
				// si no hi ha el metode, busca una funcio ( em va be quan barrejo mòduls amb v2 com a custumer_demand_functions linea 108
	            $function_name = $this->module && method_exists( $this->module, $this->function_name ) ?
		            array( &$this->module, $this->function_name ) :
		            $this->function_name;
            }
            $ret = call_user_func_array($function_name, $parameters);
            if (is_array($ret)) $rs = array_merge($rs, $ret);

            if (isset($ret['hidden']))
                $this->hidden .= $ret['hidden'];
            // això lo seu es posar el primer argument del call : this,
            // i fer servir this->hidden directament i altres que aniran sortint,
            // hauria de tindre tot l'objecte a mà dins la funció de call
            // $parameters = array(&this);
			// **** ara ja li tinc aquest paràmetre, haig d'arreglar aixòoooooooooooooooo *****
        }
    }

	public function _set_field_value_after_set_records( &$rs, $ret_key, $ret_val ) {

    	// Si no es una plantilla automàtica no fem res
    	if (empty($rs['fields'])) return;

		foreach ( $rs['fields'] as &$field ) {
			if ( $field['field_name'] == $ret_key ) {
				foreach ($field['val'] as &$v){
					if ( $v['lang'] ) {
						die( 'No està fet per idiomes encara' );
					}
					else {
						$v['val'] = $ret_val;
					}
				}
			}
		}
	}
    /**
     *
     * @access public
     * @return array
     */
    function sublist_languages($field_config, $id, $blocked)
    {
        global $gl_languages;
        $field = $field_config['name'];
        $num_languages = count($gl_languages['public']);
        $key_to_use = $this->use_default_template?'val':$field;
	    $original_val = '';
        // REVISAR    - Masses consultes (una per cada camp amb idioma i cada resultat, poden ser mes de 50 en una pàgina
        if ($id)
        {
            // poso l'inner join per poder ordenar segons l'ordre dels idiomes
            if ($field_config['type'] == 'file'){
			// aquest query fa que surtin tots els idiomes, encara que no hi hagi cap registre
			$query = "SELECT '' AS " . $field . ", 
						'" . $id . "' AS " . $this->id_field . ", 
						all__language.code AS language
						FROM all__language
						WHERE all__language.public <> 0
						ORDER BY all__language.ordre";
			}else{
			// aquest query fa que no surtin tots els idiomes, S?HAURIA DE FER COM A DALT????
			$query = "SELECT " . $field . ", " . $this->id_field . ", all__language.code as language
					FROM " . $this->table . "_language
		            INNER JOIN all__language 
					ON " . $this->table . "_language.language = all__language.code
					WHERE " . $this->id_field . "='" . $id . "'
					ORDER BY all__language.ordre";			
			}
			Debug::add('Query sublist_language',$query);
            $results = Db::get_rows($query);
			//Debug::add('Results sublist_language',$results);
            foreach ($results as $rs)
            {
            	// TODO-i def
                // $field_config['input_name'] = $field . '[' . $id . '][' . $rs['language'] . ']';
				$field_config['input_language'] = $rs['language'];
	            $this->get_input_name ($field_config, $id);

	            // poso per valor original el de l'idioma actual ( per fer servir per exemple en les imatges d'ImageManager image_alt )

	            if ($rs['language'] == LANGUAGE) $original_val = $rs[$field];

                if (isset($this->call_function[$field]))
                {
                    $rs[$key_to_use] = call_user_func($this->call_function[$field], $field_config['input_name'],
                        $rs[$field]);
                }
                // crear inputs
                else
                {
                    if ($blocked)
                        $field_config[$this->list_show_type . '_' . $this->site_part] = 'text';
					
					$val_arr = $this->get_input($field_config, $rs[$field], $rs[$this->
                        id_field], true);
                    $rs[$key_to_use] = $val_arr['val'];
					if ($val_arr['original_val']!==false) $rs[$key_to_use . '_value'] = $val_arr['original_val'];
					
                    if ($blocked)
                        $rs[$key_to_use] = '<img src="' . DIR_TEMPLATES_ADMIN . '/images/lock.gif">' . $rs[$key_to_use];
                }
                $rs['lang'] = $num_languages > 1 && $rs[$key_to_use]?$gl_languages['names'][$rs["language"]]:
                    '';
                $loop[] = $rs;
            }
        } // formulari nou
        else
        {
            foreach ($gl_languages['public'] as $language)
            {
            	// TODO-i def
                //$field_config['input_name'] = $field . '[0][' . $language . ']';
				$field_config['input_language'] = $language;

	            $this->get_input_name ($field_config, $id);

                if (isset($this->call_function[$field]))
                {
                    $rs[$key_to_use] = call_user_func($this->call_function[$field], $field_config['input_name'],
                        $field_config['default_value']);
                }
                // crear inputs
                else
                {					
					$val_arr = $this->get_input($field_config, $field_config['default_value'],
                        '0', true);
                    $rs[$key_to_use] = $val_arr['val'];
					if ($val_arr['original_val']!==false) $rs[$key_to_use . '_value'] = $val_arr['original_val'];
                }
                $rs['lang'] = $num_languages > 1?$gl_languages['names'][$language]:'';
                $loop[] = $rs;
            }
        }
        return array('val' => $loop, 'original_val' => $original_val);
    }
    // ----------------------
    // Query
    // ----------------------
    function get_query()
    {
        global $gl_language;
        $this->original_condition = $this->condition;
        $field_list = '';
        $fields = array();
        $lang_field_included = false;
		
        foreach ($this->fields as $key=>$val)
        {
            // si es checkboxes ho excloc de la consulta, s'enten que es una relació de taules de bbdd de "molts a molts"
            // quan es out es mostrarà el camp però no l'agafa de la BBDD, per donar-li un valor s'ha de fer amb ->call()
            // incloure camp quan no es camp de idioma, o (si es camp de idioma i no els mostrem tots)
            // operacions logiques:
            // !LangField || (LangField && !showLangs)
            // !b || (b && !a) = (!b || b) && (!b || !a) per tant com que !b || b sempre es true
            // (!b || b) && (!b || !a) = !b || !a
            // conclusio
            // b || (b && !a) = !b || !a
            // si l'idioma està en el camp ordre també afegeixo el camp per així poder ordenar
            $notLangField_OR_LangFieldANDnotShowLangs = (!$this->show_langs) || (!in_array($key,
                $this->language_fields)) || strstr($this->order_by, $key);
            if (
                ($notLangField_OR_LangFieldANDnotShowLangs) &&
                ($val['type'] != 'checkboxes') &&
                ($val['type'] != 'checkboxes_long') &&
                ($val['type'] != 'sublist') &&
                ($val['type'] != 'file') &&
                ($val[$this->list_show_type . '_' . $this->site_part] != 'out') &&
                ($val[$this->list_show_type . '_' . $this->site_part] != 'no')
            )
            {
                // per previndre error sql 'ambiguous column' al camp id quan es fa join
                if ($key == $this->id_field)
                {
                    // change_sql_id_field = canviar el valor del camp id per quan es necesssari: taula.camp_id
                    $key = ($this->change_sql_id_field)?$this->change_sql_id_field:$this->table .
                        '.' . $this->id_field . ' AS ' . $this->id_field;
                }
                if (isset($val['change_field_name']))
                {
                    $key = $val['change_field_name'];
                }
                $fields[$key] = $val;
                // si hi ha un camp idioma dins aquest if, farem l'inner join
                // el || es per no cridar sempre la funció in_array, quan lang_field_included sigui true ja no la cridarà mes
                $lang_field_included = $lang_field_included || in_array($key, $this->
                    language_fields);
            }
        }
        $AND_string = $this->automatic_condition?' AND ':'';
        // posem consulta paperera automaticament
        if (($this->has_bin && $this->list_show_type == 'list') || ($this->has_bin && $this->
            is_site_public))
        {
            $this->automatic_condition .= $AND_string . ($this->is_bin?$this->table .
                '.bin = 1':$this->table . '.bin = 0');
            $AND_string = ' AND ';
        } elseif ($this->has_bin && $this->list_show_type == 'form')
        {
            // em fa falta saber el valor del bin en els formularis
	        // TODO-i Fer automatic per totes les eines i així treure el has_bin
	        if (is_field($this->table, 'bin')) {
		        $fields['bin'] = ''; // el valor no importa, nomès vull que inclogui el camp a la consulta
	        }
        }
        // posem consulta ids automaticament
		$ids = R::ids($this->id_field . 's');
        if ($ids && $this->list_show_type == 'list')
        {
            $this->automatic_condition .= $AND_string . $this->id_field . ' IN (' . $ids . ') ';
            $AND_string = ' AND ';
        }
        // posem consulta per filtres automaticament
        foreach ($this->options_filters as $key => $value)
        {
            $value = R::escape($key);

            // Excloc els custom filters
			if (isset($this->fields[$key]) && $value!==false && $value!='null' && $this->fields[$key]['is_automatic_filter'])
            {
				// per que funcioni el filtre quan change_field_name es igual a ej. tool AS tool2
				if (isset($this->fields[$key]['change_field_name']))
				{
					$key_arr = explode(' ', $this->fields[$key]['change_field_name']);
					$key2 = $key_arr[0];
				}
				else{
					$key2 = $key;
				}
            	// fi de change_field_name
				
				// relacio molts a molts
				if ($this->fields[$key]['filter_relation_table']){
                $this->automatic_condition .= $AND_string . $this->id_field . " IN(
						SELECT ".$this->id_field . "
						FROM ".$this->fields[$key]['filter_relation_table']."
						WHERE ".$key2." ='" . $value . "')";				
				}
				else{
                $this->automatic_condition .= $AND_string . $key2 . "='" . $value . "'";
				}
                $AND_string = ' AND ';
            }
        }

		$query = '';
        if ($this->join){
            $query = " " . $this->join;
        }
		
		
		// condicio sessio - abans de l'idioma, ja que no hi va mai
        $AND_string_last = $this->automatic_condition && $this->condition?' AND ':'';
		
		if ($this->is_index && $this->list_show_type=='list'){
            Main::load_class( 'db_utils' );
            Utils::set_last_list_condition($this->automatic_condition . $AND_string_last . $this->condition, $this->tool, $this->tool_section, $this->order_by);
        }
		
        // Consulta JOIN de l'idioma		
        if ($lang_field_included)
        {
            $query .= " LEFT OUTER JOIN " . $this->table . "_language using (" . $this->
                id_field . ")";
            $this->automatic_condition .= $AND_string . $this->table .
                "_language.language = '" . $gl_language . "'";
        }
		
		// condicio final
        $AND_string = $this->automatic_condition && $this->condition?' AND ':'';
        $this->condition = $this->automatic_condition . $AND_string . $this->condition;
		
        if ($this->condition)
            $query .= " WHERE " . $this->condition;
        if ($this->group_by)
            $query .= " GROUP BY " . $this->group_by;

        $query_split = $query; // query split no li sumo ni order ni limit

        // quan tenim un llistat d'ids i no em definit cap ordre
        // (al clicar sobre ordenar per un camp) ordenem per l'ordre tal com llistem els ids
        if ($ids && !isset($_GET['order_by']))
        {
            $order_ids = explode(',', $ids);
            $order_ids = implode('\',\'', $order_ids);
            $query .= " ORDER BY FIELD(" . $this->table . '.' . $this->id_field . ", '" . $order_ids .
                "')";
            $this->order_by = '';
        }
        if ($this->order_by)
			$query .= " ORDER BY " . $this->order_by;
		if ($this->limit)
			$query .= " LIMIT " . $this->limit;	

        // posem els fields
        $field_list = join(array_keys($fields), ", ");
        $query_split = $this->before_table . $this->
            table . $query_split;
        $query = "SELECT " . $field_list . ($this->extra_fields?','.$this->extra_fields:'') . " FROM " . $this->before_table . $this->
            table . $query;
        Debug::add('Consulta list o form', $query);
    	$this->automatic_condition = ''; // faig un reset per poder tornar a utilitzar un list_records o show_form
        return array('q' => $query, 'split'=>$query_split);
    }

    // carrega un config diferent al config per defecte, de moment nomès s'utilitza al llistat d'imatges
	function set_config($tool_section)
    {
		//$this->tool = $tool?$tool:false;
		$this->tool_section = $tool_section;
        include (DOCUMENT_ROOT . 'admin/modules/' . $this->tool . '/' . $this->tool .
            '_' . $this->tool_section . '_config.php');
        $this->language_fields = $gl_db_classes_language_fields;
        $this->fields = $gl_db_classes_fields;
        $this->table = $this->tool . '__' . $this->tool_section;
        $this->id_field = $this->tool_section . '_id';
    }
    // guarda cada boto que afegim en un array ($this->buttons)
    // per desprès poder contruir els botons en cada registre
    // $params = parametres que s'inclouen o es sobreescriuen als actuals en la url del botó
    //          sempre s'ha de posar un 'action' => 'show_form_ssssssss'
    // $place = 
	//	before - abans dels botons predeterminats
    //	after -  desprès dels botons predeterminats
    //	auto  -  es per construir els botons automatics que genera l'eina
    //	top_before  -  va a la zona de top buttons
    //	top_after  -  va a la zona de top buttons
    // (editar dades, insertar imatges, editar imatges)
    function add_button($name, $params, $class = 'boto1', $place = 'before', $javascript =
        '', $attribs = '', $id_field=false, $propagate_params=true, $link = false)
    {
        if (!$id_field) $id_field = $this->id_field;
		
		global $gl_page;
        $params_arr = array();
        parse_str($params, $params_arr);
        $params_arr = array_keys($params_arr);
        array_push($params_arr, $id_field, 'page' . $this->name);

	    $javascript_params = $params;

		$params = str_replace('&','&amp;',$params); // ho faig HTML correcte
        $is_options_button = $place=='top_before' || $place=='top_after';

		if ($is_options_button) {
			$link = $propagate_params?get_all_get_params($params_arr,'?',$params?'&':'') . $params:'?'.$params;
		}
		else {	
			if ($params) $params .= '&amp;';
			if ($link!=false){
				$link = $link . '&amp;' . $id_field . "=";
			}
			else{
				$link = $propagate_params?get_all_get_params($params_arr,'?','&') . $params . $id_field . "=":'?'.$params . $id_field . "=";				
			}
		}

        if ($javascript)
        {
            // Poso paràmetres a tots, aquests paràmetres son fixes, no s'en posa cap dinàmicament al llistat
	        // per si acas busco paràmetres sense ' , com pot ser en alguns phps vells on no es tenia en compte que si hi ha paràmetres es passen també al jscript
	        if (!$is_options_button && $javascript_params){
	        	// poso la coma per separar de l'altre paràmetre automàtic de l'id
		        if ( strpos( $params, '\'' ) === false ) {
			        $javascript_params = "'" . $javascript_params . "'";
		        }
		        $javascript_params = ', ' . $javascript_params;
	        }


            $single_quote = $is_options_button ? '' : '\'';
            $link_start = '<a href="javascript:' . $javascript . '(' . $single_quote;
            $link_end = $single_quote . $javascript_params . ');" class="' . $class . '" ' . $attribs . '>' . $this->caption['c_' .
                $name] . '</a>';

            /* Ara va en tots els botons
             if ($is_options_button)
                $link_start .= $params;*/

        } else
        {
            $link_start = '<a href="' . $link;
            $link_end = '" class="' . $class . '" ' . $attribs . '>' . $this->caption['c_' .
                $name] . '</a>';
        }

        $button = array('link_start' => $link_start, 'link_end' => $link_end, 'show' => true, 'params' => '');
        if ($gl_page->template != 'print.tpl')
        {
            if ($place=='top_before') $this->options_buttons []= $link_start . $link_end;
            elseif ($place=='top_after') $this->options_buttons_after []= $link_start . $link_end;
			else  $this->buttons_tmp[$place][$name] = $button;
        }
    }
    // Afegeix un boto a les opcions ( un per tota la pàgina )
    function add_options_button($name, $params, $class = 'botoOpcions1', $place = 'top_before', $javascript =
        '', $attribs = '')
    {
        $this->add_button($name, $params, $class,  $place, $javascript, $attribs);
    }
    // Afegeix un boto a les opcions ( un per tota la pàgina )
    function add_options_button_after($name, $params, $class = 'botoOpcions1', $place = 'top_after', $javascript =
        '', $attribs = '')
    {
        $this->add_button($name, $params, $class,  $place, $javascript, $attribs);
    }
    // monta els botons per cada registre un cop cridem list_records o show_form
    function set_buttons(&$rs)
    {
        $buttons_arr = $this->buttons;
        if (isset($rs['image_name']) && $rs['image_name'])
        {
            unset($buttons_arr['add_images_button']);
        } else
        {
            unset($buttons_arr['edit_images_button']);
        }
        $buttons = array();
        foreach ($buttons_arr as $key => $button)
        {
            if ($button['show'])
                $buttons[$key] = $button['link_start'] . $rs[$this->id_field] . $button['params'] . $button['link_end'];
        }

        $rs['buttons'] = $buttons;
    }


	function set_fields_editable ( $can_edit = false ) {

		static $original_fields;
		static $original_buttons;

		if ( ! $original_fields ) {
			$original_fields = $this->fields;
			$original_buttons = $this->buttons;
		}

		if ( $can_edit ) {
			$this->fields = $original_fields;
		    $this->buttons  = $original_buttons;
		}

		else {
			foreach ( $this->fields as $key => $value ) {
				$f = &$this->fields[ $key ];
				$f[ $this->list_show_type . '_' . $this->site_part ] == 'input' ? $f[ $this->list_show_type . '_' . $this->site_part ] = 'text' : false;
			}

		    if (isset($this->buttons['edit_button'])) $this->buttons['edit_button']['show']  = false;
		    if (isset($this->buttons['image_button'])) $this->buttons['image_button']['show'] = false;
		}
	}

    function rtf($name, $debug = false)
    {

        global $gl_content;
        if (!$debug)
        {
            header("Content-disposition: attachment; filename=" . $name . ".rtf");
            header("Pragma: public");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Cache-Control: private", false);
            header("Content-Type: application/rtf");
            header("Content-Transfer-Encoding: binary");
        }
        echo $gl_content;
        die();
    }
	// per comptabilitat tpl;
	function set_file($file, $clear = false){
		$this->template = substr($file,0,-4);
	}

    /**
     * ListRecords::reset()
     * Funció que posa a cero condition i rs per reutilitzar el mateix show_form per obtindre un altre registre, matinguent altres opcions com action, has_bin i file (product_customer.php, m'agafa el record primer d'una adreça i despres una altra )
     * Se suposa que quan es faci servir voldrem matindre les opcions de has_bin, file i action, i ja es veurà quines més
     */
	function reset(){
		$this->condition = '';
		$this->rs = array();
	}
	
	// 
	function get_show_record_link($rs) {
		if ($this->is_site_public && USE_FRIENDLY_URL){

			$file_name = isset($this->fields[$this->tool_section . '_file_name'])?$rs[$this->tool_section . '_file_name']:false;
			$category_file_name = isset($_GET['filecat_file_name'])?$_GET['filecat_file_name']:false;//$rs['filecat_file_name']?$rs['filecat_file_name']:false; // falta fer

			$params_arr = array('action', $this->id_field, 'id', 'page'.$this->name, 'tool', 'tool_section','language','page_file_name');

			if (R::get('template') === '') $params_arr [] = 'template';

			if (!$this->propagate_params) $params_arr = false;
			
			if (isset($_GET['loginnewsletter'])){
				$params_arr = false;
			}

            $show_record_link = Page::get_link($this->parent_tool?$this->parent_tool:$this->tool, $this->parent_tool_section?$this->parent_tool_section:$this->tool_section, false, $rs[$this->id_field], $this->friendly_params, $params_arr, $file_name, $category_file_name, $this->page_file_name);			

		}
		else{		

			if (isset($_GET['loginnewsletter'])){
				$params = '';
			}
			else{
				$params = get_all_get_params(array('action', $this->id_field, 'page'.$this->name, 'tool', 'tool_section'),'&');
			}

			$show_record_link = '?' . $this->base_link . '&amp;action=show_record' . $this->action_add . '&amp;' . $this->id_field . '=' . $rs[$this->id_field] .
			$params;
		}
		
		return $show_record_link;
	}
	
	// Nomes a FRIENDLY per obtindre un enllaç al registre
	// 
	// No faig comprobacions de l'ID, s'ha de fer abans de cridar aquesta funció
	// es per quan vull un enllaç a un registre d'un modul diferent al que estic, 
	// si estic al modul ja tinc el show_record_link
	static public function get_record_link($tool, $tool_section, $id, $page_id, $friendly_params) {
		
		$file_name = Db::get_first("
						SELECT " . $tool_section . "_file_name
							FROM " . $tool . "__" . $tool_section . "_language 
							WHERE " . $tool_section . "_id = " . $id . "
							AND language = '" . LANGUAGE . "'");
		
		if ($page_id)
			$page_file_name = Db::get_first("
						SELECT page_file_name
							FROM all__page_language 
							WHERE page_id = " . $page_id . "
							AND language = '" . LANGUAGE . "'");
		else
			$page_file_name = false;
			
		return Page::get_link($tool, $tool_section, false, $id, $friendly_params, false, $file_name, false, $page_file_name);	
	}
}

function get_record_link($tool, $tool_section, $id, $page_id = false, $friendly_params=array()) {
	return BaseListShow::get_record_link($tool, $tool_section, $id, $page_id, $friendly_params);
}