<section id="search-advanced">

	<h5><?= $c_frase_buscador ?></h5>

	<?
	extract( get_page( 'property' ), EXTR_PREFIX_ALL, 'sell' );
	extract( get_page( 'temp' ), EXTR_PREFIX_ALL, 'temp' );
	extract( get_page( 'lloguer' ), EXTR_PREFIX_ALL, 'rent' );
	extract( get_page( 'rent' ), EXTR_PREFIX_ALL, 'moblat' );
	extract( get_page( 'rent' ), EXTR_PREFIX_ALL, 'selloption' );
	$found_selected = false;
	?>

	<div class="search-content" id="search-content">

		<label class="col col1">
			<? if($title): ?><span class="title"><?= $title ?></span><? endif ?>

			<? /* Aquí és el mateix que a inmo_filtres */ ?>

			<? foreach($loop as $l): extract( $l ) ?>
				<span class="select"><select name="<?= $var_name ?>" class="<?= $block_id ?>">

						<option value=""><?= $c_select ?></option>
						<? foreach($loop as $l): extract( $l ) ?>
							<?
							// Que no em mostri el desplegables si no està la pagina activa
							if(
								( ( $value == 'rent' || $value == 'moblat' || $value == 'selloption' ) && get_page_link( 'rent' ) )
								||
								( $value == 'temp' && get_page_link( 'temp' ) )
								||
								( $value == 'sell' && get_page_link( 'property' ) )
							): ?>

								<option value="<?= $value ?>"<? if($selected)
									echo ' selected' ?>><?= $item ?></option>
								<?
								${'has_tipus_' . $value}          =true;
								if ($selected) $found_selected = true;
								${'tipus_' . $value . '_selected'}=$selected?' selected':'';
								?>

							<? endif; ?>
						<? endforeach //$loop?>
					</select></span>
			<? endforeach //$loop?>

		</label>

		<? if(!$found_selected) $tipus_sell_selected=' selected' // selecciono el primer si no hi ha tipus ?>


		<? if(!empty( $has_tipus_sell )): ?>
			<form id="filter-sell" name="filtres" action="<?= $sell_p_link ?>" method="get" class="hidden<?= $tipus_sell_selected ?>">
				<input type="hidden" name="tipus" value="sell">
				<label class="col col2">
					<?= get_block( 'inmo_category_filter', 'tipus=sell' ) ?>
				</label>
				<label class="col col3">
					<?= get_block( 'inmo_room_filter', 'tipus=sell' ) ?>
				</label>
				<label class="col col1">
					<?= get_block( 'inmo_municipi_filter', 'tipus=sell' ) ?>
				</label>
				<label class="col col2">
					<?= get_block( 'inmo_price_filter', 'tipus=sell' ) ?>
				</label>

				<label class="col col3">
					<input type="submit" class="search-button" value="<?= $c_cercar_immobles ?>"/>
				</label>

			</form>
		<? endif // $has_tipus_sell ?>


		<? if(!empty( $has_tipus_temp )): ?>
			<form id="filter-temp" name="filtres" action="<?= $temp_p_link ?>" method="get" class="hidden<?= $tipus_temp_selected ?>">
				<input type="hidden" name="tipus" value="temp">
				<?=get_block('inmo_temp_search')?>
				<? /* <label class="col col2">
 					<?= get_block( 'inmo_category_filter', 'tipus=temp' ) ?>
 				</label>
 				<label class="col col3">
 					<?= get_block( 'inmo_room_filter', 'tipus=temp' ) ?>
 				</label>
 				<label class="col col1">
 					<?= get_block( 'inmo_municipi_filter', 'tipus=temp' ) ?>
 				</label>
 				<label class="col col2">
 					<?= get_block( 'inmo_price_filter', 'tipus=temp' ) ?>
 				</label> */ ?>
				

				<label class="col col3">
					<input type="submit" class="search-button" value="<?= $c_cercar_immobles ?>"/>
				</label>
			</form>
		<? endif // !empty($has_tipus_temp) ?>


		<? if(!empty( $has_tipus_moblat )): ?>
			<form id="filter-moblat" name="filtres" action="<?= $moblat_p_link ?>" method="get" class="hidden<?= $tipus_moblat_selected ?>">
				<input type="hidden" name="tipus" value="moblat">
				<label class="col col2">
					<?= get_block( 'inmo_category_filter', 'tipus=moblat' ) ?>
				</label>
				<label class="col col3">
					<?= get_block( 'inmo_room_filter', 'tipus=moblat' ) ?>
				</label>
				<label class="col col1">
					<?= get_block( 'inmo_municipi_filter', 'tipus=moblat' ) ?>
				</label>
				<label class="col col2">
					<?= get_block( 'inmo_price_filter', 'tipus=moblat' ) ?>
				</label>

				<label class="col col3">
					<input type="submit" class="search-button" value="<?= $c_cercar_immobles ?>"/>
				</label>
			</form>
		<? endif // $has_tipus_temp ?>


		<? if(!empty( $has_tipus_rent )): ?>

			<form id="filter-rent" name="filtres" action="<?= $rent_p_link ?>" method="get" class="hidden<?= $tipus_rent_selected ?>">
				<input type="hidden" name="tipus" value="rent">
				<label class="col col2">
					<?= get_block( 'inmo_category_filter', 'tipus=rent' ) ?>
				</label>
				<label class="col col3">
					<?= get_block( 'inmo_room_filter', 'tipus=rent' ) ?>
				</label>
				<label class="col col1">
					<?= get_block( 'inmo_municipi_filter', 'tipus=rent' ) ?>
				</label>
				<label class="col col2">
					<?= get_block( 'inmo_price_filter', 'tipus=rent' ) ?>
				</label>

				<label class="col col3">
					<input type="submit" class="search-button" value="<?= $c_cercar_immobles ?>"/>
				</label>

			</form>

		<? endif //  $has_tipus_rent  ?>

		<? if(!empty( $has_tipus_selloption )): ?>
			<form id="filter-selloption" name="filtres" action="<?= $selloption_p_link ?>" method="get" class="hidden<?= $tipus_selloption_selected ?>">
				<input type="hidden" name="tipus" value="selloption">

				<label class="col col2">
					<?= get_block( 'inmo_category_filter', 'tipus=selloption' ) ?>
				</label>
				<label class="col col3">
					<?= get_block( 'inmo_room_filter', 'tipus=selloption' ) ?>
				</label>
				<label class="col col1">
					<?= get_block( 'inmo_municipi_filter', 'tipus=selloption' ) ?>
				</label>
				<label class="col col2">
					<?= get_block( 'inmo_price_filter', 'tipus=selloption' ) ?>
				</label>

				<label class="col col3">
					<input type="submit" class="search-button" value="<?= $c_cercar_immobles ?>"/>
				</label>

			</form>
		<? endif // $has_tipus_selloption ?>

	</div>
	<div class="bottom"></div>

</section>