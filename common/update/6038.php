<?

/* 11-04-15
  Canvi inmo, afegir camps. Invetin


 */

if (is_table('inmo__property') && !is_field('inmo__property','managed_by_owner')) {

	$update_sql .= "
			ALTER TABLE inmo__property
				ADD COLUMN managed_by_owner TINYINT(1) NOT NULL DEFAULT '0' AFTER time_out,
				ADD COLUMN `floor_space_build` INT(6) NOT NULL DEFAULT '0' AFTER `floor_space`,
				ADD COLUMN `parking_area` INT(3) NOT NULL DEFAULT '0' AFTER `parking`,
				ADD COLUMN `office` TINYINT(1) NOT NULL DEFAULT '0' AFTER `terrace_area`,
				ADD COLUMN `office_area` INT(3) NOT NULL DEFAULT '0' AFTER `office`,
				ADD COLUMN `property_height` INT(5) NOT NULL DEFAULT '0' AFTER `office_area`;
		";
}
else {
	if (is_table('inmo__property')) {
		trigger_error('Ja existeixen el camp managed_by_owner');
	}
}

?>