<?php
function get_vars_llemena_agenda($content_ids, &$ids, &$names)
{
    $query = "SELECT llemena__agenda.agenda_id AS agenda_id, agenda
				FROM llemena__agenda, llemena__agenda_language
				WHERE llemena__agenda.agenda_id IN (" . implode(',', $content_ids) . ")
				AND llemena__agenda.agenda_id = llemena__agenda_language.agenda_id
				AND language = '".LANGUAGE."'
				ORDER BY ordre ASC, start_date ASC, end_date ASC" ;
    $results = Db::get_rows($query);
    foreach($results as $rs)
    {
        $nam = $rs['agenda'];
        $names[] = htmlspecialchars(str_replace(',', ' ', $nam), ENT_QUOTES);
        $ids[] = $rs['agenda_id'];
    }
}
function get_content_extra_llemena_agenda($ids, $action, $language)
{
	if ($action == 'show_record')
    {
	    $ids = explode(',', $ids);
	    $content = '';
        foreach ($ids as $id)
        {
            $content .= get_url_html(HOST_URL . '/?template=&tool=llemena&tool_section=agenda&action=show_record&loginnewsletter=letndmarc&passwordnewsletter=5356aopfqkiios5cnoqws90se&agenda_id='.$id.'&language=' . $language);
        }
    }
	else if ($action == 'list_records') {
        $content = get_url_html(HOST_URL . '?template=&tool=llemena&tool_section=agenda&action=list_records&loginnewsletter=letndmarc&passwordnewsletter=5356aopfqkiios5cnoqws90se&agenda_ids='.$ids.'&language=' . $language);
		
	}
    return $content;
}

?>