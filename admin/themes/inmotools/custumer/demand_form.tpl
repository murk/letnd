<script language="JavaScript" src="/admin/jscripts/validacio.js?v=<?=$version?>"></script>
<? /* <script language="JavaScript" src="/admin/modules/inmo/jscripts/chained_selects.js?v=<?=$version?>"></script> */ ?>

<script language="JavaScript" src="/admin/modules/inmo/jscripts/functions.js?v=<?=$version?>"></script>

<script language="JavaScript" src="/admin/jscripts/euro_calc.js?v=<?=$version?>"></script>
<script type="text/javascript">
$(function() {
	set_focus(document.theForm);
});
</script>
<form name="form_delete" method="post" action="<?=$link_action_delete?>" target="save_frame">
    <?=$custumer_id?><input name="<?=$id_field?>" type="hidden" value="<?=$id?>">
</form><form name="theForm" method="post" action="<?=$link_action?>" onsubmit="<?=$js_string?>" target="save_frame">
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="form"> 
  
  <tr>
    <td>
        <?if ($form_new):?>
        <div id="form_buttons_holder"><table id ="form_buttons">
          <tr> 
            <td class="buttons_top"><div class="right"><input type="submit" name="Submit" value="<?=$c_send_new?>" class="btn_form1"></div></td>
          </tr>
        </table></div>
        <?endif //form_new?><?if ($form_edit):?>
        <div id="form_buttons_holder"><table id ="form_buttons">
          <tr> 
            <td valign="bottom"><div class="menu"><div><?=$c_edit_data?></div></td>
            <td class="buttons_top"><div class="right"><input type="submit" name="Submit" value="<?=$c_send_edit?>" class="btn_form1"><a class="btn_form_delete" href="javascript:submit_form('<?=$c_confirm_form_deleted?>', true);">
                      <?=$c_delete?>
                      </a></div></td>
          </tr>
        </table></div>
        <?endif //form_edit?>
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr> 
            <td><table width="100%" cellpadding="4" cellspacing="0">
                <tr> 
                  <td class="formCaptionTitle"><strong>
                  <?=$c_demand_id;?><?if ($demand_id):?> <?=$demand_id;?><?endif //demand_id?>
                  </strong>
                    &nbsp;&nbsp;&nbsp;<?=$name?> 
                    <?=$surname1?> 
                    <?=$surname2?> 
					<?if ($phone1 || $phone2 || $phone3):?>tel. <?endif //phone1?>
                    <?=$phone1?><?if ($phone1 && $phone2):?> | <?endif //phone1?>
                    <?=$phone2?><?if ($phone2 && $phone3):?> | <?endif //phone1?>
                    <?=$phone3?>                  </td>
                </tr>
              </table>
              <table width="100%" cellpadding="4" cellspacing="0" class="formstbl">
                <tr>
                  <td class="formsCaption"><strong>
                    <?=$c_tipus?>:</strong></td>
                  <td class="forms"><?=$tipus?>
                    <?=$custumer_id?>
                   <input name="demand_id[<?=$demand_id?>]" type="hidden" value="<?=$demand_id?>" />
                  <input name="property_id_javascript" type="hidden" value="<?=$demand_id?>" /></td>
                </tr>
                <tr> 
                  <td class="formsCaption"><strong><?=$c_tipus2?>:</strong></td>
                  <td class="forms"><?=$tipus2?></td>
                </tr>
                <tr> 
                  <td valign="top" class="formsCaption"><strong>
                  <?=$c_category?>:</strong></td>
                  <td class="forms"> <?=$category_id?> </td>
                </tr>
                <tr> 
                  <td class="formsCaption"><strong><?=$c_floor_space?>:</strong></td>
                  <td class="forms"><?=$floor_space?></td>
                </tr>
                <tr> 
                  <td class="formsCaption"><strong><?=$c_land?>:</strong></td>
                  <td class="forms"><?=$land?>
                    &nbsp;&nbsp;
                  <?=$land_units?></td>
                </tr>
	              <? /*
                <tr> 
                  <td class="formsCaption"><strong><?=$c_country_id?>:</strong></td>
                  <td class="forms"><?=$country_id?></td>
                </tr><tr>
                   <td class="formsCaption"><strong><?=$c_provincia_id?>:</strong></td>
                   <td class="forms"><?=$provincia_id?></td>
                 </tr>
                 <tr>
                   <td class="formsCaption"><strong><?=$c_comarca_id?>:</strong></td>
                   <td class="forms"><?=$comarca_id?>
                   <input name="old_comarca_id[<?=$demand_id?>]" type="hidden" value="0" /></td>
                 </tr> */ ?>
                <tr> 
                  <td class="formsCaption"><strong><?=$c_comarca_id?> / <?=$c_municipi_id?>:</strong></td>
                  <td class="forms"><?=$municipi_id?>
                  <input name="old_municipi_id[<?=$demand_id?>]" type="hidden" value="0" /></td>
                </tr>
                <tr> 
                  <td class="formsCaption"><strong><?=$c_maxprice?>:</strong></td>
                  <td class="forms"><?=$maxprice?>
                    &nbsp;<?=CURRENCY_NAME?></td>
                </tr>
              </table><div class="sep_dark"></div> 
              <table width="100%" cellpadding="4" cellspacing="0">
                <tr>
                  <td class="formCaptionTitle"><strong>
                    <?=$c_details?>
                  </strong></td>
                </tr>
              </table>
              <table width="100%" cellpadding="4" cellspacing="0" class="formstbl">
                <tr>
                  <td width="150" class="formsCaption"><strong>
                    <?=$c_room?>:</strong></td>
                  <td class="forms"><?=$room?></td>
                  <td width="150" class="formsCaption"><strong>
                    <?=$c_bathroom?>:</strong></td>
                  <td class="forms"><?=$bathroom?></td>
                </tr>
	              <tr>
		              <td width="150" class="formsCaption"><strong>
				              <?= $c_living ?>:</strong></td>
		              <td class="forms"><?= $living ?></td>
		              <td width="150" class="formsCaption"><strong>
				              <?= $c_dinning ?>:</strong></td>
		              <td class="forms"><?= $dinning ?></td>
	              </tr>
	              <tr>
		              <td width="150" class="formsCaption"><strong>
				              <?= $c_wc ?>:</strong></td>
		              <td class="forms"><?= $wc ?></td>
		              <td class="formsCaption"><strong>
				              <?= $c_level_count ?>:</strong></td>
		              <td class="forms"><?= $level_count ?></td>
	              </tr>
	              <tr>
		              <td width="150" class="formsCaption"><strong>
				              <?= $c_floor ?>:</strong></td>
		              <td class="forms">
			              <table border="0" cellspacing="0" cellpadding="0">
				              <tr>
					              <td class="searchItemOdd" onmousedown="click_checkbox(this, 'Down', '<?= $id ?>','gres')" onmouseover="click_checkbox(this, 'Over', '<?= $id ?>','gres')" onmouseout="click_checkbox(this, 'Odd', '<?= $id ?>','gres')" id="list_row_<?= $id ?>"><?= $c_gres ?>
						              &nbsp;&nbsp;</td>
					              <td><?= $gres ?>
						              &nbsp;&nbsp;&nbsp;&nbsp;</td>
					              <td class="searchItemOdd" onmousedown="click_checkbox(this, 'Down', '<?= $id ?>','ceramic')" onmouseover="click_checkbox(this, 'Over', '<?= $id ?>','ceramic')" onmouseout="click_checkbox(this, 'Odd', '<?= $id ?>','ceramic')" id="list_row_<?= $id ?>"><?= $c_ceramic ?></td>
					              <td><?= $ceramic ?></td>
				              </tr>
				              <tr>
					              <td class="searchItemOdd" onmousedown="click_checkbox(this, 'Down', '<?= $id ?>','parket')" onmouseover="click_checkbox(this, 'Over', '<?= $id ?>','parket')" onmouseout="click_checkbox(this, 'Odd', '<?= $id ?>','parket')" id="list_row_<?= $id ?>"><?= $c_parket ?>
						              &nbsp;&nbsp;</td>
					              <td><?= $parket ?></td>
					              <td class="searchItemOdd" onmousedown="click_checkbox(this, 'Down', '<?= $id ?>','laminated')" onmouseover="click_checkbox(this, 'Over', '<?= $id ?>','laminated')" onmouseout="click_checkbox(this, 'Odd', '<?= $id ?>','laminated')" id="list_row_<?= $id ?>"><?= $c_laminated ?>
						              &nbsp;&nbsp;</td>
					              <td><?= $laminated ?></td>
				              </tr>
				              <tr>
					              <td class="searchItemOdd" onmousedown="click_checkbox(this, 'Down', '<?= $id ?>','marble')" onmouseover="click_checkbox(this, 'Over', '<?= $id ?>','marble')" onmouseout="click_checkbox(this, 'Odd', '<?= $id ?>','marble')" id="list_row_<?= $id ?>"><?= $c_marble ?>
						              &nbsp;&nbsp;</td>
					              <td><?= $marble ?></td>
					              <td></td>
					              <td></td>
				              </tr>
			              </table>
		              </td>
		              <td width="150" class="formsCaption"><strong>
				              <?= $c_laundry ?>:</strong></td>
		              <td class="forms"><?= $laundry ?></td>
	              </tr>
                <tr>
                  <td width="150" class="formsCaption"><strong>
                    <?=$c_garage?>:</strong></td>
                  <td class="forms"><?=$garage?></td>
                  <td width="150" class="formsCaption"><strong>
                    <?=$c_study?>:</strong></td>
                  <td class="forms"><?=$study?></td>
                </tr>
                <tr>
                  <td width="150" class="formsCaption"><strong>
                    <?=$c_parking?>:</strong></td>
                  <td class="forms"><?=$parking?></td>
                  <td width="150" class="formsCaption"><strong>
                    <?=$c_storage?>:</strong></td>
                  <td class="forms"><?=$storage?></td>
                </tr>
                <tr>
                  <td width="150" class="formsCaption"><strong>
                    <?=$c_courtyard?>:</strong></td>
                  <td class="forms"><?=$courtyard?></td>
                  <td width="150" class="formsCaption"><strong>
                    <?=$c_garden?>:</strong></td>
                  <td class="forms"><?=$garden?></td>
                </tr>
                <tr>
                  <td width="150" class="formsCaption"><strong>
                    <?=$c_furniture?>:</strong></td>
                  <td class="forms"><?=$furniture?></td>
                  <td width="150" class="formsCaption"><strong>
                    <?=$c_terrace?>:</strong></td>
                  <td class="forms"><?=$terrace?></td>
                </tr>
                <tr>
	                <td width="150" class="formsCaption"><strong>
				      <?=$c_centralheating?></strong>:</strong></td>
	                <td class="forms"><?=$centralheating?></td>
                  <td width="150" class="formsCaption"><strong>
                    <?=$c_citygas?>:</strong></td>
                  <td class="forms"><?=$citygas?></td>
                </tr>
                <tr>
	                <td class="formsCaption"><strong>
			                <?=$c_airconditioned?>:</strong></td>
	                <td class="forms"><?=$airconditioned?></td>
                  <td width="150" class="formsCaption"><strong>
                    <?=$c_swimmingpool?>:</strong></td>
                  <td class="forms"><?=$swimmingpool?></td>
                </tr>
	              <tr>
		              <td class="formsCaption"><strong>
				              <?= $c_elevator ?>:</strong></td>
		              <td class="formsHor"><?= $elevator ?></td>
		              <td class="formsCaption"><strong>
				              <?= $c_views ?>:</strong></td>
		              <td class="forms">
			              <table border="0" cellspacing="0" cellpadding="0">
				              <tr>
					              <td class="searchItemOdd" onmousedown="click_checkbox(this, 'Down', '<?= $id ?>','sea_view')" onmouseover="click_checkbox(this, 'Over', '<?= $id ?>','sea_view')" onmouseout="click_checkbox(this, 'Odd', '<?= $id ?>','sea_view')" id="list_row_<?= $id ?>"><?= $c_sea_view ?>
						              &nbsp;&nbsp;</td>
					              <td><?= $sea_view ?>
						              &nbsp;&nbsp;&nbsp;&nbsp;</td>
					              <td class="searchItemOdd" onmousedown="click_checkbox(this, 'Down', '<?= $id ?>','clear_view')" onmouseover="click_checkbox(this, 'Over', '<?= $id ?>','clear_view')" onmouseout="click_checkbox(this, 'Odd', '<?= $id ?>','clear_view')" id="list_row_<?= $id ?>"><?= $c_clear_view ?>
						              &nbsp;&nbsp;</td>
					              <td><?= $clear_view ?></td>
				              </tr>
			              </table>
		              </td>
	              </tr>
	              <tr>
		              <td class="formsCaption"><strong>
				              <?= $c_service_room ?>:</strong></td>
		              <td class="forms"><?= $service_room ?></td>
		              <td class="formsCaption"><strong>
				              <?= $c_balcony ?>:</strong></td>
		              <td class="forms"><?= $balcony ?></td>
	              </tr>
	              <tr>
		              <td class="formsCaption"><strong>
				              <?= $c_facing ?>:</strong></td>
		              <td class="forms"><?= $facing ?></td>
		              <td class="formsCaption"><strong>
				              <?= $c_service_bath ?></strong></td>
		              <td class="forms"><?= $service_bath ?></td>
	              </tr>
              </table><div class="sep_dark"></div> 
			  <table width="100%" cellpadding="4" cellspacing="0">
				<tr>
                  <td class="formCaptionTitle"><strong><?=$c_observations?></strong></td>
                </tr>
			</table>
			<table width="100%" cellpadding="4" cellspacing="0" class="formstbl">
				<tr>
                  <td class="forms"><?=$observations?></td>
                </tr>
			</table>
              <?if ($form_new):?>
			<table width="100%" cellpadding="4" cellspacing="2" class="formsButtonstbl">
                <tr> 
                  <td align="center" class="formsButtons"><div class="centerFloat1">
                        <div class="centerFloat2"><input type="submit" name="Submit" value="<?=$c_send_new?>" class="btn_form1"></div></div></td>
                </tr>
              </table>
              <?endif //form_new?><?if ($form_edit):?>
              <table width="100%" cellpadding="4" cellspacing="2" class="formsButtonstbl">
                <tr> 
                  <td align="center" class="formsButtons"><div class="centerFloat1">
                        <div class="centerFloat2"><input type="submit" name="Submit" value="<?=$c_send_edit?>" class="btn_form1">&nbsp;&nbsp;<a class="btn_form_delete" href="javascript:submit_form('<?=$c_confirm_form_bin?>', true);">
                      <?=$c_delete?>
                      </a></div></div></td>
                </tr>
              </table>
              <?endif //form_edit?></td>
          </tr>
        </table></td>
  </tr>      
</table>
</form>
