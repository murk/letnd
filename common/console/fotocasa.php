<?

namespace Console\Command;

// use Nette\Security\Passwords;
use Console;
use Console\Util;
use Db;
use Query;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Style\SymfonyStyle;
use Unirest\Request;

// use Symfony\Component\Console\Question\Question;

final class Fotocasa extends Command {

	private $output;
	private $input;
	private $foto_casa_url = "https://api.inmofactory.com";
	public $headers;

	protected function configure() {
		$this->setName( 'fotocasa' )
		     ->setDescription( 'Utilitats Fotocasa' )
		     ->addArgument( 'client', InputArgument::REQUIRED, "Client on realitzar les accions" )
		     ->addArgument( 'action', InputArgument::REQUIRED, "Accio a realitzar: 'api', 'api-key', 'propertys', 'publications', 'activate', 'deactivate'" )
		     ->addArgument( 'arg1', InputArgument::OPTIONAL, "" )
		     ->addArgument( 'arg2', InputArgument::OPTIONAL, "" )
		     ->addArgument( 'arg3', InputArgument::OPTIONAL, "" )
		     ->addArgument( 'arg4', InputArgument::OPTIONAL, "" );
	}

	protected function execute( InputInterface $input, OutputInterface $output ) {

		Util::init_unirest( $this );

		$this->input  = $input;
		$this->output = $output;
		$io           = new SymfonyStyle( $input, $output );

		$actions = [ 'api', 'api-key', 'propertys', 'publications', 'activate', 'deactivate' ];

		$client = $input->getArgument( 'client' );
		$action = $input->getArgument( 'action' );
		$arg1   = $input->getArgument( 'arg1' );
		$arg2   = $input->getArgument( 'arg2' );
		$arg3   = $input->getArgument( 'arg3' );
		$arg4   = $input->getArgument( 'arg4' );

		if ( ! in_array( $action, $actions ) ) {


			$helper   = $this->getHelper( 'question' );
			$question = new ChoiceQuestion(
				'Acció a realitzar?',
				$actions );

			$question->setErrorMessage( 'Accio %s no es valid.' );

			$action = $helper->ask( $input, $output, $question );


		}
		$action = str_replace( '-', '_', $action );

		if ( $client == 'test' ) {
			$clients = [
				'client_id'          => 0,
				'inmo_fotocasa_pass' => '4abe21f1fdf44f9a9f4a89db9b5bd6c3fa118e53328d4583a6abea12fc13465b',
				'fiscalname'         => 'Test',
				'domain'             => 'Api-key de probes'
			];
		}
		else {
			$clients = Util::get_server_clients( $client );
		}

		if ( ! isset( $clients['questions'] ) ) {
			$client_rs = $clients;
		}
		elseif ( empty( $clients['questions'] ) ) {
			$client_rs = $clients['clients'];
		}
		else {
			$helper   = $this->getHelper( 'question' );
			$question = new ChoiceQuestion(
				'Quin client?',
				$clients['questions'] );

			$question->setErrorMessage( 'El client no és valid' );

			$client_dir = $helper->ask( $input, $output, $question );
			$client_rs  = $clients['clients'][ $client_dir ];
		}

		//$output->writeln( "Acció: $action" );
		//$output->writeln( "Client: {$client_rs['client_dir']}" );
		if ( $client_rs ) {
			$io->title( "$action: {$client_rs['fiscalname']} - {$client_rs['domain']}" );
		}
		else {
			$io->title( "$action: $client ( no trobat online )" );
		}
		$this->$action( $action, $client, $client_rs, $io, ...[ $arg1, $arg2, $arg3, $arg4 ] );

		return 0;
	}

	private function api( $action, $client, $client_rs, SymfonyStyle $io, ...$args ) {

		if ( ! Util::is_client_on_server( $client_rs, $io ) ) return;
		$api_key = $client_rs['inmo_fotocasa_pass'];
		if ( ! $api_key ) {
			$io->text( "No hi ha api key" );

			return;
		}

		$name  = $args[0];
		$value = isset( $args[1] ) ? $args[1] : false;

		Request::auth( 'api_key', $api_key );

		$api_url  = $this->foto_casa_url . '/' . $name;
		$response = Request::get( $api_url, $this->headers );

		$ret = json_decode( $response->body );

		$io->text( print_r( $ret, true ) );

		return $ret;

	}

	private function publications( $action, $client, $client_rs, SymfonyStyle $io ) {

		$ret = $this->api( $action, $client, $client_rs, $io, 'api/publication' );

		$count = count( $ret );
		$io->text( "Publicacions: $count" );

	}

	private function propertys( $action, $client, $client_rs, SymfonyStyle $io ) {

		$ret = $this->api( $action, $client, $client_rs, $io, 'api/publication/property' );

		$count = count( $ret );
		$io->text( "Immoles publicats: $count" );

	}

	private function api_key( $action, $client, $client_rs, SymfonyStyle $io, ...$args ) {

		if ( ! Util::is_client_on_server( $client_rs, $io ) ) return;

		Util::connect_mother();

		$value     = $args[0];
		$client_id = $client_rs['client_id'];
		$q         = new Query( "letnd.client__client" );

		if ( $value ) {
			$query = $q->prepare_update( [ 'inmo_fotocasa_pass' => $value ], $client_id );

			$helper   = $this->getHelper( 'question' );
			$question = new ConfirmationQuestion( "Executar aquest query? $query", false );

			if ( ! $helper->ask( $this->input, $this->output, $question ) ) {
				$io->text( "Abortat!" );

				return;
			}
			$q->execute_update( $query );
			$io->text( "Api key actualitzada a: \"$value\" a letnd clients" );
		}
		else {
			$api_key = $client_rs['inmo_fotocasa_pass'];
			$io->text( "Api key: $api_key" );
		}

		Db::reconnect();
	}

	private function deactivate( $action, $client, $client_rs, SymfonyStyle $io, ...$args ) {

		if ( ! Util::is_client_on_server( $client_rs, $io ) ) return;

		Util::connect_mother();

		$client_id = $client_rs['client_id'];
		$q         = new Query( "letnd.client__client" );


		$query = $q->prepare_update( [
			'inmo_fotocasa' => 0
		], $client_id );

		$helper   = $this->getHelper( 'question' );
		$question = new ConfirmationQuestion( "Executar aquest query? $query", false );

		if ( ! $helper->ask( $this->input, $this->output, $question ) ) {
			$io->text( "Abortat!" );

			return;
		}
		$q->execute_update( $query );
		$io->text( "Desactivat fotocasa a letnd clients" );


		Db::reconnect();

	}

	private function activate( $action, $client, $client_rs, SymfonyStyle $io, ...$args ) {

		if ( ! Util::is_client_on_server( $client_rs, $io ) ) return;

		$io->text( "Usage: ativate id_agencia id_agente api_key\n\n" );

		Util::connect_mother();

		$id_agencia = isset( $args[0] ) ? $args[0] : false;
		$id_agente  = isset( $args[1] ) ? $args[1] : false;
		$pass  = isset( $args[2] ) ? $args[2] : false;

		$client_id = $client_rs['client_id'];
		$q         = new Query( "letnd.client__client" );

		$values = [
			'inmo_fotocasa'      => 1,
			'inmo_fotocasa_user' => 'api_key'
		];

		if ( $id_agencia ) $values ['inmo_fotocasa_id_agencia'] = $id_agencia;
		if ( $id_agente ) $values ['inmo_fotocasa_id_agente'] = $id_agente;
		if ( $pass ) $values ['inmo_fotocasa_pass'] = $pass;

		$query = $q->prepare_update( $values, $client_id );

		$helper   = $this->getHelper( 'question' );
		$question = new ConfirmationQuestion( "Executar aquest query? $query", false );

		if ( ! $helper->ask( $this->input, $this->output, $question ) ) {
			$io->text( "Abortat!" );

			return;
		}
		$q->execute_update( $query );
		$io->text( "Activat fotocasa a letnd clients" );


		Db::reconnect();
	}
}