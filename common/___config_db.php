<?php
// ----------------------------
// NOMES CLIENTS FORA LETND
// ----------------------------
global $gl_is_local, $configuration;

// BBDD
$configuration['dbtype'] = 'mysql';
$configuration['dbport'] = '3306';
$configuration['system'] = '1';
$configuration['encoded'] = '1';

$configuration['dbhost'] = 'localhost';
$configuration['dbname'] = '{BASEDADES}';
$configuration['dbuname'] = '{USUARI}';
$configuration['dbpass'] = '{PASSWORD}';

$_SESSION['db_host'] = $configuration['dbhost'];
$_SESSION['db_name'] = $configuration['dbname'];
$_SESSION['db_user_name'] = $configuration['dbuname'];
$_SESSION['db_password'] = $configuration['dbpass'];
$_SESSION['client_id'] = '1';
$_SESSION['client_dir'] = 'clients/{CLIENT}';
$gl_is_local = false;

// configuracio mailer
$configuration['m_mailer'] = "smtp";
$configuration['m_host'] = "";
$configuration['m_SMTPAuth'] = false;
$configuration['m_username'] = "";
$configuration['m_password'] = "";
$configuration['from_address'] = "";
$configuration['from_name'] = "";