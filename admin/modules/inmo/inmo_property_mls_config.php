<?php
// aquest config surt a busquedes per mostrar les propietats per una demanda MLS

	$gl_file_protected = true;
	$gl_db_classes_fields = array (
  'property_id_client' =>
  array (
    'type' => 'int',
    'enabled' => '1',
    'form_admin' => '0',
    'form_public' => '0',
    'list_admin' => '0',
    'list_public' => '0',
    'order' => '1',
    'default_value' => '',
    'override_save_value' => '',
    'class' => '',
    'javascript' => '',
    'text_size' => '',
    'text_maxlength' => '',
    'textarea_cols' => '',
    'textarea_rows' => '',
    'select_caption' => '',
    'select_size' => '',
    'select_table' => '',
    'select_fields' => '',
    'select_condition' => '',
  ),
    'client_id' =>
  array (
    'type' => 'text',
    'enabled' => '1',
    'form_admin' => 'text',
    'form_public' => 'no',
    'list_admin' => 'text',
    'list_public' => 'no',
    'order' => '1',
    'default_value' => '',
    'override_save_value' => '',
    'class' => '',
    'javascript' => '',
    'text_size' => '',
    'text_maxlength' => '',
    'textarea_cols' => '',
    'textarea_rows' => '',
    'select_caption' => '',
    'select_size' => '',
    'select_table' => '',
    'select_fields' => '',
    'select_condition' => '',
  ),
  'phone1' =>
	  array (
		  'type' => 'text',
		  'enabled' => '1',
		  'form_admin' => 'no',
		  'form_public' => 'no',
		  'list_admin' => 'out',
		  'list_public' => 'no',
		  'order' => '1',
		  'default_value' => '',
		  'override_save_value' => '',
		  'class' => '',
		  'javascript' => '',
		  'text_size' => '',
		  'text_maxlength' => '',
		  'textarea_cols' => '',
		  'textarea_rows' => '',
		  'select_caption' => '',
		  'select_size' => '',
		  'select_table' => '',
		  'select_fields' => '',
		  'select_condition' => '',
	  ),
    'comercialname' =>
  array (
	  'type' => 'text',
	  'enabled' => '1',
	  'form_admin' => 'no',
	  'form_public' => 'no',
	  'list_admin' => 'out',
	  'list_public' => 'no',
	  'order' => '1',
	  'default_value' => '',
	  'override_save_value' => '',
	  'class' => '',
	  'javascript' => '',
	  'text_size' => '',
	  'text_maxlength' => '',
	  'textarea_cols' => '',
	  'textarea_rows' => '',
	  'select_caption' => '',
	  'select_size' => '',
	  'select_table' => '',
	  'select_fields' => '',
	  'select_condition' => '',
  ),
    'domain' =>
  array (
	  'type' => 'text',
	  'enabled' => '1',
	  'form_admin' => 'no',
	  'form_public' => 'no',
	  'list_admin' => 'out',
	  'list_public' => 'no',
	  'order' => '1',
	  'default_value' => '',
	  'override_save_value' => '',
	  'class' => '',
	  'javascript' => '',
	  'text_size' => '',
	  'text_maxlength' => '',
	  'textarea_cols' => '',
	  'textarea_rows' => '',
	  'select_caption' => '',
	  'select_size' => '',
	  'select_table' => '',
	  'select_fields' => '',
	  'select_condition' => '',
  ),
    'client_dir' =>
  array (
	  'type' => 'text',
	  'enabled' => '1',
	  'form_admin' => 'no',
	  'form_public' => 'no',
	  'list_admin' => 'out',
	  'list_public' => 'no',
	  'order' => '1',
	  'default_value' => '',
	  'override_save_value' => '',
	  'class' => '',
	  'javascript' => '',
	  'text_size' => '',
	  'text_maxlength' => '',
	  'textarea_cols' => '',
	  'textarea_rows' => '',
	  'select_caption' => '',
	  'select_size' => '',
	  'select_table' => '',
	  'select_fields' => '',
	  'select_condition' => '',
  ),
    'comission' =>
  array (
    'type' => 'text',
    'enabled' => '1',
    'form_admin' => 'text',
    'form_public' => 'no',
    'list_admin' => 'text',
    'list_public' => 'no',
    'order' => '38',
    'default_value' => '',
    'override_save_value' => '',
    'class' => '',
    'javascript' => '',
    'text_size' => '30',
    'text_maxlength' => '255',
    'textarea_cols' => '',
    'textarea_rows' => '',
    'select_caption' => '',
    'select_size' => '',
    'select_table' => '',
    'select_fields' => '',
    'select_condition' => '',
  ),
);
	?>