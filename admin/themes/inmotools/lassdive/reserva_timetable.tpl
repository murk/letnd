<? if( $is_header ): ?>
	<div id="timetables-header">
		<div class="text">
			<h5><? if ( $order_id ): ?><?= $order_id ?>: <? endif ?><span><?= $product ?></span></h5>
		</div>
		<? if( $reservaitem_date && !$is_user_popup ): ?>

			<div id="timetables-date"><span id="prev-date" onclick="reserva_time_table.prev_date()"></span><input id="timetable-date" type="text" value="<?= $reservaitem_date ?>"><span id="next-date" onclick="reserva_time_table.next_date()"></span></div>

		<? endif // $reservaitem_date ?>
		<div id="timetables-close" onclick="ReservaList.hide_popup('timetables-holder')"></div>
	</div>

	<script type="text/javascript">
		var reserva_time_table;
		 $(function () {
		    reserva_time_table = new ReservaTimetable(<?= $reservaitem_id ?>, '<?= stripslashes($c_user_id) ?>', '<?= $reservaitem_date_js ?>', <?= $is_new ?>, '<?= $product_id ?>', <?= $can_select_lower_date ?>);

		        reserva_time_table.centres = <?= $centres_json ?>;

			<? if( !$timetables ): // Quan es tria nomes instructor ?>

		        reserva_time_table.start = '<?= $start_time ?>';
				reserva_time_table.end = '<?= $end_time ?>';
				reserva_time_table.centre_id = '<?= $selected_centre_id ?>';
				reserva_time_table.centre = '<?= addslashes($selected_centre) ?>';
				reserva_time_table.show_selected_timetable();

		    <? endif // $timetables ?>
		 });
	</script>

<? else: ?>

	<? if( $timetables ): ?>

		<table id="timetables" class="<?= $has_current_center ?>">
			<thead>
			<tr>
				<td></td>
				<? foreach( $centres as $l ): extract( $l ) ?>

					<td><h6 id="centre_<?= $centre_id ?>"><?= $centre ?></h6><?= $address ?>, <?= $poblacio ?></td>

				<? endforeach //$centres?>
			</tr>
			</thead>
			<? foreach( $timetables as $l ): extract( $l ) ?>
				<tr>
					<td class="hour <?= $hour_class ?>"><?= $hour ?></td>
					<? foreach( $center_timetables as $l ): extract( $l ) ?>
						<td class="items <?= $is_busy ?>" data-is-busy="<?= $is_busy ?>" data-centre-id="<?= $centre_id ?>" data-hour="<?= $hour ?>" id="<?= $timetable_id ?>" onclick="reserva_time_table.select(this)">
							<div><?= $free ?>

								<? foreach ( $reserva_items as $l ): extract( $l ) ?>
									<span style="right: <?= $item_right ?>%;width: <?= $item_width ?>%" class="<?= $item_class ?> <?= $item_stored_class ?>"></span>
								<? endforeach //$reserva_items?>

							</div>
						</td>
					<? endforeach //$centres?>
				</tr>
			<? endforeach //$timtable?>
		</table>

	<? endif // $timetables ?>

	<div id="timetable-results">
		<div>
			<h6><span id="timetable-result-centre"></span><? if( !$is_user_popup ): ?>, <?= $c_selected_timetables ?>:<? endif // $is_user_popup ?></h6>
			<div id="timetable-result-holder">
				<div id="timetable-result"></div>

				<div class="input">
					<table id="selected_user_id_table"></table>
					<input id="selected_user_id_add" name="selected_user_id_add" type="text" size="30" value="">
					<input id="selected_user_id" name="selected_user_id" type="hidden" value="">
				</div>
			</div>
			<div>
				<span id="select-ok" onclick="reserva_time_table.done();"><?= $c_select_ok ?></span>
				<? if( !$is_user_popup ): ?>
					<span id="select-canviar" onclick="reserva_time_table.cancel();"><?= $c_select_canviar ?></span>
				<? endif // $is_user_popup ?>
			</div>
		</div>

<? endif // $is_header ?>