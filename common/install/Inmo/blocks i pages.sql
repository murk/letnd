INSERT INTO `all__page` (`page_id`, `page_text_id`, `parent_id`, `level`, `template`, `link`, `ordre`, `has_content`, `menu`, `blocked`, `modified`, `bin`, `status`, `solution`) VALUES (17, 'temp', 0, 1, '', '/inmo/property/tipus/temp/orderby/ref-desc', 4, 1, 1, 0, '2015-04-17 15:05:26', 0, 'public', 'inmo');
INSERT INTO `all__page_language` (`page_id`, `language`, `page_title`, `page_description`, `page_keywords`, `page`, `title`, `body_subtitle`, `body_description`, `page_file_name`, `page_old_file_name`) VALUES (17, 'cat', '', '', '', 'Lloguer', '', '', '', 'lloguer-vacacional', '');
INSERT INTO `all__page_language` (`page_id`, `language`, `page_title`, `page_description`, `page_keywords`, `page`, `title`, `body_subtitle`, `body_description`, `page_file_name`, `page_old_file_name`) VALUES (17, 'fra', '', '', '', 'Locations', '', '', '', 'location-vacances', '');
INSERT INTO `all__page_language` (`page_id`, `language`, `page_title`, `page_description`, `page_keywords`, `page`, `title`, `body_subtitle`, `body_description`, `page_file_name`, `page_old_file_name`) VALUES (17, 'eng', '', '', '', 'Rentals', '', '', '', 'holiday-rentals', '');
INSERT INTO `all__page_language` (`page_id`, `language`, `page_title`, `page_description`, `page_keywords`, `page`, `title`, `body_subtitle`, `body_description`, `page_file_name`, `page_old_file_name`) VALUES (17, 'spa', '', '', '', 'Alquiler', '', '', '', 'alquiler-vacacional', '');

UPDATE `all__page` SET `ordre`=5 WHERE  `page_id`=7;
UPDATE `all__page` SET `ordre`=6 WHERE  `page_id`=8;
UPDATE `all__page` SET `ordre`=7 WHERE  `page_id`=3;
UPDATE `all__page` SET `ordre`=8 WHERE  `page_id`=4;

########################################

INSERT INTO `all__block` (`block_id`, `script`, `vars`, `template`, `active`, `place`, `ordre`, `solution`) VALUES ('inmo_logged', 'all_page_recursive', 'menu=4&start_level=2&last_level=3&set_top_title=0&parent_id=1', 'login_nav', 2, 'ht', 8, 'inmo');
INSERT INTO `all__block` (`block_id`, `script`, `vars`, `template`, `active`, `place`, `ordre`, `solution`) VALUES ('inmo_login', 'all_page_recursive', 'menu=3&start_level=2&last_level=3&set_top_title=0&parent_id=1', 'login_nav', 2, 'ht', 7, 'inmo');

INSERT INTO `all__block_language` (`block_id`, `language`, `title`, `description`) VALUES ('inmo_logged', 'cat', 'Logged', '');
INSERT INTO `all__block_language` (`block_id`, `language`, `title`, `description`) VALUES ('inmo_logged', 'spa', 'Logged', '');
INSERT INTO `all__block_language` (`block_id`, `language`, `title`, `description`) VALUES ('inmo_logged', 'eng', 'Logged', '');
INSERT INTO `all__block_language` (`block_id`, `language`, `title`, `description`) VALUES ('inmo_logged', 'fra', 'Logged', '');
INSERT INTO `all__block_language` (`block_id`, `language`, `title`, `description`) VALUES ('inmo_login', 'spa', 'Login', '');
INSERT INTO `all__block_language` (`block_id`, `language`, `title`, `description`) VALUES ('inmo_login', 'eng', 'Login', '');
INSERT INTO `all__block_language` (`block_id`, `language`, `title`, `description`) VALUES ('inmo_login', 'fra', 'Login', '');
INSERT INTO `all__block_language` (`block_id`, `language`, `title`, `description`) VALUES ('inmo_login', 'cat', 'Login', '');

#######################################

INSERT INTO `all__page` (`page_id`, `page_text_id`, `parent_id`, `level`, `template`, `link`, `ordre`, `has_content`, `menu`, `blocked`, `modified`, `bin`, `status`, `solution`) VALUES (12, 'register', 1, 2, '', '/custumer/custumer/show-login', 0, 1, 3, 0, '0000-00-00 00:00:00', 0, 'public', 'all');
INSERT INTO `all__page` (`page_id`, `page_text_id`, `parent_id`, `level`, `template`, `link`, `ordre`, `has_content`, `menu`, `blocked`, `modified`, `bin`, `status`, `solution`) VALUES (13, 'login', 1, 2, '', 'javascript:custumer.open_login()', 0, 1, 3, 0, '0000-00-00 00:00:00', 0, 'public', 'all');
INSERT INTO `all__page` (`page_id`, `page_text_id`, `parent_id`, `level`, `template`, `link`, `ordre`, `has_content`, `menu`, `blocked`, `modified`, `bin`, `status`, `solution`) VALUES (15, 'logout', 1, 2, '', 'javascript:custumer.logout()', 0, 1, 4, 0, '2014-12-30 14:58:21', 0, 'public', 'all');
INSERT INTO `all__page` (`page_id`, `page_text_id`, `parent_id`, `level`, `template`, `link`, `ordre`, `has_content`, `menu`, `blocked`, `modified`, `bin`, `status`, `solution`) VALUES (14, 'account', 1, 2, '', '/custumer/custumer', 0, 1, 4, 0, '2014-12-30 14:57:28', 0, 'public', 'all');




INSERT INTO `all__page_language` (`page_id`, `language`, `page_title`, `page_description`, `page_keywords`, `page`, `title`, `body_subtitle`, `body_description`, `page_file_name`, `page_old_file_name`) VALUES (12, 'eng', '', '', '', 'Register', '', '', '', 'register', '');
INSERT INTO `all__page_language` (`page_id`, `language`, `page_title`, `page_description`, `page_keywords`, `page`, `title`, `body_subtitle`, `body_description`, `page_file_name`, `page_old_file_name`) VALUES (12, 'fra', '', '', '', 'Enregistrer', '', '', '', 'enregistrer', '');
INSERT INTO `all__page_language` (`page_id`, `language`, `page_title`, `page_description`, `page_keywords`, `page`, `title`, `body_subtitle`, `body_description`, `page_file_name`, `page_old_file_name`) VALUES (12, 'cat', '', '', '', 'Registrar-se', '', '', '', 'registrarse', '');
INSERT INTO `all__page_language` (`page_id`, `language`, `page_title`, `page_description`, `page_keywords`, `page`, `title`, `body_subtitle`, `body_description`, `page_file_name`, `page_old_file_name`) VALUES (12, 'spa', '', '', '', 'Registrarse', '', '', '', 'registrarse', '');
INSERT INTO `all__page_language` (`page_id`, `language`, `page_title`, `page_description`, `page_keywords`, `page`, `title`, `body_subtitle`, `body_description`, `page_file_name`, `page_old_file_name`) VALUES (13, 'cat', '', '', '', 'Accedir', '', '', '', '', '');
INSERT INTO `all__page_language` (`page_id`, `language`, `page_title`, `page_description`, `page_keywords`, `page`, `title`, `body_subtitle`, `body_description`, `page_file_name`, `page_old_file_name`) VALUES (13, 'spa', '', '', '', 'Acceder', '', '', '', '', '');
INSERT INTO `all__page_language` (`page_id`, `language`, `page_title`, `page_description`, `page_keywords`, `page`, `title`, `body_subtitle`, `body_description`, `page_file_name`, `page_old_file_name`) VALUES (13, 'eng', '', '', '', 'Login', '', '', '', '', '');
INSERT INTO `all__page_language` (`page_id`, `language`, `page_title`, `page_description`, `page_keywords`, `page`, `title`, `body_subtitle`, `body_description`, `page_file_name`, `page_old_file_name`) VALUES (13, 'fra', '', '', '', 'Login', '', '', '', '', '');
INSERT INTO `all__page_language` (`page_id`, `language`, `page_title`, `page_description`, `page_keywords`, `page`, `title`, `body_subtitle`, `body_description`, `page_file_name`, `page_old_file_name`) VALUES (14, 'cat', '', '', '', 'El meu compte', '', '', '', 'compte', '');
INSERT INTO `all__page_language` (`page_id`, `language`, `page_title`, `page_description`, `page_keywords`, `page`, `title`, `body_subtitle`, `body_description`, `page_file_name`, `page_old_file_name`) VALUES (14, 'spa', '', '', '', 'Mi cuenta', '', '', '', 'cuenta', '');
INSERT INTO `all__page_language` (`page_id`, `language`, `page_title`, `page_description`, `page_keywords`, `page`, `title`, `body_subtitle`, `body_description`, `page_file_name`, `page_old_file_name`) VALUES (14, 'eng', '', '', '', 'My account', '', '', '', 'account', '');
INSERT INTO `all__page_language` (`page_id`, `language`, `page_title`, `page_description`, `page_keywords`, `page`, `title`, `body_subtitle`, `body_description`, `page_file_name`, `page_old_file_name`) VALUES (14, 'fra', '', '', '', 'Mon compte', '', '', '', 'compte', '');
INSERT INTO `all__page_language` (`page_id`, `language`, `page_title`, `page_description`, `page_keywords`, `page`, `title`, `body_subtitle`, `body_description`, `page_file_name`, `page_old_file_name`) VALUES (15, 'cat', '', '', '', 'Sortir', '', '', '', '', '');
INSERT INTO `all__page_language` (`page_id`, `language`, `page_title`, `page_description`, `page_keywords`, `page`, `title`, `body_subtitle`, `body_description`, `page_file_name`, `page_old_file_name`) VALUES (15, 'spa', '', '', '', 'Salir', '', '', '', '', '');
INSERT INTO `all__page_language` (`page_id`, `language`, `page_title`, `page_description`, `page_keywords`, `page`, `title`, `body_subtitle`, `body_description`, `page_file_name`, `page_old_file_name`) VALUES (15, 'eng', '', '', '', 'Logout', '', '', '', '', '');
INSERT INTO `all__page_language` (`page_id`, `language`, `page_title`, `page_description`, `page_keywords`, `page`, `title`, `body_subtitle`, `body_description`, `page_file_name`, `page_old_file_name`) VALUES (15, 'fra', '', '', '', 'Sortir', '', '', '', '', '');