#!/bin/sh

DATE=`date +%Y-%m-%d_%H.%M.%S`

mkdir -p /letnd/vagrant/backup/$DATE

cd "/letnd/vagrant/backup/"

for i in `(mysql -u root -ppassbarrat -e "show databases;" | grep -Ev "^(Database|mysql|performance_schema|information_schema)$")`
do
   sudo mysqldump -c -u root -ppassbarrat ${i} > $DATE/${i}.sql
done

# Borra els de més de 24 hores
find /letnd/vagrant/backup/* -mmin +60 -delete