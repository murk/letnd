<?php
global $gl_caption_tv, $gl_messages_tv;
// menus eina
if (!defined('INMO_MENU_PROPERTY')){
	define('INMO_MENU_PROPERTY', 'Biens');
	define('INMO_MENU_PROPERTY_NEW', 'Saisir nouveau bien');
	define('INMO_MENU_PROPERTY_LIST', 'Établir la liste des biens');
	define('INMO_MENU_SEARCH_FORM', 'Rechercher et modifier les biens');
	define('INMO_MENU_PROPERTY_BIN', 'Corbeille');
	define('INMO_MENU_CATEGORY', 'Type de bien');
	define('INMO_MENU_CATEGORY_NEW', 'Saisir nouveau type');
	define('INMO_MENU_CATEGORY_LIST', 'Établir la liste des types');
	define('INMO_MENU_TRANSLATION', 'Traductions');
	define('INMO_MENU_PROPERTY_TRANSLATION_LIST', 'Établir la liste des biens à traduire');
	define('INMO_MENU_CATEGORY_TRANSLATION_LIST', 'Établir la liste des types à traduire');
	define('INMO_MENU_CONFIG', 'Configuration Biens');
	define('INMO_MENU_CONFIGADMIN_LIST', 'Configuration section privée');
	define('INMO_MENU_CONFIGPUBLIC_LIST', 'Configuration section publique');
	define('INMO_MENU_CONFIGADMIN', 'Configuration section privée');
	define('INMO_MENU_CONFIGPUBLIC', 'Configuration section publique');
	define('INMO_MENU_ZONE', 'Zones');
	define('INMO_MENU_ZONE_NEW', 'Saisir nouvelle zone');
	define('INMO_MENU_ZONE_LIST', 'Établir la liste des zones');
	define('INMO_MENU_ZONE_BIN', 'Corbeille');
	define('INMO_MENU_TV', 'Vitrines virtuelles');
	define('INMO_MENU_TV_LIST', 'Créer une vitrine');
	define('INMO_MENU_TV_LIST_SELECTED', 'Immobiliers dans le placard');
	define('INMO_MENU_TV_LIST_DOWNLOAD', 'Download le placard');
	define('INMO_MENU_PROPERTY_MAP_LIST', 'Chercher par carte');
	
	define ('TITLE_COMARCA_ADMIN','Par zone : ');
	define('PROPERTY_AUTO_REF', 'Automatique');

	define('BOOKING_MENU_PROMCODE','Codi promocional');
	define('BOOKING_MENU_PROMCODE_NEW','Crear codi');
	define('BOOKING_MENU_PROMCODE_LIST','Llistar codi');
	define('BOOKING_MENU_PROMCODE_LIST_BIN','Paperera');

	define('BOOKING_MENU_POLICE','Registre Mossos');
	define('BOOKING_MENU_POLICE_LIST','Llistar reserves');

	define('INMO_MENU_PORTAL', 'Portails');
	define('INMO_MENU_PORTAL_LIST', 'Sélectionner immeubles');
	define('INMO_MENU_PORTAL_LIST_SELECTED', 'Immeubles sélectionnés');
}






















// sobreescrits missatges demand custumer
if (isset($gl_messages)) {
	$gl_messages_demand = $gl_messages;
	$gl_messages_custumer = $gl_messages;
}
// sobreescrits end

// sobreescrits missatges property
$gl_messages_property['added']='Le bien a été correctement ajouté. Vous pouvez maintenant insérer les photos correspondant au nouveau bien';
$gl_messages_property['saved']='Le bien a été correctement modifié';
$gl_messages_property['no_records']='Il n\'y a aucun bien dans cette liste';
$gl_messages_property['no_records_bin']='Il n\'y a aucun bien dans la corbeille';
$gl_messages_property['list_saved']='Les modifications ont été correctement apportées aux biens';
$gl_messages_property['list_not_saved']='Aucun bien n\'a été modifié et aucune modification n\'a été détectée';
$gl_messages_property['list_deleted']='Les biens ont été effacés';
$gl_messages_property['list_bined']='Les immobiliers ont été envoyés à la corbeille';
$gl_messages_property['list_restored']='Les biens ont été récupérés';
// sobreescrits end

$gl_messages_category['related_not_activated'] = 'Impossible de désactiver ce type de bien car des biens lui sont associés';
$gl_messages_category['relateds_not_activated'] = 'Impossible de désactiver les types de bien indiqués car des biens leur sont associés';

// captions seccio
$gl_caption_demand['c_form_title'] = 'Données de la pétition du client : ';

$gl_caption['c_property_button'] = 'Afficher les biens';
$gl_caption['c_view_property'] = 'Afficher le bien';

$gl_caption_property['c_move_to_category_id'] = '[type de bien]';
$gl_caption_property['c_move_to_category_id2'] = 'Passer les biens sélectionnés à :';
$gl_caption_property['c_move_to_zone_id'] = '[zone]';
$gl_caption_property['c_move_to_zone_id2'] = 'Passer les biens sélectionnés à :';
$gl_caption_property['c_move_to_zone_id3'] = '[aucune zone]';

$gl_caption_category['c_ordre'] = 'Ordre';
$gl_caption_category['c_ref_code'] = 'Code de référence des biens';
$gl_caption_property['c_room'] = 'Chambres';
$gl_caption_property['c_bathroom'] = 'Salles de bains';
$gl_caption_property['c_wc'] = 'Toilettes';
$gl_caption_property['c_laundry'] = 'Buanderie';
$gl_caption_property['c_parking'] = 'Parking';
$gl_caption_property['c_garage'] = 'Garage';
$gl_caption_property['c_garage_area'] = 'surface';
$gl_caption_property['c_storage'] = 'Débarras';
$gl_caption_property['c_storage_area'] = 'surface';
$gl_caption_property['c_terrace'] = 'Terrasse';
$gl_caption_property['c_terrace_area'] = 'surface';
$gl_caption_property['c_garden'] = 'Jardin';
$gl_caption_property['c_garden_area'] = 'surface';
$gl_caption_property['c_swimmingpool'] = 'Piscine';
$gl_caption_property['c_swimmingpool_area'] = 'surface';
$gl_caption_property['c_citygas'] = 'Gaz de ville';
$gl_caption_property['c_centralheating'] = 'Chauffage';
$gl_caption_property['c_airconditioned'] = 'Climatisation';
$gl_caption_property['c_ref'] = 'Réf.';
$gl_caption_property['c_ref_collaborator'] = 'Ref. collaborateur';
$gl_caption_property['c_ref_cadastre']='Ref. Cadastral';
$gl_caption_property['c_gaining']='Prospection';
$gl_caption_property['c_place_id']='Bureau';
$gl_caption_property['c_cedula']='Certificat d\'habitabilité';
$gl_caption_property['c_registerpropietat']='Registre foncier';
$gl_caption_property['c_numregister']='Nombre';
$gl_caption_property['c_locationregister']='de';
$gl_caption_property['c_tomregister']='tome';
$gl_caption_property['c_bookregister']='livre';
$gl_caption_property['c_pageregister']='feuille';
$gl_caption_property['c_numberpropertyregister']='Immeuble num.';
$gl_caption_property['c_inscriptionregister']='inscription';
$gl_caption_property['c_property_private'] = 'Nom privé';
$gl_caption['c_category_id'] = 'Type';
$gl_caption_property['c_floor_space'] = 'surface ';
$gl_caption_property['c_land'] = 'Terrain';
$gl_caption_property['c_land_units'] = 'Unité';
$gl_caption_property['c_m2'] = 'm2';
$gl_caption_property['c_ha'] = 'hectares';
$gl_caption_property['c_country_id'] = 'Pays';
$gl_caption_property['c_price_private'] = 'Prix du propriétaire';
$gl_caption_property['c_price'] = 'Prix';
$gl_caption_property['c_price_m2'] = 'Prix/m2';
$gl_caption_property['c_zoom'] = 'Zoom';
$gl_caption_property['c_price_consult'] = 'prix à consulter';
$gl_caption_property['c_price_consult_1'] = 'Prix à consulter';
$gl_caption_property['c_owner_id'] = '';
$gl_caption_property['c_status'] = 'État';
$gl_caption_property['c_prepare'] = 'En préparation';
$gl_caption_property['c_review'] = 'Réviser';
$gl_caption_property['c_onsale'] = 'Public';
$gl_caption_property['c_sold'] = 'Vendu';
$gl_caption_property['c_archived'] = 'Classé';
$gl_caption_property['c_reserved'] = 'Réservé';
$gl_caption_property['c_zone'] = 'Zone'; // pour l'avant
$gl_caption_property['c_zone_id'] = 'Zone';
$gl_caption_property['c_zone_id_caption'] = '[aucune zone]';
$gl_caption_property['c_provincia_id'] = 'Province';
$gl_caption_property['c_comarca_id'] = 'Comarca';
$gl_caption_property['c_municipi_id'] = $gl_caption_property['c_municipi'] = 'Commune';
$gl_caption_property['c_property'] = 'Nom du bien';
$gl_caption_property['c_property_title'] = 'Description courte';
$gl_caption_property['c_entered'] = 'Créé';
$gl_caption_property['c_ref_number'] = '';
$gl_caption_property['c_home'] = 'Signalé';
$gl_caption_property['c_offer'] = 'Offre';
$gl_caption_property['c_data'] = 'Données du bien';
$gl_caption_property['c_descriptions'] = 'Descriptions';
$gl_caption_property['c_details'] = 'Détails';
$gl_caption_property['c_description'] = 'Description';
$gl_caption_property['c_0'] = '';
$gl_caption_property['c_1'] = '1';
$gl_caption_property['c_2'] = '2';
$gl_caption_property['c_3'] = '3';
$gl_caption_property['c_4'] = '4';
$gl_caption_property['c_5'] = '5';
$gl_caption_property['c_6'] = '6';
$gl_caption_property['c_7'] = '7';
$gl_caption_property['c_8'] = '8';
$gl_caption_property['c_9'] = '9';
$gl_caption_property['c_10'] = '10';
$gl_caption_property['c_11'] = '11';
$gl_caption_property['c_12'] = '12';
$gl_caption_property['c_13'] = '13';
$gl_caption_property['c_14'] = '14';
$gl_caption_property['c_15'] = '15';
$gl_caption_property['c_16'] = '16';
$gl_caption_property['c_17'] = '17';
$gl_caption_property['c_18'] = '18';
$gl_caption_property['c_19'] = '19';
$gl_caption_property['c_20'] = '20';
$gl_caption_property['c_tipus'] = 'Transaction';
$gl_caption_property['c_tipus_sell'] = 'Vente';
$gl_caption_property['c_tipus_rent'] = 'Location';
$gl_caption_property['c_filter_status'] = 'Tous les états';
$gl_caption_property['c_filter_category_id'] = 'Tous les types';
$gl_caption_property['c_filter_tipus'] = 'Toutes les transactions';
$gl_caption_property['c_address'] = 'Adresse';
$gl_caption_property['c_adress'] = 'Rue/Avenue';
$gl_caption_property['c_numstreet'] = 'Numéro';
$gl_caption_property['c_block'] = 'Escalier';
$gl_caption_property['c_flat'] = 'Étage';
$gl_caption_property['c_door'] = 'Porte';
$gl_caption_property['c_observations'] = 'Observations';
$gl_caption_property['c_data_private'] = 'Données privées';
$gl_caption_property['c_data_public'] = 'Données publiques';
$gl_caption_property['c_tipus2'] = 'Type de construction';
$gl_caption_property['c_elevator'] = 'Ascenseur';
$gl_caption_property['c_balcony'] = 'Balcon';
$gl_caption_property['c_new'] = 'Nouveau';
$gl_caption_property['c_second'] = 'Occasion';
$gl_caption_property['c_furniture'] = 'Meublé';
$gl_caption_property['c_file'] = 'Documents';
$gl_caption_property['c_video'] = $gl_caption_property['c_videoframe'] = 'Vidéo';
$gl_caption_property['c_edit_custumer'] = 'Propriétaires';
$gl_caption_property['c_edit_custumer_button'] = 'Modifier les propriétaires';

$gl_caption_property['c_mls']='MLS';
$gl_caption_property['c_share_mls']='Partager via MLS';
$gl_caption_property['c_comission'] = 'Commission MLS';

$gl_caption_property['c_filter_activated'] = 'Tous les clients';
$gl_caption_property['c_activated_0'] = 'Desactivé';
$gl_caption_property['c_activated_1'] = 'Activé';

$gl_caption_property['c_efficiency_letter'] = 'Lettre';
$gl_caption_property['c_efficiency_number'] = 'Énergie';
$gl_caption_property['c_efficiency_number2'] = 'Émission';
$gl_caption_property['c_sea_distance'] = 'Distance à la mer';

$gl_caption_property['c_expenses'] = 'Dépenses';
$gl_caption_property['c_community_expenses'] = 'Dépenses communautaires';
$gl_caption_property['c_ibi'] = 'IBI';
$gl_caption_property['c_municipal_tax'] = 'Taxes municipals';


$gl_caption_custumer['c_form_custumer_title'] = 'Propriétaires du bien :';
$gl_caption_custumer['c_new_custumer'] = 'Saisir nouveau propriétaire';
$gl_caption_custumer['c_add_custumer'] = 'Attribuer un propriétaire';
$gl_caption_custumer['no_assigned'] = 'Aucun propriétaire n\'est attribué à ce bien';

$gl_caption_zone['c_zone'] ='Nom du quartier et/ou de la zone';
$gl_caption_zone['c_municipi_id'] ='commune';

// sobreescrits images
$gl_caption_image['c_edit'] = 'Photos du bien';
// sobreescrits end

//zones
$gl_caption_zone['c_municipi_id'] ='Commune';
$gl_caption_zone['c_s_municipi_id'] ='Rechercher une commune';
$gl_caption_zone['c_zone'] ='Nom du quartier et/ou de la zone';

// nous globals
$gl_caption['c_by_ref'] = '(ou références séparées par une espace, exemple : "456 245 45")';
$gl_caption['c_by_words'] = 'Par mots';

$gl_caption['c_list_title'] = 'Afficher les listes';
$gl_caption['c_search_title'] = 'Rechercher un bien';
$gl_caption['c_filter_title'] = 'Filtrer les biens';
$gl_caption['c_filter_title2'] = 'Gestion immobilière';
$gl_caption['c_all_ordre'] = 'classé(e) :';
$gl_caption['c_category_title'] = 'Par type';
$gl_caption['c_comarca_title'] = 'Par Comarca';
$gl_caption['c_municipi_title'] = 'Par Commune';
$gl_caption['c_zone_title'] = 'Par zone';
$gl_caption['c_status_title'] = 'Selon l\'état';
$gl_caption['c_description_title'] = 'Selon les descriptions à faire';
$gl_caption['c_price_title'] = 'Par prix';
$gl_caption['c_images_title'] = 'Par photo';
$gl_caption['c_has_no_images'] = 'Sans photos';
$gl_caption['c_has_images'] = 'Avec photos';
$gl_caption['c_images_indiferent'] = 'Les deux';
$gl_caption['c_ref'] = 'Référence';
$gl_caption['c_property_private'] = 'Nom privé';
$gl_caption['c_entered'] = 'Date';
$gl_caption['c_active'] = 'Actif';
$gl_caption['c_category_original'] = 'Type original';
$gl_caption['c_category'] = 'Type';
$gl_caption['c_status'] = 'État';
$gl_caption['c_show_results'] = 'Afficher les résultats';
$gl_caption['c_floor_space_title'] = 'Surface';
$gl_caption['c_land_title'] = 'Terrain';
$gl_caption['c_ordered_title'] = 'Classés par';
$gl_caption['c_user_title'] = 'Agents';
$gl_caption['c_custom_title'] = 'Caractéristiques configurables';
$gl_caption['c_m2'] = 'm2';
$gl_caption['c_ha'] = 'ha';
$gl_caption['c_showwindow'] = 'imprimer la vitrine';
$gl_caption['c_album'] = 'Imprimer l\'album';


// NO TRADUIT

$gl_caption['c_images'] = 'Images';
$gl_caption['c_no_available'] = 'Cette propriété n\'est pas disponible!';
$gl_caption['c_private'] = 'Cacher Privé';
$gl_caption_tv['c_send_edit'] = 'Ajouter à le placard';
$gl_caption_tv['c_download'] = 'Fichiers';
$gl_caption_tv['c_main_files'] = 'Fichiers principaux';
$gl_caption_tv['c_property_file'] = 'Propriété';

$gl_caption['c_type_title'] = 'Par type';


$gl_messages_tv['list_saved']='Le placard a été modifié correctement';


$gl_caption_property['c_living'] = 'Séjours';
$gl_caption_property['c_dinning'] = 'Salles à manger';
$gl_caption_property['c_service_room'] = 'Chambre dependance';
$gl_caption_property['c_service_bath'] = 'Bain dependance';
$gl_caption_property['c_study'] = 'Atelier';
$gl_caption_property['c_courtyard'] = 'Cour';
$gl_caption_property['c_courtyard_area'] = 'Surface';
$gl_caption_property['c_floor'] = 'Terre';
$gl_caption_property['c_parket'] = 'Parquet';
$gl_caption_property['c_ceramic'] = 'Mosaïque';
$gl_caption_property['c_gres'] = 'Grès';
$gl_caption_property['c_laminated'] = 'Laminé';
$gl_caption_property['c_estrato'] = 'Estrato';
$gl_caption_property['c_estrato_e1'] = '1';
$gl_caption_property['c_estrato_e2'] = '2';
$gl_caption_property['c_estrato_e3'] = '3';
$gl_caption_property['c_estrato_e4'] = '4';
$gl_caption_property['c_estrato_e5'] = '5';
$gl_caption_property['c_estrato_e6'] = '6';
$gl_caption_property['c_edit_map'] = 'Carte';
$gl_caption_property['c_edit_booking'] = 'Reservations';
$gl_caption_property['c_edit_booking_extra'] = 'Prix - Extras';
$gl_caption_property['c_edit_booking_season'] = 'Saison';
$gl_caption_property['c_form_map_title'] = 'Emplacement de le bien';
$gl_caption_property['c_form_booking_title'] = 'Reservations de le bien';
$gl_caption_property['c_form_booking_extra_title'] = 'Extres de l\'immoble';
$gl_caption_property['c_form_booking_season_title'] = 'Temporades de l\'immoble';
$gl_caption_property['c_save_map'] = 'Enregistrer emplacement';
$gl_caption_property['c_latitude'] = 'Latitude';
$gl_caption_property['c_longitude'] = 'Longitude';
$gl_caption_property ['c_center_longitude'] = 'Map center latitude';
$gl_caption_property ['c_center_latitude'] = 'Map center longitude';
$gl_caption_property ['c_center_coords'] = 'Centre';
$gl_caption_property['c_click_map'] = 'Cliquez sur un point sur ​​la carte pour définir l\'emplacement';
$gl_caption_property['c_show_map'] = 'Mostrar mapa part pública';
$gl_caption_property['c_show_map_point'] = 'Mostrar punt';
$gl_messages['point_saved'] = 'Nouvel emplacement mémorisé correctement';

$gl_caption_property['c_tipus_temp'] = 'Location touristique';
$gl_caption_property['c_tipus_moblat'] = 'Location avec meubles';
$gl_caption_property['c_tipus_selloption'] = 'Location avec possibilité d\'achat';

$gl_caption_property['c_efficiency'] = $gl_caption_property['c_efficiency2'] = 'Certification énergétique';
$gl_caption_property['c_efficiency_'] = '';
$gl_caption_property['c_efficiency_progress'] = 'En cours';
$gl_caption_property['c_efficiency_exempt'] = 'Exempt';
$gl_caption_property['c_efficiency_a'] = 'A';
$gl_caption_property['c_efficiency_b'] = 'B';
$gl_caption_property['c_efficiency_c'] = 'C';
$gl_caption_property['c_efficiency_d'] = 'D';
$gl_caption_property['c_efficiency_e'] = 'E';
$gl_caption_property['c_efficiency_f'] = 'F';
$gl_caption_property['c_efficiency_g'] = 'G';

$gl_caption_property['c_efficiency2_a'] = 'A';
$gl_caption_property['c_efficiency2_b'] = 'B';
$gl_caption_property['c_efficiency2_c'] = 'C';
$gl_caption_property['c_efficiency2_d'] = 'D';
$gl_caption_property['c_efficiency2_e'] = 'E';
$gl_caption_property['c_efficiency2_f'] = 'F';
$gl_caption_property['c_efficiency2_g'] = 'G';
$gl_caption_property['c_efficiency2_notdefined'] = '';
$gl_caption_property['c_efficiency_date']='Date';


$gl_caption_property['c_filter_room'] = 'Chambres';
$gl_caption_property['c_filter_room_0'] = 'Aucune';

$gl_caption_property['c_a4'] = 'A4';
$gl_caption_property['c_a3'] = 'A3';
$gl_caption_property['c_caption_showwindow_top_fields'] = 'Illimité';
$gl_caption_property['c_caption_showwindow_pages'] = $gl_caption_property['c_caption_album_pages'] = 'Tous';
$gl_caption_property['c_submenu_0'] = 'Langue';
$gl_caption_property['c_submenu_1'] = 'Taille papier';
$gl_caption_property['c_submenu_2'] = 'Nombre de pages';
$gl_caption_property['c_submenu_3'] = 'Limite des champs';
$gl_caption_property['c_submenu_4'] = 'Montrer';
$gl_caption_property['c_submenu_5'] = 'Enregistrer ce paramètre';
$gl_caption_property['c_submenu_6'] = 'Modèle';

$gl_caption_property['c_views'] = 'Views';
$gl_caption_property['c_sea_view'] = 'Vue sur la mer';
$gl_caption_property['c_clear_view'] = ' Vue dégagée';
// FI NO TRADUIT

// nomes admin
$gl_caption_property['c_custom1'] = $gl_caption_property['c_custom2'] = $gl_caption_property['c_custom3'] = $gl_caption_property['c_custom4'] = $gl_caption_property['c_custom5'] = $gl_caption_property['c_custom6'] = $gl_caption_property['c_custom7'] = $gl_caption_property['c_custom8'] = '';

$gl_caption_property['c_filter_offer']='Selon offre';
$gl_caption_property['c_offer_0']='Non offre';
$gl_caption_property['c_offer_1']='Offre';
$gl_caption_property['c_filter_home']='Dépend signalé';
$gl_caption_property['c_home_0']='Non signalé';
$gl_caption_property['c_home_1']='Signalé';


$gl_caption_property['c_user_id'] = 'Agent';
$gl_caption_property['c_filter_user_id'] = 'Tous les agents';

$gl_caption_property['c_person'] = 'Personnes';
$gl_caption_property['c_person_s'] = 'Person';
$gl_caption_property['c_person_p'] = 'Personnes';


$gl_caption['c_edit_history'] = 'Historique';

$gl_caption_history['c_new_history'] = 'Ajouter une entrée';
$gl_caption_history['c_form_history_title'] = 'Record de la propriété:';
$gl_caption_history['no_assigned'] = $gl_messages_history['no_records'] = 'Pas de record sur cette propriété';
$gl_caption_history ['c_custumer_id_format']='%s %s %s';
$gl_caption_history ['c_delete_button']='Éliminer';

$gl_messages_history['added']='L\'entrée a été ajouté avec succès';
$gl_messages_history['saved']='L\'entrada entrée a été modifiée avec succès';


$gl_caption_property['c_exact_result'] = 'Trouvé  <strong>1</strong> application qui correspond à cette propriété' ;
$gl_caption_property['c_exact_results'] = 'Trouvé <strong>%s</strong> applications qui correspondent à cette propriété' ;
$gl_caption_property['c_exact_results_link'] = 'Voir les résultats';
$gl_caption_property['c_aprox_result'] = 'Trouvé <strong>1</strong> application approximative qui correspond à cette propriétée';
$gl_caption_property['c_aprox_results'] = 'Trouvé <strong>%s</strong> applications approximatives qui correspondent à cette propriété';
$gl_caption_property['c_aprox_results_link'] = 'Voir les résultats';
$gl_caption ['c_user_id_format']='%s %s';

$gl_caption['c_property_file_name'] = $GLOBALS['gl_caption']['c_file_name'];
$gl_caption['c_property_old_file_name'] = $GLOBALS['gl_caption']['c_old_file_name'];

$gl_caption['c_send_mail'] = 'Envoyer email';

$gl_caption_property['c_expired'] = 'Date d\'expiration';
$gl_caption_property['c_construction_date'] = 'Année de construction';
$gl_caption_property['c_facing'] = 'Orientation';
$gl_caption_property['c_facing_'] = '';
$gl_caption_property['c_facing_n'] = 'Nord';
$gl_caption_property['c_facing_ne'] = 'Nord-Est';
$gl_caption_property['c_facing_e'] = 'Est';
$gl_caption_property['c_facing_se'] = 'Sud-Est';
$gl_caption_property['c_facing_s'] = 'Sud';
$gl_caption_property['c_facing_so'] = 'Sud-Ouest';
$gl_caption_property['c_facing_o'] = 'Ouest';
$gl_caption_property['c_facing_no'] = 'Nord-Ouest';
$gl_caption_property['c_exclusive'] = 'Exclusivité';
$gl_caption_property['c_zip'] = 'Code postal';
$gl_caption_property['c_descriptionregister'] = 'Descripció registre';
$gl_caption['c_list_mls'] = 'List mls properties';
$gl_caption['c_phone1'] = 'Téléphone';
$gl_caption['c_comercialname'] = 'Entreprise';
$gl_caption['c_client_id']='';
$gl_caption['c_client_dir']='';
$gl_caption['c_domain']='';

$gl_caption['c_star']='Étoiles';
$gl_caption['c_star_0']='Aucune';
$gl_caption['c_star_1']='1';
$gl_caption['c_star_2']='2';
$gl_caption['c_star_3']='3';
$gl_caption['c_star_4']='4';
$gl_caption['c_star_5']='5';

// Booking
$gl_caption_property['c_book_search_title'] = 'Chercher les disponibilités';
$gl_caption['c_dates_from'] = 'Du';
$gl_caption['c_dates_to'] = 'au';
$gl_caption_property['c_date_in'] = 'Date d\'entrée';
$gl_caption_property['c_date_out'] = 'Data de sortida';
$gl_caption_property['c_adult'] = 'Adults';
$gl_caption_property['c_child'] = 'Nens';
$gl_caption_property['c_baby'] = 'Bebès';
$gl_caption_property['c_book_property_button'] = 'Reservar';
$gl_caption_property['c_time_in'] = 'Hora d\'entrada';
$gl_caption_property['c_time_out'] = 'Date de sortie';

$gl_caption_property['c_marble'] = 'Marbre';
$gl_caption_property['c_hut'] = 'HUT';
$gl_caption_property['c_level_count'] = 'Nombre d\'étages';

// Traduccions
$gl_caption_adjective['c_gender'] = 'Génere';
$gl_caption_adjective['c_adjective_before'] = 'Antes';
$gl_caption_adjective['c_adjective_after'] = 'Después';

// Portal
$gl_caption['c_portal_config_title'] = "Options de publication";
$gl_caption['c_apicat_export_all'] = "Exporter toutes les propriétés";
$gl_caption['c_apicat_export_all_text'] = "Si vous cochez la boîte, toutes les propriétés publiques seront exportés sans sélectionner.";
$gl_caption['c_apicat_export_all_text_2'] = "Seules les propriétés qui ont prix et code postal ou emplacement exact seront exportés, Api.cat ne admet pas location touristique. Fotocasa doit avoir surface";
$gl_caption['c_excluded_'] = "Exportable - No exportable";
$gl_caption['c_excluded_apicat'] = "Excluded Apicat";
$gl_caption['c_excluded_fotocasa'] = "Excluded Fotocasa";
$gl_caption['c_excluded_habitaclia'] = "Excluded Habitaclia";

$gl_caption['c_no_subscrit'] = "Information on portals publication";
$gl_caption['c_no_subscrit_apicat'] = "You are not registered to publish on Api.cat";
$gl_caption['c_no_subscrit_fotocasa'] = "You are not registered to publish on Fotocasa";
$gl_caption['c_no_subscrit_habitaclia'] = "You are not registered to publish on Habitaclia";

$gl_caption['c_portal_check_title'] = "The following data is needed to export";
$gl_caption['c_fotocasa_check_price'] = "Price";
$gl_caption['c_fotocasa_check_latitude'] = "Map location";
$gl_caption['c_fotocasa_check_floor_space'] = "Floor space";

$gl_caption['c_habitaclia_check_ref'] = "Reference";
$gl_caption['c_habitaclia_check_adress'] = "Address and map location with \"Show point\" checked";

$gl_caption['c_apicat_check_tipus'] = "Turístic rent not allowed";
$gl_caption['c_apicat_check_consult'] = "Uncheck \"Price on application\"";
$gl_caption['c_apicat_check_zip'] = "Postal code or map location with \"Show point\" checked";