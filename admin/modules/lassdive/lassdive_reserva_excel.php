<?

/**
 * LassdiveReservaExcel
 *
 * @property array search_vars
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2011
 * @version $Id$
 * @access public
 */
class LassdiveReservaExcel {

	var $fields = array(), $column_formats, $table = 'product__order', $caption = [], $id_field, $view_format;

	public function __construct() {

		$this->column_formats = array(
			'order_id'              => 'FORMAT_NUMBER',
			'ref'                   => 'FORMAT_TEXT',
			'product_title'         => 'FORMAT_TEXT',
			'quantity'              => 'FORMAT_NUMBER',
			'product_date'          => 'FORMAT_DATE_DDMMYYYY',
			'product_basetax'       => 'FORMAT_CURRENCY_EUR_SIMPLE',
			'product_total_base'    => 'FORMAT_CURRENCY_EUR_SIMPLE',
			'product_total_tax'     => 'FORMAT_CURRENCY_EUR_SIMPLE',
			'product_total_basetax' => 'FORMAT_CURRENCY_EUR_SIMPLE',
			'all_base'              => 'FORMAT_CURRENCY_EUR_SIMPLE',
			'all_tax'               => 'FORMAT_CURRENCY_EUR_SIMPLE',
			'all_basetax'           => 'FORMAT_CURRENCY_EUR_SIMPLE',
		);

	}

	public function set_vars( &$listing ) {

		$listing->is_excel = true;
		// $listing->parse_template(false);

		$this->caption = array_merge( $listing->caption, $this->caption );

		if ( $this->fields ) {
			$fields = array_merge( $listing->fields, $this->fields );
		}
		else {
			$fields = $listing->fields;
		}
		$fields['order_id']['type']= 'int'; // Ho sobreescriu a hidden a orderitems
		$this->id_field = $listing->id_field;
		$this->fields   = $fields;


		return $this;
	}

	public function make( $loop ) {

		// aquest unset s'hauria de fer a la clase Excel ???
		if ( $this->view_format == 'all' ) unset( $this->fields['orderitems'] );

		Main::load_class( 'excel' );

		$excel = new Excel( $this );
		$excel->set_visible_fields()
		      ->hide_fields( 'orderitems' )
		      ->column_formats( $this->column_formats )
		      ->file_name( 'reserves' );

		if ( $this->view_format == 'all' ) $loop = $excel->prepare_sublist_results( $loop, 'orderitems' );

		$excel->make( $loop );
	}
}