<?php
$module = Module::load('product','product');

$destacat_type = isset($vars['destacat_type'])?$vars['destacat_type']:'destacat'; // destacat, prominent, discount
$add_dots_length = isset($vars['add_dots_length'])?$vars['add_dots_length']:false; 
$block_max_results = isset($vars['block_max_results']) ? $vars['block_max_results'] : $module->config['destacat_max_results'];
$family_id = isset($vars['family_id']) ? $vars['family_id'] : false;
$random = isset($vars['random'])?$vars['random']:0;
$order_by = isset($vars['order_by'])?$vars['order_by']:false;

//if (!defined('PRODUCT_DESTACAT_MAX_RESULTS')) Module::get_config_values('product');


if ($add_dots_length) $module->config['add_dots_length'] = $add_dots_length;

$module->parent = 'Block';
$wholesaler_sql = $module->is_wholesaler()?'_wholesaler':'';

// agafem els que tenen més descompte
if ($destacat_type=='discount'){
	$this->order_by = 'discount DESC, ordre ASC, product_title ASC';
	$module->condition = 
		"AND (discount>0) AND (status = 'onsale')";
}
else {
$module->condition = 
	"AND (".$destacat_type."=1)";
	
}

if ($family_id!==false){

	$query = "SELECT family
						FROM product__family_language
						WHERE family_id = " . $family_id . "
						AND language = '" . LANGUAGE . "'";

	$vars['family'] =  DB::get_first ($query);
	$vars['listing_family_id'] =  $family_id;

	$module->condition .= 'AND family_id = ' . $family_id;
}


$module->limit = '0,' . $block_max_results;
if ($random) $module->order_by = 'RAND()';

if($order_by) $module->order_by = $order_by;

$loop = $module->do_action('get_records');

$vars += $module->caption;



/*
$query = "
SELECT
	product__product.product_id AS product_id,
	product_private,
	product_title,
	product_description,
	ref,
	ref_number,
	family_id,
	subfamily_id,
	pvp,
	pvd,
	price_consult,
	tax_id,
	discount,	
	price_unit,
	brand_id,
	sell,
	guarantee,
	guarantee_period,
	year,
	observations,
	status,
	home,
	free_send,
	bin,
	image_id, name
	FROM product__product, product__product_language, product__product_image
	WHERE product__product_language.product_id = product__product.product_id
	AND product__product.product_id = product__product_image.product_id
	AND main_image = 1
	AND (discount" . $wholesaler_sql . "<>0 OR discount_fixed_price" . $wholesaler_sql . "<>'0.00')
	AND language = '" . LANGUAGE . "'
	AND product__product.bin = 0
	AND ( status = 'onsale' OR status = 'sold' OR status = 'reserved')
	ORDER BY product_title
	LIMIT 0," . PRODUCT_VISITED_MAX_RESULTS;

$results = Db::get_rows($query);
Debug::add('Product_visited', $query);
*/
if ($loop)
{
	
}
else{
	$show_block = false;
}


?>