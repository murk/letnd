#!/usr/bin/ruby

require 'grouper/server/jsrsServer.rb'
# Functions need to be available. It works to put them all
# in one include file.
load 'jsrsfunctions.rb'

# Get a CGI object
cgi = CGI.new
# Put output to client.
cgi.out {JSRSServer.new.result}
