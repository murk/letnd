<table width="90%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td valign="top" class="listImage" width="<?=$im_admin_thumb_w?>">	  
	  <?if ($image_name):?><div><img src="<?=$image_src_admin_thumb?>"></div>
	  <?else: // image_name?><div class="noimage" style="min-height:<?=$im_admin_thumb_h?>px;"></div>
	  <?endif // image_name?>	  
	  </td>
        <td valign="top" class="property-details-holder"><table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td><table border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td class="listTitle"><strong> <?=$ref?><span class="private">
-
<?=$property_private?><span>
                    </strong></td>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <?=$entered?></td>
                  </tr>
                </table></td>
          </tr>
            <tr>
              <td align="left"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                  <tr>
                    <td valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="listDetails2">
                      <tr>
                        <td valign="top"><strong class="listItem">
                          <?=$c_tipus?>:</strong>&nbsp;</td>
                        <td valign="top"><?=$tipus?></td>
                        <td valign="top"><strong class="listItem">
                          <?=$c_municipi_id?>:</strong>&nbsp;</td>
                        <td valign="top"><?=$municipi_id?></td>
                        </tr>
                      <tr>
                        <td valign="top"><strong class="listItem">
                          <?=$c_category?>:</strong>&nbsp;</td>
                        <td valign="top"><?=$category_id?></td>
                        <td valign="top"><strong class="listItem">
                          <?=$c_floor_space?>:</strong>&nbsp;</td>
                        <td valign="top"><?=$floor_space?></td>
                        </tr>
                      <tr>
                        <td valign="top"><strong class="listItem">
                          <?=$c_tipus2?>:</strong>&nbsp;</td>
                        <td valign="top"><?=$tipus2?></td>
                        <td valign="top"><strong class="listItem">
                          <?=$c_land?>:</strong></td>
                        <td valign="top"><?=$land?>
                          &nbsp;
                          <?=$land_units?>
                          &nbsp;</td>
                        </tr>
                      <tr>
                        <td valign="top"><strong class="listItem">
                          <?=$c_price?>:</strong>&nbsp;</td>
                        <td valign="top"><strong class="resaltatSmall">
                          <?=$price?>
                        </strong></td>
                        <td valign="top"><strong class="listItem">
                          <?=$c_status?>:</strong>&nbsp;</td>
                        <td valign="top"><?=$status?></td>
                        </tr>
                    </table></td>
                  </tr>
                </table>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td align="left"><table border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td valign="top"><strong class="listItem">
                          <?=$c_address?>:&nbsp;</strong></td>
                          <td><table border="0" cellspacing="0" cellpadding="0">
                            <tr>
                              <td><?=$c_adress?>
                                &nbsp;</td>
                              <td><?=$c_numstreet;?>
                                &nbsp;</td>
                              <td><?=$c_block;?>
                                &nbsp;</td>
                              <td><?=$c_flat;?>
                                &nbsp;</td>
                              <td><?=$c_door;?></td>
                            </tr>
                            <tr>
                              <td><?=$adress?>
                                &nbsp;&nbsp;</td>
                              <td><?=$numstreet;?>
                                &nbsp;&nbsp;</td>
                              <td><?=$block;?>
                                &nbsp;&nbsp;</td>
                              <td><?=$flat;?>
                                &nbsp;&nbsp;</td>
                              <td><?=$door;?></td>
                            </tr>
                          </table></td>
                      </tr>
                      </table>
                      </td>
                  </tr>
                  <tr>
                    <td style="padding-top:10px;padding-bottom:10px;"><div class="right"><a href="javascript:javascript:show_form_property(<?=$property_id?>, false);" class="btn_form1"><?=$c_edit_data?></a><a href="javascript:javascript:show_form_property_images(<?=$property_id?>, false);" class="btn_form1"><?=$c_edit_images?></a></div></td>
                  </tr>
                </table></td>
          </tr>
        </table></td>
  </tr>
</table>
<br />
<table width="90%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="left" class="formsCaptionHor"><strong>
      <?=$c_owner_title?>:</strong><a href="javascript:select_owners(0,<?=$property_id?>)">
      <?=$c_select_owners_link?>
                          </a> - <a href="javascript:show_form_new_custumer('','contract_new_owner')">
      <?=$c_add_owners_link?>
      </a></td>
  </tr>
</table>
<br />
