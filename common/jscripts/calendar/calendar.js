var fShow="visible";
var fHide="hidden";
var nCurrentYear = 0;
var MonthNames = MONTH_NAMES;
var nCurrentMonth = 0;
var dAvui=new Date();
var avui=dAvui.getDate();
var diaSelecionat=avui;

var colorFonsMarc='#FCF9E7'
var colorSubrallat='#f2be09'
var colorTexte='white'
var imgActes="/images/cal/fons_n.gif"
var imgAvui="/images/cal/fons_a.gif"
var imgAvuiacte="/images/cal/fons_an.gif"

var ultima=''
var marcats=''
var TempMesMenuAny=''
var inputActiu = ''

function setCurrentMonth()
{
	date = new Date();
	currentyear=date.getYear()
	if (currentyear < 1000)
	currentyear+=1900
	setYearMonth(currentyear, date.getMonth()+1); 
}

function setMonth(nMonth)
{
	setYearMonth(nCurrentYear, nMonth);
	AmagaMesMenu()
}


////////////////////////////////////
//                                //
//      FUNCIO PRINCIPAL          //
//                                //
////////////////////////////////////
                           

function setYearMonth(nYear, nMonth)
{
	
  nCurrentYear = nYear;
  nCurrentMonth = nMonth;
  
  // canviar any i mes 
  var cross_obj=document.getElementById('main');
  var cross_obj2=document.getElementById('main2');
  cross_obj.innerHTML  = nCurrentYear;
  cross_obj2.innerHTML = MonthNames[nCurrentMonth-1];
  // fi
	

  var date   = new Date(nCurrentYear, nCurrentMonth-1, 1);
  var nWeek  = 1;
  var nDate;
  var resetSetmana=1;
  var resetDia=-1;


  if (marcats) {desmarcaDia(marcats)}
// amaga els que sobren  
  
    while (resetSetmana < 7){
   
		resetDia=resetDia+1
		if (resetDia == 7) resetDia=0;	
		document.getElementById('set'+ resetSetmana + 'pos' + resetDia).innerHTML='&nbsp;'; 
		if (resetDia == 6) resetSetmana++;
		
	} 
// fi

// fer el cos del calendari dies, dies setmana, marcar festius...
  while (date.getMonth() == nCurrentMonth-1)
  {
	nDate = date.getDate();
	nLastDate = nDate;

	// posicio dia   posicio setmana
	var posDay = date.getDay()-1;
	if (posDay == -1) posDay=6;

	//alert('set'+ nWeek + 'pos' + posDay)
	if ((posDay==5) || (posDay==6)){clase='calR'} 
	else {clase='cal'};
	document.getElementById('set'+ nWeek + 'pos' + posDay).innerHTML='<table width="100%" cellspacing="0" cellpadding="1" class="tauladies"><tr><td onclick="CanviaFonsDia(document.getElementById(\'set'+ nWeek + 'pos' + posDay + '\'),colorSubrallat,colorTexte, ' + nDate + ');diaSelecionat=\'' + nDate + '\'" style="cursor:hand;cursor:pointer" class="'+clase+'">'+nDate+'</td></tr></table>';
	if (nDate==diaSelecionat){
		CanviaFonsDia(document.getElementById('set'+ nWeek + 'pos' + posDay),colorSubrallat,colorTexte, nDate);
	}
	date = new Date(nCurrentYear, date.getMonth(), date.getDate()+1);
	
	if (posDay == 6) nWeek++;
  }
// fi
	
	marcaAvui() // marca o desmarca el dia d'avui
	
}


////////////////////////////////////
//                                //
//     CANVIAR DE MES I ANY       //
//                                //
////////////////////////////////////
                           
function proximMes()
{
  TempMesMenuAny = 'proximMes';
  nCurrentMonth++;
  if (nCurrentMonth > 12)
  {
	nCurrentMonth -= 12;
	proximAny();
  }
  
  setYearMonth(nCurrentYear, nCurrentMonth);
}


function previMes()
{
  TempMesMenuAny = 'previMes';
  nCurrentMonth--;
  if (nCurrentMonth < 1)
  {
	nCurrentMonth += 12;
	previAny();
  }
  setYearMonth(nCurrentYear, nCurrentMonth);

}

function previAny()
{
  TempMesMenuAny = 'PreviAny';
  nCurrentYear--;
  setYearMonth(nCurrentYear, nCurrentMonth);
}

function proximAny()
{
  TempMesMenuAny = 'ProximAny';
  nCurrentYear++;
  setYearMonth(nCurrentYear, nCurrentMonth);
}

////////////////////////////////////////////////////

function AmagaMesMenu()
{
	document.getElementById("MenuMes").style.visibility="hidden";
}

function MesMenu()
{
	TempMesMenuAny = 'MesMenu';
	document.getElementById("MenuMes").style.visibility="visible";
}

function MostraCalendari(evt, input, CurrentDateInput)
{
	inputActiu = input;
	
	// Recuperem dia/mes/any del INPUT i ho marquem al calendari.
	// Abans comprovem que INPUT no sigui blanc.
	if (CurrentDateInput != '') 
	{
		// Marquem el dia
		a = CurrentDateInput.split("/")
		if ((a[2]==Number(a[2]))&&(a[1]==Number(a[1]))&&(a[0]==Number( a[0]))){
		CurrentYearInput = a[2];
		CurrentMonthInput = a[1];
		CurrentDateInput = a[0];
		diaSelecionat = CurrentDateInput
		setYearMonth(CurrentYearInput,CurrentMonthInput);
		}
	}
	else
	{	
		// Desmarquem els dies marcats per "inicialitzar" calendari.
		//CanviaFonsDia(document.all(marcats),colorTexte,colorTexte);
		//nCurrentYear = 0;
		//nCurrentMonth = 0;		
		//diaSelecionat=avui;
		//setYearMonth(nCurrentYear, nCurrentMonth);
	}
	// FI 
	
	// Cridem la funci� Mouse per localitzar la �ltima possissi� del mouse.
	// Per defecte estar� situat sobre l'input.
	Mouse(evt);	
    document.getElementById("Calendari").style.top = ymouse+"px";
    document.getElementById("Calendari").style.left = xmouse+"px";
	document.getElementById("Calendari").style.visibility="visible";
    document.getElementById("CalendariBach").style.top = ymouse;
    document.getElementById("CalendariBach").style.left = xmouse;
    document.getElementById("calendariFrame").height = document.getElementById("Calendari").offsetHeight;
    document.getElementById("calendariFrame").width = document.getElementById("Calendari").offsetWidth;
	document.getElementById("CalendariBach").style.visibility="visible";
	
}

function AmagaCalendari()
{
	document.getElementById("Calendari").style.visibility="hidden"; 
	document.getElementById("CalendariBach").style.visibility="hidden";  
	document.getElementById("MenuMes").style.visibility="hidden";
}

////////////////////////////////////
//                                //
//   INTERACTUEM AMB EL MOUSE     //
//                                //
////////////////////////////////////

// Declarem variables globals per localitzar la posici� del mouse.
ymouse = 0;
xmouse = 0;
// Variables per variar la posici� del calendari, que per defecte est� sobre l'input
xmouse_move = 0;
ymouse_move = 0;

function Mouse(evt)
{  
	var e = (window.event) ? window.event : evt;	
	xmouse = document.body.scrollLeft + e.clientX - 176 + xmouse_move;
	ymouse = document.body.scrollTop + e.clientY -160 + ymouse_move; 
}

////////////////////////////////////
//                                //
//   LINKS A LA PAGINA CENTRAL    //
//                                //
////////////////////////////////////
                           
function CanviaFonsDia(obj,colorFons,colorTexte,DiaCalendari){
	if (ultima){
		
	//
	// canviar el color dels dies 
	//
	CanviaFonsDiaSec(ultima,colorFonsMarc,colorTexte);
	}
	
	DiaCalendari = (DiaCalendari<10)?('0'+DiaCalendari):DiaCalendari;
	dataFinal = DiaCalendari + '/' + nCurrentMonth + '/' + nCurrentYear
	if (inputActiu != '' && TempMesMenuAny == '')  {
		eval("document.theForm['"+inputActiu+"'].value = '"+dataFinal+"'");
		AmagaCalendari();
	}
	TempMesMenuAny = '';
	CanviaFonsDiaSec(obj,colorFons,colorTexte);
	ultima=obj;

}


//////////////////////////////////
//                              //
//  MARCA PESTANYES NAVEGACIO   //
//                              //
//////////////////////////////////

function CanviaFonsDiaSec(obj2,colorFons2,colorTexte2){ 

	if (obj2){
	obj2.style.borderColor=colorFons2;
	obj2.style.borderStyle='SOLID'
	obj2.style.borderWidth='1px'
	//obj.style.color = colorTexte;
	}
}


//////////////////////////////////
//                              //
//  MARCA ELS DIES AMB ACTES   //
//                              //
//////////////////////////////////
function marcaDia (dia){	
	
	var date   = new Date(nCurrentYear, nCurrentMonth-1, 1);
	posicio = date.getDay()
	//a=marcaDia.arguments
	a = dia
	//for (i=0;i<(a.length);i++) {
	for (i=0;i<1;i++) {
		date   = new Date(nCurrentYear, nCurrentMonth-1, a);
		setm=Math.ceil((date.getDate()+posicio)/7)
		pos=date.getDay()-1;
		if (pos == -1) {pos=6;setm=setm-1}
			if (document.getElementById('set'+setm+'pos'+pos).background==imgAvui){temp=imgAvuiacte}
			else{temp=imgActes}
			//document.all('set'+setm+'pos'+pos).background=temp
			CanviaFonsDia(document.getElementById('set'+setm+'pos'+pos),colorSubrallat,colorTexte);
			marcats=marcats+'set'+setm+'pos'+pos+','
	}
	marcats=marcats.substring(0,marcats.length-1)
}


function desmarcaDia (dia){

	var date   = new Date(nCurrentYear, nCurrentMonth-1, 1);
	posicio = date.getDay()
	a=dia.split(',')
	for (i=0;i<(a.length);i++) {
		document.getElementById(a[i]).background='';
	}
	marcats=''

}

function marcaAvui (){

	if ((dAvui.getYear()==nCurrentYear) && (dAvui.getMonth()==nCurrentMonth-1)){
		temp=imgAvui}
	else{temp=''}
		date   = new Date(dAvui.getYear(), dAvui.getMonth(), 1);	
		posicio = date.getDay()
		date   = dAvui	
		setm=Math.ceil((date.getDate()+posicio)/7)
		pos=date.getDay()-1;
		if (pos == -1) {pos=6;setm=setm-1}
		document.getElementById('set'+setm+'pos'+pos).background=temp
		if (temp!=''){
			marcats=marcats+'set'+setm+'pos'+pos+','}
		marcats=marcats.substring(0,marcats.length-1)
}


//////////////////////////////////
//                              //
//  VALIDA DATA                 //
//                              //
//////////////////////////////////

function validaCalendari(obj)
{
	var fecha,DataNova;
	
	fecha = obj.value	
	fechaArray = fecha.split("/")
	
	if (fechaArray.length!=3)
		{
		fechaArray = fecha.split("-");
		}	
	
	if (fechaArray.length==3)
		{
		DataNova = new Date(fechaArray[2],fechaArray[1]-1,fechaArray[0]);
		}
	else
		{
		obj.value = '';
		return false;
		}
	if (isNaN(DataNova))
		{
		obj.value = '';
		return false;
		}
	else
		{
		MesCalendari =  DataNova.getMonth() + 1;
		DiaCalendari =  DataNova.getDate();
		MesCalendari = (MesCalendari<10)?('0'+MesCalendari):MesCalendari;
		DiaCalendari = (DiaCalendari<10)?('0'+DiaCalendari):DiaCalendari;
		obj.value = DiaCalendari + '/' + MesCalendari + '/' + DataNova.getYear() ;
		
		}
}