<?
// blocks

define('INMO_PRICE_FIRST','Fins a %s&nbsp;'.CURRENCY_NAME);
define('INMO_PRICE_MIDDLE','De %s a %s&nbsp;'.CURRENCY_NAME);
define('INMO_PRICE_LAST','Més de %s&nbsp;'.CURRENCY_NAME);
define('INMO_FLOOR_SPACE_FIRST','Fins a %s m<sup>2</sup>');
define('INMO_FLOOR_SPACE_MIDDLE','De %s a %s m<sup>2</sup>');
define('INMO_FLOOR_SPACE_LAST','Més de %s m<sup>2</sup>');
define('INMO_PERSON_FIRST','Fins a %s');
define('INMO_PERSON_MIDDLE','De %s a %s');
define('INMO_PERSON_LAST','Més de %s');
define ('LETND','Letnd');

$gl_caption['c_offers'] = 'Ofertes';


// captions

$gl_caption['c_send_mail'] = 'enviar mail';
$gl_caption['c_more_info'] = 'Més info';
$gl_caption['c_read_more'] = 'Llegir més';
$gl_caption['c_title_search'] = 'Cerca';
$gl_caption['c_yes'] = 'Sí';
$gl_caption['c_no'] = 'No';
$gl_caption['c_send'] = 'Enviar';
$gl_caption['c_clear'] = 'Esborrar';
$gl_caption['c_send_to_friend']='enviar a un amic';
$gl_caption['c_print']='imprimir';
$gl_caption['c_zoom_link']='ampliar imatges';
$gl_caption['c_contact_link']='contactar';

// bloc inmo_categories
$gl_caption['c_sell'] = 'Venda';
$gl_caption['c_rent'] = 'Lloguer';
$gl_caption['c_temp'] = 'Lloguer turístic';
$gl_caption['c_moblat'] = 'Lloguer amb mobles';
$gl_caption['c_selloption'] = 'Lloguer opció compra';

// seo
$gl_caption['c_seo_verd_1']='<a href="http://www.letnd.com" target="_blank" title="Disseny pàgines web a girona">Disseny i programació web:<img src="/common/images/logoletnd.png" width="95" height="16" alt="Disseny web, posicionament web, pagines web a girona" style="vertical-align:text-bottom" /></a>';
$gl_caption['c_seo_verd_2']='<a href="http://www.letnd.com" target="_blank" title="Disseny pàgines web a girona">Programació web:<img src="/common/images/logoletnd.png" width="95" height="16" alt="Disseny web, posicionament web, pagines web a girona" style="vertical-align:text-bottom" /></a>';
$gl_caption['c_seo_verd_3']='<a href="http://www.letnd.com" target="_blank" title="Disseny pàgines web a girona">Creat per:<img src="/common/images/logoletnd.png" width="95" height="16" alt="Letnd: Disseny web, posicionament web, pagines web a girona" style="vertical-align:text-bottom" /></a>';
$gl_caption['c_seo_verd_4']='<a href="http://www.letnd.com" target="_blank" title="Disseny pàgines web a girona">Disseny web:<img src="/common/images/logoletnd.png" width="95" height="16" alt="Letnd: Disseny web, posicionament web, pagines web a girona" style="vertical-align:text-bottom" /></a>';

$gl_caption['c_seo_blanc_1']='<a href="http://www.letnd.com" target="_blank" title="Disseny pàgines web a girona">Disseny i programació web:<img src="/common/images/logoblanc.png" width="95" height="16" alt="Disseny web, posicionament web, pagines web a girona" style="vertical-align:text-bottom" /></a>';
$gl_caption['c_seo_blanc_2']='<a href="http://www.letnd.com" target="_blank" title="Disseny pàgines web a girona">Programació web:<img src="/common/images/logoblanc.png" width="95" height="16" alt="Disseny web, posicionament web, pagines web a girona" style="vertical-align:text-bottom" /></a>';
$gl_caption['c_seo_blanc_3']='<a href="http://www.letnd.com" target="_blank" title="Disseny pàgines web a girona">Creat per:<img src="/common/images/logoblanc.png" width="95" height="16" alt="Letnd: Disseny web, posicionament web, pagines web a girona" style="vertical-align:text-bottom" /></a>';
$gl_caption['c_seo_blanc_4']='<a href="http://www.letnd.com" target="_blank" title="Disseny pàgines web a girona">Diseño web:<img src="/common/images/logoblanc.png" width="95" height="16" alt="Letnd: Disseny web, posicionament web, pagines web a girona" style="vertical-align:text-bottom" /></a>';

$gl_caption['c_seo_custom_2']='<a href="http://www.letnd.com" target="_blank" title="Disseny pàgines web a girona">Programació web:<img src="/'.CLIENT_DIR.'/templates/images/letnd.png" alt="Disseny web, posicionament web, pagines web a girona" /></a>';
$gl_caption['c_seo_custom_4']='<a href="http://www.letnd.com" target="_blank" title="Disseny pàgines web a girona">Disseny web:<img src="/'.CLIENT_DIR.'/templates/images/letnd.png" alt="Letnd: Disseny web, posicionament web, pagines web a girona" /></a>';

$gl_caption['c_tercersetena'] = 'Disseny <a class="tercersetena" target="_blank" href="http://www.tercersetena.com">Tercersetena.com</a>';

// NO TRADUIT
$gl_caption['c_tipus_temp'] = 'Lloguer de temporada';
$gl_caption['c_tipus_moblat'] = 'Lloguer amb mobles';
// FI NO TRADUIT

// TAMBÉ A ADMIN
// Caption default
$gl_caption['c_form_title'] = 'Editar dades';
$gl_caption['c_config_title'] = 'Editar configuració';
$gl_caption['c_edit'] = 'Editar';


$gl_caption['c_edit_button'] = 'Editar dades';
$gl_caption['c_view_button'] = 'Veure dades';
$gl_caption['c_edit_images_button'] = 'Editar imatges';
$gl_caption['c_add_images_button'] = 'Afegir imatges';



$gl_caption['c_edit_data'] = 'Editar dades';
$gl_caption['c_edit_images'] = 'Editar imatges';
$gl_caption['c_add_images'] = 'Afegir imatges';
$gl_caption['c_search_form'] = 'Buscar';
$gl_caption['c_send_new'] = 'Guardar';
$gl_caption['c_send_edit'] = 'Guardar canvis';
$gl_caption['c_delete'] = 'Eliminar';
$gl_caption['c_action'] = 'Acció';
$gl_caption['c_delete_selected'] = 'Esborrar seleccionats';
$gl_caption['c_restore_selected'] = 'Recuperar seleccionats';
$gl_caption['c_confirm_deleted'] = 'Els elements seleccionats s\\\'esborraran definitivament de la base de dades \\n\\nDesitges continuar?';
$gl_caption['c_confirm_images_deleted'] = 'Les imatges seleccionats s\\\'esborraran definitivament\\n\\nDesitges continuar?';
$gl_caption['c_confirm_form_bin'] = 'El registre s\\\'enviarà a la paperera de reciclatge \\n\\nDesitges continuar?';
$gl_caption['c_confirm_form_deleted'] = 'El registre s\\\'esborrarà definitivament de la base de dades \\n\\nDesitges continuar?';
$gl_caption['c_move_to'] = 'Moure a:';
$gl_caption['c_print_all'] = 'Imprimir tot';
$gl_caption['c_print'] = 'Imprimir';
$gl_caption['c_order_by'] = 'Ordenar per:';
$gl_caption['c_select'] = 'Seleccionar';
$gl_caption['c_all'] = 'tot';
$gl_caption['c_nothing'] = 'res';
$gl_caption['c_preview'] = 'Previsualitzar';
$gl_caption['c_yes'] = 'Sí';
$gl_caption['c_no'] = 'No';
$gl_caption['c_options_bar'] = 'Opcions';
$gl_caption['c_file_maximum'] = 'màxim %s arxius alhora';
$gl_caption['c_file_maximum_1'] = 'màxim 1 arxiu alhora';


// Message default
$gl_messages['title_edit']='Editar';
$gl_messages['title_copy']='Copiar dades';
$gl_messages['added']='El registre s\'ha afegit correctament.';
$gl_messages['add_exists']='Ja existeix un registre amb aquest/a "%s"';
$gl_messages['saved']='El registre ha estat modificat correctament';
$gl_messages['not_saved']='El registre no ha estat modificat';
$gl_messages['no_records']='No hi ha cap registre en aquest llistat';
$gl_messages['no_records_bin']='No hi ha cap registre a la paperera';
$gl_messages['list_saved']='Els canvis en els registres s\'han efectuat correctament';
$gl_messages['list_not_saved']='No s\'ha modificat cap registre, no s\'ha detectat cap canvi';
$gl_messages['list_deleted']='Els registres han estat esborrats';
$gl_messages['list_bined']='Els registres s\'han enviat a la paperera de reciclatge';
$gl_messages['list_restored']='Els registres s\'han recuperat correctament';
$gl_messages['concession_no_acces']='No tens permisos per aquesta secció';
$gl_messages['concession_no_write']='No tens permisos d\'escritura per aquesta secció';
$gl_messages['select_item']='Fes un click en un registre de la llista per seleccionar';
$gl_messages['related_not_deleted']='Els registres marcats no s\'han pogut esborrar ja que tenen registres relacionats';


$gl_messages['deleted']='El registre ha estat esborrat';
$gl_messages['bined']='El registre  s\'ha enviat a la paperera de reciclatge';
$gl_messages['restored']='El registre s\'ha recuperat correctament';
$gl_caption['c_ok'] = 'Acceptar';
$gl_caption['c_cancel'] = 'Cancelar';
$gl_messages['post_max_size']='El pes del formulari (contingut + arxius adjunts) excedeix el límit de ';
$gl_messages['post_file_max_size']='El pes dels arxius adjunts excedeix el límit de ';
$gl_messages['assigned']='Registres sel·leccionats';

$gl_caption['c_form_sent'] = 'Gràcies per contactar amb nosaltres, respondrem al seu missatge el més aviat possible';
$gl_caption['c_form_not_sent'] = 'El formulari no s\'ha pogut enviar, si us plau comprovi que l\'adreça d\'email que ha escrit sigui correcta';

$gl_caption['c_cookies_message'] = 'Aquesta web utilitza cookies pròpies i de tercers per a millorar l\'experiència de navegació així com per a tasques d\'anàlisi. En continuar navegant, entenem que accepta l\'';
$gl_caption['c_cookies_message_more'] = 'ús de cookies';
$gl_caption['c_cookies_message_accept'] = 'Accepto';

$gl_caption['c_cookies_text'] = '

<p class="accept-cookies-title">Què són les cookies?</p>
<p>Una cookie es un petit arxiu que s’emmagatzema a l’ordinador de l’usuari i ens permet reconèixer-lo. El conjunt de cookies ens ajuda a millorar la qualitat del nostre web, permetent-nos controlar quines pàgines troben els nostres usuaris útils i quines no.</p>
<p>Les cookies són essencials per al funcionament d’Internet, aportant innumerables avantatges en la prestació de serveis interactius, facilitant-nos la navegació i usabilitat del nostre web. Tingueu en compte que les cookies no poden fer malbé el vostre equip i que, a canvi, si són activades ens ajuden a identificar i resoldre els errors.</p>
<p class="accept-cookies-title">Quin tipus de cookies utilitza aquesta web?</p>
<p>Cookies pròpies: són aquelles que s’envien a l’equip de l’usuari des d’un equip o domini gestionat per l’editor i des del que es presta el servei sol•licitat per l’usuari.</p>
<p>Cookies de tercers: són aquelles que s’envien a l’equip de l’usuari des d’un equip o domini no gestionat per l’editor sinó per una altra entitat que analitza les dades obtingudes.</p>
<p>Cookies persistents:són un tipus de cookies en el qual les dades segueixen emmagatzemades en el terminal i poden ser accedides i tractades pel responsable de la cookie.</p>
<p>Cookies d\'analítica web: a través de l\'analítica web s\'obté informació relativa al nombre d\'usuaris que accedeixen al web, el nombre de pàgines vistes, la freqüència i repetició de les visites, la seva durada, el navegador utilitzat, l\'operador que presta el servei, l\'idioma , el terminal que utilitza, o la ciutat a la qual està assignada la seva adreça IP. Informació que possibilita un millor i més apropiat servei per part d\'aquesta web.</p>
<p class="accept-cookies-title">Acceptació de l\'ús de cookies.</p>
<p>Assumim que vostè accepta l’ús de cookies. No obstant, vostè pot restringir, bloquejar o borrar les cookies d’aquesta web o qualsevol altra pàgina a partir de la configuración del seu navegador.</p>
<p>A continuación li mostrem les pàgines d’ajuda dels navegadors principals:</p>
<ul>
<li>Internet Explorer: <a href="http://windows.microsoft.com/es-xl/internet-explorer/delete-manage-cookies#ie="ie-10"" target="_blank">windows.microsoft.com/es-xl/internet-explorer/delete-manage-cookies#ie="ie-10"</a></li>
<li>FireFox: <a href="http://support.mozilla.org/es/kb/Borrar%20cookies" target="_blank">support.mozilla.org/es/kb/Borrar%20cookies</a></li>
<li>Chrome: <a href="http://support.google.com/chrome/answer/95647?hl="es"" target="_blank">support.google.com/chrome/answer/95647?hl="es"</a></li>
<li>Safari: <a href="http://www.apple.com/es/privacy/use-of-cookies/" target="_blank">www.apple.com/es/privacy/use-of-cookies/</a></li>
</ul>
';

// contrasenyes
$gl_caption['c_password_forgotten'] = 'Ha oblidat la seva contrasenya?';
$gl_caption['c_password_insert_mail'] = 'Si us plau, introdueixi la seva adreça d\'email i li enviarem una contrasenya nova al seu compte de correu.';
$gl_caption['c_password_insert_mail_exists'] = 'Si ha oblidat la seva contrasenya, si us plau, cliqui acceptar i li enviarem una contrasenya nova al seu compte de correu.';
$gl_caption['c_password_mail_error'] = 'L\'adreça d\'email proporcionada no existeix en la nostra base de dades, si us plau, escrigui l\'adreça correctament';
$gl_caption['c_password_sent'] = 'S\ha enviat una nova contrasenya a la seva adreça d\'email';
$gl_caption['c_password_mail_subject'] = 'La seva nova contrasenya';
$gl_caption['c_password_mail_text_1'] = 'La seva nova contrasenya és';
$gl_caption['c_password_mail_text_2'] = 'Pot canviar la contrasenya en la nostra pàgina web';
$gl_caption['c_welcome'] = 'Benvingut';

$gl_caption['c_new_custumer_exists'] = 'Ja ha entrat les dades d\'una una persona amb aquest nom i cognoms o data de naixement, pot:';
$gl_caption['c_new_custumer_exists_change_name'] = 'Corregir nom o data de naixement';
$gl_caption['c_new_custumer_exists_return'] = 'Tornar a la reserva i seleccionar persona de la llista';

// captcha
$gl_caption['c_recaptcha_error'] = 'Si us plau validi el "CAPTCHA" correctament';
?>