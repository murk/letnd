<?php
function list_records()
{
	global $gl_config, $gl_action;

    $listing = new ListRecords;
	if ($gl_config['custumer_table'] == 'custumer__custumer') {
    $listing->add_button ('assign_custumer', '', 'boto2', 'before','add_newsletter');
	}
	if ($gl_config['custumer_table'] == 'newsletter__custumer') {
    $listing->add_button ('assign_newsletter', 'action=save_rows_add_custumer', 'boto2', 'before');
	}
	$listing->has_bin = false;
    $listing->list_records();
}
function save_rows()
{
	global $gl_action, $gl_messages, $gl_message;
    /*
	switch($gl_action){
	case 'save_rows_add_custumer':
	window.save_frame.location = '/admin/?menu_id=206&action=save_rows&contact=' + contact_id ;
	return;
	} // switch
*/

  	$save_rows = new SaveRows;
    $save_rows->save();
}
function save_custumer () {
	global $gl_message, $gl_messages;

	$gl_message = $gl_messages['saved'];
}



function show_form()
{
    $show = new ShowForm;
	$show->has_bin = false;
    $show->show_form();
}

function write_record()
{
    $writerec = new SaveRows;
    $writerec->save();
}

function manage_images()
{
    $image_manager = new ImageManager;
    $image_manager->execute();
}
?>