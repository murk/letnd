<link href="/admin/themes/inmotools/styles/inmo_booking.css?v=<?=$version?>" rel="stylesheet" type="text/css">
<script language="JavaScript">

$(document).ready(				
	function ()
		{				

			$('#date1').datepicker({
				changeMonth: true,
				changeYear: true,
				yearRange: '+0:+5',
				onClose: function( selectedDate ) { 
					/*
					var date = $(this).datepicker('getDate');
					$( '#date1' ).datepicker( 'option', 'minDate', date );
					if (date) {
						  date.setDate(date.getDate() + 4);
					}
					$( '#date2' ).datepicker( 'option', 'maxDate', date );
					*/
				}
			});
			$('#date2').datepicker({
				changeMonth: true,
				changeYear: true,
				yearRange: '+0:+5',
				onClose: function( selectedDate ) {
					/*
					var date = $(this).datepicker('getDate');
					$( '#date1' ).datepicker( 'option', 'maxDate', date );
					if (date) {
						  date.setDate(date.getDate() - 4);
					}
					$( '#date1' ).datepicker( 'option', 'minDate', date );
					*/
				}
			}); 
	}
)

</script>


<div class="search police">
<h3><?=$c_police_search_title?></h3> 
<form name="search" method="get" action="/admin/"> 
    <input name="menu_id" type="hidden" value="<?=$menu_id?>">
	<table border="0" cellspacing="0" cellpadding="0">
        <tr valign="middle">	
            <td><?=$c_dates_from?>
              <input id="date1" name="date1" type="text" size="8" value="<?=$date1?>"></td>
            <td class="date2"><?=$c_dates_to?>
              <input id="date2" name="date2" type="text" size="8" value="<?=$date2?>"></td>
            <td class="buttons-td"> 
              <input type="submit" name="Submit" value="ok" class="boto-search"> </td>
        </tr>
      </table></form>
</div>