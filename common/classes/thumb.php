<?php

/**
 * Created by PhpStorm.
 * User: sanahuja
 * Date: 13/04/2015
 * Time: 15:37
 */
class Thumb {
	var $thumb, $quality, $image;

	public function __construct( $image ) {
		$this->thumb = new Imagick( $image );
		$this->image = $image;
	}

	public function resize( $width = 0, $height = 0 ) {

		// Qualitat
		$this->thumb->setImageCompression( Imagick::COMPRESSION_JPEG );
		$this->thumb->setImageCompressionQuality( $this->quality );


		// a la bd pot haver-hi el valor ''
		if ( ! $width ) {
			$width = 0;
		}
		if ( ! $height ) {
			$height = 0;
		}


		// FILTER_CATROM es més ràpid
		// SI la imatge és mes petita la redimensiona més gran ( millor qualitat així )
		if ( $width && $height ) {
			$this->get_closest_side( $width, $height );
		}

		$this->thumb->resizeImage( $width, $height, Imagick::FILTER_LANCZOS, 1 );

	}

	function get_closest_side( &$width, &$height ) {

		$size = $this->thumb->getImageGeometry();
		$w    = $size['width'];
		$h    = $size['height'];

//		if ($w <= $bound_width && $h <= $bound_height) {
//			return $this;
//		}

		if ( $width / $w > $height / $h ) {
			$width = 0;
		} else {
			$height = 0;
		}

	}

	/**
	 * @param $cg
	 * L'offset es posa en la mateixa imatge, per tant sempre s'alinea a tocar
	 * Opacity també a la mateixa imatge
	 * watermark_halign - > left, right. center
	 * watermark_valign - > top, bottom, middle
	 */
	public function add_watermark($cg) {

		$watermark_image = CLIENT_PATH . '/images/logo_watermark.png';

		// Calculo sobre una imatge de 2000 ample imaginaria ( uns 3 megapixels ), perquè sempre surti el watermark proporcionat a la imatge, i a partir de 2000 deixar el watermark igual
		$size = $this->thumb->getImageGeometry();

		$image_w = $size['width'];
		$image_h = $size['height'];

		$proportion = $image_w/2000;

		$watermark = new Imagick($watermark_image);
		$watermark_size = $watermark->getImageGeometry();

		$watermark_w = $watermark_size['width'];
		$watermark_h = $watermark_size['height'];

		// Així mai s'amplia el watermark, si l'ample de la imatge es més petit de 2000 es redueix a proporció
		if ($proportion < 1) {
			$watermark_w  = $watermark_w * $proportion;
			$watermark_h = $watermark_h * $proportion;
			$watermark->resizeImage( $watermark_w, $watermark_h, Imagick::FILTER_LANCZOS, 1 );
		}

		$h_align = $cg['watermark_halign'];
		$v_align = $cg['watermark_valign'];
		// $opacity = $this->config['watermark_opacity'];

		$x = 0;
		$y = 0;

		switch ( $h_align ) {
			case 'left':
				$x = 0;
				break;

			case 'center':
				$x = ($image_w / 2) - ($watermark_w / 2) ;
				break;

			case 'right':
				$x = $image_w - $watermark_w;
				break;
		}

		switch ( $v_align ) {
			case 'top':
				$y = 0;
				break;

			case 'middle':
			case 'center':
				$y = ($image_h / 2) - ($watermark_h / 2) ;
				break;

			case 'bottom':
				$y = $image_h - $watermark_h;
				break;
		}

		Debug::add('Watermark', $this->thumb);

		$this->thumb->compositeImage( $watermark, $watermark->getImageCompose(), $x, $y );
		$this->thumb->writeImage($this->image);


	}


	function auto_rotate() {

	    $orientation = $this->thumb->getImageOrientation();
	    $rotated = false;

	    switch($orientation) {
	        case imagick::ORIENTATION_BOTTOMRIGHT:
	            $this->thumb->rotateimage("#000", 180); // rotate 180 degrees
				$rotated = true;
	            break;

	        case imagick::ORIENTATION_RIGHTTOP:
	            $this->thumb->rotateimage("#000", 90); // rotate 90 degrees CW
				$rotated = true;
	            break;

	        case imagick::ORIENTATION_LEFTBOTTOM:
	            $this->thumb->rotateimage("#000", -90); // rotate 90 degrees CCW
				$rotated = true;
	            break;
	    }

	    // Now that it's auto-rotated, make sure the EXIF data is correct in case the EXIF gets saved with the image!
		if ($rotated) {
			$this->thumb->setImageOrientation( imagick::ORIENTATION_TOPLEFT );
		}
		return $rotated;
	}


	/**
	 * @param string $settings radius,sigma,amount,unsharpThreshold 0.5|0.5|2.5|0.02
	 *
	 */
	public function unsharp( $settings = '0.5|0.5|2.5|0.02' ) {

		if (!$settings) return;

		$settings = explode( '|', $settings );
		//debug::add( 'Unsharp mask', $settings );

		$this->thumb->unsharpMaskImage( floatval($settings[0]), floatval($settings[1]), floatval($settings[2]), floatval($settings[3]) );
	}

	public function save( $path ) {
		if ( is_file( $path ) ) {
			unlink( $path );
		}

		//$this->thumb->setImageFormat( "jpeg" );
		$this->thumb->setInterlaceScheme(Imagick::INTERLACE_PLANE);

		if ( $this->thumb->writeImage( $path ) ) {
			Debug::add( 'Thumb generat', $path );
		} else {
			// do something with error message
			Debug::add( 'Thumb error', 'No s\'ha pogut gravar' );
		}
	}

	function clear() {
		$this->thumb->clear();
	}
}