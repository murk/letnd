<?php
/**
 * Clase ImageManager
 *
 * @author  Joan Sanahuja <sanahuja@gmail.com>
 * @version 1.0
 */

/**
 * Classe per gestionar les imatges
 *
 * @author  Joan Sanahuja <sanahuja@gmail.com>
 * @version 1.0
 */
class ImageManager Extends ModuleBase {
	/**
	 * Template del block
	 *
	 * @access public
	 * @var object
	 */
	var $template;
	var $use_default_template;
	var $template_base;
	var $title;
	var $config;
	var $parent_language_fields;
	var $parent_fields;
	var $parent_id_field;
	var $tool;
	var $tool_section;
	var $reload = true;
	var $id;

	var $action = '';
	var $caption = '';
	var $language_fields, $fields, $id_field, $table, $form_tabs = array(), $show_categorys = true, $show_main_image = true, $function_name = '', $images_dir;

	function __construct( &$module = false ) {
		if ( VERSION == 5 ) {
			$module_copy = $module ? clone ( $module ) : $module;
		} else  $module_copy = $module;
		$this->link_module( $module_copy );
		//$this->link_module(&$module);
		// crec que fa falta sense referencia, ja que modifiquem les vars fields, table, id_field i alguna mes, i no s'hauria de modificar a tot el modul
		// quan hagi fet un cas practic ho probaré


		//////////////////////////////////////////
		// que s'ha de fer amb aquestes globals???????
		/////////////////////////////////////////
		global $gl_messages_image, $gl_caption_image;


		$GLOBALS['gl_page']->is_image_form = true;
		$this->parent_language_fields      = $this->language_fields;
		$this->parent_fields               = $this->fields;
		$this->language_fields             = array( 0 => 'image_alt', 1 => 'image_title' );
		$this->related_tables              = array();
		$this->upload_settings             = array();
		$this->has_images                  = false;
		$this->table                       = $this->table . '_image';
		$this->parent_id_field             = $this->id_field;
		$this->id_field                    = 'image_id';
		$this->images_dir = defined ('IMAGES_DIR') ? IMAGES_DIR : '';

		$this->fields = array(
			'image_id'    =>
				array(
					'type'                => 'hidden',
					'enabled'             => '1',
					'form_admin'          => 'input',
					'form_public'         => '0',
					'list_admin'          => 'input',
					'list_public'         => '0',
					'order'               => '0',
					'default_value'       => '',
					'override_save_value' => '',
					'class'               => '',
					'javascript'          => '',
					'text_size'           => '',
					'text_maxlength'      => '11',
					'textarea_cols'       => '',
					'textarea_rows'       => '',
					'select_caption'      => '',
					'select_size'         => '',
					'select_table'        => '',
					'select_fields'       => '',
					'select_condition'    => '',
				),
			'ordre'       =>
				array(
					'type'                => 'hidden',
					'enabled'             => '1',
					'form_admin'          => 'input',
					'form_public'         => '0',
					'list_admin'          => 'input',
					'list_public'         => '0',
					'order'               => '0',
					'default_value'       => '',
					'override_save_value' => '',
					'class'               => '',
					'javascript'          => '',
					'text_size'           => '3',
					'text_maxlength'      => '11',
					'textarea_cols'       => '',
					'textarea_rows'       => '',
					'select_caption'      => '',
					'select_size'         => '',
					'select_table'        => '',
					'select_fields'       => '',
					'select_condition'    => '',
				),
			'main_image'  =>
				array(
					'type'                => 'radio',
					'enabled'             => '1',
					'form_admin'          => 'input',
					'form_public'         => '0',
					'list_admin'          => 'input',
					'list_public'         => '0',
					'order'               => '0',
					'default_value'       => '',
					'override_save_value' => '',
					'class'               => '',
					'javascript'          => '',
					'text_size'           => '',
					'text_maxlength'      => '1',
					'textarea_cols'       => '',
					'textarea_rows'       => '',
					'select_caption'      => '',
					'select_size'         => '',
					'select_table'        => '',
					'select_fields'       => '',
					'select_condition'    => '',
				),
			'name'        =>
				array(
					'type'                => 'text',
					'enabled'             => '1',
					'form_admin'          => 'input',
					'form_public'         => '0',
					'list_admin'          => 'text',
					'list_public'         => '0',
					'order'               => '0',
					'default_value'       => '',
					'override_save_value' => '',
					'class'               => '',
					'javascript'          => '',
					'text_size'           => '',
					'text_maxlength'      => '100',
					'textarea_cols'       => '',
					'textarea_rows'       => '',
					'select_caption'      => '',
					'select_size'         => '',
					'select_table'        => '',
					'select_fields'       => '',
					'select_condition'    => '',
				),
			'image_alt'   =>
				array(
					'type'                => 'text',
					'enabled'             => '1',
					'form_admin'          => 'input',
					'form_public'         => '0',
					'list_admin'          => 'input',
					'list_public'         => '0',
					'order'               => '0',
					'default_value'       => '',
					'override_save_value' => '',
					'class'               => 'wide',
					'javascript'          => '',
					'text_size'           => '',
					'text_maxlength'      => '150',
					'textarea_cols'       => '',
					'textarea_rows'       => '',
					'select_caption'      => '',
					'select_size'         => '',
					'select_table'        => '',
					'select_fields'       => '',
					'select_condition'    => '',
				),
			'image_title' =>
				array(
					'type'                => 'text',
					'enabled'             => '1',
					'form_admin'          => 'input',
					'form_public'         => '0',
					'list_admin'          => 'input',
					'list_public'         => '0',
					'order'               => '0',
					'default_value'       => '',
					'override_save_value' => '',
					'class'               => 'wide',
					'javascript'          => '',
					'text_size'           => '',
					'text_maxlength'      => '250',
					'textarea_cols'       => '',
					'textarea_rows'       => '',
					'select_caption'      => '',
					'select_size'         => '',
					'select_table'        => '',
					'select_fields'       => '',
					'select_condition'    => '',
				),
			'imagecat_id' =>
				array(
					'type'                => 'select',
					'enabled'             => '1',
					'form_admin'          => 'input',
					'form_public'         => '0',
					'list_admin'          => 'input',
					'list_public'         => '0',
					'order'               => '5',
					'default_value'       => '',
					'override_save_value' => '',
					'class'               => 'small',
					'javascript'          => '',
					'text_size'           => '',
					'text_maxlength'      => '',
					'textarea_cols'       => '',
					'textarea_rows'       => '',
					'select_caption'      => '',
					'select_size'         => '',
					'select_table'        => $this->table . 'cat',
					'select_fields'       => 'imagecat_id,imagecat',
					'select_condition'    => '1 = 1 ORDER BY ordre ASC, imagecat ASC',
				),
		);

		if ( is_array( $gl_messages_image ) ) {
			$this->messages = array_merge( $this->messages, $gl_messages_image );
		}

		// poder serà millor treure aquests captions i posar-ho tot directament als generals
		// no vull posar la variable ->caption_image lincada al modul, ja que nomès l'utilitza aquesta classe, quan no sigui v3 es pot eliminar la linea de sota
		if ( $this->is_v3 ) {
			$caption_image = $module->caption_image;
		} else  $caption_image = $GLOBALS['gl_caption_image'];
		$this->caption = array_merge( $this->caption, $caption_image );

		$this->template_base = PATH_TEMPLATES . $this->tool . '/' . $this->tool_section . '_';
		$this->set_options();
	}

	function execute() {
		global $gl_page, $gl_message, $gl_write;

		// si es per ajax no es pot fer plantilla especifica d'una eina ( crec que no cal, si no hauria de fer el mateix sistema que les 3 opcions de sota mes un sufix '_ajax', però ara m'estavio fer-ne una per cada eina que porta plantilla especial - De moment totes les que hi ha especials és perque feien falta les pestanyes )
		if ( isset( $_GET['is_ajax'] ) ) {
			$this->template             = 'image_ajax';
			$this->use_default_template = true;
		} elseif ( $this->template ) {
			$this->template             = $this->tool . '/' . $this->template;
			$this->use_default_template = false;
		} elseif ( file_exists( $this->template_base . 'image.tpl' ) ) {
			$this->template             = $this->tool . '/' . $this->tool_section . '_' . 'image';
			$this->use_default_template = false;
		} else {
			$this->template             = 'image';
			$this->use_default_template = true;
		}

		switch ( true ) {
			case strstr( $this->action, 'list_records_images' ):
				$this->list_records();
				break;
			case strstr( $this->action, "save_rows_get_images" ):
				$this->save_rows_get_images();
				Main::do_next( $this->action );
				break;
			case strstr( $this->action, 'save_rows_save_records_images' ):
			case strstr( $this->action, 'save_rows_delete_selected_images' ):
				// un cop acabat
				// Despres de guardar el formulari mostrem el mateix llistat d'on venim
				if ( $gl_write ) {
					if ( isset( $_POST[ $this->id_field ] ) ) // quan no hi ha cap imatge i premem guardar, que no faci res, sino peta
					{
						$this->save_rows();
						//if ($this->reload) Main::do_next($this->action);					
						// QUAN IMATGES VAGIN AMB IDS if ($this->action == 'save_rows_delete_selected') print_javascript('top.delete_selected_images();');
						if ( $this->action == 'save_rows_delete_selected' ) {
							print_javascript( "window.location.href = '" . '/admin/?action=list_records_images' . $action_add . get_all_get_params( array( 'action' ), '&', '&', true ) . "is_ajax=true';" );
						}
					} // encara que no fagi res, almenys mostro el missatge de guardat. Això em serveix si vull posar un altre camp a la imatge, com a la home de amctaic
					else {
						$gl_message = $this->messages['saved'];
					}
					$gl_page->show_message( $gl_message );
				} else {
					$gl_page->show_message( $this->messages['concession_no_write'] );
				}
				Debug::p_all();
				die();
				break;
			case strstr( $this->action, "show_uploader_images" ):
				$this->show_uploader_images();
				break;
		} // switch
	}

	function save_rows() {
		$main_image_deleted = false;
		$save_rows          = new SaveRows( $this );
		// borrar les imatges abans de borrar-les de la BBDD
		if ( strstr( $this->action, 'save_rows_delete_selected_images' ) ) {
			$save_rows->action = 'save_rows_delete_selected';
			// m'asseguro que nomès borra les del mateix registre ($parent_id)
			$save_rows->condition = $this->parent_id_field . '=' . $_GET[ $this->parent_id_field ];
			$query                = '';
			foreach ( $_POST["selected"] as $key ) {
				$query .= $this->id_field . " = " . $key . " OR ";
			}
			if ( $query ) {
				$query = substr( $query, 0, - 4 );
				$query = "SELECT " . $this->id_field . ", name, main_image
						FROM " . $this->table . "
						WHERE " . $query . "
						AND " . $save_rows->condition;
				Debug::add( 'Consulta arxius per borrar:', $query );
				$results = Db::get_rows( $query );
			}
			foreach ( $results as $rs ) {
				$this->delete_images_resized( $rs[ $this->id_field ], strrchr( substr( $rs["name"], - 5, 5 ), '.' ) );
				$main_image_deleted = $main_image_deleted || $rs["main_image"];
			}
			$save_rows->save();
			if ( $main_image_deleted ) {
				$query = 'UPDATE ' . $this->table . ' SET main_image = 1
	               WHERE ' . $this->parent_id_field . ' = ' . R::id( $this->parent_id_field ) . '
				   ORDER BY ordre LIMIT 1';
				Db::execute( $query );
			}
		}
		elseif ( strstr( $this->action, 'save_rows_save_records_images' ) ) {
			$save_rows->action = 'save_rows_save_records';
			// el $save_rows->save() ja me'ls posa tots el main_image a 0

			$save_rows->save();
			// poso el save abans així no hem sobreescriu lo de main_image
			$query = "UPDATE " . $this->table . " SET main_image = 1
	               WHERE " . $this->id_field . " = " . R::id( 'main_image', false, '_POST' );
			Debug::add( "Consulta im 2", $query );

			Db::execute( $query );
			Debug::add( "", "" );


			/*	Per fer aquest canvi, també s'hauria de recarregar l'html de les imatges
			$id = $_GET[ $this->parent_id_field ];
			if ($id) {
				$images = UploadFiles::get_record_images($id, true, $this->tool, $this->tool_section );
				$images_js = ImageManager::get_gallery_images( $images['images']);
				print_javascript(
					"top.gl_gallery_images =  " . ImageManager::get_gallery_images( $images_js ) . ";" );
			}*/
		}
	}

	// guarda una sola imatge
	function drop_save_image( $id, $tool, $tool_section ) {

		//$this->id_field = $this->tool_section . '_id'; // per insertar l'id de la taula relacionada
		// mirem si hi ha una main image o no
		Debug::add( 'Post', $_POST );

		$table = $tool . '__' . $tool_section . '_image';


		$query = "SELECT count(main_image) FROM " . $table . " WHERE main_image = 1
	               AND " . $tool_section . "_id = " . $id;
		Debug::add( 'Query', $query );
		$results           = Db::get_rows( $query );
		$is_set_main_image = $results[0]['count(main_image)'] != 0;
		Debug::add( 'Count main image', $results );

		$this->uploads['image']->get_files();

		//$gl_db_classes_id_field = $old_id_field; // ho torno a deixar com era
		// bucle per totes les imatges pujades
		$count = 0;
		foreach ( $this->uploads['image']->files as $file ) {
			Debug::add( 'Count, $is_set_main_image', $count . ' ' . $is_set_main_image );
			if ( ( $count == 0 ) && ( ! $is_set_main_image ) ) {
				// marquem la primera com a principal
				$query = "UPDATE " . $table . " SET main_image = 1
	               WHERE image_id = " . R::id( $file['id'], false, false );
				Debug::add( "Consulta Prira main_image", $query );
				Db::execute( $query );
			}
			$count ++;
			$this->resize_images( $file['id'], strrchr( substr( $file['store_name'], - 5, 5 ), '.' ), $this->config, true );
		}


		// Mostro les imatges noves al formulari per ajax ( iframe )
		$_GET['is_ajax'] = true;
		$_GET[$tool_section . '_id'] = $id;
		$this->action = 'list_records_images';
		$this->execute();

	}

	function save_rows_get_images() {
		//$this->id_field = $this->tool_section . '_id'; // per insertar l'id de la taula relacionada
		// mirem si hi ha una main image o no
		Debug::add( 'Post', $_POST );
		// el jumploader no passa el post del formulari en safari i chrome
		// m'asseguro que almenys hi hagi el post que envia el jumploader (filepath)
		// defineixo la variable del post així la tinc per fer-la servir en upload_files i no haig de fer tot aquest rotllo un altre cop
		$_POST[ $this->parent_id_field ] = isset( $_POST[ $this->parent_id_field ] ) ?
			$_POST[ $this->parent_id_field ] :
			( isset( $_POST['filePath'] ) ? $_GET[ $this->parent_id_field ] : 0 );
		$query                           = "SELECT count(main_image) FROM " . $this->table . " WHERE main_image = 1
	               AND " . $this->tool_section . "_id = " . R::id( $this->parent_id_field, false, '_POST' );
		Debug::add( 'Query', $query );
		$results           = Db::get_rows( $query );
		$is_set_main_image = $results[0]['count(main_image)'] != 0;
		Debug::add( 'Count main image', $results );
		$this->uploads['image']->get_files();
		//$uploader_initial_location = $_POST['filePath'];
		$uploader_initial_location = str_replace( $_POST['fileName'], '', $_POST['filePath'] );
		setcookie( 'uploader_initial_location', $uploader_initial_location );

		//$gl_db_classes_id_field = $old_id_field; // ho torno a deixar com era
		// bucle per totes les imatges pujades
		$count = 0;
		foreach ( $this->uploads['image']->files as $file ) {
			Debug::add( 'Count, $is_set_main_image', $count . ' ' . $is_set_main_image );
			if ( ( $count == 0 ) && ( ! $is_set_main_image ) ) {
				// marquem la primera com a principal
				$query = "UPDATE " . $this->table . " SET main_image = 1
	               WHERE image_id = " . R::id( $file['id'], false, false );
				Debug::add( "Consulta Prira main_image", $query );
				Db::execute( $query );
			}
			$count ++;
			$this->resize_images( $file['id'], strrchr( substr( $file['store_name'], - 5, 5 ), '.' ) );
		}
	}

	function call($function_name){
		 $this->function_name = $function_name;
	}

	function list_records() {
		if ( ! $this->show_categorys || ! is_table( $this->table . 'cat' ) ) {
			$this->unset_field( 'imagecat_id' );
			$this->set_var( 'imagecat_id', false );
			$this->show_categorys = false; // si no hi ha taula, no mostro categories
		}
		if ( ! $this->show_main_image ) {
			$this->unset_field( 'main_image' );
			$this->set_var( 'main_image', false );
		}


		global $gl_languages;

		$listing                        = new ListRecords( $this );
		$this->last_listing             = $listing->last_listing;
		$listing->show_langs            = true;
		$listing->has_images            = false;
		$listing->paginate              = false;
		$listing->has_bin               = false;
		$listing->always_parse_template = true;

		$listing->cols       = $this->config['im_list_cols'];
		$listing->action_add = str_replace( "_images", '', $listing->action_add );

		$this->id = R::id($this->tool_section . '_id');

		$listing->set_var( 'show_seo', $_SESSION['show_seo'] );
		$listing->set_var( $this->tool_section . '_id', $this->id );
		$listing->set_var( 'tool_section_id', $this->id );
		$listing->set_var( 'id', $this->id );
		$listing->set_var( 'id_field', $this->parent_id_field );
		$listing->set_var( 'images_title', $this->get_images_title() );
		$listing->set_var( 'back_button', $listing->last_listing['link'] );
		$action_edit_images = "?action=save_rows_save_records_images" . $listing->action_add . get_all_get_params( array( 'action' ), '&' );
		// show_uploader_images   ---- $action_upload_images = "?action=save_rows_get_images&" . get_all_get_params(array('action'));
		$listing->set_var( 'link_action_edit_images', $action_edit_images );
		// show_uploader_images   ---- $tpl->set_var('link_action_upload_images', $action_upload_images);
		$listing->set_var( 'uploader_link', '/admin/?action=show_uploader_images' . $listing->action_add . get_all_get_params( array(
				'action',
				'q',
				'Submit'
			), '&' ) );
		$listing->set_var( 'data_link', '/admin/?action=show_form_edit' . $listing->action_add . get_all_get_params( array( 'action' ), '&' ) );

		$language = current( $gl_languages['public'] );

		if (USE_FRIENDLY_URL){
			$preview_link = "/$language/$this->tool/$this->tool_section/$this->id.htm?preview";
		}
		else {
			$preview_link = "/index.php?$this->base_link&amp;action=show_record&amp;$this->id_field=$this->id&amp;language=$language&preview";
		}

		$listing->set_var( 'preview_link', $preview_link );

		$this->set_prev_next_links();

		// show_uploader_images   ---- $tpl->set_var('reload_link', '/admin/?action=list_records_images&' . get_all_get_params(array('action')));
		// show_uploader_images   ---- $tpl->set_var('section_id_field', $tool_section . '_id');
		// show_uploader_images   ---- $tpl->set_var('section_id', $_GET[$tool_section . '_id']);
		$listing->condition          = $this->tool_section . '_id' . ' = ' . $this->id;
		$listing->no_records_message = "";

		$listing->template = $this->template;

		$listing->call( "get_file_name_i", "image_id,name", true );
		$listing->order_by = 'ordre asc';

		$listing->set_records();

		// obtinc parametre de l'array d'imatges mitjanes per passar a un visualitzador
		// else per im_list_cols = 1
		$images = array();
		foreach ( $listing->tpl->loops['loop'] as $rs ) {
			if ( $rs['loop'] ) {
				foreach ( $rs['loop'] as $rs ) {
					if ( $rs['image_id'] ) {
						$images []= $rs;
					}
				}
			}
			else {
				$images []= $rs;
			}
		}
		$listing->set_var( 'images', $images );
		$listing->set_var( 'images_dir', CLIENT_DIR . '/' . $this->tool . '/' . $this->tool_section . '/images/' );
		// tabs
		$listing->set_var( 'form_tabs', $this->form_tabs );
		$listing->set_var( 'parent_id_field', $this->parent_id_field );
		$listing->set_var( 'parent_id', $_GET[ $this->parent_id_field ] );
		// tabs end

		if ( isset( $_GET['is_ajax'] ) ) {

			$images_html = $listing->tpl->process();

			if ( isset( $_GET['is_dropzone_form'] ) ) {
				echo( ' {
							"images_html": ' . json_encode( $images_html ) . ',
							"gallery_images": ' . ImageManager::get_gallery_images($images) . ' }'
				);
			} else {
				set_inner_html( 'ajax_list', $images_html );
				print_javascript(
					"top.start_images_drag();
					top.gl_gallery_images =  " . ImageManager::get_gallery_images($images) . ";" );
				if ( isset( $_GET['is_uploader'] ) ) {
					$GLOBALS['gl_page']->show_message( $this->messages['images_uploaded'] );
					Debug::p_all();
					die();
				}
			}
		} else {
			$GLOBALS['gl_content'] = $listing->tpl->process();
		}
	}

	function show_uploader_images() {
		$GLOBALS['gl_page']->template = '';

		$action_add = str_replace( "show_uploader_images", '', $this->action );

		$this->set_vars( $this->caption );
		$this->set_var( 'watermark', 0 ); // per si no hi es al config
		$this->set_var( 'custom_ratios', 0 ); // per si no hi es al config
		$this->set_vars( $this->config );

		$action_upload_images = "?action=save_rows_get_images" . $action_add . get_all_get_params( array( 'action' ), '&' );
		$this->set_var( 'section_id_field', $this->tool_section . '_id' );
		$this->set_var( 'section_id', $_GET[ $this->tool_section . '_id' ] );
		$this->set_var( 'link_action_upload_images', $action_upload_images );
		$this->set_var( 'reload_link', '/admin/?action=list_records_images' . $action_add . get_all_get_params( array( 'action' ), '&', '', true ) );
		$this->set_var( 'uploader_initial_location', isset( $_COOKIE['uploader_initial_location'] ) ? $_COOKIE['uploader_initial_location'] : '' );

		// Desconecto ja per sempre el aurigma i el seu missatge de trial
		//$this->set_file($_SESSION['browser']=='Internet Explorer'?'image_uploader.tpl':'image_uploader_java.tpl');
		$this->set_file( 'image_uploader_java.tpl' );
		$GLOBALS['gl_content'] = $this->process();
	}

	static function get_gallery_images( &$images ) {

		$ret = array();
		foreach ( $images as $image ) {
			$ret [] = array(
				'src'     => $image['image_src'],
				'thumb'   => $image['image_src_admin_thumb'],
				'subHtml' => '<h4>' . $image['image_title_value'] . '</h4><p>' . $image['image_alt_value'] . '</p>'
			);
		}

		return json_encode( $ret );
	}

	/**
	 *
	 * @access public
	 * @return void
	 * Si no config, es creen els thumbs automàticament al pujar desde jumploader
	 * Si config, serveix per crear els thumbs a partir d'una imatge desde qualsevol lloc manualment
	 * ej.
	 *   $cg = $this->get_config('admin','inmo__configadmin');
	 *   ImageManager::resize_images($image['image_id'], $ext, $cg);
	 */
	function resize_images( $image_id, $image_ext, $cg = false, $resize_all = false ) {
		set_time_limit( 0 );
		$add_watermark_medium = $add_watermark_big = false;
		$image_big       = $this->images_dir . $image_id . $image_ext;
		$new_images_path = array(
			'im_details'     => $this->images_dir . $image_id . "_details" . $image_ext,
			// 'im_medium' => $this->images_dir . $image_id . "_medium" . $image_ext,
			'im_thumb'       => $this->images_dir . $image_id . "_thumb" . $image_ext,
			'im_admin_thumb' => $this->images_dir . $image_id . "_admin_thumb" . $image_ext,
		);

		if ( ! $cg ) {
			// jumploader
			$cg = $this->config;
			$add_watermark_medium = true;
		}
		else {
			// desde el dropzone només puja la gran o desde un altre mòdul
			$new_images_path['im_medium'] = $this->images_dir . $image_id . "_medium" . $image_ext;
		}

		// a dropzone fa un resize_all
		if ( $resize_all ) {
			$new_images_path['im_big'] = $this->images_dir . $image_id . "" . $image_ext;
		}
		else {
			// Ho faig després del resize$this->add_watermark($image_big, $cg);
			$add_watermark_big = true;
		}
		Debug::p( [
					[ 'resize_all', $resize_all ],
					[ 'new_images_path', $new_images_path ],
				], 'images');


		foreach ( $new_images_path as $key => $path ) {

			// LINUX
			// imagemagick
			//if ( !$GLOBALS['gl_is_windows'] ) {
			if ( class_exists("Imagick")) {

				$thumb = new Thumb( $image_big ); // TODO Haig de probar si es pot posar fora del bucle

				$thumb->quality = isset( $cg[ $key . '_q' ] ) ? $cg[ $key . '_q' ] : '60';
				$thumb->auto_rotate();
				$thumb->resize( $cg[ $key . '_w' ], $cg[ $key . '_h' ] );

				$thumb->unsharp( isset( $cg[ $key . '_unsharp' ] ) ? $cg[ $key . '_unsharp' ] : '0.5|0.5|2.5|0.02' );
				$thumb->save( $path );

				$thumb->clear();

				$this->add_watermark($path, $cg);


				debug::add( 'Llibreria imatges', 'imagick' );
			}
			// WINDOWS , obsolet, només en local ja que no se com instalar imagemagick
			// també en servidors que no estigui instalat
			else {
				include (DOCUMENT_ROOT.'common/includes/phpthumb/phpthumb.class.php');

				$phpThumb      = new phpThumb();
				$phpThumb->src = $image_big;
				$phpThumb->f   = 'jpeg';

				$phpThumb->w = $cg[ $key . '_w' ];
				$phpThumb->h = $cg[ $key . '_h' ];
				$phpThumb->q = isset( $cg[ $key . '_q' ] ) ? $cg[ $key . '_q' ] : '60';
				$phpThumb->setParameter( 'fltr', 'usm|80|0.5|3' );

				if ( $phpThumb->GenerateThumbnail() ) {
					$phpThumb->RenderToFile( $path );
				}

				debug::add( 'Llibreria imatges', 'gd' );
			}

		}

		// Quan ve de jumploader o es fa un resize_all
		if ( $add_watermark_medium ) {
			$this->add_watermark($this->images_dir . $image_id . "_medium" . $image_ext, $cg);
			// $this->auto_rotate_not_resized($this->images_dir . $image_id . "_medium" . $image_ext);
		}
		if ( $add_watermark_big ) {
			$this->add_watermark($image_big, $cg);
			// $this->auto_rotate_not_resized($image_big);
		}

	}

	function auto_rotate_not_resized($image) {
		// Jumploader sembla que fa be la orientacio, per tant no faig servir aquesta funció
		$thumb = new Thumb( $image );
		if ($thumb->auto_rotate()) $thumb->thumb->writeImage($image);

	}

	function add_watermark( $image, $cg ) {

		if (!isset($cg['watermark']) || $cg['watermark'] == '0' || !class_exists("Imagick")) return;

		$thumb = new Thumb( $image );
		$thumb->quality = 100;
		$thumb->add_watermark ( $cg );
		$thumb->clear();


		return;
	}

	/**
	 *
	 * @access public
	 * @return void
	 */
	function delete_images_resized( $image_id, $image_ext ) {
		$image_big         = $this->images_dir . $image_id . $image_ext;
		$image_medium      = $this->images_dir . $image_id . "_medium" . $image_ext;
		$image_details     = $this->images_dir . $image_id . "_details" . $image_ext;
		$image_thumb       = $this->images_dir . $image_id . "_thumb" . $image_ext;
		$image_admin_thumb = $this->images_dir . $image_id . "_admin_thumb" . $image_ext;
		if ( file_exists( $image_big ) ) {
			unlink( $image_big );
		}
		if ( file_exists( $image_medium ) ) {
			unlink( $image_medium );
		}
		if ( file_exists( $image_details ) ) {
			unlink( $image_details );
		}
		if ( file_exists( $image_thumb ) ) {
			unlink( $image_thumb );
		}
		if ( file_exists( $image_admin_thumb ) ) {
			unlink( $image_admin_thumb );
		}
	}
	//////// per cridar desde fora la clase s'ha de substitur per UploadFiles::get_record_images ja que es mes practica la funcio
	/**
	 *
	 * @access public
	 * @return array
	 * Si especifico tool i tool_section, puc cridar la funció independentment de la clase,
	 * ej. a print_contract.php per agafar les imatges de l'inmoble relacionat
	 * $rs += ImageManager::get_file_name($results[0]['image_id'], $results[0]['name'],'inmo','property');
	 */
	// per cridar desde instancia
	function get_file_name_i( &$listing, $image_id, $name, $tool = '', $tool_section = '' ) {
		$tool         = $tool ? $tool : $this->tool;
		$tool_section = $tool_section ? $tool_section : $this->tool_section;
		$ret = self::get_file_name( $image_id, $name, $tool, $tool_section );

		if ( $this->function_name ) {
			$ret = array_merge( $ret, $this->module->{$this->function_name}( $listing, $this ) );
		}

		return $ret;
	}
	static function get_file_name( $image_id, $name, $tool = '', $tool_section = '' ) {
		global $gl_tool, $gl_tool_section; // -v3- per fer una crida desde listing->call .... que no es poden posar tool ni section com a parametres

		$tool         = $tool ? $tool : $gl_tool;
		$tool_section = $tool_section ? $tool_section : $gl_tool_section;

		$name              = strrchr( substr( $name, - 5, 5 ), '.' );
		$image             = $image_id . $name;
		$image_medium      = $image_id . '_medium' . $name;
		$image_details     = $image_id . '_details' . $name;
		$image_thumb       = $image_id . '_thumb' . $name;
		$image_admin_thumb = $image_id . '_admin_thumb' . $name;

		return array(
			"image_name"             => $image,
			"image_src"              => '/' . CLIENT_DIR . '/' . $tool . '/' . $tool_section . '/images/' . $image,
			"image_name_medium"      => $image_medium,
			"image_src_medium"       => '/' . CLIENT_DIR . '/' . $tool . '/' . $tool_section . '/images/' . $image_medium,
			"image_name_details"     => $image_details,
			"image_src_details"      => '/' . CLIENT_DIR . '/' . $tool . '/' . $tool_section . '/images/' . $image_details,
			"image_name_thumb"       => $image_thumb,
			"image_src_thumb"        => '/' . CLIENT_DIR . '/' . $tool . '/' . $tool_section . '/images/' . $image_thumb,
			"image_name_admin_thumb" => $image_admin_thumb,
			"image_src_admin_thumb"  => '/' . CLIENT_DIR . '/' . $tool . '/' . $tool_section . '/images/' . $image_admin_thumb
		);
	}

	function get_images_title() {
		global $gl_languages;
		if ( ! $this->form_settings['images_title'] ) {
			return '';
		}
		$fields = $this->form_settings['images_title'];

		$id     = $this->tool_section . '_id';
		$id_val = R::id( $id );
		// si no hi ha cap camp d'idiomes
		$table     = $this->tool . '__' . $this->tool_section;
		$condition = ' WHERE ' . $id . '=' . $id_val;
		// si un dels camps es en idiomes

		foreach ( $this->parent_language_fields as $field ) {
			if ( strpos( $fields, $field ) !== false ) {
				$lang = current( $gl_languages['public'] );
				$condition = " WHERE " . $table . "." . $id . "=" . $id_val . "
								AND " . $table . "." . $id . "=" . $table . "_language." . $id . "
								AND language = '" . $lang . "'";
				$table .= ',' . $table . '_language';
				break;
			}
		}

		$query = "SELECT " . $fields . " FROM " . $table . $condition;
		Debug::add( "Consulta images title", $query );
		$rs  = Db::get_row( $query );
		$ret = '';
		foreach ( $rs as $key => $val ) {
			// si es un select
			if ( $this->parent_fields[ $key ]['type'] == 'select' ) {
				//  enum, agafo del caption
				if ( isset( $this->caption[ 'c_' . $key . '_' . $val ] ) ) {
					$val = $this->caption[ 'c_' . $key . '_' . $val ];
				}
				// select, agafo de la taula relacionada
				// TODO
			}
			if ( $val ) {
				$ret .= $val . ' - ';
			}
		}
		$ret = substr( $ret, 0, - 2 );

		return $ret;
	}
	// Configuració del menu d'opcions d'editar
	// REVISAR no tinc temps, $save_button i $delete_button el list records el sobreescriu
	function set_options( $save_button = 1, $delete_button = 1, $preview_button = 0, $print_button = 0 ) {
		$this->set_var( 'save_button', $save_button );
		$this->set_var( 'delete_button', $delete_button );
		$this->set_var( 'preview_button', $preview_button );
		$this->set_var( 'print_button', $print_button );
	}

	function set_prev_next_links() {
		// TODO-i S'ha de fer amb l'objecte de formulari, sino ´faria el next_prev de les imatges
		// Main::load_class( 'db_utils' );
		// Utils::set_prev_next_links( $this );
	}

	/**
	 * @param $file
	 *
	 * @return bool True si es vertical
	 */
	static public function is_image_vertical( $file ) {
		if (!is_file( DOCUMENT_ROOT . $file ) ) return false; // per local si no tinc les imatges

		$size = getimagesize( DOCUMENT_ROOT . $file );
		$w            = $size[0];
		$h            = $size[1];

		return  $h > $w;
	}
	/**
	 *
	 * @access public
	 *
	 * @param $file
	 * @param $alt
	 * @param bool $max_w
	 * @param bool $max_h
	 * @param bool $center_vertical
	 * @param string $domain
	 * @param bool $responsive
	 *
	 * @return string M'encuadra una imatge en unes mides concretes, centra vertical si convé i retorna
	 * M'encuadra una imatge en unes mides concretes, centra vertical si convé i retorna
	 * O em calcula la mida que em falta si he posat a false una de les 2 mides, ej. vull que faci 100 amplada, calcula amb proporció quina alçada té segons la imatge original
	 */
	static public function get_image_attrs( $file, $alt, $max_w = false, $max_h = false, $center_vertical = true, $domain = '', $responsive = false ) {

		$size = false;

		if ( $domain ) {
			$path = $domain;
		} else $path = DOCUMENT_ROOT;

		// per evitar errors en local que no tinc les imatges
		// si tinc domini no comprovo si es file ja que no funciona per http
		if ( $domain || is_file( $path . $file ) ) {
			$size = getimagesize( $path . $file );
		}

		if ( ! $size ) {
			return ' src=""';
		}

		$w            = $size[0];
		$h            = $size[1];
		$src          = ' src="' . $domain . $file . '"';
		$alt          = ' alt="' . htmlspecialchars( $alt ) . '"'; // mostro sempre un alt, aixi valida w3c
		$result_width = '';
		//debug::add('size',$size);
		//debug::p($w);

		// si un es false em serveix per calcular la proporcio i prou
		if ( $max_h === false ) {
			$ret           = 'width="' . $max_w . '"';
			$result_width  = $max_w;
			$result_height = $h * $result_width / $w;
			$ret .= ' height="' . ceil( $result_height ) . '"';

			return $ret . $src . $alt;
		}

		// si un es false em serveix per calcular la proporcio i prou
		if ( $max_w === false ) {
			$ret           = 'height="' . $max_h . '"';
			$result_height = $max_h;
			$result_width  = $w * $result_height / $h;
			$ret .= ' width="' . ceil( $result_width ) . '"';

			return $ret . $src . $alt;
		}

		// si més ample ( hor ) i proporcio més petita
		if ( $w >= $h && ( $w / $h > $max_w / $max_h ) ) {
			if ( $w > $max_w ) {
				$ret           = 'width="' . $max_w . '"';
				$result_width  = $max_w;
				$result_height = $h * $result_width / $w;
				$ret .= ' height="' . ceil( $result_height ) . '"';
			} else {
				$ret           = 'width="' . $w . '"';
				$result_width  = $w;
				$result_height = $h;
				$ret .= ' height="' . ceil( $result_height ) . '"';
			}
		} else {
			if ( $h > $max_h ) {
				$ret           = 'height="' . $max_h . '"';
				$result_height = $max_h;
				$result_width  = $w * $result_height / $h;
				$ret .= ' width="' . ceil( $result_width ) . '"';
			} else {
				$ret           = 'height="' . $h . '"';
				$result_width  = $w;
				$result_height = $h;
				$ret .= ' width="' . ceil( $result_width ) . '"';
			}
		}


		if ( $center_vertical &&  $result_height != $max_h && !$responsive) {
			$ret .= ' style="padding-top:' . ceil( ( $max_h - $result_height ) / 2 ) . 'px"';
		}

		$padding = '';
		// Tot en % de l'ample de la imatge
		if ( $responsive ) {
			if ( $max_h != $result_height ) {

				if ( $center_vertical ) {
					$padding_height         = ceil( ( $max_h - $result_height ) / 2 );
					$padding_height_percent = round( $padding_height * 100 / $max_w, 2 );
					$padding .= 'padding-top:' . $padding_height_percent . '%;padding-bottom:' . $padding_height_percent . '%;';
				} else {
					$padding_height         = $max_h - $result_height;
					$padding_height_percent = round( $padding_height * 100 / $max_w, 2 );
					$padding .= 'padding-bottom:' . $padding_height_percent . '%;';
				}
			}
			if ( $max_w != $result_width ) {
				$padding_width         = ceil( ( $max_w - $result_width ) / 2 );
				$padding_width_percent = round( $padding_width * 100 / $max_w, 2 );
				$padding .= 'padding-left:' . $padding_width_percent . '%;padding-right:' . $padding_width_percent . '%;';
			}
			if ( $padding ) {
				$ret .= ' style="' . $padding . '"';
			}
		}

		return $ret . $src . $alt;
	}
	/**
	 *
	 * @access public
	 *
	 * @param $file
	 * @param $alt
	 * @param double $ratio Rati de la imatge, s'ha de posar el mateix que al padding-bottom del contenidor
	 * @param bool $is_contain Si es true, encaixa la imatge de manera que quedi totalment visible, fer servir la funció get_image_contain();
	 *
	 * @return string Encaixa la imatge en una proporció concreta cobrint-la totalment i Retorna img amb les mides originals i la clase fit-width o fit-height per poder fer que la imatge cobreixi tota la capa
	 *
	 *
	 * <p><br><br><strong>S'ha de posar només al figure concret:</strong></p>
	 *   <pre>
	 * figure {
	 *    font-size: 0;
	 *    line-height: 0;
	 *    overflow: hidden;
	 *    padding-bottom: 66.67%;
	 *    position: relative;
	 * }
	 *
	 * </pre>
	 *  <strong>Aquestes ja estan a general.scss:</strong>
	 * <pre>
	 * .fit-width {
	 *    width: 100% !important;
	 *    height: auto !important;
	 *    position: absolute !important;
	 *    z-index: 1;
	 * }
	 *
	 * .fit-height {
	 *    width: auto !important;
	 *    height: 100% !important;
	 *    position: absolute !important;
	 *    z-index: 1;
	 *    }
	 *
	 * </pre>
	 *  <strong>I si es vol centrar quan es contain ( els padding han d'anar al parent ):</strong>
	 * <pre>
	 * .fit-width {
	 *    top: 50%;
	 *    transform: translateY(-50%);
	 * }
	 * .fit-width {
	 *    left: 50%;
	 *    transform: translateX(-50%);
	 * }
	 * </pre>
	 *
	 */
	static public function get_image_cover( $file, $alt, $height_percent, $is_contain = false ) {

		$size = false;
	    $path = DOCUMENT_ROOT;

		// per evitar errors en local que no tinc les imatges
		// si tinc domini no comprovo si es file ja que no funciona per http
		if ( is_file( $path . $file ) ) {
			$size = getimagesize( $path . $file );
		}

		if ( ! $size ) {
			return ' src=""';
		}

		$w            = $size[0];
		$h            = $size[1];

		$real_height_precent = $h/$w*100;

		$alt          = htmlspecialchars( $alt );

		$class =$height_percent > $real_height_precent? 'fit-height' : 'fit-width';

		if ($is_contain){
			$class =$real_height_precent > $height_percent? 'fit-height' : 'fit-width';
		}
		else {
			$class =$height_percent > $real_height_precent? 'fit-height' : 'fit-width';
		}

		return  "<img width='$w' height='$h' class='$class' src='$file' alt='$alt'>";
	}
}