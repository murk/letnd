<?php

use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\PHPMailer;

// detecte el mobil automaticament
function detect_mobile() {
	
	global $gl_site_part, $gl_auto_detect_mobile;
	
	$is_mobile = false;
	$auto_detect = $gl_auto_detect_mobile = Db::get_first("SELECT value FROM all__config" . $gl_site_part . " WHERE name = 'auto_detect_mobile'");
	
	//if ($auto_detect){

		// variable per forçar veure la web mobil o la desktop
		$viewsite = R::text_id('viewsite');

		// si hi ha el get no deixo indexar ja que seria una copia de la pagina sense el get
		if ($viewsite !== false){
			unset($_GET['viewsite']);
			header("X-Robots-Tag: noindex", true);
		}

		if (!$viewsite) $viewsite = R::text_id('viewsite',false,'_SESSION');

		if ($viewsite !== false){

			if ($viewsite=='mobile') {
				$is_mobile = true;
			}
			$_SESSION['viewsite'] = $viewsite;

		}
		else{		

			include (DOCUMENT_ROOT . 'common/includes/mobile-detect/Mobile_Detect.php');
			$detect = new Mobile_Detect; 
			// Any mobile device (phones not tablets).
			if( $detect->isMobile() && !$detect->isTablet() ){
				$is_mobile = true;
			}		
		}
		
		if ($is_mobile){			
			header("Vary: User-Agent");
		}
	//}
	return $is_mobile;	
}


/**
 * Per obtindre la configuració publica desde admin, per exemple al fer una plantilla de newsletter amb idioma
 *
 * @access public
 * @return array de configuració
 */
 
 //////////////////   nomès està a newsletter - s'ha de substituir per l'altra get_config_array('public', $tool);  //////////////////
function get_config_values_public($language)
{
    $table_lang = 'all__configpublic_language';

    if (is_table($table_lang))
    {
        $query = "SELECT all__configpublic.name,
		all__configpublic.value as value,  " . $table_lang . ".value as valuelang
		FROM `all__configpublic`
		LEFT OUTER JOIN  " . $table_lang . "
		USING (name)
		WHERE language = '" . $language . "' OR isnull(language)";
    } else
    {
        $query = "SELECT name, value
				FROM all__configpublic";
    }
    $results = Db::get_rows($query);
    foreach ($results as $rs)
    {
        if ($rs['name'] != 'language')
        {
            // revisar, treure les constants, va millor amb un array
			$valuelang = isset($rs['valuelang'])?$rs['valuelang']:'';		
			$value = $valuelang?$valuelang:$rs['value']; // te preferencia l'idioma
            $config[$rs['name']] = $value;
        }
    }
    return $config;
}

/**
ALL__CONFIG
get_config_values i get_config_array son funcions iguals a les de Module:get_config_array,
però les crido abans de carregar el mòdul, per això han d'estar duplicades
 */
function get_config_values()
{
	global $gl_config, $gl_language, $gl_is_mobile, $gl_auto_detect_mobile;
	$tool = 'all';
	$config = &$gl_config;
	$client_config_table =  "client__" . substr($_SESSION['client_dir'], strpos($_SESSION['client_dir'], "/") + 1);

	$cg['admin'] = get_config_array('admin', $tool);
	$cg['public'] = get_config_array('public', $tool);
	

	$site_part = $GLOBALS['gl_site_part'];
	$other_part = $site_part=='public'?'admin':'public';

	// sobreescric amb preferencia de la part on som
	// gl_congig es buit aquí, li donc preferència així puc definir valors desde php que tinguin preferència sobre la bbdd ( clubmoto, clubmotoshop ) 
	$config = array_merge($cg[$other_part], $cg[$site_part], $config);
	
	
	// sobresscric per mobil ( gl_config el primer, aqui va sobresscrit )
	if ($gl_is_mobile && $gl_auto_detect_mobile){
		$cg['admin_m'] = get_config_array('admin_m', $tool);
		$cg['public_m'] = get_config_array('public_m', $tool);
		$config = array_merge($config, $cg[$other_part.'_m'], $cg[$site_part.'_m']);
	}

	$cg['client'] = get_config_array('public', $client_config_table);
	if ($cg['client']){
		$config = array_merge($config, $cg['client']);
	}

	//Debug::add('Config values',$tool, 2);
	//Debug::add('Admin', $cg['admin']);
	//Debug::add('Public', $cg['public']);
	//Debug::add('Config ' . $site_part, $config);

	foreach ($config as $key => $val)
	{
		if (!defined(strtoupper($key)))
			define(strtoupper($key), $val);
	}
}
function get_config_array($site_part, $tool){
	$table = $tool . '__config' . $site_part;
	$config = array();
	if (!is_table($table)) return $config;
	$table_lang = $tool . '__config' . $site_part . '_language';

	if (is_table($table_lang))
	{
		$query = "SELECT  " . $table . ".name,
	" . $table . ".value as value,  " . $table_lang . ".value as valuelang
	FROM " . $table . "
	LEFT OUTER JOIN  " . $table_lang . "
	USING (name)
	WHERE language = '" . $GLOBALS['gl_language'] . "' OR isnull(language)";
	} else
	{
		$query = "SELECT name, value
			FROM " . $table;
	}
	$results = Db::get_rows($query);
	foreach ($results as $rs)
	{
		// revisar, treure les constants, va millor amb un array
		$valuelang = isset($rs['valuelang'])?$rs['valuelang']:'';
		$value = $valuelang?$valuelang:$rs['value']; // te preferencia l'idioma
		$config[$rs['name']] = $value;
	}
	return $config;
}
function get_language()
{
    global $gl_site_part, $gl_languages, $gl_default_language;
    
    $languages = $gl_languages[$gl_site_part];
    // l'idioma miro que no es passi per get, post o cookie abans d'agafar el de el navegador o la configuració que es el per defecte
    // revisar
    // mentre no hi hagi un apartat de preferencies per posar un idioma com a preferit, sempre que es canvii per get o post el poso a la cookie
    // i quan el detecta automaticament o per defecte també el poso en cookie per estalviar recursos
	$lang = isset($_POST['language'])?$_POST['language']:false; // si no hi ha ni get ni post agafa la cookie, així es pot cambiar d'idioma i quan es torna a la pàgina es torna amb el mateix idioma
	$lang = isset($_COOKIE['language'])?$_COOKIE['language']:$lang; // 2on en preferencia es el post
	$lang = isset($_GET['language'])?$_GET['language']:$lang; // així té preferencia el get

    if (in_array($lang, $languages))
    {
        setcookie('language_' . $gl_site_part, $_GET['language']);
        return R::lang($lang);
    } elseif ($lang && ($gl_site_part=='admin') && in_array($lang, $gl_languages['public']) ) // si es admin també miro a idiomes de public
	{
        return R::lang($lang);
	}
	// s'ha trobat idioma al get, però idioma no està actiu, s'ha de fer error 404
	elseif ($lang && isset($_GET['language']))
    {
		Main::error_404();
    }
	else
    {
        return R::lang($gl_default_language);
    }
}

/**
 * @return string
 */
function get_default_language(){
	global $gl_site_part, $gl_languages, $gl_default_language;
	$language_in_url = false;
	$host_url = substr(HOST_URL,0,-1);

	// OBTINC IDIOMES
	// necessari per l'update 6027
	$url = is_field('all__language','url')?'':" '' as";
	$query = "SELECT language, code, code2, admin, public,".$url." url FROM all__language WHERE admin <> 0 or public <> 0 ORDER BY ordre";

    $results = Db::get_rows($query);

    

    // si public = 1 -> l'idioma està online, es pot veure de la part pública
    // si public = 2 -> l'idioma no està online, vol dir que encara està en preparació, per tant ha de sortir a la part admin per poder introduir-los però no a public ja que es suposa que encara no hi han les descripcions traduides
    // si estem a admin, el llistat de idiomes publics son els que estan en preparacio + els que estan online
    // si estem a public, el llistat de idiomes publics son els que estan online
    foreach($results as $rs)
    {
        if ($rs['admin'] == '1')
        {
            $gl_languages['admin'][$rs['code2']] = $rs['code'];
        }
        if (($gl_site_part == 'admin') && ($rs['public'] != '0'))
        {
            $gl_languages['public'][$rs['code2']] = $rs['code'];
        } elseif ($rs['public'] == '1')
        {
            $gl_languages['public'][$rs['code2']] = $rs['code'];
        }
        $gl_languages['names'][$rs['code']] = $rs['language'];
	    $gl_languages['urls'][$rs['code']] = $rs['url'];

	    if ( $rs['url'] == $host_url) $language_in_url = $rs['code'];

        $gl_languages['codes2'][$rs['code']] = $rs['code2'];
    }
	$languages = $gl_languages[$gl_site_part];

	//Debug::p($language_in_url);
	
	// si he definit abans el default_language, el deixo com està, això serveix per definir un idioma en un domini com immosun.fr que l'idioma per defecte sigui frances, que no autodetecti
	if (isset($gl_default_language)) return $gl_default_language;
	
	// DETECTO IDIOMA DEFAULT
	 // primer saber si puc autodetectar l'idioma
	$query = "SELECT value
		FROM all__configpublic
		WHERE name = 'auto_detect_language'";
	$auto_detect_language = Db::get_first($query);
	
	$browser_lang = isset($_SERVER['HTTP_ACCEPT_LANGUAGE']) ? substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2) : '';

	
	// si l'idioma no el tenim donat d'alta a la BBDD

	// agafem l'idioma segons url encara que hi hagi autodetect
	// sino no hi ha mai home als dominis per els idiomes secundaris
	// TODO-i Segurament ve d'aquí l'error a les admin de easybrava
	if ($language_in_url){
		return $language_in_url;
	}
	// sino l'agafem del navegador
	elseif ($auto_detect_language && isset($languages[$browser_lang]))
	{
		//setcookie('language_' . $gl_site_part, $languages[$browser_lang]); // cookies no en faig servir de moment
		return $languages[$browser_lang];
	}
	// sino agafem l'idioma per defecte de la configuracio
	else
	{
		global $gl_site_part;
		$query = "
			SELECT value
			FROM all__config" . $gl_site_part . "
			WHERE name = 'language'";
		$result = Db::get_first($query);
		return $result;
	}
}
// sempre ha d'anar al final
function get_all_get_params($exclude_array = array(), $before='?', $after='', $is_javascript = false)
{
	
	// excloc sempre de propagar-se
	$exclude_array[] = 'gclid'; // Etiquetat automàtic del google per linkar adwords i google analytics
	
    $get_url = '';
	if ($is_javascript){
		$amp = '&';
		$chars = 1;
	}
	else{	
		$amp = '&amp;';
		$chars = 5;
	}
	if ($before=='&') $before = $amp;
	if ($after) $after = $amp;
	
    reset($_GET);

    foreach($_GET as $key => $value)
    {
        if (($key != session_name()) && ($key != 'error') && (!db_in_array($key, $exclude_array)))
        {
            if (is_array($value))
            {
                foreach ($value as $k => $v)
                {
                    $get_url .=   $key . '%5B' . $k . '%5D=' . $v . $amp;
                }
            } else
            {
                $get_url .= $key . '=' . urlencode($value) . $amp;
            }
        }
    }
	if ($get_url) {
		$get_url = substr($get_url, 0, -$chars);
		$get_url = $before . $get_url . $after;
	}
	elseif ($after) {
		$get_url = $before;
	}
	
    return $get_url;
}

function db_in_array($lookup_value, $lookup_array)
{
    if (function_exists('in_array'))
    {
        if (in_array($lookup_value, $lookup_array))
            return true;
    } else
    {
        reset($lookup_array);


        foreach($lookup_array as $key => $value)
        {
            if ($value == $lookup_value)
                return true;
        }
    }

    return false;
}

/**
 * Ordena un array estructurat com els arrays retornats per una BBDD
 *
 * Aquesta funció equival al order by del msql
 *
 * @param string $arr Array a ordenar
 * @param string $col Columna per la qual s'ordena
 * @param string $type Tipus ordre
 * @default SORT_NUMERIC
 *
 * SORT_REGULAR - Compare items normally
 *
 * SORT_NUMERIC - Compare items numerically
 *
 * SORT_STRING - Compare items as strings
 * @param string $order Orientació ordre
 * @default SORT_ASC
 *
 * SORT_ASC - Sort in ascending order
 *
 * SORT_DESC - Sort in descending order
 * @return array
 */
function array_sort_by_col($arr, $col, $type = SORT_NUMERIC, $order = SORT_ASC)
{
    foreach ($arr as $key => $row)
    {
        $order_arr[$key] = $row[$col];
    }
    array_multisort($order_arr, $order, $type, $arr);
    return $arr;
}
/**
 * Afegeix un element a un array, serveix per associatives
 *
 * @param array $array Array al cual insertar
 * @param array $insert_array Array per insertar
 * @param integer $position Lloc on inserta l'array, omitir per posar al començament
 */
function array_add (&$array, $insert_array, $position = 0) {
  //$first_array = array_splice ($array, 0, $position);
  $first_array = array();
  $array = array_merge ($first_array, $insert_array, $array);
} 

/**
 * Comproba si existeix una taula a la BBDD
 *
 * @param string $table taula a comprovar si existeix
 * @return bolean
 */
function is_table($table)
{
    // comprova si la taula existeix
	if (!$table) return false;
    $query = "show tables like '" . $table . "'";
    return Db::get_first($query)!=false;
}
function is_field($table, $field)
{
    // comprova si la columna de la taula existeix
	if (!is_table($table)) return false;
    $query = "Show columns from " . $table . " like '" . $field . "'";
    return Db::get_first($query)!=false;
}
// retalla a x caracters sempre amb paraules senceres
// si passa dels caràcters extreu l'html
// si extreu html i es un camp html torna a posar el paragraf ( així sempre quadra el diseny
// si no passa caracters, no fa res ( segueix deixant l'html
function add_dots($name, $value, $length = false)
{
    global $gl_tool;
	if ($length){
		// agafo el length del parametre
	}
	elseif (defined(strtoupper($gl_tool) . '_ADD_DOTS_LENGTH')){
		$length = constant(strtoupper($gl_tool) . '_ADD_DOTS_LENGTH');
	}
	else
	{
		$length = ADD_DOTS_LENGTH;
	}
    if (strlen($value) > $length)
    {
        // si es un camp html li torno a posar p
		// si al fer strip tags es diferent vol dir que era un camp html
		
		$new_value = strip_tags($value);
		$is_html = $new_value!=$value;
		$new_value = substr($new_value, 0, $length);
        $lastspace = strrpos($new_value, " ");
        if ($is_html) $new_value = substr($new_value, 0, $lastspace) . "&nbsp;...";
        else $new_value = substr($new_value, 0, $lastspace) . "...";
		if ($is_html) $new_value = '<p>'.$new_value.'</p>';
		$value = $new_value;
    }
    return $value;
}
// retalla a x caracters sempre amb paraules senceres
// si passa dels caràcters extreu l'html
// si extreu html i es un camp html torna a posar el paragraf ( així sempre quadra el diseny
// si no passa caracters, no fa res ( segueix deixant l'html
function add_dots_meta_description($value, $length=150)
{
	$value = strip_tags($value);
    if (strlen($value) > $length)
    {
		$value = substr($value, 0, $length);
        $lastspace = strrpos($value, " ");
        $value = substr($value, 0, $lastspace) . " ...";
    }
    return $value;
}
// retalla als caracters exactes sense tindre en compte paraules
// ja no poso la variable $name ja que no ja no utilitzo mai $listing->call_function
function add_dots_char($value, $length){
    return substr($value, 0, $length) . "...";
}

function html_to_text($str)
{
    $str = strip_tags(strtr($str, array_flip(get_html_translation_table(HTML_ENTITIES))));
    $str = trim($str);
    // treure el non-breaking space, revisar si s'han d'incloure més caracters raros
    $str = trim($str, chr(160));
    return $str;
}
function word_count($str)
{
	return  str_word_count ($str, 0); // millor fer sevir la funcio php, suposo que a PHP5 ja hauran arreglat el que fallava
	
	/*
    $words = 0;
    $str = eregi_replace("\n|\r|\t", " ", $str); // cambio salts de linea i tabulador per espai
    $str = eregi_replace(" +", " ", $str); // trec mes d'un espai seguit espais
    $array = explode(" ", $str);
    // comproba que almenys un caracter sigui una lletra i no una coma
    for ($i = 0; $i < count($array); $i++)
    {
        if (eregi("[0-9A-Za-zÀ-ÖØ-öø-ÿ]", $array[$i]))
            $words++;
    }
    return $words;
    // return preg_match_all("#(\w+)#", $str, $match_dummy);
	*/
}
function change_tool($tool, $tool_section, $action = false, $tool_section_id = false)
{
    global ${'gl_caption_' . $tool_section}, $gl_action;
    if ($action)
        $gl_action = $action;
    if ($tool_section_id)
        $_GET[$tool_section . '_id'] = $tool_section_id;

	Module::get_config_values($tool,$tool_section);
    set_tool($tool, $tool_section, $tool . '__' . $tool_section, $tool_section . '_id');
    set_config($tool . '/' . $tool . '_' . $tool_section . '_config.php');
    merge_caption($tool_section);
    if (!isset(${'gl_caption_' . $tool_section}))
    {
        set_language_vars($tool, $tool_section, $tool_section);
    }
    UploadFiles::initialize($GLOBALS['gl_module']);
}

function set_tool($tool, $tool_section, $table, $id_field)
{
    global $gl_tool_section, $gl_db_classes_table, $gl_db_classes_id_field, $gl_tool, $gl_section_base_name, $gl_section_base_public;

	$gl_section_base_name = DOCUMENT_ROOT . 'admin/modules/' . $gl_tool . '/' . $gl_tool . '_' . $gl_tool_section;
	$gl_section_base_name_public = DOCUMENT_ROOT . 'public/modules/' . $gl_tool . '/' . $gl_tool . '_' . $gl_tool_section;

    $gl_tool = $tool;
    $gl_tool_section = $tool_section;
    $gl_db_classes_table = $table;
    /*
	no seperque hauria d'anar el module
	
	$gl_tool = $gl_module->tool = $tool;
    $gl_tool_section = $gl_module->tool_section = $tool_section;
    $gl_db_classes_table = $gl_module->table = $table;*/
    $gl_db_classes_id_field = $id_field;

}
function set_table($table, $field)
{
    global $gl_db_classes_table, $gl_db_classes_id_field, $gl_table_overrided;
    $gl_db_classes_table = $table;
    $gl_db_classes_id_field = $field;
    $gl_table_overrided = true;
}
function set_language_vars($tool, $tool_section, $tool_section2)
{
    global ${'gl_caption_' . $tool_section}, ${'gl_caption_' . $tool_section2}, ${'gl_messages_' . $tool_section}, ${'gl_messages_' . $tool_section2}, $gl_caption, $gl_messages, $gl_language_vars_overrided,
        $gl_tool, $gl_language;
    include_once (DOCUMENT_ROOT . 'admin/modules/' . $gl_tool . '/languages/' . $gl_language . '.php');
    if ($gl_tool != $tool)
    {
        include_once (DOCUMENT_ROOT . 'admin/modules/' . $tool . '/languages/' . $gl_language . '.php');
    }
    if (isset(${'gl_caption_' . $tool_section}))
        $gl_caption = array_merge($gl_caption, ${'gl_caption_' . $tool_section});
    if (isset(${'gl_messages_' . $tool_section}))
        $gl_messages = array_merge($gl_messages, ${'gl_messages_' . $tool_section});
    if (isset(${'gl_caption_' . $tool_section2}))
        $gl_caption = array_merge($gl_caption, ${'gl_caption_' . $tool_section2});
    if (isset(${'gl_messages_' . $tool_section2}))
        $gl_messages = array_merge($gl_messages, ${'gl_messages_' . $tool_section2});
    $gl_language_vars_overrided = true;
}
function set_config($config_file)
{
    global $gl_db_classes_language_fields, $gl_db_classes_fields, $gl_db_classes_list, $gl_db_classes_form, $gl_config_overrided, $gl_db_classes_uploads, $gl_caption, $gl_language;
    $gl_db_classes_language_fields = $gl_db_classes_fields = $gl_db_classes_list = $gl_db_classes_form = $gl_db_classes_uploads = array();
    include (DOCUMENT_ROOT . 'admin/modules/' . $config_file);
    $gl_config_overrided = true;
}

function set_config_var( $var, $val, $table = 'all__configadmin', $create_if_empty = false ) {

	$val = Db::qstr( $val );
	$query = "UPDATE $table SET `value` = $val WHERE `name` = '$var'";
	Db::execute( $query );

	if ( $create_if_empty && Db::get_affected_rows() == 0 ) {
		if (!Db::get_first("SELECT count(*) FROM $table WHERE `name` = '$var'")) {
			$query = "INSERT INTO $table (`name`, `value`) VALUES ('$var', $val)";
			Db::execute( $query );
		}
	}
}

function get_config_var( $var, $table = 'all__configadmin' ) {
	$query = "SELECT `value` FROM $table WHERE `name` = '$var'";
	return Db::get_first( $query );
}

function delete_config_var( $var, $table = 'all__configadmin' ) {
	$query = "DELETE FROM $table WHERE `name` = '$var'";
	Db::execute( $query );
}

function merge_caption($sub_tool_section)
{
	global ${'gl_caption_' . $sub_tool_section}, ${'gl_messages_' . $sub_tool_section}, $gl_caption, $gl_messages;
    if (isset(${'gl_caption_' . $sub_tool_section}))
        $gl_caption = array_merge($gl_caption, ${'gl_caption_' . $sub_tool_section});
    if (isset(${'gl_messages_' . $sub_tool_section}))
        $gl_messages = array_merge($gl_messages, ${'gl_messages_' . $sub_tool_section});
}
function set_fields_to_text()
{
    global $gl_db_classes_fields;
    $v = &$gl_db_classes_fields;
    foreach ($v as $key => $value)
    {
        $v[$key]['form_admin'] == 'input'?$v[$key]['form_admin'] = 'text':false;
        $v[$key]['form_public'] == 'input'?$v[$key]['form_public'] = 'text':false;
        $v[$key]['list_admin'] == 'input'?$v[$key]['list_admin'] = 'text':false;
        $v[$key]['list_public'] == 'input'?$v[$key]['list_public'] = 'text':false;
    }
}

function print_javascript($javascript, $return = false)
{
    $ret = '<script type="text/javascript" >
	   ' . $javascript . '
	   </script>';
    if ($return)
        return $ret;
    echo $ret;
}
function get_javascript($javascript)
{
    return print_javascript($javascript, true);
}


// inserta per javascript un troç html en un id del document
function set_inner_html($id, $val, $add=false, $target = 'top')
{
    include_once (DOCUMENT_ROOT . 'common/includes/json/JSON.php');
    $json = new Services_JSON();
	if($add){			
		$javascript = $target . ".$('#" . $id . "').html( ". $target . ".$('#" . $id . "').html() +" . $json->encode($val) . ");";
	}		
	else{			
		$javascript = "". $target . ".$('#" . $id . "').html(" . $json->encode($val) . ");";
	}
	
    print_javascript($javascript);
}

function redirect($url)
{
    echo ('<script type="text/javascript"><!--
	   top.window.location.replace(\'' . $url . '\')
	   //--></script>');
    die();
}
// fa un implode dels valors de un camp d'un array tipus results
function implode_field(&$results, $field = 0, $glue = ',')
{
    if (!$results)
        return '';
    $arr = array();
    foreach ($results as $key)
    {
        $arr[$key[$field]] = $key[$field];
    }
    return implode($glue, $arr);
}
function set_field($field, $key, $value)
{
    global $gl_db_classes_fields;
    $gl_db_classes_fields[$field][$key] = $value;
}
// envia mail
function get_mailer($cf='', $exceptions =true )
{
    if (!$cf){
	// envia desde el conte den Marc
		global $configuration;
		$cf = $configuration;
	}
	else{
	// si deixo servidor en blanc no envia amb SMTP
		if ($cf['m_host']){
			$cf['m_mailer'] = "smtp";
			$cf['m_SMTPAuth'] = true;
		}
		else{
			$cf['m_mailer'] = "mail";
			$cf['m_SMTPAuth'] = false;
		
		}
	}
    $mail = new PHPMailer($exceptions);

	$mail->CharSet = 'UTF-8';
	// $mail->SMTPDebug = 1;
	$mail->setLanguage( LANGUAGE_CODE );
    $mail->Mailer = $cf['m_mailer'];
    $mail->Host = $cf['m_host'];
    $mail->SMTPAuth = $cf['m_SMTPAuth'];
    $mail->Username = $cf['m_username'];
    $mail->Password = $cf['m_password'];
    $mail->From = $cf['from_address'];
    if (isset($cf['from_name'])) $mail->FromName = $cf['from_name'];
    $mail->Timeout = 30;
	
	//Debug::p($cf, 'Config');
	//if (DEBUG) $mail->SMTPDebug = true;
	
    return $mail;
}
function send_mail(&$mail, $peu = true, $send_mail = true)
{
    if ($peu)
    {
        $mail->Body .= "<br /><br /><br />
		<DIV><FONT face=Arial size=2>Att.&nbsp;9 Sistema.</FONT></DIV><br /><br />
		<DIV><FONT face=Arial size=1>9 Sistema Serveis Informàtics 2005
		S.L.</FONT></DIV>
		<DIV><FONT face=Arial size=1>C/ Juli Garreta 25C 2on a(17002) Girona</FONT></DIV>
		<DIV><FONT face=Arial size=1>Telf. 777 888 999 / 665 731 656 / 661 333
		289</FONT></DIV>";
    }

	try {

		$sent = $mail->Send();
		return $sent;

	} catch ( Exception  $e ) {

		if ( $GLOBALS['gl_is_letnd'] && $send_mail ) {

			$error = $e->errorMessage() . "\n\n" . Debug::get_error_backtrace() . print_r($_REQUEST, true);

			send_mail_admintotal( 'Error enviament mail phpmailerException: ' . HOST_URL, $error );

		}

		Debug::p( 'SMTP error: ', $e->errorMessage() );
		return false;

	} catch ( \Exception $e ) {
		if ( $GLOBALS['gl_is_letnd'] && $send_mail ){

			$error = $e->errorMessage() . "\n\n" . Debug::get_error_backtrace() . print_r($_REQUEST, true);
			send_mail_admintotal( 'Error enviament mail exception: ' . HOST_URL, $error );
		}

		Debug::p( 'SMTP error: ', $e->getMessage() );
		return false;
	}
	/*
    $exito = $mail->Send();
	
    $intentos = 1;
    while ((!$exito) && ($intentos < 2))
    {
    	echo "Error sending: " . $mail->ErrorInfo;
        sleep(0.5);
        $exito = $mail->Send();
        $intentos = $intentos + 1;
    }
	
	if (!$exito && $GLOBALS['gl_is_letnd']) send_mail_admintotal('Error enviament mail: ' . HOST_URL,$mail->ErrorInfo);
	
    return $exito;*/
}
// envia mail al admin de la web
function send_mail_admin($subject,$body){
	global $configuration;
	$mail = get_mailer();
	$mail->AddAddress(DEFAULT_MAIL);
	$mail->Subject = $subject;
	$mail->Body = $body;
    $mail->isHtml(true);
	return send_mail($mail, false);
}
// envia mail al admintotal
function send_mail_admintotal($subject,$body = ' ---- ', $address = false){

	// Si no hi ha body dona error

	global $configuration;
	$mail = get_mailer();
	$mail->AddAddress($address ?: 'dev@letnd.com');
	$mail->Subject = $subject;
	$mail->Body = $body;
    $mail->isHtml(true);
	return send_mail($mail, false, false);
}
function now($time = false, $days_off = 0)
{
    $format = $time?"d/m/Y G:i":"d/m/Y";
	if ($days_off!=0){
		$symbol = $days_off>0?'+':'-';
		$date = strtotime($symbol . abs($days_off) . " day");
		return date($format,$date);
	}
	else{
		return date($format);
	}
}

function get_password ($length = 8)
{
	$password = "";
	// define possible characters
	$possible = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

	$i = 0;
	while ($i < $length) {

		// pick a random character from the possible ones
		$char = substr($possible, mt_rand(0, strlen($possible)-1), 1);
		
		$password .= $char;
		$i++;

	}

	// done!
	return $password;

}

function encode_email($e)
{
  $output='';
  for ($i = 0; $i < strlen($e); $i++) { $output .= '&#'.ord($e[$i]).';'; }
  return $output;
}

function get_url_html($url) {
	$timeout = 5;

	$userAgent = 'Letnd (compatible; MSIE 6.0; Windows NT 5.1; .NET CLR 1.1.4322)';
	$header    = array( 'Accept-Charset: UTF-8' );

	$maxRedirection = 3;
	do {
		$ch = curl_init();
		curl_setopt( $ch, CURLOPT_USERAGENT, $userAgent );
		curl_setopt( $ch, CURLOPT_HTTPHEADER, $header );
		curl_setopt( $ch, CURLOPT_URL, $url );
		curl_setopt( $ch, CURLOPT_HEADER, false );
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );

		// curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, true ); // segueix redirects
		curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, false ); // que no segueix redirects, l'agafo del curl_getinfo
		curl_setopt( $ch, CURLOPT_CONNECTTIMEOUT, $timeout );

		$html = curl_exec( $ch );

		if(DEBUG && curl_error($ch))
		{
		    echo 'error:' . curl_error($ch);
		}

		$code = curl_getinfo( $ch, CURLINFO_HTTP_CODE );

		if ($maxRedirection<1) $code = 0;

		// els 3 primers caracters poden portar BOM
		if ( substr( $html, 0, 3 ) == pack( 'CCC', 239, 187, 191 ) ) {
			$html = mb_substr( $html, 3 );
		}

		// Si hi ha un redirect, agafo html de l'adreça de redirect
		// echo( $url . "<br>" );//echo( $code . "<br>" );
		if ( $code == 301 || $code == 302 || $code == 303 || $code == 307 ) {

			// faig un altre request nomes dels headers per poder saber el pròxim enllaç - AMb el curl_getinfo no va quan safe_mode o open_dir
			curl_setopt( $ch, CURLOPT_HEADER, true );
			curl_setopt( $ch, CURLOPT_NOBODY, true );
			$headers = curl_exec( $ch );
			preg_match( '/Location:(.*?)\n/', $headers, $matches );

			$url = HOST_URL . substr( trim( array_pop( $matches ) ), 1);
			// $url = curl_getinfo( $ch );
			// print_r( $url );

			$maxRedirection --;
		} else {
			$code = 0;
		}

		curl_close( $ch );

	} while ( $code );

	return $html;
}

function nl2p($string)
{
	$paragraphs = '';

	foreach (explode("\n", $string) as $line)
	{
		if (trim($line))
		{
			$paragraphs .= '<p>' . $line . '</p>';
		}
	}

	return $paragraphs;
}

function p2nl($string)
{
	$text = '';
	
	$string = str_replace('<p>&nbsp;</p>', "<p></p>", $string); // trec els NBSP del p, l'editor html que fa: <p>&nbsp</p>
	$string = html_entity_decode($string,  ENT_COMPAT, 'UTF-8');
	
	// guardo brs per reemplaçar despres per un salt
	$string = str_replace('<br>', "{{br}}", $string);
	$string = str_replace('<br/>', "{{br}}", $string);
	$string = str_replace('<br />', "{{br}}", $string);
	
	// canvio p per 2 salts
	foreach (mb_split("</p>", $string) as $line)
	{	
		$text .= rip_tags($line) . "\n\n";
	}
	
	// canvio br per 1 salt
	$text = str_replace("{{br}}", "\n", $text);
	
	return trim($text);
}
// 1 - Deixa espai entre paraules:
//  
//				php strip_tags ho fa mal -> <p>color is blue</p><p>size</p> -> color is bluesize
//				
//				DESCONECTO, nomes ho hauria de fer amb elements amb dispplay block
//											<p>color is <stong>b</strong>lue</p> -> color is blue -> i no color is blu e
//				ES SUOPOSA que l'editor html ja formateja be els blocks p en diferents linees, per tant no fa falta això
//											 <p>color is blue</p>
//		
//														 <p>size</p>
//				
// 2 - Canvia salt de línea per espai 
//				php strip_tags ho fa mal -> <p>color is blue</p>
//											<p>size</p>				
//											-> 
//											color is blue
//											size
function rip_tags($string) {
   
    // ----- remove HTML TAGs -----
    $string = preg_replace ('/<[^>]*>/', '', $string);
    //$string = preg_replace ('/<[^>]*>/', ' ', $string);  cas 1
   
    // ----- remove control characters -----
    $string = str_replace("\r", ' ', $string);    // --- replace with space
    $string = str_replace("\n", ' ', $string);   // --- replace with space
    $string = str_replace("\t", ' ', $string);   // --- replace with space


	// Clean up things like &amp;
	$string = html_entity_decode($string, ENT_COMPAT, 'UTF-8');
   
    // ----- remove multiple spaces -----
    $string = trim(preg_replace('/ {2,}/', ' ', $string));


   
    return $string;

}

// si la primera es mes gran que la segona retorna true
// enviar format bbdd 2014/06/26
function is_date_bigger($date1,$date2, $unformat = false) {

	if ( $unformat ) {
		$date1 = unformat_date($date1);
		$date2 = unformat_date($date2);
	}

	if (substr($date1, 0, 1)=="'")
		$date1 = substr($date1, 1, -1);
	
	if (substr($date2, 0, 1)=="'")
		$date2 = substr($date2, 1, -1);

	$sep = strpos( $date1, '/' ) === false ? '-' : '/';
	$date = explode($sep,$date1);
	
	if (!$date[0]) return false;
	$date1 = mktime(0, 0, 0, $date[1], $date[2], $date[0]);
	
	$sep = strpos( $date2, '/' ) === false ? '-' : '/';
	$date = explode($sep,$date2);


	if (!$date[0]) return false;
	$date2 = mktime(0, 0, 0, $date[1], $date[2], $date[0]);	
	
	return $date1>$date2;
}

// si la primera data es troba entre les 2 següents, ambdues incloses
// enviar format bbdd 2014/06/26
function is_date_between($date_middle,$date1,$date2) {	
	
	if (substr($date_middle, 0, 1)=="'")
		$date1 = substr($date_middle, 1, -1);
	
	if (substr($date1, 0, 1)=="'")
		$date1 = substr($date1, 1, -1);
	
	if (substr($date2, 0, 1)=="'")
		$date2 = substr($date2, 1, -1);
	
	$date = explode('/',$date_middle);
	$date_middle = mktime(0, 0, 0, $date[1], $date[2], $date[0]);
	
	$date = explode('/',$date1);
	$date1 = mktime(0, 0, 0, $date[1], $date[2], $date[0]);
	
	$date = explode('/', $date2);
	$date2 = mktime(0, 0, 0, $date[1], $date[2], $date[0]);
	
	return ($date_middle>=$date1) && ($date_middle<=$date2);
}

// Retorna una data a partir d'un timestamp, en format bbdd
function time_to_date($time) {
	$format = "Y/m/d";
	return date($format,$time);
}

function check_google_recaptcha () {

	// use_recaptcha per defecte està a 1, però també haig de comprobar que s'hagi configurat el secret
	// use_recaptcha pot servir per formularis d'altres eines, per aixó no em baso només amb el secret per si vull desconectar-ho en alguna eina en concret
	if (USE_RECAPTCHA == '1' && RECAPTCHA_SECRET){
		$captcha_response = get_google_recaptcha_response();
		if ($captcha_response !== true) {

			$GLOBALS['gl_page']->show_message($captcha_response,false,false);
			Debug::p_all();
			die();
		}

	}

}
function get_google_recaptcha_response () {

	if ( $GLOBALS['gl_is_local'] ) {
		return true;
	}
	$recaptcha_secret = RECAPTCHA_SECRET;


	include_once( DOCUMENT_ROOT . 'common/includes/google_recaptcha/autoload.php' );
	$recaptcha = new \ReCaptcha\ReCaptcha($recaptcha_secret, new \ReCaptcha\RequestMethod\CurlPost());

	$resp = $recaptcha->verify($_POST['g-recaptcha-response'], $_SERVER['REMOTE_ADDR']);
	if ($resp->isSuccess()) {
		// verified!
		return true;
	} else {
		$error =  addslashes($resp->getErrorCodes());
		print_javascript("top.dp('$error')");
		return $GLOBALS['gl_caption']['c_recaptcha_error'];
		// Nomes fem servir un unic error
	}

}

// Mostro pàgina en construcció si no està en sessió o si és local
function show_under_construction ($name, $file='') {
	if (empty($_SESSION['user_id']) && !$GLOBALS['gl_is_local']){
		$include = $file ? '/' . $file : '/public/construccio.php';
		include( DOCUMENT_ROOT . $include );
		die();
	}
}
// emplenar fins a zero
function zerofill($number, $length = 2)
{
    $sprintf = '%0' . (int)$length . 's';

	return sprintf($sprintf, $number);
}


/**
 * Afegeix una imatge incrustada a l'email <br>
 * El path per defecte on posar les imatges és a /templates/newsletter/message_templates/images/
 *
 * @param $image
 * @param $preview
 * @param bool $is_repeated
 * @param string $path
 *
 * @return string
 */
function newsletter_get_inline_image ($image, $preview, $is_repeated = false, $path = '') {


    if ($preview){
        if (!$path) $path = '/' . CLIENT_DIR . '/templates/newsletter/message_templates/images/';
        return "$path/$image";
    }
    else {
        if (!$path) $path = CLIENT_PATH . '/templates/newsletter/message_templates/images/';
        else $path = DOCUMENT_ROOT . "/$path/";

        if (!$is_repeated) {
            $mail = &$GLOBALS['gl_current_mailer'];
            $mail->AddEmbeddedImage( $path . $image, $image, $image );
        }
        return 'cid:' . $image . '';
    }
}


function is_user_logged(){
	return isset($_SESSION['user_id']) && isset($_SESSION['group_id']) && $_SESSION['user_id'] != '';
}

function show_preview(){

	if (!is_user_logged()) return false;

	// Si està actiu el preview i loggejat
	if (R::get('preview')!==false)  {
		return true;
	}
	elseif (HAS_PREVIEW) {
		// Per local treiem el preview sempre posat per poder maquetar bé
		if ($GLOBALS['gl_is_local']) return false;
		return true;
	}
	else {
		return false;
	}

}

function json_end($php_obj = []) {
	header( 'Content-Type: application/json' );
	Debug::p_all();
	die ( json_encode( $php_obj ) );
}

// TODO-i Canviar tots els die()s per aquesta funció
function html_end($message = '') {
	Debug::p_all();
	die ( $message );
}

function snake_to_camel( $string, $capitalize_first = false ) {
	$string = str_replace( '_', '', ucwords( $string, '_' ) );
	if (!$capitalize_first) $string = lcfirst( $string );
	return $string;
}