<?

namespace Console\Command;

// use Nette\Security\Passwords;
use Console;
use Console\Util;
use Db;
use Query;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Style\SymfonyStyle;

// use Symfony\Component\Console\Question\Question;

final class Habitaclia extends Command {

	private $output;
	private $input;
	public $headers;

	protected function configure() {
		$this->setName( 'habitaclia' )
		     ->setDescription( 'Utilitats Habitaclia' )
		     ->addArgument( 'client', InputArgument::REQUIRED, "Client on realitzar les accions" )
		     ->addArgument( 'action', InputArgument::REQUIRED, "Accio a realitzar: 'user', 'activate', 'deactivate'" )
		     ->addArgument( 'arg1', InputArgument::OPTIONAL, "" )
		     ->addArgument( 'arg2', InputArgument::OPTIONAL, "" )
		     ->addArgument( 'arg3', InputArgument::OPTIONAL, "" )
		     ->addArgument( 'arg4', InputArgument::OPTIONAL, "" );
	}

	protected function execute( InputInterface $input, OutputInterface $output ) {

		Util::init_unirest( $this );

		$this->input  = $input;
		$this->output = $output;
		$io           = new SymfonyStyle( $input, $output );

		$actions = [ 'user', 'activate', 'deactivate' ];

		$client = $input->getArgument( 'client' );
		$action = $input->getArgument( 'action' );
		$arg1   = $input->getArgument( 'arg1' );
		$arg2   = $input->getArgument( 'arg2' );
		$arg3   = $input->getArgument( 'arg3' );
		$arg4   = $input->getArgument( 'arg4' );

		if ( ! in_array( $action, $actions ) ) {


			$helper   = $this->getHelper( 'question' );
			$question = new ChoiceQuestion(
				'Acció a realitzar?',
				$actions );

			$question->setErrorMessage( 'Accio %s no es valid.' );

			$action = $helper->ask( $input, $output, $question );


		}
		$action = str_replace( '-', '_', $action );

		$clients = Util::get_server_clients( $client );

		if ( ! isset( $clients['questions'] ) ) {
			$client_rs = $clients;
		}
		elseif ( empty( $clients['questions'] ) ) {
			$client_rs = $clients['clients'];
		}
		else {
			$helper   = $this->getHelper( 'question' );
			$question = new ChoiceQuestion(
				'Quin client?',
				$clients['questions'] );

			$question->setErrorMessage( 'El client no és valid' );

			$client_dir = $helper->ask( $input, $output, $question );
			$client_rs  = $clients['clients'][ $client_dir ];
		}

		//$output->writeln( "Acció: $action" );
		//$output->writeln( "Client: {$client_rs['client_dir']}" );
		if ( $client_rs ) {
			$io->title( "$action: {$client_rs['fiscalname']} - {$client_rs['domain']}" );
		}
		else {
			$io->title( "$action: $client ( no trobat online )" );
		}
		$this->$action( $action, $client, $client_rs, $io, ...[ $arg1, $arg2, $arg3, $arg4 ] );

		return 0;
	}

	private function user( $action, $client, $client_rs, SymfonyStyle $io, ...$args ) {

		if ( ! Util::is_client_on_server( $client_rs, $io ) ) return;

		Util::connect_mother();

		$value     = $args[0];
		$client_id = $client_rs['client_id'];
		$q         = new Query( "letnd.client__client" );

		if ( $value ) {
			$query = $q->prepare_update( [ 'inmo_habitaclia_user' => $value ], $client_id );

			$helper   = $this->getHelper( 'question' );
			$question = new ConfirmationQuestion( "Executar aquest query? $query", false );

			if ( ! $helper->ask( $this->input, $this->output, $question ) ) {
				$io->text( "Abortat!" );

				return;
			}
			$q->execute_update( $query );
			$io->text( "User actualitzat a: \"$value\" a letnd clients" );
		}
		else {
			$user = $client_rs['inmo_habitaclia_user'];
			$url = "https://letnd.com/clients/habitaclia/$user.xml";
			$io->text( "User: $user" );
			$io->text( $url );
		}

		Db::reconnect();
	}

	private function deactivate( $action, $client, $client_rs, SymfonyStyle $io, ...$args ) {

		if ( ! Util::is_client_on_server( $client_rs, $io ) ) return;

		Util::connect_mother();

		$client_id = $client_rs['client_id'];
		$q         = new Query( "letnd.client__client" );


		$query = $q->prepare_update( [
			'inmo_habitaclia' => 0
		], $client_id );

		$helper   = $this->getHelper( 'question' );
		$question = new ConfirmationQuestion( "Executar aquest query? $query", false );

		if ( ! $helper->ask( $this->input, $this->output, $question ) ) {
			$io->text( "Abortat!" );

			return;
		}
		$q->execute_update( $query );
		$io->text( "Deactivat habitaclia a letnd clients" );


		Db::reconnect();

	}

	private function activate( $action, $client, $client_rs, SymfonyStyle $io, ...$args ) {

		if ( ! Util::is_client_on_server( $client_rs, $io ) ) return;

		$io->text( "Usage: ativate habitaclia_user # directori del client on es genera l'xml \n\n" );

		Util::connect_mother();

		$user = isset( $args[0] ) ? $args[0] : false;

		$client_id = $client_rs['client_id'];
		$q         = new Query( "letnd.client__client" );

		$values = [
			'inmo_habitaclia'      => 1
		];

		if ( $user ) $values ['inmo_habitaclia_user'] = $user;

		$query = $q->prepare_update( $values, $client_id );

		$helper   = $this->getHelper( 'question' );
		$question = new ConfirmationQuestion( "Executar aquest query? $query", false );

		if ( ! $helper->ask( $this->input, $this->output, $question ) ) {
			$io->text( "Abortat!" );

			return;
		}
		$q->execute_update( $query );
		$io->text( "Activat habitaclia a letnd clients\n" );

		if (!$user) {
			$user = $client_rs['inmo_habitaclia_user'];
		}

		$url = "https://letnd.com/clients/habitaclia/$user.xml";
		$io->text( "User: $user" );
		$io->text( $url );


		Db::reconnect();
	}
}