
const LASSDIVE_PRINTING = "Imprimint";
const LASSDIVE_NUM_RESERVA = 'Número de reserva: ';
const LASSDIVE_PAGAT = 'Pagat';
const LASSDIVE_PAGA_SENYAL = 'Paga i senyal: ';
const LASSDIVE_PENDENT = 'Pendent: ';
const LASSDIVE_RESERVA_ITEM_DATE = 'Dia';
const LASSDIVE_HORARI = 'Horari';
const LASSDIVE_CENTRE = 'Centre';
const LASSDIVE_USER_IDS = 'Instructor';

const LASSDIVE_HORA_MES_GRAN = 'La segona hora seleccionada ha de ser més gran que la primera';
const LASSDIVE_NO_DISPONIBLE = 'No es pot seleccionar aquesta franja, no hi ha disponibilitat';
const LASSDIVE_HORA_DIFERENTS_CENTRES = 'No es pot seleccionar hores de diferents centres';

const LASSDIVE_NO_RESPONSE = ' L\'impresora no respon';
const LASSDIVE_PRINT_SUCCESS = ' Ticket imprès correctament';
const LASSDIVE_ASB_DRAWER_KICK = ' Status of the drawer kick number 3 connector pin= "H"';
const LASSDIVE_ASB_BATTERY_OFFLINE = ' Offline due to a weak battery';
const LASSDIVE_ASB_OFF_LINE = ' Offline status';
const LASSDIVE_ASB_COVER_OPEN = ' La coberta està oberta';
const LASSDIVE_ASB_PAPER_FEED = ' Paper feed switch is feeding paper';
const LASSDIVE_ASB_WAIT_ON_LINE = ' Waiting for online recovery';
const LASSDIVE_ASB_PANEL_SWITCH = ' Panel switch is ON';
const LASSDIVE_ASB_MECHANICAL_ERR = ' Mechanical error generated';
const LASSDIVE_ASB_AUTOCUTTER_ERR = ' Auto cutter error generated';
const LASSDIVE_ASB_UNRECOVER_ERR = ' Unrecoverable error generated';
const LASSDIVE_ASB_AUTORECOVER_ERR = ' Auto recovery error generated';
const LASSDIVE_ASB_RECEIPT_NEAR_END = ' No paper in the roll paper near end detector';
const LASSDIVE_ASB_RECEIPT_END = ' No paper in the roll paper end detector';
const LASSDIVE_ASB_BUZZER = ' Sounding the buzzer';
const LASSDIVE_ASB_WAIT_REMOVE_LABEL = ' Waiting to remove label';
const LASSDIVE_ASB_SPOOLER_IS_STOPPED = ' Stop the spooler';

const LASSDIVE_ESCOLLIR_CENTRE = 'Has d\'escollir un centre';
const LASSDIVE_ESCOLLIR_PRODUCT_CENTRE = 'En vista setmanal s\'ha d\'escollir producte i centre';

// TIPS
const LASSDIVE_RESERVA = 'Reserva';
const LASSDIVE_PAYED = 'Pagat';
const LASSDIVE_PENDING = 'Pendent';

const LASSDIVE_NO_DATE = 'Cap data';
const LASSDIVE_CONFIRMAR_ESBORRAR_HORARI = "S'esborrarà l'horari, centre i instructors assignats a aquesta activitat, segur que vols continuar?";
const LASSDIVE_NO_ESBORRAT_HORARI = "L'horari no s'ha pogut esborrar";
