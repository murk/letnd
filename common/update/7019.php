<?

/* 12-6-17
  Afegir pagament a booking

 */


if ( is_table( 'booking__book' ) && ! is_field( 'booking__book', 'order_status_1' ) ) {

	$update_sql[] = "ALTER TABLE `booking__book`
	DROP COLUMN `payment_method`;";

	$update_sql[] = "ALTER TABLE `booking__book`
	CHANGE COLUMN `status` `status` ENUM('pre_booked','booked','confirmed_email','confirmed_post','contract_signed','cancelled') NOT NULL DEFAULT 'booked' COLLATE 'utf8_spanish2_ci' AFTER `contract_id`;
";

	$update_sql[] = "ALTER TABLE `booking__book`
	ADD COLUMN `payment_method_4` ENUM('none','cash','paypal','account','directdebit','lacaixa') NOT NULL DEFAULT 'none' AFTER `status`,
	ADD COLUMN `order_status_4` ENUM('paying','failed','pending','denied','voided','refunded','completed') NOT NULL DEFAULT 'paying' AFTER `status`,
	ADD COLUMN `payment_method_3` ENUM('none','cash','paypal','account','directdebit','lacaixa') NOT NULL DEFAULT 'none' AFTER `status`,
	ADD COLUMN `order_status_3` ENUM('paying','failed','pending','denied','voided','refunded','completed') NOT NULL DEFAULT 'paying' AFTER `status`,
	ADD COLUMN `payment_method_2` ENUM('none','cash','paypal','account','directdebit','lacaixa') NOT NULL DEFAULT 'none' AFTER `status`,
	ADD COLUMN `order_status_2` ENUM('paying','failed','pending','denied','voided','refunded','completed') NOT NULL DEFAULT 'paying' AFTER `status`,
	ADD COLUMN `payment_method_1` ENUM('none','cash','paypal','account','directdebit','lacaixa') NOT NULL DEFAULT 'none' AFTER `status`,
	ADD COLUMN `order_status_1` ENUM('paying','failed','pending','denied','voided','refunded','completed') NOT NULL DEFAULT 'paying' AFTER `status`;";

	$update_sql[] = "UPDATE booking__book SET `order_status_1`='completed' WHERE  `payment_done_1`<>0.00;";
	$update_sql[] = "UPDATE booking__book SET `order_status_2`='completed' WHERE  `payment_done_2`<>0.00;";
	$update_sql[] = "UPDATE booking__book SET `order_status_3`='completed' WHERE  `payment_done_3`<>0.00;";
	$update_sql[] = "UPDATE booking__book SET `order_status_4`='completed' WHERE  `payment_done_4`<>0.00;";


	$update_sql[] = "ALTER TABLE `booking__book`
	DROP COLUMN `error_code`,
	DROP COLUMN `error_description`,
	DROP COLUMN `transaction_id`,
	DROP COLUMN `approval_code`;";


	$update_sql[] = "ALTER TABLE `booking__book`
	ADD COLUMN `approval_code_4` CHAR(17) NOT NULL AFTER `payment_method_4`,
	ADD COLUMN `transaction_id_4` CHAR(17) NOT NULL AFTER `payment_method_4`,
	ADD COLUMN `error_description_4` VARCHAR(255) NOT NULL AFTER `payment_method_4`,
	ADD COLUMN `error_code_4` VARCHAR(255) NOT NULL AFTER `payment_method_4`,
	ADD COLUMN `approval_code_3` CHAR(17) NOT NULL AFTER `payment_method_4`,
	ADD COLUMN `transaction_id_3` CHAR(17) NOT NULL AFTER `payment_method_4`,
	ADD COLUMN `error_description_3` VARCHAR(255) NOT NULL AFTER `payment_method_4`,
	ADD COLUMN `error_code_3` VARCHAR(255) NOT NULL AFTER `payment_method_4`,
	ADD COLUMN `approval_code_2` CHAR(17) NOT NULL AFTER `payment_method_4`,
	ADD COLUMN `transaction_id_2` CHAR(17) NOT NULL AFTER `payment_method_4`,
	ADD COLUMN `error_description_2` VARCHAR(255) NOT NULL AFTER `payment_method_4`,
	ADD COLUMN `error_code_2` VARCHAR(255) NOT NULL AFTER `payment_method_4`,
	ADD COLUMN `approval_code_1` CHAR(17) NOT NULL AFTER `payment_method_4`,
	ADD COLUMN `transaction_id_1` CHAR(17) NOT NULL AFTER `payment_method_4`,
	ADD COLUMN `error_description_1` VARCHAR(255) NOT NULL AFTER `payment_method_4`,
	ADD COLUMN `error_code_1` VARCHAR(255) NOT NULL AFTER `payment_method_4`;
	";

	$update_sql[] = "ALTER TABLE `booking__contract`
	ADD COLUMN `whole_import` TINYINT NULL DEFAULT '0' AFTER `enetered`;
	";
	$update_sql[] = "ALTER TABLE `booking__contract`
	CHANGE COLUMN `enetered` `entered` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `initial_percent`;
	";




} else {
	if ( is_table( 'booking__book' ) ) {
		trigger_error( 'Ja existeixen el camp error_code' );
	}
}

?>