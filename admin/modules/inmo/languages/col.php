<?php
global $gl_caption_tv, $gl_messages_tv;
// menus eina
define('INMO_MENU_PROPERTY', 'Inmuebles');
define('INMO_MENU_PROPERTY_NEW', 'Introducir nueva finca');
define('INMO_MENU_PROPERTY_LIST', 'Listar inmuebles');
define('INMO_MENU_SEARCH_FORM', 'Buscar y editar inmuebles');
define('INMO_MENU_PROPERTY_BIN', 'Papelera de reciclaje');
define('INMO_MENU_CATEGORY', 'Tipos de inmuebles');
define('INMO_MENU_CATEGORY_NEW', 'Introducir nuevo tipo');
define('INMO_MENU_CATEGORY_LIST', 'Listar tipos');
define('INMO_MENU_TRANSLATION', 'Traducciones');
define('INMO_MENU_PROPERTY_TRANSLATION_LIST', 'Inmuebles para traducir');
define('INMO_MENU_CATEGORY_TRANSLATION_LIST', 'Categorías para traducir');
define('INMO_MENU_CONFIG', 'Configuración inmuebles');
define('INMO_MENU_CONFIGADMIN_LIST', 'Configuración parte privada');
define('INMO_MENU_CONFIGPUBLIC_LIST', 'Configuración parte pública');
define('INMO_MENU_CONFIGADMIN', 'Configuración parte privada');
define('INMO_MENU_CONFIGPUBLIC', 'Configuración parte pública');
define('INMO_MENU_ZONE', 'Zonas');
define('INMO_MENU_ZONE_NEW', 'Introducir nueva zona');
define('INMO_MENU_ZONE_LIST', 'Listar zonas');
define('INMO_MENU_ZONE_BIN', 'Papelera de reciclaje');
define('INMO_MENU_TV', 'Escaparates virtuales');
define('INMO_MENU_TV_LIST_DOWNLOAD', 'Descargar escaparate');

define ('TITLE_COMARCA_ADMIN','Por zona: ');
// sobreescrits missatges demand custumer
$gl_messages_demand = $gl_messages;
$gl_messages_custumer = $gl_messages;
// sobreescrits end

// sobreescrits missatges property
$gl_messages_property['added']='El inmueble se ha añadido correctamente. A continuación puede insertar las imágenes correspondientes a la nueva finca';
$gl_messages_property['saved']='El inmueble se ha modificado correctamente';
$gl_messages_property['no_records']='No hay ningún inmueble en esta lista';
$gl_messages_property['no_records_bin']='No hay ningún inmueble en la papelera';
$gl_messages_property['list_saved']='Los cambios en los inmuebles se han realizado correctamente';
$gl_messages_property['list_not_saved']='No se ha modificado ningún inmueble ni se ha detectado ningún cambio';
$gl_messages_property['list_deleted']='Los inmuebles seleccionados se han eliminado';
$gl_messages_property['list_bined']='Los inmuebles se han enviado a la papelera de reciclaje';
$gl_messages_property['list_restored']='Los inmuebles se han recuperado correctamente';
// sobreescrits end

$gl_messages_category['related_not_activated'] = 'Este tipo no puede desactivarse, ya que contiene inmuebles relacionados';
$gl_messages_category['relateds_not_activated'] = 'Los tipos de inmuebles marcados no pueden desactivarse, ya que contienen inmuebles relacionados';



define('PROPERTY_AUTO_REF', 'Automática');

// captions seccio
$gl_caption_demand['c_form_title'] = 'Datos de solicitud del cliente: ';

$gl_caption['c_property_button'] = 'Ver inmuebles';
$gl_caption['c_view_property'] = 'Ver inmueble';

$gl_caption_property['c_move_to_category_id'] = '[tipo de inmueble]';
$gl_caption_property['c_move_to_category_id2'] = 'Cambiar inmuebles seleccionados a:';
$gl_caption_property['c_move_to_zone_id'] = '[zona]';
$gl_caption_property['c_move_to_zone_id2'] = 'Cambiar inmuebles seleccionados a:';
$gl_caption_property['c_move_to_zone_id3'] = '[ninguna zona]';

$gl_caption_category['c_ordre'] = 'Orden';
$gl_caption_category['c_ref_code'] = 'Código referencia inmuebles';
$gl_caption_property['c_room'] = 'Habitaciones';
$gl_caption_property['c_bathroom'] = 'Baños';
$gl_caption_property['c_wc'] = 'Baños sociales';
$gl_caption_property['c_laundry'] = 'Lavadero';
$gl_caption_property['c_parking'] = 'Parking';
$gl_caption_property['c_garage'] = 'Garaje';
$gl_caption_property['c_garage_area'] = 'superficie';
$gl_caption_property['c_storage'] = 'Closet';
$gl_caption_property['c_storage_area'] = 'superficie';
$gl_caption_property['c_terrace'] = 'Terraza';
$gl_caption_property['c_terrace_area'] = 'superficie';
$gl_caption_property['c_garden'] = 'Jardín';
$gl_caption_property['c_garden_area'] = 'superficie';
$gl_caption_property['c_swimmingpool'] = 'Piscina';
$gl_caption_property['c_swimmingpool_area'] = 'superficie';
$gl_caption_property['c_citygas'] = 'Gas ciudad';
$gl_caption_property['c_centralheating'] = 'Calefacción';
$gl_caption_property['c_airconditioned'] = 'Caja de aire';
$gl_caption_property['c_ref'] = 'Ref.';
$gl_caption_property['c_property_private'] = 'Nombre privado';
$gl_caption_property['c_category_id'] = 'Tipo';
$gl_caption_property['c_floor_space'] = 'Superficie';
$gl_caption_property['c_land'] = 'Terreno';
$gl_caption_property['c_land_units'] = 'Unidades';
$gl_caption_property['c_m2'] = 'm2';
$gl_caption_property['c_ha'] = 'hectáreas';
$gl_caption_property['c_country_id'] = 'País';
$gl_caption_property['c_price_private'] = 'Precio propietario';
$gl_caption_property['c_price'] = 'Precio';
$gl_caption_property['c_price_consult'] = 'Precio a consultar';
$gl_caption_property['c_price_consult_1'] = 'Precio a consultar';
$gl_caption_property['c_price_history_link'] = 'Historial de precios';
$gl_caption_property['c_owner_id'] = '';
$gl_caption_property['c_status'] = 'Estado';
$gl_caption_property['c_prepare'] = 'En preparación';
$gl_caption_property['c_review'] = 'Revisar';
$gl_caption_property['c_onsale'] = 'Público';
$gl_caption_property['c_sold'] = 'Vendido';
$gl_caption_property['c_archived'] = 'Archivado';
$gl_caption_property['c_reserved'] = 'Reservado';
$gl_caption_property['c_zone'] = 'Zona'; // para la parte pública
$gl_caption_property['c_zone_id'] = 'Zona';
$gl_caption_property['c_zone_id_caption'] = '[ninguna zona]';
$gl_caption_property['c_provincia_id'] = 'Provincia';
$gl_caption_property['c_comarca_id'] = 'Comarca';
$gl_caption_property['c_municipi_id'] = 'Municipio';
$gl_caption_property['c_property'] = 'Nombre del inmueble';
$gl_caption_property['c_property_title'] = 'Descripción breve';
$gl_caption_property['c_entered'] = 'Creada';
$gl_caption_property['c_ref_number'] = '';
$gl_caption_property['c_home'] = 'Destacado';
$gl_caption_property['c_offer'] = 'Oferta';
$gl_caption_property['c_data'] = 'Datos del inmueble';
$gl_caption_property['c_descriptions'] = 'Descripciones';
$gl_caption_property['c_details'] = 'Detalles';
$gl_caption_property['c_description'] = 'Descripción';
$gl_caption_property['c_details_temp'] = 'Alquiler temporal';
$gl_caption_property['c_0'] = '';
$gl_caption_property['c_1'] = '1';
$gl_caption_property['c_2'] = '2';
$gl_caption_property['c_3'] = '3';
$gl_caption_property['c_4'] = '4';
$gl_caption_property['c_5'] = '5';
$gl_caption_property['c_6'] = '6';
$gl_caption_property['c_7'] = '7';
$gl_caption_property['c_8'] = '8';
$gl_caption_property['c_9'] = '9';
$gl_caption_property['c_10'] = '10';
$gl_caption_property['c_11'] = '11';
$gl_caption_property['c_12'] = '12';
$gl_caption_property['c_13'] = '13';
$gl_caption_property['c_14'] = '14';
$gl_caption_property['c_15'] = '15';
$gl_caption_property['c_16'] = '16';
$gl_caption_property['c_17'] = '17';
$gl_caption_property['c_18'] = '18';
$gl_caption_property['c_19'] = '19';
$gl_caption_property['c_20'] = '20';
$gl_caption_property['c_tipus'] = 'Operación';
$gl_caption_property['c_sell'] = 'Venta';
$gl_caption_property['c_rent'] = 'Alquiler';
$gl_caption_property['c_filter_status'] = 'Todos los estados';
$gl_caption_property['c_filter_category_id'] = 'Todos los tipos';
$gl_caption_property['c_filter_tipus'] = 'Todas las operaciones';
$gl_caption_property['c_address'] = 'Dirección';
$gl_caption_property['c_adress'] = 'Calle';
$gl_caption_property['c_numstreet'] = 'Número';
$gl_caption_property['c_block'] = 'Bloque';
$gl_caption_property['c_flat'] = 'Piso';
$gl_caption_property['c_door'] = 'Puerta';
$gl_caption_property['c_observations'] = 'Observaciones';
$gl_caption_property['c_data_private'] = 'Datos privados';
$gl_caption_property['c_data_public'] = 'Datos públicos';
$gl_caption_property['c_tipus2'] = 'Tipo de obra';
$gl_caption_property['c_elevator'] = 'Ascensor';
$gl_caption_property['c_balcony'] = 'Balcón';
$gl_caption_property['c_new'] = 'Obra nueva';
$gl_caption_property['c_second'] = '2ª mano';
$gl_caption_property['c_furniture'] = 'Amueblado';
$gl_caption_property['c_file'] = 'Documentos';
$gl_caption_property['c_video'] = $gl_caption_property['c_videoframe'] = 'Vídeo';
$gl_caption_property['c_edit_custumer'] = 'Propietarios';
$gl_caption_property['c_edit_custumer_button'] = 'Editar propietarios';

$gl_caption_property['c_mls']='MLS';
$gl_caption_property['c_share_mls']='Compartir por MLS';
$gl_caption_property['c_comission'] = 'Comisión MLS (%)';


$gl_caption_custumer['c_form_custumer_title'] = 'Propietarios del inmueble:';
$gl_caption_custumer['c_new_custumer'] = 'Introducir nuevo propietario';
$gl_caption_custumer['c_add_custumer'] = 'Asignar propietario';
$gl_caption_custumer['no_assigned'] = 'No hay ningún propietario asignado a este inmueble';

$gl_caption_zone['c_zone'] ='Nombre de barrio y/o zona';
$gl_caption_zone['c_municipi_id'] ='municipio';

// sobreescrits images
$gl_caption_image['c_edit'] = 'Imágenes de la finca';
// sobreescrits end

//zones
$gl_caption_zone['c_municipi_id'] ='Municipio';
$gl_caption_zone['c_s_municipi_id'] ='Buscar municipio';
$gl_caption_zone['c_zone'] ='Nombre del barrio y/o zona';

// nous globals
$gl_caption['c_by_ref'] = '(o referencias separadas por espacios, como por ej: "456 245 45")';
$gl_caption['c_by_words'] = 'Por palabras';

$gl_caption['c_list_title'] = 'Ver listas';
$gl_caption['c_search_title'] = 'Buscar finca';
$gl_caption['c_filter_title'] = 'Filtrar fincas';
$gl_caption['c_all_ordre'] = 'ordenado por:';
$gl_caption['c_category_title'] = 'Por categoría';
$gl_caption['c_zone_title'] = 'Por zona';
$gl_caption['c_status_title'] = 'Según estado';
$gl_caption['c_description_title'] = 'Según descripciones';
$gl_caption['c_price_title'] = 'Por precio';
$gl_caption['c_images_title'] = 'Por imágenes';
$gl_caption['c_has_no_images'] = 'Sin imágenes';
$gl_caption['c_has_images'] = 'Con imágenes';
$gl_caption['c_images_indiferent'] = 'Indiferente';
$gl_caption['c_ref'] = 'Referencia';
$gl_caption['c_property_private'] = 'Nombre privado';
$gl_caption['c_entered'] = 'Fecha';
$gl_caption['c_active'] = 'Activado';
$gl_caption['c_category_original'] = 'Tipo original';
$gl_caption['c_category'] = 'Tipo';
$gl_caption['c_status'] = 'Estado';
$gl_caption['c_show_results'] = 'Mostrar resultados';
$gl_caption['c_floor_space_title'] = 'Superficie';
$gl_caption['c_land_title'] = 'Terreno';
$gl_caption['c_ordered_title'] = 'Ordenadas por';
$gl_caption['c_m2'] = 'm2';
$gl_caption['c_ha'] = 'ha';
$gl_caption['c_showwindow'] = 'Imprimir escaparate';
$gl_caption['c_album'] = 'Imprimir álbum';


// NO TRADUIT

define('INMO_MENU_TV_LIST', 'Seleccionar Inmuebles');
define('INMO_MENU_TV_LIST_SELECTED', 'Inmuebles en el escaparate');


$gl_caption['c_images'] = 'Imágenes';
$gl_caption['c_no_available'] = 'Este inmueble ja no está disponible!';
$gl_caption['c_private'] = 'Ocultar Privado';
$gl_caption_tv['c_send_edit'] = 'Añadir al Escaparate';
$gl_caption_tv['c_download'] = 'Archivos';
$gl_caption_tv['c_main_files'] = 'Archivos_principales';
$gl_caption_tv['c_property_file'] = 'Inmueble';

$gl_caption['c_type_title'] = 'Por Tipo';


$gl_messages_tv['list_saved']='El escaparate ha sido modificado correctamente';


$gl_caption_property['c_living'] = 'Salas';
$gl_caption_property['c_dinning'] = 'Comedores';
$gl_caption_property['c_service_room'] = 'Habitación servicio';
$gl_caption_property['c_service_bath'] = 'Baño servicio';
$gl_caption_property['c_study'] = 'Estudio';
$gl_caption_property['c_courtyard'] = 'Patio';
$gl_caption_property['c_courtyard_area'] = 'superficie';
$gl_caption_property['c_floor'] = 'Piso';
$gl_caption_property['c_parket'] = 'Parquet';
$gl_caption_property['c_ceramic'] = 'Baldosa';
$gl_caption_property['c_gres'] = 'Cerámica';
$gl_caption_property['c_laminated'] = 'Laminado';
$gl_caption_property['c_estrato'] = 'Estrato';
$gl_caption_property['c_estrato_e1'] = '1';
$gl_caption_property['c_estrato_e2'] = '2';
$gl_caption_property['c_estrato_e3'] = '3';
$gl_caption_property['c_estrato_e4'] = '4';
$gl_caption_property['c_estrato_e5'] = '5';
$gl_caption_property['c_estrato_e6'] = '6';
$gl_caption_property['c_edit_map'] = 'Mapa';
$gl_caption_property['c_edit_booking'] = 'Reservas';
$gl_caption_property['c_edit_booking_extra'] = 'Precios - Extras';
$gl_caption_property['c_form_map_title'] = 'Ubicación del inmueble';
$gl_caption_property['c_form_booking_title'] = 'Reservas del inmueble';
$gl_caption_property['c_save_map'] = 'Guardar ubicación';
$gl_caption_property['c_latitude'] = 'Latitud';
$gl_caption_property['c_longitude'] = 'Longitud';
$gl_caption_property['c_click_map'] = 'Haz un clic en un punto del mapa para establecer la situación';
$gl_messages['point_saved'] = 'Nueva ubicación guardada correctamente';

$gl_caption_property['c_tipus_temp'] = 'Alquiler de temporada';
$gl_caption_property['c_tipus_moblat'] = 'Alquiler con muebles';
$gl_caption_property['c_tipus_selloption'] = 'Alquiler con opción de compra';

$gl_caption_property['c_efficiency'] = 'Eficiencia energética';
$gl_caption_property['c_efficiency_progress'] = 'En trámite';
$gl_caption_property['c_efficiency_exempt'] = 'Exento';
$gl_caption_property['c_efficiency_a'] = 'A';
$gl_caption_property['c_efficiency_b'] = 'B';
$gl_caption_property['c_efficiency_c'] = 'C';
$gl_caption_property['c_efficiency_d'] = 'D';
$gl_caption_property['c_efficiency_e'] = 'E';
$gl_caption_property['c_efficiency_f'] = 'F';
$gl_caption_property['c_efficiency_g'] = 'G';

$gl_caption_property['c_efficiency2_a'] = 'A';
$gl_caption_property['c_efficiency2_b'] = 'B';
$gl_caption_property['c_efficiency2_c'] = 'C';
$gl_caption_property['c_efficiency2_d'] = 'D';
$gl_caption_property['c_efficiency2_e'] = 'E';
$gl_caption_property['c_efficiency2_f'] = 'F';
$gl_caption_property['c_efficiency2_g'] = 'G';
$gl_caption_property['c_efficiency2_notdefined'] = '';


$gl_caption_property['c_filter_room'] = 'Habitaciones';
$gl_caption_property['c_filter_room_0'] = 'Ninguna';

$gl_caption_property['c_a4'] = 'A4';
$gl_caption_property['c_a3'] = 'A3';
$gl_caption_property['c_caption_showwindow_top_fields'] = 'Sin límite';
$gl_caption_property['c_caption_showwindow_pages'] = $gl_caption_property['c_caption_album_pages'] = 'Todas';
$gl_caption_property['c_submenu_0'] = 'Idioma';
$gl_caption_property['c_submenu_1'] = 'Tamaño papel';
$gl_caption_property['c_submenu_2'] = 'Num. de páginas';
$gl_caption_property['c_submenu_3'] = 'Límite de campos';
$gl_caption_property['c_submenu_4'] = 'Mostrar';
$gl_caption_property['c_submenu_5'] = 'guardar esta configuración';
$gl_caption_property['c_submenu_6'] = 'Plantilla';
// FI NO TRADUIT

$gl_caption['c_no_subscrit'] = "Informació publicació portals ";
$gl_caption['c_no_subscrit_apicat'] = "No està donat d'alta per publicar a Api.cat";
$gl_caption['c_no_subscrit_fotocasa'] = "No està donat d'alta per publicar a Fotocasa";
$gl_caption['c_no_subscrit_habitaclia'] = "No està donat d'alta per publicar a Habitaclia";