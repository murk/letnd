<?

/* 25-5-16

	MENÚ MISSATGES
 */

if (Db::get_first('SELECT count(*) FROM all__menu WHERE menu_group = \'missatge\'')){
	$update_sql [] = "UPDATE all__menu SET read1 = ',1,', write1 = ',1,' WHERE  menu_group = 'missatge'";
}
else {
	// Poso menus de missatge només per admin total
	$update_sql [] = "INSERT INTO `all__menu` (`menu_id`, `menu_group`, `tool`, `tool_section`, `action`, `parent_id`, `toolmode_id`, `variable`, `ordre`, `link`, `process`, `bin`, `read1`, `write1`) VALUES (80000, 'missatge', 'missatge', '', '', 0, 1, 'MISSATGE_MENU_ROOT', 85, '80003', '', 0, ',1,', ',1,');";
	$update_sql [] = "INSERT INTO `all__menu` (`menu_id`, `menu_group`, `tool`, `tool_section`, `action`, `parent_id`, `toolmode_id`, `variable`, `ordre`, `link`, `process`, `bin`, `read1`, `write1`) VALUES (80001, 'missatge', 'missatge', 'missatge', '', 80000, 1, 'MISSATGE_MENU_MISSATGE', 0, '', '', 0, ',1,', ',1,');";
	$update_sql [] = "INSERT INTO `all__menu` (`menu_id`, `menu_group`, `tool`, `tool_section`, `action`, `parent_id`, `toolmode_id`, `variable`, `ordre`, `link`, `process`, `bin`, `read1`, `write1`) VALUES (80002, 'missatge', 'missatge', 'missatge', 'show_form_new', 80001, 1, 'MISSATGE_MENU_NEW', 1, '', '', 0, ',1,', ',1,');";
	$update_sql [] = "INSERT INTO `all__menu` (`menu_id`, `menu_group`, `tool`, `tool_section`, `action`, `parent_id`, `toolmode_id`, `variable`, `ordre`, `link`, `process`, `bin`, `read1`, `write1`) VALUES (80003, 'missatge', 'missatge', 'missatge', 'list_records', 80001, 1, 'MISSATGE_MENU_LIST', 1, '', '', 0, ',1,', ',1,');";
	$update_sql [] = "INSERT INTO `all__menu` (`menu_id`, `menu_group`, `tool`, `tool_section`, `action`, `parent_id`, `toolmode_id`, `variable`, `ordre`, `link`, `process`, `bin`, `read1`, `write1`) VALUES (80004, 'missatge', 'missatge', 'missatge', 'list_records_bin', 80001, 1, 'MISSATGE_MENU_BIN', 1, '', '', 0, ',1,', ',1,');";
}


?>