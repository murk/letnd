<?

/* 29-09-15

	Actualitzaci� NEWS, he afegit default_category_id per si es vol llistar nom�s una categoria a /news/mew

 */

if ( is_table( 'news__configpublic' ) && ! Db::get_first( "SELECT * FROM news__configpublic WHERE name='default_category_id'" ) ) {
	$update_sql[] = "
		INSERT INTO news__configpublic (`name`, `value`) VALUES ('default_category_id', '');";

} else {
	if ( is_table( 'news__configpublic' ) ) {
		trigger_error( 'Ja existeixen el camp default_category_id' );
	}
}
?>