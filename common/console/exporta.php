<?
namespace Console\Command;

// use Nette\Security\Passwords;
use Console;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Style\SymfonyStyle;

// use Symfony\Component\Console\Question\Question;


final class Exporta extends Command {

	private $output;
	private $input;

	protected function configure() {
		$this->setName( 'exporta' )
		     ->setDescription( 'Exportar als diferents portals.' )
		     ->addArgument( 'portal', InputArgument::OPTIONAL, "A quin portal es fa l'exportacio" )
		     ->setAliases( [ 'export' ] );
	}

	protected function execute( InputInterface $input, OutputInterface $output ) {
		$this->input  = $input;
		$this->output = $output;
		$io = new SymfonyStyle($input, $output);
		$portal       = $input->getArgument( 'portal' );

		/*if ( ! $portal ) {
		    $helper = $this->getHelper('question');
			$question = new Question( 'Quin portal?', 'all');
			$portal = $helper->ask($input, $output, $question);
		}*/

		if ( ! $portal ) {
			$helper   = $this->getHelper( 'question' );
			$question = new ChoiceQuestion(
				'Quin portal?',
				[ 'tots', 'fotocasa', 'vreasy', 'mls', 'ceigrup' ] );

			$question->setErrorMessage( 'El portal %s no es valid.' );

			$portal = $helper->ask( $input, $output, $question );
		}

		$output->writeln( sprintf(
			'Comenca exportacio a: %s', $portal
		) );

		$this->$portal( $portal, $io );

		// return value is important when using CI
		// to fail the build when the command fails
		// 0 = success, other values = fail

		return 0;
	}

	private function tots( $portal, SymfonyStyle $io ) {
		$this->output->writeln( "<question>$portal</question>" );
	}

	private function fotocasa( $portal, SymfonyStyle $io ) {
		$this->output->writeln( "<question>$portal</question>" );
	}

	private function vreasy( $portal, SymfonyStyle $io ) {
		$this->output->writeln( "<question>$portal</question>" );
		$io->success( 'Exportacio Vreasy realitzada' );
	}

	private function mls( $portal, SymfonyStyle $io ) {

		$this->output->writeln( "<question>$portal</question>" );

		include( DOCUMENT_ROOT . 'admin/modules/admintotal/admintotal_mls.php' );

		$mls = New \AdmintotalMls;
		$mls->is_cli = true;
		$mls->_start();

		$io->success( 'Exportacio mls realitzada' );

	}

	private function ceigrup( $portal ) {
		$this->output->writeln( "<question>$portal</question>" );
	}
}