<?php
function get_vars_news_new($content_ids, &$ids, &$names)
{
    $query = "SELECT news__new.new_id AS new_id, new
				FROM news__new, news__new_language
				WHERE news__new.new_id IN (" . implode(',', $content_ids) . ")
				AND news__new.new_id = news__new_language.new_id
				AND language = '".LANGUAGE."'
				ORDER BY ordre ASC, entered DESC, new_id" ;
    $results = Db::get_rows($query);
    foreach($results as $rs)
    {
        $nam = $rs['new'];
        $names[] = htmlspecialchars(str_replace(',', ' ', $nam), ENT_QUOTES);
        $ids[] = $rs['new_id'];
    }
}
function get_content_extra_news_new($ids, $action, $language)
{
	if ($action == 'show_record')
    {
	    $ids = explode(',', $ids);
	    $content = '';
        foreach ($ids as $id)
        {
            $content .= get_url_html(HOST_URL . '/?template=&tool=news&tool_section=new&action=show_record&loginnewsletter=letndmarc&passwordnewsletter=5356aopfqkiios5cnoqws90se&new_id='.$id.'&language=' . $language);
        }
    }
	else if ($action == 'list_records') {
        $content = get_url_html(HOST_URL . '?template=&tool=news&tool_section=new&action=list_records&loginnewsletter=letndmarc&passwordnewsletter=5356aopfqkiios5cnoqws90se&new_ids='.$ids.'&language=' . $language);
		
	}
    return $content;
}

?>