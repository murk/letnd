<?

/* 29-7-16

	PRODUCT

	UP-DATE
	- Afegir specs a product

 */

if ( is_table( 'product__product' ) ) {

	if ( ! is_table( 'product__spec' ) ) {

		$update_sql [] = "CREATE TABLE `product__spec` (
			`spec_id` INT(11) NOT NULL AUTO_INCREMENT,
			`spec_category_id` INT(11) NOT NULL,
			`spec_type` ENUM('text_no_lang','text','textarea','checkbox','int','select') NOT NULL COLLATE 'utf8_spanish2_ci',
			`spec_unit` VARCHAR(10) NOT NULL COLLATE 'utf8_spanish2_ci',
			`ordre` INT(11) NOT NULL DEFAULT '0',
			`bin` TINYINT(1) NOT NULL,
			PRIMARY KEY (`spec_id`)
		)
		COLLATE='utf8_spanish2_ci'
		ENGINE=MyISAM;";

	} else {
		trigger_error( 'Ja existeixen la taula product__spec' );
	}

}
if ( is_table( 'product__product' ) ) {

	if ( ! is_table( 'product__spec_category' ) ) {

		$update_sql [] = "CREATE TABLE `product__spec_category` (
				`spec_category_id` INT(11) NOT NULL AUTO_INCREMENT,
				`ordre` INT(11) NOT NULL DEFAULT '0',
				PRIMARY KEY (`spec_category_id`)
			)
			COLLATE='utf8_spanish2_ci'
			ENGINE=MyISAM;
";

	} else {
		trigger_error( 'Ja existeixen la taula ' );
	}

}

if ( is_table( 'product__product' ) ) {

	if ( ! is_table( 'product__spec_category_language' ) ) {

		$update_sql [] = "CREATE TABLE `product__spec_category_language` (
			`spec_category_id` INT(11) NOT NULL,
			`language` CHAR(3) NOT NULL DEFAULT '' COLLATE 'utf8_spanish2_ci',
			`spec_category` VARCHAR(200) NOT NULL COLLATE 'utf8_spanish2_ci'
		)
		COLLATE='utf8_spanish2_ci'
		ENGINE=MyISAM;
";

	} else {
		trigger_error( 'Ja existeixen la taula product__spec_category_language' );
	}

}

if ( is_table( 'product__product' ) ) {

	if ( ! is_table( 'product__spec_language' ) ) {

			$update_sql [] = "CREATE TABLE `product__spec_language` (
					`spec_id` INT(11) NOT NULL DEFAULT '0',
					`language` CHAR(3) NOT NULL DEFAULT '' COLLATE 'utf8_spanish2_ci',
					`spec` VARCHAR(400) NOT NULL COLLATE 'utf8_spanish2_ci'
				)
				COLLATE='utf8_spanish2_ci'
				ENGINE=MyISAM;";

	} else {
		trigger_error( 'Ja existeixen la taula product__spec_language' );
	}
}
if ( is_table( 'product__product' ) ) {

	if ( ! is_table( 'product__product_to_spec' ) ) {

		$update_sql [] = "CREATE TABLE `product__product_to_spec` (
			`product_spec_id` INT(11) NOT NULL AUTO_INCREMENT,
			`product_id` INT(11) NOT NULL DEFAULT '0',
			`spec_id` INT(11) NOT NULL DEFAULT '0',
			`spec` VARCHAR(255) NOT NULL DEFAULT '' COLLATE 'utf8_spanish2_ci',
			PRIMARY KEY (`product_spec_id`),
			INDEX `spec_id` (`spec_id`),
			INDEX `product_id` (`product_id`)
		)
		COLLATE='utf8_spanish2_ci'
		ENGINE=MyISAM;";

	} else {
		trigger_error( 'Ja existeixen la taula product__product_to_spec' );
	}

}
if ( is_table( 'product__product' ) ) {

	if ( ! is_table( 'product__product_to_spec_language' ) ) {

		$update_sql [] = "CREATE TABLE `product__product_to_spec_language` (
							`product_spec_id` INT(11) NOT NULL DEFAULT '0',
							`product_id` INT(11) NOT NULL DEFAULT '0',
							`language` CHAR(3) NOT NULL DEFAULT '' COLLATE 'utf8_spanish2_ci',
							`spec` VARCHAR(255) NOT NULL COLLATE 'utf8_spanish2_ci',
							INDEX `product_spec_id` (`product_spec_id`),
							INDEX `language` (`language`)
						)
						COLLATE='utf8_spanish2_ci'
						ENGINE=MyISAM
						ROW_FORMAT=DYNAMIC;";

	} else {
		trigger_error( 'Ja existeixen la taula product__product_to_spec_language' );
	}

}

?>