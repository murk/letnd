
##########################################
# COMPARTIR CARPETA                      #
##########################################
echo -e "\n--- Edit SAMBA conf ---\n"

# Compartir USB
mkdir /media/usb
mount /dev/sdb1 /media/usb

chmod -R a+w /media/usb

sudo -i
cat >> /etc/samba/smb.conf <<EOF
force user = root
EOF

net usershare add usb /media/usb "USB files" everyone:F guest_ok=y