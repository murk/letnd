<?php

function list_property_of_custumer ($custumer_id, $name, $is_jscript = false)
{
	$custumer_id = R::id($custumer_id,false,false);
	global $gl_content;
	change_tool('inmo', 'property', 'list_records');
    $listing = new ListRecords;
    $listing->name = 'property';
    $listing->order_by = 'ref asc';
    $listing->call_function['description'] = 'add_dots';
    $listing->template = 'inmo/property_list';
    $listing->set_options(0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
    $listing->has_bin = false; //no te bin
    $listing->join = 'INNER JOIN custumer__custumer_to_property USING (property_id)';
    $listing->condition = 'custumer_id = ' . $custumer_id;
	$listing->add_button ('edit_property_button', '', 'boto1', 'before','show_form_edit_property');
	$listing->add_button ('edit_images', '', 'boto1', 'before','show_form_edit_images');
   	$listing->no_records_message = '';
	$listing->set_var( 'show_mls_button', false );
	$listing->set_var( 'is_mls', false );

	$listing->list_records();
	if ($is_jscript) {
		return $gl_content;
	}
    $ret['properties'] = $gl_content;
    $ret ['demands'] = list_demands_of_custumer($custumer_id);
	$ret ['bookings'] = list_bookings_of_custumer($custumer_id);
    $ret += list_records_walk_1($listing, $custumer_id, $name);
    return $ret;
}
function list_demands_of_custumer ($custumer_id)
{
	$custumer_id = R::id($custumer_id,false,false);
	global $gl_content, $gl_has_images, $tpl;
	$gl_has_images = false;
    change_tool('custumer', 'demand', 'list_records');
    $listing = new ListRecords;
    $listing->has_bin = false;
	$listing->join = "INNER JOIN custumer__custumer USING (custumer_id)";
    $listing->name = 'demand';
	$listing->order_by = 'demand_id DESC';
    $listing->set_options(0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
    $listing->no_records_message = '';
    $listing->condition = 'custumer_id = ' . $custumer_id;
    $listing->add_button ('edit_demand_button', '', 'boto1', 'before','show_form_edit_demand');
	$listing->call('demand_records_walk','land,land_units,demand_id',true);
	$listing->list_records();
    if ($listing->has_results)
    {
        return $gl_content;
    }
    else
    {
        return '';
    }
}
// el parametre custumer id ja l'agafa per get a bookingbook
function list_bookings_of_custumer ($custumer_id)
{
	$module = Module::load( 'booking', 'book' );
	$module->is_custumer_form = true;
	return $module->do_action( 'get_records' );
}


// Busqueda per paraules
function search_list_condition_words ()
{
    global $gl_page, $gl_site_part;
    $is_admin = $gl_site_part == 'admin';
    $q = R::escape('words');
    // miro quantes paraules hi ha:
    $query = " company like '%" . $q . "%' OR
			   comercial_name like '%" . $q . "%' OR
			   name like '%" . $q . "%' OR
			   concat(name,' ', surname1, ' ', surname2) like '%" . $q . "%' OR
			   surname1 like '%" . $q . "%' OR
			   surname2 like '%" . $q . "%' OR
			   vat like '%" . $q . "%' OR
			   phone1 like '%" . $q . "%' OR
			   phone2 like '%" . $q . "%' OR
			   phone2 like '%" . $q . "%' OR
			   adress like '%" . $q . "%' OR
			   town like '%" . $q . "%' OR
			   province like '%" . $q . "%' OR
			   country like '%" . $q . "%' OR
			   zip like '%" . $q . "%' OR
			   mail like '%" . $q . "%' OR
			   website like '%" . $q . "%' OR
			   observations1 like '%" . $q . "%' OR
			   observations2 like '%" . $q . "%'
										" ;

		if ($is_admin)
		{
			$query .=" OR custumer_id IN (
							SELECT custumer_id
								FROM custumer__custumer_to_property
								INNER JOIN inmo__property
								USING(property_id)
								WHERE (
									ref like '%" . $q . "%'
									OR property_private like '%" . $q . "%'
									OR observations like '%" . $q . "%'
									OR adress like '%" . $q . "%'
									)
							)";
		}
	$query = " (" . $query . ")";

    $search_condition = $query;
    $gl_page->title = TITLE_SEARCH;
    return $search_condition;
}
?>