<?
/**
 * AmctaicAgenda
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2010
 * @version $Id$
 * @access public
 */
class AmctaicAgenda extends Module{
	var $url1_select = '', $url1='';
	function __construct(){
		parent::__construct();
	}
	function list_records()
	{
		$this->get_frase_home();
		$GLOBALS['gl_content'] = $this->get_records();
	}
	// es crida a la home de amctaic al block de l'agenda
	function get_records()
	{
	    $listing = new ListRecords($this);
		$listing->paginate = false;
		if ($GLOBALS['gl_is_home']){
			$listing->limit = $this->config['home_max_results'];		
		}
		$listing->condition = "status = 'public'";
		$listing->call_function['content'] = 'add_dots';
		$listing->order_by = 'entered DESC';		
	    return $listing->list_records();
	}
	function get_frase_home(){
		$page = &$GLOBALS['gl_page'];
		
		// frase de la imatge
		$page->set_var('frase',nl2br(Db::get_first("SELECT frase FROM amctaic__inici WHERE inici_id=1")));
		
		$image = Db::get_row("SELECT image_id, name FROM amctaic__inici_image WHERE inici_id=1 ORDER BY ordre DESC LIMIT 0,1");
		$image_names = ImageManager::get_file_name($image['image_id'], $image['name'], 'amctaic', 'inici');
		$page->set_var('image',$image_names['image_src_details']);
	}
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{
		$this->set_field('entered','default_value',now(true));
		$show = new ShowForm($this);
		$show->get_values();		
		if ($show->has_results)
		{
			return $show->show_form();
		}
		else
		{
			$this->do_action('list_records');
		}
	}
	function get_forums($tipus){
		$query = "
			SELECT amctaic__forum.forum_id AS forum_id,tipus,forum 
				FROM amctaic__forum 
				LEFT OUTER JOIN amctaic__forum_language using (forum_id) 
			WHERE amctaic__forum.bin = 0
				AND tipus = '".$tipus."'
				AND amctaic__forum_language.language = '".LANGUAGE."'";
				
		$results = Db::get_rows($query);
		
		$ret = '';
		
		if ($results)
		{
			foreach($results as $rs)
			{
				
				$l['link'] = '/?tool_section=forum&forum_id=' . $rs['forum_id'] . '&language=' . LANGUAGE;
				$l['item'] = $rs['forum'];
				$selected = ($this->url1 == $l['link'])?' selected':'';
				$ret.='<option value="'.$l['link'].'"'.$selected.'>'.$l['item'].'</option>';
			}
		}
		return $ret;
	}
	
	function save_rows()
	{
	    $save_rows = new SaveRows($this);
	    $save_rows->save();
	}

	function write_record()
	{
	    $writerec = new SaveRows($this);
		$writerec->save();
	}

	function manage_images()
	{
	    $image_manager = new ImageManager($this);
	    $image_manager->execute();
	}
}
?>