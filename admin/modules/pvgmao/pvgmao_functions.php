<?
	// Aquesta funció es crida cada cop que es carrega un mòdul a PV
	function pvgmao_on_load (&$module){

		Main::load_class( 'pv', 'common', 'admin' );

		PvCommon::add_page_files( $module );

		PvCommon::set_security( $module );

		PvCommon::check_client_id( $module );

		PvCommon::set_client_menus( $module );

		// De moment genero cada cop que carrego pàgina, s'haurà de millorar quan hi hagi molta cosa, un cronjob és el millor
		PvCommon::generate_manteniments( $module );

	}
?>