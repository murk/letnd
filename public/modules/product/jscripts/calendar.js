function Calendar(family_id, product_id) {
	this.start = true;
	this.date_busy = '0000/00/00';
	this.painted = {};
	this.b_original_class = false;
	this.month_width = 0;
	this.month_loaded = {};
	this.month_loaded_count = 0;
	this.first_month_viewing = '';
	this.paint_class = 'selected';
	this.unpaint_class = 'booked';
	this.caller = '';
	this.attr = '';
	this.family_id = family_id;
	this.product_id = product_id;
	this.calendar_holder = $('#calendar-' + product_id);
	this.calendar_table = $('#calendar-table-' + product_id);
	this.calendar_tr = $('#calendar-tr-' + product_id);
	this.formated_date_busy = '';
	this.reserva_items = {};
	this.is_reserva_first_choice = true;
	this.is_empty = false;
	this.is_date_busy_required = true;


	var mes_holder = this.calendar_holder.find('.mes-holder');

	// No e calendari, només reserva_items
	if (mes_holder.length < 1) {
		this.is_empty = true;
	}
	else {
		var obj = this;
		mes_holder.each(function (index) {
			var mes_id = $(this).data('mes-id');
			if (!obj.first_month_viewing) obj.first_month_viewing = mes_id;
			obj.month_loaded[mes_id] = true;
			obj.month_loaded_count++;
		});
	}

	this.fix_size = function () {

		// fixo la mida per quan es carreguin mes mesos
		mes_holder.removeAttr('style');
		this.calendar_holder.removeAttr('style');

		this.month_width = mes_holder.width();
		this.calendar_holder.width(this.calendar_holder.width());
		mes_holder.width(this.month_width);

	};
	// fixo la mida per quan es carreguin mes mesos nomes de carregar
	this.fix_size();

	this.save_day = function (dia, mes, any, id_element, process) {

		if ( process == 'occupy') {
			this.formated_date_busy = dia + '/' + mes + '/' + any;
			this.date_busy = any + '/' + mes + '/' + dia;
		}
		else {
			// Valors inicials
			this.formated_date_busy = '';
			this.date_busy = '0000/00/00';
		}

		if (typeof ReservaList !== 'undefined' && typeof ReservaList.product_init_orderitems == 'function')
			ReservaList.product_init_orderitems(this, this.product_id, this.formated_date_busy, this.date_busy);

	};
	this.do_action = function (dia, mes, any) {

		var id = '.' + dia + '-' + mes + '-' + any;
		var id_element = this.calendar_holder.find(id);

		if (id_element.hasClass('occupied')) return;

		// si està seleccionat, deselecciono
		if (
			id_element.hasClass('selected')
		) {
			this.caller = 'selected';
			this.set_day(dia, mes, any, id_element)
		}
		else {
			this.caller = 'free';
			this.set_day(dia, mes, any, id_element)
		}

	};
	this.set_day = function (dia, mes, any, id_element) {

		if (this.caller == 'selected') {
			id_element.removeClass('selected');
			id_element.addClass('free');

			this.save_day(dia, mes, any, id_element, 'free');
		}
		else if (this.caller == 'free') {
			// nomes permeto 1 seleccionat
			this.calendar_holder.find('.selected').removeClass('selected');

			id_element.removeClass('free');
			id_element.addClass('selected');
			dp('id_element', id_element);

			this.save_day(dia, mes, any, id_element, 'occupy');
		}

	};
	this.show_month = function (direction) {

		var viewing = this.first_month_viewing.split('-');

		var viewing_month = viewing[0];
		var viewing_year = viewing[1];
		var month = 0;
		var year = 0;

		if (direction == 'next') {
			month = Number(viewing_month) + 1;
			year = viewing_year;
			if (month > 12) {
				month = 1;
				year++;
			}
			this.first_month_viewing = month + '-' + year;

			month = Number(viewing_month) + 3;
			year = viewing_year;
			if (month > 12) {
				month = month - 12;
				year++;
			}
		}
		if (direction == 'prev') {
			month = viewing_month - 1;
			year = viewing_year;
			if (month < 1) {
				month = 12;
				year--;
			}
			this.first_month_viewing = month + '-' + year;
		}
		//dp(month + '-' + year);
		if (typeof(this.month_loaded[month + '-' + year]) == 'undefined') {

			var link = '/product/familybusy/get_month/?month=' + month + '&year=' + year + '&family_id=' + this.family_id + '&language=' + gl_language;

			if (this.product_id) link += '&product_id=' + product_id;


			var obj = this;
			$.ajax({
				url: link
			})
				.done(function (data) {

					if (direction == 'next') {
						obj.calendar_tr.append(data);
					}
					else {
						obj.calendar_tr.prepend(data);
						obj.calendar_table.css('margin-left', -obj.month_width);
					}
					obj.calendar_holder.find('.mes-holder').width(obj.month_width);

					obj.month_loaded[month + '-' + year] = true;
					obj.month_loaded_count++;

					obj.calendar_table.width(obj.month_width * obj.month_loaded_count);

					obj.animate_month(direction);

				});

		}
		else {
			this.animate_month(direction);
		}

	};
	this.animate_month = function (direction) {

		var dir = direction == 'next' ? -1 : 1;
		var obj = {};
		obj['marginLeft'] = dir * this.month_width + parseInt(this.calendar_table.css('margin-left'));

		this.calendar_table.stop().animate(obj, 500);
	};
};