<?php
/**
 *
 * @access public
 * @return void
 */
function list_records_walk($property_id, $ref, $category_id, $price_consult, $price, $status, $land, $land_units, $tipus, $entered, &$caption=false)
{
    global $gl_caption;
	if (!$caption) $caption = &$gl_caption;
	
    $ret = get_ref($ref, $category_id);
    $ret += get_price_consult($price_consult, $price, $caption, $property_id, $tipus);
    $ret += get_status($status);
    $ret += get_land($land,$land_units);
	$ret += get_land_units($land_units);
	$ret += get_bookmark_link($property_id);
	$ret += get_recent($entered);

    return $ret;
}
/**
 *
 * @access public
 * @return void
 */


// defineixo els tipus de custom que hi han
function set_custom_config(&$config, &$fields, &$language_fields, &$caption) {
	
		// defineixo els tipus de custom que hi han
	for ($i = 1; $i <= 8; $i++) {
		if ($config['custom'.$i.'_name']) {
			$fields['custom'.$i]['form_public'] = 'text';
			$fields['custom'.$i]['list_public'] = 'text';
			$fields['custom'.$i]['change_field_name'] = 'inmo__property.custom'.$i.' as custom'.$i.'';
			$fields['custom'.$i]['type'] = $config['custom'.$i.'_type'];
			$caption['c_custom'.$i] = $config['custom'.$i.'_name'];
			
			
			if ($config['custom'.$i.'_type'] == 'text') {
				$fields['custom'.$i]['change_field_name'] = 'inmo__property_language.custom'.$i.' as custom'.$i.'';
				$language_fields[] = 'custom'.$i;
			}
		}
		else{
			$fields['custom'.$i]['form_public'] = 'out';		
		}
	}
}

function list_records_images($property_id)
{
	// aquest arxiu l'haig de treure, les imatges ja me les mostra el listing, l'unica ventatja aquí es que pagina les imatges i que les posa en columnes
    global $gl_action, $gl_page, $tpl, $gl_db_max_results, $gl_content, $gl_has_images, $gl_db_classes_uploads;
	$gl_has_images = false; // una imatge no té una taula d'imatges relacionada :-)
	$old_uploads = $gl_db_classes_uploads;
	$gl_db_classes_uploads = array(); // una imatge no té arxius relacionats :-)
    $listing = new ListRecords();
	$listing->paginate = false;
	$listing->set_config('property_image');
    $listing->id_field = 'image_id';
    $listing->has_bin = false;
    $listing->cols = INMO_IM_LIST_COLS;

    $tpl->set_var("client_dir", CLIENT_DIR);
    $tpl->set_var("im_details_w", INMO_IM_DETAILS_W);
    $tpl->set_var("im_details_h", INMO_IM_DETAILS_H);
    $tpl->set_var("im_medium_w", INMO_IM_MEDIUM_W);
    $tpl->set_var("im_medium_h", INMO_IM_MEDIUM_H);
    $tpl->set_var("property_id", $property_id);

    $listing->condition = "property_id = " . $property_id;
    $listing->no_records_message = "";

    $listing->call("get_file_name", "image_id,name");

    $listing->order_by = 'ordre asc';

    if (isset($_GET['loginnewsletter']) && $_GET['loginnewsletter'] == 'letndmarc' && $_GET['passwordnewsletter'] == '5356aopfqkiios5cnoqws90se')
    {
        $listing->template = 'newsletter/property_image_list';
    }
    $listing->list_records();
	// tornem els valors on eren
	$gl_has_images = true;
	$gl_db_classes_uploads = $old_uploads;
    return $gl_content;
}
/**
 *
 * @access public
 * @return void
 */
function get_file_name($image_id, $name)
{
    global $gl_caption;
    $ext = strrchr(substr($name, -5, 5), '.');
    $image_details = $image_id . '_details' . $ext;
    $image_thumb = $image_id . '_thumb' . $ext;
    $image_medium = $image_id . '_medium' . $ext;

    return array("image_name_details" => $image_details,
        "image_name_thumb" => $image_thumb,
        "image_name_medium" => $image_medium,
        "client_dir" => CLIENT_DIR);
}

function get_user($user_id)
{
    $query = "SELECT concat (name, ' ', surname)
				FROM user__user
				WHERE user_id='" . $user_id . "'";
    return Db::get_first($query);
}

function get_property_language($language_id)
{
    $query = "SELECT language
				FROM all__language
				WHERE language_id='" . $language_id . "'";
    return Db::get_first($query);
}

function get_category($category_id)
{
    $query = "SELECT category
				FROM inmo__category_language
				WHERE category_id='" . $category_id . "'
				AND language='" . LANGUAGE . "'";
    $rs = Db::get_rows($query);
    return $rs[0]['category'];
}

function get_comarca($comarca_id)
{
    $query = "SELECT comarca
				FROM inmo__comarca
				WHERE comarca_id='" . $comarca_id . "'";
    $rs = Db::get_rows($query);
    return $rs[0]['comarca'];
}

function get_municipi($municipi_id)
{
    $query = "SELECT municipi
				FROM inmo__municipi
				WHERE municipi_id='" . $municipi_id . "'";
    $rs = Db::get_rows($query);
    return $rs[0]['municipi'];
}


function get_price_consult($price_consult, $price, &$caption = false, $property_id, $tipus)
{
    // Si es preu a consultar amago el preu
    global $gl_caption;
	
	if (!$caption) $caption = &$gl_caption;
    $ret = array();
	
    if ($price_consult)
    {
        $ret['old_price'] = 0;
        $ret['price_change_percent'] = 0;
        $ret['price_change_percent_value'] = 0;
        $ret['price'] = 0;
        $ret['price_value'] = 0;
        $ret['price_consult'] = $caption['c_price_consult_1'];
    }
    else
    {
        $ret +=get_old_price($price, $property_id);
	    $ret['price_consult'] = 0;
    }
	
	// preu a inmobles lloguer temporal
	if ($tipus=='temp' && !$price_consult){
		Main::load_class('booking', 'common', 'admin');
		$ret += BookingCommon::get_property_price($property_id,$price);
	}
	
    return $ret;
}

function get_old_price( $price, $property_id ) {

	$query = "SELECT history_title
				FROM custumer__history
				WHERE custumer__history.bin = 0
				AND property_id = '" . $property_id . "'
				AND tipus = 'price_changed'
				AND history_title <> '" . format_int( $price ) . "'
				AND history_title <> '" . $price . "'
				ORDER BY date DESC, history_id DESC
				LIMIT 1";

	$old_price = Db::get_first( $query );
	$old_price = unformat_int($old_price);

	// M'asseguro que no sigui igual al preu que tenim ara
	if ($old_price > 0 && $old_price != $price) {

		$ret['price_change_percent_value'] = ( $price - $old_price ) * 100 / $old_price;
		$ret['price_change_percent'] = format_int(
			abs( $ret['price_change_percent_value'])
		);

		$old_price = format_int($old_price);
	}
	else {
		$old_price = 0;
		$ret['price_change_percent_value'] = 0;
		$ret['price_change_percent'] = 0;
	}

	$ret ['old_price'] = $old_price;

	return $ret;
}
function get_ref($ref, $category_id)
{
    // Obtenir la referencia
    // if (INMO_PROPERTY_AUTO_REF == 1)
    $query = "SELECT ref_code
				FROM inmo__category
				where category_id = " . $category_id;
    $rs = Db::get_rows($query);
    $ret['ref'] = $rs[0]['ref_code'] . $ref;
    return $ret;
}
function get_status($status)
{
    if ($status == 'onsale')
    {
        $ret['status'] = '';
    }
    else
    {
        $ret['status'] = $status;
    }
    return $ret;
}
function get_land($land, $land_units){
	if ($land_units == 'ha') {
		$ret['land'] = $land/10000;
	}
	else
	{
		$ret['land'] = $land;
	}
    return $ret;
}
function get_land_units($land_units){
	if ($land_units == 'm2') {
		$ret['land_units'] = 'm<sup>2</sup>';
	}
	else
	{
		$ret['land_units'] = $land_units;
	}
    return $ret;
}
function inmo_get_first_category(){

	// TODO - suposarem que la primera catregoria es de sell ( s'haurà de posar en config també )

	$query = "SELECT inmo__category.category_id AS category_id
			FROM inmo__category, inmo__category_language
			INNER JOIN inmo__property
			USING (category_id)
			WHERE inmo__property.bin = 0
			AND (status = 'onsale' OR status = 'sold' OR status = 'reserved')
			AND tipus = 'sell'
			AND inmo__category_language.category_id = inmo__category.category_id
			AND language = '" . LANGUAGE . "'
			ORDER BY inmo__category.ordre ASC, inmo__category_language.category
			LIMIT 1 ";
	$category_id = Db::get_first($query);

	return $category_id;
}
function get_bookmark_link($property_id) {
	if(isset($_COOKIE['inmo']['bookmarks'])){
		$ids = unserialize($_COOKIE['inmo']['bookmarks']);
		$ret['bookmark_save_class'] = in_array($property_id, $ids)?'active':'';
		
	}
	else{
		$ret['bookmark_save_class'] = '';
	}
	$ret['bookmark_save_link'] = '/' . LANGUAGE . '/inmo/property/save_bookmark/property_id/' . $property_id . '/';
	
	return $ret;
}

function get_recent( $entered ) {
	if ( strtotime( $entered ) >= strtotime( '-' . INMO_RECENT_TIME .' days' ) ) {
		$ret['recent'] = true;
	} else {
		$ret['recent'] = false;
	}
	return $ret;
}

?>