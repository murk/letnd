<?php
// no es poden posar ; entre mig de la consulta ja que parteix les consultes per el ; a no ser que fem servir $update_sql en array()
	
if (is_table('product__product')){
	$update_sql .="	
CREATE TABLE `product__promcode` (
	`promcode_id` INT(10) NOT NULL AUTO_INCREMENT,
	`promcode` VARCHAR(100) NOT NULL DEFAULT '' COLLATE 'utf8_spanish2_ci',
	`discount` INT(11) NOT NULL DEFAULT '0',
	`discount_type` ENUM('fixed','percent') NOT NULL DEFAULT 'fixed' COLLATE 'utf8_spanish2_ci',
	`minimum_amount` INT(11) NOT NULL DEFAULT '0',
	`end_date` DATE NOT NULL,
	PRIMARY KEY (`promcode_id`)
)
COLLATE='utf8_spanish2_ci'
ENGINE=MyISAM;
	
CREATE TABLE `product__customer_to_promcode` (
	`customer_promcode_id` INT(10) NOT NULL AUTO_INCREMENT,
	`customer_id` INT(10) NOT NULL,
	`promcode_id` INT(10) NOT NULL,
	PRIMARY KEY (`customer_promcode_id`)
)
COLLATE='utf8_spanish2_ci'
ENGINE=MyISAM;
";
}
?>