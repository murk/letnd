<?php
/*
 * Social
 */

/**
 * Classe per publicar al facebook, twitter
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 17/10/2013
 * @version $Id$
 * @access public
 */
class Social {
	// variables
	public $id, $module, $link, $image;
	
	// facebook variables
	public $facebook_default_page_id, $facebook_default_page=array(), $facebook_user_id, $facebook_login_url, $facebook, $facebook_publish_id, $facebook_access_token, $facebook_publish_select = false;
	
	public $facebook_params = array(
			'message' => ''
			,'link' => ''
			,'name' => ''
			,'caption' => ''
			,'description' => ''
			,'picture' => ''
		);
	
	// twitter variables
	public $twitter, $twitter_user_id, $twitter_login_url;
	
	public $twitter_params = array(
			'status' => ''
		);
	
	
	public function __construct($module, $id = false, $facebook_params = false, $twitter_params = false, $link='') {
		
		$cg = &$GLOBALS['gl_config'];
		$this->module = $module;
		$this->id = $id;
		$this->link = $link;
		$this->facebook_default_page_id = $cg['facebook_default_page_id'];
		
		
		if ($facebook_params){
			$facebook_params['link'] = $link;
			$this->facebook_params = $facebook_params;
		}
		if ($twitter_params) {
			$twitter_params['status'] .= ' ' . $link;
			$this->twitter_params = $twitter_params;
		}
		Debug::p($_SESSION,'Sessions');
	}
	
	public function get_form() {		
				
		
		$tpl = new phemplate(PATH_TEMPLATES);		
		$tpl->set_vars($GLOBALS['gl_caption']);
		$tpl->set_vars($this->module->config);
		$tpl->set_file('social.tpl');
		
		$return_url = '/admin/' . get_all_get_params(array('code', 'state', 'access_token', 'user_id','oauth_verifier', 'oauth_token'));
		$tpl->set_var('return_url',urlencode($return_url));

		$excluded_params = array('action','code', 'state', 'access_token', 'user_id','oauth_verifier', 'oauth_token');

		$tpl->set_var('data_link','/admin/?action=show_form_edit' . get_all_get_params($excluded_params,'&'));
		$tpl->set_var('image_link','/admin/?action=list_records_images' . get_all_get_params($excluded_params,'&'));
		$tpl->set_var('link_action','/admin/?action=write_record_social' . get_all_get_params($excluded_params,'&'));
		
		$tpl->set_var('social_title',$this->facebook_params['name']);
		$tpl->set_var('has_images',$this->module->has_images);
		
		// facebook
		$this->facebook = $facebook = social_get_facebook();
		if ($facebook){
		
			$this->_facebook_get_user();		

			$this->facebook_params['picture'] = $this->_get_image();

			$tpl->set_var('facebook_is_published',$this->_facebook_is_published());
			$tpl->set_var('facebook_letnd_user', $this->_facebook_get_letnd_user());
			$tpl->set_var('facebook_publish_id',$this->_facebook_select_pages());
			$tpl->set_var('facebook_language',$this->_get_select_language('facebook'));
			$tpl->set_var('facebook_publish_select',$this->facebook_publish_select);
			$tpl->set_var('facebook_is_logged',!empty($this->facebook_user_id));
			$tpl->set_var('facebook_login_url',$this->facebook_login_url);

			// detalls usuari
			$user_details = json_decode(Db::get_first("SELECT facebook_user_details
								FROM  user__user
								WHERE  user_id= " . $_SESSION['user_id']));		
			if ($user_details){
				$tpl->set_var('facebook_user_name',$user_details->name);

				if (property_exists ($user_details,'page_name')) {
					$tpl->set_var('facebook_user_link',$user_details->page_link);
					// Debug::p($user_details, 'user details 2');
				}
				else{
					$tpl->set_var('facebook_user_link',$user_details->link);
				}
			}
			else{
				$tpl->set_var('facebook_user_name','');
				$tpl->set_var('facebook_user_link','');			
				$tpl->set_var('facebook_username','');			
			}

			// Debug::p( $this->facebook_params['link'] );

			foreach ($this->facebook_params as $k=>$v){
				$tpl->set_var('facebook_' . $k,$v);
			}
			
			$tpl->set_var('facebook_is_active',true);
			
		}
		// s'ha d'activar
		else{
			$tpl->set_var('facebook_is_active',false);
			$tpl->set_var('facebook_is_published',false);
			$tpl->set_var('facebook_is_logged',false);
		}
		// twitter
		$this->twitter = $twitter = social_get_twitter();	
		$this->_twitter_get_user();
		
		
		
		$tpl->set_var('twitter_is_published',$this->_twitter_is_published());
		$tpl->set_var('twitter_letnd_user', $this->_twitter_get_letnd_user());
		$tpl->set_var('twitter_language',$this->_get_select_language('twitter'));
		$tpl->set_var('twitter_is_logged',!empty($this->twitter_user_id));
		$tpl->set_var('twitter_login_url',$this->twitter_login_url);
		
		// dettalls usuari
		$user_details = json_decode(Db::get_first("SELECT twitter_user_details
							FROM  user__user
							WHERE  user_id= " . $_SESSION['user_id']));		
		if ($user_details){
			$tpl->set_var('twitter_user_name',$user_details->name);
			$tpl->set_var('twitter_user_screen_name',$user_details->screen_name);
		}
		else {
			$tpl->set_var('twitter_user_name','');
			$tpl->set_var('twitter_user_screen_name','');			
		}
		
		foreach ($this->twitter_params as $k=>$v){
			$tpl->set_var('twitter_' . $k,$v);
		}
				
		return $tpl->process();
	}
	private function _get_image() {
		$id = $this->id;
		
		$picture =  UploadFiles::get_record_images($id, false, $this->module->tool, $this->module->tool_section );		
		
		$host_url = substr(HOST_URL, 0, -1);
		if ($picture) {
			//$this->image = $picture['image_src_medium'];
			$picture = $picture['image_src_medium'];
			$picture = $this->image = $host_url . $picture;
		}
		
		return $picture;
	}
	private function _encode_image () {
			//$imgbinary = fread(fopen($this->image, "r"), filesize($this->image));
			return file_get_contents($this->image);
	}

	public function publish() {
		
		$this->id = R::id($this->module->id_field);
		
		Debug::p($_POST, 'post');
		
		if (R::post('facebook_publish')){
			$this->_facebook_publish();
		}
		
		if (R::post('facebook_delete')){
			$this->_facebook_delete();
		}
		
		if (R::post('twitter_publish')){
			$this->_twitter_publish();
		}
		
		if (R::post('twitter_delete')){
			$this->_twitter_delete();
		}		
		
		
	}
	/*
	 * 
	 * Funcions per publicar a facebook
	 * 
	 * 
	 */
	private function _facebook_publish() {		
		
		if (!$this->_facebook_initialize()) return;
		
		$publish_id_save_default = R::post('facebook_publish_id_save_default');
		
		if($publish_id_save_default=='1'){
			Db::execute("
				UPDATE all__configadmin SET value='" . $this->facebook_publish_id . "' 
					WHERE  name='facebook_default_page_id';
			");
			
			// Grabo el nom i enllaç de la pagina
			$user_details = json_decode(Db::get_first("SELECT facebook_user_details
							FROM  user__user
							WHERE  user_id= " . $_SESSION['user_id']));	
			
			
			$page = $this->_facebook_get_response('/'.$this->facebook_publish_id, 'GET');
			
			if (isset($page['about'])){
				$user_details->page_name = $page['name'];
				$user_details->page_link = $page['link'];

				Db::execute("UPDATE user__user
								SET 
									facebook_user_details='" . json_encode($user_details) . "'
								WHERE  user_id= " . $_SESSION['user_id']);
			}
			
		}
		
		$facebook = &$this->facebook;	
		
		$publish_as = R::post('facebook_publish_as');
		
		// publico com imatge, es pot posar foto i comentari	
		// el comentari a imatge es la descripció a link
		if ($publish_as == 'image') {
			
			$publish_path = 'photos';
			$publish_array = 				
					array(
						// 'locale' => 'es_ES',
						'published' => 'true',
						'url'=> R::post('facebook_picture')
					);
			$message_array = array();
			if (!empty($_POST['facebook_name'])) {
				$message_array[] = $_POST['facebook_caption'];
			}

			$message_array[] = $_POST['facebook_link'];
				
			if (!empty($_POST['facebook_caption'])) {
				$message_array[] = $_POST['facebook_caption'];
			}
				
			if (!empty($_POST['facebook_description'])) {
				$message_array[] = $_POST['facebook_description'];
			}
			if ($message_array) $publish_array['caption'] = implode("\n\n",$message_array);
		}
		// publico com enllaços
		else{
			
			$publish_path = 'feed';
			/*$publish_array =
					array(
						'locale' => 'es_ES'
					);*/

			foreach($this->facebook_params as $k=>$v){				
				if (R::post('facebook_' . $k )) $publish_array[$k] = R::post('facebook_' . $k );
			}
		}

		debug::p($publish_array, '$publish_array');
		debug::p('/' . $this->facebook_publish_id . '/' . $publish_path);

		print_r($publish_array);
		
		$ret_obj = $this->_facebook_get_response(	'/' . $this->facebook_publish_id . '/' . $publish_path ,'POST',$publish_array);
		
		if (!$ret_obj) return;
		
		if ($publish_as == 'image') {			
			$post_id = $ret_obj['post_id'];
			$image_id = $ret_obj['id'];
		}
		else{			
			$post_id = $ret_obj['id'];	
			$image_id = '';		
		}
		
		$query = "
			UPDATE " . $this->module->table . "
			SET facebook_post_id='" . $post_id . "', 
			facebook_image_id='" . $image_id . "',
			facebook_publish_id='" . $this->facebook_publish_id . "',
			facebook_letnd_user_id='" . $_SESSION['user_id'] . "',
			facebook_user_id='" . $this->facebook_user_id . "'
			WHERE  " . $this->module->id_field . "='" . $this->id . "'";
		Db::execute($query);
		debug::p($query);
		
		debug::p($ret_obj);
		
		if (!DEBUG) {
			$GLOBALS['gl_reload'] = true;
		}
		
		$GLOBALS['gl_message'] .= '<p>' . $this->module->messages['social_facebook_published'] . '</p>';
		
		// per retreure el missatge
		//$ret_obj = $facebook->api(	'/' . $this->facebook_publish_id . '/' . $publish_path );
		//debug::p($ret_obj);
		
	}
	private function _facebook_delete() {
		
		$query = "
			SELECT facebook_post_id, facebook_image_id, facebook_publish_id
			FROM " . $this->module->table . "
			WHERE  " . $this->module->id_field . "='" . $this->id . "'";
		$rs = Db::get_row($query);
		
		extract($rs);
		
		if (!$this->_facebook_initialize($facebook_publish_id)) return;
				
		if (!$facebook_post_id && !$facebook_image_id) {
			$GLOBALS['gl_message'].= '<p>' . $this->module->messages['social_facebook_not_deleted'] . '</p>';
			return;
		}		
		
		/* SI borro imatge dona error, suposo que nomes borrant el post la imatge ja queda fora 
		if ($facebook_image_id){
			$facebook->api("/" . $facebook_image_id,"DELETE");
		}
		 * 
		 */		
		//if ($facebook_post_id){
		$deleted = $this->_facebook_get_response("/" . $facebook_post_id,"DELETE");
		//}
		
		if ($deleted){

			if (!DEBUG) {
				$GLOBALS['gl_reload'] = true;
			}

			$GLOBALS['gl_message'] .= '<p>' . $this->module->messages['social_facebook_deleted'] . '</p>';
		
			$query = "
			UPDATE " . $this->module->table . "
			SET facebook_post_id='', 
			facebook_image_id='',
			facebook_publish_id='',
			facebook_letnd_user_id='',
			facebook_user_id=''
			WHERE  " . $this->module->id_field . "='" . $this->id . "'";
			Db::execute($query);

			Debug::p($query, 'query delete');
			Debug::p($rs, 'Rs delete');
			
		}
		// else ho fa el _facebook_get_response
	}
	private function _facebook_is_published() {
		$query = "
			SELECT facebook_post_id, facebook_image_id
			FROM " . $this->module->table . "
			WHERE  " . $this->module->id_field . "='" . $this->id . "'";
		$rs = Db::get_row($query);
		return $rs['facebook_post_id'] || $rs['facebook_image_id'];
	}
	private function _facebook_get_letnd_user() {
		$query = "
			SELECT facebook_letnd_user_id, facebook_user_id
			FROM " . $this->module->table . "
			WHERE  " . $this->module->id_field . "='" . $this->id . "'";
		$rs = Db::get_row($query);
		extract($rs);
		
		if (!$facebook_user_id || $facebook_user_id == $this->facebook_user_id) return false;
		
		$query = "
			SELECT concat(name,' ', surname) FROM user__user WHERE user_id = " . $facebook_letnd_user_id;
		
		return Db::get_first($query);
	}
	
	/*
	 * 
	 * Funcions per loguejar a facebook
	 * 
	 * 
	 */
	
	private function _facebook_initialize($publish_id = false) {
		
		$this->facebook = social_get_facebook();
		//$facebook->destroySession(); // per testejar nomes	
		$this->_facebook_get_user();		
		
		if (!$this->facebook_user_id) {
			$GLOBALS['gl_message'] = $this->module->messages['social_facebook_no_user'];
			return false;
		}
		$this->_facebook_get_access_token($publish_id);
		
		if (!$this->facebook_access_token) {
			$GLOBALS['gl_message'] = $this->module->messages['social_facebook_no_page'];
			return false;
		}
		return true;
	}

	private function _facebook_is_logged() {

		$fb = &$this->facebook;

		if ( ! ( isset( $_SESSION['fb_access_token'] ) ) ) {
			$saved_access_token = Db::get_first( 'SELECT facebook_access_token FROM user__user WHERE user_id= ' . $_SESSION['user_id'] );

			if ( $saved_access_token ) {
				$_SESSION['fb_access_token'] = $saved_access_token;
			}
		}

		// Si ja tinc un token de sessió, no faig res
		if (isset($_SESSION['fb_access_token'] )){
			$fb->setDefaultAccessToken($_SESSION['fb_access_token']);
			return $_SESSION['fb_access_token'];
		}


		$helper = $fb->getRedirectLoginHelper();
		if (isset($_GET['state'])) $_SESSION['FBRLH_state']=$_GET['state'];

		try {
			$accessToken = $helper->getAccessToken();
		} catch ( Facebook\Exceptions\FacebookResponseException $e ) {
			// When Graph returns an error
			$error =  'Graph returned an error: ' . $e->getMessage();
			Debug::p($error, 'Facebook login');
			return false;
		} catch ( Facebook\Exceptions\FacebookSDKException $e ) {
			// When validation fails or other local issues
			$error =  'Facebook SDK returned an error: ' . $e->getMessage();
			Debug::p($error, 'Facebook login');
			return false;
		}

		if ( ! isset( $accessToken ) ) {
			if ( $helper->getError() ) {
				header( 'HTTP/1.0 401 Unauthorized' );
				$error = '';
				$error .= "Error: " . $helper->getError() . "\n";
				$error .= "Error Code: " . $helper->getErrorCode() . "\n";
				$error .= "Error Reason: " . $helper->getErrorReason() . "\n";
				$error .= "Error Description: " . $helper->getErrorDescription() . "\n";
				Debug::p($error, 'Facebook login');

			} else {
				header( 'HTTP/1.0 400 Bad Request' );
				Debug::p('Bad request', 'Facebook login');
			}
			return false;
		}

		$this->_save_user_details($accessToken);

		return $accessToken;
	}

	private function _save_user_details( $accessToken ) {

		$fb = &$this->facebook;

		$this->_facebook_get_long_live_access_token( $accessToken );


		$user_details_obj = $fb->get( '/me?fields=id,name,picture,link' );

		$user_details = $user_details_obj->getDecodedBody();

		Debug::p( [
			[ 'user', $user ],
			[ 'user_details', $user_details ],
		], 'facebook' );

		$new_user_details         = array();
		$new_user_details['name'] = $user_details['name'];
		$new_user_details['link'] = $user_details['link'];
		// $new_user_details['username'] = $user_details['username'];

		Db::execute( "UPDATE user__user
								SET 
									facebook_access_token = '" . $this->facebook_access_token . "',
									facebook_user_details='" . json_encode( $new_user_details ) . "'
								WHERE  user_id= " . $_SESSION['user_id'] );
	}

	/*
	 * Poso el token per que duri per sempre	 *
	 */
	private function _facebook_get_long_live_access_token($accessToken) {

		$fb = &$this->facebook;

		// The OAuth 2.0 client handler helps us manage access tokens
		$oAuth2Client = $fb->getOAuth2Client();

		// Get the access token metadata from /debug_token
		$tokenMetadata = $oAuth2Client->debugToken( $accessToken );
		Debug::p( [
					[ 'tokenMetadata', $tokenMetadata ],
				], 'facebook');

		// Validation (these will throw FacebookSDKException's when they fail)
		$tokenMetadata->validateAppId( $GLOBALS['gl_config']['facebook_appid'] );
		// If you know the user ID this access token belongs to, you can validate it here
		//$tokenMetadata->validateUserId('123');
		$tokenMetadata->validateExpiration();

		if ( ! $accessToken->isLongLived() ) {
			// Exchanges a short-lived access token for a long-lived one
			try {
				$accessToken = $oAuth2Client->getLongLivedAccessToken( $accessToken );
			} catch ( Facebook\Exceptions\FacebookSDKException $e ) {
				$error  = "<p>Error getting long-lived access token: " . $e->getMessage() . "</p>\n\n";
				Debug::p ( $error, 'Facebook long live' );
				exit;
			}

			Debug::p( [
							[ 'Long-lived', $accessToken->getValue() ],
						], 'facebook');
		}

		$_SESSION['fb_access_token'] = $this->facebook_access_token = (string) $accessToken;
		$fb->setDefaultAccessToken($accessToken);


	}

	private function _facebook_get_user() {

		$fb = &$this->facebook;

		$is_logged = $this->_facebook_is_logged();

		if ( $is_logged ) {

			try {
				// Returns a `Facebook\FacebookResponse` object
				$response = $fb->get( '/me?fields=id,name' );
			} catch ( Facebook\Exceptions\FacebookResponseException $e ) {
				$error = 'Graph returned an error: ' . $e->getMessage();
				$error_code = $e->getCode();

				if ( $error_code = 100 ) {

					Db::execute( "UPDATE user__user
								SET 
									facebook_access_token = ''
								WHERE  user_id= " . $_SESSION['user_id'] );
					unset( $_SESSION['fb_access_token'] );
				}

				Debug::p( "$error, Codi: $error_code", 'Facebook user details' );

				return;
			} catch ( Facebook\Exceptions\FacebookSDKException $e ) {
				$error = 'Facebook SDK returned an error: ' . $e->getMessage();
				Debug::p( $error, 'Facebook user details' );
				exit;
			}

			$user    = $response->getGraphUser();
			$user_id = $user['id'];

		} else {

			$return_url = HOST_URL . 'admin/' . get_all_get_params( array(
					'code',
					'state',
					'access_token',
					'user_id',
					'oauth_verifier',
					'oauth_token',
					'error_code',
					'error_message'
				), '?', '', true );


			$helper = $fb->getRedirectLoginHelper();

			$permissions = [ 'email', 'public_profile', 'publish_actions', 'manage_pages', 'publish_pages', 'pages_show_list' ]; // Optional permissions
			$loginUrl    = $helper->getLoginUrl( $return_url, $permissions );


			$this->facebook_login_url = $loginUrl;

			$user_id = false;
		}

		$this->facebook_user_id = $user_id;

	}	
	
	// obtinc accees token segons publish_id del desplegable per triar on publicar
	// Així es publica la pagina o publica l'usuari			
	private function _facebook_get_access_token($publish_id){

		$publish_id = $publish_id?$publish_id:R::post('publish_id');
		$this->facebook_publish_id = $publish_id;

		// quan publica al mur propi
		if ($this->facebook_user_id == $publish_id) {
			$this->facebook_access_token = $_SESSION['fb_access_token'];
			return;
		}
		
		$accounts = $this->_facebook_get_response('/'.$this->facebook_user_id.'/accounts', 'GET');

		Debug::p( [
					[ '$publish_id', $publish_id ],
					[ 'facebook_user_id', $this->facebook_user_id ],
					[ 'accounts', $accounts ],
				], 'facebook');

		if ($accounts){			
			// Busco acces token de l'account'
			foreach ($accounts['data'] as $account) {
				if ($account['id'] == $this->facebook_publish_id){
					$this->facebook_access_token = $account['access_token'];
					return;
				}
			}
		}
		
	}
	
	// Creo desplegable per triar on publico
	private function _facebook_select_pages(){
		
		if (!$this->facebook_user_id) return false;
		
		$accounts = $this->_facebook_get_response('/'.$this->facebook_user_id.'/accounts', 'GET');

		Debug::p( [
					[ 'accounts', $accounts ],
				], 'facebook');
		
		if (!$accounts) return '<input type="hidden" name="facebook_publish_id" value="'. $this->facebook_user_id .'">';
		
		$this->facebook_publish_select = true;
		$select = '<select name="publish_id">';
		$select .= '<option value="'. $this->facebook_user_id .'">'. $GLOBALS['gl_caption']['c_facebook_publish_user'] . '</option>';
		$select .= '<optgroup label="'. $GLOBALS['gl_caption']['c_facebook_publish_pagina'] . '">';
		foreach ($accounts['data'] as $account) {
			if ($account['id'] == $this->facebook_default_page_id){				
				$selected = ' selected';
			}
			else
				$selected = '';
			
			$select .= '<option'.$selected.' value="' . $account['id'] . '">' . $account['name'] . '</option>';
		}
		$select .= '</optgroup>';
		$select .= '</select>';
		return $select;
	}
	// conecta a facebook i controla si hiha error l'error
	private function _facebook_get_response($url,$method,$options = array()) {
		
		try {

			if ( $method == 'GET' ) {

				$ret_obj =
					$this->facebook->get( $url );

			} elseif ( $method == 'POST' ) {

				$ret_obj =
					$this->facebook->post( $url, $options, $this->facebook_access_token );

			} elseif ( $method == 'DELETE' ) {
				$ret_obj = $this->facebook->delete( $url, $options, $this->facebook_access_token );

			}

				return $ret_obj->getDecodedBody();
			} catch(FacebookApiException $e) {
				Debug::p($e->getType(),$e->getMessage());
				$GLOBALS['gl_message'] .= '<p>' . $this->module->messages['social_facebook_error'] . $e->getMessage() . '</p>';
				Debug::backtrace();
				return false;
			}
	}
	static public function facebook_logout() {
		unset($_SESSION['fb_access_token']);
		Db::execute("UPDATE user__user
						SET 
							facebook_access_token='',
							facebook_user_id='',
							facebook_user_details=''
						WHERE  user_id= " . $_SESSION['user_id']);
	}
	
	
	/*
	 * 
	 * Funcions per publicar a twitter
	 * 
	 * 
	 */
	
	private function _twitter_publish() {		
		
		$status = R::post('twitter_status');
		
		if (!$status){
			$GLOBALS['gl_message'] .= '<p>' . $this->module->messages['social_twitter_empty'] . '</p>';
			return;
		}
		
		if (!$this->_twitter_initialize())	return;
		
		$connection = &$this->twitter;	
		
		$this->_get_image();
		if ($this->image) {
			$publish_path = 'statuses/update_with_media';
			$publish_array =  array(
						'status' => $status,
						'media[]' => $this->_encode_image()
						);
			$status = $connection->upload($publish_path, $publish_array);
		}
		else{
			$publish_path = 'statuses/update';
			$publish_array =  array(
						'status' => $status
						);
			$status = $connection->post($publish_path, $publish_array);
		}
		
		
		
		//debug::add('Twitter resposta publish',$status);
		debug::p($publish_array, $publish_path);
		debug::p($status, 'Twitter resposta publish');
		
		if (property_exists($status,'errors')){
			foreach($status->errors as $e){
				$GLOBALS['gl_message'] .= '<p>' . $this->module->messages['social_twitter_error'] . $e->message . '</p>';
			}
			
		}
		else
		{			
			$status_id = $status->id;

			$query = "
				UPDATE " . $this->module->table . "
				SET twitter_status_id='" . $status_id . "',
				twitter_letnd_user_id='" . $_SESSION['user_id'] . "',
				twitter_user_id='" . $this->twitter_user_id . "'
				WHERE  " . $this->module->id_field . "='" . $this->id . "'";
			Db::execute($query);
			debug::p($query);

			if (!DEBUG) {
				$GLOBALS['gl_reload'] = true;
			}

			$GLOBALS['gl_message'] .= '<p>' . $this->module->messages['social_twitter_published'] . '</p>';
		}
		
	}
	
	private function _twitter_delete() {
		
		if (!$this->_twitter_initialize()) return;
		$connection = &$this->twitter;
		
		$query = "
			SELECT twitter_status_id
			FROM " . $this->module->table . "
			WHERE  " . $this->module->id_field . "='" . $this->id . "'";
		$rs = Db::get_row($query);
		
		extract($rs);
		
		if (!$twitter_status_id) {
			$GLOBALS['gl_message'].= '<p>' . $this->module->messages['social_twitter_not_deleted'] . '</p>';
			return;
		}	
		
		$deleted = $connection->delete('statuses/destroy/' . $twitter_status_id);
		Debug::add('Twitter resposta borrar' , $deleted);
		
		if (property_exists($deleted,'errors')){
			
			foreach($deleted->errors as $e){
				$GLOBALS['gl_message'] .= '<p>' . $this->module->messages['social_twitter_error'] . $e->message . '</p>';
			}
			
		}
		else {

			if (!DEBUG) {
				$GLOBALS['gl_reload'] = true;
			}

			$GLOBALS['gl_message'] .= '<p>' . $this->module->messages['social_twitter_deleted'] . '</p>';
		
			$query = "
				UPDATE " . $this->module->table . "
				SET twitter_status_id='',
				twitter_letnd_user_id='',
				twitter_user_id=''
				WHERE  " . $this->module->id_field . "='" . $this->id . "'";
			Db::execute($query);

			Debug::p($query, 'query delete');
			
		}
	}
	private function _twitter_is_published() {
		$query = "
			SELECT twitter_status_id
			FROM " . $this->module->table . "
			WHERE  " . $this->module->id_field . "='" . $this->id . "'";
		$rs = Db::get_row($query);
		return $rs['twitter_status_id'];
	}
	private function _twitter_get_letnd_user() {
		$query = "
			SELECT twitter_letnd_user_id, twitter_user_id
			FROM " . $this->module->table . "
			WHERE  " . $this->module->id_field . "='" . $this->id . "'";
		$rs = Db::get_row($query);
		extract($rs);
		
		if (!$twitter_user_id || $twitter_user_id == $this->twitter_user_id) return false;
		
		$query = "
			SELECT concat(name,' ', surname) FROM user__user WHERE user_id = " . $twitter_letnd_user_id;
		return Db::get_first($query);
	}
	
	/*
	 * 
	 * Funcions per loguejar a twitter
	 * 
	 * 
	 */
	
	private function _twitter_get_user() {
		//unset($_SESSION['twitter']);die('unset');
	
		$s = &$_SESSION['twitter'];	
		debug::p($s, 'Sessio');
		
		// està  logejat
		if (isset($s['oauth_user_id'])) {
			$this->twitter_user_id = $s['oauth_user_id'];
			return true;			
		}
		// o guardat a la bbdd
		else{
			$rs = Db::get_row("
				SELECT twitter_user_id,twitter_token,twitter_token_secret
					FROM user__user
					WHERE user_id = " . $_SESSION['user_id']);
			
			if ($rs && $rs['twitter_user_id']){
				$s['oauth_user_id'] = $rs['twitter_user_id'];
				$s['oauth_token'] = $rs['twitter_token'];
				$s['oauth_token_secret'] = $rs['twitter_token_secret'];
				$this->twitter_user_id = $rs['twitter_user_id'];
				return true;
			}
		}
		
		$connection = &$this->twitter;
		$get_temporary_credencials = true;
		
		
		// NOOO, quan porta estona caduca l'enllaç i llavors em dona error
		// si tinc les variables defindes es que ja he fet un cop PAS 1
		//  i poder estic recarregant la pagina sense fer PAS 2, aixÃ­ estavio de tornar a conectar a twitter
		/*
		if ($s['oauth_url']){	
			$get_temporary_credencials = false;
			$this->twitter_login_url = $s['oauth_url'];	
		}
		 * 
		 */
		
		// PAS 2
		if (isset($_GET['oauth_verifier']) && isset($s['oauth_verify'])) {
			
			debug::p($_GET, 'Pas 2 get');
			$reply = $connection->getAccessToken($_REQUEST['oauth_verifier']);
			debug::p($reply, 'Pas 2 reply');
			
			
			if (!isset($reply['user_id'])) {
				$get_temporary_credencials = true; // fer pas 1
			}
			else{	
				$get_temporary_credencials = false; // no fer pas 1				
				
				unset($s['oauth_verify']);
				unset($s['oauth_url']);
				
				// GUARDAR A BBDD, per un altre cop
				$s['oauth_user_id'] = $reply['user_id'];
				$s['oauth_token'] = $reply['oauth_token'];
				$s['oauth_token_secret'] = $reply['oauth_token_secret'];
				
				$user_details = $connection->get('users/show', array(
							'user_id' => $reply['user_id'],
							'include_entities' => 0
							));
				
				
				$new_user_details = array();
				$new_user_details['name'] = $user_details->name;
				$new_user_details['screen_name'] = $user_details->screen_name;
				
				Db::execute("UPDATE user__user
								SET 
									twitter_user_id='" . $reply['user_id'] . "',
									twitter_token='" . $reply['oauth_token'] . "',
									twitter_token_secret='" . $reply['oauth_token_secret'] . "',
									twitter_user_details='" . json_encode($new_user_details) . "'
								WHERE  user_id= " . $_SESSION['user_id']);
				
				
				$this->twitter_user_id = $s['oauth_user_id'];
				// send to same URL, without oauth GET parameters
				//header('Location: ' . basename(__FILE__));
				//die();	
			}
		}
		
		// PAS 1
		// va segon ja que posa $s['oauth_verify'] = true; i fa que s'executi el segon pas
		if ($get_temporary_credencials) {
			
			$temporary_credentials = $connection->getRequestToken(
					'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']
			); 
			
			
			$s['oauth_token'] = $temporary_credentials['oauth_token'];
			$s['oauth_token_secret'] = $temporary_credentials['oauth_token_secret'];
			$s['oauth_verify'] = true;			
			$s['oauth_url'] = $connection->getAuthorizeURL($temporary_credentials);			
			
			$this->twitter_login_url = $s['oauth_url'];
			
			debug::p($s, 'Pas 1 sessio');
			debug::p($temporary_credentials, 'Pas 1 credencials');
		}
	}
	
	private function _twitter_initialize() {
		
		$this->twitter = social_get_twitter();
		//$facebook->destroySession(); // per testejar nomes	
		$this->_twitter_get_user();
		
		
		if (!$this->twitter_user_id) {
			$GLOBALS['gl_message'] .= '<p>' . $this->module->messages['social_twitter_no_user'] . '</p>';
			return false;
		}			
		
		return true;
	}
	static public function twitter_logout() {
		unset($_SESSION['twitter']);
		Db::execute("UPDATE user__user
						SET 
							twitter_user_id='',
							twitter_token='',
							twitter_token_secret='',
							twitter_user_details=''
						WHERE  user_id= " . $_SESSION['user_id']);
		Debug::p('ok', 'text');
		
	}	
	
	/*
	 * 
	 * Funcions pels llistats
	 * 
	 * 
	 */
	
	static function init_listing(&$listing) {
		
		$listing->add_field('facebook_post_id');
		$listing->add_field('twitter_status_id');
		
		$listing->add_button('button_form_twitter_published', 'action=show_form_social', 'boto-twitter-published','after');
		$listing->add_button('button_form_twitter', 'action=show_form_social', 'boto-twitter','after');
		$listing->add_button('button_form_facebook_published', 'action=show_form_social', 'boto-facebook-published','after');
		$listing->add_button('button_form_facebook', 'action=show_form_social', 'boto-facebook','after');
	}

	/**
	 * @param $listing
	 */
	static function set_buttons_listing(&$listing) {
		
		$has_facebook = $listing->rs['facebook_post_id'];
		$has_twitter = $listing->rs['twitter_status_id'];
		
		$listing->buttons['button_form_twitter_published']['show'] = $has_twitter;
		$listing->buttons['button_form_twitter']['show'] = !$has_twitter;
		$listing->buttons['button_form_facebook_published']['show'] = $has_facebook;
		$listing->buttons['button_form_facebook']['show'] = !$has_facebook;
		
	}

	/**
	 * @return array
	 * @internal param $begin
	 * @internal param $end
	 *
	 */
	private function _get_select_language ( $social_name) {

		// TODO-i S'hauria de tindre en compte el social_name que crida la funció?

		global $gl_languages;

		$select = '<select id="social_language" name="social_language">';

		$selected_language = R::text_id( 'social_language' );

		if ($selected_language){
			setcookie( "social_language", $selected_language, time() + ( 10 * 365 * 24 * 60 * 60 ), '/' ); // 30 dies
		}
		else {
			$selected_language = isset( $_COOKIE['social_language'])?$_COOKIE['social_language']:LANGUAGE;
		}

		foreach ( $gl_languages['public'] as $lang ) {

			$selected = $selected_language == $lang?' selected':'';

			$select .= '<option'.$selected.' value="' . $lang . '">' . $gl_languages['names'][$lang] . '</option>';
		}

		return $select;
	}

	/**
	 * @return string Idioma en que s'agafa el contingut
	 */
	static public function get_language() {
		$language = R::text_id( 'social_language' );
		if ( ! $language )
			$language = isset( $_COOKIE['social_language'] ) ? $_COOKIE['social_language'] : LANGUAGE;

		return $language;
	}
	
}