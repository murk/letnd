<?

/**
 * LassdiveOperator
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2011
 * @version $Id$
 * @access public
 */
class LassdiveOperator extends Module {

	public function __construct() {
		parent::__construct();
	}

	public function on_load() {
		parent::on_load();
	}

	public function list_records() {
		$GLOBALS['gl_content'] = $this->get_records();
	}

	public function get_records() {

		$listing           = new ListRecords( $this );
		$listing->has_bin  = false;
		$listing->order_by = 'operator ASC';

		return $listing->list_records();
	}

	public function show_form() {
		$GLOBALS['gl_content'] = $this->get_form();
	}

	public function get_form() {
		$show = new ShowForm( $this );
		return $show->show_form();
	}

	public function save_rows() {
		$save_rows = new SaveRows( $this );
		$save_rows->save();
	}

	public function write_record() {
		$writerec = new SaveRows( $this );
		$writerec->save();
	}

	public function manage_images() {
		$image_manager = new ImageManager( $this );
		$image_manager->execute();
	}

	public function get_text_autocomplete_values() {
		{
			$limit = 100;

			$q = R::escape( 'query' );

			$ret                = array();
			$ret['suggestions'] = array();

			$r = &$ret['suggestions'];

			// 1 - Població, que comenci
			$query = "SELECT DISTINCT poblacio
					FROM lassdive__operator
					WHERE poblacio LIKE '$q%'
					ORDER BY poblacio
					LIMIT " . $limit;

			$results = Db::get_rows( $query );

			$rest = $limit - count( $results );

			// 2 - Població, qualsevol posició
			if ( $rest > 0 ) {

				$query = "SELECT DISTINCT poblacio
						FROM lassdive__operator
						WHERE poblacio LIKE '%$q%'
			            AND poblacio NOT LIKE '$q%'
						ORDER BY poblacio
						LIMIT " . $rest;

				$results2 = Db::get_rows( $query );

				$results = array_merge( $results, $results2 );

			}


			// Presento resultats
			foreach ( $results as $rs ) {
				$r[] = array(
					'value' => $rs['poblacio'],
					'data'  => ''
				);
			}


			$rest               = $limit - count( $results );
			$ret['is_complete'] = $rest > 0;

			Debug::p_all();
			die ( json_encode( $ret ) );


		}

	}
}