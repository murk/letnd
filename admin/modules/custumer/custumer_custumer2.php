<?php
function list_records()
{
    global $gl_action, $gl_content, $listing, $gl_page, $gl_process;
    // echo "el gl_action és: ".$gl_action."</br>";

    $listing = new ListRecords;
	
	
	if (R::escape('words')) {
		$GLOBALS['gl_page']->javascript .= "
			$('table.listRecord').highlight('".addslashes(R::get('words'))."');			
		";
	}
	
    // comprovo d'on ve el client
    $show_search_buttons = true;

	if ($gl_action == 'list_records_bin') {

	}
	else if ($gl_action == 'list_records_inactive') {
	$listing->condition = 'activated = 0';
	$listing->add_options_button( 'activate', '\'activated\', 1', 'botoOpcions', 'top_after', 'submit_list_move_to_button' );

	}
	else {
		$listing->condition = 'activated = 1';
		$listing->add_options_button( 'deactivate', '\'activated\', 0', 'botoOpcions', 'top_after', 'submit_list_move_to_button' );
		//$listing->add_move_to('activated');
	}

    switch ($gl_action)
    {
       /* No se que es tot això, devia ser algo de'n Marc, però falten les funcions
       case "list_records_action":
            options_custumer();
    		$show_search_buttons = false;
            break;
	    case "list_records_save_buy":
			 save_custumer_buy();
			 $show_search_buttons = false;
			 break;
		case 'list_records_search_nif':
			 $listing->condition .= search_search_condition();
			 $show_search_buttons = false;
			 break;*/

        case "list_records_history":
            list_records_history();
            return;
        case "list_records_owners":
            $listing->add_filter('user_id');
			$listing->add_filter('category_id','custumer__demand_to_category INNER JOIN custumer__demand USING (demand_id)');
			$listing->add_filter('prefered_language');
//			$listing->add_filter('property_status');
            $listing->group_by = 'custumer__custumer.custumer_id';
            $listing->join = 'INNER JOIN custumer__custumer_to_property USING (custumer_id) INNER JOIN inmo__property USING (property_id)';
            $listing->condition .= ' AND tipus = \'sell\'';
            break;
		case "list_records_tenants":			 
            $listing->add_filter('user_id');
			$listing->add_filter('category_id','custumer__demand_to_category INNER JOIN custumer__demand USING (demand_id)');
			$listing->add_filter('prefered_language');
			$listing->group_by = 'custumer__custumer.custumer_id';
            $listing->join = 'INNER JOIN custumer__custumer_to_property USING (custumer_id) INNER JOIN inmo__property USING (property_id)';
            $listing->condition .= ' AND tipus != \'sell\'';
			break;
        case "list_records_buyers":			 
            $listing->add_filter('user_id');
			$listing->add_filter('category_id','custumer__demand_to_category INNER JOIN custumer__demand USING (demand_id)');
			$listing->add_filter('prefered_language');
            //$listing->has_bin = false;
            $listing->group_by = 'custumer__custumer.custumer_id';
            $listing->join = 'INNER JOIN custumer__demand USING (custumer_id)';
            $listing->condition .= ' AND tipus = \'sell\'';
            break;
        case "list_records_renters":			 
            $listing->add_filter('user_id');
			$listing->add_filter('category_id','custumer__demand_to_category INNER JOIN custumer__demand USING (demand_id)');
			$listing->add_filter('prefered_language');
            //$listing->has_bin = false;
            $listing->group_by = 'custumer__custumer.custumer_id';
            $listing->join = 'INNER JOIN custumer__demand USING (custumer_id)';
            $listing->condition .= ' AND tipus != \'sell\'';
            break;
	    case "list_records_bookers":
		    $listing->add_filter('user_id');
		    $listing->add_filter('prefered_language');
		    // $listing->has_bin = false;
		    $listing->group_by = 'custumer__custumer.custumer_id';
		    $listing->join = 'INNER JOIN booking__book_to_custumer USING (custumer_id)';

		    $listing->add_button ('books_button', 'action=list_records&tool=booking&tool_section=book&menu_id=5028', 'boto1', 'after');
		    break;
	    default:
            $listing->add_filter('user_id');
			$listing->add_filter('category_id','custumer__demand_to_category INNER JOIN custumer__demand USING (demand_id)');
			$listing->add_filter('prefered_language'); // per llistar desde grups

			$group_id = R::number('group_id');			
            if ($group_id)
            {
				//$listing->unset_field('group_id');
				$listing->join = "INNER JOIN newsletter__custumer_to_group ON custumer__custumer.custumer_id=newsletter__custumer_to_group.custumer_id";
                $listing->condition .= " AND group_id = " .$group_id;
				
				
				$gl_page->subtitle = Db::get_first('SELECT group_name FROM newsletter__group_language WHERE group_id = ' . $group_id . " AND language = '" . LANGUAGE . "'");
            }
			
            break;
    } // switch

    if ( $gl_process == 'list_records_search_words' ) {
        $listing->condition .= ( $listing->condition ? ' AND ' : '' ) . search_list_condition_words();
        $show_search_buttons = true;
    }

    // utilitza el custumer_custumer_config.php
    $listing->set_options(1, 1, 0, 1, 1, 1, 1);	
	
	
	
	
	
    //$listing->add_button ('add_button', 'action=show_form_new_property', 'boto1', 'after');
    //$listing->add_button ('assign_button', 'action=list_records_select_frames_property', 'boto1', 'after', 'select_addresses');
    //$listing->add_button ('demand_button', 'action=show_form_new_demand', 'boto1', 'after');
    $listing->order_by = 'surname1 ASC, name ASC';
	if ($show_search_buttons) {
		$listing->add_button ('searchexact_button', 'action=list_records_exact&menu_id=5010&selected_menu_id=5003', 'boto1', 'after');
		$listing->add_button ('searchprox_button', 'action=list_records_prox&menu_id=5011&selected_menu_id=5003', 'boto1', 'after');

	}
	$listing->call('list_records_walk_1', 'custumer_id,name', true);//aquesta funció l'utilitzo pq quan vaig a assinar propietat ja em posi les que té.

	$is_invoice = R::id( 'is_invoice' );
	if ( $is_invoice ) {
		$listing->condition .= ( $listing->condition ? ' AND ' : '' ) . " is_invoice = 1";
	}


	$listing->add_options_button('export_button', 'action=list_records_custumer_export', 'botoOpcions1 excel', 'top_after');



	if ( $gl_action == 'list_records_custumer_export' ) {
		custumer_export( $listing);
		return;
	}


    $listing->list_records();
    // sumo tpl's
    if ($gl_page->template != 'print.tpl') // ho posso pq en el print no m'inserti el tpl del search
        {
            $gl_content = get_search_content() . $gl_content;
    }
}
/* TODO-i No funciona els next i prev per tots els llistats de custumers */

function list_records_walk_1(&$listing,$custumer_id, $name)
{
	global $gl_action;

	// comprobo si te alguna solicitud
	$b = &$listing->buttons;

	if (isset($b['searchexact_button'])){ // nomes quan està definit el boto, (al buscador no està definit, no surtr mai)
		$show_search = !(
				($gl_action!='list_records_buyers')
			 	&& ($gl_action!='list_records_sellers')
				&& (!Db::get_first('SELECT count(*) FROM custumer__demand WHERE custumer_id='.$custumer_id))
			);
		$b['searchexact_button']['show']=$b['searchprox_button']['show']=$show_search;

	}

    // fer una consulta per els noms i els id's segons el custumer_id, el
    // recullo tots els id's de les propietas del client:
    $tmp_names = $tmp_ids = array();
    $query = "SELECT inmo__property.property_id AS property_id, ref, property_private
				FROM inmo__property
				INNER JOIN custumer__custumer_to_property
				USING (property_id)
				WHERE custumer_id = '" . $custumer_id . "'
				ORDER BY ref ASC" ;
    $results = Db::get_rows($query);
    foreach($results as $rs){
	    $nam = $rs['ref'] . ' - ' . $rs['property_private'];
    	$tmp_names[]= htmlspecialchars($nam,ENT_QUOTES);
    	$tmp_ids[]= $rs['property_id'];
    }

    $ids = implode (',', $tmp_ids); //creo un text resultat de tots els ids seprats per comes
    $names = implode ('#;#', $tmp_names);

    return array('hidden' => '<input name="selected_addresses_ids" type="hidden" value="' . $ids . '"><input name="selected_addresses_names" type="hidden" value="' . $names . '">');
}

function show_form()
{
    global $gl_action, $gl_content, $tpl;
	$old_message = $GLOBALS['gl_message']; // el list_property_of_custumer s'ho carrega, el problema del v2
    switch ($gl_action)
    {
        case "show_form_what":
            show_form_what();
            break;
        default:
            $content = get_search_content();
            $show = new ShowForm;
	
			$tpl->set_var('view_only_itself', inmo_view_only_itself());	
			if (inmo_view_only_itself()) $show->set_field ('user_id', 'type', 'hidden');	
			
			
			$show->get_values();			
	
			if ($show->rs && $show->rs['user_id']==0){
				$show->rs['user_id'] = $_SESSION['user_id'];
			}
			
			// es pot triar qualsevol idioma de la bbdd
			$show->set_field('prefered_language', 'select_condition', '');
			
            // form edit
            if (strstr($gl_action, "show_form_edit"))
            {
                $tpl->set_var('is_tool_custumer', true);
                $show->call('list_property_of_custumer', 'custumer_id,name');
            }
            // form new
            else
            {
                $tpl->set_var('is_tool_custumer', false);
                $tpl->set_var('hidden', false);
            }
            $show->set_vars_common();
            $show->show_form();
            $gl_content = $content . $gl_content;
            break;
    } // switch
	$GLOBALS['gl_message'] = $old_message;

}

function list_records_history () {

	// TODO-i Comporvar si està ben que s'agrupi per message_id i propietats - No, s'agrupa per missatges i custumers i propietats
	// TODO-i  Quan s'edita es canvien tots, si han d'anar plegats sempre, també ho hauria d'anar al guardar des de l'inmoble - CONTROLAR AL GUARDAR DE TOT ARREU - DILLUNS
	// potser un message_id repetit sense property_id?

	$custumer_id = R::id('custumer_id');
	$module = Module::load('custumer','history');
	$module->parent = 'custumer__custumer';
	$module->custumer_id = $custumer_id;
	$content = $module->do_action('get_records');
    show_form_history($content);
}

function show_form_history($content)
{
    global $tpl, $gl_content, $gl_caption, $gl_message;
    $custumer_id = R::id('custumer_id');
	// sino puc estar editant aquesta propietat, em fa fora directe
	inmo_check_user_permission($custumer_id, 'custumer__custumer', 'custumer_id');

    $query = "SELECT custumer_id, name, surname1, surname2  FROM custumer__custumer WHERE custumer_id = " . $custumer_id;

    $rs = Db::get_row($query);

    $show_form = new ShowForm;
    $show_form->set_vars_common();
    $show_form->set_vars_form();
    $tpl->set_vars($rs);
    $tpl->set_vars($show_form->rs);
    if (!$content)
    {
        $gl_message = '';
    }
    $tpl->set_var('content', $content);
    $tpl->set_file('custumer/custumer_history.tpl');
    $gl_content = $tpl->process();
    show_search_form();
}

// Les Funcions seguents son a custumer_functions.php:
//      list_property_of_custumer
//      list_demands_of_custumer
//

function show_form_what ()
{
    // funcio on es configurar el client
    global $tpl, $gl_content, $gl_caption;
    // recollo el custumer_id:
    $query = "SELECT custumer_id, name, surname1, surname2 FROM custumer__custumer WHERE custumer__custumer.bin != 1 ORDER BY custumer_id ASC";
    $results = Db::get_rows($query);
    $total_row = 0;
    for ($i = 0; $i < count($results); $i++)
    {
        $total_row = $total_row + 1;
    }
    $total_row = $total_row - 1;
    // El client que estic editant sempre s'era el últim que entro
    $name = $results [$total_row] ['name'];
    $surname1 = $results [$total_row] ['surname1'];
    $surname2 = $results [$total_row] ['surname2'];
    $custumer_id = $results [$total_row] ['custumer_id'];

    $show = new ShowForm;
	
    $show->set_vars_common();
    $template = 'custumer/what.tpl';
    $tpl->set_file($template);

    $tpl->set_var('name', $name);
    $tpl->set_var('surname1', $surname1);
    $tpl->set_var('surname2', $surname2);
    $tpl->set_var('custumer_id', $custumer_id);

    $gl_content = $tpl->process();
}
function write_record()
{
    $writerec = new SaveRows;	
    $writerec->field_unique = array("mail","vat");
    $writerec->save();
	
	// poso el remove key automaticament
	if ($GLOBALS['gl_action']=='add_record' && $GLOBALS['gl_insert_id'])
		newsletter_add_remove_key('custumer__custumer', $GLOBALS['gl_insert_id']);
	
}
function get_search_content()
{
    global $tpl, $gl_caption, $gl_menu_id;
    $template = 'custumer/custumer_search.tpl';
    $tpl->set_file($template);
    // POSSO EL menu_id i el section per posar-lo al from, aixi al fer el llistat segueixes a aquest menu
    $tpl->set_var('menu_id', $gl_menu_id);
	$tpl->set_var('q',  htmlspecialchars(R::get('words')));
    // set_var = agafa una sola variable // set_loop = fa un loop
    $tpl->set_vars($gl_caption);
    return $tpl->process();
}

function manage_images()
{
    // $image_manager = new ImageManager;
    // $image_manager->execute();
}
function save_rows()
{
    global $gl_action, $gl_reload, $gl_message, $gl_messages;

    if ($gl_action == 'save_rows_selected')
    {Debug::add('post',$_POST);
        save_assign_properties($_POST['selected_addresses_ids'], $_POST['custumer_id']);
		print_javascript("top.show('propertys');");
		set_inner_html('propertys',list_property_of_custumer ($_POST['custumer_id'], '', true));
		$gl_message = $gl_messages['assigned'];
    }
	else{
		$save_rows = new SaveRows;
		$save_rows->field_unique = "vat";
		$save_rows->save();
	}
}
function save_assign_properties ($properties, $custumer_id)
{
    global $gl_action, $gl_message, $gl_messages;
    // abans d'insertar les propietats tinc que eliminar les que ja tingui, així si n'ha borrat alguna desapareixera. Les borro totes i ja esta
    $query2 = "DELETE FROM custumer__custumer_to_property where custumer_id = '" . $custumer_id . "'";
    Db::execute ($query2);

    if ($properties)
    {
        $properties = explode(',', $properties);
        $numproperties = count ($properties);
        for ($i = 0;$i < $numproperties;$i++)
        {
            $query = "INSERT INTO custumer__custumer_to_property (custumer_id, property_id ) VALUES ( " . $custumer_id . " , " . $properties[$i] . ")";
            Debug::add('query assign custumer', $query);
            Db::execute($query);
        }
    }
    $gl_message = $gl_messages['saved'];
}

function newsletter_add_remove_key($table, $id) {	
	
		$table_id = strpos($table, "customer")!==false?'customer_id':'custumer_id';
	
		$q2 = "SELECT count(*) FROM ". $table . " WHERE BINARY remove_key = ";
		$q3 = "UPDATE ". $table . " SET remove_key='%s' WHERE  ". $table_id . " = '%s'";
		
		$updated = false; 
		while (!$updated){
			$pass = get_password(50);
			$q_exists = $q2 . "'".$pass."'";	
			
			// si troba regenero pass
			if(Db::get_first($q_exists)){
				$pass = get_password(50);							
			}
			// poso el valor al registre
			else{				
				$q_update = sprintf($q3, $pass, $id);
				
				Db::execute($q_update);
				break;
			}
		}
}



function custumer_export($listing)
{
	ini_set('max_execution_time',120);
	ini_set('memory_limit', '1024M');

    Main::load_class('excel');


	$column_formats = array (
		'cif' => 'FORMAT_TEXT',
		'vat' => 'FORMAT_TEXT',
		'phone1' => 'FORMAT_TEXT',
		'phone2' => 'FORMAT_TEXT',
		'phone3' => 'FORMAT_TEXT',
		'phone4' => 'FORMAT_TEXT',
		'compta' => 'FORMAT_TEXT',
	);

	$listing->limit = 5000;

	$listing->list_records( false );

	$excel = new Excel( $listing );
	$excel->set_visible_fields( [], ['list_admin', 'form_admin'] )
	      ->show_fields( 'custumer_id' )
	      ->column_formats( $column_formats )
	      ->file_name( 'customers' )
	      ->make( $listing->results );
}