<?php
// menus eina
if (!defined('RACING_MENU_RALLY')){
	define('RACING_MENU_RALLY', 'Rallis');
	define('RACING_MENU_RALLY_NEW', 'Entrar nou ralli');
	define('RACING_MENU_RALLY_LIST', 'Llistar i editar rallis');
	define('RACING_MENU_RALLY_BIN', 'Paperera de reciclatge');

	define('RACING_MENU_CATEGORY', 'Categories');
	define('RACING_MENU_CATEGORY_NEW', 'Entrar nova categoria');
	define('RACING_MENU_CATEGORY_LIST', 'Llistar categories');

	define('RACING_MENU_SUPORT', 'Suport');
	define('RACING_MENU_SUPORT_NEW', 'Entrar nou suport');
	define('RACING_MENU_SUPORT_LIST', 'Llistar i editar suports');
	define('RACING_MENU_SUPORT_BIN', 'Paperera de reciclatge');

	define('RACING_MENU_VIDEO', 'Vídeos');
	define('RACING_MENU_VIDEO_NEW', 'Entrar nou vídeo');
	define('RACING_MENU_VIDEO_LIST', 'Llistar i editar vídeos');
	define('RACING_MENU_VIDEO_BIN', 'Paperera de reciclatge');
}

$gl_messages['no_category']='Para poder insertar noticias tiene que haber al menos una categoría';

// captions seccio
$gl_caption['c_prepare'] = 'En preparación';
$gl_caption['c_review'] = 'Para revisar';
$gl_caption['c_public'] = 'Pública';
$gl_caption['c_archived'] = 'Archivada';
$gl_caption['c_rally'] = 'Ralli';
$gl_caption['c_suport'] = 'Nombre';
$gl_caption['c_video'] = 'Nombre';
$gl_caption['c_title'] = 'Título';
$gl_caption['c_subtitle'] = 'Subtítulo';
$gl_caption['c_tema'] = $gl_caption['c_tema_id'] = 'Tema';
$gl_caption['c_content'] = 'Contenido';
$gl_caption['c_entered'] = 'Fecha publicación';
$gl_caption['c_expired'] = 'Fecha caducidad';
$gl_caption_rally['c_entered'] = 'Fecha del Rally';
$gl_caption['c_modified'] = 'Fecha modificación';
$gl_caption['c_status'] = 'Estado';
$gl_caption['c_category_id'] = 'Categoría';
$gl_caption['c_filter_category_id'] = 'Todas las categorías';
$gl_caption['c_filter_status'] = 'Todos los estados';
$gl_caption['c_filter_in_home'] = 'Todas en Home';
$gl_caption['c_filter_destacat'] = 'Todas las destacadas';
$gl_caption['c_ordre'] = 'Orden';
$gl_caption['c_filter_tema_id'] = 'Tots els temes';
$gl_caption['c_tema_select_caption'] = 'Cap tema';
$gl_caption['c_manual_id'] = 'Manual relacionat';
$gl_caption['c_manual_select_caption'] = 'Cap manual';

$gl_caption_category['c_category'] = 'Categoría';
$gl_caption_category['c_racing_button'] = 'Ver soportes';
$gl_caption['c_tema_button'] = 'Veure FAQs';
$gl_caption_category['c_ordre'] = 'Orden';
$gl_caption_image['c_name'] = 'Nombre';
$gl_caption['c_url1_name']='Nombre URL(1)';
$gl_caption['c_url1']='URL(1)';
$gl_caption['c_url2_name']='Nombre URL(2)';
$gl_caption['c_url2']='URL(2)';
$gl_caption['c_url3_name']='Nombre URL(3)';
$gl_caption['c_url3']='URL(3)';
$gl_caption['c_videoframe']='Vídeo (youtube, metacafe...)';
$gl_caption['c_file']='Archivos';
$gl_caption['c_in_home']='Sale en la home';
$gl_caption['c_in_home_0']='No sale en la Home';
$gl_caption['c_in_home_1']='Sale en la home';
$gl_caption['c_content_list']='Descripción corta en el listado';
$gl_caption['c_destacat']='Noticia destacada';
$gl_caption['c_destacat_0']='No destacada';
$gl_caption['c_destacat_1']='Destacada';

$gl_caption['c_rally_file_name'] = $gl_caption['c_suport_file_name'] = $gl_caption['c_video_file_name'] = $GLOBALS['gl_caption']['c_file_name'];
$gl_caption['c_rally_old_file_name'] = $gl_caption['c_suport_old_file_name'] = $gl_caption['c_video_old_file_name'] = $GLOBALS['gl_caption']['c_old_file_name'];
$gl_caption['c_form_level1_title_page_title'] = $GLOBALS['gl_caption']['c_seo'];

$gl_caption['c_selected']='Noticias seleccionadas';
?>
