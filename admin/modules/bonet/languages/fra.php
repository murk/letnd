<?php
if (!defined('API_MENU_AGENCIA')){
	// menus eina	
	define('BONET_MENU_AGENCIA','Agències');
	define('BONET_MENU_AGENCIA_NEW','Nova agència');
	define('BONET_MENU_AGENCIA_LIST','Llistar agències');
	define('BONET_MENU_AGENCIA_BIN','Paperera de reciclatge');

	define('BONET_MENU_ONEAGENCIA','Agència');
	define('BONET_MENU_ONEAGENCIA_EDIT','Editar agència');
	define('BONET_MENU_ONEAGENCIA_LIST','Editar agents');
	define('BONET_MENU_ONEAGENCIA_LIST_FILES','La meva carpeta');
	define('BONET_COMMENT_ONEAGENCIA','Editar agència pròpia i agents pel grup "agents"');

	define('BONET_MENU_AGENTENQUESTA','Enquestes');
	define('BONET_MENU_AGENTENQUESTA_NEW','Enquesta de satisfacció');
	define('BONET_COMMENT_AGENTENQUESTA','Enquesta pel grup "agents"');

	define('BONET_MENU_AGENT','Agents');
	define('BONET_MENU_AGENT_NEW','Nou agent');
	define('BONET_MENU_AGENT_LIST','Llistar agents');
	define('BONET_MENU_AGENT_BIN','Paperera de reciclatge');

	define('BONET_MENU_AGENT_FILECAT_NEW','Nova categoria arxius');
	define('BONET_MENU_AGENT_FILECAT_LIST','Llistar categories arxius');

	define('BONET_MENU_SPECIALITY','Especialitats');
	define('BONET_MENU_SPECIALITY_NEW','Nova especialitat');
	define('BONET_MENU_SPECIALITY_LIST','Llistar especialitats');
	define('BONET_MENU_SPECIALITY_BIN','Paperera de reciclatge');

	define('BONET_MENU_PAGINA','Edició de pàgines');
	define('BONET_MENU_PAGINA_NEW','Entrar nova pàgina');
	define('BONET_MENU_PAGINA_NEW_SECOND','Entrar nova pàgina secundària');
	define('BONET_MENU_PAGINA_LIST','Llistar pàgines');
	define('BONET_MENU_PAGINA_LIST_SECOND','Llistar pàgines secundàries');
	define('BONET_MENU_PAGINA_BIN','Paperera de reciclatge');

	define('BONET_MENU_CURS','Cursos');
	define('BONET_MENU_CURS_NEW','Nou curs');
	define('BONET_MENU_CURS_LIST','Llistar cursos');
	define('BONET_MENU_CURS_BIN','Paperera de reciclatge');
}

/*--------------- nomès castellà i català, no traduit a la resta -------------------*/

/*agencies*/
$gl_caption_agencia['c_agencia'] = 'Agence';
$gl_caption_agencia['c_agents'] = 'Agents';
$gl_caption_agencia['c_user_ids'] = 'Utilisateurs';
$gl_caption_agencia['c_add_new_agent'] = 'Introduire agent';
$gl_caption_agencia['c_add_new_user'] = 'Introduire utilisateur';
$gl_caption_agencia['c_view_users'] = 'Voir utilisateurs';
$gl_caption_agencia['c_enquesta'] = 'enquête';

$gl_caption_agent['c_agencia'] = 'Agence';
$gl_caption_agent['c_agent'] = 'Agent';
$gl_caption_agent['c_agent_description'] = 'Description';
$gl_caption_agent['c_nom'] = 'Prénom';
$gl_caption_agent['c_cognoms'] = 'Nom';
$gl_caption_agent['c_adresa'] = 'Adresse';
$gl_caption_agent['c_adress'] = 'Rue';
$gl_caption_agent['c_numstreet'] = 'Número';
$gl_caption_agent['c_block'] = 'Bloc';
$gl_caption_agent['c_flat'] = 'Appartement';
$gl_caption_agent['c_door'] = 'Porte';
$gl_caption_agent['c_provincia_id'] = 'Province';
$gl_caption_agent['c_comarca_id'] = 'Comarca';
$gl_caption_agent['c_municipi_id'] = 'Ville';
$gl_caption_agent['c_cp'] = 'Code';
$gl_caption_agent['c_telefon'] = 'Téléphone';
$gl_caption_agent['c_telefon2'] = 'Téléphone 2';
$gl_caption_agent['c_fax'] = 'Fax';
$gl_caption_agent['c_correu'] = 'E-mail';
$gl_caption_agent['c_correu2'] = 'E-mail 2';
$gl_caption_agent['c_mail'] = 'E-mail newsletter';
$gl_caption_agent['c_url1'] = 'Web';
$gl_caption_agent['c_url1_name'] = 'Nom de la web';
$gl_caption_agent['c_codi_habitat'] = 'Code habitatsoft';
$gl_caption_agent['c_api_id'] = 'Nº API';
$gl_caption_agent['c_aicat_id'] = 'Nº AICAT';
$gl_caption_agent['c_nums'] = 'Identifiants';

$gl_caption_agent['c_speciality_id'] = 'Spécialités';
$gl_caption_agent['c_group_id'] = 'Grup newsletter';

$gl_caption['c_filecat_id'] = $gl_caption['c_filecat'] = "Categoria";
$gl_caption['c_ordre'] = "Ordre";
$gl_caption['c_form_files_title'] = "Documents de l'agent: ";
$gl_caption['c_edit_files'] = "Documents";
$gl_caption['c_file'] = "Documents";

$gl_caption_speciality['c_speciality'] = 'Spécialité';

$gl_caption['c_filter_actiu'] = 'Tous les agents';
$gl_caption['c_actiu'] = 'Travaille';
$gl_caption['c_actiu_1'] = 'Travaille';
$gl_caption['c_actiu_0'] = 'Ne travaille pas';

$gl_caption['c_filter_comarca_id'] = 'Toutes les comarques';
$gl_caption['c_filter_municipi_id'] = 'Tous les municipis';

$gl_caption['c_desactivar'] = 'Désactiver';
$gl_caption['c_activar'] = 'Activer';

// mapes
$gl_caption['c_edit_map'] = 'Localisation';
$gl_caption['c_search_adress']='Chercher adresse';
$gl_caption['c_latitude']='latitude';
$gl_caption['c_longitude']='longitude';
$gl_caption['c_found_direction']='adresse trouvée';
$gl_caption['c_save_map'] = 'Sauver localisation';
$gl_caption['c_click_map'] = 'Cliquez sur un point sur la carte pour introduire la localisation';
$gl_caption['c_form_map_title'] = 'Localisation';
$gl_messages['point_saved'] = 'Nouvelle localisation correctement enregistré';

/*pagines*/
$gl_caption_pagina['c_edit_title'] = 'Données de la page';
$gl_caption_pagina['c_page_title_title'] = 'Données de la page';
$gl_caption_pagina['c_column_title1'] = 'Contingut';
$gl_caption_pagina['c_column_title2'] = 'Deuxième colonne';
$gl_caption_pagina['c_column_title3'] = 'Troisième colonne';
$gl_caption_pagina['c_column_title4'] = 'Vídeo';
$gl_caption_pagina['c_column_title5'] = 'Google';
$gl_caption_pagina['c_column_title6'] = 'Plus d/information';

$gl_caption_pagina['c_breadcrumb_edit_level1'] = '"modifier la page"';
$gl_caption_pagina['c_breadcrumb_edit_level2'] = '"modifier page"';
$gl_caption_pagina['c_breadcrumb_edit_level3'] = '"modifier la page"';
$gl_caption_pagina['c_breadcrumb_edit_list'] = '"Liste des sections"';

$gl_caption_pagina['c_subtitle_level2'] = 'Liste des pages';
$gl_caption_pagina['c_subtitle_level3'] = 'Liste des pages';

$gl_caption_pagina['c_filter_status'] = 'Tous les états';
$gl_caption_pagina['c_filter_paginatpl_id'] = 'Tous les modèles';

$gl_caption_pagina['c_edit_content_button'] = 'Modifier la page';
$gl_caption_pagina['c_edit_pagina_button'] = 'Modifier les données';



$gl_caption_pagina['c_ordre'] = 'Ordre';
$gl_caption_pagina['c_entered'] = 'Créé';
$gl_caption_pagina['c_modified'] = 'Dernière modification';
$gl_caption_pagina['c_paginatpl_id'] = 'Modèle fiche';
$gl_caption_pagina['c_listtpl_id'] = 'Modèle liste';
$gl_caption_pagina['c_link'] = 'Link';
$gl_caption_pagina['c_pagina_file_name'] = $GLOBALS['gl_caption']['c_file_name'];
$gl_caption_pagina['c_body_supertitle'] = 'Supertitre';
$gl_caption_pagina['c_body_supertitle2'] = 'Supertitre';
$gl_caption_pagina['c_body_supertitle3'] = 'Supertitre';
$gl_caption_pagina['c_title'] = 'Titre';
$gl_caption_pagina['c_title2'] = 'Titre';
$gl_caption_pagina['c_title3'] = 'Titre';
$gl_caption_pagina['c_body_subtitle'] = 'Sous-titre';
$gl_caption_pagina['c_body_subtitle2'] = 'Sous-titre';
$gl_caption_pagina['c_body_subtitle3'] = 'Sous-titre';
$gl_caption_pagina['c_pagina'] = 'Nom de la page dans le menu';
$gl_caption_pagina['c_subpagina'] = 'Subsections';
$gl_caption_pagina_list['c_pagina'] = 'Nom';
$gl_caption_pagina['c_body_content'] = 'Contenu';
$gl_caption_pagina['c_body_content2'] = 'Contenu';
$gl_caption_pagina['c_body_content3'] = 'Contenu';
$gl_caption_pagina['c_prepare'] = 'En préparation';
$gl_caption_pagina['c_review'] = 'Pour réviser';
$gl_caption_pagina['c_public'] = 'Public';
$gl_caption_pagina['c_archived'] = 'Archivée';
$gl_caption['c_status'] = 'État';
$gl_caption_pagina['c_pagina_id'] = 'Id';
$gl_caption_pagina['c_parent_id'] = 'Appartient a';
$gl_caption_pagina['c_custom_id'] = 'Galerie de personnalisation';
$gl_caption_pagina['c_level'] = '';
$gl_caption_pagina['c_blocked'] = 'Block.';

$gl_caption_pagina['c_add_pagina_button'] = 'Introduire nouvelle page ici';
$gl_caption_pagina['c_list_subpagines'] = 'Liste tous';
$gl_caption_pagina['c_add_subpagina'] = 'Introduire';
$gl_caption_pagina['c_add_subpagina2'] = 'Introduire nouvelle subsection';
$gl_caption_pagina['c_url1_name']='Nom URL';
$gl_caption_pagina['c_url1']='URL';
$gl_caption_pagina['c_url2_name']='Nom URL(2)';
$gl_caption_pagina['c_url2']='URL(2)';
$gl_caption_pagina['c_url3_name']='Nom URL(3)';
$gl_caption_pagina['c_url3']='URL(3)';
$gl_caption_pagina['c_videoframe']='Vídeo (youtube, metacafe...)<br />( 340 x 255 )';
$gl_caption_pagina['c_file']='Fichiers';

$gl_caption['c_last_listing']='Liste anterieur';


$gl_caption_category['c_ordre']='Ordre';
$gl_caption_category['c_category']='Catégorie';
$gl_caption_category['c_is_clubone'] = 'Menú partenaires';

$gl_messages['c_restore_items'] = "Les articles marqués ne sont podut pas être récupéré parce qui'il doit d'abord récupérer la pagedont ils appartiennent";



/*banners*/
$gl_caption_banner['c_banner_id']='';
$gl_caption_banner['c_ordre']='Ordre';
$gl_caption_banner['c_url1']='Link';
$gl_caption_banner['c_url1_name']='Text au link';
$gl_caption_banner['c_url1_target']='Destin';
$gl_caption_banner['c_url1_target__blank']='Une nouvelle fenètre';
$gl_caption_banner['c_url1_target__self']='La mème fenètre';
$gl_caption_banner['c_file']='Image banner <br>(960x92 - 248x122)';
$gl_caption_banner['c_banner']='Banner';


$gl_caption_agentenquesta['c_send_new']='Envoyer enquête';
$gl_caption_agentenquesta['c_agencia_id']='Agence de la Propriété Immobilière';
$gl_caption_agentenquesta['c_nom_persona']='Nom de la personne qui respond l\'enquête';
$gl_caption_agentenquesta['c_entered']='Date';
$gl_messages['enquesta_sent']='L\'enquète a été envoyé correctement, merci pour votre participation';
$gl_messages['enquesta_not_sent']="Impossible d'envoyer l'enquête, s'il vous plaît essayer à nouveau";


// Cursos

$gl_caption_curs['c_prepare'] = 'En preparació';
$gl_caption_curs['c_archived'] = 'Arxivat';
$gl_caption_curs['c_content'] = 'Descripció';
$gl_caption_curs['c_entered'] = 'Creat';
$gl_caption_curs['c_modified'] = 'Modificat';
$gl_caption_curs['c_status'] = 'Estat';
$gl_caption_curs['c_subtitle'] = 'Subtítol';
$gl_caption_curs['c_videoframe'] = 'Vídeo';

$gl_caption['c_curs_file_name'] = $GLOBALS['gl_caption']['c_file_name'];
$gl_caption['c_curs_old_file_name'] = $GLOBALS['gl_caption']['c_old_file_name'];

$gl_caption_curs['c_filter_status'] = 'Tots els estats';

$gl_caption_curs['c_form_level1_title_curs']='Títols pàgina';
$gl_caption_curs['c_form_level1_title_timetable']='Característiques';
$gl_caption_curs['c_form_level1_title_file']='Arxius adjunts';
$gl_caption_curs['c_form_level1_title_page_title']='SEO';

// Públic
$gl_caption_curs['c_curs'] = $gl_caption['c_curs_id'] = 'Cours';
$gl_caption_curs['c_timetable'] = 'Horaires';
$gl_caption_curs['c_open'] = 'Ouvert';
$gl_caption_curs['c_full'] = 'Complet';
$gl_caption_curs['c_finished'] = 'Terminé';


$gl_caption['c_speciality_id'] = 'Especialitat';

$gl_caption_curs['c_start_dat'] = 'Date de début';
$gl_caption_curs['c_end_date'] = 'Data finale';
$gl_caption_curs['c_maximum_person'] = 'Maximum de personnes';
$gl_caption_curs['c_weekday_id'] = 'Jours';

$gl_caption_curs['c_durada'] = 'La durée';
$gl_caption_curs['c_modalitat'] = 'Modalité';
$gl_caption_curs['c_lloc'] = 'Place';
$gl_caption_curs['c_adressa'] = 'Adresse';
$gl_caption_curs['c_tipologia'] = 'Typologie';
$gl_caption_curs['c_convalidacio'] = 'Validations';
$gl_caption_curs['c_credit'] = 'Crédits';
$gl_caption_curs['c_temari'] = 'Temari';
$gl_caption_curs['c_ponent'] = 'Enseignats';
$gl_caption_curs['c_avaluacio'] = 'Système d\'évaluation';
$gl_caption_curs['c_preu'] = 'Prix';
$gl_caption_curs['c_descompte'] = 'Réduction';
$gl_caption_curs['c_titulacio'] = 'Qualification';
$gl_caption_curs['c_inscripcio'] = 'Enregistrement';

$gl_caption_curs['c_modalitat_none'] = "";
$gl_caption_curs['c_modalitat_presencial'] = "Presentiel";
$gl_caption_curs['c_modalitat_semi-presencial'] = "Semi-face à face";
$gl_caption_curs['c_modalitat_taller-practic'] = "Atelier pratique";
$gl_caption_curs['c_modalitat_online'] = "En ligne";