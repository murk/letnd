<?
/**
 * VREASY - Funcions i variables comuns per les 2 direccions d'exportació
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) Gener 2018
 * @version $Id$
 * @access public
 */


class AdminVreasyBookCommon {

	public $api_url = "https://api.vreasy.com";
	public static $vreasy_fields = [
		'book_id'     => 'id',
		'adult'       => 'adults',
		'child'       => 'kids',
		'price_book' => 'accommodation_cost',
		'property_id' => 'property_id',
		'date_in'     => 'checkin',
		'date_out'    => 'checkout',
		'status'    => 'status',
	];

	public static $vreasy_status = [
		'pre_booked' => 'UNCONFIRMED',
		'cancelled' => 'CANCELLED',
		'booked' => 'CONFIRMED',
		'confirmed_email' => 'CONFIRMED',
		'confirmed_post' => 'CONFIRMED',
		'contract_signed' => 'CONFIRMED',
	];

	public static $letnd_status = [
		'UNCONFIRMED' => 'pre_booked',
		'CANCELLED' => 'cancelled',
		'CONFIRMED' => 'booked',
		'ENQUIRY' => 'pre_booked',
		'UNAVAILABLE' => 'booked',
	];

	static public function get_letnd_fields() {

		$letnd_fields = [];

		foreach ( self::$vreasy_fields as $k => $v ) {
			$letnd_fields [ $v ] = $k;
		}

		return $letnd_fields;
	}

	static public function get_letnd_status() {

		$letnd_status = [];
		foreach ( self::$vreasy_status as $k => $v ) {
			$letnd_status [ $v ] = $k;
		}

		return $letnd_status;
	}

	static public function get_iso_date( $d ) {
		$date = new DateTime( $d );

		return $date->format( DateTime::ISO8601 );
	}
	static public function get_letnd_date ($d) {

		$date = new DateTime( $d );
		return $date->format( 'Y-m-d' );
	}
}


?>