<table border="0" cellspacing="0" cellpadding="0" width="100%" class="listRecord">
    <tr>
      <td class="listCaption">&nbsp;
      <?=strtoupper($c_ref)?></td>
      <td class="listCaption">
          &nbsp;
          <?=$c_product_title?></td>
      <td class="listCaption" align="right">
          <?=$c_quantity?>
          &nbsp;</td>
      <td class="listCaption" align="right">
          <?=$c_product_base?>
          &nbsp;</td>
      <td class="listCaption" align="right">
          <?=$c_product_total_base?>
          &nbsp;</td>
    </tr>
  <? foreach($loop as $l): extract ($l)?>
    <tr class="listItem<?=$odd_even?>" onmousedown="change_bg_color(this, 'Down', '<?=$id?>')" id="list_row_<?=$id?>">
      <td>
        &nbsp;
        <?=$ref?>
        &nbsp;</td>
      <td><? if ($product_title):?>
          <a href="/admin/?action=show_form_edit&menu_id=20004&product_id=<?=$product_id?>">
          <?=$product_title?>
          </a>
        <? endif //product_title?>
        &nbsp;</td>
      <td align="right">
          <?=$quantity?>
          &nbsp;</td>
      
      <td align="right">
          <?=$product_base?>
          <?=CURRENCY_NAME?>
          &nbsp; </td>
      <td align="right">
          <?=$product_total_base?> 
          <?=CURRENCY_NAME?>
         &nbsp; </td>
    </tr>
    <? endforeach //$loop?>
</table>
<table border="0" align="right" cellpadding="0" cellspacing="0" style="margin-top:10px;line-height:16px;">
  <tr>
    <td><?=$c_total_base?>&nbsp;&nbsp;<br /><?=$c_total_tax?>&nbsp;&nbsp;<br /><strong><?=$c_total_basetax?>
&nbsp;&nbsp;</strong></td>
    <td align="right"><?=$total_base?>&nbsp;<?=CURRENCY_NAME?>
    &nbsp;<br /><?=$total_tax?>&nbsp;<?=CURRENCY_NAME?>
    &nbsp;<br /><strong><?=$total_basetax?>&nbsp;<?=CURRENCY_NAME?>
    </strong>&nbsp;</td>
  </tr>
</table>