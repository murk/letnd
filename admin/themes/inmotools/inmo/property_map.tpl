<script language="JavaScript" src="/admin/jscripts/validacio.js?v=<?=$version?>"></script>
<script type="text/javascript" src="//maps.google.com/maps/api/js?key=AIzaSyAaIGZEloZFoSY8TuTDyL27MDbuDvW5AoM"></script>
<script type="text/javascript" src="/admin/jscripts/map.js?v=<?=$version?>"></script>
<script type="text/javascript">
var gl_is_default_point = <?=$is_default_point;?>;
var gl_latitude = <?=$latitude;?>;
var gl_longitude = <?=$longitude;?>;
var gl_center_latitude = <?=$center_latitude;?>;
var gl_center_longitude = <?=$center_longitude;?>;
var gl_zoom = <?=$zoom;?>;
var gl_menu_id =<?=$menu_id;?>;
var gl_tool ='<?=$tool;?>';
var gl_tool_section ='<?=$tool_section;?>';
var gl_id_field = '<?=$id_field;?>';
var gl_id =<?=$id;?>;
MAP_FRAME_TEXT_1 = '<?=addslashes($c_map_frame_1)?>';
MAP_FRAME_TEXT_2 = '<?=addslashes($c_map_frame_2)?>';
MAP_FRAME_TEXT_3 = '<?=addslashes($c_map_frame_3)?>';
MAP_FRAME_TEXT_4 = '<?=addslashes($c_map_frame_4)?>';
</script>
<div class="form">
<form name="theForm" method="post" action="" onsubmit="">
<div id="form_buttons_holder"><table id ="form_buttons">
	<tr>
		<td valign="bottom" class="buttons">
		
		<div class="right" style="margin-bottom:3px;"><a href="javascript:save_map()" class="btn_form_save">
		<?=$c_save_map?>
		</a></div>
		
		<div class="menu">
	
					<a href="?action=show_form_edit&menu_id=103&property_id=<?=$property_id?>"><?=$c_edit_data?></a>
			
					<a href="?action=list_records_images&menu_id=103&property_id=<?=$property_id?>"><?=$c_edit_images?></a>
						
					<?if($is_custumer_on):?>
					<a class="private" href="?menu_id=<?=$menu_id?>&property_id=<?=$property_id?>&action=list_records_custumer"><?=$c_edit_custumer?></a>
					<?endif //custumer_on?>

					<?if($is_history_on):?>
					<a class="private" href="?menu_id=<?=$menu_id?>&property_id=<?=$property_id?>&action=list_records_history"><?=$c_edit_history?></a>
					<?endif //is_history_on?>
		
					<div><?=$c_edit_map?></div>						

					<?if($is_booking_on):?>
					<span id="tipus_temp_link" style="display:<?=$show_tipus_temp?>;">
						<a style="display:<?=$show_tipus_temp?>;" class="sep" href="?menu_id=<?=$menu_id?>&property_id=<?=$property_id?>&action=show_form_new&process=booking"><?=$c_edit_booking?></a>
						
						<a style="display:<?=$show_tipus_temp?>;" class="private" href="?menu_id=<?=$menu_id?>&property_id=<?=$property_id?>&action=show_form_edit&process=booking_propertyrange"><?=$c_edit_booking_season?></a>
					
						<a style="display:<?=$show_tipus_temp?>;" class="private" href="?menu_id=<?=$menu_id?>&property_id=<?=$property_id?>&action=show_form_edit&process=booking_extra"><?=$c_edit_booking_extra?></a>
					<span>
					<?endif //is_booking_on?>
				
				</div>
						</td>
	</tr>
</table></div>
</form>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td>
				<table width="100%" border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td>
								<table width="100%" cellpadding="4" cellspacing="0">
									<tr>
										<td class="formCaptionTitle"><strong>
											<?=$c_form_map_title?></strong>&nbsp;&nbsp;<?=$ref?><? if ($property_private):?> - <?=$property_private?> <? endif //property_private?>
										</td>
									</tr>
							</table>
							<table width="100%" cellpadding="4" cellspacing="0" class="formstbl">               
									<tr>
										<td align="center" valign="top" class="forms"><table width="99%" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td id="custumers"><table width="100%" border="0" cellspacing="0" cellpadding="0">
												<tr>
													<td align="center">
							<form onsubmit="return search_point()" class="google_map">
								<table cellpadding="0" cellspacing="0" border="0">
									<tr>
										<td>
											<strong class="formsCaption">
												<?=$c_search_adress?>: 
											</strong>
											<input id="map_address" name="map_address" type="text" value="<? if ($adress):?><?=$adress?> , <? endif //$adress?><?=$municipi?>" size="90" class="search" />
										</td>
										<td>
											<input type="submit" value="ok" class="boto-search" />
										</td>
										<td>
											
										</td>
									</tr>
								</table>
							</form>
							<div>
								<span class="show-map">
									<?=$show_map?> <?=$c_show_map?>
								</span>
								<span class="show-map">
									<?=$show_map_point?> <?=$c_show_map_point?>
								</span>
								<span class="formsCaption"><?=$c_latitude?>:&nbsp;</span>
								<span id="latitut"><?=$latitude?></span>&nbsp;&nbsp;&nbsp;
								<span class="formsCaption"><?=$c_longitude?>:&nbsp;</span>
								<span id="longitut"><?=$longitude?></span>
								
								<span class="formsCaption center-coords"><?=$c_center_coords?>:&nbsp;</span>
								<span id="center_latitut"><?=$center_latitude?></span> ,
								<span id="center_longitut"><?=$center_longitude?></span>
							</div>
							<div id="google_map"></div>
							</td>
												</tr>
											</table></td>
											</tr>
										</table>
										</td>
									</tr>
							</table></td>
					</tr>
				</table></td>
		</tr>
</table>
<table width="100%" cellpadding="4" cellspacing="2" class="formsButtonstbl">
	<tr>
		<td align="center" class="formsButtons"><div class="centerFloat1">
												<div class="centerFloat2"><a href="javascript:save_map()" class="btn_form_save">
		<?=$c_save_map?>
		</a></div></div></td>
	</tr>
</table>
</div>
