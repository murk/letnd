<article>
	<h2>Livraison de nos produits.</h2>
	<p>Les commandes seront livrées sur une période de 3 à 14 jours ouvrables * à partir de la date d'achat.</p>
	<p>Si c'est le cas, vous êtes informé par e-mail de tout changement ou des problèmes que nous avons trouvé sur la disponibilité, le prix, le paiement ou la livraison du produit.	  
    </p>
	<p>Si vous envoyez une commande et adressez l'entrée d'utilisateur incorrect, les frais de transport supplémentaires seraient payés par l'acheteur.	  
    </p>
	<p>* Jours fériés et week-ends ne sont pas considérés comme des jours ouvrables.
    </p>
</article>