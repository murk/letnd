<?
// funcions comunes de public i admin per inmo
// 
// condicio per llistar items de l'usuari
function inmo_get_user_permission($condition) {


	if (!$_SESSION['edit_only_itself']) return false;
	
	$ret = '';
	if ($condition) $ret = ' AND ';
	
	$is_api = is_table('api__agent');
	
	if ($is_api) {
		$user_ids = Db::get_rows("SELECT user_id FROM user__user WHERE agencia_id = 
						(SELECT agencia_id FROM user__user WHERE user_id = ".$_SESSION['user_id']." )");
		
		$ret .= 'user_id IN (' . implode_field($user_ids, 'user_id') . ')';
		
		Debug::add('Permisos query', $ret);
		return $ret;
	}
	else{
		$ret = $ret . 'user_id = ' . $_SESSION['user_id'];
		Debug::add('Permisos query', $ret);
		return $ret;	
	}
}

// comproba si pot està fent una acció en concret, sino para tot
// al editar formulari
// al guardar formulari
// al guardar llistat
// puc passar table i id field automaticament ( desde history ) 
function inmo_check_user_permission($id, $table='',$id_field='') {
	
	if (!$_SESSION['edit_only_itself']) return false;
	
	global $gl_db_classes_table, $gl_db_classes_id_field, $gl_messages, $gl_action;
	
	// sempre es pot entrar nova propietat
	if (strpos($gl_action, "show_form_new")!==false || strpos($gl_action, "add_record")!==false) return false;
	
	
	if (!$table) $table = $gl_db_classes_table;
	if (!$id_field) $id_field = $gl_db_classes_id_field;

	$condition = inmo_get_user_permission('');
	
	$query = "SELECT count(*) FROM " . $table . " WHERE ".$id_field." IN (".$id . ") AND " . $condition;
	$has_permission = Db::get_first($query);
	
	Debug::add('Permisos Check', $query);
	
	if (!$has_permission){
		$_SESSION['message'] = $gl_messages['concession_no_acces'];
		print_javascript("top.window.location.href='/admin/'");
		die();
	}
	
}

// condicio per llistar items de l'usuari des de history
function inmo_history_get_user_permission($condition) {
	
	if (!$_SESSION['edit_only_itself']) return false;
	
	$ret = '';
	if ($condition) $ret = ' AND ';
	
	$condition = inmo_get_user_permission('');
	
	if ($condition) {
		$condition = Db::get_rows("SELECT custumer_id FROM custumer__custumer WHERE " . $condition);
		
		if ($condition) $condition = implode_field($condition,'custumer_id'); else $condition = "''";
		$ret .= 'custumer_id IN (' . $condition . ')';
	};
	return $ret;
}

function inmo_view_only_itself() {
	return $_SESSION['edit_only_itself'];
}
?>