<?
Main::load_class( 'lassdive', 'common', 'admin' );
Main::load_class( 'lassdive', 'reserva_search', 'admin' );
Main::load_class( 'lassdive', 'timetable_common', 'admin' );

/**
 * LassdiveTimetableCommon
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2019
 * @version $Id$
 * @access public
 *
 *
 * TODO-i Al clicar sobre una reserva poder veure-la o editar
 * TODO-i Poder realitzar la reserva des d'aquí
 * TODO-i A setmanes fer la tria de date per setmana, i al premer seguent que mostri la pròxima
 * TODO-i Al clicar activitat al cercador que canvii els centres
 * Posar fix el dia de setmana
 *
 */
class LassdiveTimetableMain {

	public $is_public = true, $view, $caption = [], $error;
	private $search, $condition, $module, $module_caption, $module_messages, $action, $start_date, $end_date, $get_days_end_date, $required_date;

	public function __construct( &$module ) {

		LassdiveCommon::set_locale();

		$this->module          = &$module;
		$this->module_caption  = &$module->caption;
		$this->module_messages = &$module->messages;
		$this->action          = $module->action;

	}


	public function get_timetable() {

		$this->required_date = unformat_date( R::get( 'timetable_date' ) );

		$search = New LassdiveReservaSearch( $this->module, 'get_records' );
		$search->init_timetable_search(); // TODO-i Es necessari aqui?
		// $this->condition = $search->get_timetable_search_condition();

		$this->{"set_first_and_last_day_{$this->view}"}();

		$vars = $this->get_table();

		json_end( $vars );

	}

	public function set_first_and_last_day_week() {

		$date = new DateTime( $this->required_date );
		$date->modify( 'monday this week' );
		$this->start_date = $date->format( 'Y/m/d' );

		$date = new DateTime( $this->required_date );
		$date->modify( 'monday next week' );
		$this->end_date = $date->format( 'Y/m/d' );
		$this->get_days_end_date = $this->end_date;
	}

	public function set_first_and_last_day_month() {

		$date = new DateTime( $this->required_date );
		$date->modify( 'first day of this month' );
		$date->modify( 'monday this week' );
		$this->start_date = $date->format( 'Y/m/d' );
		$date->modify( 'monday next week' );
		$this->get_days_end_date = $date->format( 'Y/m/d' );

		$date = new DateTime( $this->required_date );
		$date->modify( 'last day of this month' );
		$date->modify( 'monday next week' );
		$this->end_date = $date->format( 'Y/m/d' );
	}

	public function get_table() {

		$error      = $html = '';
		$product_id = R::get( 'product_id' );
		$centre_id  = R::get( 'centre_id' );
		$user_id    = R::get( 'user_id' );

		$timetables = $this->get_timetables_array( $product_id, $centre_id, $user_id, $this->required_date );

		if ( ! $timetables ) $error = $this->error;

		$tpl = new phemplate( PATH_TEMPLATES, "lassdive/timetable_{$this->view}.tpl" );
		$tpl->set_vars( $this->module->caption );
		$days = LassdiveTimetableCommon::get_days( $this->start_date, $this->get_days_end_date );
		$tpl->set_vars( [
			'can_select_lower_date' => LassdiveCommon::is_miquel() ? 'true' : 'false',
			'days'                  => $days['days'] ,
			'month_name'                  => $days['month_name'] ,
		] );

		if ( ! $error ) {
			if ($this->view == 'week') {
				$tpl->set_loop( 'timetables', $timetables['timetables'] );
				$tpl->set_loop( 'no_time_reservas', $timetables['no_time_reservas'] );
			}
			else {
				$tpl->set_loop( 'timetables', $timetables );
			}
			$html = $tpl->process();
		}

		return [ 'html' => $html, 'error' => $error ];
	}

	public function get_timetables_array( $product_id, $centre_id, $user_id, $timetable_date ) {


		$centres = $this->view == 'week' ? $this->get_centre( $product_id, $centre_id ) : false;

		$timetable_common = new LassdiveTimetableCommon();

		$timetable_common->start_date = $this->start_date;
		$timetable_common->end_date   = $this->end_date;

		if ( $this->view == 'week' ) {
			$hours      = $timetable_common->get_hours();
			$timetables = $timetable_common->get_busy_hours( $hours, $centres, $product_id, 0, $timetable_date );
		}
		else {
			$timetables = $timetable_common->get_busy_days( $product_id, $timetable_date, $centre_id );
		}

		return $timetables;
	}


	/**
	 * @param $product_id
	 *
	 *
	 * @param $centre_id
	 *
	 * @return array Centres que tenen el producte i quantitat de producte i que estan públics
	 */
	private function get_centre( $product_id, $centre_id ) {

		$query = "
			SELECT *,
			(SELECT centre_max_quantity 
				FROM lassdive__centreproduct AS t2
				WHERE product_id = $product_id
				AND t1.centre_id = t2.centre_id) AS centre_max_quantity
			FROM lassdive__centre AS t1
			WHERE status = 'public'
			AND centre_id = $centre_id ";

		$results = Db::get_rows( $query );

		// Si el centre no te centre_max_product es que no es podia triar, s'hauria de controlar quan es fa el search

		return $results;
	}


	// Contingut al entrar a la pàgina del cercador, mostra el nav però no el calendari,
	// aquest es carrega després per ajax
	public function get_main_frame() {

		$search = New LassdiveReservaSearch( $this->module, 'get_records' );
		$search->init_timetable_search();

		$tpl            = new phemplate( PATH_TEMPLATES );
		$timetable_date = date( 'Y/m/d' );

		$tpl->set_file( 'lassdive/timetable_list.tpl' );
		$tpl->set_vars( $this->module_caption );

		$tpl->set_vars( [
			'action'                => $this->action,
			'view'                  => $this->view,
			'timetable_date'        => format_date_form( $timetable_date ),
			'timetable_date_js'     => $timetable_date,
			'can_select_lower_date' => LassdiveCommon::is_miquel() ? 'true' : 'false',
		] );

		$content        = $tpl->process();
		$search_content = $search->get_timetable_search_form( $timetable_date );
		if ( $this->is_public ) {
			$GLOBALS['gl_page']->set_var( 'reserva_search', $search_content );
			$GLOBALS['gl_content'] = $content;
		}
		else {
			$GLOBALS['gl_content'] = $search_content . $content;
		}

	}

	public function get_select_centre() {

		$product_id = R::get( 'product_id' );
		$family_id = 0;

		if ( substr( $product_id, 0, 7 ) == 'family-' ) {
			$family_id = substr( $product_id, 7 );
		}

		// TODO-i Text
		if ( ! $product_id ) json_end( [ 'error' => 'No s\'ha seleccionat cap producte' ] );

		$q = new Query( 'lassdive__centreproduct' );
		if ( $family_id ) {
			$condition  = 'product_id IN ( SELECT product_id FROM product__product WHERE family_id = %s OR subfamily_id = %s)';
			$centre_ids = $q->get_imploded( 'centre_id', $condition, ',', $family_id, $family_id );
		}
		else {
			$centre_ids = $q->get_imploded( 'centre_id', 'product_id = %s', ',', $product_id );
		}

		if ( ! $centre_ids )
			json_end( [ 'error' => 'No hi ha cap centre que ofereixi aquest producte' ] );


		$q       = new Query( 'lassdive__centre' );
		$results = $q->get( 'centre_id, centre', "centre_id IN ($centre_ids)" );

		$html = '';
		foreach ( $results as $rs ) {
			$html .= "<option value='{$rs['centre_id']}'>{$rs['centre']}</option>";
		}
		$html = "<select id='centre_id'><option value=''>Centre</option>$html</select>";

		json_end( [
			'error' => '',
			'html'  => $html
		] );
	}
}