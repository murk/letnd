<?php
// menus eina
if (!defined('INVOICE_MENU_INVOICE')) {
	define( 'INVOICE_MENU_INVOICE', 'Factures' );
	define( 'INVOICE_MENU_INVOICE_NEW', 'Crear Factures' );
	define( 'INVOICE_MENU_INVOICE_LIST', 'Factures emeses' );
	define( 'INVOICE_MENU_INVOICE_LIST_NO_BATCH', 'Factures no emeses' );
	define( 'INVOICE_MENU_INVOICE_BIN', 'Paperera de reciclatge' );

	define( 'INVOICE_MENU_CONCEPT', 'Conceptes' );
	define( 'INVOICE_MENU_CONCEPT_NEW', 'Crear concepte' );
	define( 'INVOICE_MENU_CONCEPT_LIST', 'Llistar conceptes' );
	define( 'INVOICE_MENU_CONCEPT_BIN', 'Paperera de reciclatge' );

	define( 'INVOICE_MENU_CONCEPT_CATEGORY', 'Categoria de conceptes' );
	define( 'INVOICE_MENU_CONCEPT_CATEGORY_NEW', 'Crear categoria' );
	define( 'INVOICE_MENU_CONCEPT_CATEGORY_LIST', 'Llistar categoria' );
	define( 'INVOICE_MENU_CONCEPT_CATEGORY_BIN', 'Paperera de reciclatge' );

	define( 'INVOICE_MENU_BATCH', 'Remeses' );
	define( 'INVOICE_MENU_BATCH_NEW', 'Crear remesa' );
	define( 'INVOICE_MENU_BATCH_LIST', 'Llistar remeses' );
	define( 'INVOICE_MENU_BATCH_BIN', 'Paperera de reciclatge' );

	define( 'INVOICE_MENU_RECURRING', 'Factures recurrents' );
	define( 'INVOICE_MENU_RECURRING_NEW', 'Nova recurrent' );
	define( 'INVOICE_MENU_RECURRING_LIST', 'Llistar recurrents' );
	define( 'INVOICE_MENU_RECURRING_BIN', 'Paperera de reciclatge' );
	define( 'INVOICE_MENU_RECURRING_GENERATE', 'Generar factures' );

	define( 'INVOICE_MENU_EXPENSE', 'Despeses' );
	define( 'INVOICE_MENU_EXPENSE_NEW', 'Nova despesa' );
	define( 'INVOICE_MENU_EXPENSE_LIST', 'Llistar despeses' );
	define( 'INVOICE_MENU_EXPENSE_BIN', 'Paperera de reciclatge' );

	define( 'INVOICE_MENU_EXPENSERECURRING', 'Despeses recurrents' );
	define( 'INVOICE_MENU_EXPENSERECURRING_NEW', 'Nova recurrent' );
	define( 'INVOICE_MENU_EXPENSERECURRING_LIST', 'Llistar recurrents' );
	define( 'INVOICE_MENU_EXPENSERECURRING_BIN', 'Paperera de reciclatge' );

	define( 'INVOICE_MENU_GRAPH', 'Gràfics' );
	define( 'INVOICE_MENU_GRAPH_LIST', 'Facturació' );

}

// captions seccio
$gl_caption['c_custumer_id']='Client';
$gl_caption['c_entered']='Creada';
$gl_caption['c_modified']='Modificada';
$gl_caption['c_invoice_number']='Número de factura';
$gl_caption_list['c_invoice_number']='Núm';
$gl_caption['c_custumer_id']='Client';
$gl_caption['c_company']='Empresa';
$gl_caption['c_comercial_name']='Nom comercial';
$gl_caption['c_name']='Nom';
$gl_caption['c_custumer_billing_mail']='mail';
$gl_caption['c_billing_type']='Forma de pagament';
$gl_caption['c_billing_type_bank_receipt']='Rebut bancari';
$gl_caption['c_billing_type_wire_transfer']='Transfarència bancària';
$gl_caption['c_billing_type_bank_draft']='Xec bancari';
$gl_caption['c_billing_type_cash']='Efectiu';
$gl_caption['c_bill_date']='Data';
$gl_caption['c_bill_due']='Venciment';
$gl_caption['c_invoice_serial']='Serie de facturació';
$gl_caption['c_invoiceconcepts']='Conceptes';
$gl_caption ['c_custumer_id_format']='%s %s %s - %s - %s';
$gl_caption['c_comta']='IBAN';
$gl_caption['c_price']='Preu sense I.V.A';
$gl_caption['c_total_cost_price_no_vat'] = "Cost sense I.V.A.";
$gl_caption['c_total_price_no_vat'] = "Total sense I.V.A.";
$gl_caption['c_total_vat'] = "I.V.A.";
$gl_caption['c_total_price'] = "Total I.V.A. inclòs";

$gl_caption['c_custumer_surname1'] = "Primer cognom";
$gl_caption['c_custumer_surname2'] = "Segon cognom";
$gl_caption['c_custumer_vat'] = $gl_caption['c_custumer_cif'] = "Número document";
$gl_caption['c_custumer_vat_type'] = $gl_caption['c_custumer_cif_type'] = "Tipus document";
$gl_caption['c_custumer_adress'] = "Adreça";
$gl_caption['c_custumer_numstreet'] = "Num.";
$gl_caption['c_custumer_block'] = "Bloc";
$gl_caption['c_custumer_flat'] = "Pis";
$gl_caption['c_custumer_door'] = "Porta";
$gl_caption['c_custumer_town'] = "Localitat";
$gl_caption['c_custumer_province'] = "Provincia";
$gl_caption['c_custumer_zip'] = "Codi postal";
$gl_caption['c_custumer_billing'] = "Empresa / Autònom";
$gl_caption['c_dates_from'] = "Del";
$gl_caption['c_dates_to'] = "al";
$gl_caption['c_quarter'] = "Trimestre";


$gl_caption['c_get_pdf_button']='Imprimir';
$gl_caption_batch['c_get_pdf_button']='Imprimir factures';
$gl_caption['c_get_sepa_button']='SEPA';
$gl_caption_batch['c_get_sepa_button']='Rebuts SEPA';
$gl_caption['c_view_invoices_button']='Llistar factures';
$gl_caption['c_send_mail_button']='Enviar factura';
$gl_caption_batch['c_send_mail_button']='Enviar factures';
$gl_caption['c_get_facturae_button']='Electrònica';


$gl_caption['c_ordre']='Ordre';
$gl_caption['c_category_id']='Categoria';
$gl_caption['c_concept']='Concepte';
$gl_caption['c_concept_price']='Preu';
$gl_caption_concept['c_short_cut']='Short cut';
$gl_caption['c_cost_price']='Preu de cost';
$gl_caption['c_quantity']='Quantitat';

$gl_caption['c_category']='Categoria';

$gl_caption_batch['c_batch'] = 'Remesa';
$gl_caption_batch['c_batch_date'] = 'Data';
$gl_caption_batch['c_created'] = 'Creat';
$gl_caption_batch['c_mandate_sent'] = 'Rebuts enviats';
$gl_caption_batch['c_batch_total_no_vat'] = 'Total';
$gl_caption_batch['c_batch_total'] = 'Total + IVA';
$gl_caption_batch['c_batch_vat'] = 'IVA';
$gl_caption_batch['c_batch_bank_receipt_total'] = 'Rebuts + IVA';
$gl_caption_batch['c_batch_wire_transfer_total'] = 'Transferències + IVA';
$gl_caption_batch['c_batch_cash_total'] = 'Efectiu + IVA';
$gl_caption_batch['c_sent_emails'] = 'Emails enviats';
$gl_caption_batch['c_create_batch'] = 'Crear nova remesa';
$gl_caption_batch['c_edite_batch'] = 'Editar remesa';


$gl_caption_recurring['c_recurringconcepts']='Conceptes';
$gl_caption_recurring['c_recurring_frequency'] = 'Cada';
$gl_caption_recurring['c_recurring_type'] = 'Període';
$gl_caption_recurring['c_recurring_type_week'] = 'Setmana(es)';
$gl_caption_recurring['c_recurring_type_month'] = 'Mes(os)';
$gl_caption_recurring['c_recurring_type_year'] = 'Any(s)';
$gl_caption_recurring['c_concept_category'] = "Categoria";
$gl_caption_recurring['c_concept_price'] = "Preu";

$gl_caption_recurring['c_generate_top_date'] = 'Data màxima de les factures generades';
$gl_caption_recurring['c_generate'] = 'Generar factures';
$gl_caption_recurring['c_next_bill_date'] = "Data facturació";
$gl_caption_recurring['c_generate_until'] = "Factures a generar fins el";
$gl_caption_recurring['c_generate_after'] = "Factures posteriors al";

$gl_caption_recurring['c_generated_correctly'] = "Factures generades";


$gl_caption_expense['c_expense'] = 'Concepte';
$gl_caption_expense['c_price'] = 'Preu';
$gl_caption_expense['c_expense_invoice_number'] = 'Núm factura';
$gl_caption_expense['c_irpf_percent'] = 'IRPF (%)';
$gl_caption_expense['c_total_irpf'] = 'IRPF';
$gl_caption_expense['c_total_price'] = 'Total';
$gl_caption_expense['c_vat_exempted'] = 'Exempt d\'IVA';
$gl_caption_expense['c_vat_included'] = 'IVA / IRPF inclòs';
$gl_caption_expense['c_is_deductible'] = 'És deduïble';
$gl_caption_expense['c_is_deductible_1'] = $gl_caption_expense['c_vat_included_1'] = $gl_caption_expense['c_vat_exempted_1'] = 'Sí';
$gl_caption_expense['c_is_deductible_0'] =  $gl_caption_expense['c_vat_included_0'] =$gl_caption_expense['c_vat_exempted_0'] = 'No';
?>