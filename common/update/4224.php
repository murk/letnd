<?php
// no es poden posar ; entre mig de la consulta ja que parteix les consultes per el ; a no ser que fem servir $update_sql en array()
	
if (!is_table("all__configadmin_page")){
	
	$update_sql[]="CREATE TABLE IF NOT EXISTS `all__configadmin_page` (
  `configadmin_page_id` int(11) NOT NULL auto_increment,
  `name` varchar(50) collate utf8_spanish2_ci NOT NULL,
  `value` varchar(255) collate utf8_spanish2_ci NOT NULL,
  PRIMARY KEY  (`configadmin_page_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;";
	
	$update_sql[]="
INSERT INTO `all__configadmin_page` (`configadmin_page_id`, `name`, `value`) VALUES
	(1, 'im_admin_thumb_w', '200'),
	(2, 'im_admin_thumb_h', '133'),
	(3, 'im_admin_thumb_q', '80'),
	(4, 'im_thumb_w', '200'),
	(5, 'im_thumb_h', '150'),
	(6, 'im_thumb_q', '80'),
	(7, 'im_details_w', '300'),
	(8, 'im_details_h', '225'),
	(9, 'im_details_q', '80'),
	(10, 'im_medium_w', '900'),
	(11, 'im_medium_h', '675'),
	(12, 'im_medium_q', '70'),
	(13, 'im_big_w', '1200'),
	(14, 'im_big_h', '900'),
	(15, 'im_big_q', '70'),
	(16, 'im_list_cols', '2'),
	(17, 'im_custom_ratios', '');
";	
}
	$update_sql[]="
ALTER TABLE `all__block`
	CHANGE COLUMN `vars` `vars` VARCHAR(150) NULL DEFAULT '' COLLATE 'utf8_spanish2_ci' AFTER `script`;
";	
?>