<?php
/**
 * UserUser
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2013
 * @version $Id$
 * @access public
 */
class UserUser extends Module{

	var $condition = '';
	var $q = false, $save_id, $user_id = false, $is_inmo = false;

	function __construct(){
		parent::__construct();
	}
	function on_load(){
		// crec que no es necessaria ja que si l'usuari ha entrat vol dir que almenys hi ha el grup d'aquest usuari
		$this->check_groups();

		$this->is_inmo =  Main::is_inmoletnd (); //is_table( 'inmo__property' );
		if ( $this->is_inmo ) {

			$this->form_tabs = array('0' => array('tab_action'=>'show_form_map','tab_caption'=>$this->caption['c_edit_map']));

		}
		else {
			$this->unset_field( 'language_id' );
			$this->unset_field( 'description' );
			$this->unset_field( 'status' );
			$this->unset_field( 'latitude' );
			$this->unset_field( 'longitude' );
			$this->unset_field( 'center_latitude' );
			$this->unset_field( 'center_longitude' );
			$this->unset_field( 'zoom' );
		}

		if ( isset( $this->config['related_tool_section'] ) && $this->config['related_tool_section'] ) {
			$tool        = explode( ',', $this->config['related_tool_section'] );
			$module_name = Main::load_class( $tool[0], $tool[1] );
			if ( method_exists( $module_name, 'user_user_on_load' ) ) {
				$module_name::user_user_on_load( $this->action, $this );
			}
		}
	}

	function on_end( $action ) {
		if ( isset( $this->config['related_tool_section'] ) && $this->config['related_tool_section'] ) {
			$tool        = explode( ',', $this->config['related_tool_section'] );
			$module_name = Main::load_class( $tool[0], $tool[1] );
			if ( method_exists( $module_name, 'user_user_on_end' ) ) {
				$module_name::user_user_on_end( $action, $this );
			}
		}
	}
	// la funcio get_group_condition l'he posat al user_user_config.php si no peta a admin total
	/*function get_group_condition()
	{
	// es veu el propi grup, es poden crear usuaris del propi grup o llistar usuaris del propi grup
		$group_condition = $_SESSION['group_id'] > 2?3:$_SESSION['group_id'];
		$group_condition = 'group_id >=' . $group_condition;
		return $group_condition;
	}*/

	function save_rows()
	{
		$save_rows = new SaveRows($this);
		$save_rows->field_unique = 'login';
		$this->save_id = $save_rows->id;
		$save_rows->save();
	}
	
	function list_records()
	{
		$content = $this->get_records();
		$GLOBALS['gl_content'] = $this->get_search_form() . $content;
	}

	function get_records()
	{
		$listing = new ListRecords($this);
		$listing->add_swap_edit();
		$listing->order_by = 'surname ASC';
		$listing->condition = get_group_condition();

		$listing->set_field( 'phone1', 'text_size', '8' );
		$listing->set_field( 'phone2', 'text_size', '8' );

		// search
		if ($this->condition) {
			$listing->condition .= ' AND ' . $this->condition;
		}
		if ( $this->is_inmo ) {

			$listing->add_button ('edit_map', 'action=show_form_map','boto1','after');
		}
		
	    return $listing->list_records();
		// condicio per la consulta perque no surti admin total
	}
	
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{
		if($this->user_id){
			$_GET['user_id'] = $this->user_id;
			$this->unset_field('group_id');
		}
		
		$show = new ShowForm($this);
		$query = "SELECT edit_only_itself FROM user__group WHERE group_id = " .$_SESSION['group_id'];
		if (Db::get_first($query)) {
			$show->unset_field('group_id');
		}
		$show->set_form_level2_titles('phone1');

		if ( $this->is_inmo ) {
			$show->set_form_level2_titles('description');
			$show->form_tabs = $this->form_tabs;
		}
		return $show->show_form();
	}
	// funcio que comprova si hi han grups creats per no deixar crear usuaris
	// crec que no es necessaria ja que si l'usuari ha entrat vol dir que almenys hi ha el grup d'aquest usuari
	function check_groups ()
	{
		global $gl_messages; //variable ja definida anteriorment al general d'idioma.
		// conto tots els resultats de grup_id descartan que sigui diferent a 1 o la papelera diferent a 0
		$query = "SELECT COUNT('group_id') AS count FROM `user__group` WHERE group_id <> 1";
		$results = Db::get_rows($query);
		if ($results[0]['count'] == '0')
		{
			$_SESSION['message'] = $gl_messages['Error_no_groups'];

			?>
		<script>window.location="/admin/?menu_id=7008";</script>
		<?php
			die();
		}
	}

	function write_record()
	{
		if($this->user_id){
			$writerec->id = $this->user_id;
		}
		
		$writerec = new SaveRows($this);
		$this->save_id = $writerec->id;
		
		
		
		$query = "SELECT edit_only_itself FROM user__group WHERE group_id = " .$_SESSION['group_id'];
		if (Db::get_first($query)) {
			$writerec->set_field('group_id','override_save_value',$_SESSION['group_id']);
		}

		if ($writerec->get_value('login') != '') {
			$writerec->field_unique = 'login';
		}
		$writerec->save();
	}

	function manage_images()
	{
		$image_manager = new ImageManager($this);
		$image_manager->form_tabs = $this->form_tabs;
		$image_manager->execute();
	}


	//
	// FUNCIONS BUSCADOR
	//
	function search(){

		$q = $this->q = R::escape('q');
		
		$search_condition = '';

		if ($q)
		{
			// Sempre busco a tot arreu, abans si detectava que eren refrencies no hi  buscava, posaré checkbox de "buscar nomès referencies"

			$search_condition = "(
				name like '%" . $q . "%'
				OR surname like '%" . $q . "%'
				OR CONCAT(`name`, ' ', `surname`) like '%" . $q . "%'
				OR login like '%" . $q . "%'
				OR email like '%" . $q . "%'
				" ;

			// busco a grups
			$query = "SELECT group_id FROM user__group WHERE (
								 name like '%" . $q . "%'
									)" ;
			$results = Db::get_rows_array($query);

			$search_condition .= $this->get_ids_query($results, 'group_id');
			$search_condition .= ") " ;

			$GLOBALS['gl_page']->title = TITLE_SEARCH . '<strong>&nbsp;&nbsp;"' . $q . '"</strong>';
		}
		$this->condition = $search_condition;
		Debug::add('Search condition', $this->condition);
		$this->do_action('list_records');
	}

	function get_ids_query(&$results, $field)
	{
		if (!$results) return '';
		$new_arr = array();
		foreach ($results as $rs){
			$new_arr [$rs[0]] = $rs[0];
		}
		return "OR " . $field . " IN (" . implode(',', $new_arr) . ") ";
	}

	function get_search_form(){
		$tpl = new phemplate(PATH_TEMPLATES);
		$this->caption['c_by_ref']='';
		$tpl->set_vars($this->caption);
		$tpl->set_file('search.tpl');
		$tpl->set_var('menu_id',$GLOBALS['gl_menu_id']);
		$tpl->set_var('q',  htmlspecialchars(R::get('q')));		
		
		if ($this->q) {
			$GLOBALS['gl_page']->javascript .= "
				$('table.listRecord').highlight('".addslashes(R::get('q'))."');			
			";
		}
		
		return $tpl->process();
	}



	/*
	 * MAPES
	 *
	 */

	function show_form_map (){ //esenyo el napa

		$user_id = R::id('user_id');

		$rs = Db::get_row("SELECT *, concat(name, ' ', surname ) AS title FROM user__user WHERE user_id = $user_id");
		$rs['is_default_point'] = ($rs['latitude']!=0)?0:1;
		$rs['center_latitude']=$rs['center_latitude']!=0?$rs['center_latitude']:GOOGLE_CENTER_LATITUDE;
		$rs['center_longitude']=$rs['center_longitude']!=0?$rs['center_longitude']:GOOGLE_CENTER_LONGITUDE;
		$rs['zoom']=$rs['zoom']?$rs['zoom']:GOOGLE_ZOOM;
		$rs['menu_id'] =  $_GET['menu_id'];
		$rs['id'] =  $rs[$this->id_field];
		$rs['id_field'] =  $this->id_field;
		$this->set_vars($rs);//passo els resultats del array
		$this->set_file('user/user_map.tpl'); //crido el arxiu
		$this->set_vars($this->caption); //passo tots els captions
		// $gl_news = $this->caption['c_click_map'];
		$content = $this->process();//processo
		$GLOBALS['gl_content'] = $content;
	}
	function save_record_map() {
		global $gl_page;
		extract( $_GET );

		if ( $latitude && $longitude ) {

			$query = "UPDATE `user__user` SET
				`latitude` = '$latitude',
				`longitude` = '$longitude',
				`center_latitude` = '$center_latitude',
				`center_longitude` = '$center_longitude',
				`zoom` = '$zoom'
				WHERE `user_id` =$user_id
				LIMIT 1";
			//Debug::p( $query );
			Db::execute ($query) ;
			$ret ['message'] = $this->messages['point_saved'];
		}
		else {
			$ret ['message'] = $this->messages['point_not_saved'];
		}

		$gl_page->show_message($this->messages['point_saved']);

	}
}
?>