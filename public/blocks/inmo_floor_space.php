<?php
$tipus = isset($vars['tipus'])?$vars['tipus']:false;
$page_id = isset($vars['page_id']) ? $vars['page_id'] : false;

include_once('inmo_block_functions.php');
$condition = '';
$condition .= inmo_block_get_tipus($tipus);

$vars['var_name'] = 'floorspaces';

if (!defined('INMO_MENU_FLOOR_SPACE')) Module::get_config_values('inmo');

// Link
$friendly_params = array();
if ($tipus) $friendly_params['tipus'] = $tipus==INMO_DEFAULT_TIPUS?'':$tipus;

if (USE_FRIENDLY_URL) {
	$friendly_params['floorspaces'] = '%s-%s';
}
else{
	$friendly_params['floorspace1'] = '%s';
	$friendly_params['floorspace2'] = '%s';
}

$link = get_link(array(
		'tool' => 'inmo',
		'tool_section' => 'property',
		'friendly_params' => $friendly_params,
		'link_vars' => array('tipus'=>$tipus),
		'detect_page' => $page_id?false:true,
		'page_id' => $page_id
));
// Link 			

$floor_spaces = explode(',', INMO_MENU_FLOOR_SPACE);
if ($floor_spaces) {
	
    $conta = 0;
    $previous_floor_space = 0;
    $floor_spaces_count = count($floor_spaces);
	
	$selected_floor_space_1 = R::text_id('floorspaces');
	if ($selected_floor_space_1){
		$selected_floor_space_1 = explode('-',$selected_floor_space_1);
		$selected_floor_space_1 = $selected_floor_space_1[0];
	}
	
	$vars['block_selected'] = $selected_floor_space_1?1:0;
	
    foreach($floor_spaces as $floor_space) {
        if ($conta == 1) {
            $l['item'] = sprintf(INMO_FLOOR_SPACE_FIRST, $floor_space);
        }
        if ($conta > 1) {
            $l['item'] = sprintf(INMO_FLOOR_SPACE_MIDDLE, $previous_floor_space, $floor_space);
        }
        if ($conta > 0) {
			if (USE_FRIENDLY_URL)
				$l['link'] = sprintf($link, $previous_floor_space, $floor_space);
			else
				$l['link'] = sprintf($link, $previous_floor_space, $floor_space);
				
            $l['selected'] = $selected_floor_space_1 === $previous_floor_space?1:0;
			$l['value'] = $previous_floor_space . '-'.$floor_space;	
            $loop2[] = $l;
        }
        $conta++;
        $previous_floor_space = $floor_space;
    }
	
	if (USE_FRIENDLY_URL)
		$l['link'] = sprintf($link, $floor_space, '');
	else
		$l['link'] = sprintf($link, $floor_space, '');
	
    $l['item'] = sprintf(INMO_FLOOR_SPACE_LAST, $floor_space);
    $l['selected'] = $selected_floor_space_1 == $previous_floor_space?1:0;
    $l['value'] =  $previous_floor_space . '-';
    $loop2[] = $l;

    $loop[] = array ('loop' => $loop2,
        'item' => '');
}
?>