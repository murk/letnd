<?
/**
 * PvSeguimentitem
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2011
 * @version $Id$
 * @access public
 */
class PvSeguimentitem extends Module{
	var $is_actiu;
	function __construct(){
		parent::__construct();
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function get_records()
	{
		// per defecte sempre actius
		if (!isset($_GET['is_actiu'])) $_GET['is_actiu'] = 1;
		$this->is_actiu = R::number('is_actiu');
		
		if ($this->is_actiu==0){
			$this->set_field('classificacio','list_admin','text');
			$this->set_field('evaluacio','list_admin','text');
			$this->set_field('obertura','list_admin','text');
			$this->set_field('tancament','list_admin','text');
		}
		
		$seguiment_id = R::id('seguiment_id');
	    $listing = new ListRecords($this);		
		
		$listing->add_filter('is_actiu');
		
		if (($this->is_actiu==0 || $this->is_actiu==1) && !$listing->is_editable){
			$listing->set_field('is_actiu','list_admin','0');
		}
		
		$listing->add_swap_edit();	
		
		$listing->set_options (1, 0, 1, 0, 0, 1, 0, 0, 1, 1); // $options_bar = 1, $options_checkboxes = 1, $save_button = 1, $select_button = 1, $delete_button = 1, $print_button = 1, $edit_buttons = 1, $image_buttons = 1, $split_count = 1, , $order_bar = 1
		
		if (!$listing->is_editable){
			$this->set_field('is_actiu','type','select'); // així em mostra "si o no"
		}
		
		$listing->condition = "seguiment_id=" . $seguiment_id;
		
		$listing->has_bin = false;
		$listing->call('records_walk',false,true);
		
		$listing->group_fields = array('categoria');
		
		$listing->order_by =  'categoria ASC, evolucio ASC, num_requisit';
		
		$listing->join = 'INNER JOIN pv__requisit USING (requisit_id) INNER JOIN pv__norma ON pv__norma.norma_id = pv__requisit.top_id INNER JOIN pv__categoria USING (categoria_id)';
		
		$listing->extra_fields = "(SELECT pv__norma.evolucio FROM pv__norma WHERE pv__norma.norma_id = pv__requisit.norma_id) AS evolucio,
								 (SELECT pv__norma.descripcio_norma FROM pv__norma WHERE pv__norma.norma_id = pv__requisit.norma_id) AS descripcio_norma";
		
	    $listing->set_records();
		Debug::p($listing->loop, 'Loop');
		
		$loop = array();
		foreach ($listing->loop as $rs) {
			$codi = $rs['codi_lloc'] . '-' . $rs['codi_categoria'] . '-' . $rs['numero'];
			$loop[$codi][] = $rs;
		}
		
		Debug::p($loop, 'Loop');
		
		
		return $listing->list_records();
	}
	function records_walk($module){
		$rs = &$module->rs;
		$ret = array();
		
		// si no ha estat mai actiu agafo la classificació del requisit
		if (!$rs['has_been_actiu']){
			$requisit_id = $rs['requisit_id'];
			$classificacio = Db::get_first("SELECT classificacio FROM pv__requisit WHERE requisit_id = " . $requisit_id);
			$ret['classificacio'] = $classificacio;
		}
		
		//$codi_categoria = Db::get_first('SELECT codi_categoria FROM pv__categoria WHERE categoria_id = ' .  $rs['categoria_id']);
		
		$ret['codi_lloc'] = $rs['codi_lloc'] . ' ' . $rs['codi_categoria'] . ' ' . $rs['numero'];
		$ret['categoria'] =  $rs['categoria'] . ' - ' . $rs['codi_categoria'];
				
		return $ret;
	}
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{
	    $show = new ShowForm($this);
		$show->has_bin = false;
	    return $show->show_form();
	}

	function save_rows()
	{
	    $save_rows = new SaveRows($this);
	    $save_rows->save();
		
		if ($this->action == 'save_rows_save_records'){
		
			// si no hi ha dat i poso com a actiu, llavors poso la data d'avui
			foreach ($save_rows->id as $id){
				if (isset($_POST['is_actiu'][$id])){
					
					$has_been_actiu = Db::get_first('
							SELECT has_been_actiu 
							FROM pv__seguimentitem 
							WHERE seguimentitem_id = ' . $id);
					
					// nomès es fa el primer cop que s'activa
					if (!$has_been_actiu){
						$GLOBALS['gl_reload'] = true;
							
						$classificacio = $_POST['classificacio'][$id];
						
						$evaluacio = $classificacio=='A'?'noconforme':'informacio';
							
						Db::execute("
							UPDATE pv__seguimentitem 
							SET obertura = now(),
							evaluacio = '".$evaluacio."',
							has_been_actiu = 1
							WHERE seguimentitem_id = " . $id);
							
						
					}
				}
			}
		}
		
	}

	function write_record()
	{
	    $writerec = new SaveRows($this);
	    $writerec->save();
	}

	function manage_images()
	{
	    $image_manager = new ImageManager($this);
	    $image_manager->execute();
	}
}
?>