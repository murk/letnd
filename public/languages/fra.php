<?
// blocks

define('INMO_PRICE_FIRST','Jusqu\'à %s&nbsp;'.CURRENCY_NAME);
define('INMO_PRICE_MIDDLE','Entre %s et %s&nbsp;'.CURRENCY_NAME);
define('INMO_PRICE_LAST','Plus de %s&nbsp;'.CURRENCY_NAME);
define('INMO_FLOOR_SPACE_FIRST','Jusqu\'à %s m<sup>2</sup>');
define('INMO_FLOOR_SPACE_MIDDLE','Entre %s et %s m<sup>2</sup>');
define('INMO_FLOOR_SPACE_LAST','Plus de %s m<sup>2</sup>');
define('INMO_PERSON_FIRST','Jusqu\'à %s');
define('INMO_PERSON_MIDDLE','Entre %s et %s');
define('INMO_PERSON_LAST','Plus de %s');
define ('LETND','Letnd');

$gl_caption['c_offers'] = 'Offres';


// captions

$gl_caption['c_send_mail'] = 'envoyer e-mail';
$gl_caption['c_more_info'] = 'Plus d\'infos';
$gl_caption['c_read_more'] = 'En savoir plus';
$gl_caption['c_title_search'] = 'Recherche';
$gl_caption['c_yes'] = 'Oui';
$gl_caption['c_no'] = 'Non';
$gl_caption['c_send'] = 'Envoyer';
$gl_caption['c_clear'] = 'Effacer';
$gl_caption['c_send_to_friend']='envoyer à un ami';
$gl_caption['c_print']='imprimer';
$gl_caption['c_zoom_link']='élargir images';
$gl_caption['c_contact_link']='contacter';

// bloc inmo_categories
$gl_caption['c_sell'] = 'Vente';
$gl_caption['c_rent'] = 'Location';
$gl_caption['c_temp'] = 'Location touristique';
$gl_caption['c_moblat'] = 'Location avec meubles';
$gl_caption['c_selloption'] = 'Location option d\'achat';

// seo
$gl_caption['c_seo_verd_1']='<a href="http://www.letnd.com" target="_blank" title="Diseño páginas web en Girona">Diseño y programación web:<img src="/common/images/logoletnd.png" width="95" height="16" alt="Letnd: Diseño web, posicionamiento web, páginas web en Girona" style="vertical-align:text-bottom" /></a>';
$gl_caption['c_seo_verd_2']='<a href="http://www.letnd.com" target="_blank" title="Diseño páginas web en Girona">Programmation web:<img src="/common/images/logoletnd.png" width="95" height="16" alt="Letnd: Diseño web, posicionamiento web, páginas web en Girona" style="vertical-align:text-bottom" /></a>';
$gl_caption['c_seo_verd_3']='<a href="http://www.letnd.com" target="_blank" title="Diseño páginas web en Girona">Creado por:<img src="/common/images/logoletnd.png" width="95" height="16" alt="Letnd: Diseño web, posicionamiento web, páginas web en Girona" style="vertical-align:text-bottom" /></a>';
$gl_caption['c_seo_verd_4']='<a href="http://www.letnd.com" target="_blank" title="Diseño páginas web en Girona">Diseño web:<img src="/common/images/logoletnd.png" width="95" height="16" alt="Letnd: Diseño web, posicionamiento web, páginas web en Girona" style="vertical-align:text-bottom" /></a>';

$gl_caption['c_seo_blanc_1']='<a href="http://www.letnd.com" target="_blank" title="Diseño páginas web en Girona">Diseño y programación web:<img src="/common/images/logoblanc.png" width="95" height="16" alt="Letnd: Diseño web, posicionamiento web, páginas web en Girona" style="vertical-align:text-bottom" /></a>';
$gl_caption['c_seo_blanc_2']='<a href="http://www.letnd.com" target="_blank" title="Diseño páginas web en Girona">Programación web:<img src="/common/images/logoblanc.png" width="95" height="16" alt="Letnd: Diseño web, posicionamiento web, páginas web en Girona" style="vertical-align:text-bottom" /></a>';
$gl_caption['c_seo_blanc_3']='<a href="http://www.letnd.com" target="_blank" title="Diseño páginas web en Girona">Creado por:<img src="/common/images/logoblanc.png" width="95" height="16" alt="Letnd: Diseño web, posicionamiento web, páginas web en Girona" style="vertical-align:text-bottom" /></a>';
$gl_caption['c_seo_blanc_4']='<a href="http://www.letnd.com" target="_blank" title="Diseño páginas web en Girona">Diseño web:<img src="/common/images/logoblanc.png" width="95" height="16" alt="Letnd: Diseño web, posicionamiento web, páginas web en Girona" style="vertical-align:text-bottom" /></a>';

$gl_caption['c_seo_custom_2']='<a href="http://www.letnd.com" target="_blank" title="Diseño páginas web en Girona">Programación web:<img src="/'.CLIENT_DIR.'/templates/images/letnd.png" alt="Letnd: Diseño web, posicionamiento web, páginas web en Girona" /></a>';
$gl_caption['c_seo_custom_4']='<a href="http://www.letnd.com" target="_blank" title="Diseño páginas web en Girona">Diseño web:<img src="/'.CLIENT_DIR.'/templates/images/letnd.png" alt="Letnd: Diseño web, posicionamiento web, páginas web en Girona" /></a>';

$gl_caption['c_tercersetena'] = 'Disseño <a class="tercersetena" target="_blank" href="http://www.tercersetena.com">Tercersetena.com</a>';

// NO TRADUIT
$gl_caption['c_tipus_temp'] = 'Location saisonnière';
$gl_caption['c_tipus_moblat'] = 'Location meubles';
// FI NO TRADUIT

// TAMBÉ A ADMIN
// Caption default
$gl_caption['c_form_title'] = 'Modifier les données';
$gl_caption['c_config_title'] = 'Modifier la configuration';
$gl_caption['c_edit'] = 'Modifier';


$gl_caption['c_edit_button'] = 'Modifier les données';
$gl_caption['c_view_button'] = 'Afficher les données';
$gl_caption['c_edit_images_button'] = 'Modifier les photos';
$gl_caption['c_add_images_button'] = 'Ajouter des photos';



$gl_caption['c_edit_data'] = 'Modifier les données';
$gl_caption['c_edit_images'] = 'Modifier les photos';
$gl_caption['c_add_images'] = 'Ajouter des photos';
$gl_caption['c_search_form'] = 'Rechercher';
$gl_caption['c_send_new'] = 'Enregistrer';
$gl_caption['c_send_edit'] = 'Enregistrer les modifications';
$gl_caption['c_delete'] = 'Effacer';
$gl_caption['c_action'] = 'Action';
$gl_caption['c_delete_selected'] = 'Effacer les éléments sélectionnés';
$gl_caption['c_restore_selected'] = 'Récupérer les éléments sélectionnés';
$gl_caption['c_confirm_deleted'] = 'Les éléments sélectionnés vont être définitivement effacés de la base de données. \\n\\nVoulez-vous continuer ?';
$gl_caption['c_confirm_images_deleted'] = 'Les photos sélectionnées vont être définitivement effacées.\\n\\nVoulez-vous continuer ?';
$gl_caption['c_confirm_form_bin'] = 'Le fichier va être envoyé à la corbeille. \\n\\nVoulez-vous continuer ?';
$gl_caption['c_confirm_form_deleted'] = 'Le fichier va être définitivement effacé de la base de données. \\n\\nVoulez-vous continuer ?';
$gl_caption['c_move_to'] = 'Déplacer vers :';
$gl_caption['c_print_all'] = 'Tout imprimer';
$gl_caption['c_print'] = 'Imprimer';
$gl_caption['c_order_by'] = 'Classer par :';
$gl_caption['c_select'] = 'Sélectionner';
$gl_caption['c_all'] = 'tout';
$gl_caption['c_nothing'] = 'rien';
$gl_caption['c_preview'] = 'Prévisualiser';
$gl_caption['c_yes'] = 'Oui';
$gl_caption['c_no'] = 'Non';
$gl_caption['c_options_bar'] = 'Options';
$gl_caption['c_file_maximum'] = 'maximum %s fichiers à la fois';
$gl_caption['c_file_maximum_1'] = 'maximum 1 fichier à la fois';


// Message default
$gl_messages['title_edit']='Modifier';
$gl_messages['title_copy']='Copier les données';
$gl_messages['added']='Le fichier a été correctement ajouté';
$gl_messages['add_exists']='Un fichier avec ce(tte) "%s" existe déjà';
$gl_messages['saved']='Le fichier a été correctement modifié';
$gl_messages['not_saved']='Le fichier n\'a pas été modifié';
$gl_messages['no_records']='Il n\'y a aucun fichier dans cette liste';
$gl_messages['no_records_bin']='Il n\'y a aucun fichier dans la corbeille';
$gl_messages['list_saved']='Les modifications ont été correctement apportées aux fichiers';
$gl_messages['list_not_saved']='Aucun fichier n\'a été modifié et aucune modification n\'a été détectée';
$gl_messages['list_deleted']='Les fichiers ont été effacés';
$gl_messages['list_bined']='Les fichiers ont été envoyés à la corbeille';
$gl_messages['list_restored']='Les fichiers ont été récupérés';
$gl_messages['concession_no_acces']='Vous n\'êtes pas autorisé à accéder à cette section';
$gl_messages['concession_no_write']='Vous n\'êtes pas autorisé à modifier les données de cette section';
$gl_messages['select_item']='Cliquez sur un des fichiers de la liste pour le sélectionner';
$gl_messages['related_not_deleted']='Les fichiers indiqués n\'ont pas pu être effacés car d\'autres fichiers leur sont associés';


$gl_messages['deleted']='Le registre a été supprimé';
$gl_messages['bined']='Le dossier a été envoyé à la corbeille';
$gl_messages['restored']='Le registre a récupéré avec succès';
$gl_caption['c_ok'] = 'Accepter';
$gl_caption['c_cancel'] = 'Résilier';
$gl_messages['post_max_size']='Le poids de la forme (contenu et des pièces jointes) dépasse la limite ';
$gl_messages['post_file_max_size']='Le poids de la pièce jointe dépasse la limite ';
$gl_messages['assigned']='Registres sélectionnés';

$gl_caption['c_form_sent']='Le formulaire a été soumis avec succès, nous vous répondrons dans les plus brefs délais';
$gl_caption['c_form_not_sent']='Le formulaire n\'a pas pu être envoyé, vérifier que l\'adresse e-mail que vous avez écrit est correct';

$gl_caption['c_cookies_message'] = 'This website uses proprietary and third-party cookies to improve the browsing experience as well as for analysis. By continuing browsing, we understand that you agree the';
$gl_caption['c_cookies_message_more'] = 'use of cookies';
$gl_caption['c_cookies_message_accept'] = 'Accept';

$gl_caption['c_cookies_text'] = '

<p class="accept-cookies-title">What are cookies?</p>
<p>A cookie is a small file stored on the user\'s computer that allows us to recognize it. The set of cookies help us improve the quality of our website, allowing us to monitor which pages are useful to the website users or and which are not.</p>
<p>Cookies are essential for the operation of the Internet, providing innumerable advantages in the provision of interactive services, providing us with the navigation and usability of our website. Please note that cookies can not harm your computer and, if enabled, they help us identify and resolve errors.</p>
<p class="accept-cookies-title">What kind of cookies are used in this site?</p>
<p>Propietary cookies: those that are sent to the user\'s computer from a computer or domain managed by the publisher and from which provides the service requested by the user.</p>
<p>Third party cookies:those that are sent to the user\'s computer from a computer or domain not managed by the publisher but by another entity that analyzes the data collected. .</p>
<p>Persisting cookies: a type of cookie in which data are still stored in the terminal and can be accessed and processed by the cookie\'s responsible.</p>
<p>Analytics cookies: using web analytics to get information on the number of users accessing the site, number of pages viewed, and repetition frequency of visits, the duration, the browser used, operator who provides the service, the language used by the terminal or the city to which is assigned the IP address. Information that allows a better and more appropriate service by this website.</p>
<p class="accept-cookies-title">Accept the use of cookies.</p>
<p>We assume that you accept cookies. However, you can restrict, block or erase cookies from this site or any page from the browser settings. </p>
<p>Next you can find the main browsers help pages:</p>
<ul>
<li>Internet Explorer: <a href="http://windows.microsoft.com/es-xl/internet-explorer/delete-manage-cookies#ie="ie-10"" target="_blank">windows.microsoft.com/es-xl/internet-explorer/delete-manage-cookies#ie="ie-10"</a></li>
<li>FireFox: <a href="http://support.mozilla.org/es/kb/Borrar%20cookies" target="_blank">support.mozilla.org/es/kb/Borrar%20cookies</a></li>
<li>Chrome: <a href="http://support.google.com/chrome/answer/95647?hl="es"" target="_blank">support.google.com/chrome/answer/95647?hl="es"</a></li>
<li>Safari: <a href="http://www.apple.com/es/privacy/use-of-cookies/" target="_blank">www.apple.com/es/privacy/use-of-cookies/</a></li>
</ul>
';

// contrasenyes
$gl_caption['c_password_forgotten'] = 'Vous avez oublié votre mot de passe?';
$gl_caption['c_password_insert_mail'] = 'Veuillez s\'il vous plaît introduire votre adresse courriel et nous vous enverrons un nouveau mot de passe.';
$gl_caption['c_password_insert_mail_exists'] = 'Si vous avez oublié votre mot de passe, cliquez sur accepter et nous vous en enverrons un nouveau.';
$gl_caption['c_password_mail_error'] = 'L\'adresse courriel indiquée n\'existe pas dans notre base de données, veuillez s\'il vous plaît indiquer une adresse correcte. ';
$gl_caption['c_password_sent'] = 'Un nouveau mot de passe a été envoyé à votre adresse courriel';
$gl_caption['c_password_mail_subject'] = 'Votre nouveau mot de passe';
$gl_caption['c_password_mail_text_1'] = 'Votre nouveau mot de passe est';
$gl_caption['c_password_mail_text_2'] = 'Vous pouvez changer votre mot de passe sur notre site Internet';
$gl_caption['c_welcome'] = 'Bienvenu';

$gl_caption['c_new_custumer_exists'] = 'Vous avez déjà introduit une personne avec ces prénom et nom, ou date de naissance. Vous pouvez:';
$gl_caption['c_new_custumer_exists_change_name'] = 'Corriger le nom ou la date de naissance';
$gl_caption['c_new_custumer_exists_return'] = 'Retourner à la réservation et sélectionner une personne de la liste';

// captcha
$gl_caption['c_recaptcha_error'] = 'Please validate the "CAPTCHA" correctly';
?>