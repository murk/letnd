<?php
/*
****************************************************************************
****************************************************************************
Falta tindre en compta que si una empresa no està posada al mls, (taula clients mls=1) que es dongui d'alta primer

Mirar totes les possibilitats de: em presa A demana mls, i B també demana mls alhora etc...
******************************************************************************
******************************************************************************
*/
global $gl_db_max_results;
$gl_db_max_results = 100000;
function save_rows()
{
    $save_rows = new SaveRows;
    $save_rows->save();
}
/*
Atenció!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
la taula mls__client_to_client
-------------------------------------------------------------------
client_id1 --> correspon a l'empressa que dona permis
client_id2 --> correspon a l'empresa que reb el permis
-------------------------------------------------------------------
*/
function list_records()
{
    global $gl_action, $gl_content, $listing, $gl_page, $gl_content, $tpl;

    Db::connect_mother();
    set_table('client__client', 'client_id');

    set_field('permission','change_field_name','(SELECT permission
						FROM mls__client_to_client
						WHERE client_id1 ='.$_SESSION['client_id'].'
						AND client_id2 = client_id ) AS permission');

    $listing = new ListRecords;
    switch ($gl_action) {
        case "list_records_yes": // Empreses que tinc assignades
            $listing->condition = "((
				SELECT permission
				FROM mls__client_to_client
				WHERE client_id1 =".$_SESSION['client_id']."
				AND client_id2 = client_id
				) = 'accepted') AND ";
        default: // Llisto totes les empreses que volen surtir al llistat de MLS
			$listing->order_by ='permission ASC, comercialname ASC';
			$listing->group_fields = array('permission');
            $listing->has_bin = false;
            $listing->set_options(0, 0, 0, 0, 0, 0, 0, 0, 0);
            $listing->condition .= 'mls = 1 AND client_id != '.$_SESSION['client_id'].'';
			$listing->add_button ('add_company_button', 'action=write_record_add_company', 'boto1', 'before', '', 'target="save_frame"');//Evito que surti l'empresa amb que estic treballant
			$listing->add_button ('accept_company_button', 'action=write_record_accept_company', 'boto1', 'before', '', 'target="save_frame"');
			$listing->add_button ('deny_company_button', 'action=write_record_deny_company', 'boto1', 'before', '', 'target="save_frame"');
			$listing->add_button ('erase_shared_company_button', 'action=write_record_erase_shared_company', 'boto1', 'before', '', 'target="save_frame"');
			//$listing->add_button ('waiting_to_be_accepted_company_button', '', 'boto5', 'before', '', 'target="save_frame"');
			$listing->add_button ('denied_company_reaccept_company_button', 'action=write_record_accept_company', 'boto1', 'before', '', 'target="save_frame"');
			//$listing->add_button ('i_was_denied_company_button', '', 'boto6', 'before', '', 'target="save_frame"');
			$listing->call('list_records_walk_1','permission,domain',true);
            $listing->list_records();
            Db::reconnect();
    }
}
function list_records_walk_1 (&$listing,$permission, $domain){ //Ensenyo el voto per Solicitar MLS
	$b = &$listing->buttons;
	$b['add_company_button']['show']=$b['accept_company_button']['show']=$b['deny_company_button']['show']=$b['denied_company_reaccept_company_button']['show']=$b['erase_shared_company_button']['show']=false;
	switch ($permission){
		case '':
			$b['add_company_button']['show']=true;
			break;
		case 'askfor':
			break;
		case 'askedfor':
			$b['accept_company_button']['show']=true;
			$b['deny_company_button']['show']=true;
			break;
		case 'denied':
			break;
		case 'canreaccept':
			$b['denied_company_reaccept_company_button']['show']=true;
			break;
		case 'accepted':
			$b['erase_shared_company_button']['show']=true;
			break;
	}
	return array('domain'=>'<a target="_blank" href="'. HTTP . $domain . '">' . $domain . '</a>');
}

function write_record()
{
   global $gl_action, $gl_message, $gl_messages, $gl_reload;
   $outside_company = $_GET['client_id']; //l'empresa a qui autoritzo
   $client_id = $_SESSION['client_id']; //jo

   Db::connect_mother ();
   switch($gl_action){
   case "write_record_add_company": //solicitar mls
		$query = "INSERT INTO mls__client_to_client (client_id1, client_id2, permission ) VALUES
					( " . $client_id . " , " . $outside_company . ", 'askfor'),
					( " . $outside_company . " , " . $client_id . ", 'askedfor')";
	    Db::execute($query);
		$gl_message = $gl_messages['company_waiting'];
		break;
   case "write_record_accept_company": //accepto mls
		$query="UPDATE mls__client_to_client SET permission = 'accepted' where client_id1 = '" . $client_id."' AND client_id2 =  '" . $outside_company ."'";
		Db::execute($query);
		$query="UPDATE mls__client_to_client SET permission = 'accepted' where client_id1 = '" . $outside_company."' AND client_id2 =  '" . $client_id ."'";
		Db::execute($query);
		$gl_message = $gl_messages['company_authorized'];
		break;
   case "write_record_deny_company":
		$query="UPDATE mls__client_to_client SET permission = 'canreaccept' where client_id1 = '" . $client_id."' AND client_id2 ='" . $outside_company."'";
		Db::execute($query);
		$query="UPDATE mls__client_to_client SET permission = 'denied' where client_id1 = '" . $outside_company."' AND client_id2 ='" . $client_id."'";
		Db::execute($query);
		$gl_message = $gl_messages['company_denied'];
		break;
   case "write_record_erase_shared_company": //elimino mls
		$query = "DELETE FROM mls__client_to_client where client_id2 = '" . $outside_company."' AND client_id1 ='".$client_id."' ||  client_id2 = '" . $client_id."' AND client_id1 ='".$outside_company."' and permission = 'accepted'";
		Db::execute($query);
		$gl_message = $gl_messages['company_deleted'];
		break;
   }
   Db::reconnect();

	send_message($outside_company);
	$gl_reload = true;
}
function send_message ($solicitat_id) {
	
	global $gl_action, $gl_caption;
	//$solicitador_id correspon a la empresa que es fa la sol·licitud
	//1er recullo el nom de la empresa i el telefon de la empresa que fa la solicitud (el solicitador)
	Db::connect_mother ();
	$query = "SELECT comercialname, phone1 FROM client__client where client_id = '" . $_SESSION['client_id'] . "'";
	$results = Db::get_rows($query);
	//2on recullo totes les dades de la empresa a qui es fa la solicitud ( el solicitat )
	$query2 = "SELECT * FROM client__client where client_id = '" . $solicitat_id . "'";
	$results2 = Db::get_rows($query2);
	//3er creo el missatge i el guardo a la taula de missatges del solicitat
	switch($gl_action){
		case "write_record_add_company":
			$missatge= $gl_caption ['c_missatge_add_company'];
			$asunto = $gl_caption ['c_asunto_add_company'];
			break;
		case "write_record_accept_company":
			$missatge= $gl_caption ['c_missatge_authorized_company'];
			$asunto = $gl_caption ['c_asunto_authorized_company'];
			break;
		case "write_record_deny_company":
			$missatge= $gl_caption ['c_missatge_denied_company'];
			$asunto = $gl_caption ['c_asunto_denied_company'];
			break;
		case "write_record_erase_shared_company":
			$missatge= $gl_caption ['c_missatge_erase_shared_company'];
			$asunto = $gl_caption ['c_asunto_erase_shared_company'];
			break;
		case "write_record_accept_denied_company":
			$missatge= $gl_caption ['c_missatge_reaccepted_company'];
			$asunto = $gl_caption ['c_asunto_reaccepted_company'];
			break;
	}

	$missatge= sprintf($missatge, $GLOBALS['gl_config']['company_name']?$GLOBALS['gl_config']['company_name']:HOST_URL, HOST_URL);

	//4rt gravo la taula client__to__client que s'esta a l'espera de que accepti
	connect_solicitat ($results2 [0]['host'],$results2 [0]['dbusername'],$results2 [0]['dbpwd'],$results2 [0]['dbname']);
	
	Main::add_missatge(
		array(
						'status' => 'send',
						'kind' => 'mls',
						'subject' => $asunto,
						'missatge' => $missatge,
						'link' => '/admin/?menu_id=9002',
						'mls_from_id' => $_SESSION['client_id'],
						'mls_to_id' => $solicitat_id,			
		)
	);

	Db::reconnect();
}
function connect_solicitat($db_host,$db_uname,$db_pass,$db_name)
{
    // Start connection
    Db::connect($db_host, $db_uname, $db_pass, $db_name);
}
?>