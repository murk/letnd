<article>
	
	<? /* Botons */ ?>
	<div class="show-buttons right">

		<? if($show_previous_link || $show_next_link): ?>
			<? if($show_previous_link): ?>
				<a class="previous-link" href="<?= $show_previous_link ?>" title="<?= htmlspecialchars( $c_previous_new ) ?>"><?= $c_previous_new ?></a>
			<? endif ?>
			<? if($show_next_link): ?>
				<a class="next-link" href="<?= $show_next_link ?>" title="<?= htmlspecialchars( $c_next_new ) ?>"><?= $c_next_new ?></a>
			<? endif ?>
		<? endif ?>
	</div>
	<? /* Fi Botons */ ?>
	
	<?if ($images):?>

		<figure>
			<div id="slider-news">
				<? /*
				Aquest codi mostra nomes la primera imatge, la resta quan cliques
				<?=$image_conta>0?' class="hidden"':''?>"
			*/ ?>
				<? foreach( $images_cat_1 as $l ):extract( $l ) ?>
					<div class="item<?= $image_conta > 0?' hidden':'' ?>">

						<a href="<?= $image_src_medium ?>" data-sub-html="<?= htmlspecialchars( $image_title ) ?>" title="<?= htmlspecialchars( $image_title?$image_title:$new ) ?>"><img <?= get_image_attrs( $image_src_details, $image_alt?$image_alt:$new, 700, 466, false ) ?> /></a>

					</div>
				<? endforeach //images?>
			</div>
		</figure>

				
	<?endif //image?>	

	<time datetime="<?=unformat_date_attr($entered)?>"><?=$entered?></time>
	
	<?if($subtitle):?><h2><?=$subtitle?></h2><?endif //subtitle?>
	
	<div class="description"><?= $content ?></div>
	
	<?if($videoframe):?>
		<div class="videoframe"><?=$videoframe?></div>
	<?endif //videoframe?>	
	
	<? if( $url1 || $url2 || $url3 || $files ): ?>
		<aside>
			<? if( $files ): ?>
				<h6><?= $c_descarga ?></h6>
				<? foreach( $files as $l ): extract( $l ) ?>
					<a class="files" target="_blank" href="<?= $file_src ?>"><?= $file_title ?></a>
				<? endforeach //files?>

			<? endif // $files ?>
			<? if( $url1 || $url2 || $url3 ): ?>
				<h6><?= $c_links_interes ?></h6>
				<? if( $url1 ): ?>
					<a target="_blank" href="<?= $url1 ?>"><?= $url1_name ?></a>
				<? endif //url?>
				<? if( $url2 ): ?>
					<a target="_blank" href="<?= $url2 ?>"><?= $url2_name ?></a>
				<? endif //url?>
				<? if( $url3 ): ?>
					<a target="_blank" href="<?= $url3 ?>"><?= $url3_name ?></a>
				<? endif //url?>

			<? endif // $url1 || $url2 || $url3 ?>
		</aside>
	<? endif //$url1 || $url2 || $url3 || $files?>

</article>