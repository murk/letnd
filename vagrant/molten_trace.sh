#!/usr/bin/env bash
# Instalar JAVA
echo "deb http://http.debian.net/debian jessie-backports main" | \
      sudo tee --append /etc/apt/sources.list.d/jessie-backports.list > /dev/null
sudo apt-get update
sudo apt-get install -t jessie-backports openjdk-8-jdk
sudo update-java-alternatives -s java-1.8.0-openjdk-amd64

# Instalar phize
sudo apt-get install php5-dev

# Molten
sudo git clone https://github.com/chuan-yun/Molten.git /home/molten

cd /home/molten
sudo phpize
sudo ./configure
sudo make && make install

sudo cat > /etc/php5/apache2/conf.d/molten.ini <<EOF
extension=molten.so
EOF

mkdir /var/wd
mkdir /var/wd/log
mkdir /var/wd/log/tracing
mkdir /var/wd/log/tracing/php
chown -R www-data:www-data /var/wd/log/tracing/php
sudo /etc/init.d/apache2 restart

cd example
sudo sh run.sh
