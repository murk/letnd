<?
/**
 * ProductVariation
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2010
 * @version $Id$
 * @access public
 */
class ProductVariation extends Module{

	function __construct(){
		parent::__construct();
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records() . '<script type="text/javascript" src="/admin/modules/product/jscripts/functions.js?v=' . $GLOBALS['gl_version'] . '"></script>';
	}
	function get_records()
	{
		// llistat de variacions
		if ($this->process!='default'){
			
			$listing = new ListRecords($this);
			$listing->order_by = 'variation_category_id ASC,ordre ASC,variation ASC';
			$listing->set_field('variation_pvp','list_admin','no');
			$listing->set_field('variation_pvd','list_admin','no');
			$listing->set_field('variation_pvd2','list_admin','no');
			$listing->set_field('variation_pvd3','list_admin','no');
			$listing->set_field('variation_pvd4','list_admin','no');
			$listing->set_field('variation_pvd5','list_admin','no');
			
			$listing->call('list_records_walk_select', 'variation_id,variation,variation_subtitle');
			
			$variation_category_id = R::text_id('variation_category_id');
			
			if ($this->process=='select_variation'){
			
                $page = &$GLOBALS['gl_page'];
                $page->template = 'clean.tpl';
                $page->title = '';	
                $page->javascript .= 'parent.Assign.show_selected(document)';	
				
				$listing->set_field('ordre', 'list_admin', 'no');
                $listing->set_options(0, 0, 0, 0, 0, 0, 0, 0, 1);
				
				if ($variation_category_id)
					$listing->condition = 'variation_category_id = ' . $variation_category_id;
				
                $GLOBALS['gl_message'] = $GLOBALS['gl_messages']['select_item'];				
			}
			else{
				if ($variation_category_id=='null' || !$variation_category_id){
					$listing->group_fields = array('variation_category_id');
				}
				$listing->add_filter('variation_category_id');
			}
			
			return $listing->list_records();
		}
		// gestio de variacions per defecte i valors per defecte
		else
		{
			//$id = $_GET['product_id'];
			$tpl = new phemplate(PATH_TEMPLATES);
			$tpl->file = 'product/variation_variations.tpl';
			
			$tpl->set_var('menu_id', $GLOBALS['gl_menu_id']);
			$tpl->set_vars($this->caption);
			// obtinc les categories
			$query = "SELECT DISTINCT variation_category_id, variation_category 
						FROM product__variation_category_language 
						INNER JOIN product__variation
						USING (variation_category_id)
						WHERE language = '" . LANGUAGE . "'
						AND product__variation.bin=0
						ORDER BY variation_category_id";
			$results = Db::get_rows($query);
			$loop = array();
			$assign_selected_ids = array();
			foreach ($results as $rs)
			{
				$variations = $this->get_variation($rs['variation_category_id'],$rs['variation_category']);
				$loop[] = array(
							'variation'=>$variations['variation'],
							'variation_category_id' => $rs['variation_category_id'],
							'variation_category' => $rs['variation_category']);
							
				$assign_selected_ids = array_merge($assign_selected_ids, $variations['ids']);
			}
			$tpl->set_vars(array(
				'assign_selected_ids'=>json_encode($assign_selected_ids),
				'loop'=>$loop,
				'form_link'=>'?tool=product&tool_section=variation&process=default'
				));
			return  $tpl->process();
		}
	
	}
	function show_new_variations(){	
		
			$variations = $this->get_variation(false);
			
			echo $variations['variation'];
			Debug::p_all();
			die();	
	}
	// per posar el onclick al llistat
	function list_records_walk_select ($variation_id, $variation, $variation_subtitle)
	{
		if ($this->process=='select_variation'){
			$variation= addslashes($variation); // cambio ' per \'
			$variation= htmlspecialchars($variation); // cambio nomès cometes dobles per  &quot;
			$insert_name = $variation;
			if ($variation_subtitle && $insert_name) $insert_name .= ' - ' . $variation_subtitle;
			elseif ($variation_subtitle) $insert_name = $variation_subtitle;
			$ret['tr_jscript'] = 'onClick="parent.insert_address(' . $variation_id . ',\'' . $insert_name . '\')"';
			return $ret;
		}
	}
	function get_variation($variation_category_id,$variation_category=0){
		
		$query_ids = '';
		$ids = array();
		$query_category = '';
		$default_found = false;
		$selected_ids = array();
		$selected_item = array();
		$first_selected = -1;
		$is_ajax = $variation_category_id==0;
		
		if (!$is_ajax){
		}
		else{			
			// ids nous
			$new_ids = R::ids('ids');
			
			$query = "SELECT product__variation.variation_id as variation_id, 
							variation_pvp,
							variation_pvd,
							variation_pvd2, 
							variation_pvd3, 
							variation_pvd4, 
							variation_pvd5,     
							variation_default 
						FROM product__variation 
						WHERE variation_id IN (" . $new_ids . ")";
			$results = Db::get_rows($query);
			
			foreach ($results as $rs)
			{
				$selected_ids[] = $rs['variation_id'];
				$selected_item[$rs['variation_id']] = $rs;
			}
			if ($results) {
				$query_ids = '\''.implode($selected_ids,"','").'\'';
			}
			else{
				return array('variation' => '', 'ids'=>$ids);
			}
		}
		
	
		// si hi ha resultats faig un llistat de variacions per cada categoria

		$listing = new ListRecords($this);
		$listing->order_by = 'ordre ASC,variation ASC';
		if ($variation_category) $listing->caption['c_variation'] = $variation_category; // poso el nom de la categoria en la columna del llistat
		$listing->paginate = false;
		$listing->set_field('ordre','list_admin','no');
		$listing->set_field('variation_category_id','type','hidden');
		$listing->add_field('variation_checked');	
		$listing->set_field('variation_checked','list_admin','0');	
		$listing->set_field('variation_checked','type','none');
		$listing->set_field('variation_pvp','type','none');
		$listing->set_field('variation_pvd','type','none');
		$listing->set_field('variation_pvd2','type','none');
		$listing->set_field('variation_pvd3','type','none');
		$listing->set_field('variation_pvd4','type','none');
		$listing->set_field('variation_pvd5','type','none');	
		if ($is_ajax)					
			$listing->set_field('variation','type','none');
		
		$listing->set_var('all_variation_category_id', $variation_category_id);
		
		if (!$is_ajax){
			$listing->condition = 'variation_category_id = ' . $variation_category_id . ' AND variation_checked = 1';
		}
		else{
			$listing->condition = 'variation_id IN ('.$query_ids.')';	
		}
		
		$listing->default_template = "product/variation_list_pick";
		$listing->list_records(false);

		if ($listing->has_results) {
			
			foreach ($listing->results as $key=>$val){
				$rs = &$listing->results[$key];
				// tots els ids pel javascript				
				$ids[]= $rs['variation_id'];
				
				$pvp = $pvd = $pvd2 = $pvd3 = $pvd4 = $pvd5 = '';
				$default_selected = '';
				$pvp = format_currency($rs['variation_pvp']);
				$pvd = format_currency($rs['variation_pvd']);
				$pvd2 = format_currency($rs['variation_pvd2']);
				$pvd3 = format_currency($rs['variation_pvd3']);
				$pvd4 = format_currency($rs['variation_pvd4']);
				$pvd5 = format_currency($rs['variation_pvd5']);
				
				// els que hem guardat a product__product_to_variation hi posem el preu guardat i quin es per defecte i el checkbox seleccionat
				
				if ($rs['variation_checked']=='1') {
					$selected_ids[] = $rs['variation_id'];
					$first_selected==-1?$first_selected = $key:false;
					
					if ($rs['variation_default']=='1'){
						$default_selected = '  checked="checked"';
						$default_found = true;
					};
				}
				else{				
				}
				
				// mostro el nom de la categoria en el llistat de noves variacions
				if ($is_ajax) 
					$rs['variation'] = '<strong>' . $this->get_variation_category($rs['variation_category_id']) . ':</strong> ' . $rs['variation'];
				
				$rs['variation_pvp'] = $this->get_input($rs['variation_id'], $pvp, 'pvp');
				$rs['variation_pvd'] = $this->get_input($rs['variation_id'], $pvd, 'pvd');
				$rs['variation_pvd2'] = $this->get_input($rs['variation_id'], $pvd2, 'pvd2');
				$rs['variation_pvd3'] = $this->get_input($rs['variation_id'], $pvd3, 'pvd3');
				$rs['variation_pvd4'] = $this->get_input($rs['variation_id'], $pvd4, 'pvd4');
				$rs['variation_pvd5'] = $this->get_input($rs['variation_id'], $pvd5, 'pvd5');				
				
				$rs['default_selected'] = $default_selected;
			}
			
			// si no he trobat cap default, poso el primer de la llista seleccionat
			if (!$default_found && $selected_ids && !$is_ajax){
				$listing->results[$first_selected]['default_selected'] = '  checked="checked"';
			}
			$listing->set_var('is_ajax',$is_ajax);

			$content = $listing->parse_template();
		}
		else{
			$content = '';
		}
		return array('variation' => $content, 'ids'=>$ids);
	}
	function get_variation_category($id){
		return Db::get_first(
			"SELECT variation_category FROM  product__variation_category_language
				WHERE variation_category_id = " . $id . " AND language='".LANGUAGE."'"
				);
	}
	function get_input($variation_id, $value, $field){
		return '<input id="variation_'.$field.'_'.$variation_id.'" name="variation_'.$field.'['.$variation_id.']" type="text" value="'.$value.'" size="6" maxlength="11" /><input name="old_variation_'.$field.'['.$variation_id.']" type="hidden" value="'.$value.'" />';
	}
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{
	    $show = new ShowForm($this);
	    return $show->show_form();
	}
	
	function insert_language_row($variation_id, $product_id, $name, $language, $val) {
		// si ja existeix el registre, updatejo
		
		$product_variation_id = Db::get_first("
				SELECT product_variation_id
				FROM product__product_to_variation
				WHERE product_id =".$product_id."
				AND variation_id =".$variation_id);
		
		if (Db::get_first("
			SELECT count(*)
			FROM product__product_to_variation_language
			WHERE product_variation_id =".$product_variation_id . "
			AND language = '" . $language. "'"))
		{
			$query = "
				UPDATE product__product_to_variation_language
				SET ".$name." = ". Db::qstr($val) ."
				WHERE product_variation_id =".$product_variation_id . "
				AND language = '" . $language. "'";
			
		}
		else{
			$query = "
				INSERT INTO product__product_to_variation_language
				(product_variation_id,".$name.",language)
				VALUES
				(".$product_variation_id.",".Db::qstr($val).",'".$language."')";
		}
		Debug::p($query, 'QUery idioma');
		Db::execute($query);
	}
	
	function save_rows()
	{

		$product_id = R::id('product_id');
		
		if ($product_id) {
			$this->save_rows_product( $product_id );
		}
		elseif ($this->process=='default'){
			$this->save_rows_default();
		}
		else{
			$save_rows = new SaveRows($this);
			$save_rows->save();
			if ($this->action == 'save_rows_bin_selected') $this->on_bin();
			if ($this->action == 'save_rows_restore_selected') $this->on_restore();
		}
		
	}


	/**
	 * Guardo desde variacions per defecte
	 */
	function save_rows_default(){


		$querys='';

		//debug::p($_POST);

		if (isset($_POST['variation_id'])){
			foreach ($_POST['variation_id'] as $variation_id){
				$variation_category_id = $_POST['variation_category_id'][$variation_id];
				$default = (isset($_POST['default'][$variation_category_id]) && $_POST['default'][$variation_category_id]==$variation_id)?'1':'0'; // miro si està marcat com a default
				$pvp = (isset($_POST['variation_pvp'][$variation_id]))?$_POST['variation_pvp'][$variation_id]:'0';
				$pvd = (isset($_POST['variation_pvd'][$variation_id]))?$_POST['variation_pvd'][$variation_id]:'0';
				$pvd2 = (isset($_POST['variation_pvd2'][$variation_id]))?$_POST['variation_pvd2'][$variation_id]:'0';
				$pvd3 = (isset($_POST['variation_pvd3'][$variation_id]))?$_POST['variation_pvd3'][$variation_id]:'0';
				$pvd4 = (isset($_POST['variation_pvd4'][$variation_id]))?$_POST['variation_pvd4'][$variation_id]:'0';
				$pvd5 = (isset($_POST['variation_pvd5'][$variation_id]))?$_POST['variation_pvd5'][$variation_id]:'0';
				$pvp = unformat_currency($pvp,true);
				$pvd = unformat_currency($pvd,true);
				$pvd2 = unformat_currency($pvd2,true);
				$pvd3 = unformat_currency($pvd3,true);
				$pvd4 = unformat_currency($pvd4,true);
				$pvd5 = unformat_currency($pvd5,true);
				$query = "
					UPDATE product__variation
					SET variation_pvp = ".$pvp.",
					variation_pvd = ".$pvd.",
					variation_pvd2 = ".$pvd2.",
					variation_pvd3 = ".$pvd3.",
					variation_pvd4 = ".$pvd4.",
					variation_pvd5 = ".$pvd5.",
					variation_default = '".$default."',
					variation_checked = '1'
					WHERE variation_id =".$variation_id;


				//debug::p($query);
				Db::execute($query);
				$querys .='<br>' . $query . ';';

				$GLOBALS['gl_message'] = $GLOBALS['gl_messages']['list_saved'];
			}

			// desmarco el registres que hem eliminat,

			$query = "
					UPDATE product__variation
					SET 
						variation_pvp = 0.00,
						variation_pvd = 0.00,
						variation_pvd2 = 0.00,
						variation_pvd3 = 0.00,
						variation_pvd4 = 0.00,
						variation_pvd5 = 0.00,
						variation_default=0, 
						variation_checked=0
					WHERE  variation_id NOT IN (".implode($_POST['variation_id'],','). ')';


			Db::execute($query);
		}
		// variation_id
		// si no hi ha cap variacio, simplement resetejo totes
		else{
			$query = "
					UPDATE product__variation
					SET 
						variation_pvp = 0.00,
						variation_pvd = 0.00,
						variation_pvd2 = 0.00,
						variation_pvd3 = 0.00,
						variation_pvd4 = 0.00,
						variation_pvd5 = 0.00,
						variation_default=0, 
						variation_checked=0";

			Db::execute($query);

			// com que és l'últim faig un reload de la pagina
			$GLOBALS['gl_message'] = $GLOBALS['gl_messages']['list_saved'];
			$GLOBALS['gl_reload'] = true;
		}

		$querys .='<br>' . $query . ';';
		Debug::add('Save rows querys', $querys, 2);

		$this->check_defaults();

	}


	/**
	 * guardo desde la fitxa de productes
	 *
	 * @param $product_id
	 */
	function save_rows_product( $product_id ) {

		$querys='';

		$stock = 0;
		$found_variations = false;


		if (isset($_POST['variation_id'])){

			foreach ($_POST['variation_id'] as $variation_id){
				$variation_category_id = $_POST['variation_category_id'][$variation_id];
				$default = (isset($_POST['default'][$variation_category_id]) && $_POST['default'][$variation_category_id]==$variation_id)?'1':'0'; // miro si està marcat com a default

				if (isset($_POST['selected']) && in_array ($variation_id, $_POST['selected'])){

					$stock += $_POST['variation_stock'][$variation_id];
					$variation_status = Db::qstr ($_POST['variation_status'][$variation_id] );

					$found_variations = true;

					// others no idioma
					for ($i = 1; $i <= 4; $i++) {

						$type = $this->config['variation_others'.$i.'_type'];

						if ( $type == 'text' ) {
							${'variation_others' . $i} = "''";
						}
						else {

							$variation_others_val = isset($_POST[ 'variation_others' . $i ][ $variation_id ] ) ? $_POST[ 'variation_others' . $i ][ $variation_id ] : '';

							if ( $type == 'text_no_lang' ) {
								${'variation_others' . $i} = Db::qstr( $variation_others_val );
							}
							elseif ( $type == 'textarea' ) {
								${'variation_others' . $i} = Db::qstr( $variation_others_val );
							}
							elseif ( $type == 'checkbox' ) {
								${'variation_others' . $i} = Db::qstr( $variation_others_val );
							}
						}
					}

					// si ja existeix el registre, updatejo
					if (Db::get_first("
							SELECT count(*)
							FROM product__product_to_variation
							WHERE product_id =".$product_id."
							AND variation_id =".$variation_id))
					{

						$query_ref = ($this->config['auto_ref'])?'':
							"
							variation_ref = ".unformat_int($_POST['variation_ref'][$variation_id],true).",";

						$query = "
							UPDATE product__product_to_variation
							SET variation_pvp = ".unformat_currency($_POST['variation_pvp'][$variation_id],true).",
							variation_pvd = ".unformat_currency($_POST['variation_pvd'][$variation_id],true).",
							variation_pvd2 = ".unformat_currency($_POST['variation_pvd2'][$variation_id],true).",
							variation_pvd3 = ".unformat_currency($_POST['variation_pvd3'][$variation_id],true).",
							variation_pvd4 = ".unformat_currency($_POST['variation_pvd4'][$variation_id],true).",
							variation_pvd5 = ".unformat_currency($_POST['variation_pvd5'][$variation_id],true).",
							variation_status = ".$variation_status.",
							variation_others1 = ".$variation_others1.",
							variation_others2 = ".$variation_others2.",
							variation_others3 = ".$variation_others3.",
							variation_others4 = ".$variation_others4.",
							variation_stock = ".unformat_int($_POST['variation_stock'][$variation_id],true).",
							variation_last_stock_units_public = ".unformat_int($_POST['variation_last_stock_units_public'][$variation_id],true).",
							variation_last_stock_units_admin = ".unformat_int($_POST['variation_last_stock_units_admin'][$variation_id],true).",".
							$query_ref . "
							variation_ref_supplier = ".unformat_int($_POST['variation_ref_supplier'][$variation_id],true).",										
							variation_default = '".$default."'
							WHERE product_id =".$product_id."
							AND variation_id =".$variation_id;
					}
					// si no existeix inserto nou registre
					else
					{
						$query = "
							INSERT INTO product__product_to_variation (
							product_id ,
							variation_id ,
							variation_pvp ,
							variation_pvd ,
							variation_pvd2 ,
							variation_pvd3 ,
							variation_pvd4 ,
							variation_pvd5 ,
							variation_status ,
							variation_others1 ,
							variation_others2 ,
							variation_others3 ,
							variation_others4 ,
							variation_stock ,
							variation_last_stock_units_public ,
							variation_last_stock_units_admin ,
							variation_ref,
							variation_ref_supplier,
							variation_default
							)
							VALUES (
							'".$product_id."',
							'".$variation_id."',
							".unformat_currency($_POST['variation_pvp'][$variation_id],true).",
							".unformat_currency($_POST['variation_pvd'][$variation_id],true).",
							".unformat_currency($_POST['variation_pvd2'][$variation_id],true).",
							".unformat_currency($_POST['variation_pvd3'][$variation_id],true).",
							".unformat_currency($_POST['variation_pvd4'][$variation_id],true).",
							".unformat_currency($_POST['variation_pvd5'][$variation_id],true).",
							".$variation_status.",
							".$variation_others1.",
							".$variation_others2.",
							".$variation_others3.",
							".$variation_others4.",
							".unformat_int($_POST['variation_stock'][$variation_id],true).",
							".unformat_int($_POST['variation_last_stock_units_public'][$variation_id],true).",
							".unformat_int($_POST['variation_last_stock_units_admin'][$variation_id],true).",
							".unformat_int(($this->config['auto_ref'])?0:$_POST['variation_ref'][$variation_id],true).",
							".unformat_int($_POST['variation_ref_supplier'][$variation_id],true).",
							'".$default."'
							)";
					}


				}

				//debug::p($query);
				$querys .='<br>' . $query . ';';
				Db::execute($query);

				// others idioma - va aqui perque primer haig d'haver insertat el registre a product__product_to_variation
				for ($i = 1; $i <= 4; $i++) {

					$type = $this->config['variation_others'.$i.'_type'];

					if ($type == 'text') {
						${'variation_others'.$i} = $_POST['variation_others' . $i][$variation_id];
						foreach (${'variation_others'.$i} as $language=>$val){
							$this->insert_language_row($variation_id, $product_id, 'variation_others'.$i, $language, $val);
						}

					}
				}

				$GLOBALS['gl_message'] = $GLOBALS['gl_messages']['list_saved'];
			}

			// borro el registres que hem eliminat
			$query = "
					DELETE
					FROM product__product_to_variation_language
					WHERE product_variation_id 
						IN (
							SELECT product_variation_id
							FROM product__product_to_variation
							WHERE product_id =".$product_id."
							AND variation_id NOT IN (".implode($_POST['variation_id'],','). ')
						)';
			Db::execute($query);

			$query = "
					DELETE
					FROM product__product_to_variation
					WHERE product_id =".$product_id."
					AND variation_id NOT IN (".implode($_POST['variation_id'],','). ')';

			Db::execute($query);
		}
		// variation_id
		// si no hi ha cap variacio, simplement borro totes les del producte
		else{
			$query = "
					DELETE
					FROM product__product_to_variation_language
					WHERE product_variation_id 
						IN (
							SELECT product_variation_id
							FROM product__product_to_variation
							WHERE product_id =".$product_id."
						)";

			Db::execute($query);

			$query = "
					DELETE
					FROM product__product_to_variation
					WHERE product_id =".$product_id;

			Db::execute($query);

			// com que és l'últim faig un reload de la pagina
			$GLOBALS['gl_message'] = $GLOBALS['gl_messages']['list_saved'];
			$GLOBALS['gl_reload'] = true;
		}

		$querys .='<br>' . $query . ';';
		Debug::add('Save rows querys', $querys, 2);

		$this->check_product_defaults( $product_id );

		$this->set_stock_status( $product_id, $found_variations, $stock );

    }


	function write_record()
	{
	    // al borrar una variació:
		// - comprobar que quedi la predeterminada
		// - si no hi ha predeterminada poso la primera de les seleccionades i els preus a 0
		// - posar predeterminada a tots els productes o cambiar l'script de public d'agafar el primer predeterminat
		// - posar totes les predeterminades de tots els productes amb preu 0
		
		$writerec = new SaveRows($this);
	    $writerec->save();
		
		if ($this->action == 'bin_record') $this->on_bin();
	}
	function on_restore(){
		$this->check_defaults();
	}
	function on_bin(){
		// trec qualsevol predeterminada dels bin, així si es recupera algun dia que no hi hagi més d'una predeterminada
		$query = "UPDATE  product__variation 
					SET  variation_default =  0
					WHERE bin = 1";
		Db::execute($query);
		Debug::add('Trec predeterminat dels bins',$query);
		$this->check_defaults();
		
	}
	function check_defaults(){
	// loop per les categories
		$query = "SELECT DISTINCT variation_category_id 
			FROM product__variation_category_language 
			INNER JOIN product__variation
			USING (variation_category_id)
			WHERE language = '" . LANGUAGE . "'";
		$results = Db::get_rows($query);
		$loop = array();
		
		// Per cada categoria:
		// 		Si no hi ha cap seleccionat, no hi ha cap predeterminada
		// 		Si hi ha almenys una seleccionada, sempre ha d'haver una predeterminada, per tant si s'ha borrat la predeterminada, haig de marcar una altra com a predeterminada
		foreach ($results as $rs)
		{
			// comprovo que hi hagi una predeterminada a la categoria
			$query = "
				SELECT variation_id
				FROM product__variation
				WHERE variation_category_id ={$rs['variation_category_id']}
				AND variation_default =1
				AND bin = 0";
				Debug::add('Comprovo que hi hagi una predeterminada a la categoria',$query);
				
			// si no hi ha predet comprovo si hi ha una seleccionada
			if (!Db::get_first($query)){
			$query = "
				SELECT variation_id
				FROM product__variation
				WHERE variation_category_id ={$rs['variation_category_id']}
				AND variation_checked = 1
				AND bin = 0
				LIMIT 1;";
				Debug::add('si no hi ha predet comprovo si hi ha una seleccionada',$query);
			
				// si hi ha almenys una seleccionada marco la primera com predeterminada
				if (Db::get_first($query)){
					$query = "
					UPDATE  product__variation 
					SET  variation_default =  1
					WHERE variation_category_id =" .  $rs['variation_category_id'] . "
					AND variation_checked = 1
					AND bin = 0
					ORDER BY ordre
					LIMIT 1";
					Db::execute($query);
					Debug::add('si hi ha almenys una seleccionada marco la primera com predeterminada', $query);
				}
			}	
		}
	}

	function check_product_defaults( $product_id ) {
		// loop per les categories
		$query   = "SELECT DISTINCT variation_category_id 
			FROM product__product_to_variation 
			INNER JOIN product__variation
			USING (variation_id)
			WHERE product_id = $product_id";
		$results = Db::get_rows( $query );

		// Per cada categoria:
		// 		Si no hi ha cap seleccionat, no hi ha cap predeterminada
		// 		Si hi ha almenys una seleccionada, sempre ha d'haver una predeterminada, per tant si s'ha borrat la predeterminada, haig de marcar una altra com a predeterminada
		foreach ( $results as $rs ) {
			// comprovo que hi hagi una predeterminada a la categoria
			$query = "
				SELECT count(*)
					FROM product__product_to_variation 
				INNER JOIN product__variation
					USING (variation_id)
				WHERE variation_category_id ={$rs['variation_category_id']}
					AND product_id = $product_id
					AND product__product_to_variation.variation_default =1
					AND variation_status = 'onsale'";
			Debug::add( 'Comprovo que hi hagi una predeterminada a la categoria', $query );

			// Marco la primera com a predeterminada si no n'hi ha cap
			if ( ! Db::get_first( $query ) ) {

				$query = "
				SELECT product_variation_id, variation_id 
				FROM product__product_to_variation 
				INNER JOIN product__variation
					USING (variation_id)
				WHERE variation_category_id ={$rs['variation_category_id']}
					AND product_id = $product_id
					AND variation_status = 'onsale'
				ORDER BY product__variation.ordre
				LIMIT 1";

				$first_variation = Db::get_row( $query );

				if ($first_variation) {
					extract ($first_variation);
					// Poso a 0 totes primer per si n'hi ha alguna variation_status != 'onsale que estigui a 1
					$query = "
						UPDATE  product__product_to_variation
						INNER JOIN product__variation						
							USING (variation_id)
						SET  product__product_to_variation.variation_default =  0
						WHERE variation_category_id ={$rs['variation_category_id']}
							AND product_id = $product_id";
					Db::execute( $query );
					$query = "
						UPDATE  product__product_to_variation 
						SET  variation_default =  1
						WHERE product_variation_id = $product_variation_id";
					Db::execute( $query );

					print_javascript("top.$('#default_$variation_id').prop('checked', true);");

					Debug::add( 'Marco la primera com predeterminada', $query );
				}
			}
		}
	}
	// ----------------------------------------------------------------------------
	// control stock
	// ----------------------------------------------------------------------------
	// poso per vendre si hi ha stock, o poso no-stock si no n'hi ha
	// nomès canvia d'status si es igual a onsale o nostock 
	// només es fa quan hi ha control de stock i no es permet stock negatiu
	// si permeto negatiu i estat ha de ser per vendre també el poso ( per si a cas estava a nostock ),
	// 		amb permetre stock negatiu no ha d'haver mai cap a 'nostock'
	function set_stock_status($product_id, $found_variations, $stock){
		// si no hi ha control return
		if (!$this->config['stock_control']) return;
		// nomès canvia d'status si es igual a onsale o nostock 
		$query = "
				SELECT status
				FROM product__product
				WHERE product_id = '" . $product_id . "'
				AND (status = 'onsale' OR status = 'nostock')";
		$status = Db::get_first($query);
		// si status es qualsevol altre return
		if (!$status) return;
		
		// si trobo variacions, poso stock del producte a 0, ja que es conta l'stock desde les variacions
		if ($found_variations){
			Db::Execute("UPDATE product__product set stock='0' WHERE product_id ='" . $product_id . "'");
		}
		// si no te variacions, haig d'agafar l'stock del producte ( segurament serà cero si abans hi havia variacions, però haig de canviar l'estat segons això
		else {
			$stock = Db::get_first("
				SELECT stock
				FROM product__product
				WHERE product_id = '" . $product_id . "'");		
		}
		
		// si permeto negatiu i estat ha de ser per vendre també el poso ( per si a cas estava a nostock )
		if ($this->config['allow_null_stock'])
			Db::execute("UPDATE product__product set status='onsale' WHERE product_id ='" . $product_id . "'");
			
		$new_status = (intval($stock)>0)?'onsale':'nostock';
		if ($new_status==$status) return;
		// si no permeto negatiu canvio l'estat
		if (!$this->config['allow_null_stock'])
			Db::execute("UPDATE product__product set status='".$new_status."' WHERE product_id ='" . $product_id . "'");
	}
    // Per poder escollir variacions desde una finestra amb frame, desde variacions de productes
    function select_frames()
    {
        $page = &$GLOBALS['gl_page'];

        $page->menu_tool = false;
        $page->menu_all_tools = false;
        $page->template = '';

        $this->set_file('product/variation_select_frames.tpl');
        $this->set_vars($this->caption);
		
		$results = Db::get_rows(
			"SELECT variation_category , variation_category_id
				FROM  product__variation_category_language
				WHERE variation_category_id IN (SELECT Distinct variation_category_id FROM product__variation) 
				AND language='".LANGUAGE."'"
				);
		
		$category_select = '
		<select id="variation_category_select" onchange="Assign.show_category_list(this)">
			<option value="null">'.$this->caption['c_variation_category_select'].'</option>';
		
		foreach ($results as $rs){
			$category_select .= '<option value="'.$rs['variation_category_id'].'">'.$rs['variation_category'].'</option>';
		}	
				
		$category_select .= 
			"</select>"	;
		
        $this->set_var('category_select', $category_select);
		
        $this->set_var('menu_id', $GLOBALS['gl_menu_id']);
        // $this->set_var('custumer_id',$_GET['custumer_id']);
        $GLOBALS['gl_content'] = $this->process();
    }
	function manage_images()
	{
	    $image_manager = new ImageManager($this);
	    $image_manager->execute();
	}
}
?>