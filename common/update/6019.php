<?php

/* 18-12-14 
  
 * Booking: Marco si es reserva de propietari

 */

	
	
if (is_table('booking__book') && !(is_field('booking__book','is_owner'))){
	
	$update_sql .= "
	ALTER TABLE `booking__book`
		ADD COLUMN `is_owner` TINYINT(1) NOT NULL DEFAULT 0 AFTER `price_total`;
	";	
	
}
else{
	if (is_table('booking__book')) {		
		trigger_error('Ja existeixen el field booking__book.is_owner');
	}
}
	
?>