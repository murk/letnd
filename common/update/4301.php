<?php
/* 28-6-12 
Actualitzacio mostrar seo segons grup i mostrar html segons grup
*/
// no es poden posar ; entre mig de la consulta ja que parteix les consultes per el ; a no ser que fem servir $update_sql en array()

$update_sql[]="ALTER TABLE `user__group`
	ADD COLUMN `show_html_editor` TINYINT(1) NOT NULL DEFAULT '0' AFTER `edit_only_itself`;";
	
$update_sql[]="ALTER TABLE `user__group`
	ADD COLUMN `show_seo` TINYINT(1) NOT NULL DEFAULT '0' AFTER `edit_only_itself`;";	
	
$update_sql[]="UPDATE `user__group`
	set `show_seo` = '1', `show_html_editor` = '1' WHERE group_id = 1;";
	
?>