<? //debug::p($_SESSION);

/**
 * ProductCart
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2010
 * @version $Id$
 * @access public
 */
class ProductCart extends Module {
	var $is_block = false;
	var $is_ajax = false;
	var $is_get_cart_vars = false;
	var $cart_block_class = '';
	var $has_results; // faig servir en el block per saber si hi ha resultats
	var $payment_method = '';
	var $ajax_error = '';
	var $order_id = 0;

	function __construct() {
		parent::__construct();
	}

	function on_load() {
		// si es privada no deixo fer res excepte entrar login i password
		if ( $this->is_index && $this->config['is_private_shop'] && ! $this->is_logged() ) {
			redirect( '/?tool=product&tool_section=customer&action=login&language=' . LANGUAGE );
			die();
		}
	}

	// obtinc html del list_records per al block
	function get_records_block() {
		// al carregar tota la pagina està amagat
		$this->cart_block_class = ' hidden';

		$this->is_block = true;

		return $this->get_records();
	}

	function list_records() {
		$GLOBALS['gl_page']->title = $this->caption['c_title_cart'];
		$GLOBALS['gl_content']     = $this->get_records();
	}

	// assigno html del list_records al gl_content per qualsevol crida ajax (va sense template general, nomes content)
	function list_records_ajax() {
		$this->is_block = $_GET['is_block'] == '1';
		$this->is_ajax  = true;

		if ( $this->is_block && $this->action == 'add_item' ) {
			// al afegir, es fa fora del block ( el boto del llistat o del form ) per tant no mostro el carro
			// Al canviar quantitat si mostro el carro ( de fet no deixo de mostrar-lo )
			$this->cart_block_class = ' hidden';
		}

		//header('Content-Type: text/html; charset=utf-8');
		$GLOBALS['gl_page']->template = '';
		$content                      = $this->get_records();

		if ( $this->config['use_v2_cart'] ) {
			$ret['content']        = $content['content'];
			$ret['total_quantity'] = $content['totals']['total_quantity'];
			$ret['total_price']    = $content['totals']['total_price'];
			$ret['js_error']       = $this->ajax_error;
			Debug::p_all();
			header( 'Content-Type: application/json' );
			die ( json_encode( $ret ) );
		}
		else {
			$GLOBALS['gl_content'] = $content;
		}
	}

	/**
	 *  Per obtindre les variables des de qualsevol altre mòdul, sense cap html
	 */
	function get_cart_vars() {
		$this->is_get_cart_vars = true;

		return $this->get_records();
	}

	function get_records() {
		// obtinc id dels productes del carro
		$ids = $this->get_ids();
		if ( ! $ids['product_ids'] ) return $this->end_records();
		$pvp_name = $this->is_wholesaler() ? 'pvd' . $this->get_wholesaler_rate() : 'pvp';
		// carrego mòdul productes pel llistat de productes del carro
		$module            = Module::load( 'product', 'product' );
		$module->parent    = 'ProductCart';
		$module->condition = "
			AND product_id IN (" . $ids['product_ids'] . ")
			AND status='onsale'
			AND " . $pvp_name . " !=0
			AND sell=1
			AND price_consult=0";
		// condicions per poder comprar: ha d'estar a la venta, ha d'haver preu, ha d'haver 'sell' marcat, ha d'estar 'price_consult' desmarcat

		// el mòdul de productes em retorna el listing quan el crido des d'aquí
		$listing                   = $module->do_action( 'get_records' );
		$listing->paginate         = false;
		$listing->propagate_params = false;
		$listing->join             = "JOIN product__tax USING (tax_id)";
		$listing->add_field( 'tax_value' );

		$listing->list_records( false );
		$listing->has_results = $this->has_results;
		if ( ! $listing->results ) return $this->end_records();

		Debug::p( [
			[ 'Results abans variacions', $listing->results ],
		], 'cart' );

		// obtinc les variacions i els preus totals
		$listing->results = $this->get_variations( $listing->results, $ids['product_variation_ids'], $module );

		Debug::p( [
			[ 'Results despres variacions', $listing->results ],
		], 'cart' );

		$totals = $this->get_totals( $listing->results, $module, $ids );

		// listing de productes amb la plantilla del carro
		$listing->template = 'product/cart_list.tpl';
		$listing->set_vars( $totals );
		$listing->set_var( 'is_block', $this->is_block );
		$listing->set_var( 'is_summary', $this->parent == 'ProductPayment' ); // també es true a order
		$listing->set_var( 'is_downloadable', false );
		$listing->set_var( 'is_ajax', $this->is_ajax );
		$listing->set_var( 'is_invoice', false );
		$listing->set_var( 'is_payment', $this->parent == 'ProductPayment' ); // nomes es true si estem pagant
		$listing->set_var( 'is_order', false );
		$listing->set_var( 'is_logged', $this->is_logged() );
		$listing->set_var( 'cart_block_class', $this->cart_block_class );

		// links en el carro
		$listing->set_var( 'cart_link', '/?' . $this->base_link );

		// en el summary poso camps extra
		$this->add_summary_fields( $listing );

		if ( USE_FRIENDLY_URL ) {
			$payment_link = Page::get_link( 'product', 'payment', 'login', false, array( 'process' => 'identify' ), false );
		}
		else {
			$payment_link = '/?tool=product&tool_section=payment&language=' . LANGUAGE . '&process=identify&action=login';
		}
		$listing->set_var( 'payment_link', $payment_link );

		// desde order save_cart() per guardar productes i totals a les comandes
		// Aqui no calen imatges
		if ( $this->parent == 'ProductOrder' || $this->is_get_cart_vars )
			return array( 'results' => $listing->results, 'totals' => $totals );

		$listing->parse_template( false );

		$listing->call_after_set_records( 'after_records_walk', 'loop', $this );

		$content = $listing->process();

		if ( $this->is_ajax && $this->is_block && $this->config['use_v2_cart'] ) {
			return array( 'content' => $content, 'totals' => $totals );
		}

		return $content;
	}

	function add_summary_fields( &$listing ) {

		$customer_comment = $is_gift_card = $is_gift_card_value = '';
		if ( $this->parent == 'ProductPayment' ) {
			$customer_comment =
				empty( $_SESSION['product']['cart_vars']['customer_comment'] ) ?
					'' :
					htmlspecialchars( $_SESSION['product']['cart_vars']['customer_comment'] );

			$customer_comment = '<textarea name="customer_comment">' . $customer_comment . '</textarea>';


			if ( $this->config['has_gift_card'] ) {
				$is_gift_card_value =
					empty( $_SESSION['product']['cart_vars']['is_gift_card'] ) ?
						'' :
						$_SESSION['product']['cart_vars']['is_gift_card'];

				$checked            = $is_gift_card_value == '1' ? ' checked' : '';
				$is_gift_card = "<input value='1' $checked type=\"checkbox\" name=\"is_gift_card\">";
			}

		}


		$listing->set_var( 'customer_comment', $customer_comment );
		$listing->set_var( 'is_gift_card', $is_gift_card );
		$listing->set_var( 'has_gift_card', $this->config['has_gift_card']  );
		$listing->set_var( 'is_gift_card_value', $is_gift_card_value );
	}

	function after_records_walk( $rs ) {

		Main::load_class( 'product', 'common_order', 'admin' );

		return ProductCommonOrder::cart_get_variation_image( $rs );

	}


	function end_records() {
		if ( $this->is_block ) {
			if ( $this->config['cart_parse_template'] ) {
				$this->set_file( 'product/cart_list.tpl' );
				$this->set_loop( 'loop', array() );
				$this->set_var( 'is_block', $this->is_block );
				$this->set_var( 'is_summary', $this->parent == 'ProductPayment' );
				$this->set_var( 'is_downloadable', false );
				$this->set_var( 'is_ajax', $this->is_ajax );
				$this->set_var( 'is_invoice', false );
				$this->set_var( 'is_payment', $this->parent == 'ProductPayment' ); // nomes es true si estem pagant
				$this->set_var( 'is_order', false );
				$this->set_var( 'is_logged', $this->is_logged() );
				$this->set_var( 'cart_block_class', $this->cart_block_class );
				$this->set_var( 'language', $GLOBALS['gl_language'] );
				$this->set_vars( $this->caption );


				if ( $this->is_ajax && $this->is_block && $this->config['use_v2_cart'] ) {
					return array( 'content' => $this->process(), 'totals' => '0' );
				}

				return $this->process();
			}
			else {
				return $this->messages['no_records'];
			}
		}
		// si la cistella es buida recarrego la page per treure els botons de sota
		elseif ( $this->is_ajax ) {
			print_javascript( 'window.location.replace(window.location);' );

			return false;
		}
		else {
			$GLOBALS['gl_message'] = $this->messages['no_records'];

			return false;
		}
	}
	// duplico els resultats que siguin necessaris per fer les combinacions de variacions
	// per tant genero un nou results amb els productes amb variacions duplicats per cada combinació de variacions
	function get_variations( &$results, $variations_ids, &$module ) {
		$results_new = array();

		$new_cart = [];
		$cart     = $_SESSION['product']['cart'];

		foreach ( $results as $rs ) {
			$product_ids [] = $rs['product_id'];
		}

		Debug::p( [
			[ 'Product_ids', $product_ids ],
		], 'cart' );

		// Poso al cart només els Ids de productes que han sortit
		// ( pot ser que algún del cart ja no hi sigui )
		foreach ( $cart as $k => $v ) {

			$product_id = explode( '_', $k )[0];

			if ( in_array( $product_id, $product_ids ) ) {
				$new_cart[ $k ] = $v;
			}

		}
		$_SESSION['product']['cart'] = $new_cart;

		Debug::p( [
			[ 'new_cart', $new_cart ],
			[ 'Sessió new_cart', $_SESSION['product']['cart'] ],
		], 'cart' );

		// Abans de posar dates
		foreach ( $results as $rs ) {
			// si existeix l'array amb aquest id de producte, hi ha variacions i ho duplico
			if ( isset( $variations_ids[ $rs['product_id'] ] ) ) {
				foreach ( $variations_ids[ $rs['product_id'] ] as $val ) {
					//$rs_new = $rs;
					$rs_new        = array_merge( $rs, $this->get_variations_rs( $val, $rs['product_id'], $module, $rs['discount'], $rs['discount_fixed_price'], $rs['discount_wholesaler'], $rs['discount_fixed_price_wholesaler'] ) );
					$results_new[] = $rs_new;
				}
			}
			else {
				$rs['product_date']                = '';
				$rs['product_variations']          = ''; // per guardar a product__orderitem
				$rs['product_variation_categorys'] = ''; // per guardar a product__orderitem
				$rs['variation_increment']         = 0;
				$rs['old_variation_increment']     = 0;
				$rs['product_variation_ids']       = '0';
				$rs['cart_key']                    = $rs['product_id'] . '_' . $rs['product_variation_id'];
				$results_new[]                     = $rs;
			}
		}

		return $results_new;
	}

	function get_variations_rs( $ids, $product_id, &$module, $discount, $discount_fixed_price, $discount_wholesaler, $discount_fixed_price_wholesaler ) {

		// Si es distribuidor, agafo increment del preu de distribuidor
		$is_wholesaler = $this->is_wholesaler();
		$which_pv      = $is_wholesaler ? 'variation_pvd' . $this->get_wholesaler_rate() : 'variation_pvp';

		$ids_in  = implode( ',', $ids['variations'] );
		$query   = "
				SELECT 
					variation, 
					product__product_to_variation." . $which_pv . " as variation_pvp,
					product__product_to_variation.variation_pvd as variation_pvd, 
					variation_category, 
					variation_ref,
					product_variation_id
				FROM product__variation_language, 
					product__variation_category_language, 
					product__product_to_variation, 
					product__variation 
				WHERE product__variation_language.variation_id = product__product_to_variation.variation_id
					AND  product__variation.variation_id = product__product_to_variation.variation_id
					AND  product__variation_category_language.variation_category_id = product__variation.variation_category_id
					AND  product__variation_language.language = product__variation_category_language.language
					AND bin = 0
					AND product_variation_id IN (" . $ids_in . ")
					AND product__variation_category_language.language = '" . LANGUAGE . "'";
		$results = Db::get_rows( $query );

		$ret                                = array();
		$ret['product_variations']          = '';
		$ret['product_variation_categorys'] = '';
		$ret['product_variation_loop']      = array();
		$ret['variation_increment']         = 0;
		$ret['old_variation_increment']     = 0;

		foreach ( $results as $rs ) {
			if ( $this->parent == 'ProductOrder' ) {
				$ret['product_variations']          .= $rs['variation'] . "\r"; // guardo cada un amb un salt de linea ( que no es pot entrar de cap manera al insertar una variació ), així tinc un caracter vàlid per fer l'explode desprès
				$ret['product_variation_categorys'] .= $rs['variation_category'] . "\r"; // guardo cada un amb un salt de linea
			}
			else {
				$ret['product_variations']          .= $rs['variation'] . ', ';
				$ret['product_variation_categorys'] .= "<strong>" . $rs['variation_category'] . ":</strong> " . $rs['variation'] . ', ';
			}
			$ret['old_variation_increment'] += $rs['variation_pvp'];
			$ret['variation_increment']     += $module->get_variation_discount( $rs['variation_pvp'], $discount, $discount_fixed_price, $discount_wholesaler, $discount_fixed_price_wholesaler );


			$ret['product_variation_loop'][] = array(
				'product_variation_id'       => $rs['product_variation_id'],
				'product_variation_category' => $rs['variation_category'],
				'product_variation'          => $rs['variation']
			);
		}

		// Només mostro old_price si hi ha hagut algún descompte
		if ( $ret['variation_increment'] == $ret['old_variation_increment'] ) $ret['old_variation_increment'] = 0;

		if ( $this->parent != 'ProductOrder' ) {
			$ret['product_variations']          = substr( $ret['product_variations'], 0, - 2 );
			$ret['product_variation_categorys'] = substr( $ret['product_variation_categorys'], 0, - 2 );
		}

		$ids_key                      = implode( '_', $ids['variations'] );
		$ret['product_variation_ids'] = $ids_key;
		$ret['product_date']          = format_date_list( $ids['product_date'] );
		$ret['cart_key']              = $product_id . '_' . $ids_key;

		if ( $ids['product_date'] ) {
			$ret['cart_key'] = $ret['cart_key'] . '|' . $ids['product_date'];
		}

		// en més d'una variació en un producte, stock, unitats venudes i ref deixa de funcionar
		if ( count( $results ) == 1 && $rs['variation_ref'] ) $ret['ref'] = $rs['variation_ref'];

		return $ret;
	}

	function get_totals( &$results, &$module, $ids ) {
		// "base" = base sense iva
		// "tax" = iva
		// "equivalencia" = impost d'equivalencia, quan un majorista ven a un botiguer que no es SA o SL i que no declara IVA
		// "basetax" = base + iva
		// "price" mostra amb iva o sense segons la configuracio de la intra
		//
		// PREU DEL PRODUCTE
		// product_base
		// product_tax
		// product_equivalencia
		// product_basetax
		// product_price
		//
		// PREU DEL PRODUCTE * QUANTITAT
		// product_total_basetax
		// product_total_base
		// product_total_equivalencia
		// product_total_tax
		// product_total_price
		//
		// TOTAL SUMA PRODUCTES
		// total_basetax
		// total_base
		// total_equivalencia
		// total_tax
		// total_price
		//
		// TOTAL DESPESES ENVIAMENT
		// rate_base
		// rate_tax
		// rate_basetax
		// rate_price
		//
		// TOTAL SUMA PRODUCTES + DESPESES ENVIAMENT - DESCOMPTE PROMOCIONAL
		// all_basetax
		// all_base
		// all_equivalencia
		// all_tax
		// all_price
		//
		// TOTAL SUMA PRODUCTES + DESPESES ENVIAMENT abans d'aplicar descompte
		// TOTAL DESCOMPTE PROMOCIONAL
		// promcode_all_basetax
		// promcode_discount


		// $cart = $_SESSION['product']['cart'];
		// totals absoluts de tots els productes i quantitats
		$r['total_base'] = $r['total_tax'] = $r['total_equivalencia'] = $r['total_basetax'] = $r['total_price'] = $r['total_quantity'] = $r['promcode_discount'] = $r['promcode_all_basetax'] = 0;

		// calculo si l'iva està inclòs en el preu
		$is_tax_included      = $this->is_wholesaler() ? $this->config['is_wholesaler_tax_included'] : $this->config['is_public_tax_included'];
		$r['is_tax_included'] = $is_tax_included;

		// iva a aplicar a les despeses
		$rate_tax_value = $this->get_rate_tax_value();

		foreach ( $results as $key => $rs ) {

			// poso 'res' perque rs es perd fora el bucle
			$res = &$results[ $key ];

			// aplico descompte a pvp
			$rs = array_merge( $rs, $module->get_discount( $rs['pvp'], $rs[ 'pvd' . $this->get_wholesaler_rate() ], $rs['discount'], $rs['discount_fixed_price'], $rs['discount_wholesaler'], $rs['discount_fixed_price_wholesaler'], $rs['price_consult'] ) ); // només em canvia el rs aquí, desprès el list_records_walk es crida més endavant però necessitava el pvp calculat aquí per calcular bé la resta

			// agafo quantitat del session
			//$quantity =  $this->check_stock($cart[$rs['cart_key']]['quantity'], $rs['product_id'], $rs['product_variation_ids']);
			$quantity = $this->check_stock( $ids[ $rs['cart_key'] ]['quantity'], $rs['product_id'], $rs['product_variation_ids'] );
			//debug::p($rs);

			// calculo tots els imports aquí i ho dono calculat a la bbdd i al tpl
			// així totes les operacions es queden aquí
			$rs['pvp'] = $rs['pvp'] + $rs['variation_increment'];

			/*Debug::p( [
						[ 'rs', $rs ],
					], 'productDiscount');*/

			$tax_value = $this->get_tax_value( $rs['tax_value']);

			if ( $is_tax_included ) {
				$product_base    = $rs['pvp'] / ( ( $tax_value / 100 ) + 1 );// base sense iva
				$product_basetax = $rs['pvp']; //  base+ iva, o sigui iva inclos
				$product_old_base    = $rs['old_price'] / ( ( $tax_value / 100 ) + 1 );// base sense iva
				$product_old_basetax = $rs['old_price']; //  base+ iva, o sigui iva inclos
			}
			// l'iva no està inclòs
			else {
				$product_base    = $rs['pvp'];
				$product_basetax = $rs['pvp'] * ( ( $tax_value / 100 ) + 1 ); //  base+ iva, o sigui iva inclos
				$product_old_base    = $rs['old_price'];
				$product_old_basetax = $rs['old_price'] * ( ( $tax_value / 100 ) + 1 ); //  base+ iva, o sigui iva inclos

			}
			$product_tax = $product_basetax - $product_base; // iva corresponent
			// equivalencia no està inclos encara que sigui iva inclos, es un impost apart que es cobra nomès a distribuidors petits
			// Nomès el posaré al total absolut de la compra ( no ho desgloso ni en producte ni en total del producte )
			$product_equivalencia = $this->get_equivalencia( $product_base, $rs['tax_id'] );
			$product_basetax      = $product_basetax + $product_equivalencia;
			$product_price        = $rs['pvp'];

			$product_total_base         = $product_base * $quantity;
			$product_total_basetax      = $product_basetax * $quantity;
			$product_total_tax          = $product_tax * $quantity;
			$product_total_equivalencia = $product_equivalencia * $quantity;
			$product_total_price        = $product_price * $quantity;


			// assigno tot lo calculat formatejat al $results per anar al tpl o bbdd			
			$res['quantity']             = $quantity;
			$res['product_base']         = number_format( $product_base, 2, ',', '.' );
			$res['product_old_base']     = number_format( $product_old_base, 2, ',', '.' );
			$res['product_tax']          = number_format( $product_tax, 2, ',', '.' );
			$res['product_equivalencia'] = format_currency( $product_equivalencia, false );
			$res['product_basetax']      = number_format( $product_basetax, 2, ',', '.' );
			$res['product_old_basetax']  = number_format( $product_old_basetax, 2, ',', '.' );
			$res['product_price']        = number_format( $product_price, 2, ',', '.' );
			$res['product_discount']     = $rs['discount']; // sempre és el %, calculat en el cas de fixed
			$res['product_discount_fixed'] = $rs['discount_fixed'] ? 1 : 0;

			$res['product_total_basetax']      = number_format( $product_total_basetax, 2, ',', '.' );
			$res['product_total_base']         = number_format( $product_total_base, 2, ',', '.' );
			$res['product_total_equivalencia'] = format_currency( $product_total_equivalencia, false );
			$res['product_total_tax']          = number_format( $product_total_tax, 2, ',', '.' );
			$res['product_total_price']        = number_format( $product_total_price, 2, ',', '.' );

			// "total_" = vaig sumant tot al total absolut
			$r['total_basetax']      += $product_total_basetax;
			$r['total_base']         += $product_total_base;
			$r['total_equivalencia'] += $product_total_equivalencia;
			$r['total_tax']          += $product_total_tax;
			$r['total_price']        += $product_total_price;
			$r['total_quantity']     += $quantity;

		}

		if ( $this->is_logged() ) {

			$r['show_rate'] = true;

			// les despeses d'enviament i el preu minim per enviament gratuit porten o no iva segons la part publica (is_public_tax_included), per distribuidors també, sino hauriem de posar 2 preus
			if ( $this->config['is_public_tax_included'] )
				$total_to_compare_for_free_send = $r['total_basetax'];
			else
				$total_to_compare_for_free_send = $r['total_base'];


			$r['rate_base']                        =
			$r['rate_tax'] =
			$r['rate_price'] =
			$r['rate_basetax'] = '0';
			$r['rate_encrease_ondelivery_basetax'] = $r['rate_encrease_ondelivery_base'] = $r['rate_encrease_ondelivery_tax'] = $r['rate_encrease_ondelivery_price'] = '';

			$rates = $this->get_rate( $results, $total_to_compare_for_free_send );

			$rate                     = $rates['rate'];
			$rate_encrease_ondelivery = $rates['rate_encrease_ondelivery'];

			if ( $rate != '0' ) {


				// l'iva està inclòs en el preu
				if ( $this->config['is_public_tax_included'] ) {
					$r['rate_base']    = $rate / ( ( $rate_tax_value / 100 ) + 1 );// base sense iva
					$r['rate_basetax'] = $rate; //  base+ iva, o sigui iva inclos

					$r['rate_encrease_ondelivery_base']    = $rate_encrease_ondelivery / ( ( $rate_tax_value / 100 ) + 1 );// base sense iva
					$r['rate_encrease_ondelivery_basetax'] = $rate_encrease_ondelivery; //  base+ iva, o sigui iva inclos
				}
				// l'iva no està inclòs
				else {
					$r['rate_base']    = $rate;
					$r['rate_basetax'] = $rate * ( ( $rate_tax_value / 100 ) + 1 ); //  base+ iva, o sigui iva inclos

					$r['rate_encrease_ondelivery_base']    = $rate_encrease_ondelivery;
					$r['rate_encrease_ondelivery_basetax'] = $rate_encrease_ondelivery * ( ( $rate_tax_value / 100 ) + 1 ); //  base+ iva, o sigui iva inclos

				}
				$r['rate_tax']   = $r['rate_basetax'] - $r['rate_base'];
				$r['rate_price'] = $is_tax_included ? $r['rate_basetax'] : $r['rate_base'];

				$r['rate_encrease_ondelivery_tax']   = $r['rate_encrease_ondelivery_basetax'] - $r['rate_encrease_ondelivery_base'];
				$r['rate_encrease_ondelivery_price'] = $is_tax_included ? $r['rate_encrease_ondelivery_basetax'] : $r['rate_encrease_ondelivery_base'];
			}

		}
		else {
			$r['rate_basetax']                     = $r['rate_base'] = $r['rate_tax'] = $r['rate_price'] = '';
			$r['rate_encrease_ondelivery_basetax'] = $r['rate_encrease_ondelivery_base'] = $r['rate_encrease_ondelivery_tax'] = $r['rate_encrease_ondelivery_price'] = '';
			$r['show_rate']                        = false;
		}


		// ajusto preus per que no hi hagi el desquadre del redondeig
		// si l'iva està inclòs en el preu, llavors la suma de tots els preus més iva estan be
		// si l'iva no està inclòs, llavors les sumes de tots els productes sense iva estan be
		if ( $is_tax_included ) {
			$r['total_base'] = $r['total_basetax'] - $r['total_tax'] - $r['total_equivalencia'];
		}
		else {
			$r['total_basetax'] = $r['total_base'] + $r['total_tax'] + $r['total_equivalencia'];
		}

		// "all_" = preus totals més gastos d'envio
		$this->get_totals_all( $r );
		$this->get_deposit( $r, $results );


		// descompte promocional
		if ( $this->is_logged() && $this->order_id ) {
			$this->get_promcode_from_order( $r, $is_tax_included );
		}
		elseif ( $this->is_logged() ) {

			if ( isset( $_POST['promcode'] ) ) $_SESSION['product']['cart_vars']['promcode'] = $_POST['promcode'];
			$promcode = isset( $_SESSION['product']['cart_vars']['promcode'] ) ? $_SESSION['product']['cart_vars']['promcode'] : false;

			// fins que no comprovo el codi, mostrar codi es false i mostrar form es true
			$r['show_promcode']      = false;
			$r['show_promcode_form'] = true;
			$r['promcode_error']     = false;
			$r['promcode_form']      = '<form class="promcode" action="" method="post"><input type="text" name="promcode" /><input type="submit" name="promcodebtn" value="' . $this->caption['c_promcode_button'] . '" /></form>';

			// per fer-ho sense form
			$r['promcode_input']  = '<input type="text" name="promcode" />';
			$r['promcode_button'] = '<input onclick="payment_get_promcode(this.form)" type="button" name="promcodebtn" value="' . $this->caption['c_promcode_button'] . '" />';

			// si promcode canbio mostrar promcode o mostrar_form
			if ( $promcode !== false ) {
				$this->get_promcode( $r, $promcode, $is_tax_included );
			}


		}
		else {
			$r['show_promcode']      = false;
			$r['show_promcode_form'] = false;
		}


		// formatejo a: 12.000,70  // canviar-ho per curency_format
		$r['total_base']         = number_format( $r['total_base'], 2, ',', '.' );
		$r['total_tax']          = number_format( $r['total_tax'], 2, ',', '.' );
		$r['total_equivalencia'] = format_currency( $r['total_equivalencia'], false );
		$r['total_basetax']      = number_format( $r['total_basetax'], 2, ',', '.' );
		$r['total_price']        = number_format( $r['total_price'], 2, ',', '.' );

		if ( $r['show_rate'] ) {
			$r['rate_base']    = number_format( $r['rate_base'], 2, ',', '.' );
			$r['rate_tax']     = number_format( $r['rate_tax'], 2, ',', '.' );
			$r['rate_basetax'] = number_format( $r['rate_basetax'], 2, ',', '.' );
			$r['rate_price']   = number_format( $r['rate_price'], 2, ',', '.' );

			$r['rate_encrease_ondelivery_base']    = format_currency( $r['rate_encrease_ondelivery_base'], false );
			$r['rate_encrease_ondelivery_tax']     = format_currency( $r['rate_encrease_ondelivery_tax'], false );
			$r['rate_encrease_ondelivery_basetax'] = format_currency( $r['rate_encrease_ondelivery_basetax'], false );
			$r['rate_encrease_ondelivery_price']   = format_currency( $r['rate_encrease_ondelivery_price'], false );
		}

		$r['all_base']         = number_format( $r['all_base'], 2, ',', '.' );
		$r['all_tax']          = number_format( $r['all_tax'], 2, ',', '.' );
		$r['all_equivalencia'] = format_currency( $r['all_equivalencia'], false );
		$r['all_basetax']      = number_format( $r['all_basetax'], 2, ',', '.' );
		$r['all_price']        = number_format( $r['all_price'], 2, ',', '.' );

		if ( $r['show_promcode'] ) {
			$r['promcode_discount']    = number_format( $r['promcode_discount'], 2, ',', '.' );
			$r['promcode_all_basetax'] = number_format( $r['promcode_all_basetax'], 2, ',', '.' );
		}

		$_SESSION['product']['cart_vars']['all_basetax']                      = $r['all_basetax'];
		$_SESSION['product']['cart_vars']['rate_encrease_ondelivery_basetax'] = $r['rate_encrease_ondelivery_basetax'];

		// Només Lassdive
		if ( isset( $r['deposit'] ) ) {
			$r['deposit']                                     = number_format( $r['deposit'], 2, ',', '.' );
			$r['deposit_rest']                                = number_format( $r['deposit_rest'], 2, ',', '.' );
			$_SESSION['product']['cart_vars']['deposit']      = $r['deposit'];
			$_SESSION['product']['cart_vars']['deposit_rest'] = $r['deposit_rest'];
		}

		// poso els captions neutres per quan faig servir price ( en serveix amb iva i sense )
		$c = &$module->caption;

		// s'utilitza a kupukupu, de moment nomes calen aquests
		$r['c_total_price'] = $is_tax_included ? $c['c_total_basetax'] : $c['c_total_base'];
		$r['c_all_price']   = $is_tax_included ? $c['c_all_basetax'] : $c['c_all_base'];

		Debug::add( 'Resultats totals', $r );
		Debug::add( 'Resultats per producte', $results );

		return $r;
	}

	// em calcula tots els all_  -> així la puc reutilitzar a promcode
	function get_totals_all( &$r ) {

		$r['all_base']         = $r['total_base'] + $r['rate_base'];
		$r['all_tax']          = $r['total_tax'] + $r['rate_tax'];
		$r['all_equivalencia'] = $r['total_equivalencia'];// com que equivalencia no s'aplica
		$r['all_basetax']      = $r['total_basetax'] + $r['rate_basetax']; // total es total de tot, producs, iva i despeses
		$r['all_price']        = $r['total_price'] + $r['rate_price'];

	}

	private function get_rate_tax_value() {
		if ( $this->customer_has_no_vat() ) return 0;
		else return Db::get_first( 'SELECT tax_value FROM product__tax WHERE tax_id=1' );
	}

	private function get_tax_value( $tax_value ) {
		if ( $this->customer_has_no_vat() ) return 0;
		else return $tax_value;
	}

	private function customer_has_no_vat() {
		if ( $this->is_wholesaler() ) {
			$customer_id = $_SESSION['product']['customer_id'];
			$has_no_vat  = Db::get_first( 'SELECT has_no_vat FROM product__customer WHERE customer_id = %s', $customer_id );
			if ( $has_no_vat ) return true;
		}

		return false;
	}

	// Fet només per Lassdive, calcula només el price, en aquest cas té iva inclòs,
	// sino fos així s'hauria de calcular amb IVA i calcular sense IVA
	function get_deposit( &$r, &$results ) {

		if ( ! $this->config['deposit_payment_on'] || ! $this->config['deposit_payment_percent'] ) return;


		if ( $this->check_deposit_familys( $results ) && $this->check_is_gift_card()) {

			$deposit = $this->get_deposit_amount( $r['total_basetax'] );

			// Només per Lassdive, no cal complicar-se, l'IVA està inclòs
			$r['deposit']       = $deposit + $r['rate_basetax'];
			$r['deposit_value'] = $r['deposit'];
			$r['deposit_rest']  = $r['total_basetax'] - $deposit;
		}
		else {
			$r['deposit']       = 0;
			$r['deposit_value'] = 0;
			$r['deposit_rest']  = $r['total_basetax'];
		}

	}

	function get_deposit_amount( $amount ) {

		$percent = (double) $this->config['deposit_payment_percent'];

		$round      = (double) $this->config['deposit_payment_round'];
		$rest_round = (double) $this->config['deposit_rest_round'];

		$deposit = $amount * $percent / 100;

		if ( $round ) {
			$deposit = round( $amount / $round ) * $round;
		}
		elseif ( $rest_round ) {
			$rest    = $amount - $deposit;
			$rest    = round( $rest / $rest_round ) * $rest_round;
			$deposit = $amount - $rest;
		}

		return $deposit;
	}

	function check_deposit_familys( &$results ) {
		foreach ( $results as $rs ) {
			$family_id               = $rs['family_id'];
			$query                   = "SELECT accepts_deposit_payment FROM product__family WHERE family_id = $family_id";
			$accepts_deposit_payment = Db::get_first( $query );

			if ( ! $accepts_deposit_payment ) return false;
		}

		return true;
	}


	function check_is_gift_card() {

		if ($this->order_id) {
			$query       = "SELECT is_gift_card FROM product__order WHERE order_id = %s";
			$is_gift_card = Db::get_first( $query, $this->order_id);
			return !$is_gift_card;
		}
		else {
			return empty( $_SESSION['product']['cart_vars']['is_gift_card'] );
		}
	}


	// retorna 3 variables, una es el descompte i l'altra preu total menys descompte i l'error
	function get_promcode_from_order( &$r, $is_tax_included ) {
		$discount                  = Db::get_first( "SELECT promcode_discount FROM product__order WHERE order_id = $this->order_id" );
		$r['show_promcode']        = false;
		$r['show_promcode_form']   = false;
		$r['promcode_discount']    = $discount;
		$r['promcode_all_basetax'] = $r['all_basetax'];

		// l'unic que puc fer es descomptar sobre el total quan hi ha iva
		// TODO o descomptar sobre la base quan no hi ha iva inclos
		// De moment nomes descompta del total, ho utilitza kaskote
		// O SIGUI SEMPRE DESCOMPTAR SOBRE all_price
		if ( $is_tax_included ) {
			$r['all_basetax'] = $r['all_basetax'] - $discount;
			$r['all_price']   = $r['all_price'] - $discount;
		}
		else {
			$r['all_basetax'] = $r['all_basetax'] - $discount;
			$r['all_price']   = $r['all_price'] - $discount;

			//$r['total_base'] = $r['total_base'] - $discount;
			//$this->get_totals_all( $r );
		}
	}

	// retorna 3 variables, una es el descompte i l'altra preu total menys descompte i l'error
	function get_promcode( &$r, $promcode, $is_tax_included ) {
		// si no arriba a despesa mínima es false
		// si no està entre les dates es false

		// Si es distribuidor, agafo el descompte de distribuidor
		$is_wholesaler = $this->is_wholesaler();
		$which_pv      = $is_wholesaler ? 'discount_pvd' . $this->get_wholesaler_rate() : 'discount_pvp';

		$rs_promcode = Db::get_row( "
							SELECT promcode_id, " . $which_pv . " as discount_pvp, discount_type, minimum_amount, start_date, end_date,
								((start_date=0 OR start_date <= NOW()) AND (end_date=0 OR end_date >= (CURDATE()))) as is_valid_date
								FROM product__promcode 
								WHERE bin=0
								AND promcode = '" . $promcode . "'" );

		if ( ! $rs_promcode || $rs_promcode['discount_pvp'] == 0 ) {
			$r['promcode_error'] = $this->caption['c_promcode_err_not_valid'];
			unset( $_SESSION['product']['cart_vars']['promcode'] );
		}
		elseif ( ! $rs_promcode['is_valid_date'] ) {
			$r['promcode_error'] = sprintf( $this->caption['c_promcode_err_expired'], $rs_promcode['start_date'], $rs_promcode['end_date'] );
			unset( $_SESSION['product']['cart_vars']['promcode'] );
		}
		elseif ( $r['all_basetax'] < $rs_promcode['minimum_amount'] ) {
			$r['promcode_error'] = sprintf( $this->caption['c_promcode_err_minimum'], number_format( $rs_promcode['minimum_amount'], 2, ',', '.' ) ) . ' ' . CURRENCY_NAME;
			unset( $_SESSION['product']['cart_vars']['promcode'] );
		}
		// si usuari ja ha disposat de la promocio
		elseif ( Db::get_first( "SELECT customer_id 
									FROM product__customer_to_promcode 
									WHERE customer_id = '" . $_SESSION['product']['customer_id'] . "'
									AND promcode_id = '" . $rs_promcode['promcode_id'] . "'" ) ) {
			$r['promcode_error'] = $this->caption['c_promcode_err_used'];
			unset( $_SESSION['product']['cart_vars']['promcode'] );
		}
		else {
			$r['show_promcode']      = true;
			$r['show_promcode_form'] = false;
			$r['promcode_form']      = '';
			// calculo import segons tipus
			if ( $rs_promcode['discount_type'] == 'fixed' ) {
				$discount = $rs_promcode['discount_pvp'];
			}
			else {
				$discount = $r['all_basetax'] * $rs_promcode['discount_pvp'] / 100;
			}
			$r['promcode_id']          = $rs_promcode['promcode_id'];
			$r['promcode_discount']    = $discount;
			$r['promcode_all_basetax'] = $r['all_basetax'];

			// all_basetax
			// all_base
			// all_equivalencia
			// all_tax
			// all_price
			//
			// no puc calcular ni base ni tax ja que cada productes te els seus ives diferents
			//

			// l'unic que puc fer es descomptar sobre el total quan hi ha iva
			// TODO o descomptar sobre la base quan no hi ha iva inclos
			// De moment nomes descompta del total, ho utilitza kaskote
			// O SIGUI SEMPRE DESCOMPTAR SOBRE all_price
			if ( $is_tax_included ) {
				$r['all_basetax'] = $r['all_basetax'] - $discount;
				$r['all_price']   = $r['all_price'] - $discount;
			}
			else {
				$r['all_basetax'] = $r['all_basetax'] - $discount;
				$r['all_price']   = $r['all_price'] - $discount;

				//$r['total_base'] = $r['total_base'] - $discount;
				//$this->get_totals_all( $r );
			}
		}
	}

	function get_rate( &$results, &$total_to_compare_for_free_send ) {

		$cg_admin = $this->get_config( 'admin', 'product__configadmin' );

		if ( $this->order_id ) {
			$collect = Db::get_first( "SELECT collect FROM product__order WHERE order_id = $this->order_id" );
		}
		else {
			$collect = @$_SESSION['product']['cart_vars']['collect'];
		}

		if ( $cg_admin['has_rate'] == '0' || $collect == '1' ) {
			return array( 'rate' => 0, 'rate_encrease_ondelivery' => 0 );
		}

		// Si nomes hi ha productes digitals, l'enviament és gratuit
		$is_digital_order = true;
		$can_free_send    = true;

		foreach ( $results as $rs ) {
			// Debug::p( $rs);

			if ( $is_digital_order ) {
				$query = "
				SELECT download_id
				FROM product__product_download
				WHERE product_id = '" . $rs['product_id'] . "' LIMIT 1";

				// nomes 1 sense producte digital ja fa que es cobrin ports
				if ( ! Db::get_first( $query ) ) $is_digital_order = false;
			}

			if ( $rs['free_send'] == 0 ) $can_free_send = false;
		}
		if ( $is_digital_order ) {
			return array( 'rate' => 0, 'rate_encrease_ondelivery' => 0 );
		}

		//
		// TROBO PAIS
		//
		$sql     = "SELECT country_id, provincia_id FROM product__address, product__customer WHERE delivery_id = address_id AND product__customer.customer_id = '" . $_SESSION['product']['customer_id'] . "'";
		$rs_addr = Db::get_row( $sql );

		// si nomès hi ha una adreça busco el country de la adreça de facturació
		if ( ! $rs_addr ) {
			$sql     = "SELECT country_id,provincia_id FROM product__address, product__customer WHERE product__customer.customer_id = product__address.customer_id AND product__customer.customer_id = '" . $_SESSION['product']['customer_id'] . "'";
			$rs_addr = Db::get_row( $sql );
		}

		$country_id   = $rs_addr['country_id'];
		$provincia_id = $rs_addr['provincia_id'];

		// 
		// VARIABLE DEL PAIS, ara ja nomes per brand
		// 
		$send_rate_country = '';

		switch ( $country_id ) {
			case 'ES':
				$send_rate_country = 'send_rate_spain';
				break;
			case 'FR':
				$send_rate_country = 'send_rate_france';
				break;
			case 'AD':
				$send_rate_country = 'send_rate_andorra';
				break;
			default:
				$send_rate_country = 'send_rate_world';
		} // switch


		//
		// CALCULA la tarifa normal
		//

		// te preferencia provincia
		$send_rates = Db::get_row( "SELECT rate, rate_encrease_ondelivery, minimum_buy_free_send FROM product__rate WHERE provincia_id = '" . $provincia_id . "'" );

		// segon el pais
		if ( ! $send_rates )
			$send_rates = Db::get_row( "SELECT rate, rate_encrease_ondelivery, minimum_buy_free_send FROM product__rate WHERE country_id = '" . $country_id . "'" );

		if ( $send_rates ) {
			$send_rate = $send_rates['rate'];
			if ( $this->payment_method == 'ondelivery' ) $send_rate += $send_rates['rate_encrease_ondelivery'];

			$send_rate_encrease_ondelivery = $send_rates['rate_encrease_ondelivery'];
			$minimum_buy_free_send         = (double) $send_rates['minimum_buy_free_send'] ? (double) $send_rates['minimum_buy_free_send'] : $cg_admin['minimum_buy_free_send'];
		}
		// tercer resta mon
		else {
			$send_rate = $cg_admin['send_rate_world'];
			if ( $this->payment_method == 'ondelivery' ) $send_rate += $cg_admin['send_rate_encrease_ondelivery_world'];

			$send_rate_encrease_ondelivery = $cg_admin['send_rate_encrease_ondelivery_world'];
			$minimum_buy_free_send         = $cg_admin['minimum_buy_free_send'];
		}


		if ( $this->config['is_brand_rate_on'] ) {


			//Debug::p($results, 'text');
			//
			// agrupo productes per marca
			// 
			// Es calcula
			//		1 - Productes que no tenen tarifa a la marca  ( tarifa normal i corrent )
			//		   S'aplica tarifa segons provincia o pais
			//		   
			//		2 - Cada producte que la marca te una tarifa
			//			S'aplica cada una de les tarifes, ja que les marques son les 
			//			que envien els productes directament de fàbrica i cada una cobra la seva
			//			
			// si trobo almenys un producte d'una marca sense tarifes, hi sumo a més la tarifa per defecte
			// calculo que es gasta en cada marca


			// minimum = 0 no es paga mai
			// send_rate_pais = 0 agafa valor general de gestio
			// per pagar sempre posar un valor molt alt

			$brands = array();

			// no defined es suma tots totals dels que no està definida una despesa en la marca, 
			// per calcular si hi ha despeses d'enviament op no
			$brands['no_defined'] = array(
				'price'                    => 0,
				'rate'                     => $send_rate,
				'rate_encrease_ondelivery' => $send_rate_encrease_ondelivery,
				'minimum'                  => $minimum_buy_free_send,
				'can_free_send'            => true,
				'items'                    => 0
			);

			foreach ( $results as $rs ) {
				if ( ! isset( $brands[ $rs['brand_id'] ] ) ) {

					$rs_brand = Db::get_row( "
						SELECT " . $send_rate_country . " as rate, minimum_buy_free_send 
						FROM product__brand
						WHERE brand_id = " . $rs['brand_id'] );

					$brands[ $rs['brand_id'] ] = array(
						'price'         => 0,
						'rate'          => $rs_brand['rate'],
						'minimum'       => $rs_brand['minimum_buy_free_send'],
						'can_free_send' => true,
						'items'         => 0,
						'no_defined'    => $rs_brand['rate'] == '0.00'
					);
				}

				// els no definits van tots junts
				if ( $brands[ $rs['brand_id'] ]['no_defined'] ) {
					$brands['no_defined']['price'] += unformat_currency( $rs['product_total_price'] );
					$brands['no_defined']['items'] += 1;
					if ( $rs['free_send'] == 0 ) $brands['no_defined']['can_free_send'] = false;
				}
				// els definits per marques
				else {
					$brands[ $rs['brand_id'] ]['price'] += unformat_currency( $rs['product_total_price'] );
					$brands[ $rs['brand_id'] ]['items'] += 1;
					if ( $rs['free_send'] == 0 ) $brands[ $rs['brand_id'] ]['can_free_send'] = false;
				}
			}

			$ret = 0;
			// bucle per brands per definir el preu final			
			foreach ( $brands as $key => &$brand_rs ) {

				// els no definits van tots junts
				if ( $key == 'no_defined' ) {

					if (
						$brands['no_defined']['can_free_send'] &&
						( $brands['no_defined']['price'] > $minimum_buy_free_send )
					)
						//&& $brands[$rs['brand_id']]['minimum']!=0)
						$brands['no_defined']['rate'] = 0;

					if ( $brands['no_defined']['items'] ) {
						$ret += $brands['no_defined']['rate'];
					}

				}
				// els definits per marques
				elseif ( $brand_rs['no_defined'] == false ) {
					// minimum 0 no es paga mai
					// per pagar sempre posar un valor molt alt
					if ( $brand_rs['can_free_send'] && ( $brand_rs['price'] > $brand_rs['minimum'] ) )
						$brand_rs['rate'] = 0;

					$ret += $brand_rs['rate'];
				}


			}

			Debug::p( $brands );
			Debug::p( $ret );

			// TODO - Rate ondelivery per les marques també
			return array( 'rate' => $ret, 'rate_encrease_ondelivery' => 0 );
			// return $ret;

		}
		else {
			// enviament gratuit
			//Debug::p($total_to_compare_for_free_send, '$total_to_compare_for_free_send');
			//Debug::p($can_free_send, '$can_free_send');
			//Debug::p($cg_admin['minimum_buy_free_send'], 'minimum_buy_free_send');

			if ( $can_free_send && ( $total_to_compare_for_free_send > $minimum_buy_free_send ) ) {

				return array( 'rate' => 0, 'rate_encrease_ondelivery' => 0 );
			}
			else {
				return array( 'rate' => $send_rate, 'rate_encrease_ondelivery' => $send_rate_encrease_ondelivery );
			}

		}

	}


	/**
	 * @return array|bool
	 * ids dels productes i variacions per posar al query
	 * <p>Faig un array així:</p>
	 * <pre>
	 * [
	 * 'product_ids' => 'id del producte,id del producte2',
	 *
	 * 'product_variation_ids' =>
	 * [
	 * 'id del producte' =>
	 * [0 =>
	 * [
	 * 'product_date' => '2000/10/01',
	 * 'quantity' => 'quantitat',
	 * 'variations' =>
	 * [
	 * 0 => 'id de la variació',
	 * 1 => 'id de la variació',
	 * ]
	 * ]
	 * ],
	 * ]
	 * ];
	 * </pre>
	 */
	function get_ids() {

		if ( $this->order_id ) {
			return $this->get_ids_saved();
		}
		else {
			return $this->get_ids_cart();
		}


	}

	function get_ids_saved() {

		$ret = array( 'product_ids' => array(), 'product_variation_ids' => array() );

		$query   = "
			SELECT product_id, product_date, product_variation_ids, quantity 
			FROM product__orderitem
			WHERE order_id = $this->order_id";
		$results = Db::get_rows( $query );

		if ( ! $results ) return false;

		foreach ( $results as $rs ) {

			$id                    = $rs['product_id'];
			$product_variation_ids = $rs['product_variation_ids'];

			$product_date = strtotime( $rs['product_date'] ) > 0 ? $rs['product_date'] : false;
			$quantity     = $rs['quantity'];
			$arr          = explode( '_', $product_variation_ids );

			$ret['product_ids'][] = $id;

			if ( ! isset( $ret['product_variation_ids'][ $id ] ) ) {
				$ret['product_variation_ids'][ $id ] = [];
			}

			$cart_key = $id . '_' . $product_variation_ids;

			if ( $product_date ) {
				$cart_key = $cart_key . '|' . $product_date;
			}

			$ret[ $cart_key ]['quantity'] = $quantity;

			$ret['product_variation_ids'][ $id ][] = [
				'product_date' => $product_date,
				'variations'   => $arr
			];

		}
		$ret['product_ids'] = implode( ',', $ret['product_ids'] );

		return $ret;

	}

	function get_ids_cart() {

		if ( ! isset( $_SESSION['product']['cart'] ) ) return false;
		Debug::add( 'Carro', $_SESSION['product']['cart'] );
		$keys = array_keys( $_SESSION['product']['cart'] );
		$ret  = array( 'product_ids' => array(), 'product_variation_ids' => array() );
		foreach ( $keys as $key ) {

			$arr          = explode( '|', $key );
			$ids          = $arr[0];
			$product_date = isset( $arr[1] ) ? $arr[1] : false;
			$quantity     = $_SESSION['product']['cart'][ $key ]['quantity'];
			$arr          = explode( '_', $ids );

			$ret['product_ids'][] = $id = $arr[0];
			unset( $arr[0] );

			if ( ! isset( $ret['product_variation_ids'][ $id ] ) ) {
				$ret['product_variation_ids'][ $id ] = [];
			}

			$ret[ $key ]['quantity'] = $quantity;

			$ret['product_variation_ids'][ $id ][] = [
				'product_date' => $product_date,
				'variations'   => $arr
			];

		}
		$ret['product_ids'] = implode( ',', $ret['product_ids'] );

		Debug::p( [
			[ 'Array ids carro', $ret ],
			[ 'Sessió carro', $_SESSION['product']['cart'] ],
		], 'cart' );

		return $ret;
	}


	/*
	FUNCIONS QUE ES CRIDEN PER AJAX
	*/
	// afegim un producte al carro
	function add_item() {
		$cart = &$_SESSION['product']['cart'];
		$id   = $this->get_product_variation_id();
		$id   .= $this->get_cart_product_date();

		$quantity = R::number( 'quantity' );


		if ( ! isset( $cart[ $id ] ) ) {
			$cart[ $id ] = array();
		}
		else {
			$quantity = $quantity + $cart[ $id ]['quantity'];
		}

		$quantity                = $this->check_stock_from_ajax( $quantity, $id );
		$cart[ $id ]['quantity'] = $quantity;

		// lassdive
		$extra_vars = R::post( 'extra_vars' );

		if ( $extra_vars ) {

			foreach ( $extra_vars as &$extra_var ) {
				$extra_var['cart_key'] = $id;
			}

			$product_id                             = R::id( 'product_id' );
			$cart_vars                              = &$_SESSION['product']['cart_vars'];
			$saved_extra_vars                       = ( isset( $cart_vars[ $product_id ]['extra_vars'] ) ) ? $cart_vars[ $product_id ]['extra_vars'] : [];
			$cart_vars[ $product_id ]['extra_vars'] = array_merge( $saved_extra_vars, $extra_vars );
		}
		// fi lassdive

		if ( ! $quantity ) {
			unset ( $cart[ $id ] );
			if ( ! $_SESSION['product']['cart'] ) {
				$this->empty_cart();
			}
		}

		$this->list_records_ajax();

	}

	// cambia la quantitat de productes
	function change_quantity() {
		$cart = &$_SESSION['product']['cart'];
		$id   = $this->get_product_variation_id();
		$id   .= $this->get_cart_product_date();

		$quantity = R::number( 'quantity' );

		$quantity                = $this->check_stock_from_ajax( $quantity, $id );
		$cart[ $id ]['quantity'] = $quantity;


		if ( ! $quantity ) {
			unset ( $cart[ $id ] );
		}

		$this->list_records_ajax();
	}

	function check_stock_from_ajax( $quantity, $id ) {

		$ids          = explode( '_', $id );
		$product_id   = $ids[0];
		$variation_id = ( $ids[1] != '0' ) ? $ids[1] : false;

		$new_quantity = $this->check_stock( $quantity, $product_id, $variation_id );

		if ( $new_quantity != $quantity ) {
			if ( $new_quantity == 0 ) {
				$message = $this->messages['c_no_stock_message'];
			}
			if ( $new_quantity > 0 ) {
				$message = $this->messages['c_max_stock_reached_message'];
			}

			$js = $GLOBALS['gl_page']->get_javascript_show_message( $GLOBALS['gl_message'], 'nostock' );
			$js .= 'else{
						if (typeof top.hidePopWin == "function") top.window.setTimeout("hidePopWin(false);", 1500);
					if (typeof top.showPopWin == "function") top.showPopWin("", 300, 100, null, false, false, "' . addslashes( $message ) . '");
						}';

			if ( $this->config['use_v2_cart'] ) {
				$this->ajax_error = $js;
			}
			else {
				print_javascript( $js );
			}
		}


		return $new_quantity;
	}
	// ----------------------------------------------------------------------------
	// control stock
	// ----------------------------------------------------------------------------
	// si no hi ha stock automatic o permeto venta negativa: no faig res
	// else: miro la quantitat d'estock i si la quantitat ho supera, retorno stock que es el màxim que es pot comprar
	function check_stock( $quantity, $product_id, $variation_id ) {

		// primer comprovo quantitat mínima i màxima del producte que es pot vendre

		$rs_quantity = Db::get_row( "SELECT min_quantity, max_quantity FROM product__product WHERE product_id = $product_id" );

		$min_quantity = $rs_quantity['min_quantity'];
		$max_quantity = $rs_quantity['max_quantity'];

		if ( $quantity < $min_quantity )
			$quantity = $min_quantity;

		// maxquantity 0 s'ignora, vol dir que no s'ha establert cap quantitat màxima
		if ( $max_quantity != 0 && $quantity > $max_quantity )
			$quantity = $max_quantity;


		if ( ! $this->config['stock_control'] || $this->config['allow_null_stock'] ) return $quantity; //vol dir que permeto vendre amb stock negatiu.

		// Comprovo stock si hi ha control d'stock
		if ( $variation_id ) {
			$query = "
				SELECT variation_stock
				FROM product__product_to_variation
				WHERE product_id = '" . $product_id . "'
				AND product_variation_id = '" . $variation_id . "'";
			$stock = Db::get_first( $query );
		}
		else {
			$query = "
				SELECT stock
				FROM product__product
				WHERE product_id = '" . $product_id . "'";
			$stock = Db::get_first( $query );
		}


		if ( $quantity > $stock ) $quantity = $stock;

		if ( $quantity < 0 ) {
			$quantity = 0; // pot ser que si permetia vendre amb stock negatiu, tingui l'stock menor que 0, i no puc permetre quantitats negatives
		}

		return $quantity;
	}

	// borro producte del carro
	function delete_item() {

		$id = $this->get_product_variation_id();
		$id .= $this->get_cart_product_date();

		unset( $_SESSION['product']['cart'][ $id ] );
		if ( ! $_SESSION['product']['cart'] ) {
			$this->empty_cart();

			return; // empty_cart tambe fa el list_records_afax
		}
		$this->list_records_ajax();
	}

	// borro tot el carro
	function empty_cart() {
		unset( $_SESSION['product']['cart'] );
		unset( $_SESSION['product']['cart_vars'] );
		$this->list_records_ajax();
	}

	// Si hi ha calendari, poso la data al key del cart
	private function get_cart_product_date() {
		$product_date = R::escape( 'product_date' );

		if ( $product_date ) {
			return '|' . $product_date;
		}
		else {
			return '';
		}
	}

	public function set_collect() {

		$collect = R::get( 'collect' );

		$_SESSION['product']['cart_vars']['collect'] = $collect;

		$cart_vars = $this->get_cart_vars();
		$totals    = $cart_vars['totals'];

		$vars = [
			'success'       => true,
			'currency_name' => $this->config['currency_name'],
			'rate_price'    => $totals['rate_price'],
			'total_price'   => $totals['total_price'],
			'all_price'     => $totals['all_price'],
		];

		json_end( $vars );
	}


	// obtinc id de variació, si es igual a zero agafo les predeterminades 
	// Al formulari la variacio es zero quan no n'hi ha
	// Al llistat sempre es zero ja que no hi poso variacions, per tant hai d'agafar la que hi hagi per defecte
	// 
	function get_product_variation_id() {
		$id = R::escape( 'product_variation_ids' );

		if ( $id != 0 ) {
			// trec últim guió
			$id = ( substr( $id, - 1 ) == '_' ) ? substr( $id, 0, - 1 ) : $id;
		}
		else {
			$query = "SELECT variation_category_id, product_variation_id, product__product_to_variation.variation_default
						FROM product__product_to_variation, product__variation 
						WHERE product__product_to_variation.variation_id = product__variation.variation_id 
						AND product_id = '" . $_GET['product_id'] . "' 
						AND product__variation.bin = 0 
						ORDER BY variation_category_id ASC, product__variation.ordre ASC, product__variation.variation_id ASC";
			// agafar el primer predeterminat de la categoria (al gestionar variacions em puc trobar que un producte tingui més d'un predeterminat)
			// ordenat per ordre i id
			// si no hi ha cap predeterminat agafa el primer
			Debug::add( 'Consulta variacions afegir carro desde llistat', $query );
			$results    = Db::get_rows( $query );
			$variations = array();
			$id         = '';
			// comprovo que hi hagi algun default en la categoria
			foreach ( $results as $rs ) {
				$vs = &$variations[ $rs['variation_category_id'] ];
				// el primer trobat a la categoria
				if ( ! isset( $vs['category_has_default'] ) ) {
					$vs['category_has_default'] = false;
				}
				$vs['category_has_default'] = ( $vs['category_has_default'] || $rs['variation_default'] ) ? true : false;
			}
			// faig els llistats de variacions pel formulari
			foreach ( $results as $rs ) {
				extract( $rs );
				$vs = &$variations[ $variation_category_id ];

				// el primer trobat a la categoria
				if ( ! isset( $vs['variation_category_id'] ) ) {
					$vs['variation_category_id'] = true;
					// si la categoria no te cap default poso el primer que es aquest
					if ( ! $vs['category_has_default'] ) $id .= $product_variation_id . '_';
				}
				// agafo nomès el primer default ( en cas que n'hi hagin més d'un )
				if ( ! isset( $vs['found_variation_default'] ) && $variation_default ) {
					$vs['found_variation_default'] = true;
					$id                            .= $product_variation_id . '_';
				}
			}
			// trec últim guió
			$id = ( substr( $id, - 1 ) == '_' ) ? substr( $id, 0, - 1 ) : $id;
			if ( ! $results ) $id = '0';
		}

		return $_GET['product_id'] . '_' . $id;
	}

	function is_wholesaler() {
		return ( isset( $_SESSION['product']['customer_id'] ) && isset( $_SESSION['product']['is_wholesaler'] ) && $_SESSION['product']['is_wholesaler'] );
	}

	function get_equivalencia( $product_base, $tax_id ) {

		if ( isset( $_SESSION['product']['customer_id'] ) ) {

			$query            = "SELECT has_equivalencia FROM product__customer WHERE customer_id = " . $_SESSION['product']['customer_id'];
			$has_equivalencia = Db::get_first( $query );

			if ( $has_equivalencia ) {
				$query        = "SELECT equivalencia FROM product__tax WHERE tax_id = " . $tax_id;
				$equivalencia = Db::get_first( $query );
			}

			if ( $has_equivalencia && $equivalencia ) return $product_base * ( $equivalencia / 100 );
			else return 0;
		}
		else {
			return 0;
		}

	}

	function get_wholesaler_rate() {

		if ( isset( $_SESSION['product']['customer_id'] ) ) {
			$query = "SELECT wholesaler_rate FROM product__customer WHERE customer_id = " . $_SESSION['product']['customer_id'];
			$ret   = Db::get_first( $query );

			return $ret == 1 ? '' : $ret;
		}
		else {
			return false;
		}
	}

	/**
	 * ProductCustomer::is_logged()
	 * Saber si està loguejat
	 * @return
	 */
	function is_logged() {
		// miro si hi ha les 2 sessions per més seguretat de que esta logged
		return ( isset( $_SESSION['product']['customer_id'] ) && isset( $_SESSION['product']['mail'] ) );
	}
}