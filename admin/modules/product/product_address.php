<?
/**
 * ProductAddress
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2010
 * @version $Id$
 * @access public
 */
class ProductAddress extends Module{
	var $customer_id, $delivery_id, $invoice_id, $same_address='checked';
	var $is_seller; // de payment->send_mails
	function __construct(){
		parent::__construct();
	}
	function show_form()
	{
		$GLOBALS['gl_content'] = '<script language="JavaScript" src="/admin/modules/product/jscripts/functions.js?v= '
           . $GLOBALS['gl_version'] . '"></script>' . $this->get_form();
	}
	function get_form()
	{
		$show = new ShowForm($this);
		if ($this->action=='show_form_new') {
			$show->set_field('customer_id','default_value',$_GET['customer_id']);
		}
		$show->has_bin = false;
		return $show->show_form();
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function get_records()
	{
		$listing = new ListRecords($this);
		$listing->has_bin = false;

		$listing->condition = 'customer_id = ' . $this->customer_id . ' AND invoice=0';
		$listing->set_options(0,0,0,0,0,0,0,0,0,0);
		$listing->add_button('edit_address', '','','before','address_edit');

		// una altra forma enlloc de fer un list_records_walk
		$listing->list_records(false);
		foreach ($listing->results as $key=>$val){
			$rs = &$listing->results[$key];
			if ($rs['address_id']==$this->delivery_id){
				$rs['checked'] = 'checked';
				$this->same_address= '';
			}
			else{
				$rs['checked'] = '';
			}
		}

		$listing->set_var('same_address',$this->same_address);
		$listing->always_parse_template = true; // si no hi ha resultats mostra el template igualment en lloc del missatge de "No hi ha registres...."
		return $listing->parse_template();
	}
	// retorna el valor de les dues adresses per a order i customer quan es get_record
	function get_addresses()
	{
		$show = new ShowForm($this);
		$show->has_bin = false;
		$show->action = 'get_record';
		$show->set_file('product/address_record.tpl');

		// adreça invoice
		$show->id = $this->invoice_id;
		$show->set_var('dni',''); // això fa que no es mostrin dni, tel, mobile, etc...
		$show->get_values();

		// trec variables nom per agafar de taula customer, ja que adreça invoice agafa el nom de customer
		unset($show->rs['a_name']);
		unset($show->rs['a_surname']);
		$fields = 'mail, dni, telephone, telephone_mobile, name as a_name, surname as a_surname, company';
		$show->rs +=
			Db::get_row('SELECT '.$fields.'
				FROM product__customer
				WHERE customer_id = ' . $show->rs['customer_id']);

		$address_invoice = $show->show_form();

		// adreça delivery
		$show->id = $this->delivery_id;
		$show->set_var('company',''); // això fa que no es mostrin dni, tel, mobile, etc...
		$show->set_var('dni',''); // això fa que no es mostrin dni, tel, mobile, etc...
		$show->reset();
		// si no hi ha addreça d'entrega poso la mateixa de facturació
		$show->get_values();
		if ($show->has_results ) {
			$is_same_address = false;
			$address_delivery = $show->show_form();
		}
		else{
			$is_same_address = true;
			$address_delivery = $address_invoice;
		}

		return array(
			'address_delivery' => $address_delivery,
			'address_invoice' => $address_invoice,
			'is_same_address' => $is_same_address,
		);
	}

	function write_record()
	{
		$writerec = new SaveRows($this);// quan guardo desde customer ja he establert el id
		$this->customer_id = $this->customer_id?$this->customer_id:current($_POST['customer_id']);
		$this->set_field('customer_id','override_save_value',$this->customer_id);

		$country = $writerec->get_value( 'country_id' );
		if ($country != 'ES') $writerec->set_value( 'provincia_id', 0 );

		$writerec->save();

		// Si no guardo desde customer ( ho faig amb els dos formularis en un "merge")
		// Quan guardo desde editar dades de form customer, la nova addreça la poso d'entrega
		// i retorno inner_html
		if ($this->parent != 'ProductCustomer') {
			Debug::add('Write address', $writerec);
			Debug::add('$this->customer_id', $this->customer_id);

			// si es nova poso com a adreça d'entrega, ja que se suposa que si el client entra
			// una nova adreça és per que vol que sigui l'adreça d'entrega
			if ($this->action == 'add_record'){
				$this->delivery_id = $GLOBALS['gl_insert_id'];
				Db::execute("UPDATE product__customer SET delivery_id = " . $this->delivery_id . " WHERE customer_id =" . $this->customer_id);
			}
			else{
				$query = "SELECT delivery_id FROM product__customer WHERE customer_id = " . $this->customer_id;
				$this->delivery_id = Db::get_first($query);
			}

			set_inner_html('addresses_list', $this->get_records());
		}

	}
}
?>