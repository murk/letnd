<?php
/* 14-9-18
Others privats a product per us intern ( per exemple un client que demana un checkbox per que surti un ellaç a talles
*/


if ( is_table( 'product__product' ) && ! is_field( 'product__product', 'others_private1' ) ) {

	$update_sql[] = "
					ALTER TABLE `product__product`
						ADD COLUMN `others_private1` TEXT NOT NULL AFTER `others20`,
						ADD COLUMN `others_private2` TEXT NOT NULL AFTER `others_private1`;";

	for ( $i = 1; $i <= 3; $i ++ ) {
		$update_sql[] = "
			INSERT INTO `product__configadmin` (`name`)
			VALUES ('others_private" . $i . "');";
		$update_sql[] = "
			INSERT INTO `product__configadmin` (`name`,value)
			VALUES ('others_private" . $i . "_type','checkbox');";
	}


	global $gl_languages;

	foreach ( $gl_languages['names'] as $lang => $val ) {
		for ( $i = 1; $i <= 3; $i ++ ) {
			$update_sql[] = "
			INSERT INTO `product__configadmin_language` (`name`, `language`)
			VALUES ('others_private" . $i . "','" . $lang . "');";
		}
	}


} else {
	if ( is_table( 'product__product' ) ) {
		trigger_error( 'Ja existeixen el camp others_private1 a others_private3' );
	}
}