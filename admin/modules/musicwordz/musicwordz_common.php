<?
/**
 * MusicwordzCommon
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2016
 * @version $Id$
 * @access public
 *
 * TODO-i comprovar això
 * L'artista quan crea una sala o festival, només ho pot veure ell, o l'administrador
 * Les sales o festivals no surten al desplegable fins que no s'aproven ( només al de l'usuari que l'ha creat )
 * L'artista quan crea algo queda com estat "en preparació", ha de canviar a "per revisar" per que l'administrador ho revisi i deixi per bo
 * Que s'ha de fer un cop aprovat, impedir que l'usuari ho pugui editar?
 * Usuaris nomes es llisten actuacions, cançons i sales pròpies
 * Artista no es pot modificar en els llistats
 * L'artista no pot esborrar registres
 *
 *
 *
 *
 * 
 * TODO-i wide posar responsive * 
 * TODO-i Posar el padding en els camp que no s'editen
 * TODO-i Posar bé els estils quan editem llistats
 *
 * TODO-i Arxiu d'ajuda
 */
class MusicwordzCommon {

	/**
	 * @return bool
	 */
	static public function is_admin()
	{
		return $_SESSION['group_id'] <= 2;
	}

	/**
	 * @return bool
	 */
	static public function is_artist()
	{
		return $_SESSION['group_id'] > 2;
	}

	/**
	 * @return string
	 */
	static public function get_user_id()
	{
		return $_SESSION['user_id'];
	}

	/**
	 * @param $listing
	 */
	static public function set_listing( &$listing ) {

		if ( MusicwordzCommon::is_artist() ) {
			$listing->condition = 'user_id = ' . MusicwordzCommon::get_user_id();
			$listing->unset_field( 'user_id' );
			$listing->call( 'records_walk_artist', '', true );
			$listing->set_options( 1, 0, 0, 0, 0, 1, 1, 1, 1, 1 );
		}


		if ( MusicwordzCommon::is_admin() ) {
			$listing->add_filter( 'user_id' );
			$listing->add_swap_edit();
		}
		$listing->add_filter( 'status' );

	}

	/**
	 * @param $show
	 */
	static public function set_show( &$show ) {

		// Comprovo si l'artista pot editar
		if ( MusicwordzCommon::is_artist() && $show->action != 'show_form_new' ) {
			$id = $show->id;
			$rs = Db::get_row( "SELECT status, user_id FROM musicwordz__" . $show->tool_section . " WHERE " . $show->id_field . " =" . $id );

			if ($rs['status'] != 'prepare'){
				Main::redirect( '/admin/' );
			}
			if ($rs['user_id'] != MusicwordzCommon::get_user_id()){
				Main::redirect( '/admin/' );
			}
		}


		if ( MusicwordzCommon::is_admin() ) {
			$show->set_field( 'user_id', 'form_admin', 'input' );
		}

		if ( MusicwordzCommon::is_artist() ) {
			$show->unset_field( 'status' );
			$show->unset_field( 'user_id' );
			$show->set_options( 1, 0 );
		}

		$show->set_field('entered','default_value',now(true));

		if ( $show->action == 'show_form_new' ) {
			$show->unset_field( 'modified' );
		}

	}

	/**
	 * @param $writerec
	 */
	static public function set_writerec( &$writerec ) {

		// Obligo sempre a guardar l'user_id del loguejat
		if ( MusicwordzCommon::is_artist() ) {
			$writerec->set_field( 'user_id', 'override_save_value', MusicwordzCommon::get_user_id() );
			$writerec->set_field( 'status', 'override_save_value', "'prepare'" );
		}

		$writerec->set_field('modified','override_save_value','now()');

	}

	static public function records_walk_artist( &$listing ) {
		$status = $listing->rs['status'];
		$listing->buttons['edit_button']['show'] = $status == 'prepare';		
	}
	static public function user_user_on_load( $action, &$module ) {

		$module->related_tables = array(
			'0' =>
				array(
					'table'  => 'musicwordz__place',
					'action' => 'notify',
				),
			'1' =>
				array(
					'table'  => 'musicwordz__song',
					'action' => 'notify',
				),
			'2' =>
				array(
					'table'  => 'musicwordz__performance',
					'action' => 'notify',
				),
		);

		$module->add_field( 'artistic_name',
			array(
				'type'           => 'text',
				'form_admin'     => 'input',
				'form_public'    => 'text',
				'list_admin'     => 'text',
				'list_public'    => 'text',
				'text_size'      => '60',
				'text_maxlength' => '100',
			)
		);

		$module->set_field_position( 'artistic_name', 5 );

		include_once( DOCUMENT_ROOT . 'admin/modules/musicwordz/languages/' . LANGUAGE . '.php' );
		$module->caption ['c_artistic_name'] = $gl_caption ['c_artistic_name'];
	}

	static public function add_dynamic_missatges(&$caption) {

		if ( MusicwordzCommon::is_artist() ) {
			return;
		}

		$query = "SELECT count(*) FROM musicwordz__performance WHERE status <> 'approved';";
		$num_performances = Db::get_first( $query );

		$query = "SELECT count(*) FROM musicwordz__song WHERE status <> 'approved';";
		$num_songs = Db::get_first( $query );

		$query = "SELECT count(*) FROM musicwordz__place WHERE status <> 'approved';";
		$num_places = Db::get_first( $query );

		if ( ! $num_performances && ! $num_places && ! $num_songs ) {
			return;
		}

		Main::add_dynamic_missatge( $caption['c_title_missatges_to_approve'] );

		if ($num_performances)
			Main::add_dynamic_missatge( '',  $caption['c_title_missatges_performances_to_approve'] . ' ' . $num_performances, '/admin/?menu_id=2403', $caption['c_title_missatges_to_approve_link']);

		if ($num_songs)
		Main::add_dynamic_missatge( '',  $caption['c_title_missatges_songs_to_approve'] . ' ' . $num_songs, '/admin/?menu_id=2303', $caption['c_title_missatges_to_approve_link']);

		if ($num_places)
		Main::add_dynamic_missatge( '',  $caption['c_title_missatges_places_to_approve'] . ' ' . $num_places, '/admin/?menu_id=2503', $caption['c_title_missatges_to_approve_link']);
	}
}
?>