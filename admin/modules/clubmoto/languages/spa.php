<?php
// menus eina
if (!defined('CLUBMOTO_MENU_PAGINA')){
define('CLUBMOTO_MENU_PAGINA','Edición de revistas'); 
define('CLUBMOTO_MENU_PAGINA_NEW','Entrar nueva revista');
define('CLUBMOTO_MENU_PAGINA_LIST','Listar revistas'); 
define('CLUBMOTO_MENU_CATEGORY','Categorías de apartados'); 
define('CLUBMOTO_MENU_CATEGORY_NEW','Nueva categoría'); 
define('CLUBMOTO_MENU_CATEGORY_LIST','Listar categorías');
define('CLUBMOTO_MENU_PAGINA_BIN','Papelera de reciclage'); 
}

/*--------------- nomès castellà i català, no traduit a la resta -------------------*/


$gl_caption_pagina['c_ordre'] = 'Orden';
$gl_caption_pagina['c_template'] = 'Plantilla';
$gl_caption_pagina['c_paginatpl_id'] = 'Plantilla';
$gl_caption_pagina['c_link'] = 'Link';
$gl_caption_pagina['c_body_supertitle'] = 'Supertítol';
$gl_caption_pagina['c_body_supertitle2'] = 'Supertítol';
$gl_caption_pagina['c_body_supertitle3'] = 'Supertítol';
$gl_caption_pagina['c_title'] = 'Título';
$gl_caption_pagina['c_title2'] = 'Título';
$gl_caption_pagina['c_title3'] = 'Título';
$gl_caption_pagina['c_body_subtitle'] = 'Subtítulo';
$gl_caption_pagina['c_body_subtitle2'] = 'Subtítulo';
$gl_caption_pagina['c_body_subtitle3'] = 'Subtítulo';
$gl_caption_pagina['c_pagina'] = 'Nombre de la página en el menú';
$gl_caption_pagina_list['c_pagina'] = 'Nombre';
$gl_caption_pagina['c_body_content'] = 'Contenido';
$gl_caption_pagina['c_body_content2'] = 'Contenido';
$gl_caption_pagina['c_body_content3'] = 'Contenido';
$gl_caption_pagina['c_prepare'] = 'En preparación';
$gl_caption_pagina['c_review'] = 'Para revisar';
$gl_caption_pagina['c_public'] = 'Público';
$gl_caption_pagina['c_archived'] = 'Archivado';
$gl_caption_pagina['c_status'] = 'Estado';
$gl_caption_pagina['c_pagina_id'] = 'Id';
$gl_caption_pagina['c_parent_id'] = 'Pertenece a';
$gl_caption_pagina['c_level'] = '';
$gl_caption_pagina['c_blocked'] = 'Block.';

$gl_caption_pagina['c_add_revista_button'] = 'Insertar nueva revista';
$gl_caption_pagina['c_add_apartat_button'] = 'Insertar nuevo reportaje';
$gl_caption_pagina['c_add_pagina_button'] = 'Insertar nueva página';
$gl_caption_pagina['c_edit_revista_button'] = 'Editar revista';
$gl_caption_pagina['c_edit_apartat_button'] = 'Editar reportaje';
$gl_caption_pagina['c_edit_pagina_button'] = 'Editar página';
$gl_caption_pagina['c_view_apartat_button'] = 'Listar reportajes';
$gl_caption_pagina['c_view_pagina_button'] = 'Llistar páginas';
$gl_caption_pagina['c_url1_name']='Nom URL';
$gl_caption_pagina['c_url1']='URL';
$gl_caption_pagina['c_url2_name']='Nom URL(2)';
$gl_caption_pagina['c_url2']='URL(2)';
$gl_caption_pagina['c_url3_name']='Nom URL(3)';
$gl_caption_pagina['c_url3']='URL(3)';
$gl_caption_pagina['c_videoframe']='Vídeo (youtube, metacafe...)';
$gl_caption_pagina['c_file']='Arxius';
$gl_caption_pagina['c_category_id']='Categoría';

$gl_caption['c_last_listing']='Listado anterior';


$gl_caption_category['c_ordre']='Ordre';
$gl_caption_category['c_category']='Categoría';
$gl_caption_category['c_is_clubone'] = 'Menú clubone';




/*banners*/
$gl_caption_banner['c_banner_id']='';
$gl_caption_banner['c_ordre']='Ordre';
$gl_caption_banner['c_url1']='Enllaç';
$gl_caption_banner['c_url1_name']='Text a l\'enllaç';
$gl_caption_banner['c_url1_target']='Destí';
$gl_caption_banner['c_url1_target__blank']='Una nova finestra';
$gl_caption_banner['c_url1_target__self']='La mateixa finestra';
$gl_caption_banner['c_file']='Imatge banner <br>(960x92 - 248x122)';
$gl_caption_banner['c_banner']='Banner';
?>