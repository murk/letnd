<?php
$days_off = isset($vars['days_off'])?$vars['days_off']:1;
$show_default_days = isset($vars['show_default_days'])?$vars['show_default_days']:1;
$show_captions = isset($vars['show_captions'])?true:false;

// fitre amb les opcions de cercar disponibilitat

$add_jscript = isset($vars['add_jscript'])?$vars['add_jscript']:true;

$date_in_caption = $date_out_caption = "";


include(DOCUMENT_ROOT . 'admin/modules/booking/booking_book_config.php');


if (isset($GLOBALS['gl_module']) && $GLOBALS['gl_module']->tool == 'book') {
	$caption = $GLOBALS['gl_module']->caption;
	$fields =  $GLOBALS['gl_module']->fields;
}
else{	
	$module = Module::load('booking','book');
	$caption = $module->caption;
	$fields = $module->fields;
}

if ( $show_captions ) {
	$date_in_caption = " placeholder = \"" . $caption['c_date_in'] . "\"";
	$date_out_caption = " placeholder = \"" . $caption['c_date_out'] . "\"";
	$fields['adult']['select_caption'] = $caption['c_adult'];
	$fields['child']['select_caption'] = $caption['c_child'];
	$fields['baby']['select_caption'] = $caption['c_baby'];
}

$adult = R::escape('adult');
$child = R::escape('child');
$baby = R::escape('baby');
$date_in = R::escape('date_in',$show_default_days?now():false);
$date_out = R::escape('date_out',$show_default_days?now(false,$days_off):false);
	
$adult = get_select('adult', '', $adult, $fields['adult'], $caption);
$child = get_select('child', '', $child, $fields['child'], $caption);
$baby = get_select('baby', '', $baby, $fields['baby'], $caption);

$date_in = '<input autocomplete="off" type="text" value="'.$date_in.'"' . $date_in_caption .' size="8" name="date_in" id="date_in">';
$date_out = '<input autocomplete="off" type="text" value="'.$date_out.'"' . $date_out_caption .' size="8" name="date_out" id="date_out">';

$jscript =
	"
		$('#date_in').datepicker({
			changeMonth: true,
			changeYear: true,
			yearRange: '+0:+5',
			onClose: function( selectedDate ) {
				var date = $(this).datepicker('getDate');
				if (date) {
					  date.setDate(date.getDate() + ".$days_off.");
				}
				$( '#date_out' ).datepicker( 'option', 'minDate', date );
			}
		});
		$('#date_out').datepicker({
			changeMonth: true,
			changeYear: true,
			yearRange: '+0:+5',
			onClose: function( selectedDate ) {
				var date = $(this).datepicker('getDate');
				if (date) {
					  date.setDate(date.getDate() - ".$days_off.");
				}
				$( '#date_in' ).datepicker( 'option', 'maxDate', date );
			}
		});
	";

if ($add_jscript){
	
	$GLOBALS['gl_page']->javascript .= $jscript;
	
}

$vars += array (
	'adult' => $adult,
	'child' => $child,
	'baby' => $baby,
	'date_in' => $date_in,
	'date_out' => $date_out,
	'c_adult' => $caption['c_adult'],
	'c_child' => $caption['c_child'],
	'c_baby' => $caption['c_baby'],
	'c_date_in' => $caption['c_date_in'],
	'c_date_out' => $caption['c_date_out'],
	'temp_jscript' =>  $jscript
	);


?>