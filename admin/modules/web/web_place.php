<?

/**
 * WebPlace
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2016
 * @version $Id$
 * @access public
 */
class WebPlace extends Module {

	function __construct() {
		parent::__construct();
	}

	function list_records() {
		$GLOBALS['gl_content'] = $this->get_records();
	}

	function get_records() {
		$listing           = new ListRecords( $this );
		$listing->has_bin  = false;
		$listing->order_by = 'place ASC';

		return $listing->list_records();
	}

	function show_form() {
		$GLOBALS['gl_content'] = $this->get_form();
	}

	function get_form() {
		$show = new ShowForm( $this );

		return $show->show_form();
	}

	function save_rows() {
		$save_rows = new SaveRows( $this );
		$save_rows->save();
	}

	function write_record() {
		$writerec = new SaveRows( $this );
		$writerec->save();
	}

	function manage_images() {
		$image_manager = new ImageManager( $this );
		$image_manager->execute();
	}

	public function get_select_long_values() {
		{

			//	$field = R::get( 'field' );
			//	$id = R::get( 'id' );
			$limit = 100;

			$q = R::escape( 'query' );

			$ret                = array();
			$ret['suggestions'] = array();

			$r = &$ret['suggestions'];


			// 1 - Municipis, que comenci
			$query = "SELECT municipi_id, municipi, comarca_id, comarca
			FROM inmo__municipi
			INNER JOIN inmo__comarca USING (comarca_id)
			WHERE municipi LIKE '" . $q . "%'
			ORDER BY municipi
			LIMIT " . $limit;

			$results = Db::get_rows( $query );


			$rest = $limit - count( $results );

			// 2 - Municipis, qualsevol posició i a comarca
			if ( $rest > 0 ) {
				$query = "SELECT municipi_id, municipi, comarca_id, comarca
				FROM inmo__municipi
				INNER JOIN inmo__comarca USING (comarca_id)
				WHERE
						(municipi LIKE '%" . $q . "%'
						AND municipi NOT LIKE '" . $q . "%')
			        OR
						(comarca LIKE '%" . $q . "%')
				ORDER BY municipi
				LIMIT " . $rest;

				$results2 = Db::get_rows( $query );

				$results = array_merge( $results, $results2 );
			}

			// Presento resultats
			foreach ( $results as $rs ) {
				$value = $rs['municipi'] ?
					$rs['municipi'] . " - " . $rs['comarca'] :
					$rs['comarca'];
				$r[]   = array(
					'value' => $value,
					'data'  => $rs['municipi_id']
				);
			}

			$rest = $limit - count( $results );
			$ret['is_complete'] = $rest>0;

			die ( json_encode( $ret ) );


		}

	}
}

?>