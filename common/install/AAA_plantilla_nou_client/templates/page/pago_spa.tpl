<?if (!defined('PRODUCT_DISCOUNT_MAX_RESULTS')) Module::get_config_values('product');?>
<article>
  <h2>Al comprar en  <?=COMPANY_NAME?>, usted puede comprar mediante:
  </h2>
  
  <p><strong>Pago con Paypal:</strong><br />
  Pague de la forma más rápida y extendida mediante tarjeta de crédito o transferencia bancaria con su cuenta Paypal</p>
  
  <p><strong>Pago tarjeta de crédito o de débito:</strong><br />
  Puede utilizar su tarjeta de crédito o de débito, Visa o Mastercard</p>
  
  <p><strong>Pago contra reembolso: </strong></strong><br />
  Usted paga en el momento en que correos le entrega su pedido. 
  </p>
  
  <p><strong>Mediante transferencia bancaria:</strong><br />
  Usted realiza una transferencia bancaria a: <?=PRODUCT_BANC_NAME?>, número de cuenta: <?=PRODUCT_ACCOUNT_NUMBER?>, con el importe total, y una vez confirmada su transferencia <?=COMPANY_NAME?> le enviará su pedido.
  </p>
</article>