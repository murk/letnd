<link href="/admin/themes/inmotools/styles/product_calendar.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="/admin/jscripts/validacio.js?v=<?=$version?>"></script>
<script type="text/javascript" src="/admin/modules/product/jscripts/calendar.js?v=<?=$version?>"></script>
<script type="text/javascript" src="/admin/jscripts/assign.js?v=<?=$version?>"></script>
<script type="text/javascript" src="/admin/modules/product/jscripts/list_pick.js?v=<?=$version?>"></script>

<div class="form">
	
	<div id="form_buttons_holder">		
	<table id ="form_buttons">
		<tr>
			<td valign="bottom">
				<div class="menu">

					<? if ( $data_link ): ?>
						<a href="<?= $data_link ?>"><?= $c_edit_data ?></a>
					<? endif // $data_link ?>

					<? if ( $image_link ): ?>
						<a href="<?= $image_link ?>"><?= $c_edit_images ?></a>
					<? endif // iamge_link ?>

					<? foreach ( $form_tabs as $r ): extract( $r ) ?>
						<? if ( $tab_action ): ?>
							<a href="?action=<?= $tab_action ?>&<?= $id_field ?>=<?= $id ?>&menu_id=<?= $menu_id ?>"><?= $tab_caption ?></a>
						<? else: // ?>
							<div><?= $tab_caption ?></div>
						<? endif // $tab_action ?>
					<? endforeach //$form_tabs?>


					
					</div></td>
			<td class="buttons_top"><div class="right">
										<? if($show_previous_link): ?><a class="boto2 btn_show_previous_link"
										                          href="<?= $show_previous_link ?>"></a><? endif //show_previous_link?>
										<? if($show_next_link): ?><a class="boto2 btn_show_next_link"
										                          href="<?= $show_next_link ?>"></a><? endif //show_next_link?>
			<? if ($back_button):?><a class="btn_form_back" href="<?=$back_button?>"></a><? endif //back_button?>
			</div></td>
		</tr>
	</table></div>

	<table width="100%" cellpadding="0" cellspacing="0">
		<tr>
			<td nowrap class="formCaptionTitle padding-right-3"><strong>
					<?=$c_form_calendar_title?></strong>&nbsp;&nbsp;<?=$family?>
			</td>
			<td width="100%" class="formCaptionTitle">
							<table border="0" cellpadding="0" cellspacing="0">
								<tbody><tr>
									<td class="padding-right-1"><strong><?= $c_has_calendar ?></strong></td>
									<td><?= $has_calendar ?></td>
									<td class="padding-right-1 padding-left-3"><strong><?= $c_is_calendar_public ?></strong></td>
									<td><?= $is_calendar_public ?></td>
								</tr>
							</tbody></table>
						</td>
		</tr>
	</table>
	<table id="whole-calendar" width="100%" cellpadding="4" cellspacing="0" class="formstbl<?= $calendar_hidden ?>">
		<tr>
			<td align="left" valign="top" class="forms"><table width="99%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td>

							<div class="calendar-holder" id="calendar-<?= $family_id ?>"><table id="calendar-table-<?= $family_id ?>" border="0" cellpadding="0" cellspacing="0">
									<tr id="calendar-tr-<?= $family_id ?>">
										<?=$calendar?>
									</tr>
								</table>
							</div>
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td><a class="calendar-prev" href="javascript:calendar.show_month('prev');"></a></td>
									<td align="right"><a class="calendar-next" href="javascript:calendar.show_month('next');"></a></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>

			</td>
		</tr>
	</table>
	<? /* 
 	<div class="sep_dark" style="margin-top:30px;"></div>
 
 	<div id="list-records">
 		<?=$calendar_list?>
 	</div>
 	 */ ?>
	
</div>