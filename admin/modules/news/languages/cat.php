<?php
// menus eina
if (!defined('NEWS_MENU_NEW')){
	define('NEWS_MENU_NEW', 'Notícies');
	define('NEWS_MENU_NEW_NEW', 'Entrar nova notícia');
	define('NEWS_MENU_NEW_LIST', 'Llistar i editar notícies');
	define('NEWS_MENU_NEW_BIN', 'Paperera de reciclatge');
	define('NEWS_MENU_CATEGORY', 'Categories');
	define('NEWS_MENU_CATEGORY_NEW', 'Entrar nova categoria');
	define('NEWS_MENU_CATEGORY_LIST', 'Llistar categories');
}

$gl_messages_new['no_category']='Per poder insertar notícies hi ha d\'haver almenys una categoria';

// captions seccio
$gl_caption_new['c_prepare'] = 'En preparació';
$gl_caption_new['c_review'] = 'Per revisar';
$gl_caption_new['c_public'] = 'Públic';
$gl_caption_new['c_archived'] = 'Arxivat';
$gl_caption_new['c_new'] = 'Notícia';
$gl_caption_new['c_title'] = 'Títol';
$gl_caption_new['c_subtitle'] = 'Subtítol';
$gl_caption_new['c_content'] = 'Contingut';
$gl_caption_new['c_entered'] = 'Data publicació';
$gl_caption_new['c_expired'] = 'Data caducitat';
$gl_caption_new['c_modified'] = 'Data modificació';
$gl_caption_new['c_status'] = 'Estat';
$gl_caption_new['c_category_id'] = 'Categoria';
$gl_caption_new['c_filter_category_id'] = 'Totes les categories';
$gl_caption_new['c_filter_status'] = 'Tots els estats';
$gl_caption_new['c_filter_in_home'] = 'Totes, home o no';
$gl_caption_new['c_filter_destacat'] = 'Totes, destacades o no';
$gl_caption_new['c_ordre'] = 'Ordre';

$gl_caption_category['c_category'] = 'Categoria';
$gl_caption_category['c_new_button'] = 'Veure notícies';
$gl_caption_category['c_ordre'] = 'Ordre';
$gl_caption_image['c_name'] = 'Nom';
$gl_caption_new['c_url1_name']='Nom URL(1)';
$gl_caption_new['c_url1']='URL(1)';
$gl_caption_new['c_url2_name']='Nom URL(2)';
$gl_caption_new['c_url2']='URL(2)';
$gl_caption_new['c_url3_name']='Nom URL(3)';
$gl_caption_new['c_url3']='URL(3)';
$gl_caption_new['c_videoframe']='Video (youtube, metacafe...)';
$gl_caption_new['c_file']='Arxius';
$gl_caption_new['c_in_home']='Surt a la Home';
$gl_caption_new['c_in_home_0']='No surt a la Home';
$gl_caption_new['c_in_home_1']='Surt a la Home';
$gl_caption_new['c_content_list']='Descripció curta al llistat';
$gl_caption_new['c_destacat']='Notícia destacada';
$gl_caption_new['c_destacat_0']='No destacada';
$gl_caption_new['c_destacat_1']='Destacada';

$gl_caption['c_new_file_name'] = $GLOBALS['gl_caption']['c_file_name'];
$gl_caption['c_new_old_file_name'] = $GLOBALS['gl_caption']['c_old_file_name'];
$gl_caption_new['c_form_level1_title_page_title'] = $GLOBALS['gl_caption']['c_seo'];

$gl_caption_new['c_selected']='Notícies seleccionades';

$gl_caption['c_listtpl_id'] = 'Plantilla llistat';
$gl_caption['c_formtpl_id'] = 'Plantilla';

$gl_caption['c_next_new'] = "Notícia següent";
$gl_caption['c_previous_new'] = "Notícia anterior";
?>