<?php
if (!isset($_POST['login']) || empty($_COOKIE)) {
	header('Location: /');
	Header( "HTTP/1.1 301 Moved Permanently" );
}

include ('common/classes/main.php');
Main::set_vars(); // defineixo les 4 variables necessaries abans de fer res
Debug::start(); // començo el debug si s'escau

include ('common/main.php'); // carrego arxius que tenen variables globals, a la llarga ha d'anar a Main::load_files();
Main::load_files(); // carrego includes i classes

unset($_SESSION['login']); // trec la sessió, si no no comproba login i pass. Per si envies el form per entrar si ja estas logejat 

User::check_is_logged();// comprovar login o si es vol deixar la sessió
$is_top = R::text_id('is_top',false,'_POST');

// redirigeixo a admin
if ($gl_is_admin_logged){
	$language =  R::text_id('language',false,'_POST');
	Debug::p($language, 'text');
	if ($language) setcookie('language', $language);
	
	if ($is_top) {
		print_javascript("
			top.window.location.href='/admin/';
		");
	}
	else{
		header('Location: /admin/');
	}
}
else { // que els buscadors creguis que s'ha mogut permanentment
	
	Header( "HTTP/1.1 301 Moved Permanently" ); 
	if ($is_top) {
		print_javascript("			
			top.$('#loginform_message').html('". addslashes(USER_LOGIN_INVALID) ."');
			top.$('#loginform_message').show();
			if (typeof top.hidePopWin == 'function') top.hidePopWin(false);
		");
	}
	else{	
		$_SESSION['message'] = USER_LOGIN_INVALID;
		header('Location: /'); 
	}
	
}

Debug::p_all(); // mostro les variables del debug si toca
?>