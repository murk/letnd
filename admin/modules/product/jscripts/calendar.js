function Calendar(id) {
	this.start = true;
	this.date_busy = '';
	this.painted = {};
	this.b_original_class = false;
	this.month_width = 0;
	this.month_loaded = {};
	this.month_loaded_count = 0;
	this.first_month_viewing = '';
	this.paint_class = 'selected';
	this.unpaint_class = 'booked';
	this.caller = '';
	this.attr = '';
	this.family_id = id;
	this.calendar_holder = $('#calendar-' + id);
	this.calendar_table = $('#calendar-table-' + id);
	this.calendar_tr = $('#calendar-tr-' + id);

	var mes_holder = this.calendar_holder.find('.mes-holder');

	var obj = this;
	mes_holder.each(function (index) {
		var mes_id = $(this).data('mes-id');
		if (!obj.first_month_viewing) obj.first_month_viewing = mes_id;
		obj.month_loaded[mes_id] = true;
		obj.month_loaded_count++;
	});

	this.fix_size = function () {

		// fixo la mida per quan es carreguin mes mesos
		mes_holder.removeAttr('style');
		this.calendar_holder.removeAttr('style');

		this.month_width = mes_holder.width();
		this.calendar_holder.width(this.calendar_holder.width());
		mes_holder.width(this.month_width);

	};
	// fixo la mida per quan es carreguin mes mesos nomes de carregar
	this.fix_size();

	this.save_day = function (dia, mes, any, id_element, process) {

		var formated_date_busy = dia + '/' + mes + '/' + any;

		$.ajax({
			url: '/admin/'
			+ '?template=window'
			+ '&tool=product'
			+ '&tool_section=familybusy'
			+ '&action=write_record_from_calendar'
			+ '&process=' + process
			+ '&date_busy=' + formated_date_busy
			+ '&family_id=' + this.family_id,
			dataType: 'json'
		})
			.done(function (data) {

				if (data.error) {
					alert(data.error);
				}
				else {
					// alert(data.message);
					if (process == 'occupy') {
						id_element.removeClass('free selected');
						id_element.addClass('occupied');
					}
					else {
						id_element.removeClass('occupied selected');
						id_element.addClass('free');
					}
				}

			});

	};
	this.do_action = function (dia, mes, any) {

		var id = '.' + dia + '-' + mes + '-' + any;
		id_element = this.calendar_holder.find(id);

		// si està ocupat, desocupo
		if (
			id_element.hasClass('occupied')
		) {
			this.caller = 'occupied';
			this.set_day(dia, mes, any, id_element);
		}
		// si està seleccionat, deselecciono
		else if (
			id_element.hasClass('selected')
		) {
			this.caller = 'selected';
			this.set_day(dia, mes, any, id_element)
		}
		else {
			this.caller = 'free';
			this.set_day(dia, mes, any, id_element)
		}
		dp('Caller', this.caller);

	};
	this.set_day = function (dia, mes, any, id_element) {


		if (this.caller == 'occupied') {
			id_element.removeClass('occupied');
			id_element.addClass('selected');
			this.save_day(dia, mes, any, id_element, 'free');
		}
		else if (this.caller == 'selected') {
			id_element.removeClass('selected');
			id_element.addClass('free');
		}
		else if (this.caller == 'free') {
			id_element.removeClass('free');
			id_element.addClass('selected');
			this.save_day(dia, mes, any, id_element, 'occupy');
		}
		dp('Id', id);

	};
	this.show_month = function (direction) {

		var viewing = this.first_month_viewing.split('-');

		var viewing_month = viewing[0];
		var viewing_year = viewing[1];
		var month = 0;
		var year = 0;

		if (direction == 'next') {
			month = Number(viewing_month) + 1;
			year = viewing_year;
			if (month > 12) {
				month = 1;
				year++;
			}
			this.first_month_viewing = month + '-' + year;

			month = Number(viewing_month) + 3;
			year = viewing_year;
			if (month > 12) {
				month = month - 12;
				year++;
			}
		}
		if (direction == 'prev') {
			month = viewing_month - 1;
			year = viewing_year;
			if (month < 1) {
				month = 12;
				year--;
			}
			this.first_month_viewing = month + '-' + year;
		}
		//dp(month + '-' + year);
		if (typeof(this.month_loaded[month + '-' + year]) == 'undefined') {

			var link = '/admin/?tool=product&tool_section=familybusy&action=get_month&month=' + month + '&year=' + year + '&family_id=' + this.family_id + '&language=' + gl_language;


			var obj = this;
			$.ajax({
				url: link
			})
				.done(function (data) {

					if (direction == 'next') {
						obj.calendar_tr.append(data);
					}
					else {
						obj.calendar_tr.prepend(data);
						obj.calendar_table.css('margin-left', -obj.month_width);
					}
					obj.calendar_holder.find('.mes-holder').width(obj.month_width);

					obj.month_loaded[month + '-' + year] = true;
					obj.month_loaded_count++;

					obj.calendar_table.width(obj.month_width * obj.month_loaded_count);

					obj.animate_month(direction);

				});

		}
		else {
			this.animate_month(direction);
		}

	};
	this.animate_month = function (direction) {

		var dir = direction == 'next' ? -1 : 1
		var obj = {};
		obj['marginLeft'] = dir * this.month_width + parseInt(this.calendar_table.css('margin-left'));

		this.calendar_table.stop().animate(obj, 500);
	};
};

$(function () {
	$('#has_calendar,#is_calendar_public').change(function () {

		if (typeof calendar == 'undefined') return;
		var has_calendar, is_calendar_public;

		if ($('#has_calendar').is(":checked")) {
			has_calendar = 1;
		}
		else {
			has_calendar = 0;
		}
		if ($('#is_calendar_public').is(":checked")) {
			is_calendar_public = 1;
		}
		else {
			is_calendar_public = 0;
		}

		$.ajax({
			url: '/admin/?action=save_has_calendar&tool=product&tool_section=family',
			data: 'has_calendar=' + has_calendar +
			'&is_calendar_public=' + is_calendar_public +
			'&family_id=' + calendar.family_id,
			dataType: 'json',
			type: 'POST'
		})
			.done(function (data) {
				dp(data.saved);

				if (!data.saved) return;

				if (has_calendar) {
					$('#whole-calendar').show();
					calendar.fix_size();
				}
				else {
					$('#whole-calendar').hide();
				}
			});


	});
});
