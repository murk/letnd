<?

/**
 * ContactContact
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2010
 * @version $Id$
 * @access public
 */
class ContactContact extends Module {
	var $matter, $template, $is_block = false;

	function __construct() {
		parent::__construct();
	}

	function on_load() {
		// això ja no es fa així
		if ( isset( $_GET['free'] ) ) {
			$this->set_config( 'contact/contact_contact_config2.php' );
		}

		// Ara es fa així: es pot afagir els camps que es vulgui a qualsevol client
		// l'arxiu /contact_contact_config.php va a l'arrel del client
		// els captions van a /languages del client

		if ( is_file( CLIENT_PATH . '/contact_contact_config.php' ) ) {
			include( CLIENT_PATH . '/contact_contact_config.php' );
			if ( $fields ) $this->fields = array_merge( $this->fields, $fields );
		}
	}

	function show_form() {
		$GLOBALS['gl_content'] = $this->get_form();
	}

	// obtinc html del list_records per al block
	function get_form_block() {

		$this->is_block = true;

		return $this->do_action( 'get_form_new' );
	}

	function list_records() {
		// a public mai llistem tots els contactes
		$GLOBALS['gl_content'] = $this->do_action( 'get_form_new' );
	}

	function get_form() {
		// El faig entrar per show_form pq m'hagafi automàticament la validacion
		if ( isset( $_GET['contact_subject'] ) ) {
			$this->set_field( 'matter', 'default_value', R::escape( 'contact_subject' ) );
		}
		// millor matter que te el mateix nom que el camp
		if ( isset( $_GET['matter'] ) ) {
			$this->set_field( 'matter', 'default_value', R::escape( 'matter' ) );
		}


		$show          = new ShowForm( $this );
		$show->has_bin = false;
		if ( isset( $_GET['free'] ) ) {
			$show->template = 'contact/contact_form2';
		}
		if ( $this->parent == 'GetContact' ) {
			if ( $this->matter ) $show->set_field( 'matter', 'default_value', $this->matter );
			if ( $this->template ) $show->template = $this->template;
		}
		$show->set_var( 'is_block', $this->is_block );
		$this->set_var( 'is_get_contact', $this->parent == 'GetContact' );
		$html = $show->show_form();

		return $html;

	}

	function save_rows() {
		$save_rows = new SaveRows( $this );
		$save_rows->save();
	}

	function send_test() {
		$this->write_record( true );
	}

	/**
	 *
	 * #form_sent -> capa amb missatge de enviat
	 * #form_not_sent -> capa amb missatge de no enviat
	 * .form_area -> formulari i textes relacionats que s'amaguen quan s'envia el missatge
	 * per TESTEJAR /cat/contact/contact/send_test/
	 *
	 *  Per canviar el missatge que s'envia en l'autoresposta al client
	 * 	<input type="hidden" name="auto_response_text_var" value="c_missatge_autoresposta_email">
	 *
	 *
	 * @param bool $test
	 */
	function write_record( $test = false ) {

		$this->get_test_post( $test );
		if( isset ($_POST['mail'][0])) $_POST['mail'][0] = trim ($_POST['mail'][0]);

		$writerec = new SaveRows( $this );

		if ( ! $writerec->has_requireds( array( 'mail', 'phone' ), true ) ) {
			return;
		};

		// Recaptcha
		if (!$test) check_google_recaptcha();

		$company = ( isset( $_POST['company'][0] ) ) ? $_POST['company'][0] : '';

		// Envio el correu electrònic
		// si hi ha el select de to_address, poso l'adreça seleccionada en el 'to'
		if ( isset( $_POST['to_address'][0] ) && $_POST['to_address'][0] ) $this->config['from_address'] = $_POST['to_address'][0];

		$send_mail_copy = $this->config['send_mail_copy'] && ! empty( $_POST['mail'][0] );

		$mail = $GLOBALS['gl_current_mailer'] = get_mailer( $this->config );
		$mail->IsHTML( true );


		// puto arsyss
		if ( ! empty( $this->config['use_reply'] ) ) {

			$mail->AddReplyTo( $_POST['mail'][0], $company ? $company : $_POST['name'][0] );
			$mail->From = $this->config['from_address'];

		}
		// normal
		else {
			$mail->From     = $_POST['mail'][0]; //mail que l'envia
			$mail->FromName = $company ? $company : $_POST['name'][0]; //
			if ( ! empty( $_POST['surnames'][0] ) ) $mail->FromName .= ' ' . $_POST['surnames'][0];
		}

		$mail->AddAddress( $this->config['from_address'] );

		$caption_admin = $this->get_vars_admin();

		$mail->Subject = $caption_admin['c_contact_mail_subject'];
		if ( ! empty( $_POST['matter'][0] ) ) $mail->Subject .= " - " . $_POST['matter'][0];

		// mail en format html
		$body        = $this->get_body( true );
		$body_client = '';

		if ( $send_mail_copy ) $body_client = $this->get_body( false );


		if ( $this->use_template() ) {
			$mail->Body = $this->get_body_with_template( $mail, $body, $test );
			if ( $body_client ) $body_client = $this->get_body_with_template( $mail, $body_client, $test );
		}
		else {
			$mail->Body = "<html>$body</html>";
			if ( $body_client ) $body_client = "<html>$body_client</html>";
		}


		// mail en format text
		$alt_body      = $this->get_alt_body();
		$mail->AltBody = $alt_body;


		Debug::add( 'Mailer de contact', $mail );

		$exito      = $test ? true : send_mail( $mail, false );
		$exito_copy = true;

		if ( $send_mail_copy ) {
			$mail->Body = $body_client;
			$exito_copy = $test ? true : $this->send_copy( $mail, $_POST['mail'][0], $test );
		}

		if ( $test ) {
			html_end( $mail->Body );
		}
		elseif ( $exito && $exito_copy ) {
			print_javascript( "top.window.$('#form_sent').show();top.window.$('.form_sent').show();top.window.$('#form_not_sent').hide();top.window.$('#form_area').hide();top.window.$('.form_area').hide();" );

			$analytics = $this->config['google_analytics_conversion'];

			$analytics = str_replace( '"es"', '"' . LANGUAGE_CODE . '"', $analytics );
			$analytics = str_replace( '"en"', '"' . LANGUAGE_CODE . '"', $analytics );
			echo $analytics;

			$writerec->save(); //pq surti el missatge a la part públic has de posar el tpl que surtin missatges


			$custom_message = R::escape( 'custom_message', false, '_POST' );
			echo $custom_message;
			$message = $custom_message !== false ? $custom_message : $GLOBALS['gl_message'];


			$GLOBALS['gl_page']->show_message( $message, false, false );
		}
		else {
			global $gl_message, $gl_messages, $gl_saved;
			print_javascript( "top.window.$('#form_sent').hide();top.window.$('.form_sent').hide();top.window.$('#form_not_sent').show();top.window.$('#form_area').show();top.window.$('.form_area').show();" );
			$gl_message = $gl_messages['form_not_sent'];
			$gl_saved   = false;
		}
		html_end();

	}

	function get_body( $is_owner ) {
		$body = '';

		foreach ( $this->fields as $key => $val ) {
			if (
				$val['type'] != 'hidden' &&
				isset( $_POST[ $key ] ) &&
				$key != 'accept_info' &&
				$key != 'matter'
			) {
				$v = $_POST[ $key ][0];
				if ( $val['type'] == 'textarea' ) $v = nl2br( $v );
				$body .=
					'<tr><th align="left" valign="top">' .
					$this->caption[ 'c_' . $key ] .
					'</th><td align="left" valign="top">' .
					$v .
					'</td></tr>';
			}
		}
		if ( $is_owner ) {
		$body .=
			'<tr><th align="left" valign="top">' .
			$this->caption['c_contact_language'] .
			'</th><td align="left" valign="top">' .
			$GLOBALS['gl_languages']['names'][ LANGUAGE ] .
			'</td></tr>';
		}
		$body = '<table cellpadding="2" class="form">' . $body . "</table>";

		$auto_response_var = R::post('auto_response_text_var') ? : 'c_missatge_autoresposta_email';


		if ( !$is_owner && !empty ( $this->caption[$auto_response_var] ) ) {
			$body = "<div class='autoreply'>{$this->caption[$auto_response_var]}</div>$body";
		}

		if ( $is_owner ) {
			$caption_admin = $this->get_vars_admin();
			$body          = "<div class='autoreply'>{$caption_admin['c_contact_mail_title']}</div>$body";
		}

		return $body;
	}

	function get_alt_body() {
		$alt_body = '';
		foreach ( $this->fields as $key => $val ) {
			if ( $val['type'] != 'hidden' && isset( $_POST[ 'old_' . $key ] ) ) {
				$v        = $_POST[ $key ][0];
				$alt_body .=
					$this->caption[ 'c_' . $key ] .
					': ' .
					$v . "\n";
			}
		}
		$alt_body .=
			$this->caption['c_contact_language'] .
			': ' .
			$GLOBALS['gl_languages']['names'][ LANGUAGE ];

		return $alt_body;
	}


	/**
	 * Per enviar qualsevol formulari, sense guardar a BBDD ( poden haver els camps que un vulgui )
	 * Es necessiten almenys aquest camps:
	 * - mail
	 * - name ( opcional per fromName )
	 * - matter
	 * Els camps que comencen per caption_ son lo que es mostra com a nom del camp
	 * Els camps que comencen per title_ serveixen per mostrar un títol que agrupi varis camps
	 * Els camps que comencen per button s'ignoren
	 * //
	 * #form_sent -> capa amb missatge de enviat
	 * #form_not_sent -> capa amb missatge de no enviat
	 * .form_area -> formulari i textes relacionats que s'amaguen quan s'envia el missatge
	 * per TESTEJAR action=send_mail_test
	 *
	 * <input type="hidden" name="matter" value="Assumpte">
	 * <input type="hidden" name="action" value="send_mail">
	 * <input type="hidden" name="send_mail_copy" value="1">
	 *
	 * @param bool $test
	 */
	function send_mail( $test = false ) {

		$this->get_test_post( $test, true );

		$GLOBALS['gl_page']->is_frame = true;

		$send_mail_copy = R::post( 'send_mail_copy' ) ?: $this->config['send_mail_copy'];

		$send_mail_copy = $send_mail_copy && ! empty( $_POST['mail'] );

		$mail = $GLOBALS['gl_current_mailer'] = get_mailer( $this->config );
		$mail->IsHTML( true );


		$mail->From     = $_POST['mail']; //mail que l'envia
		$mail->FromName = isset( $_POST['name'] ) ? $_POST['name'] : $_POST['mail']; //
		if ( isset( $_POST['surname'] ) ) $mail->FromName .= ' ' . $_POST['surname'];
		$mail->AddAddress( $this->config['from_address'] );


		$caption_admin = $this->get_vars_admin();

		$mail->Subject = $caption_admin['c_contact_mail_subject'];
		if ( ! empty( $_POST['matter'] ) ) $mail->Subject .= " - " . $_POST['matter'];

		$body = $this->get_body_send_mail( $test );

		if ( $this->use_template() ) {
			$mail->Body = $this->get_body_with_template( $mail, $body, $test );
		}
		else {
			$mail->Body = "<html>$body</html>";
		}

		$exito      = $test ? true : ( send_mail( $mail, false ) ); // només envio si no test
		$exito_copy = true;

		if ( $send_mail_copy ) {
			$exito_copy = $test ? true : $this->send_copy( $mail, $_POST['mail'], $test );
		}

		if ( $test ) {
			html_end( $mail->Body );
		}

		global $gl_message, $gl_messages;
		if ( $exito && $exito_copy ) {
			print_javascript( "top.window.$('#form_sent').show();top.window.$('#form_not_sent').hide();top.window.$('#form_area').hide();top.window.$('.form_area').hide();" );

			$custom_message = R::escape( 'custom_message' );

			$gl_message = $custom_message ? $custom_message : $gl_messages['form_sent'];
		}
		else {
			print_javascript( "top.window.$('#form_sent').hide();top.window.$('#form_not_sent').show();top.window.$('#form_area').show();top.window.$('.form_area').show();" );
			$gl_message = $gl_messages['form_not_sent'];
		}
	}

	public function get_body_send_mail() {
		$body = '';

		foreach ( $_POST as $key => $value ) {
			if ( substr( $key, 0, 8 ) != 'caption_' &&
			     substr( $key, 0, 6 ) != 'button' &&
			     $key != 'accepto' &&
			     $key != 'accept_info' &&
			     $key != 'matter' &&
			     $key != 'action' &&
			     $key != 'send_mail_copy' ) {


				$has_title = substr( $key, 0, 6 ) == 'title_';

				$caption = isset( $_POST[ 'caption_' . $key ] ) ? $_POST[ 'caption_' . $key ] : '';

				$attribs = ( $has_title ) ? 'valign="bottom" height="60" style="font-size:1.3em;"' : 'valign="top"';

				// Si no té caption, el value va a la primera cela i colspan 2
				if ( ! $caption ) {
					$value   = nl2br( $value );
					$colspan = 'colspan = "2"';
				}
				// Si té caption, el value va a la segona cela
				elseif ( $value ) {
					$value   = "<td>" . nl2br( $value ) . "</td>";
					$colspan = "";
				}
				else {
					$value   = "";
					$colspan = 'colspan = "2"';
				}

				if ( $caption ) {
					$body .= "<tr><td $attribs $colspan><strong>" . $caption . "</strong></td>$value</tr>";
				}
				else {
					$body .= "<tr><td $attribs $colspan>$value</tr>";
				}
			}
		}

		return "<table class=\"form\">$body</table>";
	}


	public function get_body_with_template( $mail, $body, $is_test ) {

		if ( is_file( CLIENT_PATH . 'images/logo_newsletter.jpg' ) ) {
			$mail->AddEmbeddedImage( CLIENT_PATH . 'images/logo_newsletter.jpg', 'logo', 'logo.jpg' );
		}
		elseif ( is_file( CLIENT_PATH . 'images/logo_newsletter.gif' ) ) {
			$mail->AddEmbeddedImage( CLIENT_PATH . 'images/logo_newsletter.gif', 'logo', 'logo.gif' );
		}

		$body = $this->get_template_content( $body, $is_test );

		$instyler = new \Pelago\Emogrifier( $body );
		$body     = $instyler->emogrify();

		return $body;

	}

	/**
	 * Obté el contingut del mail
	 *
	 * @param $body
	 * @param $is_test
	 *
	 * @return mixed
	 */
	public function get_template_content( $body, $is_test ) {

		include_once( DOCUMENT_ROOT . 'admin/modules/newsletter/newsletter_message_functions.php' );

		$page = clone $GLOBALS['gl_page'];

		$page->template = 'mail.tpl';

		$page->set_vars( array(
			'preview'        => $is_test ? true : false,
			'url'            => HOST_URL,
			'default_mail'   => DEFAULT_MAIL,
			'page_slogan'    => PAGE_SLOGAN,
			'remove_text'    => '',
			'short_host_url' => substr( HOST_URL, 7, - 1 )
		) );

		$page->content = $body;
		$body          = $page->get();

		return $body;
	}

	/**
	 * Utilitzo la variable del config use_mail_template per que sigui compatible amb els clients vells que no ho tenien configurat
	 * @return bool
	 */
	public function use_template() {
		return $this->config['use_mail_template'] && is_file( CLIENT_PATH . 'templates/mail.tpl' );
	}

	function send_copy( $mail, $post_mail, $test ) {
		$mail->clearAddresses();
		$mail->clearReplyTos();
		$mail->AddAddress( $post_mail );
		$mail->setFrom( $this->config['from_address'], $this->config['from_name'] );

		return $test ? true : send_mail( $mail, false );
	}


	function get_vars_admin() {
		// Carrego idioma intranet
		$admin_language = current( $GLOBALS['gl_languages']['admin'] );
		$gl_caption     = [];
		include( DOCUMENT_ROOT . "/public/modules/contact/languages/$admin_language.php" );

		if (is_file(CLIENT_PATH . 'languages/' . $admin_language . '.php')) include (CLIENT_PATH . 'languages/' . $admin_language . '.php');

		return $gl_caption;
	}

	// per fer probes sense enviar mail
	function send_mail_test() {
		$this->send_mail( true );
	}

	public function get_test_post( $test, $is_send_mail = false ) {

		if ( $test && ! $_POST ) {
			if ( $is_send_mail ) {

				$_POST = [
					'name'    => 'Jonhy',
					'mail'    => 'mail@gmail.com',
					'phone'   => '777 78 78 77',
					'matter'  => 'Prova',
					'comment' => "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus aliquam corporis cum doloremque ducimus excepturi fugiat illo itaque labore magnam nemo, perferendis porro quaerat qui quos repellendus, ut vel vero? \n\n Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus aliquam corporis cum doloremque ducimus excepturi fugiat illo itaque labore magnam nemo, perferendis porro quaerat qui quos repellendus, ut vel vero? "
				];
			}
			else {
				$_POST = [
					'name'     =>
						[
							0 => 'Jonhy',
						],
					'surnames' =>
						[
							0 => 'Be Good',
						],
					'mail'     =>
						[
							0 => 'mail@gmail.com',
						],
					'phone'    =>
						[
							0 => '777 78 78 77',
						],
					'matter'   =>
						[
							0 => 'Prova',
						],
					'comment'  =>
						[
							0 => "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus aliquam corporis cum doloremque ducimus excepturi fugiat illo itaque labore magnam nemo, perferendis porro quaerat qui quos repellendus, ut vel vero? \n\n Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus aliquam corporis cum doloremque ducimus excepturi fugiat illo itaque labore magnam nemo, perferendis porro quaerat qui quos repellendus, ut vel vero? ",
						],
				];
			}
		}
	}
}