<?php
/* 12-4-17
Afegir camp data per un producte comprat

*/
if ( is_table( 'product__orderitem' ) && ! is_field( 'product__orderitem', 'product_date' ) ) {

	$update_sql[] = "ALTER TABLE `product__orderitem`
	ADD COLUMN `product_date` DATE NOT NULL AFTER `quantity`;";

} else {
	if ( is_table( 'product__orderitem' ) ) {
		trigger_error( 'Ja existeixen el camp product_date' );
	}
}

?>