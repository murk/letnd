$(document).ready(function(){
  //init_values(); no va no se per que
});
$(window).bind("load", function(){
  init_values(); // aquest si va
}); 

//window.onload = init_values;
var arrRelated_options = new Array(); // holds values for each object
var arrRelated_options2 = new Array(); // holds values for each object
var arrRelated_options3 = new Array(); // holds values for each object

var selProvincies,selComarques,selMunicipis,selZones; // Name and Values Select object

var dosIniciat = false;

function init_values()
{
	selProvincies = eval('document.theForm[\'provincia_id['+document.theForm.customer_id_javascript.value+']\']');
	selComarques =  eval('document.theForm[\'comarca_id['+document.theForm.customer_id_javascript.value+']\']');
	selMunicipis =  eval('document.theForm[\'municipi_id['+document.theForm.customer_id_javascript.value+']\']');	
	
	//if (eval('document.theForm[\'zone_id['+document.theForm.customer_id_javascript.value+']\']')) selZones = eval('document.theForm[\'zone_id['+document.theForm.customer_id_javascript.value+']\']');
	selProvincies.onchange = change_values;
	selComarques.onchange = change_values2;
	//if (selZones) selMunicipis.onchange = change_values3;

	for (var i=0; i != selProvincies.length; i++)
	{
		arrRelated_options[selProvincies.options[i].value] = new RelatedOptions(selProvincies.options[i].value, selComarques);
	}
 	for (var i=0; i != selComarques.length; i++)
	{
    arrRelated_options2[selComarques.options[i].value] = new RelatedOptions(selComarques.options[i].value, selMunicipis);
	}
	
   /*if (selZones){
	   for (var i=0; i != selMunicipis.length; i++)
		{ 
		arrRelated_options3[selMunicipis.options[i].value] = new RelatedOptions(selMunicipis.options[i].value, selZones);
		}
	}*/
	dosIniciat = true;
	change_values()
	//change_values2()		
	//if (selZones) change_values3()
	//load_all()
}

function change_values()
{
	var i = selProvincies.options[selProvincies.selectedIndex].value;
	arrRelated_options[i].show();
  	if (dosIniciat)change_values2();
  
}

function change_values2()
{
	var i = selComarques.options[selComarques.selectedIndex].value;
	arrRelated_options2[i].show();
	//if (dosIniciat)change_values3();
}
function change_values3()
{	
	if (!selZones) return;
	var i = selMunicipis.options[selMunicipis.selectedIndex].value;
	arrRelated_options3[i].show();
}

// -------------------
// class related_options
// -------------------

function RelatedOptions(_id, selValues)
{
	_id= "#"+_id+"#";
	this.values =  new Array();
	this.texts =   new Array();
    this.selectedIndex = 0;
	this.show = show;

	// store values and text in each array
	var _text, count = 0;
	for(var i=0; i!=selValues.length; i++)
	{
		if (selValues.options[i].text.indexOf(_id)==0)
		{
			this.values[count] = selValues.options[i].value;
			_text =  selValues.options[i].text;
			this.texts[count] = _text.substring(_id.length,_text.length);
			  // ja no es un més ja que he tret el valor per defecte
			  if (selValues.selectedIndex==i) this.selectedIndex = count;
			count++;
		}
		else if ((i==0) && (selValues.options[i].value==0))
		{
			this.values[count] = selValues.options[i].value;
			this.texts[count] = selValues.options[i].text;
			count++;
		}
	}

	// reset and repopulate the Select
	function show()
	{
		selValues.length = this.values.length;

  	//selValues.options[0].value = 0;
  	//selValues.options[0].text = '';

		for(var i=0; i!=this.values.length; i++)
		{
			selValues.options[i].value = this.values[i];
			selValues.options[i].text = this.texts[i];
		}
    selValues.selectedIndex=this.selectedIndex;
	}
}
