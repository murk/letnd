<?
/**
 * TecnomixNew
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2010
 * @version $Id$
 * @access public
 */
class TecnomixNew extends Module{
	var $form_template = '';
	var $list_template = '';
	function __construct(){
		parent::__construct();
		$this->parent = 'TecnomixNew';
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function on_load(){
		
		$this->set_friendly_params(array(
			'category_id'=>R::id('category_id')			
			));
		parent::on_load();
	}
	/* funció que es crida per fer una acció del modul automatica o desde un altre modul ( list_records, show_form ) etc...*/
	function do_action($action = ''){
		switch ($this->parent){		
			case 'TecnomixNew':
				$this->form_template = 'tecnomix/new_form';
				$this->list_template = 'tecnomix/new_list';
			break;		
			case 'TecnomixProjecte':
				$this->form_template = 'tecnomix/projecte_form';	
				$this->list_template = 'tecnomix/projecte_list';
			break;	
			case 'TecnomixEquip':	
				$this->form_template = 'tecnomix/equip_form';
				$this->list_template = 'tecnomix/equip_list';
			break;
			case 'TecnomixInstalacio':	
				$this->form_template = 'tecnomix/equip_form';
				$this->list_template = 'tecnomix/equip_list';
			break;
		}
		$this->set_var('parent',$this->parent);
		return parent::do_action($action);
	}
	function get_records()
	{	    
		$this->set_news_fields();
		$listing = new ListRecords($this);
		$listing->template = $this->list_template;
		$listing->condition = "parent_module = '" . $this->parent . "'";
		$listing->condition .= " AND status = 'public'";
		
		$category_id = R::id('category_id');
		if ($category_id) $listing->condition .= " AND category_id = '" . $category_id . "'";
		
		$listing->call('records_walk','url1,url1_name,url2,url2_name,content,content_list');
		$listing->order_by = 'ordre ASC, entered DESC';
		return $listing->list_records();
	}
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{
	    $this->set_news_fields();
		$show = new ShowForm($this);
		$show->template = $this->form_template;
		
		// si no hi ha id, agafo per defecte la primera noticia de la categoria, i si no hi ha categoria, de la primera categoria
		if (!$show->id){
			// si no hi ha categoria agafo la primera ordenat per nom i id			
			$category_id = R::id('category_id');
			if (!$category_id){
				$query = "SELECT tecnomix__category.category_id as category_id
					FROM tecnomix__category_language, tecnomix__category
					WHERE tecnomix__category_language.category_id = tecnomix__category.category_id
					AND language = '" . LANGUAGE . "'
					ORDER BY category, category_id
					LIMIT 0,1";
				$category_id = Db::get_first($query);
			}
			$query = "SELECT tecnomix__new.new_id as new_id
				FROM tecnomix__new_language, tecnomix__new
				WHERE tecnomix__new_language.new_id = tecnomix__new.new_id
				AND status = 'public'
				AND bin <> 1
				AND category_id = '" . $category_id . "'
				AND language = '" . LANGUAGE . "'
				ORDER BY entered DESC, new_id
				LIMIT 0,1";
			$show->id = Db::get_first($query);
		}
		$show->condition = "status = 'public'";
		$show->call('records_walk','url1,url1_name,url2,url2_name');
		$show->get_values();		
		if ($show->has_results)
		{
			$GLOBALS['gl_page']->page_title = 
				$show->rs['page_title']?$show->rs['page_title']:$show->rs['new'];
			
			$GLOBALS['gl_page']->page_description = 
				$show->rs['page_description']?$show->rs['page_description']:add_dots_meta_description($show->rs['content']);
			if ($show->rs['page_keywords']!='')
				$GLOBALS['gl_page']->page_keywords = $show->rs['page_keywords'];
			
			return $show->show_form();
		}
		else
		{
			$this->do_action('list_records');
		}
	}
	function records_walk($url1,$url1_name,$url2,$url2_name,$content=false,$content_list=false){		
		// Això ho poso aquí així no cal posar-ho al tpl, simplement posar: <a href="<=$url1>" target="_blank"><=$url1_name></a>
		$ret['url1_name'] = $url1_name?$url1_name:$url1;
		$ret['url2_name'] = $url2_name?$url2_name:$url2;
		
		// si es llistat passa lavariable content
		if ($content!==false){
			$ret['content']=$content_list?$content_list:add_dots('content', $content,$this->config['add_dots_length']);
		}
			
		//$ret['url1']=str_replace('http://','',$url1);	
		//$ret['url1']=str_replace('http://','',$url1);
		return $ret;
	}
	function set_news_fields(){	
		global $gl_config;
		if (!$gl_config['show_entered_time'])
		{
			$this->set_field('entered', 'type', 'date');
		}
	}
}
?>