<?

/**
 * InsideTour
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 1/8/2018
 * @version $Id$
 * @access public
 */
class InsideTour extends Module {
	var $category_id, $limit, $order_by, $condition;

	function __construct() {
		parent::__construct();
	}

	function on_load() {

		$this->category_id = R::id( 'category_id' );
		if ( $this->category_id ) {

			$this->set_friendly_params( array(
				'category_id' => $this->category_id
			) );
		}
		else {
			$this->category_id = R::id( 'category' );

			if ( $this->category_id ) {
				$this->set_friendly_params( array(
					'category' => $this->category_id
				) );
			}
		}

		parent::on_load();
	}

	function list_records() {
		$GLOBALS['gl_content'] = $this->get_records();
	}

	function get_records() {


		$listing = new ListRecords( $this );
		$listing->call( 'records_walk', 'content', true );

		$listing->condition .= "status = 'public'";

		$listing->order_by = 'ordre ASC, tour ASC, entered DESC';


		if ( $this->parent == 'Block' ) {
			$listing->name     = 'Block';
			$listing->paginate = false;
			$listing->condition .= " $this->condition";
			$listing->order_by = $this->order_by;
			$listing->limit    = $this->limit;
			$listing->set_records();

			return $listing->loop;
		}

		$category_id = $this->category_id;

		$listing->set_var( 'listing_category_id', $category_id );


		if ( $category_id ) {
			$listing->condition .= " AND category_id IN ('$category_id')";
		}

		return $listing->list_records();
	}

	function records_walk( &$listing, $content = false ) {

		$ret            = array();
		$ret['content'] = add_dots( 'content', $content );

		return $ret;
	}

	function show_form() {
		$GLOBALS['gl_content'] = $this->get_form();
	}

	function get_form() {

		$show            = new ShowForm( $this );
		$show->condition .= "status = 'public'";

		$show->get_values();


		// pel block de productes visitats
		if ( isset( $_SESSION['visited_tours'][ $show->id ] ) ) unset( $_SESSION['visited_tours'][ $show->id ] );
		$_SESSION['visited_tours'][ $show->id ] = $show->id;

		if ( $show->has_results ) {

			Page::set_page_title( $show->rs['page_title'], $show->rs['tour'] );
			Page::set_page_description( $show->rs['page_description'], $show->rs['content'] );
			Page::set_page_keywords( $show->rs['page_keywords'] );

			$html = $show->show_form();

			Page::set_page_og( $show->rs );

			return $html;

		}
		else {
			Main::redirect( '/' );
		}

	}
}