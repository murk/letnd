<?
/**
 * AmctaicBonaplata
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2010
 * @version $Id$
 * @access public
 */
class AmctaicBonaplata extends Module{
	var $page_id_select = '', $bonaplata_id = 0, $bonaplata_page_id = 0, $bonaplata_parent_id = 0;
	function __construct(){
		parent::__construct();
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function list_historic()
	{
		$this->action = 'list_records';
		$GLOBALS['gl_content'] = $this->get_records(true);
	}
	function get_records($historial=false)
	{
	    $listing = new ListRecords($this);
		$listing->paginate = false;
		$menu = ($this->process=='adult')?2:3;
				
		if ($historial){
			// miro quins page_id contenen adults o joves
			$query = "SELECT all__page.page_id as page_id
							FROM all__page_language, all__page
							WHERE all__page_language.page_id = all__page.page_id
							AND status = 'public'
							AND bin <> 1
							AND menu = '" . $menu . "'
							AND language = '" . LANGUAGE . "'
							ORDER BY ordre ASC, page_id";
			$listing->condition = "status = 'archived' AND page_id IN (" . $query . ")"; // "Històric de premis"
			$listing->set_loop('loop_lateral',array());
		}
		else{		
			$this->_get_menu();
			$listing->condition = "status = 'public' AND page_id = '" . $this->bonaplata_parent_id . "'"; 
			
		}
		$listing->order_by = "entered desc";
		$listing->set_field('page_id','type','none');
		$listing->call('list_records_walk','page_id');
	    return $listing->list_records();
	}
	function list_records_walk($page_id){
		$query = "SELECT all__page.page_id as page_id, page, parent_id
						FROM all__page_language, all__page
						WHERE all__page_language.page_id = all__page.page_id
						AND status = 'public'
						AND all__page.page_id = " . $page_id . "
						AND language = '" . LANGUAGE . "'";		
		$rs = Db::get_row($query);
		$query = "SELECT page
						FROM all__page_language, all__page
						WHERE all__page_language.page_id = all__page.page_id
						AND status = 'public'
						AND all__page.page_id = " . $rs['parent_id'] . "
						AND language = '" . LANGUAGE . "'";		
		$parent = Db::get_first($query);
		$ret['page_id'] = str_replace(' ','&nbsp;',$parent) . ' - ' . $rs['page'];
		return $ret;
	}
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{		
		redirect('/?tool=amctaic&tool_section=bonaplata&action=list_records&process=adult&language='.LANGUAGE.'&page_id=23');
		/*$this->_get_menu();
		$show = new ShowForm($this);
		$show->id = $this->bonaplata_id;//$id; 
		debug::add('Bonaplata_id',$this->bonaplata_id);
		
		//$show->always_parse_template=true;
		$show->get_values();		
		if ($show->has_results)
		{
			return $show->show_form();			
		}
		else
		{
			$this->do_action('list_records');
		}*/
	}

	function _get_menu()
	{
		
		$page = &$GLOBALS['gl_page'];
		// faig servir el top id per marcar una pàgina, ja que pot ser que estiguem en un submenu i s'hagi de marcar la pagina d'on penja
		
		$this->bonaplata_page_id = R::id('bonaplata_page_id','23');
		
		$parent_id = Db::get_first("SELECT parent_id FROM all__page WHERE page_id = " . $this->bonaplata_page_id);
		$level = Db::get_first("SELECT level FROM all__page WHERE page_id = " . $this->bonaplata_page_id);

		// valors per defecte
		$start_level = '1';
		$last_level = '4';
	    $menu = ($this->process=='adult')?2:3;

		$parents = $selected_ids = array();
		$parents[$level] = $parent_id; // el parent d'aquest nivell on som
		$current_parent = $parent_id;
		$selected_ids[$level] = $this->bonaplata_page_id; // el parent d'aquest nivell on som
		$i = $level-1;

		if ($level<$start_level){
			// si estem en un nivell superior del que volem mostrar, haig de cambiar el parent_id
			// En aquest cas nomès poden ser els fills dierctes d'aquest menu, per tant puc tindre un menu principal que mostra nivells 1-2 i un secuhdari que mostra 3-4, però el secundari no pot mostrar 4 nomès, ja que no sé quin es
			if (($start_level-$level) >1) {
				// si no hi ha resultats no presento el block
				$show_block = false;
				return array();
				}
			$current_parent = $this->bonaplata_page_id;	
		}
		else{
			// busco el parent del level d'inici i els tots els sleccionats fins el nivell inicial
			//  (si es un submenu puc començar del segon nivell o tercer, el primer nivell pot ser en un menu a dalt)
			while ($i>=$start_level){
				$parents[$i] = $current_parent = Db::get_first(
				"SELECT parent_id FROM all__page WHERE page_id = " . $parents[$i+1]);
				$selected_ids[$i] = $parents[$i+1]; // es el page_id actual, si a la consulta de dalt ho he posat ... page_id = " . $parents[$i+1]);
				$i--;
			}
		}
		Debug::add('nivell',$level);	
		Debug::add('parents',$parents);	
		Debug::add('selected_ids',$selected_ids);	
		
		$loop = $this->_get_children($current_parent, $start_level, $selected_ids, $menu, $last_level);	
		$this->_clean_categories($loop);
		
		$this->set_loop('loop_lateral',$loop);
	}
	function _get_children($parent_id, $level, $selected_ids, $menu, $last_level){
		$loop = array();
		if ($last_level == $level) return $loop;
		$query = "SELECT all__page.page_id as page_id, link, page, parent_id, has_content
						FROM all__page_language, all__page
						WHERE all__page_language.page_id = all__page.page_id
						AND status = 'public'
						AND parent_id = " . $parent_id . "
						AND bin <> 1
						AND menu = '" . $menu . "'
						AND language = '" . LANGUAGE . "'
						ORDER BY ordre ASC, page_id";
		Debug::add($query,'Query all_page_recursive');	
		$results = Db::get_rows($query);

		if ($results)
		{
			$conta = 0;
			$loop_count = count($results);
			foreach($results as $rs)
			{					
				$selected = (isset($selected_ids[$level]) && ($selected_ids[$level] == $rs["page_id"]))?1:0;
				
				$l['link'] = '/?tool=amctaic&tool_section=bonaplata&action=list_records&process=' . $this->process . '&page_id=' . $_GET['page_id'] . '&bonaplata_page_id=' . $rs['page_id'] . '&language=' . LANGUAGE . '&no_default';
				$l['selected'] = $selected;
				$l['item'] = $rs['page'];
				$l['has_content'] = $rs['has_content'];
				$l['id'] = $rs['page_id'];
				$l['conta'] = $conta;
				$l['loop_count'] = $loop_count;
				$l['loop_lateral'] = $this->_get_children($rs['page_id'], $level+1, $selected_ids, $menu, $last_level);
				$conta++;
				//$loop[] = $l;
				
				// AMCTAIC
				$query = "
					SELECT bonaplata_id 
					FROM amctaic__bonaplata 
					WHERE page_id = " . $rs['page_id'] . " 
					AND status ='public'
					AND bin=0
					ORDER BY entered DESC
					LIMIT 1";
				$l['bonaplata_id'] = Db::get_first($query);
				$l['page_id'] = $rs['page_id'];
				// si has_content vol dir que es un premi i no una categoria, i si no te premi assignat no el mostro
				if ($rs['has_content'] && !$l['bonaplata_id']){				
				}else {				
					if ($rs["page_id"]==$this->bonaplata_page_id) {
					$this->bonaplata_id = $l['bonaplata_id'];
					$this->bonaplata_parent_id = $rs["page_id"];
					}
					//if ($rs["page_id"]==$this->bonaplata_page_id) $this->bonaplata_parent_id = $rs['page_id'];
					$loop[] = $l;
				}
				// AMCTAIC
			}
			//$loop[] = array ('loop' => $loop2,
			//	'item' => '');
		}
		return $loop;
	}
	function _clean_categories(&$loop){
		
		// si has_content, es un premi i ja he conprovat abans que tingui un premi assignat, per tant el mostro
		// si no te content es una categoria
		//    si no te loop, es una categoria buida i la borro
		//    si te loop, pot tindre a dins una categoria buida també i llavors s'hauria de borrar 
		foreach ($loop as $key=>$value){
			$l = &$loop[$key];
			if (!$l['has_content'] && !$l['loop_lateral']) {
				unset($loop[$key]);
			}
			elseif(!$l['has_content']){
				if (!$this->_clean_categories($loop[$key]['loop_lateral'])) unset($loop[$key]);
			}
			elseif(!$this->bonaplata_id){
				if (!isset($_GET['no_default'])) $l['selected']=1;
				$this->bonaplata_id = $l['bonaplata_id'];
				$this->bonaplata_parent_id = $l["page_id"];
			}
		}
		return count($loop);
		
	}
	
	
}
?>