<?
/**
 * ProductSellpoint
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2011
 * @version $Id$
 * @access public
 */
class ProductSellpoint extends Module{

	var $condition = '';
	var $q = false;
	var $form_tabs;

	function __construct(){
		parent::__construct();
	}
	function on_load(){
		$this->form_tabs = array('0' => array('tab_action'=>'show_form_map&amp;tool=product&amp;tool_section=sellpoint','tab_caption'=>$this->caption['c_edit_map']));	
	}
	function list_records()
	{
		$content = $this->get_records();
		/*
		if ($this->q) {
			$GLOBALS['gl_page']->javascript .= "
				$('table.listRecord').highlight('".addslashes(R::get('q'))."');			
			";
		}
		$GLOBALS['gl_content'] = $this->get_search_form() . $content;*/
		$GLOBALS['gl_content'] = $content;
	}
	function get_records()
	{		
	
		$listing = new ListRecords($this);	
		$listing->add_button ('edit_map', 'action=show_form_map','boto1','after');
		
		// search
		if ($this->condition) {
			$listing->condition = $this->condition;
		}
		
		//$listing->set_options (1, 1, 1);		
		if ($this->action!='list_records_bin') {
			$listing->add_filter('provincia_id');
			$listing->add_filter('seller_kind');
		}
		
		$listing->order_by = 'empresa ASC';
		
	    return $listing->list_records();
	}
	function show_form()
	{
		$GLOBALS['gl_content'] = 
		$this->get_form();
	}
	function get_form()
	{
	    $show = new ShowForm($this);
		$show->form_tabs = $this->form_tabs;		
		
		//$show->call_function['comunitat_id'] = "get_select_comunitat";
		//$show->call_function['provincia_id'] = "get_select_provincia";
		
	    return $show->show_form() .
		'<script language="JavaScript" src="/admin/modules/product/jscripts/chained_selects_sellpoint.js?v='.$GLOBALS['gl_version'].'"></script>';
	}	
	
	

	function save_rows()
	{		
	    $save_rows = new SaveRows($this);
	    $save_rows->save();
	}

	function write_record()
	{
	    $writerec = new SaveRows($this);
		
		
		$writerec->save();
		
		if ($this->action =='save_record' || $this->action =='add_record'){
			
			$provincia_id = $writerec->get_value('provincia_id');
			$country_id = $writerec->get_value('country_id');
			$id = $this->action =='add_record'?$GLOBALS['gl_insert_id']:$writerec->id;
			
			if ($country_id!='ES'){
				
				Db::Execute("
					UPDATE product__sellpoint 
					SET `provincia_id`=0, `comunitat_id`=0 
					WHERE  `sellpoint_id`=" . $id);
				
			}
			else{
				
				if ($provincia_id!='null'){
					$comunitat_id = Db::get_first("
						SELECT comunitat_id FROM all__provincia WHERE provincia_id = " . $provincia_id);

					Debug::p("
						SELECT comunitat_id FROM all__provincia WHERE provincia_id = " . $provincia_id);

					Db::Execute("
						UPDATE product__sellpoint 
						SET `comunitat_id`=".$comunitat_id. "
						WHERE  `sellpoint_id`=" . $id);

					Debug::p("UPDATE product__sellpoint 
						SET `comunitat_id`=".$comunitat_id. "
						WHERE  `sellpoint_id`=" . $id, 'text');
				}
				
			}
			
		}
	}

	function manage_images()
	{
	    $image_manager = new ImageManager($this);
		$image_manager->form_tabs = $this->form_tabs;
	    $image_manager->execute();
	}
	/*
	//
	// FUNCIONS BUSCADOR
	//
	function search(){

		$q = $this->q = R::escape('q');
		
		$search_condition = '';

		if ($q)
		{
			// Sempre busco a tot arreu, abans si detectava que eren refrencies no hi  buscava, posaré checkbox de "buscar nomès referencies"

			$search_condition = "(
				nom like '%" . $q . "%'
				OR cognoms like '%" . $q . "%'
				OR CONCAT(nom, ' ', cognoms) like '%" . $q . "%'
				OR adresa like '%" . $q . "%'
				OR cp like '%" . $q . "%'
				OR telefon like '%" . $q . "%'
				OR telefon2 like '%" . $q . "%'
				OR fax like '%" . $q . "%'
				OR correu like '%" . $q . "%'
				OR url1 like '%" . $q . "%'
				OR agent_description like '%" . $q . "%'
				" ;

			// busco a agencia
			$query = "SELECT agencia_id FROM newgrass__agencia WHERE (
								 agencia like '%" . $q . "%'
									)" ;
			$results = Db::get_rows_array($query);

			$search_condition .= $this->get_ids_query($results, 'agencia_id');
			$search_condition .= ") " ;
			$search_condition = $this->get_ref_query('sellpoint_id',$q, $search_condition);

			$GLOBALS['gl_page']->title = TITLE_SEARCH . '<strong>&nbsp;&nbsp;"' . $q . '"</strong>';
		}
		$this->condition = $search_condition;
		Debug::add('Search condition', $this->condition);
		$this->do_action('list_records');
	}

	function get_ids_query(&$results, $field)
	{
		if (!$results) return '';
		$new_arr = array();
		foreach ($results as $rs){
			$new_arr [$rs[0]] = $rs[0];
		}
		return "OR " . $field . " IN (" . implode(',', $new_arr) . ") ";
	}
	function get_ref_query($field, $q, $search_condition){
		//
		// busqueda automatica de referencies separades per espais o comes
		//
		$ref_array = explode (",", $q); // si l'array hem dona 1 probo de fer-ho amb espais, si es una sola referncia donarà el mateix resultat
		count($ref_array) == 1?$ref_array = explode (" ", $q):false;

		foreach($ref_array as $key => $value)
		{
			if (!is_numeric($value))
			{
				$ref_array = '';
				break;
			}
		}

		// consulta
		if ($ref_array)
		{
			$query_ref = "";
			foreach ($ref_array as $key)
			{
				$query_ref .= $field . " = '" . $key . "' OR ";
			}
			$query_ref = "(" . substr($query_ref, 0, -3) . ") ";

			return '( ' . $query_ref . ' OR ' . $search_condition . ')';
		}
		else{
			return $search_condition;
		}
	}

	function get_search_form(){
		$this->caption['c_by_ref']='';
		$this->set_vars($this->caption);
		$this->set_file('search.tpl');
		$this->set_var('menu_id',$GLOBALS['gl_menu_id']);
		return $this->process();
	}
	*/
	//
	// FUNCIONS MAPA
	//	
	function show_form_map (){ //esenyo el napa
		global $gl_news;
		
		$sellpoint_id = R::id('sellpoint_id');
		$menu_id = R::id('menu_id');
		
		$rs = Db::get_row('SELECT adress, poblacio, cp, provincia_id, empresa, latitude, longitude, zoom, sellpoint_id FROM product__sellpoint WHERE sellpoint_id = ' . $sellpoint_id);
		
		$rs['provincia']=Db::get_first("SELECT name FROM all__provincia  WHERE provincia_id = " . $rs['provincia_id'] . "");
		
		$rs['title'] = 		
			$rs['empresa'];
			
		$rs['address'] =  $rs['adress'] . ', ' . $rs['poblacio'] . ', ' . $rs['cp'] . ', ' . $rs['provincia'];
		
		
		$rs['is_default_point'] = ($rs['latitude']!=0)?0:1;
		$rs['latitude']=$rs['latitude']!=0?$rs['latitude']:GOOGLE_CENTER_LATITUDE;
		$rs['longitude']=$rs['longitude']!=0?$rs['longitude']:GOOGLE_CENTER_LONGITUDE;
		$rs['zoom']=$rs['zoom']?$rs['zoom']:GOOGLE_ZOOM;
		$rs['menu_id'] =  $menu_id;		
		$rs['tool'] =  $this->tool;		
		$rs['tool_section'] =  $this->tool_section;		
		$rs['menu_id'] =  $menu_id;		
		$rs['id'] =  $rs[$this->id_field];
		$rs['id_field'] =  $this->id_field;
		$this->set_vars($rs);//passo els resultats del array
		$this->set_file('product/sellpoint_map.tpl'); //crido el arxiu
		$this->set_vars($this->caption); //passo tots els captions
		$gl_news = $this->caption['c_click_map'];
		$content = $this->process();//processo
		$GLOBALS['gl_content'] = $content;		
	}
	function save_record_map(){
		global $gl_page;
		extract($_GET);
		
		Db::execute ("UPDATE `product__sellpoint` SET
			`latitude` = '$latitude',
			`longitude` = '$longitude',
			`zoom` = '$zoom'
			WHERE `sellpoint_id` =$sellpoint_id
			LIMIT 1") ;
		$gl_page->show_message($this->messages['point_saved']);
		Debug::p_all();
		die();			
	}
}
/*
// fora la clase
function get_select_comunitat($key, $value)
	{
		$query = "ORDER BY name";
		return get_select_sub($value, $key, "all__comunitat", "name, country_id, comunitat_id", $query, '');
	}
	function get_select_provincia($key, $value)
	{
		$query = "ORDER BY name";
		return get_select_sub($value, $key, "all__provincia", "name, comunitat_id, provincia_id", $query, '');
	}

	function get_select_sub($value, $name, $table, $fields, $condition = '', $caption = '', $onchange = '', $disabled = '')
	{
		$html = '';
		$results = get_results_sub($table, $fields, $condition);

		if ($caption)
			$html .= '<option value="0">' . $caption . '</option>';
		if ($results)
		{
			foreach ($results as $rs)
			{
				$selected = '';
				if ($rs[2] == $value)
					$selected = " selected";
				$html .= '<option value="' . $rs[2] . '"' . $selected . '>#' . $rs[1] . "#" . $rs[0] . '</option>';
			}
		}
		if ($onchange)
			$onchange = ' onChange="' . $this->onchange . '"';
		if ($disabled)
			$disabled = ' disabled';
		$html = '<select name="' . $name . '"' . $onchange . $disabled . '>' . $html . '</select>';
		Debug::add("Get select", $html);
		return $html;
	}

	function get_results_sub($table, $fields, $condition)
	{

		$query = "SELECT " . $fields . " FROM " . $table;
		if ($condition)
			$query .= ' ' . $condition;
		Debug::add("Consulta form objects", $query);
		$results = Db::get_rows_array($query);
		return $results;
	}
 * 
 */
?>