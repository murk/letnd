<?

/* 9-1-17

	Inmobles


 */
if ( is_table( 'inmo__property' ) && ! is_field( 'inmo__property', 'sea_distance' ) ) {

	$update_sql[] = "ALTER TABLE `inmo__property`
	ADD COLUMN `sea_distance` INT NOT NULL DEFAULT '0' AFTER `star`;";

} else {
	if ( is_table( 'inmo__property' ) ) {
		trigger_error( 'Ja existeixen el camp sea_distance' );
	}
}


if ( is_table( 'inmo__property' ) && ! is_field( 'inmo__property', 'community_expenses' ) ) {

	$update_sql[] = "ALTER TABLE `inmo__property`
	ADD COLUMN `community_expenses` INT(11) NOT NULL DEFAULT '0' AFTER `star`;";

} else {
	if ( is_table( 'inmo__property' ) ) {
		trigger_error( 'Ja existeixen el camp community_expenses' );
	}
}


if ( is_table( 'inmo__property' ) && ! is_field( 'inmo__property', 'efficiency_number' ) ) {

	$update_sql[] = "ALTER TABLE `inmo__property`
	ADD COLUMN `efficiency_number` INT NOT NULL DEFAULT '0' AFTER `efficiency`;";

} else {
	if ( is_table( 'inmo__property' ) ) {
		trigger_error( 'Ja existeixen el camp efficiency_number' );
	}
}



if ( is_table( 'inmo__property' ) && ! is_field( 'inmo__property', 'efficiency_number2' ) ) {

	$update_sql[] = "ALTER TABLE `inmo__property`
	ADD COLUMN `efficiency_number2` INT NOT NULL DEFAULT '0' AFTER `efficiency2`;";

} else {
	if ( is_table( 'inmo__property' ) ) {
		trigger_error( 'Ja existeixen el camp efficiency_number2' );
	}
}


?>