<?php
/* 31-7-12 */
// Pagament 4b
// Codis promocionals
if (is_table("product__configpublic")){
	$update_sql[]="INSERT INTO `product__configpublic` (`name`, `value`) VALUES ('4b_simulador', '1');";
	$update_sql[]="INSERT INTO `product__configpublic` (`name`) VALUES ('4b_id_comercio');";
	$update_sql[]="ALTER TABLE `product__order`
	CHANGE COLUMN `payment_method` `payment_method` 
	ENUM('paypal','account','ondelivery','directdebit','4b') 
	NOT NULL COLLATE 'utf8_spanish2_ci';";
	
	$update_sql[]="ALTER TABLE `product__order`
	ADD COLUMN `error_code` VARCHAR(255) NOT NULL AFTER `comment`,
	ADD COLUMN `error_description` VARCHAR(255) NOT NULL AFTER `error_code`,
	ADD COLUMN `approval_code` CHAR(17) NOT NULL AFTER `transaction_id`;";
	
	$update_sql[]="ALTER TABLE `product__promcode`
	CHANGE COLUMN `discount` `discount_pvp` INT(11) NOT NULL DEFAULT '0' AFTER `promcode`,
	ADD COLUMN `start_date` DATE NOT NULL AFTER `minimum_amount`;";
	
	$update_sql[]="ALTER TABLE `product__promcode`
	CHANGE COLUMN `discount_pvp` `discount_pvp` DECIMAL(11,2) NOT NULL DEFAULT '0' AFTER `promcode`,
	CHANGE COLUMN `minimum_amount` `minimum_amount` DECIMAL(11,2) NOT NULL DEFAULT '0' AFTER `discount_type`;";
		
	$update_sql[]="CREATE TABLE `product__promcode_language` (
	`promcode_id` INT(11) NOT NULL DEFAULT '0',
	`language` CHAR(3) NOT NULL DEFAULT '' COLLATE 'utf8_spanish2_ci',
	`name` VARCHAR(100) NOT NULL DEFAULT '' COLLATE 'utf8_spanish2_ci'
	)
	COLLATE='utf8_spanish2_ci'
	ENGINE=MyISAM;";
	
	$update_sql[]="ALTER TABLE `product__order`
	ADD COLUMN `promcode_discount` DECIMAL(11,2) NOT NULL AFTER `all_basetax`;";
	
	$permits = Db::get_row("SELECT read1, write1 FROM all__menu WHERE menu_id = 20060");
	
	$update_sql[]="INSERT INTO `all__menu`
	(`menu_id`, `menu_group`, `tool`, `tool_section`, `action`, `parent_id`, `toolmode_id`, `variable`, `ordre`, `link`, `process`, `read1`, `write1`) VALUES
	('20070', 'product', 'product', 'promcode', '', '20000', '1', 'PRODUCT_MENU_PROMCODE', '8', '', '', '" .$permits['read1'] . "', '" .$permits['write1'] . "'),
	('20071', 'product', 'product', 'promcode', 'show_form_new', '20070', '1', 'PRODUCT_MENU_PROMCODE_NEW', '1', '', '', '" .$permits['read1'] . "', '" .$permits['write1'] . "'),
	('20072', 'product', 'product', 'promcode', 'list_records', '20070', '1', 'PRODUCT_MENU_PROMCODE_LIST', '2', '', '', '" .$permits['read1'] . "', '" .$permits['write1'] . "'),
	('20073', 'product', 'product', 'promcode', 'list_records_bin', '20070', '1', 'PRODUCT_MENU_PROMCODE_LIST_BIN', '3', '', '', '" .$permits['read1'] . "', '" .$permits['write1'] . "');";

	$update_sql[]="UPDATE all__menu SET ordre = '1' WHERE menu_id = 20001;";
	$update_sql[]="UPDATE all__menu SET ordre = '2' WHERE menu_id = 20010;";
	$update_sql[]="UPDATE all__menu SET ordre = '3' WHERE menu_id = 20020;";
	$update_sql[]="UPDATE all__menu SET ordre = '4' WHERE menu_id = 20030;";
	$update_sql[]="UPDATE all__menu SET ordre = '5' WHERE menu_id = 20040;";
	$update_sql[]="UPDATE all__menu SET ordre = '6' WHERE menu_id = 20050;";
	$update_sql[]="UPDATE all__menu SET ordre = '7' WHERE menu_id = 20060;";
	
	$update_sql[]="ALTER TABLE `product__promcode` ADD COLUMN `bin` TINYINT(1) NOT NULL DEFAULT '0' AFTER `end_date`;";
}
?>