<?

/* 10-10-16

	INMO

	UP-DATE
	- Reserves, afegir estat a reserves

 */
if ( is_table( 'booking__book' ) && ! is_field( 'booking__book', 'status' ) ) {

	$update_sql[] = "ALTER TABLE `booking__book`
	ADD COLUMN `status` ENUM('pre_booked','booked','confirmed_email','confirmed_post','contract_signed','cancelled') NOT NULL DEFAULT 'booked' AFTER `contract_id`;";

} else {
	if ( is_table( 'booking__book' ) ) {
		trigger_error( 'Ja existeixen el camp status' );
	}
}

if ( is_table( 'booking__configadmin' ) && ! Db::get_first( "SELECT * FROM booking__configadmin WHERE name='default_status'" ) ) {
	$update_sql[] = "
		INSERT INTO booking__configadmin (`name`, `value`) VALUES ('default_status', 'booked');";

} else {
	if ( is_table( 'booking__configadmin' ) ) {
		trigger_error( 'Ja existeixen el camp default_status' );
	}
}

if ( is_table( 'booking__configadmin' ) && ! Db::get_first( "SELECT * FROM booking__configadmin WHERE name='public_default_status'" ) ) {
	$update_sql[] = "
		INSERT INTO booking__configadmin (`name`, `value`) VALUES ('public_default_status', 'booked');";

} else {
	if ( is_table( 'booking__configadmin' ) ) {
		trigger_error( 'Ja existeixen el camp public_default_status' );
	}
}

?>