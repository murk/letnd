<?

/**
 * BookingOrder
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 7/2012
 * @version $Id$
 * @access public
 * Acces a https://sis-t.redsys.es:25443/canales/lacaixa/
 * 1 - http://www.agromercader.com/spa/booking/payment/v/action/get_order/process/lacaixa/
 * 2 - http://www.agromercader.com/spa/booking/payment/v/action/set_notice/process/lacaixa/
 * 3 - http://www.agromercader.com/spa/booking/payment/v/action/show_end/process/end/
 *
 * 1 - Conectar a la caixa:
 * $this->get_content
 * --------------------------------------------
 * https://sis-t.redsys.es:25443/sis/realizarPago
 * https://sis.redsys.es/sis/realizarPago
 *
 * --------------------------------------------
 * Import total compra M97810000   -> 978 -> codi euos, 10000 -> import multiplicat per 100 ( per treure els dos decinals )
 *
 * 2 - Comunicació online del resultat. Apartat 3.2.3
 * --------------------------------------------
 *
 * test: /spa/booking/payment/v/action/set_notice/process/lacaixa/?&order=76&store=PI00020630&test=1
 *
 *
 * Adreça resposta banc
 * /spa/booking/payment/v/action/set_notice/process/lacaixa/
 *
 * Autoritzat
 *
 *
 * Denegat
 *
 *
 * 3 - Pàgina de notificació de la compra
 * --------------------------------------------
 *
 * Adreça:
 * /spa/booking/payment/v/action/show_end/process/end/
 *
 * clau sha2 de probes: sq7HjrUOBfKmC576ILgskD5srU870gJ7
 */
class BookingPaymentLacaixa extends ModuleBase {
	var $vars = array();
	var $config;
	var $url;
	var $book_id;

	var $status = '';
	var $transaction_id;
	var $approval_code = '';
	var $error_code = '';
	var $error_description = '';

	function __construct( &$module, $book_vars = '' ) {
		$this->config = &$module->config;
		$this->url    = $this->config['lacaixa_simulador'] ? 'sis-t.redsys.es:25443/sis/realizarPago' : 'sis.redsys.es/sis/realizarPago';

		if ( $book_vars ) {
			// variables del TPV
			$products = $book_vars['results'];
			$book    = $book_vars['totals'];
			$payment_number    = $book_vars['payment_number'];

			$count       = 1;
			$description = '';
			// formo els productes de la cistella per la descripcio de la venta
			foreach ( $products as $product ) {
				if ($product['quantity'] == '1') {
					$description .= $product['title'] . ', ';
				}
				else {
					$description .= $product['quantity'] . ' ' . $product['title'] . ', ';
				}
			}
			$description = substr( $description, 0, - 2 );

			$amount          = $book['price_total'] * 100;
			$book_id        = sprintf( '%010d', $book_vars['book_id'] ) . '-' . $payment_number; // poso a 12 dígits
			$currency        = '978';
			$transactionType = '0';


			$return_url = HOST_URL . LANGUAGE . '/booking/book/add-custumers/book_id/' . $book_vars['book_id'] . '/';

			$url             = HOST_URL . LANGUAGE . '/booking/payment/v/action/set_notice/process/lacaixa/?book_id=' . $book_vars['book_id'] . '&payment_number=' . $payment_number;

			// Forço sempre http ja que resys no soporta SNI
			$url = str_replace( 'https://', 'http://', $url );

			Debug::p( [
						[ 'amount', $amount ],
						[ 'book_id', $book_id ],
						[ 'merchant code', $this->config['lacaixa_merchant_code'] ],
						[ 'currency', $currency ],
						[ 'transactionType', $transactionType ],
						[ 'url', $url ],
						[ 'lacaixa_clave_sha2', $this->config['lacaixa_clave_sha2']  ],
					], 'laCaixa');

			if (VERSION == 7)
				include_once( DOCUMENT_ROOT . "common/includes/redsys/php7/apiRedsys.php" );
			else
				include_once( DOCUMENT_ROOT . "common/includes/redsys/apiRedsys.php" );

			$redsys = new RedsysAPI;

			$redsys->setParameter( "DS_MERCHANT_PRODUCTDESCRIPTION", $description );
			$redsys->setParameter( "DS_MERCHANT_AMOUNT", $amount );
			$redsys->setParameter( "DS_MERCHANT_ORDER", $book_id );
			$redsys->setParameter( "DS_MERCHANT_MERCHANTCODE", $this->config['lacaixa_merchant_code'] );
			$redsys->setParameter( "DS_MERCHANT_CURRENCY", $currency );
			$redsys->setParameter( "DS_MERCHANT_TRANSACTIONTYPE", $transactionType );
			$redsys->setParameter( "DS_MERCHANT_TERMINAL", $this->config['lacaixa_terminal'] );
			$redsys->setParameter( "DS_MERCHANT_MERCHANTURL", $url );
			$redsys->setParameter( "DS_MERCHANT_URLOK", $return_url );
			$redsys->setParameter( "DS_MERCHANT_URLKO", $return_url );

			$version    = "HMAC_SHA256_V1";
			$parameters = $redsys->createMerchantParameters();
			$signature  = $redsys->createMerchantSignature( $this->config['lacaixa_clave_sha2'] );

			$this->vars = array(
				'Ds_SignatureVersion'   => $version,
				'Ds_MerchantParameters' => $parameters,
				'Ds_Signature'          => $signature
			);
		}


	}

	// obté variables de adreça i dades usuari
	function get_customer( $customer_id ) {
		$sql = "SELECT name AS first_name, CONCAT(surname1, ' ', surname2) AS last_name FROM custumer__custumer WHERE custumer_id = " . $customer_id;
		$ret = Db::get_row( $sql );

		//Debug::p($ret);
		return $ret['first_name'] . ' ' . $ret['last_name'];
	}

	// obté formulari html
	function get_content() {
		$ret = '<form action="https://' . $this->url . '" method="post" id="lacaixa_form">
				';
		foreach ( $this->vars as $key => $val ) {
			$ret .= '<input type="hidden" name="' . $key . '" value="' . $val . '" />
			';
		}
		$ret .= '<input class="paypal boto" type="submit">';
		$ret .= '</form>';
		if ( ! $this->config['lacaixa_simulador'] ) {
			$ret .= get_javascript( '$("#lacaixa_form").submit();' );
		}

		return $ret;
	}

	// test:
	function set_notice() {
		$this->trace( "Variables POST:\n---------------\n" . print_r( $_POST, true ) . "\n\n", '', 'set_notice' );

		$emailtext = "";
		// send_mail_admintotal('Intent de pagament invàlid per HTTP ERROR amb paypal a ' . HOST_URL,$emailtext);

		/*
		 * AUTORITZADA
       Array
		(
			[Ds_TransactionType] => 0
			[Ds_Card_Country] => 724
			[Ds_Date] => 11/07/2013
			[Ds_SecurePayment] => 1
			[Ds_Signature] => D69AC645E983D502ABB603A09D5D2D61A470C6F5
			[Ds_Order] => 000000000023
			[Ds_Hour] => 11:02
			[Ds_Response] => 0000
			[Ds_AuthorisationCode] => 472681
			[Ds_Currency] => 978
			[Ds_ConsumerLanguage] => 1
			[Ds_MerchantCode] => 22264105
			[Ds_Amount] => 4150
			[Ds_Terminal] => 001
		)
		 * NO AUTORITZADA
		Array
		(
			[Ds_ErrorCode] => SIS0253
			[Ds_TransactionType] => 0
			[Ds_Card_Country] => 0
			[Ds_Date] => 11/07/2013
			[Ds_SecurePayment] => 0
			[Ds_Order] => 000000000024
			[Ds_Signature] => C8D02B36F36225951BF146D24DB2D74459082D82
			[Ds_Hour] => 11:05
			[Ds_Response] => 0180
			[Ds_AuthorisationCode] =>
			[Ds_Currency] => 978
			[Ds_ConsumerLanguage] => 1
			[Ds_MerchantCode] => 22264105
			[Ds_Amount] => 23320
			[Ds_Terminal] => 001
		)
		 */

		$signature_post = R::escape( 'Ds_Signature', '', '_POST' );


		if (VERSION == 7)
				include_once( DOCUMENT_ROOT . "common/includes/redsys/php7/apiRedsys.php" );
			else
				include_once( DOCUMENT_ROOT . "common/includes/redsys/apiRedsys.php" );

		$redsys = new RedsysAPI;

		$params    = $_POST["Ds_MerchantParameters"];
		$decodec   = $redsys->decodeMerchantParameters( $params );
		$signature = $redsys->createMerchantSignatureNotif( $this->config['lacaixa_clave_sha2'], $params );

		// ids
		$this->transaction_id    = $redsys->getParameter( 'Ds_Order' );
		$this->approval_code     = $redsys->getParameter( 'Ds_AuthorisationCode' );
		$this->book_id          = $book_id = R::id('book_id');
		$this->error_code        = $redsys->getParameter( 'Ds_ErrorCode' );
		$this->error_description = $redsys->getParameter( 'Ds_Response' ); // hi poso el response per saber que ha contestat el banc
		$merchant_code_post      = $redsys->getParameter( 'Ds_MerchantCode' );



		$this->trace( $this->transaction_id, 'transaction_id' );
		$this->trace( $this->approval_code, 'approval_code' );
		$this->trace( $this->book_id, 'book_id' );
		$this->trace( $this->error_code, 'error_code' );
		$this->trace( $this->error_description, 'error_description' );
		$this->trace( $this->config['lacaixa_merchant_code'], 'merchant_code' );
		$this->trace( $signature_post, 'signatura_post' );
		$this->trace( $signature, 'signatura' );

		// comprovo signatura
		if ( $signature != $signature_post ) {

			$this->trace( "Intent de pagament invàlid per ERROR en signatura de La Caixa" );

			send_mail_admintotal( 'Intent de pagament invàlid per ERROR en signatura de La Caixa a ' . HOST_URL, $emailtext );

			return false;

		}
		// comprovo MerchantCode //
		if ( $this->config['lacaixa_merchant_code'] != $merchant_code_post ) {

			$this->trace( "Intent de pagament invàlid per ERROR en MerchantCode de La Caixa" );

			send_mail_admintotal( 'Intent de pagament invàlid per ERROR en MerchantCode de La Caixa a ' . HOST_URL, $emailtext );

			return false;

		}


		switch ( true ) {
			case ltrim( $this->error_description, '0' ) < 100:
				$this->status = 'completed';
				break;
			default:
				$this->status = 'failed';
				break;

		}

		$this->trace( $this->status, 'status' );

		return true;
	}

	function trace( $text, $name = '', $title = false ) {
		BookingPayment::trace( $text, $name, $title, 'lacaixa' );
	}
}

?>