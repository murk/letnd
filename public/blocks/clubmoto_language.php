<?php
$query = "SELECT language, code, code2
				FROM all__language
				WHERE public=1
				ORDER BY ordre";

$results = Db::get_rows($query);

$conta = 0;
$loop_count = count($results);

	$base_link = $_SERVER["REQUEST_URI"];
	$base_link = ($base_link=='/'.LANGUAGE)?'/':str_replace('/'.LANGUAGE.'/','/',$base_link); // el primer m'asseguro si l'adreça es www.url.com/cat i el segon www.url.com/cat/ , ho faig així en lloc de un sol replace de '/'.LANGUAGE , per que podria ser això www.url/catelquesigui, m'haid'assegurar que es /cat o /cat/
	
	// m'asseguro, trec si hi ha per algun link intern el language al query string
	$base_link = str_replace('language='.LANGUAGE.'&','',$base_link);
	$base_link = str_replace('&language='.LANGUAGE,'',$base_link);
	$base_link = str_replace('?language='.LANGUAGE,'',$base_link); // si nomès hi ha el parametre de idioma
	$base_link = str_replace('&','&amp;',$base_link); // ho faig HTML correcte


if ($results && count($results) > 1) {
    foreach($results as $rs) {
        LANGUAGE == $rs["code"]?$selected = 1:$selected = 0;

			$new_base_link = $base_link; // per si no faig l'if de sota
			$file_name = R::file_name('file_name');
			if ($file_name){
				$lang_page_file_name = Db::get_first("
					SELECT pagina_file_name 
						FROM clubmoto__pagina_language 
						WHERE language='" . $rs['code'] . "' 
						AND pagina_id= (
											SELECT pagina_id 
											FROM clubmoto__pagina_language 
											WHERE language='" . LANGUAGE . "'
											AND pagina_file_name = '" . $file_name . "'
										)
				");
				$new_base_link = str_replace($file_name.'.htm',$lang_page_file_name.'.htm',$base_link);
			}
			$domain = $rs['code']=='spa'?'http://www.clubmoto.es':'http://www.clubmoto.cat';
			$l['link'] = $domain . '/' . $rs['code'] . $new_base_link;
		
		// si es la home, trec el link del menu d'idiomes
		if ($GLOBALS['gl_is_home']) $l['link'] = $domain;
		
        $l['selected'] = $selected;
        $l['item'] = $rs['language'];
        $l['id'] = $rs['code'];
        $l['code2'] = $rs['code2'];
		$l['conta'] = $conta++;
		$l['loop_count'] = $loop_count;
        $loop2[] = $l;
    }
    $loop[] = array ('loop' => $loop2,
        'item' => '');
}
?>