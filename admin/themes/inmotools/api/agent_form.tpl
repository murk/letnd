<script language="JavaScript" src="/admin/jscripts/validacio.js?v=<?= $version ?>"></script>
<script language="JavaScript" src="/admin/jscripts/euro_calc.js?v=<?= $version ?>"></script>
<? if ( $has_images ): ?><? if ( $image_name ): ?>
	<script type="text/javascript">

		 var gl_gallery_images = <?= ImageManager::get_gallery_images( $images ) ?>;

		 gl_default_imatge = '<?=$images[0]['image_name_medium']?>';
	</script>
<? endif // image_name?><? endif // has_images?>

<form name="form_delete" method="post" action="<?= $link_action_delete ?>" target="save_frame">
	<input name="<?= $id_field ?>" type="hidden" value="<?= $id ?>">
</form>
<form name="theForm" method="post" action="<?= $link_action ?>" onsubmit="<?= $js_string ?>" encType="multipart/form-data" target="save_frame">
	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="form">
		<tr>
			<td><? if ( $form_new ): ?><? if ( $save_button ): ?>
					<div id="form_buttons_holder">
						<table id="form_buttons">
							<tr>
								<td class="buttons_top">
									<div class="right">
										<input type="submit" name="Submit" value="<?= $c_send_new ?>" class="btn_form1"/>
									</div>
								</td>
							</tr>
						</table>
					</div>
				<? endif //save_button?>
				<? endif //form_new?>
				<? if ( $form_edit ): ?>
					<div id="form_buttons_holder">
						<table id="form_buttons">
							<tr>
								<td valign="bottom">
									<div class="menu">
										<div><?= $c_edit_data ?></div><? if ( $has_images ): ?>
											<a href="<?= $image_link ?>"><?= $c_edit_images ?></a>
										<? endif //has_images?><? foreach ( $form_tabs as $r ): extract( $r ) ?>
											<a href="?action=<?= $tab_action ?>&<?= $id_field ?>=<?= $id ?>&menu_id=<?= $menu_id ?>"><?= $tab_caption ?></a><? endforeach //$loop?>
									</div>
								</td>
								<td class="buttons_top">
									<div class="right">
										<? if ( $show_previous_link ): ?><a class="boto2 btn_show_previous_link"
										                                    href="<?= $show_previous_link ?>"></a><? endif //show_previous_link?>
										<? if ( $show_next_link ): ?><a class="boto2 btn_show_next_link"
										                                href="<?= $show_next_link ?>"></a><? endif //show_next_link?>
										<? if ( $back_button ): ?>
										<a class="btn_form_back" href="<?= $back_button ?>"></a><? endif //back_button?>
										<? if ( $save_button ): ?>
											<input type="submit" name="Submit" value="<?= $c_send_edit ?>" class="btn_form1"><? endif //save_button?><? if ( $delete_button ): ?>
										<a class="btn_form_delete" href="javascript:submit_form('<? if ( $has_bin ): ?><?= $c_confirm_form_bin ?><? else: ?><?= $c_confirm_form_deleted ?><? endif //has_bin?>', true);">
											<?= $c_delete ?>
											</a><? endif //delete_button?><? if ( $preview_button ): ?>
										<a class="btn_form1" href="<?= $preview_link ?>" target="_blank">
											<?= $c_preview ?>
											</a><? endif //preview_button?><? if ( $print_button ): ?>
											<a class="btn_form1 btn_print" href="<?= $print_link ?>">
												<?= $c_print ?>
											</a> <? endif //print_button?></div>
								</td>
							</tr>
						</table>
					</div>
				<? endif //form_edit?>


				<? ////////////   TITOL  ////////////////?>


				<table width="100%" cellpadding="0" cellspacing="0">
					<tr>
						<td class="formCaptionTitle"><strong>
								<?= $c_edit_data ?>
								&nbsp;&nbsp;</strong>
							<? if ( $form_edit ): ?><span><?= $agencia ?></span><? endif //form_edit?></td>
					</tr>
				</table>


				<? ////////////   CAMPS  ////////////////?>

				<table width="100%" cellpadding="0" cellspacing="0" class="formstbl">

					<tr>
						<td valign="top" class="formsCaption"><strong>
								<?= $c_nom ?>: </strong>
							<input name="agent_id[<?= $id ?>]" type="hidden" value="<?= $id ?>"/>
							<input name="agent_id_javascript" type="hidden" value="<?= $id ?>"/><? if ( $form_new ): ?><? endif //form_new?>
						</td>
						<td class="forms">
							<table border="0" cellspacing="0" cellpadding="0">
								<tr>
									<th><?= $c_nom ?></th>
									<th><?= $c_cognoms ?></th>
								</tr>
								<tr>
									<td><?= $nom ?>
										&nbsp;&nbsp;
									</td>
									<td><?= $cognoms ?>
										&nbsp;&nbsp;
									</td>
								</tr>
							</table>
						</td>
					</tr>

					<tr>
						<td valign="top" class="formsCaption"><strong>
								<?= $c_nums ?>: </strong></td>
						<td class="forms">
							<table border="0" cellspacing="0" cellpadding="0">
								<tr>
									<th><?= $c_api_id ?></th>
									<th><?= $c_aicat_id ?></th>
									<th><?= $c_codi_habitat ?></th>
									<? if ( ! $user_is_agent ): ?>
										<th><?= $c_actiu ?></th>
									<? endif //actiu?>
								</tr>
								<tr>
									<td><?= $api_id ?>
										&nbsp;&nbsp;
									</td>
									<td><?= $aicat_id ?>
										&nbsp;&nbsp;
									</td>
									<td><?= $codi_habitat ?>
										&nbsp;&nbsp;
									</td>
									<? if ( ! $user_is_agent ): ?>
										<td><?= $actiu ?>
											&nbsp;&nbsp;
										</td>
									<? endif //actiu?>
								</tr>
							</table>
						</td>
					</tr>

					<tr>
						<td valign="top" class="formsCaption"><strong>
								<?= $c_adresa ?>: </strong></td>
						<td>
							<?= $adresa ?>
						</td>
					</tr>


					<? if ( ! $user_is_agent ): ?>
						<tr>
							<td valign="top" class="formsCaption"><strong>
									<?= $c_adresa ?>: </strong></td>
							<td class="forms">
								<table border="0" cellspacing="0" cellpadding="0">
									<tr>
										<th><?= $c_adress ?></th>
										<th><?= $c_numstreet ?></th>
										<th><?= $c_block ?></th>
										<th><?= $c_flat ?></th>
										<th><?= $c_door ?></th>
									</tr>
									<tr>
										<td><?= $adress ?>
											&nbsp;&nbsp;
										</td>
										<td><?= $numstreet ?>
											&nbsp;&nbsp;
										</td>
										<td><?= $block ?>
											&nbsp;&nbsp;
										</td>
										<td><?= $flat ?>
											&nbsp;&nbsp;
										</td>
										<td><?= $door ?></td>
									</tr>
								</table>
							</td>
						</tr>
					<? endif //user_is_agent?>
					<tr>
						<td valign="top" class="formsCaption"></td>
						<td class="forms">
							<table border="0" cellspacing="0" cellpadding="0">
								<tr>
									<th><?= $c_municipi_id ?></th>
									<th><?= $c_comarca_id ?></th>
									<th><?= $c_provincia_id ?></th>
									<th><?= $c_cp ?></th>
								</tr>
								<tr>
									<td><?= $municipi_id ?>
										&nbsp;&nbsp;
									</td>
									<td><?= $comarca_id ?>
										&nbsp;&nbsp;
									</td>
									<td><?= $provincia_id ?>
										&nbsp;&nbsp;
									</td>
									<td><?= $cp ?>
										&nbsp;&nbsp;
									</td>
								</tr>
							</table>
						</td>
					</tr>

					<tr>
						<td valign="top" class="formsCaption"><strong>
								<?= $c_telefon ?>: </strong></td>
						<td class="forms">
							<table border="0" cellspacing="0" cellpadding="0">
								<tr>
									<th><?= $c_telefon ?></th>
									<th><?= $c_telefon2 ?></th>
									<th><?= $c_fax ?></th>
								</tr>
								<tr>
									<td><?= $telefon ?>
										&nbsp;&nbsp;
									</td>
									<td><?= $telefon2 ?>
										&nbsp;&nbsp;
									</td>
									<td><?= $fax ?>
										&nbsp;&nbsp;
									</td>
								</tr>
							</table>
						</td>
					</tr>

					<tr>
						<td valign="top" class="formsCaption"><strong>
								<?= $c_correu ?>: </strong></td>
						<td class="forms">
							<table border="0" cellspacing="0" cellpadding="0">
								<tr>
									<th><?= $c_correu ?></th>
									<th><?= $c_correu2 ?></th>
									<th><?= $c_mail ?></th>
								</tr>
								<tr>
									<td><?= $correu ?>
										&nbsp;&nbsp;
									</td>
									<td><?= $correu2 ?>
										&nbsp;&nbsp;
									</td>
									<td><?= $mail ?></td>
								</tr>
							</table>
						</td>
					</tr>

					<tr>
						<td valign="top" class="formsCaption"><strong>
								<?= $c_url1 ?>: </strong></td>
						<td class="forms">
							<table cellpadding="4" cellspacing="0">
								<? foreach ( $url1 as $l ): extract( $l ) ?>
									<tr>
										<td class="forms" valign="top"><?= $lang ?></td>
										<td><?= $url1 ?></td>
									</tr>
								<? endforeach //$url1?>
							</table>
						</td>
					</tr>

					<tr>
						<td valign="top" class="formsCaption"><strong>
								<?= $c_speciality_id ?>: </strong></td>
						<td class="forms">
							<?= $speciality_id ?>
						</td>
					</tr>
					<? if ( ! $user_is_agent ): ?>
						<tr>
							<td valign="top" class="formsCaption"><strong>
									<?= $c_group_id ?>: </strong></td>
							<td class="forms">
								<?= $group_id ?>
							</td>
						</tr>
					<? endif //user_is_agent?>

					<tr>
						<td valign="top" class="formsCaption"><strong>
								<?= $c_agent_description ?>:</strong></td>
						<td class="forms">
							<table cellpadding="4" cellspacing="0">
								<? foreach ( $agent_description as $l ): extract( $l ) ?>
									<tr>
										<td class="forms" valign="top"><?= $lang ?></td>
										<td><?= $agent_description ?></td>
									</tr>
								<? endforeach //$agent_description?>
							</table>
						</td>
					</tr>

				</table>


				<? if ( $form_new ): ?>
					<table width="100%" cellpadding="4" cellspacing="2" class="formsButtonstbl">
						<tr>
							<td align="center" class="formsButtons">
								<input type="submit" name="Submit" value="<?= $c_send_new ?>" class="btn_form1"></td>
						</tr>
					</table>
				<? endif //form_new?>
				<? if ( $form_edit ): ?>
					<table width="100%" cellpadding="4" cellspacing="2" class="formsButtonstbl">
						<tr>
							<td align="center" class="formsButtons">
								<div class="centerFloat1">
									<div class="centerFloat2">
										<input type="submit" name="Submit" value="<?= $c_send_edit ?>" class="btn_form1">
									</div>
								</div>
							</td>
						</tr>
					</table>
				<? endif //form_edit?></td>
		</tr>
	</table>
</form>