<?php
// no es poden posar ; entre mig de la consulta ja que parteix les consultes per el ; a no ser que fem servir $update_sql en array()
	
if (is_table("product__family")){	
	$update_sql .="

ALTER TABLE `product__product_to_variation`
	ADD COLUMN `variation_ref` VARCHAR(255) NOT NULL DEFAULT '' AFTER `variation_last_stock_units_admin`,
	ADD COLUMN `variation_ref_number` INT(11) NOT NULL DEFAULT '0' AFTER `variation_ref`,
	ADD COLUMN `variation_ref_supplier` VARCHAR(50) NOT NULL DEFAULT '' AFTER `variation_ref_number`;";
}	

?>