<style type="text/css">
	#import div{
		padding:20px;
		border-top: 1px solid #D3D3D3;
	}
	#import table td{
		padding:2px 5px;
	}
	#import table th{
		padding:2px 5px;
		text-align:left;
	}
	h6{
		font-size:20px;
		margin:10px 5px;
	}
	#confirm{
		display:none;
		padding:25px;
		overflow:hidden;
		width:80%;
	}
	#group_exists{
		display:none;
		padding:25px;
		font-size:16px;
	}
	#done{
		display:none;
		padding:25px;
		font-size:20px;
	}
	td.moblesgrau-gris{
		color:#a8a8a8;
	}
</style>
<script type="text/javascript">
	
	$(function(){
		$('#excel').change(function(){
		
			var val = $('#excel').val();
			var ext_arr = val.split('.');
			var ext = ext_arr[ext_arr.length-1];
			
			if (ext!='xlsx'){
				alert('L\'arxiu seleccionat no és un arxiu d\'excel correcte');
				 $('#excel').val('');
				 return;
			}
		
			if (!$('#group').val()){				
				$('#group').val(ext_arr[ext_arr.length-2]);
			}
		});
	});
	
</script>
<form name="theForm" method="post" action="/admin/?menu_id=2202&action=add_record" enctype="multipart/form-data" target="save_frame">
  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="form">
    <tr>
      <td>            
          <div id="form_buttons_holder"><table id ="form_buttons">
            <tr>
              <td valign="bottom"><div class="menu"></div></td>
              <td class="buttons_top"><div class="right"></div></td>
            </tr>
          </table></div>


		 <?////////////   TITOL  ////////////////?>	  
		  
		  
        <table width="100%" cellpadding="0" cellspacing="0" >
          <tr>
            <td class="formCaptionTitle"><strong>
              <?=$c_import_title?>
              &nbsp;&nbsp;</strong></td>
          </tr>
        </table>


		 <?////////////   CAMPS  ////////////////?>	  
		 
        <table width="100%" cellpadding="0" cellspacing="0" class="formstbl">
		
			<tr>
			<td valign="top" class="formsCaption">
              <?=$c_group?>:</td>
            <td valign="top" class="forms">
				<input size="50" type="input" id="group" name="group" />				
			</td>
          </tr>
		
			<tr>
			<td valign="top" class="formsCaption">
              <?=$c_select_file?>:</td>
            <td valign="top" class="forms">
				<input class="file" type="file" id="excel" name="excel" />				
			</td>
          </tr>
		  
        </table>
		
          <table width="100%" cellpadding="4" cellspacing="2" class="formsButtonstbl">
            <tr>
              <td align="center" class="formsButtons"><div class="centerFloat1">
                  <div class="centerFloat2">
                    <input type="submit" name="preview" value="<?=$c_import_preview?>" class="btn_form1">
                  </div>
                </div></td>
            </tr>
          </table>
		
		<div id="import">							
		</div>
		<div id="group_exists">
			Ja existeix un grup amb aquest nom, si es segueix l'importació s'agefiran les adreces noves al grup existent i es mantindran les que coincideixen
		</div>
		<div id="confirm">
			<input type="submit" name="import" value="<?=$c_import?>" class="btn_form1">
		</div>
		<div id="done">
			Importació finalitzada
		</div>
		  
		</td>
    </tr>
  </table>
</form>