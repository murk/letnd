<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr valign="middle"> 
    <td width="20%" align="left" nowrap class="pageResults"><?=$split_results_count?></td>
    <td align="right" nowrap class="pageResults"><?=$split_results?></td>
  </tr>
</table>
<div id='opcions_holder'><table width="100%" border="0" cellspacing="0" cellpadding="0" id="opcions">
    <tr>
      <td class="opcionsTitol"><strong>Opcions</strong></td>
    </tr>
    <tr>
      <td class="opcions"><table border="0" cellpadding="2" cellspacing="0">
          <tr valign="middle">
            <td><strong><?=$c_order_by?></strong></td>
            <td><strong><a class="opcions" href="/admin?menu_id=<?=$menu_id?>&sort_by=0&sort_order=<?=$sort_order0?>">
              <?=$c_ref?>
    &nbsp;<img src="/admin/themes/inmotools/images/<?=$sort_img0?>.gif" width="9" height="9" border="0" align="middle"></a></strong></td>
            <td><strong><a class="opcions" href="/admin?menu_id=<?=$menu_id?>&sort_by=1&sort_order=<?=$sort_order1?>">
              <?=$c_property_private?>
    &nbsp;<img src="/admin/themes/inmotools/images/<?=$sort_img1?>.gif" width="9" height="9" border="0" align="middle"></a></strong></td>
            <td><strong><a class="opcions" href="/admin?menu_id=<?=$menu_id?>&sort_by=2&sort_order=<?=$sort_order2?>">
              <?=$c_entered?>
    &nbsp;<img src="/admin/themes/inmotools/images/<?=$sort_img2?>.gif" width="9" height="9" border="0" align="middle"></a></strong></td>
            <td><strong><a class="opcions" href="/admin?menu_id=<?=$menu_id?>&sort_by=3&sort_order=<?=$sort_order3?>">
              <?=$c_category?>
    &nbsp;<img src="/admin/themes/inmotools/images/<?=$sort_img3?>.gif" width="9" height="9" border="0" align="middle"></a></strong></td>
            <td><strong><a class="opcions" href="/admin?menu_id=<?=$menu_id?>&sort_by=4&sort_order=<?=$sort_order4?>">
              <?=$c_status?>
    &nbsp;<img src="/admin/themes/inmotools/images/<?=$sort_img4?>.gif" width="9" height="9" border="0" align="middle"></a></strong></td>
          </tr>
        </table>
          <table border="0" cellpadding="2" cellspacing="0">
            <tr valign="middle">
              <td><strong><a class="botoOpcionsGuardar" href="javascript:submit_form_save('save_rows_save_records<?=$action_add?>')">
                <?=$c_send_edit?>
              </a></strong></td>
              <td><?=$c_select?>
                &nbsp;<strong><a class="opcions" href="javascript:select_all(true, document.list)">
                  <?=$c_all?>
                  </a>&nbsp;-&nbsp;<a class="opcions" href="javascript:select_all(false, document.list)">
                    <?=$c_nothing?>
                  </a></strong></td>
              <?if ($has_bin):?>
              <?if ($is_bin):?>
              <td><strong><a class="botoOpcions" href="javascript:submit_list('save_rows_delete_selected', '<?=$c_confirm_deleted?>', true)">
                <?=$c_delete_selected?>
              </a></strong></td>
              <td><strong><a class="botoOpcions" href="javascript:submit_list('save_rows_restore_selected')">
                <?=$c_restore_selected?>
              </a></strong></td>
              <?else:?>
              <td><strong><a class="botoOpcionsBorrar" href="javascript:submit_list('save_rows_bin_selected')">
                <?=$c_delete_selected?>
              </a></strong></td>
              <?endif // is_bin?>
              <?else:?>
              <td><strong><a class="botoOpcionsBorrar" href="javascript:submit_list('save_rows_delete_selected', '<?=$c_confirm_deleted?>', true)">
                <?=$c_delete_selected?>
              </a></strong></td>
              <?endif // has_bin?>
              <!--<td>
              <td>&nbsp;</td><strong><a class="botoOpcions" href="/admin?menu_id=<?=$menu_id?>&sort_by=0&sort_order=<?=$sort_order0?>">
                <?=$c_move_to?>
               
            <input type="submit" name="bin_selected" value="<?=$c_delete_selected?>" class="boto1">
              </a></strong></td>-->
              <td><strong><a class="botoOpcions" href="<?=$print_link?>">
                <?=$c_print_all?>
              </a></strong></td>
            </tr>
          </table></td>
    </tr>
  </table></div>
<form method="post" action="<?=$form_link?>" name="list" onsubmit="return check_selected(this)" target="save_frame">
  <input name="action" type="hidden" value="save_rows">
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="listRecord">  
  <?foreach($loop as $l): extract ($l)?>
    <tr><?if ($image_name):?>
      <td valign="middle" class="listImage" width="<?=$im_admin_thumb_w?>"><img src="<?=$client_dir?>/inmo/images/<?=$image_name?>" width="<?=$im_admin_thumb_w?>" height="<?=$im_admin_thumb_h?>"> 
        </td><?endif // image_name?>
      <td valign="top"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td class="listTitle"><strong> 
              <input name="selected[<?=$conta?>]" type="checkbox" value="<?=$id?>">
              <?=$fields[0]['val'][0]['val']?> - <?=$fields[1]['val'][0]['val']?></strong></td>
          </tr>
          <tr>
            <td><table border="0" cellspacing="0" cellpadding="0" class="listDetails">
<?foreach($fields as $l): extract ($l)?>
                <tr> 
                  <td><strong class="listItem"><?=$key?>:&nbsp;</strong></td>
                  <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
						<?foreach($val as $l): extract ($l)?>
                      <tr><?if ($lang):?>
                        <td><?=$lang?></td><?endif //lang?>
						<tr></tr>
                        <td><?=$val?></td>
                      </tr>
						<?endforeach //$val?>
                    </table></td>
                </tr>
<?endforeach //$field?>
              </table>
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td><a href="<?=$edit_link?>" class="boto1"><?=$c_edit_data?></a><?if ($is_image_on):?> 
                    <?if (!$image_name):?><a href="<?=$link_image?>" class="boto1"><?=$c_add_images?> 
                    </a><?endif // image_name?><?if ($image_name):?>
                    <a href="<?=$image_link?>" class="boto1"><?=$c_edit_images?></a><?endif // image_name?>&nbsp;&nbsp;<?endif // is_image_on?></td>
                </tr>
              </table></td>
          </tr>
        </table></td>
  </tr>  
<?endforeach //$loop?>  
</table>
</form>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td height="10"></td>
  </tr>
</table> 
  <table width="100%" border="0" cellpadding="0" cellspacing="0">
    <tr valign="middle">
      <td width="20%" align="left" nowrap class="pageResults"><?=$split_results_count?></td>
      <td align="right" nowrap class="pageResults"><?=$split_results?></td>
    </tr>
  </table>