<?php
$query = "SELECT news__new.new_id as new_id, new, subtitle, image_id, name
				FROM news__new_language, news__new, news__new_image
				WHERE news__new_language.new_id = news__new.new_id
				AND news__new.new_id = news__new_image.new_id
				AND main_image = 1
				AND (status = 'public')
				AND (entered=0 OR entered <= now())
				AND (expired=0 OR expired > (curdate()))
				AND in_home = 1
				AND bin <> 1
				AND language = '" . LANGUAGE . "'
				ORDER BY news__new.ordre, entered DESC, new_id LIMIT 3";

$results = Db::get_rows($query);
Debug::add('Results block: news_new',$results);
$selected_found = false;
if ($results)
{
	$conta = 0;
	$loop_count = count($results);
    foreach($results as $rs)
    {
		$l = array();
		if (isset($_GET["new_id"]) && ($_GET["new_id"] == $rs["new_id"]))
		{
			$selected = 1;
			$selected_found = true;
		}
		else
		{
			$selected = 0;
		}
        
        
		if (USE_FRIENDLY_URL){
			$l['link'] = '/' . LANGUAGE . '/news/' . $vars['module'] . '/' . $rs['new_id'];
			//if (isset($_GET['page_file_name'])) $l['link'] .= '/' . $_GET['page_file_name'] . '.html';
		}
		else{
			$l['link'] = '/?tool=news&amp;tool_section=new&amp;action=show_record&amp;new_id=' . $rs['new_id'] . '&amp;language=' . LANGUAGE;
			//if (isset($_GET['page_id'])) $l['link'] .= '&page_id=' . $rs['page_id'];
		}
		$l += ImageManager::get_file_name($rs['image_id'], $rs['name'],'news','new');
        $l['selected'] = $selected;
        $l['item'] = $rs['new'];
        $l['subtitle'] = $rs['subtitle'];
		$l['id'] = $rs['new_id'];
		$l['conta'] = $conta++;
		$l['class'] = '';
		$l['loop_count'] = $loop_count;
        $loop[] = $l;
    }
	$loop[0]['class'] = 'first';	
	$loop[count($loop)-1]['class'] = 'last';
	if (!$selected_found) $loop[0]['selected'] = 1;
}

?>