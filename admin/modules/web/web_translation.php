<?php
global $gl_action, $gl_translation, $gl_db_classes_table, $gl_db_classes_id_field;
if (strstr($gl_action, "_page")) {
    set_tool('web', 'page', 'all__page', 'page_id');
    $gl_translation = '';
} else {
    $gl_db_classes_table = 'translation__translation'; //dic taula
    $gl_db_classes_id_field = 'translation_id'; //dic id
}

function save_rows()
{
    global $gl_action;
    if (strstr($gl_action, "_page")) {
        $gl_translation = new Translation();
        $gl_translation->save_rows();
    }
}
function list_records()
{
return; // això està tot equivocat, tothom pot veure totes les traduccions de tothom
    global $gl_translation, $gl_action, $gl_action;

    if (strstr($gl_action, "_page")) {
        $listing = new ListRecords();
        $gl_translation = new Translation();
        $listing->condition = $gl_translation->get_condition($listing);
        $listing->list_records();
        $gl_translation->show_table();
    } else {
        Db::connect_mother();
        $listing = new ListRecords;
        // $listing->condition = "state != 3";
        $listing->order_by = 'join_date DESC';
        $listing->list_records();
        Db::reconnect();
    }
}
function get_translation_record($name, $val)
{
    global $gl_translation;
    return $gl_translation->get_translation_record($name, $val);
}
function show_form()
{
return; // REVISAR això està tot equivocat, tothom pot veure totes les traduccions de tothom
Db::connect_mother();

    global $tpl, $gl_content, $gl_menu_id, $origin_text, $translate_language, $languageoriginal, $gl_caption, $numwords;
    // recullo la traduccio sel·lecionada i recullo totes les dades necessaries del sql.
    $query = "SELECT * FROM translation__translation WHERE translation_id = '" . $_GET['translation_id'] . "'";
    $results = Db::get_rows($query);

    $voriginal_language = $results [0]['original_language'];
    $vprice = $results [0] ['price'] ;
    $vnumwords = $results [0] ['numwords'];
    $vstate = $results [0] ['state'];
    $voriginal_text = $results [0]['original_text'];
    $accio = 'list_records';

    $vstate = state_name ($vstate);

    $show = new ShowForm;
    $show->set_vars_common();
    $template = 'web/view_translation.tpl';
    $tpl->set_file($template);
    // passo les variables al tpl
    $tpl->set_var('voriginal_text', $voriginal_text);
    $tpl->set_var('vnumwords', $vnumwords);
    $tpl->set_var('vstate', $vstate);
    $tpl->set_var('accio', $accio);
    // $tpl->set_var('link_action',$link_action);
    // processo el tpl
    $gl_content = $tpl->process();

    Db::reconnect();
}

function state_name ($state)
{
    switch ($state) {
        case "1": $estat = "Pendent";
            break;
        case "2": $estat = "En traducció";
            break;
        case "3": $estat = "Finalitzat";
            break;
        case "4": $estat = "Error";
            break;
    } // switch
    return $estat;
}

?>