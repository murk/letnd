<?
/**
 * ClinkSearch
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2010
 * @version $Id$
 * @access public
 */
class ClinkSearch extends Module{
	function __construct(){
		parent::__construct();
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function get_records()
	{	
		$content = '';
		/***** Example
		* PhpDig installed at: http://www.domain.com/phpdig/
		* Want search page at: http://www.domain.com/search.php
		* Copy http://www.domain.com/phpdig/search.php to http://www.domain.com/search.php
		* Copy http://www.domain.com/phpdig/clickstats.php to http://www.domain.com/clickstats.php
		* Set $relative_script_path = './phpdig'; in search.php, clickstats.php, and function_phpdig_form.php
		* Add ($relative_script_path != "./phpdig") && to if statement
		*****/
		
		/* CANVIS 
		config.php linea 81
		templates/simple.html
		*/
		
		//exec('c:\server\php4\php.exe -d max_execution_time=64800 '.CLIENT_PATH.'orca2.3a/os2/spider.php > '.CLIENT_PATH.'temp/spider.log');
		//exec('/usr/bin/php -d max_execution_time=64800 '.CLIENT_PATH.'orca2.3a/os2/spider.php > '.CLIENT_PATH.'temp/spider.log');
		
		if (isset($_GET['q']) && $_GET['q']){
			//ob_start(); // Start output buffering
			//require(HOST_URL . '/clients/clink/phpdig-1.8.8/search.php?query_string='.urlencode($_GET['q']).'&template_demo=simple.html&phpdig_language=' . LANGUAGE_CODE); // Include the file
			
			//require(HOST_URL . '/clients/clink/sphider-1.3.5/search.php?query='.urlencode($_GET['q']).'&search=1&sphider_language=' . LANGUAGE_CODE); // Include the file
			//if (!isset($_GET['query'])) $_GET['query'] = $_GET['q'];
			//require(HOST_URL . '/clients/clink/sphider-1.3.5/search.php?search=1&'.get_all_get_params(array('language','tool','tool_section','search','sphider_language','q')).'&sphider_language=' . LANGUAGE_CODE); // Include the file
			
			//require(HOST_URL . '/clients/clink/orca2.3a/_search.php?'.get_all_get_params(array('language','tool','tool_section','orca_language')).'orca_language=' . LANGUAGE_CODE); // Include the file
			
			
			//$content .= ob_get_contents(); // Get the contents of the buffer
			//ob_end_clean(); // End buffering and discard
			
			$content = '<iframe frameborder="0" allowtransparency="allowtransparency"  src="' . HOST_URL . '/clients/clink/search/search.php'.get_all_get_params(array('language','tool','tool_section','search_language'),'?','&').'search_language=' . LANGUAGE_CODE . '" id="os_search" scrolling="no"></iframe>';
		}
	
		return $content;
	}
}
?>