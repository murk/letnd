<?php

function list_records()
{
    global $gl_print_link, $gl_content;
    if (isset($_GET['dir']))
    {
        $dir = $_GET['dir'] . '/templates';
    }
    else
    {
        $dir = DOCUMENT_ROOT . "clients/finquesgarrotxa/templates";
        $_GET['dir'] =  DOCUMENT_ROOT . "clients/finquesgarrotxa";
    }
    if (is_dir($dir))
    {
        $dir = dir($dir);
        $exclude = array('flash', 'images', '_notes', 'tmp', 'styles');
        print_dirs(0, $dir, $exclude);
    }
    if (isset($_GET['plantilla']))
    {
        $gl_content = '<table width="100%" border="0" cellspacing="0" cellpadding="0" style=""><tr><td valign="top">' . $gl_content . '</td><td valign="top" style="padding-left:10px;">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td class="opcions"><b>Tags a: </b>' . $_GET['plantilla'] . '
			</td>
		</tr>
		<tr>
			<td heigth="30"> </td>
		</tr>
		<tr>
			<td style="background-color:#FFFFF4; border: 1px #CCCCCC solid">
		' . get_code($_GET['plantilla']) . '</td></tr></table></td></tr></table>';
    }
    // desplegable de clients
    $dir = dir(DOCUMENT_ROOT . "clients/");
    $html = '';
    while ($dir_entry = $dir->read())
    {
        if (($dir_entry != '.') && ($dir_entry != '..') && !(is_file($dir->path . "/" . $dir_entry)))
        {
            $selected = (($dir->path . $dir_entry)==$_GET['dir'])?' selected':'';
			$html .= '<option value="' . $dir->path . $dir_entry . '" '.$selected.'>' . $dir_entry . '</option>';
        }
    }
    $html = '<select name="select" id="select" onchange=";window.location=\'/admin/?menu_id=1007&dir=\'+this.options[this.selectedIndex].value">' . $html . '</select>';

    $gl_content = $html . '
		<script type="text/javaScript" src="/common/jscripts/chili/jquery.chili.pack.js"></script>
		<script type="text/javascript">
		    ChiliBook.recipeFolder     = "/common/jscripts/chili/";
		    ChiliBook.stylesheetFolder = "/common/jscripts/chili/";
		</script>
		</script><a class="botoOpcions" href="' . $gl_print_link . '">Imprimir</a><br><br>' . $gl_content;
}

function print_dirs($padding, $dir, $exclude)
{
    global $gl_content;
    while ($dir_entry = $dir->read())
    {
        if (is_file($dir->path . "/" . $dir_entry))
        {
            if ((strstr($dir_entry, "tpl")) || (strstr($dir_entry, "js")))
            {
                $gl_content .= '<table border="0" cellspacing="0" cellpadding="0" style="margin-left: ' . $padding . '">
  <tr>
    <td class="listitem"><a href="/admin/?menu_id=1007&dir='. $_GET['dir'].'&plantilla=' . $dir->path . "/" . $dir_entry . '">' . $dir_entry . '</a></td></tr></table><br />
		  ';
            }
        } elseif (($dir_entry != '.') && ($dir_entry != '..') && !in_array($dir_entry, $exclude))
        {
            $gl_content .= '<table border="0" cellspacing="0" cellpadding="0" style="margin-left: ' . $padding . '">
  <tr>
    <td><b>' . $dir_entry . '</b></td></tr></table><br />
		  ';
            print_dirs($padding + 40, dir($dir->path . "/" . $dir_entry), $exclude);
        }
    }
}

function get_code($file)
{
    $html = file_get_contents ($file);

    $tokens = token_get_all($html);
    $indent = 0;
    $is_inside_php = false;
    $i = 0;
    $php_blocks = array();
    foreach ($tokens as $token)
    {
        if (is_string($token))
        {
            $value = $token;
            $name = '';
        }
        else
        {
            list($name, $value, $line) = $token;
        }
        switch ($name)
        {
            case T_OPEN_TAG_WITH_ECHO:
            case T_OPEN_TAG:
                $is_inside_php = true;
                $php_blocks[$i]['content'] = '';
                $php_blocks[$i]['indent'] = $indent;
                break;
            case T_IF:
            case T_FOR:
            case T_FOREACH:
            case T_SWITCH:
            case T_WHILE:
                $indent ++;
                break;
            case T_ENDIF:
            case T_ENDFOR:
            case T_ENDFOREACH:
            case T_ENDSWITCH:
            case T_ENDWHILE:
                $indent --;
                $php_blocks[$i]['indent'] = $indent;
                break;
            default: ;
        } // switch
        if ($is_inside_php)
        {
            if ($name == T_OPEN_TAG)
            {
            }
            $php_blocks[$i]['content'] .= str_replace("\n", '', $value);
            if ($name == T_CLOSE_TAG)
            {
                $i ++;
                $is_inside_php = false;
            }
        }
    }
    $code = '';
    foreach($php_blocks as $block)
    {
        extract ($block);
        $code .= get_tabs($indent);
        $code .= $content . "\n";
    }

    $code = "
		<pre style='margin-left:20px;margin-top:10px;font-size: 14px;'><code class='php'>" . htmlspecialchars($code) . "</code></pre>";
    return $code;
}

function get_tabs($num)
{
    return str_repeat("\t", $num);
}

?>
