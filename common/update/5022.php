<?

/* 18-6-13 
  Actualitzacio inmoletnd camps SEO



 */
if (is_table("inmo__property_language")){	
	$update_sql .="
ALTER TABLE `inmo__property_language`
	ADD COLUMN `page_title` VARCHAR(255) NOT NULL DEFAULT '' AFTER `custom4`,
	ADD COLUMN `page_description` VARCHAR(255) NOT NULL DEFAULT '' AFTER `page_title`,
	ADD COLUMN `page_keywords` VARCHAR(255) NOT NULL DEFAULT '' AFTER `page_description`,
	ADD COLUMN `property_file_name` VARCHAR(100) NOT NULL DEFAULT '' AFTER `page_keywords`,
	ADD COLUMN `property_old_file_name` VARCHAR(100) NOT NULL DEFAULT '' AFTER `property_file_name`;";
}

?>