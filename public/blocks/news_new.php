<?php

global $gl_is_home;

if (!defined('NEWS_SHOW_ENTERED_TIME')) {
	$cf = get_config_array('public', 'news');
	$cf_admin =  get_config_array('admin', 'news');
	$cf = array_merge($cf_admin, $cf); // preferencia public

	$block_max_results = $cf['block_max_results'];
	$ignore_publish_date =  $cf['ignore_publish_date'];
	$has_files_show_home =  $cf['has_files_show_home'];
} else {
	$block_max_results = NEWS_BLOCK_MAX_RESULTS;
	$ignore_publish_date =  NEWS_IGNORE_PUBLISH_DATE;
	$has_files_show_home =  NEWS_HAS_FILES_SHOW_HOME;
}

$in_home = isset($vars['in_home']) ? $vars['in_home'] : false;
$destacat = isset($vars['destacat']) ? $vars['destacat'] : false;
$category_id = isset($vars['category_id']) ? $vars['category_id'] : false;
$show_images = isset($vars['show_images']) ? $vars['show_images'] : false;
$show_files = isset($vars['show_files']) ? $vars['show_files'] : false;
$block_max_results = isset($vars['block_max_results']) ? $vars['block_max_results'] : $block_max_results;
$auto_select_first = isset($vars['auto_select_first']) ? $vars['auto_select_first'] : true;
$page_id = isset($vars['page_id']) ? $vars['page_id'] : false;
$orderby = isset($vars['orderby']) ? $vars['orderby'] : false;
$add_dots_length = isset($vars['add_dots_length']) ? $vars['add_dots_length'] : false; // add_dots_length
$add_dots_content_list = isset($vars['add_dots_content_list']) ? $vars['add_dots_content_list'] : false; // si es 1 també retallo el contentlist

$query = "SELECT news__new.new_id as new_id, 
				new, 
				subtitle,
				content,
				content_list,
				entered,
				expired,
				url1,
				url1_name,
				destacat,
				in_home,
				category_id
				FROM news__new_language, news__new
				WHERE news__new_language.new_id = news__new.new_id
				AND (status = 'public')
				AND bin <> 1
				AND language = '" . LANGUAGE . "'";
if ($ignore_publish_date)
	$query .= "
				AND (expired=0 OR expired > (curdate()))";
else
	$query .= "
				AND (entered=0 OR entered <= now())
				AND (expired=0 OR expired > (curdate()))";
if ($category_id)
	{
		if ($category_id[0] == '!')
			$query .= "
				AND category_id <> '" . substr($category_id,1) . "'";
		else
			$query .= "
				AND category_id = '" . $category_id . "'";
	}
if ($in_home)
	$query .= "
				AND in_home = 1";
if ($destacat)
	$query .= "
				AND destacat = 1";
if ($orderby)
	$query .= "		
				ORDER BY " . $orderby;
else
	$query .= "		
				ORDER BY ordre ASC,  entered DESC, new_id";

$query .= "		
				LIMIT " . $block_max_results;

$results = Db::get_rows($query);

$selected_found = false;
if ($results) {
	$conta = 0;
	$loop_count = count($results);
	foreach ($results as $rs) {
		$l = array();
		if (isset($_GET["new_id"]) && ($_GET["new_id"] == $rs["new_id"])) {
			$selected = 1;
			$selected_found = true;
		} else {
			$selected = 0;
		}

		//$l['link'] = get_block_link($rs['new_id'], $file_name, $tool, $tool_section, $category, $category_id);
				
		if (USE_FRIENDLY_URL){
			// TODO-i Està malament el new_id, no agafa cap file_name. S'ha de posar la funció get_link
			$l['link'] = '/' . LANGUAGE . '/news/new/' . $rs['new_id'] . '/';
			
			// EVITAR DUPLICATS - Posar un  page automàticament
			$page_name = '';
			$page_query = "SELECT  page_file_name FROM all__page INNER JOIN all__page_language USING(page_id) WHERE language='" . LANGUAGE . "' AND link = '";

			// 1 - Busco /news/new/category_id/
			$link_query = '/news/new/category_id/' . $rs['category_id'];
			$query      = $page_query . $link_query . "'";
			$page_name = Db::get_first( $query );
			if ( $page_name ) {
				$l['link'] .= 'category_id/' . $rs['category_id'] . '/';
			}

			// 2 - Busco amb /news/new/category/
			if (!$page_name) {

				$link_query = '/news/new/category/' . $rs['category_id'];
				$query      = $page_query . $link_query . "'";
				$page_name  = Db::get_first( $query );
				if ( $page_name )
					$l['link'] = '/' . LANGUAGE . '/news/new/' . $rs['new_id'] . '/category/' . $rs['category_id'] . '/';
			};

			// 3 - Busco a /news/new/
			if (!$page_name) {
				$link_query = '/news/new';
				$query = $page_query . $link_query . "'";

				$page_name = Db::get_first($query);
			}

			// 4 - Alguns llocs com a api.cat, no surt al page, però ha de sortir la categoria igualment
			if (!$page_name && $category_id) {
				$l['link'] .= 'category_id/' . $category_id . '/';
			}

			if ($page_name) $l['link'] .= $page_name . '.html';
			
		}
		else {
			$l['link'] = '/?tool=news&tool_section=new&action=show_record&new_id=' . $rs['new_id'] . '&language=' . LANGUAGE;
			if (isset($_GET['page_id']))
				$l['link'] .= '&page_id=' . $_GET['page_id'];
		}

		$category = Db::get_first("SELECT category FROM news__category_language WHERE category_id = '" . $rs['category_id'] . "' AND language = '" . LANGUAGE . "'");

		$l['show_record_link'] = $l['link'];
		$l['selected'] = $selected;
		$l['new'] = $rs['new'];
		$l['subtitle'] = $rs['subtitle'];
		if ($add_dots_content_list) {
			$rs['content_list'] = add_dots('content_list', $rs['content_list'], $add_dots_content_list);
		}
		$l['content'] = $rs['content_list'] ? $rs['content_list'] : add_dots('content', $rs['content'], $add_dots_length);
		$l['category_id'] = $rs['category_id'];
		$l['category'] = $category;
		$l['id'] = $rs['new_id'];
		$l['entered'] = format_date_form($rs['entered']);
		$l['expired'] = format_date_form($rs['expired']);
		$l['url1'] = $rs['url1'];
		$l['url1_name'] = $rs['url1_name'] ? $rs['url1_name'] : $rs['url1'];
		$l['destacat'] = $rs['destacat'];
		$l['in_home'] = $rs['in_home'];
		$l['conta'] = $conta++;
		$l['class'] = '';
		if ($show_images) {
			$l += UploadFiles::get_record_images($rs['new_id'], true, 'news', 'new');
		}
		if ($show_files) {
			$condition = $has_files_show_home?'show_home = 1':'';
			// TODO-i Mirar on més canviar quan hi ha categories de files
			$files = UploadFiles::get_record_files($rs['new_id'], 'file', 'news', 'new', false, false, $condition);
			if ( isset ( $files [ 'files' ] ) ) {
				$l += $files;
			} else {
				$l['files'] = $files;
			}
		}
		$l['loop_count'] = $loop_count;
		$loop2[] = $l;
	}
	$loop2[0]['class'] = 'first';
	$loop2[count($loop2) - 1]['class'] = 'last';
	if (!$selected_found && $auto_select_first)
		$loop2[0]['selected'] = 1;
	$loop[] = array('loop' => $loop2,
		'item' => '');
}
else {
	/* si no hi ha cap resultat no mostro el block */
	$show_block = false;
	return;
}
?>