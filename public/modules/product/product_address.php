<?
/**
 * ProductAddress
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2010
 * @version $Id$
 * @access public
 */
class ProductAddress extends Module{
	var $customer_id, $delivery_id, $invoice_id, $same_address='checked';
	var $is_seller; // de payment->send_mails
	var $is_invoice = false; // per quan es mostra una factura
	function __construct(){
		parent::__construct();
	}
	function on_load(){
		// si es privada no deixo fer res excepte entrar login i password
		if ($this->is_index && $this->config['is_private_shop'] && !$this->is_logged()) {
			redirect('/?tool=product&tool_section=customer&action=login&language='.LANGUAGE);
			die();
		}
	}
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{
		$show = new ShowForm($this);
		$show->has_bin = false;
		$show->caption['c_form_title'] = $this->action=='show_form_new'?$show->caption['c_new_address']:$show->caption['c_edit_address'];
		return $show->show_form();
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function get_records()
	{
	    $listing = new ListRecords($this);
		$listing->has_bin = false;

		$listing->condition = 'customer_id = ' . $this->customer_id . ' AND invoice=0';
		$listing->set_var('customer_id',$this->customer_id);
		$listing->set_field('customer_id','type','none');
		$listing->set_options(0,0,0,0,0,0,0,0,0,0);
		$listing->add_button('edit_address', '','','before','address_edit');

		// una altra forma enlloc de fer un list_records_walk
	    $listing->list_records(false);
		foreach ($listing->results as $key=>$val){
			$rs = &$listing->results[$key];
			if ($rs['address_id']==$this->delivery_id){
				$rs['checked'] = 'checked';
				$this->same_address= '';
			}
			else{
				$rs['checked'] = '';
			}
		}

		$listing->set_var('same_address',$this->same_address);
		$listing->always_parse_template = true; // si no hi ha resultats mostra el template igualment en lloc del missatge de "No hi ha registres...."
		return $listing->parse_template();
	}
	// retorna el valor de les dues adresses per a order i customer quan es get_record
	function get_addresses()
	{
		$show = new ShowForm($this);
		$show->has_bin = false;
		$show->action = 'get_record';
		$show->set_file('product/address_record.tpl');

		// adreça invoice
		$show->id = $this->invoice_id;
		$show->set_var('is_invoice',$this->is_invoice);
		$show->set_var('dni',''); // això fa que no es mostrin dni, tel, mobile, etc...
		$show->get_values();

		// trec variables nom per agafar de taula customer, ja que adreça invoice agafa el nom de customer
		unset($show->rs['a_name']);
		unset($show->rs['a_surname']);
		
		/* No se per que ha de ser diferent en el seller
		$fields = $this->is_seller?
			'dni, telephone, telephone_mobile, name as a_name, surname as a_surname':
			'name as a_name, surname as a_surname';*/
		$fields = 'mail, dni, telephone, telephone_mobile, name as a_name, surname as a_surname, company';
		$show->rs +=
			Db::get_row('SELECT '.$fields.'
				FROM product__customer
				WHERE customer_id = ' . $show->rs['customer_id']);

		$address_invoice = $show->show_form();

		// adreça delivery
		$show->id = $this->delivery_id;
		$show->set_var('company',''); // això fa que no es mostrin dni, tel, mobile, etc...
		$show->set_var('dni',''); // això fa que no es mostrin dni, tel, mobile, etc...
		$show->reset();
		// si no hi ha addreça d'entrega poso la mateixa de facturació
		$show->get_values();
		if ($show->has_results ) {
			$is_same_address = false;
			$address_delivery = $show->show_form();
		}
		else{
			$is_same_address = true;
			$show->reset();
			$show->id = $this->invoice_id;
			$address_delivery = $show->show_form();
		}

		return array(
			'address_delivery' => $address_delivery,
			'address_invoice' => $address_invoice,
			'is_same_address' => $is_same_address,
		);
	}

	function write_record()
	{
		$writerec = new SaveRows($this);
		$this->customer_id = $this->customer_id?$this->customer_id:$_SESSION['product']['customer_id']; // nomes es pot editar un mateix desde public
		$this->set_field('customer_id','override_save_value',$this->customer_id);

		$country = $writerec->get_value( 'country_id' );
		if ($country != 'ES') $writerec->set_value( 'provincia_id', 0 );
		
	    $writerec->save();

		// Si no guardo desde customer ( ho faig amb els dos formularis en un "merge")
		// Quan guardo desde editar dades de form customer, la nova addreça la poso d'entrega
		// i retorno inner_html
		if ($this->parent != 'ProductCustomer') {
			Debug::add('Write address', $writerec);
			Debug::add('$this->customer_id', $this->customer_id);

			// si es nova poso com a adreça d'entrega, ja que se suposa que si el client entra
			// una nova adreça és per que vol que sigui l'adreça d'entrega
			if ($this->action == 'add_record'){
				$this->delivery_id = $GLOBALS['gl_insert_id'];
				Db::execute("UPDATE product__customer SET delivery_id = " . $this->delivery_id . " WHERE customer_id =" . $this->customer_id);
			}
			else{
				$query = "SELECT delivery_id FROM product__customer WHERE customer_id = " . $this->customer_id;
				$this->delivery_id = Db::get_first($query);
			}

			set_inner_html('addresses_list', $this->get_records());
		}

	}

	/**
	 * ProductCustomer::is_logged()
	 * Saber si està loguejat
	 * @return
	 */
	function is_logged(){
		// miro si hi ha les 2 sessions per més seguretat de que esta logged
		return (isset($_SESSION['product']['customer_id']) && isset($_SESSION['product']['mail']));
	}
}
?>