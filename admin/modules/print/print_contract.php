<?php
function save_rows()
{
    $save_rows = new SaveRows;
    $save_rows->save();
}
function give_me_name_town ($town_id){
	
	$query = "SELECT municipi FROM inmo__municipi WHERE municipi_id = ".$town_id;
	$town_id = Db::get_row($query);
	$town_id = $town_id ['municipi'];
	return $town_id;
}
function put_points ($value){ //funciona que posa punts als valors que estiguin buits i així al contracte quedi marcat que falten dades
	if ($value == "" ){
	$value = "..............";
	}
	return $value;
}
function give_me_values_print_contract ($contract_id)
{
    $query = "SELECT * FROM print__contract WHERE contract_id = " . $contract_id;
    $rs = Db::get_rows($query);
	foreach ($rs[0] as $item => $value){ //poso puntents per al contracte    					
			$value=put_points ($value);			
			$rs[0][$item]=$value;
		}
    if ($rs[0]['property_id'] == null) {
        $error = true;
    } else { // falta la propietat
	   $query2 = "SELECT * FROM inmo__property INNER JOIN inmo__property_language USING (property_id) WHERE inmo__property.property_id  =" . $rs[0]['property_id'] . " AND inmo__property_language.language ='" . $rs[0]['language_id'] . "'";
        $rs2 = Db::get_rows($query2); //dades de la propietat		
		foreach ($rs2[0] as $item => $value){ //poso puntents per al contracte    					
			$value=put_points ($value);			
			$rs2[0][$item]=$value;
		}			
		$rs2[0]['municipi_id']= give_me_name_town($rs2[0]['municipi_id']);
		$rs2[0]['locationregister']= give_me_name_town($rs2[0]['locationregister']);		
    }
    $query3 = "SELECT custumer_id FROM custumer__custumer_to_property WHERE property_id =" . $rs[0]['property_id'];
    $rs3 = Db::get_rows($query3); //id del propietari
    if ($rs3[0]['custumer_id'] == null) {
        $error = true;
    } else { // falta el propietari
        $i = 0;
        foreach ($rs3 as $res) {
            $query4 = "SELECT * FROM custumer__custumer WHERE custumer_id =" . $res['custumer_id'];
            $rs14 = Db::get_rows($query4); //dades del propietari
			foreach ($rs14[0] as $item => $value){ //poso puntents per al contracte    					
			$value=put_points ($value);			
			$rs14[0][$item]=$value;
			}			
            $rs4[$i] = $rs14[0];
            $i++;
        }
    }
    $query5 = "SELECT custumer_id FROM print__contract_to_custumer WHERE contract_id =" . $contract_id;
    $rs5 = Db::get_rows($query5); //id del comprador/llogater
    if ($rs5[0]['custumer_id'] == null) {
        $error = true;
    } else { // falta el comprador
        $query6 = "SELECT * FROM custumer__custumer WHERE custumer_id =" . $rs5[0]['custumer_id'];
        $rs6 = Db::get_rows($query6); //id del comprador/llogater
		foreach ($rs6[0] as $item => $value){ //poso puntents per al contracte    					
			$value=put_points ($value);			
			$rs6[0][$item]=$value;
			}	
    }    
	$values = array();
    $values['dadescontracte'] = $rs;
    $values['dadespropietat'] = $rs2;
    $values['dadespropietari'] = $rs4;
    $values['dadescomprador'] = $rs6;
 /*echo"<strong>dades de la propietat<br/><br/></strong>";print_r ($values['dadespropietat']);echo "<br/><br/><br/>";
	echo"<strong>dades del propietari<br/><br/></strong>";print_r ($values['dadespropietari']);echo "<br/><br/><br/>";
	echo"<strong>dades del comprador<br/><br/></strong>";print_r ($values['dadescomprador']);echo "<br/><br/><br/>";
	echo"<strong>dades del contracte<br/><br/></strong>";print_r ($values['dadescontracte']);echo "<br/><br/><br/>";*/
    $values['valid'] = $error;
    return $values;
}
function sumarmeses ($fechaini, $meses) // funcio que suma mesos a una data inicial
{
    $dia = substr($fechaini, 0, 2);
    $mes = substr($fechaini, 3, 2);
    $anio = substr($fechaini, 6, 4);
    $tmpanio = floor($meses / 12);
    $tmpmes = $meses % 12;
    $anionew = $anio + $tmpanio;
    $mesnew = $mes + $tmpmes;
    if ($mesnew > 12) {
        $mesnew = $mesnew-12;
        if ($mesnew < 10)
            $mesnew = "0" . $mesnew;
        $anionew = $anionew + 1;
    }
    $fecha = date("Y-m-d", mktime(0, 0, 0, $mesnew, $dia, $anionew));
    return $fecha;
}
function tell_me_name_month ($date) // funcio que canvia el numero de mes per a lletres exemple transforma 1 a gener
{
    global $gl_caption_months;
    $value = $date-1;
    $value = $gl_caption_months[$value];
    return $value;
}
function divide_date ($date)
{
    // funció que divideix una data i retorna un array separant el dia, el mes i el any;
    $values = explode ("-", $date);
    return $values; //values[0]=any, $values[1]=mes, $values[2]=dia
}

function show_form()
{
    global $gl_process, $gl_action, $tpl, $gl_caption_months, $gl_page, $gl_messages, $gl_caption;

    switch ($gl_action) {
        case 'show_form_edit_printcontract':// vull impimir el contracte
            $contract_id = $_GET['contract_id'];
            // recullo totes les variables a passar per el contracte.
            $values = give_me_values_print_contract($contract_id);
            if ($values['valid'] != true) {
                $dadescontracte = $values['dadescontracte']; //ho necessito pel nom del arxiu i per la data de signatura;
                $data_firma = divide_date($dadescontracte[0]['signed']);
                $data_mes = tell_me_name_month($data_firma[1]);
                // contracte 1->arres, 2->compravenda, 3->lloguer temporal, 4->lloguer habitual, 5->lloguer local
                $show = new ShowForm;
                $dadespropietari = $values['dadespropietari'];
                switch ($dadescontracte[0]['kind_id']) {
                    case '4': // lloguer habitual, tinc que calcular tb la data de quan caduca el contracte
                        $data_mes_end = sumarmeses($dadescontracte[0]['signed'], $dadescontracte[0]['duration']);
                        $data_mes_end = divide_date($data_mes_end);
                        $data_mes_end_name = tell_me_name_month($data_mes_end[1]);
                        $tpl->set_var('data_mes_end', $data_mes_end);
                        $tpl->set_var('data_mes_end_name', $data_mes_end_name);
                        break;
                } // switch					
                $tpl->set_var('data_firma', $data_firma);
                $tpl->set_var('data_mes', $data_mes);
                $tpl->set_var('dadescontracte', $values['dadescontracte']);
                $tpl->set_var('dadespropietat', $values['dadespropietat']);
                $tpl->set_var('dadespropietari', $values['dadespropietari']);
                $tpl->set_var('dadescomprador', $values ['dadescomprador']);
                // !!!!!!!!!!!! atenció no agafa mai la ruta del client!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1
                if (is_file(CLIENT_PATH . '/templates/print/contract_' . $dadescontracte[0]['kind_id'] . '_' . $dadescontracte[0]['language_id'])) {
                    $tpl->path = CLIENT_PATH;
                    $show->template = '/templates/print/contract_' . $dadescontracte[0]['kind_id'] . '_' . $dadescontracte[0]['language_id'];
                } else {
                    $show->template = '/print/contract_' . $dadescontracte[0]['kind_id'] . '_' . $dadescontracte[0]['language_id'];
                }

                $show->show_form();
                $show->rtf('contracte_letnd'); ///es el nom del document quan s'obre*/
                die();
            } //tenco el if de validació $values['valid'];
            else {
                $gl_message = $gl_caption['c_error_contract'];
            }
        default:
            switch ($gl_process) {
                case 'get_property_jscript':
                case 'get_buyers_jscript':
                case 'get_owners_jscript':
                    $old_tpl = '';
                    if ($gl_process == 'get_buyers_jscript') {
                        $sel = 'selected_addresses_ids';
                        $sel_ids = isset($_POST[$sel])?$_POST[$sel]:$_GET[$sel];
                        $buyers = $sel_ids?show_get_custumers(true, true, '', $sel_ids, $old_tpl):'';
						set_inner_html('buyers', $buyers);
                    } elseif ($gl_process == 'get_owners_jscript') {
                        $sel = 'selected_addresses_ids';
                        $sel_ids = isset($_POST[$sel])?$_POST[$sel]:$_GET[$sel];
                        $owners = $sel_ids?show_get_custumers(false, true, '', $sel_ids, $old_tpl):'';
						set_inner_html('owners', $owners);
                    } elseif ($gl_process == 'get_property_jscript') {
						set_inner_html('property', show_get_property($_GET['property_id'], $old_tpl));
						set_inner_html('owners', show_get_custumers(false, true, $_GET['property_id'], '', $old_tpl));
                    }
					$gl_page->show_message($gl_messages['assigned']);
                    die();
                    break;

                default:
                    $show = new ShowForm;
                    $show->call('show_records_walk', 'property_id,state,kind_id');
                    $show->show_form();
            } // switch
    } // switch
}
function show_records_walk($property_id, $state, $kind_id)
{
    global $tpl, $gl_action, $gl_message;
    $ret['property_id'] = $property_id?$property_id:'';
    if ($gl_action == 'show_form_new') {
        global $gl_db_classes_fields;
        $ret['property'] = $ret['owners'] = $ret['buyers'] = '';
        $ret['ipcs'] = array();
        $tpl->set_var('selected_buyer_ids', '');
        $tpl->set_var('selected_buyer_names', '');
        $tpl->set_var('selected_property_ids', '');
        $tpl->set_var('selected_property_names', '');
        $state = $gl_db_classes_fields['state']['default_value'];
        $kind_id = $gl_db_classes_fields['kind_id']['default_value'];
    } else {
        $old_tpl = $tpl;
        $old_message = $gl_message;
        $ret['property'] = show_get_property($property_id, $old_tpl);
        $tpl = $old_tpl;
        $ret['owners'] = show_get_custumers(false, false, $property_id, '', $old_tpl);
        $tpl = $old_tpl;
        $ret['buyers'] = show_get_custumers(true, false, '', '', $old_tpl);
        $tpl = $old_tpl;
        $ret['ipcs'] = show_get_ipcs();
        $tpl->set_var('form_edit', true); // se'm sobreescriu en algun lloc, torno a posar tal com ha d'estar
        $gl_message = $old_message;
    }

    if ($state == 'current') {
        $tpl->set_var('rescinded_visible', 'none');
    } else {
        $tpl->set_var('rescinded_visible', 'block');
    }
    if ($kind_id == '1' || $kind_id == '2') {
        $tpl->set_var('registered_visible', 'block');
        $tpl->set_var('ipc_visible', 'none');
    } else {
        $tpl->set_var('registered_visible', 'none');
        $tpl->set_var('ipc_visible', 'table-row');
    }

    return $ret;
}
function show_get_property($property_id, &$old_tpl)
{
    if (!$property_id) return false;
    global $gl_content, $gl_has_images, $tpl;
    change_tool('inmo', 'property', 'show_record', $property_id);
    $gl_has_images = true;
    $show = new ShowForm;
    $show->template = 'print/contract_property_form';
    $show->show_form();
    if ($old_tpl) {
        $rs = &$show->rs;
        $old_tpl->set_var('selected_property_ids', $property_id);
        $old_tpl->set_var('selected_property_names', $rs['ref'] . ' ' . $rs['property_private']);
    }
    return $gl_content;
}
function show_get_custumers($is_buyer, $is_javascript, $property_id, $buyer_ids, &$old_tpl)
{
    // if (!$property_id && !$buyer_ids) return false;
    global $gl_content, $gl_has_images, $tpl;
    change_tool('custumer', 'custumer', 'show_record');
    set_field('country', 'list_admin', 'text');
    $gl_content = '';
    $gl_has_images = false;
    $listing = new ListRecords;
    $listing->order_by = 'name asc, surname1 asc, surname2 asc';
    $listing->template = 'print/contract_custumer_list';

    if ($is_buyer) {
        $tpl->set_var('is_owner', false);
        $listing->add_button ('edit_button', '', 'boto1', 'before', 'show_form_edit_buyer');
    } else {
        $tpl->set_var('is_owner', true);
        $listing->add_button ('edit_button', '', 'boto1', 'before', 'show_form_edit_owner');
    }
    // compradors
    if ($is_buyer && !$is_javascript) {
        $listing->join = 'INNER JOIN print__contract_to_custumer USING (custumer_id)';
        $listing->condition = 'contract_id = ' . $_GET['contract_id'];
        $listing->list_records(false);
        $ids = $names = array();
        foreach ($listing->results as $key) {
            $ids[] = $key['custumer_id'];
            $names[] = $key['name'] . ' ' . $key['surname1'] . ' ' . $key['surname2'];
        }

        $old_tpl->set_var('selected_buyer_ids', implode('#;#', $ids));
        $old_tpl->set_var('selected_buyer_names', implode('#;#', $names));
        $listing->parse_template();
    }
    // propietaris desde get_property_jscript i Javascript propietaris
    elseif ($property_id || $buyer_ids) {
        if ($buyer_ids) {
            $listing->condition = 'custumer_id IN (' . $buyer_ids . ')';
        } else {
            $listing->join = 'INNER JOIN custumer__custumer_to_property USING (custumer_id)';
            $listing->condition = 'property_id = ' . $property_id;
        }
        $listing->list_records(false);
        $ids = $names = array();
        foreach ($listing->results as $key) {
            $ids[] = $key['custumer_id'];
            $names[] = $key['name'] . ' ' . $key['surname1'] . ' ' . $key['surname2'];
        }
        $tpl->set_var('selected_owner_ids', implode(',', $ids));
        $tpl->set_var('selected_owner_names', implode(',', $names));
        $listing->parse_template();
    }
    // Javascript compradors
    elseif ($is_javascript) {
        $listing->condition = 'custumer_id IN (' . $buyer_ids . ')';
        $listing->list_records();
    }
    return $gl_content;
}
function show_get_ipcs()
{
    $contract_id = $_GET['contract_id'];
    $query = 'SELECT contract_id, rise, rised FROM print__ipc WHERE contract_id = ' . $contract_id;
    $results = Db::get_rows($query);
    foreach($results as $key => $rs) {
        $results[$key]['rised'] = $rs['rised']?' checked':'';
        $results[$key]['rise'] = format_date_form($rs['rise']);
    }
    return $results;
}

function list_records()
{
    set_language_vars('inmo', 'property', 'contract');
    $listing = new ListRecords;
    $listing->add_filter('kind_id');
    $listing->join = 'LEFT OUTER JOIN inmo__property USING (property_id)';
    $listing->group_fields = array('kind_id');
    $listing->call('list_records_walk', 'contract_id,property_id,expires,ipc');
    $listing->list_records();
    // global $tpl; Debug::p($tpl);
}
function list_records_walk($contract_id, $property_id, $expires, $ipc)
{
    // obtinc propietaris
    $query = "SELECT name, surname1, surname2, phone1, phone2, phone3
				FROM custumer__custumer
				INNER JOIN custumer__custumer_to_property USING (custumer_id)
				WHERE property_id = " . $property_id;
    $rs['owners'] = Db::get_rows($query);
    // obtinc compradors
    $query = "SELECT name, surname1, surname2, phone1, phone2, phone3
				FROM print__contract_to_custumer
				INNER JOIN custumer__custumer USING (custumer_id)
				WHERE contract_id = " . $contract_id;
    $rs['buyers'] = Db::get_rows($query);
    // obtinc imatge
	$rs += UploadFiles::get_record_images($property_id, false,  'inmo', 'property');
	
    // calculo anys, mesos i dies
    // $rs += get_year_month_day($ipc,'ipc');
    // $rs += get_year_month_day($expires,'expires');
    $rs['ipc_color'] = $ipc > 10?'vert':'vermell';
    $rs['expires_color'] = $expires > 10?'vert':'vermell';

    return $rs;
}
function get_year_month_day($days, $prefix)
{
    $ret[$prefix . '_year'] = floor($days / 365);
    $rest = fmod($days, 365);
    $ret[$prefix . '_month'] = floor($rest / 30);
    $rest = fmod($rest, 30);
    $ret[$prefix . '_day'] = $rest;
    return $ret;
}
function write_record()
{
    // set_field('property', 'form_admin', 'input');
    $writerec = new SaveRows;
    $writerec->save();
    global $gl_action, $gl_insert_id, $gl_message;

    $contract_id = $gl_insert_id?$gl_insert_id:current($_POST['contract_id']);

    $query = "DELETE FROM print__contract_to_custumer where contract_id = '" . $contract_id . "'";
    Db::execute ($query);

    if ($_POST['selected_buyer_ids']) {
        $query = 'INSERT INTO print__contract_to_custumer (custumer_id, contract_id ) VALUES ';
        $custumers = explode(',', $_POST['selected_buyer_ids']);
        foreach ($custumers as $custumer_id) {
            $query .= "( " . $custumer_id . " , " . $contract_id . "),";
        }
        $query = substr($query, 0, -1) . ';';
        Db::execute($query);
    }

    $property_id = current($_POST['property_id']);
    $query = "DELETE FROM custumer__custumer_to_property where property_id = '" . $property_id . "'";
    Db::execute ($query);
    if (isset($_POST['selected_owner_ids']) && $_POST['selected_owner_ids']) {
        $query = 'INSERT INTO custumer__custumer_to_property (custumer_id, property_id ) VALUES ';
        $custumers = explode(',', $_POST['selected_owner_ids']);
        foreach ($custumers as $custumer_id) {
            $query .= "( " . $custumer_id . " , " . $property_id . "),";
        }
        $query = substr($query, 0, -1) . ';';
        Db::execute($query);
    }
    // insertar dates per puja ipc
    $signed = explode('/', current($_POST['signed']));
    $duration = current($_POST['duration']);
    $years = floor($duration / 12);

    $query = "DELETE FROM print__ipc where contract_id = '" . $contract_id . "'";
    Db::execute ($query);
    if ($years) {
        $query = 'INSERT INTO print__ipc (contract_id, rise, rised ) VALUES ';
        $custumers = explode(',', $_POST['selected_buyer_ids']);
        for ($y = 1; $y <= $years; $y++) {
            $rised = $rise = ($signed[2] + $y) . '-' . $signed[1] . '-' . $signed[0];
            $rise_checked = $signed[0] . '/' . $signed[1] . '/' . ($signed[2] + $y);
            $rised = isset($_POST['ipcs'][$rise_checked])?'1':'0';
            $query .= "( " . $contract_id . " , '" . $rise . "', " . $rised . "),";
        }
        $query = substr($query, 0, -1) . ';';
        Db::execute($query);
    }

    if ($gl_action == 'add_record') {
        $_SESSION['message'] = $gl_message;
        print_javascript('top.window.location=\'/admin/?action=show_form_edit&menu_id=207&contract_id=' . $gl_insert_id . '\'');
        die();
    }
}

function manage_images()
{
    $image_manager = new ImageManager;
    $image_manager->execute();
}

?>