<?
$query = "SELECT Distinct price_rank, portal__price_language.price_id
				FROM portal__price_language, portal__price
				WHERE portal__price_language.price_id = portal__price.price_id
				AND language = '" . LANGUAGE . "'
				";
$results = Db::get_rows($query);

$base_link = '/?tool=portal&tool_section=customer&language=' . LANGUAGE;  

$category_id = R::id('category_id');    
if($category_id){
	$base_link .='&category_id='.$category_id;						
}

// poso opcio de qualsevol preu
$l['link'] = $base_link; 
$l['id'] = 'null';
$l['selected'] = !isset($_GET['price_id'])?' selected':'';
$l['item'] = $gl_caption['c_any_price'];	

$loop[] = $l;
	
if ($results && count($results) > 1) {	
	foreach($results as $rs) {        		
        $l['link'] = $base_link . '&price_id=' . $rs['price_id'];		
		$l['item'] = $rs['price_rank'];
        $l['id'] = $rs['price_id'];
		
		if ((isset($_GET['price_id'])) AND ($rs['price_id']==$_GET['price_id'])) {				
			$l['selected']=' selected';		    
		}
		else{
			$l['selected']='';	
		}
        $loop[] = $l;
    }
}
$vars['select_name'] = 'price';
?>