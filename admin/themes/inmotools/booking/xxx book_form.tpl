
<link rel="stylesheet" type="text/css" href="/common/jscripts/submodal/subModal.css" />
<script type="text/javascript" src="/common/jscripts/calendar_res/calendario_res.js?v=<?=$version?>"></script>
<script type="text/javascript" src="/common/jscripts/submodal/common.js?v=<?=$version?>"></script>
<script type="text/javascript" src="/common/jscripts/submodal/subModal.js?v=<?=$version?>"></script>
<script type="text/javascript">
function show_form(booking_id){		
	showPopWin('?action=show_form_edit&template=clean&set_month=<?=$current_month?>&booking_id=' + booking_id, 670, 212, null, true, true, '');
}
function validar(obj){
	d_in_arr = obj.date_in.value.split('-');
	d_out_arr = obj.date_out.value.split('-');
	
	d = d_in_arr;
	d_in = new Date(d[0],d[1]-1,d[2]);
	d = d_out_arr;
	d_out = new Date(d[0],d[1]-1,d[2]);
	if (d_out>d_in) return true;	
	alert('La fecha de salida debe ser mayor a la fecha de entrada');
	return false;
}
</script>

<style type="text/css">
.reserves{
	font-family: Arial, Helvetica, sans-serif;
	font-size:12px;
	line-height:normal;
}
.alerta {
	font-size:16px;
	color: #0031a0;
}
.error {
	font-size:16px;
	color: #e50404;
}
.camp_form{
	color: #a14901;
}
.titol_form{
	font-size:14px;
	color: #000;
}
#calendari{
	overflow:auto;
	width: 800px;
	height:<?=$table_height?>px;
}
div.radios{
	font-size:12px;
	display:block; 
	float:left;
	width:100px;
	padding: 2px;
}
table.form_reserva {
	border: solid #999999 1px;
	margin-top:4px;
}
table.mes {
	border-right: solid #910202 1px;
}
table.mes tr td div.dia {
	font-family: Arial, Helvetica, sans-serif;
	font-size:7px;
	line-height: 7px;
	width: 22px;
}
div.nom_mes {
	font-family: Arial, Helvetica, sans-serif;
	padding-top: 10px;
	padding-bottom: 0px;
	height:12px;
	line-height: 12px;
	font-size:12px;
}
.day, .day_first_half, .day_last_half {
	background-color:#f5f5f5;
	border-top: solid #bbbbbb 1px;
	border-right: solid #bbbbbb 1px;
	height:21px;
	width: 21px;
}
.day_weekend, .day_weekend_first_half, .day_weekend_last_half {
	background-color:#b5b5b5;
	border-top: solid #bbbbbb 1px;
	border-right: solid #bbbbbb 1px;
	height:21px;
	width: 21px;
}
.day_first_half, .day_weekend_first_half {
	height:21px;
	width: 10px;
	float:left;
}
.day_last_half, .day_weekend_last_half {
	height:21px;
	width: 10px;
	float:right;
}
.day_mig, .day_first, .day_last {
	background-color:#e2a92f;
	cursor:pointer;
}
.day_weekend_mig, .day_weekend_first, .day_weekend_last {
	background-color:#c28c18;
	cursor:pointer;
}
.day_first, .day_weekend_first {
	border-top: solid #000 1px;
	border-bottom: solid #000 1px;
	border-left: solid #000 1px;
	height:20px;
	width: 10px;
	float:right;
}
.day_last, .day_weekend_last {
	border-top: solid #000 1px;
	border-bottom: solid #000 1px;
	border-right: solid #000 1px;
	height:20px;
	width: 10px;
	float:left;
}
.day_mig, .day_weekend_mig {
	border-top: solid #000 1px;
	border-bottom: solid #000 1px;
	height:20px;
	width: 22px;
}
-->
</style>
<? if ($show_booking):?>
<table border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td valign="top" width="100"><div class="nom_mes">&nbsp;</div><table border="0" cellspacing="0" cellpadding="0">
        <? foreach($propertys as $pr): extract ($pr)?>
          <tr>
            <td height="22" nowrap="nowrap"><strong class="camp_form"><?=$property?></strong></td>
          </tr>
          <? endforeach //$cases?>
      </table></td>
    <td><div id="calendari"><table border="0" cellspacing="0" cellpadding="0">
        <tr>
          <? foreach($calendar as $l): extract ($l)?>
            <td><div class="nom_mes"><?=$month?></div><table border="0" cellspacing="0" cellpadding="0" bgcolor="#999999" class="mes">
                <? foreach($propertys as $pr): extract ($pr)?>
                  <tr>
                    <? foreach($days as $d): extract ($d)?>
                      <td><div class="dia"><? if ($class2):?><div class="<?=$class1?>" <?=$onclick1?>><?=$day?></div><div class="<?=$class2?>" <?=$onclick2?>><?=$day?></div><? else:?><div class="<?=$class1?>" <?=$onclick1?>><?=$day?></div><? endif //?></div></td>
                      <? endforeach //$days?>
                  </tr>
                  <? endforeach //$cases?>
              </table></td>
            <? endforeach //$calendar?>
        </tr>
      </table></div></td>
  </tr>
  <tr>
    <td valign="top">&nbsp;</td>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="00">
      <tr>
        <td><a href="?set_month=<?=$previous_month?>">&lt; anterior</a></td>
        <td align="right"><a href="?set_month=<?=$next_month?>">siguiente &gt;</a></td>
      </tr>
    </table></td>
  </tr>
</table>
<table width="900" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td valign="top"><strong class="titol_form"><div style="height:30px;"></div>
        <?=$c_form_new?></strong><form id="form1" name="form1" method="POST" action="<?=$url?>" onSubmit="return validar(this)" target="save_frame">
        <table border="0" cellspacing="0" cellpadding="0" class="form_reserva">
    <tr>
      <td width="350" valign="top"><table border="0" cellspacing="0" cellpadding="5" width="350">
        <tr>
          <td align="left"><strong class="camp_form">Apartamento</strong></td>
          </tr>
        <tr>
          <td align="left"><? foreach($propertys as $l): extract ($l)?>
            <div class="radios">
              <input name="property_id" type="radio" value="<?=$property_id?>"<?=$checked?> />
              <?=$property?>
              </div>
            <? endforeach //$propertys?></td>
          </tr>
        </table></td>
      <td width="200" align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="4">
        <tr>
          <td><strong class="camp_form">Fecha de entrada</strong>
            <script>DateInput('date_in', true, 'YYYY-MM-DD', '<?=$today_form_date?>')</script></td>
          </tr>
        <tr>
          <td><strong class="camp_form">Fecha de salida</strong>
            <script>DateInput('date_out', true, 'YYYY-MM-DD', '<?=$tomorrow_form_date?>')</script></td>
          </tr>
        <tr>
          <td></td>
          </tr>
        <tr>
          <td><input type="submit" name="Submit" value="Hacer reserva" /></td>
          </tr>
        </table></td>
      </tr>
     <input type="hidden" name="set_month" value="<?=$current_month?>" />
    <input type="hidden" name="action" value="add_record">
      </table>
  </form></td>
    </tr>
  </table>
<? else: //?>
<style type="text/css">
body{
	padding-left:10px;
<? if (!$message):?>
	padding-top:20px;
<? endif //message?>
}

-->
</style>
<table border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><form id="form2" name="form1" method="post" action="<?=$url?>" onSubmit="return validar(this)" target="save_frame"><table border="0" cellspacing="0" cellpadding="0" class="form_reserva">
                <tr>
                  <td width="350" valign="top"><table border="0" cellspacing="0" cellpadding="5" width="350">
        <tr>
          <td align="left"><strong class="camp_form">Apartamento</strong></td>
          </tr>
        <tr>
          <td align="left"><? foreach($propertys as $l): extract ($l)?>
            <div class="radios">
               <input name="property_id" type="radio" value="<?=$property_id?>"<?=$checked?> />
              <?=$property?>
              </div>
            <? endforeach //$propertys?></td>
          </tr>
                  </table></td>
                  <td width="200" align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="4">
        <tr>
          <td><strong class="camp_form">Fecha de entrada</strong>
            <script>DateInput('date_in', true, 'YYYY-MM-DD', '<?=$date_in?>')</script></td>
          </tr>
        <tr>
          <td><strong class="camp_form">Fecha de salida</strong>
            <script>DateInput('date_out', true, 'YYYY-MM-DD', '<?=$date_out?>')</script></td>
          </tr>
                    <tr>
                      <td></td>
                    </tr>
                    <tr>
                      <td nowrap="nowrap"><input type="submit" name="save" value="Guardar cambios" />
                        <input type="submit" name="delete" value="Borrar reserva" /></td>
                    </tr>
                  </table></td>
                </tr>
                <input type="hidden" name="set_month" value="<?=$current_month?>" />
                <input type="hidden" name="action" value="save_record" />
    			<input type="hidden" name="booking_id" value="<?=$booking_id?>">
              
            </table></form></td>
  </tr>
</table>
<? endif // show_booking?>