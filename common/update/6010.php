<?

/* 01-7-14 
Modificacions portfoli
 Canvia "un a molts" a "molts a molts"
 */

if (is_table('portfoli__portfoli') && !is_table('portfoli__portfoli_to_category')) {
		
		
		$update_sql .= "
			CREATE TABLE `portfoli__portfoli_to_category` (
				`portfoli_category_id` INT(11) NOT NULL AUTO_INCREMENT,
				`portfoli_id` INT(11) NOT NULL DEFAULT '0',
				`category_id` INT(11) NOT NULL DEFAULT '0',
				PRIMARY KEY (`portfoli_category_id`)
			)
			COLLATE='utf8_spanish2_ci'
			ENGINE=MyISAM
			;";
		$update_sql .= "
			INSERT into portfoli__portfoli_to_category (portfoli_id,category_id) SELECT portfoli_id,category_id FROM portfoli__portfoli;";
		
		$update_sql .= "
			ALTER TABLE `portfoli__portfoli`
	DROP COLUMN `category_id`;";
	
	
}
else{	
	if (is_table('portfoli__portfoli')) {		
		trigger_error('Ja existeix la taula portfoli__portfoli_to_category');
	}
}

?>