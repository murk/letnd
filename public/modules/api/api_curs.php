<?
/**
 * ApiCurs
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) Agost 2017
 * @version $Id$
 * @access public
 */
class ApiCurs extends Module{
	var $is_past = false;
	function __construct(){
		parent::__construct();
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function get_records()
	{
		$this->set_var( 'is_past', $this->is_past );
	    $listing = new ListRecords($this);

		if (show_preview()){
			$listing->condition = "(status <> 'archived')";
		}
		elseif($this->is_past) {
			$listing->condition = "(status = 'public' || status = 'open' || status = 'full' || status = 'finished')
							AND ( end_date < now() )";
		}
		else {
			$listing->condition = "(status = 'public' || status = 'open' || status = 'full' || status = 'finished')
							AND ( end_date >= now() )";
		}

		
		$listing->order_by = 'ordre ASC, curs ASC';
		
		
		if ($this->parent == 'sitemap') {//Sitemap
			$listing->paginate = false;
			$listing->set_field('modified', 'type', 'none');
			$listing->set_records();
			return $listing->loop;
		}
		
	    return $listing->list_records();
	}

	function list_past() {
		$this->action = 'list_recors';
		$this->is_past = true;
		$this->list_records();
	}
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form() {

		$show = new ShowForm( $this );

		if ( show_preview() ) {
			// Al formulari es veuen tots, arxivades també
		} else {
			$show->condition = "(status = 'public' || status = 'open' || status = 'full' || status = 'finished')";
		}

		$show->get_values();

		Page::set_page_title( $show->rs['page_title'], $show->rs['curs'] );
		Page::set_page_description( $show->rs['page_description'], $show->rs['temari'] );
		Page::set_page_keywords( $show->rs['page_keywords'] );

		$content = $show->show_form();

		Page::set_page_og( $show->rs );

		return $content;
	}

}
?>