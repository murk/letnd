<? if(!$is_listing): ?>
	<div>
		<table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
			<tr valign="middle">
				<td width="20%" align="left" nowrap class="pageResults"><?= $split_results_count ?></td>
				<td align="right" nowrap class="pageResults"><?= $split_results ?></td>
			</tr>
		</table>
	</div>
<? endif //is_listing?>

<table width="95%" border="0" align="center" cellpadding="0" cellspacing="0" class="subListRecord">
	<? foreach($loop as $l): extract( $l ) ?>
		<tr class="listItem<?= $odd_even ?>" id="list_row_<?= $id ?>_demand_<?= $demand_id ?>_client_0">
			<td valign="top" class="listImage" width="<?= $im_admin_thumb_w ?>" style="cursor:pointer;" onclick="show_demand_property('<?= $id ?>','<?= $menu_id ?>','','<?= LANGUAGE ?>')">
				<? if($image_name): ?>
					<div><img width="<?= $im_admin_thumb_w ?>" src="<?= $image_src_admin_thumb ?>"></div>
				<? else: // image_name?>
					<div class="noimage" style="min-height:<?= $im_admin_thumb_h ?>px;"></div>
				<? endif // image_name?>
			</td>
			<td valign="top" class="custom-table-holder">
				<table class="icons-holder" border="0" cellspacing="0" cellpadding="0" class="noPadding">
					<tr>
						<td>
							<table border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td class="listTitle">
										<div class="demand-icons">
											<div title="<?= $c_send_information ?>" class="icon mailed<?= $mailed?'':' hidden-important' ?>"></div>
											<div title="<?= $c_like_property ?>" class="icon liked<?= $liked?'':' hidden-important' ?>"></div>
											<div title="<?= $c_demand_refused ?>" class="icon refused<?= $refused?'':' hidden-important' ?>"></div>
										</div>
										<strong>
											<input name="property_id[<?= $property_id ?>]" type="hidden" value="<?= $property_id ?>"/>
											<?= $ref ?>
											<span class="private"> -
												<?= $property_private ?>
                                            </span>
										</strong>
									</td>
									<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										<?= $entered ?></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td>
							<table width="100%" border="0" cellpadding="0" cellspacing="0" style="padding-right:8px">
								<tr>
									<td valign="top">
										<table width="100%" border="0" cellpadding="0" cellspacing="0" class="listDetails2">
											<tr>
												<td valign="top"><strong class="listItem"><?= $c_tipus ?>:</strong>&nbsp;
												</td>
												<td valign="top"><?= $tipus ?></td>
											</tr>
											<tr>
												<td valign="top"><strong class="listItem"><?= $c_tipus2 ?>:</strong>&nbsp;
												</td>
												<td valign="top"><?= $tipus2 ?></td>
											</tr>
											<td valign="top"><strong class="listItem"><?= $c_category ?>:</strong>&nbsp;
											</td>
											<td valign="top"><?= $category_id ?></td>
											</tr>
											<tr>
											<? if($floor_space): ?>
											<tr>
												<td valign="top"><strong class="listItem"><?= $c_floor_space ?>: </strong>&nbsp;</td>
												<td valign="top"><?= $floor_space ?></td>
											</tr>
											<? endif // $floor_space ?>
											<? if($land): ?>
												<tr>
													<td valign="top"><strong class="listItem"><?= $c_land ?>: </strong>&nbsp;</td>
													<td valign="top"><?= $land ?>
														&nbsp;<? if($land): ?><?= $land_units ?><? endif //land?></td>
												</tr>
											<? endif // $land ?>
											<? if($municipi_id): ?>
												<tr>
													<td valign="top"><strong class="listItem"><?= $c_municipi_id ?>: </strong>&nbsp;</td>
													<td valign="top"><?= $municipi_id ?></td>
												</tr>
											<? endif // $municipi_id ?>
											<tr>
												<td valign="top"><strong class="listItem"><?= $c_price ?>:</strong>&nbsp;
												</td>
												<td valign="top"><strong class="resaltatSmall"><?= $price ?></strong>
												</td>
											</tr>
										</table>
									</td>
									<td valign="top" class="padding-left-2">
										<table width="100%" border="0" cellpadding="0" cellspacing="0" class="listDetails2">

											<?= demand_get_rows_details ( $this, $l ) ?>

										</table>
									</td>
								</tr>
							</table>
							<table width="100%" border="0" cellspacing="8" cellpadding="0">
								<tr>
									<td align="right">
										<div class="right" id="demand-change-<?= $id ?>-<?= $demand_id ?>-0">

											<? foreach($buttons as $button): ?>
												<?= $button ?>
											<? endforeach //$buttons?>

											<a class="btn_form_save btn_like<?= $liked?' hidden-important':'' ?>" onclick="demand_change_property(<?= $demand_id ?>,<?= $custumer_id ?>, <?= $property_id ?>,0,'like_property', this)"><?= $c_interest_property ?></a>

											<a class="btn_form_delete btn_refuse<?= $refused?' hidden-important':'' ?>" onclick="demand_change_property(<?= $demand_id ?>,<?= $custumer_id ?>, <?= $property_id ?>,0,'refuse_property', this)"><?= $c_refuse_property ?></a>

											<a class="boto1 btn_reset<?= $liked || $refused?'':' hidden-important' ?>" onclick="demand_change_property(<?= $demand_id ?>,<?= $custumer_id ?>, <?= $property_id ?>,0,'reset_property', this)"><?= $c_reset_property ?></a>

											<a class="boto1" href="javascript:show_demand_property('<?= $id ?>','<?= $menu_id ?>','','<?= LANGUAGE ?>')"><?= $c_view_property ?></a>
										</div>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	<? endforeach //$loop?>
</table>

<script>
	var gl_caption =
	{
		'c_refuse_property_done': '<?=addslashes($c_refuse_property_done)?>',
		'c_reset_property_done': '<?=addslashes($c_reset_property_done)?>',
		'c_like_property_done': '<?=addslashes($c_like_property_done)?>',
		'c_history': '<?=addslashes($c_history)?>',
		'c_history_title': '<?=addslashes($c_history_title)?>'
	};
</script>


<? if($show_more_results): ?>
	<table style="margin-top:20px;" width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
		<tr valign="middle">
			<td align="left"><a class="botoOpcions1" href="<?= $show_more_link ?>"><?= $c_show_all_results ?></a></td>
		</tr>
	</table>
<? endif //show_more_results?>
