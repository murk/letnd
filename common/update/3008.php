<?php
// no es poden posar ; entre mig de la consulta ja que parteix les consultes per el, a no ser que fem servir $update_sql en array() ;

if (is_table('product__product')){
	$update_sql .= "
	CREATE TABLE IF NOT EXISTS `product__variation_category` (
	  `variation_category_id` int(11) NOT NULL auto_increment,
	  PRIMARY KEY  (`variation_category_id`)
	) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;
	CREATE TABLE IF NOT EXISTS `product__variation_category_language` (
	  `variation_category_id` int(11) NOT NULL,
	  `language` char(3) collate utf8_spanish2_ci NOT NULL default '',
	  `variation_category` varchar(200) collate utf8_spanish2_ci NOT NULL,
	  `variation_description` varchar(255) collate utf8_spanish2_ci NOT NULL
	) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;";
	
	if (!is_field('product__variation','variation_category_id')){
		$update_sql .="ALTER TABLE `product__variation` ADD `variation_category_id` INT( 11 ) NOT NULL AFTER `variation_id`;
						ALTER TABLE `product__variation` ADD `variation_pvp` DECIMAL( 11, 2 ) NOT NULL AFTER `variation_category_id` ,
														ADD `variation_pvd` DECIMAL( 11, 2 ) NOT NULL AFTER `variation_pvp`;
						ALTER TABLE `product__variation` ADD `variation_default` tinyint( 1 ) NOT NULL DEFAULT '0' AFTER `variation_pvd`;
						ALTER TABLE `product__variation` ADD `variation_checked` tinyint( 1 ) NOT NULL DEFAULT '0' AFTER `variation_pvd`;";
	}

	if (!Db::get_first("SELECT count(*) FROM all__menu WHERE menu_id = '20064'")){
		$update_sql .="
			INSERT INTO `all__menu` (`menu_id`, `menu_group`, `tool`, `tool_section`, `action`, `parent_id`, `toolmode_id`, `variable`, `ordre`, `link`, `process`, `bin`, `read1`, `write1`) VALUES
			(20064, 'product', 'product', 'variation_category', 'show_form_new', 20060, 1, 'PRODUCT_MENU_VARIATION_CATEGORY_NEW', 3, '', '', 0, ',1,', ',1,'),
			(20065, 'product', 'product', 'variation_category', 'list_records', 20060, 1, 'PRODUCT_MENU_VARIATION_CATEGORY_LIST', 4, '', '', 0, ',1,', ',1,');
			UPDATE `all__menu` SET `ordre` = '5' WHERE `all__menu`.`menu_id` =20063;";
	}
	if (!is_field('product__orderitem','product_variations')){
		$update_sql .="
			ALTER TABLE `product__orderitem` ADD `product_variations` VARCHAR( 600 ) NOT NULL AFTER `product_title`;
			ALTER TABLE `product__orderitem` ADD `product_variation_categorys` VARCHAR( 600 ) NOT NULL AFTER `product_variations`;
			ALTER TABLE `product__orderitem` ADD `product_variation_ids` VARCHAR( 300 ) NOT NULL AFTER `product_title`;";
	}
	if (!Db::get_first("SELECT count(*) FROM product__configadmin WHERE name='insert_default_variations'")){
		$update_sql .="
			INSERT INTO `product__configadmin` (`name`, `value`) VALUES ('insert_default_variations', '0');";	
	}
	$update_sql .="
	ALTER TABLE `product__product` CHANGE `ref` `ref` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL DEFAULT '';";
}

if (is_table('news__new')){
	if (!is_field('news__new','videoframe')){
		$update_sql .="ALTER TABLE `news__new` ADD `videoframe` TEXT NOT NULL AFTER `url2`;";
	}
	$update_sql .=
		"
	   CREATE TABLE IF NOT EXISTS `news__new_file` (
	  `file_id` int(11) NOT NULL auto_increment,
	  `new_id` int(11) NOT NULL,
	  `name` varchar(100) collate utf8_spanish2_ci NOT NULL,
	  `size` int(11) NOT NULL,
	  `ordre` int(11) NOT NULL,
	  `main_file` int(1) NOT NULL,
	  PRIMARY KEY  (`file_id`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci AUTO_INCREMENT=1;
		";
}
?>