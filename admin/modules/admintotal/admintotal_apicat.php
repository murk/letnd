<?
/**
 * APICAT
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) Gener 2016
 * @version 1
 * @access public
 */
/*
FEATURES


 *
 DEMANDES
 *

 CUSTOMERS
 *

 PROPERTY
 *

PER FER

PER ELLS


CANVIS EN BBDD


LETND NO TE

	Categories de categories d'inmobles
	ContratoExclusividad
	FechaFinExclusiva
	PrecioTrapaso

APICAT NO TE




VARS

*/

class AdmintotalApicat extends Module {

	var $config_rs = array();
	// var $export_results = array();
	var $export_rs = array();
	var $property_rs = array();
	var $agencia_rs = array();
	var $client_images_url = '';
	var $conta = 0;
	var $is_cli = false;

	//var $inmofactory_table = 0;
	//var $letnd_id = 0;
	//var $demand_id = 0;
	//var $demand_tipus = '';
	//var $propertys_added = array();
	//var $errors = array();
	// el key es l'id a inmofactory, i el val l'id a letnd

    // TODO-i  Posar amb BBDD igual que a l-import
	var $apicat_categorys_category = array(

		'1'  => 'Casa', // Masia - Rústica
		'2'  => 'Casa', // Casa de poble
		'3'  => 'Casa', // Casa urbana
		'4'  => 'Vivienda', // Pis
		'5'  => 'Suelo', // Terreny
		'7'  => 'Local', // Local
		'8'  => 'Casa', // Obra nova
		'10' => 'Parking', // Pàrquing
		'11' => 'Otro', // Amarratge
		'12' => 'Vivienda', // Apartament
		'13' => 'Casa', // Xalet
		'14' => 'Vivienda', // Àtic
		'15' => 'Vivienda', // Dúplex
		'16' => 'Vivienda', // Loft
		'17' => 'Industrial', // Nau
		'18' => 'Vivienda', // Protecció oficial
		'19' => 'Casa', // Ruïna
		'20' => 'Otro', // Negoci
		'21' => 'Casa', // Finca exclusiva
		'22' => 'Casa', // Casa adosada
		'23' => 'Casa', // Casa aparellada
		'24' => 'Casa', // Casa unifamiliar
		'25' => 'Vivienda', // Pis planta baixa
		'26' => 'Parking', // Garatge
		'27' => 'Edificio', // Edifici

	);

	/*			<TipoGenerico> Vivienda</>
			.. Piso
			.. Apartamento
			.. Ático
			.. Loft
			• Estudio
			.. Dúplex
			• Habitación
			• Buhardilla
			• Otro
			<TipoGenerico> Casa</>
			• Bungalow
			.. Casa rural
			.. Unifamiliar adosada
			.. Unifamiliar aislada
			.. Masia
			.. Chalet / Torre
			• Otro
			• Buhardilla
			<TipoGenerico> Local</>
			.. Local comercial
			• Tienda
			• Otro
			<TipoGenerico> Oficina</>
			• Oficina
			• Despacho
			• Otro
			<TipoGenerico> Edificio</>
			• Propiedad vertical
			.. Propiedad
			<TipoGenerico> Suelo</>
			• Suelo rústico
			.. Suelo urbano
			• Otro
			• Terciario
			<TipoGenerico>Industrial</>
			.. Nave industrial
			• Local
			• Otro
			<TipoGenerico>Parking</>
			• Negocio de aparcamiento
			.. Plaza de aparcamiento
			• Otro
			<TipoGenerico>Hotel</>
			• Hotel
			• Balneario
			• Hostal
			<TipoGenerico>Otro</>*/
	var $apicat_categorys = array(

		'1'  => 'Masia', // Masia - Rústica
		'2'  => 'Casa rural', // Casa de poble
		'3'  => 'Chalet / Torre', // Casa urbana
		'4'  => 'Piso', // Pis
		'5'  => 'Suelo urbano', // Terreny
		'7'  => 'Local comercial', // Local
		'8'  => 'Otro', // Obra nova
		'10' => 'Plaza de aparcamiento', // Pàrquing
		'11' => 'Otro', // Amarratge
		'12' => 'Apartamento', // Apartament
		'13' => 'Chalet / Torre', // Xalet
		'14' => 'Ático', // Àtic
		'15' => 'Dúplex', // Dúplex
		'16' => 'Loft', // Loft
		'17' => 'Nave industrial', // Nau
		'18' => 'Piso', // Protecció oficial
		'19' => 'Masia', // Ruïna
		'20' => 'Otro', // Negoci
		'21' => 'Masia', // Finca exclusiva
		'22' => 'Unifamiliar adosada', // Casa adosada
		'23' => 'Unifamiliar adosada', // Casa aparellada
		'24' => 'Unifamiliar aislada', // Casa unifamiliar
		'25' => 'Piso', // Pis planta baixa
		'26' => 'Plaza de aparcamiento', // Garatge
		'27' => 'Propiedad', // Edifici

	);
	var $apicat_anunci_languages = array(
		'spa' => '',
		'cat' => 'CAT',
		'eng' => 'ENG',
		'fra' => 'FR',
		'deu' => 'GER',
		'ita' => 'ITA',
		'por' => 'PT',
	);
	var $apicat_facings = array(
		'e'  => 'Este',
		'o'  => 'Oeste',
		's'  => 'Sur',
		'se' => 'Sur-Este',
		'so' => 'Sur-Oeste',
		'n'  => 'Norte',
		'ne' => 'Nord-Este',
		'no' => 'Nord-Oeste',
	);

	var $apicat_floors = array(
		// '' => 'Baldosa',
		// '' => 'Terrazo',
		'marble'    => 'Mármol',
		// '' => 'Registrable',
		// '' => 'Cerámico',
		'ceramic'   => 'Ceramico',
		// '' => 'Pulido',
		// '' => 'Otros',
		'parket'    => 'Parquet',
		'laminated' => 'Parquet',
		// '' => 'Mixto',
		// '' => 'Madera',
		'gres'      => 'Gres',
	);

	var $apicat_views = array(
		// '' => 'Montaña/valle',
		// '' => 'Puerto',
		// '' => 'Zona deportiva',
		// '' => 'Lago',
		// '' => 'Calle',
		// '' => 'Patio interior manzana',
		// '' => 'Naturaleza',
		// '' => 'Zona comunitaria',
		// '' => 'Sin vistas',
		// '' => 'Zona verde',
		// '' => 'Ciudad',
		'sea_view'   => 'Mar',
		// '' => 'Montaña',
		// '' => 'Calle',
		// '' => 'Patio interior',
		// '' => 'Otros',
		'clear_view' => 'Buenas vistas',
		// '' => 'Zona deportiva',
	);

	function __construct() {

		include_once( DOCUMENT_ROOT . 'admin/modules/admintotal/export_common.php' );
		parent::__construct();
	}

	function _start() {

		Debug::add(date( "d-m-Y H:i" ),"");
		
		set_time_limit( 60 * 60 );        //ini_set('implicit_flush', true);		//error_reporting(E_WARNING);

		echo '<link href="/admin/themes/inmotools/styles/export.css" rel="stylesheet" type="text/css">';

		Db::connect_mother();

		$agencias = $this->get_agencias();
		$this->write_agencias_xml( $agencias );

		$this->export_properties( $agencias );

		$this->upload_files();

		$this->show_end();

		send_mail_admintotal('APICAT - Exportació finalitzada', 'Data: ' . now(true));

		die();
	}

	function get_config_results() {


		$query = "SELECT apicat_field, letnd_table, letnd_field, `function`, `condition`
							FROM export__apicat
							WHERE apicat_table <>  ''";

		return Db::get_rows( $query );

	}

	function get_agencias() {

		$query    = "SELECT client_id, comercialname, nif, inmo_apicat_code, inmo_apicat_dni, province, town, typestreet, adress, numstreet, zip, respname, surnames, phone1, fax, mail1, domain, dbname, client_dir FROM letnd.client__client WHERE inmo_apicat = 1";
		$agencias = Db::get_rows( $query );

		//Debug::p( $results, 'Clients' );

		return $agencias;

	}


	function export_properties( $agencias ) {

		// TEST
		/*for ( $i = 0; $i < 5; $i ++ ) {
			$agencias = array_merge( $agencias, $agencias );
		}*/
		$xml          = $this->get_XMLWRITER( 'INMUEBLES_MODIFICADOS', 'inmuebles' );
		$xml_borrados = $this->get_XMLWRITER( 'INMUEBLES_BORRADOS', 'ArrayOfAnyType' );

		foreach ( $agencias as $rs ) {
			$this->client_images_url = 'http://' . $rs['domain'] . '/' . $rs['client_dir'] . '/inmo/property/images/';

			$this->agencia_rs =  &$rs;

			$this->export_agencia_properties( $xml, $xml_borrados );
		}

		$xml->endElement();
		$xml->flush();
		unset( $xml );

		$xml_borrados->endElement();
		$xml_borrados->flush();
		unset( $xml_borrados );
	}

	private function export_agencia_properties( &$xml, &$xml_borrados ) {

		$this->get_agencia_config();

		$condition = "(status = 'onsale' || status = 'reserved')
				 AND bin = 0
				 AND country_id = '1'
				 AND price > 0
				 AND price_consult = 0
                 AND (
                        zip != '' 
                        || 
                        (
                            show_map = 1 &&
                            show_map_point = 1 &&
                            latitude != 0.0000000 &&
                            longitude != 0.0000000
                        )
                    )
				 AND tipus != 'temp'";

		if (!$this->agencia_rs['apicat_export_all'] ){
			$condition .= "
				AND property_id IN (
					SELECT property_id
					FROM `" . $this->agencia_rs['dbname'] . "`.inmo__property_to_portal
					WHERE apicat = 1
				)
			";
		}

		$this->p( "Consulta immobles: " . $this->agencia_rs['comercialname'], 1 );

		$this->export_borrados( $xml_borrados, $condition ); // primer perquè canvia dates
		$this->export_modificados( $xml, $condition );
	}

	private function export_modificados( &$xml, $condition ) {

		$this->p( "Extracció d'inmobles: " . $this->agencia_rs['comercialname'], 1 );
		$this->p( "Exportar tots: " . ($this->agencia_rs['apicat_export_all']?'Sí':'No'), 3 );

		$query   = "
			SELECT * FROM `" . $this->agencia_rs['dbname'] . "`.inmo__property
			WHERE " . $condition;
		$results = Db::get_rows( $query );


		$config_results = $this->get_config_results();

		foreach ( $results as $key => $rs ) {

			$this->property_rs = &$rs;
			$this->export_rs   = array();

			$dates = Db::get_row(
				"SELECT DATE(entered) AS entered, modified
					FROM `letnd`.`export__apicat_property`
					WHERE property_id = " . $rs['property_id'] . "
					AND client_id = " . $this->agencia_rs['client_id'] );
			// Altres Valors
			$this->export_rs['IdAgencia']         = $this->agencia_rs['client_id'];
			$this->export_rs['IdFicha']         = $this->agencia_rs['client_id'] . '_' . $rs['property_id'];
			$this->export_rs['FechaAlta']         = $dates['entered'];
			$this->export_rs['FechaModificacion'] = $dates['modified'];
			// TODO-i Falta posar comunitats, de moment nomes catalunya
			$this->export_rs['Estado']    = 'Disponible'; // Disponibilitat, de moment només té aquest valor
			$this->export_rs['Pais']      = 'España'; // Valor nomes españa
			$this->export_rs['Comunidad'] = 'Catalunya'; // Valor nomes españa
			$this->get_anunci();
			$this->export_rs['Caracteristicas'] = array();
			$this->get_images();

			foreach ( $config_results as $config_rs ) {
				$this->config_rs = &$config_rs;
				if ( $config_rs['function'] ) {
					$val = $this->{$config_rs['function']}( $rs[ $config_rs['letnd_field'] ] );
					if ( $val !== false ) {
						$this->export_rs[ $config_rs['apicat_field'] ] = $val;
					}
				} else {
					$this->export_rs[ $config_rs['apicat_field'] ] = $rs[ $config_rs['letnd_field'] ];
				}
			}
			$this->config_rs = array();

			$this->write_modificados_xml( $xml );

			$this->p( "Ref: {$rs['ref']} , id: {$rs['property_id']}", 2 );
		}

		$this->property_rs = array();
		$this->export_rs   = array();

	}


	function export_borrados( &$xml_borrados, $condition ) {

		$table = '`' . $this->agencia_rs['dbname'] . '`.inmo__property';

		// Afegeixo els que falten
		$query =
			"INSERT INTO `letnd`.`export__apicat_property`
				(`property_id`, `client_id`)
				SELECT property_id, '" . $this->agencia_rs['client_id'] . "'
					FROM " . $table . "
					WHERE " . $condition . "
					AND property_id NOT IN (
						SELECT property_id
						FROM `letnd`.`export__apicat_property`
						WHERE client_id = " . $this->agencia_rs['client_id'] . "
					)";
		Db::execute( $query );

		// Marco tots els que exportem com a no borrats i poso la data modificacio
		$query =
			"UPDATE `letnd`.`export__apicat_property`
				SET deleted = 0, modified=now()
				WHERE property_id IN (
					SELECT property_id
					FROM " . $table . "
					WHERE " . $condition . "
				)
				AND client_id = " . $this->agencia_rs['client_id'];
		Db::execute( $query );

		// Marco tota la resta que no son els que exportem com a borrats
		$query =
			"UPDATE `letnd`.`export__apicat_property`
				SET deleted = 1
				WHERE property_id NOT IN (
					SELECT property_id
					FROM " . $table . "
					WHERE " . $condition . "
					)
				AND client_id = " . $this->agencia_rs['client_id'];
		Db::execute( $query );

		// Agafo els borrats per fer l'XML
		$query = "SELECT property_id
						FROM `letnd`.`export__apicat_property`
						WHERE client_id = " . $this->agencia_rs['client_id'] . "
						AND deleted =1";

		$results = Db::get_rows( $query );
		foreach ( $results as $rs ) {
			$this->write_borrados_xml( $xml_borrados, $rs['property_id'] );
		}

	}

	function write_agencias_xml( $agencias ) {

		$xml = $this->get_XMLWriter( 'AGENCIAS', 'Agencias' );

		foreach ( $agencias as $rs ) {
			$xml->startElement( 'Agencia' );

			$xml->writeElement( 'IdAgencia', $rs['client_id'] );
			$xml->writeElement( 'Nombre', $rs['comercialname'] );
			$xml->writeElement( 'DNI', $rs['inmo_apicat_dni'] );
			$xml->writeElement( 'CodigoColegiado', $rs['inmo_apicat_code'] );
			$xml->writeElement( 'Provincia', $rs['province'] );
			$xml->writeElement( 'Localidad', $rs['town'] );
			$xml->writeElement( 'TipoVia', $rs['typestreet'] );
			$xml->writeElement( 'NombreVia', $rs['adress'] );
			$xml->writeElement( 'NumeroVia', $rs['numstreet'] );
			$xml->writeElement( 'CodigoPostal', $rs['zip'] );
			$xml->writeElement( 'PersonaContacto', $rs['respname'] . ' ' . $rs['surnames'] );
			$xml->writeElement( 'Telefono', $rs['phone1'] );
			$xml->writeElement( 'Fax', $rs['fax'] );
			$xml->writeElement( 'Email', $rs['mail1'] );
			$xml->writeElement( 'PaginaWeb', $rs['domain'] );

			$xml->endElement();
		}
		$xml->endElement();
		$xml->flush();
		unset( $xml );

	}

	function write_borrados_xml( &$xml, $property_id ) {

		$client_id = $this->agencia_rs['client_id'];

		$xml->startElement( 'anyType' );
		$xml->writeAttribute( 'xsi:type', "xsd:string" );
		$xml->text( $client_id . '_' . $property_id );

		$xml->endElement();
		if ( $this->conta % 1000 == 0 && $this->conta != 0 ) {
			$xml->flush();
			$this->p( 'FLUSH', 1 );
		}
	}

	function write_modificados_xml( &$xml ) {

		$xml->startElement( 'inmueble' );

		foreach ( $this->export_rs as $k => $v ) {
			if ( $k == 'Caracteristicas' ) {
				if ( $v ) {
					$xml->startElement( 'Caracteristicas' );
					foreach ( $v as $ca ) {
						$xml->startElement( 'Caracteristica' );
						$xml->writeElement( 'Descripcion', $ca['Descripcion'] );
						$xml->writeElement( 'Valor', $ca['Valor'] ? $ca['Valor'] : null );

						$xml->endElement();
					}
					$xml->endElement();

				}
			} elseif ( $k == 'Imagenes' ) {
				$xml->startElement( 'Imagenes' );
				foreach ( $v as $im ) {
					$xml->startElement( 'foto' );
					$xml->writeElement( 'Nombre', $im['Nombre'] ? $im['Nombre'] : null );
					$xml->writeElement( 'url', $im['url'] );
					$xml->endElement();
				}
				$xml->endElement();
			} else {
				// Write element fa htmlentities i cambia salts de linea per &#13;
				// text també fa hmlspecialchars ???
				// $xml->writeElement( $k, $v ? $v : null );
				/*$xml->startElement( $k );
				if ($v)
					$xml->text( $v );
				$xml->endElement();*/

				$xml->startElement( $k );
				if ($v){
					// $v = htmlspecialchars( $v, ENT_COMPAT);

					// Anuncio ja te el cdata
					if (substr($k,0,7) == 'Anuncio'){

					}
					else {
						$v = str_replace('"', '‘', $v);
						$v = str_replace("'", '‘', $v);
						$v = str_replace('&', '&amp;', $v);
					}

					$xml->writeRaw( $v );
				}
				$xml->endElement();

			}
		}

		$xml->endElement();
		if ( $this->conta % 1000 == 0 && $this->conta != 0 ) {
			$xml->flush();
			$this->p( 'FLUSH', 1 );
		}
	}

	function get_XMLWriter( $file, $element ) {
		global $gl_is_local;
		$xml = new XMLWriter();
		$xml->openUri( ($gl_is_local ? DOCUMENT_ROOT : "/var/www/letnd.com/datos/motor/") . 'data/apicat/' . $file . '.xml' );
		$xml->startDocument( '1.0', 'UTF-8' );
		//if ( $GLOBALS['gl_is_local'] ) { // És més fàcil per depurar
			$xml->setIndent( true );
			$xml->setIndentString( "\t" );
		//}
		$xml->startElement( $element );
		$xml->writeAttribute( 'xmlns:xsi', "http://www.w3.org/2001/XMLSchema-instance" );
		$xml->writeAttribute( 'xmlns:xsd', "http://www.w3.org/2001/XMLSchema" );

		return $xml;

	}


	function show_end() {
		$this->p( 'Exportació finalitzada  - <i>' . $this->conta . ' operacions en ' . $execution_time = Debug::get_execution_time() . ' segons </i>', 1 );
		echo "
		<script>
			a = false;
		</script></body></html>";
	}

	private function get_agencia_config() {
		$apicat_export_all = Db::get_first( "SELECT value FROM `" . $this->agencia_rs['dbname'] . "`.inmo__configadmin WHERE name = 'apicat_export_all'" );

		$this->agencia_rs['apicat_export_all'] = $apicat_export_all;
	}

	// Funcions per camps
	function get_category( $val ) {

		if ( ! $val ) {
			$this->export_rs ['TipoEspecifico'] = 'Otro';

			return 'Otro';
		}

		$this->export_rs ['TipoEspecifico'] = $this->apicat_categorys [ $val ];

		return $this->apicat_categorys_category [ $val ];
	}

	function get_anunci() {

		$id    = $this->property_rs['property_id'];
		$query = "SELECT
				language, description FROM `" . $this->agencia_rs['dbname'] . "`.inmo__property_language
			 WHERE property_id = '" . $id . "'";

		$results = Db::get_rows( $query );

		foreach ( $results as $rs ) {
			$apicat_language = isset( $this->apicat_anunci_languages[ $rs['language'] ] ) ? $this->apicat_anunci_languages[ $rs['language'] ] : false;
			if ( $apicat_language !== false && $rs['description'] ) {
				$this->export_rs [ 'Anuncio' . $apicat_language ] = '<![CDATA[' . $rs['description'] . ']]>';
			}
		}
	}

	function get_images() {

		$id     = $this->property_rs['property_id'];
		$images = Db::get_rows(
			"SELECT
				image_id, name
				FROM `" . $this->agencia_rs['dbname'] . "`.inmo__property_image
				WHERE property_id = " . $id . "
				ORDER BY main_image DESC, ordre");

		if ( $images ) {
			$this->export_rs ['Imagenes'] = array();

			foreach ( $images as $image ) {


				$image = $image['image_id'] . '_medium' . strrchr(substr($image['name'], -5, 5), '.');

				$this->export_rs ['Imagenes'][] = array(
				    /*
					TODO De moment no es mostra a apicat, no cal, Poso alt? normalment es fa per posicionar, el text pot ser una mica raro $image
				     */
					'Nombre' => '',
					'url'    => $this->client_images_url . $image
				);
			}

		}
	}

	function get_full_address( $val ) {

		$full_adress = '';
		$rs          = &$this->property_rs;

		// Nomes mostro l'adreça si el client vol mostrar també el punt del mapa y el mapa
		if ( !$rs['show_map_point'] || !$rs['show_map']  )
			return false;

		if ( $rs['adress'] ) {
			$full_adress .= $rs['adress'];
		}
		if ( $rs['numstreet'] ) {
			$full_adress .= ', ' . $rs['numstreet'];
		}

		if ( $rs['block'] ) {
			$full_adress .= ', ' . $rs['block'];
		}
		if ( $rs['flat'] != '' || $rs['door'] ) {
			$full_adress .= ', ' . $rs['flat'] . '-' . $rs['door'];
		}

		return $full_adress ? $full_adress : false;
	}

	function get_price( $val ) {
		if ( ! $val || $this->property_rs['price_consult'] ) {
			return false;
		}

		switch ( $this->property_rs['tipus'] ) {
			case 'sell':
				$this->export_rs ['PrecioVenta'] = $val;
				break;
			case 'rent':
			case 'temp':
			case 'moblat':
			case 'selloption':
				$this->export_rs ['PrecioAlquiler'] = $val;
				break;

			// $this->export_rs ['PrecioTraspaso'] = $val;
		}

		return false;
	}

	function get_tipus2( $val ) {
		return ( $val == 'new' ) ? 'Si' : 'No';
	}

	function get_provincia( $val ) {
		$val = Db::get_first( "SELECT provincia FROM `" . $this->agencia_rs['dbname'] . "`.inmo__provincia WHERE provincia_id = " . $val );

		return $val;
	}

	function get_municipi( $val ) {
		$val = Db::get_first( "SELECT municipi FROM `" . $this->agencia_rs['dbname'] . "`.inmo__municipi WHERE municipi_id = " . $val );

		return $val;
	}

	function get_zone( $val ) {
		$val = Db::get_first( "SELECT zone FROM `" . $this->agencia_rs['dbname'] . "`.inmo__zone WHERE zone_id = " . $val );

		return $val ? $val : false;
	}

	function get_latitude_longitud( $val ) {
		if ( ! $this->property_rs['show_map_point'] || ! $val || $val == '0.0000000' ) {
			return false;
		} else {
			return $val;
		}
	}

	function get_facing( $val ) {

		if ( ! $val ) {
			return false;
		}
		$val = $this->apicat_facings[ $val ];

		return $this->get_caracteristica( $val );
	}

	function get_parking( $val ) {

		$garage = $this->property_rs['garage'];

		if ( ! $val && ! $garage ) {
			return false;
		}
		$num = 0;
		if ( $val ) {
			$num ++;
		}
		if ( $garage ) {
			$num ++;
		}

		return $this->get_caracteristica( $num );
	}

	// Nomes es seleciona el primer
	function get_floor_type() {
		foreach ( $this->apicat_floors as $k => $v ) {
			if ( $this->property_rs[ $k ] ) {
				return $this->get_caracteristica( $v );
			}
		}

		return false;
	}

	// Nomes es seleciona el primer
	function get_view( $val ) {
		foreach ( $this->apicat_views as $k => $v ) {
			if ( $this->property_rs[ $k ] ) {
				return $this->get_caracteristica( $v );
			}
		}

		return false;
	}


	function get_cedula( $val ) {

		// Només a importar, per exportar es necessita la data
		return false;

	}


	function get_date( $val ) {

		if ( $val == '0000-00-00' ) {
			return false;
		}

		return $this->get_caracteristica( $val );
	}


	function get_construction( $val ) {

		if ( $val == '0000-00-00' ) {
			return false;
		}
		$val = substr( $val, 0, 4 );

		return $this->get_caracteristica( $val );
	}

	// SI es un terreny, la superfície pot ser "land"
	function get_floor_space( $val ) {

		if (!$val && $this->property_rs['category_id'] == '5' ) {
			$val = $this->property_rs['land'];
		}
		return $this->get_not_required ($val);

	}

	// Genériques
	function get_not_required( $val ) {
		return $val ? $val : false;
	}

	function get_boolean_not_required( $val ) {
		return ( $val ) ? 'Si' : false;
	}

	function get_caracteristica_boolean( $val ) {
		if ( $val ) {
			$val = 'Si';
		}

		return $this->get_caracteristica( $val );
	}

	function get_caracteristica( $val ) {

		if ( ! $val ) {
			return false;
		}
		if ( $this->config_rs['condition'] ) {
			$condition = eval( "return " . $this->config_rs['condition'] . ";" );
			// Debug::p( $condition, $this->config_rs['condition'] . ' - ' . $val);
			if ( ! $condition ) {
				return false;
			}
			// Debug::p( 'Ha passat condition' );
		}

		$this->export_rs['Caracteristicas'][] = array(
			'Descripcion' => $this->config_rs['apicat_field'],
			'Valor'       => $val
		);

		return false;
	}

	function get_ref( $ref ){
		return ExportCommon::get_ref( $ref, $this->property_rs, $this->agencia_rs );
	}


	// FTP
	function upload_files() {
		global $gl_ftp_apicat, $gl_is_local;

		if ( $gl_is_local ) {
			$local_dir  = DOCUMENT_ROOT . 'data/apicat/';
			$remote_dir = '/letnd.com/datos/motor/data/apicat/proves/';
		} else {
			$local_dir  = '/var/www/letnd.com/datos/motor/data/apicat/';
			$remote_dir = '';
		}

		$zip_name = date( "dmY" ) . '_letnd.zip';
		$this->p( "Crear zip i pujar per ftp: " . $local_dir . $zip_name, 1 );

		$zip      = new ZipArchive();
		$zip->open( $local_dir . $zip_name, ZipArchive::CREATE | ZipArchive::OVERWRITE );


		$files = array(
			'AGENCIAS.xml',
			'INMUEBLES_MODIFICADOS.xml',
			'INMUEBLES_BORRADOS.xml'
		);

		foreach ( $files as $file ) {

			$zip->addFile( $local_dir . $file, $file );
			$this->p( $local_dir . $file, 2 );

		}
		$zip->close();
		$this->p( "Arxiu zip creat: " . $local_dir . $zip_name, 2 );

		// if ( $gl_is_local )
			// return; // no pujo si es local

		// Pujo per ftp
		$conn_id      = ftp_connect( $gl_ftp_apicat['server'] );
		$login_result = ftp_login( $conn_id, $gl_ftp_apicat['user'], $gl_ftp_apicat['password'] );
		// turn passive mode on
		ftp_pasv( $conn_id, true );
		if ( ftp_put( $conn_id, $remote_dir . $zip_name, $local_dir . $zip_name, FTP_BINARY ) ) {
			$this->p( "Pujat correctament: $zip_name\n", 2 );
		} else {
			$this->p( "NO s'ha pogut pujar: $zip_name\n", 2 );
			send_mail_admintotal( 'APICAT, a fallat la transferencia d\'arxius', '' );
		}
		ftp_close( $conn_id );

		$this->clean_files( $local_dir );

	}

	function clean_files( $local_dir ) {
		$files = glob( $local_dir . '*.zip');
		// natsort($files);

		array_multisort(array_map('filemtime', $files), SORT_NUMERIC, SORT_DESC, $files);

		// Deixo els 5 arxius més nous
		$conta = 0;
		foreach ( $files as $file ) {
			$conta++;
			if ( $conta > 5 ) {
				unlink( $file );
			}
		}

		return $files;
	}

	function p( $txt, $level ) {

		if ( $level == 2 ) {
			$this->conta ++;
			$txt = '<em>' . $this->conta . '</em> ' . $txt . ' - <i>(' . $execution_time = Debug::get_execution_time() . ' s.) </i>';
		}
		if ( $level == 'error' ) {
			$txt .= ' (error)';
		}
		echo '<p class="m' . $level . '">' . $txt . '</p>';
		//@ob_flush();
		if ( $this->conta % 1000 == 0 && $this->conta != 0 ) {
			flush();
		}
	}

}