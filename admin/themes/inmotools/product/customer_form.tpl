<script language="JavaScript" src="/admin/jscripts/validacio.js?v=<?=$version?>"></script>
<script language="JavaScript" src="/admin/modules/product/jscripts/functions.js?v=<?=$version?>"></script>
<div class="form">
<form name="form_delete" method="post" action="<?=$link_action_delete?>">
  <input name="<?=$id_field?>" type="hidden" value="<?=$id?>">
</form>
<form name="theForm" method="post" action="<?=$link_action?>" onsubmit="<?=$js_string?>">
<input name="order_id[<?=$id?>]" type="hidden" value="<?=$id?>" />
<input name="order_id_javascript" type="hidden" value="<?=$id?>" />
<?if ($form_new):?>
  <div id="form_buttons_holder"><table id ="form_buttons">
    <tr>
      <td class="buttons_top"><div class="right">
          <input type="submit" name="Submit" value="<?=$c_send_new?>" class="btn_form1">
        </div></td>
    </tr>
  </table></div>
  <? endif //form_new?>
<? if ($form_edit):?>  
  <div id="form_buttons_holder"><table id ="form_buttons">
    <tr>
      <td valign="bottom"><div class="menu"><div><?=$c_edit_data?></div><?if ($has_images):?>
            	<a href="<?=$image_link?>"><?=$c_edit_images?></a>
         <?endif //has_images?></div></td>
                <td class="buttons_top"><div class="right" style="margin-bottom:3px;">
    <input type="submit" name="Submit" value="<?=$c_send_edit?>" class="btn_form1" />
    <div class="btn_sep"></div>
    <a class="btn_form_delete" href="javascript:submit_form('<?=$c_confirm_form_bin?>', true);">
    <?=$c_delete?>
    </a></div></td>
    </tr>
  </table></div>
  <? endif //form_edit?>
<? if ($c_form_title):?>
  <table width="100%" cellpadding="4" cellspacing="0">
    <tr>
      <td class="formCaptionTitle"><strong>
        <?=$c_form_title?>
        </strong>&nbsp;&nbsp;
        <?=$c_customer_id_title?>
        <?=$customer_id?></td>
    </tr>
  </table>
  <? endif //c_form_title?>
  <table width="100%" border="0" cellspacing="0" class="formstbl">
      <tr>
        <td width="150" valign="top" class="formsCaption"><strong>
          <?=$c_treatment?>:</strong></td>
        <td valign="top" class="forms"><?=$treatment?>
          <?=$address_id?>
          <?=$delivery_id?>
          <input name="customer_id[<?=$customer_id?>]" type="hidden" value="<?=$customer_id?>" />
          <input name="customer_id_javascript" type="hidden" value="<?=$customer_id?>" /></td>
      </tr>
      <tr>
        <td width="150" valign="top" class="formsCaption"><strong>
          <?=$c_name?>:</strong></td>
        <td valign="top" class="forms"><?=$name?></td>
      </tr>
      <tr>
        <td width="150" valign="top" class="formsCaption"><strong>
          <?=$c_surname?>:</strong></td>
        <td valign="top" class="forms"><?=$surname?></td>
      </tr>
      <tr>
        <td width="150" valign="top" class="formsCaption"><strong>
          <?=$c_birthdate?>:</strong></td>
        <td valign="top" class="forms"><?=$birthdate?></td>
      </tr>
      <tr>
        <td width="150" valign="top" class="formsCaption"><strong>
          <?=$c_telephone?>:</strong></td>
        <td valign="top" class="forms"><?=$telephone?></td>
      </tr>
      <tr>
        <td width="150" valign="top" class="formsCaption"><strong>
          <?=$c_telephone_mobile?>:</strong></td>
        <td valign="top" class="forms"><?=$telephone_mobile?></td>
      </tr>
    </table><? if ($group_id):?>
    <table width="100%" cellpadding="0" cellspacing="0">
      <tr>
        <td height="1" class="formsCaptionHor"><?=$c_group_id;?></td>
      </tr>
    </table>
    <table width="100%" border="0" cellspacing="0"  class="formstbl">
      <tr>
        <td width="150" valign="top" class="formsCaption">&nbsp;</td>
        <td valign="top" class="forms"><?=$group_id;?></td>
      </tr>
    </table><? endif // group_id?>
    <table width="100%" cellpadding="0" cellspacing="0">
      <tr>
        <td class="formsCaptionHor"><?=$c_form_title2?></td>
      </tr>
    </table>
    <table width="100%" border="0" cellspacing="0" class="formstbl">
      <tr>
        <td width="150" valign="top" class="formsCaption"><strong>
          <?=$c_mail?>:</strong></td>
        <td valign="top" class="forms"><?=$mail?></td>
      </tr>
      <tr>
        <td valign="top" class="formsCaption"><strong>
          <?=$c_password?>:</strong></td>
        <td valign="top" class="forms"><?=$password?><? if ($form_edit):?> <a class="padding-left-2" href="/admin/?menu_id=20124&action=send_password&customer_id=<?=$id?>" target="save_frame">
    <?=$c_send_password?>
    </a><?endif //form_edit?></td>
      </tr>
      <tr>
        <td valign="top" class="formsCaption"><strong>
          <?=$c_password_repeat?>:</strong></td>
        <td valign="top" class="forms"><?=$password_repeat?></td>
      </tr>
    </table><div class="sep_dark"></div><? if ($is_wholesaler):?>
  <table width="100%" cellpadding="4" cellspacing="0">
    <tr>
      <td class="formCaptionTitle"><strong>
        <?=$c_form_title_wholesaler?>
        </strong></td>
    </tr>
  </table>
  <table width="100%" border="0" cellpadding="4" cellspacing="0" class="formstbl">
          <tr>
            <td width="150" valign="top" class="formsCaption"><strong>
              <?=$c_wholesaler_rate?>:</strong></td>
            <td valign="top" class="forms"><?=$wholesaler_rate?></td>
          </tr>
		  <tr>
            <td width="150" valign="top" class="formsCaption"><strong>
              <?=$c_has_no_vat?>:</strong></td>
            <td valign="top" class="forms"><?=$has_no_vat?></td>
          </tr>
		  <tr>
            <td width="150" valign="top" class="formsCaption"><strong>
              <?=$c_has_equivalencia?>:</strong></td>
            <td valign="top" class="forms"><?=$has_equivalencia?></td>
          </tr>
        </table><div class="sep_dark"></div><?endif //is_wholesaler?>
  <table width="100%" cellpadding="4" cellspacing="0">
    <tr>
      <td class="formCaptionTitle"><strong>
        <?=$c_form_title3_list?>
        </strong></td>
    </tr>
  </table>
  <table width="100%" border="0" cellpadding="4" cellspacing="0" class="formstbl">
          <? if ($is_wholesaler):?><?endif //is_wholesaler?>
          <tr>
            <td width="150" valign="top" class="formsCaption"><strong>
              <?=$c_company?>:</strong></td>
            <td valign="top" class="forms"><?=$company?></td>
          </tr>
          <tr>
            <td width="150" valign="top" class="formsCaption"><strong>
              <?=$c_dni?>:</strong></td>
            <td valign="top" class="forms"><?=$dni?></td>
          </tr>
          <tr>
            <td width="150" valign="top" class="formsCaption"><strong>
              <?=$c_address?>:</strong></td>
            <td valign="top" class="forms"><?=$address?></td>
          </tr>
          <tr>
            <td width="150" valign="top" class="formsCaption"><strong>
              <?=$c_city?>:</strong></td>
            <td valign="top" class="forms"><?=$city?></td>
          </tr>
          <tr>
            <td width="150" valign="top" class="formsCaption"><strong>
              <?=$c_zip?>:</strong></td>
            <td valign="top" class="forms"><?=$zip?></td>
          </tr>
          <tr>
            <td width="150" valign="top" class="formsCaption"><strong>
              <?=$c_country_id?>:</strong></td>
            <td valign="top" class="forms"><?=$country_id?></td>
          </tr>
          <tr id="provincia_id_holder">
            <td width="150" valign="top" class="formsCaption"><strong>
              <?=$c_provincia_id?>:</strong></td>
            <td valign="top" class="forms"><?=$provincia_id?></td>
          </tr>
    </table><div class="sep_dark"></div>
  <table width="100%" cellpadding="4" cellspacing="0">
    <tr>
      <td class="formCaptionTitle"><div class="title"> <strong>
          <?=$c_form_title4?>
          <? if ($form_edit):?>
            &nbsp;&nbsp; <a class="middle" href="javascript:address_add(<?=$id?>)">
            <?=$c_add_button?>
            </a>
            <? endif //form_edit?>
          </strong></div></td>
    </tr>
  </table>
  <? if ($form_new):?>
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="formstbl">
      <tr>
        <td class="formsCaptionHor"><table cellpadding="4" cellspacing="0">
            <tr>
              <td class="formsCaptionHor"><?=$c_same_address?>:</td>
              <td valign="top"><input onclick="customer_show_address(this)" name="same_address" type="checkbox" id="same_address" checked="checked" /></td>
            </tr>
          </table>
          <div class="hidden" id="delivery_address">
            <table width="100%" cellpadding="4" cellspacing="0">
              <tr>
                <td width="150" valign="top" class="formsCaption"><strong>
                  <?=$c_a_name?>:</strong></td>
                <td valign="top" class="forms"><input name="a_name2" type="text" value="" maxlength="50" /></td>
              </tr>
              <tr>
                <td width="150" valign="top" class="formsCaption"><strong>
                  <?=$c_a_surname?>:</strong></td>
                <td valign="top" class="forms"><input name="a_surname2" type="text" value="" maxlength="50" /></td>
              </tr>
              <tr>
                <td width="150" valign="top" class="formsCaption"><strong>
                  <?=$c_address?>:</strong></td>
                <td valign="top" class="forms"><input name="address2" type="text" value="" maxlength="255" /></td>
              </tr>
              <tr>
                <td width="150" valign="top" class="formsCaption"><strong>
                  <?=$c_city?>:</strong></td>
                <td valign="top" class="forms"><input name="city2" type="text" value="" maxlength="50" /></td>
              </tr>
              <tr>
                <td width="150" valign="top" class="formsCaption"><strong>
                  <?=$c_zip?>:</strong></td>
                <td valign="top" class="forms"><input name="zip2" type="text" value="" maxlength="10" /></td>
              </tr>
              <tr>
                <td width="150" valign="top" class="formsCaption"><strong>
                  <?=$c_country_id?>:</strong></td>
                <td valign="top" class="forms"><?=$country_id2?></td>
              </tr>
              <tr id="provincia_id2_holder">
                <td width="150" valign="top" class="formsCaption"><strong>
                  <?=$c_provincia_id?>:</strong></td>
                <td valign="top" class="forms"><?=$provincia_id2?></td>
              </tr>
            </table>
          </div></td>
      </tr>
    </table>
    <? endif //form_new?>
  <? if ($form_edit):?>
    <table width="100%" border="0" cellpadding="8" cellspacing="0" class="formstbl">
      <tr>
        <td style="padding-top:18px;padding-bottom:18px;" id="addresses_list"><?=$addresses?></td>
      </tr>
    </table>
    <? endif //form_edit?>
  <? if ($form_new):?>
    <table width="100%" cellpadding="4" cellspacing="2" class="formsButtonstbl">
      <tr>
        <td align="center" class="formsButtons"><input type="submit" name="Submit" value="<?=$c_send_new?>" class="btn_form1" /></td>
      </tr>
    </table>
    <? endif //form_new?>
  <? if ($form_edit):?>
    <table width="100%" cellpadding="4" cellspacing="2" class="formsButtonstbl">
      <tr>
        <td align="center" class="formsButtons"><input type="submit" name="Submit" value="<?=$c_send_edit?>" class="btn_form1" />
          &nbsp;&nbsp;<a class="btn_form_delete" href="javascript:submit_form('<?=$c_confirm_form_bin?>', true);">
          <?=$c_delete?>
          </a>&nbsp;&nbsp;</td>
      </tr>
    </table>
    <? endif //form_edit?>
	 </form>
  </div>
  
