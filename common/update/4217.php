<?php
// no es poden posar ; entre mig de la consulta ja que parteix les consultes per el ; a no ser que fem servir $update_sql en array()
	
if (is_table("news__new_language")){	
	$update_sql .="
ALTER TABLE `news__new_language`
	ADD COLUMN `new_old_file_name` VARCHAR(100) NOT NULL DEFAULT '' AFTER `new_file_name`;";
}		
if (is_table("product__product_language")){	
	$update_sql .="
ALTER TABLE `product__product_language`
	ADD COLUMN `product_old_file_name` VARCHAR(100) NOT NULL DEFAULT '' AFTER `product_file_name`;";
}	

?>