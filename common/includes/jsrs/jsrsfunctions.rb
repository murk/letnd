# Here's a simple sample JSRS function class. Your class
# can do whatever you like it to do and return whatever
# you like, as long as it does not exceed browser-set
# limits on payload size (perhaps 64K).
class DataTypeTest < JSRSFunction
  def data
    return [1, 2, 3, '<b>hello&</b>',{'a' => 'a<', 'b' => 'b'}, [4, 5]]
  end
end
