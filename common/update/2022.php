<?php
// no es poden posar ; entre mig de la consulta ja que parteix les consultes per el ;

// Afegir categories d'immobles noves
if (!Db::get_first('SELECT count(*) FROM inmo__category WHERE `category_id` =18')){
	$update_sql .="
	INSERT INTO `inmo__category` (`category_id`, `ref_code`, `ordre`, `blocked`, `active`) VALUES
	(18, '', 0, 0, 0),
	(19, '', 0, 0, 0);
	INSERT INTO `inmo__category_language` (`category_id`, `language`, `category_original`, `category`) VALUES
	(18, 'cat', 'Protecció oficial', 'Protecció oficial'),
	(18, 'spa', 'Protección oficial', 'Protección oficial'),
	(18, 'eng', 'State-subsidized', 'State-subsidized'),
	(18, 'deu', 'State-subsidized', 'State-subsidized'),
	(18, 'fra', 'State-subsidized', 'State-subsidized'),
	(18, 'dut', 'State-subsidized', 'State-subsidized'),
	(19, 'cat', 'Ruïna', 'Ruïna'),
	(19, 'spa', 'Ruina', 'Ruina'),
	(19, 'eng', 'Ruin', 'Ruin'),
	(19, 'deu', 'Ruine', 'Ruine'),
	(19, 'dut', 'Ruin', 'Ruin'),
	(19, 'fra', 'Ruin', 'Ruin');
	";
}

$update_sql .="
INSERT INTO `inmo__category` (`category_id`, `ref_code`, `ordre`, `blocked`, `active`) VALUES
(20, '', 0, 0, 0),
(21, '', 0, 0, 0);
INSERT INTO `inmo__category_language` (`category_id`, `language`, `category_original`, `category`) VALUES
	(20, 'cat', 'Negoci', 'Negoci'),
	(20, 'spa', 'Negocio', 'Negocio'),
	(20, 'eng', 'Business', 'Business'),
	(20, 'deu', 'Betrieb', 'Betrieb'),
	(20, 'fra', 'Affaire', 'Affaire'),
	(20, 'dut', 'Business', 'Business'),
	(21, 'cat', 'Finca exclusiva', 'Finca exclusiva'),
	(21, 'spa', 'Finca exclusiva', 'Finca exclusiva'),
	(21, 'eng', 'Exclusive property', 'Exclusive property'),
	(21, 'deu', 'Exklusive immobilie', 'Exklusive immobilie'),
	(21, 'dut', 'Exclusive property', 'Exclusive property'),
	(21, 'fra', 'Propieté exclusive', 'Propieté exclusive');
";
?>