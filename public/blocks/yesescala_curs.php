<?

$module = Module::load('yesescala','curs');
$this->tpl->module = &$module;
$module->parent = 'Block';

$loop = $module->do_action('get_records');



if (!$loop) {
	$show_block = false;
	return;
}



$conta = 0;
$loop_count = count($loop);
foreach ($loop as $key=>$val){
	$rs = &$loop[$key];		
	
	$rs['link'] = $rs['show_record_link'];// = Blocks::get_block_link($rs['property_id'], $rs['property_file_name'], $listing->tool, $listing->tool_section, 'category', $rs['category_id_value']);
	
	$curs_id = R::escape('file_name');
	
	if (is_numeric($curs_id)) {
		$rs['selected'] = $curs_id==$rs['curs_id']?1:0;		
	}
	else{
		$rs['selected'] = $curs_id==$rs['curs_file_name']?1:0;				
	}
	
	
	$rs['conta'] = $conta;
	$rs['loop_count'] = $loop_count;
	$conta++;
	
}

?>