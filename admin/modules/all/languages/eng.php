<?php
global $gl_caption_admin, $gl_caption_public, $gl_caption_newsletter, $gl_caption_inmo_admin, $gl_caption_inmo_public, $gl_caption_translation, $gl_caption_custumer, $gl_caption_page, $gl_caption_save, $gl_caption_product_admin, $gl_caption_product_public;
// menus eina
if (!defined('WEB_MENU_TITLE')){
	define('WEB_MENU_TITLE','Titol web'); //Nom apareix dalt internet explorer
	define('WEB_MENU_ROBOTS','Tags de la web'); //Paraules dels tags
	define('WEB_MENU_SAVE','Copies de seguretat'); //Paraules dels tags
	define('WEB_MENU_SAVE_LIST','Realitzar copia'); //Paraules dels tags
	define('WEB_MENU_TRANSLATION','Traduccions');
	define('WEB_MENU_TRANSLATION_LIST','Llistar totes les traduccions');
	define('WEB_MENU_PAGE_TRANSLATION_LIST','Llistar apartats per traduir');
	define('WEB_MENU_BACKOFFICE_SEARCH','Busquedes clients');


	define('WEB_MENU_CONFIG', 'Configuració');
	define('WEB_MENU_CONFIGADMIN_LIST', 'General part privada');
	define('WEB_MENU_CONFIGPUBLIC_LIST', 'General part pública');
	define('WEB_MENU_CONFIGINMOADMIN_LIST', 'Propietats part privada');
	define('WEB_MENU_CONFIGINMOPUBLIC_LIST', 'Propietats part pública');
	define('WEB_MENU_CONFIGCUSTUMER_LIST', 'Clients');
	define('WEB_MENU_CONFIGNEWSLETTER_LIST', 'Newsletter');
	define('WEB_MENU_CONFIGUTILITY_LIST', 'Utilitats');
	define('WEB_MENU_CONFIGMLS_LIST','MLS');
	define('WEB_MENU_CONFIGCONTACT_PUBLIC_LIST','Contacte part pública');
	define('WEB_MENU_CONFIGCONTACT_ADMIN_LIST','Contacte part privada');

	define('WEB_MENU_CONFIGPRODUCT_ADMIN_LIST','Productes part privada');
	define('WEB_MENU_CONFIGPRODUCT_PUBLIC_LIST','Productes part pública');

		
	define('WEB_MENU_BANNER','Banners');
	define('WEB_MENU_BANNER_NEW','Nou banner');
	define('WEB_MENU_BANNER_LIST','Llistar banners');
	define('WEB_MENU_BANNER_LIST_BIN','Paperera');
}

/*--------------- no traduit -------------------*/
$gl_caption_config['c_others1']='Característica 1';
$gl_caption_config['c_others2']='Característica 2';
$gl_caption_config['c_others3']='Característica 3';
$gl_caption_config['c_others4']='Característica 4';


$gl_caption_config['c_default_home_tool'] = 'Eina per defecte';
$gl_caption_config['c_default_home_tool_section'] = 'Secció per defecte';
$gl_caption_config['c_language'] = 'Idioma per defecte';
$gl_caption_config['c_home_max_results'] = 'Nombre de resultats a la pàgina d\'inici';
$gl_caption_config['c_page_slogan'] = 'Frase corporativa';
$gl_caption_public['c_page_title'] = 'Títol de la web';
$gl_caption_config['c_page_phone'] = 'Telèfon';
$gl_caption_config['c_page_address'] = 'Adreça';
$gl_caption_config['c_page_title'] = 'Títol Intranet';
$gl_caption_config['c_default_menu_id'] = 'Menú per defecte';
$gl_caption_config['c_theme'] = 'Tema';
$gl_caption_config['c_language'] = 'Idioma';
$gl_caption_config['c_max_results'] = 'Nombre de resultats per pàgina';
$gl_caption_config['c_max_page_links'] = 'Nombre de pàgines a mostrar';
$gl_caption_config['c_add_dots_length'] = 'Màxim de paraules a les descripcions';
$gl_caption_config['c_date_created'] = 'Data de creació de la web';
$gl_caption_admin['c_group_1'] = 'Opcions dels llistats';
$gl_caption_public['c_group_1'] = $gl_caption_admin['c_group_1'];
$gl_caption_config['c_templatehtml_id'] = 'Plantilla en html';
$gl_caption_config['c_templatetext_id'] = 'Plantilla de text';
$gl_caption_config['c_group_1'] = 'De';
$gl_caption_config['c_group_2'] = 'Contestar a';
$gl_caption_config['c_group_3'] = 'Dades SMTP';
$gl_caption_config['c_m_username'] = 'Nom d\'usuari';
$gl_caption_config['c_m_password'] = 'Contrasenya';
$gl_caption_config['c_m_password_repeat'] = 'Repetir contrasenya';
$gl_caption_config['c_m_host'] = 'Servidor';
$gl_caption['c_custumer_table'] = 'Taula d\'adreces';
$gl_caption['c_newsletter__custumer'] = 'newsletter__custumer';
$gl_caption['c_custumer__custumer'] = 'custumer__custumer';
$gl_caption['c_contact__contact'] = 'contact__contact';
$gl_caption['c_product__customer'] = 'product__customer';
$gl_caption_newsletter['c_content_extra'] = 'Contingut inclòs';
$gl_caption_config['c_only_test'] = 'Mode test';
$gl_caption_config['c_only_test_description'] = 'Si marques aquesta casella, no s\'enviarà cap mail, nomès es simularà l\'enviament';

$gl_caption_config['c_text'] = 'text';
$gl_caption_config['c_provincies'] = 'Províncies habituals';
$gl_caption_config['c_default_provincia'] = 'Província per defecte';
$gl_caption_config['c_default_comarca'] = 'Comarca per defecte';
$gl_caption_config['c_default_municipi'] = 'Municipi per defecte';
$gl_caption_config['c_units'] = 'Unitats';

$gl_caption_inmo_admin['c_group_1'] = 'Thumbnail admin';
$gl_caption_config['c_im_admin_thumb_w'] = 'amplada';
$gl_caption_config['c_im_admin_thumb_h'] = 'alçada';
$gl_caption_config['c_im_admin_thumb_q'] = 'qualitat';

$gl_caption_inmo_admin['c_group_2'] = 'Thumbnail públic';
$gl_caption_inmo_public['c_group_2'] = $gl_caption_inmo_admin['c_group_2'];
$gl_caption_config['c_im_thumb_w'] = 'amplada';
$gl_caption_config['c_im_thumb_h'] = 'alçada';
$gl_caption_config['c_im_thumb_q'] = 'qualitat';

$gl_caption_inmo_admin['c_group_3'] = 'Imatge fitxa públic';
$gl_caption_inmo_public['c_group_3'] = $gl_caption_inmo_admin['c_group_3'];
$gl_caption_config['c_im_details_w'] = 'amplada';
$gl_caption_config['c_im_details_h'] = 'alçada';
$gl_caption_config['c_im_details_q'] = 'qualitat';

$gl_caption_inmo_admin['c_group_4'] = 'Imatge gran públic';
$gl_caption_inmo_public['c_group_4'] = $gl_caption_inmo_admin['c_group_4'];
$gl_caption_config['c_im_medium_w'] = 'amplada';
$gl_caption_config['c_im_medium_h'] = 'alçada';
$gl_caption_config['c_im_medium_q'] = 'qualitat';


$gl_caption_inmo_admin['c_group_5'] = 'Imatge gran impressió';
$gl_caption_config['c_im_big_w'] = 'amplada';
$gl_caption_config['c_im_big_h'] = 'alçada';

$gl_caption_config['c_im_list_cols'] = 'Col·lumnes imatge';
$gl_caption_config['c_property_auto_ref'] = 'Referència automàtica';
$gl_caption_config['c_menu_price'] = 'Valors del menú de preu';
$gl_caption_config['c_home_max_results'] = 'Nombre destacats';
$gl_caption_config['c_home_title'] = 'Títol de propietats destacades';
$gl_caption_config['c_html'] = 'html';
$gl_caption_config['c_from_address'] = 'E-mail';
$gl_caption_config['c_from_address_repeat'] = 'Repetir e-mail';
$gl_caption_config['c_from_name'] = 'Nom';
$gl_caption_config['c_reply_to'] = 'E-mail';
$gl_caption_config['c_reply_to_repeat'] = 'Repetir e-mail';
$gl_caption_config['c_reply_to_name'] = 'Nom';
$gl_caption_config['c_subject'] = 'Assumpte';
$gl_caption_config['c_format'] = 'Format';
$gl_caption_config['c_send_method'] = 'Forma d\'enviament';
$gl_caption_config['c_CC'] = 'Còpia a cada adreça (CC)';
$gl_caption_config['c_BCC'] = 'Còpia oculta a cada adreça (BCC)';
$gl_caption_config['c_one_by_one'] = 'Un missatge diferent per cada adreça';
$gl_caption['c_template_caption']='[Cap plantilla]';

$gl_caption_translation['c_2'] = 'En traducció';
$gl_caption_translation['c_3'] = 'Finalitzat';
$gl_caption_translation['c_1'] = 'Pendent';
$gl_caption_translation['c_4'] = 'Error';
$gl_caption['c_description'] = 'Descripció de la web';
$gl_caption['c_mail'] = 'Correu predeterminat';
$gl_caption['c_title'] = 'Titol de la web';
$gl_caption['c_robots'] = 'Tags de la web';
$gl_caption_config['c_interest'] = 'Interès predeterminat';
$gl_caption_custumer['c_group_1'] = 'Cerca aproximativa';
$gl_caption_custumer['c_group_2'] = 'MLS';
$gl_caption_config['c_search_percent'] = 'Desnivell a les cerques client i MLS (%)';
$gl_caption_config['c_search_level'] = 'Nivell d\'aproximació en la cerca';
$gl_caption_translation['c_join_date'] = 'Data d\'enviament';
$gl_caption_translation['c_state'] = 'Estat actual:';
$gl_caption_translation['c_numwords'] = 'Número de paraules:';
$gl_caption_translation['c_originaltext'] = 'Text original:';

$gl_caption['c_search_level_1'] = 'Restrictiu';
$gl_caption['c_search_level_2'] = 'Normal';
$gl_caption['c_search_level_3'] = 'Poc restrictiu';

$gl_caption_config ['c_mls_all'] = 'Marcar per defecte la casella MLS dels immobles';
$gl_caption_config ['c_mls_comission'] = 'Comissió per defecte de les propietats MLS (%)';
$gl_caption_config ['c_mls_search_always'] = 'Cerca MLS activada';
$gl_caption_config ['c_mls_allow_listing'] = 'Permetre llistar immobles';

$gl_caption_config ['c_0'] = 'No';
$gl_caption_config ['c_1'] = 'Si';
$gl_caption_config ['c_default_mail']='E-mail predeterminat';

$gl_caption_save ['c_save_1'] = 'Clica aquí';
$gl_caption_save ['c_save_2'] = 'per descarregar la Copia de seguretat';


$gl_caption_config ['c_google_center_longitude'] = 'Ubicació';
$gl_caption_inmo_admin['c_google_center_longitude'] = 'Posició inicial del mapa';
$gl_caption_config ['c_google_latitude'] = 'Latitud';
$gl_caption_config ['c_google_longitude'] = 'Longitud';
$gl_caption_config ['c_google_center_latitude'] = 'Latitud centre mapa';
$gl_caption_config ['c_center_longitude'] = 'Longitud centre mapa';
$gl_caption_config ['c_google_zoom']='';
$gl_caption_config ['c_allow_seo']='Activar SEO';

// productes
$gl_caption_product_admin ['c_default_sell']='Venta predeterminada';
$gl_caption_product_admin ['c_pvp_required']='P.V.P obligatori';
$gl_caption_product_admin ['c_home_title']='Titol dels productes destacats';
$gl_caption_product_admin ['c_group_1'] = $gl_caption_product_public ['c_group_1'] = $gl_caption_inmo_admin['c_group_1'];
$gl_caption_product_admin ['c_group_2'] = $gl_caption_product_public ['c_group_2'] = $gl_caption_inmo_admin['c_group_2'];
$gl_caption_product_admin ['c_group_3'] = $gl_caption_product_public ['c_group_3'] = $gl_caption_inmo_admin['c_group_3'];
$gl_caption_product_admin ['c_group_4'] = $gl_caption_product_public ['c_group_4'] = $gl_caption_inmo_admin['c_group_4'];
$gl_caption_product_admin ['c_group_5'] = $gl_caption_product_public ['c_group_5'] = $gl_caption_inmo_admin['c_group_5'];

$gl_caption_product_public ['c_group_5'] = 'Opcions del mòdul de pagament';
$gl_caption_product_public ['c_group_6'] = 'Pàgina d\'inici';
$gl_caption_product_public ['c_no_sell_text']='Descripció producte no en venda';
$gl_caption_product_public ['c_paypal_sandbox']='Paypal en mode sandbox';
$gl_caption_product_public ['c_banc_name']='Nom del banc';
$gl_caption_product_public ['c_account_number']='Número de compte';
$gl_caption_product_public ['c_paypal_mail']='E-mail del compte PayPal';
$gl_caption_product_public ['c_paypal_mail_repeat'] = 'Repetir e-mail';
$gl_caption_product_public ['c_home_show_marked']='Mostra destacats';
$gl_caption_product_public ['c_home_show_familys']='Mostra famílies i subfamílies';
$gl_caption_product_public ['c_home_max_familys']='Nombre màxim de famílies';
$gl_caption_product_public ['c_home_max_family_products']='Nombre màxim de productes de cada família';
$gl_caption_product_public ['c_payment_methods']='Mòduls de pagament actius';
$gl_caption_product_public ['c_paypal']='PayPal';
$gl_caption_product_public ['c_account']='Transferència bancària';
$gl_caption_product_public ['c_ondelivery']='Contrareembols';
$gl_caption_product_public ['c_directdebit'] = 'Rebut bancari';
$gl_caption_product_public['c_home_title'] = 'Títol de la pàgina inicial';

$gl_caption_product_admin['c_is_public_tax_included']='Tots els preus (PVP) són IVA inclòs';
$gl_caption_product_admin['c_is_wholesaler_tax_included']='Tots els preu distribuïdor (PVD) són IVA inclòs';
$gl_caption_product_admin['c_group_12']='Impostos';
$gl_caption_product_admin['c_has_equivalencia_default']='Equivalència marcada a l\'insertar distribuïdor';
$gl_caption_product_admin['c_stock_control']='Control d\'stock';
$gl_caption_product_admin['c_allow_null_stock']='Permetre venta productes amb stock negatiu';
$gl_caption_product_admin['c_group_10']='Stock';
$gl_caption_product_admin['c_group_11']='Tarifes d\'enviament';
$gl_caption_product_admin['c_send_rate_spain']='Espanya';
$gl_caption_product_admin['c_send_rate_france']='França';
$gl_caption_product_admin['c_send_rate_world']='Resta del món';
$gl_caption_product_admin['c_allow_free_send']='Casella activada, permetre enviament gratuit';
$gl_caption_product_admin['c_minimum_buy_free_send']='Compra mínima de';
$gl_caption_product_admin['c_tax_included'] = 'IVA inclòs';
$gl_caption_product_admin['c_tax_not_included'] = 'IVA no inclòs';


$gl_caption_page['c_ordre'] = 'Ordre';
$gl_caption_page['c_template'] = 'Plantilla';
$gl_caption_page['c_link'] = 'Link';
$gl_caption_page_list['c_page_title'] = 'Title crawlers';
$gl_caption_page_list['c_page_file_name'] = 'Page name';
$gl_caption_page['c_page'] = 'Nom menú o títol cos';
$gl_caption_page['c_title'] = 'Títol al cos';
$gl_caption_page['c_content'] = 'Contingut';
$gl_caption_page['c_prepare'] = 'En preparació';
$gl_caption_page['c_review'] = 'Per revisar';
$gl_caption_page['c_public'] = 'Públic';
$gl_caption_page['c_archived'] = 'Arxivat';
$gl_caption_page['c_status'] = 'Estat';
$gl_caption_page['c_page_id'] = 'Id';
$gl_caption_page['c_parent_id'] = 'Parent';
$gl_caption_page['c_level'] = 'Nivell';
$gl_caption_page['c_menu'] = 'Menú';
$gl_caption_page['c_blocked'] = 'Block.';
$gl_caption_page['c_filter_parent_id'] = 'Parent';
$gl_caption_page['c_filter_level'] = 'Level';
$gl_caption_page['c_has_content'] = 'Content';
$gl_caption_page['c_has_content_0'] = 'no';
$gl_caption_page['c_has_content_1'] = 'si';
$gl_caption_page['c_edit_content_button'] = 'Editar pàgina';
$gl_caption_page['c_edit_page_button'] = 'Editar dades';
$gl_caption_page['c_filter_menu2'] = 'Menú de navegació';

/*banners*/
$gl_caption_banner['c_banner_id']='';
$gl_caption_banner['c_ordre']='Ordre';
$gl_caption_banner['c_url1']='Enllaç';
$gl_caption_banner['c_url1_name']='Text a l\'enllaç';
$gl_caption_banner['c_url1_target']='Destí';
$gl_caption_banner['c_url1_target__blank']='Una nova finestra';
$gl_caption_banner['c_url1_target__self']='La mateixa finestra';
$gl_caption_banner['c_file']='Imatge banner';
$gl_caption_banner['c_banner']='Banner';
$gl_caption_banner['c_place_id']='Secció';
$gl_caption_banner['c_banner_text']='Text';
$gl_caption_banner['c_banner_text2']='Text 2';
?>