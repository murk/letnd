<?
/**
 * AmctaicBonaplata
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2010
 * @version $Id$
 * @access public
 */
class AmctaicBonaplata extends Module{
	var $page_id_select = '', $page_id='';
	function __construct(){
		parent::__construct();
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function get_records()
	{
	    $listing = new ListRecords($this);
		$listing->call_function['content'] = 'add_dots';
		$listing->set_field('page_id','type','none');
		$listing->call('list_records_walk','page_id');
	    return $listing->list_records();
	}
	function list_records_walk($page_id){
	
		$ret['page_id'] = '';
		if ($page_id){
			$query = "SELECT all__page.page_id as page_id, page, parent_id
							FROM all__page_language, all__page
							WHERE all__page_language.page_id = all__page.page_id
							AND status = 'public'
							AND all__page.page_id = " . $page_id . "
							AND language = '" . LANGUAGE . "'";		
			$rs = Db::get_row($query);
			$query = "SELECT page
							FROM all__page_language, all__page
							WHERE all__page_language.page_id = all__page.page_id
							AND status = 'public'
							AND all__page.page_id = " . $rs['parent_id'] . "
							AND language = '" . LANGUAGE . "'";		
			$parent = Db::get_first($query);
			$ret['page_id'] = '<strong>' . str_replace(' ','&nbsp;',$parent) . '</strong><br>' . $rs['page'];
		}
		
		return $ret;
	}
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{
		$this->set_field('entered','default_value',now(true));
	    $show = new ShowForm($this);
		$show->set_field('page_id','type','none');
		$show->call('records_walk','bonaplata_id,page_id');
	    return $show->show_form();
	}
	function records_walk($agenda_id,$page_id){
		$this->page_id = $page_id;
		$this->get_pages(0,1);
		$selected = $page_id?'':' selected';
		if ($this->action=='show_form_new'){
			$this->page_id_select = substr_replace($this->page_id_select, '<option selected', strpos($this->page_id_select,'<option'),7);
		}
		$this->page_id_select = 
			'<select size="32" style="width:635px" name="page_id['.$agenda_id.']">
				<optgroup label="">' . 
				$this->page_id_select . '
				<optgroup label="">
			</select>
			<input name="old_page_id['.$agenda_id.']" type="hidden" value="'.$page_id.'">';		
		return array('page_id' => $this->page_id_select);
	}
	function get_pages($parent_id, $level){
		$loop = array();
		$query = "SELECT all__page.page_id as page_id, link, page, parent_id, has_content
						FROM all__page_language, all__page
						WHERE all__page_language.page_id = all__page.page_id
						AND status = 'public'
						AND parent_id = " . $parent_id . "
						AND bin <> 1
						AND (menu = '2' OR menu = '3')
						AND language = '" . LANGUAGE . "'
						ORDER BY menu ASC, ordre ASC, page_id";
		Debug::add($query,'Query get_page_recursive');	
		$results = Db::get_rows($query);

		if ($results)
		{
			foreach($results as $rs)
			{
				if ($rs['link'])
				{
					$sep = strstr($rs['link'], '?')?'&':'?';
					$l['link'] = $rs['link'] . $sep . 'language=' . LANGUAGE;
					if ($rs['link'] != '/') // la home page no porta el page_id
					{
						$l['link'] .= '&page_id=' . $rs['page_id'];
					}
				}
				else
				{
					$l['link'] = '/?page_id=' . $rs['page_id'] . '&language=' . LANGUAGE;
				}
				$space = '';
				$space2 = '';
				for ($i = 1; $i < $level; $i++) {
					$space.='&nbsp;&nbsp;&nbsp;&nbsp;';
					$space2.='&nbsp;&nbsp;&nbsp;&nbsp;';
				}				
				$selected = ($this->page_id == $rs['page_id'])?' selected':'';
				$style = $level!=1?' style="color:#000;"':'';
				if ($rs['page_id']=='78') $this->page_id_select.='<optgroup label=""><optgroup label="">';
				if ($rs['has_content']=='1'){			
					$l['item'] = $space . $rs['page'];
					$this->page_id_select.='<option value="'.$rs['page_id'].'"'.$selected.$style.'>'.$l['item'].'</option>';
				}
				else{			
					$l['item'] = $space2 . $rs['page'];
					$this->page_id_select.='<optgroup label="'.$l['item'].'">';				
				}
				$this->get_pages($rs['page_id'], $level+1);
			}
		}
	}

	function save_rows()
	{
	    $save_rows = new SaveRows($this);
	    $save_rows->save();
	}

	function write_record()
	{
	    $writerec = new SaveRows($this);
	    $writerec->save();
	}

	function manage_images()
	{
	    $image_manager = new ImageManager($this);
	    $image_manager->execute();
	}
}
?>