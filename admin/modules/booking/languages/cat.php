<?
// menus eina
if (!defined('INMO_MENU_PROPERTY')){
	define('INMO_MENU_PROPERTY', 'Immobles');
	define('INMO_MENU_PROPERTY_NEW', 'Entrar nou immoble');
	define('INMO_MENU_PROPERTY_LIST', 'Llistar immobles');
	define('INMO_MENU_SEARCH_FORM', 'Buscar i Editar immobles');
	define('INMO_MENU_PROPERTY_BIN', 'Paperera de reciclatge');
	define('INMO_MENU_CATEGORY', 'Tipus d\'immobles');
	define('INMO_MENU_CATEGORY_NEW', 'Entrar nou tipus');
	define('INMO_MENU_CATEGORY_LIST', 'Llistar tipus');
	define('INMO_MENU_TRANSLATION', 'Traduccions');
	define('INMO_MENU_PROPERTY_TRANSLATION_LIST', 'Llistar immobles per traduir');
	define('INMO_MENU_CATEGORY_TRANSLATION_LIST', 'Llistar categories per traduir');
	define('INMO_MENU_CONFIG', 'Configuració Immobles');
	define('INMO_MENU_CONFIGADMIN_LIST', 'Configuració part privada');
	define('INMO_MENU_CONFIGPUBLIC_LIST', 'Configuració part pública');
	define('INMO_MENU_CONFIGADMIN', 'Configuració part privada');
	define('INMO_MENU_CONFIGPUBLIC', 'Configuració part pública');
	define('INMO_MENU_ZONE', 'Zones');
	define('INMO_MENU_ZONE_NEW', 'Entrar nova zona');
	define('INMO_MENU_ZONE_LIST', 'Llistar zones');
	define('INMO_MENU_ZONE_BIN', 'Paperera de reciclatge');
	define('INMO_MENU_TV', 'Aparadors virtuals');
	define('INMO_MENU_TV_LIST', 'Sel·leccionar immobles');
	define('INMO_MENU_TV_LIST_SELECTED', 'Immobles a l\'aparador');
	define('INMO_MENU_TV_LIST_DOWNLOAD', 'Descarregar aparador');
	define('INMO_MENU_PROPERTY_MAP_LIST', 'Buscar per mapa');
	define('INMO_MENU_ADJECTIVE_NEW', 'Entrar nou adjectiu');
	define('INMO_MENU_ADJECTIVE_LIST', 'Listar adjectius');

	define('BOOKING_MENU_BOOK','Reserves');
	define('BOOKING_MENU_BOOK_NEW','Entrar nova reserva');
	define('BOOKING_MENU_BOOK_LIST','Llistar reserves');
	define('BOOKING_MENU_BOOK_LIST_PAST','Reserves finalitzades');
	define('BOOKING_MENU_BOOK_SEARCH','Disponibilitat');

	define('BOOKING_MENU_EXTRA','Serveis');
	define('BOOKING_MENU_EXTRA_NEW','Entrar nou servei');
	define('BOOKING_MENU_EXTRA_LIST','Llistar serveis');
	define('BOOKING_MENU_EXTRA_LIST_BIN','Paperera de reciclatge');

	define('BOOKING_MENU_SEASON','Temporades');
	define('BOOKING_MENU_SEASON_NEW','Entrar nova temporada');
	define('BOOKING_MENU_SEASON_LIST','Llistar temporades');

	define ('TITLE_COMARCA_ADMIN','Per zona: ');
	define('PROPERTY_AUTO_REF', 'Automàtica');

	define('BOOKING_MENU_PROMCODE','Codi promocional');
	define('BOOKING_MENU_PROMCODE_NEW','Crear codi');
	define('BOOKING_MENU_PROMCODE_LIST','Llistar codi');
	define('BOOKING_MENU_PROMCODE_LIST_BIN','Paperera');

	define('BOOKING_MENU_POLICE','Registre Mossos');
	define('BOOKING_MENU_POLICE_LIST','Llistar reserves');
}

// captions seccio
$gl_caption_book['c_payment'] = 'Pagament';
$gl_caption_book['c_payed'] = 'Pagat';
$gl_caption_book['c_payment_num'] = 'Núm.';
$gl_caption_book['c_payment_1'] = 'Pagament 1';
$gl_caption_book['c_payment_2'] = 'Pagament 2';
$gl_caption_book['c_payment_3'] = 'Pagament 3';
$gl_caption_book['c_payment_4'] = 'Pagament 4';
$gl_caption_book['c_payment_done_1'] =
$gl_caption_book['c_payment_done_2'] =
$gl_caption_book['c_payment_done_3'] =
$gl_caption_book['c_payment_done_4'] = 'Pagat';
$gl_caption_book['c_payment_entered_1'] =
$gl_caption_book['c_payment_entered_2'] =
$gl_caption_book['c_payment_entered_3'] =
$gl_caption_book['c_payment_entered_4'] = 'Data';
$gl_caption_book['c_payment_rest'] = 'Pagament pendent';
$gl_caption_book['c_payment_uncheck_message'] = '¿Segur que vols desmarcar el pagament?';
$gl_caption_book['c_payment_send_mail'] = 'enviar&nbsp;mail';
$gl_caption_book['c_payment_send_mail_subject'] = 'Notificació de pagament de la reserva: %1$s';
$gl_caption_book['c_payment_send_mail_body'] = 'Benvolgut %1$s, <p>Li notifiquem que hem rebut el pagament per import de %2$s € de la seva reserva núm. %3$s en data de %4$s</p>';
$gl_caption_book['c_payment_send_mail_body_rest'] = '<p>Queda pendent de pagament l\'import de %1$s €</p>';
$gl_caption_book['c_payment_send_mail_body2'] = '<p>Restem a la seva disposició</p>';
$gl_caption_book['c_payment_mail_sent'] = 'L\'E-mail s\'ha enviat correctament a:';
$gl_caption_book['c_payment_mail_not_sent'] = 'No s\'ha pogut enviar l\'E-mail a:';
$gl_caption_book['c_payment_mail_not_sent_no_mail'] = 'El client no té cap adreça d\'email entrada';
$gl_caption_book['c_is_owner'] = 'Propietari';
$gl_caption_book['c_caption_is_owner'] = 'Tots';
$gl_caption_book['c_is_owner_0'] = 'No';
$gl_caption_book['c_is_owner_1'] = 'Sí';

$gl_caption_book['c_property_title'] = 'Immoble';
$gl_caption_book['c_names'] = 'Clients';
$gl_caption_book['c_dates'] = 'Dates reserva';
$gl_caption['c_dates_from'] = 'Del';
$gl_caption['c_dates_to'] = 'al';
$gl_caption_book['c_date_in'] = 'Data d\'entrada';
$gl_caption_book_list['c_date_in'] = 'Entrada';
$gl_caption_book['c_date_out'] = 'Data de sortida';
$gl_caption_book_list['c_date_out'] = 'Sortida';
$gl_caption_book['c_book_entered'] = 'Reservado';
$gl_caption_book['c_capacity'] = 'Capacitat';
$gl_caption_book['c_person'] = 'Persones';
$gl_caption_book['c_adult'] = 'Adults';
$gl_caption_book['c_child'] = 'Nens';
$gl_caption_book['c_baby'] = 'Bebès';
$gl_caption_book['c_book_id'] = 'Reserva';
$gl_caption_book['c_property_id'] = 'Immoble';
$gl_caption_book['c_choose_first'] = "Primer s'ha d'escollir l'Adult 1";
$gl_caption['c_status'] = "Estat";
$gl_caption['c_status_group_free'] = "Lliure";
$gl_caption['c_status_group_not_free'] = "Ocupat";
$gl_caption['c_status_pre_booked'] = "Pre reserva";
$gl_caption['c_status_cancelled'] = "Cancelada";
$gl_caption['c_status_booked'] = "Reservat";
$gl_caption['c_status_confirmed_email'] = "Confirmada per email";
$gl_caption['c_status_confirmed_post'] = "Confirmada per correu";
$gl_caption['c_status_contract_signed'] = "Contracte firmat";
$gl_caption_book['c_caption_filter_status'] = 'Tots';

for ($i=0;$i<=25;$i++){

	$gl_caption_book['c_adult_'.$i] = $i;
	$gl_caption_book['c_child_'.$i] = $i;
	$gl_caption_book['c_baby_'.$i] = $i;

}

$gl_caption_book ['c_maximum'] = 'Màxim';
$gl_caption_book ['c_name'] = 'Nom';
$gl_caption_book ['c_surnames'] = 'Cognoms';
$gl_caption_book ['c_mail'] = 'E-mail';
$gl_caption_book ['c_phone'] = 'Telèfon';
$gl_caption_book ['c_comment'] = 'Observacions';
$gl_caption_book ['c_comment_private'] = 'Observacions privades';
//$gl_caption_book ['c_property_id'] = '';
$gl_caption_book ['c_send_new'] = 'Reservar';
$gl_caption_book ['c_custumers'] = 'Clients';
$gl_caption_book ['c_price_extras'] = 'Extres';
$gl_caption_book ['c_price_extras_search'] = 'Precio extras';
$gl_caption_book ['c_price_total'] = 'Preu total';
$gl_caption_book ['c_price_book'] = 'Preu reserva';
$gl_caption_book ['c_price'] = 'Preu';
$gl_caption_book ['c_edit_book_button'] = 'Editar reserva';
$gl_caption_book ['c_download_contract'] = 'Contracte';
$gl_caption_book ['c_custumer_search_group'] = 'Clients';
$gl_caption_book ['c_extra_search_group'] = 'Extras';
$gl_caption_book ['c_recalculate_rate'] = 'Recalcular import';
$gl_caption_book ['c_is_manual_price'] = 'Preu manual';

$gl_caption_book['c_form_title'] = 'Reserva';

$gl_messages_book['added'] = "Reserva realitzada entre les dates: %s i %s";
$gl_messages_book['added2'] = "Reserva realitzada per \"%s\" entre les dates: %s i %s";
$gl_messages_book['saved'] = "Reserva actualitzada entre les dates: %s i %s";
$gl_messages_book['saved2'] = "Reserva actualitzada per \"%s\" entre les dates: %s i %s";
$gl_messages_book['deleted'] = "Reserva borrada entre les dates: %s i %s";
$gl_messages_book['deleted2'] = "Reserva borrada per %s entre les dates: %s i %s";
$gl_messages_book['full'] = "Està ocupat entre les dates: %s i %s";
$gl_messages_book['full2'] = 'L\'inmoble "%s" està ocupat entre les dates: %s i %s';
$gl_messages_book['full3'] = 'Reserves en conflicte:';
$gl_messages_book['has_no_range'] = 'L\'inmoble no es lloga en aquestes dates';
$gl_messages_book['too_many_person'] = $gl_caption['c_too_many_person_error'] = 'El número de persones excedeix el màxim per aquest inmoble';
$gl_messages_book['exceeds_31_days'] = 'Els habitatges de lloguer de temporada no es poden llogar per un periode superior a 31 dies, per qualsevol dubte, si us plau contacti la nostra oficina';


// custumers
$gl_caption['c_add_new_custumer'] = "Afegir client nou";
$gl_caption['c_edit_custumer'] = "Editar client";
$gl_caption['c_add_custumer'] = "Seleccionar client";

$gl_caption['c_adult_singular'] = "Adult";
$gl_caption['c_child_singular'] = "Niño";
$gl_caption['c_baby_singular'] = "Bebè";


$gl_caption_extra ['c_extra_book_selected'] = 'Seleccionar';
$gl_caption_extra ['c_extra_book_input'] = '';

// serveis
$gl_caption['c_extra_type'] = "Tipus";
$gl_caption['c_extra_type_required'] = "Obligatori";
$gl_caption['c_extra_type_optional'] = "Adicional";
$gl_caption['c_extra_type_included'] = "Inclòs";
$gl_caption['c_extra'] = "Extra";
$gl_caption['c_has_quantity'] = "Quantitat seleccionable";
$gl_caption_list['c_has_quantity'] = "Quantitat";
$gl_caption['c_price'] = "Preu";
$gl_caption['c_extra_price'] = "Preu";
$gl_caption['c_comment'] = "Comentari";
$gl_caption['c_extras'] = "Extres";
$gl_caption['c_concept'] = "Concepte";
$gl_caption['c_concept_normal'] = "Extra";
$gl_caption['c_concept_deposit'] = "Fiança";
$gl_caption['c_concept_cleaning'] = "Neteja";

$gl_caption['c_extra_type_required_search'] = "Extres obligatoris";
$gl_caption['c_extra_type_optional_search'] = "Extres adicionals";
$gl_caption['c_extra_type_included_search'] = "Extres inclosos";
$gl_caption['c_extra_total_price_search'] = "Preu total extra";
$gl_caption['c_extra_quantity_search'] = "Quantitat extra";
$gl_caption['c_extra_type_search'] = "Tipus extra";

// temporades
$gl_caption['c_season'] = $gl_caption['c_season_id'] = "Temporada";
$gl_caption['c_seasons'] = "Temporades";
$gl_caption['c_ordre'] = "Ordre";
$gl_caption['c_date_start'] = "Inici";
$gl_caption['c_date_end'] = "Final";
$gl_caption['c_minimum_nights'] = "Mínim";
$gl_caption['c_minimum_nights_1'] = "1 nit";
$gl_caption['c_minimum_nights_2'] = "2 nits";
$gl_caption['c_minimum_nights_3'] = "3 nits";
$gl_caption['c_minimum_nights_4'] = "4 nits";
$gl_caption['c_minimum_nights_5'] = "5 nits";
$gl_caption['c_minimum_nights_6'] = "6 nits";
$gl_caption['c_minimum_nights_7'] = "1 setmana";
$gl_caption['c_minimum_nights_8'] = "8 nits";
$gl_caption['c_minimum_nights_9'] = "9 nits";
$gl_caption['c_minimum_nights_10'] = "10 nits";
$gl_caption['c_minimum_nights_11'] = "11 nits";
$gl_caption['c_minimum_nights_12'] = "12 nits";
$gl_caption['c_minimum_nights_13'] = "13 nits";
$gl_caption['c_minimum_nights_14'] = "2 setmanes";
$gl_caption['c_price_night'] = "Preu per nit";
$gl_caption_list['c_price_night'] = "Nit";
$gl_caption['c_price_weekend'] = "Preu nit cap de setmana";
$gl_caption_list['c_price_weekend'] = "Cap setmana";
$gl_caption['c_price_more_week'] = "Preu + de 7 nits";
$gl_caption['c_price_week'] = "Preu setmana";
$gl_caption_list['c_price_week'] = "Setmana";
$gl_caption['c_price_week_type'] = "";
$gl_caption['c_price_week_type_week'] = "Per setmana";
$gl_caption['c_price_week_type_night'] = "Per nit";
$gl_caption['c_color_id'] = $gl_caption['c_color'] = "Color";
$gl_caption['c_already_used'] = "Colors ja assignats";

$gl_caption['c_detail_season'] = "Temporada";
$gl_caption['c_detail_day'] = "Dia";
$gl_caption['c_detail_type'] = "Tarifa";
$gl_caption['c_detail_price'] = "Preu";
$gl_caption['c_detail_price_night'] = "Nit";
$gl_caption['c_detail_price_weekend'] = "Nit cap de setmana";
$gl_caption['c_detail_price_week'] = "Setmana preu nit";
$gl_caption['c_detail_whole_week'] = "Setmana preu setmana";


$gl_caption['c_whole_week'] = "Setmanes senceres";
$gl_caption_list['c_whole_week_0'] = "";
$gl_caption_list['c_whole_week_1'] = "Sí";

$gl_caption['c_start_day'] = "Primer dia";
$gl_caption['c_start_day_0'] = "Qualsevol";
$gl_caption_list['c_start_day_0'] = "";
$gl_caption['c_start_day_1'] = "Dl";
$gl_caption['c_start_day_2'] = "Dm";
$gl_caption['c_start_day_3'] = "Dc";
$gl_caption['c_start_day_4'] = "Dj";
$gl_caption['c_start_day_5'] = "Dv";
$gl_caption['c_start_day_6'] = "Ds";
$gl_caption['c_start_day_7'] = "Dg";

$gl_caption_propertyrange['c_form_title'] = "Preu període reserva";
$gl_messages_propertyrange['full2'] = "Ja hi ha definida una temporada entre les dates: %s i %s per l'inmoble \"%s\"";

$gl_caption['c_minimum_nights_error'] = "No es pot reservar menys de %s nits pel periode entre el %s i el %s";
$gl_caption['c_whole_week_error'] = "S'han de reservar setmanes senceres pel periode entre el %s i el %s";
$gl_caption['c_start_day_error'] = "S'han de reservar setmanes senceres de %s a %s pel periode entre el %s i el %s";
$gl_caption['c_equal_dates_error'] = "Les dues dates no poden ser iguals";
$gl_caption['c_no_date_selected'] = "No s'ha seleccionat cap data";



$gl_caption['c_promcode'] = 'Codi promocional';
$gl_caption_promcode['c_name'] = 'Nom';
$gl_caption_promcode['c_discount_pvp'] = 'Descompte PVP';
$gl_caption_promcode['c_discount_type'] = 'Tipus de descompte';
$gl_caption_promcode['c_discount_type_fixed'] = 'Import fixe';
$gl_caption_promcode['c_discount_type_percent'] = 'Percentatge';
$gl_caption_promcode['c_minimum_amount'] = 'Despesa mínima';
$gl_caption_promcode['c_start_date'] = 'Data inicial';
$gl_caption_promcode['c_end_date'] = 'Data final';
$gl_caption_book['c_promcode_discount'] = 'Dte. codi promocional';
$gl_caption['c_promcode_button'] = '';

$gl_caption_promcode['c_custumer_id'] = 'Client';
$gl_caption['c_custumer_caption'] = 'Tots';
$gl_caption_promcode['c_custumer_id_format'] = '%s %s %s';
$gl_caption_promcode['c_one_time_only'] = 'Aplicar només un cop per client';
$gl_caption_promcode_list['c_one_time_only'] = 'Només un cop';


$gl_caption_police['c_police_search_title'] = 'Reservas entre las fechas';
$gl_caption['c_treatment_police'] = 'Sexe';
$gl_caption_police['c_treatment_sr'] = 'Masculí';
$gl_caption_police['c_treatment_sra'] = 'Femení';
$gl_caption['c_download_selected'] = 'Descarregar arxiu';


$gl_caption['c_tt'] = $gl_caption['c_price_tt'] = 'Taxa turística';
$gl_caption['c_promcode_button'] = 'Veure';

$gl_caption['c_promcode_err_not_valid'] = 'El codi promocional no és vàlid';
$gl_caption['c_promcode_err_minimum'] = 'Per aplicar el descompte la compra ha de ser superior a %s';
$gl_caption['c_promcode_err_expired'] = 'El codi promocional ha expirat';
$gl_caption['c_promcode_err_used'] = 'Aquest codi ja ha estat utilitzat un cop';
$gl_caption['c_promcode_err_not_logged'] = 'Per aplicar descompte s\'ha d\'estar registrat';
$gl_caption['c_promcode_err_not_main_custumer'] = 'Per aplicar descompte seleccionar "Adult 1"';

$gl_caption ['c_promcode']='Descompte codi';

// buscador
$gl_caption['c_by_ref'] = '( <strong>Ref</strong>:</strong> "10 23 45" . <strong>Data:</strong> "12/2015" , "2015" , "22/12/2015" )';
$gl_caption['c_fields'] = "Camps";
$gl_caption['c_nights'] = "Nits";
$gl_caption['c_excel_name'] = "reserves";
$gl_caption['c_exact_date'] = "Data exacta";

$gl_messages ['has_no_owner']="Atenció, l'immoble no té cap propietari assignat";


// Pagament
$gl_caption['c_order_status'] = 'Estat pagament';
$gl_caption_list['c_order_status'] = 'Pagament';
$gl_caption['c_order_status_paying'] = 'Pendent pagament';
$gl_caption['c_order_status_failed'] = 'Pagament fallit';
$gl_caption['c_order_status_pending'] = 'Pagament pendent d\'estar acceptat';
$gl_caption['c_order_status_denied'] = 'Pagament rebutjat';
$gl_caption['c_order_status_voided'] = 'Pagament anulat';
$gl_caption['c_order_status_refunded'] = 'Pagament retornat';
$gl_caption['c_order_status_completed'] = 'Pagat';

$gl_caption['c_payment_method'] = 'Mètode de pagament';
$gl_caption['c_payment_method_paypal'] = 'Paypal';
$gl_caption['c_payment_method_account'] = 'Transferència bancària';
$gl_caption['c_payment_method_directdebit'] = 'Rebut bancari';
$gl_caption['c_payment_method_lacaixa'] = 'Targeta de crèdit';
$gl_caption['c_payment_method_none'] = '';
$gl_caption['c_payment_method_cash'] = 'Efectiu';
$gl_caption['c_payment_method_physicaltpv'] = 'TPV físic';

for ( $i = 1; $i <= 4; $i ++ ) {
	$gl_caption[ 'c_order_status_' . $i ]               = $gl_caption['c_order_status'];
	$gl_caption_list[ 'c_order_status_' . $i ]          = $gl_caption_list['c_order_status'];
	$gl_caption[ 'c_order_status_' . $i . '_paying' ]        = $gl_caption['c_order_status_paying'];
	$gl_caption[ 'c_order_status_' . $i . '_failed' ]        = $gl_caption['c_order_status_failed'];
	$gl_caption[ 'c_order_status_' . $i . '_pending' ]       = $gl_caption['c_order_status_pending'];
	$gl_caption[ 'c_order_status_' . $i . '_denied' ]        = $gl_caption['c_order_status_denied'];
	$gl_caption[ 'c_order_status_' . $i . '_voided' ]        = $gl_caption['c_order_status_voided'];
	$gl_caption[ 'c_order_status_' . $i . '_refunded' ]      = $gl_caption['c_order_status_refunded'];
	$gl_caption[ 'c_order_status_' . $i . '_completed' ]     = $gl_caption['c_order_status_completed'];
	$gl_caption[ 'c_payment_method_' . $i ]             = $gl_caption['c_payment_method'];
	$gl_caption[ 'c_payment_method_' . $i . '_paypal' ]      = $gl_caption['c_payment_method_paypal'];
	$gl_caption[ 'c_payment_method_' . $i . '_account' ]     = $gl_caption['c_payment_method_account'];
	$gl_caption[ 'c_payment_method_' . $i . '_directdebit' ] = $gl_caption['c_payment_method_directdebit'];
	$gl_caption[ 'c_payment_method_' . $i . '_lacaixa' ]     = $gl_caption['c_payment_method_lacaixa'];
	$gl_caption[ 'c_payment_method_' . $i . '_none' ]        = $gl_caption['c_payment_method_none'];
	$gl_caption[ 'c_payment_method_' . $i . '_cash' ]        = $gl_caption['c_payment_method_cash'];
	$gl_caption[ 'c_payment_method_' . $i . '_physicaltpv' ]        = $gl_caption['c_payment_method_physicaltpv'];
}

$gl_caption ['c_books_not_deleted_not_owner'] = "Les reserves marcades no s'han esborrat ja que s'han efectuat des de un portal o aplicació externa";

$gl_caption['c_book_owner'] = "Reservat per";