<?

/* 13-12-14 

  Actualitzacions mossos
 
  Actualització Custumer

 */


if (Db::get_first("SELECT * FROM all__menu WHERE menu_id='100'" ) && !Db::get_first("SELECT * FROM all__menu WHERE menu_id='190'" )) {	
	
	// li dono els mateixos permisos traduccions de inmo
	$read_write = Db::get_row("SELECT read1,write1 FROM all__menu WHERE menu_id = 114");
	$read = $read_write['read1'];
	$write = $read_write['write1'];
	
	$update_sql .= "
		INSERT INTO `all__menu` (`menu_id`, `menu_group`, `tool`, `tool_section`, `action`, `parent_id`, `toolmode_id`, `variable`, `ordre`, `link`, `process`, `bin`, `read1`, `write1`) VALUES (190, 'booking', 'booking', 'police', '', 150, 1, 'BOOKING_MENU_POLICE', 4, '', '', 0, '$read', '$write');
		INSERT INTO `all__menu` (`menu_id`, `menu_group`, `tool`, `tool_section`, `action`, `parent_id`, `toolmode_id`, `variable`, `ordre`, `link`, `process`, `bin`, `read1`, `write1`) VALUES (192, 'booking', 'booking', 'police', 'list_records', 190, 1, 'BOOKING_MENU_POLICE_LIST', 2, '', '', 0, '$read', '$write');
";	
	
}
else{
	if (Db::get_first("SELECT * FROM all__menu WHERE menu_id='190'" ))
		trigger_error('Ja existeixen els menus de mossos');
}


if (is_table('custumer__custumer') && !is_field('custumer__custumer', 'billing')) {
		
		
		$update_sql .= "
			ALTER TABLE `custumer__custumer`
				ADD COLUMN `billing` ENUM('company','citizen') NULL DEFAULT 'citizen' AFTER `comta`;";		
}
else{
	if (is_table('custumer__custumer')) {		
		trigger_error('Ja existeixen el field custumer__custumer.billing');
	}
}


if (is_table('booking__configadmin') && !Db::get_first("SELECT * FROM booking__configadmin WHERE name='codi_establiment'" )) {	
	
	$update_sql .= "
		INSERT INTO `booking__configadmin` (`name`, `value`) VALUES ('codi_establiment', '1111111111');
		INSERT INTO `booking__configadmin` (`name`, `value`) VALUES ('nom_establiment', '');
		";	
}
else{
	if (is_table('booking__configadmin')) {		
		trigger_error('Ja existeixen el name codi_establiment, nom_establiment');
	}
}

?>