<!DOCTYPE html>
<html lang="<?=LANGUAGE_CODE?>">
	<head>
		
		<?/* 
		Nomes per webs responsive:		
		
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
		<meta name="format-detection" content="telephone=no" />
		
		*/?>
		
		
		<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>	
		
		<?=load_css('general',
			'/common/jscripts/prettyphoto3.1.4/css/prettyPhoto.css,
			/common/jscripts/jquery-ui-1.8.20/css/ui-lightness/jquery-ui-1.8.20.custom.css')?>
		<?=get_header($this)?>
		
		<?/* JSCRIPT */?>		
		<script type="text/javascript">
		
			var a_vars = Array();var pagename='<?=addslashes($title)?>';var phpmyvisitesSite = 1;var phpmyvisitesURL = "<?=HOST_URL?>common/includes/phpmv2/phpmyvisites.php";
			
			var gl_language = '<?=$language?>';
			var gl_is_home = <?=$is_home?'true':'false'?>;	
			var gl_tool = '<?=$tool?>';	
			var gl_tool_section = '<?=$tool_section?>';	
			var gl_action = '<?=$action?>';	
			var gl_page_text_id = '<?=$page_text_id?>';
			var gl_dir_templates = '<?=DIR_TEMPLATES?>';
			$(function(){
				<?=$javascript?>
			});	
		</script>
		
		<?=load_jscript('
			/common/includes/phpmv2/phpmyvisites.js,
			/common/jscripts/prettyphoto3.1.4/js/jquery.prettyPhoto.js,
			/common/jscripts/caroufredsel/jquery.touchSwipe.min.js,
			/common/jscripts/caroufredsel/jquery.carouFredSel-6.2.1-packed.js,
			/common/jscripts/jquery.blockUI-2.35.min.js,
			/common/jscripts/jquery-ui-1.8.20/js/jquery-ui-1.8.20.datepicker.min.js,
			/common/jscripts/jquery-ui-1.8.20/languages/jquery.ui.datepicker-'.$language_code.'.js,
			/public/jscripts/hide_m.js,
			/admin/languages/'.$language.'.js,
			/admin/jscripts/validacio.js,
			/common/jscripts/functions.js,
			'.DIR_TEMPLATES.'jscripts/contact.js'
		)?>		

	</head>

<body class="clean">
<div id="content"><?if ($message):?><div id="message">
                    <?=$message?>
                    </div><?endif //message?><?=$content?></div>
					
		<iframe name="save_frame" id="save_frame" class="<?=$showiframe?>"></iframe>
		
		<script type="text/javascript" src="http://maps.google.com/maps/api/js?key=AIzaSyAaIGZEloZFoSY8TuTDyL27MDbuDvW5AoM"></script>
		<?/* FI JSCRIPT */?>
</body>
</html>
