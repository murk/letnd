<?php
include_once(DOCUMENT_ROOT . '/admin/modules/custumer/languages/'.LANGUAGE.'.php');

define('MLS_MENU_SEARCH','Recherches');
define('MLS_MENU_SEARCH_EXACT','Recherches exactes');
define('MLS_MENU_SEARCH_PROX','Recherches aproximatives');

$gl_caption['c_fiscalname'] = 'Nom procureurl';
$gl_caption['c_comercialname'] = 'Nom de entreprise';
$gl_caption['c_adress'] = 'Adresse';
$gl_caption['c_town'] = 'Ville';
$gl_caption['c_province'] = 'Province';
$gl_caption['c_zip'] = 'Code';
$gl_caption['c_country'] = 'Pays';
$gl_caption['c_respname'] = 'Prénom';
$gl_caption['c_surnames'] = 'Nom';
$gl_caption['c_phone1'] = 'Téléphone (1)';
$gl_caption['c_phone2'] = 'Téléphone (2)';
$gl_caption['c_fax'] = 'Fax';
$gl_caption['c_mail1'] = 'Mail';
$gl_caption['c_domain'] = 'Site web';

$gl_caption_companies['c_client_id2'] = 'Entreprise partagée';
$gl_caption_companies['c_add_company_title']='Ajouter Société MLS';
$gl_caption_companies['c_badd']='Ajouter entreprise';
$gl_caption_companies['c_nif']='NIF';
$gl_caption_companies['c_mlscode']='MLS ID';
$gl_caption_companies['c_permission']='État';
$gl_caption_companies['c_permission_askfor']='En attendant pour nous accepter';
$gl_caption_companies['c_permission_askedfor']='<span class="vert">Nous avons demandé la permission de partager<span>';
$gl_caption_companies['c_permission_denied']='<span class="vermell">On nous a refusé la permission de partager<span>';
$gl_caption_companies['c_permission_canreaccept']='Nous avons refusé la permission';
$gl_caption_companies['c_permission_accepted']='Partageant';
$gl_caption_companies['c_permission_']='Aucune relation';

$gl_caption ['c_add_company_button'] = 'Demander MLS';
$gl_caption ['c_accept_company_button']='Accepter à MLS';
$gl_caption ['c_deny_company_button']='Refuser MLS';
$gl_caption ['c_erase_shared_company_button']='Retirez du MLS';
//$gl_caption ['c_waiting_to_be_accepted_company_button']='Espérant être accepté à MLS';
$gl_caption ['c_denied_company_reaccept_company_button']='Accepter à MLS';
//$gl_caption['c_i_was_denied_company_button']='Cette société ne me permet pas les demandes à MLSS';


$gl_messages ['company_deleted']='Société éliminé du MLS.';
$gl_messages ['company_authorized']='Entreprise autorisé à partager MLS.';
$gl_messages ['company_waiting']='Requérant MLS envoyé, en attente de acceptation.';
$gl_messages ['company_denied']='Requérant du MLS refusée.';
$gl_messages ['company_reaccepted']='Entreprise autorisé à partager MLS';

$gl_caption ['c_missatge_add_company']='La société <a href = "%s">%s</a> vous a demandé de partager des propriétés avec MLS';
$gl_caption ['c_asunto_add_company'] = 'Partager avec des propriétés MLS';
$gl_caption ['c_missatge_authorized_company']='La société <a href = "%s">%s</a>  a permis de partager des propriétés avec MLS';
$gl_caption ['c_asunto_authorized_company'] = 'MLS autorisés';
$gl_caption ['c_missatge_denied_company']='La société <a href = "%s">%s</a>  a refusé de partager avec vous des propriétés MLS';
$gl_caption ['c_asunto_denied_company'] = 'Partager avec des propriétés MLS refuséa';
$gl_caption ['c_missatge_reaccepted_company']='La société <a href = "%s">%s</a>  a permis de partager des propriétés avec MLS';
$gl_caption ['c_asunto_reaccepted_company'] = 'Votre demande à partager MLS a été acceptée';
$gl_caption ['c_missatge_erase_shared_company']='La société <a href = "%s">%s</a>  a éliminé ses MLS avec vous';
$gl_caption ['c_asunto_erase_shared_company'] = 'MLS éliminé';

?>