<?
class ProjectsNew extends Module{

	function __construct(){
		parent::__construct();
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function get_records()
	{	    		
		$listing = new ListRecords($this);
		$listing->condition = "status = 'public'";
		$listing->order_by = 'ordre ASC';
		
		$category_id = R::id('category_id');
		$listing->set_var('listing_category_id',$category_id);	
			
		if ($category_id) $category = Db::get_first("
				SELECT category 
				FROM news__category_language
				WHERE language = '". LANGUAGE ."' 
				AND category_id = '" . $category_id ."'");
				
		else  $category = '';
		
		$listing->set_var('listing_category',$category);	
		
		if ($category_id) $listing->condition .= " AND category_id = '" . $category_id . "'";
		//$listing->call_function['content'] = 'add_dots';
		$listing->call('records_walk','url1,url1_name,url2,url2_name,content,content_list');
		$listing->order_by = 'ordre ASC, entered DESC, new_id';
		
		//$listing->call_function['description'] = 'add_dots';
		return $listing->list_records();
	}	
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{	  
		$show = new ShowForm($this);
		$show->condition = "status = 'public'";	
		$show->get_values();									
		return $show->show_form();		
	}	
}	
?>