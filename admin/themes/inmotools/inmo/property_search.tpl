<? if ($only_search_form):?>
<script language="JavaScript" src="/admin/modules/inmo/jscripts/functions.js?v=<?=$version?>"></script>
<?endif //only_search_form?>
<?if ($search_search):?>
<div class="search">
<h3><?=$c_search_title?></h3>  
<form name="search" method="get" action="/admin/"><table border="0" cellspacing="0" cellpadding="0">
				<tr valign="middle">		
						<td><?=$c_by_words?></td>
						<td><input name="action" type="hidden" value="list_records_search_search">
							<input name="menu_id" type="hidden" value="<?=$menu_id?>">
							<input name="q" type="text" size="30" value="<?=$q?>"></td><td><input type="submit" name="Submit" value="ok" class="boto-search"></td>
						<td>&nbsp;&nbsp;
							<?=$c_by_ref?>&nbsp;</td>	
				</tr>
			</table></form>
</div>
	<? if (!$only_search_form):?>
		<script type="text/javascript">
		$(function() {
				if (document.search.q) document.search.q.focus();
		});
		</script>
		<?endif //only_search_form?>
<?endif //search_search?>
<?if ($search_filter):?>
<div class="sep_dark"></div>
	<table width="100%" cellpadding="4" cellspacing="0">
		<tr>
			<td class="formCaptionTitle"><strong>
				<?=$c_filter_title?>
				</strong></td>
		</tr>
	</table>
<div class="content_container">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>          
		<td height="20" align="left" valign="bottom"><form name="search_filter" method="get" action="/admin/"><table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr> 
						<td class="searchGroupTop"><strong>
							<?=$c_type_title?><input name="menu_id" type="hidden" value="<?=$menu_id?>">
						</strong></td>
						<td><img src="/admin/themes/inmotools/images/spacer.gif" width="10" height="1" /></td>
						<td class="searchGroupTop"><strong>
							<?=$c_category_title?>
						</strong></td>
						<td><img src="/admin/themes/inmotools/images/spacer.gif" width="10" height="1" /></td>
						<td class="searchGroupTop"><strong>
							<?=$c_comarca_title?>
						</strong></td>
						<td><img src="/admin/themes/inmotools/images/spacer.gif" width="10" height="1" /></td>
						<td class="searchGroupTop"><strong>
							<?=$c_municipi_title?>
						</strong></td>
						<td><img src="/admin/themes/inmotools/images/spacer.gif" width="10" height="1" /></td>
						<td class="searchGroupTop"><strong>
							<?=$c_zone_title?>
						</strong></td>
						<td><img src="/admin/themes/inmotools/images/spacer.gif" width="10" height="1" /></td>
						<td class="searchGroupTop"><strong>
							<?=$c_status_title?>
						</strong></td>
						</tr>
					<tr>
						<td valign="top" class="searchGroupBottom"><table class="searchGroupOptions" width="100%" border="0" cellspacing="0" cellpadding="0">
							<?foreach($filter_tipus as $l):?>
							<tr class="searchItem<?=$l['class_name']?>" onmousedown="click_checkbox(this, 'Down', '<?=$l['tipus']?>','tipus')" onmouseover="click_checkbox(this, 'Over', '<?=$l['tipus']?>','tipus')" onmouseout="click_checkbox(this, 'Odd', '<?=$l['tipus']?>','tipus')" id="list_row_<?=$l['tipus']?>">
								<td><?=$l['tipus_lang']?>                </td>
								<td align="right"><input type="checkbox" name="tipus[<?=$l['tipus']?>]" value="<?=$l['tipus']?>" <?=$l['marked']?> onclick="return false" /></td>
							</tr>
							<?endforeach //$filter_tipus?>
						</table></td> 
						<td><img src="/admin/themes/inmotools/images/spacer.gif" width="10" height="1" /></td>
						<td class="searchGroupBottom" valign="top"><table class="searchGroupOptions" width="100%" border="0" cellspacing="0" cellpadding="0">
							<?foreach($filter_categories as $l):?>
							<tr class="searchItem<?=$l['class_name']?>" onmousedown="click_checkbox(this, 'Down', '<?=$l['category_id']?>','category_id')" onmouseover="click_checkbox(this, 'Over', '<?=$l['category_id']?>','category_id')" onmouseout="click_checkbox(this, 'Odd', '<?=$l['category_id']?>','category_id')" id="list_row_<?=$l['category_id']?>">
								<td><?=$l['category']?></td>
								<td align="right"><input type="checkbox" name="category_id[<?=$l['category_id']?>]" value="<?=$l['category_id']?>" <?=$l['marked']?> onclick="return false" /></td>
							</tr>
							<?endforeach //$filter_categories?>
						</table></td>
						<td><img src="/admin/themes/inmotools/images/spacer.gif" width="10" height="1" /></td>
						<td class="searchGroupBottom" valign="top"><table class="searchGroupOptions" width="100%" border="0" cellspacing="0" cellpadding="0">
							<?foreach($filter_comarques as $l):?>
							<tr class="searchItem<?=$l['class_name']?>" onmousedown="click_checkbox(this, 'Down', '<?=$l['comarca_id']?>','comarca_id')" onmouseover="click_checkbox(this, 'Over', '<?=$l['comarca_id']?>','comarca_id')" onmouseout="click_checkbox(this, 'Odd', '<?=$l['comarca_id']?>','comarca_id')" id="list_row_<?=$l['comarca_id']?>">
								<td><?=$l['comarca']?>                </td>
								<td align="right"><input type="checkbox" name="comarca_id[<?=$l['comarca_id']?>]" value="<?=$l['comarca_id']?>" <?=$l['marked']?> onclick="return false" /></td>
							</tr>
							<?endforeach //$filter_comarques?>
						</table></td>
						<td><img src="/admin/themes/inmotools/images/spacer.gif" width="10" height="1" /></td>
						<td class="searchGroupBottom" valign="top"><table class="searchGroupOptions" width="100%" border="0" cellspacing="0" cellpadding="0">
							<?foreach($filter_municipis as $l):?>
							<tr class="searchItem<?=$l['class_name']?>" onmousedown="click_checkbox(this, 'Down', '<?=$l['municipi_id']?>','municipi_id')" onmouseover="click_checkbox(this, 'Over', '<?=$l['municipi_id']?>','municipi_id')" onmouseout="click_checkbox(this, 'Odd', '<?=$l['municipi_id']?>','municipi_id')" id="list_row_<?=$l['municipi_id']?>">
								<td><?=$l['municipi']?>                </td>
								<td align="right"><input type="checkbox" name="municipi_id[<?=$l['municipi_id']?>]" value="<?=$l['municipi_id']?>" <?=$l['marked']?> onclick="return false" /></td>
							</tr>
							<?endforeach //$filter_municipis?>
						</table></td>
						<td><img src="/admin/themes/inmotools/images/spacer.gif" width="10" height="1" /></td>
						<td class="searchGroupBottom" valign="top"><table class="searchGroupOptions" width="100%" border="0" cellspacing="0" cellpadding="0">
							<?foreach($filter_zones as $l):?>
							<tr class="searchItem<?=$l['class_name']?>" onmousedown="click_checkbox(this, 'Down', '<?=$l['zone_id']?>','zone_id')" onmouseover="click_checkbox(this, 'Over', '<?=$l['zone_id']?>','zone_id')" onmouseout="click_checkbox(this, 'Odd', '<?=$l['zone_id']?>','zone_id')" id="list_row_<?=$l['zone_id']?>">
								<td><?=$l['zone']?>                </td>
								<td align="right"><input type="checkbox" name="zone_id[<?=$l['zone_id']?>]" value="<?=$l['zone_id']?>" <?=$l['marked']?> onclick="return false" /></td>
							</tr>
							<?endforeach //$filter_zones?>
						</table></td>
						<td><img src="/admin/themes/inmotools/images/spacer.gif" width="10" height="1" /></td>
						<td class="searchGroupBottom" valign="top"><table class="searchGroupOptions" width="100%" border="0" cellspacing="0" cellpadding="0">
							<?foreach($filter_status as $l):?>
							<tr class="searchItem<?=$l['class_name']?>" onmousedown="click_checkbox(this, 'Down', '<?=$l['status']?>','status')" onmouseover="click_checkbox(this, 'Over', '<?=$l['status']?>','status')" onmouseout="click_checkbox(this, 'Odd', '<?=$l['status']?>','status')" id="list_row_<?=$l['status']?>">
								<td><?=$l['status_lang']?>                </td>
								<td align="right"><input type="checkbox" name="status[<?=$l['status']?>]" value="<?=$l['status']?>" <?=$l['marked']?> onclick="return false" /></td>
							</tr>
							<?endforeach //$filter_status?>
						</table></td>
						</tr>
					<tr>
						<td valign="top">&nbsp;</td>
						<td valign="top">&nbsp;</td>
						<td valign="top">&nbsp;</td>
						<td valign="top">&nbsp;</td>
						<td valign="top">&nbsp;</td>
						<td valign="top">&nbsp;</td>
						<td valign="top">&nbsp;</td>
						<td valign="top">&nbsp;</td>
						<td valign="top">&nbsp;</td>
						<td valign="top">&nbsp;</td>
					</tr>
					<tr> 
						<td width="10%" class="searchGroupTop"><strong>
						<?=$c_price_title?>
						</strong></td>
						<td><img src="/admin/themes/inmotools/images/spacer.gif" width="10" height="1" /></td>
						<td width="10%" class="searchGroupTop"><strong>
						<?=$c_ordered_title?>
						</strong></td>
						<td><img src="/admin/themes/inmotools/images/spacer.gif" width="10" height="1" /></td>
						<td class="searchGroupTop"><strong>
							<?=$c_land_title?>
							</strong></td>
						<td><img src="/admin/themes/inmotools/images/spacer.gif" width="10" height="1" /></td>
						<td class="searchGroupTop"><strong>
							<?=$c_floor_space_title?>
							</strong></td>
						<td><img src="/admin/themes/inmotools/images/spacer.gif" width="10" height="1" /></td>
						<td class="searchGroupTop"><strong>
							<?=$c_custom_title?>
						</strong></td>
						<td><img src="/admin/themes/inmotools/images/spacer.gif" width="10" height="1" /></td>
						<td class="searchGroupTop">&nbsp;</td>
					</tr>
					<tr valign="top">
						<td class="searchGroupBottom" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="searchGroupOptions">
							<tr class="searchItem<?=$price1_class?>" onclick="click_input(this, 'Down', 'price1')" onmouseover="click_input(this, 'Over', 'price1')" onmouseout="click_input(this, 'Odd', 'price1')" id="tr_price1">
								<td width="2">De&nbsp;</td>
								<td nowrap><input name="price1" type="text" size="10" style="text-align: right" value="<?=$price1?>" onblur="click_input(this, 'blur','')" />&nbsp;<?=CURRENCY_NAME?></td>
								<td></td>
							</tr>
							<tr class="searchItem<?=$price2_class?>" onclick="click_input(this, 'Down', 'price2')" onmouseover="click_input(this, 'Over', 'price2')" onmouseout="click_input(this, 'Odd', 'price2')" id="tr_price2">
								<td align="right">a&nbsp;</td>
								<td nowrap><input name="price2" type="text" size="10" style="text-align: right"  value="<?=$price2?>" onblur="click_input(this, 'blur','')" />&nbsp;<?=CURRENCY_NAME?></td>
								<td></td>
							</tr>
						</table></td>
						<td><img src="/admin/themes/inmotools/images/spacer.gif" width="10" height="1" /></td>
						<td class="searchGroupBottom"><table class="searchGroupOptions" style="padding:0 12px;" width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td><?=$c_asc?></td>
									<td><?=$c_desc?></td>
								</tr>
								<?foreach($filter_order as $l): extract ($l)?>
								<tr>
									<td><?=$conta?>&nbsp;-&nbsp;</td>
									<td><?=$select?></td>
									<td align="center" class="searchItem<?=$l['asc_class_name']?>" onmousedown="click_radio(this, 'Down', 'asc','order_way[<?=$conta-1?>]')" onmouseover="click_radio(this, 'Over', 'asc','order_way[<?=$conta-1?>]')" onmouseout="click_radio(this, 'Odd', 'asc','order_way[<?=$conta-1?>]')" id="tr_order_way[<?=$conta-1?>]_asc"><?=$asc?></td>
									<td align="center" class="searchItem<?=$l['desc_class_name']?>" onmousedown="click_radio(this, 'Down', 'desc','order_way[<?=$conta-1?>]')" onmouseover="click_radio(this, 'Over', 'desc','order_way[<?=$conta-1?>]')" onmouseout="click_radio(this, 'Odd', 'desc','order_way[<?=$conta-1?>]')" id="tr_order_way[<?=$conta-1?>]_desc"><?=$desc?></td>
								</tr>
								<?endforeach //$filter_order?>
						</table></td>
						<td><img src="/admin/themes/inmotools/images/spacer.gif" width="10" height="1" /></td>
						<td class="searchGroupBottom"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="searchGroupOptions">
							<tr class="searchItem<?=$land1_class?>" onclick="click_input(this, 'Down', 'land1')" onmouseover="click_input(this, 'Over', 'land1')" onmouseout="click_input(this, 'Odd', 'land1')" id="tr_land1">
								<td width="2">De&nbsp;</td>
								<td><input name="land1" type="text" size="10" style="text-align: right" value="<?=$land1?>" onblur="click_input(this, 'blur','')" /></td>
								</tr>
							<tr class="searchItem<?=$land2_class?>" onclick="click_input(this, 'Down', 'land2')" onmouseover="click_input(this, 'Over', 'land2')" onmouseout="click_input(this, 'Odd', 'land2')" id="tr_land2">
								<td align="right">a&nbsp;</td>
								<td><input name="land2" type="text" size="10" style="text-align: right" value="<?=$land2?>" onblur="click_input(this, 'blur','')" /></td>
								</tr>
							<tr>
								<td>&nbsp;</td>
								<td><select name="land_units">
									<option value="m2">
									<?=$c_m2?>
									</option>
									<option value="ha"<?=$land_units2?>>
									<?=$c_ha?>
									</option>
								</select></td>
								</tr>
						</table></td>
						<td><img src="/admin/themes/inmotools/images/spacer.gif" width="10" height="1" /></td>
						<td class="searchGroupBottom"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="searchGroupOptions">
							<tr class="searchItem<?=$floor_space1_class?>" onclick="click_input(this, 'Down', 'floor_space1')" onmouseover="click_input(this, 'Over', 'floor_space1')" onmouseout="click_input(this, 'Odd', 'floor_space1')" id="tr_floor_space1">
								<td width="2">De&nbsp;</td>
								<td><input name="floor_space1" type="text" size="10" style="text-align: right" value="<?=$floor_space1?>" onblur="click_input(this, 'blur','')" /></td>
							</tr>
							<tr class="searchItem<?=$floor_space2_class?>" onclick="click_input(this, 'Down', 'floor_space2')" onmouseover="click_input(this, 'Over', 'floor_space2')" onmouseout="click_input(this, 'Odd', 'floor_space2')" id="tr_floor_space2">
								<td align="right">a&nbsp;</td>
								<td><input name="floor_space2" type="text" size="10" style="text-align: right" value="<?=$floor_space2?>" onblur="click_input(this, 'blur','')" /></td>
							</tr>
						</table></td> 
						<td><img src="/admin/themes/inmotools/images/spacer.gif" width="10" height="1" /></td>
						<td class="searchGroupBottom"><table class="searchGroupOptions" width="100%" border="0" cellspacing="0" cellpadding="0">
							<?foreach($filter_customs as $l):?>
							<tr class="searchItem<?=$l['class_name']?>" onmousedown="click_checkbox(this, 'Down', '<?=$l['custom_id']?>','custom_id')" onmouseover="click_checkbox(this, 'Over', '<?=$l['custom_id']?>','custom_id')" onmouseout="click_checkbox(this, 'Odd', '<?=$l['custom_id']?>','custom_id')" id="list_row_<?=$l['custom_id']?>">
								<td><?=$l['name']?>                </td>
								<td align="right"><?=$l['input']?></td>
							</tr>
							<?endforeach //$filter_users?></table></td>
						<td><img src="/admin/themes/inmotools/images/spacer.gif" width="10" height="1" /></td>
						<td class="">&nbsp;</td>
					</tr>
				</table>
				<div class="separador"></div>
				<div class="listTitle2" style="margin-bottom:4px;">
					<strong><?=$c_filter_title2?></strong>
				</div>
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr> 
					<?if($filter_users):?>
						<td width="10%" class="searchGroupTop"><strong>
						<?=$c_user_title?>
						</strong></td><?endif //filter_users?>
						<td><img src="/admin/themes/inmotools/images/spacer.gif" width="10" height="1" /></td>
						<td width="10%" class="searchGroupTop"><strong>
						<?=$c_images_title?>
						</strong></td>
						<td><img src="/admin/themes/inmotools/images/spacer.gif" width="10" height="1" /></td>
						<td class="searchGroupTop"><strong>
						<?=$c_description_title?>
						</strong></td>
						<td><img src="/admin/themes/inmotools/images/spacer.gif" width="10" height="1" /></td>
						<td class="searchGroupTop">&nbsp;</td>
						<td><img src="/admin/themes/inmotools/images/spacer.gif" width="10" height="1" /></td>
						<td class="searchGroupTop">&nbsp;</td>
						<td><img src="/admin/themes/inmotools/images/spacer.gif" width="10" height="1" /></td>
						<td class="searchGroupTop">&nbsp;</td>
					</tr>
					<tr valign="top">
					
					<?if($filter_users):?>
						<td width="16%" valign="top" class="searchGroupBottom"><table class="searchGroupOptions" width="100%" border="0" cellspacing="0" cellpadding="0">
							<?foreach($filter_users as $l):?>
							<tr class="searchItem<?=$l['class_name']?>" onmousedown="click_checkbox(this, 'Down', '<?=$l['user_id']?>','user_id')" onmouseover="click_checkbox(this, 'Over', '<?=$l['user_id']?>','user_id')" onmouseout="click_checkbox(this, 'Odd', '<?=$l['user_id']?>','user_id')" id="list_row_<?=$l['user_id']?>">
								<td><?=$l['name']?> <?=$l['surname']?>                </td>
								<td align="right"><input type="checkbox" name="user_id[<?=$l['user_id']?>]" value="<?=$l['user_id']?>" <?=$l['marked']?> onclick="return false" /></td>
							</tr>
							<?endforeach //$filter_users?></table></td>
					<?endif //filter_users?>	
							
							
						<td><img src="/admin/themes/inmotools/images/spacer.gif" width="10" height="1" /></td>
						<td width="16%" class="searchGroupBottom"><table class="searchGroupOptions" width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr class="searchItem<?=$has_images_class?>" onmousedown="click_radio(this, 'Down', '1','images')" onmouseover="click_radio(this, 'Over', '1','images')" onmouseout="click_radio(this, 'Odd', '1','images')" id="tr_images_2">
								<td><?=$c_has_images?></td>
								<td align="right"><input type="radio" name="images" value="1" <?=$has_images?> onclick="return false" /></td>
							</tr>
							<tr class="searchItem<?=$has_no_images_class?>" onmousedown="click_radio(this, 'Down', '0','images')" onmouseover="click_radio(this, 'Over', '0','images')" onmouseout="click_radio(this, 'Odd', '0','images')" id="tr_images_3">
								<td><?=$c_has_no_images?></td>
								<td align="right"><input type="radio" name="images" value="0" <?=$has_no_images?> onclick="return false" /></td>
							</tr>
						</table></td>
						<td><img src="/admin/themes/inmotools/images/spacer.gif" width="10" height="1" /></td>
						<td width="16%" class="searchGroupBottom"><table class="searchGroupOptions" width="100%" border="0" cellspacing="0" cellpadding="0">
							<?foreach($filter_description as $l):?>
							<tr class="searchItem<?=$l['class_name']?>" onmousedown="click_checkbox(this, 'Down', '<?=$l['code']?>','description')" onmouseover="click_checkbox(this, 'Over', '<?=$l['code']?>','description')" onmouseout="click_checkbox(this, 'Odd', '<?=$l['code']?>','description')">
								<td><?=$l['language']?></td>
								<td align="right"><input type="checkbox" name="description[<?=$l['code']?>]" value="<?=$l['code']?>" <?=$l['marked']?>  onclick="return false" /></td>
							</tr>
							<?endforeach //$filter_description?>
						</table></td>
						<td><img src="/admin/themes/inmotools/images/spacer.gif" width="10" height="1" /></td>
						<td width="16%" class="">&nbsp;</td> 
						<td><img src="/admin/themes/inmotools/images/spacer.gif" width="10" height="1" /></td>
						<td width="16%" class="">&nbsp;</td>
						<td><img src="/admin/themes/inmotools/images/spacer.gif" width="10" height="1" /></td>
						<td width="16%" class="">&nbsp;</td>
					</tr>
				</table>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td><input name="action" type="hidden" value="list_records_search_filter" />
						<input type="submit" name="Submit" value="<?=$c_show_results?>" class="btn_form_save" /></td>
				</tr>
			</table></form></td>
	</tr>
</table>
</div>
<?endif //search_filter?>
<?if ($search_list):?>
<div class="sep_dark"></div>
	<table width="100%" cellpadding="4" cellspacing="0">
		<tr>
			<td class="formCaptionTitle"><strong>
				<?=$c_list_title?>
				</strong></td>
		</tr>
	</table>
<div class="content_container">
<table width="100%" border="0" cellspacing="0" cellpadding="0"> 
	<tr> 
		<td align="left" valign="bottom"> <table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr valign="top"> 
					<td class="searchGroupTop"><strong><?=$c_category_title?></strong></td>
					<td><img src="/admin/themes/inmotools/images/spacer.gif" width="10" height="1" /></td>
					<td class="searchGroupTop"><strong><?=$c_zone_title?></strong></td>
					<td><img src="/admin/themes/inmotools/images/spacer.gif" width="10" height="1" /></td>
					<td class="searchGroupTop"><strong><?=$c_status_title?></strong></td>
					<td><img src="/admin/themes/inmotools/images/spacer.gif" width="10" height="1" /></td>
					<td class="searchGroupTop"><strong><?=$c_price_title?></strong></td>
					<td><img src="/admin/themes/inmotools/images/spacer.gif" width="10" height="1" /></td>
					<td class="searchGroupTop"><strong><?=$c_images_title?></strong></td>
				</tr>
				<tr valign="top"> 
					<td class="searchGroupBottom">
						<table class="searchGroupOptions" width="100%" border="0" cellspacing="0" cellpadding="0">
							<?foreach($menu_categories as $l):?>               
							<tr> 
								<td><a href="/admin/?category_id=<?=$l['category_id']?>&action=list_records_search_list" class="menulnk<?=$l['marked']?>"><?=$l['category']?></a></td>
							</tr>
							<?endforeach //$menu_categories?>
					</table></td>
					<td><img src="/admin/themes/inmotools/images/spacer.gif" width="10" height="1" /></td>
					<td class="searchGroupBottom"> <table class="searchGroupOptions" width="100%" border="0" cellspacing="0" cellpadding="0">
							<?foreach($menu_comarques as $l):?>
							<tr> 
								<td><a href="/admin/?comarca_id=<?=$l['comarca_id']?>&action=list_records_search_list" class="menulnk<?=$l['marked']?>"><?=$l['comarca']?></a></td>
							</tr>
					<?endforeach //$menu_comarques?>
			</table></td>
					<td><img src="/admin/themes/inmotools/images/spacer.gif" width="10" height="1" /></td>
					<td class="searchGroupBottom">
							<table class="searchGroupOptions" width="100%" border="0" cellspacing="0" cellpadding="0">
								<?foreach($menu_status as $l):?>
								<tr> 
									<td><a href="/admin/?status=<?=$l['status']?>&action=list_records_search_list" class="menulnk<?=$l['marked']?>"><?=$l['status_lang']?></a></td>
								</tr>
					<?endforeach //$menu_status?> </table></td>
					<td><img src="/admin/themes/inmotools/images/spacer.gif" width="10" height="1" /></td>
					<td class="searchGroupBottom"> <table class="searchGroupOptions" width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr> 
								<td><a href="/admin/?price1=<?=$menu_first_price?>&price2=<?=$menu_second_price?>&action=list_records_search_list" class="menulnk<?=$menu_first_price_marked?>">Fins 
									a <?=$menu_second_price?> <?=CURRENCY_NAME?></a></td>
							</tr>
							<?foreach($menu_prices as $l):?>
							<tr> 
								<td><a href="/admin/?price1=<?=$l['price']?>&price2=<?=$l['next_price']?>&action=list_records_search_list" class="menulnk<?=$l['marked']?>">De 
									<?=$l['price']?> a <?=$l['next_price']?> <?=CURRENCY_NAME?></a></td>
							</tr>
							<?endforeach //$menu_prices?> 
							<tr> 
								<td><a href="/admin/?price1=<?=$menu_before_last_price?>&action=list_records_search_list" class="menulnk<?=$menu_last_price_marked?>">Més 
									de <?=$menu_before_last_price?> <?=CURRENCY_NAME?></a></td>
							</tr>
						</table></td>
					<td><img src="/admin/themes/inmotools/images/spacer.gif" width="10" height="1" /></td>
					<td class="searchGroupBottom"><table class="searchGroupOptions" width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr> 
								<td><a href="/admin/?images=1&action=list_records_search_list" class="menulnk<?=$menu_first_images_marked?>"><?=$c_has_images?></a></td>
							</tr>
							<tr> 
								<td><a href="/admin/?images=0&action=list_records_search_list" class="menulnk<?=$menu_second_images_marked?>"><?=$c_has_no_images?></a></td>
							</tr></table></td>
				</tr>
			</table></td>
	</tr>
</table>
</div>
<?endif //search_list?>