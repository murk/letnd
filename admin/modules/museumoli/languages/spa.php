<?php
// menus eina
if (!defined('MUSEUMOLI_MENU_VISITA')){
	define('MUSEUMOLI_MENU_VISITA','Visites');
	define('MUSEUMOLI_MENU_VISITA_NEW','Nova visita');
	define('MUSEUMOLI_MENU_VISITA_LIST','Llistar visites');
	define('MUSEUMOLI_MENU_VISITA_LIST_BIN','Paperera de reciclatge');
	
	define('MUSEUMOLI_MENU_VISITATALLER_NEW','Nova visita-taller');
	define('MUSEUMOLI_MENU_VISITATALLER_LIST','Llistar visites-taller');
	define('MUSEUMOLI_MENU_VISITATALLER_LIST_BIN','Paperera de reciclatge');
	
	define('MUSEUMOLI_MENU_VISITANT','Tipus de visitants');
	define('MUSEUMOLI_MENU_VISITANT_NEW','Nou tipus de visitant');
	define('MUSEUMOLI_MENU_VISITANT_LIST','Llistar tipus visitants');
	define('MUSEUMOLI_MENU_VISITANT_LIST_BIN','Paperera de reciclatge');
	
	define('MUSEUMOLI_MENU_CURS','Cursos');
	define('MUSEUMOLI_MENU_CURS_NEW','Nou curs');
	define('MUSEUMOLI_MENU_CURS_LIST','Llistar cursos');
	define('MUSEUMOLI_MENU_CURS_LIST_BIN','Paperera de reciclatge');
	
	define('MUSEUMOLI_MENU_DOCUMENTACIOCATEGORY_NEW','Nova categoria');
	define('MUSEUMOLI_MENU_DOCUMENTACIOCATEGORY_LIST','Llistar categories');
	define('MUSEUMOLI_MENU_DOCUMENTACIOCATEGORY_LIST_BIN','Paperera de reciclatge');
	
	define('MUSEUMOLI_MENU_DOCUMENTACIO','Documentació');
	define('MUSEUMOLI_MENU_DOCUMENTACIO_NEW','Nou document');
	define('MUSEUMOLI_MENU_DOCUMENTACIO_LIST','Llistar documents');
	define('MUSEUMOLI_MENU_DOCUMENTACIO_LIST_BIN','Paperera de reciclatge');
	
	define('MUSEUMOLI_MENU_COLABORADOR','Colaboradors');
	define('MUSEUMOLI_MENU_COLABORADOR_NEW','Nou colaborador');
	define('MUSEUMOLI_MENU_COLABORADOR_LIST','Llistar colaboradors');
	define('MUSEUMOLI_MENU_COLABORADOR_LIST_BIN','Paperera de reciclatge');
	
	define('MUSEUMOLI_MENU_EXPOSICIOCATEGORY_NEW','Nova categoria');
	define('MUSEUMOLI_MENU_EXPOSICIOCATEGORY_LIST','Llistar categories');
	define('MUSEUMOLI_MENU_EXPOSICIOCATEGORY_LIST_BIN','Paperera de reciclatge');
	
	define('MUSEUMOLI_MENU_EXPOSICIO','Exposicions');
	define('MUSEUMOLI_MENU_EXPOSICIO_NEW','Nova exposició');
	define('MUSEUMOLI_MENU_EXPOSICIO_LIST','Llistar exposicions');
	define('MUSEUMOLI_MENU_EXPOSICIO_LIST_BIN','Paperera de reciclatge');
	
	define('MUSEUMOLI_MENU_SENTENCE','Frases Home');
	define('MUSEUMOLI_MENU_SENTENCE_NEW','Nova frase');
	define('MUSEUMOLI_MENU_SENTENCE_LIST','Llistar frases');
	define('MUSEUMOLI_MENU_SENTENCE_LIST_BIN','Paperera');
	
	define('MUSEUMOLI_MENU_NEW', 'Notícies');
	define('MUSEUMOLI_MENU_NEW_NEW', 'Entrar nova notícia');
	define('MUSEUMOLI_MENU_NEW_LIST', 'Llistar i editar notícies');
	define('MUSEUMOLI_MENU_NEW_BIN', 'Paperera de reciclatge');
	define('MUSEUMOLI_MENU_CATEGORY', 'Categories');
	define('MUSEUMOLI_MENU_CATEGORY_NEW', 'Entrar nova categoria');
	define('MUSEUMOLI_MENU_CATEGORY_LIST', 'Llistar categories');
}

$gl_messages['no_delete_taller']='El registre 12 i 19 no es poden esborrar, pertanyen a "Visites+taller" i "Cursos i seminaris"';

// captions seccio
$gl_caption['c_visitant_id'] = '';
$gl_caption['c_videoframe'] = 'Vídeo';
$gl_caption['c_preu'] = 'Precio por persona';
$gl_caption['c_preu_alumne'] = 'Precio por alumno';
$gl_caption['c_url1'] = 'URL 1';
$gl_caption['c_url2'] = 'URL 2';
$gl_caption['c_status'] = 'Estado';
$gl_caption['c_parent_module'] = '';
$gl_caption['c_entered'] = 'Creado';
$gl_caption['c_ordre'] = 'Orden';
$gl_caption['c_bin'] = '';
$gl_caption['c_visitant_id'] = 'Tipo de visitante';
$gl_caption['c_visitant'] = 'Tipo de visitante';
$gl_caption['c_visita'] = 'Visita';
$gl_caption_exposicio['c_visita'] = 'Exposición';
$gl_caption_curs['c_visita'] = 'Curso';
$gl_caption_colaborador['c_visita'] = 'Nombre colaborador';
$gl_caption['c_datatext'] = 'Horario';
$gl_caption['c_year'] = 'Anyo';
$gl_caption_exposicio['c_datatext'] = 'Inauguración';
$gl_caption['c_mes_info'] = 'Información general';
$gl_caption_curs['c_mes_info'] = 'Programa';
$gl_caption_colaborador['c_mes_info'] = 'Otra información';
$gl_caption['c_reserva'] = 'Reservas';
$gl_caption['c_documentacio'] = 'Documentación';
$gl_caption['c_nivell_educatiu'] = 'Nivel educativo';
$gl_caption['c_duracio'] = 'Durada';
$gl_caption['c_inscripcio'] = 'Inscripción';
$gl_caption['c_tipus_curs'] = 'Tipo';
$gl_caption['c_tipus_curs_curs'] = 'Cursos y talleres';
$gl_caption['c_tipus_curs_seminari'] = 'Seminarios';
$gl_caption['c_tipus_curs_moli'] = 'Molino - Arte papel';
$gl_caption['c_tipus_colaborador'] = 'Tipo';
$gl_caption['c_tipus_colaborador_artista'] = 'Artistas';
$gl_caption['c_tipus_colaborador_entitat'] = 'Entitadades';
$gl_caption['c_tipus_colaborador_empresa'] = 'Empresas y instituciones';


$gl_caption['c_prepare'] = 'En preparación';
$gl_caption['c_review'] = 'Para revisar';
$gl_caption['c_public'] = 'Público';
$gl_caption['c_archived'] = 'Archivado';
$gl_caption['c_new'] = 'Noticia';
$gl_caption['c_title'] = 'Título';
$gl_caption['c_subtitle'] = 'Subtítulo';
$gl_caption['c_content'] = 'Descripción en la ficha';
$gl_caption['c_status'] = 'Estado';
$gl_caption['c_category_id'] = 'Categoría';
$gl_caption['c_filter_category_id'] = 'Todas las categorías';
$gl_caption['c_filter_status'] = 'Todos los estados';
$gl_caption['c_ordre'] = 'Orden';


$gl_caption['c_category_id'] = 'Categoría';
$gl_caption['c_ordre'] = 'Orden';
$gl_caption_image['c_name'] = 'Nombre';
$gl_caption['c_url1_name']='Nombre URL(1)';
$gl_caption['c_url1']='URL(1)';
$gl_caption['c_url2_name']='Nombre URL(2)';
$gl_caption['c_url2']='URL(2)';
$gl_caption['c_videoframe']='Vídeo (youtube, metacafe...)';
$gl_caption['c_file']='Documentación';
$gl_caption_curs['c_file_download']='Descargar programa';
$gl_caption['c_in_home']='Sale en la home';
$gl_caption['c_content_list']='Descripción en el índice de visitas';

$gl_caption['c_sentence_title']='Título';
$gl_caption['c_sentence']='Frase';
$gl_caption['c_sentence_url_name']='Nombre del enlace';
$gl_caption['c_sentence_url']='Enlace';

$gl_caption['c_mes_informacio']='Más información';
?>
