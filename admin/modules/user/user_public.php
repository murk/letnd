<?php
/**
 * Modul
 *
 */
class UserPublic extends Module{
	var $tool = 'user';
	var $tool_section = 'public';

	function __construct(){
		parent::__construct();
	}

	function list_records()
	{
		//echo "ererererere";
	    $listing = new ListRecords;
	    $listing->list_records();
	}

	function show_form()
	{
	    $show = new ShowForm;
	    $show->show_form();
	}

	function save_rows()
	{
	    $save_rows = new SaveRows;
	    $save_rows->save();
	}

	function write_record()
	{
	    $writerec = new SaveRows;
	    $writerec->save();
	}

	function manage_images()
	{
	    $image_manager = new ImageManager;
	    $image_manager->execute();
	}
}
?>