<?
if (!defined('USER_LOGIN_TITLE')) {

	define('USER_LOGIN_TITLE','Benvingut a Letnd');
	define('USER_LOGOUT_TITLE','Estàs desconnectat');
	define('USER_LOGIN_TEXT','Entra el login i password i prem acceptar');
	define('USER_LOGIN_AGAIN_TEXT','La sessió ha caducat. Torna a entrar el login i password');
	define('USER_LOGOUT_TEXT','Per tornar a connectar entra el login i password');




	// menus eina
	define('USER_MENU_USER','Usuaris');
	define('USER_MENU_USER_NEW','Crear Usuari');
	define('USER_MENU_USER_LIST','Llistar Usuaris');
	define ('USER_MENU_USER_BIN','Paperera');
	define('USER_MENU_CONCESSION','Privilegis');
	define('USER_MENU_CONCESSION_LIST','Assignar Privilegis');
	define ('USER_MENU_GROUP','Grups d\'usuaris');
	define('USER_MENU_GROUP_NEW','Crear grup');
	define('USER_MENU_GROUP_LIST','Llistar grups');
	define ('USER_MENU_GROUP_BIN','Paperera');
	define ('USER_MENU_LOG','Historial');
	define ('USER_MENU_LOG_LIST','Llistar històrics');
	define ('USER_LOGIN_BROWSER_INCORRECT','Per poder utilitzar aquest programa es necessita almenys Internet Explorer 8, Firefox 3.6, Safari 5 o Google Chrome 7<br><br>Estàs utilitzant %s versió %s');
	define('USER_MENU_ONEUSER', USER_MENU_USER);
	define('USER_MENU_ONEUSER_EDIT', 'Editar dades');
	define('USER_COMMENT_ONEUSER', 'Només editar dades propies de l\'usuari');

	define('USER_MENU_PUBLIC','Usuaris public');
	define('USER_MENU_PUBLIC_NEW','Crear Usuari');
	define('USER_MENU_PUBLIC_LIST','Llistar Usuaris');
	define ('USER_MENU_PUBLIC_BIN','Paperera');
	
}

$users_login_form['c_login'] = 'Login';
$users_login_form['c_password'] = 'Password';
$users_login_form['c_send'] = 'Acceptar';



// captions seccio
$gl_caption_concession['c_tool'] = 'Modul';
$gl_caption_concession['c_tool_section'] = 'Secció';
$gl_caption_concession['c_read1'] = 'Lectura';
$gl_caption_concession['c_write1'] = 'Escritura';
$gl_caption_concession['c_group_id'] = '';
$gl_caption_concession['c_comment'] = '';
$gl_caption_concession['c_bin'] = '';
$gl_caption_concession['c_search1'] = 'Tria grup:';
$gl_caption_group['c_name']= "Nom del grup";
$gl_caption_group['c_view_users']= "Llistar usuaris";
$gl_caption_group['c_show_html_editor']= "Permetre html en WYSIWYG";
$gl_caption_group['c_show_seo']= "Permetre SEO";
$gl_caption_group['c_is_editable']= "Editable";
$gl_caption_group['c_show_seo_0']= "No";
$gl_caption_group['c_show_seo_1']= "Sí";
$gl_caption_user['c_group_id']="Nom del grup";
$gl_caption_user['c_name']= "Nom";
$gl_caption_user['c_surname']= "Cognoms";
$gl_caption_user['c_login']= "Nom d'usuari";
$gl_caption_user['c_passw']= "Contrasenya";
$gl_caption_user['c_passw_repeat']= "Repetir contrasenya";
$gl_caption_user['c_email']= "E-mail";
$gl_caption_user['c_email_repeat']= "Repetir E-mail";
$gl_caption_log['c_ip']= "Ip del ordinador";
$gl_caption_log['c_user_id']= "Usuari";
$gl_caption_log['c_date']= "Data";
$gl_caption_log['c_login']= "Hora d'entrada";
$gl_caption_log['c_logout']= "Hora de sortida";


$gl_messages['Error_no_groups'] = "Error!, Abans de crear usuaris, s'han de crear grups.";
$gl_messages['user_exist_del']='Error!, El grup: <b>"%s"</b> conté usuaris actius, no es pot esborrar.';
$gl_messages['user_admin_del']='Error!, El grup: <b>"%s"</b> no es pot esborrar.';
$gl_messages['add_exists']='Ja existeix un registre amb aquest nom d\'usuari.';

// NO TRADUIT

$gl_caption_public['c_group_id']="Nom del grup";
$gl_caption_public['c_name']= "Nom";
$gl_caption_public['c_surname']= "Cognoms";
$gl_caption_public['c_login']= "Nom d'usuari";
$gl_caption_public['c_passw']= "Contrassenya";
$gl_caption_public['c_email']= "E-mail";
// FI NO TRADUIT

$gl_caption_user['c_phone1'] = 'Principal';
$gl_caption_user['c_phone2'] = 'Feina';
$gl_caption_user['c_phone4'] = 'Privat';
$gl_caption_user['c_phone3'] = 'Fax';
$gl_caption_user['c_form_level2_title_phone1'] = 'Telèfons';

$gl_caption_user['c_language_id'] = 'Idiomes';
$gl_caption_user['c_description'] = 'Descripció';
$gl_caption_user['c_form_level2_title_description'] = 'Descripcions';
$gl_caption_user['c_status'] = 'Estat';
$gl_caption_user['c_status_private'] = 'Privat';
$gl_caption_user['c_status_public'] = 'Públic';

$gl_caption_user['c_edit_map'] = 'Mapa';
$gl_caption_user['c_form_map_title'] = 'Ubicació de l\'agent';
$gl_caption_property['c_save_map'] = 'Guardar ubicació';
$gl_caption_user['c_save_map'] = 'Guardar ubicació';
$gl_caption_user['c_latitude'] = 'Latitud';
$gl_caption_user['c_longitude'] = 'Longitud';
$gl_caption_user ['c_center_longitude'] = 'Longitud centre mapa';
$gl_caption_user ['c_center_latitude'] = 'Latitud centre mapa';
$gl_caption_user['c_zoom'] = 'Zoom';

$gl_caption_user ['c_center_coords'] = 'Centre';
$gl_caption_user['c_click_map'] = 'Fés clic en un punt del mapa per establir la situació';
$gl_messages['point_saved'] = 'Nova ubicació guardada correctament';
?>