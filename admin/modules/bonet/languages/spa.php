<?php
if (!defined('API_MENU_AGENCIA')){
	// menus eina	
	define('BONET_MENU_AGENCIA','Agències');
	define('BONET_MENU_AGENCIA_NEW','Nova agència');
	define('BONET_MENU_AGENCIA_LIST','Llistar agències');
	define('BONET_MENU_AGENCIA_BIN','Paperera de reciclatge');

	define('BONET_MENU_ONEAGENCIA','Agència');
	define('BONET_MENU_ONEAGENCIA_EDIT','Editar agència');
	define('BONET_MENU_ONEAGENCIA_LIST','Editar agents');
	define('BONET_MENU_ONEAGENCIA_LIST_FILES','La meva carpeta');
	define('BONET_COMMENT_ONEAGENCIA','Editar agència pròpia i agents pel grup "agents"');

	define('BONET_MENU_AGENTENQUESTA','Enquestes');
	define('BONET_MENU_AGENTENQUESTA_NEW','Enquesta de satisfacció');
	define('BONET_COMMENT_AGENTENQUESTA','Enquesta pel grup "agents"');

	define('BONET_MENU_AGENT','Agents');
	define('BONET_MENU_AGENT_NEW','Nou agent');
	define('BONET_MENU_AGENT_LIST','Llistar agents');
	define('BONET_MENU_AGENT_BIN','Paperera de reciclatge');

	define('BONET_MENU_AGENT_FILECAT_NEW','Nova categoria arxius');
	define('BONET_MENU_AGENT_FILECAT_LIST','Llistar categories arxius');

	define('BONET_MENU_SPECIALITY','Especialitats');
	define('BONET_MENU_SPECIALITY_NEW','Nova especialitat');
	define('BONET_MENU_SPECIALITY_LIST','Llistar especialitats');
	define('BONET_MENU_SPECIALITY_BIN','Paperera de reciclatge');

	define('BONET_MENU_PAGINA','Edició de pàgines');
	define('BONET_MENU_PAGINA_NEW','Entrar nova pàgina');
	define('BONET_MENU_PAGINA_NEW_SECOND','Entrar nova pàgina secundària');
	define('BONET_MENU_PAGINA_LIST','Llistar pàgines');
	define('BONET_MENU_PAGINA_LIST_SECOND','Llistar pàgines secundàries');
	define('BONET_MENU_PAGINA_BIN','Paperera de reciclatge');

	define('BONET_MENU_CURS','Cursos');
	define('BONET_MENU_CURS_NEW','Nou curs');
	define('BONET_MENU_CURS_LIST','Llistar cursos');
	define('BONET_MENU_CURS_BIN','Paperera de reciclatge');
}

/*--------------- nomès castellà i català, no traduit a la resta -------------------*/

/*agencies*/
$gl_caption_agencia['c_agencia'] = 'Agencia';
$gl_caption_agencia['c_agents'] = 'Agentes';
$gl_caption_agencia['c_user_ids'] = 'Usuarios';
$gl_caption_agencia['c_add_new_agent'] = 'Añadir agente';
$gl_caption_agencia['c_add_new_user'] = 'Añadir usuario';
$gl_caption_agencia['c_view_users'] = 'Ver usuarios';
$gl_caption_agencia['c_enquesta'] = 'Enquesta';

$gl_caption_agent['c_agencia'] = 'Agencia';
$gl_caption_agent['c_agent'] = 'Agente';
$gl_caption_agent['c_agent_description'] = 'Descripción';
$gl_caption_agent['c_nom'] = 'Nombre';
$gl_caption_agent['c_cognoms'] = 'Apellidos';
$gl_caption_agent['c_adresa'] = 'Dirección';
$gl_caption_agent['c_adress'] = 'Calle';
$gl_caption_agent['c_numstreet'] = 'Número';
$gl_caption_agent['c_block'] = 'Bloque';
$gl_caption_agent['c_flat'] = 'Piso';
$gl_caption_agent['c_door'] = 'Puerta';
$gl_caption_agent['c_provincia_id'] = 'Provincia';
$gl_caption_agent['c_comarca_id'] = 'Comarca';
$gl_caption_agent['c_municipi_id'] = 'Municipio';
$gl_caption_agent['c_cp'] = 'Codigo postal';
$gl_caption_agent['c_telefon'] = 'Teléfono';
$gl_caption_agent['c_telefon2'] = 'Teléfono 2';
$gl_caption_agent['c_fax'] = 'Fax';
$gl_caption_agent['c_correu'] = 'E-mail';
$gl_caption_agent['c_correu2'] = 'E-mail 2';
$gl_caption_agent['c_mail'] = 'E-mail newsletter';
$gl_caption_agent['c_url1'] = 'Web';
$gl_caption_agent['c_url1_name'] = 'Nom de la web';
$gl_caption_agent['c_codi_habitat'] = 'Codigo habitatsoft';
$gl_caption_agent['c_api_id'] = 'Núm. API';
$gl_caption_agent['c_aicat_id'] = 'Núm. AICAT';
$gl_caption_agent['c_nums'] = 'Identificadores';

$gl_caption_agent['c_speciality_id'] = 'Especialidades';
$gl_caption_agent['c_group_id'] = 'Grup newsletter';

$gl_caption['c_filecat_id'] = $gl_caption['c_filecat'] = "Categoria";
$gl_caption['c_ordre'] = "Ordre";
$gl_caption['c_form_files_title'] = "Documents de l'agent: ";
$gl_caption['c_edit_files'] = "Documents";
$gl_caption['c_file'] = "Documents";

$gl_caption_speciality['c_speciality'] = 'Especialidad';

$gl_caption['c_filter_actiu'] = 'Todos los agentes';
$gl_caption['c_actiu'] = 'Ejerce';
$gl_caption['c_actiu_1'] = 'Ejerce';
$gl_caption['c_actiu_0'] = 'No ejerce';

$gl_caption['c_filter_comarca_id'] = 'Todas las comarcas';
$gl_caption['c_filter_municipi_id'] = 'Todos los municipios';

$gl_caption['c_desactivar'] = 'Desactivar';
$gl_caption['c_activar'] = 'Activar';

// mapes
$gl_caption['c_edit_map'] = 'Ubicación';
$gl_caption['c_search_adress']='Buscar dirección';
$gl_caption['c_latitude']='latitud';
$gl_caption['c_longitude']='longitud';
$gl_caption['c_found_direction']='dirección encontrada';
$gl_caption['c_save_map'] = 'Guardar ubicación';
$gl_caption['c_click_map'] = 'Haz un click en un punto del mapa para definir la ubicación';
$gl_caption['c_form_map_title'] = 'Ubicación';
$gl_messages['point_saved'] = 'Nueva ubicación guardada correctamente';

/*pagines*/
$gl_caption_pagina['c_edit_title'] = 'Datos de la pàgina';
$gl_caption_pagina['c_page_title_title'] = 'Datos de la página';
$gl_caption_pagina['c_column_title1'] = 'Contingut';
$gl_caption_pagina['c_column_title2'] = 'Segunda columna';
$gl_caption_pagina['c_column_title3'] = 'Tercera columna';
$gl_caption_pagina['c_column_title4'] = 'Vídeo';
$gl_caption_pagina['c_column_title5'] = 'Google';
$gl_caption_pagina['c_column_title6'] = 'Más información';

$gl_caption_pagina['c_breadcrumb_edit_level1'] = '"editar pàgina"';
$gl_caption_pagina['c_breadcrumb_edit_level2'] = '"editar pàgina"';
$gl_caption_pagina['c_breadcrumb_edit_level3'] = '"editar pàgina"';
$gl_caption_pagina['c_breadcrumb_edit_list'] = '"llistat de subapartats"';

$gl_caption_pagina['c_subtitle_level2'] = 'Listar páginas';
$gl_caption_pagina['c_subtitle_level3'] = 'Listar páginas';

$gl_caption_pagina['c_filter_status'] = 'Todos los estados';
$gl_caption_pagina['c_filter_paginatpl_id'] = 'Todas las plantillas';

$gl_caption_pagina['c_edit_content_button'] = 'Editar página';
$gl_caption_pagina['c_edit_pagina_button'] = 'Editar datos';



$gl_caption_pagina['c_ordre'] = 'Orden';
$gl_caption_pagina['c_entered'] = 'Creado';
$gl_caption_pagina['c_modified'] = 'Última Modificación';
$gl_caption_pagina['c_paginatpl_id'] = 'Plantilla ficha';
$gl_caption_pagina['c_listtpl_id'] = 'Plantilla listado';
$gl_caption_pagina['c_link'] = 'Link';
$gl_caption_pagina['c_pagina_file_name'] = $GLOBALS['gl_caption']['c_file_name'];
$gl_caption_pagina['c_body_supertitle'] = 'Supertítulo';
$gl_caption_pagina['c_body_supertitle2'] = 'Supertítulo';
$gl_caption_pagina['c_body_supertitle3'] = 'Supertítulo';
$gl_caption_pagina['c_title'] = 'Título';
$gl_caption_pagina['c_title2'] = 'Título';
$gl_caption_pagina['c_title3'] = 'Título';
$gl_caption_pagina['c_body_subtitle'] = 'Subtítulo';
$gl_caption_pagina['c_body_subtitle2'] = 'Subtítulo 2';
$gl_caption_pagina['c_paragraph_title'] = 'Título';
$gl_caption_pagina['c_paragraph_title2'] = 'Título';
$gl_caption_pagina['c_paragraph_title3'] = 'Título';
$gl_caption_pagina['c_pagina'] = 'Nombre de la página en el menú';
$gl_caption_pagina['c_subpagina'] = 'Subapartats';
$gl_caption_pagina_list['c_pagina'] = 'Nombre';
$gl_caption_pagina['c_body_content'] = 'Contenido';
$gl_caption_pagina['c_body_content2'] = 'Contenido';
$gl_caption_pagina['c_body_content3'] = 'Contenido';
$gl_caption_pagina['c_body_content_llistat'] = 'Explicació llistat';
$gl_caption_pagina['c_prepare'] = 'En preparación';
$gl_caption_pagina['c_review'] = 'Para revisar';
$gl_caption_pagina['c_public'] = 'Público';
$gl_caption_pagina['c_archived'] = 'Archivado';
$gl_caption_['c_status'] = 'Estado';
$gl_caption_pagina['c_pagina_id'] = 'Id';
$gl_caption_pagina['c_parent_id'] = 'Pertenece a';
$gl_caption_pagina['c_custom_id'] = 'Galeria de personalitzación';
$gl_caption_pagina['c_level'] = '';
$gl_caption_pagina['c_blocked'] = 'Block.';
$gl_caption_pagina['c_is_full_pagina'] = '';

$gl_caption_pagina['c_add_pagina_button'] = 'Insertar nueva página';
$gl_caption_pagina['c_add_pagina_button_to'] = 'Entrar subapartat a: ';
$gl_caption_pagina['c_list_subpagines'] = 'Listar todos';
$gl_caption_pagina['c_add_subpagina'] = 'Añadir';
$gl_caption_pagina['c_add_subpagina2'] = 'Añadir subapartat';
$gl_caption_pagina['c_url1_name']='Nom URL';
$gl_caption_pagina['c_url1']='URL';
$gl_caption_pagina['c_url2_name']='Nom URL(2)';
$gl_caption_pagina['c_url2']='URL(2)';
$gl_caption_pagina['c_url3_name']='Nom URL(3)';
$gl_caption_pagina['c_url3']='URL(3)';
$gl_caption_pagina['c_videoframe']='Vídeo (youtube, metacafe...)<br />( 340 x 255 )';
$gl_caption_pagina['c_file']='Archivos';

$gl_caption['c_last_listing']='Listado anterior';


$gl_caption_category['c_ordre']='Ordre';
$gl_caption_category['c_category']='Categoría';
$gl_caption_category['c_is_clubone'] = 'Menú socios';

$gl_messages['c_restore_items'] = "Los elementos marcados no se han podido recuperar, ya que primero hay que recuperar la página superiror a la que pertenecen";



/*banners*/
$gl_caption_banner['c_banner_id']='';
$gl_caption_banner['c_ordre']='Orden';
$gl_caption_banner['c_url1']='Enlace';
$gl_caption_banner['c_url1_name']='Texto en el enlace';
$gl_caption_banner['c_url1_target']='Destino';
$gl_caption_banner['c_url1_target__blank']='Una nueva ventana';
$gl_caption_banner['c_url1_target__self']='La misma ventana';
$gl_caption_banner['c_file']='Imagen banner <br>(960x92 - 248x122)';
$gl_caption_banner['c_banner']='Banner';


$gl_caption_agentenquesta['c_send_new']='Enviar enquesta';
$gl_caption_agentenquesta['c_agencia_id']='Agència de la Propietat Immobiliària';
$gl_caption_agentenquesta['c_nom_persona']='Nom de la persona que contesta l’enquesta';
$gl_caption_agentenquesta['c_entered']='Data';
$gl_messages['enquesta_sent']='S\'ha enviat l\'enquesta correctament, gràcies per la seva participació';
$gl_messages['enquesta_not_sent']="No s'ha pogut enviar l'enquesta, si us plau torna a probar-ho";


// Cursos

$gl_caption_curs['c_prepare'] = 'En preparació';
$gl_caption_curs['c_archived'] = 'Arxivat';
$gl_caption_curs['c_content'] = 'Descripció';
$gl_caption_curs['c_entered'] = 'Creat';
$gl_caption_curs['c_modified'] = 'Modificat';
$gl_caption_curs['c_status'] = 'Estat';
$gl_caption_curs['c_subtitle'] = 'Subtítol';
$gl_caption_curs['c_videoframe'] = 'Vídeo';

$gl_caption['c_curs_file_name'] = $GLOBALS['gl_caption']['c_file_name'];
$gl_caption['c_curs_old_file_name'] = $GLOBALS['gl_caption']['c_old_file_name'];

$gl_caption_curs['c_filter_status'] = 'Tots els estats';

$gl_caption_curs['c_form_level1_title_curs']='Títols pàgina';
$gl_caption_curs['c_form_level1_title_timetable']='Característiques';
$gl_caption_curs['c_form_level1_title_file']='Arxius adjunts';
$gl_caption_curs['c_form_level1_title_page_title']='SEO';

// Públic
$gl_caption['c_curs'] = $gl_caption['c_curs_id'] = 'Curso';
$gl_caption_curs['c_timetable'] = 'Horarios';
$gl_caption_curs['c_open'] = 'Abierto';
$gl_caption_curs['c_full'] = 'Completo';
$gl_caption_curs['c_finished'] = 'Finalizado';


$gl_caption_curs['c_speciality_id'] = 'Especialidad';

$gl_caption_curs['c_start_date'] = 'Fecha inicio';
$gl_caption_curs['c_end_date'] = 'Fecha final';
$gl_caption_curs['c_maximum_person'] = 'Máximo personas';
$gl_caption_curs['c_weekday_id'] = 'Días';

$gl_caption_curs['c_durada'] = 'Duración';
$gl_caption_curs['c_modalitat'] = 'Modalidad';
$gl_caption_curs['c_lloc'] = 'Sitio';
$gl_caption_curs['c_adressa'] = 'Dirección';
$gl_caption_curs['c_tipologia'] = 'Tipología';
$gl_caption_curs['c_convalidacio'] = 'Convalidaciones';
$gl_caption_curs['c_credit'] = 'Créditos';
$gl_caption_curs['c_temari'] = 'Temario';
$gl_caption_curs['c_ponent'] = 'Ponentes';
$gl_caption_curs['c_avaluacio'] = 'Sistema de evaluación';
$gl_caption_curs['c_preu'] = 'Precio';
$gl_caption_curs['c_descompte'] = 'Descuento';
$gl_caption_curs['c_titulacio'] = 'Titulación';
$gl_caption_curs['c_inscripcio'] = 'Inscripción';

$gl_caption_curs['c_modalitat_none'] = "";
$gl_caption_curs['c_modalitat_presencial'] = "Presencial";
$gl_caption_curs['c_modalitat_semi-presencial'] = "Semi presencial";
$gl_caption_curs['c_modalitat_taller-practic'] = "Taller práctico";
$gl_caption_curs['c_modalitat_online'] = "Online";