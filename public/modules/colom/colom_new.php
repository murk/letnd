<?
/**
 * ColomNew
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2010
 * @version $Id$
 * @access public
 */
class ColomNew extends Module{

	function __construct(){
		parent::__construct();
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function get_records()
	{
		$listing = new ListRecords($this);
		$listing->condition = "status = 'public'";
		$listing->order_by = 'ordre ASC, new ASC';
		$listing->paginate = false;
		return $listing->list_records();
	}
}
?>