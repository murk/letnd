<?php
global $gl_show_config, $gl_process;
$gl_show_config = '';
switch ($gl_process)
{
    case 'admin':
        set_config('web/web_config_admin_config.php');
        set_table('all__configadmin', 'configadmin_id');
        break;
    case 'public':
        set_config('web/web_config_public_config.php');
        set_table('all__configpublic', 'configpublic_id');
        break;
    case 'inmo_admin':
        set_config('web/web_config_inmo_admin_config.php');
        set_table('inmo__configadmin', 'configadmin_id');
        break;
    case 'inmo_public':
        set_config('web/web_config_inmo_public_config.php');
        set_table('inmo__configpublic', 'configpublic_id');
        break;
	case 'booking_admin':
        set_config('web/web_config_booking_admin_config.php');
        set_table('booking__configadmin', 'configadmin_id');
        break;
	case 'booking_public':
        set_config('web/web_config_booking_public_config.php');
        set_table('booking__configpublic', 'configpublic_id');
        break;
    case 'custumer':
        set_config('web/web_config_custumer_config.php');
        set_table('all__configadmin', 'configadmin_id');
        break;
    case 'newsletter':
        set_table('newsletter__configadmin', 'configadmin_id');
        break;
    case 'utility':
        set_config('web/web_config_utility_config.php');
        set_table('utility__configadmin', 'configadmin_id');
        break;
    case 'mls':
        set_config('web/web_config_mls_config.php');
        set_table('all__configadmin', 'configadmin_id');
        break;
	 case 'contact_public':
        set_config('web/web_config_contact_public_config.php');
        set_table('contact__configpublic', 'configpublic_id');
        break;
	case 'contact_admin':
        set_config('web/web_config_contact_admin_config.php');
        set_table('contact__configadmin', 'configadmin_id');
        break;
	case 'product_admin':
        set_config('web/web_config_product_admin_config.php');
        set_table('product__configadmin', 'configadmin_id');
        break;
	case 'product_public':
        set_config('web/web_config_product_public_config.php');
        set_table('product__configpublic', 'configpublic_id');
        break;	
	case 'client_public':
				
		global $gl_db_classes_language_fields, $gl_db_classes_fields, $gl_caption;
		
		
		$page_text_id = client_get_page_text_id();
		
		$results = Db::get_rows("
			SELECT *, 
				(SELECT page FROM all__page_language,all__page
					WHERE 
						all__page_language.page_id = all__page.page_id
						AND 
						all__page.page_text_id = client__configpublic.page_text_id
						AND language = '".LANGUAGE."'
				) AS page 
			FROM client__configpublic
			WHERE page_text_id = '".$page_text_id."'
			ORDER BY ordre,page_text_id,name");
		
		
		foreach ($results as $rs) {
			extract($rs);
			$gl_db_classes_fields['configpublic_id'] =   array (
												'type' => 'hidden',
												'enabled' => '1',
												'form_admin' => 'input',
												'order' => '0',
												'default_value' => '',
												'override_save_value' => '',
												'group' => '',
												'class' => '',
												'javascript' => '',
												'text_size' => '',
												'text_maxlength' => '11',
												'textarea_cols' => '',
												'textarea_rows' => '',
												'select_caption' => '',
												'select_size' => '',
												'select_table' => '',
												'select_fields' => '',
												'select_condition' => '',
											  );
			$gl_db_classes_fields[$name] =  array (
												'type' => $type,
												'enabled' => '1',
												'form_admin' => 'input',
												'order' => '0',
												'default_value' => '',
												'override_save_value' => '',
												'group' => $page_text_id,
												'class' => 'wide',
												'javascript' => '',
												'text_size' => '',
												'text_maxlength' => '',
												'textarea_cols' => '',
												'textarea_rows' => '',
												'select_caption' => '',
												'select_size' => '',
												'select_table' => '',
												'select_fields' => '',
												'select_condition' => '',
											  );			
			
			if (
					$type=='text' || 
					$type=='htmlbasic' || 
					$type=='html' || 
					$type=='htmlfull'
					){
				$gl_db_classes_language_fields []= $name;
			}
			if (
					$type=='date' || 
					$type=='datetime'|| 
					$type=='int'
					){
				$gl_db_classes_fields[$name]['class']='';
			}
			
			
			
			$gl_caption['c_' . $name] = ucfirst(str_replace('_',' ',$name));
			
			$gl_caption['c_group_' . $page_text_id] = $page?$page:$page_text_id;
		}
        set_table('client__configpublic', 'configpublic_id');
        break;	
}

/* 
 * 
 * SUBMENUS
 * 
 */
$page_text_id = 'false';
if ($GLOBALS['gl_menu_id']==4015) $page_text_id = client_get_page_text_id();

$results = Db::get_rows("
		SELECT 
			DISTINCT (
					SELECT page
					FROM all__page, all__page_language
					WHERE all__page.page_id = all__page_language.page_id
					AND language = '".LANGUAGE . "'
					AND all__page.page_text_id = client__configpublic.page_text_id
					LIMIT 1
			)
				AS item,
			(
					SELECT ordre
					FROM all__page
					WHERE all__page.page_text_id = client__configpublic.page_text_id
					LIMIT 1
			) AS ordre,
			CONCAT('&page_text_id=',page_text_id) as id,
			IF (page_text_id = '".$page_text_id."','1','0') as selected
			FROM client__configpublic
			ORDER BY ordre ASC, item ASC");

foreach ($results as &$rs2) {

	if (!$rs2['item']){
		$rs2['item'] = 'General';
	}
}

$GLOBALS['gl_page']->last_sub_menu[4015] = $results;


function write_record()
{
    global $gl_process;
	
    switch ($gl_process)
    {
        case 'client_public':
			
			$name = R::escape('name',false,'_POST');
			$ordre = R::escape('ordre', '0','_POST');

			if((!preg_match("/^([a-z_0-9]+)$/", $name ) && $name!='') || (!$name))		
			{
				$GLOBALS['gl_message'] = 'Falta el nom de la variable o no és vàlida';
				return;
			}
			if (Db::get_first("SELECT count(*) FROM client__configpublic WHERE name ='".$name."'")){
				
				$GLOBALS['gl_message'] = 'Ja existeix aquesta nom de variable';
				return;
			}
			
			$page_text_id = R::text_id('page_text_id',false,'_POST');
			$type = R::text_id('type',false,'_POST');
			
			$values = $_POST['value'];
			R::escape_array($values);
			
			
			Db::execute("
				INSERT INTO client__configpublic (`name`, `ordre`, `page_text_id`, `type`)
				VALUES ('$name', '$ordre', '$page_text_id', '$type');");
			
			foreach ($values as $key => $value) {
				
				Db::execute("
					INSERT INTO client__configpublic_language (`name`, `value`, `language`)
					VALUES ('$name', '$value', '$key');");
			}
			
			$GLOBALS['gl_message'] = 'Guardat';
			$GLOBALS['gl_reload'] = true;
			$_SESSION['web']['page_text_id'] = $page_text_id;
			$_SESSION['web']['ordre'] = $ordre;

            break;
    }
	
}


function save_rows()
{
    global $gl_process;
    if ($gl_process=='newsletter') {
    	set_config('web/web_config_newsletter_config.php'); // poso aquí pel caption
    }
    $save_rows = new SaveRows;
    switch ($gl_process)
    {
        case 'inmo_admin':
            $save_rows->call_function['provincies'] = 'save_provincies';
            $save_rows->call_function['menu_price'] = 'save_menu_price';
            break;
        case 'inmo_public':
            $save_rows->call_function['menu_price'] = 'save_menu_price';
            $save_rows->call_function['menu_price_rent'] = 'save_menu_price';
            break;
        case 'booking_public':
        case 'product_public':
            $save_rows->call_function['payment_methods'] = 'save_methods';
            break;
	    case 'custumer':
            $save_rows->call_function['mls_allow_listing'] = 'save_mls_allow_listing';
            break;
    }
    $save_rows->save();
}

function list_records()
{
    global $gl_process, $gl_show_config;

	delete_menus_config();

	if ($gl_process=='newsletter') {
    	set_config('web/web_config_newsletter_config.php'); // poso aquí pel caption
    }
	
    merge_caption($gl_process);
    $gl_show_config = new ShowConfig;
    switch ($gl_process)
    {
        case 'public':
            $gl_show_config->call_function['google_center_longitude'] = 'get_google_link';
            
            break;
        case 'inmo_admin':
            $gl_show_config->call_function['google_center_longitude'] = 'get_google_link';
            $gl_show_config->call_function['provincies'] = 'list_provincies';
            $gl_show_config->call_function['menu_price'] = 'list_menu_price';
            break;
        case 'inmo_public':
            $gl_show_config->call_function['menu_price'] = 'list_menu_price';
            $gl_show_config->call_function['menu_price_rent'] = 'list_menu_price';
            break;
        case 'booking_public':
            $gl_show_config->call_function['payment_methods'] = 'list_methods';
            break;
        case 'product_admin':
			set_tax_included();
            break;
        case 'product_public':
            $gl_show_config->call_function['payment_methods'] = 'list_methods';
            break;
    }
	
    if ($gl_show_config->fields) $gl_show_config->show_config();
	
	
	if($gl_process=='client_public') {
		global $gl_content;
		
		// nomes admintotal
		if ($_SESSION['group_id']=='1'){

			$page_text_id = '';
			$selected_page_text_id = isset($_SESSION['web']['page_text_id'])?$_SESSION['web']['page_text_id']:false;
			$selected_ordre = isset($_SESSION['web']['ordre'])?$_SESSION['web']['ordre']:false;
			$values = '';
			foreach ($GLOBALS['gl_languages']['public'] as $key=>$val){				
				$values .= '
						<tr>
							<td><span class="formsCaption">'.$GLOBALS['gl_languages']['names'][$val].'</span></td>
						</tr>
						<tr>
							<td><textarea name="value['.$val.']" id="value_'.$val.'" cols="90" rows="2"></textarea>
						</td>
						</tr>';
			}
			
			$results = Db::get_rows("
				SELECT * 
					FROM all__page, all__page_language
					WHERE all__page.page_id = all__page_language.page_id
					AND language = '".LANGUAGE . "'
					ORDER BY page");
			$page_text_id = '<select name="page_text_id">';
			$page_text_id .='<option value="">Totes les pàgines</option>';
			foreach ($results as $rs) {
				$selected = $selected_page_text_id == $rs['page_text_id'] ? ' selected' : '';
				$page_text_id .='<option' . $selected . ' value="'.$rs['page_text_id'].'">' . $rs['page'] . ' - ' . $rs['page_text_id'] . '</option>';
			}
			$page_text_id .= '</select>';
			
			
			
			
			$results = Db::get_rows("
				SELECT page_text_id, name, ordre 
					FROM client__configpublic
					ORDER BY page_text_id, ordre, name");
			$names = '<div style="position:absolute;right:0;top:0;background-color: white;padding:20px 30px;"><div style="font-size:18px;padding-bottom:10px;">Noms variables</div><table>';

			$right_page_text_id = $current_page_text_id = '';

			foreach ($results as $rs) {

				if ($rs['page_text_id'] == $current_page_text_id){
					$right_page_text_id = '';
				}
				else {
					$right_page_text_id = $current_page_text_id = $rs['page_text_id'];
				}

				$names .='
					<tr>
						<td class="padding-right-2"><strong>' . $right_page_text_id . '</strong></td>
						<td class="padding-right-1">' . $rs['name'] . '</td>
						<td>' . $rs['ordre'] . '</td>
					</tr>';

			}
			$names .= '</table>';

			$names .= '<div style="font-size:18px;padding-top:30px;padding-bottom:10px;">Variables</div><table>';
			foreach ($results as $rs) {

				if ($rs['page_text_id'] == $current_page_text_id){
					$right_page_text_id = '';
				}
				else {
					$right_page_text_id = $current_page_text_id = $rs['page_text_id'];
				}

				$names .='
					<tr>
						<td class="padding-right-2"><strong>' . $right_page_text_id . '</strong></td>
						<td class="padding-right-1">&lt;?= $c_<strong>' . $rs['name'] . '</strong> ?&gt;</td>
					</tr>';

			}
			$names .= '</table></div>';


			$ordre_value = $selected_ordre ? $selected_ordre +1 : 0;

			$gl_content = '
		<form style="display:block;position:relative;" target="save_frame" enctype="multipart/form-data" action="?action=add_record&amp;menu_id=4015&amp;process=client_public" method="post" name="theForm2">
		'.$names.'
		<table width="100%" border="0" cellspacing="10" cellpadding="0" class="form formstbl">
			<tr>
				<td valign="top">
					Nom de variable: (nom_var_guions_baixos)
				</td>
				<td>
					<input style="width:300px;" type="text" value="" name="name">
				</td>
			</tr>
			<tr>
				<td valign="top">
					Ordre
				</td>
				<td>
					<input style="width:20px;" type="text" value="'.$ordre_value.'" name="ordre">
				</td>
			</tr>
			<tr>
				<td valign="top">
					Tipus:
				</td>
				<td>
					<select name="type">
						<option value="text">Text</option>
						<option value="htmlbasic">htmlbasic</option>
						<option value="html">html</option>
					</select>
				</td>
			</tr>
			<tr>
				<td valign="top">
					Pàgina:
				</td>
				<td>
					' . $page_text_id . ' 
				</td>
			</tr>
			<tr>
				<td valign="top">
					Value:
				</td>
				<td class="forms">
					<table class="padding-bottom-2" width="100%" border="0" cellpadding="0" cellspacing="0">
						' . $values . '                        
					</table>
					<input type="submit" class="btn_form1" value="Entrar text nou" name="Submit">
				</td>
			</tr>
		</table>
		</form>	' . $gl_content;
		}
	}
}


function client_get_page_text_id(){
	
	$page_text_id = R::text_id('page_text_id');
		
		if ($page_text_id===false){
			
			$rs = Db::get_row("
				SELECT (
					SELECT page
					FROM all__page, all__page_language
					WHERE all__page.page_id = all__page_language.page_id
					AND language = '".LANGUAGE . "'
					AND all__page.page_text_id = client__configpublic.page_text_id
			)
				AS item,
			(
					SELECT ordre
					FROM all__page
					WHERE all__page.page_text_id = client__configpublic.page_text_id
			) AS ordre,
			page_text_id
					FROM client__configpublic
					ORDER BY ordre ASC, item ASC
					LIMIT 1");
			$page_text_id = $rs['page_text_id'];
		}
	return $page_text_id;
}


function set_tax_included(){
	global $gl_show_config;
	
	$is_tax = Db::get_first("SELECT value FROM product__configadmin WHERE name='is_public_tax_included'");
		
	
	// missatge de iva inclos o no inclos
	//$gl_show_config->caption['c_group_11'] .= ' ' . ($is_tax?$gl_show_config->caption['c_tax_included']:$gl_show_config->caption['c_tax_not_included']);
	
}
function get_google_link($name, $val)
{
	global $gl_show_config, $gl_caption, $gl_process, $gl_version;
	$rs = &$gl_show_config->rs;
	$ret = '';
	if ($gl_process=='public') {
		$ret .= '<br /><strong>'.$gl_caption['c_google_latitude'].': </strong><span id="latitut">'.$rs['google_latitude'].'</span>&nbsp;&nbsp;&nbsp;<strong>'.$gl_caption['c_google_longitude'].': </strong><span id="longitut">'.$rs['google_longitude'].'</span><br /><br />';
	}
	$ret .= 
	'<strong>'.$gl_caption['c_google_center_latitude'].': </strong>
	<span id="center_latitut">'.$rs['google_center_latitude'].'</span>&nbsp;&nbsp;&nbsp;
	<strong>'.$gl_caption['c_center_longitude'].': </strong>
	<span id="center_longitut">'.$rs['google_center_longitude'].'</span><br /><br />
	
	<input name="google_center_longitude[999]" id="google_center_longitude_999" type="hidden" value="'.$rs['google_center_longitude'].'" />
	<input name="old_google_center_longitude[999]" id="old_google_center_longitude_999" type="hidden" value="'.$rs['google_center_longitude'].'" />

	<iframe name="google_map" id="google_map" scrolling="no" marginwidth="0" marginheight="0" frameborder="0" style="overflow:visible;" src="/admin/themes/inmotools/web/config_map.php?client_dir='.CLIENT_DIR.'&amp;language='.LANGUAGE.'&amp;version='.$gl_version.'"></iframe>';
    return $ret;
}
function list_provincies($name, $provincies)
{
    global $gl_db_classes_fields;
    $provs = explode(',', $provincies);
    $query = 'SELECT provincia_id,provincia FROM inmo__provincia';
    $results = Db::get_rows($query);
    $html = '';
    foreach ($results as $rs)
    {
        $selected = in_array($rs['provincia_id'], $provs)?' selected':'';
        $html .= '<option value="' . $rs['provincia_id'] . '"' . $selected . '>' . $rs['provincia'] . '</option>';
    }
    $html = '<select multiple name="' . $name . '[]" size="' . $gl_db_classes_fields['provincies']['select_size'] . '">' . $html . '</select>';
    $html .= '<input name="old_' . $name . '" type="hidden" value="' . $provincies . '">';
    return $html;
}
function list_methods($name, $methods) {
	global $gl_db_classes_fields, $gl_caption, $gl_process;
	$m = explode( ',', $methods );

	if ( $gl_process == 'booking_public' ) {
		$results = [ 'paypal', 'account', 'lacaixa' ];
	}
	else {
		$results = [ 'paypal', 'account', 'ondelivery', 'directdebit', 'lacaixa' ];
	}
	$html = '';
	foreach ( $results as $rs ) {
		$selected = in_array( $rs, $m ) ? ' selected' : '';
		$html     .= '<option value="' . $rs . '"' . $selected . ' >' . $gl_caption[ 'c_' . $rs ] . '</option>';
	}
	$html = '<select multiple name="' . $name . '[]" size="' . $gl_db_classes_fields['payment_methods']['select_size'] . '">' . $html . '</select>';
	$html .= '<input name="old_' . $name . '" type="hidden" value="' . $methods . '">';

	return $html;
}

function save_provincies($name, $provincies)
{
    return '\'' . implode (',', $provincies) . '\'';
}
function save_methods($name, $methods)
{
    return '\'' . implode (',', $methods) . '\'';
}
function list_menu_price($name, $menu_price)
{
    global $gl_db_classes_fields;
    $cf = &$gl_db_classes_fields['menu_price'];
    $html = '';
    $prices = explode(',', $menu_price);
    foreach ($prices as $price)
    {
        $html .= '<input name="' . $name . '[]" type="text" value="' . $price . '" size="' . $cf['text_size'] . '" maxlength="' . $cf['text_maxlength'] . '"> - ';
    }
    $html = substr($html, 0, -2);
    $html .= '<input name="old_' . $name . '" type="hidden" value="' . $menu_price . '">';
    return $html;
}
function save_menu_price($name, $menu_price)
{
    $ret = '';
	foreach ($menu_price as $price) {
		if ($price!='') $ret .=$price . ',';
	}
	
	if ($ret) $ret = substr($ret, 0, -1);
	
	return '\'' . $ret . '\'';
}

function save_mls_allow_listing ( $name, $mls_allow_listing ) {

	Db::connect_mother();

	Db::execute( "
		UPDATE client__client
		SET mls_allow_listing=" . $mls_allow_listing . "
		WHERE  `client_id`=" . $_SESSION['client_id'] );

	Db::reconnect();

	return $mls_allow_listing;

}
function delete_menus_config () {
	$page = &$GLOBALS['gl_page'];
	if (!User::has_write_permission('inmo')){
		$page->delete_menu(4100);
		$page->delete_menu(4101);
		$page->delete_menu(4103);
		$page->delete_menu(4104);
	}
	if (!User::has_write_permission('product')){
		$page->delete_menu(4110);
		$page->delete_menu(4111);
	}
	if (!User::has_write_permission('newsletter')){
		$page->delete_menu(4102);
	}
	if ($_SESSION['group_id'] != '1'){
		$page->delete_menu(4107);
	}
}