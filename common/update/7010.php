<?php
/* 21-4-17
Afegir camps facturació a clients de INMO

*/
if ( is_table( 'custumer__custumer' ) ) {

	$update_sql[] = "ALTER TABLE `custumer__custumer`
	CHANGE COLUMN `nif` `cif` VARCHAR(10) NULL DEFAULT NULL COLLATE 'utf8_spanish2_ci' AFTER `comercial_name`;";

} else {
}
?>