<?

/**
 * InvoiceGraph
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2011
 * @version $Id$
 * @access public
 */
class InvoiceGraph extends Module {

	public function __construct() {
		parent::__construct();
	}

	public function list_records() {
		$GLOBALS['gl_content'] = $this->get_records();
	}

	public function get_records() {

		Page::add_css_file( '/admin/themes/inmotools/invoice/invoice.css' );
		$this->set_file( '/invoice/graph_list.tpl' );

		/*
		 * Mesos reals facturació
		*/
		$query = "
				SELECT MONTHNAME(bill_date) AS month, YEAR(bill_date) AS year, SUM(total_price_no_vat) AS total
				FROM invoice__invoice
				WHERE bin = 0 OR (bin=1 AND fake = 1)
				GROUP BY YEAR(bill_date), MONTH(bill_date);				
				";

		$results_invoice = Db::get_rows( $query );

		$query = "
				SELECT MONTHNAME(bill_date) AS month, YEAR(bill_date) AS year, SUM(total_price_no_vat) AS total
				FROM invoice__expense
				WHERE bin = 0 OR (bin=1 AND fake = 1)
				GROUP BY YEAR(bill_date), MONTH(bill_date);				
				";

		$results_expense = Db::get_rows( $query );

		$this->merge_results( $results_invoice, $results_expense );

		$labels       = $this->get_labels( $results_invoice, [ 'month', 'year' ] );
		$data_invoice = $this->get_datas( $results_invoice, [ 'total' ] );
		$data_expense = $this->get_datas( $results_expense, [ 'total' ] );

		$data_earning = [];
		$data_earning_colors = [];

		foreach ( $data_invoice as $key => $invoice ) {
			$earning = $invoice - $data_expense[ $key ];
			$data_earning [] = $earning;

			if ( $earning > 0 ) {
				$color = "rgba(139, 203, 0, 1)";
			}
			else {
				$color = "rgba(237, 72, 72, 1)";
			}
			$data_earning_colors [] = $color;
		}

		$this->set_json_vars( [
			'labels_invoice' => $labels,
			'data_invoice'   => $data_invoice,
			'data_expense'   => $data_expense,
			'data_earning'   => $data_earning,
			'data_earning_colors'   => $data_earning_colors,
		] );

		/*$this->set_vars( [
			'data_invoice_c3' => implode( ',', $data_invoice ),
			'data_expense_c3' => implode( ',', $data_expense ),
			'data_earning_c3' => implode( ',', $data_earning ),
		] );*/


		/*
		 * Mesos facturació per data remesa
		*/
		$query = "
				SELECT MONTHNAME(invoice__batch.batch_date) AS month, YEAR(invoice__batch.batch_date) AS year, SUM(total_price_no_vat) AS total
				FROM invoice__invoice INNER JOIN invoice__batch USING (batch_id)
				WHERE invoice__invoice.bin = 0 OR ( invoice__invoice.bin=1 AND  invoice__invoice.fake = 1)
				GROUP BY YEAR(invoice__batch.batch_date), MONTH(invoice__batch.batch_date);			
				";

		$results_invoice = Db::get_rows( $query );

		$query = "
				SELECT MONTHNAME(bill_due) AS month, YEAR(bill_due) AS year, SUM(total_price_no_vat) AS total
				FROM invoice__expense
				WHERE invoice__expense.bin = 0 OR ( invoice__expense.bin=1 AND  invoice__expense.fake = 1)
				GROUP BY YEAR(bill_due), MONTH(bill_due);				
				";

		$results_expense = Db::get_rows( $query );

		$this->merge_results( $results_invoice, $results_expense );

		$labels       = $this->get_labels( $results_invoice, [ 'month', 'year' ] );
		$data_invoice = $this->get_datas( $results_invoice, [ 'total' ] );
		$data_expense = $this->get_datas( $results_expense, [ 'total' ] );

		$data_earning = [];
		$data_earning_colors = [];

		foreach ( $data_invoice as $key => $invoice ) {
			$earning = $invoice - $data_expense[ $key ];
			$data_earning [] = $earning;

			if ( $earning > 0 ) {
				$color = "rgba(139, 203, 0, 1)";
			}
			else {
				$color = "rgba(237, 72, 72, 1)";
			}
			$data_earning_colors [] = $color;
		}

		$this->set_json_vars( [
			'labels_invoice_batch' => $labels,
			'data_invoice_batch'   => $data_invoice,
			'data_expense_batch'   => $data_expense,
			'data_earning_batch'   => $data_earning,
			'data_earning_colors_batch'   => $data_earning_colors,
		] );


		/*
		 * Anys reals facturació
		*/
		$query = "
				SELECT 'January' AS month, YEAR(bill_date) AS year, SUM(total_price_no_vat) AS total
				FROM invoice__invoice
				WHERE bin = 0 OR (bin=1 AND fake = 1)
				GROUP BY YEAR(bill_date);				
				";

		$results_invoice = Db::get_rows( $query );

		$query = "
				SELECT 'January' AS month, YEAR(bill_date) AS year, SUM(total_price_no_vat) AS total
				FROM invoice__expense
				WHERE bin = 0 OR (bin=1 AND fake = 1)
				GROUP BY YEAR(bill_date);				
				";

		$results_expense = Db::get_rows( $query );

		$this->merge_results( $results_invoice, $results_expense );

		$labels       = $this->get_labels( $results_invoice, [ 'year' ] );
		$data_invoice = $this->get_datas( $results_invoice, [ 'total' ] );
		$data_expense = $this->get_datas( $results_expense, [ 'total' ] );

		$data_earning = [];
		$data_earning_colors = [];

		foreach ( $data_invoice as $key => $invoice ) {
			$earning = $invoice - $data_expense[ $key ];
			$data_earning [] = $earning;

			if ( $earning > 0 ) {
				$color = "rgba(139, 203, 0, 1)";
			}
			else {
				$color = "rgba(237, 72, 72, 1)";
			}
			$data_earning_colors [] = $color;
		}

		$this->set_vars( [
			'labels_invoice_year' => json_encode( $labels ),
			'data_invoice_year'   => json_encode( $data_invoice ),
			'data_expense_year'   => json_encode( $data_expense ),
			'data_earning_year'   => json_encode( $data_earning ),
			'data_earning_colors_year'   => json_encode( $data_earning_colors ),
		] );


		/*
		 * Fixes
		 *
		 * 16 - Antispam -> posicionament
		 *

		 Per comprvar conceptes:

		 SELECT concept_id, invoice_id
				FROM invoice__invoice INNER JOIN invoice__invoiceconcept USING (invoice_id)
				WHERE recurring_id <> 0
				GROUP BY concept_id;

		Conceptes orfes:
		SELECT * FROM invoice__invoice WHERE recurring_id <> 0 AND recurring_id NOT IN (SELECT recurring_id FROM invoice__recurring)

		*/
		$concepts = "(7, 8, 9, 10, 11, 12, 16, 17)";
		$query = "
				SELECT MONTHNAME(bill_date) AS month,  YEAR(bill_date) AS year, SUM(invoice__invoiceconcept.price) AS total
				FROM invoice__invoice INNER JOIN invoice__invoiceconcept USING (invoice_id)
				WHERE (bin = 0 OR (bin=1 AND fake = 1)) AND recurring_id <> 0 AND concept_id IN $concepts
				GROUP BY YEAR(bill_date), MONTH(bill_date);			
				";

		$results_fix = Db::get_rows( $query );

		$query = "
				SELECT MONTHNAME(bill_date) AS month, YEAR(bill_date) AS year, SUM(invoice__invoiceconcept.price) AS total
				FROM invoice__invoice INNER JOIN invoice__invoiceconcept USING (invoice_id)
				WHERE (bin = 0 OR (bin=1 AND fake = 1)) AND recurring_id <> 0 AND (concept_id = 7 OR concept_id = 8)
				GROUP BY YEAR(bill_date), MONTH(bill_date);			
				";

		$results_inmo = Db::get_rows( $query );

		$query = "
				SELECT MONTHNAME(bill_date) AS month, YEAR(bill_date) AS year, SUM(invoice__invoiceconcept.price) AS total
				FROM invoice__invoice INNER JOIN invoice__invoiceconcept USING (invoice_id)
				WHERE (bin = 0 OR (bin=1 AND fake = 1)) AND recurring_id <> 0 AND concept_id = 17
				GROUP BY YEAR(bill_date), MONTH(bill_date);			
				";

		$results_shop = Db::get_rows( $query );

		$query = "
				SELECT MONTHNAME(bill_date) AS month, YEAR(bill_date) AS year, SUM(invoice__invoiceconcept.price) AS total
				FROM invoice__invoice INNER JOIN invoice__invoiceconcept USING (invoice_id)
				WHERE (bin = 0 OR (bin=1 AND fake = 1)) AND recurring_id <> 0 AND concept_id = 9
				GROUP BY YEAR(bill_date), MONTH(bill_date);			
				";

		$results_hosting = Db::get_rows( $query );

		$query = "
				SELECT MONTHNAME(bill_date) AS month, YEAR(bill_date) AS year, SUM(invoice__invoiceconcept.price) AS total
				FROM invoice__invoice INNER JOIN invoice__invoiceconcept USING (invoice_id)
				WHERE (bin = 0 OR (bin=1 AND fake = 1)) AND recurring_id <> 0 AND (concept_id = 10 OR concept_id = 11 OR concept_id = 12 OR concept_id = 16)
				GROUP BY YEAR(bill_date), MONTH(bill_date);			
				";

		$results_seo = Db::get_rows( $query );

		$labels       = $this->get_labels( $results_fix, [ 'month', 'year' ] );
		$data_fix = $this->get_datas( $results_fix, [ 'total' ] );
		$data_inmo = $this->get_datas( $results_inmo, [ 'total' ] );
		$data_shop = $this->get_datas( $results_shop, [ 'total' ] );
		$data_hosting = $this->get_datas( $results_hosting, [ 'total' ] );
		$data_seo = $this->get_datas( $results_seo, [ 'total' ] );


		$this->set_json_vars( [
			'labels_fix' => $labels,
			'data_fix'   => $data_fix,
			'data_inmo'   => $data_inmo,
			'data_shop'   => $data_shop,
			'data_hosting'   => $data_hosting,
			'data_seo'   => $data_seo,
		] );


		/*
		 * IVA
		*/
		$query = "
				SELECT QUARTER(bill_date) AS month, YEAR(bill_date) AS year, SUM(total_vat) AS total
				FROM invoice__invoice
				WHERE bin = 0 OR (bin=1 AND fake = 1)
				GROUP BY YEAR(bill_date), QUARTER(bill_date);				
				";

		$results_vat = Db::get_rows( $query );

		$query = "
				SELECT QUARTER(bill_date) AS month, YEAR(bill_date) AS year, SUM(total_vat) AS total
				FROM invoice__expense
				WHERE bin = 0 OR (bin=1 AND fake = 1)
				GROUP BY YEAR(bill_date), QUARTER(bill_date);				
				";

		$results_vat_expense = Db::get_rows( $query );

		$query = "
				SELECT QUARTER(bill_date) AS month, YEAR(bill_date) AS year, SUM(total_irpf) AS total
				FROM invoice__expense
				WHERE bin = 0 OR (bin=1 AND fake = 1)
				GROUP BY YEAR(bill_date), QUARTER(bill_date);				
				";

		$results_irpf_expense = Db::get_rows( $query );

		$this->merge_results( $results_vat, $results_vat_expense, 'sort_quarter' );

		$labels       = $this->get_trimestre_names($this->get_labels( $results_vat, [ 'month', 'year' ] ));
		$data_vat = $this->get_datas( $results_vat, [ 'total' ] );
		$data_vat_expense = $this->get_datas( $results_vat_expense, [ 'total' ] );
		$data_irpf_expense = $this->get_datas( $results_irpf_expense, [ 'total' ] );

		$data_iva_irpf = [];
		$data_to_pay = [];
		$data_vat_to_pay = [];

		foreach ( $data_vat as $key => $vat ) {
			$vat_to_pay = $vat - $data_vat_expense[ $key ];
			$to_pay = $vat_to_pay + $data_irpf_expense[ $key ];
			$iva_irpf = $vat + $data_irpf_expense[ $key ];
			$data_to_pay []= $to_pay;
			$data_vat_to_pay []= $vat_to_pay;
			$data_iva_irpf []= $iva_irpf;
		}

		$this->set_vars( [
			'labels_vat' => json_encode( $labels ),
			'data_vat'   => json_encode( $data_vat ),
			'data_iva_irpf'   => json_encode( $data_iva_irpf ),
			'data_to_pay'   => json_encode( $data_to_pay ),
			'data_vat_to_pay'   => json_encode( $data_vat_to_pay ),
			'data_irpf_expense'   => json_encode( $data_irpf_expense ),
			'data_vat_expense'   => json_encode( $data_vat_expense ),
		] );




		return $this->process();

	}

	private function set_json_vars( $vars ) {
		foreach ( $vars as $key => $var ) {
			$vars ["${key}_arr"] = $var;
			$vars [ $key ] = json_encode( $var );
		}
		$this->set_vars( $vars );
	}

	private function get_labels( &$results, $fields ) {
		$labels = [];
		foreach ( $results as $result ) {
			$label = '';
			foreach ( $fields as $field ) {
				$label .= $result[ $field ] . ' ';
			}
			$labels [] = $label;
		}

		return $labels;
	}

	private function get_datas( &$results, $fields ) {
		$datas = [];
		foreach ( $results as $result ) {
			$data = 0;
			foreach ( $fields as $field ) {
				$data += $result[ $field ];
			}
			$datas [] = $data;
		}

		return $datas;
	}

	private function merge_results( &$array1, &$array2, $order_function = 'sort_date' ) {

		$months = [];

		// Primer faig un array dels mesos
		for ( $i = 1; $i < 3; $i ++ ) {
			foreach ( ${"array$i"} as $key => $rs ) {
				$month = $rs['month'];
				$year  = $rs['year'];
				$total = $rs['total'];

				if ( ! isset ( $months ["$month $year"] ) ) {
					$months ["$month $year"] = ['month' => $month, 'year' => $year];
				}
				$months ["$month $year"]["array$i"] = $total;

			}
		}

		// Ordeno per mesos
		uksort( $months, array( $this, $order_function ) );

		// Refaig arrays
		$array1 = $array2 = [];
		foreach ( $months as $key => $rs ) {
			$total1 = isset( $rs['array1'] ) ? $rs['array1'] : '0';
			$total2 = isset( $rs['array2'] ) ? $rs['array2'] : '0';
			$month = $rs['month'];
			$year = $rs['year'];

			$array1 [] = [
				'month' => $month,
				'year' => $year,
				'total' => $total1,
			];
			$array2 [] = [
				'month' => $month,
				'year' => $year,
				'total' => $total2,
			];
		}

		return;
	}

	private function sort_date( $a, $b ) {
		return strtotime( $a ) - strtotime( $b );
	}

	private function sort_quarter( $a, $b ) {

		$a = $this->get_trimestre_date($a);
		$b = $this->get_trimestre_date($b);

		return strtotime( $a ) - strtotime( $b );
	}

	private function get_trimestre_date($a) {

		$a = str_replace('1 ', 'January ', $a );
		$a = str_replace('2 ', 'April ', $a );
		$a = str_replace('3 ', 'July ', $a );
		$a = str_replace('4 ', 'September ', $a );

		return $a;
	}

	private function get_trimestre_names($as) {

		foreach ( $as as &$a ) {
			$a = str_replace( '1 ', '1 Trimestre - ', $a );
			$a = str_replace( '2 ', '2 Trimestre - ', $a );
			$a = str_replace( '3 ', '3 Trimestre - ', $a );
			$a = str_replace( '4 ', '4 Trimestre - ', $a );
		}

		return $as;
	}
}