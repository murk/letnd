<?
// menus eina
if (!defined('INMO_MENU_PROPERTY')){
	define('INMO_MENU_PROPERTY', 'Property');
	define('INMO_MENU_PROPERTY_NEW', 'Enter new property');
	define('INMO_MENU_PROPERTY_LIST', 'List properties');
	define('INMO_MENU_SEARCH_FORM', 'Search and edit properties');
	define('INMO_MENU_PROPERTY_BIN', 'Recycling bin');
	define('INMO_MENU_CATEGORY', 'Property category');
	define('INMO_MENU_CATEGORY_NEW', 'Enter new category');
	define('INMO_MENU_CATEGORY_LIST', 'Category list');
	define('INMO_MENU_TRANSLATION', 'Translations');
	define('INMO_MENU_PROPERTY_TRANSLATION_LIST', 'List of properties for translation');
	define('INMO_MENU_CATEGORY_TRANSLATION_LIST', 'List of categories for translation');
	define('INMO_MENU_CONFIG', 'Property configuration');
	define('INMO_MENU_CONFIGADMIN_LIST', 'Configuration (admin. only)');
	define('INMO_MENU_CONFIGPUBLIC_LIST', 'Configuration (public)');
	define('INMO_MENU_CONFIGADMIN', 'Configuration (admin. only)');
	define('INMO_MENU_CONFIGPUBLIC', 'Configuration (public)');
	define('INMO_MENU_ZONE', 'Zones');
	define('INMO_MENU_ZONE_NEW', 'Enter new zone');
	define('INMO_MENU_ZONE_LIST', 'List zones');
	define('INMO_MENU_ZONE_BIN', ' Recycling bin ');
	define('INMO_MENU_TV', 'Virtual shop window');
	define('INMO_MENU_TV_LIST', 'Create shop window');
	define('INMO_MENU_TV_LIST_SELECTED', 'Properties at the storefront');
	define('INMO_MENU_TV_LIST_DOWNLOAD', 'Download storefront');
	define('INMO_MENU_PROPERTY_MAP_LIST', 'Search through map');
	define('INMO_MENU_ADJECTIVE_NEW', 'Entrar nou adjectiu');
	define('INMO_MENU_ADJECTIVE_LIST', 'Listar adjectius');

	define('BOOKING_MENU_BOOK','Reserves');
	define('BOOKING_MENU_BOOK_NEW','Entrar nova reserva');
	define('BOOKING_MENU_BOOK_LIST','Llistar reserves');
	define('BOOKING_MENU_BOOK_LIST_PAST','Reserves finalitzades');
	define('BOOKING_MENU_BOOK_SEARCH','Disponibilitat');
	
	define('BOOKING_MENU_EXTRA','Serveis');
	define('BOOKING_MENU_EXTRA_NEW','Entrar nou servei');
	define('BOOKING_MENU_EXTRA_LIST','Llistar serveis');
	define('BOOKING_MENU_EXTRA_LIST_BIN','Paperera de reciclatge');
	
	define('BOOKING_MENU_SEASON','Temporades');
	define('BOOKING_MENU_SEASON_NEW','Entrar nova temporada');
	define('BOOKING_MENU_SEASON_LIST','Llistar temporades');

	define ('TITLE_COMARCA_ADMIN','Per zona: ');
	define('PROPERTY_AUTO_REF', 'Automàtica');

	define('BOOKING_MENU_PROMCODE','Codi promocional');
	define('BOOKING_MENU_PROMCODE_NEW','Crear codi');
	define('BOOKING_MENU_PROMCODE_LIST','Llistar codi');
	define('BOOKING_MENU_PROMCODE_LIST_BIN','Paperera');

	define('BOOKING_MENU_POLICE','Registre Mossos');
	define('BOOKING_MENU_POLICE_LIST','Llistar reserves');
}

// captions seccio
$gl_caption_book['c_payment'] = 'Payment';
$gl_caption_book['c_payed'] = 'Payed';
$gl_caption_book['c_payment_num'] = 'Núm.';
$gl_caption_book['c_payment_1'] = 'Payment 1';
$gl_caption_book['c_payment_2'] = 'Payment 2';
$gl_caption_book['c_payment_3'] = 'Payment 3';
$gl_caption_book['c_payment_4'] = 'Payment 4';
$gl_caption_book['c_payment_done_1'] =
$gl_caption_book['c_payment_done_2'] =
$gl_caption_book['c_payment_done_3'] =
$gl_caption_book['c_payment_done_4'] = 'Payed';
$gl_caption_book['c_payment_entered_1'] =
$gl_caption_book['c_payment_entered_2'] =
$gl_caption_book['c_payment_entered_3'] =
$gl_caption_book['c_payment_entered_4'] = 'Date';
$gl_caption_book['c_payment_rest'] = 'Pending paymnent';
$gl_caption_book['c_payment_uncheck_message'] = '¿Are you sure you want to uncheck the payment?';
$gl_caption_book['c_payment_send_mail'] = 'send&nbsp;e-mail';
$gl_caption_book['c_payment_send_mail_subject'] = 'Payment notification of booking: %1$s';
$gl_caption_book['c_payment_send_mail_body'] = 'Dear %1$s, <p>We notify you that we have received the payment with an amount of %2$s € of the booking núm. %3$s on the date %4$s</p>';
$gl_caption_book['c_payment_send_mail_body_rest'] = '<p>Remaining amount, %1$s €</p>';
$gl_caption_book['c_payment_send_mail_body2'] = '<p>Sincerely yours</p>';
$gl_caption_book['c_payment_mail_sent'] = 'The e-mail has been sent to:';
$gl_caption_book['c_payment_mail_not_sent'] = 'The e-mail could not be sent to:';
$gl_caption_book['c_payment_mail_not_sent_no_mail'] = 'The customer has no email address defined';
$gl_caption_book['c_is_owner'] = 'Owner';
$gl_caption_book['c_caption_is_owner'] = 'All';
$gl_caption_book['c_is_owner_0'] = 'No';
$gl_caption_book['c_is_owner_1'] = 'Yes';

$gl_caption_book['c_property_title'] = 'Property';
$gl_caption_book['c_names'] = 'Customers';
$gl_caption_book['c_dates'] = 'Booking date';
$gl_caption['c_dates_from'] = 'Del';
$gl_caption['c_dates_to'] = 'al';
$gl_caption_book['c_date_in'] = 'Date of entry';
$gl_caption_book_list['c_date_in'] = 'Entry';
$gl_caption_book['c_date_out'] = 'Date of departure';
$gl_caption_book_list['c_date_out'] = 'Departure';
$gl_caption_book['c_book_entered'] = 'Booked';
$gl_caption_book['c_capacity'] = 'Capacity';
$gl_caption_book['c_person'] = 'People';
$gl_caption_book['c_adult'] = 'Adults';
$gl_caption_book['c_child'] = 'Children';
$gl_caption_book['c_baby'] = 'Babies';
$gl_caption_book['c_book_id'] = 'Booking';
$gl_caption_book['c_property_id'] = 'Property';
$gl_caption_book['c_choose_first'] = "In first , must choose Adult 1";
$gl_caption['c_status'] = "State";
$gl_caption['c_status_group_free'] = "Free";
$gl_caption['c_status_group_not_free'] = "Occupied";
$gl_caption['c_status_pre_booked'] = "Pre booked";
$gl_caption['c_status_cancelled'] = "Cancelled";
$gl_caption['c_status_booked'] = "Booked";
$gl_caption['c_status_confirmed_email'] = "Confirmed by email";
$gl_caption['c_status_confirmed_post'] = "Confirmed by post";
$gl_caption['c_status_contract_signed'] = "Contract signed";
$gl_caption_book['c_caption_filter_status'] = 'All';

for ($i=0;$i<=25;$i++){	
	
	$gl_caption_book['c_adult_'.$i] = $i;
	$gl_caption_book['c_child_'.$i] = $i;
	$gl_caption_book['c_baby_'.$i] = $i;
	
}

$gl_caption_book ['c_maximum'] = 'Maximum';
$gl_caption_book ['c_name'] = 'Name';
$gl_caption_book ['c_surnames'] = 'Surname';
$gl_caption_book ['c_mail'] = 'E-mail';
$gl_caption_book ['c_phone'] = 'Telephone';
$gl_caption_book ['c_comment'] = 'Comments';
$gl_caption_book ['c_comment_private'] = 'Private comments';
//$gl_caption_book ['c_property_id'] = '';
$gl_caption_book ['c_send_new'] = 'Book';
$gl_caption_book ['c_custumers'] = 'Customers';
$gl_caption_book ['c_price_extras'] = 'Extras';
$gl_caption_book ['c_price_extras_search'] = 'Extras price';
$gl_caption_book ['c_price_total'] = 'Total price';
$gl_caption_book ['c_price_book'] = 'Booking price';
$gl_caption_book ['c_price'] = 'Price';
$gl_caption_book ['c_edit_book_button'] = 'Edit booking';
$gl_caption_book ['c_download_contract'] = 'Contract';
$gl_caption_book ['c_custumer_search_group'] = 'Customers';
$gl_caption_book ['c_extra_search_group'] = 'Extras';
$gl_caption_book ['c_recalculate_rate'] = 'Recalculate price';
$gl_caption_book ['c_is_manual_price'] = 'Manual price';

$gl_caption_book['c_form_title'] = 'Booking';

$gl_messages_book['added'] = "Booking realized between the dates %s and %s";
$gl_messages_book['added2'] = "Booking realized for %s between the dates %s and %s";
$gl_messages_book['saved'] = "Updated reservation  between the dates %s %s";
$gl_messages_book['saved2'] = "Updated reservation for %s between the dates";
$gl_messages_book['deleted'] = "Booking annulled between %s and %s";
$gl_messages_book['deleted2'] = "Booking annulled for %s between %s and %s";
$gl_messages_book['full'] = "Is complety  full between:  %s and %s";
$gl_messages_book['full2'] = 'This property is completely full between : %s and %s';
$gl_messages_book['full3'] = 'Conflicting booking';
$gl_messages_book['has_no_range'] = 'This property isn´t rented in these dates';
$gl_messages_book['too_many_person'] = $gl_caption['c_too_many_person_error'] = 'The number of persons exceeds to the maximum for this property';
$gl_messages_book['exceeds_31_days'] = 'Els habitatges de lloguer de temporada no es poden llogar per un periode superior a 31 dies, per qualsevol dubte, si us plau contacti la nostra oficina';


// custumers
$gl_caption['c_add_new_custumer'] = "Add new Customer";
$gl_caption['c_edit_custumer'] = "Edit customer";
$gl_caption['c_add_custumer'] = "Select costumer";

$gl_caption['c_adult_singular'] = "Adult";
$gl_caption['c_child_singular'] = "Children";
$gl_caption['c_baby_singular'] = "Baby";


$gl_caption_extra ['c_extra_book_selected'] = 'Select';
$gl_caption_extra ['c_extra_book_input'] = '';

// serveis
$gl_caption['c_extra_type'] = "Type";
$gl_caption['c_extra_type_required'] = "Required";
$gl_caption['c_extra_type_optional'] = "Additional";
$gl_caption['c_extra_type_included'] = "Included";
$gl_caption['c_extra'] = "Extras";
$gl_caption['c_has_quantity'] = "Selected quantity";
$gl_caption_list['c_has_quantity'] = "Quantity";
$gl_caption['c_price'] = "Price";
$gl_caption['c_extra_price'] = "Price";
$gl_caption['c_comment'] = "Comments";
$gl_caption['c_extras'] = "Extras";
$gl_caption['c_concept'] = "Concept";
$gl_caption['c_concept_normal'] = "Extra";
$gl_caption['c_concept_deposit'] = "Deposit";
$gl_caption['c_concept_cleaning'] = "Cleaning";

$gl_caption['c_extra_type_required_search'] = "Required extras";
$gl_caption['c_extra_type_optional_search'] = "Additional extras";
$gl_caption['c_extra_type_included_search'] = "Included extras";
$gl_caption['c_extra_total_price_search'] = "Extra total price";
$gl_caption['c_extra_quantity_search'] = "Extra quantity";
$gl_caption['c_extra_type_search'] = "Extra type";

// temporades
$gl_caption['c_season'] = $gl_caption['c_season_id'] = "Season";
$gl_caption['c_seasons'] = "Seasons";
$gl_caption['c_ordre'] = "Order";
$gl_caption['c_date_start'] = "Start";
$gl_caption['c_date_end'] = "Final";
$gl_caption['c_minimum_nights'] = "Minimum";
$gl_caption['c_minimum_nights_1'] = "1 night";
$gl_caption['c_minimum_nights_2'] = "2 nights";
$gl_caption['c_minimum_nights_3'] = "3 nights";
$gl_caption['c_minimum_nights_4'] = "4 nights";
$gl_caption['c_minimum_nights_5'] = "5 nights";
$gl_caption['c_minimum_nights_6'] = "6 nights";
$gl_caption['c_minimum_nights_7'] = "1 week";
$gl_caption['c_minimum_nights_8'] = "8 nights";
$gl_caption['c_minimum_nights_9'] = "9 nights";
$gl_caption['c_minimum_nights_10'] = "10 nights";
$gl_caption['c_minimum_nights_11'] = "11 nights";
$gl_caption['c_minimum_nights_12'] = "12 nights";
$gl_caption['c_minimum_nights_13'] = "13 nights";
$gl_caption['c_minimum_nights_14'] = "2 weeks";
$gl_caption['c_price_night'] = "Price per night";
$gl_caption_list['c_price_night'] = "Night";
$gl_caption['c_price_weekend'] = "Price per weekend night";
$gl_caption_list['c_price_weekend'] = "Weekend";
$gl_caption['c_price_more_week'] = "Price + than 7 nights";
$gl_caption['c_price_week'] = "Price a week";
$gl_caption_list['c_price_week'] = "Week";
$gl_caption['c_price_week_type'] = "";
$gl_caption['c_price_week_type_week'] = "A week";
$gl_caption['c_price_week_type_night'] = "Per night";
$gl_caption['c_color_id'] = $gl_caption['c_color'] = "Colour";
$gl_caption['c_already_used'] = "Already assigned colours";

$gl_caption['c_detail_season'] = "Season";
$gl_caption['c_detail_day'] = "Day";
$gl_caption['c_detail_type'] = "Rate";
$gl_caption['c_detail_price'] = "Price";
$gl_caption['c_detail_price_night'] = "Night";
$gl_caption['c_detail_price_weekend'] = "Weekend night";
$gl_caption['c_detail_price_week'] = "Week night price";
$gl_caption['c_detail_whole_week'] = "";


$gl_caption['c_whole_week'] = "Whole weeks";
$gl_caption_list['c_whole_week_0'] = "";
$gl_caption_list['c_whole_week_1'] = "Oui / Si";

$gl_caption['c_start_day'] = "First day";
$gl_caption['c_start_day_0'] = "Anything";
$gl_caption_list['c_start_day_0'] = "";
$gl_caption['c_start_day_1'] = "Mon.";
$gl_caption['c_start_day_2'] = "Thu.";
$gl_caption['c_start_day_3'] = "Wens.";
$gl_caption['c_start_day_4'] = "Thues";
$gl_caption['c_start_day_5'] = "Fri.";
$gl_caption['c_start_day_6'] = "Sat.";
$gl_caption['c_start_day_7'] = "Sun.";

$gl_caption_propertyrange['c_form_title'] = " Price booking period";
$gl_messages_propertyrange['full2'] = "There is already a defined season between dates: %s and %s for the property \"%s\"";

$gl_caption['c_minimum_nights_error'] = "Can´t book less than %s nights for the dates between %s and %s";
$gl_caption['c_whole_week_error'] = "Must book whole weeks for the dates %s to %s";
$gl_caption['c_start_day_error'] = "For this period it´s necessary to book whole weeks";
$gl_caption['c_equal_dates_error'] = "Two dates can´t be the same";
$gl_caption['c_no_date_selected'] = "Hasn´t been selected any date";



$gl_caption['c_promcode'] = 'Promotional code';
$gl_caption_promcode['c_name'] = 'Name';
$gl_caption_promcode['c_discount_pvp'] = 'Discount';
$gl_caption_promcode['c_discount_type'] = 'Discount rate';
$gl_caption_promcode['c_discount_type_fixed'] = 'Fixed amount';
$gl_caption_promcode['c_discount_type_percent'] = 'Percentege';
$gl_caption_promcode['c_minimum_amount'] = 'Minimal expense';
$gl_caption_promcode['c_start_date'] = 'Start date';
$gl_caption_promcode['c_end_date'] = 'Final date';
$gl_caption_book['c_promcode_discount'] = 'Dte. codi promocional';
$gl_caption['c_promcode_button'] = '';

$gl_caption_promcode['c_custumer_id'] = 'Costumer';
$gl_caption['c_custumer_caption'] = 'Everyone';
$gl_caption_promcode['c_custumer_id_format'] = '%s %s %s';
$gl_caption_promcode['c_one_time_only'] = 'Aplicar sólo una vez por cliente';
$gl_caption_promcode_list['c_one_time_only'] = 'Sólo una vez';


$gl_caption_police['c_police_search_title'] = 'Booking between dates';
$gl_caption['c_treatment_police'] = 'Sexe';
$gl_caption_police['c_treatment_sr'] = 'Masculí';
$gl_caption_police['c_treatment_sra'] = 'Femení';
$gl_caption['c_download_selected'] = 'Download file';


$gl_caption['c_tt'] =  $gl_caption['c_price_tt'] = 'Tourist tax';
$gl_caption['c_promcode_button'] = 'See';

$gl_caption['c_promcode_err_not_valid'] = 'The promotional code is not good';
$gl_caption['c_promcode_err_minimum'] = 'To apply the promotional code, the purchase must be over %s';
$gl_caption['c_promcode_err_expired'] = 'The promotional code has expired';
$gl_caption['c_promcode_err_used'] = 'This code has been used';
$gl_caption['c_promcode_err_not_logged'] = 'You have to be registered to apply the promotional code';

$gl_caption ['c_promcode']='Promotional code';

// buscador
$gl_caption['c_by_ref'] = '( <strong>Ref</strong>:</strong> "10 23 45" . <strong>Date:</strong> "12/2015" , "2015" , "22/12/2015" )';
$gl_caption['c_fields'] = "Fields";
$gl_caption['c_nights'] = "Nights";
$gl_caption['c_excel_name'] = "bookings";
$gl_caption['c_exact_date'] = "Exact date";

$gl_messages ['has_no_owner']="Attention, the property has no owner assigned";


// Pagament
$gl_caption['c_order_status'] = 'Payment status';
$gl_caption_list['c_order_status'] = 'Payment';
$gl_caption['c_order_status_paying'] = 'Outstanding';
$gl_caption['c_order_status_failed'] = 'Payment failed';
$gl_caption['c_order_status_pending'] = 'Pending payment to be accepted';
$gl_caption['c_order_status_denied'] = 'Pagament rebutjat';
$gl_caption['c_order_status_voided'] = 'Payment canceled';
$gl_caption['c_order_status_refunded'] = 'Payment canceled';
$gl_caption['c_order_status_completed'] = 'Completed payment';

$gl_caption['c_payment_method'] = 'Payment method';
$gl_caption['c_payment_method_paypal'] = 'Paypal';
$gl_caption['c_payment_method_account'] = 'Bank transfer';
$gl_caption['c_payment_method_directdebit'] = 'Direct debit';
$gl_caption['c_payment_method_lacaixa'] = 'Credit card';
$gl_caption['c_payment_method_none'] = '';
$gl_caption['c_payment_method_cash'] = 'Cash';
$gl_caption['c_payment_method_physicaltpv'] = 'Physical TPV';

for ( $i = 1; $i <= 4; $i ++ ) {
	$gl_caption[ 'c_order_status_' . $i ]                    = $gl_caption['c_order_status'];
	$gl_caption_list[ 'c_order_status_' . $i ]               = $gl_caption_list['c_order_status'];
	$gl_caption[ 'c_order_status_' . $i . '_paying' ]        = $gl_caption['c_order_status_paying'];
	$gl_caption[ 'c_order_status_' . $i . '_failed' ]        = $gl_caption['c_order_status_failed'];
	$gl_caption[ 'c_order_status_' . $i . '_pending' ]       = $gl_caption['c_order_status_pending'];
	$gl_caption[ 'c_order_status_' . $i . '_denied' ]        = $gl_caption['c_order_status_denied'];
	$gl_caption[ 'c_order_status_' . $i . '_voided' ]        = $gl_caption['c_order_status_voided'];
	$gl_caption[ 'c_order_status_' . $i . '_refunded' ]      = $gl_caption['c_order_status_refunded'];
	$gl_caption[ 'c_order_status_' . $i . '_completed' ]     = $gl_caption['c_order_status_completed'];
	$gl_caption[ 'c_payment_method_' . $i ]                  = $gl_caption['c_payment_method'];
	$gl_caption[ 'c_payment_method_' . $i . '_paypal' ]      = $gl_caption['c_payment_method_paypal'];
	$gl_caption[ 'c_payment_method_' . $i . '_account' ]     = $gl_caption['c_payment_method_account'];
	$gl_caption[ 'c_payment_method_' . $i . '_directdebit' ] = $gl_caption['c_payment_method_directdebit'];
	$gl_caption[ 'c_payment_method_' . $i . '_lacaixa' ]     = $gl_caption['c_payment_method_lacaixa'];
	$gl_caption[ 'c_payment_method_' . $i . '_none' ]        = $gl_caption['c_payment_method_none'];
	$gl_caption[ 'c_payment_method_' . $i . '_cash' ]        = $gl_caption['c_payment_method_cash'];
	$gl_caption[ 'c_payment_method_' . $i . '_physicaltpv' ] = $gl_caption['c_payment_method_physicaltpv'];
}

$gl_caption ['c_books_not_deleted_not_owner'] = "The marked booking have not been deleted because they where created by an external application";

$gl_caption['c_book_owner'] = "Booked by";