<?php
// no es poden posar ; entre mig de la consulta ja que parteix les consultes per el, a no ser que fem servir $update_sql en array() ;

if (is_table('product__product')){
	$update_sql .="
CREATE TABLE IF NOT EXISTS `product__variation` (
  `variation_id` int(11) NOT NULL auto_increment,
  `ordre` int(11) NOT NULL default '0',
  `bin` tinyint(1) NOT NULL,
  PRIMARY KEY  (`variation_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;
CREATE TABLE IF NOT EXISTS `product__variation_language` (
  `variation_id` int(11) NOT NULL default '0',
  `language` char(3) collate utf8_spanish2_ci NOT NULL default '',
  `variation` varchar(255) collate utf8_spanish2_ci NOT NULL default ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;
CREATE TABLE IF NOT EXISTS `product__product_to_variation` (
  `product_variation_id` int(11) NOT NULL auto_increment,
  `product_id` int(11) NOT NULL default '0',
  `variation_id` int(11) NOT NULL default '0',
  `variation_pvp` decimal(11,2) NOT NULL default '0.00',
  `variation_pvd` decimal(11,2) NOT NULL default '0.00',
  PRIMARY KEY  (`product_variation_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;
ALTER TABLE `product__brand` ENGINE = MYISAM;
ALTER TABLE `product__color` ENGINE = MYISAM;
ALTER TABLE `product__color_language` ENGINE = MYISAM;
ALTER TABLE `product__configadmin` ENGINE = MYISAM;
ALTER TABLE `product__configadmin_language` ENGINE = MYISAM;
ALTER TABLE `product__configpublic` ENGINE = MYISAM;
ALTER TABLE `product__configpublic_language` ENGINE = MYISAM;
ALTER TABLE `product__material` ENGINE = MYISAM;
ALTER TABLE `product__material_language` ENGINE = MYISAM;
ALTER TABLE `product__product_file` ENGINE = MYISAM;
ALTER TABLE `product__product_image` ENGINE = MYISAM;
ALTER TABLE `all__block` ENGINE = MYISAM;
ALTER TABLE `all__country` ENGINE = MYISAM;
ALTER TABLE `all__page` ENGINE = MYISAM;
ALTER TABLE `contact__configadmin` ENGINE = MYISAM;
ALTER TABLE `contact__configpublic` ENGINE = MYISAM;
ALTER TABLE `contact__contact` ENGINE = MYISAM;
ALTER TABLE `missatge__missatge` ENGINE = MYISAM;";
}
if (!Db::get_first("SELECT count(*) FROM all__menu WHERE menu_id = '20060'")){
	$update_sql .="
INSERT INTO `all__menu` (`menu_id`, `menu_group`, `tool`, `tool_section`, `action`, `parent_id`, `toolmode_id`, `variable`, `ordre`, `link`, `process`, `bin`, `read1`, `write1`) VALUES
(20060, 'product', 'product', 'variation', '', 20000, 1, 'PRODUCT_MENU_VARIATION', 0, '', '', 0, ',1,2,3,', ',1,2,'),
(20061, 'product', 'product', 'variation', 'show_form_new', 20060, 1, 'PRODUCT_MENU_VARIATION_NEW', 1, '', '', 0, ',1,2,3,', ',1,2,'),
(20062, 'product', 'product', 'variation', 'list_records', 20060, 1, 'PRODUCT_MENU_VARIATION_LIST', 2, '', '', 0, ',1,2,3,', ',1,2,'),
(20063, 'product', 'product', 'variation', 'list_records_bin', 20060, 1, 'PRODUCT_MENU_VARIATION_LIST_BIN', 3, '', '', 0, ',1,2,3,', ',1,2,');";
}
?>