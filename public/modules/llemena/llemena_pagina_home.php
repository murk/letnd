<?
/**
 * LlemenaPaginaHome
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2011
 * @version $Id$
 * @access public
 */
class LlemenaPaginaHome extends Module{

	function __construct(){
		parent::__construct();
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function get_records()
	{
		$module = Module::load('llemena','pagina');
		$module->parent = 'home';
		$this->get_agenda($module);
		return $module->do_action('get_record');
	}
	function get_agenda(&$module)
	{
		$module_agenda = Module::load('llemena','agenda');
		$module_agenda->parent = 'home';
		$module->set_loop('loop', $module_agenda->do_action('get_records'));
	}
	function get_form()
	{	
	}
}	
?>