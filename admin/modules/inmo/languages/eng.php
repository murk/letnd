<?php
global $gl_caption_tv, $gl_messages_tv;
// menus eina
if (!defined('INMO_MENU_PROPERTY')){
	define('INMO_MENU_PROPERTY', 'Property');
	define('INMO_MENU_PROPERTY_NEW', 'Enter new property');
	define('INMO_MENU_PROPERTY_LIST', 'List properties');
	define('INMO_MENU_SEARCH_FORM', 'Search and edit properties');
	define('INMO_MENU_PROPERTY_BIN', 'Recycling bin');
	define('INMO_MENU_CATEGORY', 'Property category');
	define('INMO_MENU_CATEGORY_NEW', 'Enter new category');
	define('INMO_MENU_CATEGORY_LIST', 'Category list');
	define('INMO_MENU_TRANSLATION', 'Translations');
	define('INMO_MENU_PROPERTY_TRANSLATION_LIST', 'List of properties for translation');
	define('INMO_MENU_CATEGORY_TRANSLATION_LIST', 'List of categories for translation');
	define('INMO_MENU_CONFIG', 'Property configuration');
	define('INMO_MENU_CONFIGADMIN_LIST', 'Configuration (admin. only)');
	define('INMO_MENU_CONFIGPUBLIC_LIST', 'Configuration (public)');
	define('INMO_MENU_CONFIGADMIN', 'Configuration (admin. only)');
	define('INMO_MENU_CONFIGPUBLIC', 'Configuration (public)');
	define('INMO_MENU_ZONE', 'Zones');
	define('INMO_MENU_ZONE_NEW', 'Enter new zone');
	define('INMO_MENU_ZONE_LIST', 'List zones');
	define('INMO_MENU_ZONE_BIN', ' Recycling bin ');
	define('INMO_MENU_TV', 'Virtual shop window');
	define('INMO_MENU_TV_LIST', 'Create shop window');
	define('INMO_MENU_TV_LIST_SELECTED', 'Properties at the storefront');
	define('INMO_MENU_TV_LIST_DOWNLOAD', 'Download storefront');
	define('INMO_MENU_PROPERTY_MAP_LIST', 'Search through map');

	define ('TITLE_COMARCA_ADMIN','By zone: ');
	define('PROPERTY_AUTO_REF', 'Automatic');

	define('BOOKING_MENU_PROMCODE','Codi promocional');
	define('BOOKING_MENU_PROMCODE_NEW','Crear codi');
	define('BOOKING_MENU_PROMCODE_LIST','Llistar codi');
	define('BOOKING_MENU_PROMCODE_LIST_BIN','Paperera');

	define('BOOKING_MENU_POLICE','Registro Mossos');
	define('BOOKING_MENU_POLICE_LIST','Listar reservas');

	define('INMO_MENU_PORTAL', 'Portals');
	define('INMO_MENU_PORTAL_LIST', 'Select properties');
	define('INMO_MENU_PORTAL_LIST_SELECTED', 'Properties selected');
}






















// sobreescrits missatges demand custumer
if (isset($gl_messages)) {
	$gl_messages_demand = $gl_messages;
	$gl_messages_custumer = $gl_messages;
}
// sobreescrits end

// sobreescrits missatges property
$gl_messages_property['added']='The property has been successfully added. Next, you can insert the images for the new property';
$gl_messages_property['saved']='The property has been successfully modified';
$gl_messages_property['no_records']='There is no property in this listing';
$gl_messages_property['no_records_bin']= 'There is no property in the recycling bin';
$gl_messages_property['list_saved']='The changes to the property have been made successfully';
$gl_messages_property['list_not_saved']='No property has been modified: no change has been detected';
$gl_messages_property['list_deleted']='The selected properties have been deleted';
$gl_messages_property['list_bined']='The properties have been moved to the recycling bin';
$gl_messages_property['list_restored']='The properties have been successfully recovered';
// sobreescrits end

$gl_messages_category['related_not_activated'] = 'This category cannot be deactivated: it contains related properties';
$gl_messages_category['relateds_not_activated'] = 'The property categories marked cannot be deactivated: they contain related properties';

// captions seccio
$gl_caption_demand['c_form_title'] = 'Client request details: ';

$gl_caption['c_property_button'] = 'View properties';
$gl_caption['c_view_property'] = 'View propertie';

$gl_caption_property['c_move_to_category_id'] = '[property category]';
$gl_caption_property['c_move_to_category_id2'] = 'Change selected properties to:';
$gl_caption_property['c_move_to_zone_id'] = '[zone]';
$gl_caption_property['c_move_to_zone_id2'] = 'Change selected properties to:';
$gl_caption_property['c_move_to_zone_id3'] = '[no zone]';

$gl_caption_category['c_ordre'] = 'Order';
$gl_caption_category['c_ref_code'] = 'Property reference code';
$gl_caption_property['c_room'] = 'Rooms';
$gl_caption_property['c_bathroom'] = 'Bathrooms';
$gl_caption_property['c_wc'] = 'Toilet';
$gl_caption_property['c_laundry'] = 'Washing machine';
$gl_caption_property['c_parking'] = 'Parking';
$gl_caption_property['c_garage'] = 'Garage';
$gl_caption_property['c_garage_area'] = 'Surface area';
$gl_caption_property['c_storage'] = 'Utility room/area';
$gl_caption_property['c_storage_area'] = 'Surface area';
$gl_caption_property['c_terrace'] = 'Terrace';
$gl_caption_property['c_terrace_area'] = 'Surface area';
$gl_caption_property['c_garden'] = 'Garden';
$gl_caption_property['c_garden_area'] = 'Surface area';
$gl_caption_property['c_swimmingpool'] = 'Swimming pool';
$gl_caption_property['c_swimmingpool_area'] = 'Surface area';
$gl_caption_property['c_citygas'] = 'Mains gas';
$gl_caption_property['c_centralheating'] = 'Central heating';
$gl_caption_property['c_airconditioned'] = 'Air conditioning';
$gl_caption_property['c_ref'] = 'Ref.';
$gl_caption_property['c_ref_collaborator'] = 'Ref. collaborator';
$gl_caption_property['c_ref_cadastre']='Ref. Catastral';
$gl_caption_property['c_gaining']='Gaining';
$gl_caption_property['c_place_id']='Office';
$gl_caption_property['c_cedula']='Cédula Habitabilitat';








$gl_caption_property['c_property_private'] = 'Private name';
$gl_caption['c_category_id'] = 'Category';
$gl_caption_property['c_floor_space'] = 'Surface area';
$gl_caption_property['c_land'] = 'Land';
$gl_caption_property['c_land_units'] = 'Units';
$gl_caption_property['c_m2'] = 'm2';
$gl_caption_property['c_ha'] = 'hectares';
$gl_caption_property['c_country_id'] = 'Country';
$gl_caption_property['c_price_private'] = 'Owner price';
$gl_caption_property['c_price'] = 'Price';
$gl_caption_property['c_price_m2'] = 'Price/m2';
$gl_caption_property['c_zoom'] = 'Zoom';
$gl_caption_property['c_price_consult'] = 'Price on application';
$gl_caption_property['c_price_consult_1'] = 'Price on application';
$gl_caption_property['c_price_history_link'] = 'Price history';
$gl_caption_property['c_owner_id'] = '';
$gl_caption_property['c_status'] = 'Status';
$gl_caption_property['c_prepare'] = 'Being prepared';
$gl_caption_property['c_review'] = 'Review';
$gl_caption_property['c_onsale'] = 'Public';
$gl_caption_property['c_sold'] = 'Sold';
$gl_caption_property['c_archived'] = 'Archived';
$gl_caption_property['c_reserved'] = 'Reserved';
$gl_caption_property['c_zone'] = 'Zone'; // per la part publica
$gl_caption_property['c_zone_id'] = 'Zone';
$gl_caption_property['c_zone_id_caption'] = '[no zone]';
$gl_caption_property['c_provincia_id'] = 'Province';
$gl_caption_property['c_comarca_id'] = 'District';
$gl_caption_property['c_municipi_id'] = $gl_caption_property['c_municipi'] = 'Municipality';
$gl_caption_property['c_property'] = 'Property name';
$gl_caption_property['c_property_title'] = 'Brief description';
$gl_caption_property['c_entered'] = 'Created';
$gl_caption_property['c_ref_number'] = '';
$gl_caption_property['c_home'] = 'Highlighted';
$gl_caption_property['c_offer'] = 'Offer';
$gl_caption_property['c_data'] = 'Property details';
$gl_caption_property['c_descriptions'] = 'Descriptions';
$gl_caption_property['c_details'] = 'Details';
$gl_caption_property['c_description'] = 'Description';
$gl_caption_property['c_details_temp'] = 'Season renta';
$gl_caption_property['c_0'] = '';
$gl_caption_property['c_1'] = '1';
$gl_caption_property['c_2'] = '2';
$gl_caption_property['c_3'] = '3';
$gl_caption_property['c_4'] = '4';
$gl_caption_property['c_5'] = '5';
$gl_caption_property['c_6'] = '6';
$gl_caption_property['c_7'] = '7';
$gl_caption_property['c_8'] = '8';
$gl_caption_property['c_9'] = '9';
$gl_caption_property['c_10'] = '10';
$gl_caption_property['c_11'] = '11';
$gl_caption_property['c_12'] = '12';
$gl_caption_property['c_13'] = '13';
$gl_caption_property['c_14'] = '14';
$gl_caption_property['c_15'] = '15';
$gl_caption_property['c_16'] = '16';
$gl_caption_property['c_17'] = '17';
$gl_caption_property['c_18'] = '18';
$gl_caption_property['c_19'] = '19';
$gl_caption_property['c_20'] = '20';
$gl_caption_property['c_tipus'] = 'Transaction';
$gl_caption_property['c_tipus_sell'] = 'Sale';
$gl_caption_property['c_tipus_rent'] = 'Rental';
$gl_caption_property['c_filter_status'] = 'All statuses';
$gl_caption_property['c_filter_category_id'] = 'All categories';
$gl_caption_property['c_filter_tipus'] = 'All transactions';
$gl_caption_property['c_address'] = 'Address';
$gl_caption_property['c_adress'] = 'Street';
$gl_caption_property['c_numstreet'] = 'Number';
$gl_caption_property['c_block'] = 'Block';
$gl_caption_property['c_flat'] = 'Floor';
$gl_caption_property['c_door'] = 'Door';
$gl_caption_property['c_observations'] = 'Comments';
$gl_caption_property['c_data_private'] = 'Private details';
$gl_caption_property['c_data_public'] = 'Public details';
$gl_caption_property['c_tipus2'] = 'Work category';
$gl_caption_property['c_elevator'] = 'Lift';
$gl_caption_property['c_balcony'] = 'Balcony';
$gl_caption_property['c_new'] = 'New construction';
$gl_caption_property['c_second'] = 'Pre-owned';
$gl_caption_property['c_furniture'] = 'Furnished';
$gl_caption_property['c_file'] = 'Documents';
$gl_caption_property['c_video'] = $gl_caption_property['c_videoframe'] = 'Video';
$gl_caption_property['c_edit_custumer'] = 'Owners';
$gl_caption_property['c_edit_custumer_button'] = 'Edit owners';

$gl_caption_property['c_mls']='MLS';
$gl_caption_property['c_share_mls']='Share by MLS';
$gl_caption_property['c_comission'] = 'MLS commission';

$gl_caption_property['c_filter_activated'] = 'All customers';
$gl_caption_property['c_activated_0'] = 'Deactivated';
$gl_caption_property['c_activated_1'] = 'Activated';

$gl_caption_property['c_efficiency_letter'] = 'Letter';
$gl_caption_property['c_efficiency_number'] = 'Energy';
$gl_caption_property['c_efficiency_number2'] = 'Emission';
$gl_caption_property['c_sea_distance'] = 'Distance to the sea';

$gl_caption_property['c_expenses'] = 'Expenses';
$gl_caption_property['c_community_expenses'] = 'Community expenses';
$gl_caption_property['c_ibi'] = 'Property tax';
$gl_caption_property['c_municipal_tax'] = 'Local taxes';


$gl_caption_custumer['c_form_custumer_title'] = 'Property owners:';
$gl_caption_custumer['c_new_custumer'] = 'Enter new owner';
$gl_caption_custumer['c_add_custumer'] = 'Assign owner';
$gl_caption_custumer['no_assigned'] = 'There is no owner assigned to this property';

$gl_caption_zone['c_zone'] ='Name of urban district and/or zone';
$gl_caption_zone['c_municipi_id'] ='municipality';

// sobreescrits images
$gl_caption_image['c_edit'] = 'Property images';
// sobreescrits end

//zones
$gl_caption_zone['c_municipi_id'] ='Municipality';
$gl_caption_zone['c_s_municipi_id'] ='Search for municipality';
$gl_caption_zone['c_zone'] ='Name of urban district and/or zone';

// nous globals
$gl_caption['c_by_ref'] = '(or reference nos. separated by spaces, e.g. "456 245 45")';
$gl_caption['c_by_words'] = 'By words';

$gl_caption['c_list_title'] = 'View listings';
$gl_caption['c_search_title'] = 'Search for property';
$gl_caption['c_filter_title'] = 'Filter properties';
$gl_caption['c_filter_title2'] = 'Manage properties';
$gl_caption['c_all_ordre'] = 'ordered by:';
$gl_caption['c_category_title'] = 'By category';
$gl_caption['c_comarca_title'] = 'By Comarca';
$gl_caption['c_municipi_title'] = 'By municipality';
$gl_caption['c_zone_title'] = 'By zone';
$gl_caption['c_status_title'] = 'By status';
$gl_caption['c_description_title'] = 'By descriptions to do';
$gl_caption['c_price_title'] = 'By price';
$gl_caption['c_images_title'] = 'By images';
$gl_caption['c_has_no_images'] = 'Without images';
$gl_caption['c_has_images'] = 'With images';
$gl_caption['c_images_indiferent'] = 'With or without images';
$gl_caption['c_ref'] = 'Reference no.';
$gl_caption['c_property_private'] = 'Private name';
$gl_caption['c_entered'] = 'Date';
$gl_caption['c_active'] = 'Activity';
$gl_caption['c_category_original'] = 'Original category';
$gl_caption['c_category'] = 'Category';
$gl_caption['c_status'] = 'Status';
$gl_caption['c_show_results'] = 'Show results';
$gl_caption['c_floor_space_title'] = 'Surface area';
$gl_caption['c_land_title'] = 'Land';
$gl_caption['c_ordered_title'] = 'Ordered by';
$gl_caption['c_user_title'] = 'Agents';
$gl_caption['c_custom_title'] = 'Característiques configurables';
$gl_caption['c_m2'] = 'm2';
$gl_caption['c_ha'] = 'ha';
$gl_caption['c_showwindow'] = 'Print shop window';
$gl_caption['c_album'] = 'Print album';


// NO TRADUIT

$gl_caption['c_images'] = 'Images';
$gl_caption['c_no_available'] = 'This property isn\'t available any more!';
$gl_caption['c_private'] = 'Hide private';
$gl_caption_tv['c_send_edit'] = 'Add to the storefront';
$gl_caption_tv['c_download'] = 'Files';
$gl_caption_tv['c_main_files'] = 'Main files';
$gl_caption_tv['c_property_file'] = 'Property';

$gl_caption['c_type_title'] = 'By type';


$gl_messages_tv['list_saved']='The storefront has been correctly modified';


$gl_caption_property['c_living'] = 'Living rooms';
$gl_caption_property['c_dinning'] = 'Dinning rooms';
$gl_caption_property['c_service_room'] = 'Service room';
$gl_caption_property['c_service_bath'] = 'Service bathroom';
$gl_caption_property['c_study'] = 'Study';
$gl_caption_property['c_courtyard'] = 'Courtyard';
$gl_caption_property['c_courtyard_area'] = 'Surface area';
$gl_caption_property['c_floor'] = 'Floor';
$gl_caption_property['c_parket'] = 'Parket';
$gl_caption_property['c_ceramic'] = 'Ceramic';
$gl_caption_property['c_gres'] = 'Gres';
$gl_caption_property['c_laminated'] = 'Laminated';
$gl_caption_property['c_estrato'] = 'Estrato';
$gl_caption_property['c_estrato_e1'] = '1';
$gl_caption_property['c_estrato_e2'] = '2';
$gl_caption_property['c_estrato_e3'] = '3';
$gl_caption_property['c_estrato_e4'] = '4';
$gl_caption_property['c_estrato_e5'] = '5';
$gl_caption_property['c_estrato_e6'] = '6';
$gl_caption_property['c_edit_map'] = 'Map';
$gl_caption_property['c_edit_booking'] = 'Bookink';
$gl_caption_property['c_edit_booking_extra'] = 'Prices - Extras';
$gl_caption_property['c_edit_booking_season'] = 'Seasons';
$gl_caption_property['c_form_map_title'] = 'Property location';
$gl_caption_property['c_form_booking_title'] = 'Property\'s Extra stuff';
$gl_caption_property['c_form_booking_extra_title'] = 'Season of the property';
$gl_caption_property['c_form_booking_season_title'] = 'Temporades de l\'immoble';
$gl_caption_property['c_save_map'] = 'Save location';
$gl_caption_property['c_latitude'] = 'Latitude';
$gl_caption_property['c_longitude'] = 'Longitude';
$gl_caption_property ['c_center_longitude'] = 'Map center latitude';
$gl_caption_property ['c_center_latitude'] = 'Map center longitude';
$gl_caption_property ['c_center_coords'] = 'Center';
$gl_caption_property['c_click_map'] = 'Click a point on the map to establish a location';
$gl_caption_property['c_show_map'] = 'Mostrar mapa part pública';
$gl_caption_property['c_show_map_point'] = 'Mostrar punt';
$gl_messages['point_saved'] = 'New location correctly saved';

$gl_caption_property['c_tipus_temp'] = 'Tourist rent';
$gl_caption_property['c_tipus_moblat'] = 'Rental with furniture';
$gl_caption_property['c_tipus_selloption'] = 'Rental with option for buying';

$gl_caption_property['c_efficiency'] = $gl_caption_property['c_efficiency2'] = 'Energy efficiency';
$gl_caption_property['c_efficiency_'] = '';
$gl_caption_property['c_efficiency_progress'] = 'Pending';
$gl_caption_property['c_efficiency_exempt'] = 'Exempt';
$gl_caption_property['c_efficiency_a'] = 'A';
$gl_caption_property['c_efficiency_b'] = 'B';
$gl_caption_property['c_efficiency_c'] = 'C';
$gl_caption_property['c_efficiency_d'] = 'D';
$gl_caption_property['c_efficiency_e'] = 'E';
$gl_caption_property['c_efficiency_f'] = 'F';
$gl_caption_property['c_efficiency_g'] = 'G';

$gl_caption_property['c_efficiency2_a'] = 'A';
$gl_caption_property['c_efficiency2_b'] = 'B';
$gl_caption_property['c_efficiency2_c'] = 'C';
$gl_caption_property['c_efficiency2_d'] = 'D';
$gl_caption_property['c_efficiency2_e'] = 'E';
$gl_caption_property['c_efficiency2_f'] = 'F';
$gl_caption_property['c_efficiency2_g'] = 'G';
$gl_caption_property['c_efficiency2_notdefined'] = '';
$gl_caption_property['c_efficiency_date']='Date';


$gl_caption_property['c_filter_room'] = 'Rooms';
$gl_caption_property['c_filter_room_0'] = 'None';

$gl_caption_property['c_a4'] = 'A4';
$gl_caption_property['c_a3'] = 'A3';
$gl_caption_property['c_caption_showwindow_top_fields'] = 'No limit';
$gl_caption_property['c_caption_showwindow_pages'] = $gl_caption_property['c_caption_album_pages'] = 'All';
$gl_caption_property['c_submenu_0'] = 'Language';
$gl_caption_property['c_submenu_1'] = 'Size paper';
$gl_caption_property['c_submenu_2'] = 'Number of pages';
$gl_caption_property['c_submenu_3'] = 'Limit of fields';
$gl_caption_property['c_submenu_4'] = 'Show';
$gl_caption_property['c_submenu_5'] = 'Save this configuration';
$gl_caption_property['c_submenu_6'] = 'Template';

$gl_caption_property['c_views'] = 'Views';
$gl_caption_property['c_sea_view'] = 'Sea view';
$gl_caption_property['c_clear_view'] = 'Clear view';
// FI NO TRADUIT

// nomes admin
$gl_caption_property['c_custom1'] = $gl_caption_property['c_custom2'] = $gl_caption_property['c_custom3'] = $gl_caption_property['c_custom4'] = $gl_caption_property['c_custom5'] = $gl_caption_property['c_custom6'] = $gl_caption_property['c_custom7'] = $gl_caption_property['c_custom8'] = '';









$gl_caption_property['c_user_id'] = 'Agent';
$gl_caption_property['c_filter_user_id'] = 'All agents';

$gl_caption_property['c_person'] = 'Persons';
$gl_caption_property['c_person_s'] = 'Person';
$gl_caption_property['c_person_p'] = 'Persons';


$gl_caption['c_edit_history'] = 'History';

$gl_caption_history['c_new_history'] = 'Add history';
$gl_caption_history['c_form_history_title'] = 'Property record:';
$gl_caption_history['no_assigned'] = $gl_messages_history['no_records'] = 'There is no records in this property';
$gl_caption_history ['c_custumer_id_format']='%s %s %s';
$gl_caption_history ['c_delete_button']='Eliminate';

$gl_messages_history['added']='L\'entrada s\'ha afegit correctament';
$gl_messages_history['saved']='L\'entrada ha estat modificada correctament';


$gl_caption_property['c_exact_result'] = 'We have found <strong>1</strong> demand that coincides with this property' ;
$gl_caption_property['c_exact_results'] = 'We have found <strong>%s</strong> demands that coincide with this property' ;
$gl_caption_property['c_exact_results_link'] = 'See results';
$gl_caption_property['c_aprox_result'] = 'We have found <strong>1</strong>aproximate demand for this property';
$gl_caption_property['c_aprox_results'] = 'We have found <strong>%s</strong> aproximate demands for this property';
$gl_caption_property['c_aprox_results_link'] = 'See results';
$gl_caption ['c_user_id_format']='%s %s';

$gl_caption['c_property_file_name'] = $GLOBALS['gl_caption']['c_file_name'];
$gl_caption['c_property_old_file_name'] = $GLOBALS['gl_caption']['c_old_file_name'];

$gl_caption['c_send_mail'] = 'Send email';

$gl_caption_property['c_expired'] = 'Expiry date';
$gl_caption_property['c_construction_date'] = 'Construction date';
$gl_caption_property['c_facing'] = 'Orientation';
$gl_caption_property['c_facing_'] = '';
$gl_caption_property['c_facing_n'] = 'North';
$gl_caption_property['c_facing_ne'] = 'North-east';
$gl_caption_property['c_facing_e'] = 'East';
$gl_caption_property['c_facing_se'] = 'South-East';
$gl_caption_property['c_facing_s'] = 'South';
$gl_caption_property['c_facing_so'] = 'South-west';
$gl_caption_property['c_facing_o'] = 'West';
$gl_caption_property['c_facing_no'] = 'North-west';
$gl_caption_property['c_exclusive'] = 'Exclusive';
$gl_caption_property['c_zip'] = 'Postal code';
$gl_caption_property['c_descriptionregister'] = 'Registry description';
$gl_caption['c_list_mls'] = 'List mls properties';
$gl_caption['c_phone1'] = 'Telephone';
$gl_caption['c_comercialname'] = 'Company';
$gl_caption['c_client_id']='';
$gl_caption['c_client_dir']='';
$gl_caption['c_domain']='';

$gl_caption['c_star']='Stars';
$gl_caption['c_star_0']='None';
$gl_caption['c_star_1']='1';
$gl_caption['c_star_2']='2';
$gl_caption['c_star_3']='3';
$gl_caption['c_star_4']='4';
$gl_caption['c_star_5']='5';

// Booking
$gl_caption_property['c_book_search_title'] = 'Find availability';
$gl_caption['c_dates_from'] = 'From';
$gl_caption['c_dates_to'] = 'to';
$gl_caption_property['c_date_in'] = 'Check- in date';
$gl_caption_property['c_date_out'] = 'Check-out date';
$gl_caption_property['c_adult'] = 'Adults';
$gl_caption_property['c_child'] = 'Children';
$gl_caption_property['c_baby'] = 'Babies';
$gl_caption_property['c_book_property_button'] = 'Book';
$gl_caption_property['c_time_in'] = 'Check-in time';
$gl_caption_property['c_time_out'] = 'Check-out time';

$gl_caption_property['c_marble'] = 'Marble';
$gl_caption_property['c_hut'] = 'HUT';
$gl_caption_property['c_level_count'] = 'Number of floors';

$gl_caption_property['c_managed_by_owner'] = 'Managed by owner';

// Traduccions
$gl_caption_adjective['c_gender'] = 'Génere';
$gl_caption_adjective['c_adjective_before'] = 'Antes';
$gl_caption_adjective['c_adjective_after'] = 'Después';

// Portal
$gl_caption['c_portal_config_title'] = "Publishing options";
$gl_caption['c_apicat_export_all'] = "Export all properties";
$gl_caption['c_apicat_export_all_text'] = "If you check, all public properties will be exported without the need of selecting any of them";
$gl_caption['c_apicat_export_all_text_2'] = "Only properties with price and ZIP Code or exact location will be exported, Api.cat doesn't allow tourist rental. Fotocasa needs floor space";
$gl_caption['c_excluded_'] = "Exportable - No exportable";
$gl_caption['c_excluded_apicat'] = "Excluded Apicat";
$gl_caption['c_excluded_fotocasa'] = "Excluded Fotocasa";
$gl_caption['c_excluded_habitaclia'] = "Excluded Habitaclia";

$gl_caption['c_no_subscrit'] = "Information on portals publication";
$gl_caption['c_no_subscrit_apicat'] = "You are not registered to publish on Api.cat";
$gl_caption['c_no_subscrit_fotocasa'] = "You are not registered to publish on Fotocasa";
$gl_caption['c_no_subscrit_habitaclia'] = "You are not registered to publish on Habitaclia";

$gl_caption['c_portal_check_title'] = "The following data is needed to export";
$gl_caption['c_fotocasa_check_price'] = "Price";
$gl_caption['c_fotocasa_check_latitude'] = "Map location";
$gl_caption['c_fotocasa_check_floor_space'] = "Floor space";

$gl_caption['c_habitaclia_check_ref'] = "Reference";
$gl_caption['c_habitaclia_check_adress'] = "Address and map location with \"Show point\" checked";

$gl_caption['c_apicat_check_tipus'] = "Turístic rent not allowed";
$gl_caption['c_apicat_check_consult'] = "Uncheck \"Price on application\"";
$gl_caption['c_apicat_check_zip'] = "Postal code or map location with \"Show point\" checked";