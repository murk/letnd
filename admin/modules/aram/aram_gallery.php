<?
/**
 * AramGallery
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2012
 * @version $Id$
 * @access public
 */
class AramGallery extends Module{

	function __construct(){
		parent::__construct();
		$_GET['gallery_id'] = '1'; // així sempre edito imatges del registre 1
	}

	function manage_images()
	{
	    $image_manager = new ImageManager($this);
	    $image_manager->execute();
	}
}
?>