	<script language="JavaScript" src="/admin/jscripts/validacio.js?v=<?=$version?>"></script>
	<script language="JavaScript" src="/admin/modules/custumer/jscripts/custumer.js?v=<?=$version?>"></script>
	<script language="JavaScript" src="/admin/jscripts/assign.js?v=<?=$version?>"></script>
	<script type="text/javascript">
		$(function() {
			set_focus(document.theForm);
		});
	</script>
	<form action="<?=$link_action_delete?>" method="post" name="form_delete" id="form_delete" target="save_frame">
		<input name="<?=$id_field?>" type="hidden" value="<?=$id?>" />
	</form>
	<form action="<?=$link_action?>" method="post" name="theForm" id="theForm" onsubmit="<?=$js_string?>" encType="multipart/form-data" target="save_frame">
		<table width="100%" border="0" cellspacing="0" cellpadding="0" class="form">

			<tr>
				<td><?if ($form_new):?>
					<div id="form_buttons_holder"><table id="form_buttons">
							<tr>
								<td class="buttons_top"><div class="right"><input type="submit" name="Submit" value="<?=$c_send_new?>" class="btn_form1"></div>
								</td>
							</tr>
						</table></div>
						<?endif //form_new?>
						<?if ($form_edit):?>
						<div id="form_buttons_holder"><table id="form_buttons">
								<tr>
									<td valign="bottom">
										<div class="menu">

											<div><?=$c_edit_data?></div>
											<a href="?menu_id=<?=$menu_id?>&custumer_id=<?=$custumer_id?>&action=list_records_history"><?=$c_edit_history?></a>

										</div>
									</td>
									<td align="right" class="buttons_top">
										<div class="right" style="margin-bottom:3px;">
									<? if($show_previous_link): ?><a class="boto2 btn_show_previous_link"
									                          href="<?= $show_previous_link ?>"></a><? endif //show_previous_link?>
									<? if($show_next_link): ?><a class="boto2 btn_show_next_link"
										                          href="<?= $show_next_link ?>"></a><? endif //show_next_link?>
									<? if ( $back_button ): ?>
										<a class="btn_form_back" href="<?= $back_button ?>"></a><? endif //back_button?>
											<input type="submit" name="Submit" value="<?= $c_send_edit ?>" class="btn_form1">

											<div class="btn_sep"></div>
											<a class="btn_form_delete" href="javascript:submit_form('<?= $c_confirm_form_bin ?>', true);">
												<?= $c_delete ?>
											</a></div></td>
								</tr>
							</table></div>
							<?endif //form_edit?>
						<table width="100%" cellpadding="4" cellspacing="0">
							<tr>
								<td class="formCaptionTitle"><strong> </strong>
									<table border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td><strong>
													<?=$c_name?>:</strong></td>
											<td>&nbsp;
												<?=$name?>
												&nbsp;</td>
											<td><strong>&nbsp;
													<?=$c_surnames?>:&nbsp;</strong></td>
											<td>&nbsp;
												<?=$surname1?>
												&nbsp;</td>
											<td>&nbsp;
												<?=$surname2?></td>
											<td valign="middle" style="padding-left:10px;padding-right:10px;"><?=$treatment?></td>
											<td><?=$c_is_invoice?>&nbsp;</td>
											<td>&nbsp;&nbsp;&nbsp;<?=$is_invoice?></td>
												<?if ($enteredc):?>
												<td><strong>&nbsp;&nbsp;&nbsp;&nbsp;
														<?=$c_enteredc?>:&nbsp;</strong></td>
												<td>&nbsp;
													<?=$enteredc?>
													&nbsp;</td>
													<?endif //enteredc?>
											</tr>
										</table></td><? if ($is_tool_custumer):?>
									<td class="formCaptionTitle" align="right"><a href="javascript: toggle('custumer_information');" class="links_generals"><?=$c_show_not_show;?></a></td><? endif //is_tool_custumer?>
								</tr>
							</table><div id='custumer_information'>
								<table width="100%" cellpadding="4" cellspacing="0" class="formstbl">
									<?if(!$view_only_itself):?>
									<tr>
										<td valign="top" class="formsCaption"><strong>
												<?=$c_user_id?>: </strong></td>
										<td class="forms"><?=$user_id?></td>
									</tr>
									<?endif //view_only_itself?>
									<tr>
										<td valign="top" class="formsCaption"><strong>
												<?=$c_company?>: </strong></td>
										<td class="forms"><?=$company?>&nbsp;&nbsp;&nbsp;<?=$c_comercial_name;?>&nbsp;&nbsp;<?=$comercial_name;?>&nbsp;&nbsp;&nbsp;<?=$c_cif;?>&nbsp;&nbsp;<?=$cif;?>&nbsp;&nbsp;&nbsp;<?=$c_job;?>&nbsp;&nbsp;<?=$job;?>

									</tr>
									<tr>
										<td width="150" valign="top" class="formsCaption"><strong>
												<?=$c_phone?>:</strong><?if($view_only_itself):?><?=$user_id?><?endif //view_only_itself?></td>
										<td class="forms"><table border="0" cellspacing="0" cellpadding="0">
												<tr>
													<td><?=$c_phone1b?></td>
													<td><?=$c_phone2;?></td>
													<td><?=$c_phone4;?></td>
													<td><?=$c_phone3;?></td>
												</tr>
												<tr>
													<td><input name="custumer_id[<?=$custumer_id?>]" type="hidden" value="<?=$custumer_id?>" />
														<input name="custumer_id_javascript" type="hidden" value="<?=$custumer_id?>" />
															<?=$hidden?>
														<?=$phone1?>
														&nbsp;&nbsp;</td>
													<td><?=$phone2;?>
														&nbsp;&nbsp;</td>
													<td><?=$phone4;?>
														&nbsp;&nbsp;</td>
													<td><?=$phone3;?>
														&nbsp;&nbsp;</td>
												</tr>
											</table></td>
									</tr>
									<tr>
										<td valign="top" class="formsCaption"><strong>
												<?=$c_address?>: </strong></td>
										<td class="forms"><table border="0" cellspacing="0" cellpadding="0">
												<tr>
													<td><?=$c_adress?></td>
													<td><?=$c_numstreet;?></td>
													<td><?=$c_block;?></td>
													<td><?=$c_flat;?></td>
													<td><?=$c_door;?></td>
												</tr>
												<tr>
													<td><?=$adress?>&nbsp;&nbsp;</td>
													<td><?=$numstreet;?>&nbsp;&nbsp;</td>
													<td><?=$block;?>&nbsp;&nbsp;</td>
													<td><?=$flat;?>&nbsp;&nbsp;</td>
													<td><?=$door;?></td>
												</tr>
											</table></td>
									</tr>
									<tr>
										<td valign="top" class="formsCaption"><table width="100%" border="0" cellspacing="0" cellpadding="0">
												<tr>
													<td nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;
														<?=$c_town;?>:</td>
												</tr>
											</table></td>
										<td class="forms"><table border="0" cellspacing="0" cellpadding="0">
												<tr>
													<td><?=$town;?></td>
												</tr>
											</table></td>
									</tr>
									<tr>
										<td valign="top" class="formsCaption"><table width="100%" border="0" cellspacing="0" cellpadding="0">
												<tr>
													<td nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;
														<?=$c_province;?>:</td>
												</tr>
											</table></td>
										<td class="forms"><table border="0" cellspacing="0" cellpadding="0">
												<tr>
													<td><?=$province;?></td>
												</tr>
											</table></td>
									</tr>
									<tr>
										<td valign="top" class="formsCaption"><table width="100%" border="0" cellspacing="0" cellpadding="0">
												<tr>
													<td nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;
														<?=$c_zip;?>:</td>
												</tr>
											</table></td>
										<td class="forms"><table border="0" cellspacing="0" cellpadding="0">
												<tr>
													<td><?=$zip;?></td>
												</tr>
											</table></td>
									</tr>
									<tr>
										<td valign="top" class="formsCaption"><table width="100%" border="0" cellspacing="0" cellpadding="0">
												<tr>
													<td nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;
														<?=$c_country;?>:</td>
												</tr>
											</table></td>
										<td class="forms"><table border="0" cellspacing="0" cellpadding="0">
												<tr>
													<td><?=$country;?>&nbsp;&nbsp;&nbsp;<?=$c_nationality?>&nbsp;&nbsp;<?=$nationality?></td>
												</tr>
											</table></td>
									</tr>
									<tr>
										<td valign="top" class="formsCaption"><strong>
												<?=$c_vat;?>:</strong></td>
										<td class="forms"><table border="0" cellspacing="0" cellpadding="0">
												<tr>
													<td><?=$c_vat_num?></td>
													<td><?=$c_vat_type?></td>
													<td><?=$c_vat_date?></td>
												</tr>
												<tr>
													<td><?=$vat?>&nbsp;&nbsp;</td>
													<td><?=$vat_type?>&nbsp;&nbsp;</td>
													<td><?=$vat_date?>&nbsp;&nbsp;</td>
												</tr>
											</table></td>
									</tr>

									<tr>
										<td valign="top" class="formsCaption"><strong>
												<?=$c_birthdate;?>:</strong></td>
										<td class="forms"><?=$birthdate;?></td>
									</tr>
									<tr>
										<td valign="top" class="formsCaption"><strong>
												<?=$c_marital_status?>:</strong></td>
										<td class="forms"><?=$marital_status?></td>
									</tr>
									<tr>
										<td valign="top" class="formsCaption"><strong>
												<?=$c_mail;?>:</strong></td>
										<td class="forms"><?=$mail;?></td>
									</tr>
									<tr>
										<td valign="top" class="formsCaption"><strong>
												<?=$c_password?>:</strong></td>
										<td class="forms"><?=$password?>&nbsp;&nbsp;&nbsp;<?=$c_password_repeat?> <?=$password_repeat?></td>
									</tr>
									<tr>
										<td valign="top" class="formsCaption"><strong>
												<?=$c_website;?>:</strong></td>
										<td class="forms"><?=$website;?></td>
									</tr>
									<tr>
										<td valign="top" class="formsCaption"><strong>
												<?=$c_prefered_language?>:</strong></td>
										<td class="forms"><?=$prefered_language?></td>
									</tr>

									<tr>
										<td valign="top" class="formsCaption"><strong>
												<?=$c_observations1?>: </strong></td>
										<td class="forms"><?=$observations1?></td>
									</tr>
									<tr>
										<td valign="top" class="formsCaption"><strong>
												<?=$c_file?>: </strong></td>
										<td class="forms">										
											<?=$file?>											
										</td>
									</tr>
									<tr>
										<td valign="top" class="formsCaption"><strong>
												<?=$c_activated?>: </strong></td>
										<td class="forms"><?=$activated?></td>
									</tr>
								</table>
								<div class="sep_dark"></div><table width="100%" border="0" cellpadding="4" cellspacing="0">
							<tr>
								<td class="formCaptionTitle" align="left"><strong>
										<?=$c_invoice_information;?>:</strong></td>
							</tr></table>
								<table width="100%" cellpadding="4" cellspacing="0" class="formstbl">
									<tr>
										<td valign="top" class="formsCaption"><table width="100%" border="0" cellspacing="0" cellpadding="0">
												<tr>
													<td nowrap="nowrap"><strong><?=$c_billing?>:</strong></td>
												</tr>
											</table></td>
										<td class="forms"><table border="0" cellspacing="0" cellpadding="0">
												<tr>
													<td><?=$billing?></td>
												</tr>
											</table></td>
									</tr>
									<tr>
										<td valign="top" class="formsCaption"><strong> <strong>
													<?=$c_bankdata;?>
												</strong>:</strong></td>
										<td class="forms"><table border="0" cellspacing="0" cellpadding="0">
												<tr>
													<td><?=$c_bank?>
													</td>
													<td><?=$c_bic?></td>
													<td><?=$c_comta?></td>
													<td><?=$c_account_holder?></td>

												</tr>
												<tr>
													<td><?=$bank?>
														&nbsp;&nbsp;</td>
													<td><?=$bic?>
														&nbsp;&nbsp;</td>
													<td><?=$comta?>
														&nbsp;&nbsp;</td>
													<td><?=$account_holder?>
														&nbsp;&nbsp;</td>

												</tr>
											</table></td>
									</tr>
									<tr>
										<td valign="top" class="formsCaption"><strong>
												<?=$c_billing_mail;?>:</strong></td>
										<td class="forms"><?=$billing_mail;?></td>
										</tr>
										<tr>
										<td valign="top" class="formsCaption"><strong>
												<?=$c_billing_type;?>:</strong></td>
										<td class="forms"><?=$billing_type;?></td>
										</tr>
										<tr>
										<td valign="top" class="formsCaption"><strong>
												<?=$c_invoice_informations;?>:</strong></td>
										<td class="forms"><?=$invoice_informations;?></td>
										</tr>


									</table>
								<table width="100%" cellpadding="4" cellspacing="0" class="formstbl">	
									<tr>
										<td valign="top" class="formsCaptionHor"><?=$c_group_id?></td>
									</tr>
									  <tr>
										<td height="30" valign="top" class="forms"><?=$group_id;?></td>
									  </tr>
								</table>
							</div>

							<?if ($form_new):?>
								<table width="100%" cellpadding="4" cellspacing="2" class="formsButtonstbl">
									<tr>
										<td align="center" class="formsButtons"><div class="centerFloat1">
												<div class="centerFloat2"><input type="submit" name="Submit" value="<?=$c_send_new?>" class="btn_form1"></div></div>
										</td>
									</tr>
								</table>
								<?endif //form_new?>
								<?if ($form_edit):?>
									<table width="100%" cellpadding="4" cellspacing="2" class="formsButtonstbl">
										<tr>
											<td align="center" class="formsButtons"><div class="centerFloat1">
													<div class="centerFloat2"><input type="submit" name="Submit" value="<?=$c_send_edit?>" class="btn_form1"><div class="btn_sep"></div><a class="btn_form_delete" href="javascript:submit_form('<?=$c_confirm_form_bin?>', true);">
															<?=$c_delete?>
														</a></div></div></td>
										</tr>
									</table>
									<?endif //form_edit?></td>
							</tr>
						</table>
					</form>
					<?if ($is_tool_custumer):?>
						<div class="sep_dark"></div><table width="100%" border="0" cellpadding="4" cellspacing="0">
							<tr>
								<td class="formCaptionTitle"><strong>
										<?=$c_propertyscustumer;?>:</strong> <a href="javascript:select_addresses('<?=$custumer_id?>')">
										<?=$c_select_property_link?>
									</a> - <a href="javascript:show_form_new_property('<?=$custumer_id?>')">
										<?=$c_add_property_link?>
									</a></td>
								<td class="formCaptionTitle" align="right"><a href="javascript: toggle('propertys');" class="links_generals"><?=$c_show_not_show;?></a></td>	
							</tr>
						</table>
						<table width="100%" border="0" cellpadding="0" cellspacing="0">
							<tr>
								<td height="10">&nbsp;</td>
							</tr>
						</table>
						<div id="propertys" style="padding:0px 16px 60px 16px;"><?=$properties;?></div>
						<div class="sep_dark"></div><table width="100%" cellpadding="4" cellspacing="0">
							<tr>
								<td class="formCaptionTitle"><strong>
										<?=$c_demandscustumer;?>: </strong><a href="javascript:show_form_new_demand('<?=$custumer_id?>')">
										<?=$c_add_demand_link?>
									</a> - <a href="/admin/?action=list_records_exact&menu_id=5010&selected_menu_id=5003&custumer_id=<?=$custumer_id?>">
										<?=$c_searchexact_button?>
									</a> - <a href="/admin/?action=list_records_prox&menu_id=5011&selected_menu_id=5003&custumer_id=<?=$custumer_id?>">
										<?=$c_searchprox_button?>
									</a></td>
								<td class="formCaptionTitle" align="right"><a href="javascript: toggle('demands');" class="links_generals"><?=$c_show_not_show;?></a></td>
							</tr>
						</table><div id='demands' style="padding:8px;">
							<?=$demands;?></div>



						<div class="sep_dark"></div><table width="100%" cellpadding="4" cellspacing="0">
							<tr>
								<td class="formCaptionTitle"><strong>
										<?=$c_bookingcustumer?>: </strong>
									<a href="/admin/?action=list_records&tool=booking&tool_section=book&menu_id=5028&custumer_id=<?=$custumer_id?>">
										<?=$c_bookingcustumer_list?>
									</a>
								</td>
								<td class="formCaptionTitle" align="right"><a href="javascript: toggle('bookings');" class="links_generals"><?=$c_show_not_show;?></a></td>
							</tr>
						</table><div id='bookings' style="padding:8px;">
							<?=$bookings;?></div>


							<?endif //$is_tool_custumer?>