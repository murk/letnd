

var booking = {
	start: true,
	date_in: false,
	date_out: false,
	painted: {},
	b_original_class: false,
	inicialized: false,
	month_width:0,
	month_loaded:{},
	month_loaded_count:0,
	first_month_viewing:'',
	paint_class:'selected',
	unpaint_class:'booked',
	caller: '',
	attr:'',	
	is_season_calendar:false,
	property_id: 0,
	is_set_booking: false,
	init: function (){
	
		if (this.inicialized) return;
		
		// fixo la mida per quan es carreguin mes mesos
		this.month_width = $('.mes-holder').width();
		$('#book-calendar').width($('#book-calendar').width());
		$('.mes-holder').width(this.month_width);
		
		$('.mes-holder').each(function( index ) {
			if (!booking.first_month_viewing) booking.first_month_viewing = this.id;
			booking.month_loaded[this.id] = true;
			booking.month_loaded_count++;	
		});
		
		this.inicialized = true;
	},
	set_booking: function(book_id,date_in,date_out,season_id,old_season_id){
		
		this.is_set_booking = true;
		this.caller = 'free';
		
		if (typeof(season_id)=='undefined') season_id = false;
		if (typeof(old_season_id)=='undefined') old_season_id = false;
		
		// hem editat un registre, hem de despintar tots els que tenen data-book-id
		if (!this.date_in && !this.date_out){
			$('[data-book-id='+book_id+']').each(
				function(){
					// no te en compte first-free i last-free, ara no cal
					var new_class = $(this).attr('class');
					new_class = new_class.replace('middle-booked','free');
					new_class = new_class.replace('first-booked','free');
					new_class = new_class.replace('last-booked','free');
					
					if (season_id)
						new_class = new_class.replace('season'+old_season_id,'');
					
					$(this).attr('class',new_class);
				}
			);
		}		
		else{
			this.paint_days(false);
			
			this.date_in = false;
			this.date_out = false;
		}
		this.attr = book_id;
		
		if(season_id) {
			this.paint_class = 'booked season' + season_id;
		}
		else {
			this.paint_class = 'booked';
		}
		this.unpaint_class = 'selected';
		
		
		var date = date_in.split('/');
		var dia = date[2];
		var mes = date[1];
		var any = date[0];
				
		var id = '#'+dia+'-'+mes+'-'+any;
		this.set_day(dia,mes,any,'b');
				
		var date = date_out.split('/');
		var dia = date[2];
		var mes = date[1];
		var any = date[0];
				
		var id = '#'+dia+'-'+mes+'-'+any;
		this.set_day(dia,mes,any,'a');
		
		// resetejo per poder tornar a reservar de nou
		this.paint_class = 'selected';
		this.unpaint_class = 'booked';
		
		this.date_in = false;
		this.date_out = false;		
		
		this.is_set_booking = false;
		
	},
	cancel_date_out: function (){
		
		// nomes ho fa al calendari
		if (typeof(this.date_in)!='undefined' && this.date_in != ''){
			var dia = this.date_in.getDate();
			var mes = this.date_in.getMonth()+1;
			var any = this.date_in.getFullYear();
			
			this.do_action(dia,mes,any,'b');
		}
		
		
	},
	show_form_edit: function(book_id, id){
		
		if (this.is_set_booking) return;
		
		// quan he creat book desde js
		if (!book_id) book_id = $(id).attr('data-book-id');
		
		
		if (this.is_season_calendar){
		
			var link = '/admin/?template=window&tool=booking&tool_section=propertyrange&action=show_form_edit&propertyrange_id=' + book_id;
			showPopWin(link,700,480,false,true);
			
		}
		else{
		
			this.show_form_edit_book(book_id);
			
			/*
			$.ajax({
				url: link
			})
				.done(function(data) {			
					$('#show-form').html(data);				
				});
			*/
		
		}
		
		
	},	
	show_form_edit_book: function(book_id){
		var link = '/admin/?template=window&tool=booking&tool_section=book&action=show_form_edit&book_id='+book_id;
		showPopWin(link,990,80,false,true,false,'',false,false,'px','%');
	},
	show_form_new: function(formated_date_in,formated_date_out){
		
		if (this.is_set_booking) return;
		
		if (this.is_season_calendar){
			var link = '/admin/?template=window&tool=booking&tool_section=propertyrange&action=show_form_new&property_id='+this.property_id + '&date_start='+formated_date_in + '&date_end='+formated_date_out;
			showPopWin(link,700,400,false,true);
		}
		else{	
			bookingform.get_price('show_form_new_done',this.property_id, formated_date_in, formated_date_out);
			
			//$('#dates').html(formated_date_in+' - '+formated_date_out);
			//$('#date_in_0').val(formated_date_in);
			//$('#date_out_0').val(formated_date_out);		
		}
	},
	show_form_new_done: function(formated_date_in,formated_date_out){
		var link = '/admin/?template=window&tool=booking&tool_section=book&action=show_form_new&property_id='+this.property_id + '&date_in='+formated_date_in + '&date_out='+formated_date_out;		
		showPopWin(link,950,80,false,true,false,'',false,false,'px','%');
	},
	do_action: function (dia, mes, any, half, book_id){
		
		var id = '#'+dia+'-'+mes+'-'+any;
		if (typeof(book_id)=='undefined') book_id = false;
		
		if (
			$(id+'-'+half).hasClass('disabled')
		){
			return false;
		}
		else if  (
			$(id+'-a,' +id+'-b').hasClass('middle-booked') ||
			$(id+'-'+half).hasClass('first-booked') ||
			$(id+'-'+half).hasClass('last-booked')
		){	
			this.caller = 'booked';
			if (!this.date_in) this.show_form_edit(book_id, id+'-'+half);
		}
		else if (
			$(id+'-a,' +id+'-b').hasClass('middle-selected') ||
			$(id+'-'+half).hasClass('first-selected') ||
			$(id+'-'+half).hasClass('last-selected')		
		){
			this.caller = 'selected';	
			this.set_day (dia, mes, any, half, id)	
		}
		else{
			this.caller = 'free';
			this.set_day (dia, mes, any, half, id)
		}
		dp('Caller', this.caller);
		
	},
	set_day: function (dia, mes, any, half, id){
		
		
		var other_half = half=='a'?'b':'a';
		
		if (this.date_in && this.date_out) this.paint_days(false);
		
		//book_id = document.theForm.book_id_javascript.value;
		
		var date_entered = new Date(any,mes-1,dia);	
		
		
		if (!this.date_in){
		
			
			// marco el primer click
			//if (!this.date_out){		
			
			if (this.caller != 'booked'){	
				
				this.date_in = date_entered;
				
				this.painted['a'] = {'id':id+'-a','old_class':$(id+'-a').attr('class')};
				this.painted['b'] = {'id':id+'-b','old_class':$(id+'-b').attr('class')};
											
				// el mig que he clicat el marco perque ja se que no es booked				
				$(id+'-'+half).addClass('middle-' + this.paint_class);
				$(id+'-'+half).removeClass('middle-' + this.unpaint_class);
				$(id+'-'+half).removeClass((half=='a'?'first':'last')+'-free');	
				$(id+'-'+half).removeClass('free');
						
				// l'altre mig el marco si no es booked	
				if (
					!$(id+'-'+other_half).hasClass('first-booked') &&
					!$(id+'-'+other_half).hasClass('last-booked')
					)
				{
					
					$(id+'-'+other_half).addClass('middle-' + this.paint_class);
					$(id+'-'+other_half).removeClass('middle-' + this.unpaint_class);
					$(id+'-'+other_half).removeClass((other_half=='a'?'first':'last')+'-free');	
					$(id+'-'+other_half).removeClass('free');
				}
			}
		}
		else {
			// quan clico sobre un dia marcat el desmarco i resetejo tot
			if  (date_entered.getTime() === this.date_in.getTime()){	
				$(this.painted.a.id).attr('class',this.painted.a.old_class);
				$(this.painted.b.id).attr('class',this.painted.b.old_class); 
				this.date_in = false;		
				this.cancel_paint();
				
			}
			else if  (date_entered < this.date_in){	
				this.date_out = this.date_in;		
				this.date_in = date_entered;		
			}
			else {	
				this.date_out = date_entered;			
			}
			
		}
		
		if (this.date_in && this.date_out) {
			
			
			if (this.paint_days('test')) {
				
				// restauro classes del primer dia clicat
				$(this.painted.a.id).attr('class',this.painted.a.old_class);
				$(this.painted.b.id).attr('class',this.painted.b.old_class);
				
				this.paint_days(this.paint_class);				
				
			}
			else {	
			
				if (this.date_in == date_entered) this.date_in = this.date_out;
				this.cancel_paint();
			}
			
		}
	},
	paint_days: function (paint){
	
		var conta = 1;
		var formated_date_in = '';
		for (var d = new Date(this.date_in.valueOf()); d <= this.date_out; d.setDate(d.getDate() + 1)) {
		
			var dia = d.getDate();
			var mes = d.getMonth()+1;
			var any = d.getFullYear();
			
			var id = '#'+dia+'-'+mes+'-'+any+'-a';
			var id2 = '#'+dia+'-'+mes+'-'+any+'-b';
			
			// defineixo classes per afegir i treure
			
			var middle_class = 'middle-' + this.paint_class;
			var first_class = 'first-' + this.paint_class;
			var last_class = 'last-' + this.paint_class;
			
			var unmiddle_class = 'middle-' + this.unpaint_class;
			var unfirst_class = 'first-' + this.unpaint_class;
			var unlast_class = 'last-' + this.unpaint_class;
			
			
			// canvio la classes del primer quadre del primer dia
			if (conta==1){
				$(id).removeClass('free');
				if (this.can_be_free(id))
					$(id).addClass('first-free');	
				id=false;
				formated_date_in = dia+'/'+mes+'/'+any;
			}			
			
			// comprobo si es pot reservar
			if (paint=='test'){				
				return this.can_be_free(id);
			}
			// pinto els dies
			else if (paint){			
				
				
				if (id){
					this.painted[id] = {'id':id,'old_class':$(id).attr('class')};
					$(id).addClass( middle_class );
					$(id).removeClass(unmiddle_class);
					$(id).removeClass('free');
					
					if (this.attr) $(id).attr('data-book-id',this.attr);
				}
				if (id2){
					this.painted[id2] = {'id':id2,'old_class':$(id2).attr('class')};
					
					$(id2).removeClass('free');
					if (this.attr) $(id2).attr('data-book-id',this.attr);
					
					if (!id){
						$(id2).addClass( first_class );
						$(id2).removeClass( unfirst_class );
					}
					else{
						$(id2).addClass( middle_class );
						$(id2).removeClass( unmiddle_class );
					}
				}
			}
			// desselecciono els dies
			else {
				if (id){
					$(id).attr('class',this.painted[id].old_class);
				}
				if (id2){
					$(id2).attr('class',this.painted[id2].old_class);
				}
			}				
			conta++;
		}
		
				
		if (paint==='test'){
		
		}
		//
		// He seleccionat una franja
		//
		else if (paint){	
			// canvio la classes de l'ultim dia	
			$(id).removeClass( middle_class );			
			$(id).addClass( last_class );
			$(id).removeClass( unlast_class );
			
			$(id2).removeClass( middle_class );	
			if (this.can_be_free(id2))
				$(id2).addClass('last-free');
			
			if (this.attr) $(id2).attr('data-book-id','');
			
			formated_date_out = dia+'/'+mes+'/'+any;
			
			this.show_form_new(formated_date_in,formated_date_out);
		}
		else{
			this.date_in = this.date_out = false;
		}
		
		return true;
		
	},
	cancel_paint: function (){
		this.date_out = false;
		//this.date_in = this.date_out = this.a_original_class = this.b_original_class = false;		
	},
	can_be_free: function(id){
		return !( $(id).hasClass('first-booked') || $(id).hasClass('last-booked') || $(id).hasClass('middle-booked')) ;
	},
	show_month: function(direction,property_id){
	
		var viewing = this.first_month_viewing.split('-');
		
		var viewing_month = viewing[0];
		var viewing_year = viewing[1];
		var month=0;
		var year=0
		
		if (direction == 'next'){
			month = Number(viewing_month)+1;
			year = viewing_year;
			if (month>12) {
				month = 1;
				year++;
			}			
			this.first_month_viewing = month + '-' + year;
			
			month = Number(viewing_month)+3;
			year = viewing_year;
			if (month>12) {
				month = month-12;
				year++;
			}
		}
		if (direction == 'prev'){
			month = viewing_month-1;
			year = viewing_year;
			if (month<1) {				
				month = 12;
				year--;
			}
			this.first_month_viewing = month + '-' + year;
		}
		//dp(month + '-' + year);
		if (typeof(this.month_loaded[month + '-' + year])=='undefined'){
		
			var link = this.is_season_calendar?'season':'book';
			link = '/admin/?tool=booking&tool_section=book&action=get_month&process='+link+'&month='+month+'&year='+year+'&property_id='+property_id+'&language='+gl_language;
			
			$.ajax({
				url: link
			})
				.done(function(data) {
				
					if (direction=='next'){
						$('#book-calendar-tr').append(data);
					}
					else{
						$('#book-calendar-tr').prepend(data);
						$('#book-calendar-table').css('margin-left',-booking.month_width);
					}
					$('.mes-holder').width(booking.month_width);
					
					booking.month_loaded[month + '-' + year] = true;
					booking.month_loaded_count++;
					
					$('#book-calendar-table').width(booking.month_width*booking.month_loaded_count);
					
					booking.animate_month(direction);	
					
				});
			
		}
		else{
			booking.animate_month(direction);
		}
		
	},
	animate_month: function(direction){
	
		var dir = direction=='next'?-1:1
		var obj = {};
		obj['marginLeft'] =  dir*this.month_width + parseInt($('#book-calendar-table').css('margin-left'));
				
		$('#book-calendar-table').stop().animate(obj, 500);
	}
};

var propertyrange = {

	id: 0,
	initial_season_id: 0,
	is_form_new: false,
	season_prices:{},
	old_date_start: false,
	old_date_end: false,
	init: function(){
		$('#season_id_' + this.id).focus();
		
		this.initial_season_id = $('#season_id_' + this.id).val();
		
		$('#season_id_' + this.id).change(
			function(){
				propertyrange.change_prices($(this).val());				
			}
		);
		$('#whole_week_' + this.id).prop('checked')?$('#start_day').show():$('#start_day').hide();
		$('#whole_week_' + this.id).change(
			function(){
				propertyrange.set_minimum_nights(this);				
			}
		);
		
		propertyrange.old_date_start = $('#date_start_' + this.id).val();
		propertyrange.old_date_end = $('#date_end_' + this.id).val();
		
		$('#date_start_' + this.id).change(
			function(){				
				propertyrange.check_dates();
			}
		);
		$('#date_end_' + this.id).change(
			function(){
				propertyrange.check_dates();
			}
		);
		
		if (this.is_form_new) this.change_prices($('#season_id_' + this.id).val());
	},
	
	check_dates: function(){
		var date_start = $('#date_start_' + this.id).datepicker('getDate');
		date_start.setHours(0,0,0,0);
		var date_end = $('#date_end_' + this.id).datepicker('getDate');
		date_end.setHours(0,0,0,0);
		
		var start_val = $('#date_start_' + this.id).val();
		var end_val = $('#date_end_' + this.id).val();
		
		if (date_start.toString() == date_end.toString()){
		
			$('#date_start_' + propertyrange.id).val(propertyrange.old_date_start);
			$('#date_end_' + propertyrange.id).val(propertyrange.old_date_end);
			alert(equal_dates_error);
			return;
			
		}
		else if (date_start > date_end){
		
			$('#date_start_' + propertyrange.id).val(end_val);
			$('#date_end_' + propertyrange.id).val(start_val);
		}
		propertyrange.old_date_start = start_val;
		propertyrange.old_date_end = end_val;
	},
	
	// canvio preus al canviar de temporada, si la temporada ja s'havia entrat algun cop
	change_prices: function(val){
	
		var prices = propertyrange.season_prices[val];
		if (prices){
			$('#price_night_' + propertyrange.id).val(prices['price_night']);
			$('#price_weekend_' + propertyrange.id).val(prices['price_weekend']);
			$('#price_week_' + propertyrange.id).val(prices['price_week']);
			$('#price_week_type_' + propertyrange.id + '_' + prices['price_week_type']).attr('checked', true);
			$('#minimum_nights_' + propertyrange.id).val(prices['minimum_nights']);
			//dp(prices['price_week_type'],'price_week_type'); 
		}
	},
	
	// canvio a 1 setmana si es clica a setmanes senceres i mostro opcio de primer dia
	set_minimum_nights: function(obj){
		
		id = propertyrange.id;
		
		if(obj.checked){
			minimun_nights = $('#minimum_nights_' + id).val();
			//dp(minimun_nights);
			if (minimun_nights%7 != 0){
				$('#minimum_nights_' + id).val('7');
			}
			$('#start_day').show();
		}
		else{
			$('#start_day').hide();			
		}
	}
}


var bookingform = {

	id: 0,
	payment_1: 0,
	payment_2: 0,
	payment_3: 0,
	payment_4: 0,
	price_book: 0,
	price_extras: 0,
	price_total: 0,
	old_date_in: false,
	old_date_out: false,
	season_prices: {},
	date_message_error: '',
	equal_dates_error: '',
	too_many_person_error: '',
	promcode_err_not_main_custumer: '',
	old_adult: 0,
	old_child: 0,
	old_baby: 0,
	person: 0,
	ordre: 0,
	total_payments: 4,
	custumers: {},
	recalculate_rate: '',
	init: function () {

		bookingform.old_date_in = $('#date_in_' + this.id).val();
		bookingform.old_date_out = $('#date_out_' + this.id).val();


		this.old_adult = $('#adult_' + this.id).val();
		this.old_child = $('#child_' + this.id).val();
		this.old_baby = $('#baby_' + this.id).val();

		$('#adult_' + this.id + ',#child_' + this.id + ',#baby_' + this.id).change(
			function () {
				bookingform.custumer_on_select(false);
			}
		);
		$('#date_in_' + this.id).change(
			function () {
				bookingform.get_price();
			}
		);
		$('#date_out_' + this.id).change(
			function () {
				bookingform.get_price();
			}
		);
		$('input[name="recalculate_rate"]').change(
			function () {
				bookingform.recalculate_rate = $('input[name="recalculate_rate"]').is(":checked")?1:'';
				bookingform.get_price();
			}
		);
		$('[name^="extra_book_selected"]').click(
			function () {
				bookingform.get_price_extras();
			}
		);
		$('[name^="quantity"]').change(
			function () {
				bookingform.get_price_extras();
			}
		);
		$('#promcode-button').click(
			function () {
				var main_custumer = $('#selected_custumer_id_1').val();
				if (main_custumer == '0') {
					alert(bookingform.promcode_err_not_main_custumer);
					return;
				}
				var promcode = $('#promcode').length ? $('#promcode').val() : '';

				if (promcode)
					bookingform.get_price();
			}
		);

		this.init_payment();
		this.init_manual_price();

		form_set_serialized();

	},
	init_payment: function (obj) {

		var payment_inputs = '#order_status_1_' + this.id + ',#order_status_2_' + this.id + ',#order_status_3_' + this.id + ',#order_status_4_' + this.id;

		// $(payment_inputs).prop('disabled', false);

		for (var i=1; i<=this.total_payments; i++) {
			if ($('#payment_status_' + i + '_' + this.id).val() == 'completed'){
				$('#payment_mail_' + i).show();
			}
		}


		$(payment_inputs).change(
			function () {
				bookingform.on_payment_done(this);
			}
		);

		var payment_inputs2 = '#payment_1_' + this.id + ',#payment_2_' + this.id + ',#payment_3_' + this.id + ',#payment_4_' + this.id;
		$(payment_inputs2).change(
			function () {
				bookingform.on_payment(this);
			}
		);

		bookingform.set_payment_rest();
	},
	on_payment: function (obj) {

		var payment_name = obj.id.substr(8,1);
		var id = this.id;
		var first_zero = 0;
		var first_zero_val = 0;
		var total = 0;
		var val = 0;

		// actualitzo variable de pago i resta
		dp ('id',  obj.id);
		dp ('payment_name', payment_name);
		dp ('checkbox', 'order_status_' + payment_name + '_' + id);
		dp ('Valor pagat: ', $('#order_status_' + payment_name + '_' + id).val() );
		if ($('#order_status_' + payment_name + '_' + id).val() == 'completed') {
			this['payment_' + payment_name] = unformat_currency($(obj).val());

			dp ('payment_' + payment_name,unformat_currency($(obj).val()));
			bookingform.set_payment_rest();
		}

/*
	Donava problemes quan hi havia algún negatiu, serà millor posar un botó per calcualr quan volguem fer-ho

		for (var i=1; i<=this.total_payments; i++) {
			// dp('#payment_' + i + '_' + id)
			val = unformat_currency($('#payment_' + i + '_' + id).val());

			if (val<=0 && !first_zero) {
				first_zero = i;
				first_zero_val = val;
			}
			total +=val;
		}

		if (this.price_total - total != 0) {
			// agafo valors de l'ultim pagament
			if (!first_zero) {
				first_zero = this.total_payments;
				first_zero_val = val;
			}
			// sumo el valor actual de first_zero per si te un valor negatiu
			var rest = (this.price_total*100 - total*100 + first_zero_val*100);
			rest = format_currency(rest);
	      $('#payment_' + first_zero + '_' + id).val(rest);
		}
*/

	},
	set_payment_rest: function () {
		var rest = Math.round(this.price_total*100) - Math.round(this.payment_1*100) - Math.round(this.payment_2*100) - Math.round(this.payment_3*100) - Math.round(this.payment_4*100);
		// dp('rest',rest);
		// dp('rest currency',format_currency(rest));
		$('#payment_rest').html( format_currency(rest) );
	},
	on_payment_done: function (obj) {

		var sufix = obj.id.substr(12);
		var payment_number = sufix.substr(1,1);
		var $old_input = $('input[name="old_order_status_'+ payment_number +'[' + this.id + ']"]');
		var old_val = $old_input.val();
		var val = $(obj).val();
		dp('val', val);
		dp('old_val', 'input[name="old_order_status_'+ payment_number +'[' + this.id + ']"]');
		dp('old_val', old_val);

		if (val == 'completed') {
			this['payment_' + payment_number] = unformat_currency($('#payment' + sufix).val());
			$('#payment_mail_' + payment_number).show();
		}
		else if (old_val == 'completed') {
			if (confirm(bookingform.payment_uncheck_message)) {
				this['payment_' + payment_number] = 0;
				$('#payment_mail_' + payment_number).hide();
			}
			else{
				$(obj).val(old_val);
				return;
			}
		}
	   $old_input.val(val);

		this.set_payment_rest();

		if (!$('#payment_entered' + sufix).val()) {
			var date_entered = new Date();

			var dia = date_entered.getDate();
			var mes = date_entered.getMonth() + 1;
			var any = date_entered.getFullYear();

			if (dia < 9) dia = '0' + dia;
			if (mes < 9) mes = '0' + mes;

			var date = dia + '/' + mes + '/' + any;
			$('#payment_entered' + sufix).val(date);
		}


	},
	send_payed_mail: function (payment_number) {

		check_form_change ();

		// envio mail
		//showPopWin('', 300, 100, null, false, false, MODALBOX_SAVE);
		var payment = $('#payment_' + payment_number + '_' + this.id).val();
		var payment_rest = $('#payment_rest').html();
		var payment_date =$('#payment_entered_' + payment_number + '_' + this.id).val();

		$.ajax({
			url: '/admin/?action=send_mail_ajax&tool=booking&tool_section=book',
			data: 'book_id=' + this.id +
			'&payment_number=' + payment_number +
			'&payment=' + payment +
			'&payment_rest=' + payment_rest +
			'&payment_date=' + payment_date,
			dataType: 'json',
			type: 'POST'
		})
			.done(function(data) {
				// guardo formulari
				$('#theForm').submit();

				top.window.location.href = '/admin/?menu_id=5&custumer_ids=' + data.custumer_id + '&related_property_ids=' + data.property_id + '&subject=' + data.subject + '&message=' + data.message + '&prefered_language=' + data.prefered_language +  '&message_return_url=' + encodeURIComponent(top.window.location.href);
			});
	},

	get_price: function (return_function, property_id, date_in, date_out) {

		var property_id = (typeof(property_id) == 'undefined') ? $('#property_id_' + this.id).val() : property_id;
		var date_in = (typeof(date_in) == 'undefined') ? $('#date_in_' + this.id).val() : date_in;
		var date_out = (typeof(date_out) == 'undefined') ? $('#date_out_' + this.id).val() : date_out;
		var recalculate_rate = this.recalculate_rate;
		var adult = $('#adult_' + this.id).length ? $('#adult_' + this.id).val() : '1';
		var child = $('#child' + this.id).length ? $('#child_' + this.id).val() : '';
		var baby = $('#baby' + this.id).length ? $('#baby_' + this.id).val() : '';
		var promcode = $('#promcode').length ? $('#promcode').val() : '';

		if (typeof(return_function) == 'undefined') return_function = false;

		// dates no editables
		if (typeof(date_in) === 'undefined'){
			date_in = '';
			date_out = '';
		}
		else if ((date_in == date_out)) {
			$('#date_in_' + this.id).val(bookingform.old_date_in);
			$('#date_out_' + this.id).val(bookingform.old_date_out);
			alert(this.equal_dates_error);
			return false;
		}

		var selected_custumer_ids = '';
		$('input[name^="selected_custumer_id["],select[name^="selected_custumer_id["]').each(function () {
			selected_custumer_ids += $(this).val() + ',';
			// dp($(this).val());
		});

		if (selected_custumer_ids)
			selected_custumer_ids = selected_custumer_ids.substring(0, selected_custumer_ids.length - 1);

		$.ajax({
			url: '/admin/?tool=booking&tool_section=book&action=get_price_ajax'
			+ '&date_in=' + date_in
			+ '&date_out=' + date_out
			+ '&recalculate_rate=' + recalculate_rate
			+ '&book_id=' + this.id
			+ '&adult=' + adult
			+ '&child=' + child
			+ '&baby=' + baby
			+ '&property_id=' + property_id
			+ '&promcode=' + promcode
			+ '&selected_custumer_ids=' + selected_custumer_ids,
			dataType: 'json'
		})
			.done(function (data) {

				var errors = '';


				if (data.promcode_error) {
					errors += '\n' + data.promcode_error;
					$('#promcode_error').html(data.promcode_error);
					// dp(errors);
				}
				else if (data.promcode_discount) {
					$('#promcode_form').html('');
					$('#promcode_tr').show();
					bookingform.promcode_discount = data.promcode_discount;
					$('#promcode_discount').html('-' + format_currency(data.promcode_discount * 100) + ' €');
					bookingform.set_price_total();
				}

				if (data.error) {
					if (data.new_date_in) {
						$('#date_in_' + bookingform.id).val(data.new_date_in);
						$('#date_out_' + bookingform.id).val(data.new_date_out);
					}
					else {
						$('#date_in_' + bookingform.id).val(bookingform.old_date_in);
						$('#date_out_' + bookingform.id).val(bookingform.old_date_out);
					}
					booking.cancel_date_out();
					errors += '\n' + data.error;
				}
				else {


					if (data.invert_dates) {
						var temp = date_out;
						date_out = date_in;
						date_in = temp;
						$('#date_in_' + bookingform.id).val(date_in);
						$('#date_out_' + bookingform.id).val(date_out);
					}

					bookingform.price_book = data.price_book;
					$('#price_book').html(format_currency(data.price_book * 100));

					bookingform.price_tt = data.price_tt;
					$('#price_tt').html(format_currency(data.price_tt * 100) + ' €');

					// Ho agafa dels extres de la pàgina i no per ajax - excepte a preu manual
					// A preu manual ho recalcula només el primer cop
					if ($('#is_manual_price_holder input[type="hidden"]').val() == '1') {
						$('#is_manual_price_holder input[type="hidden"]').val('0');
						bookingform.price_extras = data.price_extras;
						$('#price_extras').html(format_currency(data.price_extras * 100));
					}

					$('#detail').html(data.detail);

					bookingform.set_price_total();

					bookingform.old_date_in = date_in;
					bookingform.old_date_out = date_out;
					//dp(return_function,'Return func');
					if (return_function) booking[return_function](date_in, date_out);
				}

				if (errors) alert(errors);

			});
	},
	get_price_extras: function () {
		var price = 0;
		$('[name^="extra_book_selected"]').each(
			function () {
				if (this.type == 'hidden' || $(this).is(":checked")) {

					if ($('[name="quantity\[' + this.value + '\]"]').length) {
						quantity = $('[name="quantity\[' + this.value + '\]"]').val();
					}
					else {
						quantity = 1;
					}

					price += Number($(this).attr('data-price')) * 100 * quantity;
				}
			}
		);
		this.price_extras = price / 100;
		$('#price_extras').html(format_currency(price) + ' €');
		bookingform.set_price_total();
	},
	set_price_total: function () {
		var price = this.price_book * 100 + this.price_extras * 100 + this.price_tt * 100;
		price -= this.promcode_discount * 100;
		$('#price_total').html(format_currency(price) + ' €');
	},

	add_new_custumer: function (ordre) {
		this.ordre = ordre;
		showPopWin('/admin/?action=show_form_new_custumer&menu_id=103&book_id=' + this.id + '&process=book_custumer', 850, 80, false, true, false, '', false, false, 'px', '%');
	},

	select_custumer: function (ordre) {
		this.ordre = ordre;
		//gl_submit_addresses = false;
		//gl_default_input_prefix = 'selected_property';

		var exclude = '';
		var main_custumer = $('#selected_custumer_id_1').val();
		if (ordre != 1) {
			exclude = '&exclude_custumer_ids=' + main_custumer;
		}

		showPopWin('/admin/?action=list_records_select_frames_custumer&menu_id=103&book_id=' + this.id + '&main_custumer=' + main_custumer + '&ordre=' + ordre + exclude, 90, 80, null, true, false, '', false, '', '%', '%');
	},
	edit_custumer: function (custumer_id, book_id) {
		showPopWin('/admin/?action=show_form_edit_custumer&menu_id=103&book_id=' + book_id + '&custumer_id=' + custumer_id + '&process=book_custumer', 95, 80, null, true, false, '', false, '', '%', '%');
	},

	custumer_on_select: function (custumer_id) {

		if (custumer_id) this.custumers[this.ordre] = custumer_id;

		var adult = $('#adult_' + this.id).val();
		var child = $('#child_' + this.id).val();
		var baby = $('#baby_' + this.id).val();

		if (!this.check_person(adult, child, baby)) {
			return;
		}


		var custumers_url = '';
		for (ordre in this.custumers) {
			custumers_url += '&custumers[' + ordre + ']=' + this.custumers[ordre];
		}

		$.ajax({
			url: '/admin/?tool=booking&tool_section=book&action=get_rest_of_custumers_ajax'
			+ '&custumer_id=' + custumer_id
			+ '&adult=' + adult
			+ '&child=' + child
			+ '&baby=' + baby
			+ '&book_id=' + this.id
			+ custumers_url,
			dataType: 'json'
		})
			.done(function (data) {
				$('#book-custumer').html(data.html);
				bookingform.get_price();
			});
		hidePopWin();
	},
	check_person: function (adult, child, baby) {
		//dp(adult,child);
		//dp((Number(adult)+Number(child)));
		if ((Number(adult) + Number(child)) > this.person) {

			alert(this.too_many_person_error);

			$('#adult_' + this.id).val(this.old_adult);
			$('#child_' + this.id).val(this.old_child);
			$('#baby_' + this.id).val(this.old_baby);

			return false;
		}
		else {
			this.old_adult = adult;
			this.old_child = child;
			this.old_baby = baby;
			return true;
		}
	},
	init_manual_price: function () {
		$('#is_manual_price_holder').click(function () {
			if ($(this).find('input[type="checkbox"]').is(":checked")) {
				$('.manual_price_input').show();
			}
			else {
				$('.manual_price_input').hide();
				$('.automatic_price_text').show();
			}
		});
		$('#manual_price_book, #manual_price_extras').on('keyup', function () {
			var manual_book = unformat_currency($('#manual_price_book').val());
			var manual_extras = unformat_currency($('#manual_price_extras').val());
			var total = manual_book + manual_extras;
			total = format_currency(total * 100);
			$('#price_total').html(total + ' €');
		});
	}

};


var bookseason = {
	init: function (){
		$('select[id^="color_id_"]').each( function(index){
			$(this).change ( function(){
				var color = $('option',this).filter(":selected").attr('data-color');
				
				color = 'rgba('+color+',0.8)';
				$(this).css('background-color',color);
				$(this).blur();
			});
		});
	}
}

var bookpolice = {
	download_selected: function (){	
		submit_list('download_selected');
	}
}

function unformat_currency(currency){
	currency = currency.replace('.', '');
	currency = currency.replace(',', '.');
	dp ('unformat',currency);
	currency = parseFloat (currency);
	dp ('unformat despres parse',currency);

	if (isNaN(currency)) currency = 0;

	return currency;
}
function format_currency(currency){

	currency = Math.round(currency);

	var is_negative = currency<0;
	currency = Math.abs(currency);

	ret_curr = currency.toString();
	
	if (currency>99999){
		coma_index = ret_curr.indexOf(',');
		if (coma_index==-1) coma_index = ret_curr.length;
		punt_index = coma_index-5;
		ret_curr = ret_curr.substr(0,punt_index) + '.' + ret_curr.substr(punt_index)
	}
	if (currency>99){
		punt_index = ret_curr.length-2;
		ret_curr = ret_curr.substr(0,punt_index) + ',' + ret_curr.substr(punt_index);
	}
	else if (currency>9){
		ret_curr = '0,' + ret_curr;
	}
	else{
		ret_curr = '0,0' + ret_curr;
	}

	if (is_negative) ret_curr = '-' + ret_curr;

	return ret_curr;
}

function xvalidar(obj){
	d_in_arr = obj.date_in.value.split('-');
	d_out_arr = obj.date_out.value.split('-');
	
	d = d_in_arr;
	d_in = new Date(d[0],d[1]-1,d[2]);
	d = d_out_arr;
	d_out = new Date(d[0],d[1]-1,d[2]);
	if (d_out>d_in) return true;	
	alert('La fecha de salida debe ser mayor a la fecha de entrada');
	return false;
}


/*
function get_custumer_jscript(custumer_id, custumer_name, gl_process, action, book_id)
{
	if (document.theForm){
		custumers_ids = document.theForm['selected_custumer_ids'].value;
		custumers_names = document.theForm['selected_custumer_names'].value;
	}
	else{
		custumers_ids = 0;
	}
	
	if (action=='add_record_custumer'){
		custumers_ids = custumers_ids?(custumers_ids + ',' + custumer_id):custumer_id;
	}	
	
	link = '/admin/?tool=booking&tool_section=book&action=get_custumers&process=save_record&selected_addresses_ids=' + custumers_ids + '&book_id=' + book_id;
	$.ajax({
			url: link
		})
			.done(function(data) {			
				$('#custumers').html(data);
				$('#list_row_' + book_id + ' .custumers').parent().html(data);				
			});
	

}
*/