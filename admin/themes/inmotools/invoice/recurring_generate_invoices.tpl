<form name="theForm" method="post" action="/admin/?tool=invoice&tool_section=recurring" enctype="multipart/form-data" target="save_frame">
	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="form">
		<tr>
			<td>
				<div id="form_buttons_holder">
					<table id="form_buttons">
						<tr>
							<td valign="bottom">
								<div class="menu"></div>
							</td>
							<td class="buttons_top">
								<div class="right"></div>
							</td>
						</tr>
					</table>
				</div>
				<? ////////////   TITOL  ////////////////?>


				<table width="100%" cellpadding="0" cellspacing="0">
					<tr>
						<td class="formCaptionTitle"><strong>
								Generar factures
								&nbsp;&nbsp;</strong></td>
					</tr>
				</table>


				<? ////////////   CAMPS  ////////////////?>

				<table width="100%" cellpadding="0" cellspacing="0" class="formstbl">

					<tr>
						<td valign="top" class="formsCaption">
							<?= $c_generate_top_date ?>:
						</td>
						<td valign="top" class="forms">
							<input type="text" id="generate_top_date" name="generate_top_date" value="<?= $generate_top_date ?>">
							<input name="action" type="hidden" value="generate_invoices_save"/>
						</td>
					</tr>

				</table>

				<div id="report">
					<p><strong><?= $invoices_before_count ?> - </strong> <?= $c_generate_until ?>
						<strong><?= $generate_top_date ?></strong></p>

					<? /* <div class="sublist"><?= $invoices_before_listing ?></div> */ ?>


					<table>
						<tr>
							<th><?= $c_next_bill_date ?></th>
							<th><?= $c_bill_due ?></th>
						</tr>
						<? foreach ( $invoices_before as $l ): extract( $l ) ?>
							<tr>
								<td><?= format_date_list( $next_date ) ?></td>
								<td><?= format_date_list( $next_due ) ?></td>
							</tr>
						<? endforeach //$invoices_before?>
					</table>

					<p><strong><?= $invoices_after_count ?> - </strong> <?= $c_generate_after ?>
						<strong><?= $generate_top_date ?></strong></p>

					<? /* <div class="sublist"><?= $invoices_after_listing ?></div> */ ?>


					<table>
						<tr>
							<th><?= $c_next_bill_date ?></th>
							<th><?= $c_bill_due ?></th>
						</tr>
						<? foreach ( $invoices_after as $l ): extract( $l ) ?>
							<tr>
								<td><?= format_date_list( $next_date ) ?></td>
								<td><?= format_date_list( $next_due ) ?></td>
							</tr>
						<? endforeach //$invoices_after?>
					</table>

				</div>

				<table width="100%" cellpadding="4" cellspacing="2" class="formsButtonstbl">
					<tr>
						<td align="center" class="formsButtons">
							<div class="centerFloat1">
								<div class="centerFloat2">
									<input type="submit" name="Submit"
									       value="<?= $c_generate ?>"
									       class="btn_form1">
								</div>
							</div>
						</td>
					</tr>
				</table>


			</td>
		</tr>
	</table>
</form>