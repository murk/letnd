<?

/* 1-12-15

	Actualització inmo TV opcions de limitar imatges

 */
if ( is_table( 'inmo__configadmin' ) && ! Db::get_first( "SELECT * FROM inmo__configadmin WHERE name='tv_num_images'" ) ) {
	$update_sql[] = "
		INSERT INTO inmo__configadmin (`name`, `value`) VALUES ('tv_num_images', '');";

} else {
	if ( is_table( 'inmo__configadmin' ) ) {
		trigger_error( 'Ja existeixen el camp tv_num_images' );
	}
}

?>