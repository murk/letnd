<?php
include_once( DOCUMENT_ROOT . 'clients/investin/functions.php' );

$vars['var_name'] = 'municipi_id';

$comarca_id  = R::number( 'comarca_id', 20 ); // per defecte Girona

$municipi_id = R::number( 'municipi_id' );
$espai_id    = R::number( 'espai_id' );

$query = "SELECT Distinct municipi, inmo__municipi.municipi_id
				FROM inmo__municipi
				WHERE municipi_id IN (
					SELECT Distinct municipi_id
					FROM investin__espai
					WHERE status='public'
					AND investin__espai.bin = 0
				)
				AND inmo__municipi.comarca_id = '" . $comarca_id . "'
				ORDER BY municipi";

$results = Db::get_rows($query);

if (!$municipi_id)
	$municipi_id = $results[0]['municipi_id'];


$vars['block_selected'] = $municipi_id?1:0;


if ($results) {
	foreach($results as $rs) {
		$l = array();
		$municipi_id == $rs["municipi_id"]?$selected = 1:$selected = 0;


		$l['link'] = get_link(array(
			'tool' => 'investin',
			'tool_section' => 'espai',
			'friendly_params' => array(
				'comarca_id'=> $comarca_id,
				'municipi_id'=> $rs['municipi_id']),
			'detect_page' => false));

		$l['selected'] = $selected;
		$l['value'] = $rs['municipi_id'];
		$l['item'] = $rs['municipi'];

		$loop2 = array();

		$query = "
		SELECT espai_id,latitude, longitude, zone, zone_id, municipi_id, superficies, tipus
			FROM investin__espai
			LEFT OUTER JOIN investin__espai_language using (espai_id)
			WHERE investin__espai_language.language = '" . LANGUAGE . "'
			AND investin__espai.bin = 0
			AND status = 'public'
			AND municipi_id = " . $rs["municipi_id"] . "
			ORDER BY zone ASC";

		$results2 = Db::get_rows($query);

		foreach ($results2 as $rs2) {
			$l2 = array();

			if ( $espai_id == $rs2["espai_id"] ) {
				$selected = 1;
				$l['selected'] = 1;
			} else {
				$selected = 0;
			}

			$l2['selected'] = $selected;
			$l2['value'] = $rs2['espai_id'];
			$l2['item'] = $rs2['zone'];
			$l2['link'] = '/' . LANGUAGE . '/investin/espai/'. $rs2['espai_id'] .'/?comarca_id='. $comarca_id .'&municipi_id='. $rs2['municipi_id'] .'&pagina_id=4';

			$loop2[] = $l2;
		}

		if ($loop2)
		{
			$loop2[0]['class']                     = 'first';
			$loop2[ count( $loop2 ) - 1 ]['class'] = 'last';
		}
		$l['loop'] = $loop2;

		$loop[] = $l;
	}

	$loop[0]['class'] = 'first';
	$loop[count($loop) - 1]['class'] = 'last';
}

?>