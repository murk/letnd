<form action="<?= $form_link ?>" method="post" name="list" id="list" onsubmit="return check_selected(this)" target="save_frame">
	<input name="action" type="hidden" value="save_rows"/>
	<?= $out_loop_hidden ?>
	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="listRecord" style="margin-top:0px">
		<? foreach ( $loop as $l ): extract( $l ) ?>
			<tr class="listItem<?= $odd_even ?>" id="list_row_<?= $id ?>">
				<? if ( $has_images ): ?>
					<td valign="top" class="listImage" width="<?= $im_admin_thumb_w ?>">
						<? if ( $image_name ): ?>
							<? if ( $is_mls ): ?>
								<div class="no-dropzone">
									<img width="<?= $im_admin_thumb_w ?>" src="http://<?= $domain ?>/<?= $client_dir ?>/inmo/property/images/<?= $image_name_admin_thumb ?>">
								</div>
							<? else: // ?>
								<div><img width="<?= $im_admin_thumb_w ?>" src="<?= $image_src_admin_thumb ?>">
								</div>
							<? endif //$is_mls ?>
						<? else: // image_name?>
							<div class="noimage <?= $is_mls ? 'no-dropzone' : '' ?>" style="min-height:<?= $im_admin_thumb_h ?>px;"></div>
						<? endif // image_name?>
					</td>
				<? endif // $has_images ?>
				<td valign="top">
					<table width="100%" border="0" cellspacing="0" cellpadding="0" class="noPadding">
						<tr>
							<td>
								<table width="100%" border="0" cellpadding="0" cellspacing="0">
									<tr>
										<td class="listTitleProperty">
											<input name="property_id[<?= $property_id ?>]" type="hidden" value="<?= $property_id ?>"/><? if ( $ref ): ?>
												&nbsp;<?= $ref ?>
											<? endif // $ref ?>
											<span class="private">
												<? if ( $property_private ): ?><? if ( $ref ): ?>
													-
												<? endif // $ref ?><?= $property_private ?><? endif //property_private?>
												<? if ( $address ): ?>
													<? if ( $adress || $numstreet || $block || $flat || $door ): ?>
														<span class="smaller <? if ( $property_private )
															echo "padding-left-2" ?>">
													<? if ( $adress ): ?><?= $adress ?><? endif ?><? if ( $numstreet ): ?>,&nbsp;<?= $numstreet ?><? endif //numstreet?><? if ( $block ): ?>,&nbsp;<?= $c_block ?><?= $block ?><? endif //block?><? if ( $flat ): ?>,&nbsp;<?= $flat ?><? endif //flat?><? if ( $door ): ?>-<?= $door ?><? endif //door?>
												</span>
													<? endif //tot?>
													<? endif // $address ?></span></td>
										<td align="right"><?= $entered ?>
											&nbsp;</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td>
								<table width="99%" border="0" cellpadding="0" cellspacing="0">
									<tr>
										<td valign="top">
											<table width="100%" border="0" cellpadding="0" cellspacing="0">
												<tr>
													<td height="30" valign="top" class="listTitleProperty2"><? if ( $category_id ): ?>
																<?= $category_id ?>
															<? endif // $category_id ?><? if ( $tipus ): ?>
																<? if ( $category_id ): ?> | <? endif //$ ?>
																<?= $tipus ?>
															<? endif // $tipus ?><? if ( $floor_space ): ?>
																<? if ( $tipus ): ?> | <? endif //$ ?>
																<?= $floor_space ?>
																m2
															<? endif // $floor_space ?>
															<? if ( $tipus_value == 'temp' && $person ): ?>
																<? if ( $floor_space ): ?> | <? endif //$ ?>
																<?= $person ?> <?= $person == 1 ? $c_person_s : $c_person_p ?>
															<? endif ?></td>
													<td height="30" align="right" valign="top" class="listResaltat"><? if ( $price ): ?>
															<?= $price ?>
															&nbsp;<?= CURRENCY_NAME ?>
														<? endif // $price ?></td>
												</tr>
											</table>
											<table width="100%" border="0" cellpadding="0" cellspacing="0" class="listDetails2">
												<tr>
													<td valign="top"><strong class="listItem">
															<?= $c_tipus2 ?>:</strong>&nbsp;</td>
													<td valign="top"><?= $tipus2 ?></td>
													<td valign="top"><strong class="listItem">
															<?= $c_municipi_id ?>:</strong>&nbsp;</td>
													<td valign="top"><?= $municipi_id ?></td>
												</tr>
												<tr>
													<td valign="top"><strong class="listItem">
															<?= $c_room ?>:</strong>&nbsp;</td>
													<td valign="top"><? if ( $room ): ?>
															<?= $room ?>
														<? else: //?>
															-
														<? endif //room?></td>
													<td valign="top"><strong class="listItem">
															<?= $c_land ?>:</strong></td>
													<td valign="top"><? if ( $land ): ?>
															<?= $land ?>
															&nbsp;
															<?= $land_units ?>
															&nbsp;
														<? else: //?>
															-
														<? endif //land?></td>
												</tr>
												<tr>
													<td valign="top"><strong class="listItem">
															<?= $c_bathroom ?>:</strong>&nbsp;</td>
													<td valign="top"><?= $bathroom ?></td>
													<td valign="top"><strong class="listItem">
															<?= $c_courtyard ?>&nbsp;/&nbsp;<?= $c_garden ?>:</strong>
													</td>
													<td valign="top"><?= $courtyard ?>&nbsp;/&nbsp;<?= $garden ?></td>
												</tr>
												<tr>
													<td valign="top"><strong class="listItem">
															<?= $c_wc ?>:</strong>&nbsp;</td>
													<td valign="top"><?= $wc ?></td>
													<td valign="top"><strong class="listItem">
															<?= $c_status ?>:</strong>&nbsp;</td>
													<td valign="top"><?= $status ?></td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td align="left"><? if ( $user_id ): ?>
												<strong><?= $c_user_id ?></strong>: <?= $user_id ?><? endif //user_id?>
										</td>
									</tr>
									<tr>
										<td><img src="/admin/themes/inmotools/images/spacer.gif" width="1" height="10">
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>

				</td>
			</tr><? if ( $description || $observations ): ?>

				<tr>
					<td colspan=2 class="observations">
						<? if ( $description ): ?>
							<?= $description ?>
						<? endif // $description ?>
						<? if ( $observations ): ?>
							<p style="margin-top:20px"><strong><?= $c_observations ?>:</strong></p>
							<p><?= $observations ?></p>
						<? endif //observacions?>
					</td>
				</tr>
			<? endif // $description || $observations ?>

		<? endforeach //$loop?>
	</table>
</form>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td height="10"></td>
	</tr>
</table> 
  