<div class="search">
	<form id="search-advanced" name="search_advanced" method="get" action="/admin/" class="forms-sep">
		<table border="0" cellspacing="0" cellpadding="0" class="search-top">
			<tr valign="middle">
				<td id="view-format-holder" title="<?= $c_view_format_title ?>">
					<a data-value="all" onclick="ProductSearch.set_view_format('all')"><?= $c_view_format_all ?></a>
					<a data-value="order" onclick="ProductSearch.set_view_format('order')"><?= $c_view_format_order ?></a>
					<a data-value="orderitems" onclick="ProductSearch.set_view_format('orderitems')"><?= $c_view_format_orderitems ?></a>
					<input name="view_format" id="view-format-hidden" type="hidden" value="<?= $view_format ?>" checked="checked">
				</td>
				<td><?= $c_by_words ?></td>
				<td>
					<input name="action" type="hidden" value="<?= $action ?>">
					<input name="menu_id" type="hidden" value="<?= $menu_id ?>">
					<input class="alone" name="q" type="text" size="30" value="<?= $q ?>"></td>
				<td class="padding-left-2">
					<?= $c_by_ref ?>
				</td>
			</tr>
		</table>
		<table class="search-top vertical-caption" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td>
					<div><?= $c_product_id ?></div>
					<input class="alone" id="product_id_add" name="product_id_add" type="text" size="30" value="<?= $product_id_add ?>">
					<input id="product_id" name="product_id" type="hidden" value="<?= $product_id ?>">
				</td>
				<td>
					<div><?= $c_order_status ?></div>
					<?= $order_status_select ?>
				</td>
				<td>
					<div><?= $c_send_date ?></div>
					<?= $send_date_select ?>
				</td>
				<td>
					<div><?= $c_payment_method ?></div>
					<?= $payment_method_select ?>
				</td>
			</tr>
		</table>
		<table class="vertical-caption" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td>
					<div><?= $c_dates_from ?></div>
					<input autocomplete="off" class="alone" id="date_from" name="date_from" type="text" size="8" value="<?= $date_from ?>">
				</td>
				<td>
					<div><?= $c_dates_to ?></div>
					<input autocomplete="off" class="alone" id="date_to" name="date_to" type="text" size="8" value="<?= $date_to ?>">
				</td>
				<td class="buttons-td bottom"><input type="submit" name="advanced" value="ok" class="boto-search"></td>
				<td class="buttons-td bottom"><input type="button" id="reset-search" name="reset" value="Reset" class="boto-search reset"></td>
				<? /* TODO-i Posar l'excel <td class="buttons-td bottom"><input type="submit" name="excel" value="Excel" class="boto-search excel"></td> */ ?>

			</tr>
		</table>
	</form>
</div>

<script language="JavaScript">

	ProductSearch.view_format = '<?= $view_format ?>';
	ProductSearch.init();


</script>