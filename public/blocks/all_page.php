<?php
global $gl_is_home;
$menu = isset($secondary)?'2':'1';
$menu = isset($vars['menu'])?$vars['menu']:$menu; // $vars['menu'] l'agafa de la taula all__block -> vars 

$query = "SELECT all__page.page_id as page_id, link, page, page_file_name, has_content, page_text_id
				FROM all__page_language, all__page
				WHERE all__page_language.page_id = all__page.page_id
				AND status = 'public'
				AND level = 1
				AND bin <> 1
				AND menu = '" . $menu . "'
				AND language = '" . LANGUAGE . "'
				ORDER BY ordre";

$results = Db::get_rows($query);
Debug::add('Results all_page',$results);
if ($results)
{
	$conta = 0;
	$loop_count = count($results);
    foreach($results as $rs)
    {
        (
			(isset($_GET["page_id"]) && ($_GET["page_id"] == $rs["page_id"]))
			||
			(isset($_GET["page_file_name"]) && ($GLOBALS["gl_page"]->page_id == $rs["page_id"])) //($_GET["page_file_name"] == $rs["page_file_name"])) // agafo l'id que ja he calculat a page_public.php
			||
			($rs['link']=='/' && $gl_is_home)
		)?$selected = 1:$selected = 0;

        /*if ($rs['link'])
        {
            if (USE_FRIENDLY_URL){
				$l['link'] = '/' . LANGUAGE;
				if ($rs['link'] != '/') // la home page no porta el page_id
				{
					$l['link'] .= $rs['link'] . '/' . $rs['page_file_name'] . '.html';
				}
			}
			else{
				$sep = strstr($rs['link'], '?')?'&':'?';
				$l['link'] = $rs['link'] . $sep . 'language=' . LANGUAGE;
				if ($rs['link'] != '/') // la home page no porta el page_id
				{
					$l['link'] .= '&page_id=' . $rs['page_id'];
				}
			}
        }
        else
        {
            if (USE_FRIENDLY_URL){
				$l['link'] = '/' . LANGUAGE . '/' . $rs['page_file_name'] . '.html';
			}
			else{
				$l['link'] = '/?page_id=' . $rs['page_id'] . '&language=' . LANGUAGE;
			}
        }*/
		$l['link'] = Page::get_page_link($rs['page_id'],$rs['link'],$rs['page_file_name'],$rs['has_content']);
        $l['selected'] = $selected;
        $l['item'] = $rs['page'];
		$l['id'] = $rs['page_id'];
		$l['conta'] = $conta++;
		$l['class'] = '';
		$l['loop_count'] = $loop_count;
		$l['page_file_name'] = $rs['page_file_name'];
		$l['page_text_id'] = $rs['page_text_id'];
        $loop2[] = $l;
    }
	$loop2[0]['class'] = 'first';	
	$loop2[count($loop2)-1]['class'] = 'last';

    $loop[] = array ('loop' => $loop2,
        'item' => '');
}

?>