<?php
/* 5-10-18
Canvi page
*/

if ( is_table( 'all__page_language' ) ) {

	$update_sql[] = "ALTER TABLE `all__page_language`
	CHANGE COLUMN `title` `title` VARCHAR(255) NOT NULL DEFAULT '' COLLATE 'utf8_spanish2_ci' AFTER `page`;";

}
else {
	if ( is_table( 'all__page_language' ) ) {
		trigger_error( 'No hi ha la taula all__page_language' );
	}
}