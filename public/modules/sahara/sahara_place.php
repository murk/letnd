<?
/**
 * SaharaPlace
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c)  30/11/2013
 * @version $Id$
 * @access public
 */
class SaharaPlace extends Module{
	var $page_file_name;
	function __construct(){
		parent::__construct();
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function get_records()
	{		
		
		$query = "SELECT  page_file_name FROM all__page INNER JOIN all__page_language USING(page_id) WHERE language='".LANGUAGE."' AND page_id=11";

		$this->page_file_name = Db::get_first($query);
		
	    $listing = new ListRecords($this);
		$listing->has_bin = false;
		$listing->paginate = false;
		
		
		$condition = "SELECT count(*) 
						FROM sahara__circuit
						WHERE sahara__place.place_id = sahara__circuit.place_id
						AND (status = 'public') 						
						AND bin = 0 ";
		
		$listing->condition = "(" . $condition . ")>0";
		
		if ($GLOBALS['gl_is_home']){
			$listing->condition .= " AND (in_home = '1')";
			$listing->order_by = ' ordre asc, place asc';
			$listing->limit = 4;
		}
		else{
			$listing->order_by = 'ordre asc, place asc';
		}
		
		$listing->call('records_walk','place_id,place_description,place_description_list');
		
	    return $listing->list_records();
	}
	function records_walk($place_id,$place_description,$place_description_list) {			
		
		$friendly_params['place_id'] = $place_id;
		$ret['circuits_link'] = Page::get_link('sahara','circuit',false,false,$friendly_params,false,'',false,$this->page_file_name);
		
		$ret['place_description']=$place_description_list?$place_description_list:add_dots('place_description', $place_description);
		
		return $ret;
	}
}
?>