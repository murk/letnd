<?
$gl_db_classes_table = "newsletter__templatetext";
$gl_db_classes_id_field = "templatetext_id";

function save_rows()
{
  $save_rows = new SaveRows;
  $save_rows->fields = array ("name");
  $save_rows->message_deleted = NEWSLETTER_TEMPLATES_MESSAGE_LIST_DELETED;
  $save_rows->message_saved = NEWSLETTER_TEMPLATES_MESSAGE_LIST_SAVED;
  $save_rows->message_not_saved = NEWSLETTER_TEMPLATES_MESSAGE_LIST_NOT_SAVED;
  $save_rows->save();
}

function list_records()
{
  global $newsletter_templates_list, $gl_action;
  $listing = new ListRecords;
  
  $listing->condition;
  $listing->template = "templates_list.htm";
  $listing->no_records_message = NEWSLETTER_TEMPLATES_MESSAGE_NO_RECORDS;
  $listing->fields = array(
                            "name" => "text",
                            "templatetext_id" => ""
                          );
  $listing->order_by = 'name';
  $listing->caption = $newsletter_templates_list;
  $listing->list_records();
}


function show_form($action)
{
  global $newsletter_templates_form;
  $show = new ShowForm;
  $show->action = $action;
  $show->caption = $newsletter_templates_form;
  $show->title_modify = NEWSLETTER_TEMPLATES_TITLE_EDIT;
  $show->template = 'templates_form.htm';
  $show->fields =array (
                    "templatetext_id" => "",
                    "name" => "",
                    "header" => "",
                    "footer" => ""
                  );
  $show-> show_form();
}


function write_record($action)
{
  global $gl_action;
  $writerec = new WriteRecords;
  $writerec->fields = array (
                    "templatetext_id" => "",
                    "name" => "",
                    "footer" => "",
                    "header" => ""
                  );
  $writerec->field_unique = "name";
  $writerec->message_add = NEWSLETTER_TEMPLATES_MESSAGE_ADDED;
  $writerec->message_save = NEWSLETTER_TEMPLATES_MESSAGE_SAVED;
  $writerec->message_add_exists = NEWSLETTER_TEMPLATES_MESSAGE_ADD_EXISTS;
  
  
  if ($action == "add_record")
  {
    $writerec->add_record();
  }
  elseif ($action == "save_record")
  {
    $writerec->save_record();
  }
  elseif ($action == "delete_record")
  {
    $writerec->delete_record();
  }
  elseif ($action == "bin_record")
  {
    $writerec->bin_record();
  }
  
  // redirect
  if ($gl_action=="add_record"){
  show_form("show_form_new");
  }
}

?>