<? if ( ! $results ): ?>
	<? if ( $options_bar ): ?>
		<div id='opcions_holder'>
			<table width="100%" border="0" cellspacing="0" cellpadding="0" id="opcions">
				<tr>
					<td class="opcions">
						<div id="opcions_fixed_bar">
							<div class="more"></div>
						</div>
						<table class="filters">
							<tr valign="middle">
								<? foreach ( $options_filters as $options_filter ): ?>
									<form>
									<td><?= $options_filter ?></td>
									</form>
								<? endforeach //$options_filters?>
							</tr>
						</table><? if ( $options_filters ): ?>
							<div class="sep"></div><? endif //options_filters?>
						<table border="0" cellpadding="2" cellspacing="0">
							<tr valign="middle">
								<? if ( $options_buttons ): ?>
									<td class="buttons">
									<? foreach ( $options_buttons as $options_button ): ?><?= $options_button ?><? endforeach //$options_button?>
									</td><? endif //options_buttons?>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>
	<? endif //$options_bar?>
<? endif //!$results?>
<? if ( $results ): ?><? if ( $split_results_count ): ?>
	<div id='split_holder'>
		<table width="100%" border="0" cellpadding="0" cellspacing="0" id="split">
			<tr valign="middle">
				<td width="20%" align="left" nowrap class="pageResults"><?= $split_results_count ?></td>
				<td align="right" nowrap class="pageResults"><?= $split_results ?></td>
			</tr>
		</table>
	</div>
<? endif //$split_count?>
	<? if ( $options_bar ): ?>
		<div id='opcions_holder'>
			<table width="100%" border="0" cellspacing="0" cellpadding="0" id="opcions">
				<tr>
					<td class="opcionsTitol"><strong><?= $c_options_bar ?></strong></td>
				</tr>
				<tr>
					<td class="opcions">
						<div id="opcions_fixed_bar">
							<div class="more"></div>
						</div>
						<table class="filters">
							<tr valign="middle">
								<? foreach ( $options_move_tos as $options_move_to ): ?>
									<td><?= $options_move_to ?></td>
								<? endforeach //$options_move_tos?>
								<? foreach ( $options_filters as $options_filter ): ?>
									<form>
									<td><?= $options_filter ?></td>
									</form>
								<? endforeach //$options_filters?>
							</tr>
						</table><? if ( $options_filters ): ?>
							<div class="sep"></div><? endif //options_filters?>
						<table border="0" cellpadding="2" cellspacing="0">
							<tr valign="middle">
								<? if ( $options_buttons ): ?>
									<td class="buttons">
									<? foreach ( $options_buttons as $options_button ): ?><?= $options_button ?><? endforeach //$options_button?>
									</td><? endif //options_buttons?>
								<? if ( $save_button ): ?>
									<td>
										<strong><a class="botoOpcionsGuardar" href="javascript:submit_form_save('save_rows_save_records<?= $action_add ?>')">
												<?= $c_send_edit ?>
											</a></strong></td>
								<? endif //save_button?><? if ( $select_button ): ?>
									<td><?= $c_select ?>
										&nbsp;<strong><a class="opcions" href="javascript:select_all(true, document.list)">
												<?= $c_all ?>
											</a>&nbsp;-&nbsp;<a class="opcions" href="javascript:select_all(false, document.list)">
												<?= $c_nothing ?>
											</a></strong></td>
								<? endif //select_button?><? if ( $delete_button ): ?>
									<? if ( $has_bin ): ?>
										<? if ( $is_bin ): ?>
											<td>
												<strong><a class="botoOpcionsBorrar" href="javascript:submit_list('save_rows_delete_selected<?= $action_add ?>', '<?= $c_confirm_deleted ?>', true)">
														<?= $c_delete_selected ?>
													</a></strong></td>
											<td>
												<strong><a class="botoOpcionsGuardar" href="javascript:submit_list('save_rows_restore_selected<?= $action_add ?>')">
														<?= $c_restore_selected ?>
													</a></strong></td>
										<? else: ?>
											<td>
												<strong><a class="botoOpcionsBorrar" href="javascript:submit_list('save_rows_bin_selected<?= $action_add ?>')">
														<?= $c_delete_selected ?>
													</a></strong></td>
										<? endif // is_bin?>
									<? else: ?>
										<td>
											<strong><a class="botoOpcionsBorrar" href="javascript:submit_list('save_rows_delete_selected<?= $action_add ?>', '<?= $c_confirm_deleted ?>', true)">
													<?= $c_delete_selected ?>
												</a></strong></td>
										<!--<td>
              <td>&nbsp;</td><strong><a class="botoOpcions" href="/admin?menu_id=<?= $menu_id ?>&sort_by=0&sort_order=<?= $sort_order0 ?>">
                <?= $c_move_to ?>
               
            <input type="submit" name="bin_selected" value="<?= $c_delete_selected ?>" class="boto1">
              </a></strong></td>-->
									<? endif // has_bin?><? endif //delete_button?>
								<? if ( $print_button ): ?>
									<td><strong><a class="botoOpcions" href="<?= $print_link ?>">
												<?= $c_print_all ?>
											</a></strong></td>
								<? endif //delete_button?>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>
		<div class="sep"></div>
	<? endif //$options_bar?>
	<form method="post" action="<?= $form_link ?>" name="list" onsubmit="return check_selected(this)" target="save_frame">
		<input name="action" type="hidden" value="save_rows">

		<table width="100%" border="0" cellpadding="0" cellspacing="0" class="listRecord">
			<? foreach ( $loop as $l ): extract( $l ) ?>
				<tr class="listItem<?= $odd_even ?>" onmousedown="change_bg_color(this, 'Down', '<?= $id ?>')" id="list_row_<?= $id ?>" <?= $tr_jscript ?>>
					<? if ( $options_checkboxes ): ?>
						<td width="30" valign="top">
						<input name="selected[<?= $conta ?>]" type="checkbox" value="<?= $id ?>" onclick="return false;"<?= $selected ?> />
						</td><? endif // options_checkboxes?>
					<td valign="top">
						<img style="margin:10px 0 30px;max-width:300px" src="<?= $image_src_admin_thumb ?>" id="im_<?= $conta ?>">
						<input name="<?= $id_field ?>[<?= $id ?>]" type="hidden" value="<?= $id ?>">
						<input name="image_id_<?= $conta ?>" type="hidden" value="<?= $id ?>">
						<table border="0" cellpadding="2" cellspacing="0" class="listRecordHor">
							<tr>
								<th><?= $c_ordre ?></th>
								<td><?= $ordre ?></td>
							</tr>
							<tr>
								<th><?= $c_status ?></th>
								<td><?= $status ?></td>
							</tr>
							<tr>
								<th><?= $c_place_id ?></th>
								<td><?= $place_id ?></td>
							</tr>
							<tr>
								<th><?= $c_url1 ?></th>
								<td>
									<? if ( $url1 ): ?>
										<table class="langList" width="100%" border="0" cellpadding="0" cellspacing="0">
											<? foreach ( $url1 as $l ): extract( $l ) ?>
												<tr>
													<th><?= $lang ?></th>
												</tr>
												<tr>
													<td width="100%"><?= $url1 ?></td>
												</tr>
											<? endforeach //$loop?>
										</table>
									<? endif //$url1 ?>
								</td>
							</tr>
							<tr>
								<th><?= $c_url1_name ?></th>
								<td>
									<? if ( $url1_name ): ?>
										<table class="langList" width="100%" border="0" cellpadding="0" cellspacing="0">
											<? foreach ( $url1_name as $l ): extract( $l ) ?>
												<tr>
													<th><?= $lang ?></th>
												</tr>
												<tr>
													<td width="100%"><?= $url1_name ?></td>
												</tr>
											<? endforeach //$loop?>
										</table>
									<? endif //$url1_name ?>
								</td>
							</tr>
							<tr>
								<th><?= $c_url1_target ?></th>
								<td><?= $url1_target ?></td>
							</tr>
							<tr>
								<th></th>
								<td><? foreach ( $buttons as $button ): ?><?= $button ?><? endforeach //$buttons?></td>
							</tr>
						</table>
					</td>
				</tr>
			<? endforeach //$loop?>
		</table>
	</form>
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td height="10"></td>
		</tr>
	</table> <? if ( $split_results_count ): ?>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr valign="middle">
			<td width="20%" align="left" nowrap class="pageResults"><?= $split_results_count ?></td>
			<td align="right" nowrap class="pageResults"><?= $split_results ?></td>
		</tr>
		</table><? endif //$split_count?>
<? endif //results?>