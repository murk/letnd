<?
/**
 * ClubmotoPagina
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2012
 * @version $Id$
 * @access public 
 
 */
class ClubmotoPaginaHome extends Module{
	
	function __construct(){
		parent::__construct();
	}
	// això és la home
	function list_records()
	{
		$this->action = 'show_record';
		// busco la revista actual
		$query = "SELECT clubmoto__pagina.pagina_id as pagina_id, pagina
				FROM clubmoto__pagina_language, clubmoto__pagina
				WHERE clubmoto__pagina_language.pagina_id = clubmoto__pagina.pagina_id
				".$GLOBALS['gl_clubmoto_status']."
				AND level = 1
				AND bin <> 1
				AND language = '" . LANGUAGE . "'
				ORDER BY ordre ASC, pagina desc
				LIMIT 12";

		$results = Db::get_rows($query);
		$id = $results[0]['pagina_id'];
		
		$show = new ShowForm($this);
		$show->id = $id;
		$first_apartat = clubmoto_get_first_apartat($id);
		$show->set_var ('link',clubmoto_get_pagina_link($first_apartat));
		$show->get_values();		
		
		
		$show->template = 'clubmoto/pagina_home';
		$show->show_form(false);
		
		$this->set_images($show);
		$this->set_revistes($show, $results);
		$this->set_banners($show);
		
		$GLOBALS['gl_content'] = $show->parse_template();
		
	}
	function set_images(&$show){
	
		$image_portada_src = false;
		$image_fons_portada_src = false;
		
		for($i=0; $i<count($show->rs['images']); $i++){
			$imagecat = $show->rs['images'][$i]['image_imagecat_id'];
			$image = $show->rs['images'][$i]['image_src'];
			if ($imagecat==1 && LANGUAGE=='cat') $image_portada_src = $image;
			if ($imagecat==2 && LANGUAGE=='spa') $image_portada_src = $image;
			if ($imagecat==3) $image_fons_portada_src = $image;
		}
		
		if($image_portada_src){
			$size = getimagesize (DOCUMENT_ROOT . $image_portada_src );
			$show->set_var('image_portada_src_height',$size[1]);
		}
		
		$show->set_var('image_portada_src',$image_portada_src);
		$show->set_var('image_fons_portada_src',$image_fons_portada_src);
	}
	function get_image_revista($images){
		$images = $images['images'];
		for($i=0; $i<count($images); $i++){
			$imagecat = $images[$i]['image_imagecat_id'];
			$image = $images[$i]['image_src'];
			if ($imagecat==4) return $image;
		}
		return false;
	}
	function set_revistes(&$show, $results){
		$loop = array();
		$conta = 0;
		foreach($results as $rs){		
			$first_apartat = clubmoto_get_first_apartat($rs['pagina_id']);
			
			$l['link'] = clubmoto_get_pagina_link($first_apartat);
			$l['pagina'] = $rs['pagina'];
			$l['id'] = $rs['pagina_id'];
			$l['conta'] = $conta;
			$l['image_thumb_revista_src']= $this->get_image_revista(UploadFiles::get_record_images($l['id'], true, 'clubmoto', 'pagina'));
			$loop[] = $l;
			$conta++;
		}
		$show->set_var('revistes',$loop);
	}
	function set_banners(&$show){
		// Banners
		$module = Module::load('clubmoto','banner', '', false); // així no necessito el clubmoto_banner.php
		$listing = new ListRecords($module);
		$listing->use_no_template = true;
		$listing->order_by = 'ordre ASC';
		//$listing->condition = $GLOBALS['gl_clubmoto_status'];
		$listing->set_records();

		foreach ($listing->loop as $key=>$val){
			$rs = &$listing->loop[$key];
			if ($rs['files']){
				$rs['banner_src'] = $rs['files'][0]['file_src'];
			}
			else{
				$rs['banner_src'] = '';
			}
			unset ($rs['files']);
		}
		debug::add('caption',$listing->caption);
		$show->set_var('banner1',$listing->loop[0]);
		$show->set_var('banner2',$listing->loop[1]);
		$show->set_var('banner3',$listing->loop[2]);
		
	}

	/**
	 * Es el logged de newsletter
	 * NewsletterCustomer::is_logged()
	 * Saber si està loguejat
	 * @return
	 */
	function is_logged(){
		return (isset($_SESSION['newsletter']['customer_id']) && isset($_SESSION['newsletter']['mail']));
	}
}
?>