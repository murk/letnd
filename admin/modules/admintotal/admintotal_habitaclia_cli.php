<?
/*
CRON - cada dia a les 2 am
0 2 * * * php -f "/var/www/letnd.com/datos/motor/admin/modules/admintotal/admintotal_habitaclia_cli.php" > /var/www/letnd.com/datos/motor/data/logs/habitaclia.htm

SERVIDOR
php -f "/var/www/letnd.com/datos/motor/admin/modules/admintotal/admintotal_habitaclia_cli.php" > /var/www/letnd.com/datos/motor/data/logs/habitaclia.htm
php -d memory_limit=512M "/var/www/letnd.com/datos/motor/admin/modules/admintotal/admintotal_habitaclia_cli.php"

SERVIDOR PER EXCLOURE IMATGES I UNA AGENCIA CONCRETA

php /var/www/letnd.com/datos/motor/admin/modules/admintotal/admintotal_habitaclia_cli.php exclude_images agencia_id=208 > /var/www/letnd.com/datos/motor/data/logs/habitaclia.htm

WEB
/admin/?tool=admintotal&tool_section=habitaclia&action=start

VAGRANT
php "/letnd/admin/modules/admintotal/admintotal_habitaclia_cli.php" agencia_id=10 > /letnd/data/logs/habitaclia_linux.htm
php -d include_path=/php_includes "/letnd/admin/modules/admintotal/admintotal_habitaclia_cli.php"

DOS
C:\server\php5\php.exe -f "D:\Letnd\admin\modules\admintotal\admintotal_habitaclia_cli.php" > d:\letnd\data\logs\habitaclia_windows.htm

DEBUG CLI EX
php -f "/letnd/admin/modules/admintotal/admintotal_habitaclia_cli.php" debug > /letnd/data/logs/habitaclia_linux.htm

*/

$dir = str_replace ( '/admin/modules/admintotal','/common/', __DIR__ );
$dir = str_replace ( '\admin\modules\admintotal','\\common\\', $dir );

include ($dir . 'cli_init.php');

include (DOCUMENT_ROOT . 'admin/modules/admintotal/admintotal_habitaclia.php');

include (DOCUMENT_ROOT . 'common/classes/upload_files.php');

$habitaclia = New AdmintotalHabitaclia;
$habitaclia->is_cli = true;
$habitaclia->_start();

// TODO-i  Activar enviar email send_mail_admintotal('HABITACLIA - Exportació finalitzada', 'Data: ' . now(true));