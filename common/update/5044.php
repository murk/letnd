<?php

/* 29-10-13 
  
 * Product: Afegir nous camps personalitzats, i opcions de text, text amb idiomes, textarea, nombre i casella verificació

 */


if (is_table('product__product')){
	
	$update_sql[] = "
					ALTER TABLE `product__product`
						ADD COLUMN `others8` VARCHAR(255) NOT NULL AFTER `others4`,
						ADD COLUMN `others7` VARCHAR(255) NOT NULL AFTER `others4`,
						ADD COLUMN `others6` VARCHAR(255) NOT NULL AFTER `others4`,
						ADD COLUMN `others5` VARCHAR(255) NOT NULL AFTER `others4`;
					";	

	$update_sql[] = "
					ALTER TABLE `product__product_language`
						ADD COLUMN `others8` VARCHAR(255) NOT NULL DEFAULT '' AFTER `product_old_file_name`,
						ADD COLUMN `others7` VARCHAR(255) NOT NULL DEFAULT '' AFTER `product_old_file_name`,
						ADD COLUMN `others6` VARCHAR(255) NOT NULL DEFAULT '' AFTER `product_old_file_name`,
						ADD COLUMN `others5` VARCHAR(255) NOT NULL DEFAULT '' AFTER `product_old_file_name`,
						ADD COLUMN `others4` VARCHAR(255) NOT NULL DEFAULT '' AFTER `product_old_file_name`,
						ADD COLUMN `others3` VARCHAR(255) NOT NULL DEFAULT '' AFTER `product_old_file_name`,
						ADD COLUMN `others2` VARCHAR(255) NOT NULL DEFAULT '' AFTER `product_old_file_name`,
						ADD COLUMN `others1` VARCHAR(255) NOT NULL DEFAULT '' AFTER `product_old_file_name`;
					";	

	$update_sql[]="
			INSERT INTO `product__configadmin` (`name`,value) 
			VALUES ('others1_type','text_no_lang');";

	$update_sql[]="
			INSERT INTO `product__configadmin` (`name`, value) 
			VALUES ('others2_type','text_no_lang');";

	$update_sql[]="
			INSERT INTO `product__configadmin` (`name`,value) 
			VALUES ('others3_type','text_no_lang');";

	$update_sql[]="
			INSERT INTO `product__configadmin` (`name`,value) 
			VALUES ('others4_type','text_no_lang');";
	
	
	
	

	$update_sql[]="
			INSERT INTO `product__configadmin` (`name`) 
			VALUES ('others5');";
	$update_sql[]="
			INSERT INTO `product__configadmin` (`name`,value) 
			VALUES ('others5_type','text_no_lang');";

	$update_sql[]="
			INSERT INTO `product__configadmin` (`name`) 
			VALUES ('others6');";
	$update_sql[]="
			INSERT INTO `product__configadmin` (`name`,value) 
			VALUES ('others6_type','text_no_lang');";

	$update_sql[]="
			INSERT INTO `product__configadmin` (`name`) 
			VALUES ('others7');";
	$update_sql[]="
			INSERT INTO `product__configadmin` (`name`,value) 
			VALUES ('others7_type','text_no_lang');";

	$update_sql[]="
			INSERT INTO `product__configadmin` (`name`) 
			VALUES ('others8');";
	$update_sql[]="
			INSERT INTO `product__configadmin` (`name`,value) 
			VALUES ('others8_type','text_no_lang');";
	
	
	
	global $gl_languages;	
	
	foreach($gl_languages['names'] as $lang=>$val){
		$update_sql[]="
			INSERT INTO `product__configadmin_language` (`name`, `language`) 
			VALUES ('others5', '".$lang."');";
		$update_sql[]="
			INSERT INTO `product__configadmin_language` (`name`, `language`) 
			VALUES ('others6', '".$lang."');";
		$update_sql[]="
			INSERT INTO `product__configadmin_language` (`name`, `language`) 
			VALUES ('others7', '".$lang."');";
		$update_sql[]="
			INSERT INTO `product__configadmin_language` (`name`, `language`) 
			VALUES ('others8', '".$lang."');";
	}
	
}

?>