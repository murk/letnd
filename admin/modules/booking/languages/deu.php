<?
// menus eina
if (!defined('INMO_MENU_PROPERTY')){
	define('INMO_MENU_PROPERTY', 'Immobles');
	define('INMO_MENU_PROPERTY_NEW', 'Entrar nou immoble');
	define('INMO_MENU_PROPERTY_LIST', 'Llistar immobles');
	define('INMO_MENU_SEARCH_FORM', 'Buscar i Editar immobles');
	define('INMO_MENU_PROPERTY_BIN', 'Paperera de reciclatge');
	define('INMO_MENU_CATEGORY', 'Tipus d\'immobles');
	define('INMO_MENU_CATEGORY_NEW', 'Entrar nou tipus');
	define('INMO_MENU_CATEGORY_LIST', 'Llistar tipus');
	define('INMO_MENU_TRANSLATION', 'Traduccions');
	define('INMO_MENU_PROPERTY_TRANSLATION_LIST', 'Llistar immobles per traduir');
	define('INMO_MENU_CATEGORY_TRANSLATION_LIST', 'Llistar categories per traduir');
	define('INMO_MENU_CONFIG', 'Configuració Immobles');
	define('INMO_MENU_CONFIGADMIN_LIST', 'Configuració part privada');
	define('INMO_MENU_CONFIGPUBLIC_LIST', 'Configuració part pública');
	define('INMO_MENU_CONFIGADMIN', 'Configuració part privada');
	define('INMO_MENU_CONFIGPUBLIC', 'Configuració part pública');
	define('INMO_MENU_ZONE', 'Zones');
	define('INMO_MENU_ZONE_NEW', 'Entrar nova zona');
	define('INMO_MENU_ZONE_LIST', 'Llistar zones');
	define('INMO_MENU_ZONE_BIN', 'Paperera de reciclatge');
	define('INMO_MENU_TV', 'Aparadors virtuals');
	define('INMO_MENU_TV_LIST', 'Sel·leccionar immobles');
	define('INMO_MENU_TV_LIST_SELECTED', 'Immobles a l\'aparador');
	define('INMO_MENU_TV_LIST_DOWNLOAD', 'Descarregar aparador');
	define('INMO_MENU_PROPERTY_MAP_LIST', 'Buscar per mapa');
	define('INMO_MENU_ADJECTIVE_NEW', 'Entrar nou adjectiu');
	define('INMO_MENU_ADJECTIVE_LIST', 'Listar adjectius');
	
	define('BOOKING_MENU_BOOK','Reserves');
	define('BOOKING_MENU_BOOK_NEW','Entrar nova reserva');
	define('BOOKING_MENU_BOOK_LIST','Llistar reserves');
	define('BOOKING_MENU_BOOK_LIST_PAST','Reserves finalitzades');
	define('BOOKING_MENU_BOOK_SEARCH','Disponibilitat');
	
	define('BOOKING_MENU_EXTRA','Serveis');
	define('BOOKING_MENU_EXTRA_NEW','Entrar nou servei');
	define('BOOKING_MENU_EXTRA_LIST','Llistar serveis');
	define('BOOKING_MENU_EXTRA_LIST_BIN','Paperera de reciclatge');
	
	define('BOOKING_MENU_SEASON','Temporades');
	define('BOOKING_MENU_SEASON_NEW','Entrar nova temporada');
	define('BOOKING_MENU_SEASON_LIST','Llistar temporades');

	define ('TITLE_COMARCA_ADMIN','Per zona: ');
	define('PROPERTY_AUTO_REF', 'Automàtica');

	define('BOOKING_MENU_PROMCODE','Codi promocional');
	define('BOOKING_MENU_PROMCODE_NEW','Crear codi');
	define('BOOKING_MENU_PROMCODE_LIST','Llistar codi');
	define('BOOKING_MENU_PROMCODE_LIST_BIN','Paperera');

	define('BOOKING_MENU_POLICE','Registre Mossos');
	define('BOOKING_MENU_POLICE_LIST','Llistar reserves');
}

// captions seccio
$gl_caption_book['c_payment'] = 'Pago';
$gl_caption_book['c_payed'] = 'Pagado';
$gl_caption_book['c_payment_num'] = 'Núm.';
$gl_caption_book['c_payment_1'] = 'Pago 1';
$gl_caption_book['c_payment_2'] = 'Pago 2';
$gl_caption_book['c_payment_3'] = 'Pago 3';
$gl_caption_book['c_payment_4'] = 'Pago 4';
$gl_caption_book['c_payment_done_1'] =
$gl_caption_book['c_payment_done_2'] =
$gl_caption_book['c_payment_done_3'] =
$gl_caption_book['c_payment_done_4'] = 'Pagado';
$gl_caption_book['c_payment_entered_1'] =
$gl_caption_book['c_payment_entered_2'] =
$gl_caption_book['c_payment_entered_3'] =
$gl_caption_book['c_payment_entered_4'] = 'Fecha';
$gl_caption_book['c_payment_rest'] = 'Pago pendiente';
$gl_caption_book['c_payment_uncheck_message'] = '¿Seguro que quieres desmarcar el pago?';
$gl_caption_book['c_payment_send_mail'] = 'enviar&nbsp;mail';
$gl_caption_book['c_payment_send_mail_subject'] = 'Mitteilung über die Zahlung der Reservierung: %1$s';
$gl_caption_book['c_payment_send_mail_body'] = 'Lieber %1$s, <p>Wir informieren Sie, dass wir die Zahlung von %2$s € für Ihre Buchung núm. %3$s, am Tag %4$s, empfangen haben.</p>';
$gl_caption_book['c_payment_send_mail_body_rest'] = '<p>Restbetrag, %1$s €</p>';
$gl_caption_book['c_payment_send_mail_body2'] = '<p>Mit freundlich Grüßen</p>';
$gl_caption_book['c_payment_mail_sent'] = 'El E-mail se ha enviado correctamente a:';
$gl_caption_book['c_payment_mail_not_sent'] = 'No se pudo enviar el E-mail a:';
$gl_caption_book['c_payment_mail_not_sent_no_mail'] = 'El client no tiene ninguna dirección de email entrada';
$gl_caption_book['c_is_owner'] = 'Owner';
$gl_caption_book['c_caption_is_owner'] = 'All';
$gl_caption_book['c_is_owner_0'] = 'No';
$gl_caption_book['c_is_owner_1'] = 'Yes';

$gl_caption_book['c_property_title'] = 'Eigentum';
$gl_caption_book['c_names'] = 'Kunden';
$gl_caption_book['c_dates'] = 'Buchungsdatum';
$gl_caption['c_dates_from'] = 'Vom';
$gl_caption['c_dates_to'] = 'bis zum';
$gl_caption_book['c_date_in'] = 'Datum der Anreise';
$gl_caption_book_list['c_date_in'] = 'Entrée';
$gl_caption_book['c_date_out'] = 'Datum der Abreise';
$gl_caption_book_list['c_date_out'] = 'Sortie';
$gl_caption_book['c_book_entered'] = 'Reserviert';
$gl_caption_book['c_capacity'] = 'Kapazität';
$gl_caption_book['c_person'] = 'Personen';
$gl_caption_book['c_adult'] = 'Erwachsene';
$gl_caption_book['c_child'] = 'Kinder';
$gl_caption_book['c_baby'] = 'Babys';
$gl_caption_book['c_book_id'] = 'Reservierung';
$gl_caption_book['c_property_id'] = 'Eigentum';
$gl_caption_book['c_choose_first'] = "Sie müssen zuerst Erwachsene wählen ( 1 )";
$gl_caption['c_status'] = "State";
$gl_caption['c_status_group_free'] = "Free";
$gl_caption['c_status_group_not_free'] = "Occupied";
$gl_caption['c_status_pre_booked'] = "Pre booked";
$gl_caption['c_status_cancelled'] = "Cancelled";
$gl_caption['c_status_booked'] = "Booked";
$gl_caption['c_status_confirmed_email'] = "Confirmed by email";
$gl_caption['c_status_confirmed_post'] = "Confirmed by post";
$gl_caption['c_status_contract_signed'] = "Contract signed";
$gl_caption_book['c_caption_filter_status'] = 'All';

for ($i=0;$i<=25;$i++){	
	
	$gl_caption_book['c_adult_'.$i] = $i;
	$gl_caption_book['c_child_'.$i] = $i;
	$gl_caption_book['c_baby_'.$i] = $i;
	
}

$gl_caption_book ['c_maximum'] = 'Maximum';
$gl_caption_book ['c_name'] = 'Name';
$gl_caption_book ['c_surnames'] = 'Vorname';
$gl_caption_book ['c_mail'] = 'E-mail';
$gl_caption_book ['c_phone'] = 'Telefon';
$gl_caption_book ['c_comment'] = 'Anmerkungen';
$gl_caption_book ['c_comment_private'] = 'Private comments';
//$gl_caption_book ['c_property_id'] = '';
$gl_caption_book ['c_send_new'] = 'Reservieren/Buchen';
$gl_caption_book ['c_custumers'] = 'Kunden';
$gl_caption_book ['c_price_extras'] = 'Extras';
$gl_caption_book ['c_price_extras_search'] = 'Extraspreis';
$gl_caption_book ['c_price_total'] = 'Gesamtpreis';
$gl_caption_book ['c_price_book'] = 'Buchungspreis';
$gl_caption_book ['c_price'] = 'Preis';
$gl_caption_book ['c_edit_book_button'] = 'die Buchung bearbeiten';
$gl_caption_book ['c_download_contract'] = 'Contract';
$gl_caption_book ['c_custumer_search_group'] = 'Clientes';
$gl_caption_book ['c_extra_search_group'] = 'Extras';
$gl_caption_book ['c_recalculate_rate'] = 'Recalculate price';
$gl_caption_book ['c_is_manual_price'] = 'Manual price';

$gl_caption_book['c_form_title'] = 'Buchung';

$gl_messages_book['added'] = "Buchung zwischen %s und %s ausgeführt ";
$gl_messages_book['added2'] = "Buchung von %s zwischen %s und %s ausgeführt";
$gl_messages_book['saved'] = "Aktualisierte Buchung zwischen : %s %s";
$gl_messages_book['saved2'] = "Buchung von %s zwischen %s und %s aktualisiert";
$gl_messages_book['deleted'] = "Buchung zwischen %s und %s annuliert";
$gl_messages_book['deleted2'] = "Buchung von %s zwischen %s und %s annuliert";
$gl_messages_book['full'] = "Voll zwischen %s und %s";
$gl_messages_book['full2'] = 'Dieses Eigentum ist  belegt  zwischen: %s und %s';
$gl_messages_book['full3'] = 'Buchung nicht möglich ( Konflikt ) ';
$gl_messages_book['has_no_range'] = 'Dieses Eigentum wird an diesem Datum nicht vermietet';
$gl_messages_book['too_many_person'] = $gl_caption['c_too_many_person_error'] = 'Die Personenanzahl übersteigt die Wohnungs / Haus kapazität ';
$gl_messages_book['exceeds_31_days'] = 'Els habitatges de lloguer de temporada no es poden llogar per un periode superior a 31 dies, per qualsevol dubte, si us plau contacti la nostra oficina';


// custumers
$gl_caption['c_add_new_custumer'] = "Neue Kund eingeben";
$gl_caption['c_edit_custumer'] = "Kunde bearbeiten";
$gl_caption['c_add_custumer'] = "Kunde wählen";

$gl_caption['c_adult_singular'] = "Erwachsene";
$gl_caption['c_child_singular'] = "Kinder";
$gl_caption['c_baby_singular'] = "Baby";


$gl_caption_extra ['c_extra_book_selected'] = 'Auswählen';
$gl_caption_extra ['c_extra_book_input'] = '';

// serveis
$gl_caption['c_extra_type'] = "Typ/art";
$gl_caption['c_extra_type_required'] = "Obligatorisch";
$gl_caption['c_extra_type_optional'] = "Zusätzlich";
$gl_caption['c_extra_type_included'] = "Inbegriffen";
$gl_caption['c_extra'] = "Extras";
$gl_caption['c_has_quantity'] = "wählbaren Betrag";
$gl_caption_list['c_has_quantity'] = "Betrag";
$gl_caption['c_price'] = "Preis";
$gl_caption['c_extra_price'] = "Preis";
$gl_caption['c_comment'] = "Anmerkungen";
$gl_caption['c_extras'] = "Extras";
$gl_caption['c_concept'] = "Concepte";
$gl_caption['c_concept_normal'] = "Extra";
$gl_caption['c_concept_deposit'] = "Fiança";
$gl_caption['c_concept_cleaning'] = "Neteja";

$gl_caption['c_extra_type_required_search'] = "Obligatorisch extras";
$gl_caption['c_extra_type_optional_search'] = "Zusätzlich extras";
$gl_caption['c_extra_type_included_search'] = "Inbegriffen extras";
$gl_caption['c_extra_total_price_search'] = "Preis total extra";
$gl_caption['c_extra_quantity_search'] = "Betrag extra";
$gl_caption['c_extra_type_search'] = "Typ/art extra";

// temporades
$gl_caption['c_season'] = $gl_caption['c_season_id'] = "Jahreszeit";
$gl_caption['c_seasons'] = "Jahreszeiten";
$gl_caption['c_ordre'] = "Reihenfolge";
$gl_caption['c_date_start'] = "Anfang";
$gl_caption['c_date_end'] = "Ende";
$gl_caption['c_minimum_nights'] = "Minimum";
$gl_caption['c_minimum_nights_1'] = "1 Nacht";
$gl_caption['c_minimum_nights_2'] = "2 Nächte";
$gl_caption['c_minimum_nights_3'] = "3 Nächte";
$gl_caption['c_minimum_nights_4'] = "4 Nächte";
$gl_caption['c_minimum_nights_5'] = "5 Nächte";
$gl_caption['c_minimum_nights_6'] = "6 Nächte";
$gl_caption['c_minimum_nights_7'] = "1 Woche";
$gl_caption['c_minimum_nights_8'] = "8 Nächte";
$gl_caption['c_minimum_nights_9'] = "9 Nächte";
$gl_caption['c_minimum_nights_10'] = "10 Nächte";
$gl_caption['c_minimum_nights_11'] = "11 Nächte";
$gl_caption['c_minimum_nights_12'] = "12 Nächte";
$gl_caption['c_minimum_nights_13'] = "13 Nächte";
$gl_caption['c_minimum_nights_14'] = "2 Wochen";
$gl_caption['c_price_night'] = "Preis pro Nacht";
$gl_caption_list['c_price_night'] = "Nacht";
$gl_caption['c_price_weekend'] = "Preis pro Nacht Wochenende";
$gl_caption_list['c_price_weekend'] = "Wochenende";
$gl_caption['c_price_more_week'] = "Preis + 7 Nächte";
$gl_caption['c_price_week'] = "Preis pro Woche";
$gl_caption_list['c_price_week'] = "Woche";
$gl_caption['c_price_week_type'] = "";
$gl_caption['c_price_week_type_week'] = "Pro Woche";
$gl_caption['c_price_week_type_night'] = "Pro Nacht";
$gl_caption['c_color_id'] = $gl_caption['c_color'] = "Farbe";
$gl_caption['c_already_used'] = "Farbe schon zugeordnet";

$gl_caption['c_detail_season'] = "Jahreszeit";
$gl_caption['c_detail_day'] = "Tag";
$gl_caption['c_detail_type'] = "Tarif";
$gl_caption['c_detail_price'] = "Preis";
$gl_caption['c_detail_price_night'] = "Nacht";
$gl_caption['c_detail_price_weekend'] = "Nacht am Wochenende";
$gl_caption['c_detail_price_week'] = "Woche Preis pro Nacht";
$gl_caption['c_detail_whole_week'] = "Woche Preis pro Woche";


$gl_caption['c_whole_week'] = "Ganze Wochen";
$gl_caption_list['c_whole_week_0'] = "";
$gl_caption_list['c_whole_week_1'] = "Ja";

$gl_caption['c_start_day'] = "Erster Tag";
$gl_caption['c_start_day_0'] = "Irgend welche ";
$gl_caption_list['c_start_day_0'] = "";
$gl_caption['c_start_day_1'] = "Mo";
$gl_caption['c_start_day_2'] = "Di";
$gl_caption['c_start_day_3'] = "Mi";
$gl_caption['c_start_day_4'] = "Do";
$gl_caption['c_start_day_5'] = "Fr";
$gl_caption['c_start_day_6'] = "Sam";
$gl_caption['c_start_day_7'] = "So";

$gl_caption_propertyrange['c_form_title'] = " Preis Buchungs Zeitraum ";
$gl_messages_propertyrange['full2'] = "There is already a defined season between dates:  %s and %s for the property \"%s\"";

$gl_caption['c_minimum_nights_error'] = "Es ist nicht möglich weniger als %s Nächte zu buchen für die Daten zwischen %s und %s";
$gl_caption['c_whole_week_error'] = "Sie müssen komplette Wochen buchen für die Daten zwischen %s und %s";
$gl_caption['c_start_day_error'] = "Für diesen Zeitraum es ist erforderlich komplette Wochen zu buchen";
$gl_caption['c_equal_dates_error'] = "Zwei Datum können nicht gleich sein";
$gl_caption['c_no_date_selected'] = "Kein Datum ausgewählt";



$gl_caption['c_promcode'] = 'Angebotscode';
$gl_caption_promcode['c_name'] = 'Name';
$gl_caption_promcode['c_discount_pvp'] = 'Rabatt';
$gl_caption_promcode['c_discount_type'] = 'Rabattcode';
$gl_caption_promcode['c_discount_type_fixed'] = 'Fixbetrag';
$gl_caption_promcode['c_discount_type_percent'] = 'Prozentsatz';
$gl_caption_promcode['c_minimum_amount'] = 'Aufwand minimum ';
$gl_caption_promcode['c_start_date'] = 'Startdatum';
$gl_caption_promcode['c_end_date'] = 'Enddatum';
$gl_caption_book['c_promcode_discount'] = 'Dte. codi promocional';
$gl_caption['c_promcode_button'] = '';

$gl_caption_promcode['c_custumer_id'] = 'Kunde';
$gl_caption['c_custumer_caption'] = 'Alle';
$gl_caption_promcode['c_custumer_id_format'] = '%s %s %s';
$gl_caption_promcode['c_one_time_only'] = 'Aplicar sólo una vez por cliente';
$gl_caption_promcode_list['c_one_time_only'] = 'Sólo una vez';


$gl_caption_police['c_police_search_title'] = 'Buchung zwischen Datums';
$gl_caption['c_treatment_police'] = 'Sexe';
$gl_caption_police['c_treatment_sr'] = 'Masculí';
$gl_caption_police['c_treatment_sra'] = 'Femení';
$gl_caption['c_download_selected'] = 'Download file';


$gl_caption['c_tt'] =  $gl_caption['c_price_tt'] = 'Kurtaxe';
$gl_caption['c_promcode_button'] = 'See';

$gl_caption['c_promcode_err_not_valid'] = 'Der Angebotscode ist nicht gültig';
$gl_caption['c_promcode_err_minimum'] = 'Um den Rabatt zu benutzen, muss der Reservationsbetrag um % höher sein ';
$gl_caption['c_promcode_err_expired'] = 'Der Angebotscode ist abgelaufen';
$gl_caption['c_promcode_err_used'] = 'Der Code wurde bereits benutzt ';
$gl_caption['c_promcode_err_not_logged'] = 'Um der Angebotscode zu wenden, Sie müssen registriert sein';

$gl_caption ['c_promcode']='Angebotscode';

// buscador
$gl_caption['c_by_ref'] = '( <strong>Ref</strong>:</strong> "10 23 45" . <strong>Date:</strong> "12/2015" , "2015" , "22/12/2015" )';
$gl_caption['c_fields'] = "Fields";
$gl_caption['c_nights'] = "Nights";
$gl_caption['c_excel_name'] = "reservas";
$gl_caption['c_exact_date'] = "Data exacta";

$gl_messages ['has_no_owner']="Atenció, l'immoble no té cap propietari assignat";


// Pagament
$gl_caption['c_order_status'] = 'Payment status';
$gl_caption_list['c_order_status'] = 'Payment';
$gl_caption['c_order_status_paying'] = 'Outstanding';
$gl_caption['c_order_status_failed'] = 'Payment failed';
$gl_caption['c_order_status_pending'] = 'Pending payment to be accepted';
$gl_caption['c_order_status_denied'] = 'Pagament rebutjat';
$gl_caption['c_order_status_voided'] = 'Payment canceled';
$gl_caption['c_order_status_refunded'] = 'Payment canceled';
$gl_caption['c_order_status_completed'] = 'Completed payment';

$gl_caption['c_payment_method'] = 'Payment method';
$gl_caption['c_payment_method_paypal'] = 'Paypal';
$gl_caption['c_payment_method_account'] = 'Bank transfer';
$gl_caption['c_payment_method_directdebit'] = 'Direct debit';
$gl_caption['c_payment_method_lacaixa'] = 'Credit card';
$gl_caption['c_payment_method_none'] = '';
$gl_caption['c_payment_method_cash'] = 'Cash';
$gl_caption['c_payment_method_physicaltpv'] = 'Physical TPV';

for ( $i = 1; $i <= 4; $i ++ ) {
	$gl_caption[ 'c_order_status_' . $i ]                    = $gl_caption['c_order_status'];
	$gl_caption_list[ 'c_order_status_' . $i ]               = $gl_caption_list['c_order_status'];
	$gl_caption[ 'c_order_status_' . $i . '_paying' ]        = $gl_caption['c_order_status_paying'];
	$gl_caption[ 'c_order_status_' . $i . '_failed' ]        = $gl_caption['c_order_status_failed'];
	$gl_caption[ 'c_order_status_' . $i . '_pending' ]       = $gl_caption['c_order_status_pending'];
	$gl_caption[ 'c_order_status_' . $i . '_denied' ]        = $gl_caption['c_order_status_denied'];
	$gl_caption[ 'c_order_status_' . $i . '_voided' ]        = $gl_caption['c_order_status_voided'];
	$gl_caption[ 'c_order_status_' . $i . '_refunded' ]      = $gl_caption['c_order_status_refunded'];
	$gl_caption[ 'c_order_status_' . $i . '_completed' ]     = $gl_caption['c_order_status_completed'];
	$gl_caption[ 'c_payment_method_' . $i ]                  = $gl_caption['c_payment_method'];
	$gl_caption[ 'c_payment_method_' . $i . '_paypal' ]      = $gl_caption['c_payment_method_paypal'];
	$gl_caption[ 'c_payment_method_' . $i . '_account' ]     = $gl_caption['c_payment_method_account'];
	$gl_caption[ 'c_payment_method_' . $i . '_directdebit' ] = $gl_caption['c_payment_method_directdebit'];
	$gl_caption[ 'c_payment_method_' . $i . '_lacaixa' ]     = $gl_caption['c_payment_method_lacaixa'];
	$gl_caption[ 'c_payment_method_' . $i . '_none' ]        = $gl_caption['c_payment_method_none'];
	$gl_caption[ 'c_payment_method_' . $i . '_cash' ]        = $gl_caption['c_payment_method_cash'];
	$gl_caption[ 'c_payment_method_' . $i . '_physicaltpv' ] = $gl_caption['c_payment_method_physicaltpv'];
}

$gl_caption ['c_books_not_deleted_not_owner'] = "The marked booking have not been deleted because they where created by an external application";

$gl_caption['c_book_owner'] = "Booked by";