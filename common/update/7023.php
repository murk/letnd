<?php
/* 22-6-17
Files, actualització per poder descarregar o  no, resetejar nom al upload, i el nom vell d'arxiu

*/

if ( is_table( 'all__configadmin' ) && ! Db::get_first( "SELECT * FROM all__configadmin WHERE name='files_show_download'" ) ) {
	$update_sql[] = "
		INSERT INTO all__configadmin (`name`, `value`) VALUES ('files_show_download', '1');";

} else {
	if ( is_table( 'all__configadmin' ) ) {
		trigger_error( 'Ja existeixen el camp files_show_download' );
	}
}

if ( is_table( 'all__configadmin' ) && ! Db::get_first( "SELECT * FROM all__configadmin WHERE name='files_override_file_name'" ) ) {
	$update_sql[] = "
		INSERT INTO all__configadmin (`name`, `value`) VALUES ('files_override_file_name', '0');";

} else {
	if ( is_table( 'all__configadmin' ) ) {
		trigger_error( 'Ja existeixen el camp files_override_file_name' );
	}
}


if ( is_table( 'all__configadmin' ) && ! Db::get_first( "SELECT * FROM all__configadmin WHERE name='files_show_original_name'" ) ) {
	$update_sql[] = "
		INSERT INTO all__configadmin (`name`, `value`) VALUES ('files_show_original_name', '1');";

} else {
	if ( is_table( 'all__configadmin' ) ) {
		trigger_error( 'Ja existeixen el camp files_show_original_name' );
	}
}



$tables = Db::get_rows_array( 'SHOW TABLES' );

// Bucle per totes les taules amb arxius
foreach ( $tables as $rs ) {
	$table = $rs[0];
	if (
		substr( $table, - 5 ) === '_file'
		|| $table == 'pv__seguimentitem_evidencia'
		|| $table == 'product__product_download'
		|| $table == 'newsletter__message_picture'
		) {

		$update_sql[] = "ALTER TABLE $table
							ADD COLUMN `name_original` VARCHAR(255) NOT NULL AFTER `name`;";
		$update_sql[] = "ALTER TABLE $table
							CHANGE COLUMN `name` `name` VARCHAR(255);";
		$update_sql[] = "UPDATE $table
							set `name_original` = `name`;";

	}
}




?>