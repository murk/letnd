<link href="/admin/themes/inmotools/styles/inmo_booking.css?v=<?=$version?>" rel="stylesheet" type="text/css">
<script language="JavaScript">

$(document).ready(				
	function ()
		{	
			$('#content').height($("#popupFrame", top.document).height());			
			

			$('#date_in').datepicker({
				changeMonth: true,
				changeYear: true,
				yearRange: '+0:+5',
				onClose: function( selectedDate ) { 
					var date = $(this).datepicker('getDate');
					if (date) {
						  date.setDate(date.getDate() + 4);
					}
					$( '#date_out' ).datepicker( 'option', 'minDate', date );
				}
			});
			$('#date_out').datepicker({
				changeMonth: true,
				changeYear: true,
				yearRange: '+0:+5',
				onClose: function( selectedDate ) {
					var date = $(this).datepicker('getDate');
					if (date) {
						  date.setDate(date.getDate() - 4);
					}
					$( '#date_in' ).datepicker( 'option', 'maxDate', date );
				}
			}); 
	}
)

</script>

<div class="search booking">
	<h3><?=$c_book_search_title?></h3>  
	<form name="search" method="get" action="/admin/">
		<table border="0" cellspacing="0" cellpadding="0">
			<tr valign="middle">		
					<td><?=$c_adult?>
						<input name="action" type="hidden" value="list_records_book_search">
						<input name="menu_id" type="hidden" value="<?=$menu_id?>">
						<?=$adult?>
					</td>	
					<td><?=$c_child?>
						<?=$child?>
					</td>	
					<td><?=$c_baby?>
						<?=$baby?>
					</td>
			</tr>
			<tr>			
					<td><?=$c_dates_from?>
						<input id="date_in" name="date_in" type="text" size="8" value="<?=$date_in?>">
					</td>	
					<td><?=$c_dates_to?>
						<input id="date_out" name="date_out" type="text" size="8" value="<?=$date_out?>">
					</td>
					<td class="buttons-td"><input type="submit" name="Submit" value="ok" class="boto-search"></td>
			</tr>
		</table>
	</form>
</div>