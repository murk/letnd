<?
$gl_caption['c_subject_booked'] = 'Booking property - #';
$gl_caption['c_subject_booked_owner'] = 'Booking property - #';
$gl_caption['c_no_date_selected'] = "No date selected";
$gl_caption['c_booked_message_1'] = "The booking has been made successfuly";
$gl_caption['c_booked_message_2'] = "Below enter the rest of the information";
$gl_caption['c_form_book_title'] = "Booking data";
$gl_caption['c_form_book_custumer_title'] = "Personal data";
$gl_caption['c_book_finish_button'] = "Finish booking";
$gl_caption['c_book_form_title'] = "Booking #%s";

$gl_caption['c_edit_book'] = "Edit booking";

$gl_caption['c_mail_book_details'] = "Booking details";
$gl_caption['c_mail_custumer_details'] = "Personal data";
$gl_caption['c_mail_extras'] = "Extras";
$gl_caption ['c_ref']='Ref.';
$gl_caption['c_promcode_button'] = 'Apply';


$gl_caption_payment['c_payment_title'] = 'Making the purchase';
$gl_caption_payment['c_method_title1']="Choose your payment method.";
$gl_caption_payment['c_method_title2']="The amount is <strong>%s&nbsp;%s</strong> VAT included.";
$gl_caption_payment['c_method_title_initial']="The initial payment is <strong>%s&nbsp;%s</strong> VAT included. The rest of the payment, %s&nbsp;%s, must be done before the entry date";
$gl_caption_payment['c_method_title_rest']="The amount of the %s payment is <strong>%s&nbsp;%s</strong> VAT included.";
$gl_caption_payment['c_method_title_number_2']="second";
$gl_caption_payment['c_method_title_number_3']="third";
$gl_caption_payment['c_method_title_number_4']="fourth";

$gl_caption_payment['c_method_paypal']="PayPal payment";
$gl_caption_payment['c_method_account']="Bank transfer payment";
$gl_caption_payment['c_method_lacaixa']="Credit card";

$gl_caption_payment['c_show_paypal']="Paypal payment is being made, if the process doesn't begin automatically please clik here:";
$gl_caption_payment['c_show_lacaixa']="Card payment is being made, if the process doesn't begin automatically please clik here:";

$gl_caption['c_account_title']= 'Bank trasfer information' ;
$gl_caption['c_account_amount']= 'Amount' ;
$gl_caption['c_account_concept']= 'Concept' ;

$gl_caption_book['c_payment_whole']="Payment";
$gl_caption_book['c_payment_1']="First payment";
$gl_caption_book['c_payment_2']="Second payment";
$gl_caption_book['c_payment_3']="Third payment";
$gl_caption_book['c_payment_4']="Fourth payment";
$gl_caption_book['c_payment_paid']="Paid";
$gl_caption_book['c_payment_pending']="Pending";
$gl_caption_book['c_payment_pay']="Pay";
?>