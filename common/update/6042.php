<?

/* 2-06-15
  Canvi inmo, afegir camps

UP-DATE
Actualització del mòdul d'inmobles, s'ha afegit el camp "Estrelles" i el camp "Gestionat pel propietari" a lloguer turístic

 */

if (is_table('inmo__property') && !is_field('inmo__property','star')) {

	$update_sql .= "
			ALTER TABLE inmo__property
				ADD COLUMN star ENUM('0','1','2','3','4','5') NOT NULL DEFAULT '0' AFTER managed_by_owner;
		";
}
else {
	if (is_table('inmo__property')) {
		trigger_error('Ja existeixen el camp star');
	}
}

?>