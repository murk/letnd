<?if (!defined('PRODUCT_DISCOUNT_MAX_RESULTS')) Module::get_config_values('product');?>
<article>
  <h2>Al comprar a  <?=COMPANY_NAME?>, vosté pot comprar mitjançant:
  </h2>
  <p><strong>Pagament amb Paypal:</strong><br />
  Pagui de la forma més ràpida i estesa mitjançant targeta de crèdit o transferència bancària amb el seu compte Paypal</p>
  
  <p><strong>Pagament targeta de crèdit o de dèbit:</strong><br />
  Pot utilitzar la seva targeta de crèdit o de dèbit, Visa o Mastercard</p>
  
  <p><strong>Pagament contra reembors: </strong><br />
  Vostè paga en el moment en que correus li entrega la seva comanda. 
  </p>
  <p><strong>Mitjançant transferència bancària:</strong><br />
  Vostè realitza una transferència bancària a: <?=PRODUCT_BANC_NAME?>, número de compte: <?=PRODUCT_ACCOUNT_NUMBER?>, amb l'import total, i un cop confirmada la seva transferència <?=COMPANY_NAME?> li enviarà la seva comanda.
  </p>
</article>