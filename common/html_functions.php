<?
/*
 * Funcions per obtindre html dins les plantilles
 * 
 * @copyright Copyright (c) 17/10/2013
 * @version $Id$
 * @access public
 */

function get_header($tpl) {
	
	$ret =  '
		<title>' . $tpl->get_var('page_title') . '</title>
		<meta name="Description" content="' . $tpl->get_var('page_description') . '" />
		<meta name="Keywords" content="' . $tpl->get_var('page_keywords') . '" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link rel="profile" href="http://microformats.org/profile/hcard">		
		<link rel="shortcut icon" href="' . DIR_TEMPLATES . 'images/favicon.ico?v='.CLIENT_VERSION.'" />		
		<!--[if lt IE 9]>
		<script src="/common/jscripts/html5shiv.js"></script>
		<![endif]-->
	';

	$host_url = substr(HOST_URL,0,-1);

	// No se si es necessari posar-lo, però hauria de portar la uri, no la url: <meta property="og:url" content="' . $host_url . '" />
	$ret .=  '
		<meta property="og:title" content="' . $tpl->get_var('page_og_title') . '" />
		<meta property="og:description" content="' . $tpl->get_var('page_og_description') . '" />
	';

	$og = $tpl->get_var('page_og');

	$image = isset($og['image'])?$og['image']:'';

	if ($image){
		$ret .= '<meta property="og:image" content="' . $host_url . $image . '" />';
	}

	$type = isset($og['type'])?$og['type']:'';

	if ($type){
		$ret .= '<meta property="og:type" content="' . $type . '" />';
	}

	return $ret;
	
}


function load_scss() {
	
	$is_local = $GLOBALS['gl_is_local'];

	if ($is_local){

		$ret = '<link rel="stylesheet" type="text/css" href="' . DIR_TEMPLATES . 'styles/styles.css" />';

	}
	else{

		$client_version=CLIENT_VERSION;
		// $force = FORCE_CSS_JSCRIPT_GENERATION;
		// TODO-i Quan i com ho faig aixó? Mentres jscript no vagi per gulp no cal, quan canvi retocar també consola client update
		// nova versio de l'arxiu
		// if ($force) Db::execute( "UPDATE all__configpublic SET `value`='" . ( ++ $client_version ) . "' WHERE  `name`='client_version'" );


		$ret = '<link rel="stylesheet" type="text/css" href="' . DIR_TEMPLATES . 'styles/styles.min.css?v=' . $client_version . '" />';
	}
	
	return $ret;
}


function load_css($css, $other = '') {

	$is_local = $GLOBALS['gl_is_local'];
	//$is_local = false; // PER TESTEJAR

	$file = CLIENT_PATH . 'web/styles.css';

	// deixo els arxius al seu lloc
	if ($is_local){
		$ret = '';

		$css = explode(',',$css);
		foreach ($css as $c) {
			$ret .= '<link rel="stylesheet" type="text/css" href="' . DIR_TEMPLATES . 'styles/' . trim($c) . '.css" />';
		}

		$css = explode(',',$other);
		foreach ($css as $c) {
			$ret .= '<link rel="stylesheet" type="text/css" href="' . trim($c) . '" />';
		}
		return $ret;
	}

	$client_version = CLIENT_VERSION;
	$force = FORCE_CSS_JSCRIPT_GENERATION;

	// a public genero un sol arxiu, i nomes el regenero si el borro manualment
	if(!file_exists($file) || $force){

		// a public
		$content = '';

		$css = explode(',',$css);
		foreach ($css as $c) {
			$new_content = file_get_contents(PATH_TEMPLATES . 'styles/' . trim($c) . '.css');
			$new_content = str_replace('../images/','../templates/images/',$new_content);
			$content .= $new_content;
		}

		$css = explode(',',$other);
		foreach ($css as $c) {
			$c = trim($c);

			$dirs = explode('/',$c);

			array_pop($dirs);
			$path1 = implode('/',$dirs);

			array_pop($dirs);
			$path2 = implode('/',$dirs);

			array_pop($dirs);
			$path3 = implode('/',$dirs);


			// No crec que hi hagi més de 2 nivells
			$new_content = file_get_contents(DOCUMENT_ROOT . $c);
			// substitueixo ../../
			$new_content = str_replace('url(\'../..','url{xxxx}(\''.$path3 ,$new_content);
			$new_content = str_replace('url("../..','url{xxxx}("'.$path3 ,$new_content);
			$new_content = str_replace('url(../..','url{xxxx}('.$path3 ,$new_content);

			// substitueixo ../
			$new_content = str_replace('url(\'..','url{xxxx}(\''.$path2 ,$new_content);
			$new_content = str_replace('url("..','url{xxxx}("'.$path2 ,$new_content);
			$new_content = str_replace('url(..','url{xxxx}('.$path2 ,$new_content);

			// substitueixo
			$new_content = str_replace('url( \'/','url{xxxx}(\'/' ,$new_content);
			$new_content = str_replace('url("/','url{xxxx}("/' ,$new_content);
			$new_content = str_replace('url(/','url{xxxx}(/' ,$new_content);

			// substitueixo
			$new_content = str_replace('url( \'','url{xxxx}(\''.$path1 . '/' ,$new_content);
			$new_content = str_replace('url("','url{xxxx}("'.$path1 . '/' ,$new_content);
			$new_content = str_replace('url(','url{xxxx}('.$path1 . '/' ,$new_content);

			// substitueixo
			$new_content = str_replace('url{xxxx}(','url(' ,$new_content);


			$content .= $new_content;

		}
		file_put_contents($file, $content);

		// nova versio de l'arxiu
		Db::execute("UPDATE all__configpublic SET `value`='".(++$client_version)."' WHERE  `name`='client_version'");
		if ($force) Db::execute("UPDATE all__configpublic SET `value`='0' WHERE  `name`='force_css_jscript_generation'");
	}

	return '<link media="all" rel="stylesheet" type="text/css" href="/' . CLIENT_DIR . '/web/styles.css?v='.$client_version.'" />';
}
function load_admin_css() {

	$css = '
		general,
		header,
		footer,
		left_nav,
		fixed_menus,
		messages,
		menus,
		buttons,
		form,
		list,
		search,
		select_frame,
		uploader,
		modal,
		form_object,
		inmo,
		tooltip,
		results,
		login,
		map,
		print,
		drop_zone,
		tinymce,
		pnotify';

	$is_local = $GLOBALS['gl_is_local'];
	$make_general_css = R::text_id( 'make_general_css' );
	// $is_local = false; // PER TESTEJAR

	$file = PATH_TEMPLATES . 'styles/general.css';

	// Genero arxiu si poso el parametre a la url: make_general_css
	// if(!file_exists($file)){
	if($make_general_css !== false && $is_local){

		// a public
		$content = '';

		$css_array = explode(',',$css);
		foreach ($css_array as $c) {
			$new_content = file_get_contents(PATH_TEMPLATES . 'styles/local/' . trim($c) . '.css');
			$new_content = str_replace('../../images/','../images/',$new_content);
			$content .= $new_content;
		}
		file_put_contents($file, $content);

		return '<link media="all" rel="stylesheet" type="text/css" href="' . DIR_TEMPLATES . 'styles/general.css?v='. $GLOBALS['gl_version'] .'" />';
	}

	// deixo els arxius al seu lloc
	if ($is_local){
		$ret = '';

		$css_array = explode(',',$css);
		foreach ($css_array as $c) {
			$ret .= '<link rel="stylesheet" type="text/css" href="' . DIR_TEMPLATES . 'styles/local/' . trim($c) . '.css" />';
		}

		if (R::get('livereload') || isset($_SESSION['livereload'])) {
			$ret .= "<script src=\"//localhost:35729/livereload.js\"></script>";
			$_SESSION['livereload'] = true;
		} // així em carrega a totes ( window, clean, ... )
		
		return $ret;
	}
	else {

		return '<link media="all" rel="stylesheet" type="text/css" href="' . DIR_TEMPLATES . 'styles/general.css?v='. $GLOBALS['gl_version'] .'" />';

	}

}

function load_jscript($js) {
	
	$is_local = $GLOBALS['gl_is_local'];		
	//$is_local = false; // PER TESTEJAR
	
	$file = CLIENT_PATH . 'web/jscripts.js';
	
	$client_version = CLIENT_VERSION;
	$force = FORCE_CSS_JSCRIPT_GENERATION;

	// deixo els arxius al seu lloc
	if ($is_local){
		$ret = '';

		$js = explode(',',$js);
		foreach ($js as $j) {
			$ret .= '<script type="text/javascript" src="' . trim($j) . '"></script>';
		}

		$ret .= "<script src=\"//localhost:35729/livereload.js\"></script>"; // Al apache amb substitute donava molts problemes

		return $ret;
	}
	
	if (!file_exists($file) || $force){
		
		include_once DOCUMENT_ROOT . '/common/includes/jshrink/minifier.php';
		
		//$content = GOOGLE_ANALYTICS . ';'; el deixo al general.tpl
		$content = '';
		
		$js = explode(',',$js);		
		foreach ($js as $j) {
			$new_content = file_get_contents(DOCUMENT_ROOT . trim($j)). ";\n";

			if (strpos($j, '.min.') !== false){
				$content.=$new_content;
				Debug::add( $j,  "is min" );
			}
			else {
				$content.=JShrink\Minifier::minify( $new_content );
				Debug::add( $j,  "not is min" );
			}
		}
		file_put_contents($file, $content);
		
		// nova versio de l'arxiu
		Db::execute("UPDATE all__configpublic SET `value`='".(++$client_version)."' WHERE  `name`='client_version'");
		if ($force) Db::execute("UPDATE all__configpublic SET `value`='0' WHERE  `name`='force_css_jscript_generation'");
		
	}
	return '<script type="text/javascript" src="/' . CLIENT_DIR . '/web/jscripts.js?v='.$client_version.'"></script>';
}

// obtindre data formatejada per camp date html5 
// Ej: <time datetime="< =unformat_date_attr($entered) >">< =$entered ></time>
function unformat_date_attr($date) {

	// date time
	$pos = strpos($date , ' ');
	$time = '';
	if ($pos !== false){
	    $date_arr = explode( ' ', $date );
		$time = $date_arr[1];
	    $date = $date_arr[0];
	}

	$date = str_replace('/', '-', unformat_date($date));
	if ($time) $date .= ' ' . $time;

	return $date;
}

// Obtinc telefon com enllaç si convé

function get_phone ($phone=PAGE_PHONE, $class='tel', $no_mobile_tag = 'span', $text = '', $hide_tel = false) {

	if ($text && !$hide_tel)
		$text = $text . ' ';

	if (!$hide_tel)
		$text .= $phone;

	if ( $GLOBALS['gl_is_mobile'] ) {
		$phone_href = get_phone_link($phone);
		$html       = '<a href="' . $phone_href . '" title="' . $phone . '" class="' . $class . '">' . $text . '</a>';
	} else {
		$html='<' . $no_mobile_tag . ' title="' . $phone . '" class="' . $class . '">' . $text . '</' . $no_mobile_tag . '>';
	}
	
	return $html;
}
function get_phone_link ($phone) {

	$phone_href = str_replace( '.', '', $phone );
	$phone_href = str_replace( ' ', '', $phone_href );
	$phone_href = trim ($phone_href);
	return "tel:$phone_href";
}
// Obtinc calendari de families

function get_family_calendar ($family_id, $product_id) {
	
	$module = Module::load('product','familybusy');
	$module->parent = 'GetFamilyCalendar';
	
	return $module->get_calendar($family_id, $product_id);
}
// Obtinc html per formulari de contacte

function get_contact_form ($matter=false, $template=false) {

	$module = Module::load('contact','contact');
	$module->parent = 'GetContact';
	$module->matter = $matter;
	$module->template = $template;

	$html = $module->do_action('get_form_new');
	return $html;
}

// Obtinc html per reserva immoble

function get_book_form ($property_id, $template=false) {
	
	$module = Module::load('booking','book');
	$module->parent = 'GetBook';
	$module->property_id = $property_id;
	$module->one_property = true;
	
	$html = $module->do_action('get_property_books');
	return $html;
}

// Obtinc html per reserva immoble

function get_user_form ($user_id, $template=false) {

	$module = Module::load('user','user');
	$module->parent = 'GetUser';
	$module->template = $template;
	$module->user_id = $user_id;

	$html = $module->do_action('get_record');
	return $html;
}


/*
 * Funcions de Inmo Property
 * 
 */
 
function count_property_details($details, &$tpl, $loop_vars = false, $exclude = false){

	$details = explode(',',$details);
	$vars = &$tpl->vars;
	$module = &$tpl->module;
	$module_details = array_merge($module->main_details,$module->details);
	
	if ($loop_vars===false) $loop_vars = &$tpl->vars;
	
	if ($exclude){		
		$details = array_diff($module_details, $details);
	}
	
	// conto tots
	$total = 0;
	foreach ($details as $k){	
		if (!empty($loop_vars[$k])){
			$total++;
		}
	}
	
	return $total;
	
}

/**
 * @param $details
 * @param $tpl
 * @param bool|false $loop_vars
 * @param bool|false $exclude Si es add mels mostra tots, però primer els que poso com a $details ( em serveig per controlar l'ordre dels que jo vulgui
 * @param string $type table, ul, p, array ( retorna en forma d'array per posar-ho com convingui )
 * @param int $cols
 * @param string $sep
 * @param string $class
 *
 * @return string | array
 */
function get_property_details($details, &$tpl, $loop_vars = false, $exclude = false, $type='table', $cols = 2, $sep='', $class = 'property-details') {
	
	$details = explode(',',$details);
	$vars = &$tpl->vars;
	$module = &$tpl->module;
	$html = '';
	$module_details = array_merge($module->main_details,$module->details);
	$ret_array = [];
	
	if ($loop_vars===false) $loop_vars = &$tpl->vars;
	
	if ($exclude === 'add'){
		$module_details = array_diff($module_details, $details);
		$details = array_merge($details, $module_details);
	}
	elseif ($exclude){
		$details = array_diff($module_details, $details);
	}
	
	if ($type=='table'){
		$total_cols = $sep?$cols+1:$cols;
		$table = New Table($total_cols,$class);

		$increment = $sep?3:2;
		
		for ($i=1;$i<$total_cols;$i+=$increment)
		$table->set_col($i,'th');
	}
	// conto tots
	$total = 0;
	foreach ($details as $k){	
		if (!empty($loop_vars[$k])){
			$total++;
		}
	}
	
	$conta = 0;
	foreach ($details as $k){	
		if (!empty($loop_vars[$k])){
			$val = 	!empty($loop_vars[$k .'_value'])?$loop_vars[$k .'_value']:$loop_vars[$k];

			$text = $loop_vars[$k];
			
			if ($module->fields[$k]['type']=='checkbox'){		
				if ($type=='table') $text = $module->caption['c_yes'];
				if ($type=='p') $text = '';
				if ($type=='ul') $text = '';
				if ($type=='array') $text = '';
			}
			
			if (!empty($loop_vars[$k .'_area']))
			{
				$text = $loop_vars[$k .'_area'] . ' m<sup>2</sup>';
			}
			
			if ($k=='floor_space')
			{
				$text .= ' m<sup>2</sup>';
			}

			if ($k=='land')
			{
				$text .= ' ' . $loop_vars['land_units'];
			}

			if ($k=='price_m2')
			{
				$text .= ' €';
			}

			if ($k=='efficiency')
			{
				$efficiency_number = $loop_vars['efficiency_number'];
				$efficiency_number_value = $loop_vars['efficiency_number_value'];
				$efficiency2 = $loop_vars['efficiency2'];
				$efficiency_number2 = $loop_vars['efficiency_number2'];
				$efficiency_number2_value = $loop_vars['efficiency_number2_value'];

				if( $efficiency_number_value != '0' ){
					$text.= ' - ' . $efficiency_number . ' kWh/m<sup>2</sup>';
				}
				if( $efficiency_number2_value != '0' ){
					$text .=', ' . $efficiency2 . ' - ' . $efficiency_number2 . ' kg CO<sub>2</sub>/m<sup>2</sup>';
				}
			}

			if ($val) {
				$two_points=$text?': ':'';
				if ($type=='table'){
					$table->add_cell($vars['c_' . $k] . ': ', $k);
					$table->add_cell($text, $k);
					
					// la separacio a la taula es nomes celes del mig
					if ($sep && (($conta+1)%($cols/2)!=0))  $table->add_cell('', 'sep');	
				}
				elseif ($type=='p'){
					$html .= '<p class="'.$k.'"><strong>' . $vars['c_' . $k]. $two_points . '</strong>';
					if ($text) $html .= $text;
					if ($sep && ($conta<$total-1))  $html .= $sep;					
					$html .= '</p>';
				}
				elseif ($type=='ul'){
					$html .= '<li class="'.$k.'"><strong>' . $vars['c_' . $k]. $two_points . '</strong>';
					if ($text) $html .= $text;
					if ($sep && ($conta<$total-1))  $html .= $sep;					
					$html .= '</li>';
				}
				elseif ($type=='array'){
					$ret_array []= [
						'detail_caption' => $vars['c_' . $k],
						'detail_key' => $k,
						'detail_text' => $text,
						'detail_conta' => $conta
					];
				}
			}
			
			$conta++;
		}
	}

	if ($type=='array') return $ret_array;
	if ($type=='table') $html = $table->get_table();
	if ($type=='p') $html = '<div class="'.$class.'">' . $html . '</div>';
	
	return $html;
}

/*
 * Funcions de Page
 * 
 */
// alias per la funció dins la clase Page
function get_page($page_id, $content = false)
{
    return Page::get_page($page_id, $content);
}
// alias per la funció dins la clase Page, per obtindre nomes el camp page_file_name
function get_page_file_name($page_id, $content = false)
{
    return Page::get_page($page_id, $content, true);
}
// alias per la funció dins la clase Page
function get_page_link($page_id)
{
    $ret = Page::get_page($page_id);
	return  isset($ret['p_link'])?$ret['p_link']:false;
}
// per establir noves variables al page desde la plantilla per exemple
function set_vars_page ($vars = array())
{
    global $gl_page;
    $gl_page->tpl->set_vars($vars);
}
// per establir nova variable al page desde la plantilla per exemple
function set_var_page ($var, $value)
{
    global $gl_page;
    $gl_page->tpl->set_var($var, $value);
}
// per obtindre la pàgina actual
function page(){
	return $GLOBALS['gl_page'];
}


/*
 * Funcions de ImageManager
 * 

// alias per la funció dins la clase Image

	* M'encuadra una imatge en unes mides concretes, centra vertical si convé i retorna 
	* O em calcula la mida que em falta si he posat a false una de les 2 mides, ej. vull que faci 100 amplada, calcula amb proporció quina alçada té segons la imatge original
 */
function get_image_attrs($file, $alt, $max_w=false, $max_h=false, $center_vertical = true, $domain = '', $responsive = false)
{
    return ImageManager::get_image_attrs($file, $alt, $max_w, $max_h, $center_vertical, $domain, $responsive);
}

/**
 *
 * @see ImageManager::get_image_cover()
 *
 * @return string
 */
function get_image_cover( $file, $alt, $height_percent )
{
    return ImageManager::get_image_cover( $file, $alt, $height_percent );
}
function get_image_contain( $file, $alt, $height_percent )
{
    return ImageManager::get_image_cover( $file, $alt, $height_percent, true );
}
function is_image_vertical( $file )
{
    return ImageManager::is_image_vertical( $file );
}

// cookies
function can_accept_cookies() {
	return isset($_COOKIE['accept_cookies']);
}
function get_cookies_message( $template = '', $cookies_text = '', $cookies_message = '' ) {
	if (isset($_COOKIE['accept_cookies'])){
		// no faig res ja que si ha tirat endavant es que accepta les cookies
		return '';
	}
	else{
		$c = &$GLOBALS['gl_caption'];
		
		// ja poso la cookie que s'ha acceptat, segons el text si un segueix navegant es que ho accepta
				
		setcookie(
		  "accept_cookies",
		  "1",
		  time() + (10 * 365 * 24 * 60 * 60)
		);
		
		
		
		// mostro missatge per si accepta cookies
		$cookies_message = $cookies_message ? $cookies_message : $c['c_cookies_message'];
		$cookies_text = $cookies_text ? $cookies_text : $c['c_cookies_text'];

		if ( $template ) {
			$file = PATH_TEMPLATES . 'page/' . $template . '_' . LANGUAGE . '.tpl';
			if ( is_file( $file ) ) {
				$cookies_text = file_get_contents( $file );
			}
		}

		return '
			<div id="accept-cookies"><div class="container">
					<span>' . $cookies_message . '</span>
					<a href="#" onclick="$(\'#accept-cookies-more\').slideToggle(function(){$(\'#accept-cookies\').toggleClass(\'active\')});return false;" class="more">' . $c['c_cookies_message_more'] . '</a>
				</div><div class="container">
					<div id="accept-cookies-more">' . $cookies_text . '</div>
					<div class="accept"><a href="#" onclick="$(\'#accept-cookies\').slideUp();return false;">' . $c['c_cookies_message_accept'] . '</a></div>
			</div></div>
		';
		
	}
	
}
function get_list_all_link ($tool, $tool_section){
	$link =
		get_link( array(
			'tool' => $tool,
			'tool_section' => $tool_section,
			'friendly_params' => array('list'=>'all'),
			'detect_page' => true
		));

	return $link;
}

function get_google_iframe( $ignore_company = false, $latitude = false, $longitude = GOOGLE_LONGITUDE, $center_latitude = GOOGLE_CENTER_LATITUDE, $center_longitude = GOOGLE_CENTER_LONGITUDE, $zoom = GOOGLE_ZOOM, $show_point = true ) {

	$is_default = $latitude === false;

	// no ho poso com paràmere per saber si es default
	$latitude = $latitude?$latitude:GOOGLE_LATITUDE;

	$q = $ignore_company || ! COMPANY_NAME ? "$latitude,$longitude" : COMPANY_NAME;

	// Si te place_id i no poso més paràmetres ho centro automàticament
	// Per contacte millor així, menys configuració, simplement s'ha de entrar el google_place_id a all__configpublic
	$center = $is_default && GOOGLE_PLACE_ID ?'': "&amp;center=$center_latitude,$center_longitude";

	// sino puc mostrar el punt, marca l'empresa per la cerca,i si te place-id agafarà place_id
	if (!$show_point) $q = COMPANY_NAME;

	// Per inmobiliaries, ha de ignorar GOOGLE_PLACE_ID si està marcat per mostar el punt
	if ( GOOGLE_PLACE_ID && $is_default || GOOGLE_PLACE_ID && !$show_point ) {

		// Cap porta una coma, per tant si hi ha coma (i punt per assegurar-me), són les coordenades
		$place_id = trim( GOOGLE_PLACE_ID );

		if (strpos($place_id, ',') > 0 && strpos($place_id, '.') > 0){
			$q = urlencode( $place_id );
		}
		else {
			$q = "place_id:" . urlencode( $place_id );
		}
	}


	return "<iframe src='https://www.google.com/maps/embed/v1/place?key=AIzaSyAaIGZEloZFoSY8TuTDyL27MDbuDvW5AoM&amp;q=$q&amp;zoom=$zoom$center'></iframe>";
}

function has_google_recaptcha(){
	return  $GLOBALS['gl_is_local'] || RECAPTCHA_KEY;
}

function get_google_recaptcha() {

	$key    = "6LfA4aQUAAAAAHgdKd5BO0dbhC1JVRo0DNRQ50Nf";

	if ( has_google_recaptcha() ) {
		$recaptcha_key = RECAPTCHA_KEY ?: $key;
		return "<div class=\"g-recaptcha\" data-sitekey =\"$recaptcha_key\"></div >";
	}
}