<?
/**
 * R ( request )
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 10/2012
 * @version $Id$
 * @access public
 */

class R { 

    /**
     * R::id()
     * Obté id i comprova que sigui correcte
     * @param string $var_name
     * @return int
     */ 	
	static public function id($var_name, $default = false, $request='_GET'){	
		
		if ($request!==false){
			$get = &$GLOBALS[$request];
			
			if (!isset($get[$var_name])) return $default;
			
			$id = $get[$var_name];
		}
		// per fer amb un valor directe, ej. en un foreach ($_POST["selected"] as $key)
		else{
			$id = $var_name;
		}
		
		if (!is_numeric(trim($id)))
			Main::error_404('Id no es numeric: ' . $var_name . ' - ' . $id);
		else
			return $id;
	}

    /**
     * R::number()
     * Igual que id però també pot ser cadena buida
     * @param string $var_name
     * @return int
     */ 	
	static public function number($var_name, $default = false, $request='_GET'){	
		
		if ($request!==false){
			$get = &$GLOBALS[$request];
			
			if (!isset($get[$var_name])) return $default;
			
			$id = $get[$var_name];
		}
		// per fer amb un valor directe, ej. en un foreach ($_POST["selected"] as $key)
		else{
			$id = $var_name;
		}
		
		if ($id=='') $id = 0;
		
		if (!is_numeric($id))
			Main::error_404('Request no es numeric: ' . $var_name . ' - ' . $id);
		else
			return $id;
	}

    /**
     * R::text_id()
     * Obté text_id i comprova que sigui correcte ( serveix per tool, tool_section i action, he afegit el guio baix a tots )
     * @param string $var_name
     * @return string
     */ 	
	static public function text_id($var_name, $default = false, $request='_GET'){	

		if ($request!==false) {
			$get = &$GLOBALS[ $request ];

			if ( ! isset( $get[ $var_name ] ) ) return $default;

			$id = $get[ $var_name ];
		}
		// per fer amb un valor directe, ej. en un foreach ($_POST["selected"] as $key)
		else{
			$id = $var_name;
		}
		
		if(!preg_match("/^([A-Za-z_0-9-.]+)$/", $id ) && $id!='') // també deixo passar el que son cadena buida, no fa cap mal i hi ha llocs on ho tinc buit ( el login de products var: process )
			Main::error_404('Text Id no es valid: ' . $var_name . ' - ' . $id);
		else
			return $id;
	} 

    /**
     * R::ids()
     * Obté ds i comprova que sigui correcte
     * @return string file_name
     */ 	
	static public function ids($var_name){	
		
		if (!isset($_GET[$var_name])) return false;
		
		$ids = $_GET[$var_name];
		
		return mysqli_real_escape_string( Db::cn(), $ids );
	}

    /**
     * R::lang()
     * Obté filename i comprova que sigui correcte
     * @param string $var_name
     * @return file_name
     */ 	
	static public function lang($lang){	
				
		if(!preg_match("/^([a-z]{3})$/", $lang ) && $lang!='')
			Main::error_404('No es idioma: ' . $var_name . ' - ' . $lang);
		else
			return $lang;
	} 

    /**
     * R::file_name()
     * Obté filename i comprova que sigui correcte
     * @return file_name
     */ 	
	static public function file_name($var_name = 'page_file_name'){	
		
		if (!isset($_GET[$var_name])) return false;
		
		$page_file_name = $_GET[$var_name];
		
		if (!preg_match("/^([a-z_0-9-]+)$/", $page_file_name ))
			Main::error_404('Page_file_name no es valid: ' . $var_name . ' - ' . $page_file_name);
		else
			return $page_file_name;
	} 

    /**
     * R::escape()
     * Obté un get o post i l'escapa (per substituir Db::qstr, no vull sempre posar '' al retornar el valor )
     * @return string
     */ 	
	static public function escape($var_name, $default = false, $request='_GET'){	
		
		
		if ($request!==false){
			$get = &$GLOBALS[$request];
			
			if (!isset($get[$var_name])) return $default;
			
			$val = $get[$var_name];
		}
		// per fer amb un valor directe, ej. en un foreach ($_POST["selected"] as $key)
		else{
			$val = $var_name;
		}
		
		return mysqli_real_escape_string( Db::cn(), $val );
	}

    /**
     * R::escape_array()
     * Per escapar un array quan es crida-> R::scape_array($array);
     * @return array escapat
     */ 	
	static public function escape_array(&$array){

		if (!is_array($array)) return;
		
		foreach ($array as $k=>&$v) {
			$v = R::escape($v, false, false);
		}
	}

    /**
     * R::get()
     * Obté un get o post i prou ( m'estalvia de comprovar isset
	 * NO ES segura per base de dades, s'ha de fer escape
     * @return string|array()
     */ 	
	static public function get($var_name, $default = false, $request='_GET'){	


		$get = &$GLOBALS[$request];

		if (!isset($get[$var_name])) return $default;

		$val = $get[$var_name];		
		
		return $val;
	}

    /**
     * R::post()
     * Obté un post i prou ( m'estalvia de comprovar isset
	 * NO ES segura per base de dades, s'ha de fer escape
     * @return string
     */ 	
	static public function post($var_name, $default = false){	
		
		return R::get($var_name, $default, '_POST');
		
	}

    /**
     * R::cookie()
     * Obté una cookie i prou ( m'estalvia de comprovar isset
	 * NO ES segura per base de dades, s'ha de fer escape
     * @return string
     */
	static public function cookie($var_name, $default = false){

		return R::get($var_name, $default, '_COOKIE');

	}


	// Retorna true si hi ha la variable, i valor si hi ha valor Ej: id=10
	static function get_opt( $var_name ) {
		if ( php_sapi_name() == 'cli' ) {

			$argv = $_SERVER['argv'];
			$opts = array_slice( $argv, 1 );
			if ( ! $opts ) return false;

			foreach ( $opts as $val ) {
				if ( $val == $var_name ) return true;
				elseif ( strpos( $val, "$var_name=" ) === 0 ) {
					$pair = explode( '=', $val );

					return isset( $pair[1] ) ? $pair[1] : true;
				}
			}

			return false;

		}
		else {
			return R::get( $var_name );
		}
	}

	static public function is_ajax() {
		return isset( $_SERVER['HTTP_X_REQUESTED_WITH'] ) && $_SERVER['HTTP_X_REQUESTED_WITH'] === 'XMLHttpRequest';
	}
}

?>