function calcular_onclick(idioma) {	
	if(idioma == null){
		idioma = 'e';
	}
	
	if(idioma=='c'){
		var me1 = "El valor ha de ser numèric";
		var me2 = "El valor ha de ser numèric";
		var me3 = "No s´autoritzen amortitzacions de més de 40 anys (Dades del Banc d'Espanya)";
		var me4 = "No es permès introduïr valors negatius";
		var me5 = "No es permès introduïr anys negatius";
		var me6 = "Interés màxim 20%!";
		var me7 = "Introduïr valor en el camp 'Import'";
		var me8 = "Introduïr valor en el camp 'Termini'";
		var me9 = "Introduïr valor en el camp 'Interès'";
		var me10 = "Introduïr valor en el camp 'Interès'";
		var me11 = "Introduïr valor en el camp 'Anys'";
		var me12 = "Introduïr valor en el camp 'Capital'";
		var me13 = "resultats";
		var me14 = "Reajust del valor en pesetes!";
	}
	
	if(idioma=='i'){
		var me1 = "Entry should be numeric.";
		var me2 = "Entry should be numeric.";
		var me3 = "Not authorized amortizations more than  40 years (Information from the Banco de España)";
		var me4 = "Entry should be a positive value";
		var me5 = "Entry should be a positive value";
		var me6 = "Maxium interest 20%!";
		var me7 = "Insert value for 'Amount'";
		var me8 = "Insert value for 'Period'";
		var me9 = "Insert value for 'Interest'";
		var me10 = "Insert value for 'Interest'";
		var me11 = "Insert value for 'Years'";
		var me12 = "Insert value for 'Money'";
		var me13 = "results";
		var me14 = "Readjustment of the value in pesetas!";
	}
	
	if(idioma=='f'){
		var me1 = "L'entrée doit être numérique";
		var me2 = "L'entrée doit être numérique";
		var me3 = "Des amortissements ne sont autorisés de plus de 40 ans (Des données du Banco de l'Espagne)";
		var me4 = "Il n'est pas permis d'introduire des valeurs négatives";
		var me5 = "Il n'est pas permis d'introduire des valeurs négatives";
		var me6 = "¡Un intérêt maximal 20%!";
		var me7 = "Introduire une valeur dans 'Montant'";
		var me8 = "Introduire une valeur dans 'Délai'";
		var me9 = "Introduire une valeur dans 'Intérêt'";
		var me10 = "Introduire une valeur dans 'Intérêt'";
		var me11 = "Introduire une valeur dans 'Années'";
		var me12 = "Introduire une valeur dans 'Capital'";
		var me13 = "résultats";
		var me14 = "¡Un rajustement de la valeur dans des pesetas!";
	}
	
	if(idioma=='a'){
		var me1 = "Die Eingabe muss numerisch sein";
		var me2 = "Die Eingabe muss numerisch sein";
		var me3 = "Not authorized amortizations more than  40 years (Information from the Banco de España)";
		var me4 = "Entry should be a positive value";
		var me5 = "Entry should be a positive value";
		var me6 = "Maxium interest 20%!";
		var me7 = "Insert value for 'Import'";
		var me8 = "Insert value for 'Term'";
		var me9 = "Insert value for 'Interest'";
		var me10 = "Insert value for 'Interest'";
		var me11 = "Insert value for 'Years'";
		var me12 = "Insert value for 'Money'";
		var me13 = "results";
		var me14 = "Readjustment of the value in pesetas!";
	}
	
	if(idioma=='t'){
		var me1 = "Il valore introdotto deve essere numerico.";
		var me2 = "Il valore introdotto deve essere numerico.";
		var me3 = "Not authorized amortizations more than  40 years (Information from the Banco de España)";
		var me4 = "Entry should be a positive value";
		var me5 = "Entry should be a positive value";
		var me6 = "Maxium interest 20%!";
		var me7 = "Insert value for 'Import'";
		var me8 = "Insert value for 'Term'";
		var me9 = "Insert value for 'Interest'";
		var me10 = "Insert value for 'Interest'";
		var me11 = "Insert value for 'Years'";
		var me12 = "Insert value for 'Money'";
		var me13 = "results";
		var me14 = "Readjustment of the value in pesetas!";
	}
	
	if(idioma=='e'){
		var me1 = "El valor debe ser numérico";
		var me2 = "El valor debe ser numérico";
		var me3 = "No se autorizan amortizaciones de más de 40 años (Datos del Banco de España)";
		var me4 = "No se permite introducir valores negativos";
		var me5 = "No se permite introducir años negativos";
		var me6 = "¡Interés máximo 20%!";
		var me7 = "Introducir valor en el campo 'Importe'";
		var me8 = "Introducir valor en el campo 'Plazo'";
		var me9 = "Introducir valor el campo 'Interés'";
		var me10 = "Introducir valor el campo 'Interés'";
		var me11 = "Introducir valor el campo 'Años'";
		var me12 = "Introducir valor el campo 'Capital'";
		var me13 = "resultados";
		var me14 = "¡Reajuste del valor en pesetas!";
	}
	

		var valor1 = Delete_miles(document.formAhorro.importeh.value);

		if (!EsNumerico(document.formAhorro.anos.value)){
		alert (me1); //me1
		document.formAhorro.anos.focus();
		return;}

		if (!EsNumerico(document.formAhorro.interes.value)){
		alert (me2); // me2
		document.formAhorro.interes.focus();
		return;}

		if (document.formAhorro.anos.value > 40)
		{
			alert(me3); // me3
			document.formAhorro.anos.focus();
			//document.formAhorro.anos.value = 40;
			//return false;
		}
		if (document.formAhorro.interes.value < 0){
				alert (me4); // me4
				document.formAhorro.interes.focus();
				return; }
		if (document.formAhorro.anos.value < 0){
				alert (me5); //me5
				document.formAhorro.anos.focus();
				return; }
		if (document.formAhorro.interes.value > 20)
		{
		alert (me6); //me6
		document.formAhorro.interes.value = "";
		document.formAhorro.interes.focus();
		//document.formAhorro.interes.value = 20;
		//return false;
		}
		var suma;
		var totalsuma;

	if (document.formAhorro.importeh.value == "" || document.formAhorro.importeh.value == 0 )
	{ alert(me7) //me7
	  document.formAhorro.importeh.focus()
	  return}
	if ( isNaN(document.formAhorro.anos.value) || document.formAhorro.anos.value == 0 )
	{ alert(me8) //me8
	  document.formAhorro.anos.focus()
	  return}
	if ( isNaN(document.formAhorro.interes.value) || document.formAhorro.interes.value == 0.0 )
	{ alert(me9) //me9
	  document.formAhorro.interes.focus()
	  return}


		CalculaAhorro();
		document.formAhorroRes.iprimera.value = Formato_numero(Math.round(document.formAhorroRes.iprimera.value));
		document.formAhorroRes.itotal.value = Formato_numero(Math.round(document.formAhorroRes.itotal.value));
		document.formAhorroRes.dcprimera.value = Formato_numero(Math.round(document.formAhorroRes.dcprimera.value));
		document.formAhorroRes.dctotal.value = Formato_numero(Math.round(document.formAhorroRes.dctotal.value));
		document.formAhorroRes.diprimera.value = Formato_numero(Math.round(document.formAhorroRes.diprimera.value));
		document.formAhorroRes.ditotal.value = Formato_numero(Math.round(document.formAhorroRes.ditotal.value));


        //Formato en comision, tasacion, notario, gestión, registro y iva.
        //document.formAhorroRes.comision.value = Formato_numero(document.formAhorroRes.comision.value);
        //document.formAhorroRes.tasacion.value = Formato_numero(document.formAhorroRes.tasacion.value);
        //document.formAhorroRes.notario.value = Formato_numero(document.formAhorroRes.notario.value);
        //document.formAhorroRes.gestion.value = Formato_numero(document.formAhorroRes.gestion.value);
        //document.formAhorroRes.registro.value = Formato_numero(document.formAhorroRes.registro.value);
}

function CalculaCuota()
{
	var importeq;
	var entra;
	var imp;
	var plazomeses;
	var tipo_mensual;
	var res;
	var intetemp;
	var capinitemp;
	var plazotemp;
	var mensutemp;
	var dato;
	var indi;
	var deci;
	
	importeq = parseInt(Delete_miles(document.formAhorro.importeh.value));
	entra = 0;
	if (entra == 0)
		imp = (importeq * 1.1)
	else
		imp = (importeq) - entra
		plazomeses = parseInt(document.formAhorro.anos.value)
		tipo_mensual = parseFloat(document.formAhorro.interes.value)
		plazomeses = plazomeses * 12.0
		tipo_mensual= tipo_mensual / 1200
		res = importeq * ((Math.pow( 1 + tipo_mensual, plazomeses) * tipo_mensual) / ( Math.pow( 1 + tipo_mensual, plazomeses) -1)) //Formula calculo
		intetemp =tipo_mensual
		capinitemp = importeq
		plazotemp = plazomeses
		mensutemp = res
		dato = res + ""
		indi = dato.indexOf(".")
	if (dato!=-1)
	{
		deci = dato.substring( indi + 1, indi + 3);
		if (parseInt(deci)>=50)
		{
			res=Math.ceil(res)
		}
		else
		{
			res=parseInt(res)
		}
	}
	
	return res;
}

function CalculaAhorro()
{
	var cuota;
	
	cuota = CalculaCuota();
	document.formAhorro.cuota.value = Math.round(cuota)
	//fin del calculo de cuota mensual
	
	//calculo del ahorro fiscal
	var CA;
	var CA2;
	var primero;
	var segundo;
	var tercero;
	var suma;
	var plazo;
	var sacar;
	var suma2;
	var i;
	
	//Pasamos el valor total de cuotas en un año.
	CA = document.formAhorro.cuota.value * 12;
	CA2 = CA;
	//Recuperamos el valor de los plazos solicitados, en años
	plazo = parseInt(document.formAhorro.anos.value);
	
	//Si operamos con Euros
	if (document.formAhorro.radio[0].checked)
	{
		if (parseInt(CA) > 1500000)
		{
			CA = 1500000;
		}
		
		tercero = 0;
		
		if (parseInt(CA) > 750000)
		{
			segundo = ((CA - 750000) * 0.15) + 187500;
			tercero = ((CA - 750000) * 0.15) + 150000;
			sacar = tercero;
			primero = segundo;
			for (i=3;i<plazo;i++)
			{
				tercero = tercero + ((CA - 750000) * 0.15) + 150000;
			}
			suma = primero + segundo + tercero;
		}
		else
		{
			segundo = parseInt(CA) * 0.25;
			tercero = parseInt(CA) * 0.2;
			sacar = tercero;primero = segundo;
			for (i=3;i<plazo;i++)
			{
				tercero = tercero + parseInt(CA) * 0.2;
			}
			
			suma = primero + segundo + tercero;
		}
		
		document.formAhorroRes.iprimera.value = primero;
		document.formAhorroRes.dcprimera.value = primero;
		document.formAhorroRes.itotal.value = suma;
		document.formAhorroRes.dctotal.value = suma;
		tercero = 0;
	
		CA2 = CA2 / 2;
		
		if (parseInt(CA2) > 1500000)
		{
			CA2 = 1500000;
		}
			if (parseInt(CA2) > 750000)
			{
				segundo = (((CA2 - 750000) * 0.15) + 187500) * 2;
				tercero = (((CA2 - 750000) * 0.15) + 150000) * 2;
				sacar = tercero;
				primero = segundo;
				
				for (i=3;i<plazo;i++)
				{
					tercero = tercero + (((CA2 - 750000) * 0.15) + 150000) * 2;
				}
				
				suma2 = primero + segundo + tercero;
			}
			else
			{
				segundo = (parseInt(CA2) * 0.25) * 2;tercero = (parseInt(CA2) * 0.2) * 2;
				sacar = tercero;primero = segundo;
				
				for (i=3;i<plazo;i++)
				{
					tercero = tercero + (parseInt(CA2) * 0.2) * 2;
				}
				
				suma2 = primero + segundo + tercero;
			}	
			
			document.formAhorroRes.ditotal.value = suma2;
			document.formAhorroRes.diprimera.value = primero;
			
			//Validacion de resultados. Si supera el millon y medio, sera siempre 300.000
			plazo = document.formAhorro.anos.value;
			
			if (parseInt(document.formAhorroRes.iprimera.value) > 1500000 )
			{
				document.formAhorroRes.iprimera.value = 300000;
				document.formAhorroRes.dcprimera.value = 300000;
				document.formAhorroRes.diprimera.value = 600000;
				if (plazo < 2)
				{
					document.formAhorroRes.itotal.value = 300000;
					document.formAhorroRes.dctotal.value = 300000;
					document.formAhorroRes.ditotal.value = 600000;
				}
				if (plazo == 2)
				{
					document.formAhorroRes.itotal.value = 600000;
					document.formAhorroRes.dctotal.value = 600000;
					document.formAhorroRes.ditotal.value = 1200000;
				}
				if (parseInt(plazo) > 2)
				{
					document.formAhorroRes.itotal.value = 600000;
					document.formAhorroRes.dctotal.value = 600000;
					document.formAhorroRes.ditotal.value = 1200000;
					var contador;
					contador = document.formAhorroRes.itotal.value;
					var h;
					for (h=2;h<plazo;h++)
					{
						contador = parseInt(contador) + 262500;
					}
					document.formAhorroRes.itotal.value = contador;
					document.formAhorroRes.dctotal.value = contador;
					document.formAhorroRes.diprimera.value = parseInt(document.formAhorroRes.iprimera.value) * 2;
					document.formAhorroRes.ditotal.value = contador * 2;
				}
			}
		}
		else
		{
			if (parseInt(CA) > 9015.1815)
			{
				CA = 9015.1815;
			}
			tercero = 0;
			if (parseInt(CA) > 4507.59)
			{
				segundo = ((CA - 4507.59) * 0.15) + 1126.8976;
				tercero = ((CA - 4507.59) * 0.15) + 901.518;
				sacar = tercero;
				primero = segundo;
				for (i=3;i<plazo;i++)
				{
					tercero = tercero + ((CA - 4507.59) * 0.15) + 901.518;
				}
				suma = primero + segundo + tercero;
			}
			else
			{
				segundo = parseInt(CA) * 0.25;
				tercero = parseInt(CA) * 0.2;
				sacar = tercero;
				primero = segundo;
				for (i=3;i<plazo;i++)
				{
					tercero = tercero + parseInt(CA) * 0.2;
				}
				suma = primero + segundo + tercero;
			}			
				document.formAhorroRes.iprimera.value = primero;
				document.formAhorroRes.dcprimera.value = primero;
				document.formAhorroRes.itotal.value = suma;
				document.formAhorroRes.dctotal.value = suma;
				tercero = 0;
				CA2 = CA2 / 2;
				if (parseInt(CA2) > 9015.1815)
				{
					CA2 = 9015.1815;
				}
				if (parseInt(CA2) > 4507.59)
				{
					segundo = (((CA2 - 4507.59) * 0.15) + 1126.8976) * 2;
					tercero = (((CA2 - 4507.59) * 0.15) + 901.518) * 2;
					sacar = tercero;
					primero = segundo;
					for (i=3;i<plazo;i++)
					{
						tercero = tercero + (((CA2 - 4507.59) * 0.15) + 901.518) * 2;
					}
					suma2 = primero + segundo + tercero;
				}
				else
				{
					segundo = (parseInt(CA2) * 0.25) * 2;
					tercero = (parseInt(CA2) * 0.2) * 2;
					sacar = tercero;
					primero = segundo;
					for (i=3;i<plazo;i++)
					{
						tercero = tercero + (parseInt(CA2) * 0.2) * 2;
					}
					suma2 = primero + segundo + tercero;
				}			
				document.formAhorroRes.ditotal.value = suma2;
				document.formAhorroRes.diprimera.value = primero;

				//Validacion de resultados. Si supera el millon y medio, sera siempre 300.000
				plazo = document.formAhorro.anos.value;
			if (parseInt(document.formAhorroRes.iprimera.value) > 9015 )
			{
				document.formAhorroRes.iprimera.value = 1803.0363;
				document.formAhorroRes.dcprimera.value = 1803.0363;
				document.formAhorroRes.diprimera.value = 3606.0726;
				if (plazo < 2)
				{
					document.formAhorroRes.itotal.value = 1803.0363;
					document.formAhorroRes.dctotal.value = 1803.0363;
					document.formAhorroRes.ditotal.value = 3606.0726;
				}
				if (plazo == 2)
				{
					document.formAhorroRes.itotal.value = 3606.0726;
					document.formAhorroRes.dctotal.value = 3606.0726;
					document.formAhorroRes.ditotal.value = 7212.1452;
				}
				if (plazo > 2)
				{
					document.formAhorroRes.itotal.value = 3606.0726;
					document.formAhorroRes.dctotal.value = 3606.0726;
					document.formAhorroRes.ditotal.value = 7212.1452;
					var contador;
					contador = document.formAhorroRes.itotal.value;
					var h;
					for (h=2;h<plazo;h++)
					{
						contador = parseInt(contador) + 1577.6567;
					}
					document.formAhorroRes.itotal.value = contador;
					document.formAhorroRes.dctotal.value = contador;
					document.formAhorroRes.diprimera.value = parseInt(document.formAhorroRes.iprimera.value) * 2;
					document.formAhorroRes.ditotal.value = contador * 2;
				}
		}
	}
}

function CleanThis(Tipo)
{
	if (Tipo=="dinero")
		document.formAhorro.importeh.value="";
	else if(Tipo=="Apertura")
		document.hipoteca.MinApert.value="";
}


function euro(idioma) {
	
		if(idioma == null){
	idioma = 'e';
	}
	
	if(idioma =='c'){
		var me14 = "Reajust del valor en euros!";
	}
	
	if(idioma =='i'){
		var me14 = "Readjustment of the value in euros!";
	}
	
	if(idioma =='f'){
		var me14 = "¡Un rajustement de la valeur dans des euros!";
	}
	
	if(idioma =='a'){
		var me14 = "Readjustment of the value in euros!";
	}
	
	if(idioma =='t'){
		var me14 = "Readjustment of the value in euros!";
	}
	
	if(idioma =='e'){
		var me14 = "¡Reajuste del valor en euros!";
	}
	
	if (!(document.formAhorro.importeh.value == "")){
		var valor_actual;
		var res;
		var flag_aviso;
		var valor1;
	
		valor1=Delete_miles(document.formAhorro.importeh.value);
	
		valor_actual = valor1 / 166.386;
		document.formAhorro.importeh.value = Math.round(valor_actual);
		document.formAhorro.importeh.value = Formato_numero(document.formAhorro.importeh.value);
		alert (me14); //me14
		calcular_onclick(idioma);
	}
}

function pts(idioma) {
	
	if(idioma == null){
	idioma = 'e';
	}
	
	if(idioma =='c'){
		var me14 = "Reajust del valor en pessetes!";
	}
	
	if(idioma =='i'){
		var me14 = "Readjustment of the value in pesetas!";
	}
	
	if(idioma =='f'){
		var me14 = "¡Un rajustement de la valeur dans des pesetas!";
	}
	
	if(idioma =='a'){
		var me14 = "Readjustment of the value in pesetas!";
	}
	
	if(idioma =='t'){
		var me14 = "Readjustment of the value in pesetas!";
	}
	
	if(idioma =='e'){
		var me14 = "¡Reajuste del valor en pesetas!";
	}
	
	if (!(document.formAhorro.importeh.value == "")){
		var valor_actual;
		var res;
		var flag_aviso;
		var valor1;
	
		valor1=Delete_miles(document.formAhorro.importeh.value);
	
		valor_actual = valor1 * 166.386;
		document.formAhorro.importeh.value = Math.round(valor_actual);
		document.formAhorro.importeh.value = Formato_numero(document.formAhorro.importeh.value);
		valor_actual = 0;
		alert (me14); //me14
		calcular_onclick(idioma);
	}
}