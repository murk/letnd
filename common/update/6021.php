<?php

/* 18-12-14 
  
 * Immo: camp temporal per preus de lloguer temporada

 */

	
	
if (is_table('inmo__property') && !(is_field('inmo__property','price_tmp'))){
	
	$update_sql .= "
	ALTER TABLE `inmo__property`
	ADD COLUMN `price_tmp` DECIMAL(8,2) NOT NULL DEFAULT '0' AFTER `price_private`;
	";	
	
}
else{
	if (is_table('inmo__property')) {		
		trigger_error('Ja existeixen el field inmo__property.price_tmp');
	}
}
	
?>