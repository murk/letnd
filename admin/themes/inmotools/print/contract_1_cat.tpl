{\rtf1\ansi\deff0\adeflang1025
{\fonttbl{\f0\froman\fprq2\fcharset0 Times New Roman;}{\f1\froman\fprq2\fcharset0 Times New Roman;}{\f2\fswiss\fprq2\fcharset0 Arial;}{\f3\froman\fprq2\fcharset0 Times New Roman;}{\f4\fswiss\fprq0\fcharset0 Arial;}{\f5\fswiss\fprq2\fcharset128 Arial;}{\f6\fscript\fprq0\fcharset0 Comic Sans MS;}{\f7\fnil\fprq2\fcharset0 MS Mincho;}{\f8\fswiss\fprq2\fcharset0 Lucida Sans Unicode;}{\f9\fnil\fprq2\fcharset0 MS Mincho;}{\f10\fswiss\fprq2\fcharset0 Arial Unicode MS;}{\f11\fnil\fprq2\fcharset0 Tahoma;}{\f12\fnil\fprq0\fcharset0 Tahoma;}}
{\colortbl;\red0\green0\blue0;\red128\green128\blue128;}
{\stylesheet{\s1\rtlch\afs24\lang255\ltrch\dbch\af8\langfe255\hich\fs24\lang1034\loch\fs24\lang1034\snext1 Normal;}
{\s2\sb240\sa120\keepn\rtlch\af11\afs28\lang255\ltrch\dbch\af7\langfe255\hich\f2\fs28\lang1034\loch\f2\fs28\lang1034\sbasedon1\snext3 Heading;}
{\s3\sa120\rtlch\afs24\lang255\ltrch\dbch\af8\langfe255\hich\fs24\lang1034\loch\fs24\lang1034\sbasedon1\snext3 Body Text;}
{\s4\sa120\rtlch\afs24\lang255\ltrch\dbch\af8\langfe255\hich\fs24\lang1034\loch\fs24\lang1034\sbasedon3\snext4 List;}
{\s5\sb120\sa120\rtlch\af12\afs24\lang255\ai\ltrch\dbch\af8\langfe255\hich\fs24\lang1034\i\loch\fs24\lang1034\i\sbasedon1\snext5 caption;}
{\s6\rtlch\af12\afs24\lang255\ltrch\dbch\af8\langfe255\hich\fs24\lang1034\loch\fs24\lang1034\sbasedon1\snext6 Index;}
{\s7\sb240\sa120\keepn\rtlch\af2\afs28\lang255\ltrch\dbch\af9\langfe255\hich\f2\fs28\lang1034\loch\f2\fs28\lang1034\sbasedon1\snext3 Heading;}
{\s8\sb120\sa120\rtlch\afs24\lang255\ai\ltrch\dbch\af8\langfe255\hich\fs24\lang1034\i\loch\fs24\lang1034\i\sbasedon1\snext8 caption;}
{\s9\rtlch\afs24\lang255\ltrch\dbch\af8\langfe255\hich\fs24\lang1034\loch\fs24\lang1034\sbasedon1\snext9 Index;}
{\s10\sb240\sa120\keepn\rtlch\af2\afs28\lang255\ltrch\dbch\af9\langfe255\hich\f2\fs28\lang1034\loch\f2\fs28\lang1034\sbasedon1\snext3 WW-Heading;}
{\s11\sb120\sa120\rtlch\afs24\lang255\ai\ltrch\dbch\af8\langfe255\hich\fs24\lang1034\i\loch\fs24\lang1034\i\sbasedon1\snext11 WW-caption;}
{\s12\rtlch\afs24\lang255\ltrch\dbch\af8\langfe255\hich\fs24\lang1034\loch\fs24\lang1034\sbasedon1\snext12 WW-Index;}
{\s13\sb240\sa120\keepn\rtlch\af2\afs28\lang255\ltrch\dbch\af9\langfe255\hich\f2\fs28\lang1034\loch\f2\fs28\lang1034\sbasedon1\snext3 WW-Heading1;}
{\s14\sb120\sa120\rtlch\afs24\lang255\ai\ltrch\dbch\af8\langfe255\hich\fs24\lang1034\i\loch\fs24\lang1034\i\sbasedon1\snext14 WW-caption1;}
{\s15\rtlch\afs24\lang255\ltrch\dbch\af8\langfe255\hich\fs24\lang1034\loch\fs24\lang1034\sbasedon1\snext15 WW-Index1;}
{\s16\sb240\sa120\keepn\rtlch\af2\afs28\lang255\ltrch\dbch\af9\langfe255\hich\f2\fs28\lang1034\loch\f2\fs28\lang1034\sbasedon1\snext3 WW-Heading11;}
{\s17\sb120\sa120\rtlch\afs24\lang255\ai\ltrch\dbch\af8\langfe255\hich\fs24\lang1034\i\loch\fs24\lang1034\i\sbasedon1\snext17 WW-caption11;}
{\s18\rtlch\afs24\lang255\ltrch\dbch\af8\langfe255\hich\fs24\lang1034\loch\fs24\lang1034\sbasedon1\snext18 WW-Index11;}
{\s19\sb240\sa120\keepn\rtlch\af2\afs28\lang255\ltrch\dbch\af9\langfe255\hich\f2\fs28\lang1034\loch\f2\fs28\lang1034\sbasedon1\snext3 WW-Heading111;}
{\s20\sb120\sa120\rtlch\afs24\lang255\ai\ltrch\dbch\af8\langfe255\hich\fs24\lang1034\i\loch\fs24\lang1034\i\sbasedon1\snext20 WW-caption111;}
{\s21\rtlch\afs24\lang255\ltrch\dbch\af8\langfe255\hich\fs24\lang1034\loch\fs24\lang1034\sbasedon1\snext21 WW-Index111;}
{\s22\sb240\sa120\keepn\rtlch\af2\afs28\lang255\ltrch\dbch\af9\langfe255\hich\f2\fs28\lang1034\loch\f2\fs28\lang1034\sbasedon1\snext3 WW-Heading1111;}
{\s23\sb120\sa120\rtlch\afs24\lang255\ai\ltrch\dbch\af8\langfe255\hich\fs24\lang1034\i\loch\fs24\lang1034\i\sbasedon1\snext23 WW-caption1111;}
{\s24\rtlch\afs24\lang255\ltrch\dbch\af8\langfe255\hich\fs24\lang1034\loch\fs24\lang1034\sbasedon1\snext24 WW-Index1111;}
{\s25\sb240\sa120\keepn\rtlch\af2\afs28\lang255\ltrch\dbch\af9\langfe255\hich\f2\fs28\lang1034\loch\f2\fs28\lang1034\sbasedon1\snext3 WW-Heading11111;}
{\s26\sb120\sa120\rtlch\afs24\lang255\ai\ltrch\dbch\af8\langfe255\hich\fs24\lang1034\i\loch\fs24\lang1034\i\sbasedon1\snext26 WW-caption11111;}
{\s27\rtlch\afs24\lang255\ltrch\dbch\af8\langfe255\hich\fs24\lang1034\loch\fs24\lang1034\sbasedon1\snext27 WW-Index11111;}
{\s28\sb240\sa120\keepn\rtlch\af2\afs28\lang255\ltrch\dbch\af9\langfe255\hich\f2\fs28\lang1034\loch\f2\fs28\lang1034\sbasedon1\snext3 WW-Heading111111;}
{\s29\sb120\sa120\rtlch\afs24\lang255\ai\ltrch\dbch\af8\langfe255\hich\fs24\lang1034\i\loch\fs24\lang1034\i\sbasedon1\snext29 WW-caption111111;}
{\s30\rtlch\afs24\lang255\ltrch\dbch\af8\langfe255\hich\fs24\lang1034\loch\fs24\lang1034\sbasedon1\snext30 WW-Index111111;}
{\s31\sb240\sa120\keepn\rtlch\af2\afs28\lang255\ltrch\dbch\af9\langfe255\hich\f2\fs28\lang1034\loch\f2\fs28\lang1034\sbasedon1\snext3 WW-Heading1111111;}
{\s32\sb120\sa120\rtlch\afs24\lang255\ai\ltrch\dbch\af8\langfe255\hich\fs24\lang1034\i\loch\fs24\lang1034\i\sbasedon1\snext32 WW-caption1111111;}
{\s33\rtlch\afs24\lang255\ltrch\dbch\af8\langfe255\hich\fs24\lang1034\loch\fs24\lang1034\sbasedon1\snext33 WW-Index1111111;}
{\s34\sb240\sa120\keepn\rtlch\af2\afs28\lang255\ltrch\dbch\af9\langfe255\hich\f2\fs28\lang1034\loch\f2\fs28\lang1034\sbasedon1\snext3 WW-Heading11111111;}
{\s35\sb120\sa120\rtlch\afs24\lang255\ai\ltrch\dbch\af8\langfe255\hich\fs24\lang1034\i\loch\fs24\lang1034\i\sbasedon1\snext35 WW-caption11111111;}
{\s36\rtlch\afs24\lang255\ltrch\dbch\af8\langfe255\hich\fs24\lang1034\loch\fs24\lang1034\sbasedon1\snext36 WW-Index11111111;}
{\s37\sb240\sa120\keepn\rtlch\af2\afs28\lang255\ltrch\dbch\af9\langfe255\hich\f2\fs28\lang1034\loch\f2\fs28\lang1034\sbasedon1\snext3 WW-Heading111111111;}
{\s38\sb120\sa120\rtlch\afs24\lang255\ai\ltrch\dbch\af8\langfe255\hich\fs24\lang1034\i\loch\fs24\lang1034\i\sbasedon1\snext38 WW-caption111111111;}
{\s39\rtlch\afs24\lang255\ltrch\dbch\af8\langfe255\hich\fs24\lang1034\loch\fs24\lang1034\sbasedon1\snext39 WW-Index111111111;}
{\s40\sb240\sa120\keepn\rtlch\af2\afs28\lang255\ltrch\dbch\af9\langfe255\hich\f2\fs28\lang1034\loch\f2\fs28\lang1034\sbasedon1\snext3 WW-Heading1111111111;}
{\s41\sb120\sa120\rtlch\afs24\lang255\ai\ltrch\dbch\af8\langfe255\hich\fs24\lang1034\i\loch\fs24\lang1034\i\sbasedon1\snext41 WW-caption1111111111;}
{\s42\rtlch\afs24\lang255\ltrch\dbch\af8\langfe255\hich\fs24\lang1034\loch\fs24\lang1034\sbasedon1\snext42 WW-Index1111111111;}
{\s43\sb240\sa120\keepn\rtlch\af2\afs28\lang255\ltrch\dbch\af9\langfe255\hich\f2\fs28\lang1034\loch\f2\fs28\lang1034\sbasedon1\snext3 WW-Heading11111111111;}
{\s44\sb120\sa120\rtlch\afs24\lang255\ai\ltrch\dbch\af8\langfe255\hich\fs24\lang1034\i\loch\fs24\lang1034\i\sbasedon1\snext44 WW-caption11111111111;}
{\s45\rtlch\afs24\lang255\ltrch\dbch\af8\langfe255\hich\fs24\lang1034\loch\fs24\lang1034\sbasedon1\snext45 WW-Index11111111111;}
{\s46\sb240\sa120\keepn\rtlch\af2\afs28\lang255\ltrch\dbch\af9\langfe255\hich\f2\fs28\lang1034\loch\f2\fs28\lang1034\sbasedon1\snext3 WW-Heading111111111111;}
{\s47\sb120\sa120\rtlch\afs24\lang255\ai\ltrch\dbch\af8\langfe255\hich\fs24\lang1034\i\loch\fs24\lang1034\i\sbasedon1\snext47 WW-caption111111111111;}
{\s48\rtlch\afs24\lang255\ltrch\dbch\af8\langfe255\hich\fs24\lang1034\loch\fs24\lang1034\sbasedon1\snext48 WW-Index111111111111;}
{\s49\sb240\sa120\keepn\rtlch\af2\afs28\lang255\ltrch\dbch\af9\langfe255\hich\f2\fs28\lang1034\loch\f2\fs28\lang1034\sbasedon1\snext3 WW-Heading1111111111111;}
{\s50\sb120\sa120\rtlch\afs24\lang255\ai\ltrch\dbch\af8\langfe255\hich\fs24\lang1034\i\loch\fs24\lang1034\i\sbasedon1\snext50 WW-caption1111111111111;}
{\s51\rtlch\afs24\lang255\ltrch\dbch\af8\langfe255\hich\fs24\lang1034\loch\fs24\lang1034\sbasedon1\snext51 WW-Index1111111111111;}
{\s52\sb240\sa120\keepn\rtlch\af2\afs28\lang255\ltrch\dbch\af9\langfe255\hich\f2\fs28\lang1034\loch\f2\fs28\lang1034\sbasedon1\snext3 WW-Heading11111111111111;}
{\s53\sb120\sa120\rtlch\afs24\lang255\ai\ltrch\dbch\af8\langfe255\hich\fs24\lang1034\i\loch\fs24\lang1034\i\sbasedon1\snext53 WW-caption11111111111111;}
{\s54\rtlch\afs24\lang255\ltrch\dbch\af8\langfe255\hich\fs24\lang1034\loch\fs24\lang1034\sbasedon1\snext54 WW-Index11111111111111;}
{\s55\sb240\sa120\keepn\rtlch\af2\afs28\lang255\ltrch\dbch\af9\langfe255\hich\f2\fs28\lang1034\loch\f2\fs28\lang1034\sbasedon1\snext3 WW-Heading111111111111111;}
{\s56\sb120\sa120\rtlch\afs24\lang255\ai\ltrch\dbch\af8\langfe255\hich\fs24\lang1034\i\loch\fs24\lang1034\i\sbasedon1\snext56 WW-caption111111111111111;}
{\s57\rtlch\afs24\lang255\ltrch\dbch\af8\langfe255\hich\fs24\lang1034\loch\fs24\lang1034\sbasedon1\snext57 WW-Index111111111111111;}
{\s58\sb240\sa120\keepn\rtlch\af2\afs28\lang255\ltrch\dbch\af9\langfe255\hich\f2\fs28\lang1034\loch\f2\fs28\lang1034\sbasedon1\snext3 WW-Heading1111111111111111;}
{\s59\sb120\sa120\rtlch\afs24\lang255\ai\ltrch\dbch\af8\langfe255\hich\fs24\lang1034\i\loch\fs24\lang1034\i\sbasedon1\snext59 WW-caption1111111111111111;}
{\s60\rtlch\afs24\lang255\ltrch\dbch\af8\langfe255\hich\fs24\lang1034\loch\fs24\lang1034\sbasedon1\snext60 WW-Index1111111111111111;}
{\s61\sb240\sa120\keepn\rtlch\af2\afs28\lang255\ltrch\dbch\af9\langfe255\hich\f2\fs28\lang1034\loch\f2\fs28\lang1034\sbasedon1\snext3 WW-Heading11111111111111111;}
{\s62\sb120\sa120\rtlch\afs24\lang255\ai\ltrch\dbch\af8\langfe255\hich\fs24\lang1034\i\loch\fs24\lang1034\i\sbasedon1\snext62 WW-caption11111111111111111;}
{\s63\rtlch\afs24\lang255\ltrch\dbch\af8\langfe255\hich\fs24\lang1034\loch\fs24\lang1034\sbasedon1\snext63 WW-Index11111111111111111;}
{\s64\sb240\sa120\keepn\rtlch\af2\afs28\lang255\ltrch\dbch\af9\langfe255\hich\f2\fs28\lang1034\loch\f2\fs28\lang1034\sbasedon1\snext3 WW-Heading111111111111111111;}
{\s65\sb120\sa120\rtlch\afs24\lang255\ai\ltrch\dbch\af8\langfe255\hich\fs24\lang1034\i\loch\fs24\lang1034\i\sbasedon1\snext65 WW-caption111111111111111111;}
{\s66\rtlch\afs24\lang255\ltrch\dbch\af8\langfe255\hich\fs24\lang1034\loch\fs24\lang1034\sbasedon1\snext66 WW-Index111111111111111111;}
{\s67\sb240\sa120\keepn\rtlch\af2\afs28\lang255\ltrch\dbch\af9\langfe255\hich\f2\fs28\lang1034\loch\f2\fs28\lang1034\sbasedon1\snext3 WW-Heading1111111111111111111;}
{\s68\sb120\sa120\rtlch\afs24\lang255\ai\ltrch\dbch\af8\langfe255\hich\fs24\lang1034\i\loch\fs24\lang1034\i\sbasedon1\snext68 WW-caption1111111111111111111;}
{\s69\rtlch\afs24\lang255\ltrch\dbch\af8\langfe255\hich\fs24\lang1034\loch\fs24\lang1034\sbasedon1\snext69 WW-Index1111111111111111111;}
{\s70\sb240\sa120\keepn\rtlch\af2\afs28\lang255\ltrch\dbch\af9\langfe255\hich\f2\fs28\lang1034\loch\f2\fs28\lang1034\sbasedon1\snext3 WW-Heading11111111111111111111;}
{\s71\sb120\sa120\rtlch\afs24\lang255\ai\ltrch\dbch\af8\langfe255\hich\fs24\lang1034\i\loch\fs24\lang1034\i\sbasedon1\snext71 WW-caption11111111111111111111;}
{\s72\rtlch\afs24\lang255\ltrch\dbch\af8\langfe255\hich\fs24\lang1034\loch\fs24\lang1034\sbasedon1\snext72 WW-Index11111111111111111111;}
{\s73\sb240\sa120\keepn\rtlch\af2\afs28\lang255\ltrch\dbch\af9\langfe255\hich\f2\fs28\lang1034\loch\f2\fs28\lang1034\sbasedon1\snext3 WW-Heading111111111111111111111;}
{\s74\sb120\sa120\rtlch\afs24\lang255\ai\ltrch\dbch\af8\langfe255\hich\fs24\lang1034\i\loch\fs24\lang1034\i\sbasedon1\snext74 WW-caption111111111111111111111;}
{\s75\rtlch\afs24\lang255\ltrch\dbch\af8\langfe255\hich\fs24\lang1034\loch\fs24\lang1034\sbasedon1\snext75 WW-Index111111111111111111111;}
{\s76\sb240\sa120\keepn\rtlch\af2\afs28\lang255\ltrch\dbch\af8\langfe255\hich\f2\fs28\lang1034\loch\f2\fs28\lang1034\sbasedon1\snext3 WW-Heading1111111111111111111111;}
{\s77\sb120\sa120\rtlch\afs24\lang255\ai\ltrch\dbch\af8\langfe255\hich\fs24\lang1034\i\loch\fs24\lang1034\i\sbasedon1\snext77 WW-caption1111111111111111111111;}
{\s78\rtlch\afs24\lang255\ltrch\dbch\af8\langfe255\hich\fs24\lang1034\loch\fs24\lang1034\sbasedon1\snext78 WW-Index1111111111111111111111;}
{\s79\sb240\sa120\keepn\rtlch\af2\afs28\lang255\ltrch\dbch\af8\langfe255\hich\f2\fs28\lang1034\loch\f2\fs28\lang1034\sbasedon1\snext3 WW-Heading11111111111111111111111;}
{\s80\sb120\sa120\rtlch\afs24\lang255\ai\ltrch\dbch\af8\langfe255\hich\fs24\lang1034\i\loch\fs24\lang1034\i\sbasedon1\snext80 WW-caption11111111111111111111111;}
{\s81\rtlch\afs24\lang255\ltrch\dbch\af8\langfe255\hich\fs24\lang1034\loch\fs24\lang1034\sbasedon1\snext81 WW-Index11111111111111111111111;}
{\s82\sb240\sa120\keepn\rtlch\af2\afs28\lang255\ltrch\dbch\af8\langfe255\hich\f2\fs28\lang1034\loch\f2\fs28\lang1034\sbasedon1\snext3 WW-Heading111111111111111111111111;}
{\s83\sb120\sa120\rtlch\afs24\lang255\ai\ltrch\dbch\af8\langfe255\hich\fs24\lang1034\i\loch\fs24\lang1034\i\sbasedon1\snext83 WW-caption111111111111111111111111;}
{\s84\rtlch\afs24\lang255\ltrch\dbch\af8\langfe255\hich\fs24\lang1034\loch\fs24\lang1034\sbasedon1\snext84 WW-Index111111111111111111111111;}
{\s85\sb240\sa120\keepn\rtlch\af2\afs28\lang255\ltrch\dbch\af0\langfe255\hich\f2\fs28\lang1034\loch\f2\fs28\lang1034\sbasedon1\snext3 WW-Heading1111111111111111111111111;}
{\s86\sb120\sa120\rtlch\afs24\lang255\ai\ltrch\dbch\af8\langfe255\hich\fs24\lang1034\i\loch\fs24\lang1034\i\sbasedon1\snext86 WW-caption1111111111111111111111111;}
{\s87\rtlch\afs24\lang255\ltrch\dbch\af8\langfe255\hich\fs24\lang1034\loch\fs24\lang1034\sbasedon1\snext87 WW-Index1111111111111111111111111;}
{\s88\sb240\sa120\keepn\rtlch\af0\afs48\lang255\ab\ltrch\dbch\af10\langfe255\hich\f0\fs48\lang1034\b\loch\f0\fs48\lang1034\b\sbasedon7\snext3{\*\soutlvl0} heading 1;}
{\s89\sb240\sa120\keepn\rtlch\af0\afs36\lang255\ab\ltrch\dbch\af10\langfe255\hich\f0\fs36\lang1034\b\loch\f0\fs36\lang1034\b\sbasedon7\snext3{\*\soutlvl1} heading 2;}
{\s90\sb240\sa120\keepn\rtlch\af0\afs28\lang255\ab\ltrch\dbch\af10\langfe255\hich\f0\fs28\lang1034\b\loch\f0\fs28\lang1034\b\sbasedon7\snext3{\*\soutlvl2} heading 3;}
{\*\cs92\rtlch\afs24\lang255\ab\ltrch\dbch\langfe255\hich\fs24\lang1034\b\loch\fs24\lang1034\b Strong Emphasis;}
}
{\info{\creatim\yr0\mo0\dy0\hr0\min0}{\revtim\yr0\mo0\dy0\hr0\min0}{\printim\yr0\mo0\dy0\hr0\min0}{\comment StarWriter}{\vern3000}}\deftab709
{\*\pgdsctbl
{\pgdsc0\pgdscuse195\pgwsxn11905\pghsxn16837\marglsxn1134\margrsxn1134\margtsxn1134\margbsxn1134\pgdscnxt0 Standard;}
{\pgdsc1\pgdscuse195\pgwsxn11905\pghsxn16837\marglsxn1134\margrsxn567\margtsxn567\margbsxn567\pgdscnxt1 HTML;}}
{\*\pgdscno0}\paperh16837\paperw11905\margl1134\margr1134\margt1134\margb1134\sectd\sbknone\pgwsxn11905\pghsxn16837\marglsxn1134\margrsxn1134\margtsxn1134\margbsxn1134\ftnbj\ftnstart1\ftnrstcont\ftnnar\aenddoc\aftnrstcont\aftnstart1\aftnnrlc
\pard\plain \ltrpar\s88\qc\sb240\sa120\keepn\rtlch\af0\afs48\lang255\ab\ltrch\dbch\af10\langfe255\hich\f0\fs48\lang1034\b\loch\f0\fs48\lang1034\b {\rtlch \ltrch\loch\f0\fs48\lang1034\i0\b CONTRACTE D\'92ARRES}
\par \pard\plain \ltrpar\s3\qj\sa120\rtlch\afs24\lang255\ltrch\dbch\af8\langfe255\hich\fs24\lang1034\loch\fs24\lang1034 {\rtlch \ltrch\loch \~}
\par \pard\plain \ltrpar\s3\sa120\ql {\rtlch \ltrch\loch \~}
\par \pard\plain \ltrpar\s3\sa120{\rtlch \ltrch\loch\f0\fs24\lang1034\i0\b0{\rtlch\ltrch\hich\f4\lang1027\loch\f4\lang1027 A {\*\bkmkstart DDE_LINK}<?=$dadescontracte[0]['place']?>{\*\bkmkend DDE_LINK}, a .}}{\rtlch \ltrch\loch\f0\fs24\lang1034\i0\b0{\rtlch\ltrch\hich\f5\lang1027\loch\f5\lang1027 <?=$data_firma[2]?> de <?=$data_mes?> de <?=$data_firma [0]?>}}
\par \pard\plain \ltrpar\s3\sa120\ql\rtlch\afs24\lang255\ltrch\dbch\af8\langfe255\hich\fs24\lang1034\loch\fs24\lang1034 {\rtlch \ltrch\loch \~}
\par \pard\plain \ltrpar\s3\qc\sa120 {\rtlch \ltrch\loch\f4\fs24\lang1027\i0\b R E U N I T S }
\par \pard\plain \ltrpar\s3\qc\sa120\rtlch\afs24\lang255\ltrch\dbch\af8\langfe255\hich\fs24\lang1034\loch\fs24\lang1034 {\rtlch \ltrch\loch \~}
\par \pard\plain \ltrpar\s3\sa120\ql {\rtlch \ltrch\loch \~}
\par \pard\plain \ltrpar\s3\qj\sa120 {\rtlch \ltrch\loch \~\~\~\~\~\~\~\~\~\~\~ }{\rtlch \ltrch\loch\f0\fs24\lang1034\i0\b0{\rtlch\ltrch\hich\f4\lang1027\loch\f4\lang1027 D\'92una part, el Senyors }}{\rtlch \ltrch\loch\f0\fs24\lang1034\i0\b0{\rtlch\ltrch\hich\f5\lang1027\b\loch\f5\lang1027\b {\*\bkmkstart DDE_LINK1}<? foreach($dadespropietari as $rs):extract($rs)?>{\*\bkmkend DDE_LINK1}<?=$name?> <?=$surname1?> <?=$surname2?> }{\rtlch\ltrch\hich\f4\lang1027\loch\f4\lang1027 ,}{\rtlch\ltrch\hich\f5\lang1027\b\loch\f5\lang1027\b <?endforeach //$dadepropietari?>}{\rtlch\ltrch\hich\f4\lang1027\loch\f4\lang1027 . com a part venedora, majors d\'92edat, ve\'efns ........., amb domicili al carrer:}}
\par \pard\plain \ltrpar\s3\sa120\rtlch\afs24\lang255\ltrch\dbch\af8\langfe255\hich\fs24\lang1034\loch\fs24\lang1034{\rtlch \ltrch\loch\f0\fs24\lang1034\i0\b0{\rtlch\ltrch\hich\f5\lang1027\loch\f5\lang1027 <? foreach($dadespropietari as $rs):extract($rs)?><?=$adress?> <?=$numstreet;?> <?=$block?> <?=$flat?> <?=$door?>,  <?endforeach;?>}}{\rtlch \ltrch\loch\f0\fs24\lang1034\i0\b0{\rtlch\ltrch\hich\f5\lang1027\loch\f5\lang1027 prove\u239\'3fts dels DNI n\u250\'3fmeros}{\rtlch\ltrch\hich\f5\lang1027\loch\f5\lang1027  {\*\bkmkstart DDE_LINK3}<? foreach($dadespropietari as $rs):extract($rs)?><?=$vat?> ,<?endforeach //$dadepropietari?>}{\rtlch\ltrch\hich\f5\lang1027\loch\f5\lang1027 {\*\bkmkend DDE_LINK3} respe
ctivament.\~\~\~\~\~\~\~}{\rtlch\ltrch\hich\f4\lang1027\loch\f4\lang1027 \~\~\~\~\~\~\~ }}
\par \pard\plain \ltrpar\s3\qc\sa120\rtlch\afs24\lang255\ltrch\dbch\af8\langfe255\hich\fs24\lang1034\loch\fs24\lang1034 
\par \pard\plain \ltrpar\s3\sa120\rtlch\afs24\lang255\ltrch\dbch\af8\langfe255\hich\fs24\lang1034\loch\fs24\lang1034 
\par \pard\plain \ltrpar\s3\sa120\rtlch\afs24\lang255\ltrch\dbch\af8\langfe255\hich\fs24\lang1034\loch\fs24\lang1034 {\rtlch \ltrch\loch \~}
\par \pard\plain \ltrpar\s3\sa120\ql {\rtlch \ltrch\loch \~\~\~\~\~\~\~\~\~\~\~\~\~\~\~\~\~\~\~\~\~\~\~\~\~\~\~\~\~\~ }
\par \pard\plain \ltrpar\s3\qj\sa120 {\rtlch \ltrch\loch \~\~\~\~\~\~\~\~\~\~\~ }
\par \pard\plain \ltrpar\s3\sa120\ql {\rtlch \ltrch\loch \~\~\~\~\~\~\~\~\~\~\~\~ }{\rtlch \ltrch\loch\f0\fs24\lang1034\i0\b0{\rtlch\ltrch\hich\f4\lang1027\loch\f4\lang1027 I d\'92una altra part, el Senyor/a }}{\rtlch \ltrch\loch\f0\fs24\lang1034\i0\b0{\rtlch\ltrch\hich\f5\lang1027\b\loch\f5\lang1027\b <?foreach ($dadescomprador as $rs)\{?> <?=$rs['name']?> <?=$rs['surname1']?> <?=$rs['surname2']?> <?\}?>}{\rtlch\ltrch\hich\f4\lang1027\loch\f4\lang1027 , com a part compradora,\~ major d\'92edat, ve\'ed d\'92Olot, amb domicili al carrer }{\rtlch\ltrch\hich\f5\lang1027\loch\f5\lang1027 <?=$dadescomprador[0]['adress']?>}{\rtlch\ltrch\hich\f4\lang1027\loch\f4\lang1027 ,
\~ n\'fam }{\rtlch\ltrch\hich\f5\lang1027\loch\f5\lang1027 <?=$dadescomprador[0]['numstreet']?> <?=$dadescomprador[0]['block']?> <?=$dadescomprador[0]['flat']?> <?=$dadescomprador[0]['door']?>}{\rtlch\ltrch\hich\f4\lang1027\loch\f4\lang1027  i prove\'eft de DNI n\'famero }{\rtlch\ltrch\hich\f6\fs22\lang1027\loch\f6\fs22\lang1027 <?=$dadescomprador[0]['vat']?>}}
\par \pard\plain \ltrpar\s3\sa120\ql\rtlch\afs24\lang255\ltrch\dbch\af8\langfe255\hich\fs24\lang1034\loch\fs24\lang1034 {\rtlch \ltrch\loch \~\~\~\~\~\~\~ }
\par \pard\plain \ltrpar\s3\sa120\ql {\rtlch \ltrch\loch \~\~\~\~\~\~\~\~\~\~\~ }{\rtlch \ltrch\loch\f0\fs24\lang1034\i0\b0\rtlch\ltrch\hich\f4\lang1027\loch\f4\lang1027 INTERVENEN, cadascun d\'92ells en el seu propi nom, dret i inter\'e8s. Reconeixent rec\'edprocament la suficient capacitat legal per contractar en els termes del present document, i a tal efecte }
\par \pard\plain \ltrpar\s3\sa120\ql\rtlch\afs24\lang255\ltrch\dbch\af8\langfe255\hich\fs24\lang1034\loch\fs24\lang1034 {\rtlch \ltrch\loch \~}
\par \pard\plain \ltrpar\s3\sa120\ql {\rtlch \ltrch\loch \~\~\~\~\~\~\~\~\~\~\~ }
\par \pard\plain \ltrpar\s3\qc\sa120 {\rtlch \ltrch\loch\f4\fs24\lang1034\i0\b E X P O S E N}
\par \pard\plain \ltrpar\s3\qc\sa120\rtlch\afs24\lang255\ltrch\dbch\af8\langfe255\hich\fs24\lang1034\loch\fs24\lang1034 {\rtlch \ltrch\loch \~}
\par \pard\plain \ltrpar\s3\qj\sa120 {\rtlch \ltrch\loch \~}
\par \pard\plain \ltrpar\s3\sa120\ql {\rtlch \ltrch\loch \~\~\~\~\~\~\~\~\~\~\~ }{\rtlch \ltrch\loch\f0\fs24\lang1034\i0\b0{\rtlch\ltrch\hich\f4\lang1027\b\loch\f4\lang1027\b PRIMER.- }}{\rtlch \ltrch\loch\f0\fs24\lang1034\i0\b0{\rtlch\ltrch\hich\f4\lang1027\loch\f4\lang1027 Que els Senyors }{\rtlch\ltrch\hich\f5\lang1027\loch\f5\lang1027 <?foreach ($dadespropietari as $rs)\{?> <?=$rs['name']?> <?=$rs['surname1']?> <?=$rs['surname2']?>, <?\}?>}{\rtlch\ltrch\hich\f4\lang1027\loch\f4\lang1027  son propietaris en \~ple domini,\~ de la seg\'fcent finca:}}
\par \pard\plain \ltrpar\s3\sa120\ql\rtlch\afs24\lang255\ltrch\dbch\af8\langfe255\hich\fs24\lang1034\loch\fs24\lang1034 {\rtlch \ltrch\loch \~}
\par \pard\plain \ltrpar\s3\qj\sa120 {\rtlch \ltrch\loch\f4\fs24\lang1027\i0\b0 Urbana <?=$dadespropietat[0]['property'];?>}
\par \pard\plain \ltrpar\s3\qj\sa120\rtlch\afs24\lang255\ltrch\dbch\af8\langfe255\hich\fs24\lang1034\loch\fs24\lang1034 {\rtlch \ltrch\loch \~}
\par \pard\plain \ltrpar\s3\qj\sa120 {\rtlch \ltrch\loch\f4\fs24\lang1027\i0\b0 INSCRITA al Registre de la Propietat d\'92Olot, al tom ........., llibre ............, foli ......, finca n\'famero................ de .......................}
\par \pard\plain \ltrpar\s3\qj\sa120\rtlch\afs24\lang255\ltrch\dbch\af8\langfe255\hich\fs24\lang1034\loch\fs24\lang1034 {\rtlch \ltrch\loch \~}
\par \pard\plain \ltrpar\s3\qj\sa120 {\rtlch \ltrch\loch\f4\fs24\lang1027\i0\b0 REFERENCIA CASASTRAL: .......................................}
\par \pard\plain \ltrpar\s3\qj\sa120\rtlch\afs24\lang255\ltrch\dbch\af8\langfe255\hich\fs24\lang1034\loch\fs24\lang1034 {\rtlch \ltrch\loch \~}
\par \pard\plain \ltrpar\s3\sa120\ql {\rtlch \ltrch\loch \~\~\~\~\~\~\~\~\~\~\~ }{\rtlch \ltrch\loch\f0\fs24\lang1034\i0\b0{\rtlch\ltrch\hich\f4\lang1027\b\loch\f4\lang1027\b SEGON.- }}{\rtlch \ltrch\loch\f0\fs24\lang1034\i0\b0{\rtlch\ltrch\hich\f4\lang1027\loch\f4\lang1027 Que interessant a les parts l\'92entrega d\'92Arres de la finca descrita en la primera manifestaci\'f3, ambdues parts, de com\'fa acord, decideixen formalitzar el present document segons els seg\'fcents: }}
\par \pard\plain \ltrpar\s3\sa120\ql\rtlch\afs24\lang255\ltrch\dbch\af8\langfe255\hich\fs24\lang1034\loch\fs24\lang1034 {\rtlch \ltrch\loch \~}
\par \pard\plain \ltrpar\s89\sb240\sa120\keepn\ql {\rtlch \ltrch\loch\f0\fs36\lang1027\i0\b P A C T ES}
\par \pard\plain \ltrpar\s3\qc\sa120\rtlch\afs24\lang255\ltrch\dbch\af8\langfe255\hich\fs24\lang1034\loch\fs24\lang1034 {\rtlch \ltrch\loch \~}
\par \pard\plain \ltrpar\s3\qj\sa120 {\rtlch \ltrch\loch \~\~\~\~\~\~\~\~\~\~\~ }{\rtlch \ltrch\loch\f0\fs24\lang1034\i0\b0{\rtlch\ltrch\hich\f4\lang1027\b\loch\f4\lang1027\b PRIMER.- }}{\rtlch \ltrch\loch\f0\fs24\lang1034\i0\b0{\rtlch\ltrch\hich\f4\lang1027\loch\f4\lang1027 El preu de la venda ser\'e0 la quantitat de ................................ MIL EUROS (}{\rtlch\ltrch\hich\f5\lang1027\loch\f5\lang1027 <?=$dadespropietat[0]['price']?>}{\rtlch\ltrch\hich\f4\lang1027\loch\f4\lang1027 ), de la que pr\'e8via deducci\'f3 de la rebuda en aquest acte de ................... EUROS (...000\'80), els restants ........
............................ MIL EUROS (......................000.\'80) que es faran efectius al comptat en l\'92acte de l\'92atorgament de l\'92escriptura p\'fablica de compra-venda. }}
\par \pard\plain \ltrpar\s3\sa120\ql\rtlch\afs24\lang255\ltrch\dbch\af8\langfe255\hich\fs24\lang1034\loch\fs24\lang1034 {\rtlch \ltrch\loch \~}
\par \pard\plain \ltrpar\s3\qj\li0\ri0\lin0\rin0\fi709\sa120{\rtlch \ltrch\loch\*\cs92 \~}
\par \pard\plain \ltrpar\s3\qj\sa120{\rtlch \ltrch\loch\f0\fs24\lang1034\i0\b\*\cs92\rtlch\ltrch\dbch\hich\fs24\lang1034\b\loch\fs24\lang1034\b\rtlch\ltrch\hich\f4\lang1027\loch\f4\lang1027 \tab SEGON.- La finca es vendr\'e0 lliure de c\'e0rregues, grav\'e0mens i llogaters, i al corrent de pagament, tot acreditant la part venedora mitjan\'e7ant l\'92aportaci\'f3 dels corresponents rebuts en el moment d\'92efectuar l\'92escriptura p\'fablica de compra-venda. Qualsevol retr\'e0
s tributari o altres seran carregats a la part venedora. }
\par \pard\plain \ltrpar\s3\qj\sa120\rtlch\afs24\lang255\ltrch\dbch\af8\langfe255\hich\fs24\lang1034\loch\fs24\lang1034 {\rtlch \ltrch\loch \~}
\par \pard\plain \ltrpar\s3\sa120\ql {\rtlch \ltrch\loch \~}
\par \pard\plain \ltrpar\s3\sa120 {\rtlch \ltrch\loch \~\~\~\~\~\~\~\~\~\~\~ }{\rtlch \ltrch\loch\f0\fs24\lang1034\i0\b0{\rtlch\ltrch\hich\f4\lang1027\b\loch\f4\lang1027\b TERCER.- }}{\rtlch \ltrch\loch\f0\fs24\lang1034\i0\b0{\rtlch\ltrch\hich\f4\lang1027\loch\f4\lang1027 Seran a c\'e0rrec de la part compradora, els honoraris, impostor, arbitris i tots aquells costos que s\'92originin amb motiu de l\'92escriptura p\'fablica de compra-venda que s\'92atorgui fins la inscripci\'f3 de la mateixa en el Registre de la Propieta
t corresponent, excepte l\'92arbitri municipal de plusvalia, que ser\'e0 a c\'e0rrec de la part venedora, conforme la llei.}}
\par \pard\plain \ltrpar\s3\sa120\ql\rtlch\afs24\lang255\ltrch\dbch\af8\langfe255\hich\fs24\lang1034\loch\fs24\lang1034 {\rtlch \ltrch\loch \~}
\par \pard\plain \ltrpar\s3\sa120\ql {\rtlch \ltrch\loch \~}
\par \pard\plain \ltrpar\s3\sa120 {\rtlch \ltrch\loch \~\~\~\~\~\~\~\~\~\~\~}{\rtlch \ltrch\loch\f0\fs24\lang1034\i0\b0{\rtlch\ltrch\hich\f5\lang1027\b\loch\f5\lang1027\b QUART}}{\rtlch \ltrch\loch\f0\fs24\lang1034\i0\b0{\rtlch\ltrch\hich\f5\lang1027\loch\f5\lang1027 .- }{\rtlch\ltrch\hich\f5\loch\f5  }{\rtlch\ltrch\hich\f5\lang1027\loch\f5\lang1027 L\'81\'66escriptura p\u250\'3fblica de compra-venda s\'81\'66efectuar\u224\'3f abans del dia ......de ............ de 2.00...... Les claus de la finca objecte d\'81\'66aquest contracte i la possessi\u243\'3f de la mateixa s\'81\'66entregaran en l\'81\'66acte de la firma de l\'81\'66escriptura p\u250\'3fblica 
de compra-venda.}{\ltrch\hich\lang1027\loch\lang1027  \~}}
\par \pard\plain \ltrpar\s3\sa120\ql\rtlch\afs24\lang255\ltrch\dbch\af8\langfe255\hich\fs24\lang1034\loch\fs24\lang1034 {\rtlch \ltrch\loch \~}
\par \pard\plain \ltrpar\s90\li720\ri0\lin720\rin0\fi0\sb240\sa120\keepn\ql {\rtlch \ltrch\loch \~}
\par \pard\plain \ltrpar\s3\sa120\ql{\rtlch \ltrch\loch\f0\fs24\lang1034\i0\b0{\ltrch\hich\lang1027\loch\lang1027 \tab }}{\rtlch \ltrch\loch\f0\fs24\lang1034\i0\b0{\rtlch\ltrch\hich\f5\lang1027\b\loch\f5\lang1027\b CINQU\u200\'3f.-}{\rtlch\ltrch\hich\f5\lang1027\loch\f5\lang1027  En Cas d\'81\'66incompareixen\u231\'3fa de la part compradora a l\'81\'66acte de la firma de l\'81\'66escriptura de venda i del simultani pagament del preu en la forma consignada, el contracte quedar\u224\'3f rescindit i sense cap tipus d\'81\'66efecte considerant-se com no fet i perdent l
a quantitat entregada en aquest acte. Igualment podr\u224\'3f rescindir el contracte a inst\u224\'3fncia de la part venedora sempre que s\'81\'66avingui a tornar duplicada la quantitat rebuda en aquest moment i la rescissi\u243\'3f tindr\u224\'3f efecte des del mateix moment en qu\u232\'3f pagui a la p
art compradora, o consigni Notarial o Judicialment a la seva disposici\u243\'3f, aquest import.} \~\~\~\~}
\par \pard\plain \ltrpar\s3\sa120\ql\rtlch\af5\afs24\lang255\ltrch\dbch\af8\langfe255\hich\f5\fs24\lang1034\loch\f5\fs24\lang1034 {\rtlch \ltrch\loch \~}
\par \pard\plain \ltrpar\s3\sa120\ql {\rtlch \ltrch\loch \~}
\par \pard\plain \ltrpar\s3\qj\sa120 {\rtlch \ltrch\loch \~\~\~\~\~\~\~\~\~\~\~\~\~\~\~}{\rtlch \ltrch\loch\f0\fs24\lang1034\i0\b0{\rtlch\ltrch\hich\f5\lang1027\b\loch\f5\lang1027\b SIS\u200\'3f.-\~\~\~ }}{\rtlch \ltrch\loch\f0\fs24\lang1034\i0\b0{\ltrch\hich\f5\lang1027\loch\f5\lang1027 Fent una ren\u250\'3fncia expressa i formal dels furs que podrien correspondre\'81\'66ls, les parts acorden establir una cl\u224\'3fusula de submissi\u243\'3f expressa als Jutjats i Tribunals de Girona, per dirimir qualsevol conflicte que podria sorgir en la int
erpretaci\u243\'3f o compliment de contracte. }}
\par \pard\plain \ltrpar\s3\sa120\ql\rtlch\afs24\lang255\ltrch\dbch\af8\langfe255\hich\fs24\lang1034\loch\fs24\lang1034 {\rtlch \ltrch\loch \~}
\par \pard\plain \ltrpar\s3\sa120\ql {\rtlch \ltrch\loch \~}
\par \pard\plain \ltrpar\s3\sa120 {\rtlch \ltrch\loch \~\~\~\~\~\~\~\~\~\~\~ }{\rtlch \ltrch\loch\f0\fs24\lang1034\i0\b0{\rtlch\ltrch\hich\f5\lang1027\loch\f5\lang1027 Sota la seva forma i pactes s\'81\'66afirmen i rectifiquen els atorgants, \~firmant el present document a la ciutat de }}{\rtlch \ltrch\loch\f0\fs24\lang1034\i0\b0{\rtlch\ltrch\hich\f5\lang1027\b\loch\f5\lang1027\b <?=$dadescontracte[0]['place']?>}{\rtlch\ltrch\hich\f5\lang1027\loch\f5\lang1027 , a }{\rtlch\ltrch\hich\f5\lang1027\b\loch\f5\lang1027\b <?=$data_firma[2]?> de <?=$data_mes?> de <?=$data_firma [0]?>}{\rtlch\ltrch\hich\f5\lang1027\loch\f5\lang1027  }}
\par \pard\plain \ltrpar\s3\sa120\ql\rtlch\afs24\lang255\ltrch\dbch\af8\langfe255\hich\fs24\lang1034\loch\fs24\lang1034 {\rtlch \ltrch\loch \~}
\par \pard\plain \ltrpar\s3\qj\sa120 {\rtlch \ltrch\loch \~}
\par \pard\plain \ltrpar\s3\sa120\ql {\rtlch \ltrch\loch \~}
\par \pard\plain \ltrpar\s3\sa120{\rtlch \ltrch\loch\f0\fs24\lang1034\i0\b0{\rtlch\ltrch\hich\f4\lang1046\b\loch\f4\lang1046\b LA PART VENEDORA}}{\rtlch \ltrch\loch\f0\fs24\lang1034\i0\b0 \~}
\par \pard\plain \ltrpar\s3\sa120\ql\rtlch\afs24\lang255\ltrch\dbch\af8\langfe255\hich\fs24\lang1034\loch\fs24\lang1034 {\rtlch \ltrch\loch \~}{\rtlch \ltrch\loch\f0\fs24\lang1034\i0\b0{\rtlch\ltrch\hich\f4\lang1046\loch\f4\lang1046 SR. }}{\rtlch \ltrch\loch\f0\fs24\lang1034\i0\b0{\rtlch\ltrch\hich\f5\lang1027\loch\f5\lang1027 <?foreach ($dadespropietari as $rs)\{?> <?=$rs['name']?> <?=$rs['surname1']?> <?=$rs['surname2']?>, <?\}?>}}
\par \pard\plain \ltrpar\s3\qj\sa120\rtlch\afs24\lang255\ltrch\dbch\af8\langfe255\hich\fs24\lang1034\loch\fs24\lang1034{\rtlch \ltrch\loch\f0\fs24\lang1034\i0\b0{\rtlch\ltrch\hich\f4\lang1046\loch\f4\lang1046 DNI\~ }}{\rtlch \ltrch\loch\f0\fs24\lang1034\i0\b0{\rtlch\ltrch\hich\f5\fs22\lang1027\loch\f5\fs22\lang1027 <? foreach($dadespropietari as $rs):extract($rs)?><?=$vat?> ,<?endforeach //$dadepropietari?>}}
\par \pard\plain \ltrpar\s3\sa120\ql\rtlch\afs24\lang255\ltrch\dbch\af8\langfe255\hich\fs24\lang1034\loch\fs24\lang1034 
\par \pard\plain \ltrpar\s3\sa120\rtlch\afs24\lang255\ltrch\dbch\af8\langfe255\hich\fs24\lang1034\loch\fs24\lang1034 
\par \pard\plain \ltrpar\s3\sa120\rtlch\afs24\lang255\ltrch\dbch\af8\langfe255\hich\fs24\lang1034\loch\fs24\lang1034 
\par \pard\plain \ltrpar\s3\sa120\rtlch\afs24\lang255\ltrch\dbch\af8\langfe255\hich\fs24\lang1034\loch\fs24\lang1034 
\par \pard\plain \ltrpar\s3\qj\sa120\rtlch\af4\afs24\lang255\ab\ltrch\dbch\af8\langfe255\hich\f4\fs24\lang1046\b\loch\f4\fs24\lang1046\b {\rtlch \ltrch\loch \~}{\rtlch \ltrch\loch\f4\fs24\lang1046\i0\b LA PART COMPRADORA}
\par \pard\plain \ltrpar\s3\sa120\ql\rtlch\afs24\lang255\ltrch\dbch\af8\langfe255\hich\fs24\lang1034\loch\fs24\lang1034{\rtlch \ltrch\loch\f0\fs24\lang1034\i0\b0{\rtlch\ltrch\hich\f4\lang1046\loch\f4\lang1046 SR. }}{\rtlch \ltrch\loch\f0\fs24\lang1034\i0\b0{\rtlch\ltrch\hich\f5\lang1027\loch\f5\lang1027 <?foreach ($dadescomprador as $rs)\{?> <?=$rs['name']?> <?=$rs['surname1']?> <?=$rs['surname2']?>, <?\}?>}}
\par \pard\plain \ltrpar\s3\qj\sa120\rtlch\afs24\lang255\ltrch\dbch\af8\langfe255\hich\fs24\lang1034\loch\fs24\lang1034{\rtlch \ltrch\loch\f0\fs24\lang1034\i0\b0{\rtlch\ltrch\hich\f4\lang1046\loch\f4\lang1046 DNI\~ .}}{\rtlch \ltrch\loch\f0\fs24\lang1034\i0\b0{\rtlch\ltrch\hich\f6\fs22\lang1027\loch\f6\fs22\lang1027 <?=$dadescomprador[0]['vat']?>}}
\par \pard\plain \ltrpar\s3\sa120\ql\rtlch\afs24\lang255\ltrch\dbch\af8\langfe255\hich\fs24\lang1034\loch\fs24\lang1034 {\rtlch \ltrch\loch \~}
\par \pard\plain \ltrpar\s1\ql 
\par }