<div class="planxa">
<div class="planxatop">		
<h2>NO SÓM UNS</br> ALTRES PLANXISTES,</br> SOM ELS TEUS PLANXISTES.</h2>
</div>
<div class="planxaleft">
    <p><strong>Mironauto</strong> ofereix un servei de reparació total de la carrosseria del seu vehicle, amb eines modernes i adaptades a les necessitats de cada vehicle.</br>
El servei de pintura es realitza amb productes que cumpleixen la normativa V.O.C., de màxima qualitat i respectuosos amb el medi ambient. La pintura es realitza en una cabina especialment concebuda per aquest fi. A més, sutilitza gas natural per a la seva funció dassecat, governada aquesta electrònicament per optimitzar recursos i temps.</br>
També disposem dun box tintomètric que ens permet trobar el color i la tonalitat exactes de la carrosseria del seu vehicle.</p>    
  </div>
</div>
