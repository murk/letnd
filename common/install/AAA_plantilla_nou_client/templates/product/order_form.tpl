	<div class="order">
		<? if($is_mail):?>
			<? if($is_status_change):?>
				<?=$c_text_status_change?>
			<? endif // is_status_change?>
			<? if($is_buyer):?>
				<p style="padding-top: 30px;"><?=$c_text_buyer?></p>
				<p><?=$c_text_buyer2?></p>
				<p><?=$c_text_buyer3?></p>
			<? endif // is_buyer?>
			<? if($is_seller):?>
				<p style="padding-top: 30px;"><?=$c_text_seller?></p>
				<p><?=$c_text_seller2?></p>
			<? endif // is_seller?>
		<? endif // is_mail?>

		<? if ( $status_change != 'delivered' ): // prepared, shipped, delivered ?>

			<h3><?= $c_order_title ?>&nbsp;<?= $id ?></h3>
			<div class="order-details">
				<table class="form order">
					<tr>
						<th>
							<?= $c_entered_order ?>:
						</th>
						<td>
							<?= $entered_order ?>
						</td>
					</tr>


					<? if ( ! $is_mail ): ?>
						<tr>
							<th>
								<?= $c_shipped ?>:
							</th>
							<td>
								<?= $shipped ?>
							</td>
						</tr>
						<tr>
							<th>
								<?= $c_delivered ?>:
							</th>
							<td>
								<?= $delivered ?>
							</td>
						</tr>
					<? endif // is_mail?>


					<tr>
						<th>
							<?= $c_order_status ?>:
						</th>
						<td>
							<?= $order_status ?>
							<? if ( $repay_form ): ?><?= $repay_form ?><? endif //repay_form?>
						</td>
					</tr>
					<tr>
						<th>
							<?= $c_payment_method ?>:
						</th>
						<td>
							<?= $payment_method ?>
						</td>
					</tr>


					<? if ( $show_account ): ?>
						<tr>
							<th>
								<?= $banc_name ?>:
							</th>
							<td>
								<?= $account_number ?>
							</td>
						</tr>
						<tr>
							<th>
								<?= $c_account_amount ?>:
							</th>
							<td>
								<?= $all_basetax ?> <?= CURRENCY_NAME ?>
							</td>
						</tr>
						<tr>
							<th>
								<?= $c_account_concept ?>:
							</th>
							<td>
								<?= $concept ?>
							</td>
						</tr>
					<? endif // show_account?>


					<tr>
						<th>
							<?= $c_order_title3 ?>:
						</th>
						<td>
							<?= $address_invoice ?>
						</td>
					</tr>
					<tr>
						<th>
							<?= $c_order_title4 ?>:
						</th>
						<td>
							<?= $address_delivery ?>
						</td>
					</tr>

					<? if ( $can_collect ): ?>

						<tr class="collect">
							<th>
								<?= $c_collect ?>:
							</th>
							<td>
								<?= $collect ?>
							</td>
						</tr>

					<? endif // $can_collect ?>


				</table>
			</div>
			<h3><?= $c_title_cart ?></h3>
			<?= $orderitems ?>

		<? endif // $status_change != 'delivered' ?>

	</div>