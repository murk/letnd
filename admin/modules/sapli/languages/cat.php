<?php
// menus eina
define('SAPLI_MENU_NEW', 'Aplicacions');
define('SAPLI_MENU_NEW_NEW', 'Entrar nova aplicació');
define('SAPLI_MENU_NEW_LIST', 'Llistar i editar aplicacions');
define('SAPLI_MENU_NEW_BIN', 'Paperera de reciclatge');
define('SAPLI_MENU_CATEGORY', 'Categories');
define('SAPLI_MENU_CATEGORY_NEW', 'Entrar nova categoria');
define('SAPLI_MENU_CATEGORY_LIST', 'Llistar categories');

$gl_messages_new['no_category']='Per poder insertar notícies hi ha d\'haver almenys una categoria';

// captions seccio
$gl_caption_new['c_prepare'] = 'En preparació';
$gl_caption_new['c_review'] = 'Per revisar';
$gl_caption_new['c_public'] = 'Públic';
$gl_caption_new['c_archived'] = 'Arxivat';
$gl_caption_new['c_new'] = 'Notícia';
$gl_caption_new['c_title'] = 'Títol';
$gl_caption_new['c_subtitle'] = 'Subtítol';
$gl_caption_new['c_description'] = 'Descripció';
$gl_caption_new['c_entered'] = 'Data';
$gl_caption_new['c_status'] = 'Estat';
$gl_caption_new['c_category_id'] = 'Categoria';
$gl_caption_new['c_filter_category_id'] = 'Totes les categories';
$gl_caption_new['c_ordre']='Ordre';
$gl_caption_new['c_new_list']='Titol llistat';

$gl_caption_category['c_category'] = 'Categoria';
$gl_caption_category['c_new_button'] = 'Veure notícies';
$gl_caption_category['c_ordre'] = 'Ordre';
$gl_caption_image['c_name'] = 'Nom';
$gl_caption_new['c_url1_name']='Nom URL(1)';
$gl_caption_new['c_url1']='URL(1)';
$gl_caption_new['c_url2_name']='Nom URL(2)';
$gl_caption_new['c_url2']='URL(2)';
$gl_caption_new['c_videoframe']='Video (youtube, metacafe...)';
$gl_caption_new['c_file']='Arxius';
$gl_caption_new['c_in_home']='Surt a la home';
?>
