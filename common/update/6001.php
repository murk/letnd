<?php

/* 3-11-14 
  
 * Immo: Promotional code

 */

	
	
if (is_table('booking__book')){
	
	$update_sql []= "
		CREATE TABLE IF NOT EXISTS `booking__promcode` (
			`promcode_id` INT(10) NOT NULL AUTO_INCREMENT,
			`promcode` VARCHAR(100) NOT NULL DEFAULT '' COLLATE 'utf8_spanish2_ci',
			`discount_pvp` DECIMAL(11,2) NOT NULL DEFAULT '0.00',
			`discount_type` ENUM('fixed','percent') NOT NULL DEFAULT 'fixed' COLLATE 'utf8_spanish2_ci',
			`minimum_amount` DECIMAL(11,2) NOT NULL DEFAULT '0.00',
			`start_date` DATE NOT NULL,
			`end_date` DATE NOT NULL,
			`bin` TINYINT(1) NOT NULL DEFAULT '0',
			PRIMARY KEY (`promcode_id`)
		)
		COLLATE='utf8_spanish2_ci'
		ENGINE=MyISAM;
	";	
	
	$update_sql []= "
		CREATE TABLE IF NOT EXISTS `booking__promcode_language` (
			`promcode_id` INT(11) NOT NULL DEFAULT '0',
			`language` CHAR(3) NOT NULL DEFAULT '' COLLATE 'utf8_spanish2_ci',
			`name` VARCHAR(100) NOT NULL DEFAULT '' COLLATE 'utf8_spanish2_ci'
		)
		COLLATE='utf8_spanish2_ci'
		ENGINE=MyISAM;
	";	
	
	$update_sql []= "
		CREATE TABLE IF NOT EXISTS `booking__custumer_to_promcode` (
			`custumer_promcode_id` INT(10) NOT NULL AUTO_INCREMENT,
			`custumer_id` INT(10) NOT NULL,
			`promcode_id` INT(10) NOT NULL,
			PRIMARY KEY (`custumer_promcode_id`)
		)
		COLLATE='utf8_spanish2_ci'
		ENGINE=MyISAM;
	";	
	
	if (!is_field('booking__book','promcode_discount')){
		$update_sql []= "
			ALTER TABLE `booking__book`
				ADD COLUMN `promcode_discount` DECIMAL(11,2) NOT NULL AFTER `book_entered`,
				ADD COLUMN `price_book` DECIMAL(11,2) NOT NULL AFTER `promcode_discount`,
				ADD COLUMN `price_extras` DECIMAL(11,2) NOT NULL AFTER `price_book`,
				ADD COLUMN `price_total` DECIMAL(11,2) NOT NULL AFTER `price_extras`;
		";
	}
	
	$update_sql []= "
		UPDATE `all__menu` SET `menu_id`=154 WHERE  `menu_id`=153;
	";	
	
	$update_sql []= "
		UPDATE `all__menu` SET `menu_id`=153 WHERE  `menu_id`=152;
	";	
	
	$update_sql []= "
		UPDATE `all__menu` SET `menu_id`=152 WHERE  `menu_id`=151;
	";	
	
	$update_sql []= "
		UPDATE `all__menu` SET `menu_id`=151 WHERE  `menu_id`=150;
	";	
	
	
	$read_write = Db::get_row("SELECT read1,write1 FROM all__menu WHERE menu_id = 100");
	$read = $read_write['read1'];
	$write = $read_write['write1'];
	
	
	$update_sql []= "
		INSERT INTO `all__menu` 
		(`menu_id`, `menu_group`, `tool`, `tool_section`, `action`, `parent_id`, `toolmode_id`, `variable`, `ordre`, `link`, `process`, `bin`, `read1`, `write1`) 
		VALUES (150, 'booking', 'booking', '', '', 0, 1, 'BOOKING_MENU_ROOT', 1, '152', '', 0, '$read', '$write');



	";	
	$update_sql []= "
		INSERT INTO `all__menu` 
		(`menu_id`, `menu_group`, `tool`, `tool_section`, `action`, `parent_id`, `toolmode_id`, `variable`, `ordre`, `link`, `process`, `bin`, `read1`, `write1`) VALUES 
		(180, 'booking', 'booking', 'promcode', '', 150, 1, 'BOOKING_MENU_PROMCODE', 8, '', '', 0, '$read', '$write');
	";	
	$update_sql []= "
		INSERT INTO `all__menu` 
		(`menu_id`, `menu_group`, `tool`, `tool_section`, `action`, `parent_id`, `toolmode_id`, `variable`, `ordre`, `link`, `process`, `bin`, `read1`, `write1`) VALUES 
		(181, 'booking', 'booking', 'promcode', 'show_form_new', 180, 1, 'BOOKING_MENU_PROMCODE_NEW', 1, '', '', 0, '$read', '$write');
	";	
	$update_sql []= "
		INSERT INTO `all__menu` 
		(`menu_id`, `menu_group`, `tool`, `tool_section`, `action`, `parent_id`, `toolmode_id`, `variable`, `ordre`, `link`, `process`, `bin`, `read1`, `write1`) VALUES 
		(182, 'booking', 'booking', 'promcode', 'list_records', 180, 1, 'BOOKING_MENU_PROMCODE_LIST', 2, '', '', 0, '$read', '$write');
	";	
	$update_sql []= "
		INSERT INTO `all__menu` 
		(`menu_id`, `menu_group`, `tool`, `tool_section`, `action`, `parent_id`, `toolmode_id`, `variable`, `ordre`, `link`, `process`, `bin`, `read1`, `write1`) VALUES 
		(183, 'booking', 'booking', 'promcode', 'list_records_bin', 180, 1, 'BOOKING_MENU_PROMCODE_LIST_BIN', 3, '', '', 0, '$read', '$write');
	";	
	$update_sql []= "
		UPDATE `all__menu` SET `menu_group`='booking', `parent_id`=150 WHERE  `menu_id`=151;
	";	
	$update_sql []= "
		UPDATE `all__menu` SET `menu_group`='booking', `parent_id`=151 WHERE  `menu_id`=152;
	";	
	$update_sql []= "
		UPDATE `all__menu` SET `menu_group`='booking', `parent_id`=151 WHERE  `menu_id`=153;
	";	
	$update_sql []= "
		UPDATE `all__menu` SET `menu_group`='booking', `parent_id`=151 WHERE  `menu_id`=154;
	";	
	$update_sql []= "
		UPDATE `all__menu` SET `menu_group`='booking', `parent_id`=150 WHERE  `menu_id`=160;
	";	
	$update_sql []= "
		UPDATE `all__menu` SET `menu_group`='booking' WHERE  `menu_id`=161;
	";	
	$update_sql []= "
		UPDATE `all__menu` SET `menu_group`='booking' WHERE  `menu_id`=162;
	";	
	$update_sql []= "
		UPDATE `all__menu` SET `menu_group`='booking' WHERE  `menu_id`=163;
	";	
	$update_sql []= "
		UPDATE `all__menu` SET `menu_group`='booking', `parent_id`=150 WHERE  `menu_id`=170;
	";
	$update_sql []= "
		UPDATE `all__menu` SET `menu_group`='booking' WHERE  `menu_id`=162;
	";	
	$update_sql []= "
		UPDATE `all__menu` SET `menu_group`='booking' WHERE  `menu_id`=163;
	";
	$update_sql []= "
		UPDATE `all__menu` SET `ordre`=140 WHERE  `menu_id`=80000;
	";	
	$update_sql []= "
		UPDATE `all__menu` SET `ordre`=120 WHERE  `menu_id`=2000;
	";	
	$update_sql []= "
		UPDATE `all__menu` SET `ordre`=110 WHERE  `menu_id`=6000;
	";	
	$update_sql []= "
		UPDATE `all__menu` SET `ordre`=100 WHERE  `menu_id`=4000;
	";	
	$update_sql []= "
		UPDATE `all__menu` SET `ordre`=90 WHERE  `menu_id`=7000;
	";	
	$update_sql []= "
		UPDATE `all__menu` SET `ordre`=80 WHERE  `menu_id`=90000;
	";	
	$update_sql []= "
		UPDATE `all__menu` SET `ordre`=70 WHERE  `menu_id`=10000;
	";	
	$update_sql []= "
		UPDATE `all__menu` SET `ordre`=60 WHERE  `menu_id`=8000;
	";	
	$update_sql []= "
		UPDATE `all__menu` SET `ordre`=50 WHERE  `menu_id`=3000;
	";	
	$update_sql []= "
		UPDATE `all__menu` SET `ordre`=40 WHERE  `menu_id`=1;
	";	
	$update_sql []= "
		UPDATE `all__menu` SET `ordre`=30 WHERE  `menu_id`=200;
	";	
	$update_sql []= "
		UPDATE `all__menu` SET `ordre`=20 WHERE  `menu_id`=150;
	";		
	$update_sql []= "
		UPDATE `all__menu` SET `ordre`=10 WHERE  `menu_id`=5000;
	";	
	
}
?>