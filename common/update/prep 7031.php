<?php
/* 15-9-17
Netejar registres de languages orfes, de taules d'arxius i imatges

*/

$tables = Db::get_rows_array( 'SHOW TABLES' );

// Bucle per totes les taules amb arxius
foreach ( $tables as $rs ) {
	$table = $rs[0];
	if (
		substr( $table, - 15 ) === '_image_language'
		) {

		$parent_table = substr( $table, - 9 );
		$update_sql[] = "DELETE FROM $table WHERE image_id NOT IN ( SELECT $parent_table.image_id FROM  $parent_table)";

	}
}




?>