<?php
// no es poden posar ; entre mig de la consulta ja que parteix les consultes per el ; a no ser que fem servir $update_sql en array()
	
if (!is_table('web__banner_place')){
	$update_sql .="	
ALTER TABLE `web__banner`
	ADD COLUMN `place_id` INT(11) NOT NULL DEFAULT '0' AFTER `status`;
	
CREATE TABLE `web__banner_place` (
	`place_id` INT(11) NOT NULL AUTO_INCREMENT,
	PRIMARY KEY (`place_id`)
)
COLLATE='utf8_spanish2_ci'
ENGINE=MyISAM;

CREATE TABLE `web__banner_place_language` (
	`place_id` INT(11) NOT NULL DEFAULT '0',
	`language` CHAR(3) NOT NULL COLLATE 'utf8_spanish2_ci',
	`place` VARCHAR(100) NOT NULL COLLATE 'utf8_spanish2_ci'
)
COLLATE='utf8_spanish2_ci'
ENGINE=MyISAM;

INSERT INTO `web__banner_place` 
	(`place_id`) VALUES (1);
";

	global $gl_languages;
	
	foreach($gl_languages['names'] as $lang=>$val){
		$update_sql .="INSERT INTO `web__banner_place_language` (`place_id`,`language`,`place`) VALUES (1,'".$lang."','General');"; // em serveix per cat i spa, els idiomes que utilitzem al backoffice
	}
}
?>