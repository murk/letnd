<?
/**
 * DirectoryDocument
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2013
 * @version $Id$
 * @access public
 */
class DirectoryDocument extends Module{

	var $condition = '';
	var $q = false;

	function __construct(){
		parent::__construct();
		$this->is_user = $GLOBALS['gl_menu_id'] == '312';
	}
	function on_load()
	{
		if (!$this->config['file_has_langs']) unset($this->language_fields[2]);
		// TODO No està fet el is_long , dropzone, per quan hi ha idiomes als arxius
		// Com  que no està fet, ho desconecto automaticament
		if ($this->config['file_has_langs']=='1')
			$this->set_field( 'file', 'is_long', false );

		if ($this->config['add_file_types']) {
			$accept = $this->get_field('file', 'accept');
			$accept .= '|' . $this->config['add_file_types'];
			$this->set_field( 'file', 'accept', $accept );
		}
	}
	function list_records()
	{
		$content = $this->get_records();
		if ($this->q) {
			$GLOBALS['gl_page']->javascript .= "
				$('table.listRecord').highlight('".addslashes(R::get('q'))."');
			";
		}
		$GLOBALS['gl_content'] =
			'<script language="JavaScript" src="/admin/modules/directory/jscripts/list_pick.js?v=' . $GLOBALS['gl_version'] . '"></script>' . $this->get_search_form() . $content;
	}
	function get_records()
	{
		if ($this->config['show_file_names']=='0') {
			$this->set_field('file', 'list_admin', '0');
			$this->set_field('download', 'list_admin', 'out');
		}
		
		
		$listing = new ListRecords($this);


		// search
		if ($this->condition) {
			$listing->condition = $this->condition;
		}

		//$listing->call('records_walk',false,true);
		$listing->table_class = 'big';	
		$category_id = R::text_id('category_id');
		if ($category_id == 'null') $category_id = false;

		if ($category_id) {
			$listing->add_options_button( 'download_all', '', $class = 'botoOpcions1', $place = 'top_after', 'download_all' );
		}
		$listing->add_options_button('download_selected', '', $class = 'botoOpcions1', $place = 'top_after', 'download_selected' );
		
		if ($this->is_user) {
			$listing->set_options( 1, 1, 0, 0, 0, 0, 0, 0, 1 ); // $options_bar = 1, $options_checkboxes = 1, $save_button = 1, $select_button = 1, $delete_button = 1, $print_button = 1, $edit_buttons = 1, $image_buttons = 1, $split_count = 1


			// nomes per en toti !!
			// Desactivo els arxius normals i poso el nom de l'arxiu en cada preview i un checkbox per descarregar
			if ($this->config['preview_all']) {
				$listing->set_options( 1, 0, 0, 0, 0, 0, 0, 0, 1 );
				$listing->set_field( 'file', 'list_admin', '0' );
			}

			// si no es cercador

			$listing->condition .= $this->condition ? ' AND ' : '';

			if ( $this->condition ) {

				$listing->condition .= "
				    status='public'
					AND category_id IN (
							SELECT category_id
							FROM directory__category
							WHERE language = '".LANGUAGE."' " . DirectoryCommon::get_category_condition(true). "
						)";

				$listing->condition .= DirectoryCommon::get_document_condition( $category_id, true, false );


			} else {

				$listing->condition .= "status='public' AND category_id = '" . $category_id . "'";
				$listing->condition .= DirectoryCommon::get_document_condition( $category_id, true );

			}
			$listing->set_field('status','list_admin','0');			
			$listing->set_field('concession','list_admin','no');
		
		}
		else
		{
		
			if (isset($_GET['selected_menu_id']))
			{
				$listing->condition .= $this->condition?' AND ':'';
				$listing->condition = 'category_id = ' . $category_id;
			}
			else
			{
				$listing->add_filter('category_id');
			}
			$listing->add_filter('status');
			$listing->add_button ('concession_change', '', 'boto1', 'after', 'select_concession_document');
		}


		if ($this->config['file_has_langs']) {
			$listing->set_field( 'file', 'show_langs', $this->config['file_show_langs'] );
		}
		$listing->call('records_walk', false, true);
		
		$listing->order_by = 'document ASC';
	    return $listing->list_records();
	}
	function records_walk($module){		
		$rs = &$module->rs;
		$ret = array();
		$ret['download'] = '';
		$files = $rs['files'];
		
		// rs[files] nomes agafa l'arxiu de l'idioma actual
		// si mostro tots els idiomes, miro si hi ha algun arxiu en els altres idiomes
		
		if ($this->config['file_show_langs']=='1' && !$files){
			$files = Db::get_rows('
				SELECT name AS file_name_original, title as file_title, file_id
				FROM directory__document_file
				WHERE document_id = ' . $rs['document_id']);
			//Debug::p($files, 'files2');
		}


		// Si tenim videoframe, no es mostra preview de la imatge sino que es mostra el video
		if ($rs['videoframe']){
			$rs['document_icon'] = $rs['videoframe'];
		}
		// nomes per en toti !!
		// Desactivo els arxius normals i poso el nom de l'arxiu en cada preview i un checkbox per descarregar
		elseif ($this->config['preview_all']) {

			//Debug::p($files, 'text2');
			$rs['document_icon'] = '';
			if ($files) {
				foreach ($files as $file) {

					$type = explode( '.', $file['file_name_original'] );
					$file_name = $file['file_name'];
					$icon = $this->get_preview( $type, $file_name );

					$rs['document_icon'] .= '<tr><td style="padding-top:15px" valign="top"><input type="checkbox" value="' . $file['file_id'] . '" name="download_selected[' . $file['file_id'] . ']"><a target="save_frame" href="' . $file['file_src'] . '&force=1">' .  $file['file_title'] . '</a></td></tr>';
					$rs['document_icon'] .= '<tr><td valign="top"><div class="listImagePreview">' . $icon . '</div></td></tr>';
				}
				$rs['document_icon'] = '<table>' . $rs['document_icon'] . '</table>';
			}

		}
		else {

			foreach ($files as $file){

				$ret['download'] .= '<div><a target="save_frame" href="' . $file['file_src'] . '&force=1">' . $this->caption['c_download'] . '</a></div>';
			}

			//Debug::p($files, 'text2');
			$rs['document_icon'] = '';
			if ($files) {
				foreach ($files as $file) {

					$type = explode( '.', $file['file_name_original'] );
					$file_name = $file['file_name'];
					$rs['document_icon'] = $this->get_preview( $type, $file_name );
					break;
				}

				$rs['document_icon'] = '<div class="listImagePreview">' . $rs['document_icon'] . '</div>';
			}

		}
		
		$rs['concession'] = DirectoryCommon::get_concession_names($rs['category_id'], false, false, $rs['document_id']);
		if (!$rs['concession']) $rs['concession'] = '<div id = "concession_'.$rs['document_id'].'"></div>';
		
		return $ret;
		
	}
	function create_preview($file_name) {
		$file = CLIENT_PATH .'directory/document/files/' . $file_name;
		$file_preview = CLIENT_PATH .'directory/document/files/preview/' . $file_name;

		if (!is_file($file))
			return;

		// LINUX
		// imagemagick
		//if ( !$GLOBALS['gl_is_windows'] ) {
		if ( class_exists("Imagick")) {


			$thumb = new Thumb( $file ); // TODO Haig de probar si es pot posar fora del bucle

			$thumb->quality = $this->config['im_admin_thumb_h'];
			$thumb->resize( $this->config['im_admin_thumb_w'], $this->config['im_admin_thumb_h'] );

			$thumb->unsharp( $this->config['im_admin_thumb_unsharp'] );
			$thumb->save( $file_preview );

			$thumb->clear();

			debug::add( 'Llibreria imatges', 'imagick' );

		}
		// WINDOWS , obsolet, només en local ja que no se com instalar imagemagick
		// també en servidors que no estigui instalat
		else {			
			include (DOCUMENT_ROOT.'common/includes/phpthumb/phpthumb.class.php');
			
			$phpThumb      = new phpThumb();
			$phpThumb->src = $file;

			$phpThumb->w = $this->config['im_admin_thumb_w'];
			$phpThumb->h = $this->config['im_admin_thumb_h'];
			$phpThumb->q = $this->config['im_admin_thumb_q'];
			$phpThumb->setParameter( 'fltr', 'usm|80|0.5|3' );

			$phpThumb->GenerateThumbnail();
			$phpThumb->RenderToFile( $file_preview );

			debug::add( 'Llibreria imatges', 'gd' );
		}
	}
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{
	    // si no hi ha cap categoria faig un redirect a entrar categoria nova
		if (!Db::get_first("SELECT count(*) FROM directory__category LIMIT 0,1")){
			$_SESSION['message'] = $this->messages['no_category'];
			redirect('/admin/?menu_id=306');
		}
		$this->set_field('entered','default_value',now(true));
		
		$show = new ShowForm($this);
	    return $show->show_form();
	}

	function save_rows()
	{
	    $save_rows = new SaveRows($this);
	    $save_rows->save();
	}

	function write_record()
	{
	    $writerec = new SaveRows($this);
		$writerec->set_field('modified','override_save_value','now()');	
		$writerec->save();
	}

	function manage_images()
	{
	    $image_manager = new ImageManager($this);
	    $image_manager->execute();
	}
	
	function download_all() {
		
		$category_id = R::text_id('category_id');
		if ($category_id == 'null') $category_id = false;
		
		$results = Db::get_rows("SELECT document_id FROM directory__document WHERE category_id = '" . $category_id . "' AND bin = 0");
		/*
		$category = Db::get_first("
			SELECT category 
			FROM directory__category_language 
			WHERE category_id = " . $category_id . " 
				AND language = '" . LANGUAGE . "'");
		 * 
		 */
		
		$this->zip_files($results,$this->caption['c_zip_name']);
	}
	
	function download_selected() {
		
		if (!isset($_POST['selected']) && !isset($_POST['download_selected'])) die();


		// nomes per en toti !!
		// desconecto descarregar amb "selected"
		if ($this->config['preview_all']) {
			$results = $_POST['download_selected'];
			$this->zip_files($results,$this->caption['c_selected_zip_name'],true);
		} else {
			$results = $_POST['selected'];
			$this->zip_files($results,$this->caption['c_selected_zip_name']);
		}

	}
	function zip_files($results, $name, $is_file_id = false){
		
		if (!$results) die();
		
		include(DOCUMENT_ROOT . '/common/includes/zip/zip.php');
		// ara esta a 128M per defecte // ini_set("memory_limit","50M"); // aumento memoria, haig de controlar l'error quan sobrepassa
		
		
		$zipfile = new Zip($name);
		Debug::p($results, 'results');
		foreach ($results as $rs){
			$id = is_array($rs)?$rs['document_id']:$rs;

			if ($is_file_id){
				$f = UploadFiles::get_file_by_id ($this, 'file', $id);
				$file_name = $f['file_name_original'];
				$zipfile->addFile(DOCUMENT_ROOT . $f['file_src_real'], $file_name);
			}
			else
			{
				$files  = UploadFiles::get_record_files($id, 'file', 'directory', 'document', false);

				foreach ($files as $f){
					$file_name = $f['file_name_original'];
					$zipfile->addFile(DOCUMENT_ROOT . $f['file_src_real'], $file_name);
				}
			}
		}	

		$zipfile->output();

	}
	//
	// FUNCIONS BUSCADOR
	//
	function search(){

		$q = $this->q = R::escape('q');

		$search_condition = '';

		if ($q)
		{

			$search_condition = "(
				document like '%" . $q . "%'
				OR description like '%" . $q . "%'
				" ;



			// busco a categoria
			$query   = "SELECT category_id
						FROM directory__category_language
						 WHERE (
							 category LIKE '%" . $q . "%'
							 OR category_description LIKE  '%" . $q . "%'
								)";
			$results = Db::get_rows_array( $query );
			$search_condition .= $this->get_ids_query( $results, 'category_id' );



			// busco arxiu
			$query = "SELECT document_id
							FROM directory__document_file
							 WHERE (
								 name like '%" . $q . "%'
									)" ;
			$results = Db::get_rows_array($query);
			$search_condition .= $this->get_ids_query($results, 'document_id');

			$search_condition .= ") " ;



			$GLOBALS['gl_page']->title = TITLE_SEARCH . '<strong>&nbsp;&nbsp;"' . $q . '"</strong>';
		}
		$this->condition = $search_condition;
		Debug::add('Search condition', $this->condition);
		$this->do_action('list_records');
	}

	function get_ids_query(&$results, $field)
	{
		if (!$results) return '';
		$new_arr = array();
		foreach ($results as $rs){
			$new_arr [$rs[0]] = $rs[0];
		}
		return "OR " . $field . " IN (" . implode(',', $new_arr) . ") ";
	}

	function get_search_form(){
		$this->caption['c_by_ref']='';
		$this->set_vars($this->caption);
		$this->set_file('directory/document_search.tpl');
		$this->set_var('menu_id',$GLOBALS['gl_menu_id']);
		$this->set_var('q',  htmlspecialchars(R::get('q')));

		$this->set_var('is_user',$this->is_user);

		return $this->process();
	}

	/**
	 * @param $type
	 * @param $file_name
	 * @param $rs
	 */
	private function get_preview( $type, $file_name ) {

		$type = $type[ count( $type ) - 1 ];

		if (
			strtolower( $type ) == 'jpg' ||
			strtolower( $type ) == 'jpeg' ||
			strtolower( $type ) == 'gif' ||
			strtolower( $type ) == 'png' ||
			strtolower( $type ) == 'bmp'
		) {
			$icon = '<img src="/' . CLIENT_DIR . '/directory/document/files/preview/' . $file_name . '" />';
			if ( ! is_file( CLIENT_PATH . 'directory/document/files/preview/' . $file_name ) )
				$this->create_preview( $file_name );
		} elseif ( is_file( DOCUMENT_ROOT . '/common/images/icons/' . $type . '.png' ) ) {
			$icon = '<img width="60" src="/common/images/icons/' . $type . '.png" />';
		} else {
			$icon = '<img width="60" src="/common/images/icons/default.png" />';
		}

		return $icon;
	}

}
?>