<?php
// menus eina
define('NAUTICA_MENU_EMBARCACION_LIST', 'Listar y editar embarcaciones');
define('NAUTICA_MENU_EMBARCACION', 'Embarcaciones');
define('NAUTICA_MENU_EMBARCACION_NEW', 'Nueva embarcación');
define('NAUTICA_MENU_EMBARCACION_BIN', 'Papelera de reciclage');
define('NAUTICA_MENU_TIPUS', 'Tipo');
define('NAUTICA_MENU_TIPUS_LIST', 'Llistado de tipos');
define('NAUTICA_MENU_TIPUS_NEW', 'Nuevo tipo');
define('NAUTICA_MENU_SHIPYARD', 'Astilleros');
define('NAUTICA_MENU_SHIPYARD_LIST', 'Listado de astilleros');
define('NAUTICA_MENU_SHIPYARD_NEW', 'Nuevo astillero');
define('NAUTICA_MENU_FLAG', 'Banderas');
define('NAUTICA_MENU_FLAG_LIST', 'Llistado de banderas');
define('NAUTICA_MENU_FLAG_NEW', 'Nueva bandera');

// captions seccio
$gl_caption_embarcacion['c_status'] = 'Estado';
$gl_caption_embarcacion['c_clau_dossier'] = 'Datos internos';
$gl_caption_embarcacion['c_clau'] = 'Llave';
$gl_caption_embarcacion['c_dossier'] = 'Expediente ';
$gl_caption_embarcacion['c_entered'] = 'Entrada';
$gl_caption_embarcacion['c_entered_form'] = 'Fecha de entrada';
$gl_caption_embarcacion['c_prepare'] = 'En preparación';
$gl_caption_embarcacion['c_review'] = 'Revisar';
$gl_caption_embarcacion['c_onsale'] = 'A la venta';
$gl_caption_embarcacion['c_sold'] = 'Vendida';
$gl_caption_embarcacion['c_archived'] = 'Archivada';
$gl_caption_embarcacion['c_reserved'] = 'Pendiente';
$gl_caption_embarcacion['c_tipus_id'] = 'Tipo de embarcación';
$gl_caption['c_shipyard'] = 'Astillero';
$gl_caption_embarcacion['c_shipyard_id'] = 'Astillero';
$gl_caption_embarcacion['c_model'] = 'Modelo';
$gl_caption_embarcacion['c_buildyear'] = 'Año';
$gl_caption_embarcacion['c_buildyear_form'] = 'Año de construcción';
$gl_caption_embarcacion['c_length'] = 'Eslora';
$gl_caption_embarcacion['c_breadth'] = 'Manga';
$gl_caption_embarcacion['c_weight'] = 'Peso';
$gl_caption_embarcacion['c_depth'] = 'Calado';
$gl_caption_embarcacion['c_depth2'] = 'Calado';
$gl_caption_embarcacion['c_engine'] = 'Motor';
$gl_caption_embarcacion['c_fuel'] = 'Carburante';
$gl_caption_embarcacion['c_fuel_gasoline'] = 'Gasolina';
$gl_caption_embarcacion['c_fuel_diesel'] = 'Diesel';
$gl_caption['c_flag'] = 'Bandera';
$gl_caption_embarcacion['c_flag_id'] = 'Bandera';
$gl_caption_embarcacion['c_hour_use'] = 'Horas';
$gl_caption_embarcacion['c_price'] = 'Precio';
$gl_caption_embarcacion['c_price_pounds'] = 'Precio (&)';

$gl_caption_embarcacion['c_vhf'] = 'VHF';
$gl_caption_embarcacion['c_depthsounder'] = 'Sonda';
$gl_caption_embarcacion['c_speedlog'] = 'Corredera';
$gl_caption_embarcacion['c_radar'] = 'Radar';
$gl_caption_embarcacion['c_gps'] = 'GPS';
$gl_caption_embarcacion['c_plotter'] = 'Plotter';
$gl_caption_embarcacion['c_autopilot'] = 'Piloto automático';
$gl_caption_embarcacion['c_trimmtabs'] = 'Flaps Hidráulicos';
$gl_caption_embarcacion['c_searchlight'] = 'Faro pirata';
$gl_caption_embarcacion['c_electrical_capstan'] = 'Molinete eléctrico';
$gl_caption_embarcacion['c_gangway'] = 'Pasarela';
$gl_caption_embarcacion['c_liferaft'] = 'Balsa salvavidas';
$gl_caption_embarcacion['c_tender'] = 'Embarcación auxiliar';
$gl_caption_embarcacion['c_offshore_connexion'] = 'Conexión 220V';
$gl_caption_embarcacion['c_battery_charger'] = 'Cargador de baterias';
$gl_caption_embarcacion['c_generator'] = 'Generador';
$gl_caption_embarcacion['c_cover'] = 'Lona de fondeo';
$gl_caption_embarcacion['c_camping_cover'] = 'Capota';
$gl_caption_embarcacion['c_bimini_top'] = 'Toldo sol';
$gl_caption_embarcacion['c_bowthruster'] = 'Hélice de proa';
$gl_caption_embarcacion['c_hot_water'] = 'Agua caliente';
$gl_caption_embarcacion['c_davit'] = 'Pescante';
$gl_caption_embarcacion['c_anemometer'] = 'Equipo de viento';
$gl_caption_embarcacion['c_mainsail'] = 'Vela Mayor';
$gl_caption_embarcacion['c_genoa'] = 'Gènova';
$gl_caption_embarcacion['c_mizzen'] = 'Mesana';
$gl_caption_embarcacion['c_jib'] = 'Foque';
$gl_caption_embarcacion['c_stormsail'] = 'Tormentín';
$gl_caption_embarcacion['c_forsail2'] = 'Trinqueta';
$gl_caption_embarcacion['c_spinaker'] = 'Spinaker';
$gl_caption_embarcacion['c_spi_boom'] = 'Tangón';
$gl_caption_embarcacion['c_roller_genoa'] = 'Enrollador de Génova';

$gl_caption_embarcacion['c_music_equipment'] = 'Equipo de música';
$gl_caption_embarcacion['c_television'] = 'Televisión';
$gl_caption_embarcacion['c_attached_engine'] = 'Motor auxiliar';
$gl_caption_embarcacion['c_fridge'] = 'Nevera eléctrica';
$gl_caption_embarcacion['c_heating'] = 'Calefacción';
$gl_caption_embarcacion['c_air_conditioning'] = 'Aire acondicionado';
$gl_caption_embarcacion['c_teka'] = 'Cubierta de teca';
$gl_caption_embarcacion['c_wc'] = 'WC';
$gl_caption_embarcacion['c_black_water'] = 'Depósito de aguas negras';
$gl_caption_embarcacion['c_roller_mainsail'] = 'Enrollador de mayor';




$gl_caption_embarcacion['c_equipament'] = 'Equipamiento';

$gl_caption_embarcacion['c_data'] = 'Datos del barco';
$gl_caption_embarcacion['c_ref'] = 'Referencia';
$gl_caption_embarcacion['c_general_data'] = 'Datos generales';
$gl_caption_embarcacion['c_measures_weight'] = 'Medidas - Peso';
$gl_caption_embarcacion['c_measures'] = 'Medidas';
$gl_caption_embarcacion['c_weight_units'] = 'kg.';
$gl_caption_embarcacion['c_characteristics_electronic'] = 'Electrónica';
$gl_caption_embarcacion['c_characteristics_electronic_caps'] = 'ELECTRÓNICA';
$gl_caption_embarcacion['c_characteristics_extra'] = 'Extras';
$gl_caption_embarcacion['c_characteristics_sail'] = 'Vela';
$gl_caption_embarcacion['c_characteristics'] = 'Características';

$gl_caption_embarcacion['c_filter_shipyard_id'] = 'Todos los astilleros';
$gl_caption_embarcacion['c_filter_tipus_id'] = 'Todos los tipos';
$gl_caption_embarcacion['c_filter_status'] = 'Todos los estados';
$gl_caption_embarcacion['c_filter_length'] = 'Todas las esloras';
$gl_caption_embarcacion['c_filter_price'] = 'Todos los precios';
$gl_caption_embarcacion['c_print_pdf'] = 'Imprimir pdf';
$gl_caption_embarcacion['c_changeable'] = 'Acepto embarcación a cambio';

$gl_caption_tipus['c_ordre']='Ordre';
$gl_caption_tipus['c_tipus']='Tipus';

$gl_caption['c_embarcacion_file_name'] = $GLOBALS['gl_caption']['c_file_name'];
$gl_caption['c_embarcacion_old_file_name'] = $GLOBALS['gl_caption']['c_old_file_name'];


$gl_caption_embarcacion['c_engine_type'] = 'Tipo motor';
$gl_caption_embarcacion['c_engine_type_fora'] = 'Fueraborda';
$gl_caption_embarcacion['c_engine_type_intra'] = 'Intraborda';
$gl_caption_embarcacion['c_fuel_tank'] = 'Depósito combustible';
$gl_caption_embarcacion['c_water_tank'] = 'Depósito agua';
$gl_caption_embarcacion['c_compass'] = 'Compás';
$gl_caption_embarcacion['c_bath_stair'] = 'Escalera de baño';
$gl_caption_embarcacion['c_trim'] = 'Trim';
$gl_caption_embarcacion['c_kitchen'] = 'Cocina';
$gl_caption_embarcacion['c_microwave'] = 'Microondas';
$gl_caption_embarcacion['c_shower'] = 'Ducha';
?>