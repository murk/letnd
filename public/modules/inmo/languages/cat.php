<?php

if (!defined('TITLE_COMARCA')){
	define('TITLE_COMARCA', 'Propietats a: ');
	define('TITLE_PRICE', 'Propietats fins a %s '.CURRENCY_NAME);
	define('TITLE_PRICE_2', 'Propietats de %s a %s '.CURRENCY_NAME);
	define('TITLE_PRICE_3', 'Propietats de més de %s '.CURRENCY_NAME);

	define('TITLE_FLOORSPACE', 'Propietats fins a %s m<sup>2</sup>');
	define('TITLE_FLOORSPACE_2', 'Propietats de %s a %s m<sup>2</sup>');
	define('TITLE_FLOORSPACE_3', 'Propietats de més de %s m<sup>2</sup>');

	define('TITLE_PERSON', 'Fins a %s persones');
	define('TITLE_PERSON_2', 'De %s a %s persones');
	define('TITLE_PERSON_3', 'Més de %s persones');
}

$gl_caption['c_filter_price_min'] = 'Preu min.';
$gl_caption['c_filter_price_max'] = 'Preu max.';
$gl_caption['c_filter_price_min_temp'] = 'Preu min. / nit';
$gl_caption['c_filter_price_max_temp'] = 'Preu max. / nit';

$gl_caption['c_recent'] = 'Nou';

// NO TRADUIT
$gl_caption['c_property_list__view_all'] = 'Veure tots els inmobles visitats';
$gl_caption['c_characteristics'] = 'Característiques';
// FI NO TRADUIT

$gl_caption['c_booking_close_results_message'] = 'No s\'ha trobat cap inmoble per les dates seleccionades, els següents inmobles es poden reservar per dates similars';
?>