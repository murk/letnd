<?
define('BOOKING_MENU_CONTACT','Reservas');
define('BOOKING_MENU_CONTACT_LIST','Editar reservas');

// captions seccio
$gl_caption_book['c_date_in'] = 'Fecha de entrada';
$gl_caption_book['c_date_out'] = 'Fecha de salida';
$gl_caption_book ['c_name'] = 'Nombre';
$gl_caption_book ['c_surnames'] = 'Apellidos';
$gl_caption_book ['c_mail'] = 'E-mail';
$gl_caption_book ['c_phone'] = 'Teléfono';
$gl_caption_book ['c_comment'] = 'Observaciones';
$gl_caption_book ['c_property_id'] = '';
$gl_caption_book ['c_send_new'] = 'Reservar';

$gl_caption_book['c_form_new'] = 'Insertar una nueva reserva';

$gl_messages_book['added'] = "Reserva realizada entre las fechas: %s y %s";
$gl_messages_book['added2'] = "Reserva realizada para \"%s\" entre las fechas: %s y %s";
$gl_messages_book['saved'] = "Reserva actualizada entre las fechas: %s y %s";
$gl_messages_book['saved2'] = "Reserva actualizada para \"%s\" entre las fechas: %s y %s";
$gl_messages_book['deleted'] = "Reserva borrada entre las fechas: %s y %s";
$gl_messages_book['deleted2'] = "Reserva borrada para %s entre las fechas: %s y %s";
$gl_messages_book['full'] = "Está ocupado entre las fechas: %s y %s";
$gl_messages_book['full2'] = "%s está ocupado entre las fechas: %s y %s";
?>