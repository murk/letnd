<?php
// no es poden posar ; entre mig de la consulta ja que parteix les consultes per el, a no ser que fem servir $update_sql en array() ;
if (is_table('inmo__configpublic')){
	$update_sql .="
	DELETE FROM `inmo__configpublic` WHERE `inmo__configpublic`.`name` = 'im_medium_w';
	DELETE FROM `inmo__configpublic` WHERE `inmo__configpublic`.`name` = 'im_medium_h';
	DELETE FROM `inmo__configpublic` WHERE `inmo__configpublic`.`name` = 'im_details_w';
	DELETE FROM `inmo__configpublic` WHERE `inmo__configpublic`.`name` = 'im_details_h';
	DELETE FROM `inmo__configpublic` WHERE `inmo__configpublic`.`name` = 'im_thumb_w';
	DELETE FROM `inmo__configpublic` WHERE `inmo__configpublic`.`name` = 'im_thumb_h';";
}
if (is_table('product__configpublic')){
	$update_sql .="
	DELETE FROM `product__configpublic` WHERE `product__configpublic`.`name` = 'im_medium_w';
	DELETE FROM `product__configpublic` WHERE `product__configpublic`.`name` = 'im_medium_h';
	DELETE FROM `product__configpublic` WHERE `product__configpublic`.`name` = 'im_details_w';
	DELETE FROM `product__configpublic` WHERE `product__configpublic`.`name` = 'im_details_h';
	DELETE FROM `product__configpublic` WHERE `product__configpublic`.`name` = 'im_thumb_w';
	DELETE FROM `product__configpublic` WHERE `product__configpublic`.`name` = 'im_thumb_h';";
}
$update_sql .="
	DELETE FROM `all__configpublic` WHERE `all__configpublic`.`name` = 'version';";
?>