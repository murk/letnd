<?
/**
 * AmctaicTransparencia
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2011
 * @version $Id$
 * @access public
 */
class AmctaicTransparencia extends Module{

	public function __construct(){
		parent::__construct();
	}
	public function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	public function get_form()
	{
	    $show = new ShowForm($this);
	    $show->id = 1;
	    return $show->show_form();
	}

	public function save_rows()
	{
	    $save_rows = new SaveRows($this);
	    $save_rows->save();
	}

	public function write_record()
	{
	    $writerec = new SaveRows($this);
	    $writerec->save();
	}
}