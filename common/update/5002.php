<?php
/* 13-12-12 */
// is_private_shop -> vol dir que només pots veure i comprar productes si estas logejat
// activate_customer_by_default -> al registrar-se un usuari s'activa automaticament o no
// sell_only_to_wholesalers -> nomes poden comprar distribuidors i prou
if ((is_table("product__configpublic")) && (!Db::get_first("SELECT count(*) FROM product__configpublic WHERE name='activate_customer_by_default'"))){

	$is_private_shop = Db::get_first("SELECT value FROM product__configpublic WHERE name='is_private_shop'");
	// si es botiga privada poso que no activar per defecte ja que les botigues que hi ha fins ara funcionen així
	$activate_customer_by_default = $is_private_shop=='1'?'0':'1';
	
	// Botiga privada o no, privada es necessari entrar login i pass, sino no es veu res
	$update_sql[]="INSERT INTO `product__configpublic` (`name`, `value`) VALUES ('activate_customer_by_default', '".$activate_customer_by_default."');";
	$update_sql[]="INSERT INTO `product__configpublic` (`name`, `value`) VALUES ('sell_only_to_wholesalers', '0');";
}				
?>