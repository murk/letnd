<?
// blocks

define('INMO_PRICE_FIRST','Hasta %s '.CURRENCY_NAME);
define('INMO_PRICE_MIDDLE','De %s a %s '.CURRENCY_NAME);
define('INMO_PRICE_LAST','Más de %s '.CURRENCY_NAME);
define('INMO_FLOOR_SPACE_FIRST','Hasta %s m<sup>2</sup>');
define('INMO_FLOOR_SPACE_MIDDLE','De %s a %s m<sup>2</sup>');
define('INMO_FLOOR_SPACE_LAST','Más de %s m<sup>2</sup>');
define('INMO_PERSON_FIRST','Hasta %s');
define('INMO_PERSON_MIDDLE','De %s a %s');
define('INMO_PERSON_LAST','Más de %s');

$gl_caption['c_offers'] = 'Ofertas';


// captions

$gl_caption['c_send_mail'] = 'enviar mail';
$gl_caption['c_more_info'] = 'más info';
$gl_caption['c_title_search'] = 'buscar';
$gl_caption['c_yes'] = 'Sí';
$gl_caption['c_no'] = 'No';
$gl_caption['c_send'] = 'Enviar';
$gl_caption['c_clear'] = 'Borrar';
$gl_caption['c_send_to_friend']='enviar a un amigo';
$gl_caption['c_print']='imprimir';
$gl_caption['c_zoom_link']='ampliar imágenes';
$gl_caption['c_contact_link']='contactar';

// bloc inmo_categories
$gl_caption['c_sell'] = 'Venta';
$gl_caption['c_rent'] = 'Alquiler';


// NO TRADUIT
$gl_caption['c_tipus_temp'] = 'Alquiler de temporada';
$gl_caption['c_tipus_moblat'] = 'Alquiler con muebles';
// FI NO TRADUIT



// TAMBÉ A ADMIN
// Caption default
$gl_caption['c_form_title'] = 'Editar datos';
$gl_caption['c_config_title'] = 'Editar configuración';
$gl_caption['c_edit'] = 'Editar';


$gl_caption['c_edit_button'] = 'Editar datos';
$gl_caption['c_view_button'] = 'Ver datos';
$gl_caption['c_edit_images_button'] = 'Editar imágenes';
$gl_caption['c_add_images_button'] = 'Añadir imágenes';



$gl_caption['c_edit_data'] = 'Editar datos';
$gl_caption['c_edit_images'] = 'Editar imágenes';
$gl_caption['c_add_images'] = 'Añadir imágenes';
$gl_caption['c_search_form'] = 'Buscar';
$gl_caption['c_send_new'] = 'Guardar';
$gl_caption['c_send_edit'] = 'Guardar cambios';
$gl_caption['c_delete'] = 'Eliminar';
$gl_caption['c_action'] = 'Acción';
$gl_caption['c_delete_selected'] = 'Borrar seleccionados';
$gl_caption['c_restore_selected'] = 'Recuperar seleccionados';
$gl_caption['c_confirm_deleted'] = 'Los elementos seleccionados se eliminarán definitivamente de la base de datos \\n\\n¿Desea continuar?';
$gl_caption['c_confirm_images_deleted'] = 'Las imágenes seleccionadas se eliminarán definitivamente \\n\\n¿Desea continuar?';
$gl_caption['c_confirm_form_bin'] = 'El registro se enviará a la papelera de reciclaje \\n\\n¿Desea continuar?';
$gl_caption['c_confirm_form_deleted'] = 'El registro se eliminará definitivamente de la base de datos \\n\\n¿Desea continuar?';
$gl_caption['c_move_to'] = 'Mover a:';
$gl_caption['c_print_all'] = 'Imprimir todo';
$gl_caption['c_print'] = 'Imprimir';
$gl_caption['c_order_by'] = 'Ordenar por:';
$gl_caption['c_select'] = 'Seleccionar';
$gl_caption['c_all'] = 'todo';
$gl_caption['c_nothing'] = 'nada';
$gl_caption['c_preview'] = 'Previsualizar';
$gl_caption['c_yes'] = 'Sí';
$gl_caption['c_no'] = 'No';
$gl_caption['c_options_bar'] = 'Opciones';
$gl_caption['c_file_maximum'] = 'máximo %s archivos a la vez';
$gl_caption['c_file_maximum_1'] = 'máximo 1 archivo a la vez';
?>