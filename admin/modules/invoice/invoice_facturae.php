<?

use josemmo\Facturae\Facturae;
use josemmo\Facturae\FacturaeCentre;
use josemmo\Facturae\FacturaeParty;

/**
 * InvoicePdf
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2017
 * @version $Id$
 * @access public
 *
 */
class InvoiceFacturae {
	var $fac, $rs, $module;

	function __construct( &$module ) {

		$this->module = &$module;

		// create new PDF document

	}

	public function set_data( $invoice_id ) {

		$this->rs = Db::get_row( "SELECT * FROM invoice__invoice WHERE invoice_id = $invoice_id" );

		$fac = new Facturae();

		extract( $this->rs );

		$fac->setNumber( $invoice_serial, $invoice_number );
		$fac->setIssueDate( $bill_date );

		$fac->setSeller( new FacturaeParty( $this->get_seller() ) );
		$fac->setBuyer( new FacturaeParty( $this->get_customer() ) );

		$this->set_concepts( $invoice_id, $fac );

		$this->fac = $fac;

	}

	public function output( $file_name = false, $save_path = false ) {


		if ( $this->rs['invoice_number'] != 0 ) {
			$rs = &$this->rs;
		}
		else {
			$custumer_id = $this->rs['custumer_id'];
			$rs          = Db::get_row( "SELECT * FROM custumer__custumer WHERE custumer_id = $custumer_id" );
		}

		if ( ! $file_name ) {
			if ( $rs['custumer_billing'] == 'citizen' ) {
				$file_name = $rs['custumer_name'] . ' ' . $rs['custumer_surname1'] . ' ' . $rs['custumer_surname2'];
			}
			else {
				$file_name = $rs['custumer_company'];

			}
			$file_name = preg_replace( '/[^a-zA-Z0-9 _.-]/', '', $file_name ) . '_' . $rs['invoice_number'] . '.xsig';
		}
		else {
			$file_name .= '.xsig';
		}

		$signat = $this->fac->sign( '/var/www/letnd.com/datos/includes/letnd2018.pfx', null, $GLOBALS['configuration']['m_password_certificate'] );

		if ( $save_path ) {

			$this->fac->export( $save_path . $file_name );

			return $save_path . $file_name;
		}
		else {
			$file_content = $this->fac->export();
			header( 'Content-Description: File Transfer' );
			// header( 'Content-Type: application/force-download' );
			header('Content-Type: application/octet-stream');
			header( 'Content-Disposition: attachment; filename=' . $file_name );
			// header( 'Content-Type: xlm' );
			header('Content-Transfer-Encoding: binary');
			header( 'Expires: 0' );
			header( 'Cache-Control: must-revalidate' );
			header( 'Pragma: public' );
			header( 'Content-Length: ' . strlen( $file_content ) );

			ob_clean();
			flush();
			html_end( $file_content );
		}
	}

	/**
	 * @return array
	 */
	public function get_customer() {

		$custumer_id = $this->rs['custumer_id'];

		// No ha d'haver invoice_number ???
		if ( $this->rs['invoice_number'] ) {
			$rs = &$this->rs;

			$rs['invoice_informations'] = Db::get_first( "SELECT invoice_informations FROM custumer__custumer WHERE custumer_id = $custumer_id" );
		}
		else {
			$rs          = Db::get_row( "SELECT * FROM custumer__custumer WHERE custumer_id = $custumer_id" );
		}


		if ( $rs['custumer_billing'] == 'citizen' ) {
			$company  = $rs['custumer_name'];
			$custumer_surname1 = $rs['custumer_surname1'];
			$custumer_surname2 = $rs['custumer_surname2'];
			$cif      = $rs['custumer_vat'];
		}
		else {
			$company  = $rs['custumer_company'];
			$custumer_surname1 = '';
			$custumer_surname2 = '';
			$cif      = $rs['custumer_cif'];
		}
		$company = mb_strtoupper( $company, 'UTF-8' );

		$adress = '';
		if ( $rs['custumer_adress'] ) {
			$adress .= $rs['custumer_adress'];
			if ( $rs['custumer_numstreet'] ) {
				$adress .= ', ' . $rs['custumer_numstreet'];
			}

			if ( $rs['custumer_block'] ) {
				$adress .= ' ' . $rs['custumer_block'];
			}
			if ( $rs['custumer_flat'] ) {
				$adress .= ' ' . $rs['custumer_flat'];
				if ( $rs['custumer_door'] ) {
					$adress .= '-' . $rs['custumer_door'];
				}
			}
		}

		$town     = $rs['custumer_town'];
		$province = mb_strtoupper( $rs['custumer_province'], 'UTF-8' );
		$zip      = $rs['custumer_zip'];
		$invoice_informations = $rs['invoice_informations'];


		$ret =
			[
				"isLegalEntity" => $rs['custumer_billing'] == 'company',
				"taxNumber"     => $cif,
				"name"          => $company,
				"firstSurname"  => $custumer_surname1,
				"lastSurname"   => $custumer_surname2,
				"address"       => $adress,
				"postCode"      => $zip,
				"town"          => $town,
				"province"      => $province
			];

		// Posar igual que camp ninots
		if ( $invoice_informations && strpos( $invoice_informations, 'Centre administratiu' ) === 0 ) {
			$center_info     = explode( PHP_EOL, $invoice_informations );
			$ret ['centres'] = [
				new FacturaeCentre( [
					"role"     => FacturaeCentre::ROLE_CONTABLE,
					"code"     => $center_info[1],
					"name"     => $center_info[2]
				] ),
				new FacturaeCentre( [
					"role"     => FacturaeCentre::ROLE_GESTOR,
					"code"     => $center_info[3],
					"name"     => $center_info[4]
				] ),
				new FacturaeCentre( [
					"role"     => FacturaeCentre::ROLE_TRAMITADOR,
					"code"     => $center_info[5],
					"name"     => $center_info[6]
				] ),
			];
		}

		Debug::add('Factures invoice_informations', $invoice_informations);
		Debug::add('Factures client', $ret);
		return $ret;
	}

	public function get_seller() {
		return [
			"taxNumber" => "47184738P",
			"name"      => "Marc Sanahuja Campanyà",
			"address"   => "Juli Garreta 25C - 2º - 2ª",
			"postCode"  => "17002",
			"town"      => "Girona",
			"province"  => "Girona"
		];
	}

	public function set_concepts( $invoice_id, &$fac ) {


		$results = Db::get_rows( "
			SELECT * 
			FROM invoice__invoiceconcept 
			WHERE invoice_id = $invoice_id 
			ORDER BY ordre ASC, concept ASC" );


		foreach ( $results as $rs ) {
			extract( $rs );
			$fac->addItem( $concept, $price * 1.21, $quantity, Facturae::TAX_IVA, 21 );
		}
	}

}
