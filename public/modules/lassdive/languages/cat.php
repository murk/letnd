<?
$gl_caption['c_order_status'] = 'Pagat';
$gl_caption['c_order_status_paying'] = 'Pendent';
$gl_caption['c_order_status_failed'] = 'Fallit';
$gl_caption['c_order_status_deposit'] = 'Paga i senyal';
$gl_caption['c_order_status_pending'] = 'Pendent';
$gl_caption['c_order_status_denied'] = 'Rebutjat';
$gl_caption['c_order_status_voided'] = 'Anul·lat';
$gl_caption['c_order_status_refunded'] = 'Retornat';
$gl_caption['c_order_status_completed'] = 'Pagat';
$gl_caption['c_order_status_preparing'] = 'Preparant';
$gl_caption['c_order_status_shipped'] = 'Enviat';
$gl_caption['c_order_status_delivered'] = 'Entregat';

$gl_caption['c_nav_product_search'] = "Nova reserva";
$gl_caption['c_nav_product_search_text'] = "Nova";
$gl_caption['c_nav_reserva'] = "Llistar reserves";
$gl_caption['c_nav_reserva_text'] = "Reserves";
$gl_caption['c_nav_timetable'] = "Calendari";
$gl_caption['c_nav_timetable_text'] = "Calendari";
$gl_caption['c_nav_back'] = "Enrera";
$gl_caption['c_nav_languages'] = "Idioma";
$gl_caption['c_nav_logout'] = "Desconectar";

$gl_caption['c_explicacio_buscador'] = "Cercar per paraules o Referències <i>(ej: \"456 245 45\")</i>";
$gl_caption['c_explicacio_buscador_producte'] = "Cercar activitats";
$gl_caption['c_top_products'] = "Més venudes";
$gl_caption['c_last_products'] = "Últimes activitats";
$gl_caption['c_families'] = "Famílies";
$gl_caption['c_ref_list'] = "ref.";
$gl_caption['c_activitat'] = "Activitat";
$gl_caption['c_reservar_ara'] = "Finalitzar reserva";

$gl_caption['c_titol_paperera'] = "Paperera de reciclatge";
$gl_caption['c_titol_paperera_2'] = "Reserves eliminades";

$gl_caption['c_set_payed_not_saved'] = "No s'ha pogut marcar com a pagat, si us plau torna a probar-ho";
$gl_caption['c_set_payed_saved'] = "La reserva s'ha marcat com a pagada";
$gl_caption['c_set_payed_saved_deposit'] = "El dipòsit s'ha marcat com a pagat";

$gl_messages ['no_records'] = 'No s\'ha trobat cap comanda';