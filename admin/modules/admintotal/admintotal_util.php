<?php
function list_records()
{
	global $gl_action;
	switch ($gl_action)
	{
		case 'list_records_vars':
			list_records_vars();
			break;
		case 'list_records_tags':
			list_records_tags();
			break;
		default:
			list_records_languages();
	}
}

function list_records_languages()
{
	global $tpl, $gl_content, $gl_languages;

    $template = 'admintotal/util.tpl';
	$tpl->set_file($template);
	$tables = Db::get_rows_array('SHOW TABLES');
	foreach($tables as $table){
		$table = current($table);
		$prefix = substr($table, 0, strpos($table,'_language'));
		if ($prefix && $table!='all__language' && $table!='user__user_to_language'){
			$id = explode('__',$prefix);
			
			if ( strpos($table,'image_language')!==false) $id[1] = 'image';			
			
			$id = $id[1].'_id';
			$sqls = array();
			foreach($gl_languages['public'] as $lang){
				$sqls[]= array(
				'lang'=> $lang,
				'sql'=> (strpos($table, '__config')===false)?
							"INSERT INTO $table ($id, language) SELECT DISTINCT $id, '$lang' FROM $table;":
							"INSERT INTO $table ($id, language, name) SELECT DISTINCT $id, '$lang', name FROM $table;",
				'sql2'=> "SELECT * FROM $table WHERE language='$lang';",
				'sql3'=> "DELETE FROM $table WHERE language not IN (SELECT code FROM all__language WHERE public<>0);",
				'count'=> Db::get_first("SELECT count(*) FROM $table WHERE language='$lang'"),
				'count3'=> Db::get_first("SELECT count(*) FROM $table WHERE language not IN (SELECT code FROM all__language WHERE public<>0);")
				);
			}
			$table_arr['sqls'] = $sqls;
			$loop[] = $table_arr;
		}

	}
	$tpl->set_loop('languages', $gl_languages['public']);
	$tpl->set_loop('loop', $loop);
	$gl_content = $tpl->process();
}
function list_records_vars(){

	$lang1 = isset($_GET['lang1'])&&$_GET['lang1']?$_GET['lang1']:'cat';
	$lang2 = isset($_GET['lang2'])&&$_GET['lang2']?$_GET['lang2']:'spa';
	
	$file = DOCUMENT_ROOT . (isset($_GET['file'])&&$_GET['file']?'admin/modules/'.$_GET['file'].'/languages/'.$lang1.'.php':'admin/languages/'.$lang1.'.php');
	$_GET['file2'] = DOCUMENT_ROOT . (isset($_GET['file'])&&$_GET['file']?'admin/modules/'.$_GET['file'].'/languages/'.$lang2.'.php':'admin/languages/'.$lang2.'.php');
	
	$_GET['file_public'] = DOCUMENT_ROOT . (isset($_GET['file'])&&$_GET['file']?'public/modules/'.$_GET['file'].'/languages/'.$lang1.'.php':'public/languages/'.$lang1.'.php');
	$_GET['file_public2'] = DOCUMENT_ROOT . (isset($_GET['file'])&&$_GET['file']?'public/modules/'.$_GET['file'].'/languages/'.$lang2.'.php':'public/languages/'.$lang2.'.php');
	
	$_GET['client'] = (isset($_GET['client'])&&$_GET['client']?DOCUMENT_ROOT . 'clients/' . $_GET['client']:CLIENT_PATH);
	$_GET['file_client'] = $_GET['client'] . '/languages/'.$lang1.'.php'; // faig el GET així no surt a defined_vars
	$_GET['file_client2'] = $_GET['client'] . '/languages/'.$lang2.'.php'; // faig el GET així no surt a defined_vars
	$constants1 = get_defined_constants();
	include ($file);
	$constants2 = get_defined_constants();

	// array_diff_assoc manualment ja que la funció php no funciona
	// resto les constants definides abans de carregar l'arxiu d'idiomes de les constants que ja existien ( que son les generals i les del modul admintotal
	// excepte a l'arrel
	if (isset($_GET['file'])&&$_GET['file']){
		foreach ($constants2 as $key=>$val)
		{
			if ($key!='NULL' && !isset($constants1[$key])){
				$constants[$key] = $val;
			}
		}
	}
	else{
		$constants = array(); //array_merge($constants2, $constants1); ---> hauria de ser així, però em posa moltes altres constants que no m'interessen
	}
	// no va, per que?    $constants = array_diff_assoc ($constants1,$constants2);
	//Debug::p($constants);

	$loop = array();
	
	// agafo variables de admin
	unset($constants1);
	unset($constants2);
	unset($key);
	unset($val);
	unset($lang1);
	unset($lang2);
	unset($file);
	
	$vars = get_defined_vars();
	foreach ($vars as $key=>$val){
		if ($key!='loop'){
			$var_values = array();
			foreach ($val as $k=>$v){
				$var_values[] = array('k'=>$k, 'lang1'=>$v, 'lang2'=>get_lang_vars($key, $k, $_GET['file2']));
			}
			$loop[] = array ( 'key'=> $key, 'var_values'=> $var_values);
			unset($$key);
		}
	}
	// agafo variables de public
	include ($_GET['file_public']);
	//unset($loop);
	unset($vars);
	unset($key);
	unset($val);
	unset($var_values);
	unset($k);
	unset($v);
	
	$vars = get_defined_vars();	
	foreach ($vars as $key=>$val){
		if ($key!='loop'){
			$var_values = array();
			foreach ($val as $k=>$v){
				$var_values[] = array('k'=>$k, 'lang1'=>$v, 'lang2'=>get_lang_vars($key, $k, $_GET['file_public2']));
			}
			$loop[] = array ( 'key'=> $key . ' (public)', 'var_values'=> $var_values);
			unset($$key);
		
	// agafo variables del client
	include ($_GET['file_client']);
	//unset($loop);
	unset($vars);
	unset($key);
	unset($val);
	unset($var_values);
	unset($k);
	unset($v);
	
	$vars = get_defined_vars();	
	foreach ($vars as $key=>$val){
		if ($key!='loop'){
			$var_values = array();
			foreach ($val as $k=>$v){
				$var_values[] = array('k'=>$k, 'lang1'=>$v, 'lang2'=>get_lang_vars($key, $k, $_GET['file_client2']));
			}
			$loop[] = array ( 'key'=> $key . ' ('.$_GET['client'].')', 'var_values'=> $var_values);
			unset($$key);
		}
	}}
	}


	// passo vars al template
	global $tpl, $gl_content, $gl_languages;
    $template = 'admintotal/util_vars.tpl';
	$tpl->set_file($template);
	$tpl->set_loop('loop', $loop);

	$lang1 = isset($_GET['lang1'])&&$_GET['lang1']?$_GET['lang1']:'cat';
	$lang2 = isset($_GET['lang2'])&&$_GET['lang2']?$_GET['lang2']:'spa';
	$file = isset($_GET['file'])&&$_GET['file']?$_GET['file']:'';

	$tpl->set_var('lang1', $lang1);
	$tpl->set_var('lang2', $lang2);

	// desplegable de moduls
    $dir = dir(DOCUMENT_ROOT . "admin/modules");
    $html = '';
    while ($dir_entry = $dir->read())
    {
        if (($dir_entry != '.') && ($dir_entry != '..') && !(is_file($dir->path . "/" . $dir_entry)))
        {
            $selected = (($dir_entry)==$file)?' selected':'';
			$html .= '<option value="' . $dir_entry . '" '.$selected.'>' . $dir_entry . '</option>';
        }
    }
    $html = '<select class="cap" name="file"><option value="">Arrel - General</option>' . $html . '</select>';

	$tpl->set_var('file', $html);
	
	// desplegable client
    $dir = dir(DOCUMENT_ROOT . "clients");
    $html = '';
    while ($dir_entry = $dir->read())
    {
        if (($dir_entry != '.') && ($dir_entry != '..') && !(is_file($dir->path . "/" . $dir_entry)))
        {
			$selected = (('clients/'.$dir_entry)==CLIENT_DIR)?' selected':'';
			$html .= '<option value="' . $dir_entry . '" '.$selected.'>' . $dir_entry . '</option>';
        }
    }
    $html = '<select class="cap" name="client">' . $html . '</select>';

	$tpl->set_var('client', $html);

	$gl_content = $tpl->process();

}
function get_lang_vars($key, $k, $file){

	$lang2 = isset($_GET['lang2'])&&$_GET['lang2']?$_GET['lang2']:'spa';

	include ($file);
	return ${$key}[$k];
}

function list_records_tags(){
	global $tpl, $gl_content, $gl_languages;
    $template = 'admintotal/util_tags.tpl';
	
	$table = isset($_GET['table'])&&$_GET['table']?$_GET['table']:false;
	
	$query = "SELECT all__menu.menu_id AS menu_id,tool,tool_section 
		FROM all__menu 
		WHERE all__menu.bin = 0 
		AND tool_section <> '' 
		GROUP BY tool, tool_section 
		ORDER BY tool ASC, tool_section";
	$results = Db::get_rows($query);

	$table_select = '';
    foreach ($results as $rs){
		if ($rs['tool']!='admintotal'){
			$table_rs = $rs['tool'] . '__' . $rs['tool_section'];
            $selected = ($table_rs==$table)?' selected':'';
			$table_select .= '<option value="' . $table_rs . '" '.$selected.'>' . $table_rs . '</option>';
		}
    }
    $table_select = '<select class="cap" name="table"><option value="">---- Tool</option>' . $table_select . '</select>';
	
	
	$tpl->set_file($template);
	$tpl->set_var('table_select', $table_select);
	$tpl->set_var('has_images', is_table($table . '_image'));
	$tpl->set_var('has_files', is_table($table . '_file'));
	$loop = array();
	if ($table){
		$names = explode('__',$table);
		$tool = $names[0];
		$tool_section =  $names[1];
		if (is_table($table)) {        
				$loop += meta_columns($table);
				unset($loop[strtoupper($tool_section . '_id')]);
		}
		if (is_table($table . '_language')) {        
				$loop += meta_columns($table . '_language');
				unset($loop[strtoupper($tool_section . '_id')]);
				unset($loop['LANGUAGE']);
		}
		
	}
	$tpl->set_var('loop', $loop);
	$gl_content = $tpl->process();

}


function meta_columns($table, $normalize = true)
{
    $results = Db::get_rows('SHOW COLUMNS FROM ' . $table);

    $ret = array();
    foreach ($results as $rs) {
        $new_rs = array();
        $new_rs['name'] = $rs['Field'];
        $type = $rs['Type'];
        // split type into type(length):
        $new_rs['scale'] = null;
        if (preg_match("/^(.+)\((\d+),(\d+)/", $type, $query_array)) {
            $new_rs['type'] = $query_array[1];
            $new_rs['max_length'] = is_numeric($query_array[2]) ? $query_array[2] : - 1;
            $new_rs['scale'] = is_numeric($query_array[3]) ? $query_array[3] : - 1;
        } elseif (preg_match("/^(.+)\((\d+)/", $type, $query_array)) {
            $new_rs['type'] = $query_array[1];
            $new_rs['max_length'] = is_numeric($query_array[2]) ? $query_array[2] : - 1;
        } elseif (preg_match("/^(enum)\((.*)\)$/i", $type, $query_array)) {
            $new_rs['type'] = $query_array[1];
            $arr = explode(",", $query_array[2]);
            $new_rs['enums'] = $arr;
            $zlen = max(array_map("strlen", $arr)) - 2; // PHP >= 4.0.6
            $new_rs['max_length'] = ($zlen > 0) ? $zlen : 1;
        } else {
            $new_rs['type'] = $type;
            $new_rs['max_length'] = - 1;
        }
        $new_rs['not_null'] = ($rs['Null'] != 'YES');
        $new_rs['primary_key'] = ($rs['Key'] == 'PRI');
        $new_rs['auto_increment'] = (strpos($rs['Extra'], 'auto_increment') !== false);
        $new_rs['binary'] = (strpos($type, 'blob') !== false);
        $new_rs['unsigned'] = (strpos($type, 'unsigned') !== false);

        if (!$new_rs['binary']) {
            $d = $rs['Default'];
            if ($d != '' && $d != 'NULL') {
                $new_rs['has_defaul'] = true;
                $new_rs['default_value'] = $d;
            } else {
                $new_rs['has_default'] = false;
            }
        }

        $ret[strtoupper($new_rs['name'])] = $new_rs;
    }
    return $ret;
}

?>