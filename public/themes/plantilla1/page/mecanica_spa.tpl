<div class="mecanica">
<div class="mecanicatop">		
<h2>NO SOMOS</br> OTROS MECÁNICOS,</br> SOMOS TUS MECÁNICOS.</h2>
</div>
<div class="mecanicaleft">
    <strong>Mironauto</strong> dispone de servicio de cambio de neumáticos, equilibrado de ruedas, sean de hierro o aluminio. También le podemos hinchar sus neumáticos con nitrógeno, (gas con una molécula más grande que la del oxígeno. Este gas evita las explosiones de neumáticos por exceso de temperatura y la presión de las ruedas se mantiene mucho más tiempo, evitando la corrosión de las llantas, sean de hierro o aluminio.</br></br>Si usted ha canviado los pneumáticos o si su vehiculo ha sufrido un accidente, podemos alinearle la dirección en todos sus ángulos (siempre que estos sean ajustables), segun la cotas originales del fabricante. Tambien podemos hacerle un diagnóstico de su vehículo para saber exáctamente el problema yu encontrar una solución con la máxima eficácia.   
  </div>
</div>
