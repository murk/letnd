<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Finques Garrotxa</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<base href="<?=$url?>">
</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<div style="height:10px;"></div><table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td valign="top" bgcolor="#999999"><?if ($preview):?>
        <img src="/<?=CLIENT_DIR?>/images/logo_newsletter.jpg" width="180" height="119" /><?endif // $preview?><?if (!$preview):?><img src="cid:logo" width="180" height="119" /><?endif // $preview?></td>
    <td align="right" valign="bottom" bgcolor="#999999"><table border="0" cellpadding="6" cellspacing="0">
      <tr>
        <td><font face="Arial, Helvetica, sans-serif" size="-2"><strong><br />
           <?=$page_address?></font></td>
      </tr>
    </table></td>
  </tr>
</table>
<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr> 
    <td valign="top"> <br>
      <table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td width="100%" valign="top"><?=$message?></td>
          <td valign="top"><table border="0" cellspacing="0" cellpadding="0">
            <?foreach($pictures as $picture): extract ($picture)?>
            <tr>
              <td><img src="<?=$picture_src?>"></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
            </tr>
            <?endforeach //$images?>
          </table></td>
        </tr>
      </table>    </td>
  </tr>
</table>
<br>
<br>
</body>
</html>
