<?php

if (!defined('PRODUCT_SHOW_EMPTY_FAMILYS')) Module::get_config_values('product');

$show_images = isset($vars['show_images']) ? $vars['show_images'] : false;
$show_empty_familys = isset($vars['show_empty_familys']) ? $vars['show_empty_familys'] : PRODUCT_SHOW_EMPTY_FAMILYS;

$vars['block_selected'] = 0;

// Busco totes les families
$query = "SELECT family, product__family.family_id, family_description, family_alt
				FROM product__family_language, product__family
				WHERE product__family_language.family_id = product__family.family_id
				AND language = '" . LANGUAGE . "'
				AND product__family.parent_id= 0
				AND bin = 0
				ORDER BY ordre ASC, family";
if (isset($home_max_familys)) {
	$query .= ' LIMIT 0, ' . $home_max_familys;
}
$results = Db::get_rows($query);
// Debug::add('Provant families', $query);

// això estava al block
$famselect = null;
if (isset($_GET['subfamily_id'])) {
	$familia = $_GET['subfamily_id'];
	$vars['block_selected'] = 1;
} elseif (isset($_GET['product_id'])) { // per si estic al formulari,
	$query = "SELECT family_id,subfamily_id FROM product__product where product_id = " . R::id('product_id');
	$rs_form = Db::get_row($query);
	if ($rs_form['subfamily_id'] != 0) { // comprovo si aquest producte ve d'una subfamília
		$familia = $rs_form['subfamily_id'];
	} else {
		$familia = $rs_form['family_id'];
	}
} elseif (isset($_GET['filecat_file_name'])) { // de moment així, quan ja funcioni filecat_file_name s'haurà de fer la consulta
	$familia = R::id('filecat_file_name');
	$vars['block_selected'] = 1;
} else {
	$familia = 0;
	if (isset($_GET['family_id'])) {
		$vars['block_selected'] = 1;
		$famselect = R::number('family_id'); //ho faig servir per la família sel·leccionada
	}
}

// filtre
$family_ids = R::get( 'family_ids' );
if ($family_ids)	$vars['block_selected'] = 1;


$conta = 0;
$loop_count = count($results);
foreach ($results as $rs) {

	$l = [];

	// CONTA PRODUCTES, es mes rapid que posant la consulta com a subquery
	$product_count = Db::get_first("SELECT count(*) 
						FROM product__product
						WHERE (
						product__product.family_id = " . $rs['family_id'] . "
						|| product__product.subfamily_id = " . $rs['family_id'] . " )
						AND (status = 'onsale' OR status = 'nostock' OR status = 'nocatalog') 
						AND product__product.bin = 0");

	if ( $show_empty_familys || $product_count ) {
		$l['is_empty'] = !$product_count;
		if (USE_FRIENDLY_URL) {
			/*
			  $l['link'] = '/' . LANGUAGE . '/product/product/family_id/' . $rs['family_id'];
			  if (isset($_GET['page_file_name']))
			  {
			  $l['link'] .= '/' . $_GET['page_file_name'] . '.html';
			  }
			 */
			$l['link'] = Page::get_link('product', 'product', false, false, array(), false, '', $rs['family_id'], '');

/*			$l['link'] =
				get_link( array(
						'tool' => 'product',
						'tool_section' => 'product',
						'category' => 'family_id',
						'category_id' => $rs['family_id'],
						'detect_page' => true
				));*/


		} else {
			$l['link'] = '/?tool=product&amp;tool_section=product&amp;family_id=' . $rs['family_id'] . '&amp;language=' . LANGUAGE;
		}
		$l['id'] = $rs['family_id'];
		$l['family_description'] = nl2br(htmlspecialchars($rs['family_description'])); // per la home nomès
		$l['family_alt'] = htmlspecialchars($rs['family_alt']);
		$l['item'] = $rs['family'];
		$l['class'] = '';
		$l['conta'] = $conta++;
		$l['loop_count'] = $loop_count;

		if (($l['id'] == $familia) OR ($l['id'] == $famselect))
			$selected = 1;
		else
			$selected = 0;

		// pel filtre
		if ( $family_ids &&
		     in_array( $l['id'], $family_ids ) ) {
			$selected = 1;
		}

		$l['selected'] = $selected;

		if ($show_images)
			$l += UploadFiles::get_record_images($rs['family_id'], true, 'product', 'family');

		// miro per cada un si té subfamilies
		$query = "SELECT family, family_description, family_alt, product__family_language.family_id
						FROM product__family_language, product__family
						WHERE  product__family_language.family_id = product__family.family_id
						AND language = '" . LANGUAGE . "'
						AND product__family.parent_id= '" . $l['id'] . "'
						AND bin = 0
						ORDER BY ordre ASC, family";
		$results2 = Db::get_rows($query);

		$show = false;
		// comprovo si al llistat de productes o a la fitxa de producte està ja dins d'una subfamilia per deixar-lo obert
		if ($familia) {
			// consulto si aquesta familia es una subfamilia
			$query2 = "SELECT parent_id FROM product__family WHERE product__family.family_id = " . $familia;
			$resultssubfa = Db::get_rows($query2);
			if ($resultssubfa[0]['parent_id'] != 0) {
				$show = true;
			}
		}

		$loop2 = array(); // segon loop dins de loop
		$conta2 = 0;
		$loop_count2 = count($results2);

		if ($results2) {
			// vol dir que té subfamilies

			if (($show == true) AND ($l['id'] == $resultssubfa[0]['parent_id'])) {
				$l['display'] = 'inherit';
				$l['selected'] = 1; // mostro la familia com a seleccionada
			}
			else
				$l['display'] = 'none';

			foreach ($results2 as $rs2) {

				$l2 = array();

				// CONTA PRODUCTES, es mes rapid que posant la consulta com a subquery
				$sub_product_count = Db::get_first("SELECT count(*) 
							FROM product__product
							WHERE product__product.subfamily_id = " . $rs2['family_id'] . "
							AND (status = 'onsale' OR status = 'nostock' OR status = 'nocatalog') 
							AND product__product.bin = 0");

				if ( $show_empty_familys || $sub_product_count ) {

					$l2['is_empty'] = !$sub_product_count;
					if (USE_FRIENDLY_URL) {
						/* $l2['link'] = '/' . LANGUAGE . '/product/product/subfamily_id/' . $rs2['family_id'];
						  if (isset($_GET['page_file_name']))
						  {
						  $l2['link'] .= '/' . $_GET['page_file_name'] . '.html';
						  } */
						$l2['link'] = Page::get_link('product', 'product', false, false, array(), false, '', $rs2['family_id'], '');


/*						$l2['link'] =
							get_link( array(
									'tool' => 'product',
									'tool_section' => 'product',
									'category' => 'family_id',
									'category_id' => $rs2['family_id'],
									'detect_page' => true
							));*/


					} else {
						$l2['link'] = '/?tool=product&amp;tool_section=product&amp;subfamily_id=' . $rs2['family_id'] . '&amp;language=' . LANGUAGE;
					}
					$l2['id'] = $rs2['family_id'];
					$l2['item'] = $rs2['family'];
					$l2['family_description'] = $rs2['family_description'];
					$l2['family_alt'] = htmlspecialchars($rs2['family_alt']);
					$l2['class'] = '';
					$l2['conta'] = $conta2++;
					$l2['loop_count'] = $loop_count2;

					if ( ( $l2['id'] == $familia ) )
						$selected = 1;
					else
						$selected = 0;

					// pel filtre
					if ( $family_ids &&
					      in_array( $l2['id'], $family_ids ) ) {
						$selected = 1;
					}

					$l2['selected'] = $selected;


					if ($show_images)
						$l2 += UploadFiles::get_record_images($rs2['family_id'], true, 'product', 'family');

					include( DOCUMENT_ROOT . 'public/blocks/product_family_subfamily2.php' );

				}
			}
		}

		if ($loop2) {
			$loop2[0]['class'] = 'first';
			$loop2[count($loop2) - 1]['class'] = 'last';
			
			// poso be el loop_count, pot ser que alguna familia no tingui productes i llavors falla
			if (count($loop2) != $loop_count2) {
				foreach ($loop2 as &$val) {
					$val['loop_count'] = count($loop2);
				}
			}
		}


		$l['loop'] = $loop2;
		$loop[] = $l;

	}
}
if ($loop) {
	$loop[0]['class'] = 'first';
	$loop[count($loop) - 1]['class'] = 'last';
	// poso be el loop_count, pot ser que alguna familia no tingui productes i llavors falla
	if (count($loop) != $loop_count) {
		foreach ($loop as &$val) {
			$val['loop_count'] = count($loop);
		}
	}
	
} else {
	/* si no hi ha cap resultat no mostro el block */
	$show_block = false;
	return;
}
// Debug::pre( $loop );die();