gl_related_table_id = 0; // ej. id de la taula custumer a la qual s'han de assignar els propietaris
gl_default_input_prefix = 'selected_addresses';
gl_odd_even = 'Even';
gl_submit_addresses = true;
var addreses_selected = new Array();

function insert_address(table_id, name){	
	if (!addreses_selected[table_id]) //s'assegurar que no estigui ja marcada
	{
		gl_odd_even = gl_odd_even=='Even'?'Odd':'Even';   
		var start_html = '<table width="100%" border="0" cellspacing="0" cellpadding="0">' +
		       '<tr height="20" style="cursor:hand" class="listItem'+ gl_odd_even +
		 '" onclick="extract_address(this,\''+table_id+ 
		 '\')" onmouseover="change_bg_color(this, \'Over\',\''+table_id+
		 '\')" onmouseout="change_bg_color(this, \''+ gl_odd_even +
		 '\',\''+table_id+
		 '\')"  id="list_row_'+table_id+'"><td>';
		var end_html = '</td></tr></table>';
		addreses_selected[table_id] = name;
		var write_cell = document.getElementById('write_cell');
		write_cell.innerHTML = write_cell.innerHTML + start_html + name + end_html;		
		
		$('#listItems').contents().find('#list_row_'+table_id).attr('class','listItemDown');
		$('#listItems').contents().find('#list_row_'+table_id + ' td').css('cursor','default');
		
	}
	if (top.on_insert_address){ // si existeix la funció en el top la faig servir, així puc sobreescriure aquesta funcio segons l'eina on som
		top.on_insert_address(table_id, name);
		return;
	}
}

function extract_address(obj,table_id){ //funcio que elimina els seleccionats al clikar sobre	
  delete addreses_selected[table_id];
  //obj.parentNode.parentNode.outerHTML='';
  $(obj).parent().remove();
  
  $('#listItems').contents().find('#list_row_'+table_id).attr('class','listItemOdd');
  $('#listItems').contents().find('#list_row_'+table_id + ' td').css('cursor','pointer');
  
  gl_odd_even = gl_odd_even=='Even'?'Odd':'Even';
}

function pass_selected_addresses() // passa els resultats al formulari principal
{
	
	var path;
	
	if (parent.document.theForm) {
		obj = parent.document.theForm;
		path = parent;
	}
	else{
		obj=top.document.theForm?top.document.theForm:top.document.list;
		path = top;
	}	
	
	input_prefix = path.gl_default_input_prefix;	
	related_table = path.gl_related_table_id;	
	
	selected_addresses_ids = obj[input_prefix + '_ids']?obj[input_prefix + '_ids']:obj[input_prefix + '_ids' + related_table];
	selected_addresses_names = obj[input_prefix + '_names']?obj[input_prefix + '_names']:obj[input_prefix + '_names' + related_table];
	
	var ids = '', names = '', namesp = '';
    for (key in addreses_selected)
    {
      ids+= key +','; //property_id
      names+= addreses_selected[key] +'#;#'; //ref
      namesp+= addreses_selected[key] +'<br>'; // refs per visulitzar en el formulari
    }
    ids = ids.substr(0,ids.length-1);
    names = names.substr(0,names.length-3);
    namesp = namesp.substr(0,namesp.length-4);
	
    document.theForm.selected_addresses_ids.value = ids;
    document.theForm.selected_addresses_names.value = names;
    if (path.document.getElementById(input_prefix + '_text_selected')) path.document.getElementById(input_prefix + '_text_selected').innerHTML = namesp;
	
	if (selected_addresses_ids){		
		selected_addresses_ids.value = ids;
		selected_addresses_names.value = names;
	}
    if (path.gl_submit_addresses) document.theForm.submit();
	else path.hidePopWin(false) // tanca la finestra abans de fer el submit, s'ha de tancar al save_rows
}

function populate_table()
{
    // agafa valors del objectes del formulari o dels llistats
	
	var path;
	
	if (parent.document.theForm) {
		obj = parent.document.theForm;
		path = parent;
	}
	else{
		obj=top.document.theForm?top.document.theForm:top.document.list;
		path = top;
	}	
	
	input_prefix = path.gl_default_input_prefix;
	
	selected_addresses_ids = obj[input_prefix + '_ids']?obj[input_prefix + '_ids']:obj[input_prefix + '_ids' + path.gl_related_table_id];
	selected_addresses_names = obj[input_prefix + '_names']?obj[input_prefix + '_names']:obj[input_prefix + '_names' + path.gl_related_table_id];	
	
    if (typeof selected_addresses_ids != "undefined" && selected_addresses_ids.value)
    {
	  var start_html;
	  var end_html = '</td></tr></table>';
	  var write_cell = document.getElementById('write_cell');
      var ids  = selected_addresses_ids.value.split(',');
      var names  = selected_addresses_names.value.split('#;#');
	    dp   (ids);
	    dp   (names);
      for (key in ids)
      {
		gl_odd_even = gl_odd_even=='Even'?'Odd':'Even'; 
	   table_id = ids[key];
       var start_html = '<table width="100%" border="0" cellspacing="0" cellpadding="0">' +
		       '<tr height="20" style="cursor:hand" class="listItem'+ gl_odd_even +
		 '" onclick="extract_address(this,\''+table_id+ 
		 '\')" onmouseover="change_bg_color(this, \'Over\',\''+table_id+
		 '\')" onmouseout="change_bg_color(this, \''+ gl_odd_even +
		 '\',\''+table_id+
		 '\')"  id="list_row_'+table_id+'"><td>';
    	write_cell.innerHTML = write_cell.innerHTML + start_html + names[key] + end_html;
        addreses_selected[ids[key]] = names[key];	
      }
    }
}


var Assign = {
	show_selected: function(target_obj){
	
		// marco les varicions que ja tinc seleccionades i borro onclick
		ids = top.assign_selected_ids;
		
		for (key in ids){
			dp('#list_row_'+ids[key]);
			$('#list_row_'+ids[key],target_obj).attr('class','listItemDown');
			$('#list_row_'+ids[key] + ' td',target_obj).css('cursor','default');
			$('#list_row_'+ids[key],target_obj).attr('onclick','');
		}
	},
	// marca les seleccionades i les posa a la dreta
	insert_addresses: function(){	
		for (i=0;i<assign_selected.length;i++){	
			insert_address(assign_selected[i]['id'],assign_selected[i]['name']);	
		}
	},
	pass_selected_items: function(){
		
	}
};