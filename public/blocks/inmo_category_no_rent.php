<?php
$page_id = isset($vars['page_id']) ? $vars['page_id'] : false;

$vars['var_name'] = 'category_id';

$query = "SELECT Distinct category, inmo__category_language.category_id
				FROM inmo__category_language, inmo__property, inmo__category
				WHERE inmo__category_language.category_id = inmo__property.category_id
				AND inmo__category_language.category_id = inmo__category.category_id
				AND language = '" . LANGUAGE . "'
				AND inmo__property.bin = 0 AND  (status = 'onsale' OR status = 'sold' OR status = 'reserved')
 				ORDER by ordre, category";

$results = Db::get_rows($query);
Debug::add('Consulta categoris menu', $query);

if ($results)
{
    foreach($results as $rs)
    {
        (isset($_GET["category_id"]) && ($_GET["category_id"] == $rs["category_id"]))?$selected = 1:$selected = 0;

       		
		$l['link'] = 
				get_link( array(
						'tool' => 'inmo', 
						'tool_section' => 'property', 
						'category' => 'category_id',
						'category_id' => $rs['category_id'],
						'friendly_params' => array('tipus'=>INMO_DEFAULT_TIPUS=='sell'?'':'sell'),
						'link_vars' => array('tipus'=>'sell'),
						'detect_page' => $page_id?false:true,
						'page_id' => $page_id
				));
		
		
        $l['selected'] = $selected;
		$l['value'] = $rs['category_id'];
        $l['item'] = $rs['category'];
        $loop2[] = $l;
    }
    $loop[] = array ('loop'=>$loop2,
	'item' => '');
}
?>