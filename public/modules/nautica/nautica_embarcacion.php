<?
/**
 * NauticaEmbarcacion
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2011
 * @version $Id$
 * @access public
 */
class NauticaEmbarcacion extends Module{

	function __construct(){
		parent::__construct();
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function get_records()
	{
		$listing = new ListRecords($this);
		$listing->order_by = 'entered DESC, shipyard_id ASC';
		$listing->cols = $this->config['list_cols'];
		$listing->always_parse_template = true;
		$listing->call('records_walk','length,status');
		
		$listing->condition = "(status = 'onsale' OR status = 'reserved' OR status = 'sold')";
		
		// FILTRES
		
		if (isset($_GET['filecat_file_name'])) $_GET['tipus_id'] = $_GET['filecat_file_name'];
		
		// tipus menu
		if (isset($_GET['tipus_id']) && $_GET['tipus_id']!='null') $listing->condition .= " AND tipus_id = '".R::id('tipus_id')."'";
		
		// tipus filtre
		if (isset($_GET['tipus_ids']))  {
			R::escape_array($_GET['tipus_ids']);
			$listing->condition .= " AND tipus_id IN (".implode(',',$_GET['tipus_ids']).")";
		}
		
		// shipyard menu
		if (isset($_GET['shipyard_id']) && $_GET['shipyard_id']!='null') $listing->condition .= " AND shipyard_id = '".R::id('shipyard_id')."'";
		
		// shipyard filtre
		if (isset($_GET['shipyard_ids']))  {
			R::escape_array($_GET['shipyard_ids']);
			$listing->condition .= " AND shipyard_id IN (".implode(',',$_GET['shipyard_ids']).")";
		}
		
		// filtre eslores - metode separat per guions
		if (isset($_GET['length']) && $_GET['length']!='null') {
			$vals = explode('-',$_GET['length']);
			if (count($vals)==1){
			$listing->condition .= " AND length >= '".R::escape($vals[0],false,false)."'";
			}
			else{
			$listing->condition .= " AND (length >= '".R::escape($vals[0],false,false)."' AND length <'".R::escape($vals[1],false,false)."')";
			
			}
		}
		
		
		// filtre eslores - metode length1 i length2
		if (isset($_GET['length1']) && isset($_GET['length2'])){

			$length1 = R::escape('length1');
			$length2 = R::escape('length2');
			
			$listing->condition .= " AND (length >= '". $length1 ."' AND length <='".$length2."')";
		}
		
		
		// filtre preus - metode separat per guions
		if (isset($_GET['price']) && $_GET['price']!='null') {
			$vals = explode('-',$_GET['price']);
			if (count($vals)==1){
			$listing->condition .= " AND price >= '".R::escape($vals[0],false,false)."'";
			}
			else{
			$listing->condition .= " AND (price >= '".R::escape($vals[0],false,false)."' AND price <'".R::escape($vals[1],false,false)."')";
			
			}
		}
		
		
		// filtre p - metode price1 i price2
		if (isset($_GET['price1']) && isset($_GET['price2'])){

			$price1 = R::escape('price1');
			$price2 = R::escape('price2');
			
			$listing->condition .= " AND (price >= '". $price1 ."' AND price <='".$price2."')";
		}
		
		$q = R::escape('q');
		if ($q){
		
			$query = "SELECT shipyard_id FROM nautica__shipyard WHERE shipyard like '%" . $q . "%' ";
            $results = Db::get_rows ($query);
			
			$listing->condition .= " AND (model like '%" . $q . "%' OR
										engine like '%" . $q . "%' OR
										shipyard_id IN(
											SELECT shipyard_id 
											FROM nautica__shipyard 
											WHERE shipyard 
											LIKE '%" . $q . "%') OR
										tipus_id IN(
											SELECT tipus_id 
											FROM nautica__tipus_language 
											WHERE tipus 
											LIKE '%" . $q . "%')
											)
										";
		}
		
		// BUSCADOR
		
	    return $listing->list_records();
	}
	function records_walk($length,$status){		
		$rs['length_f'] = format_currency($length*3.2808399,2); // passo a feet
		$rs['status_code'] = $status;
		return $rs;
	}
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form($is_pdf=false)
	{
	    $show = new ShowForm($this);
		
		if (!$is_pdf || !isset($_SESSION['user_id']))
			$show->condition = "(status = 'onsale' OR status = 'reserved' OR status = 'sold')";
			
		$this->set_field('entered','default_value',now());
		$this->set_field('weight','type','decimal');
		
		$max_dossier = Db::get_first("SELECT MAX(dossier) FROM nautica__embarcacion") + 1;
		$this->set_field('dossier','default_value',$max_dossier );
		$show->get_values();
		if (!$show->has_results) Main::error_404();
		
		$show->rs['weight'] = $show->rs['weight']/1000; // passo a tonelades
		$show->rs['length_f'] = format_currency($show->rs['length']*3.2808399,2); // passo a feet
		$show->rs['breadth_f'] = format_currency($show->rs['breadth']*3.2808399,2); // passo a feet
		$show->rs['depth_f'] = format_currency($show->rs['depth']*3.2808399,2); // passo a feet
		$show->rs['depth2_f'] = format_currency($show->rs['depth2']*3.2808399,2); // passo a feet
		$show->rs['status_code'] = $show->rs['status'];
		$this->set_details($show);
		
		$show->set_var('title_form_nautica',$show->rs['dossier'] . ' - ' . $show->rs['model']);
		
		if ($is_pdf) return $show;		
		
		//   /spa/nautica/embarcacion/v/action/save_pdf/rio-450-sol.htm	
		//Debug::p($show, 'text');
		
		if ($show->rs['embarcacion_file_name']){
			$show->set_var('print_pdf_link','/'.LANGUAGE .'/nautica/embarcacion/v/action/print_pdf/'.$show->rs['embarcacion_file_name'].'.htm');
			$show->set_var('save_pdf_link','/'.LANGUAGE .'/nautica/embarcacion/v/action/save_pdf/'.$show->rs['embarcacion_file_name'].'.htm');
		}
		else{
			$show->set_var('print_pdf_link','/'.LANGUAGE .'/nautica/embarcacion/print_pdf/embarcacion_id/'.$show->id.'/');
			$show->set_var('save_pdf_link','/'.LANGUAGE .'/nautica/embarcacion/save_pdf/embarcacion_id/'.$show->id.'/');
		}
		
	    return $show->show_form();
	}
	function print_pdf($destination = 'i'){		
		
		$this->action='show_record';
		$show = $this->get_form(true);
		
		$show->set_record();
		
		$pdf = new PDF('P', 'mm', 'A4', true, 'UTF-8', false);
		$pdf->is_admin=$GLOBALS['gl_is_admin'];
		$pdf->destination = $destination;
		$pdf->path = CLIENT_PATH . 'templates/print/';
		$pdf->show_pdf($show);
	}
	function save_pdf(){
		$this->print_pdf('d');
	}
	
	function set_details(&$show){
		// Obtenir valors i array per elbucle de detalls
		$rs = &$show->rs;
		$add = array(
			'weight' => ' t.',
			'length' => ' m.' . ' | ' . $rs['length_f'] . ' ft.',
			'breadth' => ' m.' . ' | ' . $rs['breadth_f'] . ' ft.',
			'depth' => ' m.' . ' | ' . $rs['depth_f'] . 'ft. ',
			'fuel_tank' => ' l.',
			'water_tank' => ' l.'
		);
		
		if ( $rs['depth2']) $add['depth'] .= '<span>' . $rs['depth2'] . ' m. | ' . $rs['depth2_f'] . ' ft.</span>';			
		
		/* TOTS ELS CAMPS DE LA TAULA
		$fields = array(	
			embarcacion_id,clau,dossier,entered,status,tipus_id,shipyard_id,model,buildyear,engine,fuel,length,breadth,weight,depth,depth2,flag_id,hour_use,price,vhf,depthsounder,speedlog,radar,gps,plotter,autopilot,trimmtabs,searchlight,electrical_capstan,gangway,liferaft,tender,offshore_connexion,battery_charger,generator,cover,camping_cover,bimini_top,bowthruster,hot_water,davit,anemometer,mainsail,genoa,mizzen,jib,stormsail,forsail2,spinaker,spi_boom,roller_genoa,equipament,music_equipment,television,attached_engine,fridge,heating,air_conditioning,teka,wc,black_water,roller_mainsail
			
		);
		*/
		$main_fields = 'tipus_id,shipyard_id,model,buildyear,engine,engine_type,fuel,fuel_tank,water_tank,flag_id,hour_use,length,breadth,depth,weight';
		
		
		$fields_arr['main'] = explode(',',$main_fields);
		
		$fields_arr['electronics'] = array(	
			'vhf','depthsounder','speedlog','radar','gps','plotter','anemometer','autopilot','music_equipment','television','compass'
		);
		$fields_arr['extras'] = array(	
			'trimmtabs', 'searchlight', 'electrical_capstan', 'gangway', 'liferaft', 'tender', 'attached_engine', 'offshore_connexion', 'fridge', 'battery_charger', 'generator', 'cover', 'camping_cover', 'bimini_top', 'bowthruster', 'hot_water', 'davit','heating','air_conditioning','teka','wc','black_water','bath_stair','trim','kitchen','microwave','shower'
		);
		$fields_arr['sails'] = array(	
			'mainsail','genoa','mizzen','jib','stormsail','forsail2','spinaker','spi_boom','roller_mainsail','roller_genoa'
		);
		
		$rs['electronics'] = $rs['extras'] = $rs['sails'] = array();
		
		foreach($fields_arr as $key=>$fields)
		{
			$is_odd = false;
			foreach($fields as $field)
			{
				if ($rs[$field] && $rs[$field]!='0000') // l'any pot ser '0000' quan no s'ha entrat cap
				{
					$is_odd=$is_odd?false:true; // intercanviio el valor de is_odd
					
					$new_array = array('c_detail' => $show->caption['c_'.$field],'detail' => &$rs[$field],'add' => '','is_odd' => $is_odd);
					
					// si el camp te unitat li poso enla variable units
					if (isset($add[$field])) $new_array['add'] = $add[$field];
					
					$rs[$key][]= $new_array;				
				}
			}
		}
	}
}
if (is_file(CLIENT_PATH . 'templates/print/nautica_embarcacion_pdf.php')){
	include(CLIENT_PATH . 'templates/print/nautica_embarcacion_pdf.php');	
}
else {
	include(DOCUMENT_ROOT . 'admin/modules/nautica/nautica_embarcacion_pdf.php');
}
?>