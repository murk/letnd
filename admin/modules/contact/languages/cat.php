<?php
// menus eina
define('CONTACT_MENU_CONTACT','Contacte');
define('CONTACT_MENU_LIST','E-mails de contacte');

// captions seccio
$gl_caption_contact ['c_company'] = 'Empresa';
$gl_caption_contact ['c_name'] = 'Nom';
$gl_caption_contact ['c_surnames'] = 'Cognoms';
$gl_caption_contact ['c_mail'] = 'E-mail';
$gl_caption_contact ['c_to_address'] = 'Per';
$gl_caption_contact ['c_phone'] = 'Telèfon';
$gl_caption_contact ['c_matter'] = 'Assumpte';
$gl_caption_contact ['c_comment'] = 'Observacions';
$gl_caption_contact ['c_accept_info'] = 'Permet enviar informació';
$gl_caption_contact_list ['c_accept_info'] = 'Permet info';
$gl_caption ['c_sendmail'] = 'Enviar correu';
$gl_messages ['added'] = 'Formulari enviat correctament.';
$gl_caption ['c_sendok'] = $gl_messages ['added'];
$gl_messages ['not_saved '] = 'No s\'ha pogut enviar l\'E-mail.';

$gl_caption_contact['c_nif']='N.I.F / D.NI.';
$gl_caption_contact['c_company']='Empresa';

$gl_caption_contact['c_contact_language']='Idioma pàgina de contacte';
// NO TRADUIT
$gl_caption_contact['c_assign_custumer']='Afegir a clients';
$gl_caption_contact['c_assign_newsletter']='Afegir a Newsletter';
$gl_caption_contact['c_entered']= 'Enviat';

$gl_caption_contact['c_export_button']= 'Exportar';
?>