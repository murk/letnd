$(document).ready(function(){
  //init_values(); no va no se per que
});
$(window).bind("load", function(){
  init_values(); // aquest si va
}); 

var arrRelated_options = new Array(); // holds values for each object
var arrRelated_options2 = new Array(); // holds values for each object

var selFamilies,selsubfamilies,selsubsubfamilies;  // Name and Values Select object

var dosIniciat = false;

function init_values()
{
	product_id = $('input[name=product_id_javascript]').val();
	selFamilies = $('select[name="family_id['+product_id+']"]').get(0);
	selsubfamilies = $('select[name="subfamily_id['+product_id+']"]').get(0);
	selsubsubfamilies = $('select[name="subsubfamily_id['+product_id+']"]').get(0);
	selFamilies.onchange = change_values;
	selsubfamilies.onchange = change_values2;
	
	for (var i=0; i != selFamilies.length; i++)
	{ 
		arrRelated_options[selFamilies.options[i].value] = new RelatedOptions(selFamilies.options[i].value, selsubfamilies);
	}
	arrRelated_options[0] = new RelatedOptions(0, selsubfamilies);
	
 	for (var i=0; i != selsubfamilies.length; i++)
	{
		arrRelated_options2[selsubfamilies.options[i].value] = new RelatedOptions(selsubfamilies.options[i].value, selsubsubfamilies);
	}
	arrRelated_options2[0] = new RelatedOptions(0, selsubsubfamilies);
	  
	dosIniciat = true;
	change_values();
	change_values2();
	//load_all()
}

function change_values()
{
	var i = selFamilies.options[selFamilies.selectedIndex].value;
	if (arrRelated_options[i]) arrRelated_options[i].show();
  	if (dosIniciat)change_values2();
  
}

function change_values2()
{
	var i = selsubfamilies.options[selsubfamilies.selectedIndex].value;
	if (arrRelated_options2[i]) arrRelated_options2[i].show();
	//if (dosIniciat)change_values3();
}

// -------------------
// class related_options
// -------------------


function RelatedOptions(_id, selValues)
{
	_id= "#"+_id+"#";
	this.values =  new Array();
	this.texts =   new Array();
	this.selectedIndex = 0;
	this.show = show;
	
	// store values and text in each array 
	var _text, count = 0;
	for(var i=0; i!=selValues.length; i++)
	{ 
		if (selValues.options[i].text.indexOf(_id)==0)
		{
			this.values[count] = selValues.options[i].value;
			_text =  selValues.options[i].text;
			this.texts[count] = _text.substring(_id.length,_text.length);
			count++;
      // es un més ja que tenim un valor per defecte
      if (selValues.selectedIndex==i) this.selectedIndex = count;
		}    
	}
	
	// reset and repopulate the Select
	function show()
	{
		selValues.length = this.values.length+1;
    
		selValues.options[0].value = 0;
		selValues.options[0].text = '';
			
		for(var i=0; i!=this.values.length; i++)
		{
			selValues.options[i+1].value = this.values[i];
			selValues.options[i+1].text = this.texts[i];
		}
    selValues.selectedIndex=this.selectedIndex;
	}
}

