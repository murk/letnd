<?php

/* 19-12-14 
  
 * Booking: promcode que es pugui aplicar infinites vegades

 */

	
	
if (is_table('booking__promcode') && !(is_field('booking__promcode','one_time_only'))){
	
	$update_sql .= "
	ALTER TABLE `booking__promcode`
	ADD COLUMN `one_time_only` TINYINT(1) NOT NULL DEFAULT '1' AFTER `minimum_amount`;
	";	
	
}
else{
	if (is_table('booking__promcode')) {		
		trigger_error('Ja existeixen el field booking__promcode.one_time_only');
	}
}
	
?>