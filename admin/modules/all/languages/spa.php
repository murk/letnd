<?php
// menus eina
if (!defined('ALL_MENU_PAGE')){
	
	define('ALL_MENU_PAGE','Edició de pàgines');
	define('ALL_MENU_PAGE_NEW','Nova pàgina');
	define('ALL_MENU_PAGE_LIST','Llistar pàgina');
	define('ALL_MENU_PAGE_LIST_BIN','Paperera');
}


$gl_caption_page['c_ordre'] = 'Orden';
$gl_caption_page['c_template'] = 'Plantilla';
$gl_caption_page['c_link'] = 'Link';
$gl_caption_page_list['c_page_title'] = 'Título buscadores';
$gl_caption_page_list['c_page_file_name'] = 'Nombre pàgina';
$gl_caption_page['c_page_file_name'] = $GLOBALS['gl_caption']['c_file_name'];
$gl_caption_page['c_page_old_file_name'] = $GLOBALS['gl_caption']['c_old_file_name'];
$gl_caption_page['c_page'] = 'Nombre';
$gl_caption_page['c_title'] = 'Título en el cuerpo';
$gl_caption_page['c_body_subtitle'] = 'Subtítulo';
$gl_caption_page['c_body_description'] = 'Descripción corta';
$gl_caption_page['c_content'] = 'Contenido';
$gl_caption_page['c_prepare'] = 'En preparación';
$gl_caption_page['c_review'] = 'Para revisar';
$gl_caption_page['c_public'] = 'Público';
$gl_caption_page['c_archived'] = 'Archivado';
$gl_caption_page['c_status'] = 'Estado';
$gl_caption_page['c_page_id'] = 'Id';
$gl_caption_page['c_parent_id'] = 'Parent';
$gl_caption_page['c_level'] = 'Nivel';
$gl_caption_page['c_menu'] = 'Menú';
$gl_caption_page['c_blocked'] = 'Block.';
$gl_caption_page['c_filter_parent_id'] = 'Parent';
$gl_caption_page['c_filter_level'] = 'Level';
$gl_caption_page['c_has_content'] = 'Content';
$gl_caption_page['c_has_content_0'] = 'no';
$gl_caption_page['c_has_content_1'] = 'si';
$gl_caption_page['c_edit_content_button'] = 'Editar página';
$gl_caption_page['c_edit_page_button'] = 'Editar datos';
$gl_caption_page['c_filter_menu2'] = 'Menú de navegación';

$gl_caption_page['c_data'] = 'Datos de la página';
$gl_caption_page['c_seo'] = 'Optimitzación buscadores';

?>