<?
/**
 * DirectoryDocumentuser
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2011
 * @version $Id$
 * @access public
 */
class DirectoryDocumentuser extends Module{

	function __construct(){
		parent::__construct();
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = '<style>table.listRecord tr {cursor:pointer;}</style>' . $this->get_records();
	}
	function get_records()
	{	
		return $this->_execute_module(str_replace('list_','get_',$this->action));
	}
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{
		return $this->_execute_module(str_replace('show_','get_',$this->action));
	}
	function manage_images()
	{
		$this->_execute_module();
	}
	function search(){
		$this->_execute_module();
	}
	function _execute_module($action=''){
		// $this->name = 'instalacio'; // si vull canviar el nom del parent		
		$module = Module::load('directory','category', $this);
		$action = $action?$action:$this->action;
		return $module->do_action($action);	
	}
}
?>