<?php
// menus eina
if (!defined('ATOT_MENU_ESPECTACLE')){
	define('ATOT_MENU_ESPECTACLE_NEW','Entrar nou espectacle');
	define('ATOT_MENU_ESPECTACLE','Espectacles');
	define('ATOT_MENU_ESPECTACLE_LIST','Llistar espectacles');
	define('ATOT_MENU_ESPECTACLE_BIN','Paperera de reciclatge');
	define('ATOT_MENU_MUNICIPI','Poblacions');
	define('ATOT_MENU_MUNICIPI_NEW','Entrar nova població');
	define('ATOT_MENU_MUNICIPI_LIST','Llistar poblacions');
	define('ATOT_MENU_MUNICIPI_BIN','Paperera de reciclatge');
	
	define('ATOT_MENU_REQUISIT','Requisits');
	define('ATOT_MENU_REQUISIT_NEW','Entrar nou requisit');
	define('ATOT_MENU_REQUISIT_LIST','Llistar requisits');
	define('ATOT_MENU_REQUISIT_BIN','Paperera de reciclatge');
	
	define('ATOT_MENU_CAMPANYA','Campanyes');
	define('ATOT_MENU_CAMPANYA_NEW','Entrar nova campanya');
	define('ATOT_MENU_CAMPANYA_LIST','Llistar campanyes');
	define('ATOT_MENU_CAMPANYA_BIN','Paperera de reciclatge');
	
	define('ATOT_MENU_CURS','Cursos');
	define('ATOT_MENU_CURS_NEW','Entrar nou curs');
	define('ATOT_MENU_CURS_LIST','Llistar cursos');
	define('ATOT_MENU_CURS_BIN','Paperera de reciclatge');
	
	define('ATOT_MENU_LLOC','Llocs');
	define('ATOT_MENU_LLOC_NEW','Entrar nou lloc');
	define('ATOT_MENU_LLOC_LIST','Llistar llocs');
	define('ATOT_MENU_LLOC_BIN','Paperera de reciclatge');
}

// captions seccio
$gl_caption_espectacle['c_prepare'] = 'En preparación';
$gl_caption_espectacle['c_review'] = 'Para revisar';
$gl_caption_espectacle['c_public'] = 'Público';
$gl_caption_espectacle['c_archived'] = 'Archiivado';
$gl_caption_espectacle['c_entered'] = 'Fecha creación';
$gl_caption_espectacle['c_modified'] = 'Fecha modificación';
$gl_caption_espectacle['c_status'] = 'Estado';
$gl_caption_espectacle['c_in_home'] = 'Home';
$gl_caption_espectacle['c_in_home_0']='No aparece en la Home';
$gl_caption_espectacle['c_in_home_1']='Aparece en la Home';
$gl_caption_espectacle['c_destacat'] = 'Nuevo';
$gl_caption_espectacle['c_destacat_0']='No es nuevo';
$gl_caption_espectacle['c_destacat_1']='Es nuevo';
$gl_caption_espectacle['c_only_campanya'] = 'Sólo campañas';
$gl_caption_espectacle['c_only_campanya_0']='En toda la web';
$gl_caption_espectacle['c_only_campanya_1']='Sólo sale en campanyas';
$gl_caption_espectacle['c_durada'] = 'Duración';
$gl_caption_espectacle['c_autor_espectacle'] = 'Autores del espectáculo';
$gl_caption_espectacle['c_direccio'] = 'Dirección escénica';
$gl_caption_espectacle['c_animacio'] = 'Animaciones';
$gl_caption_espectacle['c_autor_musica'] = 'Autores de la música';
$gl_caption_espectacle['c_interpret'] = 'Intérprets';
$gl_caption['c_aforament_maxim'] = 'Aforo máximo';
$gl_caption_espectacle['c_tipus_concert'] = 'Tipo de concierto';
$gl_caption_espectacle['c_tipus_concert_tots'] = 'Todos';
$gl_caption_espectacle['c_tipus_concert_familiar'] = 'Familiar';
$gl_caption_espectacle['c_tipus_concert_escolar'] = 'Escolar';
$gl_caption_espectacle['c_rang_edat'] = 'Rango de edad';
$gl_caption_espectacle['c_rang_edat_0'] = 'Todas las edades';
$gl_caption_espectacle['c_rang_edat_1'] = 'de 0 a 3';
$gl_caption_espectacle['c_rang_edat_2'] = 'de 3 a 6';
$gl_caption_espectacle['c_rang_edat_3'] = 'de 6 a 9';
$gl_caption_espectacle['c_rang_edat_4'] = 'de 9 a 12';
$gl_caption_espectacle['c_rang_edat_5'] = 'de a 16';
$gl_caption_espectacle['c_file']='Archivos';

$gl_caption_espectacle['c_filter_tipus_concert'] = 'Todos los tipos de concierto';
$gl_caption_espectacle['c_filter_status'] = 'Todos los estados';
$gl_caption_espectacle['c_filter_in_home'] = 'Totdas, home o no';
$gl_caption_espectacle['c_filter_destacat'] = 'Todos, nuevos o no';
$gl_caption_espectacle['c_filter_only_campanya'] = 'Todos o només campanyes';

$gl_caption_espectacle['c_espectacle'] = 'Espectáculo';
$gl_caption_espectacle['c_content'] = 'Descripción';
$gl_caption_espectacle['c_content_list'] = 'Descripció breve en el listado';

$gl_caption_espectacle['c_url1_name']='Nombre URL(1)';
$gl_caption_espectacle['c_url1']='URL(1)';
$gl_caption_espectacle['c_url2_name']='Nombre URL(2)';
$gl_caption_espectacle['c_url2']='URL(2)';
$gl_caption_espectacle['c_url3_name']='Nombre URL(3)';
$gl_caption_espectacle['c_url3']='URL(3)';
$gl_caption_espectacle['c_videoframe']='Video (youtube, metacafe...)';
$gl_caption_espectacle['c_videoframe2']='Video 2';
$gl_caption_espectacle['c_videoframe3']='Video 3';

$gl_caption_espectacle['c_tipus_audicio'] = 'Tipo de audición';
$gl_caption['c_genere'] = 'Género o estilo musical';
$gl_caption_espectacle['c_formacio'] = 'Formación musical';
$gl_caption_espectacle['c_familia_instrument'] = 'Família de instrumentos';
//$gl_caption_espectacle['c_requeriment_espai'] = '';

$gl_caption_municipi['c_municipi'] = 'Población';
$gl_caption_municipi['c_descripcio'] = 'Descripción campañas';
$gl_caption['c_municipi_id'] = 'Población';
$gl_caption['c_municipi'] = 'Población';
$gl_caption['c_curs_id'] = 'Curso';
$gl_caption_espectacle['c_curs_id'] = 'Rango de edad <br>( sólo familiar )';
$gl_caption['c_curs'] = 'Curso';
$gl_caption['c_lloc_id'] = 'Lugar';
$gl_caption['c_lloc'] = 'Lugar';
$gl_caption['c_espectacle_id'] = 'Espectáculo';
$gl_caption['c_espectacle'] = 'Espectacle';
$gl_caption['c_ordre'] = 'Orden';
$gl_caption['c_preu'] = 'Precio';
$gl_caption['c_edat1'] = 'Edad 1';
$gl_caption['c_edat2'] = 'Edad 2';

$gl_caption['c_data'] = 'Fechas';
$gl_caption['c_horari'] = 'Horarios';
$gl_caption['c_inscripcio'] = 'Inscripción';

$gl_caption_requisit['c_requisit'] = 'Requisito';
$gl_caption_espectacle['c_requisit_id'] = 'Requisitos';


$gl_caption['c_espectacle_file_name'] = $GLOBALS['gl_caption']['c_file_name'];
$gl_caption['c_espectacle_old_file_name'] = $GLOBALS['gl_caption']['c_old_file_name'];
		
?>
