<?
/**
 * BookingBook
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) Maig 2014
 * @version $Id$
 * @access public
 *
 * TODO-i Falla el javascript per la data de naixement, quan no es correcta no diu res ( almenys al entrar acompanyant )
 * TODO-i Al entrar acompanyant s'hauria de comprovar primer si s'ha guardat el formulari, sino demanar per guardar
 *
 *
 * Actualització dels TT tasa turística
 *
 *
 UPDATE `easybrava`.`booking__book`
	SET
	`price_total` = price_total + price_tt,
	`payment_2`= payment_2 + price_tt,
	`price_tt` = price_tt*2
	WHERE
	date_in > '2017/03/31' AND price_tt <> 0.00;
 *
 *
 */
class BookingBook extends Module{
	
	var $show_booking, $show_results, $url, $one_property, $propertys, $property_id, $is_ajax = false, $reserved_message, $price_module, $is_custumer_form = false, $q, $condition, $is_search = false, $search_vars = array(), $search_fields, $selected_fields = array(), $custumer_fields = array(),  $custumer_selected_fields = array(), $extra_fields = array(),  $extra_selected_fields = array(), $extra_selected_types = array(), $is_excel, $results_subquery, $excel_formats = array();
	
	function __construct(){
		parent::__construct();
		
	}
	function on_load() {

		include_once 'booking_price.php';
		$this->price_module = New BookingPrice( $this );
		$this->price_module->action = $this->action;

		$this->show_booking = $GLOBALS['gl_is_admin'] ? true : false;
		$this->show_results = false;

		// captions de custumers i property
		$this->caption['c_ref'] = $this->caption['c_property_id'];

		$this->init_search_advanced();

	}
	function list_records()
	{
		$GLOBALS['gl_content'] = '<script type="text/javascript" src="/admin/modules/booking/jscripts/functions.js?v='.$GLOBALS['gl_version'].'"></script>' . $this->get_search_form() .  $this->get_records();
	}
	function get_records()
	{
		Main::load_class('booking', 'common', 'admin');
		$this->init_portal_form_vars();

		$is_tool_home = (get_all_get_params(array('menu_id')))?false:true;
		$custumer_id = R::get( 'custumer_id' );
		$and = "";
		
		if ($this->selected_fields) $this->redo_fields();
		
		
	    $listing = new ListRecords($this);
		$listing->caption['c_ref'] = $listing->caption['c_property_id'];
		$listing->set_field('ref', 'change_field_name', '(SELECT ref FROM inmo__property WHERE inmo__property.property_id =  booking__book.property_id) as ref');


		$listing->add_swap_edit();
		$listing->has_bin = false;
		
		if ($this->process == 'past_bookings') {	
			$listing->condition = "date_out < DATE_FORMAT(NOW() ,'%Y-%m-01')";
			$and = ' AND ';
		}
		elseif ($is_tool_home) {
			// per defecte nomes mostra d'avui en endavant		
			// de moment desconcto, la gent no ho enten --- $listing->condition = "date_out >= DATE_FORMAT(NOW() ,'%Y-%m-01')";
			// $and = ' AND ';
		}

		// Cerques
		if ($this->is_search) {
			// trec qualsevol altra condicio anterior
			$listing->condition = $this->condition;
		}
		
		// per llistat desde la fitxa de l'inmoble
		if ($this->property_id){
			
			$listing->add_button ('edit_button', '' , 'boto1', 'auto','booking.show_form_edit');
			$listing->set_options(1, 1, 1, 1, 1, 1, 0, 0);
			
			$listing->condition .= $and . 'property_id = ' . $this->property_id;
			$and = ' AND ';
			$listing->form_link = "/admin/?tool=booking&tool_section=book&process=save_property_rows&property_id=" . $this->property_id;

			$listing->unset_field('ref');
		}
		else{
			$listing->add_button ('edit_book_button', '', 'boto1', 'before','booking.show_form_edit_book');
			$listing->set_options(1, 1, 1, 1, 1, 1, 0, 0, 1, 1);
			$listing->has_images = true;
			$this->config['im_admin_thumb_w'] = Db::get_first("SELECT value FROM inmo__configadmin WHERE name = 'im_admin_thumb_w'");
			$this->config['im_admin_thumb_h'] = Db::get_first("SELECT value FROM inmo__configadmin WHERE name = 'im_admin_thumb_h'");

			$listing->set_field('ref', 'type', 'none'); // per poder posar enllaç

			// per llistat desde custumer /admin/?menu_id=5028
		    if ($custumer_id){
			    $listing->join = 'INNER JOIN booking__book_to_custumer USING (book_id)';
			    $listing->condition .= $and . 'custumer_id = ' . $custumer_id;
				$and = ' AND ';
		    }
			$book_id = R::id( 'book_id' );

			// per llistar una sola reserva ( faig servir des del mail de vreasy )
		    if ($book_id){
			    $listing->condition .= $and . 'book_id = ' . $book_id;
				$and = ' AND ';
		    }
		}

		if ($this->is_custumer_form) {
			$listing->set_options(0, 0, 0, 0, 0, 0, 0, 0, 0, 1);
			$listing->unset_field( 'custumers' );
			$listing->is_sortable = false;
		}
		
		if ($this->config['has_contracts']) {			
			$listing->add_button('download_contract','tool=booking&tool_section=book&action=download_contract', 'boto2', 'after', '', '', false, false);
		}
		
		
		$listing->call('records_walk','season_id',true);
		$listing->always_parse_template = true;

		if ($is_tool_home && $this->process != 'past_bookings'){
			$listing->order_by = 'book_id DESC';
			// quan es mostrava només les reserves d'ara en endavant - $listing->order_by = 'date_in ASC';
		}
		else {
			$listing->order_by = 'book_id DESC';
			// quan es mostrava només les reserves d'ara en endavant  $listing->order_by = 'date_in DESC';
		}
		
		$this->set_var('back_button', $listing->last_listing['link']);

		// Debug::p( 'listing condition', $listing->condition );

		if ($this->is_excel){
			$listing->limit = 5000;

			// poso els mateixos captions que tinc a el desplegable per triar camps
			foreach ( $this->search_fields as $key => $val ) {
				$listing->caption[ 'c_' . $key ] = $val;
			}

			$listing->set_records();
			// $listing->list_records( false ); // així no va, perque ?

			$listing->call_after_set_records( 'excel_after_records_walk', 'results' );
			$this->export( $listing->results );
		}
		else {
			return $listing->list_records();
		}
	}
	public function excel_after_records_walk( $rs ){

		// custumers
		$id = $rs['book_id'];
		$q = New Query('booking__book_to_custumer');
		$q->set_id_field( 'book_id' );
		$q->order_by = 'ordre ASC';
		$custumers = $q->get_imploded('custumer_id',$id);

		if ($custumers) {
			$results = Db::get_rows( "
		SELECT CONCAT(name, ' ', surname1, ' ', surname2) AS custumer
			FROM custumer__custumer
			WHERE " . 'custumer_id IN (' . $custumers . ')
			AND bin = 0' );

			$ret['custumers'] = implode_field( $results, 'custumer', ', ' );
		}
		else {
			$ret['custumers'] = '';
		}

		return $ret;
	}

	function records_walk(&$listing) {
			
		$id = $listing->rs['book_id'];
		$date_in = $listing->rs['date_in'];
		$date_out = $listing->rs['date_out'];
		$property_id = $listing->rs['property_id'];
		$price_total = $listing->rs['price_total'];
		$is_owner =  $listing->rs['is_owner'];
		$status =  $listing->rs['status'];
		if (isset($listing->rs['ref'])) $ref =  $listing->rs['ref'];
		else $ref = false;

		
		/*
		  if (isset($listing->buttons['edit_book_button'])){
		  $listing->buttons['edit_book_button']['link_end'] ='&amp;property_id=' . $property_id . $listing->buttons['edit_book_button']['link_end'];
		  }

		 */
		
		
		$q = New Query('booking__book_to_custumer');
		$q->set_id_field( 'book_id' );
		$q->order_by = 'ordre ASC';
		$custumers = $q->get_imploded('custumer_id',$id);

		$ret = array();

		// ref inmobles
		if ($ref) $ret['ref'] = '<a href="/admin/?menu_id=103&action=show_form_edit&property_id=' . $property_id . '">' . $ref . '</a>';

		// custumers
		$ret += $this->get_custumers($custumers, $id);

		// extres
		$ret += $this->get_extras( $id, $property_id );

		// obtinc imatge
		$ret += UploadFiles::get_record_images($property_id, false,  'inmo', 'property');
				
		// $ret['price_total'] = '<div class="price-total">' . format_currency($price_total) . ' €</div>';
		$ret['price_total'] = format_currency($price_total);

		// boto contracte nomes quan hi ha cusutmer principal
		$custumer_id = BookingCommon::get_main_custumer($id);
		
		if ($this->config['has_contracts']) {
			
			// busco idioma preferit del usuari
			if ($custumer_id) {
				$prefered_language = Db::get_first("
					SELECT prefered_language 
						FROM custumer__custumer 
						WHERE custumer_id = " . $custumer_id);
				if ( !in_array($prefered_language, $GLOBALS['gl_languages']['public'])){
					$prefered_language = $this->config['contract_default_language'];					
				}
				
				$listing->buttons['download_contract']['params'] = '&language=' . $prefered_language;
			}

			//$listing->buttons['download_contract']['show'] = $custumer_id!==false && !$is_owner;

			/* Tots els estats ocupats
			(( $status == 'booked' || $status == 'confirmed_email' || $status == 'confirmed_post' || $status == 'contract_signed'));
			*/

			// No mostro contracte només si s'ha cancelat
			$listing->buttons['download_contract']['show'] =
				$custumer_id!==false &&
				!$is_owner &&
				( $status != 'cancelled' && $status != 'pre_booked');
		}

		// vreasy

		if ($_SESSION['inmo_vreasy']) {
			$book_owner =  $listing->rs['book_owner'];
			$vreasy_id =  $listing->rs['vreasy_id'];
			$vreasy_image = DIR_TEMPLATES . 'images/vreasy.png';
			if ($book_owner == 'vreasy') {
				$ret ['book_owner'] = "<a target='_blank' href='https://www.vreasy.com/calendar/#reservation/$vreasy_id'><img style='margin-top:-3px' src='$vreasy_image' alt='Vreasy' width='38' height='18'></a>";
			}
			else {
				$ret ['book_owner'] = '';
			}
		}


		// TEST Obtinc els preu de nou per comprovar el TT ( el canvi de 0.45 a 0.9 )
		/*Main::load_class('booking', 'price', 'admin');
		if (!isset($GLOBALS['tt_ids'])) $GLOBALS['tt_ids'] = [];
		$price_module = New BookingPrice($this);

		$adult =  $listing->rs['adult'];
		$child =  $listing->rs['child'];
		$price_tt =  $listing->rs['price_tt'];
		$prices = $price_module->get_price(
						$date_in,
						$date_out,
						0,
						$property_id,
						false,
						false,
						$adult,
						$child
				);
		// Debug::p ( $prices, 'prices' );
		if (((float)$price_tt) != 0.00 && (100*$price_tt != 100*$prices['price_tt_value']) ) {
			$GLOBALS['tt_ids'] [$id] = $id;
			$prices['price_tt_value'] .= 'SIII';
		}
		$ret ['price_total'] = (float)$price_tt . ' nou: ' . $prices['price_tt_value'];
		echo ( implode (',' , $GLOBALS['tt_ids'] ) . "<br>");*/
		// FI TEST
		
		return $ret;
		
	}
	
	// funcio que es crida desdel modul de propietats
	public function show_property_books()
	{		
		include_once 'booking_calendar.php';
		$calendar = New BookingCalendar($this->process);
		
		$property_id = $this->property_id = R::id('property_id');
		
		//$this->check_availability();
		$calendar_arr = $calendar->get_calendar_array();
		$this->propertys = $calendar->propertys;
		$this->set_vars($calendar_arr);
		
		//$book_form = $this->do_action('get_form_new');
		
		$book_list = $this->do_action('get_records');
		
		
				
		$this->tpl->set_vars(array(
			'propertys' => $this->propertys,
			'book_list' => $book_list,
			));
		
		$this->tpl->file = 'inmo/property_book.tpl';
		
		$rs = Db::get_row('SELECT ref, property_private, property_id FROM inmo__property WHERE property_id = ' . $property_id);
		$this->tpl->set_vars($rs);
		
		Main::load_class('booking', 'common', 'admin');
		BookingCommon::set_book_vars($this,$property_id);
		
		$GLOBALS['gl_content'] = $this->process();
	}
	/*function get_form_ajax() {
		$this->is_ajax = true;
		Debug::p_all();
		die($this->do_action('get_form_edit'));
		
	}*/
	function get_rest_of_custumers_ajax  () {
				
		$rs['book_id'] = R::number('book_id');
		$rs['adult'] = R::number('adult');
		$rs['child'] = R::number('child');
		$rs['baby'] = R::number('baby');
		$rs['custumers'] = R::get('custumers');
		$book_id = R::id('book_id');
		
		$custumer_id = isset($rs['custumers'][1])?$rs['custumers'][1]:false;
		unset($rs['custumers'][1]);
		
		$rest_of_custumers = $this->get_rest_of_custumers($rs, $custumer_id);
		
		
		$this->set_file('booking/book_form_custumers.tpl');
		
		$this->set_vars(array(
			'rest_of_custumers' => $rest_of_custumers,
			'id' => $book_id
			));		
		
		$this->set_vars($this->caption);	
		
		$ret ['html'] = $this->process();		
		
		die (json_encode($ret));
	}
	function get_rest_of_custumers(&$rs, $custumer_id = false) {		
		
		if (!$custumer_id && !empty($rs['book_id'])){
		
			$custumer_id = Db::get_first("
				SELECT custumer_id 
				FROM booking__book_to_custumer 
				WHERE book_id = " . $rs['book_id'] . "
				AND ordre <2" );
			
		}
		
		
		Main::load_class('booking', 'common', 'admin');
		$bcommon = New BookingCommon();
		$bcommon->caption = &$this->caption;
		return $bcommon->get_rest_of_custumers($rs, $custumer_id, $this->action, true);
		
	}
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{
		$this->init_portal_form_vars();
		
		$property_id = R::id('property_id');
		$date_in = R::escape('date_in');
		$date_out = R::escape('date_out');


		$this->set_field('property_id','default_value',$property_id);
		$this->set_field('date_in','default_value',$date_in);
		$this->set_field('date_out','default_value',$date_out);
		
		
		Main::load_class('booking', 'common', 'admin');
		BookingCommon::set_book_vars($this, $property_id,false);		
		
		$show = new ShowForm($this);
		$show->has_bin = false;	
		$show->get_values();

		$book_owner = isset($show->rs['book_owner']) ? $show->rs['book_owner']: 'letnd';
		$is_owner_vreasy =  $_SESSION['inmo_vreasy'] && $this->action != 'show_form_new' && $book_owner != 'letnd';

		// si editem, agafo de la bbdd els preus
		if ($show->id) {
			$show->set_field('price_book','form_admin','text');
			$show->set_field('price_extras','form_admin','text');
			$show->set_field('price_total','form_admin','text');
			$is_manual_price = $show->rs['is_manual_price'];
			$show->set_var('manual_price_book_class', $is_manual_price ? '' : 'hidden');
			$show->set_var('automatic_price_book_class', $is_manual_price ? 'hidden' : '');
		}
		else {
			$show->set_var('manual_price_book_class', 'hidden');
			$show->set_var('automatic_price_book_class', '');
		}
		
		if($property_id===false){
			
			$property_id = $show->rs['property_id'];
			
		}
		
			
		if ($show->rs){
			$price_date_in = $show->rs['date_in'];
			$price_date_out = $show->rs['date_out'];
			$adult = $show->rs['adult']; // pel preu
			$child = $show->rs['child'];
			$date_in = format_date_form($show->rs['date_in']);
			$date_out = format_date_form($show->rs['date_out']);
		}
		else{
			$price_date_in = unformat_date($date_in, false);
			$price_date_out = unformat_date($date_out, false);	
			$adult = R::escape('adult'); // pel preu
			$child = R::escape('child');
			//$date_in = $date_in;
			//$date_out = $date_out;
		}

		// Pagaments
		/*$show->set_field( 'payment_done_1', 'type', 'checkbox' );
		$show->set_field( 'payment_done_1', 'enabled', '0' );
		$show->set_field( 'payment_done_2', 'type', 'checkbox' );
		$show->set_field( 'payment_done_2', 'enabled', '0' );
		$show->set_field( 'payment_done_3', 'type', 'checkbox' );
		$show->set_field( 'payment_done_3', 'enabled', '0' );
		$show->set_field( 'payment_done_4', 'type', 'checkbox' );
		$show->set_field( 'payment_done_4', 'enabled', '0' );

		// poso el valor a 1 per que quedi marcat el checkbox
		for ($i = 1; $i <= 4; $i++ ){
			if (isset($show->rs['payment_done_' . $i]) && $show->rs['payment_done_' . $i] != 0) $show->rs['payment_done_' . $i] = 1;
		}*/

		// Dades de l'inmoble
		$rs_property = Db::get_row("
			SELECT person, ref, property_private
			FROM inmo__property WHERE property_id = " . $property_id);
		$show->set_vars($rs_property);		
		
		
		// resta de custumers
		// $rest_of_custumers = $is_owner_vreasy?'':$this->get_rest_of_custumers($show->rs);
		$rest_of_custumers = $this->get_rest_of_custumers($show->rs);

		// obtinc extres
		$module = Module::load('booking', 'extra', $this);
		$extras = $module->get_book_extras($show->id, $property_id);
		
		$show->set_vars(array(
			'date_in_text' => $date_in,
			'date_out_text' => $date_out,
			'rest_of_custumers' => $rest_of_custumers,
			'extras' => $extras,
			'season_styles' => $this->get_season_styles()
			));
		$show->set_vars(
				$this->price_module->get_price(
						$price_date_in,
						$price_date_out, 
						$show->id, 
						$property_id,
						true,
						true,
						$adult,
						$child)
				);		

		// Nomes lectura per vreasy
		if ($is_owner_vreasy) {

			//$show->set_field( 'adult', 'form_admin', 'text' );
			//$show->set_field( 'child', 'form_admin', 'text' );
			// $show->set_field( 'baby', 'form_admin', 'text' );
			$show->set_field( 'date_in', 'form_admin', 'text' );
			$show->set_field( 'date_out', 'form_admin', 'text' );
			// $show->set_field( 'status', 'form_admin', 'text' );

		}
		/*else {
		}*/
		// Select del filtre
		$this->set_filter_status( $show );

		$content = $show->show_form();
		
		$javascript = '';
		if ($this->is_ajax){
			$javascript = 
				'<script type="text/javascript">
				$(document).ready(				
					function ()
						{			
							' . $GLOBALS['gl_page']->javascript . '		 
						}
				)
				</script>';
		}
		return $javascript . $content;
		
	}

	private function init_portal_form_vars () {

		if ($_SESSION['inmo_vreasy']) {
			$this->add_field( 'book_owner', [
				'type' => 'none',
				'list_admin' => 'text',
				'form_admin' => '0',
				'default_value' => 'letnd',
			] );
			$this->add_field( 'vreasy_id', [
				'type' => 'none',
				'list_admin' => '0',
				'form_admin' => '0',
				'default_value' => '0',
			] );
			$this->caption['c_vreasy_id'] = '';
		}

	}

	private function set_filter_status(&$show) {


		$show->set_field( 'status', 'type', 'none' );

		$status_value = $show->rs['status'];
		if ( $show->action == 'show_form_new' ) {
			$status_value = $this->config['default_status'];
		}
		$id = $show->id;

		$status = $this->get_filter_status( $status_value, $id );

		$status .= '<input name="old_status['. $id . ']" value="" type="hidden">';

		$show->rs['status'] =  $status;

	}
	private function get_filter_status($status_value = '', $id = false, $caption = '') {

		if ( $id === false ) {
			$status = '
			<select name="status">';
		}
		else{
			$status = '
			<select id="status_' . $id . '" name="status[' . $id . ']">';

		}
		if ( $caption ) {
			$status .= '<option value="null">' . $caption . '</option>';
		}

		$status .= '
				<optgroup label="' . $this->caption['c_status_group_free'] . '">
				<option value="pre_booked">' . $this->caption['c_status_pre_booked'] . '</option>
				<option value="cancelled">' . $this->caption['c_status_cancelled'] . '</option>
				<optgroup label="' . $this->caption['c_status_group_not_free'] . '">
				<option value="booked">' . $this->caption['c_status_booked'] . '</option>
				<option value="confirmed_email">' . $this->caption['c_status_confirmed_email'] . '</option>
				<option value="confirmed_post">' . $this->caption['c_status_confirmed_post'] . '</option>
				<option value="contract_signed">' . $this->caption['c_status_contract_signed'] . '</option>
			</select>';

		if ($status_value)
			$status = str_replace( 'value="' . $status_value . '"', 'value="' . $status_value . '" selected', $status );

		return $status;
	}
	
	function get_season_styles() {
		
		Main::load_class('booking', 'common');
		Main::load_class('booking', 'calendar');
		$seasons_colors = BookingCommon::get_seasons_colors();
		return BookingCalendar::get_season_styles($seasons_colors);
		
	}

	public function send_mail_ajax(){

		$book_id = R::id( 'book_id', '', '_POST' );
		$payment_number = R::id( 'payment_number', '', '_POST' );
		$payment = R::escape( 'payment', '', '_POST' );
		$payment_rest = R::escape( 'payment_rest', '', '_POST' );
		$payment_date = R::escape( 'payment_date', '', '_POST' );

		$ret = array();
//		$ret ['error'] = '';
//		$ret ['message'] = '';

		Main::load_class('booking', 'common', 'admin');
		$bcommon = New BookingCommon();

		$custumer_id = $bcommon->get_main_custumer( $book_id );
		$property_id = Db::get_first( "SELECT property_id FROM booking__book WHERE book_id = " . $book_id );

		$custumer = Db::get_row( "SELECT mail, name, surname1, surname2, prefered_language FROM custumer__custumer WHERE custumer_id = " . $custumer_id );

		$mail = $custumer['mail'];
		$name = $custumer['name'] . ' ' . $custumer['surname1'] . ' ' . $custumer['surname2'];
		$name = trim( $name );

		if ($custumer['prefered_language'] != LANGUAGE){
			include( DOCUMENT_ROOT . 'admin/modules/booking/languages/' . $custumer['prefered_language'] . '.php' );

			if (file_exists(CLIENT_PATH . 'languages/' . $custumer['prefered_language'] . '_admin.php'))
					include (CLIENT_PATH . 'languages/' . $custumer['prefered_language'] . '_admin.php');

			$caption = &$gl_caption_book;

		}
		else {
			$caption = &$this->caption;
		}


//		if ($mail){ /////// si envio el mail sense anar a la newsletter

		$subject = sprintf( $caption['c_payment_send_mail_subject'], $book_id );
		$message = sprintf( $caption['c_payment_send_mail_body'], $name, $payment, $book_id, $payment_date );

		if ( $payment_rest != '0,00' ) {
			$message .= sprintf( $caption['c_payment_send_mail_body_rest'], $payment_rest );
		}

		$message .= sprintf( $caption['c_payment_send_mail_body2'] );


//			$sent = $bcommon->send_mail( $body, $subject, $mail, $this->config );
//			if ( $sent ) {
//				$ret ['message'] = $caption['c_payment_mail_sent'] . ' ' . $mail;
//			}
//			else {
//				$ret ['error'] =  $caption['c_payment_mail_not_sent'] . ' ' . $mail;
//			}
//		}
//		else {
//			$ret ['error'] =  $caption['c_payment_mail_not_sent_no_mail'];
//		}

		$subject = urlencode( $subject );
		$message = urlencode( $message );

		$ret['custumer_id'] = $custumer_id;
		$ret['property_id'] = $property_id;
		$ret['subject'] = $subject;
		$ret['message'] = $message;
		$ret['prefered_language'] = $custumer['prefered_language'];

		die (json_encode($ret));

	}
	function save_rows()
	{
		Main::load_class('booking', 'common', 'admin');

	    $save_rows = new SaveRows($this);


		$notify_ids = BookingCommon::set_portal_on_save( $save_rows );

	    $save_rows->save();

	    BookingCommon::export_to_portal($save_rows->id, $this->action);

	    if ( $notify_ids ) {
		    $save_rows->notify( $notify_ids, $save_rows->caption['c_books_not_deleted_not_owner']);
	    }

	}

	function set_write_payments (&$writerec){
		$total_payments = 4;
		$id = $writerec->id;

		if ($id == 0) return; // no es fa mai en una reserva nova

		$sql = '';
		for ( $i = 1; $i <= $total_payments; $i ++ ) {
			$val = $writerec->get_value('payment_' . $i);
			$checked = $writerec->get_value('order_status_' . $i) == 'completed';


			if ($checked) {
				$writerec->set_value('payment_done_' . $i,$val);
				$val = unformat_currency($val);
				$sql .= "payment_done_$i = $val,";
			}
			else {
				$writerec->set_value('payment_done_' . $i,0);
				$sql .= "payment_done_$i = 0.00,";
			}
		}
		$sql = substr($sql, 0, -1);

		Db::execute( "UPDATE booking__book SET " . $sql . " WHERE book_id='$id';" );
	}

	function write_record()
	{
		global $gl_message, $gl_page;

		Main::load_class('booking', 'common', 'admin');
		$bcommon = New BookingCommon();
		
		/////////// Borrar
		if ($this->action == 'delete_record')
		{
			/*$book_id = $_POST['book_id'];
			$rs = GetOne("SELECT date_in, date_out, property_id
							FROM booking__book
							WHERE book_id = '$book_id'");
			extract($rs);

			$writerec = new SaveRows($this);
			if ($this->one_property)
			{
				$writerec->message_deleted = sprintf($this->messages['deleted'], format_date_form($date_in), format_date_form($date_out));
			}
			else
			{
				$property = Db::get_row("SELECT property_private FROM inmo__property WHERE property_id = $property_id");
				$writerec->message_deleted = sprintf($this->messages['deleted2'], $property, format_date_form($date_in), format_date_form($date_out));
			}
			$writerec->save();
			$_SESSION['message'] = $gl_message;
			print_javascript("top.window.location.reload();");
			 *
			 */
			die();
		}
				
		
		///////// Guardar

		$writerec = new SaveRows( $this );

		BookingCommon::set_portal_on_save( $writerec );

		$this->set_write_payments( $writerec );
		
		$book_id = $writerec->id;
		$adult = $writerec->get_value('adult');
		$children = $writerec->get_value('child');
		$baby = $writerec->get_value('baby');
		$status = $writerec->get_value('status');
		$is_owner_vreasy = Db::get_first("SELECT count(*) FROM booking__book WHERE book_id = '$book_id' AND book_owner = 'vreasy'");

		$property_id = $this->property_id =   $writerec->get_value('property_id');		
		// si no hi ha property_id ( quan editem nomes reserva) l'agafo de la reserva
		if (!$property_id){
			$property_id = Db::get_first("SELECT property_id FROM booking__book WHERE book_id = " . $book_id);
		}
		
		$date_in2 = $writerec->get_value('date_in'); 
		$date_out2 = $writerec->get_value('date_out'); 	
		
		$date_in = unformat_date($date_in2);
		$date_out = unformat_date($date_out2);
		
		// giro les dates si la de començament es major que la final
		if (is_date_bigger($date_in, $date_out)){
			
			$t = $date_in2;
			$date_in2 = $date_out2;
			$date_out2 = $t;
			
			$_POST['date_in'][$book_id] = $date_in2;
			$_POST['date_out'][$book_id] = $date_out2;
			
			$t = $date_in;
			$date_in = $date_out;
			$date_out = $t;
			
		}	

		$property = Db::get_first("SELECT property_private FROM inmo__property WHERE property_id = $property_id");

		$is_reserved = $is_owner_vreasy?false:$this->check_is_reserved($date_in, $date_out, $date_in2, $date_out2, $book_id, $property_id, $adult, $children, $baby, $status);

		if (!$is_reserved)
		{
			if ($this->action == "save_record")
			{
					$writerec->message_saved = sprintf($this->messages['saved2'], $property, $date_in2, $date_out2);
			}
			else
			{
					$writerec->message_added = sprintf($this->messages['added2'], $property, $date_in2, $date_out2);					
			}

			
			

			
			$writerec->save();
			
			
			// en cas que hi hagi quantitat, borro si es 0 perque no es grabi a la bbdd
			foreach ($_POST['extra_book_selected'] as $extra_id){
				if(isset($_POST['quantity'][$extra_id]) && $_POST['quantity'][$extra_id]==0)
					unset($_POST['extra_book_selected'][$extra_id]);
			}
			
			// guardo altres custumers		
			$book_id = $writerec->id;
			$selected_custumers = R::post('selected_custumer_id');	

			if ($selected_custumers){
				R::escape_array($selected_custumers);
				
				$main_custumer_id = $selected_custumers[1];
				Db::execute("
					UPDATE custumer__custumer 
					SET book_group=" . $main_custumer_id . ", book_main=1 
					WHERE custumer_id=" . $main_custumer_id);
					
				$query = New Query('', 'booking__book_to_custumer','book_id,ordre','custumer_id');
				foreach ($selected_custumers as $ordre => $selected_custumer_id) {
					if ($selected_custumer_id) {						
						$query->show_querys = true;
						$query->save_related($book_id, $ordre, $selected_custumer_id);
						Db::execute("
							UPDATE custumer__custumer 
							SET book_group=" . $main_custumer_id . " 
							WHERE custumer_id=" . $selected_custumer_id);
					}
				}
				// TODO-i Mirar perque no vull que borri tots els registres quan no hi ha cap seleccionat, en el cas municipis a demandes, si que els hauria de borrar tots
				$query->delete_others(); // agafa el primer fixe sempre
				unset($query);
			}
			
			// relaciona amb extres
			Debug::p($_POST, $book_id);
			
			$query = New Query('', 'booking__extra_to_book', 'book_id,extra_id', 'extra_price');
			$query->messages = &$this->messages;
			$query->set_fixed_value($book_id);
			$query->set_config ('int','int','currency');		
			$query->save_related_post('extra_book_selected');
			
			// actualitzo quantitat
			$quantity = R::post('quantity');
			Debug::add('Quantity',$quantity);
			if ( $quantity ) {
				foreach ( $quantity as $key => $val ) {
					if ( $val != '0' ) {
						$query = "
    						UPDATE booking__extra_to_book 
    							SET quantity= '" . $val . "'
    							WHERE book_id = '" . $book_id . "' 
    							AND extra_id = '" . $key . "'";
						Db::execute( $query );
						Debug::add( 'Quantity', $query );
					}
				}
			}
			
			// Obtinc preu per guardar amb la reserva, abans de save();
			$prices = $this->price_module->get_price(
					$date_in,
					$date_out, 
					$book_id,
					$property_id,
					true,
					true,
					$adult,
					$children
				);

			// Guardo el promcode
			Debug::p('Main custumer', $main_custumer_id);
			Debug::p($prices,'Preus');
			if ($prices['promcode_id']){
				Debug::add('Main custumer', $main_custumer_id);
				if ($main_custumer_id){
					Db::execute("INSERT INTO booking__custumer_to_promcode 
								(custumer_id, promcode_id) VALUES
								('". $main_custumer_id ."', '" . $prices['promcode_id'] . "');");
				}
				else{
					$GLOBALS['gl_mesage'] = $this->caption['c_promcode_err_not_main_custumer'];
					$prices['promcode_discount_value'] = 0; // No guardo descompte
				}
			}

			// Comprovo si es sobreescriu el preu
			$is_manual_price = $writerec->get_value('is_manual_price');
			if ($is_manual_price) {
				$manual_price_book = unformat_currency(R::post( 'manual_price_book' ));
				$manual_price_extras = unformat_currency(R::post( 'manual_price_extras' ));
				$prices['promcode_discount_value'] = 0;
				$prices['price_book_value'] = $manual_price_book;
				$prices['price_extras_value'] = $manual_price_extras;
				$prices['price_total_value'] = $manual_price_book + $manual_price_extras + $prices['price_tt_value'];
			}
			
			// Guardo preus a la reserva
			Db::execute("
				UPDATE booking__book
						SET 
						promcode_discount=".$prices['promcode_discount_value'].",
						price_book=".$prices['price_book_value'].", 
						price_extras=".$prices['price_extras_value'].", 
						price_tt=".$prices['price_tt_value'].", 
						price_total=".$prices['price_total_value']." 
					WHERE book_id='".$book_id."';
				");


			if ($this->action=='add_record') {

				// Poso valors inicials de payment_1 i payment_2 i contracte
				$initial_rs = $bcommon->get_initial_percent();

				if ($initial_rs['whole_import'] == '0') {
					$payment_1 = round( $prices['price_book_promcode_discounted_value'] * $initial_rs['initial_percent'] / 100, 2 );
				}
				else {
					$payment_1 = round( $prices['price_total_value'] * $initial_rs['initial_percent'] / 100, 2 );
				}
				$payment_2 = $prices['price_total_value'] - $payment_1;

				Db::execute("
				UPDATE booking__book
						SET
						payment_1=".$payment_1.",
						payment_2=".$payment_2.",
						contract_id=".$initial_rs['contract_id']."
					WHERE book_id='".$book_id."';
				");

			}

			// Marco com a propietari fora el add_record ja que es pot canviar el prop principal, a public no
			if ($prices['is_owner']){
				Db::execute("
						UPDATE booking__book
							SET is_owner=1 WHERE
							book_id='".$book_id."';
					");
			}
			
			// jscript
			
			
			if ($this->action == 'add_record' || $this->action == 'save_record'){
				$js = "
					top.booking.set_booking(".$book_id.",'".$date_in."','".$date_out."')
				";

				print_javascript($js);
			}
			
			// recarrego llistat reserves
			// TODO-i Falta el javascript al llistat de reserves
			$book_list = $this->do_action('get_records');
			/*print_javascript('
				if (!top.$(\'#list-records\').length)) {
					top.window.location.reload();
				}
			');*/
			set_inner_html('list-records',$book_list);

			$gl_page->show_message($gl_message);

			// Exporto al portal
            BookingCommon::export_to_portal($book_id, $this->action);
			
			Debug::p_all();
			die();
		}
		// si està ocupada la reserva
		else
		{	
			$gl_message = $this->reserved_message;
			
			
			print_javascript("alert( " . json_encode($this->reserved_message). " );");
			//$gl_page->show_message($this->reserved_message,'parent.parent');
			
			Debug::p_all();
			die();
		}
			

	}
	
	function check_is_reserved($date_in, $date_out, $date_in2, $date_out2, $book_id, $property_id, $adult, $children, $baby, $status) {
		
		Main::load_class('booking', 'common', 'admin');
		$bcommon = New BookingCommon();
		$bcommon->messages = &$this->messages;
		$bcommon->reserved_message = &$this->reserved_message;

		if ($bcommon->ignore_is_reserved( $date_in, $date_out, $book_id, $status, $this->action )) return false;

		return $bcommon->check_is_reserved(
				$date_in, $date_out, $date_in2, $date_out2, $book_id, $property_id, $adult, $children, $baby, "\n");
		
	}
	
	
	/*
	 * Calendari
	 */
	
	public function get_month() {
			
		include_once 'booking_calendar.php';
		$calendar = New BookingCalendar($this->process);
		
		return $calendar->get_month();
	}




	//
	// FUNCIONS BUSCADOR
	//
	function search(){

		$q = $this->q = R::escape('q');

		$search_condition = '';

		$this->is_search = true;

		if ($q)
		{
			$search_condition = " (
				comment like '%" . $q . "%'
				OR book_entered like '%" . $q . "%'
				OR date_in like '%" . $q . "%'
				OR date_out like '%" . $q . "%'
				" ;
			if(strpos($q,'/')){

				$date_arr = explode('/',$q);
				$date = '';
				$date_arr = array_reverse( $date_arr );

				if ( count( $date_arr ) == 3 ) {
					$date = implode( '-', $date_arr );
					$search_condition .= "
							OR book_entered = '" . $date . "'
							OR date_in = '" . $date . "'
							OR date_out = '" . $date . "'
						";
				}
				elseif ( count( $date_arr ) == 2 ) {
					$search_condition .= "
							OR (
								YEAR (book_entered) = '" . $date_arr[0] . "'
								AND MONTH (book_entered) = '" . $date_arr[1] . "')
							OR (
								YEAR (date_in) = '" . $date_arr[0] . "'
								AND MONTH (date_in) = '" . $date_arr[1] . "')
							OR (
								YEAR (date_out) = '" . $date_arr[0] . "'
								AND MONTH (date_out) = '" . $date_arr[1] . "')
						";
				}
			}



			// busco a clients
			$query = "
					SELECT book_id
						FROM custumer__custumer
						INNER JOIN booking__book_to_custumer USING (custumer_id)
						WHERE (
							name like '%" . $q . "%'
							OR surname1 like '%" . $q . "%'
							OR surname2 like '%" . $q . "%'
							OR CONCAT(TRIM(name), ' ', TRIM(surname1)) like '%" . $q . "%'
							OR CONCAT(TRIM(surname1), ' ', TRIM(surname2)) like '%" . $q . "%'
							OR CONCAT(TRIM(name), ' ', TRIM(surname1), ' ', TRIM(surname2)) like '%" . $q . "%'
							OR vat like '%" . $q . "%'
					)
					AND (
						custumer__custumer.bin = 0 AND custumer__custumer.activated = 1
					)
			" ;

			// Debug::p($query);

			$results = Db::get_rows_array($query);

			$search_condition .= $this->get_ids_query($results, 'book_id');

			$search_condition .= ") " ;

			// Per referencia
			$search_condition = $this->get_ref_query('book_id',$q, $search_condition);


			$GLOBALS['gl_page']->title = TITLE_SEARCH . '<strong>&nbsp;&nbsp;"' . $q . '"</strong>';
		}
		// per fer un reset de una cerca, busco cadena buida
		else{
			Main::redirect('/admin/?menu_id=' . $GLOBALS['gl_menu_id']);

		}
		$this->condition = $search_condition;
		Debug::add('Search condition', $this->condition);
		$this->do_action('list_records');
	}

	function get_ids_query(&$results, $field)
	{
		if (!$results) return '';
		$new_arr = array();
		foreach ($results as $rs){
			$new_arr [$rs[0]] = $rs[0];
		}
		return "OR " . $field . " IN (" . implode(',', $new_arr) . ") ";
	}
	function get_ref_query($field, $q, $search_condition){
		//
		// busqueda automatica de referencies separades per espais o comes
		//
		$ref_array = explode (",", $q); // si l'array hem dona 1 probo de fer-ho amb espais, si es una sola referncia donarà el mateix resultat
		count($ref_array) == 1?$ref_array = explode (" ", $q):false;

		foreach($ref_array as $key => $value)
		{
			if (!is_numeric($value))
			{
				$ref_array = '';
				break;
			}
		}

		// consulta
		if ($ref_array)
		{
			$query_ref = "";
			foreach ($ref_array as $key)
			{
				$query_ref .= $field . " = '" . $key . "' OR ";
			}
			$query_ref = "(" . substr($query_ref, 0, -3) . ") ";

			return '( ' . $query_ref . ' OR ' . $search_condition . ')';
		}
		else{
			return $search_condition;
		}
	}

	function get_search_form(){
		$tpl = new phemplate(PATH_TEMPLATES);
		$tpl->set_vars($this->caption);
		$tpl->set_file('booking/book_search.tpl');
		$tpl->set_var('menu_id',$GLOBALS['gl_menu_id']);
		$tpl->set_var('q',  htmlspecialchars(R::get('q')));
		$this->set_search_advanced_vars();

		$tpl->set_vars( $this->search_vars );

		$ret = $tpl->process();

		if ($this->q) {
			$GLOBALS['gl_page']->javascript .= "
				$('table.listRecord').highlight('".addslashes(R::get('q'))."');
			";
		}


		return $ret;
	}




	/*
	 *
	 *
	 * Funcions filtres buscador
	 *
	 *
	 * */


	function get_custumers($ids, $book_id) {

		//if (!$book_id) $book_id = R::escape ('book_id',false,'_GET');
		//if (!$book_id) $book_id = R::escape ('book_id',false,'_POST');

		if (!$ids) return array();

		/*
		// quan edito desde el llistat
		if ($ids == 0){
			$q = New Query('booking__book_to_custumer');
			$q->id_field = 'book_id';
			$ids = $q->get_imploded('custumer_id', $book_id);
		}
		 *
		 */


		if ($this->is_excel) {

			if ($this->custumer_selected_fields) {
				$fields = array_keys( $this->custumer_selected_fields );
				$fields = implode( ',', $fields );
				// si te name, afegeixo els cognoms que van tots junts
				$fields = str_replace( 'name', 'CONCAT_WS(" ",name,surname1,surname2) AS name', $fields );

				// que agafi tota l'adressa
				$fields = str_replace( 'adress', 'adress,numstreet,block,flat,door', $fields );
				$fields = str_replace( 'country', '(SELECT all__country.country FROM all__country WHERE all__country.country_id = custumer__custumer.country) AS country', $fields );
				$fields = str_replace( 'nationality', '(SELECT all__country.country FROM all__country WHERE all__country.country_id = custumer__custumer.country) AS nationality', $fields );

				$results = Db::get_rows( "
				SELECT " . $fields . "
					FROM custumer__custumer
					WHERE " . 'custumer_id IN (' . $ids . ')' );

				$addres_fields = array ('adress','numstreet','block','flat');
				foreach ($results as &$rs) {
					$adress = array();
					foreach ($addres_fields as $f){
						if (!empty($rs[$f])) $adress[] = $rs[$f];
					}
					$rs['adress'] = implode(', ', $adress);

					if (!empty($rs['door'])) $rs['adress'] .= '-' . $rs['door'];
				}
				Debug::p( $results, 'Resultats per custumer' );

				$this->results_subquery[ $book_id ] = $results;
			}
			return array();
		}



		$module = Module::load('custumer', 'custumer', '', false);

		$listing = new ListRecords($module);

		if ($this->custumer_selected_fields){
			foreach ($this->custumer_selected_fields as $field => $val) {
				$listing->set_field($field,'list_admin', $val);
				if ($val == 'no')
					$listing->set_var( $field, '' );
			}
		}

		$listing->set_var('book_id',$book_id);
		$listing->name = 'custumers';
		$listing->condition = 'custumer_id IN ('.$ids.')';
		$listing->template = 'booking/book_custumer_list';
		$listing->order_by = 'FIELD(custumer_id,'.$ids.')';
		$listing->set_records();

		$custumers['custumers'] = $listing->process();

		return $custumers;
	}



	function get_extras ($book_id, $property_id) {

		//obtinc extres
		$module = Module::load('booking', 'extra', $this);
		$extras = $module->get_book_extras($book_id, $property_id, false, true);

		if (!$this->extra_selected_fields) {
			return array();
		}

		$results = array();

		$types = array( 'included', 'optional', 'required' );
		foreach ( $types as $type ) {
			// nomes el tipos seleccionats, o si no s'ha seleccionat cap tipo es mostren tots
			if ( ! empty ( $this->extra_selected_types[ 'extra_' . $type ] ) || ! $this->extra_selected_types ) {

				foreach ( $extras[ $type . 's' ] as $extra ) {
					$r = array();
					foreach ( $this->extra_selected_fields as $field => $val ) {
						$r[ $field ] = $extra[ $field ];
					}
					$results [] = $r;
				}

			}
		}


		if ($this->is_excel) {

			if ( isset( $this->results_subquery[ $book_id ] ) ) {
				// Array merge em suma els resutats al final, el que vull es barreja el custumer 0 amb el extra 0, el custumer 1 amb el extra 1 ....
				// $results = array_merge($this->results_subquery[ $book_id ], $results);
				$new_results     = array();
				$current_results = &$this->results_subquery[ $book_id ];
				$max             = count( $current_results );
				$max             = count( $results ) > $max ? count( $results ) : $max;

				for ( $i = 0; $i < $max; $i ++ ) {
					$array1         = isset( $current_results[ $i ] ) ? $current_results[ $i ] : array();
					$array2         = isset( $results[ $i ] ) ? $results[ $i ] : array();
					$new_results [] = array_merge( $array1, $array2 );
				}
				$results = $new_results;
			}
			$this->results_subquery[ $book_id ] = $results;

			return array();
		}

		$tpl = new phemplate(PATH_TEMPLATES);
		$tpl->set_var( 'results', true );
		$tpl->set_loop( 'loop', $results );
		$tpl->set_file('booking/book_listing_extras_list.tpl');

		$extras['extras'] = $tpl->process();

		return $extras;
	}


	function init_search_advanced () {

		if ( $this->is_index ) {

			include_once( DOCUMENT_ROOT . 'admin/modules/custumer/languages/' . LANGUAGE . '.php' );

			$this->caption['c_phone1'] = $gl_caption['c_phone1'];
			$this->caption['c_phone2'] = $gl_caption['c_phone2'];
			$this->caption['c_phone3'] = $gl_caption['c_phone3'];
			$this->caption['c_phone4'] = $gl_caption['c_phone4'];
			$this->caption['c_adress'] = $gl_caption_custumer['c_address'];
			$this->caption['c_town'] = $gl_caption_custumer['c_town'];
			$this->caption['c_province'] = $gl_caption_custumer['c_province'];
			$this->caption['c_country'] = $gl_caption_custumer['c_country'];
			$this->caption['c_nationality'] = $gl_caption_custumer['c_nationality'];
			$this->caption['c_zip'] = $gl_caption_custumer['c_zip'];
			$this->caption['c_bank'] = $gl_caption_custumer['c_bank'];
			$this->caption['c_bic'] = $gl_caption_custumer['c_bic'];
			$this->caption['c_comta'] = $gl_caption_custumer['c_comta'];
			$this->caption['c_vat'] = $gl_caption_custumer['c_vat'];
		}

		$this->is_excel = R::get( 'excel' );

		$this->excel_formats = array (
			'book_id' => 'FORMAT_NUMBER',
			'phone1' => 'FORMAT_TEXT',
			'phone2' => 'FORMAT_TEXT',
			'phone3' => 'FORMAT_TEXT',
			'phone4' => 'FORMAT_TEXT',
			'compta' => 'FORMAT_TEXT',
			'vat' => 'FORMAT_TEXT',
			'payment_rest' => 'FORMAT_NUMBER_COMMA_SEPARATED1',
			'price_extras' => 'FORMAT_NUMBER_COMMA_SEPARATED1',
			'price_tt' => 'FORMAT_NUMBER_COMMA_SEPARATED1',
			'price_book' => 'FORMAT_NUMBER_COMMA_SEPARATED1',
			'price_total' => 'FORMAT_NUMBER_COMMA_SEPARATED1',
			'extra_total_price_value' => 'FORMAT_NUMBER_COMMA_SEPARATED1',
		);
	}



	function set_search_advanced_vars () {

		// si ja he cridat la funcio, retorno ( desde advanced es crida 2 cops )
		if ( $this->search_vars )
			return;


		$c = &$this->caption;

		$fields = array(
			'ref'          => $c['c_ref'],
			'date_in'      => $c['c_date_in'],
			'date_out'     => $c['c_date_out'],
			'nights'       => $c['c_nights'],
			'adult'        => $c['c_adult'],
			'child'        => $c['c_child'],
			'baby'         => $c['c_baby'],
			'payment_rest' => $c['c_payment_rest'],
			'price_extras' => $c['c_price_extras_search'],
			'price_tt'     => $c['c_tt'],
			'price_book'   => $c['c_price_book'],
			'price_total'  => $c['c_price_total'],
		);

		$this->custumer_fields = array(
			'name'        => $c['c_name'],
			'phone1'      => $c['c_phone1'],
			'phone2'      => $c['c_phone2'],
			'phone3'      => $c['c_phone3'],
			'phone4'      => $c['c_phone4'],
			'mail'        => $c['c_mail'],
			'adress'      => $c['c_adress'],
			'town'        => $c['c_town'],
			'province'    => $c['c_province'],
			'country'     => $c['c_country'],
			'nationality' => $c['c_nationality'],
			'zip'         => $c['c_zip'],
			'bank'        => $c['c_bank'],
			'bic'         => $c['c_bic'],
			'comta'       => $c['c_comta'],
			'vat'         => $c['c_vat'],
		);

		$this->extra_fields = array(
			'extra_included'          => $c['c_extra_type_included_search'],
			'extra_optional'          => $c['c_extra_type_optional_search'],
			'extra_required'          => $c['c_extra_type_required_search'],
			'extra_total_price_value' => $c['c_extra_total_price_search'],
			'has_quantity'            => $c['c_extra_quantity_search'],
			'extra_type'              => $c['c_extra_type_search'],
		);


		// Variables formulari
//		$adult = R::escape('adult');
//		$child = R::escape('child');
//		$baby = R::escape('baby');
//		$person = (int)$adult + (int)$child;
		$date_from       = R::escape( 'date_from' );
		$exact_date_from = R::escape( 'exact_date_from' );
		$date_to         = R::escape( 'date_to' );
		$exact_date_to   = R::escape( 'exact_date_to' );
		$is_owner        = R::escape( 'is_owner' );
		$status          = R::escape( 'status' );
		$ref             = R::escape( 'ref' ); // Utilitzo ref perque no puc fer servir property_id
		$ref_add         = R::escape( 'ref_add' ); // Per l'autocomplete
		$advanced_search = R::get( 'advanced' ) || R::get( 'excel' );

		$is_owner_select = get_select( 'is_owner', '0,1', $is_owner, array(), $this->caption );
		$status_select = $this->get_filter_status($status, false, $this->caption['c_caption_filter_status']);

		$exact_date_from_checkbox = get_checkbox( 'exact_date_from', $exact_date_from );
		$exact_date_to_checkbox = get_checkbox( 'exact_date_to', $exact_date_to );

		$input_selected_fields = R::get( 'fields', array() );
		R::escape_array( $input_selected_fields );

		// Cookie - Nomes un cop, el següent serà que hem tret tots els fields expressament
		if ( ! $input_selected_fields && isset( $_COOKIE['booking']['selected_fields'] ) ) {

			// a la home del busca, afago sempre de la cookie
			if ( !$advanced_search ) {
				$input_selected_fields                         = unserialize( $_COOKIE['booking']['selected_fields'] );
			}

			elseif (empty($_SESSION['booking']['loaded_selected_fields'])) {
				$input_selected_fields                         = unserialize( $_COOKIE['booking']['selected_fields'] );
				$_SESSION['booking']['loaded_selected_fields'] = true;
			}
		}
		setcookie( "booking[selected_fields]", serialize( $input_selected_fields ), time() + ( 10 * 365 * 24 * 60 * 60 ), '/' ); // 30 dies


		// Input
		$fields_input = '';
		foreach ( $fields as $k => $v ) {
			// Primer els seleccionats
			$fields_input .= '<option value="' . $k . '">' . $v . '</option>';
		}

		// OPTGROUP impedeix guardar l'ordre en que es trien les opcions
		// $fields_input .= ' <optgroup label="' . $c['c_custumer_search_group'] . '">';
		foreach ( $this->custumer_fields as $k => $v ) {
			// Primer els seleccionats
			$fields_input .= '<option value="' . $k . '">' . $v . '</option>';
		}
		// $fields_input .= '</optgroup>';

		// $fields_input .= ' <optgroup label="' . $c['c_extra_search_group'] . '">';
		foreach ( $this->extra_fields as $k => $v ) {
			// Primer els seleccionats
			$fields_input .= '<option value="' . $k . '">' . $v . '</option>';
		}
		// $fields_input .= '</optgroup>';

		$fields_input = '<select id="fields"  multiple="multiple" name="fields[]">
		            ' . $fields_input . '
	            </select>';



		// Extres hi ha 3 opcions diferents per escollir les extres
		$selected_fields = array();
		foreach ( $input_selected_fields as $field ) {
			if (
				$field == 'extra_included' ||
				$field == 'extra_optional' ||
				$field == 'extra_required'
			) {
				if (!in_array('extra', $selected_fields)) {
					$selected_fields [] = 'extra';
				}
				$this->extra_selected_types[$field] = true;
			}
			else {
				$selected_fields [] = $field;
			}
		}
		$this->extra_fields ['extra'] = $c['c_extra'];
		unset( $this->extra_fields ['extra_included'] );
		unset( $this->extra_fields ['extra_optional'] );
		unset( $this->extra_fields ['extra_required'] );


		$this->search_fields = array_merge( $fields, $this->custumer_fields, $this->extra_fields );


		$vars = array(
			'date_from'                => $date_from,
			'exact_date_from'          => $exact_date_from,
			'exact_date_from_checkbox' => $exact_date_from_checkbox,
			'date_to'                  => $date_to,
			'exact_date_to'            => $exact_date_to,
			'exact_date_to_checkbox'   => $exact_date_to_checkbox,
			'is_owner'                 => $is_owner,
			'is_owner_select'          => $is_owner_select,
			'status'                   => $status,
			'status_select'            => $status_select,
			'ref'                      => $ref,
			'ref_add'                  => $ref_add,
			'fields'                   => $fields_input,
			'input_selected_fields'    => $input_selected_fields,
		);

		// nomes canvio els fields del llistat quan em fet una cerca avançada
		if ( $advanced_search ) {
			$this->selected_fields = $selected_fields;
		}

		$this->search_vars     = $vars;

	}


	public function redo_fields(){

		// Primer afegeixo nous fields
		if (in_array('nights', $this->selected_fields)){
			$this->add_field('nights', array(
				'list_admin' => 'text',
				'change_field_name' => 'DATEDIFF(date_out,date_in) as nights'
			));
		}
		// Primer afegeixo nous fields
		if (in_array('payment_rest', $this->selected_fields)){
			$this->add_field('payment_rest', array(
				'list_admin' => 'text',
				'change_field_name' => '(price_total - payment_done_1 -payment_done_2 - payment_done_3 - payment_done_4) as payment_rest'
			));
		}

		foreach ( $this->fields as $field => $val ) {
			// Desconecto els fields que surten al llistat i no s'han seleccionat a selected_fields
			if (
				( $val['list_admin'] == 'text' || $val['list_admin'] == 'input' ) &&
				! in_array( $field, $this->selected_fields )
				&& $field != 'book_id'
			) {
				$this->set_field( $field, 'list_admin', '0' );
			}
			// Conecto els que s'han seleccionat, pero que no surten all llistat
			elseif (
				( $val['list_admin'] == 'no' || $val['list_admin'] == '0') &&
				in_array( $field, $this->selected_fields ) ){
				$this->set_field( $field, 'list_admin', 'text' );
			}
		}


		// ho faig així per mantindre l'ordre
		foreach ($this->selected_fields as $field){

			// Custumers
			if ( array_key_exists( $field, $this->custumer_fields ) ) {
				$this->custumer_selected_fields[ $field ] = in_array( $field, $this->selected_fields ) ? 'text' : 'no';
			}

			// Extres
			if ( array_key_exists( $field, $this->extra_fields ) ) {
				$this->extra_selected_fields[ $field ] = in_array( $field, $this->selected_fields ) ? 'text' : 'no';
			}

		}

		// Custumers
		if ( $this->custumer_selected_fields ) {
			// em serveix perque surtin als visible fields, els out també surten
			if ($this->is_excel){

				foreach ($this->selected_fields as $field){
					// poso el change field perque els rs quedin ordenats, despres ja els hi dono un valor
					if ( array_key_exists( $field, $this->custumer_fields ) ) {
						$this->add_field( $field, array(
							'type' => 'none',
							'list_admin' => 'text',
							'change_field_name' => '"" AS ' . $field
						) );
					}

				}

				// desconecto clients si es excel sempre
				$this->set_field( 'custumers', 'list_admin', 'no' );
			}

		}
		else {
			// desconecto clients
			$this->set_field( 'custumers', 'list_admin', 'no' );
		}


		// Extres
		if ( $this->extra_selected_fields ){
			// em serveix perque surtin als visible fields, els out també surten
			if ($this->is_excel){

				foreach ($this->selected_fields as $field){
					// poso el change field perque els rs quedin ordenats, despres ja els hi dono un valor
					if ( array_key_exists( $field, $this->extra_fields ) ) {
						$this->add_field( $field, array(
							'type' => 'none',
							'list_admin' => 'text',
							'change_field_name' => '"" AS ' . $field
						) );
					}

				}

				// desconecto clients si es excel sempre
				$this->set_field( 'extras', 'list_admin', 'no' );
			}
			else {
				$this->set_field('extras','list_admin','out');
			}

		}
		else {
			// desconecto clients
			$this->set_field( 'extras', 'list_admin', 'no' );
		}

		// reordeno els camps, el borro i el torno a posar al final
		foreach ( $this->selected_fields as $field ) {
			if (isset ($this->fields[ $field ])) {
				$new_field = $this->fields[ $field ];
				$this->unset_field( $field );
				$this->fields[ $field ] = $new_field;
			}
		}
	}

	/**
	 * Buscador avançat
	 */
	function search_advanced (){
		// Debug::p( $_GET );
		$this->is_search = true;
		$search_condition = '';
		$and = '';

		$this->set_search_advanced_vars ();

		extract ($this->search_vars);

		$date_from = unformat_date($date_from);
		$date_to = unformat_date($date_to);

		if ($date_from && $date_to) {


			if ($exact_date_from && $exact_date_to){

				$search_condition .= "
				(
					(date_in = '" . $date_from . "')
					AND 
					(date_out = '" . $date_to . "')
				)";

			}

			elseif ($exact_date_from){

				$search_condition .= "
				(
					(date_in = '" . $date_from . "')
					AND 
					( date_out <= '" . $date_to . "')	
				)";
			}

			// la segona es exacta
			elseif ($exact_date_to){
				$search_condition .= "
				(
					(date_out = '" . $date_to . "')
					AND 
					( date_in >= '" . $date_from . "')	
				)";
			}

			else {
				// qualsevol de les dues estan entre les dues dates
				$search_condition .= $and . "
				(
					(
						date_in >= '" . $date_from . "' AND
						date_in <= '" . $date_to . "'
					) OR
					(
						date_out >= '" . $date_from . "' AND
						date_out <= '" . $date_to . "'
					)
				)";
			}

			$and = ' AND ';
		}
		elseif ($date_from) {

			if ($exact_date_from){
				$search_condition .= "
				date_in = '" . $date_from . "'";
			}
			else {
				// si date_in es major, date_out també
				$search_condition .= $and . "
				(date_in >= '" . $date_from . "')";
			}
			$and = ' AND ';
		}

		elseif  ($date_to) {

			if ($exact_date_to){
				$search_condition .= "
				date_out = '" . $date_to . "'";
			}
			else
			{
				// si date_out es menor, date_in també
				$search_condition .= $and . "
				(date_out <= '" . $date_to . "')";
			}
			$and              = ' AND ';
		}

		// a l'excel sempre mostrem tots
		//elseif (!$date_from  && !$date_to){
			// per defecte nomes mostra d'avui en endavant
			// $search_condition .=  $and . "date_out >= DATE_FORMAT(NOW() ,'%Y-%m-01')";
			// $and = ' AND ';
		//}

		if ($ref) {
			$search_condition .=  $and . "property_id = " . $ref;
			$and = ' AND ';
		}

		if ($is_owner != 'null') {
			$search_condition .=  $and . "is_owner = " . $is_owner;
			$and = ' AND ';
		}

		if ($status != 'null') {
			$search_condition .=  $and . "status = '" . $status . "'";
			$and = ' AND ';
		}


		$this->condition = $search_condition;

		$this->do_action('list_records');
	}

	function get_search_ref_var_ajax() {

		$limit = 100;

		$q = R::escape('query');
		$q = strtolower($q);

		$ret = array();
		$ret['suggestions'] = array();

		$r = &$ret['suggestions'];

		// 1 - resultats que comencin amb el text
		$query = "SELECT property_id AS id, ref, property_private
				FROM inmo__property
				WHERE ref LIKE '" . $q . "%'
				OR property_private LIKE '" . $q . "%'
				ORDER BY ref, property_private
				LIMIT " . $limit;

		$results = Db::get_rows( $query );

		$rest = $limit - count( $results );

		// 2-  resultats que continguin el text
		if ($rest > 0) {

			$query = "SELECT property_id AS id,ref, property_private
				FROM inmo__property
				WHERE ref LIKE '" . $q . "%'
				OR property_private LIKE '" . $q . "%'
				ORDER BY ref, property_private
				LIMIT " . $rest;

			$results2 = Db::get_rows( $query );

			$results = array_merge( $results, $results2 );
		}


		foreach ( $results as $rs ) {
			$name = $rs['ref'];
			$name .= $name && $rs['property_private']?', ':'';
			$name .= $rs['property_private'];
			$r[] = array(
				'value' => $name,
				'data'  => $rs['id']
			);
		}

		$rest = $limit - count( $results );
		$ret['is_complete'] = $rest>0;

		die (json_encode($ret));

	}


	/*
	 * Exportació a excel
	 *
	 * */
	function export($results)
	{
		Main::load_class('excel');

		/*// Miro els camps que estan com a text o input o out
		$visible_fields = array();
		// Debug::p( $this->fields );
		foreach ( $this->fields as $field => $val ) {
			if (
				( $val['list_admin'] == 'text' || $val['list_admin'] == 'input'  || $val['list_admin'] == 'out' || $field == 'book_id')
			) {
				$visible_fields []= $field;
			}
		}*/

		// Si tinc custumers haig de posar rows entre mig de cada client de la reserva
		if ( $this->custumer_selected_fields ||  $this->extra_selected_fields ) {
			$new_results = array();
			foreach ( $results as $rs ) {

				if ( ! empty( $this->results_subquery[ $rs['book_id'] ] ) ) {

					$conta = 1;
					foreach ( $this->results_subquery[ $rs['book_id'] ] as $subrs ) {

						// despres del primer, borro tots els rs
						if ( $conta == 1 ) {
							$conta = 2;
						} else if ( $conta == 2 ) {
							$conta = 3;
							foreach ( $rs as $k=>$v ) {
								$rs[ $k ] = false; // false per poder excloure al exportar
							}
						}

						// Custumer
						foreach ( $this->custumer_selected_fields as $field => $val ) {
							$rs [ $field ] = !empty($subrs[ $field ])?$subrs[ $field ]:false;
						}
						// Extras
						foreach ( $this->extra_selected_fields as $field => $val ) {
							$rs [ $field ] = !empty($subrs[ $field ])?$subrs[ $field ]:false;
						}

						$new_results [] = $rs;
					}
				}
				else {
					$new_results [] = $rs;
				}


			}
			$results = $new_results;
		}

		// Debug::p( $visible_fields, 'visibles' );
/*		$export                 = new Export( $this );
		$export->visible_fields = $visible_fields;
		$export->column_formats = $this->excel_formats;
		$export->file_name      = $this->caption['c_excel_name'];

		$export->print_excel( $results, true );*/

		$excel = new Excel( $this );
		$excel->set_visible_fields()
		      ->column_formats( $this->excel_formats )
		      ->file_name( 'reserves' )
		      ->make( $results );

	}


	
	/*
	 * Calcul de preus
	 */
	
	function get_price_ajax() {
		
		$this->price_module->reserved_message = $this->reserved_message;
		$this->price_module->get_price_ajax($this);
		
	}
	function download_contract() {
		
		Main::load_class('booking', 'common', 'admin');
		$bcommon = New BookingCommon();
		$bcommon->caption = &$this->caption;
		$bcommon->get_contract($this);
	}
	
	
}