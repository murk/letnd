<?
/**
 * NewgrassSellpoint
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2013
 * @version $Id$
 * @access public
 */
class NewgrassSellpoint extends Module{

	function __construct(){
		parent::__construct();
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function get_records()
	{
		$listing = new ListRecords($this);
		$listing->condition = "(status = 'public')";
		
		$listing->group_fields = array('provincia_id');
		
		$listing->join = 'LEFT OUTER JOIN all__provincia USING (provincia_id)';
		$listing->extra_fields = "all__provincia.name as provincia";
		//$listing->extra_fields = "(SELECT name FROM all__provincia WHERE all__provincia.provincia_id = newgrass__sellpoint.provincia_id) AS provincia_id";
		
		$listing->call('records_walk','url1,url1_name,sellpoint_description');
		$listing->order_by = 'provincia, empresa ASC';		
		
		return $listing->list_records();
	}
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{
		$show = new ShowForm($this);
		
		// si no hi ha id, agafo per defecte la primera noticia de la categoria, i si no hi ha categoria, de la primera categoria
		if (!$show->id) Main::error_404();
		
		$show->condition = "(status = 'public')";
		$show->call('records_walk','url1,url1_name');
		
		$show->get_values();		
		if ($show->has_results)
		{
			// $GLOBALS['gl_page']->title = $show->rs['empresa'];
			
			//$GLOBALS['gl_page']->page_title = 
				//$show->rs['page_title']?$show->rs['page_title']:$show->rs['new'];
				
			//$GLOBALS['gl_page']->page_description = 
				//$show->rs['page_description']?$show->rs['page_description']:add_dots_meta_description($show->rs['content']);
				
			//if ($show->rs['page_keywords']!='')
				//$GLOBALS['gl_page']->page_keywords = $show->rs['page_keywords'];
				
			Page::add_breadcrumb($show->rs['empresa']);
			return $show->show_form();
		}
		else
		{
			Main::error_404();
		}
	}
	function records_walk($url1,$url1_name,$sellpoint_description=false){		
		// Això ho poso aquí així no cal posar-ho al tpl, simplement posar: <a href="<=$url1>" target="_blank"><=$url1_name></a>
		$ret['url1_name'] = $url1_name?$url1_name:$url1;
		
		// si es llistat passa lavariable sellpoint_description
		if ($sellpoint_description!==false){
			$ret['sellpoint_description']=add_dots('sellpoint_description', $sellpoint_description);
		}
			
		return $ret;
	}
}
?>