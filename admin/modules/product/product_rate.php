<?

/**
 * ProductRate
 *
 * @property string $global_minimum
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2011
 * @version $Id$
 * @access public
 */
class ProductRate extends Module{
	var $global_minimum, $is_editable;
	function __construct(){
		parent::__construct();
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function get_records()
	{
		
		$listing = new ListRecords($this);
		$listing->has_bin = 0;
		$listing->add_swap_edit();
		
	    // preu resta paisos
		$this->file = 'product/rate_world.tpl';
		$this->set_tax_included();
		$this->set_vars($this->caption);
		$this->is_editable = $listing->is_editable;

		// Despesa resta països
		$send_rate_world = Db::get_first("SELECT value FROM product__configadmin WHERE name = 'send_rate_world'");
		$send_rate_world = 
				$listing->is_editable? 
					'<input type="text" name="send_rate_world" value="'.  format_currency($send_rate_world).'">':
					format_currency($send_rate_world);

		// Increment reemborls
		$send_rate_encrease_ondelivery_world = Db::get_first("SELECT value FROM product__configadmin WHERE name = 'send_rate_encrease_ondelivery_world'");
		$send_rate_encrease_ondelivery_world =
				$listing->is_editable? 
					'<input type="text" name="send_rate_encrease_ondelivery_world" value="'.  format_currency($send_rate_encrease_ondelivery_world).'">':
					format_currency($send_rate_encrease_ondelivery_world);

		// Minim
		$minimum_buy_free_send = Db::get_first("SELECT value FROM product__configadmin WHERE name = 'minimum_buy_free_send'");
		$this->global_minimum  = $minimum_buy_free_send;
		$minimum_buy_free_send =
				$listing->is_editable?
					'<input type="text" name="default_minimum_buy_free_send" value="'.  format_currency($minimum_buy_free_send).'">':
					format_currency($minimum_buy_free_send);
		
		$this->set_var('send_rate_world',$send_rate_world);
		$this->set_var('send_rate_encrease_ondelivery_world',$send_rate_encrease_ondelivery_world);
		$this->set_var('minimum_buy_free_send',$minimum_buy_free_send);

		$rate_world_html = $this->process();
		$listing->content_top = $rate_world_html;
		
		//$listing->add_field('country');
		//$listing->set_field('country','change_field_name','(SELECT all__country.country FROM all__country WHERE all__country.country_id = product__rate.country_id) AS country');
		
		$listing->order_by = "provincia ASC, country ASC";

		$listing->set_records();
		$listing->call_after_set_records( 'set_records_walk' );

		return $listing->process();
	}
	function set_records_walk($rs){
		$ret['rate_encrease_ondelivery'] = '+' . $rs['rate_encrease_ondelivery'];


		$minimum = (double) $rs['minimum_buy_free_send_value'];
		$global_minimum = $this->global_minimum;

		if ($minimum == 0) {
			if ($this->is_editable) {
				$minimum_buy_free_send = str_replace('0,00', '', $rs['minimum_buy_free_send']);
				$ret['minimum_buy_free_send'] = $minimum_buy_free_send . " ( $global_minimum ) ";
			}
			else {
				$ret['minimum_buy_free_send'] = $global_minimum;

			}
		}

		return $ret;
	}
	
	function set_tax_included(){
		global $gl_show_config;

		$is_tax = Db::get_first("SELECT value FROM product__configadmin WHERE name='is_public_tax_included'");


		// missatge de iva inclos o no inclos
		$this->caption['c_rate_title'] .= ' ' . ($is_tax?$this->caption['c_tax_included']:$this->caption['c_tax_not_included']);

	}
	
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{
		$this->is_editable = true;
		$this->global_minimum = Db::get_first("SELECT value FROM product__configadmin WHERE name = 'minimum_buy_free_send'");

	    $show = new ShowForm($this);
		$show->has_bin = 0;
		
		
		
		
		if ($this->action == 'show_form_edit') {
			$show->get_values();
			if ($show->rs['provincia_id']){
				$show->unset_field('country_id');
			}
			else{
				$show->unset_field('provincia_id');	
			}
		}
		else{
			if ($this->process=='provincia') {
				$show->unset_field('country_id');
			}
			else{
				$show->unset_field('provincia_id');	
			}			
		}
		$show->set_record();
		$show->call_after_set_record( 'set_records_walk' );
		
	    return $show->process();
	}

	function save_rows()
	{
	    $save_rows = new SaveRows($this);
	    $save_rows->save();
		
		$send_rate_world = R::escape('send_rate_world',0,'_POST');
		$send_rate_encrease_ondelivery_world = R::escape('send_rate_encrease_ondelivery_world',0,'_POST');
		$minimum_buy_free_send = R::escape('default_minimum_buy_free_send',0,'_POST');

		Db::execute("
			UPDATE product__configadmin 
				SET value='".unformat_currency($send_rate_world)."' 
				WHERE  name='send_rate_world';");
		Db::execute("
			UPDATE product__configadmin 
				SET value='".unformat_currency($send_rate_encrease_ondelivery_world)."' 
				WHERE  name='send_rate_encrease_ondelivery_world';");
		Db::execute("
			UPDATE product__configadmin 
				SET value='".unformat_currency($minimum_buy_free_send)."' 
				WHERE  name='minimum_buy_free_send';");
	}

	function write_record()
	{
	    $writerec = new SaveRows($this);
	    $writerec->save();
	}

	function manage_images()
	{
	    $image_manager = new ImageManager($this);
	    $image_manager->execute();
	}
}
?>