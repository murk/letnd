<script language="JavaScript" src="/admin/jscripts/calculator.js?v=<?=$version?>"></script>
<style type="text/css">
<!--
body {font-family: helvetica}
p {font-size: 12pt}
.red {color: red}
.blue {color: blue}
-->
</style>

<center>
<form name="calculator">
  <table border=1 bgcolor="#526584" class="formsButtons">
    <tr>
      <td><table border=0 class="formsButtons">
          <tr>
            <td align="center" bgcolor="#000080" class="opcionsTitol"><b style="color:white">
              <?=$c_calculator;?>
            </b></td>
          </tr>
          <tr>
            <td><table width="100%" border=0 cellpadding="0" cellspacing="0">
                <tr>
                  <td colspan=6 align="center" class="formsButtons"><input name="answer" type="text" class="formsButtons" onChange="CheckNumber(this.value)" size=30 maxlength=30></td>
                </tr>
                <tr>
                  <td colspan=6 align="left" class="formsButtons"><table border=0 align="left" cellpadding=0>
                      <tr>
                        <td><input type="button" name="CE" class="red" value=" CE " onClick="CECalc(); return false;">
                            <input type="reset" name="C" class="red" value="  C  " onClick="ClearCalc(); return false;">
                        </td>
                      </tr>
                  </table></td>
                </tr>
                <tr>
                  <td class="formsButtons"><input type="button" name="MC" class="red" value=" MC " onClick="MemoryClear(); return false;"></td>
                  <td class="formsButtons"><input type="button" name="calc7" class="blue" value="  7  " onClick="CheckNumber('7'); return false;"></td>
                  <td class="formsButtons"><input type="button" name="calc8" class="blue" value="  8  " onClick="CheckNumber('8'); return false;"></td>
                  <td class="formsButtons"><input type="button" name="calc9" class="blue" value="  9  " onClick="CheckNumber('9'); return false;"></td>
                  <td class="formsButtons"><input type="button" name="divide" class="red" value="  /  " onClick="DivButton(1); return false;"></td>
                  <td class="formsButtons"><input type="button" name="sqrt" class="blue" value="sqrt" onClick="SqrtButton(); return false;"></td>
                </tr>
                <tr>
                  <td class="formsButtons"><input type="button" name="MR" class="red" value=" MR " onClick="MemoryRecall(Memory); return false;"></td>
                  <td class="formsButtons"><input type="button" name="calc4" class="blue" value="  4  " onClick="CheckNumber('4'); return false;"></td>
                  <td class="formsButtons"><input type="button" name="calc5" class="blue" value="  5  " onClick="CheckNumber('5'); return false;"></td>
                  <td class="formsButtons"><input type="button" name="calc6" class="blue" value="  6  " onClick="CheckNumber('6'); return false;"></td>
                  <td class="formsButtons"><input type="button" name="multiply" class="red" value="  *  " onClick="MultButton(1); return false;"></td>
                  <td class="formsButtons"><input type="button" name="percent" class="blue" value=" %  " onClick="PercentButton(); return false;"></td>
                </tr>
                <tr>
                  <td class="formsButtons"><input type="button" name="MS" class="red" value=" MS " onClick="MemorySubtract(document.calculator.answer.value); return false;"></td>
                  <td class="formsButtons"><input type="button" name="calc1" class="blue" value="  1  " onClick="CheckNumber('1'); return false;"></td>
                  <td class="formsButtons"><input type="button" name="calc2" class="blue" value="  2  " onClick="CheckNumber('2'); return false;"></td>
                  <td class="formsButtons"><input type="button" name="calc3" class="blue" value="  3  " onClick="CheckNumber('3'); return false;"></td>
                  <td class="formsButtons"><input type="button" name="minus" class="red" value="  -  " onClick="SubButton(1); return false;"></td>
                  <td class="formsButtons"><input type="button" name="recip" class="blue" value="1/x " onClick="RecipButton(); return false;"></td>
                </tr>
                <tr>
                  <td class="formsButtons"><input type="button" name="Mplus" class="red" value=" M+  " onClick="MemoryAdd(document.calculator.answer.value); return false;"></td>
                  <td class="formsButtons"><input type="button" name="calc0" class="blue" value="  0  " onClick="CheckNumber('0'); return false;"></td>
                  <td class="formsButtons"><input type="button" name="negate" class="blue" value="+/- " onClick="NegateButton(); return false;"></td>
                  <td class="formsButtons"><input type="button" name="dot" class="blue" value="  .   " onClick="CheckNumber('.'); return false;"></td>
                  <td class="formsButtons"><input type="button" name="plus" class="red" value=" +  " onClick="AddButton(1); return false;"></td>
                  <td class="formsButtons"><input type="button" name="equal" class="red" value="  =   " onClick="EqualButton(); return false;"></td>
                </tr>
            </table></td>
          </tr>
      </table></td>
    </tr>
  </table>
</form>