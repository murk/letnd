<style type="text/css">
	#import div {
		padding: 20px;
		border-top: 1px solid #D3D3D3;
	}
	#import table td {
		padding: 2px 5px;
	}
	#import table th {
		padding: 2px 5px;
		text-align: left;
	}
	h6 {
		font-size: 20px;
		margin: 10px 5px;
	}
	#confirm {
		display: none;
		padding: 25px;
		overflow: hidden;
		width: 80%;
	}
	#group_exists {
		display: none;
		padding: 25px;
		font-size: 16px;
	}
	.done {
		display: none;
		padding: 25px;
		font-size: 20px;
	}
	tr.eica-gris td {
		color: #000000;
	}
	tr.vermell td {
		COLOR: #CB2903;
	}
	span.eica-gris {
		color: #000000;
		position: relative;
		display: block;
		padding-left: 30px;
		margin-top: 30px;
		margin-bottom: 10px;
	}
		span.eica-gris:before {
			background-color: #000000;
		}
	span.vermell {
		COLOR: #CB2903;
		position: relative;
		display: block;
		padding-left: 30px;
		margin-bottom: 30px;
	}
		span.vermell:before  {
			background-color: #CB2903;
		}
		span.eica-gris:before,
		span.vermell:before {
			content: " ";
			display: block;
			position: absolute;
			height: 3px;
			width: 10px;
			top: 4px;
			left: 0;
		}
</style>
<script type="text/javascript" src="/common/jscripts/notify/pnotify.custom.min.js"></script>
<link href="/common/jscripts/notify/pnotify.custom.css?v=<?= $version ?>" rel="stylesheet" type="text/css"/>
<script type="text/javascript">

	function notify_import_success() {

		top.$("#confirm").hide();
		top.$(".done").show();

		top.PNotify.removeAll();

		new PNotify({
			title: 'Importació finalitzada',
			icon: false,
			type: 'success',
			hide: false
		});
	}

	function notify_preview_success() {

		top.$("#confirm").show();
		top.$(".done").hide();

		top.PNotify.removeAll();

		new PNotify({
			title: 'Previsualització finalitzada',
			icon: false,
			type: 'success',
			hide: false
		});
	}

	$(function () {
		 $('#excel').change(function () {

			 var val = $('#excel').val();

			 var ext_arr = val.split('.');
			 var ext = ext_arr[ext_arr.length - 1];

			 if (ext != 'xlsx') {
				 alert('L\'arxiu seleccionat no és un arxiu d\'excel correcte');
				 $('#excel').val('');
				 return;
			 }



			 var filename = ';';
			 var group_name;

			 if (val) {
				 var startIndex = (val.indexOf('\\') >= 0 ? val.lastIndexOf('\\') : val.lastIndexOf('/'));
				 filename = val.substring(startIndex);
				 if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
					 filename = filename.substring(1);
				 }

				 group_name = filename.split('.');
				 group_name = group_name[group_name.length-2];
			 }

			 $('#group').val(group_name);

		 });

		$("input[name=import]").click(function () {

			var val = $('#excel').val();

			if (!val) {
				new PNotify({
					title: 'S\'ha de triar un arxiu',
					icon: false,
					type: 'error'
				});
				return false;
			}

			new PNotify({
				title: 'Importació',
				text: 'Obtinguent registres...',
				icon: false,
				type: 'info',
				hide: false
			});

		});
		$("input[name=preview]").click(function () {

			var val = $('#excel').val();

			if (!val) {
				new PNotify({
					title: 'S\'ha de triar un arxiu',
					icon: false,
					type: 'error'
				});
				return false;
			}

			new PNotify({
				title: 'Previsualització',
				text: 'Obtinguent registres...',
				icon: false,
				type: 'info',
				hide: false
			});

		});
	});

</script>
<form name="theForm" method="post" action="/admin/?menu_id=2932&action=add_record" enctype="multipart/form-data" target="save_frame">
	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="form">
		<tr>
			<td>
				<div id="form_buttons_holder">
					<table id="form_buttons">
						<tr>
							<td valign="bottom">
								<div class="menu"></div>
							</td>
							<td class="buttons_top">
								<div class="right"></div>
							</td>
						</tr>
					</table>
				</div>


				<? ////////////   TITOL  ////////////////?>


				<table width="100%" cellpadding="0" cellspacing="0">
					<tr>
						<td class="formCaptionTitle"><strong>
								<?= $c_import_title ?>
								&nbsp;&nbsp;</strong></td>
					</tr>
				</table>


				<? ////////////   CAMPS  ////////////////?>

				<table width="100%" cellpadding="0" cellspacing="0" class="formstbl">

					<tr>
						<td valign="top" class="formsCaption">
							<?= $c_group ?>:
						</td>
						<td valign="top" class="forms">
							<input size="50" type="input" id="group" name="group"/>
						</td>
					</tr>

					<tr>
						<td valign="top" class="formsCaption">
							<?= $c_select_file ?>:
						</td>
						<td valign="top" class="forms">
							<input class="file" type="file" id="excel" name="excel"/>
						</td>
					</tr>

				</table>

				<table width="100%" cellpadding="4" cellspacing="2" class="formsButtonstbl">
					<tr>
						<td align="center" class="formsButtons">
							<div class="centerFloat1">
								<div class="centerFloat2">
									<input type="submit" name="preview" value="<?= $c_import_preview ?>" class="btn_form1">
									<input type="submit" name="import" value="<?= $c_import ?>" class="btn_form1">
								</div>
							</div>
						</td>
					</tr>
				</table>

				<div class="done">
					Importació finalitzada
				</div>

				<div id="import">
				</div>
				<div id="confirm">
					<input type="submit" name="import" value="<?= $c_import ?>" class="btn_form1">
				</div>
				<div class="done">
					Importació finalitzada
				</div>

			</td>
		</tr>
	</table>
</form>