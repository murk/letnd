<?
/*
26-04-2019

Afegeix pagament només d'un deposit des de la web pública per Lassdive

*/
$update_sql = "";

if ( is_table( 'product__configpublic' ) && ! Db::get_first( "SELECT * FROM product__configpublic WHERE name='deposit_payment_on'" ) ) {
	$update_sql .= "
		INSERT INTO product__configpublic (`name`, `value`) VALUES ('deposit_payment_on', '0');
		INSERT INTO product__configpublic (`name`, `value`) VALUES ('deposit_payment_percent', '0');
		INSERT INTO product__configpublic (`name`, `value`) VALUES ('deposit_payment_round', '0');
		INSERT INTO product__configpublic (`name`, `value`) VALUES ('deposit_rest_round', '0');
		ALTER TABLE `product__family` ADD COLUMN `accepts_deposit_payment` TINYINT(1) NOT NULL DEFAULT '1' AFTER `pass_code`;";

}
else {
	if ( is_table( 'product__configpublic' ) ) {
		trigger_error( 'Ja existeixen el camp deposit_payment_on' );
	}
}

if ( is_table( 'product__order' ) && ! is_field( 'product__order', 'deposit_transaction_id' ) ) {

	$update_sql .= "ALTER TABLE `product__order`
	ADD COLUMN `deposit_transaction_id` CHAR(17) NOT NULL AFTER `transaction_id`,
	ADD COLUMN `deposit_status` ENUM('paying','failed','pending','denied','voided','refunded','completed') NOT NULL AFTER `order_status`;";

}
else {
	if ( is_table( 'product__order' ) ) {
		trigger_error( 'Ja existeixen el camp deposit_transaction_id' );
	}
}