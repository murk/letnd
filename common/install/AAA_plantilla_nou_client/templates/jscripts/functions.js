// safari i webkit no saben la mida de la imatge fins el load
$(document).ready(function () {

	client.init(true);
	client.is_resize = true;

	var resizeTimer = null;

	$(window).resize(function () {
		resizeTimer && clearTimeout(resizeTimer);
		resizeTimer = setTimeout(function () {

			// Chrome mobil agafa el màxim ample, no el viewport
			if (client.view_port != $('body').width()) {
				client.init_resize();
			}

		}, 200);

	});

});

// bugs mida imatges
$(window).on("load", function () {
	client.init_resize();
});

var client = {
	is_fixed: false,
	is_resize: false,
	media_width: 0,
	view_port: 0,
	menu_view: 'desktop',
	is_mobile: false,//{{#is_product}}
   slider_product_html: '',//{{/is_product}}
	init_resize: function () {

		this.is_mobile = $('#menu-mobile').is(':visible');
		this.media_width = get_media_width();
		this.view_port = $('body').width();
		dp(this.media_width);

		client.set_mobile_menu();

		var reset_only;
		reset_only = this.media_width <= 750;
		num_cols = this.media_width <= 900 ? 2 : 3;

		// TODO-i Important l'ordre dels scripts -> resetejar primer slider ( haig de fer les funcions com a parisbarcelone ) , després same_height i últim prettyPhoto
		if (gl_is_home) {

			//{{^is_web}}
				reset_only = this.media_width <= 750;
				num_cols = this.media_width <= 900 ? 2 : 3;
    			same_height('#//{{solution}}-home h3', num_cols , true, reset_only);
    			same_height('#//{{solution}}-home h4', num_cols , true, reset_only);
    			same_height('#//{{solution}}-home .description', num_cols , true, reset_only);

			//{{/is_web}}
			reset_only = this.media_width <= 750;
			num_cols = this.media_width <= 900 ? 2 : 3;
			same_height('.new-list figure', num_cols, true, reset_only);
			same_height('.new-list h3', num_cols, true, reset_only);
			same_height('.new-list .description', num_cols, true, reset_only);
			same_height('.new-list article', num_cols, true, reset_only);

		}
		else {
			//{{#is_inmo}}

			if (gl_tool_section == 'property' && $('#property-list-records').length) {
				reset_only = this.media_width <= 750;
				num_cols = this.media_width <= 900 ? 2 : 3;
				same_height('.property-list figure', num_cols, true, reset_only);
				same_height('.property-list h3', num_cols, true, reset_only);
				same_height('.property-list .description', num_cols, true, reset_only);
			}
			//{{/is_inmo}}
			//{{#is_product}}

			if (gl_tool_section == 'product' && $('#product-list-records').length) {
				reset_only = this.media_width <= 750;
				num_cols = this.media_width <= 900 ? 2 : 3;
				same_height('.product-list figure', num_cols, true, reset_only);
				same_height('.product-list h3', num_cols, true, reset_only);
				same_height('.product-list h4', num_cols, true, reset_only);
				same_height('.product-list .description', num_cols, true, reset_only);
			}
			//{{/is_product}}
			
			if ( gl_tool_section == 'new' && $('#new-list-records').length ) {
				reset_only = this.media_width <= 750;
				num_cols = this.media_width <= 900 ? 2 : 3;
				same_height('.new-list figure', num_cols, true, reset_only);
				same_height('.new-list h3', num_cols, true, reset_only);
				same_height('.new-list .description', num_cols, true, reset_only);
				same_height('.new-list article', num_cols, true, reset_only);
			}

			client.init_youtube_frames();

		}

	},
	init: function () {

		$('body').append('<div id="responsive"></div>');

		client.init_resize();
		client.init_mobile_menu();
		client.init_gallerys();
		client.init_iframe_popups();

		if (gl_is_home) {
			client.init_home_slider();
			client.init_news_home_slider();
		}

		else {

			//{{#is_inmo}}
			if (gl_tool_section == 'property' && gl_action == 'show_record') {
				client.init_inmo_slider();
			}

			if (gl_tool_section == 'property' && $('#property-list-records').length) {

			}
			//{{/is_inmo}}

			//{{#is_product}}
			if (gl_tool_section == 'product' && gl_action == 'show_record') {
				client.reset_product_slider();
				client.init_product_slider();
			}

			if (gl_tool_section == 'product' && $('#product-list-records').length) {

			}
			//{{/is_product}}
		
			if (gl_has_google_maps_key && gl_tool_section == 'contact') {
				initialize();
			}
		}

		// TODO-i Treure si no va jcf
		jcf.setOptions('Select', {
			wrapNative: false
		});
		jcf.replaceAll();

		// TODO-i Treure si no s'utilitza
		$('#scroll-top').click(function(){
			$("html, body").animate({ scrollTop: 0 }, "slow");
			return false;
		});

	},
	init_home_slider: function () {

		var slider = $('#slider-home');

		slider.slick({
			dots: false,
			// appendDots: '#dots-holder', TODO-i Si es vol posar els cercles en un div que creem nosaltres
			arrows: true,
			// appendArrows: '#arrows-holder', TODO-i Si es vol posar les fletxes en un div que creem nosaltres
			infinite: false,
			speed: 800,
			autoplaySpeed: 4000,
			slide: 'div',
			slidesToShow: 1,
			autoplay: true
		});

	},
	init_news_home_slider: function () {

		var slider = $('#news-home-slider');

		slider.slick({
			dots: false,
			arrows: true,
			infinite: false,
			speed: 500,
			slide: 'article',
			slidesToShow: 1
		});

	},
	init_youtube_frames: function () {

		// TODO-i Canviar ample màxim del vídeo
		var max_width = 720;

		var frame = $('.videoframe iframe');

		frame.each(function (index) {

			var frame = $(this);
			var width = frame.width();
			var height = frame.height();
			var videoframe_ratio = height / width;

			frame.css('width', '100%');

			var new_width = frame.width();
			new_width = new_width > max_width ? max_width : new_width;

			var new_height = new_width * videoframe_ratio;
			new_height = Math.round(new_height);

			frame.width(new_width);
			frame.height(new_height);

		});

	}, //{{#is_inmo}}

    	init_inmo_home_slider: function (reset) {

    		var slider = $('#inmo-home-slider');

    		if (slider.hasClass('slick-initialized')) {
    			slider.slick('unslick');
    		}

    		// TODO-i Responsive canviar valors segons les columnes
    		var slides_to_show = this.media_width <= 850 ? 2 :3;
    		slides_to_show = this.media_width <= 570 ? 1 : slides_to_show;

    		var item = slider.find('.item');
    		if (item.length <= slides_to_show) return;

    		slider.removeAttr('style');
    		item.removeAttr('style');

    		if (reset) {
    			item.show();
    			return;
    		}

    		slider.width(slider.width());
    		item.width(item.width());

    		slider.slick({
    			dots: false,
    			arrows: true,
    			adaptiveHeight: true,
    			infinite: false,
    			speed: 500,
    			slide: 'article',
    			slidesToShow: slides_to_show,
    			slidesToScroll: slides_to_show
    		});

    	}, //{{/is_inmo}}
	init_gallerys: function () {

		// TODO-i Treure product o inmo
		var slider = $('.slider-product,#slider-inmo,#slider-news');

		slider.lightGallery({
			selector: '.item a',
			thumbnail: true,
			zoom: true,
			overlayOpacity: 0.8,
			download: false
		});

		/* TODO-i Per posar un botó de zoom
		$('#zoom').click(function () {
			slider.find('.slick-current a').first().click();
		});
		*/
	},
	//{{^is_web}} // //{{solution}}
	init_//{{solution}}_slider: function () {

		var slider = $('//{{#is_product}}.//{{/is_product}}//{{^is_product}}#//{{/is_product}}slider-//{{solution}}//{{#is_product}}:visible//{{/is_product}}');
		var thumbs = $('//{{#is_product}}.//{{/is_product}}//{{^is_product}}#//{{/is_product}}slider-//{{solution}}-thumbs//{{#is_product}}:visible//{{/is_product}}');

		//if (typeof (slider.unslick) == 'function') {
		if (slider.hasClass('slick-initialized')) {
			slider.slick('unslick');
		}
		if (thumbs.hasClass('slick-initialized')) {
			thumbs.slick('unslick');
		}

		var item = '#slider-//{{solution}} .item';
		if ($(item).length == 1) return;

		slider.removeAttr('style');
		thumbs.removeAttr('style');
		$(item).removeAttr('style');

		slider.width(slider.width());
		thumbs.width(slider.width());
		$(item).width(slider.width());

		// responsive no va amb asNavFor
		var arrows = this.media_width > 470;
		//var center_mode = this.media_width <= 470;

		slider.slick({
			dots: false,
			arrows: arrows,
			//centerMode: center_mode,
			infinite: false,
			speed: 500,
			slide: 'div',
			slidesToShow: 1,
			adaptiveHeight: true,
			asNavFor: thumbs,
			draggable: false
		});

		thumbs.slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			slide: 'div',
			asNavFor: slider,
			arrows: arrows,
			dots: false,
			centerMode: true,
			variableWidth: true,
			focusOnSelect: true,
			centerPadding: '0'
		});
	},
	//{{/is_web}}//{{#is_product}}
   reset_product_slider: function () {

      if (client.slider_product_html) {
         $('#slider-product-holder').html(client.slider_product_html);
         client.init_gallerys();
      }
      else client.slider_product_html = $('#slider-product-holder').html();

   },
	//{{/is_product}}
	init_iframe_popups: function() {
		$("a[data-iframe='true']").lightGallery({
			selector: 'this',
			showAfterLoad: false,
			download: false,
			counter: false,
			fullScreen: false,
			zoom: false,
			iframeMaxWidth: '100%'
		});
	},
	set_mobile_menu: function () {

		if (this.is_mobile && (this.menu_view == 'mobile')) return;
		if (!this.is_mobile && (this.menu_view == 'desktop')) return;

		this.menu_view = this.is_mobile ? 'mobile' : 'desktop';

		// TODO-i Posar els menus que s'intercanvien a la versió mobil
		var swaps = ['main-nav', 'login-nav'];

		for (var i in swaps) {
			var swap,from,to;
			swap = from = to = swaps[i];

			from += this.is_mobile ? '':'-mobile';
			to += this.is_mobile ? '-mobile' : '';

			var $to = $("#" + to);
			$("#" + from).contents().appendTo($to);

			if ($to.html() == '') $to.hide();
		}
	},
	init_mobile_menu: function () {
		$('#menu-mobile').on({
			'touchstart click': function (event) {

				$('#submenu-mobile').slideToggle(400);
				$('#menu-mobile').toggleClass('active');

				event.stopPropagation();
				event.preventDefault();
			}
		});
		
		
		// TODO-i Nomes quan els menus dels idiomes van amb un hover
		$('#current-language').on({
			'touchstart click': function (event) {
				
				$('#languages').find('ul').slideToggle(400);
				$('#current-language').toggleClass('active');

				event.stopPropagation();
				event.preventDefault();
			}
		});
	}
};


function hidePopWin(callReturnFunc) {
	$.unblockUI();
	if (typeof $("a[data-iframe='true']").data('lightGallery') != 'undefined') {
		$("a[data-iframe='true']").data('lightGallery').destroy();
	}
	$('.lg-outer').remove();
	if (callReturnFunc == true && gReturnFunc != null) {
		window.setTimeout('gReturnFunc();');
	}
}



function initialize() {

	if (typeof gl_google_latitude == 'undefined') return;

	var myLatlng = new google.maps.LatLng(gl_google_latitude, gl_google_longitude);
	var myLatlng_center = new google.maps.LatLng(gl_google_center_latitude, gl_google_center_longitude);
	var draggable = true;

	var myOptions = {
		zoom: gl_zoom,
		center: myLatlng_center,
		mapTypeId: google.maps.MapTypeId.ROADMAP,
		mapTypeControlOptions: {style: google.maps.MapTypeControlStyle.DROPDOWN_MENU},
		draggable: draggable,
		zoomControl: true,
		mapTypeControl: false,
		streetViewControl: false,
		// TODO-i Styles del mapa, per posar altres colors
		// TODO-i Treure poi: labels, per posar els punts d'interes
		styles: [
			{
				"featureType": "landscape.natural",
				"elementType": "geometry.fill",
				"stylers": [{"visibility": "on"}, {"color": "#dee0e2"}]
			},
			{
				"featureType": "poi",
				"elementType": "geometry.fill",
				"stylers": [{"visibility": "on"}, {"hue": "#818181"}, {"color": "#c9d5e7"}]
			},
			{
				"featureType": "poi",
				"elementType": "labels",
				"stylers": [{"visibility": "off"}]
			},
			{
				"featureType": "road",
				"elementType": "geometry",
				"stylers": [{"lightness": 100}, {"visibility": "simplified"}]
			},
			{
				"featureType": "transit.line",
				"elementType": "geometry",
				"stylers": [{"visibility": "on"}, {"lightness": 700}]
			},
			{
				"featureType": "water",
				"elementType": "all",
				"stylers": [{"color": "#213171"}]
			}
		],

		scrollwheel: true
	};

	var map = new google.maps.Map(document.getElementById("google-map"), myOptions);

	var image = new google.maps.MarkerImage(gl_dir_templates + 'images/marker.png',
		new google.maps.Size(64, 50),
		new google.maps.Point(0, 0),
		new google.maps.Point(15, 47)
	);

	var marker = new google.maps.Marker({
		position: myLatlng,
		map: map,
		icon: image
		//title: gl_google_info_html
	});

}//{{#is_inmo}}



 function initialize_inmoform(resize, map_id) {

   if (typeof gl_google_latitude === 'undefined') return;

   var myLatlng = new google.maps.LatLng(gl_google_latitude,gl_google_longitude);
   var myLatlng_center = new google.maps.LatLng(gl_google_center_latitude,gl_google_center_longitude);
   var zoom = gl_zoom;

   var myOptions = {
      zoom: zoom,
      center: myLatlng_center,
      scrollwheel:false,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      mapTypeControlOptions: {style: google.maps.MapTypeControlStyle.DROPDOWN_MENU}
   };

   var map = new google.maps.Map(document.getElementById(map_id), myOptions);

   // si hi ha punt
   if (gl_google_latitude){
      var image = new google.maps.MarkerImage(gl_dir_templates + 'images/marker.png',
         new google.maps.Size(64, 50),
         new google.maps.Point(0,0),
         new google.maps.Point(15, 47)
      );

      var marker = new google.maps.Marker({
         position: myLatlng,
         map: map,
         icon: image
         //title: gl_google_info_html
      });
   }

 }
//{{/is_inmo}}