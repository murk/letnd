<html>
<head>
<title><?=$page_title?></title>
<meta name="description"
content="<?=$page_description?>">
<!-- <?=$page_description?> -->
<meta name="keywords"
content="<?=$page_keywords?>">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="/public/themes/<?=THEME?>/styles/general.css" rel="stylesheet" type="text/css">
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<?if ($message):?><table width="100%" border="0" cellpadding="0" cellspacing="0" >
                  <tr>
                    <td align="left" valign="top">&nbsp;</td>
                  </tr>
                  <tr>
                    <td align="center" valign="top"><strong>
                    <?=$message?>
                    </strong></td>
                  </tr>
              </table><?endif //message?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center"><?=$content?></td>
  </tr>
</table>
</body>
</html>
