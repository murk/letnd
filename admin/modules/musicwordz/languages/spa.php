<?php
// menus eina
define('MUSICWORDZ_MENU_SONG','Canciones');
define('MUSICWORDZ_MENU_PLACE','Salas, festivales');
define('MUSICWORDZ_MENU_PERFORMANCE_LIST','Listar y editar actuaciones');
define('MUSICWORDZ_MENU_PERFORMANCE_BIN','Papelera de reciclaje');
define('MUSICWORDZ_MENU_PERFORMANCE_NEW','Entrar nueva actuación');
define('MUSICWORDZ_MENU_SONG_BIN','Papelera de reciclaje');
define('MUSICWORDZ_MENU_PERFORMANCE','Actuaciones');
define('MUSICWORDZ_MENU_SONG_LIST','Listar y editar canciones');
define('MUSICWORDZ_MENU_SONG_NEW','Entrar nueva canción');
define('MUSICWORDZ_MENU_PLACE_NEW','Entrar nueva sala | festival');
define('MUSICWORDZ_MENU_PLACE_LIST','Listar y editar salas | festivales');
define('MUSICWORDZ_MENU_PLACE_BIN','Papelera de reciclaje');

define('MUSICWORDZ_MENU_ONEPLACE', MUSICWORDZ_MENU_PLACE);
define('MUSICWORDZ_MENU_ONEPLACE_NEW', MUSICWORDZ_MENU_PLACE_NEW);
define('MUSICWORDZ_MENU_ONEPLACE_LIST', MUSICWORDZ_MENU_PLACE_LIST);
define('MUSICWORDZ_COMMENT_ONEPLACE', 'Sólo editar salas | festivales propios del artista');

// captions seccio
$gl_caption_performance['c_performance'] = 'Actuación';
$gl_caption_performance['c_entered'] = 'Creada';
$gl_caption_performance['c_performance_date'] = 'Fecha actuación';
$gl_caption_performance['c_place_id'] = 'Sala | Festival';
$gl_caption['c_status'] = 'Estat';
$gl_caption['c_status_prepare'] = 'En preparación';
$gl_caption['c_status_review'] = 'Para tramitar';
$gl_caption_place['c_status_review'] = 'Para revisar';
$gl_caption['c_status_approved'] = 'Aprobada';
$gl_caption_song['c_status_approved'] = 'Tramitada';
$gl_caption_performance['c_status_approved'] = 'Declarada';
$gl_caption['c_entered'] = 'Fecha creació';
$gl_caption['c_modified'] = 'Modificado';

$gl_caption_place['c_place'] = 'Sala | Festival';
$gl_caption_place['c_address'] = 'Dirección';
$gl_caption_place['c_zip'] = 'Codigo postal';
$gl_caption_place['c_phone1'] = 'Teléfono';
$gl_caption_place['c_mail'] = 'E-mail';
$gl_caption_place['c_country_id'] = 'País';
$gl_caption_place['c_provincia_id'] = 'Provincia';
$gl_caption_place['c_comarca_id'] = 'Comarca';
$gl_caption_place['c_municipi_id'] = 'Municipio';

$gl_caption['c_song_id'] = 'Canción';
$gl_caption_song['c_song'] = 'Canción';
$gl_caption_song['c_durada'] = 'Duración ( mm:ss )';
$gl_caption_song_list['c_durada'] = 'Duración';
$gl_caption['c_user_id'] = 'Artista';
$gl_caption['c_form_level1_title_entered'] =
$gl_caption['c_form_level1_title_performance_date'] = "Fechas";


$gl_caption['c_filter_user_id'] = 'Todos los artistas';
$gl_caption['c_filter_status'] = 'Todos los estados';

$gl_caption['c_title_missatges_to_approve'] = "Están pendientes de aprobar:";
$gl_caption['c_title_missatges_songs_to_approve'] = "Canciones:";
$gl_caption['c_title_missatges_performances_to_approve'] = "Actuaciones:";
$gl_caption['c_title_missatges_places_to_approve'] = "Salas | Festivales:";
$gl_caption['c_title_missatges_to_approve_link'] = "Ir al listado";

$gl_caption['c_artistic_name'] = "Nombre artístico";

$gl_caption['c_song_subject'] = 'La canción "%s" ha sido aprobada';
$gl_caption['c_see_songs'] = "Ver canciones";

$gl_caption['c_performance_subject'] = 'La actuación "%s" ha sido declarada';
$gl_caption['c_see_performances'] = "Ver actuaciones";

?>
