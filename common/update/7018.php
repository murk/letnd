<?

/* 12-6-17
  Afegir pagament a booking

 */

if(is_table( 'booking__book' )){

	if(!is_table( 'booking__configpublic' )){

		$update_sql []="
			CREATE TABLE IF NOT EXISTS `booking__configpublic` (
			  `configpublic_id` int(11) NOT NULL auto_increment,
			  `name` varchar(50) collate utf8_spanish2_ci NOT NULL,
			  `value` varchar(1024) collate utf8_spanish2_ci NOT NULL default '',
			  PRIMARY KEY  (`configpublic_id`)
			) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;";

		$update_sql []="
			INSERT INTO `booking__configpublic` (`configpublic_id`, `name`, `value`) VALUES
			 	(1, 'google_analytics_conversion', ''),
				(3, 'lacaixa_clave_sha2', ''),
				(4, 'lacaixa_merchant_code', ''),
				(5, 'lacaixa_simulador', '1'),
				(6, 'lacaixa_terminal', '1'),
				(7, 'payment_methods', 'paypal,lacaixa'),
				(8, 'paypal_mail', ''),
				(9, 'paypal_sandbox', '1');";


	}else{
		trigger_error( 'Ja existeixen la taula booking__configpublic' );
	}

}


?>