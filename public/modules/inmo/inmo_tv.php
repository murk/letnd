<?

/**
 * InmoAgent
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2011
 * @version $Id$
 * @access public
 */
class InmoTv extends Module {

	function __construct() {
		parent::__construct();
	}

	function list_records() {

		$is_local = $GLOBALS['gl_is_local'];

		$module         = Module::load( 'inmo', 'property' );
		$module->parent = 'inmo__tv';
		$listing        = $module->do_action( 'get_records' );


		$listing->condition .= ' AND property_id IN (' . implode( ',', $this->get_tv_ids() ) . ')';

		$listing->limit = 200;

		unset ( $listing->call_function['description'] );

		$client_path = CLIENT_PATH;
		$client_dir  = CLIENT_DIR;
		$custom_css_src = '';

		// Si el client té plantilla, també ha de tindre js i css
		// ( o es pot posar el js comú dins la plantilla si no es vol duplicar
		if ( file_exists( "${client_path}templates/inmo/tv_list.tpl" ) ) {
			$listing->template = "inmo/property_tv.tpl";
			$dir_templates     = "/${client_dir}/templates/tv/";
		}
		else {
			$listing->tpl->path = DOCUMENT_ROOT . '/public/themes/common';
			$listing->template  = 'inmo/tv_list.tpl';
			$dir_templates      = "/public/themes/common/inmo/tv/";
			
			if ( file_exists( "${client_path}templates/styles/inmo_tv.css" )){
				$custom_css_src =  "/${client_dir}/templates/styles/inmo_tv.css";
			}
			
		}

		$js_src  = $is_local ? 'jscripts.js' : 'jscripts.min.js';
		$css_src = $is_local ? 'styles.css' : 'styles.min.css';

		$listing->set_var( 'dir_templates', $dir_templates );
		$listing->set_var( 'js_name', $js_src );
		$listing->set_var( 'css_name', $css_src );
		$listing->set_var( 'js_src', $dir_templates . $js_src );
		$listing->set_var( 'css_src', $dir_templates . $css_src );
		$listing->set_var( 'custom_css_src', $custom_css_src );
		$listing->set_var( 'logo', $this->get_logo( $client_path, $client_dir ) );

		$content = $listing->list_records();


		html_end( $content );
	}

	function get_logo( $client_path, $client_dir ) {

		if ( file_exists( "$client_path/images/logo_tv.png" ) ) {
			$logo = "/$client_dir/images/logo_tv.png";
		}
		elseif ( file_exists( "$client_path/images/logo_showwindow.png" ) ) {
			$logo = "/$client_dir/images/logo_showwindow.png";
		}
		elseif ( file_exists( "$client_path/images/logo_showwindow.jpg" ) ) {
			$logo = "/$client_dir/images/logo_showwindow.jpg";
		}
		else {
			$logo = '';
		}

		return $logo;

	}

	function get_tv_ids() {

		$query   = "SELECT property_id FROM inmo__property_to_tv";
		$results = Db::get_rows( $query );
		$ret     = array();
		foreach ( $results as $rs ) {
			$ret[] = $rs['property_id'];
		}
		if ( ! $ret ) $ret[] = 0;

		return $ret;
	}

	static function get_vw( $pixels ) {
		return ( $pixels / (1920 * 0.01) ) . 'vw';
	}
}