<?php
//menus com grafic de durades de visites per visitant


require_once INCLUDE_PATH . "/libs/artichow/BarPlot.class.php";
require_once INCLUDE_PATH . "/core/include/graphs/PmvGraph.class.php";

class GraphVerticalBar extends PmvGraph
{
	function GraphVerticalBar( $width, $height)
	{
		parent::PmvGraph($width, $height);	
	}
	
	function setData( $a_data )
	{
		$this->x = $a_data['axis'];
		$this->y = $a_data['data'];
		$this->title = $a_data['title'];
	}
	
	function process()
	{
		parent::process();
		
		$this->plot = new BarPlot($this->y);
		
		$this->plot->grid->setType( LINE_DASHED );
		
		$this->setPmvPadding($this->plot);		
		$this->setPmvTitle( $this->plot );
		$this->setPmvBackgroundGradient($this->plot);
		$this->setPmvBarShadowProperties( $this->plot );
		$this->setPmvLabelProperties( $this->plot, $this->y );
		$this->setPmvBarBorderProperties( $this->plot );
		$this->setPmvBarSize( $this->plot );		
		$this->setPmvBarGradient( $this->plot );
		
		$this->plot->xAxis->setColor( new Color(  47, 47, 144) ); //barra horitzontal
		$this->plot->yAxis->setColor( new Color(  47, 47, 144) ); //barra vertiacal
		$this->plot->xAxis->label->setFont($this->font8);
		
		$this->plot->xAxis->setLabelText($this->x);
	}
	
	function display()
	{
		$this->graph->add($this->plot);
		$this->draw();
	}
}
?>