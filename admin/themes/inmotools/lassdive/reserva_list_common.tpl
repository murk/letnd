

			<td>
				<?= $ref ?>
				<input name="<?= $inputs_prefix ?>[reservaitem_id][<?= $reservaitem_id ?>]" type="hidden" value="<?= $reservaitem_id ?>"/>
			</td>
			<? if ( $is_orderitems_view ): ?>
			<td>
				<? if ( $product_original_title ): ?>
					<a href="/admin/?action=show_form_edit&menu_id=20004&product_id=<?= $product_id ?>">
				<?= $product_original_title ?>
				<? endif //product_original_title?>
			</td>
			<? endif //is_orderitems_view?>
			<td>
				<? if ( $product_title ): ?>
					<a href="/admin/?action=show_form_edit&menu_id=20004&product_id=<?= $product_id ?>">
						<?= $product_title ?>
					</a>
				<? endif //product_title?>
				<? foreach($product_variation_loop as $l): extract( $l ) ?>
					<div class="variations"><strong><?= $product_variation_category ?></strong>: <?= $product_variation ?></div>
				<? endforeach //$product_variations_loop?>
			</td>

			<? if ( !$is_orderitems_view ): ?>
			<td class="product-date">
				<?= $product_date ?>
			</td>
			<? endif //is_orderitems_view?>

			<td class="reservaitem_date">
				<?= $reservaitem_date ?>
			</td>
			<td class="start-time">
				<?= $start_time ?>
			</td>
			<td class="end-time">
				<?= $end_time ?>
			</td>
			<td class="centre-id">
				<?= $centre_id ?>
			</td>
			<td class="user-id">
				<div>
					<div data-reservaitem-id="<?= $reservaitem_id ?>" class="button-user-id"></div>
					<span><?= $user_id ?></span>
				</div>
			</td>
			<td>
				<?= $is_done ?>
			</td>

			<? if ( $is_orderitems_view ): ?>
			<td>
				<?= $order_status ?>
			</td>
			<td>
				<?= $payment_method ?>
			</td>
			<td>
				<?= $deposit_status ?>
			</td>
			<td>
				<?= $deposit_payment_method ?>
			</td>
			<? endif //is_orderitems_view?>

			<? if ( $is_orderitems_view ): ?>
			<td>
				<?= $deposit ?>&nbsp;<?= CURRENCY_NAME ?>
			</td>
			<td>
				<?= $deposit_rest ?>&nbsp;<?= CURRENCY_NAME ?>
			</td>
			<? endif //is_orderitems_view?>

			<td class="align-right">
				<?= $product_basetax ?>&nbsp;<?= CURRENCY_NAME ?>
			</td>

			<td class="align-right reservaitem-buttons">
				<div data-reservaitem-id="<?= $reservaitem_id ?>" class="button-time"></div>
				<? if ( $is_orderitems_view ): ?>
					<a href="/admin/?menu_id=2303&action=show_form_edit&order_id=<?= $order_id ?>" class="button-edit-admin"></a>
				<? endif // $is_reserva_form ?>
				<? if ( $is_reserva_form ): ?>
					<div data-reservaitem-id="<?= $reservaitem_id ?>" class="button-delete-time"></div>
					<div data-reservaitem-id="<?= $reservaitem_id ?>" class="button-delete"></div>
				<? endif // $is_reserva_form ?>
			</td>