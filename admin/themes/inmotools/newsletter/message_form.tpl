<script language="JavaScript" src="/admin/jscripts/validacio.js?v=<?=$version?>"></script>
<script language="JavaScript" src="/admin/jscripts/assign.js?v=<?=$version?>"></script>
<script language="JavaScript" src="/admin/modules/newsletter/jscripts/mail.js?v=<?=$version?>"></script>
<script language="JavaScript" src="/admin/modules/newsletter/jscripts/send_mail.js?v=<?=$version?>"></script>
<form name="form_delete" method="post" action="<?=$link_action_delete?>" target="save_frame">
    <input name="<?=$id_field?>" type="hidden" value="<?=$id?>">
</form><form  action="<?=$link_action?>" onsubmit="<?=$js_string?>"  method="post" enctype="multipart/form-data" name="theForm" target="save_frame">
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="form">
  <tr>
    <td> 
<div id="form_buttons_holder"><table id ="form_buttons">
  <tr>
    <td class="buttons_top"><div class="right">
      <?if (!$record):?>
      <input type="submit" name="send" value="<?=$c_send?>" class="btn_form1" /><div class="btn_sep"></div><input type="submit" name="save" value="<?=$c_save?>" class="btn_form1" /><?endif //record?></div></td>
  </tr>
</table></div>
<table width="100%" border="0" cellpadding="0" cellspacing="0">  
    <tr>
      <td><table width="100%" cellpadding="0" cellspacing="0">
          <tr>
            <td class="formCaptionTitle"><strong>
              <?=$c_message?>
            </strong></td>
          </tr>
        </table>
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td><table width="100%" cellpadding="0" cellspacing="0" class="formstbl">

              <tr>
                <td width="150" valign="top" nowrap="nowrap" class="formsCaption"><strong>
                  <?=$c_send_to_addresses?>: </strong></td>
                <td class="forms"><?if (!$record):?>
                    <a href="javascript:select_addresses()">
                    <?=$c_select_addresses?>
                    </a><?=$hidden_content_extra?><?=$hidden_demand_results?>
                    <input name="selected_addresses_ids" type="hidden" value="<?=$selected_addresses_ids?>" />
                    <input name="selected_addresses_names" type="hidden" value="<?=$selected_addresses_names?>" />
                    <br />
                    <br />
                    <?endif //record?>
                    <div id="selected_addresses_text_selected" class="text_selected"><?=$selected_addresses_namesp?></div></td>
              </tr>
              <tr>
                <td width="150" valign="top" nowrap="nowrap" class="formsCaption"><strong>
                  <?=$c_group_id?>:</strong></td>
                <td class="forms"><?=$group_id?></td>
              </tr>
            </table>
              <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                  <td class="formsCaptionHor"><?=$c_subject?></td>
                </tr>
              </table>
              <table width="100%" cellpadding="0" cellspacing="0" class="formstbl">
                <tr>
                  <td class="forms"><input name="message_id[<?=$message_id?>]" type="hidden" value="<?=$message_id?>" />
                    <input name="message_id_copy[<?=$message_id?>]" type="hidden" value="<?=$id_copy?>" />
					<input name="message_id_javascript" type="hidden" value="<?=$message_id?>" />
                    <?=$send_method?>
                    <?=$templatetext_id?>
                    <?=$format?>
                  <?=$subject?></td>
                </tr>
                <tr>
                  <td class="formsHor"><?=$message?></td>
                </tr>
              </table><div class="sep_soft"></div> 
              <table width="100%" cellpadding="4" cellspacing="0" class="formstbl">
                <tr>
                  <td valign="top" nowrap="nowrap" class="formsCaption"><strong>
                    <?=$c_picture?>:&nbsp;</strong></td>
                  <td class="forms"><?=$picture?></td>
                </tr>
                <tr>
                  <td width="150" valign="top" nowrap="nowrap" class="formsCaption"><strong>
                    <?=$c_file?>:</strong></td>
                  <td class="forms"><?=$file?></td>
                </tr>
                <tr>
                  <td width="150" valign="top" nowrap="nowrap" class="formsCaption"><strong>
                    <?=$c_templatehtml_id?>: </strong></td>
                  <td class="forms"><?=$templatehtml_id?></td>
                </tr>
                <tr>
                  <td width="150" valign="top" nowrap="nowrap" class="formsCaption"><strong>
                    <?=$c_language_id?>:</strong></td>
                  <td class="forms"><?=$language_id?>

	                  <table border="0" cellspacing="0" cellpadding="0" class="padding-top-2">
		                  <tr>
			                  <td><label><?= $send_to_same_language_only ?> <?= $c_send_to_same_language_only ?></label>
			                  </td>
		                  </tr>
		                  <tr id="send_to_no_language_too_holder" class="<?= $send_to_same_language_only_value ? '' : 'hidden' ?>">
			                  <td><label><?= $send_to_no_language_too ?> <?= $c_send_to_no_language_too ?></label></td>
		                  </tr>
	                  </table>


                  </td>
                </tr>


                  <? if ( $inmo_property ): ?>
                      <tr>
                      <td valign="top" nowrap="nowrap" class="formsCaption">
                          <strong>
                              <?= $c_select_property ?>:&nbsp;</strong></td>
                      <td class="forms select_related"><? if ( ! $record ): ?>
                              <input name="message_return_url" type="hidden" value="<?= $message_return_url ?>" />
                              <input name="related_property_ids" type="hidden" value="<?= $related_property_ids ?>"/>
                              <input name="selected_inmo_property_ids" type="hidden" value="<?= $selected_inmo_property_ids ?>"/>
                              <input name="selected_inmo_property_names" type="hidden" value="<?= $selected_inmo_property_names ?>"/>
                              <a href="javascript:select_properties()">
                                  <?= $c_select_inmo_property_link ?>
                              </a>&nbsp;&nbsp;
                              <?= $c_select_inmo_property_show_record ?>
                              <input name="selected_inmo_property_action" type="radio" value="show_record" <?= $selected_inmo_property_show_record ?> />
                              &nbsp;
                              <?= $c_select_inmo_property_list_records ?>
                              <input name="selected_inmo_property_action" type="radio" value="list_records" <?= $selected_inmo_property_list_records ?> />
                              <br/>
                              <br/><? endif //record?>
                          <table width="96%" border="0" cellspacing="0" cellpadding="2">
                              <tr>
                                  <td id="selected_inmo_property_text_selected" class="text_selected"><?= $selected_inmo_property_namesp ?></td>
                              </tr>
                          </table>
                      </td>
                      </tr><? endif //inmo_property?>



                  <? if ( $product_product ): ?>
                      <tr>
                      <td valign="top" nowrap="nowrap" class="formsCaption">
                          <strong>
                              <?= $c_select_product ?>:&nbsp;</strong></td>
                      <td class="forms select_related"><? if ( ! $record ): ?>
                              <input name="selected_product_product_ids" type="hidden" value="<?= $selected_product_product_ids ?>" />
                              <input name="selected_product_product_names" type="hidden" value="<?= $selected_product_product_names ?>"/>
                              <a href="javascript:select_products()">
                                  <?= $c_select_product_product_link ?>
                              </a>&nbsp;&nbsp;
                              <?= $c_select_product_product_show_record ?>
                              <input name="selected_product_product_action" type="radio" value="show_record" <?= $selected_product_product_show_record ?> />
                              &nbsp;
                              <?= $c_select_product_product_list_records ?>
                              <input name="selected_product_product_action" type="radio" value="list_records" <?= $selected_product_product_list_records ?> />
                              <br/>
                              <br/><? endif //record?>
                          <table width="96%" border="0" cellspacing="0" cellpadding="2">
                              <tr>
                                  <td id="selected_product_product_text_selected" class="text_selected"><?= $selected_product_product_namesp ?></td>
                              </tr>
                          </table>
                      </td>
                      </tr><? endif //product_product?>



                  <? if ( $news_new ): ?>
                      <tr>
                      <td valign="top" nowrap="nowrap" class="formsCaption">
                          <strong>
                              <?= $c_select_new ?>:&nbsp;</strong></td>
                      <td class="forms select_related"><? if ( ! $record ): ?>
                              <input name="selected_news_new_ids" type="hidden" value="<?= $selected_news_new_ids ?>" />
                              <input name="selected_news_new_names" type="hidden" value="<?= $selected_news_new_names ?>"/>
                              <a href="javascript:select_news()">
                                  <?= $c_select_news_new_link ?>
                              </a>&nbsp;&nbsp;
                              <?= $c_select_news_new_show_record ?>
                              <input name="selected_news_new_action" type="radio" value="show_record" <?= $selected_news_new_show_record ?> />
                              &nbsp;
                              <?= $c_select_news_new_list_records ?>
                              <input name="selected_news_new_action" type="radio" value="list_records" <?= $selected_news_new_list_records ?> />
                              <br/>
                              <br/><? endif //record?>
                          <table width="96%" border="0" cellspacing="0" cellpadding="2">
                              <tr>
                                  <td id="selected_news_new_text_selected" class="text_selected"><?= $selected_news_new_namesp ?></td>
                              </tr>
                          </table>
                      </td>
                      </tr><? endif //news_new?>



                  <? if ( $llemena_agenda ): ?>
                      <tr>
                      <td valign="top" nowrap="nowrap" class="formsCaption">
                          <strong>
                              <?= $c_select_agenda ?>:&nbsp;</strong></td>
                      <td class="forms select_related"><? if ( ! $record ): ?>
                              <input name="selected_llemena_agenda_ids" type="hidden" value="<?= $selected_llemena_agenda_ids ?>" />
                              <input name="selected_llemena_agenda_names" type="hidden" value="<?= $selected_llemena_agenda_names ?>"/>
                              <a href="javascript:select_agendas()">
                                  <?= $c_select_llemena_agenda_link ?>
                              </a>&nbsp;&nbsp;
                              <?= $c_select_llemena_agenda_show_record ?>
                              <input name="selected_llemena_agenda_action" type="radio" value="show_record" <?= $selected_llemena_agenda_show_record ?> />
                              &nbsp;
                              <?= $c_select_llemena_agenda_list_records ?>
                              <input name="selected_llemena_agenda_action" type="radio" value="list_records" <?= $selected_llemena_agenda_list_records ?> />
                              <br/>
                              <br/><? endif //record?>
                          <table width="96%" border="0" cellspacing="0" cellpadding="2">
                              <tr>
                                  <td id="selected_llemena_agenda_text_selected" class="text_selected"><?= $selected_llemena_agenda_namesp ?></td>
                              </tr>
                          </table>
                      </td>
                      </tr><? endif //llemena_agenda?>



                  <? if ( $lassdive_activitat ): ?>
                      <tr>
                      <td valign="top" nowrap="nowrap" class="formsCaption">
                          <strong>
                              <?= $c_select_activitat ?>:&nbsp;</strong></td>
                      <td class="forms select_related"><? if ( ! $record ): ?>
                              <input name="selected_lassdive_activitat_ids" type="hidden" value="<?= $selected_lassdive_activitat_ids ?>" />
                              <input name="selected_lassdive_activitat_names" type="hidden" value="<?= $selected_lassdive_activitat_names ?>"/>
                              <a href="javascript:select_activitats()">
                                  <?= $c_select_lassdive_activitat_link ?>
                              </a>&nbsp;&nbsp;
                              <?= $c_select_lassdive_activitat_show_record ?>
                              <input name="selected_lassdive_activitat_action" type="radio" value="show_record" <?= $selected_lassdive_activitat_show_record ?> />
                              &nbsp;
                              <?= $c_select_lassdive_activitat_list_records ?>
                              <input name="selected_lassdive_activitat_action" type="radio" value="list_records" <?= $selected_lassdive_activitat_list_records ?> />
                              <br/>
                              <br/><? endif //record?>
                          <table width="96%" border="0" cellspacing="0" cellpadding="2">
                              <tr>
                                  <td id="selected_lassdive_activitat_text_selected" class="text_selected"><?= $selected_lassdive_activitat_namesp ?></td>
                              </tr>
                          </table>
                      </td>
                      </tr><? endif //lassdive_activitat?>
				
				
				
              </table>
              </td>
          </tr>
        </table>
        <table width="100%" cellpadding="4" cellspacing="2" class="formsButtonstbl">
          <tr>
            <td align="center" class="formsButtons"><div class="centerFloat1">
                        <div class="centerFloat2"><?if (!$record):?>
            <input type="button" name="preview" value="<?=$c_preview?>" class="btn_form1" onclick="preview_mail(this.form)"><?endif //record?></div></div></td>
          </tr>
        </table>
		  <div class="sep_dark"></div> 
        <table width="100%" cellpadding="4" cellspacing="0">
          <tr>
            <td class="formCaptionTitle"><strong>
              <?=$c_addresses_options?>
            </strong></td>
          </tr>
        </table>
        <table width="100%" cellpadding="4" cellspacing="0" class="formstbl">
          <tr>
            <td width="150" class="formsCaption"><table border="0" cellspacing="4" cellpadding="0">
                <tr>
                  <td colspan="2" nowrap><strong class="formsCaption">
                    <?=$c_from_title?>:</strong></td>
                </tr>
                <tr>
                  <td><strong>&nbsp;&nbsp;</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                  <td nowrap><?=$c_from_address?>
                    (*)</td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td nowrap><?=$c_from_name?></td>
                </tr>
              </table></td>
            <td class="forms"><table width="90%" border="0" cellspacing="4" cellpadding="0">
                <tr>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td><?=$from_address?>
                  </td>
                </tr>
                <tr>
                  <td><?=$from_name?>
                  </td>
                </tr>
              </table></td>
          </tr>
          <tr>
            <td class="formsCaption"><table border="0" cellspacing="4" cellpadding="0">
                <tr>
                  <td colspan="2" nowrap><strong class="formsCaption">
                    <?=$c_reply_to_title?>:&nbsp;</strong></td>
                </tr>
                <tr>
                  <td><strong>&nbsp;&nbsp;</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                  <td nowrap><?=$c_reply_to?></td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td nowrap><?=$c_reply_to_name?></td>
                </tr>
              </table></td>
            <td class="forms"><table width="90%" border="0" cellspacing="4" cellpadding="0">
                <tr>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td><?=$reply_to?>
                  </td>
                </tr>
                <tr>
                  <td><?=$reply_to_name?>
                  </td>
                </tr>
              </table></td>
          </tr>
        </table>
        <table width="100%" cellpadding="4" cellspacing="2" class="formsButtonstbl">
          <tr>
            <td align="center" class="formsButtons"><div class="centerFloat1">
                        <div class="centerFloat2"><?if (!$record):?><input type="submit" name="send" value="<?=$c_send?>" class="btn_form1" /><input type="submit" name="save" value="<?=$c_save?>" class="btn_form1" /><?endif //record?></div></div>
            </td>
          </tr>
        </table></td>
    </tr>
</table></td>
  </tr>      
</table>
</form>
  <form action="/admin/?action=show_preview&menu_id=5" method="post" name="previewForm" target="popupFrame">
  </form>
