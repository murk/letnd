<?php
// No hi ha taulka de categories, està fet amb parent_ids
global $gl_is_home;

// Busco la categoria seleccionada
$file_name          = R::file_name( 'file_name' );
$pack               = 0;
$selected_activitat = false;
$action             = $GLOBALS['gl_action'];

if ( $file_name ) {
	$selected_activitat = Db::get_first( "SELECT activitat_id 
					FROM lassdive__activitat_language 
					WHERE language= '" . LANGUAGE . "'
					AND activitat_file_name = '" . $file_name . "'" );
	if ( ! $selected_activitat ) {
		$selected_activitat = $file_name;
	}

}

if ( $selected_activitat || $action == 'packs' || $action == 'events' || $action == 'custom' || $action == 'holidays' ) {

	if ( ! $selected_activitat ) {
		if ($action == 'packs') $selected_activitat = ACTIVITAT_PACKS;
		if ($action == 'events') $selected_activitat = ACTIVITAT_EVENTS;
		if ($action == 'custom') $selected_activitat = ACTIVITAT_CUSTOM;
		if ($action == 'holidays') $selected_activitat = ACTIVITAT_HOLIDAYS;
	}

	// primer miro si es packs
	$pack_query = "SELECT count(*) 
			FROM lassdive__activitat 
			WHERE   activitat_id = $selected_activitat 
				AND (
			        (is_pack = 1)
			        OR 
			        ( SELECT COUNT(*)
						FROM lassdive__activitat AS second
						WHERE second.activitat_id = lassdive__activitat.parent_id 
						AND is_pack = 1 )
		        )";

	// Miro que no estigui dins dels packs
	$pack = Db::get_first( $pack_query ) ? 1 : 0;


	// segon miro si es events
	if ( ! $pack ) {
		$pack_query = "SELECT count(*) 
			FROM lassdive__activitat 
			WHERE   activitat_id = $selected_activitat 
				AND (
			        (is_pack = 2)
			        OR 
			        ( SELECT COUNT(*)
						FROM lassdive__activitat AS second
						WHERE second.activitat_id = lassdive__activitat.parent_id 
						AND is_pack = 2 )
		        )";

		// Miro que no estigui dins dels packs
		$pack = Db::get_first( $pack_query ) ? 2 : 0;
	}


	// segon miro si es custom
	if ( ! $pack ) {
		$pack_query = "SELECT count(*) 
			FROM lassdive__activitat 
			WHERE   activitat_id = $selected_activitat 
				AND (
			        (is_pack = 3)
			        OR 
			        ( SELECT COUNT(*)
						FROM lassdive__activitat AS second
						WHERE second.activitat_id = lassdive__activitat.parent_id 
						AND is_pack = 3 )
		        )";

		// Miro que no estigui dins dels packs
		$pack = Db::get_first( $pack_query ) ? 3 : 0;
	}


	// segon miro si es holidays
	if ( ! $pack ) {
		$pack_query = "SELECT count(*) 
			FROM lassdive__activitat 
			WHERE   activitat_id = $selected_activitat 
				AND (
			        (is_pack = 4)
			        OR 
			        ( SELECT COUNT(*)
						FROM lassdive__activitat AS second
						WHERE second.activitat_id = lassdive__activitat.parent_id 
						AND is_pack = 4 )
		        )";

		// Miro que no estigui dins dels packs
		$pack = Db::get_first( $pack_query ) ? 4 : 0;
	}
}

$query = "SELECT lassdive__activitat.activitat_id, activitat, activitat_file_name
				FROM lassdive__activitat_language, lassdive__activitat
				WHERE lassdive__activitat_language.activitat_id = lassdive__activitat.activitat_id
				AND language = '" . LANGUAGE . "'
				AND parent_id = 0
				AND status = 'public'
				AND bin <> 1
				AND is_pack = $pack
				ORDER BY ordre ASC, activitat ASC, activitat_id";

Debug::p( [
	[ 'pack', $pack ],
	[ 'pack_query', $pack_query ],
	[ 'query', $query ],
], 'lassdiveBlock' );

$results = Db::get_rows( $query );


$selected_category = $selected_activitat ? Db::get_first( "SELECT parent_id FROM lassdive__activitat WHERE activitat_id = " . $selected_activitat ) : false;


if ( ! $selected_category ) {
	$selected_category = $selected_activitat;
} // pot ser que estiguem mostrant la fitxa de la categoria

Debug::p( 'activitat', $selected_activitat );
Debug::p( 'categoria', $selected_category );

$selected_found = false;
if ( $results ) {
	$conta      = 0;
	$loop_count = count( $results );
	$loop       = array();
	foreach ( $results as $rs ) {
		$l      = array();
		$conta2 = 0;
		$loop2  = array();

		$query = "SELECT lassdive__activitat.activitat_id AS activitat_id, 
					activitat, activitat_file_name
					FROM lassdive__activitat_language, lassdive__activitat
					WHERE lassdive__activitat_language.activitat_id = lassdive__activitat.activitat_id
					AND (status = 'public')
					AND bin <> 1
					AND parent_id = '" . $rs['activitat_id'] . "'
					AND LANGUAGE = '" . LANGUAGE . "'
					ORDER BY ordre ASC, activitat ASC, activitat_id";

		$results_activitats = Db::get_rows( $query );

		foreach ( $results_activitats as $rs2 ) {
			$l2         = array();
			$l2['link'] =
				get_link( array(
					'id'           => $rs2['activitat_id'],
					'file_name'    => $rs2['activitat_file_name'],
					'tool'         => 'lassdive',
					'tool_section' => 'activitat',
					'page_id'      => $pack == 0 ? 9 : ($pack == '1' ? 11: 12)
				) );


			if ( $selected_activitat == $rs2["activitat_id"] ) {
				$selected       = 1;
				$selected_found = true;
			}
			else {
				$selected = 0;
			}
			$l2['selected'] = $selected;


			$l2['activitat']  = $rs2['activitat'];
			$l2['id']         = $rs2['activitat_id'];
			$l2['conta']      = $conta2 ++;
			$l2['class']      = '';
			$l2['loop_count'] = count( $results_activitats );
			$loop2[]          = $l2;

		}

		if ( $loop2 ) {
			$loop2[0]['class']                     = 'first';
			$loop2[ count( $loop2 ) - 1 ]['class'] = 'last';
		}

		$l['loop']       = $loop2;
		$l['link']       =
			get_link( array(
				'id'           => $rs['activitat_id'],
				'file_name'    => $rs['activitat_file_name'],
				'tool'         => 'lassdive',
				'tool_section' => 'activitat',
				'page_id'      => $pack == 0 ? 9 : ($pack == '1' ? 11: 12)
			) );
		$l['activitat']  = $rs['activitat'];
		$l['id']         = $rs['activitat_id'];
		$l['conta']      = $conta ++;
		$l['class']      = '';
		$l['loop_count'] = $loop_count;
		if ( $selected_category == $rs["activitat_id"] ) {
			$selected       = 1;
			$selected_found = true;
		}
		else {
			$selected = 0;
		}
		$l['selected'] = $selected;

		$loop[] = $l;

	}
	if ( $loop ) {
		$loop[0]['class']                    = 'first';
		$loop[ count( $loop ) - 1 ]['class'] = 'last';
		if ( ! $selected_found ) {
			$loop[0]['selected']            = 1;
			$loop[0]['loop'][0]['selected'] = 1;
		}
	}

	Debug::p( $loop );

}
else {
	/* si no hi ha cap resultat no mostro el block */
	$show_block = false;

	return;
}