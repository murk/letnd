<?

/* 14-10-14 
  ACtualitzacions Temporades colors

 */
if (is_table('booking__book') && !is_table('booking__color')) {
	
	
	
	$update_sql []="
		CREATE TABLE `booking__color` (
			`color_id` INT(11) NOT NULL AUTO_INCREMENT,
			`color` VARCHAR(15) NOT NULL DEFAULT '0' COLLATE 'utf8_spanish2_ci',
			PRIMARY KEY (`color_id`)
		)
		COLLATE='utf8_spanish2_ci'
		ENGINE=MyISAM;
		";
	
	
	$update_sql []="
		INSERT INTO `booking__color` (`color_id`, `color`) VALUES
			(1, '204,206,1'),
			(2, '175,177,7'),
			(3, '143,145,2'),
			(4, '253,196,21'),
			(5, '244,150,0'),
			(6, '195,120,0'),
			(7, '233,81,14'),
			(8, '255,0,0'),
			(9, '207,0,0'),
			(10, '227,50,140'),
			(11, '187,10,126'),
			(12, '149,27,128'),
			(13, '82,191,211'),
			(14, '1,164,169'),
			(15, '4,136,140'),
			(16, '144,181,255'),
			(17, '3,86,254'),
			(18, '3,59,173');
		";

}
elseif (is_table('booking__book')){	
	trigger_error('Ja existeix booking__color');
}

?>