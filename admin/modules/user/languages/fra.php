<?
if (!defined('USER_LOGIN_TITLE')) {

	define('USER_LOGIN_TITLE','Bienvenue à Letnd');
	define('USER_LOGOUT_TITLE','Vous êtes déconnecté');
	define('USER_LOGIN_TEXT','Entrez le login et le mot de passe et cliquez accepter');
	define('USER_LOGIN_AGAIN_TEXT','Votre session a expiré. Retour de entrer le login et le mot de passe');
	define('USER_LOGOUT_TEXT','Pour reconnecter, entrez le login et le mot de passe');
	// menus eina
	define('USER_MENU_USER','Utilisateurs');
	define('USER_MENU_USER_NEW','Créer un utilisateur');
	define('USER_MENU_USER_LIST','Énumérer les utilisateurs');
	define ('USER_MENU_USER_BIN','Corbeille');
	define('USER_MENU_CONCESSION','Privilèges');
	define('USER_MENU_CONCESSION_LIST','Attribuer des privilègess');
	define ('USER_MENU_GROUP','Groupes d\'utilisateurs');
	define('USER_MENU_GROUP_NEW','Créer un groupe');
	define('USER_MENU_GROUP_LIST','Énumérer les groupes');
	define ('USER_MENU_GROUP_BIN','Corbeille');
	define ('USER_MENU_LOG','Histoire');
	define ('USER_MENU_LOG_LIST','Énumérer historique');
	define ('USER_LOGIN_BROWSER_INCORRECT','Pour utiliser ce programme, vous devez au moins avoir Internet Explorer 8, Firefox 3.6, Safari 5 o Google Chrome 7<br><br>vous utilisez la version%s %s');
	define('USER_MENU_ONEUSER', USER_MENU_USER);
	define('USER_MENU_ONEUSER_EDIT', 'Modifier les données');
	define('USER_COMMENT_ONEUSER', 'Seulement modifier les données de l\'utilisateur');

	define('USER_MENU_PUBLIC','Utilisateurs publics');
	define('USER_MENU_PUBLIC_NEW','Créer un utilisateuri');
	define('USER_MENU_PUBLIC_LIST','Énumérer les utilisateurs');
	define ('USER_MENU_PUBLIC_BIN','Corbeille');
}


$users_login_form['c_login'] = 'Login';
$users_login_form['c_password'] = 'Password';
$users_login_form['c_send'] = 'Accepter';







// captions seccio
$gl_caption_concession['c_tool'] = 'Module';
$gl_caption_concession['c_tool_section'] = 'Section';
$gl_caption_concession['c_read1'] = 'Lecture';
$gl_caption_concession['c_write1'] = 'Écriture';
$gl_caption_concession['c_group_id'] = '';
$gl_caption_concession['c_comment'] = '';
$gl_caption_concession['c_bin'] = '';
$gl_caption_concession['c_search1'] = 'Choisissez groupe:';
$gl_caption_group['c_name']= "Nom du groupe";
$gl_caption_group['c_view_users']= "Énumérer les utilisateurs";
$gl_caption_group['c_show_html_editor']= "Permettre html en WYSIWYG";
$gl_caption_group['c_show_seo']= "Permettre SEO";
$gl_caption_group['c_is_editable']= "Editable";
$gl_caption_group['c_show_seo_0']= "No";
$gl_caption_group['c_show_seo_1']= "Si";
$gl_caption_user['c_group_id']="Nom du groupe";
$gl_caption_user['c_name']= "Prénom";
$gl_caption_user['c_surname']= "Noms";
$gl_caption_user['c_login']= "Nom d'utilisateur";
$gl_caption_user['c_passw']= "Mot de passe";
$gl_caption_user['c_passw_repeat']= "Répetez mot de passe";
$gl_caption_user['c_email']= "E-mail";
$gl_caption_user['c_email_repeat']= "Répetez E-mail";
$gl_caption_log['c_ip']= "Ip du ordinateur";
$gl_caption_log['c_user_id']= "Utilisateur";
$gl_caption_log['c_date']= "Date";
$gl_caption_log['c_login']= "Heure de entrée";
$gl_caption_log['c_logout']= "Heure de sortie";


$gl_messages['Error_no_groups'] = "Erreur! Avant de créer les utilisateurs, les groupes doivent être créés.";
$gl_messages['user_exist_del']='Erreur!, Le groupe: <b>"%s"</b> contient des utilisateurs actifs ne peuvent pas supprimer.';
$gl_messages['user_admin_del']='Erreur!, Le groupe: <b>"%s"</b> ne peut pas être supprimé.';
$gl_messages['add_exists']='Il existe déjà un dossier avec ce nom d\'utilisateur.';

// NO TRADUIT

$gl_caption_public['c_group_id']="Nom du groupe";
$gl_caption_public['c_name']= "Prénom";
$gl_caption_public['c_surname']= "Nom";
$gl_caption_public['c_login']= "Nom d'utilisateur";
$gl_caption_public['c_passw']= "Mot de passe";
$gl_caption_public['c_email']= "E-mail";
// FI NO TRADUIT

$gl_caption_user['c_phone1'] = 'Principal';
$gl_caption_user['c_phone2'] = 'Travail';
$gl_caption_user['c_phone4'] = 'Privée';
$gl_caption_user['c_phone3'] = 'Fax';

$gl_caption_user['c_language_id'] = 'Idiomas';
$gl_caption_user['c_description'] = 'Descripción';
$gl_caption_user['c_form_level2_title_description'] = 'Descripciones';
$gl_caption_user['c_status'] = 'Estado';
$gl_caption_user['c_status_private'] = 'Privado';
$gl_caption_user['c_status_public'] = 'Público';

$gl_caption_user['c_edit_map'] = 'Carte';
$gl_caption_user['c_form_map_title'] = 'Emplacement de l\'agent';
$gl_caption_user['c_save_map'] = 'Enregistrer emplacement';
$gl_caption_user['c_latitude'] = 'Latitude';
$gl_caption_user['c_longitude'] = 'Longitude';
$gl_caption_user ['c_center_longitude'] = 'Map center latitude';
$gl_caption_user ['c_center_latitude'] = 'Map center longitude';
$gl_caption_user['c_zoom'] = 'Zoom';

$gl_caption_user ['c_center_coords'] = 'Centre';
$gl_caption_user['c_click_map'] = 'Cliquez sur un point sur ​​la carte pour définir l\'emplacement';
$gl_messages['point_saved'] = 'Nouvel emplacement mémorisé correctement';
?>