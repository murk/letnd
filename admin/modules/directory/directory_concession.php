<?
/**
 * DirectoryConcession
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2013
 * @version $Id$
 * @access public
 * 
 */
class DirectoryConcession extends Module{

	
	function __construct(){
		parent::__construct();
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	
	function get_records()
	{
		
		if ($this->process=="user"){
			
			$module = Module::load('user', 'user', $this, false, 'directory/directory_concession_user_config.php');
			$order = 'surname ASC';
		}
		elseif ($this->process=="group"){
			
			$module = Module::load('user', 'group', $this, false, 'directory/directory_concession_group_config.php');
			$order = 'ordre ASC, name ASC';
		}
		
		
		$listing = new ListRecords($module);
		$listing->caption = array_merge($listing->caption,$this->caption);
		$listing->set_options(0, 0, 0, 0, 0, 0, 0, 0, 1, 1);
		
		$listing->order_by = $order;
		$listing->has_bin = false;
		$listing->condition = DirectoryCommon::get_group_condition();	
		
		$listing->set_records();		
		
		
		// frame
		
		$category_id = R::id('category_id');
		$listing->set_var('category_id', $category_id);
		
		$page = &$GLOBALS['gl_page'];
		$page->template = 'clean.tpl';
		$page->title = '';	
		$page->javascript .= '
			parent.Assign.insert_addresses();';

		$GLOBALS['gl_message'] = $GLOBALS['gl_messages']['select_item'];
		
		// fi frame
		
		foreach ($listing->loop as $key=>&$rs) {
			
			if ($this->process=="user"){
				$name= addslashes($rs['name'] . ' ' . $rs['surname']); // cambio ' per \'
				$name= htmlspecialchars($name); // cambio nomès cometes dobles per  &quot;
				$id = $rs['user_id'];
			}
			elseif ($this->process=="group"){
				$name= addslashes($rs['name']); // cambio ' per \'
				$name= htmlspecialchars($name); // cambio nomès cometes dobles per  &quot;
				$id = $rs['group_id'];				
			}	
				$rs['tr_jscript'] = 'onClick="parent.insert_address(' . $id . ',\'' . $name . '\')"';		
				
		}
		
		
		//$listing->order_by = 'document ASC';	
		return $listing->process();
	}
	
    // Per poder escollir variacions desde una finestra amb frame, desde variacions de productes
    function select_frames()
    {
        $page = &$GLOBALS['gl_page'];

        $page->menu_tool = false;
        $page->menu_all_tools = false;
        $page->template = '';

        $this->set_file('directory/concession_select_frames.tpl');
        $this->set_vars($this->caption);
		
		$category_id = R::id('category_id');
		$document_id = R::id('document_id');
		$related_type = R::text_id('related_type','category_id');
		$this->set_var('process', $this->process);
		$this->set_var('related_table_id', $related_type=='category_id'?$category_id:$document_id);
		$this->set_var('related_type', $related_type);
		
		
		
		
		if ($this->process=="user"){
			
			$this->set_var('assign_selected', json_encode(DirectoryCommon::get_concession_names($category_id, true, false, $document_id)));
			
		}
		elseif ($this->process=="group"){

			$this->set_var('assign_selected', json_encode(DirectoryCommon::get_concession_names($category_id, false, true, $document_id)));
		}	
		
		
        $GLOBALS['gl_content'] = $this->process();
    }
	
	
	/*
	 * 
	 * 
	 * FUNCIONA NOMES SI HO ADMINISTRA UN SOL GRUP
	 * A l'hora de canviar permisos nomes hauria de borrar els d'aquells grups que es te permisos
	 * 
	 * 
	 * 
	 */
	function save_concession() {
		$ids = R::escape('ids');
		$related_type = R::text_id('related_type');
		$related_table_id = R::id('related_table_id');
		$html  ='';
		
		// borro relacionats
		Db::execute("DELETE 
						FROM directory__concession 
						WHERE ". $related_type . "=" . $related_table_id);
		
		$id_col = $this->process=='user'?'user_id':'group_id';
		
		// guardo permisos
		if ($ids){
			$ids = explode(',',$ids);
			//Debug::p($ids, 'ids');		
			foreach ($ids as $id){
				Db::execute("INSERT INTO directory__concession (". $related_type . ", ".$id_col.") VALUES (". $related_table_id . ", ". $id . ");");
			}
		
		$html = DirectoryCommon::get_concession_names(
				$related_type=='category_id'?$related_table_id:false,
				false,false,
				$related_type=='document_id'?$related_table_id:false
				);
		}
		if (!$html && $related_type=='category_id') $html = $this->caption['c_concession_everybody'];
		echo $html;
		
		$GLOBALS['gl_message'] = $this->messages['saved'];
		
		$GLOBALS['gl_page']->is_frame=true;
		$GLOBALS['gl_page']->is_ajax=true;
	}
}
?>