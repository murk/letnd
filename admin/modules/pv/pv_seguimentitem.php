<?
/**
 * PvSeguimentitem
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2011
 * @version $Id$
 * @access public
 */
class PvSeguimentitem extends Module{
	var $is_actiu, $is_pdf;
	
	var $group_id, $user_is_admin, $user_is_client, $client_id, $condition, $is_search = false, $q, $seguiment_id, $can_edit_seguiment;
	
	function __construct(){
		parent::__construct();
		
		$this->group_id = $_SESSION['group_id'];
		
		$this->user_is_admin = $this->group_id < 3; // grups 2 i 1
		// els usuaris es el grup 3
		$this->user_is_client = $this->group_id == 4; // grups 4
		
		if ($this->user_is_client){
			$this->client_id = Db::get_first('SELECT client_id FROM user__user WHERE user_id = ' . $_SESSION['user_id']);			
		}		
	}
	function list_records()
	{
		$this->set_var('is_pdf',false);
		$this->set_var('pdf',false);
		$content =  $this->get_records();
		$GLOBALS['gl_content'] = $this->get_search_form() . $content;
	}
	function get_records()
	{
		// per defecte sempre actius
		if (!isset($_GET['is_actiu'])) $_GET['is_actiu'] = 1;
		$this->is_actiu = R::number('is_actiu');
		$this->set_var('show_actiu',true);
		
		if ($this->is_actiu==0){
			$this->set_field('classificacio','list_admin','text');
			$this->set_field('evaluacio','list_admin','text');
			$this->set_field('obertura','list_admin','text');
			$this->set_var('show_actiu',false);
			$this->set_field('tancament','list_admin','text');
		}
		
		$seguiment_id = R::text_id('seguiment_id');
		
		if ($this->user_is_client){
			$seguiment_id = Db::get_first("SELECT seguiment_id
			FROM pv__seguiment, pv__client  
			WHERE pv__seguiment.client_id =  pv__client.client_id
			AND pv__seguiment.bin = 0
			AND pv__client.client_id = " . $this->client_id);
		}
		
		$this->seguiment_id = $seguiment_id;
		
		$seguiment = Db::get_row("SELECT seguiment_id, codi_seguiment, pv__seguiment.client_id as client_id, client, can_edit_seguiment, pv__client.client_id AS pv__client_client_id
			FROM pv__seguiment, pv__client  
			WHERE pv__seguiment.client_id =  pv__client.client_id
			AND seguiment_id = " . $seguiment_id);
		
		$this->set_vars($seguiment);

		$this->can_edit_seguiment = $seguiment['can_edit_seguiment'];

		$GLOBALS ['gl_page'] -> subtitle =  $seguiment['client'];

		
		$logo = UploadFiles::get_record_images($seguiment['pv__client_client_id'], false, 'pv', 'client');
		
		$this->set_var('image_name_details',$logo['image_name_details']);
		
		$is_trimestral = R::number('trimestral');
		$is_trimestre = R::escape('trimestre');
		if ($is_trimestre=='null') $is_trimestre=false;
		$this->set_var('is_trimestral',$is_trimestral);
		$this->set_var('is_trimestre',$is_trimestre);
		
	    $listing = new ListRecords($this);		
		$listing->condition = "seguiment_id=" . $seguiment_id;
		
		// search
		if ($this->condition) {
			$listing->condition .= $this->condition;
		}
		
		$listing->has_bin = false;	
		
		
		if ($this->user_is_client){	
			$listing->condition .= " AND is_actiu=1";


			if ($this->can_edit_seguiment){
				$listing->add_swap_edit();
			}
			else {
				$listing->set_editable(false);
			}
			$menu_id = 2311;
			$listing->set_var('fitxa_link', '/admin/?action=list_records&tool_section=seguimentitem&seguiment_id=' . $seguiment_id . '&menu_id=2311&is_actiu=1&norma_id=');
		}
		else{
			$listing->add_swap_edit();	
			$menu_id = 2303;
			$listing->set_var('fitxa_link', '/admin/?action=list_records&tool_section=seguimentitem&seguiment_id=' . $seguiment_id . '&menu_id=2303&is_actiu=1&norma_id=');
		}
		
			
		$listing->call('records_walk',false,true);		
		$listing->group_fields = array('categoria');
		
		//$listing->order_by =  'categoria ASC, evolucio ASC, num_requisit';
		$listing->order_by =  'categoria ASC, codi_lloc ASC, numero ASC, evolucio ASC, num_requisit';
		
		$listing->join = 'INNER JOIN pv__requisit USING (requisit_id) INNER JOIN pv__norma ON pv__norma.norma_id = pv__requisit.top_id INNER JOIN pv__categoria USING (categoria_id)';
		
		$listing->extra_fields = "(SELECT pv__norma.evolucio FROM pv__norma WHERE pv__norma.norma_id = pv__requisit.norma_id) AS evolucio,
								 (SELECT pv__norma.titol_norma FROM pv__norma WHERE pv__norma.norma_id = pv__requisit.norma_id) AS titol_norma";
		
		if ($this->is_pdf){
			$listing->paginate = false;
			$listing->set_editable(false);
		}
		else{
			$listing->add_dots_filter();
		}	
		
		if (($this->is_actiu==0 || $this->is_actiu==1) && !$listing->is_editable){
			$listing->set_field('is_actiu','list_admin','0');
		}
		
		
		$listing->set_options (1, 0, 1, 0, 0, 1, 0, 0, 1, 1); // $options_bar = 1, $options_checkboxes = 1, $save_button = 1, $select_button = 1, $delete_button = 1, $print_button = 1, $edit_buttons = 1, $image_buttons = 1, $split_count = 1, , $order_bar = 1
		
		if (!$listing->is_editable && !$this->is_pdf){
			$this->set_field('is_actiu','type','select'); // aixÃ­ em mostra "si o no"
		}
				
		// FITXA - Requisits reglamentaris 
		$norma_id = R::escape('norma_id');
		if ($norma_id) {
			$listing->set_var('norma_id', $norma_id);
			$listing->condition .= " AND  top_id=" . $norma_id;
			$listing->template = "pv/seguimentitem_norma.tpl";
			
			$this->set_field('descripcio_requisit','list_admin','text');
			$this->set_field('descripcio_norma','list_admin','out');
			
			$back_to_seguimentitem = get_all_get_params(array('norma_id', 'page'),'?','&');
			$this->set_var('back_to_seguimentitem',$back_to_seguimentitem);
			
			
			$listing->add_filter('trimestral');
			$listing->add_filter('avis');
			
			$listing->extra_fields .= ",
								 (SELECT pv__norma.descripcio_norma FROM pv__norma WHERE pv__norma.norma_id = pv__requisit.norma_id) AS descripcio_norma";
			
			$this->set_comentari_client($seguiment_id,$norma_id);
		}
		// FI FITXA
		// LLISTAT - Seguiment reglamentari
		else{
			if(!$this->is_search){
				$listing->add_filter ('categoria_id');
				//$listing->add_filter('codi_lloc');
				$listing->add_filter('top_id');			
				$listing->add_filter('evaluacio');			
				if (!$this->user_is_client)$listing->add_filter('is_actiu');			
				if ($listing->is_editable) $listing->add_filter('trimestral');
				$listing->add_filter('avis');
			}
		}
		// FI LLISTAT 
		
		// TRIMESTRE
		
		$this->set_filter_trimestre($listing);
		
		// FI TRIMESTRE
		
		$listing->set_records();
		
		$loop = array();
		$last_norma_id = 0;
		$last_top_id = 0;		
		$norma_last = false;
		
		foreach ($listing->loop as $key=>&$rs) {
			
			$rs['no_conforme_codi_lloc'] = $rs['codi_lloc']; // es mostra en tots els registres, codi_lloc nomes en el primer de la norma
			
			$rs['entered'] = str_replace('/', '.', $rs['entered']);
			$rs['entered_top'] = str_replace('/', '.', format_date_list($rs['entered_top']));
				
			if (!$listing->is_editable){
				$rs['tancament'] = str_replace('/', '.', $rs['tancament']);
				$rs['obertura'] = str_replace('/', '.', $rs['obertura']);
			}
			
			// resta de normes iguals
			if ($rs['top_id']==$last_top_id){
				$rs['codi_lloc'] = '';
				$top_rowspan++;
			}
			// la primera norma igual que trobem
			else {				
				$last_top_id = $rs['top_id'];
				$rs['top_rowspan'] = 1;
				$top_rowspan = &$rs['top_rowspan'];
			}
			
			
			$rs['norma_last'] = false;
			
			// resta de evolucions iguals
			if ($rs['norma_id']==$last_norma_id){
				$rs['titol_norma'] = '';
				$rs['descripcio_norma'] = '';
				$norma_last = &$rs['norma_last']; // mantinc referencia sempre a l'ultima
				$norma_rowspan++;
				
				// sese rowspan
				//$requisits[] = $rs;
				//unset($listing->loop[$key]);
			}
			// la primera evolucio igual que trobem
			else {				
				$last_norma_id = $rs['norma_id'];
				$rs['norma_rowspan'] = 1;
				$norma_rowspan = &$rs['norma_rowspan'];
				$norma_last = true;
				$rs['requisits'] = array();				
				
				$rs['files'] = UploadFiles::get_record_files($rs['norma_id'], 'file', 'pv', 'norma');
				
				// sense rowspan
				// creo un array amb tots els resultats que conté aquesta norma
				//$requisits = &$rs['requisits'];
				//$requisits[] = $rs;
			}
			unset ($rs['loop']);
			
		}
		$norma_last = true;
		
		//Debug::p($listing->loop, 'Loop');
		if ($listing->is_editable)
			$listing->set_var('colspan',10);
		else
			$listing->set_var('colspan',9);
		
		if (!$listing->has_results) $GLOBALS['gl_message'] = $listing->no_records_message;		
		
		// retorno sempre perque hi ha filtres
		return $listing->process();
	}
	function records_walk($module){
		$rs = &$module->rs;
		$ret = array();
		
		// si no ha estat mai actiu agafo la classificació del requisit
		if (!$rs['has_been_actiu']){
			$requisit_id = $rs['requisit_id'];
			$classificacio = Db::get_first("SELECT classificacio FROM pv__requisit WHERE requisit_id = " . $requisit_id);
			$ret['classificacio'] = $classificacio;
		}
		
		//$codi_categoria = Db::get_first('SELECT codi_categoria FROM pv__categoria WHERE categoria_id = ' .  $rs['categoria_id']);
		//
		$ret['entered'] = Db::get_first('SELECT entered FROM pv__norma WHERE norma_id = ' .  $rs['norma_id']);
		$ret['entered_top'] = Db::get_first('SELECT entered FROM pv__norma WHERE norma_id = ' .  $rs['top_id']);
		
		$ret['codi_lloc'] = $rs['codi_lloc'] . ' ' . $rs['codi_categoria'] . ' Nº' . $rs['numero'];
		$ret['categoria'] =  $rs['categoria'] . ' - ' . $rs['codi_categoria'];

		$ret['evaluacio_class'] =  $rs['evaluacio']=='noconforme' && $rs['tancat']=='0'?'pv-red':'';
		$ret['evaluacio_text_class'] =  $rs['evaluacio']=='noconforme'?'':'hidden';

		$avis_data = $rs['avis_data'];
		$avis_en_tramit = $rs['avis_en_tramit'];

		$ret['avis_class'] = '';
		if (intval($avis_data)) {
			$ret['avis_class'] = is_date_bigger( date( "Y-m-d" ), $avis_data ) ? 'pv-red-2' : '';
		}
		if ( $avis_en_tramit )
			$ret['avis_class'] = 'pv-green';

		$ret['avis_text_class'] =  $rs['avis']=='1'?'':'hidden';
				
		return $ret;
	}

	function set_filter_trimestre($listing){
		
		$trimestre = R::escape('trimestre');		
		$this->set_var('trimestre_seleccionat','');
		
		$link = get_all_get_params(array('trimestre', 'page'),'?','&',true);
		
		$select = '<select onchange="javascript:window.location=\''.$link.'trimestre=\'+this.options[this.selectedIndex].value" class="filter" name="trimestre"><option value="null">'.$this->caption['c_trimestre_all'].'</option>';
		
		$year = date("Y");
		$month = date("n");
		
		$current_trimestre = ceil(($month)/3);
		
		
		// 20 ultims anys
		for ($i=$year;$i>$year-20;$i--){
			// trimestres
			
			if ($i==$year){
				$t_start = $current_trimestre;
			}
			else{	
				$t_start = 4;
			}
			$select .= '<optgroup label="'.$i.'">';
			
			for ($t=$t_start;$t>0;$t--){
				$value = $i.'-'.$t;
				
				if ($value==$trimestre){				
					$selected = ' selected';
					$this->set_var('trimestre_seleccionat',$this->caption['c_trimestre_'.$t] .' - '.$i);
				}
				else{
					$selected = '';	
				}
				
				$select .= '<option'.$selected.' value="'.$value.'">'.$this->caption['c_trimestre_'.$t] .' - '.$i .'</option>';
			}
			$select .= '</optgroup>';
			
		}
		$select .= '</select>';
		$this->set_var('trimestre_filter', $select);
		
		if ($trimestre!='null' && $trimestre!==false){
			
			$trimestre = explode('-',$trimestre);
			$year = $trimestre[0];
			$second_month = $trimestre[1]*3+1;
			$first_month = $second_month-3;
			$date1 =  $year . '-' . $first_month . '-1';
			$date2 =  $year . '-' . $second_month . '-1';
			
			$listing->condition .= " AND (obertura >= '" . $date1 . "' AND obertura <'" .$date2 . "')";
		}
		else{
			// quan no es editable no hi ha filtre trimestral, per tant al index genberal nomes mostro els que no son trimestrals
			if (!$listing->is_editable) $listing->condition .= " AND trimestral=0";
		}
		
	}
	function set_comentari_client($seguiment_id, $norma_id) {
		$comentari_client = Db::get_first("
			SELECT comentari_client 
			FROM pv__seguimentfitxa 
			WHERE seguiment_id = '" . $seguiment_id . "'
			AND norma_id = '".$norma_id . "'");
		
		$this->set_var('comentari_client',$comentari_client);
		$this->set_var('comentari_client_class',$comentari_client?'':' hidden');
		$this->set_var('edit_comentari_client','/admin/?action=show_form_edit&menu_id=2303&tool=pv&tool_section=seguimentfitxa&seguiment_id='.$seguiment_id . '&norma_id='.$norma_id . '&template=window');
	}
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}

	function save_rows()
	{
	    $save_rows = new SaveRows($this);
	    $save_rows->save();
		
		if ($this->action == 'save_rows_save_records'){
		
			// si no hi ha dat i poso com a actiu, llavors poso la data d'avui
			foreach ($save_rows->id as $id){
				if (isset($_POST['is_actiu'][$id])){
					
					$has_been_actiu = Db::get_first('
							SELECT has_been_actiu 
							FROM pv__seguimentitem 
							WHERE seguimentitem_id = ' . $id);
					
					// nomÃ¨s es fa el primer cop que s'activa
					if (!$has_been_actiu){
						$GLOBALS['gl_reload'] = true;
							
						$classificacio = $_POST['classificacio'][$id];
						
						$evaluacio = $classificacio=='A'?'noconforme':'informacio';
							
						Db::execute("
							UPDATE pv__seguimentitem 
							SET obertura = now(),
							evaluacio = '".$evaluacio."',
							has_been_actiu = 1
							WHERE seguimentitem_id = " . $id);
							
						
					}
				}
			}
		}
		
	}


	/**
	 *  Per editar un camp concret des del llistat de seguimentitem
	 */
	function get_form()
	{
		$field_to_edit = R::get( 'field_to_edit' );

		if ( $field_to_edit ) {
			foreach ( $this->fields as $key => &$field ) {
				if ( $key == $field_to_edit ) {
					$field['form_admin'] = 'input';
				}
				elseif ( $field['type'] != 'hidden' ) {
					$this->unset_field ( $key );
				}
			}
		}
		//Debug::p( $this->fields );

	    $show = new ShowForm($this);
		$show->has_bin = false;
	    return $show->show_form();
	}
	
	function write_record()
	{
	    $writerec = new SaveRows($this);
	    $writerec->save();

		if ( $this->process == 'edit_evidencia_from_list' ) {
			$evidencias = UploadFiles::get_record_files( $writerec->id, 'evidencia', 'pv', 'seguimentitem' );

			$html = '';
			foreach ( $evidencias as $f ) {
				extract( $f );
				$html .= '<p><a href="' . $evidencia_src . '" title="">' . $evidencia_name_original . '</a></p>';
			}
			print_javascript( '
				top.$("#evidencia_' . $writerec->id . '").html("' . addslashes($html). '")
				top.hidePopWin();
				'
			);
			Debug::p_all();

		}

	}

	function manage_images()
	{
	    $image_manager = new ImageManager($this);
	    $image_manager->execute();
	}

	function get_pdf()
	{		
		$this->action = 'get_records';
		$this->set_var('is_pdf',true);
		$this->set_var('pdf','pdf');
		$this->is_pdf = true;
		$GLOBALS['gl_page']->template = '';
		
		$html = '<!DOCTYPE HTML>
				<html>
				<head>
					<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
				</head>
				<body>
					'.($this->get_records()).'
				</body>
				</html>';
		echo $html;
		
		Debug::p_all();
		
		die();
		
		
		
		
		
		require_once(DOCUMENT_ROOT . "/common/includes/dompdf/dompdf_config.inc.php");
		$dompdf = new DOMPDF();
		$dompdf->set_base_path(DOCUMENT_ROOT . '/admin/');		
		$dompdf->load_html($html);		
		$dompdf->set_paper('a3','landscape');
		$dompdf->render();

		$dompdf->stream("dompdf_out.pdf", array("Attachment" => false));
		exit(0);
		
		Debug::p_all();
		
		
		
		
		die();
	}
	
	
	
	
	//
	// FUNCIONS BUSCADOR
	//
	function search(){

		$q = $this->q = R::escape('q');
		
		$search_condition = '';
		
		$this->is_search = true;
		
		$seguiment_id = R::text_id('seguiment_id');

		if ($q)
		{	
			$search_condition = " AND (
				requisit like '%" . $q . "%'
				OR descripcio_requisit like '%" . $q . "%'
				OR titol_norma like '%" . $q . "%'
				OR comentari like '%" . $q . "%'
				OR CONCAT(LOWER(codi_lloc),' ',LOWER(codi_categoria),' ',numero) like '%" . strtolower($q) . "%'
				OR entered like '%" . $q . "%'
				" ;
			if(strpos($q,'/')){
				
				$date_arr = explode('/',$q);
				$date = '';
				if (count($date_arr)<=3){
					$date_arr = array_reverse($date_arr);
					$date = implode('-', $date_arr);
					$search_condition .= "
					OR entered like '%" . $date . "%'
					" ;
				}
			}
			
			$search_condition .= ") " ;			
			

			$GLOBALS['gl_page']->title = TITLE_SEARCH . '<strong>&nbsp;&nbsp;"' . $q . '"</strong>';
		}
		// per fer un reset de una cerca, busco cadena buida
		else{
			if ($seguiment_id ) 
				$redirect = '/admin/?menu_id=2303&action=list_records&tool_section=seguimentitem&seguiment_id=' .$seguiment_id;
			else				
				$redirect = '/admin/?menu_id=2311';
			
			Main::redirect($redirect);
		
		}
		$this->condition = $search_condition;
		Debug::add('Search condition', $this->condition);
		$this->do_action('list_records');
	}

	function get_search_form(){
		$tpl = new phemplate(PATH_TEMPLATES);
		$tpl->set_vars($this->caption);
		$tpl->set_file('pv/seguimentitem_search.tpl');
		$tpl->set_var('seguiment_id',$this->seguiment_id);
		$tpl->set_var('menu_id',$GLOBALS['gl_menu_id']);
		$tpl->set_var('q',  htmlspecialchars(R::get('q')));
		$ret = $tpl->process();	
		
		if ($this->q) {
			$GLOBALS['gl_page']->javascript .= "
				$('#pv').highlight('".addslashes(R::get('q'))."');			
			";
		}
		
		return $ret;
	}
	
}
?>