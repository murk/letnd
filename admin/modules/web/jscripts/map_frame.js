gl_process='';


document.write( '<script type="text/javascript" src="//maps.google.com/maps/api/js?key=AIzaSyAaIGZEloZFoSY8TuTDyL27MDbuDvW5AoM&language=' +
      gl_language + 
      '" type="text/javascript"><\/script>' );


function loadMap(){
	if (top.document.getElementById('google_latitude_999')) gl_process='public';
	
		
	if (gl_process=='public'){
		latitude = parseFloat(top.document.getElementById('google_latitude_999').value);
		longitude = parseFloat(top.document.getElementById('google_longitude_999').value);
	}
	else{
		latitude = false;
		longitude = false;
	}
	center_latitude = parseFloat(top.document.getElementById('google_center_latitude_999').value);
	center_longitude = parseFloat(top.document.getElementById('google_center_longitude_999').value);
	gl_zoom = parseFloat(top.document.getElementById('google_zoom_999').value);
	
	// defecte
	if  (gl_process=='public'){
		if (!latitude) latitude=41.97;
		if (!longitude) longitude=2.82;	
	}
	if (!center_latitude) center_latitude=41.79
	if (!center_longitude) center_longitude=1.84;
	if (!gl_zoom) gl_zoom=8;
	
	guarda_coordenades_top(latitude,longitude)
	guarda_centrat_top(center_latitude,center_longitude);
	guarda_zoom_top(gl_zoom);
	
	var centerLatlng = new google.maps.LatLng(center_latitude,center_longitude);
	var myLatlng = new google.maps.LatLng(latitude,longitude);
	var myOptions = {
	  scrollwheel: false,
	  zoom: gl_zoom,
	  center: centerLatlng,
	  mapTypeId: google.maps.MapTypeId.ROADMAP,
	  mapTypeControlOptions: {style: google.maps.MapTypeControlStyle.DROPDOWN_MENU}
	}
	var map = new google.maps.Map(document.getElementById("map"), myOptions);
	
	var infowindow = new google.maps.InfoWindow({ 
		content: ''
	});	
	
	if (gl_process=='public'){
		var marker = new google.maps.Marker({
			position: myLatlng,
			map: map,
			streetViewControl: true
		});  
		google.maps.event.addListener(map, 'click', function(event) {guarda_coordenades(event,map,marker)});	
	}
	

	google.maps.event.addListener(map, "zoom_changed", guarda_zoom);
	google.maps.event.addListener(map, "center_changed", guarda_centrat);
	
	
	return;
}
function guarda_coordenades(event,map,marker){
	
	point = event.latLng;
	
	marker.setOptions({
			position: point
		});
	
		latitude = point.lat();
		longitude = point.lng();
		guarda_coordenades_top(latitude,longitude);
		
	return;
}
function guarda_coordenades_top(latitude,longitude){
	if (latitude===false) return;
	
	top.document.getElementById('google_latitude_999').value = latitude;
	top.document.getElementById('google_longitude_999').value = longitude;	
	top.document.getElementById("latitut").innerHTML = latitude;
	top.document.getElementById("longitut").innerHTML = longitude;
}
function guarda_zoom() {
	gl_zoom = this.getZoom();
	guarda_zoom_top(gl_zoom);
}
function guarda_zoom_top(zoom) {
	top.document.getElementById('google_zoom_999').value = gl_zoom;
}
function guarda_centrat(){
	latitude =  this.getCenter().lat();
	longitude = this.getCenter().lng();
	guarda_centrat_top(latitude,longitude);
}
function guarda_centrat_top(latitude,longitude){
	top.document.getElementById('google_center_latitude_999').value = top.document.getElementById("center_latitut").innerHTML = latitude;
	top.document.getElementById('google_center_longitude_999').value = top.document.getElementById("center_longitut").innerHTML = longitude;
}


$(document).ready(
	function ()
		{
			loadMap();
		}
)