<?php
	// Aquest arxiu es genera automaticament
	// Qualsevol canvi que es faci es podrà sobrescriure involuntariament desde l'eina de gestió
	// Achtung!
	$gl_file_protected = false;
	$gl_db_classes_fields_included = array (
  'tax_id' => '1',
  'tax_name' => '1',
  'tax_value' => '1',
);
	$gl_db_classes_language_fields = array (
  0 => '',
);
	$gl_db_classes_fields = array (
  'tax_id' => 
  array (
    'type' => 'hidden',
    'enabled' => '1',
    'form_admin' => '0',
    'form_public' => '0',
    'list_admin' => '0',
    'list_public' => '0',
    'order' => '0',
    'default_value' => '',
    'override_save_value' => '',
    'class' => '',
    'javascript' => '',
    'text_size' => '',
    'text_maxlength' => '11',
    'textarea_cols' => '',
    'textarea_rows' => '',
    'select_caption' => '',
    'select_size' => '',
    'select_table' => '',
    'select_fields' => '',
    'select_condition' => '',
  ),
  'tax_name' => 
  array (
    'type' => 'text',
    'enabled' => '1',
    'form_admin' => 'input',
    'form_public' => 'text',
    'list_admin' => 'text',
    'list_public' => 'text',
    'required' => '1',
    'order' => '1',
    'default_value' => '',
    'override_save_value' => '',
    'class' => '',
    'javascript' => '',
    'text_size' => '',
    'text_maxlength' => '50',
    'textarea_cols' => '',
    'textarea_rows' => '',
    'select_caption' => '',
    'select_size' => '',
    'select_table' => '',
    'select_fields' => '',
    'select_condition' => '',
  ),
  'tax_value' => 
  array (
    'type' => 'int',
    'enabled' => '1',
    'form_admin' => 'input',
    'form_public' => '0',
    'list_admin' => 'text',
    'list_public' => '0',
    'required' => '1',
    'order' => '2',
    'default_value' => '',
    'override_save_value' => '',
    'class' => '',
    'javascript' => '',
    'text_size' => '',
    'text_maxlength' => '2',
    'textarea_cols' => '',
    'textarea_rows' => '',
    'select_caption' => '',
    'select_size' => '',
    'select_table' => '',
    'select_fields' => '',
    'select_condition' => '',
  ),
);
	$gl_db_classes_list = array (
  'cols' => '1',
);
	$gl_db_classes_form = array (
  'cols' => '1',
  'images_title' => '',
);
	?>