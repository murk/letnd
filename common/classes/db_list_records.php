<?php
/*
afegir propietat de si es paperera o es llistat normal , així nomès farà falta una plantilla tant per llistat com per llistat paperera
afegir propietat de si hi ha imatge o no n'hi ha ja que la imatge va en un altre columna a part de la resta de camps
hi haurà plantilles generals per tots els apartats de formularis i llistats
*/
class ListRecords extends BaseListShow
{
    var $no_records_message, $key_values_list,
    $always_parse_template, $cols, $split_results, $action_add, $marked_ids, $selected_ids, $group_fields = array(), $group_fields_function = false,  $field_options = array(),  $cols_to_hide = array(), $results = array(),$loop, $conta=0, $loop_count=0, $has_swap_edit = false, $has_add_dots = false, $add_dots = false, $onmousedown=true, $form_link=false, $content_top = false, $is_new_records = false, $is_sublist = false;
    /**
     *
     * @access public
     * @return void
     */
    function __construct(&$module = false)
	{
        global $gl_write;
        BaseListShow::__construct($module);
        $this->list_show_type = 'list';
        $this->show_langs = false; // per defecte nomès mostrem un idioma en els llistat
        $this->always_parse_template = false;
        $this->cols = 1;
		
		if ($module && is_array($module->caption_list))$this->caption = array_merge($this->caption, $module->caption_list); // es $module->caption_list per que no està vinculat a link_module, ja que nomès es fa servir a list

        if (strstr($this->action, "list_records_bin") || strstr($this->action, "get_records_bin"))
        {
            $this->is_bin = true;
            $this->no_records_message = $this->messages['no_records_bin'];
            $this->set_options (1, 1, 0, 1, 1, 1, 1, 1, 1);
        }
        else
        {
            $this->is_bin = false;
            $this->no_records_message = $this->messages['no_records'];
			
			
			$this->set_options (); // per defecte tenim totes les opcions al menu d'opcions				
        }
		
        // calculo la teminacio afegida a list_records_bin o a list_records
        $action_add = str_replace("list_records_bin", '', $this->action);
		$action_add = $action_add == $this->action?str_replace("list_records", '', $this->action):$action_add;
		$action_add = $action_add == $this->action?str_replace("get_records_bin", '', $this->action):$action_add;
		$action_add = $action_add == $this->action?str_replace("get_records", '', $this->action):$action_add;
        $this->action_add = $action_add;

        $this->marked_ids = isset($_GET['marked_ids'])?$_GET['marked_ids']:'';
        $this->marked_ids = explode(',', $this->marked_ids);
    }
    // ----------------------
    // Funcio principal
    // ----------------------
	//
	// 1  - list_records()  -> ho fa tot, fa el query, obté results, processa els resultats i processa el template retornant el content
	// 2a - list_records(false) -> fa el query i obté la variable $this->results
	// 2b - parse_template() -> processa els resultats i processa el template retornant el content
	// 3  - set_records() -> fa tot menys processar el template i retornar el content
	// 3b - process() -> retorna el content despres de fer un set_records()
    function list_records($parse_template = true)
	{
        global $gl_page;
        // afegim els botons automàtics
        $this->add_auto_buttons();
        $this->list_show();
        if ($gl_page->template == 'print.tpl')
        {
            $this->set_options(0, 0, 0, 0, 0, 0, 0, 0, 0);
        }
        $this->set_vars_common();
        // consulta
        $query = $this->get_query();
		
		// Canvia el valor de $gl_db_max_results pel valor que hi ha en la mateixa eina ( així puc tindre 3 resultats per una eina i 10 per una altra)
		// De fet es agafar-ho sempre del this->config, per tant el gl_db_max_results es podria anular ja que no te molt sentit
		// A admin agafo nomès el max_results de all_config ( em sembla que no fa falta posar un diferent per cada eina )
		if ($this->site_part=='public') $GLOBALS['gl_db_max_results'] = $this->config['max_results'];
        $this->split_results = new SplitPageResults($query, $this->change_sql_id_field, $this->name, $this->limit || !$this->paginate);

        $this->results = Db::get_rows($query['q']);
        $this->has_results = !(empty($this->results));
		if ($parse_template) {
			return $this->parse_template();
		}
    }
	function set_records(){
		$this->list_records(false);
		$this->parse_template(false);
	}
    function parse_template($process=true){
	    global $tpl, $gl_content, $gl_message;
	    $results = &$this->results;
	    if ($results)
        {            	 
			// guardo en sessió el llistat
			$this->last_listing['link'] = $_SERVER["REQUEST_URI"];
			
			if ($this->is_bin) // si es la paperera poso tots els inputs a text
                {
                    foreach($this->fields as $key => $value)
                {
                    if ($this->fields[$key][$this->list_show_type . '_' . $this->site_part] == 'input') {
                    	$this->fields[$key][$this->list_show_type . '_' . $this->site_part] = 'text';
                    }
                }
            }
            $loop = array();
            // BUCLE RESULTATS
            $this->loop_count = count($results);
			$rs = &$this->rs;
            foreach ($results as $rs)
            {
                $this->process_result($rs, $loop);
            }
            $this->group_fields($loop);
            if (!$this->key_values_list) $this->set_vars_results($loop, true);
            if (!$process) {
				// acces directe als resultats del llistat dins el tpl
				$this->loop = &$this->tpl->loops['loop'];
            	return;
            }
			//Debug::add('Listing results processed',$this->tpl->loops);
			$content = $this->tpl->process();
	    	// nomès modifiquem el gl_content si es el modul principal iniciat a l'index
	    	if (!$this->is_v3)
	    		 $gl_content=$content;
			
			
			return $content;
        } // if ($results)
        else
        {
            if ($this->always_parse_template)
            {
				$this->last_listing['link'] = $_SERVER["REQUEST_URI"];
				
				$this->set_vars_results(array(), false);
	            $this->set_var ('module_message', '<span class="module-message">' . $this->no_records_message . '</span>');
                //crec que si vull parsejar igualment, no fa falta el missatge
            	//$gl_message = $this->no_records_message;
            	//
				if (!$process) {
					// acces directe als resultats del llistat dins el tpl
					$this->loop = &$this->tpl->loops['loop'];
					return;
				}
				$content = $this->tpl->process();
				// nomès modifiquem el gl_content si es el modul principal iniciat a l'index
				if (!$this->is_v3)
					$gl_content=$content;
				
				
				return $content;
            }
            else
            {
				$this->tpl->set_loop('loop', array()); // així no dona undefined
				if (!$process) {
				// acces directe als resultats del llistat dins el tpl
				$this->loop = &$this->tpl->loops['loop'];
				}
				if ($this->is_index)
                	$gl_message = $this->no_records_message;
				else{
					return '<span class="module-message">' . $this->no_records_message . '</span>';
				}

            }
        }
    }
    function process_result(&$rs, &$loop)
    {
        global $gl_menu_id;
		$num_rows = &$this->loop_count;
        $conta = &$this->conta;
        static $odd_even = 'Even';
        static $loop_loop;
		// reset statics quan conta es 0 ( els estatics es queden al inicialitzar clases )
		if ($conta == 0){
			$odd_even = 'Even';
			$loop_loop = array();
		}

        $rs['group_fields'] = array();
        $rs["tr_jscript"] = '';
        $rs["is_row_selectable"] = true;
        $rs["is_row_editable"] = true;
        $rs["tr_class"] = isset ($rs["tr_class"])?$rs["tr_class"]:''; // així es pot definir abans de fer el process_result
        $rs['id'] = $rs[$this->id_field]; // així puc accedir al id sense haver de saber si es property_id o category_id o el que sigui_id
        $odd_even = ($odd_even == 'Odd')?'Even':'Odd';
        $rs["odd_even"] = in_array($rs['id'], $this->marked_ids)?'Marked':$odd_even;

        // seleccionats
		$selected = '';
        if ($this->selected_ids) {
			$selected = in_array($rs['id'], $this->selected_ids)?' checked':'';
	        $rs["odd_even"] = $selected?'Selected':$rs["odd_even"];
        }
	    $rs["selected"] = $selected;
	    // fi seleccionats

		// Per marcar primer i ultim en un llistat, te preferencia primer si primer=ultim
        $rs["first_last"] = $conta==0?'first':($num_rows==$conta+1?'last':'');
        $rs["conta"] = $conta;
        $rs["loop_count"] = $num_rows;
        $rs['id_field'] = $this->id_field; // així puc accedir al nom del id sense haver de saber si es property_id o category_id o el que sigui_id

		// isset($this->uploads['image']) serveix per poder posar imatges al llistat sense que tingui imatges
        if ($this->has_images && isset($this->uploads['image'])) $rs += $this->uploads['image']->get_record_images_i($rs['id']);
        $rs += UploadFiles::gl_upload_get_record_files($this, $rs['id']);
        // o poso abans del bucle per cada camp, per així poder sobreescriure resultats en cas que sigui necessari
        // obtinc camps adicionals segons funcio establerta a this->call
        $this->get_call_rs($rs);

        // editables
	    if (!$rs["is_row_editable"]) {
		    $this->set_fields_editable( false );
		    $rs["is_row_selectable"] = false;
	    }

        // BUCLE PER CADA CAMP DEL RESUTAT
        foreach($this->fields as $key => $val)
        {
            if (($val['list_' . $this->site_part] == 'input') || ($val['list_' . $this->site_part] == 'text') || ($key == $this->id_field) || ($val['list_' . $this->site_part] == 'out'))
            {
				$this->process_field($val, $key, $rs);
            }
            // TODO-i Nomes per llistat inmobles, s'hauria de fer també per llistats automàtics i idiomes no
	        elseif (($val['list_' . $this->site_part] == 'hide')) {
		        $rs[ $key . '_value' ] = $rs[$key];
		        $rs[ $key ] = false;
	        }
        }

        /**
         * Variables automàtiques, sempre iguals a tots els llistats
         */
        // per fer seguiment del apartat on erem
        // Ex. Al fer una cerca, editem un registre dels resultats i al guardar tornem als mateixos resultats, haig de treure el page per que sino hem vol paginar les imatges
        // $rs['edit_link'] = "?action=show_form_edit".$this->action_add."&" . $this->id_field . "=" . $rs[$this->id_field] . "&" .
        // get_all_get_params(array('action', $this->id_field, 'page')) . "action_previous_list=" . $this->action;
        // $rs['image_link'] = "?action=list_records_images".$this->action_add."&" . $this->id_field . "=" . $rs[$this->id_field] . "&" .
        // get_all_get_params(array('action', $this->id_field, 'page', 'status')) . "action_previous_list=" . $this->action;
        $this->set_buttons($rs);

        // restauro editables després dels botons
	    if (!$rs["is_row_editable"]){
	    	$this->set_fields_editable( true );
	    }
		
		
		$rs['show_record_link'] = $this->get_show_record_link($rs);
		
        // li enchufo els hidden en el primer key que trobo, així ho fa automatic i no haig de posar-ho al template
        if (isset($rs['fields'][0]['val'][0]['val']))
        {
            $rs['fields'][0]['val'][0]['val'] .= $this->hidden;
        } elseif (isset($rs['fields']['val'][0]['lang'][0]['val']))
        {
            $rs['fields']['val'][0]['lang'][0]['val'] .= $this->hidden;
        }
        else
        {
            $rs['hidden'] = $this->hidden;
        }
        $this->hidden = '';
        // llistat tipus configuració, un formulari ja preparat per emplenar d'un llistat de clau -> valors
        if ($this->key_values_list)
        {
            // $this->tpl->set_var('l' . $rs[$this->id_field], $rs);
        }
        else
        {
            if ($this->cols > 1)
            {
                $rest = fmod($conta, $this->cols);
				
				$rs['first_last_col']=$rest==0?'first_col':($this->cols==$rest+1?'last_col':'');
				$rs['conta_col']=$rest;
				$rs['first_last_row']=$conta<$this->cols?'first_row':(
									  ceil(($num_rows)/$this->cols)==ceil(($conta+1)/$this->cols)?'last_row':''
									  );
				$rs['conta_row']=count($loop);
				$rs['empty_row']='';
                $loop_loop[] = $rs;
                if (($rest) == ($this->cols-1) || ($conta == $num_rows-1)) // final de columna o final de registres
                {
                    // acabar de dibuixar la taula per varies columnes
                    if (($rest != ($this->cols-1)) && ($conta == $num_rows-1))
                    {
                        //$rows_to_finish = $this->cols - ($rest + 1);
						$first_last_row = $rs['first_last_row'];
                        foreach($rs as $key => $value)
                        {
                            // poso tots els camps sense valor per acabar d'emplenar la taula fins al final sense valors
                            $rs[$key] = '';
                        }
						
						// els hi haig de donar valors per que son pels css
						$rs['first_last_row']=$first_last_row;
						$rs['conta_row']=count($loop);
						$rs['empty_row']='empty_row';
						
                        for ($i = $rest+1; $i < $this->cols; $i++)
                        {
							$rs['conta_col']=$i;
							$rs['first_last_col']=$i==0?'first_col':($this->cols==$i+1?'last_col':'');
                            $loop_loop[] = $rs;
                        }
                    }
                    // suma segon loop al primer i tornem a començar segon loop
                    $loop[] = array('loop' => $loop_loop);
                    $loop_loop = array();
                }
            }
            else
            {
                $rs['loop'] = array(array('')); // truc per funcionar els 2 tipus de llistats quan cols=1, els preparats per més d'una columna i els que no
                $loop[] = $rs;
            }
        }
		//Debug::add('Rs process field',$rs);
        $conta++;
    }
	// nomès funciona amb 1 camp, però al ser un array està preparat per poder fer més camps
    function group_fields(&$results)
    {
		// funciona ordenant per nomcamp_id o per nomcamp ( en cas de voler ordenar pel nom en la taula d'idiomes relacionada )
        if (
			!$this->group_fields || 
			(
				!in_array($this->order_col, $this->group_fields) 
				&&
				!in_array($this->order_col.'_id', $this->group_fields)
			) 
		)
		return;
		
        $value = false;
		// nomes agrupo pel que està ordenat, si no no puc agrupar
        $current_field = in_array($this->order_col, $this->group_fields)?$this->order_col:$this->order_col.'_id';
        foreach($results as $key => $rs)
        {
            $new_value = $rs[$current_field];
            if ($new_value !== $value)
            {			
                $value = $new_value;
				
				if ($this->group_fields_function){
					
					$function_name = $this->module&&method_exists($this->module, $this->group_fields_function)?
        		array(&$this->module, $this->group_fields_function):
        		$this->group_fields_function;
					
					
					$results[$key]['group_fields'][$current_field] = 
					call_user_func_array($function_name, array(&$rs, $current_field, $rs[$current_field]));
				}
				else{
					$results[$key]['group_fields'][$current_field] = $rs[$current_field];
				}
				
            }
            // per amagar la columna per la cual agrupem
            /*foreach($this->group_fields as $group_field)
            {
                unset($rs[$group_field]);
            }*/
        }
    }
	// assigno variables generals del listing, nomès han d'anar set_loops i set_vars, ja que es just abans del parse()
    function set_vars_results($loop, $has_results) {
	    global $tpl, $gl_menu_id, $gl_print_link;
	    if ( $has_results ) {
		    // si es public afegim action i tool_action
		    if ( $this->form_link ) {
			    $this->set_var( 'form_link', $this->form_link );
		    } elseif ( $this->is_site_public ) {
			    $this->set_var( 'form_link', '?' . $this->base_link .
			                                 get_all_get_params( array(
				                                 'action',
				                                 'menu_id',
				                                 'tool',
				                                 'tool_section'
			                                 ), '&' ) );
		    } else {
			    $this->set_var( 'form_link', '?menu_id=' . $gl_menu_id .
			                                 get_all_get_params( array( 'action', 'menu_id' ), '&' ) );
		    }
		    $this->set_var( 'print_link', $gl_print_link );
	    }
	    $this->tpl->set_loop( 'loop', $loop );
	    $this->tpl->set_var( 'has_bin', $this->has_bin );
	    $this->tpl->set_var( 'is_bin', $this->is_bin );
	    $this->tpl->set_var( 'results', $has_results );
	    $this->tpl->set_vars( $this->set_list_view() );

	    if ( $this->split_results ) {
		    $this->tpl->set_vars( $this->split_results->display_links( $this, $this->is_site_public && USE_FRIENDLY_URL ) );
		    $this->tpl->set_vars( $this->split_results->display_count( $this ) );
	    }

        $this->tpl->set_var('has_swap_edit', $this->has_swap_edit);
        $this->tpl->set_var('is_editable', $this->is_editable);
        $this->tpl->set_var('onmousedown', $this->onmousedown);
		$this->set_var('has_add_dots', $this->has_add_dots);
		$this->set_var('content_top', $this->content_top);
	    $this->set_var('field_options', $this->field_options);
	    $this->set_var('is_new_records', $this->is_new_records);
	    $this->set_var('is_sublist', $this->is_sublist || $this->is_new_records); // si es new_record també serà sublist

		// isset($this->uploads['image']) serveix per poder posar imatges al llistat sense que tingui imatges, si no està definit no es pot afegir imatges
	    // TODO - is_index hauria de excloure els mòduls que es criden com ara museumoli-news,
	    // que es diferent de quan utilitzo un mòdul de orderitem desde el order per exemple
		if($this->has_images && isset($this->uploads['image']) && $GLOBALS['gl_is_admin'] && $this->is_index && $GLOBALS['gl_page']->template=='general.tpl')
			$GLOBALS['gl_page']->javascript .= 
				'add_dropzones("'.($this->tool).'", "'.($this->tool_section).'", "'.$this->id_field.'", "'. $this->config['im_admin_thumb_w'].'", "'. $this->config['im_admin_thumb_h'].'");';
	
        $this->tpl->set_file($this->template);
    }

    function set_sorting($key)
    {
        if ($this->order_col == $key)
        {
			$r['order_image'] = $this->is_order_asc?'asc':'desc';
			$order_way = $this->is_order_asc?'desc':'asc';				
        }
        else
        {
			$order_way = 'asc';
			$r['order_image'] = '';
        }
		if ($this->is_site_public && USE_FRIENDLY_URL){
			$file_name = isset($this->fields[$this->tool_section . '_file_name'])?'':false;
			$category_file_name = isset($_GET['filecat_file_name'])?$_GET['filecat_file_name']:false;
			/*
			$r['order_link'] = '/'.LANGUAGE. '/' . $this->base_link_friendly . '/orderby/' . $key . '-'. $order_way . '/' . $this->page_file_name;
			$r['order_link'] .= get_all_get_params(array('language','tool','tool_section','orderby','page_file_name'),'?');
			*/
			
			
			$params_arr =$this->propagate_params?array():false;
			
			$this->friendly_params['orderby'] = $key . '-'. $order_way;
			$r['order_link'] = Page::get_link($this->parent_tool?$this->parent_tool:$this->tool, $this->parent_tool_section?$this->parent_tool_section:$this->tool_section,false,false,$this->friendly_params,$params_arr, $file_name, $category_file_name, $this->page_file_name);
			//debug::p($r['order_link']);
			
			$this->friendly_params['orderby'] = $key . '-asc';
			$r['order_link_asc'] = Page::get_link($this->parent_tool?$this->parent_tool:$this->tool, $this->parent_tool_section?$this->parent_tool_section:$this->tool_section,false,false,$this->friendly_params,$params_arr,$file_name, $category_file_name, $this->page_file_name);
			
			$this->friendly_params['orderby'] = $key . '-desc';
			$r['order_link_desc'] = Page::get_link($this->parent_tool?$this->parent_tool:$this->tool, $this->parent_tool_section?$this->parent_tool_section:$this->tool_section,false,false,$this->friendly_params,$params_arr, $file_name,$category_file_name, $this->page_file_name);
			
			$this->set_default_friendly_params(); // torno els valors a default
		}
		else{		
			$r['order_link'] = get_all_get_params(array('order_by'),'?','&') . 'order_by=' . $key . ' ' . $order_way;
		}
        return $r;
    }
	// per fer el menu de vistes dels llistats
    function set_list_view()
    {
        $file_name = isset($this->fields[$this->tool_section . '_file_name'])?'':false;
		$category_file_name = isset($_GET['filecat_file_name'])?$_GET['filecat_file_name']:false;
		
		$actual_view = isset($_GET['view'])?$_GET['view']:1;
		if ($actual_view!=1) $this->template = substr($this->template,0,-4) . '_view_' . $actual_view . '.tpl';
		
		// a view guardo el parametre de page per que alcanviar de vista em quedi a la mateixa pagina
		$this->friendly_params['page'] = R::text_id('page');
		
		$num_views = 4;
		$r['views'] = array();
		for ($v=1; $v<$num_views; $v++){
			if ($this->is_site_public){
					if (USE_FRIENDLY_URL){
						$this->friendly_params['view'] = $v==1?false:$v;						
						$params_arr =$this->propagate_params?array():false;
						
						$link =
						Page::get_link($this->parent_tool?$this->parent_tool:$this->tool, $this->parent_tool_section?$this->parent_tool_section:$this->tool_section,false,false,$this->friendly_params,$params_arr,$file_name, $category_file_name, $this->page_file_name);
					}
					else {
						if ($v==1){
							$link =
							get_all_get_params(array('view'),'?');
						}
						else{
							$link =
							get_all_get_params(array('view'),'?','&') . 'view=' . $v;						
						}
					}
					
			}
			else{
					$link = 
					get_all_get_params(array('view'),'?','&') . 'view=' . $v;
			
			}
			$r['views'][] = array(
				'v_link'=> $link,
				'v_selected'=>$v==$actual_view?1:0,
				'v_view'=>$v,
				'v_name'=>$this->caption['c_list_view_' . $v]);
		}
		$this->set_default_friendly_params(); // torno els valors a default
        return $r;
    }
    // Configuració del menu d'opcions del llistat
    function set_options ($options_bar = 1, $options_checkboxes = 1, $save_button = 1, $select_button = 1, $delete_button = 1, $print_button = 1, $edit_buttons = 1, $image_buttons = 1, $split_count = 1, $order_bar = 1)
    {
        // quan no es editable mai mostro aquests botons
		if (!$this->is_editable){
			$options_checkboxes = 0;
			$save_button = 0;
			$select_button = 0;
			$delete_button = 0;
		}

        $this->tpl->set_var('options_bar', $options_bar);
        $this->tpl->set_var('options_checkboxes', $options_checkboxes);
        $this->tpl->set_var('save_button', $save_button);
        $this->tpl->set_var('select_button', $select_button);
        $this->tpl->set_var('delete_button', $delete_button);
        $this->tpl->set_var('print_button', $print_button);
        $this->tpl->set_var('split_count', $split_count);
        $this->tpl->set_var('order_bar', $order_bar);
        // $this->tpl->set_var('edit_buttons', $edit_buttons);
        // $this->tpl->set_var('image_buttons', $image_buttons);
        $this->options['edit_buttons'] = $edit_buttons;
        $this->options['image_buttons'] = $image_buttons;
        // poso tot els fields com a text si no es pot guardar
	    if ( ! $save_button ) {
		    $this->set_fields_editable( false );
	    }
    }
    // Quan no es editable en forço unes quantes
    function set_options_not_editable ()
    {
			$options_checkboxes = 0;
			$save_button = 0;
			$select_button = 0;
			$delete_button = 0;

        $this->tpl->set_var('options_checkboxes', $options_checkboxes);
        $this->tpl->set_var('save_button', $save_button);
        $this->tpl->set_var('select_button', $select_button);
        $this->tpl->set_var('delete_button', $delete_button);

	    $this->set_fields_editable( false );
    }
    function add_auto_buttons()
    {
        debug::add('base_link', $this->base_link);
		$tool_params = !$this->is_index?'&' . $this->base_link:'';
		// TODO-i Posar només que es propagui el menu_id
		if ($this->options['edit_buttons'])
        {
            $this->add_button ('edit_button', 'action=show_form_edit' . $this->action_add . $tool_params , 'boto1', 'auto');
        }
        if ($this->has_images && $this->options['image_buttons'])
        {
            $this->add_button ('edit_images_button', 'action=list_records_images' . $this->action_add . $tool_params, 'boto1', 'auto');

            $this->add_button ('add_images_button', 'action=list_records_images' . $this->action_add . $tool_params, 'boto1', 'auto');
        }
		
		
		// LINK
		
		/*
		// si no es index afegeixo tool i tool section, ja que pot ser no coincideixen amb el tool definit pel menu_id
		$tool_params = !$this->is_index?'&tool=' . $this->tool . '&tool_section=' . $this->tool_section:'';
		if ($this->options['edit_buttons'])
        {
            $this->add_button ('edit_button', 'action=show_form_edit' . $this->action_add . $tool_params , 'boto1', 'auto');
        }
        if ($this->has_images && $this->options['image_buttons'])
        {
            $this->add_button ('edit_images_button', 'action=list_records_images' . $this->action_add . $tool_params, 'boto1', 'auto');

            $this->add_button ('add_images_button', 'action=list_records_images' . $this->action_add . $tool_params, 'boto1', 'auto');
        }
		*/
    }
    // ---------------------------------------------------------------------------
    // Afegeix 'moure a' al menu opcions
    // Al tpl del llistat ha d'haber la variable: $out_loop_hidden
    // ---------------------------------------------------------------------------
    function add_move_to($field, $select_size = '', $type = 'select')
    {
        $cg = $this->fields[$field] + array('name' => $field, 'input_name' => $field);
        // sobreescric aquest valors ja que sempre vull un input al crear un filtre
        $cg[$this->list_show_type . '_' . $this->site_part] = 'input';
        $cg['type'] = $type;
        $cg['class'] = 'filter';
        $cg['select_size'] = $select_size;
        $cg['javascript'] = 'onChange="javascript:submit_list_move_to(this,\'' . $field . '\', \'save_rows_selected_to_value' . $this->action_add . '\')"';
        if (isset($this->caption['c_move_to_' . $field]))
        {
            $cg['select_caption'] = $this->caption['c_move_to_' . $field ];
        }
        $caption2 = isset($this->caption['c_move_to_' . $field . '2'])?$this->caption['c_move_to_' . $field . '2']:'';
        // caption 3 serveix per moure a cap categoria
        $input = $this->get_input($cg, '', 1);
        if (isset($this->caption['c_move_to_' . $field . '3']))
        {
            // ho poso aquí per no sobrecarregar la clase form object amb un caption adicional
            // ej: $cg['select_caption2']
           // echo strpos($input, '</option>');
            $input = substr_replace ($input,
                '</option><option value="0">' . $this->caption['c_move_to_' . $field . '3'] . '</option>',
                strpos($input, '</option>'), 0);
        }
        $this->options_move_tos[$field] = $caption2 . ' ' . $input;
        $this->out_loop_hidden .= '<input name="' . $field . '" type="hidden" value="" />';
    }
    // ---------------------------------------------------------------------------
    // Afegeixo la funcio de fer un swap edit
    // 
    // ---------------------------------------------------------------------------
	function add_swap_edit(){	
		$this->has_swap_edit = true;
		$this->is_editable = isset($this->last_listing['edit']) ? $this->last_listing['edit']: '';
		
		$this->set_var('swap_edit_link',get_all_get_params(array('call'),'?','&') . 'call=swap_edit');
		$this->set_var('swap_edit_button',$this->caption['c_edit_list_button_' . $this->is_editable]);
		$this->set_var('swap_class',$this->is_editable?'botoOpcionsNoEditar':'botoOpcionsEditar');
		//$this->add_options_button ('edit_list_button_' . $this->is_editable, 'menu_id='.$GLOBALS['gl_menu_id'].'&call=swap_edit', 'botoOpcions1', 'top', '', 'target="save_frame"');
		
		if (!$this->is_editable){
			$this->set_options_not_editable (); // resetejo el set_options amb les opcions forçades quan no es editable
		}
	}
	function set_editable($editable){
		if ($editable){
			$this->is_editable = true;
		}
		else{			
			$this->is_editable = false;
			$this->set_options_not_editable (); // resetejo el set_options amb les opcions forçades quan no es editable
		}
	}
    function set_field_options ($fields) {

        Main::load_class( 'db_utils' );

        $field_options = array();
        foreach ( $fields as $key => $field ) {
            $field_name = 'field_option_' . $field;
            $checked = Utils::get_custom_config_item($this->tool, $field_name, '1')?' checked':'';

	        // Les imatges
	        if ( ! $checked && $field == 'images' ) {
		        $this->has_images = false;
	        } // per poder fer com una variable ( no es el nom de cap camp però pot servir per un grup de items en les plantilles a mida )

	        elseif ( ! array_key_exists( $field, $this->fields ) ) {
		        $this->set_var( $field, $checked );
	        } // Un dels camps
	        elseif ( ! $checked && $field != 'images' ) {
		        $this->cols_to_hide [] = $field;
		        $this->set_field( $field, 'list_' . $this->site_part, 'hide' );
	        }

            $field_options [ $key ] = array(
                'field_option_name'  => $this->caption[ 'c_' . $field ],
                'field_option_input' => '<input type="checkbox" name="field_option_' . $field . '" value = "1"' . $checked . ">",
            );
	        $this->set_var( 'field_option_button', '<input type="submit" name="field_option_button" value="Ok">' );
        }


	    $this->field_options = $field_options;
    }
    // ---------------------------------------------------------------------------
    // Afegeixo la funcio de fer un add_dots en tots els camps text i html
    // 
    // ---------------------------------------------------------------------------
	function add_dots_filter(){	
		$this->has_add_dots = true;
		
		$this->add_dots = $this->last_listing['add_dots'];
		$this->add_dots = $this->add_dots?' checked':''; // === false es opcio per defecte
		
		//$this->add_dots = R::text_id('add_dots');
		//$this->add_dots = $this->add_dots=='true' || $this->add_dots===false?' checked':''; // === false es opcio per defecte
		
		$this->set_var('add_dots_checked',$this->add_dots);
		
		$this->set_var('add_dots_link',get_all_get_params(array('add_dots'),'?','&') . 'call=swap_add_dots');
	}

	/**
	 * Crida una funció per cada registre del llistat, però a diferència del call, es crida amb els resultats ja processats i al moment  <br><br>
	 * Tampoc es poden afegir camps en un ret com es fa al call ( si es volen afegir camps, primer fer-ho amb call ) <br><br>
	 * TODO - Falta en idiomes i per columnes - De moment no es necessari
	 * @return mixed
	 */
	public function call_after_set_records($function_name, $set = 'loop', &$object = false) {

		if ( $object ) {
			$function_name = array( &$object, $function_name );
		}
		else {
			$function_name = array( &$this->module, $function_name );
		}

		//Debug::p( $this->loop );

		foreach ( $this->$set as &$rs ) {
			$parameters = array();
			$parameters[] = $rs;
			$parameters[] = &$this;

			$ret = call_user_func_array( $function_name, $parameters );
			if ( is_array( $ret ) ) {

				// sobrescric per llistats amb plantilla
				$rs = array_merge($rs, $ret);

				// sobrescric per llistats sense plantilla
				if ( $set == 'loop' ) {
					foreach ( $ret as $ret_key => $ret_val ) {
						$this->_set_field_value_after_set_records( $rs, $ret_key, $ret_val );
					}
				}
			}
		}
	}
}