<?
/**
 * AramHabitatge
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2012
 * @version $Id$
 * @access public
 */
class AramHabitatge extends Module{

	function __construct(){
		parent::__construct();
		$this->order_by = 'escala asc,floor_id asc,flat asc';
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function get_records()
	{
	    $listing = new ListRecords($this);
	    $listing->order_by = $this->order_by;
		$listing->add_filter('escala');
		$listing->add_filter('floor_id');
		$listing->add_filter('flat');
		
		if ($this->process=='pagament'){
			$listing->set_field('reserva','list_admin','input');
			$listing->set_field('contracte','list_admin','input');
			$listing->set_field('claus','list_admin','input');
			$listing->set_field('monthly','list_admin','input');
			$listing->set_field('quota_subsidiada','list_admin','input');
			$listing->unset_field('m2');
			$listing->unset_field('m2_exterior');
			$listing->unset_field('room');
		}
		else{
			//$listing->extra_fields = "(SELECT floor FROM aram__floor_language WHERE clubmoto__paginatpl_language.paginatpl_id = clubmoto__pagina.paginatpl_id AND language='".LANGUAGE."') AS paginatpl_id";
		}
		
	    return $listing->list_records();
	}
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{
	    $show = new ShowForm($this);
		$show->order_by = $this->order_by;
		$show->set_var('is_flat_editable',false);
		
		if ($this->action=='show_form_new' || $this->process=='edit_flat'){
			$show->set_field('escala','form_admin','input');
			$show->set_field('floor_id','form_admin','input');
			$show->set_field('flat','form_admin','input');
			$show->set_var('is_flat_editable',true);
		}
	    return $show->show_form();
	}
	function get_hipoteca_ajax(){
		extract($_GET);
		//$monthly_count = $this->config['monthly_count'];
		$interes = $this->config['interest_hipoteca'];
		$price = unformat_currency($price,false);
		$reserva = unformat_currency($reserva,false);
		$contracte = unformat_currency($contracte,false);
		$monthly = unformat_currency($monthly,false);
		$claus = unformat_currency($claus,false);
		$ret = $this->get_months_diff($reserva,$contracte);
		$monthly_count = $ret['monthly_count'];
		$contracte_date = $ret['contracte_date'];
		$end_date = $ret['end_date'];
		$reserva_date = $ret['reserva_date'];
		$claus_date = $ret['claus_date'];
		
		//$hipoteca = $price - ( $reserva + $contracte + ( $monthly * $monthly_count ) + $claus );
		//$percent = $hipoteca/$price*100;
		
		$hipoteca = $price*0.8;
		$percent = 80;
		
		$quota_hipoteca = $hipoteca * ($interes/12) / (100 * ( 1 - pow( 1 + $interes/12/100,-25*12) ));
		
		$total_entregat = $reserva + $contracte + $monthly * $monthly_count + $claus;
		
		$price = format_currency($price);
		$reserva = format_currency($reserva);
		$contracte = format_currency($contracte);
		$monthly_total = format_currency( $monthly * $monthly_count );
		$claus = format_currency($claus);
		$total_entregat = format_currency($total_entregat);
		$hipoteca = format_decimal($hipoteca);
		$percent = format_decimal($percent);
		$quota_hipoteca = format_currency($quota_hipoteca);
		
		if ($reserva)
		echo "<table cellpadding='5'>";
		echo ("<br />
		<tr><td>Reserva: </td><td><strong>$reserva_date</strong></td><td align='right'>$reserva &euro;</td></tr>");
		
		echo ("
		<tr><td>Contracte: </td><td><strong>$contracte_date</strong></td><td align='right'> $contracte &euro;</td></tr>
		<tr><td>Nombre de quotes: </td><td><strong>$monthly_count</strong></td><td align='right'> $monthly_total &euro;</td></tr>
		<tr><td>Pagament final: </td><td><strong>$end_date</strong></td><td></td></tr>
		<tr><td>Lliurament de claus: </td><td><strong>$claus_date</strong></td><td align='right'> $contracte &euro;</td></tr>
		<tr><td><strong>Total: </strong></td><td></td><td align='right'> $total_entregat &euro;</td></tr>
		<tr><td></td><td></td><td></td></tr>
		<tr><td>Hipoteca $percent%: </td><td></td><td align='right'><strong>$hipoteca &euro;</strong></td></tr>
		<tr><td>Quota mensual: </td><td><strong></td><td align='right'>$quota_hipoteca &euro;</strong></td></tr>
		<tr><td>25 anys  </td><td></td><td></td></tr>
		<tr><td>$interes% inter&egrave;s</td><td></td><td></td></tr></table>");
		die();
	}
	
	function get_months_diff($reserva,$contracte){
	
		$init_date = strtotime($this->config['init_date_payment']);//'2010-01-01';
		$end_date = strtotime($this->config['end_date_payment']);
		$reserva_date = strtotime($this->config['reserva_date']);
		$claus_date = strtotime($this->config['claus_date']);
		
		$init_date = getdate($init_date);
		$end_date = getdate($end_date);
		$reserva_date = getdate($reserva_date);
		$claus_date = getdate($claus_date);
		
		$year_start = $init_date['year'];
		$month_start = $init_date['mon'];
		$year_end = $end_date['year'];
		$month_end = $end_date['mon'];
		
		$years = $year_end-$year_start;
		$months = $month_end - $month_start;
		
		$months = ($years*12)+$months; // no sumo 1 per no contar el mes inici que es el de contracte
		
		//debug::p($init_date);
		//debug::p($end_date);
		//debug::p($months);
		//$months = $months-2; // resto el mes de contracte i pagament final
		
		$ret['monthly_count'] = $months;
		
		// inici pagament contracte noms mesos
		$month = $init_date['mon'];
		$year = $init_date['year'];
		
		$ret['contracte_date'] = $GLOBALS['month_names'][$month-1] . ' ' . $year;	
		
		// final pagament noms mesos
		$month = $end_date['mon'];
		$year = $end_date['year'];
		$ret['end_date'] = $GLOBALS['month_names'][$month-1] . ' ' . $year;	
		
		// reserva noms mesos
		$month = $reserva_date['mon'];
		$year = $reserva_date['year'];
		$ret['reserva_date'] = $GLOBALS['month_names'][$month-1] . ' ' . $year;		
		
		// entrega claus noms mesos
		$month = $claus_date['mon'];
		$year = $claus_date['year'];
		$ret['claus_date'] = $GLOBALS['month_names'][$month-1] . ' ' . $year;	
		
		return $ret;
	}

	function save_rows()
	{
	    $save_rows = new SaveRows($this);
	    $save_rows->save();
	}

	function write_record()
	{
	    $writerec = new SaveRows($this);
	    $writerec->save();
	}

	function manage_images()
	{
	    $image_manager = new ImageManager($this);
	    $image_manager->execute();
	}
}
?>