$(
	function(){
		// activo el onchange als evaluacio
		$('#pv select[name^=evaluacio]').change(
			function(data){
				if (data.target.value=='noconforme'){
					$('#texte1_'+data.target.id).show();
					$('#texte2_'+data.target.id).show();
				}
				else{
					$('#texte1_'+data.target.id).hide();
					$('#texte2_'+data.target.id).hide();
				}
			}
		);
		// activo el onchange a avis
		$('#pv input[name^=avis]').click(
			function(){
				if ($(this).is(':checked')){
					$('#texte_'+this.id).show();
					$('#input_'+this.id).show();
				}
				else{
					$('#texte_'+this.id).hide();
					$('#input_'+this.id).hide();
				}
			}
		);
	}
);

function pv_open_window(link){
	showPopWin(link, 870, 520, false, true);
}

function add_new_user(client_id){
	showPopWin('/admin/?template=window&menu_id=7002&group_id=4&client_id='+client_id, 750, 520, false, true);
}

function edit_user(client_id,user_id){
	showPopWin('/admin/?template=window&menu_id=7003&action=show_form_edit&user_id='+user_id+'&client_id='+client_id, 750, 520, false, true);
}


function open_print_window_seguimentitem(url){
	window.open(url+'&action=get_pdf','printWindow','toolbar=no,location=no,directories=no,status=no,menubar=yes,scrollbars=yes,resizable=yes,copyhistory=no,width='+ ($(window).width()-100) +',height='+ ($(window).height()-100) +',screenX=50,screenY=50,top=50,left=50')
}

$(function () {
	pv.init();
});

var pv = {

	client_id: 0,
	init: function (){
		if (gl_tool_section == 'destinatari' || gl_tool_section == 'conductor  ') pv.init_country();
		if (gl_tool == 'pvgmao' || gl_tool_section == 'maquina  ') pv.init_estat_history();
	},
	init_country: function (){

		var id = gl_current_form_id;
		if (id!==false){

			var country = '#country_id_' + id;

			if (!$(country).length) return;

			$(country).change(function(){
				pv.country_on_change (this.value)
			});

			pv.country_on_change ($(country).val());
		}
	},
	country_on_change: function (value){

		var provincia = $('#provincia_id_holder');

		if(value=='ES'){
			provincia.show();
		}
		else{
			provincia.hide();
		}

	},
	init_estat_history: function (){
		if (gl_action != 'show_form_new') {
			$('#estat_id_holder .forms td').append('<a title="' + gl_pvgmao_see_estat_history + '" class="add_new_link" href="javascript:pv.show_estat_history();">' + gl_pvgmao_see_estat_history + '</a>');
		}
	},
	show_estat_history: function (){
		var maquina_id = gl_current_form_id;

		showPopWin('/admin/?menu_id=2737&action=list_records&template=clean&maquina_id=' + maquina_id + '&client_id=' + pv.client_id, 950, 580, null, true, false, '', true);
	}

};