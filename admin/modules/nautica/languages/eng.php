<?php
// menus eina
define('NAUTICA_MENU_EMBARCACION_LIST', 'Llistat i editar embarcacions');
define('NAUTICA_MENU_EMBARCACION', 'Embarcacions');
define('NAUTICA_MENU_EMBARCACION_NEW', 'Nova embarcació');
define('NAUTICA_MENU_EMBARCACION_BIN', 'Paperera de reciclatge');
define('NAUTICA_MENU_TIPUS', 'Tipus');
define('NAUTICA_MENU_TIPUS_LIST', 'Llistat de tipus');
define('NAUTICA_MENU_TIPUS_NEW', 'Nou tipus');
define('NAUTICA_MENU_SHIPYARD', 'Astillers');
define('NAUTICA_MENU_SHIPYARD_LIST', 'Llistat d\'astillers');
define('NAUTICA_MENU_SHIPYARD_NEW', 'Nou astiller');
define('NAUTICA_MENU_FLAG', 'Banderes');
define('NAUTICA_MENU_FLAG_LIST', 'Llistat de banderes');
define('NAUTICA_MENU_FLAG_NEW', 'Nova bandera');

// captions seccio
$gl_caption_embarcacion['c_status'] = 'State';
$gl_caption_embarcacion['c_clau_dossier'] = 'Internal data';
$gl_caption_embarcacion['c_clau'] = 'Key';
$gl_caption_embarcacion['c_dossier'] = 'File';
$gl_caption_embarcacion['c_entered'] = 'Entered';
$gl_caption_embarcacion['c_entered_form'] = 'Check in';
$gl_caption_embarcacion['c_prepare'] = 'Preparing';
$gl_caption_embarcacion['c_review'] = 'Review';
$gl_caption_embarcacion['c_onsale'] = 'On sale';
$gl_caption_embarcacion['c_sold'] = 'Sold';
$gl_caption_embarcacion['c_archived'] = 'Filed';
$gl_caption_embarcacion['c_reserved'] = 'Pending';
$gl_caption_embarcacion['c_tipus_id'] = 'Kind of boat';
$gl_caption['c_shipyard'] = 'Shipyard';
$gl_caption_embarcacion['c_shipyard_id'] = 'Shipyard';
$gl_caption_embarcacion['c_model'] = 'Model';
$gl_caption_embarcacion['c_buildyear'] = 'Year';
$gl_caption_embarcacion['c_buildyear_form'] = 'Year';
$gl_caption_embarcacion['c_length'] = 'Length';
$gl_caption_embarcacion['c_breadth'] = 'Beam';
$gl_caption_embarcacion['c_weight'] = 'Weight';
$gl_caption_embarcacion['c_depth'] = 'Draught';
$gl_caption_embarcacion['c_depth2'] = 'Draught';
$gl_caption_embarcacion['c_engine'] = 'Engine';
$gl_caption_embarcacion['c_fuel'] = 'Fuel';
$gl_caption_embarcacion['c_fuel_gasoline'] = 'Petrol';
$gl_caption_embarcacion['c_fuel_diesel'] = 'Diesel';
$gl_caption['c_flag'] = 'Flag';
$gl_caption_embarcacion['c_flag_id'] = 'Flag';
$gl_caption_embarcacion['c_hour_use'] = 'Hours';
$gl_caption_embarcacion['c_price'] = 'Price';
$gl_caption_embarcacion['c_price_pounds'] = 'Price (&)';

$gl_caption_embarcacion['c_vhf'] = 'VHF';
$gl_caption_embarcacion['c_depthsounder'] = 'Deepthsounder';
$gl_caption_embarcacion['c_speedlog'] = 'Speed Log';
$gl_caption_embarcacion['c_radar'] = 'Radar';
$gl_caption_embarcacion['c_gps'] = 'GPS';
$gl_caption_embarcacion['c_plotter'] = 'Plotter';
$gl_caption_embarcacion['c_autopilot'] = 'Autopilot';
$gl_caption_embarcacion['c_trimmtabs'] = 'Trimmtabs';
$gl_caption_embarcacion['c_searchlight'] = 'Searchlight';
$gl_caption_embarcacion['c_electrical_capstan'] = 'Capstan El.';
$gl_caption_embarcacion['c_gangway'] = 'Gangway';
$gl_caption_embarcacion['c_liferaft'] = 'Liferaft';
$gl_caption_embarcacion['c_tender'] = 'Tender';
$gl_caption_embarcacion['c_offshore_connexion'] = 'Offshore con.';
$gl_caption_embarcacion['c_battery_charger'] = 'Battery charger';
$gl_caption_embarcacion['c_generator'] = 'Generator';
$gl_caption_embarcacion['c_cover'] = 'Cover';
$gl_caption_embarcacion['c_camping_cover'] = 'Cabriolet';
$gl_caption_embarcacion['c_bimini_top'] = 'Bimini top';
$gl_caption_embarcacion['c_bowthruster'] = 'Bowthruster';
$gl_caption_embarcacion['c_hot_water'] = 'Hot water';
$gl_caption_embarcacion['c_davit'] = 'Davit';
$gl_caption_embarcacion['c_anemometer'] = 'Wind Log';
$gl_caption_embarcacion['c_mainsail'] = 'Mainsail';
$gl_caption_embarcacion['c_genoa'] = 'Genoa';
$gl_caption_embarcacion['c_mizzen'] = 'Mizzen';
$gl_caption_embarcacion['c_jib'] = 'Jib';
$gl_caption_embarcacion['c_stormsail'] = 'Storm sail';
$gl_caption_embarcacion['c_forsail2'] = '2nd. Forsail';
$gl_caption_embarcacion['c_spinaker'] = 'Spinaker';
$gl_caption_embarcacion['c_spi_boom'] = 'Spi-boom';
$gl_caption_embarcacion['c_roller_genoa'] = 'Roller-reefing Genoa';

$gl_caption_embarcacion['c_music_equipment'] = 'Music radio';
$gl_caption_embarcacion['c_television'] = 'TV';
$gl_caption_embarcacion['c_attached_engine'] = 'Out board engine';
$gl_caption_embarcacion['c_fridge'] = 'Fridge';
$gl_caption_embarcacion['c_heating'] = 'Heating';
$gl_caption_embarcacion['c_air_conditioning'] = 'Air conditioning';
$gl_caption_embarcacion['c_teka'] = 'Teak deck';
$gl_caption_embarcacion['c_wc'] = 'WC';
$gl_caption_embarcacion['c_black_water'] = 'Holding tank';
$gl_caption_embarcacion['c_roller_mainsail'] = 'Roller-reefing Mainsali';




$gl_caption_embarcacion['c_equipament'] = 'Equipment';

$gl_caption_embarcacion['c_data'] = 'Vessel';
$gl_caption_embarcacion['c_ref'] = 'Reference';
$gl_caption_embarcacion['c_general_data'] = 'General information';
$gl_caption_embarcacion['c_measures_weight'] = 'Mides - Pes';
$gl_caption_embarcacion['c_measures'] = 'Sizes';
$gl_caption_embarcacion['c_weight_units'] = 'kg.';
$gl_caption_embarcacion['c_characteristics_electronic'] = 'Electronik';
$gl_caption_embarcacion['c_characteristics_electronic_caps'] = 'ELECTRONIK';
$gl_caption_embarcacion['c_characteristics_extra'] = 'Accessories';
$gl_caption_embarcacion['c_characteristics_sail'] = 'Sail';
$gl_caption_embarcacion['c_characteristics'] = 'Specification';

$gl_caption_embarcacion['c_filter_shipyard_id'] = 'All the shipyards';
$gl_caption_embarcacion['c_filter_tipus_id'] = 'Kind of boats';
$gl_caption_embarcacion['c_filter_status'] = 'All the states';
$gl_caption_embarcacion['c_filter_length'] = 'All the lengths';
$gl_caption_embarcacion['c_filter_price'] = 'All the prices';
$gl_caption_embarcacion['c_print_pdf'] = 'Print pdf';
$gl_caption_embarcacion['c_changeable'] = 'Acepto embarcación a cambio';

$gl_caption_tipus['c_ordre']='Order';
$gl_caption_tipus['c_tipus']='Type';

$gl_caption_embarcacion['c_seo']='Herramientas SEO ( Posicionamiento web )';
$gl_caption_embarcacion['c_embarcacion_file_name']=$GLOBALS['gl_caption']['c_file_name'];
$gl_caption_embarcacion['c_embarcacion_old_file_name']= $GLOBALS['gl_caption']['c_old_file_name'];


$gl_caption_embarcacion['c_engine_type'] = 'Tipo motor';
$gl_caption_embarcacion['c_engine_type_fora'] = 'Fueraborda';
$gl_caption_embarcacion['c_engine_type_intra'] = 'Intraborda';
$gl_caption_embarcacion['c_fuel_tank'] = 'Depósito combustible';
$gl_caption_embarcacion['c_water_tank'] = 'Depósito agua';
$gl_caption_embarcacion['c_compass'] = 'Compás';
$gl_caption_embarcacion['c_bath_stair'] = 'Escalera de baño';
$gl_caption_embarcacion['c_trim'] = 'Trim';
$gl_caption_embarcacion['c_kitchen'] = 'Cocina';
$gl_caption_embarcacion['c_microwave'] = 'Microondas';
$gl_caption_embarcacion['c_shower'] = 'Ducha';
?>