<?

/* 6-3-18
User


 */

if ( is_table( 'user__group' ) && ! is_field( 'user__group', 'is_editable' ) ) {

	$update_sql[] = "ALTER TABLE `user__group`
	ADD COLUMN `is_editable` TINYINT(1) NOT NULL DEFAULT '1' AFTER `edit_only_descriptions`;
";

}
else {
	if ( is_table( 'user__group' ) ) {
		trigger_error( 'Ja existeixen el camp is_editable' );
	}
}