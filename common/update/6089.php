<?

/* 4-10-16

	DIRECTORY

	UP-DATE
	- Afegir camp videoframe

 */
if ( is_table( 'directory__document' ) && ! is_field( 'directory__document', 'videoframe' ) ) {

	$update_sql[] = "ALTER TABLE `directory__document`
	ADD COLUMN `videoframe` TEXT NOT NULL AFTER `category_id`;";

} else {
	if ( is_table( 'directory__document' ) ) {
		trigger_error( 'Ja existeixen el camp videoframe' );
	}
}

?>