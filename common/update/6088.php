<?

/* 3-10-16

	PRODUCT

	UP-DATE
	- Afegir camp a product

 */

if ( is_table( 'product__product' ) && ! is_field( 'product__product', 'ishandmade' ) ) {

	$update_sql[] = "ALTER TABLE `product__product`

      ADD COLUMN `ishandmade` TINYINT(1) NOT NULL DEFAULT '0' AFTER `videoframe`;
";

} else {
	if ( is_table( 'product__product' ) ) {
		trigger_error( 'Ja existeixen el camp ishandmade' );
	}
}


?>