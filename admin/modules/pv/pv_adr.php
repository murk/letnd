<?
/**
 * PvAdr
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2011
 * @version $Id$
 * @access public
 */
class PvAdr extends Module{

	var $needs_client_id = true;
	var $group_id, $user_is_admin, $user_is_client, $client_id;

	function __construct(){
		parent::__construct();
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function get_records()
	{
	    $listing = new ListRecords($this);
		$listing->set_field( 'adr_to_etiqueta', 'type', 'checkboxes' );
		$listing->condition = "client_id = " . $this->client_id;
		$listing->add_swap_edit();
		$listing->order_by = 'nom_comercial ASC, designacio ASC, codi_producte ASC';
	    return $listing->list_records();
	}
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{
	    $show = new ShowForm($this);
		$show->set_form_level1_titles( 'capacitat_envas' );
		$show->condition = "client_id = " . $this->client_id;
	    return $show->show_form();
	}

	function save_rows()
	{
	    $save_rows = new SaveRows($this);
	    $save_rows->save();
	}

	function write_record()
	{
	    $writerec = new SaveRows($this);
	    $writerec->save();
	}

	function manage_images()
	{
	    $image_manager = new ImageManager($this);
	    $image_manager->execute();
	}

	public function get_text_autocomplete_values() {
		{
			$limit = 100;

			$q = R::escape( 'query' );

			$ret                = array();
			$ret['suggestions'] = array();

			$r = &$ret['suggestions'];

			//	if ($q)
			//	{
			// 1 - Designació, que comenci
			$query = "SELECT adr_id, designacio
					FROM pv__adr
					WHERE designacio LIKE '" . $q . "%'
					ORDER BY designacio
					LIMIT " . $limit;

			$results = Db::get_rows( $query );

			$rest = $limit - count( $results );

			// 2 - Designació, qualsevol posició
			if ( $rest > 0 ) {

				$query = "SELECT adr_id, designacio
						FROM pv__adr
						WHERE designacio LIKE '%" . $q . "%'
			            AND designacio NOT LIKE '" . $q . "%'
						ORDER BY designacio
						LIMIT " . $rest;

				$results2 = Db::get_rows( $query );

				$results = array_merge( $results, $results2 );

			}


			// Presento resultats
			foreach ( $results as $rs ) {
				$r[]   = array(
					'value' => $rs['designacio'],
					'data'  => $rs['adr_id']
				);
			}
			//	}


			$rest = $limit - count( $results );
			$ret['is_complete'] = $rest>0;

			die ( json_encode( $ret ) );


		}

	}
	public function get_checkboxes_long_values() {
		{
			$limit = 100;

			$g = R::escape( 'query' );

			$ret                = array();
			$ret['suggestions'] = array();

			$r = &$ret['suggestions'];

			$q = new Query( 'pv__etiqueta' );

			$q->set_order_by('etiqueta');
			$fields = "etiqueta_id, etiqueta";

			//	if ($q)
			//	{
			// 1 - Designació, que comenci
			$results = $q->set_limit($limit)->get( $fields, "etiqueta LIKE '$g%'");

			$rest = $limit - count( $results );

			// 2 - Designació, qualsevol posició
			if ( $rest > 0 ) {

				$results2 = $q->set_limit($rest)->get( $fields, "etiqueta LIKE '$g%' AND etiqueta NOT LIKE '$g%'");

				$results = array_merge( $results, $results2 );

			}


			// Presento resultats
			foreach ( $results as $rs ) {
				$r[]   = array(
					'value' => $rs['etiqueta'],
					'data'  => $rs['etiqueta_id']
				);
			}
			//	}

			$rest = $limit - count( $results );
			$ret['is_complete'] = $rest>0;

			die ( json_encode( $ret ) );


		}

	}


	public function get_sublist_html( $id, $field, $is_input, $is_word = false ) {
		$listing = NewRow::get_sublist_html( $this, $id, $field, $is_input );

		$listing->order_by = 'ordre ASC, etiqueta ASC';

		$listing->list_records( false );

		return $listing->parse_template();

	}

	// ajax
	public function get_sublist_item() {

		$listing = new NewRow( $this );

		$etiqueta_id = $listing->selected_id;

		// Valors per defecte i no modificables
		$rs = array(
			'etiqueta' => Db::get_first( "SELECT pv__etiqueta.etiqueta FROM pv__etiqueta WHERE pv__etiqueta.etiqueta_id = '$etiqueta_id'" ),
		);

		$listing->add_row( $rs );
		$listing->new_row();
	}

}