<?php
// menus eina
define('CONTACT_MENU_CONTACT','Nous contacter');
define('CONTACT_MENU_LIST','Adresses E-mail de contact');

// captions seccio
$gl_caption_contact ['c_company'] = 'Compagnie';
$gl_caption_contact ['c_name'] = 'Prénom';
$gl_caption_contact ['c_surnames'] = 'Nom';
$gl_caption_contact ['c_mail'] = 'E-mail';
$gl_caption_contact ['c_to_address'] = 'Pour';
$gl_caption_contact ['c_phone'] = 'Téléphone';
$gl_caption_contact ['c_matter'] = 'Sujet';
$gl_caption_contact ['c_comment'] = 'Observations';
$gl_caption_contact ['c_accept_info'] = 'Allow send information';
$gl_caption_contact_list ['c_accept_info'] = 'Allow info';
$gl_caption ['c_sendmail'] = 'Envoyer le courrier';
$gl_messages ['added'] = 'Le courrier a été correctement envoyé.';
$gl_caption ['c_sendok'] = $gl_messages ['added'];
$gl_messages ['not_saved '] = 'Le courrier n\'a pas pu être envoyé.';

$gl_caption_contact['c_nif']='N.I.F / D.NI.';
$gl_caption_contact['c_company']='Compagnie';

$gl_caption_contact['c_contact_language']='Contact page language';
// NO TRADUIT
$gl_caption_contact['c_assign_custumer']='Ajouter à clients';
$gl_caption_contact['c_assign_newsletter']='Ajouter à Newsletter';
$gl_caption_contact['c_entered']= 'Envoyée';

$gl_caption_contact['c_export_button']= 'Exportar';