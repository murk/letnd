<?php

/* 18-11-14 
  
 * Immo: Paísos de la comunitat europea

 */

	
	
if (is_table('all__country') && !(is_field('all__country','european_comunity'))){
	
	$update_sql .= "
	ALTER TABLE `all__country`
		ADD COLUMN `european_comunity` TINYINT(1) NULL DEFAULT 0 AFTER `country`;
	";	
	
}
	
	
if (is_table('all__country')){
	
	$update_sql .= "
	UPDATE `all__country` SET `european_comunity`=1 WHERE  `country_id`='DE';
	UPDATE `all__country` SET `european_comunity`=1 WHERE  `country_id`='AT';
	UPDATE `all__country` SET `european_comunity`=1 WHERE  `country_id`='BE';
	UPDATE `all__country` SET `european_comunity`=1 WHERE  `country_id`='BG';
	UPDATE `all__country` SET `european_comunity`=1 WHERE  `country_id`='CY';
	UPDATE `all__country` SET `european_comunity`=1 WHERE  `country_id`='HR';
	UPDATE `all__country` SET `european_comunity`=1 WHERE  `country_id`='DK';
	UPDATE `all__country` SET `european_comunity`=1 WHERE  `country_id`='SK';
	UPDATE `all__country` SET `european_comunity`=1 WHERE  `country_id`='SI';
	UPDATE `all__country` SET `european_comunity`=1 WHERE  `country_id`='ES';
	UPDATE `all__country` SET `european_comunity`=1 WHERE  `country_id`='EE';
	UPDATE `all__country` SET `european_comunity`=1 WHERE  `country_id`='FI';
	UPDATE `all__country` SET `european_comunity`=1 WHERE  `country_id`='FR';
	UPDATE `all__country` SET `european_comunity`=1 WHERE  `country_id`='GR';
	UPDATE `all__country` SET `european_comunity`=1 WHERE  `country_id`='NL';
	UPDATE `all__country` SET `european_comunity`=1 WHERE  `country_id`='HU';
	UPDATE `all__country` SET `european_comunity`=1 WHERE  `country_id`='IE';
	UPDATE `all__country` SET `european_comunity`=1 WHERE  `country_id`='IT';
	UPDATE `all__country` SET `european_comunity`=1 WHERE  `country_id`='LV';
	UPDATE `all__country` SET `european_comunity`=1 WHERE  `country_id`='LT';
	UPDATE `all__country` SET `european_comunity`=1 WHERE  `country_id`='LU';
	UPDATE `all__country` SET `european_comunity`=1 WHERE  `country_id`='MT';
	UPDATE `all__country` SET `european_comunity`=1 WHERE  `country_id`='PL';
	UPDATE `all__country` SET `european_comunity`=1 WHERE  `country_id`='PT';
	UPDATE `all__country` SET `european_comunity`=1 WHERE  `country_id`='GB';
	UPDATE `all__country` SET `european_comunity`=1 WHERE  `country_id`='CZ';
	UPDATE `all__country` SET `european_comunity`=1 WHERE  `country_id`='RO';
	UPDATE `all__country` SET `european_comunity`=1 WHERE  `country_id`='SE';
	";	
	
}
	
?>