<?

/**
 * LassdiveLog
 *
 * /admin/?tool_section=log&menu_id=2303
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2018
 * @version $Id$
 * @access public
 *
 *
SELECT *
FROM lassdive__orderitem
	INNER JOIN product__orderitem USING (orderitem_id)
WHERE reservaitem_id = 712
 *
 *
 */
class LassdiveLog extends Module {

	public function __construct() {
		parent::__construct();
		$this->table = 'a__log';
	}

	public function list_records() {
		$GLOBALS['gl_content'] = $this->get_records();
	}

	public function get_records() {
		$GLOBALS['gl_page']->add_javascript_file( '/common/jscripts/prism/prism.js' );
		$GLOBALS['gl_page']->add_css_file( '/common/jscripts/prism/prism.css' );

		$listing = new ListRecords( $this );
		$listing->set_options( 1, 0, 0, 0, 0, 0 );
		$listing->call( 'records_walk', false, true );

		$listing->condition = "session LIKE '%cart_vars%'";
		
		$listing->has_bin = false;

		return $listing->list_records();
	}

	public function records_walk( &$listing ) {

		$rs           = $listing->rs;
		$ret          = array();
		$s            = $rs['mysql'];
		$ret['mysql'] = "<pre><code class=\"language-sql\">$s</code></pre>";

		$s            = $rs['session'];
		$data = json_decode( $s );
		$s = json_encode($data, JSON_PRETTY_PRINT);
		$ret['session'] = "<pre><code class=\"language-json\">$s</code></pre>";

		$s            = $rs['cookie'];
		$data = json_decode( $s );
		$s = json_encode($data, JSON_PRETTY_PRINT);
		$ret['cookie'] = "<pre><code class=\"language-json\">$s</code></pre>";

		$s            = $rs['vars'];
		$data = json_decode( $s );
		$s = json_encode($data, JSON_PRETTY_PRINT);
		$ret['vars'] = "<pre><code class=\"language-json\">$s</code></pre>";

		$s                   = $rs['query_string'];
		$ret['query_string'] = str_replace( '&', '<br>&', $s );

		return $ret;

	}

	public function show_form() {
		$GLOBALS['gl_content'] = $this->get_form();
	}

	public function get_form() {
		$show = new ShowForm( $this );

		return $show->show_form();
	}

	public function save_rows() {
		$save_rows = new SaveRows( $this );
		$save_rows->save();
	}

	public function write_record() {
		$writerec = new SaveRows( $this );
		$writerec->save();
	}

	public function manage_images() {
		$image_manager = new ImageManager( $this );
		$image_manager->execute();
	}
}