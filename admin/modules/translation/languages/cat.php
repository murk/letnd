<?php
// menus eina


define('TRANSLATION_MENU_TRANSLATION','Llistat de traduccions:');
define('TRANSLATION_MENU_LIST','Traduccions per data:');
define('TRANSLATION_MENU_LIST_STATE','Traduccions per estat:');
define ('SAVED_TRANSLATION','Traducció guardada');
define ('SAVED_TRANSLATION_ERROR','Traducció guardada amb ERROR');
define ('FINISHED_TRANSLATION','Traducció finalitzada');


//tpl llistar
$gl_caption_translation['c_join_date'] = 'Data d\'alta';
$gl_caption_translation['c_numwords'] = 'Número de paraules: ';
$gl_caption_translation['c_translation_id'] = 'Ref.';
$gl_caption_translation['c_client_id'] = 'Client';
$gl_caption_translation['c_id_fake'] = 'Registre';
$gl_caption_translation['c_2'] = 'En traducció';
$gl_caption_translation['c_3'] = 'Finalitzat';
$gl_caption_translation['c_1'] = 'Pendent';
$gl_caption_translation['c_4'] = 'Error';
$gl_caption_translation['c_send_edit'] = $GLOBALS['gl_caption']['c_send_edit'];
$gl_caption_translation['c_original_text']='text original';
$gl_caption_translation['c_translation']='Traducció';
//tpl editar
$gl_caption_translation['c_languagetranslate']= 'Idioma a traduir: ';
$gl_caption_translation['c_state'] = 'Estat actual:';
$gl_caption_translation['c_originaltext'] = 'Text original:';
$gl_caption_translation['c_original_language'] = 'Idioma original:';
$gl_caption_translation['c_originaltranslator'] = 'Traductor inicial:';
$gl_caption_translation['c_changestate']= 'Canviar l\'estat actual: ';

$gl_caption_translation['c_filter_state2']= 'Tria estat:';
$gl_caption_translation['c_filter_state']= 'Tots els estats';
$gl_caption_translation['c_filter_original_language2']= 'Tria idioma original:';
$gl_caption_translation['c_filter_original_language']= 'Tots els idiomes';

?>