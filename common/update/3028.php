<?php
// no es poden posar ; entre mig de la consulta ja que parteix les consultes per el ; a no ser que fem servir $update_sql en array()
if (is_table("product__product")){	
	
	// creo el directori per guardar els productes digitals
	$client_dir = DOCUMENT_ROOT . '/' . $_SESSION['client_dir'];
	$dir=$client_dir . '/uploads/';
	if (!is_dir($dir)){
		mkdir ($dir, 0776);
		
		$dir=$client_dir . '/uploads/files/';
		mkdir ($dir, 0776);
		
		$dir=$client_dir . '/uploads/images/';
		mkdir ($dir, 0776);
	}
	$dir=$client_dir . '/templates/page/';
	if (is_dir($dir)){
		$dir=$client_dir . '/templates/page/old/';
		if (!is_dir($dir)){
			mkdir ($dir, 0776);
		}
	}
	
}				
?>