<?php
// menus eina
if (!defined('INSIDE_MENU_TOUR')){
	define('INSIDE_MENU_TOUR','Tours');
	define('INSIDE_MENU_TOUR_NEW','Nou tour');
	define('INSIDE_MENU_TOUR_LIST','Llistar tours');
	define('INSIDE_MENU_TOUR_BIN','Paperera de reciclatge');
	define('INSIDE_MENU_PLACE','Llocs');
	define('INSIDE_MENU_PLACE_NEW','Nou lloc');
	define('INSIDE_MENU_PLACE_LIST','Llistar llocs');
	define('INSIDE_MENU_ELEMENT','Inclosos / no inclosos');
	define('INSIDE_MENU_ELEMENT_NEW','Nou ítem');
	define('INSIDE_MENU_ELEMENT_LIST','Llistar ítems');
	define('INSIDE_MENU_CATEGORY', 'Categories');
	define('INSIDE_MENU_CATEGORY_NEW', 'Entrar nova categoria');
	define('INSIDE_MENU_CATEGORY_LIST', 'Llistar categories');
}

// captions seccio
$gl_caption['c_subtitle'] = "Subtítol";

$gl_caption_tour['c_prepare'] = 'En preparació';
$gl_caption_tour['c_review'] = 'Per revisar';
$gl_caption_tour['c_public'] = 'Públic';
$gl_caption_tour['c_archived'] = 'Arxivat';
$gl_caption_tour['c_tour'] = 'Tour';
$gl_caption_tour['c_content'] = 'Descripció';
$gl_caption_tour['c_entered'] = 'Data creació';
$gl_caption_tour['c_modified'] = 'Data modificació';
$gl_caption_tour['c_status'] = 'Estat';
$gl_caption_tour['c_filter_category_id'] = 'Totes les categories';
$gl_caption_tour['c_filter_status'] = 'Tots els estats';
$gl_caption_tour['c_filter_in_home'] = 'Totes, home o no';
$gl_caption_tour['c_ordre'] = 'Ordre';
$gl_caption_tour['c_url1_name']='Nom URL(1)';
$gl_caption_tour['c_url1']='URL(1)';
$gl_caption_tour['c_url2_name']='Nom URL(2)';
$gl_caption_tour['c_url2']='URL(2)';
$gl_caption_tour['c_url3_name']='Nom URL(3)';
$gl_caption_tour['c_url3']='URL(3)';
$gl_caption_tour['c_videoframe']='Video (youtube, metacafe...)';
$gl_caption_tour['c_file']='Arxius';
$gl_caption_tour['c_in_home']='Surt a la Home';
$gl_caption_tour['c_in_home_0']='No surt a la Home';
$gl_caption_tour['c_in_home_1']='Surt a la Home';
$gl_caption_tour['c_content_list']='Descripció curta al llistat';
$gl_caption_tour['c_price']='Preu';
$gl_caption_tour['c_price_children']='Preu nens';
$gl_caption_tour['c_price_text']='Comentari preu';

$gl_caption_tour['c_form_level1_title_page_title']='Eines SEO';

$gl_caption_tour['c_form_level1_title_includes'] = "Inclou - exclou";
$gl_caption_tour['c_form_level1_title_bites'] = "Bites & Tastings";
$gl_caption_tour['c_form_level1_title_duration']='Tour info';
$gl_caption_tour['c_form_level1_title_price']='Preus';
$gl_caption_tour['c_form_level1_title_tour']='Descripcions';
$gl_caption_tour['c_form_level1_title_url1_name']='Enllaços i arxius adjunts';
$gl_caption_tour['c_includes'] = 'Inclou';
$gl_caption_tour['c_excludes'] = 'Exclou';
$gl_caption_tour['c_edit_dies'] = 'Dies';

$gl_caption_element['c_element'] = 'Ítem';

$gl_caption['c_ordre'] = 'Ordre';
$gl_caption['c_tour_file_name'] = $GLOBALS['gl_caption']['c_file_name'];
$gl_caption['c_tour_old_file_name'] = $GLOBALS['gl_caption']['c_old_file_name'];

$gl_caption['c_category'] = "Categoria";
$gl_caption['c_category_id'] = "Categoria";
$gl_caption['c_category_subtitle'] = "Subtítol";
$gl_caption['c_new_button'] = "Categoria";


// TRADUIR
$gl_caption['c_includes'] = 'Includes';
$gl_caption['c_not_includes'] = 'Does not include';
$gl_caption['c_highlights'] = "Tour highlights";
$gl_caption['c_bites'] = "Bites & Tastings";
$gl_caption['c_spoken_language'] = "Idioma";
$gl_caption['c_spoken_language_eng'] = "English";
$gl_caption['c_spoken_language_cat'] = "Catalan";
$gl_caption['c_spoken_language_spa'] = "Spanish";
$gl_caption['c_spoken_language_fra'] = "French";
$gl_caption['c_spoken_language_dut'] = "Netherlands";
$gl_caption['c_duration'] = "Duration";
$gl_caption['c_place'] = "Lloc";
$gl_caption['c_timetable'] = "Horari";
$gl_caption['c_people'] = "Persones";
$gl_caption['c_is_new'] = "Nou";
$gl_caption['c_is_full'] = "Complet";

$gl_caption['c_tour_list_view_all'] = 'Voir tous';