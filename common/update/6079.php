<?

/* 21-6-16

	Afegeixo others a product ( amplio a 20 )
 */

if ( is_table( 'product__product' ) && ! is_field( 'product__product', 'others11' ) ) {

	$update_sql[] = "
					ALTER TABLE `product__product`
						ADD COLUMN `others20` TEXT NOT NULL DEFAULT '' AFTER `others10`,
						ADD COLUMN `others19` TEXT NOT NULL DEFAULT '' AFTER `others10`,
						ADD COLUMN `others18` TEXT NOT NULL DEFAULT '' AFTER `others10`,
						ADD COLUMN `others17` TEXT NOT NULL DEFAULT '' AFTER `others10`,
						ADD COLUMN `others16` TEXT NOT NULL DEFAULT '' AFTER `others10`,
						ADD COLUMN `others15` TEXT NOT NULL DEFAULT '' AFTER `others10`,
						ADD COLUMN `others14` TEXT NOT NULL DEFAULT '' AFTER `others10`,
						ADD COLUMN `others13` TEXT NOT NULL DEFAULT '' AFTER `others10`,
						ADD COLUMN `others12` TEXT NOT NULL DEFAULT '' AFTER `others10`,
						ADD COLUMN `others11` TEXT NOT NULL DEFAULT '' AFTER `others10`;";
	$update_sql[] = "
					ALTER TABLE `product__product_language`
						ADD COLUMN `others20` TEXT NOT NULL DEFAULT '' AFTER `others10`,
						ADD COLUMN `others19` TEXT NOT NULL DEFAULT '' AFTER `others10`,
						ADD COLUMN `others18` TEXT NOT NULL DEFAULT '' AFTER `others10`,
						ADD COLUMN `others17` TEXT NOT NULL DEFAULT '' AFTER `others10`,
						ADD COLUMN `others16` TEXT NOT NULL DEFAULT '' AFTER `others10`,
						ADD COLUMN `others15` TEXT NOT NULL DEFAULT '' AFTER `others10`,
						ADD COLUMN `others14` TEXT NOT NULL DEFAULT '' AFTER `others10`,
						ADD COLUMN `others13` TEXT NOT NULL DEFAULT '' AFTER `others10`,
						ADD COLUMN `others12` TEXT NOT NULL DEFAULT '' AFTER `others10`,
						ADD COLUMN `others11` TEXT NOT NULL DEFAULT '' AFTER `others10`;
					";

	for ( $i = 11; $i <= 20; $i ++ ) {
		$update_sql[] = "
			INSERT INTO `product__configadmin` (`name`)
			VALUES ('others" . $i . "');";
		$update_sql[] = "
			INSERT INTO `product__configadmin` (`name`,value)
			VALUES ('others" . $i . "_type','text_no_lang');";
	}


	global $gl_languages;

	foreach ( $gl_languages['names'] as $lang => $val ) {
		for ( $i = 11; $i <= 20; $i ++ ) {
			$update_sql[] = "
			INSERT INTO `product__configadmin_language` (`name`, `language`)
			VALUES ('others" . $i . "','" . $lang . "');";
		}
	}


} else {
	if ( is_table( 'product__product' ) ) {
		trigger_error( 'Ja existeixen el camp others11 a others20' );
	}
}

?>