<!DOCTYPE html	PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?=LANGUAGE_CODE?>" lang="<?=LANGUAGE_CODE?>">
<head>
<title>
<?=$page_title?>
</title>
<meta name="Description"
content="<?=$page_description?>" />
<!-- <?=$page_description?> -->
<meta name="Keywords"
content="<?=$page_keywords?>" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="<?=DIR_TEMPLATES?>styles/general.css" rel="stylesheet" type="text/css">
<script type="text/javascript">
<!--
var a_vars = Array();var pagename='<?=$title?>';var phpmyvisitesSite = 1;var phpmyvisitesURL = "<?=HOST_URL?>common/includes/phpmv2/phpmyvisites.php";
//-->
</script>
<script language="javascript" src="/common/includes/phpmv2/phpmyvisites.js" type="text/javascript"></script>
<script language="JavaScript" src="/public/jscripts/hide_m.js"></script>
<script type="text/javascript" src="/common/jscripts/mootools.js"></script>
<script type="text/javascript" src="/common/jscripts/modalbox.js"></script>
</head>
<body>
<div id="container">
<div id="logo"><img src="<?=CLIENT_DIR?>/images/logo_web.png" /></div>
<div id="header">
  <!--obro el header -->
  <div id="header_left"></div>
  <div id="header_right">
    <!--header_right -->
    <div id="header_right_text">
      <?if ($blocks_ht):?>
        <?foreach($blocks_ht as $b): extract ($b)?>
          <?=$block?>
          <?endforeach //$blocks?>
        <?endif //$blocks_ht?>
    </div>
    <div id="header_right_phone"><?=PAGE_PHONE?></div>
  </div>
  <!--header_right -->
</div>
<!--tenco el header-->
<div id="center">
  <div id="center_top_right"></div>
  <div id="center_top_left">
    <h1>
      <?=$page_slogan?>
    </h1>
  </div>
  <div id="center_menu_text">
    <?foreach($blocks_t as $b): extract ($b)?>
      <?=$block?>
      <?endforeach //$blocks?>
  </div>
  <div id="center_content">
    <!-- contingut -->
    <div id="center_content_left">
      <div id="center_content_left_search">
        <table border="0" cellspacing="0" cellpadding="0">
          <form action="/" method="get" name="form1" id="form1">
            <tr>
              <td><input name="tool" type="hidden" value="inmo">
                <input name="tool_section" type="hidden" value="property">
                <input name="action" type="hidden" value="list_records_search_search">
                <input name="q" type="text" size="20" class="search"></td>
              <td><input name="imageField" type="image" style="border:0;" src="<?=DIR_TEMPLATES?>images/boto_enviar.jpg" align="middle"></td>
            </tr>
          </form>
        </table>
      </div>
      <div id="center_content_left_top"></div>
      <div id="center_content_left_middle">
        <?if ($blocks_l):  ?>
          <?foreach($blocks_l as $b): extract ($b)?>
            <?=$block?>
            <?endforeach //$blocks  ?>
          <?endif //$blocks_l?>
      </div>
      <div id="center_content_left_bottom"></div>
    </div>
    <div id="center_content_right">
      <h2>
        <?=$title?>
      </h2>
      <div class="clearfloat"></div>
      <?=$content?>
    </div>
  </div>
  <!-- tenca contingut -->
</div>
<!-- fi del center -->
<div id="footer">
  <?=$page_address?>
  &nbsp;&nbsp;&nbsp;&nbsp;<a href="mailto:<?=$default_mail?>" class="links_generals">
  <?=$default_mail?>
  </a></div>
<div id="footer_des"><a  href="http://www.letnd.com" target="_blank"><img border="0" src="/common/images/creatper<?=LANGUAGE?>.jpg" width="138" height="22" alt="Immoletnd" /></a></div> 
</div>
</body>
</html>
