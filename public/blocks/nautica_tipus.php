<?

$vars['block_selected'] = 0;


$query = "SELECT tipus, nautica__tipus.tipus_id
				FROM nautica__tipus, nautica__tipus_language
				WHERE nautica__tipus_language.tipus_id = nautica__tipus.tipus_id
				AND language = '" . LANGUAGE . "'
				ORDER BY ordre ASC, tipus";

$results = Db::get_rows($query);
Debug::add('Query nautica tipus', $query);


$tipus_id = R::id('tipus_id');
$vars['block_selected'] = $tipus_id?1:0;
	
$conta = 0;
$loop_count = count($results);
foreach ($results as $rs) {

	$l = array();

	// CONTA VAIXELLS, es mes rapid que posant la consulta com a subquery
	$embarcacion_count = Db::get_first("SELECT count(*) 
						FROM nautica__embarcacion
						WHERE nautica__embarcacion.tipus_id = " . $rs['tipus_id'] . "
						AND (status = 'onsale' OR status = 'reserved' OR status = 'sold') 
						AND nautica__embarcacion.bin = 0");

	if ($embarcacion_count) {
		
		$l['link'] = Page::get_link('nautica', 'embarcacion', false, false, array(), false, '', $rs['tipus_id'], '');
		
		$l['id'] = $rs['tipus_id'];
		$l['item'] = $rs['tipus'];
		$l['embarcacion_count'] = $embarcacion_count;
		$l['class'] = '';
		$l['conta'] = $conta++;
		$l['loop_count'] = $loop_count;
		// cheched tant si vinc directe del filtre o per enllaç directe amb el tipus_id
		$checked = (isset($_GET['tipus_ids'][$l['id']]) || $tipus_id==$l['id'])?' checked':'';
		$l['checkbox'] = '<input name="tipus_ids['.$l['id'].']" value="'.$l['id'].'" type="checkbox" '.$checked.' />';

		$l['selected'] = $l['id']==$tipus_id?1:0;
		$loop[] = $l;
		
	}
}
if ($loop) {
	$loop[0]['class'] = 'first';
	$loop[count($loop) - 1]['class'] = 'last';
	// poso be el loop_count, pot ser que alguna familia no tingui productes i llavors falla
	if (count($loop) != $loop_count) {
		foreach ($loop as &$val) {
			$val['loop_count'] = count($loop);
		}
	}
	
} else {
	/* si no hi ha cap resultat no mostro el block */
	$show_block = false;
	return;
}
?>