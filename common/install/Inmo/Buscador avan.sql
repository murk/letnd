#
# <?= get_block( 'inmo_search' ) ?>
#
#
#



INSERT INTO `all__block` (`block_id`, `script`, `vars`, `template`, `active`, `place`, `ordre`, `solution`) VALUES ('inmo_search', 'inmo_tipus', '', 'inmo_search', 0, 't', 0, 'inmo');

INSERT INTO `all__block_language` (`block_id`, `language`, `title`, `description`) VALUES ('inmo_search', 'eng', 'Sell or Rent', '');
INSERT INTO `all__block_language` (`block_id`, `language`, `title`, `description`) VALUES ('inmo_search', 'fra', 'Vente ou Location', '');
INSERT INTO `all__block_language` (`block_id`, `language`, `title`, `description`) VALUES ('inmo_search', 'spa', 'Venta o Alquiler', '');
INSERT INTO `all__block_language` (`block_id`, `language`, `title`, `description`) VALUES ('inmo_search', 'cat', 'Venda o Lloguer', '');



INSERT INTO `all__block` (`block_id`, `script`, `vars`, `template`, `active`, `place`, `ordre`, `solution`) VALUES ('inmo_category_filter', 'inmo_category', 'tipus=auto', 'inmo_filtres', 0, 't', 0, 'inmo');
INSERT INTO `all__block_language` (`block_id`, `language`, `title`, `description`) VALUES ('inmo_category_filter', 'spa', 'Tipo', '');
INSERT INTO `all__block_language` (`block_id`, `language`, `title`, `description`) VALUES ('inmo_category_filter', 'fra', 'Sorte', '');
INSERT INTO `all__block_language` (`block_id`, `language`, `title`, `description`) VALUES ('inmo_category_filter', 'eng', 'Type', '');
INSERT INTO `all__block_language` (`block_id`, `language`, `title`, `description`) VALUES ('inmo_category_filter', 'cat', 'Tipus', '');


INSERT INTO `all__block` (`block_id`, `script`, `vars`, `template`, `active`, `place`, `ordre`, `solution`) VALUES ('inmo_municipi_filter', 'inmo_municipi', 'tipus=auto', 'inmo_filtres', 0, 't', 0, 'inmo');
INSERT INTO `all__block_language` (`block_id`, `language`, `title`, `description`) VALUES ('inmo_municipi_filter', 'spa', 'Municipio', '');
INSERT INTO `all__block_language` (`block_id`, `language`, `title`, `description`) VALUES ('inmo_municipi_filter', 'cat', 'Municipi', '');
INSERT INTO `all__block_language` (`block_id`, `language`, `title`, `description`) VALUES ('inmo_municipi_filter', 'fra', 'Ville', '');
INSERT INTO `all__block_language` (`block_id`, `language`, `title`, `description`) VALUES ('inmo_municipi_filter', 'eng', 'Town', '');


INSERT INTO `all__block` (`block_id`, `script`, `vars`, `template`, `active`, `place`, `ordre`, `solution`) VALUES ('inmo_price_filter', 'inmo_price', 'tipus=auto&bock_type=links', 'inmo_filtres', 0, 't', 0, 'inmo');INSERT INTO `all__block_language` (`block_id`, `language`, `title`, `description`) VALUES ('inmo_price_filter', 'spa', 'Precio', '');
INSERT INTO `all__block_language` (`block_id`, `language`, `title`, `description`) VALUES ('inmo_price_filter', 'cat', 'Preu', '');
INSERT INTO `all__block_language` (`block_id`, `language`, `title`, `description`) VALUES ('inmo_price_filter', 'fra', 'Prix', '');
INSERT INTO `all__block_language` (`block_id`, `language`, `title`, `description`) VALUES ('inmo_price_filter', 'eng', 'Price', '');


INSERT INTO `all__block` (`block_id`, `script`, `vars`, `template`, `active`, `place`, `ordre`, `solution`) VALUES ('inmo_room_filter', 'inmo_select', 'field=room&tipus=auto', 'inmo_filtres', 0, 't', 0, 'inmo');
INSERT INTO `all__block_language` (`block_id`, `language`, `title`, `description`) VALUES ('inmo_room_filter', 'spa', 'Habitaciones', '');
INSERT INTO `all__block_language` (`block_id`, `language`, `title`, `description`) VALUES ('inmo_room_filter', 'cat', 'Habitacions', '');
INSERT INTO `all__block_language` (`block_id`, `language`, `title`, `description`) VALUES ('inmo_room_filter', 'fra', 'Chambres', '');
INSERT INTO `all__block_language` (`block_id`, `language`, `title`, `description`) VALUES ('inmo_room_filter', 'eng', 'Rooms', '');





# NOMES PER VACACIONAL

INSERT INTO `all__block` (`block_id`, `script`, `vars`, `template`, `active`, `place`, `ordre`, `solution`) VALUES ('inmo_temp_search', 'inmo_temp_search', 'days_off=4&add_jscript=false', 'inmo_temp_search', 2, 'l', 30, 'inmo');


INSERT INTO `all__block_language` (`block_id`, `language`, `title`, `description`) VALUES ('inmo_temp_search', 'spa', '', '');
INSERT INTO `all__block_language` (`block_id`, `language`, `title`, `description`) VALUES ('inmo_temp_search', 'eng', '', '');
INSERT INTO `all__block_language` (`block_id`, `language`, `title`, `description`) VALUES ('inmo_temp_search', 'fra', '', '');
INSERT INTO `all__block_language` (`block_id`, `language`, `title`, `description`) VALUES ('inmo_temp_search', 'cat', '', '');