<script language="JavaScript" src="/admin/jscripts/validacio.js?v=<?=$version?>"></script>
<script language="JavaScript" src="/admin/modules/admintotal/jscripts/functions.js?v=<?=$version?>"></script>
<script src="/common/jscripts/codemirror-2.16/lib/codemirror.js"></script>
<link rel="stylesheet" href="/common/jscripts/codemirror-2.16/lib/codemirror.css">
    <script src="/common/jscripts/codemirror-2.16/mode/xml/xml.js"></script>
    <script src="/common/jscripts/codemirror-2.16/mode/javascript/javascript.js"></script>
    <script src="/common/jscripts/codemirror-2.16/mode/css/css.js"></script>
    <script src="/common/jscripts/codemirror-2.16/mode/clike/clike.js"></script>
<script src="/common/jscripts/codemirror-2.16/mode/php/php.js"></script>
<link rel="stylesheet" href="/common/jscripts/codemirror-2.16/theme/elegant.css">
<style>
.CodeMirror-scroll {
  height: auto;
  overflow-y: hidden;
  overflow-x: auto;
  width: 100%;  
}
.CodeMirror {
	border-top: 1px solid black; 
	border-bottom: 1px solid black;
}
</style>
<script language="JavaScript">

var myCodeMirror;

$(function(){
expand('idioma_general');
	myCodeMirror = CodeMirror.fromTextArea(document.theForm.cat_php,  {
        lineNumbers: true,
        matchBrackets: true,
        mode: "application/x-httpd-php",
        indentUnit: 8,
        indentWithTabs: true,
        enterMode: "keep",
        theme: "elegant",
        tabMode: "shift"
	});
})

function insertar(text_area, var_constants, text_constant_replace, var_captions, text_caption_replace){
	//contingut_cat = document.theForm[text_area].value;
	contingut_cat = myCodeMirror.getValue();
	var define_text = '';
	for (var_constant in var_constants)
	{
		define_text += "\ndefine('"+var_constants[var_constant]+"','"+document.theForm[var_constants[var_constant]].value+"');";
	}
	contingut_cat = contingut_cat.replace(text_constant_replace, text_constant_replace + define_text);
	var caption_text = '';
	for (var_caption in var_captions)
	{	
		var var_name = var_captions[var_caption]=='modules_list_languages_new_var_name'?'c_'+document.theForm['modules_list_languages_new_var_name'].value:var_captions[var_caption];
		var var_value = var_captions[var_caption]=='modules_list_languages_new_var_name'?document.theForm['modules_list_languages_new_var_value'].value:document.theForm[var_captions[var_caption]].value;

		caption_text += "\n$gl_caption_<?=$tool_section?>['"+var_name+"'] = '"+var_value+"';";
		if ( var_captions[var_caption]=='modules_list_languages_new_var_name'){
			document.theForm['modules_list_languages_new_var_name'].value = '';
			document.theForm['modules_list_languages_new_var_value'].value = '';
			document.theForm['modules_list_languages_new_var_name'].focus();
		}
	}
	contingut_cat = contingut_cat.replace(text_caption_replace, text_caption_replace + caption_text);
	document.theForm[text_area].value = contingut_cat;
	myCodeMirror.setValue(contingut_cat);
	myCodeMirror.focus();
}
function modificar(text_area, var_constants, var_captions){
	//contingut_cat = document.theForm[text_area].value;
	contingut_cat = myCodeMirror.getValue();
	for (var_constant in var_constants)
	{
		var var_name = var_constants[var_constant];
		var var_value = document.theForm[var_constants[var_constant]].value;
		var var_old_value = document.theForm['old_' + var_constants[var_constant]].value;
		var new_define_text1 = "define('" + var_name + "','" + var_value.replace('\'','\\\'') + "');";
		var new_define_text2 = "define(\"" + var_name + "\",\"" + var_value.replace('"','\\"') + "\");";
		var old_define_text1 = "define('" + var_name + "','" + var_old_value.replace('\'','\\\'') + "');";
		var old_define_text2 = "define(\"" + var_name + "\",\"" + var_old_value.replace('"','\\"') + "\");";
		contingut_cat = contingut_cat.replace(old_define_text1, new_define_text1);
		contingut_cat = contingut_cat.replace(old_define_text2, new_define_text2);
		document.theForm['old_' + var_constants[var_constant]].value = var_value;
		
	}
	document.theForm[text_area].value = contingut_cat;
	myCodeMirror.setValue(contingut_cat);
	myCodeMirror.focus();
}

function expand(nom_bloc){
	document.getElementById(nom_bloc).style.display=(document.getElementById(nom_bloc).style.display!="block")? "block" : "none"
}
</script>
<form name="theForm" method="post" target="save_frame" action="?action=save_rows_languages&menu_id=1006&action_previous_list=list_records_languages&tool2=<?=$tool?>&tool_section2=<?=$tool_section?>">

<table width="100%" border="0" cellspacing="0" cellpadding="0" class="form"> 
  <tr><td class="formCaptionTitle"><strong>Menu arrel &quot;
                      <?=$tool?>__<?=$tool_section?>&quot;</strong></td>
    </tr>
  <tr>
    <td>
    <table width="100%" cellpadding="4" cellspacing="0">
                <tr>
                  <td width="30%" valign="top">
                  
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td width="30%"><?if ($menu_root):?>

              <table width="100%" cellpadding="4" cellspacing="0" class="formstbl">
                <tr id="<?=$name?>config_select" style="display:<?=$dconfig_select?>;">
                  <td class="forms">
              <table border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td class="formsCaption">
                            <?=$menu_root?>
                            &nbsp;&nbsp;</td>
                        <td><input name="<?=$menu_root?>" type="text" value="" size="100"></td> </tr>
                      <tr>
                    </table></td>
                </tr>
                <tr>
                  <td valign="top" class="forms"><strong><a class="boto1" href="javascript:insertar('root_cat_php',['<?=$menu_root?>'],'// menus',[],'');">Insertar</a></strong></td>
                </tr>
                <tr>
                  <td height="3" valign="top" class="formsHor"></td>
                </tr>
              </table>
              <?endif //$menu_root?>

              <table width="100%" cellpadding="4" cellspacing="0">
                <tr>
                  <td class="formCaptionTitle"><strong><a href="javascript:expand('idioma_general')">Modificar variables idioma general</a></strong></td>
                </tr>
              </table>
				<!-- start bloc idioma general --><div id = "idioma_general" style="display:block;">
              <table width="100%" cellpadding="4" cellspacing="0" class="formstbl">
                <tr id="<?=$name?>config_select" style="display:<?=$dconfig_select?>;">
                  <td class="forms"><table border="0" cellspacing="0" cellpadding="0">
					<?foreach($loop_root_constant_edit as $l): extract ($l)?>
                      <tr>
                        <td class="formsCaption">
                            <?=$var_name?>
                            &nbsp;&nbsp;</td>
                        <td><input name="<?=$var_name?>" type="text" value="<?=$var_value?>" size="100"><input name="old_<?=$var_name?>" type="hidden" value="<?=$var_value?>"></td> </tr>
                      <tr>
					<?endforeach //$loop_root_constant_edit?>
                    </table></td>
                </tr>
                <tr>
                  <td valign="top" class="forms"><strong><a class="boto1" href="javascript:modificar('root_cat_php',[<?foreach($loop_root_constant_edit as $l): extract ($l)?>'<?=$var_name?>',<?endforeach //$loop?>],'');">Modificar</a></strong></td>
                </tr>
                <tr>
                  <td height="3" valign="top" class="formsHor"></td>
                </tr>
              </table></div>
				<!-- fi bloc idioma general -->
              <table width="100%" cellpadding="4" cellspacing="0">
                <tr>
                  <td class="formCaptionTitle"><strong>Menus i camps &quot;
                      <?=$tool?>__<?=$tool_section?>&quot;</strong></td>
                </tr>
              </table>
              <table width="100%" cellpadding="4" cellspacing="0" class="formstbl">
                <tr id="<?=$name?>config_select" style="display:<?=$dconfig_select?>;">
                  <td class="forms"><table border="0" cellspacing="0" cellpadding="0">
					<?foreach($loop as $l): extract ($l)?>
                      <tr>
                        <td class="formsCaption">
                            <?=$var_name?>
                            &nbsp;&nbsp;</td>
                        <td><input name="<?=$var_name?>" type="text" value="<?=$var_value?>" size="70"></td> </tr>
                      <tr>
					<?endforeach //$loop?>
					<?foreach($loop_caption as $l): extract ($l)?>
                      <tr>
                        <td class="formsCaption">
                            <?=$var_name?>
                            &nbsp;&nbsp;</td>
                        <td><input name="<?=$var_name?>" type="text" value="<?=$var_value?>" size="70"></td> </tr>
                      <tr>
					<?endforeach //$loop_caption?>
                    </table></td>
                </tr>
                <tr>
                  <td valign="top" class="forms"><strong><a class="boto1" href="javascript:insertar('cat_php',[<?foreach($loop as $l): extract ($l)?>'<?=$var_name?>',<?endforeach //$loop?>],'// menus eina',[<?foreach($loop_caption as $l): extract ($l)?>'<?=$var_name?>',<?endforeach //$loop_caption?>],'// captions seccio');">Insertar</a></strong></td>
                </tr>
                <tr>
                  <td height="3" valign="top" class="formsHor"></td>
                </tr>
              </table>


<table width="100%" cellpadding="4" cellspacing="0" class="formstbl">
                <tr>
                  <td class="forms"><table border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td class="formsCaption">
                            c_</td>
                        <td>
                            <input name="modules_list_languages_new_var_name" type="text" value="" size="30">
                            &nbsp;&nbsp;</td>
                        <td class="formsCaption">
                            =
                            &nbsp;&nbsp;</td>
                        <td><input name="modules_list_languages_new_var_value" type="text" value="" size="50"></td> </tr>
                      <tr>
                    </table></td>
                </tr>
                <tr>
                  <td valign="top" class="forms"><strong><a class="boto1" href="javascript:insertar('cat_php',[],'',['modules_list_languages_new_var_name'],'// captions seccio');">Insertar nova variable</a></strong></td>
                </tr>
                <tr>
                  <td height="3" valign="top" class="formsHor"></td>
                </tr>
              </table>




              <table width="100%" cellpadding="4" cellspacing="0">
                <tr>
                  <td class="formCaptionTitle"><strong>Modificar variables idioma &quot;
                  <?=$tool?>
                  &quot;</strong></td>
                </tr>
              </table>
              <table width="100%" cellpadding="4" cellspacing="0" class="formstbl">
                <tr id="<?=$name?>config_select" style="display:<?=$dconfig_select?>;">
                  <td class="forms"><table border="0" cellspacing="0" cellpadding="0">
					<?foreach($loop_constant_edit as $l): extract ($l)?>
                      <tr>
                        <td class="formsCaption">
                            <?=$var_name?>
                            &nbsp;&nbsp;</td>
                        <td><input name="<?=$var_name?>" type="text" value="<?=$var_value?>" size="50"><input name="old_<?=$var_name?>" type="hidden" value="<?=$var_value?>"></td> </tr>
                      <tr>
					<?endforeach //$loop_constant_edit?>
                    </table></td>
                </tr>
                <tr>
                  <td valign="top" class="forms"><strong><a class="boto1" href="javascript:modificar('cat_php',[<?foreach($loop_constant_edit as $l): extract ($l)?>'<?=$var_name?>',<?endforeach //$loop_constant_edit?>],'');">Modificar</a></strong></td>
                </tr>
                <tr>
                  <td height="3" valign="top" class="formsHor"></td>
                </tr>
              </table><? /* 
               <table width="100%" cellpadding="4" cellspacing="2" class="formsButtonstbl">
                 <tr>
                   <td align="center" class="formsButtons"><strong><a class="botoOpcions" href="javascript:cat_php.toggleEditor();root_cat_php.toggleEditor();document.theForm.submit();cat_php.toggleEditor();root_cat_php.toggleEditor();">Guardar</a></strong> </td>
                 </tr>
             </table> */ ?>
                
            </td>
          </tr>          
      </table></td>
    </tr>
</table>
                  
                  </td>
                  
                  
                  <td valign="top"><table width="100%" cellpadding="4" cellspacing="0">
                <tr>
                  <td class="formCaptionTitle"><strong>Arxiu idioma modul: cat.php</strong></td>
                </tr>
              </table>
              <table width="100%" cellpadding="4" cellspacing="0">
                <tr>
                  <td> <textarea name="cat_php" id="cat_php" style="width:100%;height:500px;"><?=$cat_php?>
                  </textarea></td>
                </tr>
            </table></td>
               </tr>
            </table>
    
    
    </td>
    </tr>
</table>
</form>
<?if ($menu_root):?>
<script language="JavaScript1.2" type="text/javascript">
expand('idioma_general');
</script>
<?endif //$menu_root?>
