<?
/**
 * * Gestiona connexions bd
 */

class Db {
	static $debug = false;
	static $is_mother_connected;
	private static $are_errors_enabled = true;
	public static $query = ''; // per consultar el query que s'ha fet a insert o update o get
    /**
     * Db::execute()
     * Executa varis sql statement alhora, només per inserts, updates, deletes
     * @param mixed $query
     * @return void
     */
    static public function multi_query($query)
    {
        $cn = Db::cn();
	    Debug::startQuery( $query );

	    mysqli_multi_query( $cn, $query ) or Db::error($query);
	    do {
			if ( $result = mysqli_store_result( $cn ) ) mysqli_free_result( $result );
		    if (!mysqli_more_results( $cn )) break;
		} while ( mysqli_next_result( $cn ) );

	    Debug::stopQuery();
    }
    /**
     * Db::execute()
     * Executa un sql statement
     * @param mixed $query
     * @return void
     */
    static public function execute($query)
    {
        $cn = Db::cn();
	    Debug::startQuery( $query );
	    mysqli_query( $cn, $query ) or Db::error($query);
	    Debug::stopQuery();
    }
    static public function get_affected_rows (){
        $cn = Db::cn();
    	return mysqli_affected_rows( $cn );
    }

    private static function put_condition_vars(&$query, ...$condition_vars){
    	if ( $condition_vars ) {
		    foreach ( $condition_vars as &$condition_var ) {
			    $condition_var = Db::qstr( $condition_var );
		    }
		    $query = sprintf( $query, ...$condition_vars );
	    }
    }

    /**
     * Db::get()
     *
     * @param mixed $query
     * @return array associatiu de resultats
     */
    static function get($query, ...$condition_vars){
		$cn  = Db::cn();
	    $arr = [];
	    if ( ! $query ) return $arr;// posat per php5 a /var/www/letnd.com/datos/web/admin/modules/custumer/custumer_demand_functions.php linea: 136

	    Db::put_condition_vars( $query, ...$condition_vars );

	    Debug::startQuery( $query );
	    $result = mysqli_query( $cn, $query ) or Db::error( $query );
	    while ( $fila = mysqli_fetch_assoc( $result ) ) {
		    $arr[] = $fila;
	    }
	    mysqli_free_result( $result );
	    Debug::stopQuery();
	    self::$query = $query;

	    return $arr;
    }

	/**
	 *  Alias de {@link Db::get}
	 *
	 * @param mixed $query
	 * @param array $condition_vars
	 * <p>Variables per escapar i posar a condition</p>
	 * get ('*',  camp1=% AND camp2=% )
	 *
	 * @return array associatiu de resultats
	 */
    static public function get_rows($query, ...$condition_vars) {
	    return Db::get($query, ...$condition_vars);
    }

	/**
	 * Db::get_rows_array()
	 *
	 * @param mixed $query
	 * @param array $condition_vars
	 * <p>Variables per escapar i posar a condition</p>
	 * get ('*',  camp1=% AND camp2=% )
	 *
	 * @return array de resultats
	 */
	static public function get_rows_array($query, ...$condition_vars)
	{
		$cn = Db::cn();
		$arr = array();
		if (!$query) return $arr; // posat per php5 a /var/www/letnd.com/datos/web/admin/modules/custumer/custumer_demand_functions.php linea: 136

	    Db::put_condition_vars( $query, ...$condition_vars );

	    Debug::startQuery( $query );
		$result = mysqli_query( $cn, $query ) or Db::error($query);
		while ($fila = mysqli_fetch_row($result)) {
			$arr[] = $fila;
		}
		mysqli_free_result($result);
	    Debug::stopQuery();
	    self::$query = $query;
		return $arr;
	}

    /**
     * Db::get_row()
     *
     * @param mixed $query
	 * @param array $condition_vars
	 * <p>Variables per escapar i posar a condition</p>
	 * get ('*',  camp1=% AND camp2=% )
	 * @return array la primera fila de resultat
	 * <p>Molt util per exemple en una consulta tipus: SELECT preu, nom, altres FROM preus WHERE id=5,
	 * retorna nomes la primera fila ja que sabem que només hi haurà un resultat</p>
     */
    static public function get_row($query, ...$condition_vars)
    {
        $cn = Db::cn();

		if (!$query) return [];
	    Db::put_condition_vars( $query, ...$condition_vars );

	    Debug::startQuery( $query );
	    $result = mysqli_query( $cn, $query ) or Db::error($query);
        $row = mysqli_fetch_assoc($result);
        mysqli_free_result($result);
	    Debug::stopQuery( );
	    self::$query = $query;
        return $row;
    }

    /**
     * Db::get_first()
     *
     * @param string $query
	 * @param array $condition_vars
	 * <p>Variables per escapar i posar a condition</p>
	 * get ('*',  camp1=% AND camp2=% )
     * @return string El valor del primer camp del primer resultat de la consulta o  false si no hi ha registre
     * <p>Molt util per exemple en una consulta tipus: SELECT preu FROM preus WHERE id=5,
     * retorna el preu, no fa falta ni les files, ni els camps</p>
     */
    static public function get_first($query, ...$condition_vars)
    {
        $cn = Db::cn();

		$row = false;
	    Db::put_condition_vars( $query, ...$condition_vars );

	    Debug::startQuery( $query );
	    $result = mysqli_query( $cn, $query ) or Db::error($query);
		if ($result) {
			$row = mysqli_fetch_array($result);
			mysqli_free_result($result);
		}
	    Debug::stopQuery();
	    self::$query = $query;
        return $row?$row[0]:false;
    }

	/**
	 * @param $table
	 * @param $row
	 * <p>Array de valors per insertar</p>
	 * <p>Si row es multidimensional amb el mateix format que get_rows, inserta tots els registres</p>
	 *
	 */
	public static function insert( $table, $row ) {

		if (empty($row)) {
			trigger_error( 'No es pot fer insert sense valors', E_USER_ERROR );
		}

		$rows         = empty( $row[0] ) ? [ $row ] : $row;
		$whole_values = '';
		foreach ( $rows as $row ) {
			$fields = '`';
			$values = '';
			foreach ( $row as $key => $r ) {
				$fields .= "$key`,`";

				if ( $r != 'now()' ) {
					$r = DB::qstr( $r );
				}
				$values .= "$r,";
			}
			$fields       = substr( $fields, 0, - 2 );
			$values       = substr( $values, 0, - 1 );
			$whole_values .= "($values),";

		}
		$whole_values = substr( $whole_values, 0, - 1 );

		$query = "INSERT INTO $table
					($fields) 
					VALUES $whole_values";


		Db::execute( $query );
	    self::$query = $query;
	}

	/**
	 * @param $table
	 * @param array $row Pars nom-valor per actualitzar al registre. Exclou automàticament el id_field i el posa com a condició al update, per tant es pot passar a l'array o posar-ho al paràmetre $condition, preferentment si hi ha més d'una condició
	 * @param string $condition Si hi ha condició no agafa automàticament la condició del id_field
	 *
	 * @param array $condition_vars
	 * <p>Variables per escapar i posar a condition</p>
	 * get ('*',  camp1=% AND camp2=% )
	 *
	 */
	public static function update( $table, array $row, $condition = '', ...$condition_vars ) {

		$values = '';

		if ( $condition ) {

			if ( $condition_vars ) {
				foreach ( $condition_vars as &$condition_var ) {
					$condition_var = Db::qstr( $condition_var );
				}
				$condition = sprintf( $condition, ...$condition_vars );
			}
		}

		foreach ( $row as $key => $r ) {
			if ( $r != 'now()' ) {
				$r = DB::qstr( $r );
			}
			$values .= "$key=$r,";
		}
		$values = substr( $values, 0, - 1 );

		if ( ! $condition ) {
			trigger_error( 'No es pot fer update sense condició', E_USER_ERROR );
		}

		$query = "UPDATE $table 
			SET $values
			WHERE $condition";

		Db::execute( $query );
	    self::$query = $query;

	}

	/**
	 * Static var
	 * Guarda variable de connexió bd
	 * Es la manera de proporcionar una propietat estàtica en php4
	 * Crear una funció interna que tingui el valor estàtic, per no carregar tant aquesta ja que sempre es crida desde qualsevol funció de la clase
	 * @access protected
	 *
	 * @param bool $new_cn
	 *
	 * @return bool|mysqli|string
	 */
    static public function cn($new_cn=false)
    {
        static $cn = '';
    	if ($new_cn) {
    		$cn = $new_cn;
    	}
        elseif (!$cn) {
        	$cn = Db::connect(false);
		}
		//Debug::add('DB','llegeix cn');
        return $cn;
	}
	/**
	 * Connecta a la bd dels paràmetres
	 * Connecta a la bd general quan no hi ha paràmetres i es crida internament
	 * Re-connecta a la bd general quan no hi ha paràmetres
	 *
	 */
	static public function connect($db_host='', $db_user_name='', $db_password='', $db_name=''){
		//print_r(debug_backtrace(0));
		if ($db_host) {

			$cn = mysqli_connect($db_host, $db_user_name, $db_password) or Db::error('',true, true);
			Debug::info('DB',"Conecta o cambia cn a $db_host : $db_name");
			if (!mysqli_select_db( $cn, $db_name )) Db::kill($db_name);
			mysqli_set_charset( $cn, 'utf8');
			Db::cn($cn); // cambio el valor del cn static

		}
		else {
			global $configuration;
			$init_conn = ($db_host===false);
			$db_host = $configuration['dbhost'];
			$db_name = $configuration['dbname'];
			$db_user_name = $configuration['dbuname'];
			$db_password = $configuration['dbpass'];
			$db_port = $configuration['dbport'];
			$cn = mysqli_connect($db_host, $db_user_name, $db_password, '', $db_port) or Db::error('',true, true);
			if (!mysqli_select_db( $cn, $db_name )) Db::kill($db_name);
			$init_conn?Debug::info('DB','autocrea cn'):Debug::info('DB','crea o reconnecta cn');
			mysqli_set_charset( $cn, 'utf8');
			if ($init_conn) return $cn; // per autocrear connexio desde el metode cn()
			else Db::cn($cn); // cambio el valor del cn static
		}
		Db::p($db_host, $db_name);

		self::$is_mother_connected = false;
	}
	// mantinc aquesta funcio així se que el que vull fer es reconnectar
	// per que havia connectat a alguna altra base
	static public function reconnect()
	{
		Db::connect();
	}
	/**
	 * Conecta a la BBDD mare
	 * Usage: Db::Db::connect_mother();
	 *
	 * @access public
	 */
	static public function connect_mother()
	{
		// Get database parameters
		global $configuration;
		$dbhost = $configuration['db_host_mother'];
		$dbname = $configuration['db_name_mother'];
		$dbuname = $configuration['db_user_name_mother'];
		$dbpass = $configuration['db_password_mother'];
		// Start connection
		Db::connect($dbhost, $dbuname, $dbpass, $dbname);
		self::$is_mother_connected = true;
	}


	/**
	 * Id de l'últim registre insertat
	 * Usage: Db::insert_id();
	 *
	 * @access public
	 */

	static public function insert_id()
	{
		return Db::get_first("SELECT LAST_INSERT_ID()");
		//return mysqli_insert_id(Db::cn()); a 5.3 peta sino alguna altra consulta entre mig, encara que sigui un select
		// dona problemes amb BIGINT auto_increment, de moment no en tenim cap
	}

	/**
	 * Posa el text entre quotes per insertar directament a BBDD i fa un Trim
	 *
	 * @param string string La cadena de text per insertar
	 *
	 * @return  string Single-quoted string IE: 'Haven\'t a clue.'
	 */
	static public function qstr($string)
	{
		return "'" . mysqli_real_escape_string( Db::cn(), $string ) . "'";
		// TODO-i return "'" . mysqli_real_escape_string( Db::cn(), trim ($string) ) . "'"; POSAR EL TRIM i trure¡l de CONTACT
	}

	/**
	 * Per desabilitar error fatal en una consulta, només s'ha d'utilitzar per quan hi ha un error controlat
	 * tornar a habilitar seguidament
	 *
	 * <code>
		Db::disable_errors();
		$position = Db::get_first($query);
		Db::enable_errors();
	 * </code>
	 *
	 */
	static public function enable_errors(){
		self::$are_errors_enabled = true;
	}
	static public function disable_errors(){
		self::$are_errors_enabled = false;
	}

	/**
	 * Db::error()
	 * Serveix per saber des d'on hem cridat aquesta classe quan s'ha produït un error
	 * ( adoDb no ho feia i era un puto rotllo trobar d'on venia l'error)
	 *
	 * @param string $query
	 * @param bool $show_errors
	 * @param bool $is_connect
	 *
	 * @return bool
	 * @internal param bool $error
	 */
	static public function error($query='', $show_errors = true, $is_connect = false){
		/*$errs = debug_backtrace(0);
		$err_text = '';
		unset($errs[0]);
		$err_text .= '<br><b style="color:red">Sentencia:</b> ' . $query;
		foreach ($errs as $err){
			extract($err);
			$err_text .= '<br><b style="color:red">Provinent de</b>: ' . $file . ' <b>linea</b>: ' . $line;
		}
		*/
		if (!$show_errors || !self::$are_errors_enabled) {

				/*Debug::log_error(
						'prevnext',
					    mysqli_errno(Db::cn()),
						mysqli_error(Db::cn())
					);*/
			return false;
		}
		$error_text = '';

		if ($is_connect) {
			$error_text = mysqli_connect_error();
		}
		else {
			$error_text = mysqli_error( Db::cn() );
		}

		if (DEBUG) {
			echo '<br><b>' . $error_text . '</b>';
			if ( ! $is_connect ) {
				echo '<br><b style="color:red">Sentencia:</b> ' . $query . '<br>';
			}
			Debug::add( $error_text, $query );
		}

	    Debug::stopQuery();

		trigger_error($error_text . "\n \n $query", E_USER_ERROR);
		return true;
	}

	/**
	 * Db::kill()
	 * Si no existeix la bbdd i es local es que estic instalant
	 * Si no existeix la bbdd i es public fa un die i envia un mail ( pels putos hackers que borren BBDD
	 * @return
	 */
	static public function kill($db_name){	
        
        if ($GLOBALS['gl_is_local']) {
        	$GLOBALS['gl_site_part'] = 'admin';
			Main::load_files( true );
			include (DOCUMENT_ROOT . '/admin/modules/setup/setup_install.php');
			SetupInstall::init();
			html_end();
		}
		else{
		
			$body = '<b style="color:#900">URL:</b> ' . HOST_URL . '<br><br><br>' . "Hi ha un problema amb la BBDD: $db_name, no existeix aquesta base de dades i no es pot conectar";	
			
			send_mail_admintotal("Letnd - No existeix la BBDD : " . $db_name,$body);

			Debug::p('', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NO HI HA BBDD!!!!!',false,true,'1');
			html_end();
			
		}
	}
	static public function p($output, $message = ''){
		
		if (class_exists('debug')){
			if (Db::$debug) 
			{
				Debug::p($output, $message);			
			}
		}
	}
}