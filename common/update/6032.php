<?

/* 25-2-15

Actualització productes, promcode

*/

if ( is_table( 'product__promcode' ) ) {

	$update_sql .= "
		ALTER TABLE `product__promcode`
			ADD COLUMN `discount_pvd` DECIMAL(11,2) NOT NULL DEFAULT '0.00' AFTER `discount_pvp`,
			ADD COLUMN `discount_pvd2` DECIMAL(11,2) NOT NULL DEFAULT '0.00' AFTER `discount_pvd`,
			ADD COLUMN `discount_pvd3` DECIMAL(11,2) NOT NULL DEFAULT '0.00' AFTER `discount_pvd2`,
			ADD COLUMN `discount_pvd4` DECIMAL(11,2) NOT NULL DEFAULT '0.00' AFTER `discount_pvd3`,
			ADD COLUMN `discount_pvd5` DECIMAL(11,2) NOT NULL DEFAULT '0.00' AFTER `discount_pvd4`;
	";
} 
?>