<?
if (!defined('NEWSLETTER_ADDRESSES_MESSAGE_ADDED')){
define('NEWSLETTER_ADDRESSES_MESSAGE_ADDED','Gràcies per inscriure\'t a la nostra llista');
define('NEWSLETTER_ADDRESSES_MESSAGE_ADD_EXISTS','Aquesta adreça de mail ja figura a la base de dades!');
}


$gl_caption['c_title_edit'] = 'Dades personals';
$gl_caption['c_title_register'] = 'Registre newsletter';
$gl_caption['c_title_login'] = 'Login newsletter';

$gl_caption ['c_register']='Registrarse';

$gl_caption_customer ['c_mail_log']='Introduzca su e-mail';
$gl_caption_customer ['c_password_log']='Introduzca su contraseña';

$gl_caption['c_password_forgotten'] = '¿Ha olvidado su contraseña?';
$gl_caption['c_password_insert_mail'] = 'Por favor, introduzca su dirección de email y le enviaremos una contraseña nueva a su cuenta de correo.';
$gl_caption['c_password_mail_error'] = 'La dirección de email proporcionada no existe en nuestra base de datos, por favor, escriba la dirección correctamente';
$gl_caption['c_password_sent'] = 'Se ha enviado una nueva contraseña a su dirección de email';
$gl_caption['c_password_mail_subject'] = 'Su nueva contraseña';
$gl_caption['c_password_mail_text_1'] = 'Su nueva contraseña es';
$gl_caption['c_password_mail_text_2'] = 'Puede cambiar la contraseña en nuestra página web';

$gl_messages_customer['registered_succesful']="El registro se ha efectuado correctamente.<br><br>En breve le notificaremos por email la activación de su cuenta";
$gl_caption_customer['c_subject'] = 'Nuevo cliente registrado';
$gl_caption_customer['c_body_1'] = 'Se ha registrado un nuevo cliente en la newsletter';


$gl_caption_remove['c_admin_subject'] = 'Solicitud de baja de la lista de distribución';
$gl_caption_remove['c_admin_body'] = '<br />El cliente <strong>%s %s</strong> con la dirección de correo <strong>%s</strong> se ha dado de baja de la lista de distribución.<br /><br />Puede ver la lista de todos los clientes dados de baja en: <a href="%s/admin/?menu_id=17">%sadmin/?menu_id=17</a>';


$gl_caption_remove['c_remove_confirm_1'] = 'Se dispone a solicitar la baja de nuestra lista para la siguiente dirección de correo electrónico:';
$gl_caption_remove['c_remove_confirm_2'] = 'Recuerde que esta acción no se puede deshacer. Si desea confirmar la anulación de su subscripción a la lista de publicidad, por favor clique en el botón:';
$gl_caption_remove['c_remove_confirm_button'] = 'Confirmar';

$gl_caption_remove['c_remove_confirmed_1'] = 'Su suscripción a nuestra lista de correo electrónico ha sido anulada.';
$gl_caption_remove['c_remove_confirmed_2'] = 'Esperamos retomar el contacto con usted muy pronto.';


$gl_messages['added']=NEWSLETTER_ADDRESSES_MESSAGE_ADDED;
$gl_messages['add_exists']=NEWSLETTER_ADDRESSES_MESSAGE_ADD_EXISTS;


$text_de_una_atra_web = "Gracias por tu insripción

Para asegurarte de recibir todos nuestros correos electrónicos, te aconsejamos añadir
<a>newsletter@news.easyviajar.com </a>
a tu lista de contactos.


Primero recibirás un correo de bienvenida, y después, nuestros boletines:

Puedes gestionar tus abonos en todo momento, suspenderlos o desabonarte siguiendo el enlace que encontrarás en todos nuestros boletines.";
?>