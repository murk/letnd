<?

/* 20-3-13 
  Actualitzacio tecnomix modul SEO



 */
if (is_table("tecnomix__new")){	
	$update_sql .="
ALTER TABLE `tecnomix__new_language`
	ADD COLUMN `page_title` VARCHAR(255) NOT NULL DEFAULT '' AFTER `url2_name`,
	ADD COLUMN `page_description` VARCHAR(255) NOT NULL DEFAULT '' AFTER `page_title`,
	ADD COLUMN `page_keywords` VARCHAR(255) NOT NULL DEFAULT '' AFTER `page_description`,
	ADD COLUMN `new_file_name` VARCHAR(100) NOT NULL DEFAULT '' AFTER `page_keywords`,
	ADD COLUMN `content_list` TEXT NOT NULL COLLATE 'utf8_spanish2_ci' AFTER `subtitle`,
	ADD COLUMN `new_old_file_name` VARCHAR(100) NOT NULL DEFAULT '' AFTER `new_file_name`;";
}

?>