<?

/* 29-7-16

	PRODUCT

	UP-DATE
	- Afegir specs a product
	- Sense permisos, només per clients nous i si fa falta

 */
if ( is_table( 'product__product' ) ) {

	$update_sql .= "UPDATE `all__menu` SET `ordre`=5 WHERE  `menu_id`=20080;";
	$update_sql .= "UPDATE `all__menu` SET `ordre`=6 WHERE  `menu_id`=20070;";
	$update_sql .= "UPDATE `all__menu` SET `ordre`=4 WHERE  `menu_id`=20060;";
	$update_sql .= "UPDATE `all__menu` SET `ordre`=10 WHERE  `menu_id`=20050;";
	$update_sql .= "UPDATE `all__menu` SET `ordre`=9 WHERE  `menu_id`=20040;";
	$update_sql .= "UPDATE `all__menu` SET `ordre`=8 WHERE  `menu_id`=20030;";
	$update_sql .= "UPDATE `all__menu` SET `ordre`=7 WHERE  `menu_id`=20020;";

	if ( ! Db::get_first( "SELECT count(*) FROM all__menu WHERE menu_id = '20090'" ) ) {

		$update_sql .="
INSERT INTO `all__menu` (`menu_id`, `menu_group`, `tool`, `tool_section`, `action`, `parent_id`, `toolmode_id`, `variable`, `ordre`, `link`, `process`, `bin`, `read1`, `write1`) VALUES
(20090, 'product', 'product', 'spec', '', 20000, 1, 'PRODUCT_MENU_SPEC', 3, '', '', 0, ',1,', ',1,'),
(20091, 'product', 'product', 'spec', 'show_form_new', 20090, 1, 'PRODUCT_MENU_SPEC_NEW', 1, '', '', 0, ',1,', ',1,'),
(20092, 'product', 'product', 'spec', 'list_records', 20090, 1, 'PRODUCT_MENU_SPEC_LIST', 2, '', '', 0, ',1,', ',1,'),
(20093, 'product', 'product', 'spec', 'list_records_bin', 20090, 1, 'PRODUCT_MENU_SPEC_LIST_BIN', 5, '', '', 0, ',1,', ',1,'),
(20094, 'product', 'product', 'spec_category', 'show_form_new', 20090, 1, 'PRODUCT_MENU_SPEC_CATEGORY_NEW', 3, '', '', 0, ',1,', ',1,'),
(20095, 'product', 'product', 'spec_category', 'list_records', 20090, 1, 'PRODUCT_MENU_SPEC_CATEGORY_LIST', 4, '', '', 0, ',1,', ',1,');";


	} else {
		trigger_error( 'Ja existeixen els menus  ' );
	}

}

if ( is_table( 'product__configadmin' ) && ! Db::get_first( "SELECT * FROM product__configadmin WHERE name='is_specs_on'" ) ) {
	$update_sql .= "
		INSERT INTO product__configadmin (`name`, `value`) VALUES ('is_specs_on', '0');";

} else {
	if ( is_table( 'product__configadmin' ) ) {
		trigger_error( 'Ja existeixen el camp is_specs_on' );
	}
}

?>