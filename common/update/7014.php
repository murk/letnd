<?

/* 10-5-17
  Actualitzacions banner imatges públiques per defecte

 */

if ( is_table( 'web__banner_file' ) ) {

	$update_sql[] = "ALTER TABLE `web__banner_file`
	CHANGE COLUMN `public` `public` TINYINT(1) NOT NULL DEFAULT '1' AFTER `filecat_id`;";

} else {
	if ( is_table( 'web__banner_file' ) ) {
		trigger_error( 'No ha ha la taula web__banner_file' );
	}
}
?>