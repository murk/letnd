<script type="text/javascript" src="/common/jscripts/prism/prism.js"></script>
<link href="/common/jscripts/prism/prism.css" rel="stylesheet" type="text/css">
<link href="/admin/themes/inmotools/styles/utility.css" rel="stylesheet" type="text/css">

<pre>

	Activar client
	---------------
	Comprobar si hi ha immobles ( introduir api_key )

		Si hi ha immobles:
			Comentar-ho al client abans de continuar, ja que s'esborraràn tots

		Si no hi ha cap immoble:
			Posar les variables a client__client
			Assegurar-se que la url principal sigui la primera: ej. www.easybrava.com i no easybrava.cat ( camp 'domain' )
			S'esborrern tots els importats de d'un altre canal


</pre>

<form class="form" action="/admin/?menu_id=1084" method="post">
	<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
		<tr>
			<td class="listDetails">
				<table width="100%" cellpadding="0" cellspacing="0">
					<tr>
						<td class="listItem"><h6>Fotocasa api_key</h6></td>
						<td class="listItem">
								<input style="width: 800px" class="wide" type="text" name="api_key" value="<?= $api_key ?>"> <input type="submit" value="ok">
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</form>

<?= $immobles ?>