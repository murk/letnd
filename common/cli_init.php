<?
/*
 * Arxiu per executar php desde command line, com ara generar arxius exportació immobles
 */
if ( php_sapi_name() != 'cli' ) return;
$gl_is_gli       = true;
$gl_is_local     = false;
$gl_is_linux     = ! is_dir( 'c:\\' );
$gl_site_part    = 'admin';
$gl_is_admin     = true;
$gl_language     = 'cat';
$gl_is_letnd = true;
$gl_version      = 10000000; // m'asseguro
$gl_is_mobile    = false;
$gl_page         = new stdClass();
$gl_caption      = $gl_caption_list = $gl_caption_image = $gl_config = $gl_messages = [];

define( "PATH_TEMPLATES", '' );
define( "CLIENT_PATH", '' );
define( "VERSION", ( version_compare( PHP_VERSION, '7.0.0' ) >= 0 ) ? 7 : (( version_compare( PHP_VERSION, '5.0.0' ) >= 0 ) ? 5 : 4) );
define( 'LANGUAGE', 'cat' );
define( 'LANGUAGE_CODE', 'ca' );
define( 'CLIENT_DIR', '' );

if ( is_dir( 'D:/letnd' ) ) {
	// Defineixo host url per que agafi is_local be
	define( 'HOST_URL', 'http://w.letnd/' );
	$_SERVER["HTTP_HOST"] = 'w.letnd';
	define( "DOCUMENT_ROOT", "d:/letnd/" );
}
elseif ( is_dir( '/php_includes' ) ) {
	// Vagrant
	define( 'HOST_URL', 'http://w.letnd/' );
	$_SERVER["HTTP_HOST"] = 'w.letnd';
	define( "DOCUMENT_ROOT", "/letnd/" );
}
elseif ( strpos( $_SERVER['SCRIPT_NAME'], '/webbeta/' ) ) {
	define( 'HOST_URL', 'http://www.letnd.com/' );
	$_SERVER["HTTP_HOST"] = 'www.letnd.com';
	define( "DOCUMENT_ROOT", "/var/www/letnd.com/datos/webbeta/" );
	set_include_path( '/var/www/letnd.com/datos/includes' );
}
else {
	define( 'HOST_URL', 'http://www.letnd.com/' );
	$_SERVER["HTTP_HOST"] = 'www.letnd.com';
	define( "DOCUMENT_ROOT", "/var/www/letnd.com/datos/motor/" );
	set_include_path( '/var/www/letnd.com/datos/includes' );
}

// Comença a contar
ini_set( 'date.timezone', 'Europe/Madrid' );//date_default_timezone_set('Europe/Madrid'); PHP5
define( 'DOCUMENT_TIME_START', microtime() );


include( 'config_db_letnd.php' ); // carrego per la configuració del mail

include( DOCUMENT_ROOT . 'common/classes/debug_cli.php' );
include( DOCUMENT_ROOT . 'common/classes/request.php' );

Debug::start(); // començo el debug si s'escau

require_once( DOCUMENT_ROOT . 'common/vendor/autoload.php' );
include( DOCUMENT_ROOT . 'common/classes/module_base.php' );
include( DOCUMENT_ROOT . 'common/classes/module.php' );
include( DOCUMENT_ROOT . 'common/classes/db.php' );
include( DOCUMENT_ROOT . 'common/classes/db_query.php' );
include( DOCUMENT_ROOT . 'common/classes/thumb.php' );
include( DOCUMENT_ROOT . 'common/functions.php' );

include( 'functions_letnd.php' );
include( 'config_mls.php' ); // usuari amb permís per llegir a totes les bbdd