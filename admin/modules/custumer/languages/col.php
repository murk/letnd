<?php
// menus eina
if (!defined('CUSTUMER_MENU_CUSTUMER')){
	define('CUSTUMER_MENU_CUSTUMER','Gestión de clientes');
	define('CUSTUMER_MENU_NEW','Entrar nuevo cliente');
	define('CUSTUMER_MENU_LIST','Listar todos los clientes');
	define('CUSTUMER_MENU_LIST_OWNERS','Propietarios venta');
	define('CUSTUMER_MENU_LIST_RENTERS','Solicitantes alquiler');
	define('CUSTUMER_MENU_LIST_BUYERS','Solicitantes compra');
	define('CUSTUMER_MENU_LIST_TENANTS','Propietarios alquiler');
	define('CUSTUMER_MENU_LIST_BOOKERS','Clientes turístico');
	define('CUSTUMER_MENU_BIN','Papelera de reciclage');
	define('CUSTUMER_MENU_','Solicitudes de clientes');
	define('CUSTUMER_MENU_DEMAND','Listar solicitudes');
	define('CUSTUMER_MENU_DEMAND_LIST','Listar solicitudes');
	define('CUSTUMER_MENU_DEMAND_CHECK','Búsquedas exactas');
	define('CUSTUMER_MENU_DEMAND_PROX','Búsquedas proximativas');
	define('CUSTUMER_SELECT_WINDOW_TILE','Seleccionar propiedades');
	define('CUSTUMER_MENU_CUSTUMERLIST','Listar clientes');


	define('MLS_MENU_COMPANIES','MLS');
	define('MLS_MENU_COMPANIES_LIST','Listado empresas');
	define('MLS_MENU_COMPANIES_YES','Empresas compartidas');
}

//captions custumer_buy.tpl
$gl_caption_custumer['c_insert'] ='Tipos de propiedades';
$gl_caption_custumer['c_tipus'] ='Tipos de propiedades';
$gl_caption ['c_maxprice'] ='Precio máximo';
//captions tpl what.tpl
$gl_caption_custumer['c_prefered_language'] = 'Idioma';
$gl_caption_custumer['c_add'] = 'Alta propiedad';
$gl_caption_custumer['c_add_expand'] = '( El cliente pone en venda una propiedad.) ';
$gl_caption_custumer['c_assign'] = 'Assignar propiedad';
$gl_caption_custumer['c_assign_expand'] = '( Se assigna una propiedad existente a este cliente.) ';
$gl_caption_custumer['c_buy'] = 'Solicitud de compra';
$gl_caption_custumer['c_buy_expand'] = ' Asignar una solicitud de compra. Letnd buscara periòdicament coincidenciens amb les propietats actuals amb les sol·licituds de compra incluint les propietats MLS ) ';
$gl_caption_custumer['c_custumer'] = 'Cliente';
$gl_caption_custumer['c_insert']= 'Insertar';
//captions tpl search_custumer.tpl
$gl_caption_custumer['c_search_title'] = 'Buscar cliente';
$gl_caption_custumer['c_by_nif']= 'Por nif ';
$gl_caption_custumer['c_by_words']= 'Por palabras' ;
//custumer_selected_frame.tpl
$gl_caption_custumer['c_by_ref'] = '(o referencias separades por espacios, ej: "456 245 45")';
$gl_caption_custumer['c_by_words'] = 'Por palabras';
$gl_caption_custumer['c_selected']='Propietats sel·leccionades';
$gl_caption_custumer['c_selected_customers']='Propietaris sel·leccionats';
$gl_caption_custumer['c_status'] = 'Estado';
$gl_caption_custumer['c_property_title'] = 'Descripción corta';
$gl_caption_custumer['c_description'] = 'Descripción';
$gl_caption_custumer['c_category_id'] = 'Tipos';
$gl_caption_custumer['c_prepare'] = 'En preparación';
$gl_caption_custumer['c_review'] = 'Revisar';
$gl_caption_custumer['c_onsale'] = 'Para vender';
$gl_caption_custumer['c_sold'] = 'Vendido';
$gl_caption_custumer['c_archived'] = 'Archivado';
$gl_caption['c_property'] = 'Inmueble';
$gl_caption_custumer['c_property_private']='Nombre privado';
$gl_caption_custumer['c_surnames'] = 'Apellidos';
//captions tpl demands_list.tpl
$gl_caption_custumer_demand['c_result_search']= 'Resultados de la busqueda. ';
$gl_caption_custumer['c_demandnumber']= 'Solicitud Nº';
//$gl_caption_custumer['c_municipi']= 'Sol·licitud Nº';
$gl_caption_custumer['c_enteredc']= 'Creado';
$gl_caption_custumer['c_entered']= 'Creada';

// editar dades clients -> llistar demandes
/*$gl_caption_custumer_demand ['c_custumer_id']='Client';
$gl_caption_custumer_demand['c_tipus2'] = 'Tipus d\'obra';
$gl_caption_custumer_demand['c_new'] = 'Obra nova';
$gl_caption_custumer_demand['c_second'] = '2º ma';
$gl_caption_custumer_demand['c_tipus'] = 'Operació';
$gl_caption_custumer_demand['c_sell'] = 'Venda';
$gl_caption_custumer_demand['c_rent'] = 'Lloguer';
$gl_caption_custumer_demand['c_municipi_id'] = 'Municipi';
$gl_caption_custumer_demand['c_garage'] = 'Garatge';
$gl_caption_custumer_demand['c_storage'] = 'Trastero';
$gl_caption_custumer_demand['c_terrace'] = 'Terrassa';
$gl_caption_custumer_demand['c_garden'] = 'Jardí';*/

// captions comuns
$gl_caption ['c_custumer_id']='Codigo';
$gl_caption_demand ['c_custumer_id']='Nombre cliente';
$gl_caption_demand ['c_demand_id'] ='Solicitut';
$gl_caption_demand ['c_phone'] ='Teléfono';

// captions seccio
$gl_caption_custumer['c_demand_id']='Solicitud';
$gl_caption['c_name'] = 'Nombre';
$gl_caption['c_surname1'] = 'Primer Apellido';
$gl_caption['c_surname2'] = 'Segundo Apellido';
$gl_caption_custumer ['c_custumer_id']='Codigo';
$gl_caption_custumer['c_vat'] = 'DNI/NIF/NIE';
$gl_caption_custumer['c_phone'] = 'Teléfonos';
$gl_caption_custumer['c_phone1'] = 'Tel. 1';
$gl_caption['c_phone2'] = 'Tel. 2';
$gl_caption['c_phone3'] = 'Fax';
$gl_caption_custumer['c_adress'] = 'Calle';
$gl_caption_custumer['c_numstreet'] = 'Número';
$gl_caption_custumer['c_block'] = 'Bloque';
$gl_caption_custumer['c_flat'] = 'Piso';
$gl_caption_custumer['c_door'] = 'Puerta';
$gl_caption_custumer['c_town'] = 'Población';
$gl_caption_custumer['c_province'] = 'Provincia';
$gl_caption_custumer['c_country'] = 'País';
$gl_caption_custumer['c_bankdata'] = 'Datos bancarios';
$gl_caption_custumer['c_bank'] = 'Caja / Banco';
$gl_caption_custumer['c_comta'] = 'IBAN';
$gl_caption_custumer['c_account_holder'] = 'Titular';
$gl_caption_custumer['c_bic'] = 'BIC';
$gl_caption_custumer['c_mail'] = 'Correo electrónico';
$gl_caption_custumer['c_zip'] = 'Codigo postal';
$gl_caption_custumer['c_observations1'] = 'Observaciones';
$gl_caption_custumer['c_observations2'] = 'Observaciones (2)';
$gl_caption_custumer ['c_search1']='Búsqueda per codigo';
$gl_caption_custumer ['c_owner']='El cliente es propietario? ';
$gl_caption_custumer ['c_buyer']='El client es comprador? ';
$gl_caption_custumer ['c_category'] ='Tipo';
$gl_caption_custumer['c_address'] = 'Dirección';

$gl_caption_custumer['c_add_button'] = 'Alta propiedad';
//$gl_caption_custumer['c_assign_button'] = 'Assignar propietat';
$gl_caption_custumer['c_demand_button'] = 'Solicitud';
$gl_caption_custumer['c_add_demand_link']='Crear nueva solicitud';
$gl_caption['c_edit_demand_button']='Editar solicitudes'; // aquest ha de ser gl_caption
$gl_caption_custumer['c_propertyscustumer'] = 'Propiedades del cliente';
$gl_caption_custumer['c_demandscustumer'] = 'Solicitudes del cliente';
$gl_caption_custumer['c_bookingcustumer'] = 'Reservas del cliente';
$gl_caption_custumer['c_bookingcustumer_list'] = 'Listar reservas';
$gl_caption_custumer['c_ref']='Ref';
$gl_caption['c_edit_property_button']='Editar inmueble';
$gl_caption['c_select_property_link']='Assignar inmueble';
$gl_caption['c_add_property_link']='Crear nuevo inmueble';
$gl_caption['c_assign_button']='Assignar propiedad';
//captions propietari
$gl_caption_custumer ['c_by_code']='Busqueda por código: ';
$gl_caption_custumer_owner ['c_search']='Buscar: ';

$gl_caption ['c_books_button'] = 'Ver reservas';
$gl_caption ['c_searchexact_button'] = 'Buscar excatas';
$gl_caption ['c_searchprox_button'] = 'Buscar aprox.';
$gl_caption_demand ['c_entered'] = 'Creada';
$gl_caption_demand ['c_data_demand'] = 'Datos de la solicitud';
$gl_caption_demand ['c_garage_area'] = 'superficie';
//$gl_caption_property ['c_direccio'] = 'Direcció';

$gl_caption_demand ['c_comercialname'] = 'Empresa';
//$gl_caption_demand ['c_custumer_id_format']='<strong>Nom: </strong> %s %s <strong>Tel: </strong> %s';
$gl_caption_demand ['c_0']='-';
$gl_caption_demand ['c_1']='si';
$gl_caption_demand ['c_room_0']='-';
$gl_caption_demand ['c_room_1']='1';

$gl_caption_demand ['c_bathroom_0'] = $gl_caption_demand ['c_wc_0'] = $gl_caption_demand ['c_room_0'];
$gl_caption_demand ['c_bathroom_1'] = $gl_caption_demand ['c_wc_1'] = $gl_caption_demand ['c_room_1'];

global $gl_caption_demand_property;
$gl_caption_demand_property['c_send_edit'] = 'Assignar seleccionados';
$gl_caption_demand_property ['c_0']='-';
$gl_caption_demand_property ['c_1']='si';
$gl_caption_demand_property ['c_room_0']='-';
$gl_caption_demand_property ['c_room_1']='1';

$gl_caption ['c_nodefinit']='no o no definido';
$gl_caption_demand ['c_nodefinit']='no o no definido';
$gl_caption_demand ['c_maxprice'] ='Precio máximo';
$gl_caption_demand['c_resultsmls']= 'Resultado busqueda MLS';
$gl_caption_demand['c_observations']= 'Observaciones';

$gl_caption_demand['c_phone1']='Teléfono';
$gl_caption_demand['c_client_id']='';
$gl_caption_demand['c_client_dir']='';
$gl_caption_demand['c_domain']='';


$gl_caption_demand['c_search_level_filter'] = 'Nivel de proximidad en la busqueda';
$gl_caption_demand['c_search_level_1'] = 'Restrictivo';
$gl_caption_demand['c_search_level_2'] = 'Normal';
$gl_caption_demand['c_search_level_3'] = 'Poco restrictivo';

$gl_messages['no_records_demands'] = 'No se ha encontrado ninguna coincidencia para ninguna solicitud';
$gl_messages['no_records_demand'] = 'No se ha encontrado ninguna coincidencia para la solicitud';
$gl_messages['no_records_custumer'] = 'No se ha encontrado ninguna coincidencia para el cliente';
?>