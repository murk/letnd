<?php
// menus eina
define('ARAM_MENU_EDITOR', 'Edició pàgines');
define('ARAM_MENU_EDITOR_LIST', 'Llistar pàgines');
define('ARAM_MENU_EDITOR_BIN', 'Paperera de reciclatge');

define('ARAM_MENU_HABITATGE', 'Habitatges');
define('ARAM_MENU_HABITATGE_LIST', 'Llistat i editar habitatges');
define('ARAM_MENU_HABITATGE_PAGAMENT', 'Editar forma pagament');
define('ARAM_MENU_HABITATGE_NEW', 'Nou habitatge');
define('ARAM_MENU_HABITATGE_BIN', 'Paperera de reciclatge');
define('ARAM_MENU_CONFIG_LIST', 'Configuració');

define('ARAM_MENU_GALLERY', 'Galeria');
define('ARAM_MENU_GALLERY_LIST', 'Editar galeria');

define('ARAM_MENU_FLOOR', 'Plantes');
define('ARAM_MENU_FLOOR_NEW', 'Nova planta');
define('ARAM_MENU_FLOOR_LIST', 'Llistar plantes');

// captions seccio
$gl_caption_habitatge['c_edit_title'] = 'Dades de l\'habitatge';
$gl_caption_habitatge['c_edit_title1'] = 'Forma de pagament';
$gl_caption_habitatge['c_edit_title2'] = 'Càlcul hipoteca';

$gl_caption_habitatge['c_filter_escala'] = 'Escala';
$gl_caption_habitatge['c_filter_floor_id'] = 'Planta';
$gl_caption_habitatge['c_filter_flat'] = 'Porta';

$gl_caption_habitatge['c_escala'] = 'Escala';
$gl_caption_habitatge['c_floor_id'] = 'Planta';
$gl_caption_habitatge['c_flat'] = 'Porta';
$gl_caption_habitatge['c_room'] = 'Habitacions';
$gl_caption_habitatge['c_m2'] = 'Superfície';
$gl_caption_habitatge['c_m2_exterior'] = 'Superfície exterior';
$gl_caption_habitatge['c_price'] = 'Preu';
$gl_caption_habitatge['c_reserva'] = 'Reserva';
$gl_caption_habitatge['c_contracte'] = 'Pagament inicial (Contracte)';
$gl_caption_habitatge['c_claus'] = 'Pagament final';
$gl_caption_habitatge['c_sell'] = 'Venut';
$gl_caption_habitatge['c_file'] = 'Plànol';
$gl_caption_habitatge['c_x'] = 'Coordenada X';
$gl_caption_habitatge['c_x2'] = 'Coordenada X2';
$gl_caption_habitatge['c_y'] = 'Coordenada Y';
$gl_caption_habitatge['c_y2'] = 'Coordenada Y2';
$gl_caption_habitatge['c_monthly'] = 'Quota mensual';
$gl_caption_habitatge['c_quota_subsidiada'] = 'Quota subsidiada hipoteca';
$gl_caption_habitatge['c_quota_subsidiada2'] = 'Quota subsidiada hipoteca';
$gl_caption_habitatge['c_status'] = 'Estat';
$gl_caption_habitatge['c_prepare'] = 'En preparació';
$gl_caption_habitatge['c_review'] = 'Per revisar';
$gl_caption_habitatge['c_public'] = 'Públic';
$gl_caption_habitatge['c_archived'] = 'Arxivat';
$gl_caption_habitatge['c_escala_casa'] = 'Casa';
$gl_caption_habitatge['c_escala_0'] = '';
$gl_caption_habitatge['c_escala_a'] = 'A';
$gl_caption_habitatge['c_escala_b'] = 'B';
$gl_caption_habitatge['c_escala_c'] = 'C';
$gl_caption_habitatge['c_escala_d'] = 'D';
$gl_caption_habitatge['c_escala_e'] = 'E';
$gl_caption_habitatge['c_escala_f'] = 'F';

// captions seccio
$gl_caption['c_status'] = 'Estat';
$gl_caption['c_url1']='Enllaç';
$gl_caption['c_url1_name']='Nom enllaç';
$gl_caption['c_prepare'] = 'En preparació';
$gl_caption['c_review'] = 'Per revisar';
$gl_caption['c_public'] = 'Públic';
$gl_caption['c_sold'] = 'Venut';
$gl_caption['c_archived'] = 'Arxivat';


$gl_caption_habitatge['c_floor_0'] = 'Baixos';
$gl_caption_habitatge['c_floor_1'] = '1<sup>er</sup>';
$gl_caption_habitatge['c_floor_2'] = '2<sup>on</sup>';
$gl_caption_habitatge['c_floor_3'] = '3<sup>er</sup>';
$gl_caption_habitatge['c_floor_4'] = '4<sup>rt</sup>';
$gl_caption_habitatge['c_floor_5'] = '5<sup>é</sup>';
$gl_caption_habitatge['c_floor_6'] = '6<sup>é</sup>';

$gl_caption_habitatge['c_flat_1'] = '1<sup>a</sup>';
$gl_caption_habitatge['c_flat_2'] = '2<sup>a</sup>';
$gl_caption_habitatge['c_flat_3'] = '3<sup>a</sup>';
$gl_caption_habitatge['c_flat_4'] = '4<sup>a</sup>';
$gl_caption_habitatge['c_flat_5'] = '5<sup>a</sup>';
$gl_caption_habitatge['c_flat_6'] = '6<sup>a</sup>';
$gl_caption_habitatge['c_flat_7'] = '7<sup>a</sup>';
$gl_caption_habitatge['c_flat_8'] = '8<sup>a</sup>';
$gl_caption_habitatge['c_flat_9'] = '9<sup>a</sup>';
$gl_caption_habitatge['c_flat_10'] = '10<sup>a</sup>';
$gl_caption_habitatge['c_flat_11'] = '11<sup>a</sup>';
$gl_caption_habitatge['c_flat_12'] = '12<sup>a</sup>';

$gl_caption_habitatge['c_0'] = '';
$gl_caption_habitatge['c_1'] = '1';
$gl_caption_habitatge['c_2'] = '2';
$gl_caption_habitatge['c_3'] = '3';
$gl_caption_habitatge['c_4'] = '4';
$gl_caption_habitatge['c_5'] = '5';
$gl_caption_habitatge['c_6'] = '6';
$gl_caption_habitatge['c_7'] = '7';
$gl_caption_habitatge['c_8'] = '8';
$gl_caption_habitatge['c_9'] = '9';
$gl_caption_habitatge['c_10'] = '10';
$gl_caption_habitatge['c_11'] = '11';
$gl_caption_habitatge['c_12'] = '12';
$gl_caption_habitatge['c_13'] = '13';
$gl_caption_habitatge['c_14'] = '14';
$gl_caption_habitatge['c_15'] = '15';
$gl_caption_habitatge['c_16'] = '16';
$gl_caption_habitatge['c_17'] = '17';
$gl_caption_habitatge['c_18'] = '18';
$gl_caption_habitatge['c_19'] = '19';
$gl_caption_habitatge['c_20'] = '20';

$gl_caption_config['c_group_10'] = 'Obra';
$gl_caption_config['c_group_11'] = 'Pagaments';
$gl_caption_config['c_group_12'] = 'Reserva i lliurament';
$gl_caption_config['c_group_13'] = 'Arxius ( nomès arxius pdf )';
$gl_caption_config['c_memoria_qualitats'] = 'Memòria qualitats català';
$gl_caption_config['c_memoria_calidades'] = 'Memòria qualitats castellà';
$gl_caption_config['c_dossier_comercial'] = 'Dossier comercial català';
$gl_caption_config['c_dosier_comercial'] = 'Dossier comercial castellà';
$gl_caption_config['c_init_date_build'] = 'Inici obra';
$gl_caption_config['c_end_date_build'] = 'Final d\'obra';
$gl_caption_config['c_reserva_date'] = 'Data reserva';
$gl_caption_config['c_init_date_payment'] = 'Pagament inicial (contracte)';
$gl_caption_config['c_end_date_payment'] = 'Pagament final';
$gl_caption_config['c_claus_date'] = 'Lliurament habitatges';
$gl_caption_config['c_interest_hipoteca'] = 'Interès hipoteca';
$gl_caption_config['c_show_payment'] = 'Mostrar calendari de pagament a la web';
$gl_caption_config['c_show_informat'] = 'Mostrar botó "informa\'t" ( si no està marcat mostra el botó "contacte" )';
$gl_caption_config['c_group_1'] = 'Home - Fila 1';
$gl_caption_config['c_group_2'] = 'Home - Tres raons per comprar';
$gl_caption_config['c_group_3'] = 'Home - Altres projectes';


$gl_caption_floor['c_floor_num'] = 'Planta num.';
$gl_caption_floor['c_floor'] = 'Planta';
$gl_caption_floor['c_file'] = 'Plànol';

$gl_caption_gallery_image['c_edit'] = 'Galeria d\'imatges ( la primera imatge és el link de la home )';

?>