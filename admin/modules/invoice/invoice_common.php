<?

/**
 * InvoiceCommon
 *
 * Funcions comuns per INVOICE
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2017
 * @version $Id$
 * @access public
 */
class InvoiceCommon {

	static public function init() {
		setlocale( LC_TIME, 'ca_ES' );
	}

	static public function add_page_files() {

		Page::add_javascript_file( '/admin/modules/invoice/jscripts/functions.js' );
		// Page::add_css_file( '/admin/themes/inmotools/invoice/styles.css' );

	}

	static public function get_concept_sublist_html( $id, $field, $is_input, &$module ) {

		$listing = NewRow::get_sublist_html( $module, $id, $field, $is_input );

		$listing->order_by = 'ordre ASC, concept ASC';

		$listing->list_records( false );

		if ( $listing->has_results ) {

			/*			foreach ( $listing->results as $key => $val ) {
							$rs          = &$listing->results[ $key ];
							$rs['concept'] = Db::get_first( "SELECT concept FROM invoice__concept_language WHERE concept_id = $concept_id AND language = '$language'" );
						}*/
		}

		return $listing->parse_template();
	}

	// ajax
	static public function get_concept_sublist_item( &$module ) {

		// TODO-i Poder escollir idioma
		$language = 'cat';

		$listing = new NewRow( $module );

		$concept_id = $listing->selected_id;

		// Valors per defecte dels camps
		$rs = [
			'concept'    => Db::get_first( "SELECT concept FROM invoice__concept_language WHERE concept_id = $concept_id AND language = '$language'" ),
			'category'   => Db::get_first( "SELECT category FROM invoice__category_language, invoice__concept WHERE invoice__category_language.category_id = invoice__concept.category_id AND concept_id = $concept_id AND language = '$language'" ),
			'price'      => Db::get_first( "SELECT price FROM invoice__concept WHERE concept_id = $concept_id" ),
			'cost_price' => Db::get_first( "SELECT cost_price FROM invoice__concept WHERE concept_id = $concept_id" ),
			'ordre'      => Db::get_first( "SELECT ordre FROM invoice__concept WHERE concept_id = $concept_id" ),
		];

		$listing->add_row( $rs );
		$listing->new_row();
	}

	static public function get_concept_checkboxes_long_values() {

		$order = 'ordre asc, concept ASC';

		$limit = 200;

		$q = R::escape( 'query' );

		$language = LANGUAGE;

		$select     = "invoice__concept.concept_id as concept_id, concept, category, short_cut";
		$from       = "invoice__concept, invoice__concept_language, invoice__category_language";
		$common_sql = "
							invoice__concept.category_id = invoice__category_language.category_id 
							AND	invoice__concept.concept_id = invoice__concept_language.concept_id
				            AND invoice__category_language.language = '$language'
				            AND invoice__concept_language.language = '$language'
				            AND bin = 0";

		$ret                = array();
		$ret['suggestions'] = array();

		$r = &$ret['suggestions'];

		// 1 - Que comenci
		$query = "
					SELECT $select
					FROM $from
					WHERE
				        (
				            $common_sql
			            ) 
				        AND
				        (
				            concept LIKE '$q%'
							OR category LIKE '$q%'
							OR short_cut LIKE '$q%'
						)
					ORDER BY $order
					LIMIT $limit";

		$results = Db::get_rows( $query );

		$rest = $limit - count( $results );

		// 2 - Qualsevol posició
		if ( $rest > 0 ) {

			$query = "
						SELECT $select
						FROM $from
						WHERE
					        (
					            $common_sql
				            )
					        AND						 
							(
								( 
									concept LIKE '%$q%'
				                    AND concept NOT LIKE '$q%' 
				                )
								OR 
								( 
									category LIKE '%$q%'
				                    AND category NOT LIKE '$q%' 
				                )
								OR 
								( 
									short_cut LIKE '%$q%'
				                    AND short_cut NOT LIKE '$q%' 
				                )
			                )
						ORDER BY $order
						LIMIT $rest";

			$results2 = Db::get_rows( $query );

			$results = array_merge( $results, $results2 );

		}

		$rest = $limit - count( $results );

		$words = array();
		// 3 - Parteixo paraules
		if ( $rest > 0 ) {

			$words = explode( ' ', $q );
			if ( count( $words ) > 1 ) {

				$words_query_title    = '';
				$words_query_subtitle = '';

				foreach ( $words as $word ) {
					$words_query_title .= "AND category LIKE '%$word%'";
					$words_query_subtitle .= "AND concept LIKE '%$word%'";
				}

				$query = "						
						SELECT $select
						FROM $from
						WHERE
					        (
					            $common_sql
				            )
					        AND						 
							(
								( 
									concept NOT LIKE '%$q%'
				                    AND concept NOT LIKE '$q%' 
				                    $words_query_title
				                )
								OR 
								( 
									category NOT LIKE '%$q%'
				                    AND category NOT LIKE '$q%' 
				                    $words_query_subtitle
				                )
								OR 
								( 
									short_cut NOT LIKE '%$q%'
				                    AND short_cut NOT LIKE '$q%' 
				                    $words_query_subtitle
				                )
			                )
						ORDER BY $order
						LIMIT $rest";

				$results2 = Db::get_rows( $query );

				$results = array_merge( $results, $results2 );
			}

		}

		// Presento resultats
		foreach ( $results as $rs ) {


			$concept   = str_replace( $q, "<strong>$q</strong>", $rs['concept'] );
			$category  = str_replace( $q, "<strong>$q</strong>", $rs['category'] );
			$short_cut = str_replace( $q, "<strong>$q</strong>", $rs['short_cut'] );

			if ( count( $words ) > 1 ) {
				foreach ( $words as $word ) {
					$concept  = str_replace( $word, "<strong>$word</strong>", $concept );
					$category = str_replace( $word, "<strong>$word</strong>", $category );
				}
			}


			$r[] = array(
				'value'  => $concept,
				'custom' => "<div class='custom'><h6>$category - <i>$short_cut</i></h6><p>$concept</p></div>",
				'data'   => $rs['concept_id']
			);
		}


		$rest               = $limit - count( $results );
		$ret['is_complete'] = false; // Aquest mai es complert, ja que el jscript no pot buscar paraules separades, i llavors perdo la funcionalitat // $rest>0;
		$ret['has_custom']  = true;

		if ( ! DEBUG ) {
			header( 'Content-Type: application/json' );
		}
		die ( json_encode( $ret ) );


	}


	// Select clients



	static public function get_custumer_select_long_values () {

		//	$field = R::get( 'field' );
		//	$id = R::get( 'id' );
		$limit = 100;

		$q = R::escape('query');
		$q = strtolower($q);

		$ret = array();
		$ret['suggestions'] = array();

		$r = &$ret['suggestions'];

		$common_sql = "
					AND (name <> '' OR surname1 <> '' OR surname2 <> '' or company <> '' or comercial_name <> '' )
					AND bin = 0
					AND activated = 1";
		$order = "surname1 ASC, name ASC, surname2 ASC, company ASC, comercial_name ASC";


		// 1 - Custumer, que comenci
		$query = "SELECT company, comercial_name, name, surname1, surname2, custumer_id
					FROM custumer__custumer
					WHERE 
					(
						CONCAT(name,' ', surname1, ' ', surname2) LIKE '" . $q . "%'						
						OR company LIKE '" . $q . "%'
						OR comercial_name LIKE '" . $q . "%'					
					)
					$common_sql
					ORDER BY $order
					LIMIT " . $limit;

		$results = Db::get_rows($query);

		$rest = $limit - count( $results );

		// 2 - Custumer, qualsevol posició
		if ($rest > 0) {

			$query = "SELECT  company, comercial_name, name, surname1, surname2, custumer_id
					FROM custumer__custumer
					WHERE 
						(
							(
								CONCAT(name,' ', surname1, ' ', surname2) LIKE '%" . $q . "%'
								AND CONCAT(name,' ', surname1, ' ', surname2) NOT LIKE '" . $q . "%'
							)
							OR
							(
								company LIKE '%" . $q . "%'
								AND company NOT LIKE '" . $q . "%'
							)
							OR
							(
								comercial_name LIKE '%" . $q . "%'
								AND comercial_name NOT LIKE '" . $q . "%'
							)
						)
					$common_sql
					ORDER BY $order
					LIMIT " . $rest;

			$results2 = Db::get_rows($query);

			$results = array_merge( $results, $results2 );

		}

		// Presento resultats
		foreach ($results as $rs)
		{
			$value = $rs['name'];

			$company = str_replace ($q , "<strong>$q</strong>", $rs['company']);
			$comercial_name = str_replace ($q , "<strong>$q</strong>", $rs['comercial_name']);

			if ($rs['surname1']) $value .= $value?' ' . $rs['surname1']: $rs['surname1'];
			if ($rs['surname2']) $value .= $value?' ' . $rs['surname2']: $rs['surname2'];

			$name = str_replace ($q , "<strong>$q</strong>", $value);

			// Borrar quan hi hagi custom
			if ($rs['company']) $value = $value?$rs['company'] . ' - ': $rs['company'];
			if ($rs['comercial_name']) $value = $value?$rs['comercial_name'] . ' - ': $rs['comercial_name'];
			// Fi Borrar quan hi hagi custom


			$r[] = array(
				'value' => $value,
				'custom' => "<div class='custom'><h6>$company - <i>$comercial_name</i></h6><p>$name</p></div>",
				'data'  => $rs['custumer_id']
			);
		}

		// TODO-i Aquest objecte encara no té la funció 'custom'
		$rest = $limit - count( $results );
		$ret['has_custom'] = true;
		$ret['is_complete'] = $rest>0;

		die (json_encode($ret));


	}

	static public function format_currentcys(&...$args) {

		foreach ( $args as &$arg ) {
			$arg = format_currency( $arg );
		}

	}

}

?>