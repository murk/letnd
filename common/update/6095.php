<?

/* 26-10-16

	PORTFOLI ARXIUS


 */

if ( is_table( 'portfoli__portfoli' ) ) {

	if ( ! is_table( 'portfoli__portfoli_file' ) ) {

		$update_sql [] = "CREATE TABLE `portfoli__portfoli_file` (
						`file_id` INT(11) NOT NULL AUTO_INCREMENT,
						`portfoli_id` INT(11) NOT NULL,
						`name` VARCHAR(100) NOT NULL COLLATE 'utf8_spanish2_ci',
						`size` INT(11) NOT NULL,
						`ordre` INT(11) NOT NULL,
						`main_file` INT(1) NOT NULL,
						`show_home` TINYINT(1) NOT NULL DEFAULT '0',
						PRIMARY KEY (`file_id`)
					)
					COLLATE='utf8_spanish2_ci'
					ENGINE=MyISAM
					AUTO_INCREMENT=1
					;
";


		$client_dir = DOCUMENT_ROOT . '/' . $_SESSION['client_dir'];

		$dir = $client_dir . '/portfoli/';
		if ( ! is_dir( $dir ) ) {
			mkdir( $dir, 0776 );
		}

		$dir = $client_dir . '/portfoli/portfoli/';
		if ( ! is_dir( $dir ) ) {
			mkdir( $dir, 0776 );
		}

		$dir = $client_dir . '/portfoli/portfoli/files/';
		if ( ! is_dir( $dir ) ) {
			mkdir( $dir, 0776 );
		}


	} else {
		trigger_error( 'Ja existeixen la taula portfoli__portfoli_file' );
	}

}

if ( is_table( 'portfoli__portfoli' ) && ! is_field( 'portfoli__portfoli', 'videoframe' ) ) {

	$update_sql[] = "ALTER TABLE `portfoli__portfoli`
	ADD COLUMN `videoframe` TEXT NOT NULL AFTER `status`;";

} else {
	if ( is_table( 'portfoli__portfoli' ) ) {
		trigger_error( 'Ja existeixen el camp videoframe' );
	}
}


if ( is_table( 'portfoli__configadmin' ) && ! Db::get_first( "SELECT * FROM portfoli__configadmin WHERE name='has_files'" ) ) {
	$update_sql[] = "
		INSERT INTO portfoli__configadmin (`name`, `value`) VALUES ('has_files', '0');";

} else {
	if ( is_table( 'portfoli__configadmin' ) ) {
		trigger_error( 'Ja existeixen el camp has_files' );
	}
}


?>