<?php
global $gl_caption_tv, $gl_messages_tv;
// menus eina
if (!defined('INMO_MENU_PROPERTY')){
	define('INMO_MENU_PROPERTY', 'Immobles');
	define('INMO_MENU_PROPERTY_NEW', 'Entrar nou immoble');
	define('INMO_MENU_PROPERTY_LIST', 'Llistar immobles');
	define('INMO_MENU_SEARCH_FORM', 'Buscar i Editar immobles');
	define('INMO_MENU_PROPERTY_BIN', 'Paperera de reciclatge');
	define('INMO_MENU_CATEGORY', 'Tipus d\'immobles');
	define('INMO_MENU_CATEGORY_NEW', 'Entrar nou tipus');
	define('INMO_MENU_CATEGORY_LIST', 'Llistar tipus');
	define('INMO_MENU_TRANSLATION', 'Traduccions');
	define('INMO_MENU_PROPERTY_TRANSLATION_LIST', 'Llistar immobles per traduir');
	define('INMO_MENU_CATEGORY_TRANSLATION_LIST', 'Llistar categories per traduir');
	define('INMO_MENU_CONFIG', 'Configuració Immobles');
	define('INMO_MENU_CONFIGADMIN_LIST', 'Configuració part privada');
	define('INMO_MENU_CONFIGPUBLIC_LIST', 'Configuració part pública');
	define('INMO_MENU_CONFIGADMIN', 'Configuració part privada');
	define('INMO_MENU_CONFIGPUBLIC', 'Configuració part pública');
	define('INMO_MENU_ZONE', 'Zones');
	define('INMO_MENU_ZONE_NEW', 'Entrar nova zona');
	define('INMO_MENU_ZONE_LIST', 'Llistar zones');
	define('INMO_MENU_ZONE_BIN', 'Paperera de reciclatge');
	define('INMO_MENU_TV', 'Aparadors virtuals');
	define('INMO_MENU_TV_LIST', 'Sel·leccionar immobles');
	define('INMO_MENU_TV_LIST_SELECTED', 'Immobles a l\'aparador');
	define('INMO_MENU_TV_LIST_DOWNLOAD', 'Descarregar aparador');
	define('INMO_MENU_PROPERTY_MAP_LIST', 'Buscar per mapa');

	define ('TITLE_COMARCA_ADMIN','Per zona: ');

	define('PROPERTY_AUTO_REF', 'Automàtica');

	define('BOOKING_MENU_PROMCODE','Codi promocional');
	define('BOOKING_MENU_PROMCODE_NEW','Crear codi');
	define('BOOKING_MENU_PROMCODE_LIST','Llistar codi');
	define('BOOKING_MENU_PROMCODE_LIST_BIN','Paperera');

	define('BOOKING_MENU_POLICE','Registro Mossos');
	define('BOOKING_MENU_POLICE_LIST','Listar reservas');
}
// sobreescrits missatges demand custumer
if (isset($gl_messages)) {
	$gl_messages_demand = $gl_messages;
	$gl_messages_custumer = $gl_messages;
}
// sobreescrits end

// sobreescrits missatges property
$gl_messages_property['added']='L\'immoble s\'ha afegit correctament. A continuació pots insertar les imatges corresponents a la nova finca';
$gl_messages_property['saved']='L\'immoble ha estat modificat correctament';
$gl_messages_property['no_records']='Geen object beschikbaar in deze categorie';
$gl_messages_property['no_records_bin']='No hi ha cap immoble a la paperera';
$gl_messages_property['list_saved']='Els canvis en els immobles s\'han efectuat correctament';
$gl_messages_property['list_not_saved']='No s\'ha modificat cap immoble, no s\'ha detectat cap canvi';
$gl_messages_property['list_deleted']='Els immobles sel·leccionats han estat esborrats';
$gl_messages_property['list_bined']='Els immobles s\'han enviat a la paperera de reciclatge';
$gl_messages_property['list_restored']='Els immobles s\'han recuperat correctament';
// sobreescrits end

$gl_messages_category['related_not_activated'] = 'Aquest tipus no es pot desactivar, conté immobles relacionats';
$gl_messages_category['relateds_not_activated'] = 'Els tipus d\'immobles marcats no es poden desactivar, contenen immobles relacionats';

// captions seccio
$gl_caption_demand['c_form_title'] = 'Dades de la demanda del client: ';

$gl_caption['c_property_button'] = 'Veure immobles';
$gl_caption['c_view_property'] = 'Veure immoble';

$gl_caption_property['c_move_to_category_id'] = '[tipus d\'immoble]';
$gl_caption_property['c_move_to_category_id2'] = 'Canvia immobles seleccionats a:';
$gl_caption_property['c_move_to_zone_id'] = '[zona]';
$gl_caption_property['c_move_to_zone_id2'] = 'Canvia immobles seleccionats a:';
$gl_caption_property['c_move_to_zone_id3'] = '[cap zona]';

$gl_caption_category['c_ordre'] = 'Ordre';
$gl_caption_category['c_ref_code'] = 'Codi referència immobles';
$gl_caption_property['c_room'] = 'Slaapkamers';
$gl_caption_property['c_bathroom'] = 'Badkamers';
$gl_caption_property['c_wc'] = 'WC\'s';
$gl_caption_property['c_laundry'] = 'Washok';
$gl_caption_property['c_parking'] = 'Parking';
$gl_caption_property['c_garage'] = 'Garage';
$gl_caption_property['c_garage_area'] = 'Oppervlakte';
$gl_caption_property['c_storage'] = 'Bergruimte';
$gl_caption_property['c_storage_area'] = 'Oppervlakte';
$gl_caption_property['c_terrace'] = 'Terras';
$gl_caption_property['c_terrace_area'] = 'Oppervlakte';
$gl_caption_property['c_garden'] = 'Tuin';
$gl_caption_property['c_garden_area'] = 'Oppervlakte';
$gl_caption_property['c_swimmingpool'] = 'Zwembad';
$gl_caption_property['c_swimmingpool_area'] = 'Oppervlakte';
$gl_caption_property['c_citygas'] = 'Stadsgas';
$gl_caption_property['c_centralheating'] = 'Centraal verwarming';
$gl_caption_property['c_airconditioned'] = 'Air conditioning';
$gl_caption_property['c_ref'] = 'Ref.';
$gl_caption_property['c_property_private'] = 'Nom privat';
$gl_caption_property['c_category_id'] = 'Type';
$gl_caption_property['c_floor_space'] = 'Oppervlakte';
$gl_caption_property['c_land'] = 'Grond';
$gl_caption_property['c_land_units'] = 'Eenheden';
$gl_caption_property['c_m2'] = 'm2';
$gl_caption_property['c_ha'] = 'Hektaren';
$gl_caption_property['c_country_id'] = 'Land';
$gl_caption_property['c_price_private'] = 'Prijs eigenaar';
$gl_caption_property['c_price'] = 'Prijs';
$gl_caption_property['c_price_m2'] = 'Prijs/m2';
$gl_caption_property['c_zoom'] = 'Zoom';
$gl_caption_property['c_price_consult'] = 'prijs na te vragen';
$gl_caption_property['c_price_consult_1'] = 'Prijs na te vragen';
$gl_caption_property['c_price_history_link'] = 'Historial de precios';
$gl_caption_property['c_owner_id'] = '';
$gl_caption_property['c_status'] = 'Staat';
$gl_caption_property['c_prepare'] = 'In voorbereiding';
$gl_caption_property['c_review'] = 'Herzien';
$gl_caption_property['c_onsale'] = 'Publiek';
$gl_caption_property['c_sold'] = 'Verkocht';
$gl_caption_property['c_archived'] = 'Gearchiveerd';
$gl_caption_property['c_reserved'] = 'Gereserveerd';
$gl_caption_property['c_zone'] = 'Zone'; // per la part publica
$gl_caption_property['c_zone_id'] = 'Zone';
$gl_caption_property['c_zone_id_caption'] = '[geen enkele zone]';
$gl_caption_property['c_provincia_id'] = 'Provincie';
$gl_caption_property['c_comarca_id'] = 'Streek';
$gl_caption_property['c_municipi_id'] = $gl_caption_property['c_municipi'] = 'Gemeente';
$gl_caption_property['c_property'] = 'Naam van het eigendom';
$gl_caption_property['c_property_title'] = 'Korte beschrijving';
$gl_caption_property['c_entered'] = 'Gemaakt op';
$gl_caption_property['c_ref_number'] = '';
$gl_caption_property['c_home'] = 'Opmerkelijk';
$gl_caption_property['c_offer'] = 'Aanbod';
$gl_caption_property['c_data'] = 'Gegevens van het gebouw';
$gl_caption_property['c_descriptions'] = 'Beschrijvingen';
$gl_caption_property['c_details'] = 'Details';
$gl_caption_property['c_description'] = 'Beschrijving';
$gl_caption_property['c_details_temp'] = 'Lloguer de temporada';
$gl_caption_property['c_0'] = '';
$gl_caption_property['c_1'] = '1';
$gl_caption_property['c_2'] = '2';
$gl_caption_property['c_3'] = '3';
$gl_caption_property['c_4'] = '4';
$gl_caption_property['c_5'] = '5';
$gl_caption_property['c_6'] = '6';
$gl_caption_property['c_7'] = '7';
$gl_caption_property['c_8'] = '8';
$gl_caption_property['c_9'] = '9';
$gl_caption_property['c_10'] = '10';
$gl_caption_property['c_11'] = '11';
$gl_caption_property['c_12'] = '12';
$gl_caption_property['c_13'] = '13';
$gl_caption_property['c_14'] = '14';
$gl_caption_property['c_15'] = '15';
$gl_caption_property['c_16'] = '16';
$gl_caption_property['c_17'] = '17';
$gl_caption_property['c_18'] = '18';
$gl_caption_property['c_19'] = '19';
$gl_caption_property['c_20'] = '20';
$gl_caption_property['c_tipus'] = 'Verrichting';
$gl_caption_property['c_tipus_sell'] = 'Verkoop';
$gl_caption_property['c_tipus_rent'] = 'Verhuur';
$gl_caption_property['c_filter_status'] = 'Alle toestanden';
$gl_caption_property['c_filter_category_id'] = 'Alle types';
$gl_caption_property['c_filter_tipus'] = 'Alle verrichtingen';
$gl_caption_property['c_address'] = 'Adres';
$gl_caption_property['c_adress'] = 'Straat';
$gl_caption_property['c_numstreet'] = 'Nummer';
$gl_caption_property['c_block'] = 'Blok';
$gl_caption_property['c_flat'] = 'Appartement';
$gl_caption_property['c_door'] = 'Deur';
$gl_caption_property['c_observations'] = 'Observaties';
$gl_caption_property['c_data_private'] = 'Privé gegevens';
$gl_caption_property['c_data_public'] = 'Publieke gegevens';
$gl_caption_property['c_tipus2'] = 'Type bouwwijze';
$gl_caption_property['c_elevator'] = 'Lift';
$gl_caption_property['c_balcony'] = 'Balkon';
$gl_caption_property['c_new'] = 'Nieuwbouw';
$gl_caption_property['c_second'] = 'Tweedehands';
$gl_caption_property['c_furniture'] = 'Gemeubeld';
$gl_caption_property['c_file'] = 'Documenten';
$gl_caption_property['c_video'] = $gl_caption_property['c_videoframe'] = 'Video';
$gl_caption_property['c_edit_custumer'] = 'Eigenaars';
$gl_caption_property['c_edit_custumer_button'] = 'Editar propietaris';

$gl_caption_property['c_mls']='MLS';
$gl_caption_property['c_share_mls']='Compartir per MLS';
$gl_caption_property['c_comission'] = 'Comissió MLS';

$gl_caption_property['c_filter_activated'] = 'All customers';
$gl_caption_property['c_activated_0'] = 'Deactivated';
$gl_caption_property['c_activated_1'] = 'Activated';

$gl_caption_property['c_efficiency_letter'] = 'Letter';
$gl_caption_property['c_efficiency_number'] = 'Energy';
$gl_caption_property['c_efficiency_number2'] = 'Emission';
$gl_caption_property['c_sea_distance'] = 'Distance to the sea';

$gl_caption_property['c_expenses'] = 'Expenses';
$gl_caption_property['c_community_expenses'] = 'Community expenses';
$gl_caption_property['c_ibi'] = 'Property tax';
$gl_caption_property['c_municipal_tax'] = 'Local taxes';


$gl_caption_custumer['c_form_custumer_title'] = 'Propietaris de l\'immoble:';
$gl_caption_custumer['c_new_custumer'] = 'Entrar nou propietari';
$gl_caption_custumer['c_add_custumer'] = 'Assignar propietari';
$gl_caption_custumer['no_assigned'] = 'No hi ha cap propietari assignat a aquest immoble';

$gl_caption_zone['c_zone'] ='Nom del barri i/o zona';
$gl_caption_zone['c_municipi_id'] ='municipi';

// sobreescrits images
$gl_caption_image['c_edit'] = 'Imatges de la finca';
// sobreescrits end

//zones
$gl_caption_zone['c_municipi_id'] ='Municipi';
$gl_caption_zone['c_s_municipi_id'] ='Buscar municipi';
$gl_caption_zone['c_zone'] ='Nom del barri i/o zona';

// nous globals
$gl_caption['c_by_ref'] = '(o referències separades per espais, ej: "456 245 45")';
$gl_caption['c_by_words'] = 'Per paraules';

$gl_caption['c_list_title'] = 'Veure llistats';
$gl_caption['c_search_title'] = 'Buscar finca';
$gl_caption['c_filter_title'] = 'Filtrar finques';
$gl_caption['c_filter_title2'] = 'Gestió immobles';
$gl_caption['c_all_ordre'] = 'ordenat per:';
$gl_caption['c_category_title'] = 'Per Categoria';
$gl_caption['c_comarca_title'] = 'Per Comarca';
$gl_caption['c_municipi_title'] = 'Per Municipi';
$gl_caption['c_zone_title'] = 'Per Zona';
$gl_caption['c_status_title'] = 'Segons Estat';
$gl_caption['c_description_title'] = 'Segons Descripcions per fer';
$gl_caption['c_price_title'] = 'Per Preu';
$gl_caption['c_images_title'] = 'Per Imatges';
$gl_caption['c_has_no_images'] = 'No tenen imatges';
$gl_caption['c_has_images'] = 'Tenen imatges';
$gl_caption['c_images_indiferent'] = 'Indiferent';
$gl_caption['c_ref'] = 'Referència';
$gl_caption['c_property_private'] = 'Nom privat';
$gl_caption['c_entered'] = 'Data';
$gl_caption['c_active'] = 'Activat';
$gl_caption['c_category_original'] = 'Tipus original';
$gl_caption['c_category'] = 'Type';
$gl_caption['c_status'] = 'Estat';
$gl_caption['c_show_results'] = 'Mostrar resultats';
$gl_caption['c_floor_space_title'] = 'Oppervlakte';
$gl_caption['c_land_title'] = 'Grond';
$gl_caption['c_ordered_title'] = 'Gerangschikt volgens';
$gl_caption['c_user_title'] = 'Agents';
$gl_caption['c_custom_title'] = 'Característiques configurables';
$gl_caption['c_m2'] = 'm2';
$gl_caption['c_ha'] = 'ha';
$gl_caption['c_showwindow'] = 'Imprimir aparador';
$gl_caption['c_album'] = 'Imprimir album';


// NO TRADUIT

$gl_caption['c_images'] = 'Fotos';
$gl_caption['c_no_available'] = 'Dit eigendom is niet meer beschikbaar!';
$gl_caption['c_private'] = 'Amagar privat';
$gl_caption_tv['c_send_edit'] = 'Afegir al aparador';
$gl_caption_tv['c_download'] = 'Arxius';
$gl_caption_tv['c_main_files'] = 'Arxius_principals';
$gl_caption_tv['c_property_file'] = 'Immoble';

$gl_caption['c_type_title'] = 'Per tipus';


$gl_messages_tv['list_saved']='L\'aparador ha estat modificat correctament';


$gl_caption_property['c_living'] = 'Woonkamers';
$gl_caption_property['c_dinning'] = 'Eetkamers';
$gl_caption_property['c_service_room'] = 'Dienstkamer';
$gl_caption_property['c_service_bath'] = 'Dienstbadkamer';
$gl_caption_property['c_study'] = 'Studio';
$gl_caption_property['c_courtyard'] = 'Patio';
$gl_caption_property['c_courtyard_area'] = 'Oppervlakte';
$gl_caption_property['c_floor'] = 'Vloer';
$gl_caption_property['c_parket'] = 'Parket';
$gl_caption_property['c_ceramic'] = 'Mosaique';
$gl_caption_property['c_gres'] = 'Zandsteen';
$gl_caption_property['c_laminated'] = 'Stratifié';
$gl_caption_property['c_estrato'] = 'Estrato';
$gl_caption_property['c_estrato_e1'] = '1';
$gl_caption_property['c_estrato_e2'] = '2';
$gl_caption_property['c_estrato_e3'] = '3';
$gl_caption_property['c_estrato_e4'] = '4';
$gl_caption_property['c_estrato_e5'] = '5';
$gl_caption_property['c_estrato_e6'] = '6';
$gl_caption_property['c_edit_map'] = 'Plan';
$gl_caption_property['c_edit_booking'] = 'Reservaties';
$gl_caption_property['c_edit_booking_extra'] = 'Prices - Extras';
$gl_caption_property['c_form_map_title'] = 'Ubicació de l\'immoble';
$gl_caption_property['c_form_booking_title'] = 'Reserves de l\'immoble';
$gl_caption_property['c_save_map'] = 'Guardar ubicació';
$gl_caption_property['c_latitude'] = 'Latitud';
$gl_caption_property['c_longitude'] = 'Longitud';
$gl_caption_property ['c_center_longitude'] = 'Map center latitude';
$gl_caption_property ['c_center_latitude'] = 'Map center longitude';
$gl_caption_property ['c_center_coords'] = 'Center';
$gl_caption_property['c_click_map'] = 'Fes clic en un punt del mapa per establir la situació';
$gl_caption_property['c_show_map'] = 'Mostrar mapa part pública';
$gl_caption_property['c_show_map_point'] = 'Mostrar punt';
$gl_messages['point_saved'] = 'Nova ubicació guardada correctament';

$gl_caption_property['c_tipus_temp'] = 'Lloguer de temporada';
$gl_caption_property['c_tipus_moblat'] = 'Lloguer amb mobles';
$gl_caption_property['c_tipus_selloption'] = 'Alquiler con opción de compra';

$gl_caption_property['c_efficiency'] = 'Energiecertificaat';
$gl_caption_property['c_efficiency_'] = '';
$gl_caption_property['c_efficiency_progress'] = 'In behandeling';
$gl_caption_property['c_efficiency_exempt'] = 'Vrijgestelde';
$gl_caption_property['c_efficiency_a'] = 'A';
$gl_caption_property['c_efficiency_b'] = 'B';
$gl_caption_property['c_efficiency_c'] = 'C';
$gl_caption_property['c_efficiency_d'] = 'D';
$gl_caption_property['c_efficiency_e'] = 'E';
$gl_caption_property['c_efficiency_f'] = 'F';
$gl_caption_property['c_efficiency_g'] = 'G';

$gl_caption_property['c_efficiency2_a'] = 'A';
$gl_caption_property['c_efficiency2_b'] = 'B';
$gl_caption_property['c_efficiency2_c'] = 'C';
$gl_caption_property['c_efficiency2_d'] = 'D';
$gl_caption_property['c_efficiency2_e'] = 'E';
$gl_caption_property['c_efficiency2_f'] = 'F';
$gl_caption_property['c_efficiency2_g'] = 'G';
$gl_caption_property['c_efficiency2_notdefined'] = '';


$gl_caption_property['c_filter_room'] = 'Habitacions';
$gl_caption_property['c_filter_room_0'] = 'Cap';

$gl_caption_property['c_a4'] = 'A4';
$gl_caption_property['c_a3'] = 'A3';
$gl_caption_property['c_caption_showwindow_top_fields'] = 'Sense límit';
$gl_caption_property['c_caption_showwindow_pages'] = $gl_caption_property['c_caption_album_pages'] = 'Totes';
$gl_caption_property['c_submenu_0'] = 'Idioma';
$gl_caption_property['c_submenu_1'] = 'Tamany paper';
$gl_caption_property['c_submenu_2'] = 'Nombre de pàgines';
$gl_caption_property['c_submenu_3'] = 'Límit de camps';
$gl_caption_property['c_submenu_4'] = 'Mostrar';
$gl_caption_property['c_submenu_5'] = 'guardar aquesta configuració';
$gl_caption_property['c_submenu_6'] = 'Plantilla';

$gl_caption_property['c_views'] = 'Views';
$gl_caption_property['c_sea_view'] = 'Sea view';
$gl_caption_property['c_clear_view'] = 'Clear view';
// FI NO TRADUIT

// nomes admin
$gl_caption_property['c_custom1'] = $gl_caption_property['c_custom2'] = $gl_caption_property['c_custom3'] = $gl_caption_property['c_custom4'] = $gl_caption_property['c_custom5'] = $gl_caption_property['c_custom6'] = $gl_caption_property['c_custom7'] = $gl_caption_property['c_custom8'] = '';

$gl_caption_property['c_person'] = 'Persons';
$gl_caption_property['c_person_s'] = 'Person';
$gl_caption_property['c_person_p'] = 'Persons';

//$gl_caption_property['c_filter_custom1_0'] = 'No';
//$gl_caption_property['c_filter_custom1_1'] = 'Sí';

$gl_caption_property['c_user_id'] = 'Agent';
$gl_caption_property['c_filter_user_id'] = 'Tots els agents';


$gl_caption['c_edit_history'] = 'Historial';

$gl_caption_history['c_new_history'] = 'Afegir entrada';
$gl_caption_history['c_form_history_title'] = 'Historial de l\'immoble:';
$gl_caption_history['no_assigned'] = $gl_messages_history['no_records'] = 'No hi ha historial en aquest immoble';
$gl_caption_history ['c_custumer_id_format']='%s %s %s';
$gl_caption_history ['c_delete_button']='Eliminar';

$gl_messages_history['added']='L\'entrada s\'ha afegit correctament';
$gl_messages_history['saved']='L\'entrada ha estat modificada correctament';


$gl_caption_property['c_exact_result'] = 'S\'ha trobat  <strong>1</strong> sol·licitud que coïncideix amb aquest immoble' ;
$gl_caption_property['c_exact_results'] = 'S\'han trobat <strong>%s</strong> sol·licituds que coïncideixen amb aquest immoble' ;
$gl_caption_property['c_exact_results_link'] = 'Veure resultats';
$gl_caption_property['c_aprox_result'] = 'S\'ha trobat <strong>1</strong> sol·licitud aproximativa per aquest immoble';
$gl_caption_property['c_aprox_results'] = 'S\'han trobat <strong>%s</strong> sol·licituds aproximatives per aquest immoble';
$gl_caption_property['c_aprox_results_link'] = 'Veure resultats';
$gl_caption ['c_user_id_format']='%s %s';

$gl_caption['c_property_file_name'] = $GLOBALS['gl_caption']['c_file_name'];
$gl_caption['c_property_old_file_name'] = $GLOBALS['gl_caption']['c_old_file_name'];

$gl_caption['c_send_mail'] = 'Enviar email';
$gl_caption_adjective['c_adjective_before'] = 'Antes';
$gl_caption_adjective['c_adjective_after'] = 'Después';

// Portal
$gl_caption['c_portal_config_title'] = "Publishing options";
$gl_caption['c_apicat_export_all'] = "Export all properties";
$gl_caption['c_apicat_export_all_text'] = "If you check, all public properties will be exported without the need of selecting any of them";
$gl_caption['c_apicat_export_all_text_2'] = "Only properties with price and ZIP Code or exact location will be exported, Api.cat doesn't allow tourist rental. Fotocasa needs floor space";
$gl_caption['c_excluded_'] = "Exportable - No exportable";
$gl_caption['c_excluded_apicat'] = "Excluded Apicat";
$gl_caption['c_excluded_fotocasa'] = "Excluded Fotocasa";
$gl_caption['c_excluded_habitaclia'] = "Excluded Habitaclia";

$gl_caption['c_no_subscrit'] = "Information on portals publication";
$gl_caption['c_no_subscrit_apicat'] = "You are not registered to publish on Api.cat";
$gl_caption['c_no_subscrit_fotocasa'] = "You are not registered to publish on Fotocasa";
$gl_caption['c_no_subscrit_habitaclia'] = "You are not registered to publish on Habitaclia";

$gl_caption['c_portal_check_title'] = "The following data is needed to export";
$gl_caption['c_fotocasa_check_price'] = "Price";
$gl_caption['c_fotocasa_check_latitude'] = "Map location";
$gl_caption['c_fotocasa_check_floor_space'] = "Floor space";

$gl_caption['c_habitaclia_check_ref'] = "Reference";
$gl_caption['c_habitaclia_check_adress'] = "Address and map location with \"Show point\" checked";

$gl_caption['c_apicat_check_tipus'] = "Turístic rent not allowed";
$gl_caption['c_apicat_check_consult'] = "Uncheck \"Price on application\"";
$gl_caption['c_apicat_check_zip'] = "Postal code or map location with \"Show point\" checked";