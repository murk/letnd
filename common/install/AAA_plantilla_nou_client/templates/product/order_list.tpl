	<div class="order">
		<h3><?=$c_list_title?></h3>
		<table  class="table-products orders">
			<tr class="tr-header">
				<th>
					<?=$c_order_id?>
				</th>
				<th class="th-left">
					<?=$c_entered_order?>
				</th>
				<th class="th-left">
					<?=$c_order_status?>
				</th>
				<th>
					<?=$c_all_basetax?>
				</th>
				<th>&nbsp;</th>
			</tr>
			<? foreach($loop as $l): extract ($l)?>
				<tr>
					<td class="td-order">
						<strong class="mobile"><?= $c_order_id ?></strong><?=$order_id?>
					</td>
					<td>          
						<strong class="mobile"><?= $c_entered_order_short ?></strong><?=$entered_order?>
					</td>
					<td>
						<strong class="mobile"><?= $c_order_status ?></strong><?=$order_status?>
					</td>
					<td class="td-price">
						<strong class="price"><?=$all_basetax?>&nbsp;<?=CURRENCY_NAME?></strong>
					</td>
					<td class="td-cart">
						<?foreach($buttons as $button):?><?=$button?><?endforeach //$buttons?>
					</td>
				</tr>
			<? endforeach //$loop?>
		</table>

	</div>


	<? include( PATH_TEMPLATES . 'results.tpl' ) ?>