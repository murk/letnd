<?

/**
 * LassdiveAssigncentre
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2011
 * @version $Id$
 * @access public
 */
class LassdiveAssigncentre extends Module {

	function __construct() {
		parent::__construct();
	}
	function on_load() {
		parent::on_load();
	}

	function list_records() {
		$GLOBALS['gl_content'] = $this->get_records();
	}

	function get_records() {

		$family_id    = R::get( 'family_id' );
		$subfamily_id = R::get( 'subfamily_id' );

		$module = Module::load( 'product', 'product', $this, false, 'lassdive/lassdive_assigncentre_config.php' );


		if ( $family_id && $family_id != 'null' ) {

			// mostro desplegable de subfamilies relatives a la familia seleccionada
			$module->set_field( 'subfamily_id', 'select_condition', 'product__family.family_id=product__family_language.family_id  AND language=\'' . LANGUAGE . '\' AND product__family.parent_id = ' . $family_id . ' ORDER BY ordre ASC, family ASC' );
		}

		$listing = new ListRecords( $module );

		unset( $listing->caption['c_subfamily_id_format'] );

		$listing->has_images = false;
		$listing->set_options( 1, 0, 1, 0, 0, 0, 0, 0 );

		$listing->caption = array_merge( $listing->caption, $this->caption );

		$listing->add_filter( 'family_id' );
		$listing->add_filter( 'subfamily_id' );
		$listing->add_filter( 'status' );


		$listing->group_fields = [ 'family_id', 'subfamily_id' ];

		if ( $subfamily_id && $subfamily_id != 'null' ) {
			$listing->order_by = 'status asc, product_title ASC,ordre ASC';
		}
		else if ( $family_id && $family_id != 'null' ) {

			// mostro desplegable de subfamilies relatives a la familia seleccionada
			$listing->set_field( 'subfamily_id', 'select_condition', 'product__family.family_id=product__family_language.family_id  AND language=\'' . LANGUAGE . '\' AND product__family.parent_id = ' . $family_id . ' ORDER BY ordre ASC, family ASC' );

			$listing->order_by = 'subfamily_id, status asc, product_title ASC,ordre ASC';
		}
		else {

			$listing->order_by = 'family_id, subfamily_id, status asc, product_title ASC,ordre ASC';

		}


		$listing->list_records(false);

		foreach ($listing->results as $key => $val){
			$rs = &$listing->results[$key];
			$product_id = $rs['product_id'];
			$rs['centres'] = $this->get_centres( $product_id );
		}

		return $listing->parse_template();
	}

	public function get_centres( $product_id ) {

		$query = "SELECT centre, centre_id FROM lassdive__centre";

		$results = Db::get_rows( $query );

		$html = '';
		foreach ( $results as $rs ) {
			$centre    = $rs['centre'];
			$centre_id    = $rs['centre_id'];


			$query = "SELECT centre_id, centre_max_quantity FROM lassdive__centreproduct WHERE product_id = $product_id AND centre_id = $centre_id";

			$found_rs =  Db::get_row( $query );

			if ($found_rs) {
				$checked = Db::get_first( $query ) ? ' checked="checked"' : '';
				$centre_max_quantity = $found_rs['centre_max_quantity'];
			}
			else {
				$checked = '';
				$centre_max_quantity = '';
			}


			$html .= "
					<tr>
						<td>
							<input type=\"checkbox\" value=\"$centre_id\" $checked name=\"centre_id[$product_id][$centre_id]\" id=\"centre_id_${product_id}_$centre_id\">
							<label for=\"centre_id_${product_id}_$centre_id\">$centre</label>
						</td>
						<td>
						<input type=\"text\" value=\"$centre_max_quantity\" name=\"centre_max_quantity[$product_id][$centre_id]\" id=\"centre_max_quantity_${product_id}_$centre_id\">
						</td>
					</tr>
					";
		}
		$t1 = $this->caption['c_centre'];
		$t2 = $this->caption['c_max_quantity'];

		return "<table class=\"assigncentre_centres\">
					<tr>
						<th>$t1</th>
						<th>$t2</th>
					</tr>
					$html
					</table>";
	}

	public function save_rows() {


		$module    = Module::load( 'product', 'product', $this, false, 'lassdive/lassdive_assigncentre_config.php' );
		$save_rows = new SaveRows( $module );
		$save_rows->save();

		// Guardo a cenreproduct
		$ids = $save_rows->id;

		// TODO-i Fer-ho igual a checkboxes del motor
		foreach ( $ids as $product_id ) {

			$centres = isset($save_rows->post['centre_id'][$product_id]) ? $save_rows->post['centre_id'][$product_id]: false;

			$product_id_qs = Db::qstr( $product_id);
			$saved_centres = [];

			if ( $centres ) {

				foreach ( $centres as $centre_id ) {

					$centre_id_qs = Db::qstr( $centre_id);
					$saved_centres []= $centre_id_qs;
					$centre_max_quantity = Db::qstr($save_rows->post['centre_max_quantity'][$product_id][$centre_id]);

					$query = "
						SELECT count(*) 
						FROM lassdive__centreproduct 
						WHERE product_id = $product_id_qs
						AND centre_id = $centre_id_qs";
					$is_rs = Db::get_first( $query );

					if ( $is_rs ) {
						$query = "UPDATE lassdive__centreproduct
                                    SET centre_max_quantity = $centre_max_quantity 
									WHERE product_id = $product_id_qs
									AND centre_id = $centre_id_qs";
						Db::execute( $query );
					}
					else {
						$query = "INSERT INTO lassdive__centreproduct
									(centre_id, product_id, centre_max_quantity) 
									VALUES ($centre_id_qs, $product_id_qs, $centre_max_quantity);";
						Db::execute( $query );
					}

				}
			}
			// Borrar tots els centres que no s'ha guardat ara
			$query_centre_ids = $saved_centres ? "AND centre_id NOT IN (" . implode(",", $saved_centres) . ")" : '' ;
			$query = "DELETE FROM lassdive__centreproduct WHERE product_id = $product_id_qs $query_centre_ids";
			Db::execute( $query );
		}
	}
}

?>