<?
/**
 * VREASY - Pasar una reserva de letnd a vreasy
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) Gener 2018
 * @version $Id$
 * @access public
 */

/*

Molt important posar sempre el nom de la base de dades, aquesta clase es pot executar des de cli


*/

class AdmintotalVreasyBook {

	public $exporter = 'vreasy';
	public $section = 'book';
	public $api_url = "https://api.vreasy.com";
	public $is_cli = false;
	public $headers = [];
	public $vreasy_fields = [];
	public $letnd_fields = [];
	public $vreasy_status = [];
	public $letnd_status = [];
	public $db_name;
	public $response_errors = [];
	public $book_ids;
	public $portal_vars = false;
	public $deleted_results = [];
	public $agencia_id;
	public $agencia_rs;

	function __construct() {

		include_once( DOCUMENT_ROOT . 'admin/modules/admintotal/export_common.php' );
		include_once( DOCUMENT_ROOT . 'admin/modules/admintotal/admintotal_vreasy_book_common.php' );

		ExportCommon::init_unirest( $this );


		$this->vreasy_fields = AdminVreasyBookCommon::$vreasy_fields;
		$this->letnd_fields = AdminVreasyBookCommon::get_letnd_fields();
		$this->vreasy_status = AdminVreasyBookCommon::$vreasy_status;
		$this->letnd_status = AdminVreasyBookCommon::$letnd_status;
	}
	/*
	 * Funcions des de export_assync
	 *
	 */
	public function on_save_delete (){

		$db_name = $this->db_name;

		$results = $this->deleted_results;

		$this->trace( print_r ($results, true ), '', 'On Save Delete' );

		if (!$results) return;

		foreach ( $results as $rs ) {

			$book_owner = $rs['book_owner'];
			$vreasy_id = $rs['vreasy_id'];
			$book_id = $rs['book_id'];

			// Només es pot editar a letnd les que son propietat letnd
			if ($book_owner == 'letnd' && $vreasy_id) {

				$api_url = $this->api_url . "/reservations/$vreasy_id";

				// No es pot borrar, simplement la cancelo
				$json = '{"status": "CANCELLED"}';
				$response = Unirest\Request::put( $api_url, $this->headers, $json );
				$this->log_json( $api_url, '', $response );

				$code = $response->code;

				$this->trace( print_r ($response, true ), 'response' );

				if ( $code >= 299 || $code < 200 ) {
					send_mail_admintotal( "VREASY - No s'ha pogut borrar la reserva: $book_id de la BBDD $db_name", print_r( $response->body, true ) );
				}
			}

		}
	}
	public function on_save_update (){

		$letnd_fields = implode( ',', $this->letnd_fields );
		$db_name = $this->db_name;
		$book_ids = implode( ',', $this->book_ids );

		$query            = "
			SELECT $letnd_fields, vreasy_id, book_owner
			FROM $db_name.booking__book 
			WHERE book_id IN ($book_ids)";

		$results = Db::get_rows( $query );

		foreach ( $results as $rs ) {

			$property_id = $rs['property_id'];
			$query = "
				SELECT vreasy_id 
				FROM $db_name.inmo__property_to_vreasy 
				WHERE property_id = $property_id";
			$vreasy_property_id = Db::get_first( $query );
			$this->save_at_vreasy( $rs, $vreasy_property_id);
		}
	}

	function set_on_vars() {

		Db::connect_mother();

		$this->agencia_id = R::id( $this->agencia_id, false, false );
		R::escape_array( $this->book_ids );

		$agencias = ExportCommon::get_agencias('inmo_vreasy', $this->agencia_id);
		$this->agencia_rs = $agencias[0];
		$this->db_name = $this->agencia_rs['dbname'];
		$login = $this->agencia_rs['inmo_vreasy_user'];

		$this->auth( $login );

		$this->get_agencia_config();
	}


	/*
	 * Posar totes les reserves d'un immoble a vreasy
	 * Es crida just al insertar l'inmoble per primer cop
	 */
	function save_all_at_vreasy( $property_id, $vreasy_property_id ) {

		$this->get_agencia_config();

		$letnd_fields = implode( ',', $this->letnd_fields );
		$db_name = $this->db_name;
		$query            = "
			SELECT $letnd_fields, vreasy_id, book_owner 
			FROM $db_name.booking__book 
			WHERE property_id = $property_id
			AND date_out >= now()";

		$results = Db::get_rows( $query );

		foreach ( $results as $rs ) {
			$this->save_at_vreasy( $rs, $vreasy_property_id);
		}

	}

	/*
	 * Posar les reserves a vreasy
	 * Es crida al guardar una nova reserva per curl o al insertar inmoble nou
	 */
	function save_at_vreasy( $rs, $vreasy_property_id ) {

		$vreasy_id = $rs['vreasy_id'];
		$book_owner = $rs['book_owner'];
		$book_id = $rs['book_id'];

		// Només es pot editar a letnd les que son propietat letnd
		if ($book_owner == 'vreasy' || !$vreasy_property_id) {
			ExportCommon::mark_for_async( [$book_id], 'book', '1', $this->agencia_rs['dbname']);
			return;
		}

		$rs['property_id'] = $vreasy_property_id;
		$rs['status'] = $this->vreasy_status[ $rs['status'] ];

		$json = $this->get_vreasy_json( $rs );


		$api_url = $this->api_url . '/reservations';

		$is_update = $vreasy_id?true:false;
		// Update
		if ( $is_update ) {
			$api_url .= '/' . $vreasy_id;
			$response = Unirest\Request::put( $api_url, $this->headers, $json );
		}
		else {
			$response = Unirest\Request::post( $api_url, $this->headers, $json );
		}

		$this->log_json( $api_url, $json, $response );

		$code = $response->code;

		if ( $code >= 299 || $code < 200 ) {
			// ExportCommon::p( print_r( $response->body, true ), 'error' );
			$this->response_errors [] = [
				'error'   => 'insert_at_vreasy',
				'code'    => $code,
				'body'    => print_r( $response->body, true ),
				'book_id' => $book_id,
			];
			return;
		}

		$vreasy_id = $response->body->id;

		// AIXÒ FEIA PETAR L'EXPORTACIÓ A PARTIR D'UNES 10 RESERVES ->>>>>> ExportCommon::p( print_r( $response->body, true ), 2 );

		// Després d'insertar guardo l'id de vreasy
		if ( !$is_update ) $this->save_vreasy_book_id( $book_id, $vreasy_id, 'letnd' );

		ExportCommon::mark_for_async( [$book_id], 'book', '1', $this->agencia_rs['dbname']);

		// $this->save_payment_at_vreasy($rs, $vreasy_id);

	}
	/*
	 *
	 * M'obliga a posar la payer_user_id i payee_user_id, per tant no està fet ja que no sabem els usuaris
	 *
	function save_payment_at_vreasy($book, $book_id){

		$payment =
			[
				'reservation_id '      => $book_id,
				'amount'               => $book ['price_total'],
				'total'                => $book ['price_total'],
				'grand_total'          => $book ['price_total'],
				'user_payment_type_id' => 38130,
				'status'               => 'unpaid',
				'quantity'             => 1,

			];

		$json = json_encode( $payment );

		$api_url = $this->api_url . '/payments';

		$response = Unirest\Request::post( $api_url, $this->headers, $json );
		$this->log_json( $api_url, $json, $response );


		$code = $response->code;
		if ( $code >= 299 || $code < 200 ) {
			$this->response_errors [] = [
				'error'   => 'insert_at_vreasy',
				'code'    => $code,
				'body'    => print_r( $response->body, true ),
				'book_id' => $book_id,
			];
			return;
		}
	}*/

	function get_vreasy_json( $rs ) {

		$book = [];
		foreach ( $this->vreasy_fields as $letnd_field => $vreasy_field ) {
			$book [ $vreasy_field ] = $rs[ $letnd_field ];
		}
		unset($book [ 'id' ]);
		$book ['listor_id'] = 1;
		// Ho trec per easybrava, sino es reescriu el email i nom que han canviat a vreasy - - - - $book ['guest'] = $this->get_guest();

		return json_encode( $book );

	}

	private function save_vreasy_book_id($book_id, $vreasy_id, $book_owner) {
		$db_name = $this->db_name;
		$query = "UPDATE $db_name.booking__book
					SET vreasy_id = $vreasy_id,
					book_owner = '$book_owner'
					WHERE book_id = $book_id";
		Db::execute( $query );
	}
	public function get_guest() {
		return [
			"fname" => $this->agencia_rs['company_name'],
			"email" => $this->agencia_rs['default_mail']
		];
	}

	private function get_agencia_config() {

		ExportCommon::get_agencia_config( $this->agencia_rs, 'vreasy' );

	}

	private function auth( $login = '', $pass = '' ) {
		if ( ! $login ) $login = $this->agencia_rs['inmo_vreasy_user'];
		Unirest\Request::auth( $login, $pass );
	}

	private function log_json( $api_url, $json_sent, $json_response ) {
		ExportCommon::log_json( $api_url, $json_sent, $json_response, $this->agencia_rs, $this->exporter );
	}

	private function trace( $text, $name = '', $title = false ) {
		ExportCommon::trace( $text, $name, $title, 'vreasy_book' );
	}
}