<?
class SistemamuseuExhibition extends Module{
	var $group_id;
	var $is_museu;
	function __construct(){
		parent::__construct();
		$this->group_id = $_SESSION['group_id'];
		$this->is_museu = $this->group_id>3;
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function get_records()
	{
		$this->swap_fields('museu_id','exhibition_title');
	    $listing = new ListRecords($this);
		$listing->add_filter('kind');
		$listing->add_filter('topic_id','sistemamuseu__exhibition_to_topic');
		$listing->add_filter('status');
		//$listing->add_filter('topic_id');
		if ($this->is_museu) {
			$listing->set_field('museu_id','list_admin','no');
			$listing->set_field('destacat','list_admin','no');
			$listing->set_field('ordre','list_admin','no');
			$listing->condition  = 'museu_id = ' . $this->group_id;
		}
		else{			
			$listing->add_filter('museu_id');
		}
		$listing->order_by = 'exhibition_title ASC';
	    return $listing->list_records();
	}
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{
	    $show = new ShowForm($this);
		if ($this->is_museu){
			$show->set_field('museu_id','type','hidden');
			$show->set_field('destacat','type','hidden');
			$show->set_field('ordre','type','hidden');
		}
	    return $show->show_form();
	}

	function save_rows()
	{
	    $save_rows = new SaveRows($this);
	    $save_rows->save();
	}

	function write_record()
	{
		if ($this->is_museu){
			$this->set_field('museu_id','override_save_value',$this->group_id);
		}
	    $writerec = new SaveRows($this);
	    $writerec->save();
	}

	function manage_images()
	{
	    $image_manager = new ImageManager($this);
	    $image_manager->execute();
	}
}
?>