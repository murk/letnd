<?
/**
 * CEIGRUP
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) Gener 2016
 * @version 1
 * @access public
 */

/*

INFO Exportació portals

S'han agefit camps efficiency a apicat i ceigrup

	efficiency_number
	efficiency_number2
	modified

letnd.inmo__municipi -> es guarden les relacions de municipis

INFO FEATURES

	Obligatori preu i codi postal
	Port selva, vol que es espugin tots els inmobles ( no cal poder triar )
	Mostro l'adreça complerta o nomes carrer amb els mateixos criteris del mapa
	Es mostren els destacats

TODO-i S'hauria de controlar les imatges borrades, treure-les de la bbdd export__ceigrup_uploaded

PREGUNTAR A CEIGRUP

	Tamany imatges
	Categoria ( AltoStanding )

CANVIS EN BBDD


LETND NO TE

	Direccion_CodigoBarrio
	Direccion_CodigoDistrito
	Direccion_CodigoSigla
	Direccion_Escalera
	Categoria ( AltoStanding )

	Características:
		Està al arxiu Tablas envio.xlsx -> características

CEIGRUP NO TE




VARS

*/

class AdmintotalCeigrup extends Module {

	var $config_rs = [];
	var $export_rs = [];
	var $property_rs = [];
	var $agencia_rs = [];
	var $client_images = [];
	var $conta = 0;
	var $is_cli = false;


	/*
	IdTipoProducto	Descripcion
			1	Piso
			2	Casa
			3	Casa Rústica
			4	Local Comercial
			5	Oficina/Despacho
			6	Nave
			7	Trastero
			8	Parking
			9	Amarre
			10	Terreno
			11	Edificio
	*/
	var $ceigrup_categorys = [

		'1'  => '3', // Masia - Rústica
		'2'  => '2', // Casa de poble
		'3'  => '2', // Casa urbana
		'4'  => '1', // Pis
		'5'  => '10', // Terreny
		'7'  => '4', // Local
		'8'  => '2', // Obra nova
		'10' => '8', // Pàrquing
		'11' => '9', // Amarratge
		'12' => '1', // Apartament
		'13' => '2', // Xalet
		'14' => '1', // Àtic
		'15' => '1', // Dúplex
		'16' => '1', // Loft
		'17' => '4', // Nau
		'18' => '1', // Protecció oficial
		'19' => '2', // Ruïna
		'20' => '4', // Negoci
		'21' => '2', // Finca exclusiva
		'22' => '2', // Casa adosada
		'23' => '2', // Casa aparellada
		'24' => '2', // Casa unifamiliar
		'25' => '1', // Pis planta baixa
		'26' => '8', // Garatge
		'27' => '11', // Edifici

	];

	/*
	 * 1- Venta, 2- Alquiler
	 */
	var $ceigrup_tipus = [
		'sell'       => '1',
		'rent'       => '2',
		'temp'       => '2',
		'moblat'     => '2',
		'selloption' => '2',
	];

	var $ceigrup_anunci_languages = [
		'spa' => 'es',
		'cat' => 'ca',
		'eng' => 'en',
		'fra' => 'fr',
		'dut' => 'nl',
	];


	function __construct() {
		include_once( DOCUMENT_ROOT . 'admin/modules/admintotal/export_common.php' );
		parent::__construct();
	}
	/*
	function start() {
		$this->_start();
	}*/

	function _start() {

		ExportCommon::p( "INICI: " . date( "d-m-Y H:i" ), 1 );

		set_time_limit( 60 * 60 );        //ini_set('implicit_flush', true);		//error_reporting(E_WARNING);

		echo '<link href="/admin/themes/inmotools/styles/export.css" rel="stylesheet" type="text/css">';

		Db::connect_mother();

		$agencias = $this->get_agencias();

		$this->export_properties( $agencias );

		ExportCommon::show_end();

		send_mail_admintotal('CEIGRUP - Exportació finalitzada', 'Data: ' . now(true));

		die();
	}

	function get_config_results() {


		$query = "SELECT ceigrup_field, letnd_table, letnd_field, `function`, `condition`
							FROM export__ceigrup
							WHERE ceigrup_table <>  ''";

		return Db::get_rows( $query );

	}

	function get_agencias() {

		/*$query    = "SELECT client_id, comercialname, nif, inmo_apicat_code, province, town, typestreet, adress, numstreet, zip, respname, surnames, phone1, fax, mail1, domain, dbname, client_dir FROM letnd.client__client WHERE inmo_apicat = 1";
		$agencias = Db::get_rows( $query );*/

		// TODO-i Ara es nomes per port selva
		$query    = "SELECT client_id, comercialname, inmo_ceigrup_code, inmo_ceigrup_user, inmo_ceigrup_pass, domain, dbname, client_dir FROM letnd.client__client WHERE client_id = 102";
		$agencias = Db::get_rows( $query );

		return $agencias;

	}


	function export_properties( $agencias ) {

		// TEST
		/*for ( $i = 0; $i < 5; $i ++ ) {
			$agencias = array_merge( $agencias, $agencias );
		}*/
		foreach ( $agencias as $rs ) {

			$this->client_images = [];

			$inmo_ceigrup_code = $rs['inmo_ceigrup_code'];
			$xml               = $this->get_XMLWRITER( $inmo_ceigrup_code, 'Productos' );

			$xml->writeElement( 'CodigoTAAF', $inmo_ceigrup_code );

			$this->agencia_rs =  &$rs;

			$condition = $this->get_agencia_condition();

			$this->build_agencia_xml( $xml, $condition );

			$xml->endElement(); // Productos
			$xml->flush();
			unset( $xml );

			$this->upload_files();

		}
	}

	private function get_agencia_condition() {

		$this->get_agencia_config();

		$condition = "(status = 'onsale')
				 AND bin = 0
				 AND country_id = '1'
				 AND price > 0
				 AND price_consult = 0
                 AND zip != ''";

		// TODO-i Posar el field ceigrup a inmo__property_to_portal
		if ( ! $this->agencia_rs['ceigrup_export_all'] ) {
			$condition .= "
				AND property_id IN (
					SELECT property_id
					FROM `" . $this->agencia_rs['dbname'] . "`.inmo__property_to_portal
					WHERE ceigrup = 1
				)
			";
		}

		ExportCommon::p( "Consulta immobles: " . $this->agencia_rs['comercialname'], 1 );
		ExportCommon::p( $condition, 3 );

		return $condition;

	}

	private function build_agencia_xml( &$xml, $condition ) {

		ExportCommon::p( "Extracció d'inmobles: " . $this->agencia_rs['comercialname'], 1 );
		ExportCommon::p( "Exportar tots: " . ( $this->agencia_rs['ceigrup_export_all'] ? 'Sí' : 'No' ), 3 );

		$query   = "
			SELECT * FROM `" . $this->agencia_rs['dbname'] . "`.inmo__property
			WHERE " . $condition;
		$results = Db::get_rows( $query );

		ExportCommon::p( "Total immobles a exportar: " . count( $results ), 3 );
		$total_property_exported = 0;


		$config_results = $this->get_config_results();

		foreach ( $results as $key => $rs ) {

			$this->property_rs = &$rs;
			$this->export_rs   = [];

			$has_text = $this->get_anunci();

			if (!$has_text) continue;
			$total_property_exported++;

			$this->export_rs['Caracteristicas'] = [];
			$this->get_images();


			// Altres Valors
			$this->export_rs['CodigoDelegacion'] = '1';
			// TODO-i Posr el mail del client? o deixar el meu?
			$this->export_rs['EmailResumenCarga'] = 'dev@letnd.com';
			$this->get_contact_details();

			foreach ( $config_results as $config_rs ) {

				$this->config_rs = &$config_rs;
				$letnd_val   = $rs[ $config_rs['letnd_field'] ];

				// Si val es 0 tampoc exporta res
				if ( $config_rs['function'] ) {
					$val = $this->{$config_rs['function']}( $letnd_val );
					if ( $val ) {
						$this->export_rs[ $config_rs['ceigrup_field'] ] = $val;
					}
				} else {
					if ( $letnd_val ) {
						$this->export_rs[ $config_rs['ceigrup_field'] ] = $letnd_val;
					}
				}
			}
			$this->config_rs = [];

			$this->write_xml( $xml );

			ExportCommon::p( $rs['ref'] . " , id: " . $rs['property_id'], 2 );
		}


		ExportCommon::p( "Total immobles exportats: " . $total_property_exported, 3 );

		$this->property_rs = [];
		$this->export_rs   = [];

	}


	function write_xml( &$xml ) {

		$xml->startElement( 'Producto' );

		foreach ( $this->export_rs as $k => $v ) {
			if ( $k == 'Caracteristicas' ) {
				if ( $v ) {
					$xml->startElement( 'Caracteristicas' );
					foreach ( $v as $ca ) {
						$xml->startElement( 'Caracteristica' );
						$xml->writeElement( 'Id', $ca['Id'] );
						$xml->writeElement( 'Valor', $ca['Valor'] ? $ca['Valor'] : null );

						$xml->endElement();
					}
					$xml->endElement();

				}
			} elseif ( $k == 'Textos' ) {
				if ( $v ) {
					$xml->startElement( 'Textos' );
					foreach ( $v as $te ) {
						$xml->startElement( 'Texto' );
						foreach ( $te as $te_key => $te_val ) {
							$xml->startElement( $te_key );
							$xml->writeRaw( $te_val );
							$xml->endElement();
						}

						$xml->endElement();
					}
					$xml->endElement();

				}
			} elseif ( $k == 'Imagenes' ) {
				$xml->startElement( 'Imagenes' );
				foreach ( $v as $im ) {
					$xml->startElement( 'Imagen' );
					$xml->writeElement( 'Id', $im['Id'] );
					$xml->writeElement( 'Url', $im['Url'] );
					$xml->endElement();
				}
				$xml->endElement();
			} else {
				// Write element fa htmlentities i cambia salts de linea per &#13;
				// text també fa hmlspecialchars ???
				// $xml->writeElement( $k, $v ? $v : null );
				/*$xml->startElement( $k );
				if ($v)
					$xml->text( $v );
				$xml->endElement();*/

				$xml->startElement( $k );
				if ( $v ) {
					// $v = htmlspecialchars( $v, ENT_COMPAT);

					// Anuncio ja te el cdata
					if ( substr( $k, 0, 7 ) == 'Anuncio' ) {

					} else {
						$v = str_replace( '"', '‘', $v );
						$v = str_replace( "'", '‘', $v );
						$v = str_replace( '&', '&amp;', $v );
					}

					$xml->writeRaw( $v );
				}
				$xml->endElement();

			}
		}

		$xml->endElement();
		if ( $this->conta % 1000 == 0 && $this->conta != 0 ) {
			$xml->flush();
			ExportCommon::p( 'FLUSH', 1 );
		}
	}

	function get_XMLWriter( $file, $element ) {
		global $gl_is_local;
		$xml = new XMLWriter();
		$xml->openUri( ( $gl_is_local ? DOCUMENT_ROOT : "/var/www/letnd.com/datos/motor/" ) . 'data/ceigrup/' . $file . '.xml' );
		$xml->startDocument( '1.0', 'UTF-8' );
		//if ( $GLOBALS['gl_is_local'] ) { // És més fàcil per depurar
		$xml->setIndent( true );
		$xml->setIndentString( "\t" );
		//}
		$xml->startElement( $element );
		$xml->writeAttribute( 'xmlns:xsi', "http://www.w3.org/2001/XMLSchema-instance" );
		$xml->writeAttribute( 'xmlns:xsd', "http://www.w3.org/2001/XMLSchema" );

		return $xml;

	}

	private function get_agencia_config() {

		ExportCommon::get_agencia_config( $this->agencia_rs, 'habitaclia' );

		$agencia = &$this->agencia_rs;

		// TODO Posar a la configuració si s'exporta tot
		$agencia['ceigrup_export_all'] = '1';


	}

	private function get_contact_details() {

		$this->export_rs['Contacto_Nombre']   = $this->agencia_rs['company_name'];
		$this->export_rs['Contacto_Email']    = $this->agencia_rs['default_mail'];
		$this->export_rs['Contacto_Telefono'] = $this->agencia_rs['page_phone'];

	}

	// Funcions per camps
	function get_category( $val ) {
		return $this->ceigrup_categorys [ $val ];
	}

	function get_anunci() {

		$id    = $this->property_rs['property_id'];
		$query = "SELECT
				language, description, property, page_title, page_description, page_keywords FROM `" . $this->agencia_rs['dbname'] . "`.inmo__property_language
			 WHERE property_id = '" . $id . "'";

		$results = Db::get_rows( $query );

		if ( $results ) {

			$this->export_rs ['Textos'] = [];
			foreach ( $results as $rs ) {
				$ceigrup_language = isset( $this->ceigrup_anunci_languages[ $rs['language'] ] ) ? $this->ceigrup_anunci_languages[ $rs['language'] ] : false;
				if ( $ceigrup_language !== false && $rs ['description'] ) {
					$ex_rs                        = [];
					$ex_rs ['Idioma']             = $ceigrup_language;
					$ex_rs ['Descripcion']        = '<![CDATA[' . $rs['description'] . ']]>';
					$ex_rs ['Titulo']             = '<![CDATA[' . $rs['property'] . ']]>';
					$ex_rs ['TituloMeta']         = '<![CDATA[' . $rs['page_title'] . ']]>';
					$ex_rs ['DescripcionMeta']    = '<![CDATA[' . $rs['page_description'] . ']]>';
					$ex_rs ['PalabrasClave']      = '<![CDATA[' . $rs['page_keywords'] . ']]>';
					$this->export_rs ['Textos'][] = $ex_rs;
				}
			}
		}

		if ( ! $this->export_rs ['Textos'] ) {
			unset ( $this->export_rs ['Textos'] );
			return false;
		}
		return true;
	}

	function get_images() {

		$id     = $this->property_rs['property_id'];
		$query  = "SELECT
				image_id, name
				FROM `" . $this->agencia_rs['dbname'] . "`.inmo__property_image
				WHERE property_id = " . $id . "
				ORDER BY main_image DESC, ordre";
		$images = Db::get_rows(
			$query );

		if ( $images ) {
			$this->export_rs ['Imagenes'] = [];

			$ordre = 0;

			foreach ( $images as $image ) {

				// CodigoPublicador-CodigoProducto-Orden.jpg
				$ext      = strrchr( substr( $image['name'], - 5, 5 ), '.' );
				$image_id = $image['image_id'];
				$ordre ++;

				$url        = $this->agencia_rs['inmo_ceigrup_code'] . '-' . $image_id . $ext;
				$local_file = $image_id . '_medium' . $ext;

				$this->export_rs ['Imagenes'][] = [
					'Id'  => $ordre,
					'Url' => $url
				];
				$this->client_images []         = [
					'local_file'  => $local_file, // per pujar per ftp al acabar
					'remote_file' => $url, // per pujar per ftp al acabar
				];
			}

		}
	}

	function get_referencia( $ref ) {
		return ExportCommon::get_ref( $ref, $this->property_rs, $this->agencia_rs );
	}

	function get_tipus( $val ) {
		return $this->ceigrup_tipus [ $val ];
	}

	function get_country( $val ) {
		return '11'; // nomes espanya
	}

	function get_municipi( $val ) {
		$val = Db::get_first( "SELECT ceigrup_id FROM inmo__municipi WHERE municipi_id = " . $val );

		return $val;
	}

	function get_address( $val ) {

		$rs = &$this->property_rs;

		// Mostro l'adreça complerta o nomes carrer amb els mateixos criteris del mapa
		// Modo de visualización de la dirección. 1-Dirección completa | 2-Solo calle | 3-Solo Zona  -->
		// Modo de visualización en el mapa. 1-Mostrar punto | 2-No mostrar punto | 3-No mostrar mapa  -->
		if ( $rs['show_map'] && $rs['show_map_point'] ) {
			$this->export_rs['Direccion_ModoDireccion'] = '1';
			$this->export_rs['Direccion_ModoMapa']      = '1';
		} elseif ( $rs['show_map'] ) {
			$this->export_rs['Direccion_ModoDireccion'] = '2';
			$this->export_rs['Direccion_ModoMapa']      = '2';
		} else {
			$this->export_rs['Direccion_ModoDireccion'] = '3';
			$this->export_rs['Direccion_ModoMapa']      = '3';
		}

		return $val;
	}


	function get_latitude_longitud( $val ) {
		if ( ! $this->property_rs['show_map_point'] || ! $val || $val == '0.0000000' ) {
			return false;
		} else {
			return $val;
		}
	}

	function get_bathroom( $val ) {
		if ( $this->property_rs['wc'] ) {
			$val = (int) $val + (int) $this->property_rs['wc'];
		}

		return $val;
	}

	function get_efficiency( $val ) {
		if ( $val == 'progress' ) {
			$val = 'ZZ';
		} elseif ( $val == 'exempt' ) {
			$val = 'X';
		} elseif ( $val == 'notdefined' ) {
			$val = false;
		}


		return $val;
	}

	function get_date( $val ) {
		return date( "d/m/Y", strtotime( $val ) );
	}

	// SI es un terreny, la superfície pot ser "land"
	function get_floor_space( $val ) {

		if ( ! $val && $this->property_rs['category_id'] == '5' ) {
			$val = $this->property_rs['land'];
		}

		return (int)$val;

	}

	/* NO FA FALTA, ho guarda tot amb metres
	function get_land( $val ) {

		if ( $this->property_rs['land_units'] == 'ha' ) {
			$val = $val / 10000;
		}

		return (int)$val;
	}*/

	function get_parking( $val ) {
		$garage = $this->property_rs['garage'];

		return $this->get_caracteristica( $val || $garage );
	}

	function get_view( $val ) {
		$clear_view = $this->property_rs['clear_view'];

		return $this->get_caracteristica( $val || $clear_view );
	}

	function get_tipus2( $val ) {
		$val = ( $val == 'new' ) ? '1' : '0';

		return $this->get_caracteristica( $val );
	}

	// Genériques

	function get_int( $val ) {
		return (int)$val;
	}


	function get_caracteristica( $val ) {

		if ( ! $val ) {
			return false;
		}
		if ( $this->config_rs['condition'] ) {
			$condition = eval( "return " . $this->config_rs['condition'] . ";" );
			// Debug::p( $condition, $this->config_rs['condition'] . ' - ' . $val);
			if ( ! $condition ) {
				return false;
			}
			// Debug::p( 'Ha passat condition' );
		}

		$this->export_rs['Caracteristicas'][] = [
			'Id'    => $this->config_rs['ceigrup_field'],
			'Valor' => $val
		];

		return false;
	}


	// FTP
	private function upload_files() {
		global $gl_ftp_ceigrup, $gl_is_local;

		if ( $gl_is_local ) {
			$local_dir  = DOCUMENT_ROOT . 'data/ceigrup/';
			$remote_dir = '/letnd.com/datos/motor/data/ceigrup/proves/';
		} else {
			$local_dir  = '/var/www/letnd.com/datos/motor/data/ceigrup/';
			$remote_dir = '';
		}
		$file_name = $this->agencia_rs['inmo_ceigrup_code'] . '.xml';

		// no pujo si es local
		// if ( $gl_is_local ) return;

		if ( $gl_is_local ) {
			$conn_id = get_ftp_letnd_global();
		} else {
			// Pujo per ftp
			$conn_id = ftp_connect( $gl_ftp_ceigrup['server'] );

			$inmo_ceigrup_user = $this->agencia_rs['inmo_ceigrup_user'];
			$inmo_ceigrup_pass = $this->agencia_rs['inmo_ceigrup_pass'];

			$login_result = ftp_login( $conn_id, $inmo_ceigrup_user, $inmo_ceigrup_pass );
			// turn passive mode on
			ftp_pasv( $conn_id, true );

		}
		if ( ftp_put( $conn_id, $remote_dir . $file_name, $local_dir . $file_name, FTP_BINARY ) ) {
			ExportCommon::p( "Pujat correctament: $file_name\n", 2 );
		} else {
			ExportCommon::p( "NO s'ha pogut pujar: $file_name\n", 'error' );
			send_mail_admintotal( 'CEIGRUP, a fallat la transferencia d\'arxius' );
		}

		$this->upload_images( $conn_id, $remote_dir, $local_dir );

		ftp_close( $conn_id );

	}


	/**
	 * @param $conn_id
	 * @param $remote_dir
	 *
	 * He provat de fer que no es sobrescrigui l'arxiu al ftp remot, però amb la funció ftp_put no es pot, sobrescriu sempre
	 *
	 * He probat amb l'opció de file_exists, posant ftp://user:pass@servidor.com/$remote_dir$client_image, però és molt lent, ja que cada cop obre la connexió per ftp ( tardava 115 s. des del servidor )
	 *
	 * Per tant, la millor solució es guardar en BBDD totes les imatges pujades
	 *
	 *
	 */
	private function upload_images( $conn_id, $remote_dir ) {

		global $gl_is_local;

		$remote_dir .= 'Imagenes/';

		if ( $gl_is_local ) {
			$local_dir = DOCUMENT_ROOT;
		} else {
			// TODO-i En alguns clients no funcionarà ja que el domain1 es diferent
			$local_dir = '/var/www/' . $this->agencia_rs['domain'] . '/datos/web/';


			if ( ! is_dir( $local_dir ) ) {
				send_mail_admintotal( 'CEIGRUP - No existeix el directori del client', $local_dir );
			}
		}

		$local_dir .= $this->agencia_rs['client_dir'] . '/inmo/property/images/';

		ExportCommon::p( "Pujant imatges per FTP:", 1 );

		$conta = 1;

		foreach ( $this->client_images as $rs ) {

			$local_file  = $rs['local_file'];
			$remote_file = $rs['remote_file'];

			$query       = "SELECT remote_file FROM export__ceigrup_uploaded WHERE remote_file = '$remote_file'";
			$file_exists = Db::get_first( $query );

			if ( $file_exists ) {
				ExportCommon::p( "Imatge ja existent a la BBDD: $remote_file \n", 2 );
			} else {
				if ( ftp_put( $conn_id, $remote_dir . $remote_file, $local_dir . $local_file, FTP_BINARY ) ) {
					ExportCommon::p( "Pujat correctament: $remote_file\n", 2 );
					$query = "INSERT INTO export__ceigrup_uploaded (remote_file) VALUES ('$remote_file');";
					Db::execute( $query );
				} else {
					ExportCommon::p( "NO s'ha pogut pujar la imatge: $remote_file\n", 'error' );
				}
			}
			// $conta++;if ($conta>10) break;
		}

	}




	/*
	 *
	 * UTILITATS BBDD
	 *
	 *
	 *
	 */

	/**
	 * Posa id del municipi de ceigrup a la taula inmo__municipi
	 * /admin/?tool=admintotal&tool_section=ceigrup&action=update_municipis
	 *
	 * Comprovant la mateixa provincia, ( agafo el que te id més baix ):
	 *  Montagut 2 resutats
	 *  Gallifa 3
	 *  Viladecans 2
	 *  Canyelles 2
	 *
	 *
	 */
	function update_municipis() {

		Db::connect_mother();
		$query = "SELECT * FROM inmo__municipi ORDER BY municipi";

		$results_municipis = Db::get_rows( $query );
		$conta             = 0;

		foreach ( $results_municipis as $rs ) {
			$municipi_id = $rs['municipi_id'];
			$municipi    = Db::qstr( $rs['municipi'] );
			$comarca_id  = $rs['comarca_id'];

			$query = "SELECT Poblacion, IdPoblacion FROM export__ceigrup_poblacion WHERE Poblacion = $municipi AND IdProvincia = (SELECT provincia_id FROM inmo__comarca WHERE comarca_id = $comarca_id) ORDER BY IdPoblacion ASC LIMIT 1";


			$results = Db::get_rows( $query );
			$found   = false;
			if ( $results ) {
				/*
				 * Per comprobar repetits, treure el LIMIT de la consulta
				if ( count( $results ) > 1 ) {
					$poblacions = implode_field( $results, 'Poblacion' );
					ExportCommon::p( $poblacions, 1 );
				}
				*/
				$found = true;
				$this->update_municipi_id( $results, $municipi_id );

			}
			if ( ! $found ) {
				$municipi_array = explode( ',', $rs['municipi'] );

				// Busco els compostos
				if ( isset ( $municipi_array[1] ) ) {

					$sep = strpos( $municipi_array[1], "'" ) === false ? ' ' : '';

					$municipi2 = trim( $municipi_array[1] ) . $sep . trim( $municipi_array[0] );
					$municipi2 = Db::qstr( $municipi2 );


					$query = "SELECT Poblacion, IdPoblacion FROM export__ceigrup_poblacion WHERE Poblacion = $municipi2 AND IdProvincia = (SELECT provincia_id FROM inmo__comarca WHERE comarca_id = $comarca_id) ORDER BY IdPoblacion ASC LIMIT 1";

					$results = Db::get_rows( $query );
					if ( $results ) {
						$found = true;
						$this->update_municipi_id( $results, $municipi_id );
					} else {
						ExportCommon::p( "$conta - $municipi2 - No s'ha trobat", 1 );
					}

				} else {
					// ExportCommon::p( "$conta - $municipi - No s'ha trobat compost", 1 );
				}
			}
			if ( ! $found ) {
				$conta ++;
				ExportCommon::p( "$conta - $municipi - No s'ha trobat", 1 );
			}

		}
		die();
	}

	private function update_municipi_id( &$results, $municipi_id ) {

		$IdPoblacion = $results[0]['IdPoblacion'];
		$query       = "UPDATE inmo__municipi SET ceigrup_id = $IdPoblacion WHERE municipi_id = $municipi_id";
		Db::execute( $query );

	}


}