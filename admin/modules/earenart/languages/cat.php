<?
include (DOCUMENT_ROOT . 'admin/modules/web/languages/cat.php');

$gl_caption['c_import_preview'] = 'Previsualitzar importació';
$gl_caption['c_import'] = 'Importar';
$gl_caption['c_import_title'] = 'Importar articles';
$gl_caption['c_select_file'] = 'Seleccionar arxiu';
?>