<?

/* 25-5-16

	MISSATGES - Afegeixo tipus letnd per missatges de letnd a tots els clients
 */

$update_sql [] = "ALTER TABLE `missatge__missatge`
	CHANGE COLUMN `kind` `kind` ENUM('mls','general','letnd') NULL DEFAULT 'general' COLLATE 'utf8_spanish2_ci' AFTER `status`;"

?>