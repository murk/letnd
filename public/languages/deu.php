<?
// blocks

define('INMO_PRICE_FIRST','Bis %s&nbsp;'.CURRENCY_NAME);
define('INMO_PRICE_MIDDLE','%s bis %s&nbsp;'.CURRENCY_NAME);
define('INMO_PRICE_LAST','Über %s&nbsp;'.CURRENCY_NAME);
define('INMO_FLOOR_SPACE_FIRST','Bis %s m<sup>2</sup>');
define('INMO_FLOOR_SPACE_MIDDLE','%s bis %s m<sup>2</sup>');
define('INMO_FLOOR_SPACE_LAST','Über %s m<sup>2</sup>');
define('INMO_PERSON_FIRST','Bis %s');
define('INMO_PERSON_MIDDLE','%s bis %s');
define('INMO_PERSON_LAST','Über %s');
define ('LETND','Letnd');

$gl_caption['c_offers'] = 'Angebote';


// captions

$gl_caption['c_send_mail'] = 'E-Mail verschicken';
$gl_caption['c_more_info'] = 'Weitere Informationen';
$gl_caption['c_read_more'] = 'Weiterlesen';
$gl_caption['c_title_search'] = 'Suche';
$gl_caption['c_yes'] = 'Ja';
$gl_caption['c_no'] = 'Nein';
$gl_caption['c_send'] = 'Verschicken';
$gl_caption['c_clear'] = 'Löschen';
$gl_caption['c_send_to_friend']='An einen Freund verschicken';
$gl_caption['c_print']='Drucken';
$gl_caption['c_zoom_link']='zoomen auf';
$gl_caption['c_contact_link']='kontact';

// bloc inmo_categories
$gl_caption['c_sell'] = 'Verkauf';
$gl_caption['c_rent'] = 'Vermietung';
$gl_caption['c_temp'] = 'Touristen mieten';
$gl_caption['c_moblat'] = 'Miete möblierte';
$gl_caption['c_selloption'] = 'Miete Kaufoption';

// seo
$gl_caption['c_seo_verd_1']='<a href="http://www.letnd.com" target="_blank" title="Diseño páginas web en Girona">Diseño y programación web:<img src="/common/images/logoletnd.png" width="95" height="16" alt="Letnd: Diseño web, posicionamiento web, páginas web en Girona" style="vertical-align:text-bottom" /></a>';
$gl_caption['c_seo_verd_2']='<a href="http://www.letnd.com" target="_blank" title="Web site design in Girona">Web programming:<img src="/common/images/logoletnd.png" width="95" height="16" alt="Letnd: Web design, web position, web pages girona" style="vertical-align:text-bottom" /></a>';
$gl_caption['c_seo_verd_3']='<a href="http://www.letnd.com" target="_blank" title="Diseño páginas web en Girona">Creado por:<img src="/common/images/logoletnd.png" width="95" height="16" alt="Letnd: Diseño web, posicionamiento web, páginas web en Girona" style="vertical-align:text-bottom" /></a>';
$gl_caption['c_seo_verd_4']='<a href="http://www.letnd.com" target="_blank" title="Diseño páginas web en Girona">Diseño web:<img src="/common/images/logoletnd.png" width="95" height="16" alt="Letnd: Diseño web, posicionamiento web, páginas web en Girona" style="vertical-align:text-bottom" /></a>';

$gl_caption['c_seo_blanc_1']='<a href="http://www.letnd.com" target="_blank" title="Diseño páginas web en Girona">Diseño y programación web:<img src="/common/images/logoblanc.png" width="95" height="16" alt="Letnd: Diseño web, posicionamiento web, páginas web en Girona" style="vertical-align:text-bottom" /></a>';
$gl_caption['c_seo_blanc_2']='<a href="http://www.letnd.com" target="_blank" title="Diseño páginas web en Girona">Programación web:<img src="/common/images/logoblanc.png" width="95" height="16" alt="Letnd: Diseño web, posicionamiento web, páginas web en Girona" style="vertical-align:text-bottom" /></a>';
$gl_caption['c_seo_blanc_3']='<a href="http://www.letnd.com" target="_blank" title="Diseño páginas web en Girona">Creado por:<img src="/common/images/logoblanc.png" width="95" height="16" alt="Letnd: Diseño web, posicionamiento web, páginas web en Girona" style="vertical-align:text-bottom" /></a>';
$gl_caption['c_seo_blanc_4']='<a href="http://www.letnd.com" target="_blank" title="Diseño páginas web en Girona">Diseño web:<img src="/common/images/logoblanc.png" width="95" height="16" alt="Letnd: Diseño web, posicionamiento web, páginas web en Girona" style="vertical-align:text-bottom" /></a>';

$gl_caption['c_seo_custom_2']='<a href="http://www.letnd.com" target="_blank" title="Diseño páginas web en Girona">Programación web:<img src="/'.CLIENT_DIR.'/templates/images/letnd.png" alt="Letnd: Diseño web, posicionamiento web, páginas web en Girona" /></a>';
$gl_caption['c_seo_custom_4']='<a href="http://www.letnd.com" target="_blank" title="Diseño páginas web en Girona">Diseño web:<img src="/'.CLIENT_DIR.'/templates/images/letnd.png" alt="Letnd: Diseño web, posicionamiento web, páginas web en Girona" /></a>';

$gl_caption['c_tercersetena'] = 'Diseño <a class="tercersetena" target="_blank" href="http://www.tercersetena.com">Tercersetena.com</a>';

// NO TRADUIT
$gl_caption['c_tipus_temp'] = 'Saisonmiete';
$gl_caption['c_tipus_moblat'] = 'Miete möblierte';
// FI NO TRADUIT

// TAMBÉ A ADMIN
// Caption default
$gl_caption['c_form_title'] = 'Daten bearbeiten';
$gl_caption['c_config_title'] = 'Einstellung bearbeiten';
$gl_caption['c_edit'] = 'Bearbeiten';


$gl_caption['c_edit_button'] = 'Daten bearbeiten';
$gl_caption['c_view_button'] = 'Daten anzeigen';
$gl_caption['c_edit_images_button'] = 'Bilder bearbeiten';
$gl_caption['c_add_images_button'] = 'Bilder anhängen';



$gl_caption['c_edit_data'] = 'Daten bearbeiten';
$gl_caption['c_edit_images'] = 'Bilder';
$gl_caption['c_add_images'] = 'Bilder anhängen';
$gl_caption['c_search_form'] = 'Suchen';
$gl_caption['c_send_new'] = 'Speichern';
$gl_caption['c_send_edit'] = 'Änderungen speichern';
$gl_caption['c_delete'] = 'Löschen';
$gl_caption['c_action'] = 'Aktion';
$gl_caption['c_delete_selected'] = 'Auswahl löschen';
$gl_caption['c_restore_selected'] = 'Auswahl wiederherstellen';
$gl_caption['c_confirm_deleted'] = 'Die ausgewählten Elemente werden endgültig aus der Datenbank gelöscht. \\n\\nMöchten Sie fortfahren?';
$gl_caption['c_confirm_images_deleted'] = 'Die ausgewählten Bilder werden endgültig gelöscht. \\n\\nMöchten Sie fortfahren?';
$gl_caption['c_confirm_form_bin'] = 'Das Verzeichnis wird in den Papierkorb verschoben. \\n\\nMöchten Sie fortfahren?';
$gl_caption['c_confirm_form_deleted'] = 'Das Verzeichnis wird endgültig aus der Datenbank gelöscht. \\n\\nMöchten Sie fortfahren?';
$gl_caption['c_move_to'] = 'Verschiebung nach:';
$gl_caption['c_print_all'] = 'Alles drucken';
$gl_caption['c_print'] = 'Drucken';
$gl_caption['c_order_by'] = 'Ordnen nach:';
$gl_caption['c_select'] = 'Auswählen';
$gl_caption['c_all'] = 'alle';
$gl_caption['c_nothing'] = 'keine';
$gl_caption['c_preview'] = 'Voranzeige';
$gl_caption['c_yes'] = 'Ja';
$gl_caption['c_no'] = 'Nein';
$gl_caption['c_options_bar'] = 'Optionen';
$gl_caption['c_file_maximum'] = 'jeweils maximal %s Dateien';
$gl_caption['c_file_maximum_1'] = 'jeweils maximal 1 Datei';


































$gl_caption['c_cookies_message'] = 'This website uses proprietary and third-party cookies to improve the browsing experience as well as for analysis. By continuing browsing, we understand that you agree the';
$gl_caption['c_cookies_message_more'] = 'use of cookies';
$gl_caption['c_cookies_message_accept'] = 'Accept';

$gl_caption['c_cookies_text'] = '

<p class="accept-cookies-title">What are cookies?</p>
<p>A cookie is a small file stored on the user\'s computer that allows us to recognize it. The set of cookies help us improve the quality of our website, allowing us to monitor which pages are useful to the website users or and which are not.</p>
<p>Cookies are essential for the operation of the Internet, providing innumerable advantages in the provision of interactive services, providing us with the navigation and usability of our website. Please note that cookies can not harm your computer and, if enabled, they help us identify and resolve errors.</p>
<p class="accept-cookies-title">What kind of cookies are used in this site?</p>
<p>Propietary cookies: those that are sent to the user\'s computer from a computer or domain managed by the publisher and from which provides the service requested by the user.</p>
<p>Third party cookies:those that are sent to the user\'s computer from a computer or domain not managed by the publisher but by another entity that analyzes the data collected. .</p>
<p>Persisting cookies: a type of cookie in which data are still stored in the terminal and can be accessed and processed by the cookie\'s responsible.</p>
<p>Analytics cookies: using web analytics to get information on the number of users accessing the site, number of pages viewed, and repetition frequency of visits, the duration, the browser used, operator who provides the service, the language used by the terminal or the city to which is assigned the IP address. Information that allows a better and more appropriate service by this website.</p>
<p class="accept-cookies-title">Accept the use of cookies.</p>
<p>We assume that you accept cookies. However, you can restrict, block or erase cookies from this site or any page from the browser settings. </p>
<p>Next you can find the main browsers help pages:</p>
<ul>
<li>Internet Explorer: <a href="http://windows.microsoft.com/es-xl/internet-explorer/delete-manage-cookies#ie="ie-10"" target="_blank">windows.microsoft.com/es-xl/internet-explorer/delete-manage-cookies#ie="ie-10"</a></li>
<li>FireFox: <a href="http://support.mozilla.org/es/kb/Borrar%20cookies" target="_blank">support.mozilla.org/es/kb/Borrar%20cookies</a></li>
<li>Chrome: <a href="http://support.google.com/chrome/answer/95647?hl="es"" target="_blank">support.google.com/chrome/answer/95647?hl="es"</a></li>
<li>Safari: <a href="http://www.apple.com/es/privacy/use-of-cookies/" target="_blank">www.apple.com/es/privacy/use-of-cookies/</a></li>
</ul>
';

// contrasenyes
$gl_caption['c_password_forgotten'] = 'Haben Sie Ihr Password vergessen?';
$gl_caption['c_password_insert_mail'] = 'Geben Sie bitte ihre E-mail Adresse ein und wir werden Ihnen ein neues Password senden';
$gl_caption['c_password_insert_mail_exists'] = 'Wenn Sie Ihr Passwort vergessen haben, klicken Sie bitte akzeptieren und wir weden Ihnen ein neues zu Ihrem E-Mail-Konto schicken.';
$gl_caption['c_password_mail_error'] = 'Die e-mail Adresse exisitiert nicht in unserer Datenbank, schreiben Sie bitte Ihre korrekte E-mail Adresse';
$gl_caption['c_password_sent'] = 'Wir haben Ihnen ein neues Password an ihre E-mail Adresse gesendet';
$gl_caption['c_password_mail_subject'] = 'Ihr neues Password';
$gl_caption['c_password_mail_text_1'] = 'Ihr neues Password ist';
$gl_caption['c_password_mail_text_2'] = 'Sie können Ihr Password auf unserer neuen Website ändern';
$gl_caption['c_welcome'] = 'Willkomm';

$gl_caption['c_new_custumer_exists'] = 'Sie haben bereits eine Person mit diesem Vorname, Familienname oder Geburtsdatum eingegeben. Sie dürfen:';
$gl_caption['c_new_custumer_exists_change_name'] = 'Korrigieren den Namen oder Geburtsdatum';
$gl_caption['c_new_custumer_exists_return'] = 'Gehen an die Reservierung zurück, und wählen eine Person auf der Liste';
?>