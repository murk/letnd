<?php
function save_rows()
{
    $notify_ids = get_not_unactived();
    $save_rows = new SaveRows;
    $save_rows->save();
    notify_not_unactived($notify_ids);
}

function list_records()
{
    $listing = new ListRecords;
    $listing->has_bin = false;
    $listing->order_by = 'ordre asc, category asc';
    $listing->set_options (1, 0, 1, 1, 0, 1, 1, 0, 1); // $options_bar = 1, $options_checkboxes = 1, $save_button = 1, $select_button = 1, $delete_button = 1, $print_button = 1, $edit_buttons = 1, $image_buttons = 1, $split_count = 1
    $listing->add_button ('property_button', 'action=list_records_property', 'boto1', 'after');
    $listing->list_records();
}

function show_form()
{
    $show = new ShowForm;
    $show->has_bin = false;
    $show->show_form();
}

function write_record()
{
    $notify_ids = get_not_unactived();
    $writerec = new SaveRows;
    $writerec->save();
    notify_not_unactived($notify_ids, true);
}

function manage_images()
{
    $image_manager = new ImageManager;
    $image_manager->execute();
}
function get_not_unactived()
{

    $notify_ids = array();
    $query_ids = array();
    // borro o no tots els registres d'altres taules relacionats amb l'apartat
    // miro també quins ids no puc borrar, quan action = notify
    // primer faig el bucle pel notify així descarto els ids que no puc borrar
    foreach($_POST['category_id'] as $key => $value)
    {
        if (!isset($_POST['active'][$key]))
        {
            $query_ids[$key] = $key;
        }
    }
    if ($query_ids)
    {
        $query = 'SELECT DISTINCT category_id
							FROM inmo__property
							WHERE category_id IN (' . implode(',', $query_ids) . ')';
        $results = Db::get_rows($query);
        foreach($results as $rs)
        {
            $_POST['active'][$rs['category_id']] = '1';
            $notify_ids[] = $rs['category_id'];
        }
    }
    return $notify_ids;
}
function notify_not_unactived($notify_ids, $write_records = false)
{
	if (!$notify_ids) return;
	global $gl_messages;
    $url_query = parse_url($_SERVER["HTTP_REFERER"]);
    $url_query = $url_query['query'];
    $_GET = array();
    parse_str($url_query, $_GET);
    $js = "top.window.location = '?marked_ids=" . implode(',', $notify_ids) . get_all_get_params(array('marked_ids'),'&','',true) . "';";
    $_SESSION['message'] = $write_records?$gl_messages['related_not_activated']:$gl_messages['relateds_not_activated'];
    print_javascript($js);
    die();
}

?>