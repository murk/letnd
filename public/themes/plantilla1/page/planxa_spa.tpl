<div class="planxa">
<div class="planxatop">		
<h2>NO SOMOS</br> OTROS PLANCHISTAS,</br> SOMOS TUS PLANCHISTAS.</h2>
</div>
<div class="planxaleft">
    <p><strong>Mironauto</strong> ofrece un servicio de reparación total de la carrocería de su vehículo, con herramientas modernas y adaptadas a las necesidades de cada vehículo.</br>
El servicio de pintura se realiza con productos que cumplen la normativa V.O.C., de máxima calidad y respetuosa con el medio ambiente. La pintura se realiza en una cabina especialmente concebida para este fin. Además, se utiliza gas natural para su función de secado, dirigida esta electrónicamente para optimizar recursos y tiempo.</br>
También disponemos de un box tintométrico que nos permite encontrar el color y la tonalidad exactos de la carrocería de su vehículo.</p>    
  </div>
</div>
