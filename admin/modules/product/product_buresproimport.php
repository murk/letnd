<?
/**
 * ProductBuresproimport
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 12/2013
 * @version $Id$
 * @access public
 */
class ProductBuresproimport extends Module{
	function __construct(){
		parent::__construct();
	}
	
	function list_records() {
		
			Db::$debug = true;
			set_time_limit ( 300 );
			
			$idiomes = array(				
				2 => 'spa',
				4 => 'eng',
				3 => 'fra'
			);
		
			// productes
			// product__product 
			// product__product_to_sector 
			
			Db::connect('localhost', 'root', '', 'burespro_vella');			
			$productes = Db::get_rows("SELECT * FROM producte");
			
			Db::reconnect();
			
			foreach($productes as $rs){
				
				extract($rs);
				$product_id = $id;
				
				if ($grup_id == 10) $grup_id = 0;
				
				Db::execute ("INSERT INTO product__product (product_id,subfamily_id,status) VALUES ($product_id,$familia_id,'onsale')");
				Db::execute ("INSERT INTO product__product_to_sector (product_id, sector_id) values ($product_id,$grup_id)");
				
				
				
			}
			
			// productes idiomes
			// product__product_language
			
			Db::connect('localhost', 'root', '', 'burespro_vella');				
			$productes = Db::get_rows("SELECT * FROM producte_idioma WHERE idioma<>1");
			
			Db::reconnect();
			
			foreach ($productes as $rs){
				extract($rs);
				
				$titol = Db::qstr($titol);
				$subtitol = Db::qstr($subtitol);
				$descripcio = Db::qstr(p2nl($descripcio));
				$idioma = Db::qstr($idiomes[$idioma]);
				
				Db::execute ("INSERT INTO product__product_language (product_id, product_title, product_subtitle, product_description, language) values ($producte_id,$titol,$subtitol,$descripcio,$idioma)");				
				
			}
			
			// grups
			// product__sector
			// product__sector_language
			
			Db::connect('localhost', 'root', '', 'burespro_vella');				
			$grups = Db::get_rows("SELECT * FROM grups WHERE id <> 10");
			
			Db::reconnect();
			
			foreach ($grups as $rs){
				extract($rs);
				$sector_id = $id;
				
				Db::execute ("INSERT INTO product__sector (sector_id, ordre) values ($sector_id,$ordre)");
				
				foreach ($idiomes as $key=>$idioma){
					
					$sector = Db::qstr(${'nom_lg'.$key});
					$idioma = Db::qstr($idioma);
					Db::execute ("INSERT INTO product__sector_language (sector_id, language, sector) values ($sector_id,$idioma,$sector)");
					
				}
				
			}
			
			// families CATALEG
			// product__family
			// product__family_language
			
			Db::connect('localhost', 'root', '', 'burespro_vella');				
			$families = Db::get_rows("SELECT * FROM catalegs");
			
			Db::reconnect();
			
			foreach ($families as $rs){
				extract($rs);
				
				Db::execute ("INSERT INTO product__family (family_id) values (10$id)");
				
				foreach ($idiomes as $key=>$idioma){
					
					$family = Db::qstr(${'nom_lg'.$key});
					$idioma = Db::qstr($idioma);
					Db::execute ("INSERT INTO product__family_language (family_id, language, family) values (10$id,$idioma,$family)");
					
				}
				
			}
			// sub families - FAMILIES
			// product__family
			// product__family_language
			
			Db::connect('localhost', 'root', '', 'burespro_vella');				
			$families = Db::get_rows("SELECT * FROM families");
			
			Db::reconnect();
			
			foreach ($families as $rs){
				extract($rs);
				
				Db::execute ("INSERT INTO product__family (family_id, ordre, parent_id) values ($id,$ordre,10$cataleg_id)");
				
				foreach ($idiomes as $key=>$idioma){
					
					$family = Db::qstr(${'nom_lg'.$key});
					$idioma = Db::qstr($idioma);
					Db::execute ("INSERT INTO product__family_language (family_id, language, family) values ($id,$idioma,$family)");
					
				}
				
			}
			
			
			// sub producte
			
			// entro variacions
			Db::connect('localhost', 'root', '', 'burespro_vella');				
			$variations = Db::get_rows("SELECT DISTINCT titol_subtitol FROM subproducte_idioma WHERE idioma=2 ORDER BY titol_subtitol");
			
			
			Db::reconnect();
			
			foreach ($variations as $rs){
				extract($rs);				
				
				Db::execute ("INSERT INTO product__variation (`variation_category_id`) VALUES (1);");				
				$variation_id = Db::insert_id();
				
				foreach ($idiomes as $key=>$idioma){
					
					// idiomes de la variacio
					Db::connect('localhost', 'root', '', 'burespro_vella');
					$rs_idioma = Db::get_row("SELECT titol, subtitol FROM subproducte_idioma WHERE subproducte_id = (SELECT subproducte_id FROM subproducte_idioma WHERE titol_subtitol = '$titol_subtitol' LIMIT 1) AND idioma = $key");
					extract($rs_idioma);
					
					Db::reconnect();
					
					$variation = Db::qstr($titol);
					$variation_subtitle = Db::qstr($subtitol);
					$idioma = Db::qstr($idioma);
					
					Db::execute ("INSERT INTO product__variation_language (`variation_id`, `language`, `variation`, `variation_subtitle`) VALUES ($variation_id, $idioma, $variation, $variation_subtitle);");
					
				}
								
				
			}
			
			
			// entro variacions del producte
			Db::connect('localhost', 'root', '', 'burespro_vella');				
			$variations = Db::get_rows("SELECT * FROM subproducte, subproducte_idioma WHERE subproducte.id = subproducte_idioma.subproducte_id AND idioma=2");
			Db::reconnect();
			
			foreach ($variations as $rs){
				extract($rs);
				
				$id = Db::get_first("SELECT variation_id FROM product__variation_language WHERE variation='$titol' AND variation_subtitle='$subtitol' AND language='spa'");
				
				$codigo = Db::qstr($codigo);
				$sp = Db::qstr($sp);
				$cb = Db::qstr($cb);
				$calibre = Db::qstr($calibre);
				
				Db::execute ("INSERT INTO product__product_to_variation
					(`variation_id`, `product_id`, variation_ref, `variation_others1`, `variation_others2`, `variation_others3`) 
					VALUES 
					($id, $producte_id, $codigo, $sp, $cb, $calibre);");				
				$product_variation_id = Db::insert_id();
				
				
				// la descripcio va en idiomes				
				foreach ($idiomes as $key=>$idioma){
					
					Db::connect('localhost', 'root', '', 'burespro_vella');
					$descripcio = Db::get_first("SELECT descripcio FROM subproducte_idioma WHERE subproducte_id = $subproducte_id AND idioma=$key");
					$descripcio = Db::qstr($descripcio);
					Db::reconnect();
					
					$idioma = Db::qstr($idioma);
					
					Db::execute ("INSERT INTO product__product_to_variation_language (`product_variation_id`, `language`, `variation_others4`) VALUES ($product_variation_id, $idioma, $descripcio);");
					
				}
				
				
				
			}
			
			
			// noticies
			// news__new
			// news__new_language
			
			Db::connect('localhost', 'root', '', 'burespro_vella');				
			$news = Db::get_rows("SELECT * FROM noticies");
			
			Db::reconnect();
			
			foreach ($news as $rs){
				extract($rs);
				
				$titol = Db::qstr($titol);
				$breu = Db::qstr($breu);
				$descripcio = Db::qstr($descripcio);
				$idioma = Db::qstr($idiomes[$idioma]);
				
				Db::execute ("INSERT INTO news__new (new_id, entered, expired, status,category_id,in_home) values ($codi,'$data_publicacio','$data_caducitat','public',1,1)");
				// nomes tenen entrat un idioma, per tant així funciona
				Db::execute ("INSERT INTO news__new_language (new_id, new, subtitle, content, language) values ($codi,$titol,$breu,$descripcio,$idioma)");				
				
			}
			
			Db::execute('UPDATE product__product SET product__product.family_id = (SELECT parent_id FROM product__family WHERE product__family.family_id = product__product.subfamily_id)');
			
			Db::execute("INSERT INTO news__new_language (new_id, new, subtitle, content, language) SELECT new_id, new, subtitle, content, 'cat' FROM news__new_language WHERE language='spa';");
			Db::execute("INSERT INTO news__new_language (new_id, new, subtitle, content, language) SELECT new_id, new, subtitle, content, 'eng' FROM news__new_language WHERE language='spa';");	
			Db::execute("INSERT INTO news__new_language (new_id, new, subtitle, content, language) SELECT new_id, new, subtitle, content, 'fra' FROM news__new_language WHERE language='spa';");
			
			Db::execute("INSERT INTO product__family_language (family_id, family, language) SELECT family_id, family, 'cat' FROM product__family_language WHERE language='spa';");
			
			Db::execute("INSERT INTO product__product_language (product_id, product_title, product_subtitle, product_description, language) SELECT product_id, product_title, product_subtitle, product_description, 'cat' FROM product__product_language WHERE language='spa';");
			
			Db::execute("INSERT INTO product__product_to_variation_language (product_variation_id, variation_others4, language) SELECT product_variation_id, variation_others4, 'cat' FROM product__product_to_variation_language WHERE language='spa';");
			
			Db::execute("INSERT INTO product__sector_language (sector_id, sector, language) SELECT sector_id, sector, 'cat' FROM product__sector_language WHERE language='spa';");
			Db::execute(" INSERT INTO product__variation_language (variation_id, variation, variation_subtitle, language) SELECT DISTINCT variation_id, variation, variation_subtitle, 'cat' FROM product__variation_language WHERE language='spa';");
			
			die();


	}
}

/* COMPROVAR TOTS ELS REGISTRES D'IDIOMES
 

 * RESET TAULES
 * ------------

TRUNCATE `product__product`;
TRUNCATE `product__product_to_sector`;

TRUNCATE `product__product_language`;

TRUNCATE `product__sector`;
TRUNCATE `product__sector_language`;

TRUNCATE `product__family`;
TRUNCATE `product__family_language`;

TRUNCATE `product__variation`;
TRUNCATE `product__variation_language`;
TRUNCATE `product__product_to_variation`;
TRUNCATE `product__product_to_variation_language`;

TRUNCATE `news__new_language`;
TRUNCATE `news__new`;


PREPARAR VELLA
 ------------
ALTER TABLE `subproducte_idioma`
	ADD COLUMN `titol_subtitol` TEXT NOT NULL AFTER `idioma`;

UPDATE subproducte_idioma SET titol_subtitol = concat(titol,'-',subtitol)

PREPARAR NOVA
--------------

INSERT INTO `burespro`.`product__variation_category` (`variation_category_id`) VALUES (1);
INSERT INTO `burespro`.`product__variation_category_language` (`variation_category_id`, `language`, `variation_category`) VALUES (1, 'cat', 'General');
INSERT INTO `burespro`.`product__variation_category_language` (`variation_category_id`, `language`, `variation_category`) VALUES (1, 'spa', 'General');
INSERT INTO `burespro`.`product__variation_category_language` (`variation_category_id`, `language`, `variation_category`) VALUES (1, 'fra', 'General');
INSERT INTO `burespro`.`product__variation_category_language` (`variation_category_id`, `language`, `variation_category`) VALUES (1, 'eng', 'General');

UPDATE `burespro`.`product__configadmin_language` SET `value`='S/P' WHERE`name`='variation_others1' AND `language`='cat' LIMIT 1;

UPDATE `burespro`.`product__configadmin_language` SET `value`='C/B' WHERE `name`='variation_others2' AND `language`='cat' LIMIT 1;

UPDATE `burespro`.`product__configadmin_language` SET `value`='Calibre' WHERE `name`='variation_others3' AND `language`='cat' LIMIT 1;

UPDATE `burespro`.`product__configadmin_language` SET `value`='Descripció' WHERE`name`='variation_others4' AND `language`='cat' LIMIT 1;




UPDATE `burespro`.`product__configadmin_language` SET `value`='S/P' WHERE`name`='variation_others1' AND `language`='spa' LIMIT 1;

UPDATE `burespro`.`product__configadmin_language` SET `value`='C/B' WHERE `name`='variation_others2' AND `language`='spa' LIMIT 1;

UPDATE `burespro`.`product__configadmin_language` SET `value`='Calibre' WHERE `name`='variation_others3' AND `language`='spa' LIMIT 1;

UPDATE `burespro`.`product__configadmin_language` SET `value`='Descripció' WHERE`name`='variation_others4' AND `language`='spa' LIMIT 1;



UPDATE `burespro`.`product__configadmin_language` SET `value`='S/P' WHERE`name`='variation_others1' AND `language`='fra' LIMIT 1;

UPDATE `burespro`.`product__configadmin_language` SET `value`='C/B' WHERE `name`='variation_others2' AND `language`='fra' LIMIT 1;

UPDATE `burespro`.`product__configadmin_language` SET `value`='Calibre' WHERE `name`='variation_others3' AND `language`='fra' LIMIT 1;

UPDATE `burespro`.`product__configadmin_language` SET `value`='Description' WHERE`name`='variation_others4' AND `language`='fra' LIMIT 1;



UPDATE `burespro`.`product__configadmin_language` SET `value`='S/P' WHERE`name`='variation_others1' AND `language`='eng' LIMIT 1;

UPDATE `burespro`.`product__configadmin_language` SET `value`='C/B' WHERE `name`='variation_others2' AND `language`='eng' LIMIT 1;

UPDATE `burespro`.`product__configadmin_language` SET `value`='Calibre' WHERE `name`='variation_others3' AND `language`='eng' LIMIT 1;

UPDATE `burespro`.`product__configadmin_language` SET `value`='Description' WHERE`name`='variation_others4' AND `language`='eng' LIMIT 1;

 */

?>