<?
// miro per cada un si té subfamilies
		$query = "SELECT family, family_description, family_alt, product__family_language.family_id
						FROM product__family_language, product__family
						WHERE  product__family_language.family_id = product__family.family_id
						AND language = '" . LANGUAGE . "'
						AND product__family.parent_id= '" . $l2['id'] . "'
						AND bin = 0
						ORDER BY ordre ASC, family";
		$results3 = Db::get_rows($query);

$show2 = false;
		// comprovo si al llistat de productes o a la fitxa de producte està ja dins d'una subfamilia per deixar-lo obert
		if ($familia) {
			// consulto si aquesta familia es una subfamilia
			$query2 = "SELECT parent_id FROM product__family WHERE product__family.family_id = " . $familia;
			$resultssubfa2 = Db::get_rows($query2);
			if ($resultssubfa2[0]['parent_id'] != 0) {
				$show2 = true;
			}
		}

		$loop3 = array(); // segon loop dins de loop
		$conta3 = 0;
		$loop_count3 = count($results3);

		if ($results3) {
			// vol dir que té subfamilies

			if (($show2 == true) AND ($l2['id'] == $resultssubfa2[0]['parent_id'])) {
				$l2['display'] = 'inherit';
				$l2['selected'] = 1; // mostro la familia com a seleccionada
				$l['display'] = 'inherit';
				$l['selected'] = 1; // mostro la familia com a seleccionada
			}
			else
				$l2['display'] = 'none';

			foreach ($results3 as $rs2) {

				$l3 = array();

				// CONTA PRODUCTES, es mes rapid que posant la consulta com a subquery
				$sub_product_count = Db::get_first("SELECT count(*) 
							FROM product__product
							WHERE product__product.subsubfamily_id = " . $rs2['family_id'] . "
							AND (status = 'onsale' OR status = 'nostock' OR status = 'nocatalog') 
							AND product__product.bin = 0");


				if ($sub_product_count) {
					if (USE_FRIENDLY_URL) {
						/* $l3['link'] = '/' . LANGUAGE . '/product/product/subfamily_id/' . $rs2['family_id'];
						  if (isset($_GET['page_file_name']))
						  {
						  $l3['link'] .= '/' . $_GET['page_file_name'] . '.html';
						  } */
						$l3['link'] = Page::get_link('product', 'product', false, false, array(), false, '', $rs2['family_id'], '');


/*						$l3['link'] =
							get_link( array(
									'tool' => 'product',
									'tool_section' => 'product',
									'category' => 'family_id',
									'category_id' => $rs2['family_id'],
									'detect_page' => true
							));*/


					} else {
						$l3['link'] = '/?tool=product&amp;tool_section=product&amp;subfamily_id=' . $rs2['family_id'] . '&amp;language=' . LANGUAGE;
					}
					$l3['id'] = $rs2['family_id'];
					$l3['item'] = $rs2['family'];
					$l3['family_description'] = $rs2['family_description'];
					$l3['family_alt'] = htmlspecialchars($rs2['family_alt']);
					$l3['class'] = '';
					$l3['conta'] = $conta3++;
					$l3['loop_count'] = $loop_count3;

					if ( ( $l3['id'] == $familia ) )
						$selected = 1;
					else
						$selected = 0;

					// pel filtre
					if ( isset( $_GET['family_ids'] ) &&
					     in_array( $l3['id'], $_GET['family_ids'] ) ) {
						$selected = 1;
					}

					$l3['selected'] = $selected;


					if ($show_images)
						$l3 += UploadFiles::get_record_images($rs2['family_id'], true, 'product', 'family');

					$loop3[] = $l3;
				}
			}
		}

		if ($loop3) {
			$loop3[0]['class'] = 'first';
			$loop3[count($loop3) - 1]['class'] = 'last';

			// poso be el loop_count, pot ser que alguna familia no tingui productes i llavors falla
			if (count($loop3) != $loop_count3) {
				foreach ($loop3 as &$val) {
					$val['loop_count'] = count($loop3);
				}
			}
		}

		$l2['loop'] = $loop3;
		$loop2[] = $l2;