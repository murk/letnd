<?php
include('inmo_common.php');

global $gl_action, $gl_process, $gl_one_property;
switch (true) {
	case isset($_GET['is_xml']):
		function manage_images(){
			include(DOCUMENT_ROOT . 'common/jscripts/viewer/inmo_property_images.php');
		}
		break;
	case strstr($gl_action, "_custumer"):
        include('inmo_property_custumer.php');
        break;
		break;
    case strstr($gl_action, "_history"):
        include('inmo_property_history.php');
        break;
    case strstr($gl_process, "booking"):
		
		function show_form(){
			
			global $gl_process;
		
			if($gl_process == 'booking'){
		
				$module = Module::load('booking', 'book');
				$module->parent = 'InmoProperty';
				$module->one_property = true;
				$module->do_action('show_property_books');
				
			}
			elseif ($gl_process == 'booking_propertyrange'){
				
				$module = Module::load('booking', 'propertyrange');
				$module->parent = 'InmoProperty';
				$module->do_action('show_property_seasons');				
				
			}
			elseif ($gl_process == 'booking_extra'){
		
				$module = Module::load('booking', 'extra');
				$module->parent = 'InmoProperty';
				$module->do_action('show_property_extras');				
				
			}
			
		}

        break;
    default:
        include('inmo_property2.php');
}
?>