<?php
// menus eina
define('CONTACT_MENU_CONTACT','Contacto');
define('CONTACT_MENU_LIST','E-mails de contacto');

// captions seccio
$gl_caption_contact ['c_company'] = 'Empresa';
$gl_caption_contact ['c_name'] = 'Nombre';
$gl_caption_contact ['c_surnames'] = 'Apellidos';
$gl_caption_contact ['c_mail'] = 'E-mail';
$gl_caption_contact ['c_to_address'] = 'Para';
$gl_caption_contact ['c_phone'] = 'Teléfono';
$gl_caption_contact ['c_matter'] = 'Asunto';
$gl_caption_contact ['c_comment'] = 'Observaciones';
$gl_caption_contact ['c_accept_info'] = 'Permitir enviar información';
$gl_caption_contact_list ['c_accept_info'] = 'Permitir info';
$gl_caption ['c_sendmail'] = 'Enviar correo';
$gl_messages ['added'] = 'Formulario enviado correctamente.';
$gl_caption ['c_sendok'] = $gl_messages ['added'];
$gl_messages ['not_saved '] = 'No se ha podido enviar el correo.';

$gl_caption_contact['c_nif']='N.I.F / D.NI.';
$gl_caption_contact['c_company']='Empresa';

$gl_caption_contact['c_contact_language']='Idioma pàgina de contacto';
// NO TRADUIT

$gl_caption_contact['c_entered']= 'Enviado...';
?>