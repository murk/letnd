<?
/**
 * ClientTransfluidImport
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 7/2017
 * @version $Id$
 * @access public
 *
 *
 * Taules afectades:
 * product__custumer
 * newsletter__group
 * newsletter__group_language
 * newsletter__custumer_to_group
 *
 */
class ClientTransfluidImport extends Module{
	var $querys = '', $cg, $is_import;
	function __construct(){
		parent::__construct();
		include (DOCUMENT_ROOT . 'admin/modules/newsletter/languages/' . LANGUAGE . '.php');
	}
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{
		$this->set_file('/client/transfluid_import_form.tpl');
		$this->set_vars($this->caption);
	    return $this->process();
	}

	function write_record()
	{
		$is_admin_total = $_SESSION['group_id'] == 1;

		Debug::p($_POST, 'text');
		Debug::p($_FILES, 'text');

		if ($_FILES) {

			// recullo arxiu
			foreach ($_FILES as $key => $upload_file) {
				if ($upload_file['error'] == 1) {
					global $gl_errors, $gl_reload;
					if (!$gl_errors['post_file_max_size']) {
						$gl_message = ' ' . $this->messages['post_file_max_size'] . (int) ini_get('upload_max_filesize') . ' Mb';
						$gl_errors['post_file_max_size'] = true;
						$gl_reload = true;
					}
					$this->end();
				}
			}

			// objecte excel
			include (DOCUMENT_ROOT . 'common/includes/phpoffice/PHPExcel.php');
			$objPHPExcel = PHPExcel_IOFactory::load($upload_file['tmp_name']);
			$results = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);

			// mode import o preview
			$this->is_import = isset($_POST['import']);
			// config de newsletter
			$this->cg = $this->get_config('admin','newsletter__configadmin');


			/////////////////
			// IMPORT
			/////////////////

			// control de flush
			if (function_exists('apache_setenv'))
				@apache_setenv('no-gzip', 1);
			@ini_set('zlib.output_compression', 0);@ini_set('implicit_flush', 1);

			$sql_th = $is_admin_total ? '<th>SQL</th>' : '';


			set_inner_html('import', '
				<div class="import">
					<h6>Previsualització d\'e-mails per importar</h6>
					<div class="llegenda">
						<div><span class="gris"></span>Registres nous</div>
						<div><span class="blau"></span>Registres existents</div>
						<div><span class="vermell"></span>Camps on hi ha canvis</div>
					</div>
					<table>
						<thead>
							<tr>
								<th>Num</th>
								<th>Codi client</th>
								<th>Nom</th>
								<th>E-mail</th>
								<th>Grup</th>
								' . $sql_th . '
							</tr>
						</thead>
						<tbody id="import_table">
						<tr>
							<td></td>
						</tr>
						<tr>
							<td></td>
						</tr>
						<tr>
							<td></td>
						</tr>
						<tr>
							<td></td>
						</tr>
						<tr>
							<td></td>
						</tr>
						</tbody>
					</table></div>');
			//Debug::p($results, 'text');

			$conta = 1;

			foreach ($results as $key => $rs) {
				$custumer_transfluid_code = $rs['A'];
				$name = $rs['B'];
				$mail = $rs['C'];
				$group = R::escape('group',false,'_POST');

				if ($key != 1 && $mail) {

					$group_id = $this->get_group( $group );
					$exists  = $this->check_custumer_exists($mail);

					$query = $this->add_mail($mail, $name, $group_id, $exists);
					$sql_td = $is_admin_total ? "<td>$query</td>" : '';

					$name_class = '';
					$mail_class = '';
					$group_class = '';
					$tr_class = '';
					if ($exists['exists']){
						$name_class = $exists['name'] != $name ? ' class="vermell"' : '';
						$mail_class = $exists['mail'] != $mail ? ' class="vermell"' : '';
						$group_class = $exists['group_name'] != $group ? ' class="vermell"' : '';
						$tr_class = ' class="blau"';
					}


					set_inner_html('import_table', "
						<tr $tr_class>
							<td align=\"right\">{$conta} </td>
							<td>{$custumer_transfluid_code}</td>
							<td $name_class>{$name}</td>
							<td $mail_class>{$mail}</td>
							<td $group_class>{$group}</td>
							{$sql_td}
						</tr>", true);


					flush();
					$conta ++;
				}
			}

			if ($this->is_import){
				print_javascript('top.notify_import_success();');
			}
			else{
				print_javascript('top.notify_preview_success();');
			}

		}
		$this->end();

	}
	function get_group($group){
		// comprovo si grup existeix ja a bbdd

			if (!$group) $group = 'Sense grup';
			$language = LANGUAGE;
			$query = "SELECT group_id, group_name FROM newsletter__group_language WHERE group_name=".Db::qstr($group) . " AND language = '$language'";

			$group_rs = Db::get_row($query);
			Debug::p($query, 'group');
			$group_id = false;

			// si el grup existeix no el creo
			if ($group_rs){

				$group_id = $group_rs['group_id'];
			}
			// si no existeix el grup el creo si en mode import
			else{

				if ($this->is_import){
					$group_id = $this->add_group($group);
				}
				print_javascript('top.$("#group_exists").hide();');
			}

			return $group_id;

	}
	function add_group($group){

		$group = Db::qstr($group);

		$query = "INSERT INTO newsletter__group ( ) VALUES ()";
		Db::execute($query);
		$this->querys .='<br>' . $query . ';';

		$group_id = Db::insert_id();

		global $gl_languages;
		foreach($gl_languages['public'] as $lang){
			$query = "INSERT INTO `newsletter__group_language` (`group_id`, `language`, `group_name`)
						VALUES (" . $group_id . ", '" . $lang . "', " . $group . ")";
			Db::execute($query);
			$this->querys .='<br>' . $query . ';';
		}

		return $group_id;
	}
	function add_mail($mail, $name, $group_id, $exists){

		$name = Db::qstr($name);
		$mail = Db::qstr($mail);

		if ($exists['exists']){
			// comprobar si l'id esta relacionat amb el grup, i si no relacionarlo
			// per si comprovem el mail enlloc del client_id

			$custumer_id = $exists['custumer_id'];

			// borro del grup que estigui per si han canviat de grup, l'input del grup mana
			$query = "DELETE FROM newsletter__custumer_to_group 
						WHERE custumer_id = '$custumer_id'";

			if ($this->is_import) Db::execute($query);

			// actualitzo el camp
			$query = "UPDATE " . $this->cg['custumer_table'] . "
						SET name=$name
						WHERE  customer_id=$custumer_id";

			if ($this->is_import) Db::execute($query);

		}
		else {
			$query = "INSERT INTO " . $this->cg['custumer_table'] . " (name, mail) 
						VALUES ($name, $mail)";

			if ( $this->is_import ) {

				Db::execute( $query );

				$custumer_id = Db::insert_id();
				$this->add_remove_key( $this->cg['custumer_table'], $custumer_id );
			}
			else  {
				$custumer_id = 0; // nomes pel previw, no fa res
			}

		}

		$ret_query = $query;

		$query = "INSERT INTO `newsletter__custumer_to_group` (`custumer_id`, `group_id`)
				VALUES (" . $custumer_id . ", " . $group_id . ")";

		if ($this->is_import) Db::execute($query);
		$ret_query .= '<br' . $query;

		return $ret_query;

	}
	function check_custumer_exists($mail) {

		$mail = Db::qstr( $mail );

		$query = "SELECT name, mail, customer_id as custumer_id FROM " . $this->cg['custumer_table'] . " WHERE mail=$mail";
		//$query = "SELECT name, mail, custumer_id FROM " . $this->cg['custumer_table'] . " WHERE name=$name AND surname1=$surname1 AND surname2=$surname2";
		$ret = Db::get_row( $query );

		if ( $ret ) {
			$language = LANGUAGE;
			$query         = "SELECT group_id FROM newsletter__custumer_to_group WHERE custumer_id = ${ret['custumer_id']} ";
			$group_id      = Db::get_first( $query );
			$query         = "SELECT group_name FROM newsletter__group_language WHERE group_id = $group_id AND language = '$language'";
			$group_name         = Db::get_first( $query );
			$ret['group_name']  = $group_name;
			$ret['exists'] = true;
		}

		else $ret = array('exists' => false, 'name' => '', 'mail' => '', 'custumer_id' => '', 'group_name' => '');
		return $ret;
	}

	function end($message=false){
		
		if($message) $GLOBALS['gl_page']->show_message($message);
		if ($this->querys) Debug::add('Save rows querys', $this->querys, 2);
		
		Debug::p_all();
		die();	
	}
	
	
	function add_remove_key($table, $id) {	

			$table_id = strpos($table, "customer")!==false?'customer_id':'custumer_id';

			$q2 = "SELECT count(*) FROM ". $table . " WHERE BINARY remove_key = ";
			$q3 = "UPDATE ". $table . " SET remove_key='%s' WHERE  ". $table_id . " = '%s'";

			$updated = false; 
			while (!$updated){
				$pass = get_password(50);
				$q_exists = $q2 . "'".$pass."'";	

				// si troba regenero pass
				if(Db::get_first($q_exists)){
					$pass = get_password(50);							
				}
				// poso el valor al registre
				else{				
					$q_update = sprintf($q3, $pass, $id);

					Db::execute($q_update);
					break;
				}
			}
	}
}
?>