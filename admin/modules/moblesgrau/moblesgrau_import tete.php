<?
/**
 * MoblesgrauImport
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 4/2013
 * @version $Id$
 * @access public
 */
class MoblesgrauImport extends Module{
	var $querys = '', $cg;
	function __construct(){
		parent::__construct();
	}
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{		
		$this->set_file('/moblesgrau/import_form.tpl');
		$this->set_vars($this->caption);
	    return $this->process();
	}

	function write_record()
	{
		
		
		Debug::p($_POST, 'text');
		Debug::p($_FILES, 'text');
		
		if ($_FILES) {
			
			// recullo arxiu
			foreach ($_FILES as $key => $upload_file) {
				if ($upload_file['error'] == 1) {
					global $gl_errors, $gl_reload;
					if (!$gl_errors['post_file_max_size']) {
						$gl_message = ' ' . $this->messages['post_file_max_size'] . (int) ini_get('upload_max_filesize') . ' Mb';
						$gl_errors['post_file_max_size'] = true;
						$gl_reload = true;
					}					
					$this->end();
				}
			}

			// objecte excel
			include (DOCUMENT_ROOT . 'common/includes/phpoffice/PHPExcel.php');
			$objPHPExcel = PHPExcel_IOFactory::load($upload_file['tmp_name']);
			$results = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
			
			// mode import o preview
			$is_import = isset($_POST['import']);
			// config de newsletter
			$this->cg = $this->get_config('admin','newsletter__configadmin');

			// comprovo si grup existeix ja a bbdd
			$group = R::escape('group',false,'_POST');
			
			if (!$group) $this->end('S\'ha de posar el nom del grup');
			
			$query = "SELECT group_id, group_name FROM newsletter__group_language WHERE group_name='".$group."'";

			$group_rs = Db::get_row($query);
			Debug::p($query, 'group');
			$group_id = false;
			
			// si el grup existeix no el creo
			if ($group_rs){
				
				$group_id = $group_rs['group_id'];
				
				if ($is_import){
					print_javascript('top.$("#group_exists").hide();');
				}
				else{
					print_javascript('top.$("#group_exists").show();');	
					// DE moment no. set_inner_html('group_exists', '<a href="/admin/?action=list_records&menu_id=11&selected_menu_id=13&group_id=' . $group_rs['group_id'] . '">' . $group_rs['group_name'] . '</a>',true);				
				}
			}
			// si no existeix el grup el creo si en mode import
			else{
				
				if ($is_import){
					$group_id = $this->add_group($group);
				}
				print_javascript('top.$("#group_exists").hide();');			
			}
			
			
			/////////////////
			// IMPORT
			/////////////////
			
			// control de flush
			@apache_setenv('no-gzip', 1);@ini_set('zlib.output_compression', 0);@ini_set('implicit_flush', 1);


			set_inner_html('import', '
				<div>
					<h6>Previsualització d\'e-mails per importar</h6>
					<table>
						<thead>
							<tr>
								<th>Nom</th>
								<th>address</th>
								<th>poblacio</th>
								<th>telephone</th>
								<th>E-mail</th>
								<th>Existents</th>
								<th></th>
							</tr>
						</thead>
						<tbody id="import_table">
						<tr>
							<td></td>
						</tr>
						<tr>
							<td></td>
						</tr>
						<tr>
							<td></td>
						</tr>
						<tr>
							<td></td>
						</tr>
						</tbody>
					</table></div>');
			//Debug::p($results, 'text');


			foreach ($results as $key => $rs) {
				//$custumer_id = (int)$rs['A'];
				$name = addslashes($rs['A']);
				$address = addslashes($rs['B']);
				$poblacio = addslashes($rs['C']);
				$telephone = addslashes($rs['D']);
				$mail = addslashes($rs['E']);

				if ($mail) {
					if ( $key != 1 ) {
						$exists = false;//$this->check_custumer_exists($custumer_id);
						//$exists  = $this->check_email_exists($mail);

						if ( $is_import ) {
							$this->add_mail( $mail, $address, $poblacio, $telephone, $name, $group_id, $exists );
						}

						$class_name = $name != $exists['name'] ? ' class="vermell"' : ' class="moblesgrau-gris"';
						$class_mail = $mail != $exists['mail'] ? ' class="vermell"' : ' class="moblesgrau-gris"';

						set_inner_html( 'import_table', '
						<tr>
							<td>' . $name . '</td>
							<td>' . $address . '</td>
							<td>' . $poblacio . '</td>
							<td>' . $telephone . '</td>
							<td>' . $mail . '</td>
							<td' . $class_name . '>' . $exists['name'] . '</td>
							<td' . $class_mail . '>' . $exists['mail'] . '</td>
						</tr>', true );


						flush();
					}
				}
			}

			if ($is_import){
				print_javascript('top.$("#confirm").hide();');				
				print_javascript('top.$("#done").show();');				
			}
			else{
				print_javascript('top.$("#confirm").show();');			
				print_javascript('top.$("#done").hide();');		
			}
			
		}		
		$this->end();
		
	}
	function add_group($group){
		$query = "INSERT INTO newsletter__group ( ) VALUES ()";
		Db::execute($query);
		$this->querys .='<br>' . $query . ';';
		
		$group_id = Db::insert_id();
		
		global $gl_languages;
		foreach($gl_languages['public'] as $lang){
			$query = "INSERT INTO `newsletter__group_language` (`group_id`, `language`, `group_name`)
						VALUES (" . $group_id . ", '" . $lang . "', '" . $group . "')";
			Db::execute($query);
			$this->querys .='<br>' . $query . ';';
		}
		
		return $group_id;
	}
	function add_mail($mail, $address, $poblacio, $telephone, $name, $group_id, $exists){
		if ($exists['exists']){
			// comprobar si l'id esta relacionat amb el grup, i si no relacionarlo
			// per si comprovem el mail enlloc del client_id // $custumer_id = $exists['custumer_id'];
			$query = "SELECT count(*) FROM newsletter__custumer_to_group 
						WHERE custumer_id = '" . $custumer_id . "' AND group_id = '" . $group_id . "'";
			$add_to_group = Db::get_first($query); // si dona true existeix group, per tant haig de invertir
			$add_to_group = !$add_to_group;
			
			$this->querys .='<br>' . $query . ';';
			
			// actualitzo el camp
			$query = "UPDATE newsletter__custumer
						SET name='".$name."',
						mail='".$mail."'
						WHERE  custumer_id='".$custumer_id."'";
			
			Db::execute($query);
			$this->querys .='<br>' . $query . ';';
		}
		else{
			$query = "INSERT INTO product__customer (name,mail,telephone) 
						VALUES ('" . $name . "','" . $mail . "','" . $telephone . "')";
			Db::execute($query);
			
			$custumer_id = Db::insert_id();
			
			$query = "INSERT INTO product__address (customer_id,address, city,country_id, invoice)
						VALUES ('" . $custumer_id . "','" . $address . "','" . $poblacio . "','ES', 1)";
			Db::execute($query);
			
			$this->querys .='<br>' . $query . ';';
			
			$this->add_remove_key($this->cg['custumer_table'], $custumer_id);
					
			$add_to_group = true;
		}
		
		if ($add_to_group){
			$query = "INSERT INTO `newsletter__custumer_to_group` (`custumer_id`, `group_id`)
					VALUES (" . $custumer_id . ", " . $group_id . ")";	
			Db::execute($query);
			$this->querys .='<br>' . $query . ';';
		}
		
	}
	function check_custumer_exists($custumer_id){
		$query = "SELECT custumer_id, name, mail FROM " . $this->cg['custumer_table'] . " WHERE custumer_id = '" . $custumer_id . "'";
		$ret = Db::get_row($query);
		
		if ($ret) $ret['exists'] = true;
		else $ret = array('exists' => false, 'name' => '', 'mail' => '');
		return $ret;
	}
	/*
	function check_email_exists($mail){
		$query = "SELECT custumer_id FROM " . $this->cg['custumer_table'] . " WHERE mail = '" . $mail . "'";
		return Db::get_first($query);
	}
	 * 
	 */
	function end($message=false){
		
		if($message) $GLOBALS['gl_page']->show_message($message);
		if ($this->querys) Debug::add('Save rows querys', $this->querys, 2);
		
		Debug::p_all();
		die();	
	}
	
	
	function add_remove_key($table, $id) {	

			$table_id = strpos($table, "customer")!==false?'customer_id':'custumer_id';

			$q2 = "SELECT count(*) FROM ". $table . " WHERE BINARY remove_key = ";
			$q3 = "UPDATE ". $table . " SET remove_key='%s' WHERE  ". $table_id . " = '%s'";

			$updated = false; 
			while (!$updated){
				$pass = get_password(50);
				$q_exists = $q2 . "'".$pass."'";	

				// si troba regenero pass
				if(Db::get_first($q_exists)){
					$pass = get_password(50);							
				}
				// poso el valor al registre
				else{				
					$q_update = sprintf($q3, $pass, $id);

					Db::execute($q_update);
					break;
				}
			}
	}
	
	// desconectada, ja es va fer la importacio
	function ____import_images() {
		
		global $gl_languages;
		
		$dir = dir(CLIENT_PATH . "/a-importar/");
		$conta = 1;
		while ($dir_entry = $dir->read())
			{
				if (is_file($dir->path . "/" . $dir_entry))
				{
					Debug::p($dir_entry, 'Imatge ' . $conta);
										
					$nom_arr = explode('.',$dir_entry);
					$nom_arxiu = $nom_arr[0];
					$ext = strrchr(substr($dir_entry, -5, 5), '.');
					$arxiu = CLIENT_PATH . "/a-importar/" . $dir_entry;
					
					
					/* inserto */
					$query = "INSERT INTO `directory__document` (`category_id`) VALUES (4)";
					Db::execute($query );
					Debug::p($query, 'Document');
					
					$insert_id = Db::insert_id();
					
					foreach($gl_languages['public'] as $lang){
						
						/* inserto idiomes */
						$query = "
							INSERT INTO `directory__document_language` (`document_id`, `language`, document) 
							VALUES (".$insert_id.", '".$lang."', '".$nom_arxiu."');";
						
						Db::execute($query );
						Debug::p($query, 'nbsp;nbsp;nbsp;nbsp;nbsp;');
						
						
						/* inserto arxius */
						$query = "
							INSERT INTO `directory__document_file` (`document_id`, `language`, `name` , `size`)
							VALUES (".$insert_id.", '".$lang."', '".$dir_entry."', '".filesize($arxiu)."');";
						
						Db::execute($query );
						Debug::p($query, 'nbsp;nbsp;nbsp;nbsp;nbsp;');
						$file_insert_id = Db::insert_id();
						
						/* copio imatge */
						copy($arxiu,CLIENT_PATH . "/directory/document/files/" . $file_insert_id . $ext);
					}
					
					
					$conta++;
				}
			}
		
	}
}
?>