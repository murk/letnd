<?php
// llistat tipus configuració, un formulari ja preparat per emplenar d'un llistat de clau -> valors
class ShowConfig extends BaseListShow
{
    var $cols, $action_add;
    var $rs = array();
    var $id, $add_dots = false;
    /**
     *
     * @access public
     * @return void
     */
    function __construct(&$module=false)
	{
        BaseListShow::__construct($module);

        $this->list_show_type = 'form';
        $this->show_langs = true; // per defecte mostrem tots els idiomes en el formulari
        $this->cols = 1;
        $this->is_bin = false;
        // calcul la teminacio afegida a list_records_bin o a list_records
        $action_add = str_replace("list_records_bin", '', $this->action);
        $action_add = $action_add == $this->action?str_replace("list_records", '', $this->action):$action_add;
        $action_add = $action_add == $this->action?str_replace("get_records_bin", '', $this->action):$action_add;
        $action_add = $action_add == $this->action?str_replace("get_records", '', $this->action):$action_add;
        $this->action_add = $action_add;
        $this->id = '999';
    }
    // ----------------------
    // Funcio principal
    // ----------------------
    function show_config()
    {
        global $gl_content, $gl_menu_id;
        $this->list_show();
        $this->set_vars_common();
		
        $this->get_call_rs($this->rs);

        $this->rs['link_action'] = "?action=save_rows_save_config&amp;menu_id=" . $gl_menu_id .
        get_all_get_params(array('action', $this->id_field, 'menu_id'),'&');
        // si el template es automàtic, cambiar el del formulari per el del config
        // així no lio la troco amb un altre valor per $this->list_show_type
        if ($this->template == $this->list_show_type . '.tpl')
        {
            $this->tpl->set_file('config.tpl');
        }
        // consulta
        $query = $this->get_query();

        $results = Db::get_rows($query);
        $this->has_results = !empty($results);
        // poso resultats com si fos un sol registre
        foreach($results as $r)
        {
            extract($r);
            if (!in_array($name, $this->language_fields))
            // faig aquest if per no posar valor al idioma per la linea 345 de db_base_list_show
			// linea 345: case in_array($key, $this->language_fields) && !(isset($rs[$key])):

            {
                $this->rs[$name] = $value;
            }
        }
        // no em fa falta per res el id, un config es un llistat de variables name = value
        // per tant amb el name en faig prou per guardar i l'id me l'invento
        $this->rs[$this->id_field] = '999';
        if ($results)
        {
            $this->fields['xxxxxx'] = array('form_' . $this->site_part => 'input', 'group' => null); // per fer un loop més

            foreach ($this->fields as $key=>$val)
            {
                // per admintotal poso els camps d'admintotal
                if ($_SESSION['group_id']==1) {
                	$val['form_' . $this->site_part] = 'input';
                }

				// aquí el id_field es decoratiu, per tant no vull que hi faci el loop
                if ($key == $this->id_field)
                {
                    $this->process_field($val, $key, $this->rs);
                } elseif ($val['form_' . $this->site_part] == 'input')
                {
                    // tot això fa un cop més el loop per poder assignar un nou group si es al final
                    if (!isset($old_group)) $old_group = $val['group'];
                    $new_group = $val['group'];

                    if ($new_group != $old_group)
                    {
                        if ($old_group != '')
                        {
                            $this->rs['groups'][] = array (
                            	'key' => isset($this->caption['c_group_' . $old_group])?$this->caption['c_group_' . $old_group]:'',
                                'val' => array(),
                                'fields' => $this->rs['fields']);
                            unset($this->rs['fields']);
                        }
                        $old_group = $new_group;
                    }
                    // tot això fa el loop els cops correctes
                    if (!is_null($val['group']))
                    {
                        $this->process_field($val, $key, $this->rs);
                        // si no hi ha grup doncs no els agrupo, nomès agrupo els que tenen algun valor
                        // (veure array al debugadd tpl)
                        if ($new_group == '')
                        {
                            $old_group = $new_group;
							if ($val['type'] != 'hidden'){
								$this->rs['fields'][count($this->rs['fields'])-1]['fields'] = array();
								$this->rs['groups'][] = current($this->rs['fields']);
                            	unset($this->rs['fields']);
							}
                        }
                    }
                }
            }
            // li enchufo els hidden en el primer key que trobo, així ho fa automatic i no haig de posar-ho al template
            if (isset($this->rs['groups'][0]['key'])) $this->rs['groups'][0]['key'] .= $this->hidden;

            $this->js_string = "return validar(this" . $this->js_string . ');';
            $this->tpl->set_var('js_string', $this->js_string);
			$this->tpl->set_vars($this->rs);
			
			$content = $this->tpl->process();
			// nomès modifiquem el gl_content per comptabilitat v2, treure a v3
			if (!$this->is_v3)
				$gl_content=$content;
			return $content;
            //Debug::add('Tpl', $tpl);
        } // if ($results)
        else
        {
            Debug::add('BERNAT Atencióooooooo', 'No hi ha cap resultat i n\'hauria d\'haber sempre en un ShowConfig');
            Debug::p_all();
            die();
        }

       // Debug::add('Tpl', $this->tpl);
    }

    function get_query()
    {
        $query = "SELECT " . $this->id_field . ", name,value FROM " . $this->table;
        return $query;
    }
    function set_sorting()
    {
        return array();
    }

    /**
     *
     * @access public
     * @return void
     */
    function sublist_languages($field_config, $id, $blocked)
    {
        global $tpl, $gl_languages;
        $field = $field_config['name'];
        $num_languages = count($gl_languages['public']);
        $key_to_use = $this->use_default_template?'val':$field;
	    $original_val = '';
        // REVISAR    - Masses consultes (una per cada camp amb idioma i cada resultat, poden ser mes de 50 en una pàgina
        if ($id)
        {
            $query = "SELECT value, all__language.code as language
					FROM " . $this->table . "_language
		            INNER JOIN all__language 
					ON " . $this->table . "_language.language = all__language.code
					WHERE name='" . $field . "'
					AND (public = 1 || public = 2)
					ORDER BY all__language.ordre";
            $results = Db::get_rows($query);
			//Debug::p($results, 'text');
            foreach ($results as $rs)
            {
                $field_config['input_name'] = $field . '[' . $id . '][' . $rs['language'] . ']';
				$field_config['input_language'] = $rs['language'];

	            if ($rs['language'] == LANGUAGE) $original_val = $rs['value'];

                if (isset($this->call_function[$field]))
                {
                    $rs[$key_to_use] = call_user_func($this->call_function[$field], $field_config['input_name'], $rs['value']);
                }
                // crear inputs
                else
                {
                    if ($blocked) $field_config[$this->list_show_type . '_' . $this->site_part] = 'text';
                    $rs[$key_to_use] = $this->get_input($field_config, $rs['value'], 999);
                    if ($blocked) $rs[$key_to_use] = '<img src="' . DIR_TEMPLATES_ADMIN . '/images/lock.gif">' . $rs[$key_to_use];
                }
                $rs['lang'] = $num_languages > 1 && $rs[$key_to_use]?$gl_languages['names'][$rs["language"]]:'';
                $loop[] = $rs;
            }
        }
        return array('val' => $loop, 'original_val' => $original_val);
    }
}