<?
/**
 * BookingExtra
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 7-2014
 * @version $Id$
 * @access public
 */
class BookingExtra extends Module{
	
	var $is_editable = true, $is_mail = false, $is_contract = false;
	
	function __construct(){
		parent::__construct();
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function set_listing_commons(&$listing){	
		$listing->group_fields = array('extra_type');
		$listing->order_by = "FIELD(booking__extra.extra_type,'optional','required','included'), extra ASC";
	}
	function get_records()
	{
		
	}
	
	
	/*
	 * 
	 *  Extres per una reserva concreta
	 * 
	 * Plantilla: booking/property_extras_list
	 * 
	 * Deixo en una plantilla apart ja que no entra en el js del submit
	 * 
	 * 
	 */	
	
	function get_book_extras($book_id, $property_id, $is_editable= true){
		
		
		Main::load_class('booking','common','admin');
		$common = New BookingCommon;
		$common->is_mail = $this->is_mail;
		$common->is_contract = $this->is_contract;
		$common->is_editable = $is_editable;
		return $common->get_book_extras($this, $book_id, $property_id);
		
	}
	
	
	/*
	 * 
	 *  Extres per una propietat
	 *  
	 *  Per mostrar els extres d'una propietat en la fitxa de característiques
	 *  Posa les variables dels extres al form de immo
	 * 
	 * 
	 */	
	function get_property_extras(&$module, $property_id)
	{
		
		$query = "
			SELECT booking__extra.extra_id AS extra_id,booking__extra.extra_type as extra_type,extra,comment as extra_comment,extra_price, has_quantity
			FROM booking__extra, booking__extra_to_property
			LEFT OUTER JOIN booking__extra_language using (extra_id) 
			WHERE 
				booking__extra.bin = 0 
				AND booking__extra_to_property.extra_id =  booking__extra.extra_id
				AND booking__extra_language.language = '".LANGUAGE."'
				AND property_id = ".$property_id."
				AND extra_type <> 'optional'
			ORDER BY FIELD(booking__extra.extra_type,'required','included'), extra ASC";
		
		$results = Db::get_rows($query);
		
		$ret = array();
		
		foreach ($results as $rs){

			if (!isset($ret['extras_' . $rs['extra_type']])) $ret['extras_' . $rs['extra_type']] = array();
			$ret['extras_' . $rs['extra_type']][] = $rs;
			
		}
		
		$module->set_vars($ret);
		
	}
	
	
	function show_form()
	{
	}
	function get_form()
	{
	}
	/*
	function save_property_extras() {
		
		$query = New Query('', 'booking__extra_to_property', 'property_id,extra_id', 'extra_price');
		$query->messages = &$this->messages;
		$query->set_config ('none','int','currency');		
		$query->save_related_post('extra_book_selected');		
		
	}
	*/
	function save_rows()
	{ 		
		return;
		$save_rows = new SaveRows($this);		
	    $save_rows->save();
	}

	function write_record()
	{
		return;		
	    $writerec = new SaveRows($this);
	    $writerec->save();
	}
}
?>