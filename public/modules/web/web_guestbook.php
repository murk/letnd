<?

/**
 * WebGuestbook
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 3/12/2013
 * @version $Id$
 * @access public
 */
class WebGuestbook extends Module {
	var $limit, $order_by, $condition;

	function __construct() {
		parent::__construct();
	}

	function list_records() {
		$GLOBALS['gl_content'] = $this->get_records();
	}

	function get_records() {
		// formulari al list
		$module = Module::load( 'web', 'guestbook' );
		$form   = $module->do_action( 'get_form_new' );
		$this->set_var( 'form', $form );

		$listing = new ListRecords( $this );
		//$listing->call('list_records_walk','',true);
		$listing->condition = "( status='review' || status='public' )";
		$listing->order_by  = 'entered DESC';


		if ( $this->parent == 'Block' ) {
			$listing->name      = 'Block';
			$listing->paginate  = false;
			$listing->condition .= " $this->condition";
			$listing->order_by  = $this->order_by;
			if ( $this->limit ) $listing->limit = $this->limit;
		}

		$listing->set_records();

		$split_results_total_count = $listing->tpl->vars['split_results_total_count'];

		$split_from_product = 0;
		if ( isset( $listing->tpl->vars['split_from_product'] ) )
			$split_from_product = $listing->tpl->vars['split_from_product'];
		
		$split_start = $split_results_total_count - $split_from_product + 1;

		foreach ( $listing->loop as $key => &$rs ) {
			$rs['guest_num'] = $split_start --;

		}

		if ( $this->parent == 'Block' ) {
			return $listing->loop;
		}

		return $listing->process();
	}

	function list_records_walk( &$listing ) {
		Debug::p( $listing->tpl, 'text' );
	}

	function show_form() {
		$GLOBALS['gl_content'] = $this->get_form();
	}

	function get_form() {
		$show = new ShowForm( $this );

		return $show->show_form();
	}

	function save_rows() {
		$save_rows = new SaveRows( $this );
		$save_rows->save();
	}

	function write_record() {
		if ( R::post( 'emptyinput' ) ) {
			$GLOBALS['gl_saved'] = false;

			return;
		}
		if ( $this->action == 'add_record' ) {
			$writerec = new SaveRows( $this );
			$guest    = $writerec->get_value( 'guest' );
			$comment  = $writerec->get_value( 'comment' );

			if ( $guest && $comment ) {

				if ( ! $writerec->get_value( 'prefered_language' ) )
					$writerec->set_field( 'prefered_language', 'override_save_value', "'" . LANGUAGE . "'" );

				$writerec->save();

				send_mail_admin(
					$this->caption['c_new_guest_subject'],
					sprintf( $this->caption['c_new_guest_body'],
						$guest,
						nl2br( $comment ),
						now( true ),
						$GLOBALS['gl_languages']['names'][ LANGUAGE ]
					)
				);

				$GLOBALS['gl_reload'] = true;
			}
			else {

				$GLOBALS['gl_message'] = $this->caption['c_guestbook_required'];

			}
		}
	}

	function manage_images() {
		$image_manager = new ImageManager( $this );
		$image_manager->execute();
	}
}