<?php
include_once(DOCUMENT_ROOT . '/admin/modules/custumer/languages/'.LANGUAGE.'.php');

define('MLS_MENU_SEARCH','Cerques');
define('MLS_MENU_SEARCH_EXACT','Cerques exactes');
define('MLS_MENU_SEARCH_PROX','Cerques aproximatives');

$gl_caption['c_fiscalname'] = 'Nom fiscal';
$gl_caption['c_comercialname'] = 'Nom comercial';
$gl_caption['c_adress'] = 'Adreça';
$gl_caption['c_town'] = 'Ciutat';
$gl_caption['c_province'] = 'Província';
$gl_caption['c_zip'] = 'Codi Postal';
$gl_caption['c_country'] = 'País';
$gl_caption['c_respname'] = 'Nom';
$gl_caption['c_surnames'] = 'Cognoms';
$gl_caption['c_phone1'] = 'Telèfon (1)';
$gl_caption['c_phone2'] = 'Telèfon (2)';
$gl_caption['c_fax'] = 'Fax';
$gl_caption['c_mail1'] = 'Mail';
$gl_caption['c_domain'] = 'Pàgina web';

$gl_caption_companies['c_client_id2'] = 'Empresa compartida';
$gl_caption_companies['c_add_company_title']='Afegir empresa MLS';
$gl_caption_companies['c_badd']='Afegir empresa';
$gl_caption_companies['c_nif']='NIF';
$gl_caption_companies['c_mlscode']='Codi MLS';
$gl_caption_companies['c_permission']='Estat';
$gl_caption_companies['c_permission_askfor']='Esperant a que ens accepti';
$gl_caption_companies['c_permission_askedfor']='<span class="vert">Ens sol·licita permís per compartir<span>';
$gl_caption_companies['c_permission_denied']='<span class="vermell">Ens ha denegat permís per compartir<span>';
$gl_caption_companies['c_permission_canreaccept']='Li hem denegat permís';
$gl_caption_companies['c_permission_accepted']='Compartint';
$gl_caption_companies['c_permission_']='Cap relació';

$gl_caption ['c_add_company_button'] = 'Sol·licitar MLS';
$gl_caption ['c_accept_company_button']='Acceptar MLS';
$gl_caption ['c_deny_company_button']='Denegar MLS';
$gl_caption ['c_erase_shared_company_button']='Eliminar del MLS';
//$gl_caption ['c_waiting_to_be_accepted_company_button']='Esperant ser acceptat a MLS';
$gl_caption ['c_denied_company_reaccept_company_button']='Reacceptar a MLS';
//$gl_caption['c_i_was_denied_company_button']='Aquesta empresa no em permet solicitar MLS';


$gl_messages ['company_deleted']='Empresa eliminada del MLS.';
$gl_messages ['company_authorized']='Empresa autorizada a compartir MLS.';
$gl_messages ['company_waiting']='Sol·licitud MLS enviada, esperant acceptació.';
$gl_messages ['company_denied']='Sol·licitud MLS denegada.';
$gl_messages ['company_reaccepted']='Empresa reacceptada a compartir MLS';

$gl_caption ['c_missatge_add_company']='L\'empresa <a href = "%s">%s</a> ha sol·licitat compartir propietats MLS amb vostè';
$gl_caption ['c_asunto_add_company'] = 'Compartir propietats amb MLS';
$gl_caption ['c_missatge_authorized_company']='L\'empresa <a href = "%s">%s</a> ha permès compartir propietats MLS amb vostè';
$gl_caption ['c_asunto_authorized_company'] = 'MLS autoritzat';
$gl_caption ['c_missatge_denied_company']='L\'empresa <a href = "%s">%s</a> ha denegat compartir propietats MLS amb vostè';
$gl_caption ['c_asunto_denied_company'] = 'Compartir propietats amb MLS denegada';
$gl_caption ['c_missatge_reaccepted_company']='L\'empresa <a href = "%s">%s</a> ha reacceptat compartir propietats MLS amb vostè';
$gl_caption ['c_asunto_reaccepted_company'] = 'Ha estat reacceptada la sol·licitud de compartir MLS';
$gl_caption ['c_missatge_erase_shared_company']='L\'empresa <a href = "%s">%s</a> ha finalitzat la relació MLS amb vostè';
$gl_caption ['c_asunto_erase_shared_company'] = 'MLS eliminat';

?>