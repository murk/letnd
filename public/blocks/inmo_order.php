<?php
$exclude_fields = isset($vars['exclude_fields'])?explode(',',$vars['exclude_fields']):array();
$vars['var_name'] = 'order';
$page_id = isset($vars['page_id']) ? $vars['page_id'] : false;


$orders = array(
	'price-asc' =>
		array
			(
				'order' => 'price',
				'way' => 'asc',
			)
	,
	'price-desc' =>
		array
			(
				'order' => 'price',
				'way' => 'desc',
			)
	,
	'price_m2-asc' =>
		array
			(
				'order' => 'price_m2',
				'way' => 'asc',
			)
	,
	'price_m2-desc' =>
		array
			(
				'order' => 'price_m2',
				'way' => 'desc',
			)
	,
	'floor_space-asc' =>
		array
			(
				'order' => 'floor_space',
				'way' => 'asc',
			)
	,
	'floor_space-desc' =>
		array
			(
				'order' => 'floor_space',
				'way' => 'desc',
			)
	,
	'municipi-asc' =>
		array
			(
				'order' => 'municipi',
				'way' => 'asc',
			)
	,
	'municipi-desc' =>
		array
			(
				'order' => 'municipi',
				'way' => 'desc',
			)
	,
	'ref-asc' =>
		array
			(
				'order' => 'ref',
				'way' => 'asc',
			)
	,
	'ref-desc' =>
		array
			(
				'order' => 'ref',
				'way' => 'desc',
			)
	,
);

if (isset($GLOBALS['gl_module']) && $GLOBALS['gl_module']->tool == 'inmo') {
	$caption = $GLOBALS['gl_module']->caption;
	$friendly_params = $GLOBALS['gl_module']->friendly_params;
}
else{	
	$module = Module::load('inmo','property');
	$caption = $module->caption;
	$friendly_params = $module->friendly_params;
}

unset($friendly_params['page']);

foreach($exclude_fields as $exclude_field) {
	unset($orders[$exclude_field]);
}

$selected_order = R::get('orderby');
$vars['block_selected'] = $selected_order?1:0;
$vars['selected_item'] = '';
$conta = 0;

if ($orders) {
    foreach($orders as $rs) {
		extract($rs);
        ($order . '-' . $way == $selected_order)?$selected = 1:$selected = 0;
		$friendly_params['orderby'] = $order . '-' . $way;
		$l['link'] = get_link(array(
				'tool' => 'inmo', 
				'tool_section' => 'property', 
				'friendly_params' => $friendly_params,
				'exclude_params' => array('page'),
				'link_vars' => array('tipus'=>R::escape('tipus')),
				'detect_page' => $page_id?false:true,
				'page_id' => $page_id
			));
		//Debug::p($l['link'], 'text');
		
        $l['selected'] = $selected;
        $l['value'] = $order . '-' . $way;
        $l['item'] = $caption['c_' .$order] . ' ' . $caption['c_' .$way];

	    // Selecciono el primer si no n'hi ha cap
	    if ($selected || $conta == 0) $vars['selected_item'] = $l['item'];

		$l['class'] = '';
		$l['conta'] = $conta++;
        $loop[] = $l;
    }
}
	
	
$loop[0]['class'] = 'first';	
$loop[count($loop)-1]['class'] = 'last';

?>