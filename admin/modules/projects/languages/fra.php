<?php
// menus eina
define('NEWS_MENU_NEW', 'Noticias');
define('NEWS_MENU_NEW_NEW', 'Entrar nueva noticia');
define('NEWS_MENU_NEW_LIST', 'Listar i editar noticias');
define('NEWS_MENU_NEW_BIN', 'Papelera de reciclaje');
define('NEWS_MENU_CATEGORY', 'Categorías');
define('NEWS_MENU_CATEGORY_NEW', 'Entrar nueva categoría');
define('NEWS_MENU_CATEGORY_LIST', 'Listar categorías');

$gl_messages_new['no_category']='Para poder insertar noticias tiene que haber almenos una categoría';

// captions seccio
$gl_caption_new['c_prepare'] = 'En preparación';
$gl_caption_new['c_review'] = 'Para revisar';
$gl_caption_new['c_public'] = 'Pública';
$gl_caption_new['c_archived'] = 'Archivada';
$gl_caption_new['c_new'] = 'Noticia';
$gl_caption_new['c_title'] = 'Título';
$gl_caption_new['c_subtitle'] = 'Subtítulo';
$gl_caption_new['c_content'] = 'Contenido';
$gl_caption_new['c_entered'] = 'Fecha';
$gl_caption_new['c_status'] = 'Estado';
$gl_caption_new['c_category_id'] = 'Categoría';
$gl_caption_new['c_filter_category_id'] = 'Todas las categorías';

$gl_caption_category['c_category'] = 'Categoría';
$gl_caption_category['c_new_button'] = 'Ver noticias';
$gl_caption_category['c_ordre'] = 'Orden';
$gl_caption_image['c_name'] = 'Nombre';
$gl_caption_new['c_url1_name']='Nombre URL(1)';
$gl_caption_new['c_url1']='URL(1)';
$gl_caption_new['c_url2_name']='Nombre URL(2)';
$gl_caption_new['c_url2']='URL(2)';
$gl_caption_new['c_videoframe']='Vídeo (youtube, metacafe...)';
$gl_caption_new['c_file']='Archivos';
?>
