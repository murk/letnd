<?

/* 7-04-14 
  Actualitzacions immo

 */

if (is_table('inmo__property')) {	
	
	$update_sql .= "
		ALTER TABLE `inmo__property`
	ADD COLUMN `numregister` VARCHAR(10) NOT NULL DEFAULT '' AFTER `door`,
	ADD COLUMN `locationregister` VARCHAR(10) NOT NULL DEFAULT '' AFTER `numregister`,
	ADD COLUMN `tomregister` VARCHAR(10) NOT NULL DEFAULT '' AFTER `locationregister`,
	ADD COLUMN `bookregister` VARCHAR(10) NOT NULL DEFAULT '' AFTER `tomregister`,
	ADD COLUMN `pageregister` VARCHAR(10) NOT NULL DEFAULT '' AFTER `bookregister`,
	ADD COLUMN `numberpropertyregister` VARCHAR(10) NOT NULL DEFAULT '' AFTER `pageregister`,
	ADD COLUMN `inscriptionregister` VARCHAR(10) NOT NULL DEFAULT '' AFTER `numberpropertyregister`;

ALTER TABLE `inmo__property`
	CHANGE COLUMN `locationregister` `locationregister` INT(11) ZEROFILL NOT NULL DEFAULT '0' AFTER `numregister`;
ALTER TABLE `inmo__property`
	CHANGE COLUMN `locationregister` `locationregister` INT(11) NOT NULL DEFAULT '0' AFTER `numregister`;
		ALTER TABLE `inmo__property`
	ADD COLUMN `cedula` VARCHAR(20) NOT NULL DEFAULT '' AFTER `ref_cadastre`;
		
ALTER TABLE `inmo__property`
	ADD COLUMN `efficiency_date` DATE NOT NULL AFTER `efficiency`;
		";		
	
}

if (is_table('print__kind')) {	
	
	$update_sql .= "
		UPDATE `print__kind_language` SET `kind`='Lloguer habitatge nou' WHERE  `kind_id`=3 AND `language`='cat' AND `kind`='Lloguer temporal' LIMIT 1;
UPDATE `print__kind_language` SET `kind`='Alquiler vivienda nueva' WHERE  `kind_id`=3 AND `language`='spa' AND `kind`='Alquiler temporal' LIMIT 1;
UPDATE `print__kind_language` SET `kind`='Lloguer de temporada' WHERE  `kind_id`=4 AND `language`='cat' AND `kind`='Lloguer habitual' LIMIT 1;
UPDATE `print__kind_language` SET `kind`='Alquiler de temporadal' WHERE  `kind_id`=4 AND `language`='spa' AND `kind`='Alquiler habitual' LIMIT 1;
UPDATE `print__kind_language` SET `kind`='Lloguer turístic' WHERE  `kind_id`=5 AND `language`='cat' AND `kind`='Lloguer local' LIMIT 1;
UPDATE `print__kind_language` SET `kind`='Alquiler turístico' WHERE  `kind_id`=5 AND `language`='spa' AND `kind`='Alquiler local' LIMIT 1;

INSERT INTO `print__kind` (`kind_id`) VALUES (6);
INSERT INTO `print__kind` (`kind_id`) VALUES (7);
INSERT INTO `print__kind` (`kind_id`) VALUES (8);
INSERT INTO `print__kind` (`kind_id`) VALUES (9);
INSERT INTO `print__kind_language` (`kind_id`, `language`) VALUES (6, 'cat');
INSERT INTO `print__kind_language` (`kind_id`, `language`) VALUES (6, 'spa');
INSERT INTO `print__kind_language` (`kind_id`, `language`) VALUES (6, 'eng');
INSERT INTO `print__kind_language` (`kind_id`, `language`) VALUES (6, 'fra');
INSERT INTO `print__kind_language` (`kind_id`, `language`) VALUES (7, 'cat');
INSERT INTO `print__kind_language` (`kind_id`, `language`) VALUES (7, 'spa');
INSERT INTO `print__kind_language` (`kind_id`, `language`) VALUES (7, 'eng');
INSERT INTO `print__kind_language` (`kind_id`, `language`) VALUES (7, 'fra');
INSERT INTO `print__kind_language` (`kind_id`, `language`) VALUES (8, 'cat');
INSERT INTO `print__kind_language` (`kind_id`, `language`) VALUES (8, 'spa');
INSERT INTO `print__kind_language` (`kind_id`, `language`) VALUES (8, 'eng');
INSERT INTO `print__kind_language` (`kind_id`, `language`) VALUES (8, 'fra');
INSERT INTO `print__kind_language` (`kind_id`, `language`) VALUES (9, 'cat');
INSERT INTO `print__kind_language` (`kind_id`, `language`) VALUES (9, 'spa');
INSERT INTO `print__kind_language` (`kind_id`, `language`) VALUES (9, 'eng');
INSERT INTO `print__kind_language` (`kind_id`, `language`) VALUES (9, 'fra');
		";
}
if (is_table('custumer__custumer')) {	
	$update_sql .= "
		ALTER TABLE `custumer__custumer`
	ADD COLUMN `job` VARCHAR(150) NULL DEFAULT NULL AFTER `company`;
		";
}


?>