<!DOCTYPE html>
<html lang="<?= LANGUAGE_CODE ?>">
<head>
	<title><?= $page_title ?></title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

	<?=load_admin_css()?>

	<link rel="stylesheet" href="/common/includes/font-awesome/css/font-awesome.min.css">

 	<link href="/common/jscripts/jquery-ui-1.8.20/css/ui-lightness/jquery-ui-1.8.20.custom.css" rel="stylesheet"
 	      type="text/css"/>
 	<link href="/common/jscripts/lightgallery/css/lightgallery_custom.css" rel="stylesheet"
 	      type="text/css"/>

	<!--[if IE 8]>
	<link href="/admin/themes/inmotools/styles/general-ie8.css?v=<?=$version?>" rel="stylesheet" type="text/css"/>
	<![endif]-->
	<!--[if IE 7]>
	<link href="/admin/themes/inmotools/styles/general-ie7.css?v=<?=$version?>" rel="stylesheet" type="text/css"/>
	<![endif]-->

	<script type="text/javascript" src="/common/jscripts/jquery/jquery-1.7.2.min.js"></script>
	<? /*
        TODO - Falla en l'autocomplete del sell_form
        <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
 */ ?>
	<? /* TODO-i Pasar html_editor.php a js */ ?>

	<script type="text/javascript">

		var gl_language = '<?=$language?>';
		var gl_menu_id = '<?=$GLOBALS['gl_menu_id']?>';
		var gl_tool = '<?=$tool?>';
		var gl_tool_section = '<?=$tool_section?>';
		var gl_parent_tool = '<?=$parent_tool?>';
		var gl_parent_tool_section = '<?=$parent_tool_section?>';
		var gl_child_tool = '<?=$child_tool?>';
		var gl_child_tool_section = '<?=$child_tool_section?>';
		var gl_action = '<?=$action?>';
		var gl_current_form_id = <?=$current_form_id===false?'false':$current_form_id?>;
		var gl_is_admin = true;

		var html_editors = <?=$html_editors?>;

		$(document).ready(
			function () {
				<?=$javascript?>
			}
		)
	</script>

	<? if($html_editors_params): ?>

		<script type="text/javascript" src="/common/jscripts/tinymce.5/tinymce.min.js?v=<?= $version ?>"></script>
		<? if ( is_file(PATH_TEMPLATES_PUBLIC . 'tinymce/templates.js') ): ?>
			<script type="text/javascript" src="<?=DIR_TEMPLATES_PUBLIC ?>tinymce/templates.js"></script>
		<? endif //$ ?>

		<script type="text/javascript" src="/common/jscripts/html_editor.php?<?= $html_editors_params ?>"></script>
	<? endif //html_editors?>


	<? if( ! DEBUG && !$GLOBALS['gl_is_local']): ?>
		<script type="text/javascript" src="/common/jscripts/jquery.onerror.js?v=<?= $version ?>"></script>
	<? endif //DEBUG?>
	<script type="text/javascript" src="/common/jscripts/notify/pnotify.custom.min.js"></script>
	<script type="text/javascript" src="/common/jscripts/dropzone/dropzone.js"></script>
	<script type="text/javascript"
	        src="/common/jscripts/jquery-ui-1.8.20/js/jquery-ui-1.8.20.custom.min.js?v=<?= $version ?>"></script>
	<script type="text/javascript"
	        src="/common/jscripts/jquery-ui-1.8.20/languages/jquery.ui.datepicker-<?= $language_code ?>.js?v=<?= $version ?>"></script>
	<script type="text/javascript" src="/common/jscripts/jquery.highlight-4.js?v=<?= $version ?>"></script>
	<script type="text/javascript" src="/common/jscripts/jquery.dump.js?v=<?= $version ?>"></script>
	<script type="text/javascript" src="/common/jscripts/jquery.mousewheel.js?v=<?= $version ?>"></script>
	<script type="text/javascript" src="/common/jscripts/jquery.timepicker.js?v=<?= $version ?>"></script>
	<script type="text/javascript" src="/common/jscripts/jquery.cookie.js?v=<?= $version ?>"></script>
	<script type="text/javascript" src="/common/jscripts/jquery.blockUI.js?v=<?= $version ?>"></script>
	<script type="text/javascript" src="/common/jscripts/jquery.dropshadow.js?v=<?= $version ?>"></script>
	<script type='text/javascript' src='/common/jscripts/jquery.autocomplete.min.js'></script>
	<script language="JavaScript" src="/common/jscripts/multifile/jquery.MultiFile.js?v=<?= $version ?>"></script>
	<script type="text/javascript" src="/admin/jscripts/main.js?v=<?= $version ?>"></script>
	<script type="text/javascript" src="/admin/languages/<?= $language ?>.js?v=<?= $version ?>"></script>

	<? /* Light Gallery  */ ?>
	<script type="text/javascript" src="/common/jscripts/lightgallery/js/lightgallery.min.js?v=<?= $version ?>"></script>
	<script type="text/javascript" src="/common/jscripts/lightgallery/js/lg-thumbnail.min.js?v=<?= $version ?>"></script>
	<script type="text/javascript" src="/common/jscripts/lightgallery/js/lg-zoom.min.js?v=<?= $version ?>"></script>
	<? /* Fi  Light Gallery  */ ?>

	<? /* Validacions  */ ?>
	<script type="text/javascript" src="/common/jscripts/jquery-validation/jquery.validate.js"></script>
	<script type="text/javascript" src="/common/jscripts/jquery-validation/localization/messages_<?= $language_code ?>.min.js"></script>
	<script type="text/javascript" src="/common/jscripts/jquery-validation/jquery-validate.bootstrap-tooltip.min.js"></script>
	<script type="text/javascript" src="/common/jscripts/bootstrap-js/tooltip.js"></script>
	<script type="text/javascript" src="/common/jscripts/jquery.inputmask/jquery.inputmask.bundle.min.js"></script>
	<script type="text/javascript" src="/common/jscripts/datejs/date.min.js"></script>
	<script type="text/javascript" src="/common/jscripts/datejs/date-<?= $language_code ?>.min.js"></script>
	<script type="text/javascript" src="/common/jscripts/keymap/mousetrap.min.js?v=<?= $version ?>"></script>
	<? /* Fi validacions */ ?>

	<? foreach ( $javascript_files as $l ): extract( $l ) ?>
		<script type="text/javascript" src="<?= $javascript_file ?>?v=<?= $version ?>"></script>
	<? endforeach //$javascript_files?>
	<? foreach ( $css_files as $l ): extract( $l ) ?>
		<link href="<?= $css_file ?>?v=<?= $version ?>" rel="stylesheet" type="text/css"/>
	<? endforeach //$css_files?>

</head>
<body>
<? if(DEBUG): ?>
	<div id="tancar-debug">
		<a href="javascript:top.hidePopWin(false)">TANCA POPWIN</a>
	</div>
<? endif //DEBUG?>

<? if( ! $login){ ?>
	<div id="header_login">
		<div id="logo_login"><? if(USE_CUSTOM_LOGO): ?><img src="/<?= $client_dir ?>/images/logo_intranet.png"
		                                                    border="0" /><? else: ?><img
				src="/admin/themes/inmotools/images/logo.png" alt="Letnd" width="191" height="42"
				border="0"/><? endif //USE_CUSTOM_LOGO?></div>
		<div class="sep_dark"></div>
		<div id="message_container" class="<?= $message_class ?>">
			<noscript>
				<div id="message" class="noscript">
					<?= $c_noscript ?>
				</div>
			</noscript>
			<script language="JavaScript">
				document.write('<div id="message" ><?=addslashes($message)?></div>');
			</script>
		</div>
		<?= $content ?>
	</div>
<? }else{//login?>
	<div id="header_top">
		<div class="left"><a href="/admin/"><? if(USE_CUSTOM_LOGO): ?><img
					src="/<?= $client_dir ?>/images/logo_intranet.png" border="0" /><? else: ?><img
					src="/admin/themes/inmotools/images/logo.png" alt="Letnd" width="191" height="42"
					border="0"/><? endif //USE_CUSTOM_LOGO?></a></div>
		<div class="right">
			<? if($user_name): ?>
				<div class="user"><?= $user_text ?>: <strong><?= $user_name ?>    <?= $user_surname ?></strong>
				</div><? endif //username?>
			<a href="/" target="_blank" class="first"><?= $menu_home_public ?></a>
			<a href="/admin/" class="second"><?= $menu_home ?></a>
			<a href="/admin/?logout=true" class="last"><?= $menu_logout ?></a>
		</div>
		<div class="cls"></div>
	</div>
	<div id="header_middle">
		<? foreach($menu_tools as $m):extract( $m ) ?><? if($selected): ?>
			<div class="selected"><?= $variable ?></div><? else: ?><a
			href="/admin/?menu_id=<?= $link ?>"><?= $variable ?></a><? endif //selected?><? endforeach//menu_tools ?>
		<div class="cls"></div>
	</div>
	<div class="cls"></div>
	<div id="header_bottom">
		<?= $title ?>
		<div class="subtitle"><?= $subtitle ?></div>
		<? if($title_right): ?>
			<div class="right"><?= $title_right ?></div><? endif //title_right?>
	</div>
	<table id="middle" border="0" cellspacing="0" cellpadding="0">
		<tr><? if($show_menu): ?>
				<td id="menu_holder">
				<div id="menu_holder2">
					<div id="menu">
						<div id="menu_arrow" class="m_arrow" onclick="collapse_menu();"></div>
						<div id="menu_content"><? foreach($menu_tools_2 as $m):extract( $m ) ?>
								<strong><?= $variable ?></strong>
								<? foreach($menu_tools_3 as $mm):extract( $mm ) ?>
									<? if($link == 'sep'): ?>
										<div class="sep"></div><? else: //sep?><a href="/admin/?menu_id=<?= $menu_id ?><?= $menu_params ?>"
									                                              class="lnk<?= $selected ?>">
										<?= $variable ?>
										</a><? endif //sep?>
									<? if($last_menu): ?>
										<div class="last_menu">
											<? if($last_menu['link']): ?>
												<a href="<?= $last_menu['link'] ?>"
												   class="last_menu"><?= $last_menu['name'] ?></a>
											<? else: //last_menu?>
												<?= $last_menu['name'] ?>
											<? endif //last_menu?>
											<? if($last_menu_sub): ?>
												<? if($last_menu_sub['link']): ?>
													<a href="<?= $last_menu_sub['link'] ?>"
													   class="last_menu_sub"><?= $last_menu_sub['name'] ?></a>
												<? else: //last_menu?>
													<div class="last_menu_sub"><?= $last_menu_sub['name'] ?></div>
												<? endif //last_menu link?>
											<? endif //last_menu_sub?>

										</div>
									<? endif //last_menu?>

									<? if($last_sub_menu): ?>
										<div class="last_menu">
											<? foreach($last_sub_menu as $l): ?>
												<a href="/admin/?menu_id=<?= $menu_id ?><?= $l['id'] ?>"
												   class="last_menu selected<?= $l['selected'] ?>"><?= $l['item'] ?></a>

												<? if ( isset($l['loop']) ): ?>
													<div>
														<? foreach ( $l['loop'] as $l2 ): ?>
															<a href="/admin/?menu_id=<?= $menu_id ?><?= $l2['id'] ?>"
															   class="last_menu selected<?= $l2['selected'] ?>"><?= $l2['item'] ?></a>
														<? endforeach //$last_sub_menu?>
													</div>
												<? endif // $loop ?>

											<? endforeach //$last_sub_menu?>
										</div>
									<? endif //last_sub_menu?>

								<? endforeach //$menu_tools_3?>
							<? endforeach //menu_tools_2 ?>
						</div>
					</div>
				</div>
				</td><? endif //show_menu?>
			<td id="contingut">
				<div id="message_container" class="<?= $message_class ?>">
					<div id="message"><?= $message ?></div>
				</div>
				<? if($news): ?>
					<div id="news"><?= $news ?></div>
				<? endif //news?>
				<? if($breadcrumb): ?>
					<div class="breadcrumb"><?= $breadcrumb ?></div>
				<? endif //breadcrumb?>
				<?= $content ?></td>
		</tr>
	</table>
	<? /*<div id="footer_top">
		<? foreach($menu_tools as $m): extract( $m ) ?><? if($selected): ?><strong><?= $variable ?></strong>
		<? else: ?><a href="/admin/?menu_id=<?= $link ?>"><?= $variable ?></a>
		<? endif //selected?><? endforeach //menu_tools ?>
	</div>*/ ?>
<? }//login?>

<div id="footer_bottom">
	<? if(USE_CUSTOM_FOOT): ?>
		<? if(USE_CUSTOM_FOOT != '1') echo USE_CUSTOM_FOOT . ' - ' ?>
		<strong>v.
			<?= number_format( $version, 0, ',', '.' ) ?>
		</strong>
	<? else: //USE_CUSTOM_FOOT?>
		9 Sistema Serveis Informàtics 2005 S.L.  &nbsp; &nbsp; Telf: 972 40 27 56 &nbsp; &nbsp;
		<a href="mailto:info@letnd.com">info@letnd.com</a><br/>
		<a href="http://www.letnd.com" target="_blank"><img alt="Letnd"
		                                                        src="/admin/themes/inmotools/images/footer_letnd.png"
		                                                        width="74" height="28" border="0"/></a> <strong>v.
			<?= number_format( $version, 0, ',', '.' ) ?>
		</strong>
	<? endif //USE_CUSTOM_FOOT?>
	<? if(USE_CUSTOM_LOGO_FOOTER): ?><img class="sistema" src="/<?= $client_dir ?>/images/logo_intranet_footer.png"
	                                      border="0" height="28" /><? endif //USE_CUSTOM_LOGO?></div>
<iframe name="save_frame" id="save_frame" scrolling="yes" marginwidth="0" marginheight="0" frameborder="1"
        style="overflow:visible; width:100%; height: 400px; display:<?= $showiframe ?>"></iframe>
<?= $calendar ?>
</body>
</html>
