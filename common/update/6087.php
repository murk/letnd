<?

/* 21-9-16

	DIRECTORI

	UP-DATE
	- Afegir opció de llistar el primer directory

 */

if ( is_table( 'directory__configadmin' ) && ! Db::get_first( "SELECT * FROM directory__configadmin WHERE name='list_first_category'" ) ) {
	$update_sql[] = "
		INSERT INTO directory__configadmin (`name`, `value`) VALUES ('list_first_category', '0');";

} else {
	if ( is_table( 'directory__configadmin' ) ) {
		trigger_error( 'Ja existeixen el camp list_first_category' );
	}
}


?>