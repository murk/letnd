<div id="graphs" class="padding-left-4">
	<div class="row">
		<div class="chart-double padding-right-1">
			<canvas id="invoice"></canvas>
		</div>
		<div class="table-single padding-left-1">
			<table>
				<thead>
				<tr>
					<td></td>
					<th>Total</th>
					<th>Despeses</th>
					<th>Guanys</th>
				</tr>
				</thead>
				<? foreach ( $labels_invoice_arr as $key => $label ): ?>
					<?
					$total   = $data_invoice_arr[ $key ];
					$expense = $data_expense_arr[ $key ];
					$earning = $data_earning_arr[ $key ];
					?>
					<tr>
						<th><?= $label ?></th>
						<td><?= $total ?></td>
						<td><?= $expense ?></td>
						<td><?= $earning ?></td>
					</tr>
				<? endforeach //$labels_invoice?>
			</table>
		</div>
	</div>
	<div class="row">
		<div class="chart-single padding-left-1">
			<canvas id="invoice_year"></canvas>
		</div>
	</div>
	<div class="row">
		<div class="chart-double padding-right-1">
			<canvas id="invoice_batch"></canvas>
		</div>
		<div class="table-single padding-left-1">
			<table>
				<thead>
				<tr>
					<td></td>
					<th>Total</th>
					<th>Despeses</th>
					<th>Guanys</th>
				</tr>
				</thead>
				<? foreach ( $labels_invoice_batch_arr as $key => $label ): ?>
					<?
					$total   = $data_invoice_batch_arr[ $key ];
					$expense = $data_expense_batch_arr[ $key ];
					$earning = $data_earning_batch_arr[ $key ];
					?>
					<tr>
						<th><?= $label ?></th>
						<td><?= $total ?></td>
						<td><?= $expense ?></td>
						<td><?= $earning ?></td>
					</tr>
				<? endforeach //$labels_invoice?>
			</table>
		</div>
	</div>

	<div class="row">
		<div class="chart-double padding-right-1">
			<canvas id="recurring"></canvas>
		</div>
		<div class="table-single padding-left-1">
			<table>
				<thead>
				<tr>
					<td></td>
					<th>Total</th>
					<? /* <th>Total suma</th> */ ?>
					<th>Inmo</th>
					<th>Botiga</th>
					<th>Hosting</th>
					<th>SEO</th>
				</tr>
				</thead>
				<? foreach ( $labels_fix_arr as $key => $label ): ?>
					<?
					$total   = $data_fix_arr[ $key ];
					$inmo    = $data_inmo_arr[ $key ];
					$shop    = $data_shop_arr[ $key ];
					$hosting = $data_hosting_arr[ $key ];
					$seo     = $data_seo_arr[ $key ];
					$total_2 = $inmo + $shop + $hosting + $seo;
					?>
					<tr>
						<th><?= $label ?></th>
						<? /* <td><?= $total ?></td> */ ?>
						<td><?= $total_2 ?></td>
						<td><?= $inmo ?></td>
						<td><?= $shop ?></td>
						<td><?= $hosting ?></td>
						<td><?= $seo ?></td>
					</tr>
				<? endforeach //$labels_invoice?>
			</table>
		</div>
	</div>

	<div class="row">
		<div class="chart-double padding-right-1">
			<canvas id="vat"></canvas>
		</div>
	</div>

	<div class="row">
		<div class="chart-double padding-right-1">
			<canvas id="hores"></canvas>
		</div>
	</div>
	<!--<div class="padding-top-4" id="invoice-c3"></div>-->


</div>


<script type="text/javascript" src="/common/jscripts/charts/chartjs/Chart.min.js"></script>
<script type="text/javascript" src="/common/jscripts/charts/c3js/d3.v3.js"></script>
<script type="text/javascript" src="/common/jscripts/charts/c3js/c3.min.js"></script>
<link href="/common/jscripts/charts/c3js/c3.min.css" rel="stylesheet">
<script>
	var options = {
		responsive: true,
		maintainAspectRatio: false,
		title: {
			display: true,
			fontSize: 20,
			fontStyle: 'normal',
			padding: 20,
			text: ""
		},
		scales: {
			xAxes: [{
				stacked: true
			}],
			yAxes: [{
				stacked: true
			}]
		},
		tooltips: {
			titleFontSize: 16,
			titleSpacing: 12,
			bodyFontSize: 14,
			bodySpacing: 12,
			xPadding: 20,
			yPadding: 20
		}
	};

	options.title.text = "Gràfic guanys mensuals";
	var ctx = document.getElementById("invoice");
	var invoice = new Chart(ctx, {
		type: 'bar',
		data: {
			labels: <?= $labels_invoice ?>,
			datasets: [
				{
					label: 'Facturació',
					data: <?= $data_invoice ?>,
					backgroundColor: "rgba(228, 188, 47, 0.4)",
					stack: 'Stack 0',
					borderWidth: 0
				},
				{
					label: 'Guanys',
					data: <?= $data_earning ?>,
					backgroundColor: <?= $data_earning_colors ?>,
					stack: 'Stack 1',
					borderWidth: 0
				},
				{
					label: 'Despeses',
					data: <?= $data_expense ?>,
					backgroundColor: "rgba(255, 99, 132, 0.4)",
					stack: 'Stack 1',
					borderWidth: 0
				}
			]
		},
		options: options
	});

	options.title.text = "Gràfic guanys anuals";
	var ctx = document.getElementById("invoice_year");
	var invoice_year = new Chart(ctx, {
		type: 'bar',
		data: {
			labels: <?= $labels_invoice_year ?>,
			datasets: [
				{
					label: 'Facturació',
					data: <?= $data_invoice_year ?>,
					backgroundColor: "rgba(228, 188, 47, 0.4)",
					stack: 'Stack 0',
					borderWidth: 0
				},
				{
					label: 'Guanys',
					data: <?= $data_earning_year ?>,
					backgroundColor: <?= $data_earning_colors_year ?>,
					stack: 'Stack 1',
					borderWidth: 0
				},
				{
					label: 'Despeses',
					data: <?= $data_expense_year ?>,
					backgroundColor: "rgba(255, 99, 132, 0.4)",
					stack: 'Stack 1',
					borderWidth: 0
				}
			]
		},
		options: options
	});

	options.title.text = "Gràfic guanys mensuals per data remesa";
	var ctx = document.getElementById("invoice_batch");
	var invoice_year = new Chart(ctx, {
		type: 'bar',
		data: {
			labels: <?= $labels_invoice_batch ?>,
			datasets: [
				{
					label: 'Facturació',
					data: <?= $data_invoice_batch ?>,
					backgroundColor: "rgba(228, 188, 47, 0.4)",
					stack: 'Stack 0',
					borderWidth: 0
				},
				{
					label: 'Guanys',
					data: <?= $data_earning_batch ?>,
					backgroundColor: <?= $data_earning_colors_batch ?>,
					stack: 'Stack 1',
					borderWidth: 0
				},
				{
					label: 'Despeses',
					data: <?= $data_expense_batch ?>,
					backgroundColor: "rgba(255, 99, 132, 0.4)",
					stack: 'Stack 1',
					borderWidth: 0
				}
			]
		},
		options: options
	});

	options.title.text = "Gràfic manteniments";
	var ctx = document.getElementById("recurring");
	var invoice_year = new Chart(ctx, {
		type: 'bar',
		data: {
			labels: <?= $labels_fix ?>,
			datasets: [
				{
					label: 'Total',
					data: <?= $data_fix ?>,
					backgroundColor: "rgba(228, 188, 47, 0.4)",
					stack: 'Stack 0',
					borderWidth: 0
				},
				{
					label: 'Inmo',
					data: <?= $data_inmo ?>,
					backgroundColor: "#abe06c",
					stack: 'Stack 1',
					borderWidth: 0
				},
				{
					label: 'Botiga',
					data: <?= $data_shop ?>,
					backgroundColor: "#eae97f",
					stack: 'Stack 1',
					borderWidth: 0
				},
				{
					label: 'Hosting',
					data: <?= $data_hosting ?>,
					backgroundColor: "#edca4f",
					stack: 'Stack 1',
					borderWidth: 0
				},
				{
					label: 'Posicionament',
					data: <?= $data_seo ?>,
					backgroundColor: "#f8c5f5",
					stack: 'Stack 1',
					borderWidth: 0
				}
			]
		},
		options: options
	});


	options.title.text = "IVA trimestres";
	var ctx = document.getElementById("vat");
	var invoice_year = new Chart(ctx, {
		type: 'bar',
		data: {
			labels: <?= $labels_vat ?>,
			datasets: [
				{
					label: 'IVA',
					data: <?= $data_vat ?>,
					backgroundColor: "rgba(228, 188, 47, 0.4)",
					stack: 'Stack 0',
					borderWidth: 0
				},
				{
					label: 'IVA despeses',
					data: <?= $data_vat_expense ?>,
					backgroundColor: "rgba(139, 203, 0, 0.4)",
					stack: 'Stack 7',
					borderWidth: 0
				},
				{
					label: 'IVA+IRPF',
					data: <?= $data_iva_irpf ?>,
					backgroundColor: "#02aee0",
					stack: 'Stack 3',
					borderWidth: 0
				},
				{
					label: 'Total a pagar',
					data: <?= $data_to_pay ?>,
					backgroundColor: "rgba(237, 72, 72, 1)",
					stack: 'Stack 4',
					borderWidth: 0
				},
				{
					label: 'IVA a pagar',
					data: <?= $data_vat_to_pay ?>,
					backgroundColor: "#dc6734",
					stack: 'Stack 5',
					borderWidth: 0
				},
				{
					label: 'IRPF',
					data: <?= $data_irpf_expense ?>,
					backgroundColor: "#dc9734",
					stack: 'Stack 6',
					borderWidth: 0
				}
			]
		},
		options: options
	});


	options.title.text = "Hores treballades ( Marc, mmm, ja en parlarem... )";
	options.scales = {
		yAxes: [{
			ticks: {
				beginAtZero: true
			}
		}]
	};
	var ctx = document.getElementById("hores");
	var invoice_year = new Chart(ctx, {
		type: 'bar',
		data: {
			labels: ["March 2017 ", "April 2017 ", "May 2017 ", "June 2017 ", "July 2017 ", "August 2017 ", "September 2017 ", "October 2017 ", "November 2017 ", "December 2017 "],
			datasets: [
				{
					label: 'Marc',
					data: [240, 250, 244, 260, 255, 242, 257, 270, 251, 249],
					backgroundColor: "rgba(139, 203, 0, 1)",
					borderWidth: 0
				},
				{
					label: 'Marc',
					data: [140, 150, 144, 160, 155, 141, 157, 170, 151, 149],
					backgroundColor: "rgba(237, 72, 72, 1)",
					borderWidth: 0
				}
			]
		},
		options: options
	});



   <? /*
			 var chart = c3.generate({
				 bindto: '#invoice-c3',
				 data: {
					 columns: [
						 ['Facturació', <?= $data_invoice_c3 ?>],
					 ['Despeses', <?= $data_expense_c3 ?>],
					 ['Guanys', <?= $data_earning_c3 ?>]
				 ],
				 type: 'bar'
			 },
			 bar: {
				 width: {
					 ratio: 0.7 // this makes bar width 50% of length between ticks
				 }
				 // or
				 //width: 100 // this makes bar width 100px
			 },
			 axis: {
				 x: {
					 type: 'category',
					 categories: <?= $labels_invoice ?>
				 }
			 }
		 });

	*/?>

</script>