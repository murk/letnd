(function( $ ){
  $.fn.jsErrorHandler = function(options) {
  	
	var settings = {
		website: document.domain
	}
	if (options) $.extend(settings, options);

 
    window.onerror = function (msg, url, line) {
		$.ajax({
			type:"GET",
			cache:false,
			url:"/common/js_onerror.php",
			data: $.param({'message':msg, 'url': url, userAgent: navigator.userAgent, 'line': line, 'website': settings.website, 'location': window.location.href }),
			success: function(test){
				if(window.console) console.log("Report sent about the javascript error")
			}
		})
	    return true;
	}


  };
})( jQuery );
$(document).jsErrorHandler();