capital = 120000;
interes = 4.5/12;
plazo = 30*12;

cuota = (capital*interes)/(100*(1-Math.pow(1+(interes/100),-plazo)));


function GetValue(sValue)
// Función para formatear los valores numéricos introducidos
{
 
 
    var sRaw = "";							// Variable con el valor formateado
    var sChar = "";							// Variable a la que pasamos cada carácter del valor a formatear
    var i = 0;

    sValue += "";
    for (i=0; i <= sValue.length; i++) {	// Hasta que no equiparemos i con el número de carácteres del valor a formatear

        sChar = sValue.substring(i, i+1);	// Pasamos cada carácter por la variable SChar, dependiendo del contador.

	if ((sChar >= "0") && (sChar <= "9")){	// Comparamos si está entre el rango 0 a 9
            sRaw = sRaw + sChar;			// Vamos componiendo el valor formateado
        }
    }										// Fin Hasta

    if (sRaw.length > 0) {					// Si el número de carácteres del valor formateado es superior a 0
        return parseInt(sRaw);				// Retornamos el valor
    }
    else {
        return 0;
    }
}

// ---------------------------------------------------------
function GetValueRate(sValue)
// Función para formatear los valores numéricos DECIMALES introducidos (interés)
{

    var sRaw = "";									// Variable con el valor formateado
    var sChar = "";									// Variable a la que pasamos cada carácter del valor a formatear
    var bHasDecimal = 0;
    var i = 0;

    sValue += "";
    for (i=0; i <= sValue.length; i++) {			// Hasta que no equiparemos i con el número de carácteres del valor a formatear

        sChar = sValue.substring(i, i+1);			// Pasamos cada carácter por la variable SChar, dependiendo del contador.

        if ((sChar >= "0") && (sChar <= "9")){		// Comparamos si está entre el rango 0 a 9
            sRaw = sRaw + sChar;
        }

        if ((sChar == ",") || (sChar == ".")){		// Si el carácter conté "," o "."
            bHasDecimal = 1;						// Activamos flag información "contiene decimales"
            sRaw = sRaw + ".";						// Le insertamos el "." como separador de decimales
        }

    }												// Fin Hasta

    if (bHasDecimal) {								// Si el flag información "contiene decimales" = 1 (activo)
        return parseFloat(sRaw);					// Retorna los valores numéricos desde la izquierda, conservando "," y "-"
    }

	if (sRaw.length > 0) {							// Si el valor formateado es superior a 0
        return parseInt(sRaw);						// Retorna los valores númericos desde la izquierda
    }
    else {
        return 0;
    }
}

// ---------------------------------------------------------
function FormatOutput(iValue, nDec) {
// Función para formatear el "." de cientos, miles y millares

    var bIsNegative = 0;		// Flag por si valor es negativo
    var iPos = 0;				// 
    var sChar = "";				// Variable para almacenar carácter tratado
    var sTempNumber = "";
    var sNoChars = "";
    var sDollars = "";
    var sCents = "";
    var sDollarAmount = "";
    var sFormated = "";
    var x = 0;
	var IndexOfDec = -1;
	var rounded;

    if (iValue != "") {			// Si el valor es diferente a ""

        sTempNumber = iValue + "";												// Pasamos valor
        if (sTempNumber.charAt(0) == "-") {										// Si contiene el valor "-" en la primera posición
            bIsNegative = 1;													// Activamos Flag negativo
            sTempNumber = sTempNumber.substring(1, sTempNumber.length);			// Pasamos el valor sin negativo		
        }

        //sTempNumber
        IndexOfDec = sTempNumber.indexOf(".");		// Pasamos la posición en la que se encuentra el primer "." (Si lo hay, si no devuelve -1)

        if (IndexOfDec == -1) {						
            sDollars = sTempNumber;
            sCents = "00";
        }
        else if (IndexOfDec == 0) {
            sDollars = "0";
            sCents = sTempNumber.substring(IndexOfDec + 1, sTempNumber.length);
        }
        else {
            sDollars = sTempNumber.substring(0, IndexOfDec);
            if (IndexOfDec == (sTempNumber.length - 1)) {
                sCents = "00";
            }
            else {
                sCents = sTempNumber.substring(IndexOfDec + 1, sTempNumber.length)
                sCents += "0";
                sCents = sCents.charAt(0) + sCents.charAt(1);
            }
        }

        sFormated = sDollars;
        x = sDollars.length;
        iPos = 0;
        while (x > 0) {
            x--;
            sChar = sDollars.charAt(x);
            rounded = Math.round(iPos/3);
            if ( (iPos/3 == rounded ) & (iPos != 0) ) {
                sDollarAmount = "." + sDollarAmount;
            }
            sDollarAmount = sChar +  sDollarAmount;
            iPos++;
        }

        if (nDec) {
          if (bIsNegative) {
              sFormated = "-" + sDollarAmount + "," + sCents + "";
          }
          else {
              sFormated = sDollarAmount + "," + sCents  + "";
          }
	}
        else {
          if (bIsNegative) {
              sFormated = "-" + sDollarAmount + "";
          }
          else {
              sFormated = sDollarAmount + "";
          }
	}

        return (sFormated);
    }
    else {

        return("0" + "");
    }
}




// ---------------------------------------------------------
function GetMonthlyPayment(term, rate, loanAmount) {
	
	
	var rateMonthly
	var totalPayments
	var payment    
	
	if (rate >= 1) { //Si el interés es mayor o igual a 1
        (rate = rate / 100);
    }
    rateMonthly = (rate / 12); //Media interés

    totalPayments = (term * 12); //total de pagos es Plazos * 12

    if (rate == 0) { //si el interes es = 0
        payment = (loanAmount / totalPayments); //cuota = capital / plazos
    }

    else {
        payment = (loanAmount * rateMonthly) / (1 - Math.pow((1+rateMonthly), (-1*totalPayments)) ); 
		//fórumula para cálculo de cuotas vs interés vs plazos
    }

    return(payment); //Pasamos el valor
}

// ---------------------------------------------------------
function ReCalculate(_form) {
	
	var Fprice;
	var Frate;
	var Fterm;
	var iLoanAmount;
	var iMonPayment;
	var iPaymentTotal;
	

    Fprice = parseInt(_form.Fprice.value);  
    Fdownpayment    = parseInt(_form.Fdownpayment.value); //Entrada a
    Frate = document._form.Frate.value;
	//Frate  = parseInt(_form.Frate.value); //Interés
    Fterm  = parseInt(_form.Fterm.value); //Plazos
    iLoanAmount     = Fprice - Fdownpayment;// El precio Total pasa a ser el Precio-Entrada
	iMonPayment = GetMonthlyPayment(Fterm, Frate, iLoanAmount); //Calculamos mediante función la cuota mensual
    iMonPayment = Math.round(iMonPayment * 100) / 100; 
    //// FORMAT VARIABLES
    iMonPayment     = FormatOutput(iMonPayment,0);
	document._form.iPaymentTotal.value=iMonPayment;
	return (false);

}