<?php

// array details versio millorada de fields_pdf
function print_add_detail($key,$caption,$val) {	
	global $gl_print_details;
	$gl_print_details[$key] = array('caption' =>$caption, 'val' => $val);
}
function print_get_custom_field($key, $val, $c_yes){
	global $gl_config;

	if ($gl_config[$key.'_type'] == 'checkbox'){
		return $val == 1?$c_yes:false;
	}


	if (!is_array($val)) return $val;

	foreach ($val as $v) {
		if ($v['language'] == LANGUAGE) return $v[$key];
	}

}

global $gl_action, $gl_is_album, $tpl, $gl_print_details, $gl_site_part;

// Configuracions depenets del disseny
$config = pdf_get_config();
extract ($config); // obtenim totes les variables definides dins el tpl

define('PRINT_DIR', CLIENT_PATH . '/images/');
if ($gl_site_part != 'public'){
	foreach ($property as $prop) {
		if ($prop['language'] == LANGUAGE) $property = $prop['property'];
	}
	foreach ($property_title as $prop) {
		if ($prop['language'] == LANGUAGE) $property_title = $prop['property_title'];
	}
	foreach ($description as $prop) {
		if ($prop['language'] == LANGUAGE) $description = strip_tags($prop['description']);
	}
}
$logo = PRINT_DIR . 'logo_showwindow.png';

// poblar l'array $fields_pdf, que son els camps que surten al pdf
$fields_pdf[$c_tipus] = $tipus;
print_add_detail('tipus',$c_tipus,$tipus);

if ($category_id && $gl_is_album) {
	$fields_pdf[$c_category_id] = $category_id;
	print_add_detail('category_id',$c_category_id,$category_id);
}
if ($tipus2) {
	$fields_pdf[$c_tipus2] = $tipus2;	
	print_add_detail('tipus2',$c_tipus2,$tipus2);
}

$situacio_arr = array();
if ($admin_show_zone && $zone_id) $situacio_arr[] = $zone_id;
if ($admin_show_municipi) $situacio_arr[]= $municipi_id;
if ($admin_show_comarca) $situacio_arr[]= $comarca_id;
if ($admin_show_provincia) $situacio_arr[]= $provincia_id;
if ($situacio_arr) {
	$fields_pdf[$c_municipi_id] = implode(', ',$situacio_arr);
	print_add_detail('municipi_id',$c_municipi_id,implode(', ',$situacio_arr));
}

if ($floor_space) {
	$fields_pdf[$c_floor_space] = $floor_space . ' m2';
	print_add_detail('floor_space',$c_floor_space,$floor_space . ' m<sup>2</sup>');
}
if ($room) {
	$fields_pdf[$c_room] = $room;
	print_add_detail('room',$c_room,$room);
}
//print_r($fields_pdf);die();
$bathroom_tmp = '';
if ($bathroom && $wc) {
    $bathroom_wc_join = ' , ';
} else {
    $bathroom_wc_join = '';
}
if ($bathroom == 1) $bathroom_tmp = $bathroom . $c_bathroom_singular;
if ($bathroom > 1) $bathroom_tmp = $bathroom . $c_bathroom_plural;
if ($wc == 1) $bathroom_tmp = $bathroom_tmp . $bathroom_wc_join . $wc . $c_wc_singular;
if ($wc > 1) $bathroom_tmp = $bathroom_tmp . $bathroom_wc_join . $wc . $c_wc_plural;
if ($bathroom_tmp) {
	$fields_pdf[$c_bathroom] = $bathroom_tmp;
	print_add_detail('bathroom',$c_bathroom,$bathroom_tmp);
}
if ($parking) {
	$fields_pdf[$c_parking] = $c_yes;
	print_add_detail('parking',$c_parking,$c_yes);
}

$nom_var = 'garage';
$nom_var_area = $nom_var . '_area';
$nom_var_c = 'c_' . $nom_var;
$text_c_area = sprintf (${'c_'.$nom_var_area}, $$nom_var_area);
$text_c_area2 = sprintf (${'c_2'.$nom_var_area}, $$nom_var_area);
if ($$nom_var && $$nom_var_area) {
	$fields_pdf[$$nom_var_c] = $text_c_area2;
	print_add_detail($nom_var,$$nom_var_c,$text_c_area);
}
if ($$nom_var && !$$nom_var_area) {
	$fields_pdf[$$nom_var_c] = $c_yes;
	print_add_detail($nom_var,$$nom_var_c,$c_yes);
}

if ($citygas) {
	$fields_pdf[$c_citygas] = $c_yes;
	print_add_detail('citygas',$c_citygas,$c_yes);
}
if ($laundry) {
	$fields_pdf[$c_laundry] = $c_yes;
	print_add_detail('laundry',$c_laundry,$c_yes);
}

$nom_var = 'storage';
$nom_var_area = $nom_var . '_area';
$nom_var_c = 'c_' . $nom_var;
$text_c_area = sprintf (${'c_'.$nom_var_area}, $$nom_var_area);
$text_c_area2 = sprintf (${'c_2'.$nom_var_area}, $$nom_var_area);
if ($$nom_var && $$nom_var_area) {
	$fields_pdf[$$nom_var_c] = $text_c_area2;
	print_add_detail($nom_var,$$nom_var_c,$text_c_area);
}
if ($$nom_var && !$$nom_var_area) {
	$fields_pdf[$$nom_var_c] = $c_yes;
	print_add_detail($nom_var,$$nom_var_c,$c_yes);
}

$nom_var = 'terrace';
$nom_var_area = $nom_var . '_area';
$nom_var_c = 'c_' . $nom_var;
$text_c_area = sprintf (${'c_'.$nom_var_area}, $$nom_var_area);
$text_c_area2 = sprintf (${'c_2'.$nom_var_area}, $$nom_var_area);
if ($$nom_var && $$nom_var_area) {
	$fields_pdf[$$nom_var_c] = $text_c_area2;
	print_add_detail($nom_var,$$nom_var_c,$text_c_area);
}
if ($$nom_var && !$$nom_var_area) {
	$fields_pdf[$$nom_var_c] = $c_yes;
	print_add_detail($nom_var,$$nom_var_c,$c_yes);
}

$nom_var = 'garden';
$nom_var_area = $nom_var . '_area';
$nom_var_c = 'c_' . $nom_var;
$text_c_area = sprintf (${'c_'.$nom_var_area}, $$nom_var_area);
$text_c_area2 = sprintf (${'c_2'.$nom_var_area}, $$nom_var_area);
if ($$nom_var && $$nom_var_area) {
	$fields_pdf[$$nom_var_c] = $text_c_area2;
	print_add_detail($nom_var,$$nom_var_c,$text_c_area);
}
if ($$nom_var && !$$nom_var_area) {
	$fields_pdf[$$nom_var_c] = $c_yes;
	print_add_detail($nom_var,$$nom_var_c,$c_yes);
}

if ($swimmingpool) {
	$fields_pdf[$c_swimmingpool] = $c_yes;
	print_add_detail('swimmingpool',$c_swimmingpool,$c_yes);
}
if ($centralheating) {
	$fields_pdf[$c_centralheating] = $c_yes;
	print_add_detail('centralheating',$c_centralheating,$c_yes);
}
if ($airconditioned) {
	$fields_pdf[$c_airconditioned] = $c_yes;
	print_add_detail('airconditioned',$c_airconditioned,$c_yes);
}
if ($land) {
	$fields_pdf[$c_land] = $land . ' ' . $land_units;
	$land_units_sup =  $land_units=='m2'?' m<sup>2</sup>':$land_units;
	print_add_detail('land',$c_land,$land . ' ' . $land_units_sup);
}
if ($balcony) {
	$fields_pdf[$c_balcony] = $c_yes;
	print_add_detail('balcony',$c_balcony,$c_yes);
}
if ($elevator) {
	$fields_pdf[$c_elevator] = $c_yes;
	print_add_detail('elevator',$c_elevator,$c_yes);
}
if ($furniture) {
	$fields_pdf[$c_furniture] = $c_yes;
	print_add_detail('furniture',$c_furniture,$c_yes);
}

if (isset($custom1) && $custom1) {
	$custom1=print_get_custom_field( 'custom1', $custom1, $c_yes );

	if ($custom1){
		$fields_pdf[ $c_custom1 ]=$custom1;
		print_add_detail( 'custom1', $c_custom1, $custom1 );
	}
}
if (isset($custom2) && $custom2) {
	$custom2=print_get_custom_field( 'custom2', $custom2, $c_yes );
	if ($custom2){
		$fields_pdf[ $c_custom2 ]=$custom2;
		print_add_detail( 'custom2', $c_custom2, $custom2 );
	}
}
if (isset($custom3) && $custom3) {
	$custom3=print_get_custom_field( 'custom3', $custom3, $c_yes );
	if ($custom3){
		$fields_pdf[ $c_custom3 ]=$custom3;
		print_add_detail( 'custom3', $c_custom3, $custom3 );
	}
}
if (isset($custom4) && $custom4) {
	$custom4=print_get_custom_field( 'custom4', $custom4, $c_yes );
	if ($custom4){
		$fields_pdf[ $c_custom4 ]=$custom4;
		print_add_detail( 'custom4', $c_custom4, $custom4 );
	}
}
 
 

$details = array(); // variable que guarda tots els camps millorada de fields_pdf


// A l'aparador trec tots els camps que pasan del $top_fields (numero maxim de camps)
$number_fields = count($fields_pdf);

// a la variable details no afegeixo mai el preu, a partir d'ara anirà sempre separat per poder fomatejar millor
if (($number_fields > $top_fields) && !$gl_is_album) {
    $conta = 0;
    foreach($gl_print_details as $key => $value) {
        $details[$key] = $value;
        $conta++;
        if ($conta == $top_fields) break;
    }
}
else{
	$details = $gl_print_details;
}


if ($price) $top_fields--; // si hi ha preu desconto un ja que despres li posaré el preu
if (($number_fields > $top_fields) && !$gl_is_album) {
    $old_fields_pdf = $fields_pdf;
    unset($fields_pdf);
    $conta = 0;
    foreach($old_fields_pdf as $key => $value) {
        $fields_pdf[$key] = $value;
        $conta++;
        if ($conta == $top_fields) break;
    }
}
// poso sempre el preu encara que hi hagi 200 camps, i el poso l'últim
$currency_name = CURRENCY_NAME=='&euro;'?'€':CURRENCY_NAME; // al pdf queda literalment: &euro;

if ($gl_is_album) { // a l'album el preu surt apart, no es part del llistat de detalls ($fields_pdf)
    if ($price) $price = $price . ' '.$currency_name;
} else {
    if ($price) {
        $fields_pdf[$c_price] = $price;
        if (!$price_consult) $fields_pdf[$c_price] .= ' '.$currency_name;
		// a aparador surt preu a consultar, a album no
		$price = $fields_pdf[$c_price];
    }
}
$number_fields = count($fields_pdf);


// distancia entre camps
if ($number_fields <= $max_fields_showwindow) $text_fields_increment = $text_fields_increment_1;
elseif ($number_fields <= $max_fields_showwindow + 4) $text_fields_increment = $text_fields_increment_2;
else $text_fields_increment = $text_fields_increment_3;
// iniciar pdf
class PDF extends TCPDF {
    var $ref, $c_ref, $logo, $ref_size, $hut, $c_hut, $tipus_value;
    function Header()
    {
        set_pdf_header($this);
    }
    function Footer()
    {
        if (function_exists('set_pdf_footer')) set_pdf_footer($this);
    }
}

Debug::p($gl_print_details, '$gl_print_details');
Debug::p($details, 'details');
Debug::p($fields_pdf, 'fields_pdf');


$pdf = new PDF($orientation, 'pt', $size, true, 'UTF-8', false);
$pdf->logo = $logo;
$pdf->ref = $ref;
$pdf->c_ref = $c_ref;
$pdf->hut = $hut;
$pdf->c_hut = $c_hut;
$pdf->tipus_value = $tipus_value;

// sino hi ha imatges poso nomes el texte
if (count($images) == 0) {
    // així quan no hi ha imatges també fa el 'for' i posa el texte també
    $images['no_images'] = true;
}

if ($gl_is_album) {
    $pdf->ref_size = $ref_size_album;
} else {
    $pdf->ref_size = $ref_size_showwindow;
}
$file_serve_name = $gl_is_album?'album':'aparador';
$file_serve_name .= '_ref_' . $ref . '.pdf';

?>