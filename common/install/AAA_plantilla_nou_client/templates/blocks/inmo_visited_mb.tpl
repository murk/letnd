<section class="inmo-visited">

	<h5><?= $title ?></h5>

	<div id="inmo-visited-slider">

		<? foreach ( $loop as $l ): extract( $l ) ?>

			<? /* TODO-i Canviar el 3 del conta pel número de items-1 que es mostren a l'slider. Si no hi ha slider borrar-ho */ ?>
			<article class="item<?= $conta > 3 ? ' hidden' : '' ?>" onclick="javascript:window.location.href='<?=$show_record_link?>'">

				<? if ( $image_name ): ?>
					<figure>
						<a href="<?= $link ?>">
							<img <?= get_image_attrs( $image_src_details, $image_alt ? $image_alt : $property, 292 ) ?> />
						</a>
					</figure>
				<? endif //image_name?>

				<div class="text">

					<div class="ref">
						<?= $ref ?>
					</div>

					<? if ( $property ): ?>
						<h6><a href="<?=$show_record_link?>"><?= $property ?></a></h6>
					<? endif //$property ?>


					<? if ( $price ): ?>
						<div class="price"><?= $c_price ?>: <?= $price ?> <?= CURRENCY_NAME ?></div>
					<? endif //price?>

					<? if ( $price_consult ): ?>
						<div class="price-consult"><?= $price_consult ?></div>
					<? endif //price_consult?>

				</div>

			</article>

		<? endforeach //loop?>

	</div>

	<? if ( $more ): ?>
		<a href="<?= $more_link ?>" class="visited-more">
			<?= $more ?>
		</a>
	<? endif //$more?>

</section>
