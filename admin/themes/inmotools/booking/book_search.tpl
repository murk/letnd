<link media="screen" href="/common/jscripts/multiselect/css/multi-select.css" rel="stylesheet" type="text/css">
<script src="/common/jscripts/multiselect/js/jquery.multi-select.js" type="text/javascript"></script>

<div class="search">
<h3><?=$c_search_title?></h3> 
<form name="search" method="get" action="/admin/"><table border="0" cellspacing="0" cellpadding="0">
        <tr valign="middle">		
            <td><?=$c_by_words?></td>
            <td> 
              <input name="action" type="hidden" value="search">
              <input name="menu_id" type="hidden" value="<?=$menu_id?>">
              <input name="q" type="text" size="30" value="<?=$q?>"></td>
            <td> 
              <input type="submit" name="Submit" value="ok" class="boto-search"> </td>
            <td>&nbsp;&nbsp;
              <?=$c_by_ref?>&nbsp;</td>
        </tr>
      </table></form>
<form name="search_advanced" method="get" action="/admin/" class="forms-sep"><table class="vertical-caption" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td>
	            <div><?=$c_dates_from?></div>
	            <input name="action" type="hidden" value="search_advanced">
	            <input name="menu_id" type="hidden" value="<?=$menu_id?>">
                <input autocomplete="off" class="alone" id="date_from" name="date_from" type="text" size="8" value="<?=$date_from?>">
	            <label class="checkbox"><?= $exact_date_from_checkbox ?><span><?= $c_exact_date ?></span></label>
            </td>
	        <td>
		        <div><?=$c_dates_to?></div>
		        <input autocomplete="off" class="alone" id="date_to" name="date_to" type="text" size="8" value="<?=$date_to?>">
	            <label class="checkbox"><?= $exact_date_to_checkbox ?><span><?= $c_exact_date ?></span></label>
	        </td>
	        <td></td>
            <td rowspan="2">
	            <?= $fields ?>
            </td>
            <td class="buttons-td"><input type="submit" name="advanced" value="ok" class="boto-search"><input type="submit" name="excel" value="Excel" class="boto-search excel"></td>
        </tr>
		<tr>
	        <td>
		        <div><?=$c_property_id?></div>
		        <input class="alone" id="ref_add" name="ref_add" type="text" size="30" value="<?=$ref_add?>">
		        <input id="ref" name="ref" type="hidden" value="<?=$ref?>">
	        </td>
	        <td>
		        <div><?=$c_status?></div>
		        <?= $status_select ?>
	        </td>
	        <td>
		        <div><?=$c_is_owner?></div>
		        <?= $is_owner_select ?>
	        </td>
			<td></td>
		</tr>
      </table>
</form>
</div>

<script language="JavaScript">


	var fields = $('#fields');
	$(fields).multiSelect(
		{
			selectableOptgroup: true,
			keepOrder: true,
			afterSelect: function(value){
				$('#fields option[value="'+value+'"]').remove();
				$('#fields').append($("<option></option>").attr("value",value).attr('selected', 'selected'));
			},
			selectableHeader: "<div><?= $c_multislect_selectable ?> </div>",
			selectionHeader: "<div><?= $c_multislect_selected ?></div>"
		}
	);
	<? foreach($input_selected_fields as $field): ?>
	$(fields).multiSelect('select','<?= $field ?>');
	<?endforeach //$?>


	add_select_long ('booking', 'book', '152', '', 'ref', false, true, "get_search_ref_var_ajax");

//	var last_valid_data  = {};
//	last_valid_data [field] = {'data': form_id, 'value': $(base_jq + '_add').val() };
//
//	base_jq = '#ref';
//	$(base_jq).autocomplete({
//		serviceUrl: "/admin/?menu_id=152&action=get_search_ref_var_ajax",
//		minChars: 1,
//		autoSelectFirst: true,
//		onSelect: function(suggestion){
//			id = suggestion['data'];
//			value = suggestion['value'];
//
//			if (id) {
//				$(base_jq).val(id);
//			}
//
//		}
//	}).blur(function(){
//		$(base_jq + '_add').val(last_valid_data [field]['value']);
//		$(base_jq).val(last_valid_data [field]['data']);
//		//dp($(base_jq + '_add').val())
//		//dp($(base_jq).val())
//	});


	$(document).ready(
		function ()
		{
			$('#content').height($("#popupFrame", top.document).height());


			$('#date_from').datepicker({
				changeMonth: true,
				changeYear: true,
				yearRange: '+0:+5',
				onClose: function( selectedDate ) {
					var date = $(this).datepicker('getDate');
					if (date) {
						date.setDate(date.getDate() + 1);
					}
					$( '#date_to' ).datepicker( 'option', 'minDate', date );
				}
			});
			$('#date_to').datepicker({
				changeMonth: true,
				changeYear: true,
				yearRange: '+0:+5',
				onClose: function( selectedDate ) {
					var date = $(this).datepicker('getDate');
					if (date) {
						date.setDate(date.getDate() - 1);
					}
					$( '#date_from' ).datepicker( 'option', 'maxDate', date );
				}
			});

		}
	)

</script>