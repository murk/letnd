	<? /* TODO-i  si tot clicable: onclick="window.location.href='<?=$show_record_link?>'" */ ?>
	<article class="item <?= $hidden_class ?><? if ( $offer ) echo ' offer' ?> <?= $status_value ?>">
		
			<? if( $images || $status || $offer ): ?>

				<? /* TODO-i  SI NO son columnes posar la a A i FIGURE a dins l'IF */ ?>
				<a href="<?= $show_record_link ?>" title="<?= htmlspecialchars( $property ) ?>">
					<figure>
						<? if( $image_name ): ?>
							<img <?= get_image_attrs( $image_src_thumb, $image_alt ? $image_alt : $property, 500, 330, false ) ?> />
						<? endif //images?>
					</figure>
				</a>

				<? if( $status ): ?>
					<div class="flag sold">
						<span><?= $status ?></span>
					</div>
				<? elseif( $offer ): ?>
					<div class="flag offer">
						<span><?= $c_offer ?></span>
					</div>
				<? elseif( $recent ): ?>
					<div class="flag recent"><span><?= $c_recent ?></span></div>
				<? endif // $status ?>

			<? endif // images?>

			<? if( $property ): ?>
				<a href="<?= $show_record_link ?>" title="<?= htmlspecialchars( $property ) ?>"><h3>
						<?= $property ?>
					</h3></a>
			<? endif //property?>


			<? if( $ref || $hut ): ?>
				<a class="refs" href="<?= $show_record_link ?>" title="<?= htmlspecialchars( $property ) ?>">

						<span class="ref">
							<?= $c_ref ?> <?= $ref ?>
						</span>

					<? if( $hut ): ?>
						<span class="hut">
								<?= $c_hut ?>. <?= $hut ?>
							</span>
					<? endif //$hut ?>

				</a>
			<? endif // $ref ?>

			<?=
			get_property_details(
				'category_id,floor_space,room,bathroom,wc,land,municipi_id', $this, $l
			)
			?>

			<? if( $old_price || $price || $price_consult || $price_change_percent ): ?>
				<a class="prices" href="<?= $show_record_link ?>" title="<?= htmlspecialchars( $property ) ?>">

					<? if( $old_price && $price_change_percent_value < 0 && $tipus_value != 'temp' ): ?>
						<span class="old-price"><?= $c_price ?>: <?= $old_price ?> <?= CURRENCY_NAME ?></span>
					<? endif //old_price?>

					<? if( $price ): ?>
						<span class="price"><?= $c_price ?>: <?= $price ?> <?= CURRENCY_NAME ?><? if( $tipus_value == 'temp' && $total_nights == 1 ): ?>/n<? endif //$ ?></span>
					<? endif //price?>

					<? if( $price_consult ): ?>
						<span class="price-consult"><?= $price_consult ?></span>
					<? endif //price_consult?>

					<? if( $price_change_percent_value < 0 && $tipus_value != 'temp' ): // només mostra quan baixa per defecte ?>
						<span class="price-change-percent"><?= $price_change_percent_value > 0?'+':'-' ?><?= $price_change_percent ?>&nbsp;&percnt;</span>
					<? endif //price_change_percent?>

				</a>
			<? endif // $old_price || $price || $price_consult || $price_change_percent ?>
	</article>