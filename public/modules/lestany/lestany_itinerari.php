<?
/**
 * LestanyItinerari
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2011
 * @version $Id$
 * @access public
 */
class LestanyItinerari extends Module{
	var $order_by = 'nom asc';
	function __construct(){
		parent::__construct();
	}
	function on_load(){

		$this->set_friendly_params(array(
			'apartat'=>R::text_id('apartat')
			));
		parent::on_load();

	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function get_records()
	{
	    $listing = new ListRecords($this);
		$listing->order_by = $this->order_by;
		$listing->condition = "(status = 'public')";

		$apartat = R::text_id('apartat');

		$listing->set_var('listing_apartat',$apartat);
		$listing->paginate = false;

		$apartat_id = R::id('apartat_id');

		$listing->set_var('listing_apartat_id',$apartat_id);

		if ($apartat_id) $apartat = Db::get_first("
				SELECT apartat
				FROM news__apartat_language
				WHERE language = '". LANGUAGE ."'
				AND apartat_id = '" . $apartat_id ."'");

		else  $apartat = '';

		$listing->set_var('listing_apartat',$apartat);

		$listing->condition .= $this->get_apartat_condition();
		Debug::p( $listing->condition );

		return $listing->list_records();
	}

	function get_apartat_condition() {

		$apartat = R::text_id('apartat');

		if ($apartat){
			if ($apartat == 'aigua')
				$apartat_id = '1';
			if ($apartat == 'terra')
				$apartat_id = '2';
			if ($apartat == 'fonts')
				$apartat_id = '3';
		}
		else {
			$apartat_id = R::id( 'apartat_id' );
		}

		if ($apartat_id) {
			return " AND apartat_id = '" . $apartat_id . "'";
		}
		else {
			return '';
		}
	}

	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{
		$show = new ShowForm( $this );
		$show->order_by = $this->order_by;
		$show->condition = "(status = 'public')";

		$show->condition .= $this->get_apartat_condition();
		$show->get_values();

		Page::set_page_title( $show->rs['page_title'], $show->rs['nom'] );
		Page::set_page_description( $show->rs['page_description'], $show->rs['descripcio_historica'] );
		Page::set_page_keywords( $show->rs['page_keywords'] );

		return $show->show_form();
	}
}
?>