<?
/*
11-10-2019

*/
if ( is_table( 'product__configpublic' ) && ! Db::get_first( "SELECT * FROM product__configpublic WHERE name='has_gift_card'" ) ) {
	$update_sql[] = "
		INSERT INTO product__configpublic (`name`, `value`) VALUES ('has_gift_card', '0');";

}
else {
	if ( is_table( 'product__configpublic' ) ) {
		trigger_error( 'Ja existeixen el camp has_gift_card' );
	}
}

if ( is_table( 'product__order' ) && ! is_field( 'product__order', 'is_gift_card' ) ) {

	$update_sql[] = "ALTER TABLE `product__order`
	ADD COLUMN `is_gift_card` TINYINT(1) NOT NULL DEFAULT 0 AFTER `customer_comment`;
";

}
else {
	if ( is_table( 'product__order' ) ) {
		trigger_error( 'Ja existeixen el camp is_gift_card' );
	}
}