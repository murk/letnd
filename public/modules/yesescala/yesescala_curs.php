<?
/**
 * YesescalaCurs
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2010
 * @version $Id$
 * @access public
 */
class YesescalaCurs extends Module{
	function __construct(){
		parent::__construct();
	}
	function on_load(){		
		$this->set_friendly_params(array('category'=>R::id('category')));
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function get_records()
	{
	    $listing = new ListRecords($this);
				
		$listing->call('records_walk','curs_id,url1,url1_name,url2,url2_name,url3,url3_name,content,content_list');

		$category_id = R::id('category');
		if ($category_id)
		{
			$listing->join = "JOIN yesescala__curs_to_category USING (curs_id)";
			$listing->condition = 'category_id = ' . $category_id;
		}
		
		
		$listing->group_fields = array('target');
		
		$listing->order_by = 'target asc, ordre ASC, curs ASC';	
		
		
		if ($this->parent == 'sitemap') {//Sitemap
			$listing->paginate = false;
			$listing->set_field('modified', 'type', 'none');
			$listing->set_records();
			return $listing->loop;
		}
		
		if ($this->parent == 'Block') {
			$listing->name = 'Block';
			$listing->paginate = false;
			$listing->set_records();
			return $listing->loop;
		}
		
	    return $listing->list_records();
	}
	function records_walk($curs_id, $url1,$url1_name,$url2,$url2_name,$url3,$url3_name,$content=false,$content_list=false)	{		
		
		$ret['url1_name'] = $url1_name?$url1_name:$url1;
		$ret['url2_name'] = $url2_name?$url2_name:$url2;
		$ret['url3_name'] = $url3_name?$url3_name:$url3;
		
		// si es llistat passa lavariable content
		if ($content!==false){
			$ret['content']=$content_list?$content_list:add_dots('content', $content,$this->config['add_dots_length']);
		}
		
		
		$results = Db::get_rows("
			SELECT frequency, price_month, price_quarter
			FROM yesescala__price, yesescala__frequency, yesescala__frequency_language
			WHERE
				yesescala__price.frequency_id = yesescala__frequency.frequency_id AND
				yesescala__frequency.frequency_id = yesescala__frequency_language.frequency_id AND
				language = '".LANGUAGE."' AND
				bin = 0 AND
				curs_id = " . $curs_id . "
			ORDER BY ordre, frequency");
		
		$ret['count_price_month'] = 0;
		$ret['count_price_quarter'] = 0;
		
		foreach ($results as &$rs){
			$rs['price_month_value'] = $rs['price_month'];
			$rs['price_month'] = format_currency($rs['price_month']);
			if ($rs['price_month_value']!='0.00') $ret['count_price_month'] ++;
			
			$rs['price_quarter_value'] = $rs['price_quarter'];
			$rs['price_quarter'] = format_currency($rs['price_quarter']);
			if ($rs['price_quarter_value']!='0.00') $ret['count_price_quarter'] ++;
		}
		
		$ret['prices']= $results;
		
		
		return $ret;
	}
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{
		
		$show = new ShowForm($this);
		$show->call('records_walk','curs_id,url1,url1_name,url2,url2_name,url3,url3_name');
		
	    return $show->show_form();
	}

	function save_rows()
	{
	    $save_rows = new SaveRows($this);
	    $save_rows->save();
	}

	function write_record()
	{
	    $writerec = new SaveRows($this);
		$writerec->set_field('modified','override_save_value','now()');			
		$writerec->field_unique = "curs_file_name";
		$writerec->save();
	}

	function manage_images()
	{
	    $image_manager = new ImageManager($this);
		$image_manager->form_tabs = $this->form_tabs;
	    $image_manager->execute();
	}
	
}
?>