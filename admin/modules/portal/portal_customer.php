<?php
class PortalCustomer extends Module{

	var $template;
	function __construct(){
		parent::__construct();
	}
	function list_records()
	{ 
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function get_records()
	{		 
	    $listing = new ListRecords($this);
	    return $listing->list_records();
	}
	function show_form()
	{					
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{	    
		$show = new ShowForm($this);		
		/*if ($this->template) {
		$show->set_file($this->template);		    
		}*/
	    return $show->show_form();
	}

	function save_rows()
	{
	    $save_rows = new SaveRows($this);
	    $save_rows->save();
	}

	function write_record()
	{
	    $writerec = new SaveRows($this);
	    $writerec->save();
	}

	function manage_images()
	{
	    $image_manager = new ImageManager($this);
	    $image_manager->execute();
	}
	function show_form_map (){ //esenyo el napa
	global $gl_news;
						
	$rs = Db::get_row('SELECT * FROM portal__customer  INNER JOIN portal__municipi USING (municipi_id) WHERE customer_id = ' . $_GET['customer_id']);
	$provincia = Db::get_first('SELECT provincia FROM portal__provincia  WHERE provincia_id = ' . $rs['provincia_id']);
   	$rs['is_default_point'] = ($rs['latitude']!=0)?0:1;
   	$rs['latitude']=$rs['latitude']!=0?$rs['latitude']:PORTAL_GOOGLE_CENTER_LATITUDE;
   	$rs['longitude']=$rs['longitude']!=0?$rs['longitude']:PORTAL_GOOGLE_CENTER_LONGITUDE;
   	$rs['zoom']=$rs['zoom']?$rs['zoom']:PORTAL_GOOGLE_ZOOM;
   	$rs['menu_id'] =  $_GET['menu_id'];		
		$rs['tool'] =  $this->tool;		
		$rs['tool_section'] =  $this->tool_section;			
   	$rs['id'] =  $rs[$this->id_field];		
   	$rs['id_field'] =  $this->id_field;		
	$this->set_vars($rs);//passo els resultats del array
	$this->set_file('portal/customer_map.tpl'); //crido el arxiu
	$this->set_vars($this->caption); //passo tots els captions
	$gl_news = $this->caption['c_click_map'];
	$this->set_var('ondelivery_link',$this->link . '&action=show_ondelivery&process=end');
	$content = $this->process();//processo
	$GLOBALS['gl_content'] = $content;		
	}
	function save_record_map(){
	global $gl_page;
	extract($_GET);    	
	    	Db::execute ("UPDATE `portal__customer` SET
				`latitude` = '$latitude',
				`longitude` = '$longitude',
				`zoom` = '$zoom'
				WHERE `customer_id` =$customer_id
				LIMIT 1") ;
			$gl_message = $gl_messages['point_saved'];
			$gl_page->show_message($this->messages['point_saved']);
			
	}
}
?>