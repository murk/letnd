var map = null;
var polyLine;
var tmpPolyLine;
var markers = [];
var vmarkers = [];
var g = google.maps;
var gl_latlng = new g.LatLng(gl_center_latitude, gl_center_longitude);
var gl_geocoder;
var one_point_marker;

if (typeof (gl_polyline_type) == 'undefined') gl_polyline_type = 'polyline';

var polyOptions = {
	strokeColor: "#3355FF",
	strokeOpacity: 0.8,
	strokeWeight: 4
};
var tmpPolyOptions = {
	strokeColor: "#3355FF",
	strokeOpacity: 0.4,
	strokeWeight: 4
};

var initMap = function (mapHolder) {
	markers = [];
	vmarkers = [];
	var mapOptions = {
		zoom: gl_zoom,
		center: gl_latlng,
		mapTypeId: g.MapTypeId.HYBRID,
		draggableCursor: 'auto',
		draggingCursor: 'move',
		disableDoubleClickZoom: true,
		mapTypeControlOptions: {style: google.maps.MapTypeControlStyle.DROPDOWN_MENU}
	};
	map = new g.Map(document.getElementById(mapHolder), mapOptions);
	g.event.addListener(map, "click", mapLeftClick);
	mapHolder = null;
	mapOptions = null;
};

var initPolyline = function () {

	if (gl_polyline_type == 'polyline') {
		polyLine = new g.Polyline(polyOptions);
	}
	else {
		polyLine = new g.Polygon(polyOptions);
	}
	polyLine.setMap(map);
	if (gl_polyline_type == 'polyline') {
		tmpPolyLine = new g.Polyline(tmpPolyOptions);
	} else {
		tmpPolyLine = new g.Polygon(tmpPolyOptions)
	}
	tmpPolyLine.setMap(map);
};


var mapLeftClick = function (event) {

	var point = event ? event.latLng : gl_latlng;


	if (point) {
		var marker = createMarker(point);
		markers.push(marker);
		if (markers.length != 1) {
			var vmarker = createVMarker(point);
			vmarkers.push(vmarker);
			vmarker = null;
			delete_one_point_marker();
		}
		else {
			create_one_point_marker(point);
		}
		var path = polyLine.getPath();
		path.push(point);
		marker = null;
	}
	event = null;
};
var createMarker = function (point) {
	var imageNormal = new g.MarkerImage(
		"/common/images/maps_polylines/square.png",
		new g.Size(11, 11),
		new g.Point(0, 0),
		new g.Point(6, 6)
	);
	var imageHover = new g.MarkerImage(
		"/common/images/maps_polylines/square_over.png",
		new g.Size(11, 11),
		new g.Point(0, 0),
		new g.Point(6, 6)
	);
	var marker = new g.Marker({
		position: point,
		map: map,
		icon: imageNormal,
		draggable: true
	});
	g.event.addListener(marker, "mouseover", function () {
		marker.setIcon(imageHover);
	});
	g.event.addListener(marker, "mouseout", function () {
		marker.setIcon(imageNormal);
	});
	g.event.addListener(marker, "drag", function () {
		for (var m = 0; m < markers.length; m++) {
			if (markers[m] == marker) {
				polyLine.getPath().setAt(m, marker.getPosition());
				moveVMarker(m);
				break;
			}
		}
		m = null;
	});
	g.event.addListener(marker, "click", function () {
		for (var m = 0; m < markers.length; m++) {
			if (markers[m] == marker) {
				marker.setMap(null);
				markers.splice(m, 1);
				polyLine.getPath().removeAt(m);
				removeVMarkers(m);
				break;
			}
		}
		set_one_point_marker();
		m = null;
	});
	return marker;
};
var createVMarker = function (point) {
	var prevpoint = markers[markers.length - 2].getPosition();
	var imageNormal = new g.MarkerImage(
		"/common/images/maps_polylines/square_transparent.png",
		new g.Size(11, 11),
		new g.Point(0, 0),
		new g.Point(6, 6)
	);
	var imageHover = new g.MarkerImage(
		"/common/images/maps_polylines/square_transparent_over.png",
		new g.Size(11, 11),
		new g.Point(0, 0),
		new g.Point(6, 6)
	);
	var marker = new g.Marker({
		position: new g.LatLng(
			point.lat() - (0.5 * (point.lat() - prevpoint.lat())),
			point.lng() - (0.5 * (point.lng() - prevpoint.lng()))
		),
		map: map,
		icon: imageNormal,
		draggable: true
	});
	g.event.addListener(marker, "mouseover", function () {
		marker.setIcon(imageHover);
	});
	g.event.addListener(marker, "mouseout", function () {
		marker.setIcon(imageNormal);
	});
	g.event.addListener(marker, "dragstart", function () {
		for (var m = 0; m < vmarkers.length; m++) {
			if (vmarkers[m] == marker) {
				var tmpPath = tmpPolyLine.getPath();
				tmpPath.push(markers[m].getPosition());
				tmpPath.push(vmarkers[m].getPosition());
				tmpPath.push(markers[m + 1].getPosition());
				break;
			}
		}
		m = null;
	});
	g.event.addListener(marker, "drag", function () {
		for (var m = 0; m < vmarkers.length; m++) {
			if (vmarkers[m] == marker) {
				tmpPolyLine.getPath().setAt(1, marker.getPosition());
				break;
			}
		}
		m = null;
	});
	g.event.addListener(marker, "dragend", function () {
		for (var m = 0; m < vmarkers.length; m++) {
			if (vmarkers[m] == marker) {
				var newpos = marker.getPosition();
				var startMarkerPos = markers[m].getPosition();
				var firstVPos = new g.LatLng(
					newpos.lat() - (0.5 * (newpos.lat() - startMarkerPos.lat())),
					newpos.lng() - (0.5 * (newpos.lng() - startMarkerPos.lng()))
				);
				var endMarkerPos = markers[m + 1].getPosition();
				var secondVPos = new g.LatLng(
					newpos.lat() - (0.5 * (newpos.lat() - endMarkerPos.lat())),
					newpos.lng() - (0.5 * (newpos.lng() - endMarkerPos.lng()))
				);
				var newVMarker = createVMarker(secondVPos);
				newVMarker.setPosition(secondVPos);//apply the correct position to the vmarker
				var newMarker = createMarker(newpos);
				markers.splice(m + 1, 0, newMarker);
				polyLine.getPath().insertAt(m + 1, newpos);
				marker.setPosition(firstVPos);
				vmarkers.splice(m + 1, 0, newVMarker);
				tmpPolyLine.getPath().removeAt(2);
				tmpPolyLine.getPath().removeAt(1);
				tmpPolyLine.getPath().removeAt(0);
				newpos = null;
				startMarkerPos = null;
				firstVPos = null;
				endMarkerPos = null;
				secondVPos = null;
				newVMarker = null;
				newMarker = null;
				break;
			}
		}
	});
	return marker;
};

var moveVMarker = function (index) {
	var newpos = markers[index].getPosition();
	if (index != 0) {
		var prevpos = markers[index - 1].getPosition();
		vmarkers[index - 1].setPosition(new g.LatLng(
			newpos.lat() - (0.5 * (newpos.lat() - prevpos.lat())),
			newpos.lng() - (0.5 * (newpos.lng() - prevpos.lng()))
		));
		prevpos = null;
	}
	if (index != markers.length - 1) {
		var nextpos = markers[index + 1].getPosition();
		vmarkers[index].setPosition(new g.LatLng(
			newpos.lat() - (0.5 * (newpos.lat() - nextpos.lat())),
			newpos.lng() - (0.5 * (newpos.lng() - nextpos.lng()))
		));
		nextpos = null;
	}
	newpos = null;
	index = null;
};

var removeVMarkers = function (index) {
	if (markers.length > 0) {//clicked marker has already been deleted
		if (index != markers.length) {
			vmarkers[index].setMap(null);
			vmarkers.splice(index, 1);
		} else {
			vmarkers[index - 1].setMap(null);
			vmarkers.splice(index - 1, 1);
		}
	}
	if (index != 0 && index != markers.length) {
		var prevpos = markers[index - 1].getPosition();
		var newpos = markers[index].getPosition();
		vmarkers[index - 1].setPosition(new g.LatLng(
			newpos.lat() - (0.5 * (newpos.lat() - prevpos.lat())),
			newpos.lng() - (0.5 * (newpos.lng() - prevpos.lng()))
		));
		prevpos = null;
		newpos = null;
	}
	index = null;
};

var set_one_point_marker = function (point) {
	if (markers.length != 1) {
		delete_one_point_marker();
	}
	else {
		create_one_point_marker(markers[0].getPosition());
	}
}
var create_one_point_marker = function (point) {
	one_point_marker = new g.Marker({
		position: point,
		map: map
	});
}
var delete_one_point_marker = function (point) {
	if (one_point_marker) one_point_marker.setMap(null);
}

var set_polyline_points = function () {

	var latitudes = gl_latitude.split(';');
	var longitudes = gl_longitude.split(';');
	for (var m = 0; m < latitudes.length; m++) {
		if (latitudes[m]) {
			var latlng = new google.maps.LatLng(latitudes[m], longitudes[m]);

			var marker = createMarker(latlng);
			markers.push(marker);
			if (markers.length != 1) {
				var vmarker = createVMarker(latlng);
				vmarkers.push(vmarker);
				vmarker = null;
				delete_one_point_marker();
			}
			else {
				create_one_point_marker(latlng);
			}
			var path = polyLine.getPath();
			path.push(latlng);
			marker = null;
		}
	}
	m = null;
};


var remove_all_markers = function (index) {
	for (var m = 0; m < markers.length; m++) {
		markers[m].setMap(null);

	}
	for (var m = 0; m < vmarkers.length; m++) {
		vmarkers[m].setMap(null);

	}
	polyLine.setMap(null);
	tmpPolyLine.setMap(null);

	if (gl_polyline_type == 'polyline') {
		polyLine = new g.Polyline(polyOptions);
	}
	else {
		polyLine = new g.Polygon(polyOptions);
	}
	polyLine.setMap(map);

	if (gl_polyline_type == 'polyline') {
		tmpPolyLine = new g.Polyline(tmpPolyOptions);
	}
	else {
		tmpPolyLine = new g.Polygon(tmpPolyOptions);
	}
	tmpPolyLine.setMap(map);

	markers = [];
	vmarkers = [];
	delete_one_point_marker();
};


window.onload = function () {
	initMap('google_map');
	initPolyline();
	set_polyline_points();
	gl_geocoder = new google.maps.Geocoder();

	//si  no hi ha punt guardat, intenta buscar automaticament el punt
	if (gl_is_default_point) {
		search_point();
	}
};


var save_map = function save_map() {
	var latitudes = new Array();
	var longitudes = new Array();

	for (var m = 0; m < markers.length; m++) {
		var pos = markers[m].getPosition();
		latitudes[m] = pos.lat();
		longitudes[m] = pos.lng();

	}

	var gl_latitude = latitudes.join(';');
	var gl_longitude = longitudes.join(';');
	//alert(message);


	//if (gl_infowindow) gl_infowindow.close();
	var zoom_actual = map.getZoom();
	showPopWin('', 300, 100, null, false, false, MODALBOX_SAVE);

	//window.save_frame.location.href = '/admin/?action=save_record_map' +
	//'&menu_id=' + gl_menu_id +
	//'&' + gl_id_field + '=' + gl_id +
	//'&zoom=' + zoom_actual +
	//'&latitude=' + gl_latitude +
	//'&longitude=' + gl_longitude +
	//'&center_latitude=' + map.getCenter().lat() +
	//'&center_longitude=' + map.getCenter().lng();

	$.ajax({
		url: '/admin/?action=save_record_map' +
		'&menu_id=' + gl_menu_id,
		data:  gl_id_field + '=' + gl_id +
		'&zoom=' + zoom_actual +
		'&latitude=' + gl_latitude +
		'&longitude=' + gl_longitude +
		'&center_latitude=' + map.getCenter().lat() +
		'&center_longitude=' + map.getCenter().lng(),
		dataType: 'json',
		type: 'POST'
	})
		.done(function(data) {
			show_global_message (data.message);
		});



}


//funcio que busca la direcció
function search_point() {
	var address = document.getElementById("map_address").value;
	gl_geocoder.geocode({'address': address}, function (results, status) {
		if (status == google.maps.GeocoderStatus.OK) {
			gl_geocoder_results = true;
			map.setZoom(14); //canvio el zoom
			map.setCenter(results[0].geometry.location);
			gl_latlng = results[0].geometry.location;

			//mapLeftClick(false);
			//mostra_info();

		} else {
			showPopWin('', 300, 100, null, false, false, MAP_FRAME_TEXT_4);
			window.setTimeout("hidePopWin(false);", 800)
		}
	});
	return false;
}