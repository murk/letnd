customer_show_address_first = true;

function customer_show_address(obj){
	$('#delivery_address').toggle(200);
	if (customer_show_address_first){
		$('body').click();
		nom = $("input[name='name[0]']").val();
		cognom = $("input[name='surname[0]']").val();
		$("input[name='a_name']").val(nom);
		$("input[name='a_surname']").val(cognom);
		customer_show_address_first = false;
	}
}

function address_add(customer_id){
	showPopWin('/admin/?tool=product&tool_section=address&template=window&action=show_form_new&menu_id=20104&customer_id='+customer_id, 850, 500, false, true);
}
function address_edit(address_id){
	showPopWin('/admin/?tool=product&tool_section=address&template=window&action=show_form_edit&menu_id=20104&address_id='+address_id, 850, 500, false, true);

}
function activate_customer(customer_id){
	if (confirm(CONFIRM_TEXT)) {
		top.showPopWin("", 300, 100, null, false, false, WAIT_TEXT);
		window.save_frame.location.href = '/admin/?menu_id=20104&action=activate_customer&activated=1&customer_id='+customer_id;
	}
}
function deactivate_customer(customer_id){
	if (confirm(CONFIRM_TEXT)) window.save_frame.location.href = '/admin/?menu_id=20104&action=activate_customer&activated=0&customer_id='+customer_id;
}
function set_wholesaler(customer_id){
	if (confirm(CONFIRM_TEXT)) window.save_frame.location.href = '/admin/?menu_id=20104&action=set_wholesaler&is_wholesaler=1&customer_id='+customer_id;
}
function unset_wholesaler(customer_id){
	if (confirm(CONFIRM_TEXT)) window.save_frame.location.href = '/admin/?menu_id=20104&action=set_wholesaler&is_wholesaler=0&customer_id='+customer_id;
}

function customer_show_invoice(order_id){
	showPopWin('/?tool=product&tool_section=customer&template=clean&action=show_invoice&invoice=invoice&order_id='+order_id+'&language=' + gl_language, 800, 600, false, true, false, false, true);

}
function customer_show_invoice_refund(order_id){
	showPopWin('/?tool=product&tool_section=customer&template=clean&action=show_invoice&invoice=refund&order_id='+order_id+'&language=' + gl_language, 800, 600, false, true, false, false, true);

}

function set_country_change (num, id) {

	if (id==0 || id){

		var country = '#country_id' + num + '_' + id;

		if (!$(country).length) return;

		$(country).change(function(){
			country_on_change (num, this.value)
		});

		country_on_change (num, $(country).val());
	}
}
function country_on_change (num, value) {

	var provincia = $('#provincia_id' + num + '_holder');

	if(value=='ES'){
		provincia.show();
	}
	else{
		provincia.hide();
	}
}

$(function(){

	if (typeof (gl_tool_section) === 'undefined') gl_tool_section = 'customer';

	var id = $("input[name^='" + gl_tool_section + "_id']").val();

	set_country_change ('', id);
	set_country_change ('2', '999');

});

var order = {
	shipped_emailed: '', delivered_emailed: '', prepared_emailed: '',
	init: function (){
		if (gl_current_form_id !== false) {
			order.set_mail_on_change();
			order.show_mail_links();
		}
	},
	set_mail_on_change: function () {
		var id = gl_current_form_id;
		dp(id);
		$('#shipped_' + id).change(function () {
			order.show_mail_links();
		});
		$('#delivered_' + id).change(function () {
			order.show_mail_links();
		});
	},
	show_mail_links: function () {
		var id = gl_current_form_id;
		var shipped = $('#shipped_' + id).val();
		var delivered = $('#delivered_' + id).val();

		order.swap_show('prepared_mail', !order.prepared_emailed);
		order.swap_show('shipped_mail', !order.shipped_emailed && shipped);
		order.swap_show('delivered_mail', !order.delivered_emailed && delivered);

		order.swap_show('prepared_mail_re', order.prepared_emailed);
		order.swap_show('shipped_mail_re', order.shipped_emailed && shipped);
		order.swap_show('delivered_mail_re', order.delivered_emailed && delivered);

		order.swap_show('prepared_emailed', order.prepared_emailed);
		order.swap_show('shipped_emailed', order.shipped_emailed && shipped);
		order.swap_show('delivered_emailed', order.delivered_emailed && delivered);

	},
	swap_show: function (id_show, val) {
		if (val) $('#' + id_show).show();
		else  $('#' + id_show).hide();
	},
	send_mail: function (mail_status){

		// guardo formulari
		// $('#theForm').submit();
		showPopWin('', 300, 100, null, false, false, order.c_sending_email);

		var status_date_obj = $('#' + mail_status + '_' + gl_current_form_id);
		var status_date = '';
		var status_time = '';
		var courier_id = $('#courier_id_' + gl_current_form_id).val();
		var courier_follow_code = $('#courier_follow_code_' + gl_current_form_id).val();

		if (status_date_obj.length) {
			status_date = status_date_obj.val();
			status_time = $('#' + mail_status + '_time_' + gl_current_form_id).val();
		}

		$.ajax({
			url: '/admin/?action=send_mail_ajax&tool=product&tool_section=order&language='+order.customer_language,
			data: 'id=' + gl_current_form_id +
			'&mail_status=' + mail_status +
			'&status_date=' + status_date +
			'&status_time=' + status_time +
			'&courier_id=' + courier_id +
			'&courier_follow_code=' + courier_follow_code,
			dataType: 'json',
			type: 'POST'
		})
			.done(function(data) {
				if (data.error){
					hidePopWin(false);
					alert(data.error);
				}
				else {
					$('#' + mail_status + '_emailed').html(order.c_mail_sent + ': ' + data.date_emailed);
					dp('#' + mail_status + '_emailed');
					dp(order.c_mail_sent + ': ' + data.date_emailed);
					order[mail_status + '_emailed'] = data.date_emailed;
					hidePopWin(false);
					showPopWin('', 300, 100, null, false, false, order.c_email_sended_correctly);
					window.setTimeout("hidePopWin(false);", 1000);
					order.show_mail_links();
				}
			});

	}
};