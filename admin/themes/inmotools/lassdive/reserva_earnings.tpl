<div id="lassdive-report" class="no-print">
	<h6>Resum d'entrades</h6>

	Total: <strong class="lassdive-total">
				<? if ( $all_basetax ): ?>
					<span class="no-discount"><?= $all_basetax ?></span>
					<span class="discount"> - <?= $all_discount ?> =</span>
				<? endif // $all_basetax ?>

			<?= $all_basetax_discounted ?>
	</strong>


	<? if ( $products || $users ): ?>
	<table>
		<? endif // $products || $users ?>

		<? if ( $products ): ?>
			<thead>
				<tr>
					<th><?= $c_product_title ?></th>
					<th class="price"></th>
					<th class="price"><?= $c_total ?></th>
				</tr>
			</thead>
			<? foreach ( $products as $l ): extract( $l ) ?>
				<tbody>
					<tr>
						<td><?= $product_title ?></td>
						<td class="price no-discount"><?= $all_basetax ?></td>
						<td class="price"><?= $all_basetax_discounted ?></td>
					</tr>
				</tbody>

			<? endforeach //$products?>

		<? endif // $products ?>

		<? if ( $users ): ?>
			<thead>
				<tr>
					<th><?= $c_user_id ?></th>
					<th class="price"></th>
					<th class="price"><?= $c_total ?></th>
				</tr>
			</thead>
			<? foreach ( $users as $l ): extract( $l ) ?>
				<tbody>
					<tr>
						<td><?= $name ?></td>
						<td class="price no-discount"><?= $all_basetax ?></td>
						<td class="price"><?= $all_basetax_discounted ?></td>
					</tr>
				</tbody>

			<? endforeach //$users?>

		<? endif // $users ?>

		<? if ( $products || $users ): ?>
	</table>
<? endif // $products || $users ?>


</div>