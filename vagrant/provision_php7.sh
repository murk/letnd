#! /usr/bin/env bash

# Variables
APPENV=local
DBHOST=localhost
DBNAME=dbname
DBUSER=root
DBPASSWD=passbarrat

echo "set grub-pc/install_devices /dev/sda" | debconf-communicate
apt-get update
apt-get upgrade -y --force-yes -qq


##########################################
# PAQUETS BÀSICS                         #
##########################################
echo -e "\n--- Install base packages ---\n"
# apt-get -y install vim curl build-essential python-software-properties git # > /dev/null 2>&1

apt-get -y install vim
apt-get install git -y > /dev/null
apt-get install samba -y > /dev/null

##########################################
# DEFCONF UTILITATS                      #
##########################################
echo -e "\n--- Install debconf ---\n"
apt-get install debconf-utils -y > /dev/null

#echo -e "\n--- Install lsybcd ---\n"
# apt-get install nfs-kernel-server
# apt-get -y install lsyncd



##########################################
# HOSTS                                  #
##########################################
echo -e "\n--- Entrada a hosts per obrir errors ---\n"
echo '10.5.5.1 w2.de' >> /etc/hosts



##########################################
# MYSQL                                  #
##########################################
echo -e "\n--- Install MySQL specific packages and settings ---\n"

debconf-set-selections <<< "mysql-server mysql-server/root_password password $DBPASSWD"
debconf-set-selections <<< "mysql-server mysql-server/root_password_again password $DBPASSWD"
apt-get install -y mysql-server

echo -e "\n---MYSQL Allow connect all ---\n"
sed -i "s/127.0.0.1/0.0.0.0/g" /etc/mysql/mariadb.conf.d/50-server.cnf
sed -i "s/skip-networking/#skip-networking/g" /etc/mysql/mariadb.conf.d/50-server.cnf
sed -i "s/.*max_allow_packet.*/max_allow_packet = 50M/" /etc/mysql/mariadb.conf.d/50-server.cnf
sed -i "s/.*max_connections.*/max_connections = 5/" /etc/mysql/mariadb.conf.d/50-server.cnf


cat > /etc/mysql/my.cnf <<EOF
	key_buffer_size=64M
EOF

/etc/init.d/mysql restart

# essencial per accedir desde fora
mysql -uroot -ppassbarrat -e "CREATE USER 'root'@'%' IDENTIFIED BY 'passbarrat';GRANT ALL PRIVILEGES ON * . * TO 'root'@'%';FLUSH PRIVILEGES;";

# mysql -uroot -ppassbarrat -e "CREATE USER 'usuari'@'%' IDENTIFIED BY 'passbarrat';GRANT ALL PRIVILEGES ON * . * TO 'root'@'%';FLUSH PRIVILEGES;";



##########################################
# APACHE                                 #
##########################################
# Apache
echo -e "\n--- Install apache ---\n"
apt-get -y install apache2
a2enmod rewrite > /dev/null 2>&1
a2enmod substitute > /dev/null 2>&1
a2enmod headers > /dev/null 2>&1

echo -e "\n--- APACHE conf ---\n"
sed -i "s/AllowOverride None/AllowOverride All/g" /etc/apache2/apache2.conf
# sed -i "s/DocumentRoot \/var\/www/DocumentRoot \/letnd/g" /etc/apache2/apache2.conf
sed -i "s/Directory \/var\/www/Directory \/letnd/g" /etc/apache2/apache2.conf


echo -e "\n--- Edit apache conf ---\n"
cat > /etc/apache2/sites-available/000-default.conf <<EOF
<VirtualHost *:80>
DocumentRoot "/letnd"
<Location "/">
SetOutputFilter DEFLATE
</Location>
</VirtualHost>

EnableSendfile off

Header merge Cache-Control no-cache

KeepAliveTimeout 2

EOF


##########################################
# PHP  7                                 #
##########################################

sudo apt install -y apt-transport-https lsb-release ca-certificates > /dev/null 2>&1
sudo wget -O /etc/apt/trusted.gpg.d/php.gpg https://packages.sury.org/php/apt.gpg > /dev/null 2>&1
sudo sh -c 'echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" > /etc/apt/sources.list.d/php.list'
sudo apt update

sudo apt install -y php7.3 php7.3-common php7.3-cli libapache2-mod-php7.3 php7.3-mysql php7.3-xml php7.3-curl php7.3-mbstring php7.3-zip php-xdebug php7.3-enchant php-imagick php7.3-gd > /dev/null 2>&1



##########################################
# PHP CONF                               #
##########################################
echo -e "\n--- Edit PHP conf ---\n"
cat >> /etc/php/7.3/apache2/php.ini <<EOF

include_path = "/php_includes"

error_reporting = E_ALL | E_STRICT

error_prepend_string = "<a href='javascript: void(0)' onclick='(function (obj){var ch = obj.children;var client = new XMLHttpRequest();client.open(\"GET\", \"/common/open_php.php?file=\"+ch[2].innerHTML +\"&line=\"+ch[3].innerHTML);client.send();})(this);'>"
error_append_string = "</a>"

EOF
cat >> /etc/php/7.3/cli/php.ini <<EOF

include_path = "/php_includes"


EOF

sed -i "s/;realpath_cache_size = 4096k/realpath_cache_size = 4096k/" /etc/php/7.3/apache2/php.ini
sed -i "s/short_open_tag = Off/short_open_tag = On/" /etc/php/7.3/apache2/php.ini
sed -i "s/display_errors = Off/display_errors = On/" /etc/php/7.3/apache2/php.ini
sed -i "s/;always_populate_raw_post_data = -1/always_populate_raw_post_data = -1/" /etc/php/7.3/apache2/php.ini

sed -i "s/short_open_tag = Off/short_open_tag = On/" /etc/php/7.3/cli/php.ini
sed -i "s/display_errors = Off/display_errors = On/" /etc/php/7.3/cli/php.ini
sed -i "s/;always_populate_raw_post_data = -1/always_populate_raw_post_data = -1/" /etc/php/7.3/cli/php.ini

#sudo php5dismod xdebug
#sudo php5enmod xdebug

cat >> /etc/php/7.3/mods-available/xdebug.ini <<EOF

xdebug.remote_host=10.5.5.1
xdebug.remote_enable=1
xdebug.remote_port=9000
xdebug.profiler_enable=0
xdebug.profiler_enable_trigger=1
xdebug.idekey="PHPSTORM"
xdebug.profiler_output_dir="/letnd/vagrant/tmp"
xdebug.default_enable=0
xdebug.remote_autostart = 1

EOF

##########################################
# MYSQL BACKUP                           #
##########################################
echo -e "\n--- Cron per mysql backup ---\n"
chmod +x /letnd/vagrant/mysql_backup.sh
(crontab -l 2>/dev/null; echo "0,30 * * * * /letnd/vagrant/mysql_backup.sh") | crontab -



##########################################
# LOCALES                                #
##########################################
echo -e "\n--- Locales ---\n"
echo 'en_GB.UTF-8 UTF-8' >> /etc/locale.gen
echo 'en_GB ISO-8859-1' >> /etc/locale.gen

echo 'ca_ES.UTF-8 UTF-8' >> /etc/locale.gen
echo 'ca_ES ISO-8859-1' >> /etc/locale.gen

echo 'es_ES.UTF-8 UTF-8' >> /etc/locale.gen
echo 'es_ES  ISO-8859-1' >> /etc/locale.gen

echo 'fr_FR.UTF-8 UTF-8' >> /etc/locale.gen
echo 'fr_FR ISO-8859-1' >> /etc/locale.gen

echo 'de_DE.UTF-8 UTF-8' >> /etc/locale.gen
echo 'de_DE ISO-8859-1' >> /etc/locale.gen
/usr/sbin/locale-gen

timedatectl set-timezone Europe/Madrid

##########################################
# LETND COMMAND LINE                  #
##########################################
echo -e "\n--- Letnd command line setup ---\n"
chmod +x letnd
sudo ln -s /letnd/letnd /usr/local/bin/letnd

##########################################
# SSL                                    #
##########################################

#
# S'ha de obrir el port 443 del router per fer-ho de fora
#
# Afegir SSL per tots els sites
sudo a2enmod ssl
mkdir /etc/apache2/ssl

#
# Per un client concret
#

#cat >> /etc/apache2/sites-available/000-default.conf <<EOF
#
#<VirtualHost w2.letnd.com:443>
#	DocumentRoot "/letnd"
#	<Location "/">
#		SetOutputFilter SUBSTITUTE;DEFLATE
#		AddOutputFilterByType SUBSTITUTE text/html
#		Substitute "s|<!DOCTYPE html>|<!DOCTYPE html><script src=\"//localhost:35729/livereload.js\"></script>|i"
#	</Location>
#
#	SSLEngine on
#	SSLCertificateFile /etc/apache2/ssl/w2.letnd.com.cert
#	SSLCertificateKeyFile /etc/apache2/ssl/w2.letnd.com.key
#</VirtualHost>
#
#EOF

# Generar els certificats

#openssl genrsa -out /etc/apache2/ssl/w2.letnd.com.key 2048
#openssl req -new -x509 -key /etc/apache2/ssl/w2.letnd.com.key -out /etc/apache2/ssl/w2.letnd.com.cert -days 3650 -subj /CN=w2.letnd.com
#/etc/init.d/apache2 restart