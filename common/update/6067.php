<?

/* 1-3-16

	INMO
	- Seleccionar immobles portals
 */

if ( is_table( 'inmo__property' ) ) {

	$update_sql .= "
		INSERT INTO `all__menu` (`menu_id`, `menu_group`, `tool`, `tool_section`, `action`, `parent_id`, `toolmode_id`, `variable`, `ordre`, `link`, `process`, `bin`, `read1`, `write1`)
VALUES (220, 'inmo', 'inmo', 'portal', '', 100, 1, 'INMO_MENU_PORTAL', 6, '', '', 0, ',1,3,2,', ',1,2,');
INSERT INTO `all__menu` (`menu_id`, `menu_group`, `tool`, `tool_section`, `action`, `parent_id`, `toolmode_id`, `variable`, `ordre`, `link`, `process`, `bin`, `read1`, `write1`)
VALUES (221, 'inmo', 'inmo', 'portal', 'list_records', 220, 1, 'INMO_MENU_PORTAL_LIST', 1, '', '', 0, ',1,3,2,', ',1,2,');
INSERT INTO `all__menu` (`menu_id`, `menu_group`, `tool`, `tool_section`, `action`, `parent_id`, `toolmode_id`, `variable`, `ordre`, `link`, `process`, `bin`, `read1`, `write1`)
VALUES (223, 'inmo', 'inmo', 'portal', 'list_records', 220, 1, 'INMO_MENU_PORTAL_LIST_SELECTED', 2, '', 'portal_selected', 0,
        ',1,3,2,', ',1,2,');
INSERT INTO `missatge__missatge` (`status`, `kind`, `from_id`, `to_id`, `asunto`, `missatge`, `blocked`, `entered`, `link`, `link_text`, `bin`) VALUES ('waiting', NULL, 238, 208, 'Actualització portal apicat', 'Per poder publicar immobles en el portal \"api.cat\", sol·liciteu-nos informació a <a href=\"mailto:info@letnd.com?subject=Informacio publicació portal api.cat\">info@letnd.com</a>', 0, '2016-03-03 12:27:00', '', '', 0);

		";
}

if ( is_table( 'inmo__property' ) ) {

	if ( ! is_table( 'inmo__property_to_portal' ) ) {

		$update_sql .= "CREATE TABLE `inmo__property_to_portal` (
						`property_portal_id` INT(11) NOT NULL AUTO_INCREMENT,
						`property_id` INT(11) NOT NULL DEFAULT '0',
						`apicat` TINYINT(1) NOT NULL DEFAULT '0',
						PRIMARY KEY (`property_portal_id`)
					)
					COLLATE='utf8_spanish2_ci'
					ENGINE=MyISAM
					;";

	} else {
		trigger_error( 'Ja existeixen la taula inmo__property_to_portal' );
	}

}

?>