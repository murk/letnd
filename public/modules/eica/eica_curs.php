<?
/**
 * EicaCurs
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2010
 * @version $Id$
 * @access public
 */
class EicaCurs extends Module{
	function __construct(){
		parent::__construct();
	}
	function on_load(){
		$this->set_field( 'weekday_id', 'type', 'none' );
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function get_records()
	{
	    $listing = new ListRecords($this);
				
		$listing->call('records_walk','curs_id,url1,url1_name,url2,url2_name,url3,url3_name,content,content_list');
		
		
		$listing->group_fields = array('target');
		$listing->condition = "(status = 'public')";

		// filtres
		$idioma_id = R::text_id( 'idioma_id', 'null' );
		$target_id = R::text_id( 'target_id', 'null' );

		if ( $idioma_id != 'null' ) {
			$listing->condition .= " AND idioma_id = '" . $idioma_id . "'";
		}
		if ( $target_id != 'null' ) {
			$listing->condition .= " AND target_id = '" . $target_id . "'";
		}


		$listing->order_by = 'ordre ASC, curs ASC';
		
		
		if ($this->parent == 'sitemap') {//Sitemap
			$listing->paginate = false;
			$listing->set_field('modified', 'type', 'none');
			$listing->set_records();
			return $listing->loop;
		}
		
		if ($this->parent == 'Block') {
			$listing->name = 'Block';
			$listing->paginate = false;
			$listing->set_records();
			return $listing->loop;
		}
		
	    return $listing->list_records();
	}
	function records_walk($curs_id, $url1,$url1_name,$url2,$url2_name,$url3,$url3_name,$content=false,$content_list=false)	{

		if (!$curs_id) return;

		$ret['url1_name'] = $url1_name?$url1_name:$url1;
		$ret['url2_name'] = $url2_name?$url2_name:$url2;
		$ret['url3_name'] = $url3_name?$url3_name:$url3;
		
		// si es llistat passa lavariable content
		if ($content!==false){
			$ret['content']=$content_list?$content_list:add_dots('content', $content,$this->config['add_dots_length']);
		}
		
		
		$results = Db::get_rows("
			SELECT weekday
			FROM eica__curs_to_weekday, eica__weekday, eica__weekday_language
			WHERE
				eica__curs_to_weekday.weekday_id = eica__weekday.weekday_id AND
				eica__weekday.weekday_id = eica__weekday_language.weekday_id AND
				language = '".LANGUAGE."' AND
				curs_id = " . $curs_id . "
			ORDER BY ordre ASC, weekday ASC");
		
		$weekdays = array();

		$conta = 0;
		foreach ($results as $rs){
			if ( $conta == 0 ) {
				$conta = 1;
			}
			else {
				$rs['weekday'] = mb_strtolower( $rs['weekday'] );
			}
			$weekdays []= $rs['weekday'];
		}

		if ( count($weekdays) > 1 ) {
			$last_price = array_pop( $weekdays );
			$weekdays = implode( ', ', $weekdays );
			$weekdays = $weekdays . ' ' . $this->caption['c_or'] . ' ' . $last_price;
		}
		else {
			$weekdays = $weekdays[0];
		}
		
		$ret['weekday_id']= $weekdays;
		
		
		return $ret;
	}
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{
		
		$show = new ShowForm($this);
		$show->condition = "(status = 'public')";
		$show->call('records_walk','curs_id,url1,url1_name,url2,url2_name,url3,url3_name');

		// TODO-i Quan no hi hagi un curs, las clase show_form ha de fer un redirect
	    return $show->show_form();
	}

	function save_rows()
	{
	    $save_rows = new SaveRows($this);
	    $save_rows->save();
	}

	function write_record()
	{
	    $writerec = new SaveRows($this);
		$writerec->set_field('modified','override_save_value','now()');			
		$writerec->field_unique = "curs_file_name";
		$writerec->save();
	}

	function manage_images()
	{
	    $image_manager = new ImageManager($this);
		$image_manager->form_tabs = $this->form_tabs;
	    $image_manager->execute();
	}
	
}
?>