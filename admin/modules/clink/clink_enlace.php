<?
/**
 * ClinkEnlace
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2011
 * @version $Id$
 * @access public
 */
class ClinkEnlace extends Module{

	function __construct(){
		parent::__construct();
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function get_records()
	{
	    $listing = new ListRecords($this);
		$listing->condition = "clink__category_language.language = '".LANGUAGE."'";
		$listing->call('records_walk','content');
	
		if (isset($_GET['selected_menu_id']))
		{
			$listing->condition = 'category_id = ' . R::id('category_id');
		}
		else
		{
		$listing->add_filter('category_id');
		}
		$listing->add_filter('destino');
		$listing->group_fields = array('category_id');
		$listing->join = 'LEFT OUTER JOIN clink__category_language USING (category_id)';
		$listing->order_by = 'category ASC, enlace ASC';		
	    return $listing->list_records();
	}
	function records_walk($content){			
		$ret['content']=add_dots('content', $content);
		return $ret;
	}
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{
	    // si no hi ha cap categoria faig un redirect a entrar categoria nova
		if (!Db::get_first("SELECT count(*) FROM clink__category LIMIT 0,1")){
			$_SESSION['message'] = $this->messages['no_category'];
			redirect('/admin/?menu_id=3812');
		}
		$show = new ShowForm($this);
	    return $show->show_form();
	}

	function save_rows()
	{
	    $save_rows = new SaveRows($this);
	    $save_rows->save();
	}

	function write_record()
	{
	    $writerec = new SaveRows($this);
		$writerec->save();
	}

	function manage_images()
	{
	    $image_manager = new ImageManager($this);
	    $image_manager->execute();
	}
}
?>