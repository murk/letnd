<?
/**
 * CustumerCustumer
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) Juliol 2014
 * @version $Id$
 * @access public
 */
class CustumerCustumer extends Module{
	var $menus, $menu_id, $never_mind_logged = false, $is_test = false, $saved, $id, $is_ajax, $rs, $js_string, $is_property_book = false, $is_book_edit = false, $is_add_custumers = false, $is_main_custumer = true;
	
	function __construct(){
		parent::__construct();
	}
	function on_load(){
		
		$this->set_field('treatment', 'default_value', 'sr');

		$this->set_requireds();

		// si es privada no deixo fer res excepte entrar login i password
		// està tot controlat a do_action , aquí nomès es pot entrar login, registrar-se i enviar pass i enviar mail registrat sense estar logejat		
	}

	function set_requireds() {

		// els requireds per defecte
		$this->set_field('name', 'required', '1');
		$this->set_field('surname1', 'required', '1');
		$this->set_field('phone1', 'required', '1');
		$this->set_field('mail', 'required', '1');
		$this->set_field('password', 'required', '1');
		$this->set_field('birthdate', 'required', '1');
		$this->set_field('adress', 'required', '1');
		$this->set_field('town', 'required', '1');
		$this->set_field('zip', 'required', '1');
		$this->set_field('vat', 'required', '1');

		// sumo el que poso al config
		$required_add = $this->config['required_add'];
		if ($required_add) {
			$required_add = explode( ',', $required_add );
			foreach ( $required_add as $required ) {
				$this->set_field( $required, 'required', '1' );
			}

		}

		// resto els que poso al config
		$required_remove = $this->config['required_remove'];
		if ($required_remove) {
			$required_remove = explode( ',', $required_remove );
			foreach ( $required_remove as $required ) {
				$this->set_field( $required, 'required', '' );
			}
		}
	}
	function new_custumer() {
		
		// si algu treu ordre o book_id de la url, sempre vull un ordre o book
		$ordre = R::number('ordre');
		$book_id = R::number('book_id');		
		if (!$ordre || !$book_id) return; 
		
		$this->process = $_GET['process'] = 'new_custumer'; // forço el get perque surti al link_action
		$this->is_main_custumer = false;
		$this->action = 'get_form_new';
		
		$fields = array('adress','numstreet', 'block','flat','door','province','zip');
		
		$rs = Db::get_row("
			SELECT " . implode(',', $fields) . "
			FROM custumer__custumer 
			WHERE custumer_id = " . $_SESSION['custumer']['custumer_id']);
		
		
		foreach ($fields as $f) {			
			$this->set_field($f,'default_value',$rs[$f]);
		}
		
		$this->show_form();
	}
	function show_form()
	{
		// Nomes poso el menu de customer quan aquest modul es index ( o sigui no el crido desde cap altre modul)
		if ($this->is_logged() && $this->is_index) {
			$GLOBALS['gl_content'] = $this->get_menu_custumer() . $this->get_form();
			$GLOBALS['gl_page']->title = $this->caption['c_custumer_menu_personal_data'];
		}
	}
	function get_form()
	{
		$this->set_field('prefered_language', 'default_value', LANGUAGE);
		
		
		$this->set_var('is_add_custumers',$this->is_add_custumers);
		$this->set_var('is_property_book',$this->is_property_book);
		$this->set_var('is_login',$this->process=='login'?true:false);
		$this->set_var('is_book_edit',$this->is_book_edit);

		$show = new ShowForm($this);
		
		$show->set_vars(array(
			'is_logged' => $this->is_logged()
			));	
		// per el formulari de fer reserva desde la propietat, desde la fitxa reserva o per el formulari d'inici de sessio
		if ($this->parent == 'BookingBook' || $this->process == 'login') {
			
			$show->set_vars(array(
				'login_mail' => '<input type="text" id="login_mail" name="login_mail">',
				'login_password' => '<input type="password" id="login_password" name="login_password">'
			));	
			
			if ($this->process == 'login') {
				$_GET['process'] = 'login'; // així es propaga
				$show->template = 'custumer/custumer_login.tpl';
			}
			else{
				$show->template = 'booking/book_custumer_form.tpl';
			}
			
			
			if ($this->is_logged()){
				$this->set_field('password', 'form_public', 'out');
				$this->action = 'get_form_edit';
				$show->id = $_SESSION['custumer']['custumer_id'];
			}
			else{
				$this->set_field('password', 'required', '1');
				$this->action = 'get_form_new';	
			}
			
			$edit_custumer_link = '/' . LANGUAGE . '/custumer/custumer/show-form-edit/custumer_id/' . $show->id . '/?menu_id=2&amp;return-url=' . urlencode($_SERVER['REQUEST_URI']);
			
			$this->set_var('edit_custumer_link', $edit_custumer_link);
		
		}
		// la resta es per el formulari de custumer
		
		$show->get_values();
		
		if ($show->id) $this->is_main_custumer = Db::get_first ("SELECT book_main FROM custumer__custumer WHERE custumer_id = " . $show->id);
		
		
		$this->set_var('is_main_custumer',$this->is_main_custumer);
		
		$this->rs = $show->rs;
		
		$ret = $show->show_form();
		
		$this->js_string = $show->js_string;
		
	    return $ret;
	}
	function list_records()
	{
		$this->list_book();
	}
	function list_book()
	{
		$module = Module::load('booking','book',$this);
		$module->is_logged = true;
		$books = $module->do_action('get_records');

		$GLOBALS['gl_content'] = $this->get_menu_custumer() . $books;
		$GLOBALS['gl_page']->title = $this->caption['c_custumer_menu_books'];
	}
	function show_book()
	{
		$module = Module::load('booking','book');
		$module->is_logged = true;
		$book = $module->do_action('get_form_edit');

		$GLOBALS['gl_content'] = $this->get_menu_custumer() . $book;
	}

	function write_record()
	{
	    Debug::p($_POST, 'post');
	    // Debug::p($_FILES, 'files');
		// Debug::p($this->process, 'process');

		// Només poso el captcha en l'usuari principal, els altres han d'estar logejats
		if ($this->action == 'add_record' && $this->process!='new_custumer') check_google_recaptcha();

		// si algu treu ordre o book_id de la url, sempre vull un ordre o book
		if($this->process == 'new_custumer'){
			$ordre = R::number('ordre');
			$book_id = R::number('book_id');			
			if (!$ordre || !$book_id) return; 
		}
		
		// grabo custumer
		$writerec = new SaveRows($this);
		if ( !$writerec->has_requireds()) return;

		$writerec->field_unique = array("mail","vat");
		if (!$this->check_other_uniques($writerec)) return;
		
		if ($this->action=='add_record') {
			
			// si no hi ha el select de prefered language en el formulari, poso l'idioma en que es mira
			if (!$writerec->get_value('prefered_language')) $writerec->set_field('prefered_language','override_save_value',"'" . LANGUAGE . "'");
		}		
				
	    $writerec->save();
		
		
		$custumer_id = $this->id = $writerec->id;
		
		$this->saved = $writerec->saved;
		if (!$writerec->saved) {
			if ( $writerec->message_add_exists ) {
				$GLOBALS['gl_message'] = $this->get_add_exists_message($writerec);
			}
			return;
		} // si no ha grabat (ej. add_exists) no faig res més
		
		
		// NEW CUSTUMER, son els guests secundaris
		// 
		// poso el remove key automaticament
		if ($this->action=='add_record')
			$this->newsletter_add_remove_key('custumer__custumer', $custumer_id);

		// envio mail nomes al registrar principal, excepte desde la fitxa immoble
		if (!$this->parent && $this->action == 'add_record' && $this->process!='new_custumer') 
			$this->send_mail_registered($custumer_id);
		
		// logejo nomes al registrar principal
		// poso sempre com a book_main els usuaris registrats
		if ($this->action == 'add_record' && $this->process!='new_custumer') {
			$this->set_login_session_vars(false, $custumer_id);			
			Db::execute("UPDATE custumer__custumer SET book_main=1 WHERE custumer_id=" . $custumer_id);
		}
		
		// a new_custumer grabo el ordre i assigno usuari al 
		if($this->process == 'new_custumer'){		
											
			$query = New Query('', 'booking__book_to_custumer','book_id,ordre','custumer_id');
			$query->show_querys = true;
			$query->save_related($book_id, $ordre, $custumer_id);
			
			Db::execute("UPDATE custumer__custumer SET book_group=" . $_SESSION['custumer']['custumer_id'] . " WHERE custumer_id=" . $custumer_id);
			
			
			
		}
		// per quan edito desde la fitxa de book, aixì puc tornar enrera on era abans d'editar
		$return_url = R::escape('return-url');
		
		if ($return_url){
			print_javascript('top.window.location.href = "'.$return_url.'"');	
			
			Debug::p_all();
			die();
		}
		
		if ($this->process=='login'){			
			print_javascript("top.window.location.href = '/?tool=custumer&tool_section=custumer&action=show_form_edit&menu_id=2&custumer_id=" . $custumer_id ."';");				
			Debug::p_all();
			die();
		}

	}

	// Nomes comprovo quan es un usuari secundari
	function check_other_uniques(&$writerec) {

		if ($this->process!='new_custumer') return true;

		$name = $writerec->get_value( 'name' );
		$surname1 = $writerec->get_value( 'surname1' );
		$surname2 = $writerec->get_value( 'surname2' );
		$birthdate = $writerec->get_value( 'birthdate' );
		$id = $writerec->id;
		$query = "
			SELECT count(*)
			FROM custumer__custumer
			WHERE name = '" . $name . "'
			AND surname1 = '" . $surname1 . "'
			AND surname2 = '" . $surname2 . "'
			AND book_group = '" . $_SESSION['custumer']['custumer_id'] . "'";

		// El missatge ja especifica que és una cosa o una altra, per això poso el mateix en els 2 casos
		if ( Db::get_first( $query ) ) {
			$writerec->message_add_exists = $this->caption['c_new_custumer_exists'];
		}
		$query = "
			SELECT count(*)
			FROM custumer__custumer
			WHERE birthdate = '" . unformat_date ($birthdate)  . "'
			AND book_group = '" . $_SESSION['custumer']['custumer_id'] . "'";

		if ( Db::get_first( $query ) ) {
			$writerec->message_add_exists = $this->caption['c_new_custumer_exists'];
		}

		if ($writerec->message_add_exists) {
			$GLOBALS['gl_saved'] = $writerec->saved = $this->saved = false;
			$GLOBALS['gl_message'] .= $this->get_add_exists_message($writerec);
			return false;
		}
		else {
			return true;
		}

	}
	function get_add_exists_message(&$writerec) {
		$file = PATH_TEMPLATES . 'custumer/add_exists_messages.tpl';

		if (!is_file($file))
			return '';

		$tpl = new phemplate(PATH_TEMPLATES);
		$tpl->set_vars($this->caption);
		$tpl->set_file('custumer/add_exists_messages.tpl');
		$tpl->set_var( 'show_custumers', false );
		$tpl->set_var( 'show_password', false );
		$tpl->set_var( 'message', $GLOBALS['gl_message'] );

		if ($this->action == 'add_record' && $this->process=='new_custumer') {
			$return_url = R::escape('return-url');
			$tpl->set_var( 'show_custumers', true );
			$tpl->set_var( 'return_url', $return_url );
			return $tpl->process();

		} elseif ($this->action == 'add_record' && $this->process!='new_custumer') {
			$tpl->set_var( 'show_password', true );
			$tpl->set_var( 'mail_input_value', htmlspecialchars($writerec->get_value('mail')) );
			return $tpl->process();
		}
		else return '';
	}
	function send_mail_registered($custumer_id){
		// TODO-i Enviar el email de usuari registrat
		// NO ESTÀ FET
		/*$this->never_mind_logged = true;
		$this->action = 'get_record';
		
		// Agafo formulari del custumer, però amb el form.tpl per defecte
		$show = new ShowForm($this);
		$show->id = $custumer_id;
		$show->set_field('password','form_public','no');
		$show->use_default_template = true;
		$custumer = $show->show_form();
		
		$page = $GLOBALS['gl_page'];
		$page->template = 'mail.tpl';
		$page->set_var('preview','');
		$page->content = $this->caption['c_body_1'] . ': <br>' . $custumer;
		
		$body = $page->get();
		
		// envio mail
		$mailer = get_mailer($this->config);
		if (is_file(CLIENT_PATH . 'images/logo_newsletter.jpg'))
			$mailer->AddEmbeddedImage(CLIENT_PATH . 'images/logo_newsletter.jpg', 'logo', 'logo.jpg');
		elseif(is_file(CLIENT_PATH . 'images/logo_newsletter.gif')) 
			$mailer->AddEmbeddedImage(CLIENT_PATH . 'images/logo_newsletter.gif', 'logo', 'logo.gif');
		
		$mailer->Body = $body;
		$mailer->Subject = $this->caption['c_subject'];
		$mailer->IsHTML(true);
		$mailer->AddAddress ($this->config['default_mail']);
		send_mail($mailer, false);
		 * 
		 */
	}

	

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	function show_login(){
		if ($this->is_logged()) {
			$this->show_menu_custumer();
			$this->do_action('list_book');
		}
		else{
			$content = $this->get_login();
			$GLOBALS['gl_page']->title = $this->caption['c_title_login'];
			$GLOBALS['gl_content'] = $content;
		}
	}
	function get_login(){
		
		$link_action = '/' . LANGUAGE . "/custumer/custumer/login/";		
		$link_send_password = $this->link . '&action=send_password';

		$this->set_var('login_js_string',"return validar(this,'login_mail','".addslashes($this->caption['c_mail'])."','email1','login_password','".addslashes($this->caption['c_password'])."','text1');");
		$this->set_vars(array(
			'login_link_action'=>$link_action,
			'login_link_send_password'=>$link_send_password
			));
		
		$this->action = 'get_form_new';
		$this->process = 'login';
		
		return $this->get_form();
	}
	
	function set_login_session_vars($rs, $custumer_id = false)
	{
		global $gl_message, $gl_page;
		
		if (!$rs){
			$query = "SELECT custumer_id, mail, password, name, surname1, surname2, book_group, book_main
						FROM custumer__custumer
						WHERE custumer_id = " . $custumer_id ;
			$rs = Db::get_row($query);			
		}
		
		// si no es el client principal, busco el client principal per guardar al session
		// normalment si es fa la reserva desde public, els secundaris no tenen acces
		// pero desde admin se'ls pot crear contraseña
		if ($rs && $rs['book_main'] !=1){
			
			$book_group = $rs['book_group'];
			if ($book_group != 0){
				$query = "SELECT custumer_id, mail, password, name, surname1, surname2, book_group, book_main
						FROM custumer__custumer
						WHERE book_main = 1
						AND book_group = " . $book_group ;
				$rs = Db::get_row($query);
				
				// si no hi ha rs, haig de marcar com a principal el de id book_group
				if (!$rs){
					$query = "SELECT custumer_id, mail, password, name, surname1, surname2, book_group, book_main
							FROM custumer__custumer
							WHERE custumer_id = " . $book_group;
					$rs = Db::get_row($query);

					if ($rs){
						$query = "UPDATE custumer__custumer
						SET book_group=" . $rs['custumer_id'] . ", book_main=1
						WHERE custumer_id = " . $rs['custumer_id'] ;
						Db::execute($query);
					}
					// si s'ha borrat el principal hi no hi ha principal pel que sigui, surto i no loguejo
					else{
						// si no rs mes avall mostra error
					}
				}
			}
			
			// si el client no es principal, pero tampoc pertany a cap grup, el poso com a principal			
			else{
				$query = "UPDATE custumer__custumer
						SET book_group=" . $rs['custumer_id'] . ", book_main=1
						WHERE custumer_id = " . $rs['custumer_id'] ;
				Db::execute($query);
			}
			
		}
		
		
		if (!$rs){
			// mostra missatge d'error			
			
			if ($this->is_ajax && $this->parent == 'BookingBook'){
				return USER_LOGIN_INVALID;
			}
			$gl_page->show_message(USER_LOGIN_INVALID);
		
			Debug::p_all();
			die();
		}
		
		$_SESSION['custumer']['custumer_id'] = $rs['custumer_id'];
		$_SESSION['custumer']['mail'] = $rs['mail'];
		$_SESSION['custumer']['name'] = $rs['name'];
		$_SESSION['custumer']['surname'] = $rs['surname1'] . ' ' . $rs['surname2'];
		
	}
	function login()
	{
		global $gl_message, $gl_page;
		
		
		if (isset($_POST['login_mail']) && $_POST['login_mail']!='' && $_POST['login_password']!='')
		{	// nomès pot haber un login igual
			$query = "SELECT custumer_id, mail, password, name, surname1, surname2, book_group, book_main
						FROM custumer__custumer
						WHERE mail = " . trim(Db::qstr($_POST['login_mail'])) . "
						AND  password = " . trim(Db::qstr($_POST['login_password'])) . "
						AND activated = 1
						AND bin=0";
			$rs = Db::get_row($query);
			Debug::add('Query client',$query);
			Debug::add('Results client',$rs);
			// més segur amb 2 comprovacions
			if (($rs['mail'] == $_POST['login_mail'])
				&& ($rs['password'] == $_POST['login_password']))
			{
				$this->set_login_session_vars($rs);
				
					if ($this->is_ajax && $this->parent == 'BookingBook'){
						return true;
					}
					// PER FER
					elseif (isset($_POST['login_redirect'])) {					
						print_javascript('top.window.location.href = "'.addslashes($_POST['login_redirect']).'";');
					}
					else{
						print_javascript('top.window.location.href = top.window.location.href;');
					}
					

			}
			// mostra missatge d'error
			else
			{
				if ($this->is_ajax && $this->parent == 'BookingBook'){
					return USER_LOGIN_INVALID;
				}
				$gl_page->show_message(USER_LOGIN_INVALID);
			}
			Debug::p_all();
			die();
		}
		// missatge error
		else
		{
			if ($this->is_ajax && $this->parent == 'BookingBook'){
				return USER_LOGIN_INVALID;
			}
			$gl_page->show_message(USER_LOGIN_INVALID);
		}
	}
	function logout()
	{
		global $gl_message;

		unset($_SESSION['custumer']);
				
		Debug::p_all();
		die();

	}
	function send_password(){
		$new_password = get_password();
		if ($this->is_test){
			$mail = $_GET['mail'];
		}
		else {
			$mail = R::escape('mail',false,'_POST');
			$mailer = $GLOBALS['gl_current_mailer'] = get_mailer($this->config);
			if (!$mail) {
				Debug::p_all();
				die();
			}
			$sql = "UPDATE custumer__custumer SET password = '".$new_password."' WHERE mail = '" . $mail . "'";
			Debug::add('Consulta nou password', $sql);
			Db::Execute($sql);
		}
		// si o hi ha l'adreça mostro missatge d'error
		if (!$this->is_test && mysqli_affected_rows(Db::cn())==0) {
			print_javascript('
					if (typeof top.hidePopWin == "function") top.hidePopWin(false);
					top.alert("' . stripslashes($this->caption['c_password_mail_error']) . '");');
		}
		// si l'adreça es bona envio mail amb la nova contrasenya
		else{
			if (!$this->is_test)
				print_javascript('
					if (typeof top.hidePopWin == "function") top.hidePopWin(false);
					top.alert("' . stripslashes($this->caption['c_password_sent']) . '");
					top.custumer.open_login();');
			
			// preparo mail
			$page = $GLOBALS['gl_page'];
			$page->template = 'mail.tpl';
			$page->set_var('preview','');
			$page->set_var('remove_text','');
			$page->set_vars(array(
				'book'=>'', 
				'custumer'=>'',
				'extras'=>'',
				'url' => HOST_URL,
				'short_host_url' => substr(HOST_URL,7,-1)
				));
			
			$this->set_file('/custumer/mail_password.tpl');
			$rs = $this->_get_custumer_name($mail);
			$this->set_vars($rs);
			$this->set_var('password', $new_password);
			$this->set_var('treatment', $this->caption['c_treatment_'.$rs['treatment']]);
			$this->set_vars($this->caption);

			$page->content = $this->process();
			$body = $page->get();
			$subject = $this->caption['c_password_mail_subject'];
			
			if ($this->is_test)
				echo $body;
			
			else{

				$instyler = new \Pelago\Emogrifier($body);
				$body = $instyler->emogrify();

				// envio mail
				if (is_file(CLIENT_PATH . 'images/logo_newsletter.jpg'))
					$mailer->AddEmbeddedImage(CLIENT_PATH . 'images/logo_newsletter.jpg', 'logo', 'logo.jpg');
				elseif(is_file(CLIENT_PATH . 'images/logo_newsletter.gif')) 
					$mailer->AddEmbeddedImage(CLIENT_PATH . 'images/logo_newsletter.gif', 'logo', 'logo.gif');
				
				$mailer->Body = $body;
				$mailer->Subject = $subject;
				$mailer->IsHTML(true);
				$mailer->AddAddress ($mail);
				Debug::add('mailer', $mailer);
				send_mail($mailer, false);
			}
		}
		Debug::p_all();
		die();
	}
	function newsletter_add_remove_key($table, $id) {	

			$table_id = strpos($table, "custumer")!==false?'custumer_id':'custumer_id';

			$q2 = "SELECT count(*) FROM ". $table . " WHERE BINARY remove_key = ";
			$q3 = "UPDATE ". $table . " SET remove_key='%s' WHERE  ". $table_id . " = '%s'";

			$updated = false; 
			while (!$updated){
				$pass = get_password(50);
				$q_exists = $q2 . "'".$pass."'";	

				// si troba regenero pass
				if(Db::get_first($q_exists)){
					$pass = get_password(50);							
				}
				// poso el valor al registre
				else{				
					$q_update = sprintf($q3, $pass, $id);

					Db::execute($q_update);
					break;
				}
			}
	}

	function _get_custumer_name($mail){
		$sql = "SELECT CONCAT(name,' ', surname1,' ', surname2) as name, treatment FROM custumer__custumer WHERE mail = '" . $mail . "'";
		return Db::get_row($sql);
	}
	
	
	/*
	 * 
	 * 
	 *  MENUS DE CUSTUMER
	 * 
	 * 
	 * 
	 */
	
	function show_menu_custumer(){
		return;
		$this->menu_id = isset($_GET['menu_id'])?$_GET['menu_id']:0;
		$content = $this->get_menu_custumer();
		$GLOBALS['gl_page']->title = $this->menus[$this->menu_id]['item'];
		$GLOBALS['gl_content'] = $content;

	}
	function get_menu_custumer(){
		
		$this->menu_id = isset($_GET['menu_id'])?$_GET['menu_id']:1; // haig de repetir, no sempre vinc de show
		$tpl = new phemplate(PATH_TEMPLATES);
		$tpl->set_file('custumer/custumer_menu.tpl');
		$tpl->set_vars($this->caption);
		$tpl->set_var('is_index', $this->menu_id==0?1:0);
		$tpl->set_var('link_home', $this->link. '&action=show_menu_custumer');
		
		$link1 = get_link( 
			array(
				'tool'=>'custumer',
				'tool_section'=>'custumer',
				'detect_page'=>true,
				));
		
		$link2 = get_link( 
			array(
				'tool'=>'custumer',
				'tool_section'=>'custumer',
				'action'=>'show-form-edit',
				'friendly_params'=>array('custumer_id'=>$_SESSION['custumer']['custumer_id'],'menu_id'=>2),
				'detect_page'=>true,
				));
		
		$this->menus = array(
			0=>array(
				'id'=>'2',
				'item'=>$this->caption['c_custumer_menu_books'],
				//'link'=>$this->link . "&action=list_book&menu_id=1",
				'link'=>$link1,
				'conta'=>'1',
				'class'=>'first',
				'selected'=>$this->menu_id==1?1:0
			),
			1=>array(
				'id'=>'1',
				'item'=>$this->caption['c_custumer_menu_personal_data'],
				'link'=>$link2,
				'conta'=>'0',
				'class'=>'',
				'selected'=>$this->menu_id==2?1:0,
			),
			2=>array(
				'id'=>'3',
				'item'=>$this->caption['c_custumer_menu_logout'],
				'link'=>"javascript:custumer.logout()",
				'conta'=>'2',
				'class'=>'last',
				'selected'=>$this->menu_id==3?1:0
			)
		);

		$tpl->set_loop('menus',$this->menus);
		
		return $tpl->process();
	}
	

	/**
	 * CustumerCustumer::is_logged()
	 * Saber si està loguejat
	 * @return
	 */
	function is_logged(){
		// miro si hi ha les 2 sessions per més seguretat de que esta logged
		if ($this->never_mind_logged) return true; // per poder cridar desde dins la clase alguna acció encara que no estigui logejat, com ara mail a admin  quan s'haregistrat nou usuari
		
		
		return (isset($_SESSION['custumer']['custumer_id']) && isset($_SESSION['custumer']['mail']));
	}
	/**
	 * CustumerCustumer::do_action()
	 * Sobreescric la funció heredada
	 * Qualsevol acció d'aquesta clase l'haig de cridar per aquí,
	 * ja que és on controlo que l'usuari està loguejat
	 * @param string $action
	 * @return
	 */
	function do_action($action=''){


		$ac = $action?$action:$this->action;
		// es important no barrejar action amb this->action, per això faig servir una tercera variable $ac

		// aquests action no necessiten estar loguejats
		if (
				   $ac=='show_form_new'
				|| $ac=='get_form_new'
				|| $ac=='add_record'
				|| $ac=='login'
				|| $ac=='get_login'
				|| $ac=='show_login'
				|| $ac=='show_message'
				|| $ac=='send_password'
			) {
			return parent::do_action($action);
		}
		else
		{
			// miro si hi ha les 2 sessions per més seguretat de que esta logged
			if ($this->is_logged()) {
				
				
				if ($ac=='save_record'
				|| $ac=='bin_record'
				|| $ac=='get_record'
				|| $ac=='show_record'
				|| $ac=='delete_record'){
				
					// m'asseguro que el custumer_id pertanyi al grup que està logejat
					$id = $_SESSION['custumer']['custumer_id'];						
					$custumer_id = R::id('custumer_id');
					if (!$custumer_id) $custumer_id = $_POST['custumer_id'];
					if (is_array($custumer_id)) $custumer_id = current($custumer_id);
				
					$is_in_group = Db::get_first("
						SELECT count(*) 
						FROM custumer__custumer 
						WHERE custumer_id = ".$id."  
						AND book_group = ( 
							SELECT book_group 
							FROM custumer__custumer 
							WHERE custumer_id = ".$custumer_id.")");				


					if(!$is_in_group){
						Main::error_404('Algu intenta entrar amb un form propi o passa algo raro'); // algu intenta entrar amb un form propi o passa algo raro
					}
				}
				
				return parent::do_action($action);
			}
			else
			{
				parent::do_action('show_login');
			}
		}
	}
}
?>