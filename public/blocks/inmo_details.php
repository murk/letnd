<?php

// fitre amb les opcions de cercar disponibilitat

$fields = isset($vars['fields'])?$vars['fields']:'swimmingpool';
$fields = explode(',', $fields);

if (isset($GLOBALS['gl_module']) && $GLOBALS['gl_module']->tool == 'inmo') {
	$module = $GLOBALS['gl_module'];
}
else{	
	$module = Module::load('inmo','property');
}

set_custom_config($module->config, $module->fields, $module->language_fields, $module->caption);
$caption = &$module->caption;


foreach ($fields as $field){
	
	$checked = R::get($field)?' checked':'';
	$id = 'block-'.$this->block_id.'-'.$field;

	$loop []= array(
		'input' => '<input value="1" id="'.$id.'" name="'.$field.'" type="checkbox"' . $checked . '>',
		'field' => $field,
		'id' => $id,
		'item' => $caption['c_'.$field]
	);
}

?>