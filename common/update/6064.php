<?

/* 12-12-15

	Afegeixo others a product
 */

if ( is_table( 'product__product' ) && ! is_field( 'product__product', 'others9' ) ) {

	$update_sql[] = "
					ALTER TABLE `product__product`
						ADD COLUMN `others9` TEXT NOT NULL AFTER `others8`,
						ADD COLUMN `others10` TEXT NOT NULL AFTER `others9`;";
	$update_sql[] = "
					ALTER TABLE `product__product_language`
						ADD COLUMN `others9` VARCHAR(2000) NOT NULL DEFAULT '' AFTER `others8`,
						ADD COLUMN `others10` VARCHAR(2000) NOT NULL DEFAULT '' AFTER `others9`;
					";


	$update_sql[]="
			INSERT INTO `product__configadmin` (`name`)
			VALUES ('others9');";
	$update_sql[]="
			INSERT INTO `product__configadmin` (`name`,value)
			VALUES ('others9_type','text_no_lang');";

	$update_sql[]="
			INSERT INTO `product__configadmin` (`name`)
			VALUES ('others10');";
	$update_sql[]="
			INSERT INTO `product__configadmin` (`name`,value)
			VALUES ('others10_type','text_no_lang');";


	global $gl_languages;

	foreach($gl_languages['names'] as $lang=>$val){
		$update_sql[]="
			INSERT INTO `product__configadmin_language` (`name`, `language`)
			VALUES ('others9', '".$lang."');";
		$update_sql[]="
			INSERT INTO `product__configadmin_language` (`name`, `language`)
			VALUES ('others10', '".$lang."');";
	}


} else {
	if ( is_table( 'product__product' ) ) {
		trigger_error( 'Ja existeixen el camp others9' );
	}
}

?>