<?php
/* 22-6-17
Files, actualització per poder descarregar o  no, resetejar nom al upload, i el nom vell d'arxiu

*/

$tables = Db::get_rows_array( 'SHOW TABLES' );

// Bucle per totes les taules amb arxius
foreach ( $tables as $rs ) {
	$table = $rs[0];
	if (
		substr( $table, - 6 ) === '_image'
		) {

		$update_sql[] = "ALTER TABLE $table
							ADD COLUMN `name_original` VARCHAR(255) NOT NULL AFTER `name`;";
		$update_sql[] = "ALTER TABLE $table
							CHANGE COLUMN `name` `name` VARCHAR(255);";
		$update_sql[] = "UPDATE $table
							set `name_original` = `name`;";

	}
}




?>