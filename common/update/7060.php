<?php
/* 14-9-18
Flag per forçar regenerar css i jscripts
*/

if ( is_table( 'all__configadmin' ) ) {
	$update_sql[] = "
		DELETE FROM all__configadmin WHERE name = 'force_css_jscript_generation';";
}


if ( is_table( 'all__configpublic' ) && ! Db::get_first( "SELECT * FROM all__configpublic WHERE name='force_css_jscript_generation'" ) ) {
	$update_sql[] = "
		INSERT INTO all__configpublic (`name`, `value`) VALUES ('force_css_jscript_generation', '1');";

}
else {
	if ( is_table( 'all__configpublic' ) ) {
		trigger_error( 'Ja existeixen el camp force_css_jscript_generation' );
	}
}