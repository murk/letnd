<?php
// no es poden posar ; entre mig de la consulta ja que parteix les consultes per el ; a no ser que fem servir $update_sql en array()
if (is_table("product__product")){
		
	$update_sql[]="ALTER TABLE `product__product`
	ADD COLUMN `last_stock_units_public` INT(11) NOT NULL AFTER `stock`,
	ADD COLUMN `last_stock_units_admin` INT(11) NOT NULL AFTER `last_stock_units_public`;";	
	
	$update_sql[]="ALTER TABLE `product__product_to_variation`
	ADD COLUMN `variation_stock` INT(11) NOT NULL DEFAULT '0' AFTER `variation_default`,
	ADD COLUMN `variation_last_stock_units_public` INT(11) NOT NULL DEFAULT '0' AFTER `variation_stock`,
	ADD COLUMN `variation_last_stock_units_admin` INT(11) NOT NULL DEFAULT '0' AFTER `variation_last_stock_units_public`;";
	
	$update_sql[]="CREATE TABLE `product__brand_language` (
	`brand_id` INT(11) NOT NULL,
	`language` CHAR(3) NOT NULL,
	`brand_description` TEXT NOT NULL
	)
	COLLATE='utf8_spanish2_ci'
	ENGINE=MyISAM";
	
	$update_sql[]="CREATE TABLE `product__brand_image` (
	`image_id` INT(11) NOT NULL AUTO_INCREMENT,
	`brand_id` INT(11) NOT NULL DEFAULT '0',
	`name` VARCHAR(100) NOT NULL DEFAULT '',
	`size` INT(11) NOT NULL DEFAULT '0',
	`ordre` INT(11) NOT NULL DEFAULT '0',
	`main_image` INT(1) NOT NULL DEFAULT '0',
	PRIMARY KEY (`image_id`)
	)
	COLLATE='utf8_spanish2_ci'
	ENGINE=MyISAM;";
	
	$update_sql[]="
	INSERT INTO `all__menu` (`menu_id`, `menu_group`, `tool`, `tool_section`, `action`, `parent_id`, `toolmode_id`, `variable`, `ordre`) VALUES (20024, 'product', 'product', 'brand', 'show_form_new', 20020, 1, 'PRODUCT_MENU_BRAND_NEW', 1);";
	$update_sql[]="
	INSERT INTO `all__menu` (`menu_id`) VALUES (20025);";
	$update_sql[]="
	UPDATE `all__menu` SET `menu_group`='product', `tool`='product', `tool_section`='brand', `action`='list_records_images', `parent_id`=20020, `toolmode_id`=1, `variable`='PRODUCT_MENU_BRAND_NEW', `ordre`=2 WHERE  `menu_id`=20025 LIMIT 1;";
	$update_sql[]="	
	INSERT INTO `all__menu` (`menu_id`, `menu_group`, `tool`, `tool_section`, `action`, `parent_id`, `variable`, `ordre`) VALUES (20026, 'product', 'product', 'brand', 'show_form_new', 20022, 'PRODUCT_MENU_BRAND_NEW', 1);";
	$update_sql[]="
	INSERT INTO `all__menu` (`menu_id`, `menu_group`, `tool`, `tool_section`, `action`, `parent_id`, `variable`, `ordre`) VALUES (20027, 'product', 'product', 'brand', 'list_records_images', 20022, 'PRODUCT_MENU_BRAND_NEW', 2);";
	$update_sql[]="
	UPDATE `all__menu` SET `parent_id`=20021 WHERE  `menu_id`=20024 LIMIT 1;";
	$update_sql[]="
	UPDATE `all__menu` SET `parent_id`=20021 WHERE  `menu_id`=20025 LIMIT 1;";
	$update_sql[]="
	ALTER TABLE `product__product`
	ADD COLUMN `prominent` TINYINT(1) NOT NULL DEFAULT '0' AFTER `destacat`;";
}	
?>