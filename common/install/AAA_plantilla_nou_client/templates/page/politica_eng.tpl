<article>
  <h2>Privacy and data protection policy</h2>
  <p>Privacy and data protection policy of <?=COMPANY_NAME?> is aimed at offering the best service and security to our customers. The information provided will never be used for purposes other than the stated below.</p>
  <p><?=COMPANY_NAME?> respects the text of the Organic Law 5/92 of 29 October regarding the automated processing of personal data. Therefore, the customer or user of our Website can exercise his/her rights to access, amend, oppose and cancel the information, and can contact <?=COMPANY_NAME?> through the address provided on this page.</p>
  <p><?=COMPANY_NAME?> reserves the right to use the information provided by the customer or user to send marketing materials, unless otherwise stated by the customer.</p>
  <p>The data is automatically stored on a secure file with limited access. The information will not be assigned to third parties in any case. This database is only created for information purposes related to sales, promotions and statistics of <?=COMPANY_NAME?>.</p>
  <?if(COMPANY_DETAILS):?>
  <p><?=COMPANY_DETAILS?></p>
  <?endif //COMPANY_DETAILS?>
</article>