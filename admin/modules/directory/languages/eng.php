<?php
// menus eina
if (!defined('DIRECTORY_MENU_DOCUMENT')){
	define('DIRECTORY_MENU_DOCUMENT', 'Files');
	define('DIRECTORY_MENU_DOCUMENT_NEW', 'Enter new file');
	define('DIRECTORY_MENU_DOCUMENT_LIST', 'List and edit files');
	define('DIRECTORY_MENU_DOCUMENT_BIN', 'Recycle bin');
	define('DIRECTORY_MENU_CATEGORY', 'Folders');
	define('DIRECTORY_MENU_CATEGORY_NEW', 'Enter new folder');
	define('DIRECTORY_MENU_CATEGORY_LIST', 'List folders');
	define('DIRECTORY_MENU_DOCUMENTUSER', 'Files');
	define('DIRECTORY_MENU_DOCUMENTUSER_LIST', 'Categories');
	define('DIRECTORY_COMMENT_DOCUMENTUSER', 'Only view files with no edit permissions');
}

$gl_messages_new['no_category']='In order to insert files it must be at least one folder';

// captions seccio
$gl_caption_document['c_prepare'] = 'Preparing';
$gl_caption_document['c_review'] = 'To review';
$gl_caption_document['c_public'] = 'Public';
$gl_caption_document['c_archived'] = 'Archived';
$gl_caption_document['c_document'] = 'File';
$gl_caption_document['c_description'] = 'Description';
$gl_caption_document['c_entered'] = 'Creation date';
$gl_caption_document['c_modified'] = 'Modification date';
$gl_caption_document['c_status'] = 'State';
$gl_caption_document['c_category_id'] = 'Folder';
$gl_caption_document['c_filter_category_id'] = 'All folders';
$gl_caption_document['c_filter_status'] = 'All states';
$gl_caption_document['c_file']='Files';
$gl_caption_document['c_videoframe']='Video (youtube, vimeo...)';
$gl_caption_document['c_download_all']='Download folder';
$gl_caption_document['c_download_selected']='Download selected';
$gl_caption_document['c_download']='Download';
$gl_caption_document['c_selected_zip_name']='selected_files';
$gl_caption_document['c_zip_name']='files';

$gl_caption_category['c_category'] = 'Folder';
$gl_caption_category['c_category_description'] = 'Description';
$gl_caption_document['c_list_documents_button']='See files';
$gl_caption['c_document_icon']='';

$gl_caption['c_concession']='Permissions';
$gl_caption['c_concession_everybody']='All';
$gl_caption['c_concession_change']='Change permissions';
$gl_caption['c_selected']='Permissions established';
$gl_caption_concession['c_group_id'] = 'Group';
?>
