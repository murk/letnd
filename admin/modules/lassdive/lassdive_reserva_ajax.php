<?

/**
 * LassdiveReservaAjax
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2011
 * @version $Id$
 * @access public
 */
class LassdiveReservaAjax {


	static public function get_checkboxes_results_products( $is_reserva = false, $is_product_search = false ) {

		$limit = $is_product_search ? 40 : 100;

		$q            = R::escape( 'query' );
		$q            = strtolower( $q );
		$id           = R::id( 'id' );
		$selected_ids = R::get( 'selected_ids' );

		if ( $selected_ids ) {
			R::escape_array( $selected_ids );
			$selected_ids = implode( ',', $selected_ids );
		}

		$ret                = [];
		$ret['suggestions'] = [];

		$r = &$ret['suggestions'];
		$r_familys = [];


		$order    = 'status asc, product_title ASC,ordre ASC';
		$language = LANGUAGE;

		$exclude_selected_ids = $selected_ids ? " AND ( product__product.product_id NOT IN ($selected_ids))" : '';
		$exclude_saved_ids    = $id ? " AND ( product__product.product_id NOT IN (SELECT product_id FROM lassdive__centreproduct WHERE product_id = $id))" : '';

		$is_reserva_query = $is_reserva ? " AND product__product.product_id IN ( SELECT DISTINCT product_id FROM product__orderitem )" : '';

		$family_query = "(
			SELECT product__family_language.family FROM product__family_language 
			WHERE product__family_language.family_id = product__product.family_id 
			AND language =  '$language') AS family";

		$subfamily_query = "(
			SELECT product__family_language.family FROM product__family_language 
			WHERE product__family_language.family_id = product__product.subfamily_id 
			AND language =  '$language') AS subfamily";

		$family_subquery = '
			SELECT product__family_language.family_id FROM product__family_language 
			WHERE family';

		$variations_subquery_tpl = "
			SELECT product__product_to_variation.product_id AS product_id FROM product__product_to_variation WHERE variation_id IN (
				SELECT product__variation_language.variation_id FROM product__variation_language 
				WHERE variation LIKE '%s' %s	
			)";


		$common_query_1 = "
					SELECT product__product.product_id as product_id, $family_query, $subfamily_query, product_title, product_subtitle, ref, family_id, subfamily_id, subsubfamily_id
					FROM product__product, product__product_language";
		$common_query_2 = "product__product.product_id = product__product_language.product_id
			            AND 
			            language = '$language'
			            AND 
			            bin = 0";
		$common_query_3 =
			"
		            $exclude_selected_ids
		            $exclude_saved_ids
		            $is_reserva_query
					ORDER BY $order";

		if ( $is_reserva ) {
			$r_familys = self::get_checkboxes_results_familys( $q, $limit );
		}

		// 1 - Que comenci
		$variations_subquery = sprintf($variations_subquery_tpl, "$q%", '');
		$query = "
					$common_query_1
					WHERE
				        (
				            $common_query_2
			            ) 
				        AND
				        (
				            product_title LIKE '$q%'
							OR product_subtitle LIKE '$q%'
							OR ref LIKE '$q%'
							OR family_id IN ($family_subquery LIKE '$q%')
							OR subfamily_id IN ($family_subquery LIKE '$q%')
							OR product__product.product_id IN ($variations_subquery)
						)
					$common_query_3
					LIMIT $limit";

		$results = Db::get_rows( $query );

		$rest = $limit - count( $results );

		// 2 - Qualsevol posició
		if ( $rest > 0 ) {

			$variations_subquery = sprintf($variations_subquery_tpl, "%$q%", "AND variation NOT LIKE '$q%'");
			$query = "
						$common_query_1
						WHERE
					        (
					            $common_query_2
				            )
					        AND	
					        (					 
								( 
									product_title LIKE '%$q%'
				                    AND product_title NOT LIKE '$q%' 
				                )
								OR 
								( 
									family_id IN (
									$family_subquery LIKE '%$q%'
									AND family NOT LIKE '$q%' 
									)
				                )
								OR 
								( 
									subfamily_id IN (
									$family_subquery LIKE '%$q%'
									AND family NOT LIKE '$q%' 
									)
				                )
								OR 
								( 
									product__product.product_id IN (
									$variations_subquery
									)
				                )
								OR
								 ( 
								    ref LIKE '%$q%'
				                    AND ref NOT LIKE '$q%' 
				                )
				            )
						$common_query_3
						LIMIT $rest";

			$results2 = Db::get_rows( $query );

			$results = array_merge( $results, $results2 );

		}

		$rest = $limit - count( $results );

		$words = array();
		// 3 - Parteixo paraules
		if ( $rest > 0 ) {

			$words = explode( ' ', $q );
			if ( count( $words ) > 1 ) {

				$words_query_title    = '';
				$words_query_subtitle = '';

				foreach ( $words as $word ) {
					$words_query_title    .= "AND product_title LIKE '%$word%'";
					$words_query_subtitle .= "AND product_subtitle LIKE '%$word%'";
				}

				$query = "
						$common_query_1
						WHERE
					        (
					            $common_query_2
				            )
					        AND	
                            (
								( 
									product_title NOT LIKE '%$q%'
				                    AND product_title NOT LIKE '$q%' 
				                    $words_query_title
				                )
								OR 
								( 
									product_subtitle NOT LIKE '%$q%'
				                    AND product_subtitle NOT LIKE '$q%' 
				                    $words_query_subtitle
				                )
			                )
						$common_query_3
						LIMIT $rest";

				$results2 = Db::get_rows( $query );

				$results = array_merge( $results, $results2 );
			}

		}

		// Presento resultats
		foreach ( $results as $rs ) {


			$ref                = self::highlight( $q, $rs['ref'] );
			$product_title_text = $rs['product_title'];
			$product_title      = self::highlight( $q, $rs['product_title'] );
			$product_subtitle   = self::highlight( $q, $rs['product_subtitle'] );
			$family             = self::highlight( $q, $rs['family'] );
			$subfamily          = self::highlight( $q, $rs['subfamily'] );

			if ( count( $words ) > 1 ) {
				foreach ( $words as $word ) {
					$ref              = self::highlight( $word, $ref );
					$product_title    = self::highlight( $word, $product_title );
					$product_subtitle = self::highlight( $word, $product_subtitle );
					$family           = self::highlight( $word, $rs['family'] );
					$subfamily        = self::highlight( $word, $rs['subfamily'] );
				}
			}


			if ( $is_product_search ) {

				$last_family_id = self::get_last_family_id( false, $rs );

				if ( $subfamily ) $subfamily = "> $subfamily";

				$custom = <<< EOF
					<div class="ref">
						 $ref 
					</div>
					<div class="product-title">
						 $product_title 
						<div class="product-subtitle"> $product_subtitle </div>
					</div>
					<div class="product-family">
						 $family $subfamily
					</div>
					<div><a class="more"></a></div>
EOF;
			}
			else {
				$last_family_id = 0;
				$familys = '';
				if ( $family ) {
					$familys = $family;
				}
				if ( $subfamily ) {
					$familys .= " > $subfamily";
				}
				if ( $familys ) {
					$familys = "<h5>$familys</h5>";
				}
				$custom         = "<div class='custom'><i>Ref. $ref</i><h6>$product_title</h6>$familys<p>$product_subtitle</p></div>";
			}


			$r[] = array(
				'value'          => $product_title_text,
				'custom'         => $custom,
				'last_family_id' => $last_family_id,
				'data'           => $rs['product_id']
			);
		}

		$ret['suggestions'] = array_merge($r_familys, $ret['suggestions']);


		$rest               = $limit - count( $results );
		$ret['is_complete'] = false; // Aquest mai es complert, ja que el jscript no pot buscar paraules separades, i llavors perdo la funcionalitat // $rest>0;
		$ret['has_custom']  = true;

		if ( ! DEBUG ) header( 'Content-Type: application/json' );

		Debug::p_all();
		die ( json_encode( $ret ) );


	}

	static function get_checkboxes_results_familys( $q, &$limit ) {

		$limit = 300; // Així puc mostrar totes les famílies sino hi ha $q
		$common_query_1 = "
					SELECT family_id, family, parent_id, language,
						( 
					        SELECT family 
					        FROM product__family_language as t
					        WHERE product__family.parent_id = t.family_id
					        AND product__family_language.language = t.language    
					    ) as parent
					FROM product__family_language
					INNER JOIN product__family USING (family_id)";
		$common_query_2 = "
					ORDER BY parent ASC, family ASC";

		$language = LANGUAGE;
		if ( ! $q ) $common_query_2 = "AND family <> '' AND language = '$language' $common_query_2";

		// 1 - Designació, que comenci
		$query = "$common_query_1
					WHERE family LIKE '$q%'
					$common_query_2
					LIMIT $limit";

		$results = Db::get_rows( $query );

		$rest = $limit - count( $results );
		// 2 - Designació, qualsevol posició
		if ( $rest > 0 ) {

			$query = "$common_query_1
						WHERE family LIKE '%$q%'
			            AND family NOT LIKE '$q%'
			            $common_query_2
						LIMIT $rest";

			$results2 = Db::get_rows( $query );

			$results = array_merge( $results, $results2 );

		}


		$r = [];

		$words = array();

		$rest = $limit - count( $results );
		// 3 - Parteixo paraules
		if ( $rest > 0 ) {

			$words = explode( ' ', $q );
			if ( count( $words ) > 1 ) {

				$words_query    = '';

				foreach ( $words as $word ) {
					$words_query    .= "AND family LIKE '%$word%'";
				}

				$query = "
						$common_query_1
						WHERE	
							family NOT LIKE '%$q%'
		                    AND family NOT LIKE '$q%' 
		                    $words_query
			            $common_query_2
						LIMIT $rest";

				$results2 = Db::get_rows( $query );

				$results = array_merge( $results, $results2 );
			}

		}

		// Presento resultats
		foreach ( $results as $rs ) {
			$family_id = $rs['family_id'];
			$family    = $rs['family'];
			$parent_id = $rs['parent_id'];
			$subfamily = '';

			if ( $parent_id ) {
				$language  = $rs['language'];
				$subfamily = " > $family";
				$query     = "
					SELECT family 
					FROM product__family_language
					WHERE family_id = $parent_id
					AND language = '$language'";
				$family    = Db::get_first( $query );
			}

			$custom_family = "<div class='custom'><h6>$family$subfamily</h6></div>";
			$custom_family = self::highlight( $q, $custom_family );

			if ( count( $words ) > 1 ) {
				foreach ( $words as $word ) {
					$custom_family = self::highlight( $word, $custom_family );
				}
			}

			$r[] = array(
				'value'          => "$family$subfamily",
				'custom'         => $custom_family,
				'last_family_id' => 0,
				'data'           => "family-$family_id"
			);
		}

		$limit = $rest;

		if (count( $results ) > 100) $limit = 0;


		return $r;


	}

	static function get_checkboxes_results_users( $condition, $user_type = 'user' ) {

		$limit = 100;

		$q = R::escape( 'query' );
		$q = strtolower( $q );

		$id           = R::id( 'id' );
		$selected_ids = R::get( 'selected_ids' );

		if ( $selected_ids ) {
			R::escape_array( $selected_ids );
			$selected_ids = implode( ',', $selected_ids );
		}

		$exclude_selected_ids = $selected_ids ? " AND ( user__user.user_id NOT IN ($selected_ids))" : '';

		$exclude_saved_ids = $id ? " AND ( user__user.user_id NOT IN (SELECT user_id FROM lassdive__centre_to_$user_type WHERE centre_id = $id))" : '';

		$common_query_1 = "
					SELECT user_id AS id, CONCAT (name, ' ', surname) AS item
					FROM user__user";
		$common_query_2 =
			"
		            $exclude_selected_ids
		            $exclude_saved_ids
					AND $condition
					ORDER BY name, surname";

		// 1 - Designació, que comenci
		$query = "$common_query_1
					WHERE CONCAT (name, ' ', surname) LIKE '$q%'
					$common_query_2
					LIMIT $limit";

		$results = Db::get_rows( $query );

		$rest = $limit - count( $results );

		// 2 - Designació, qualsevol posició
		if ( $rest > 0 ) {

			$query = "$common_query_1
						WHERE CONCAT (name, ' ', surname) LIKE '%$q%'
			            AND CONCAT (name, ' ', surname) NOT LIKE '$q%'
			            $common_query_2
						LIMIT $rest";

			$results2 = Db::get_rows( $query );

			$results = array_merge( $results, $results2 );

		}

		$ret                = array();
		$ret['suggestions'] = array();

		$r = &$ret['suggestions'];

		// Presento resultats
		foreach ( $results as $rs ) {
			$r[] = array(
				'value' => $rs['item'],
				'data'  => $rs['id']
			);
		}

		$rest               = $limit - count( $results );
		$ret['is_complete'] = false;
		// $ret['has_custom'] = true;

		Debug::p_all();
		die ( json_encode( $ret ) );
	}

	static function get_checkboxes_results_centres() {

		$limit = 100;

		$q = R::escape( 'query' );
		$q = strtolower( $q );


		$common_query_1 = "
					SELECT centre_id AS id, centre AS item
					FROM lassdive__centre";
		$common_query_2 =
			"
					ORDER BY centre ASC";

		// 1 - Designació, que comenci
		$query = "$common_query_1
					WHERE centre LIKE '$q%'
					$common_query_2
					LIMIT $limit";

		$results = Db::get_rows( $query );

		$rest = $limit - count( $results );

		// 2 - Designació, qualsevol posició
		if ( $rest > 0 ) {

			$query = "$common_query_1
						WHERE centre LIKE '%$q%'
			            AND centre NOT LIKE '$q%'
			            $common_query_2
						LIMIT $rest";

			$results2 = Db::get_rows( $query );

			$results = array_merge( $results, $results2 );

		}

		$ret                = array();
		$ret['suggestions'] = array();

		$r = &$ret['suggestions'];

		// Presento resultats
		foreach ( $results as $rs ) {
			$r[] = array(
				'value' => $rs['item'],
				'data'  => $rs['id']
			);
		}

		$rest               = $limit - count( $results );
		$ret['is_complete'] = $rest == 0;
		// $ret['has_custom'] = true;

		Debug::p_all();
		die ( json_encode( $ret ) );
	}


	static function get_last_family_id( $last_family_id = false, &$rs = [] ) {

		$language = LANGUAGE;

		if ( $last_family_id ) {

		}
		else if ( $rs['subsubfamily_id'] )
			$last_family_id = $rs['subsubfamily_id'];
		else if ( $rs['subfamily_id'] )
			$last_family_id = $rs['subfamily_id'];
		else
			$last_family_id = $rs['family_id'];


		// TODO-i Posar be aquest enllaç
		$last_family_text_id = Db::get_first( "
			SELECT activitat_file_name 
			FROM lassdive__activitat_language 
			WHERE language = '$language'
			AND activitat_id = {$last_family_id}" );

		return $last_family_text_id ? $last_family_text_id : $last_family_id;
	}


	static function remove_cart_item() {
		$reservaitem_id = R::id( 'reservaitem_id' );
		$product_id     = R::id( 'product_id' );

		$cart       = &$_SESSION['product']['cart'];
		$extra_vars = &$_SESSION['product']['cart_vars'][ $product_id ]['extra_vars'];

		foreach ( $extra_vars as $key => $vars ) {
			if ( $reservaitem_id == $vars['reservaitem_id'] ) {

				$variation_ids    = $vars['variation_ids'];
				$product_date = $vars['product_date'];

				$cart_product_key = "${product_id}_${variation_ids}";

				if ($product_date) $cart_product_key .= "|${product_date}";

				$cart_product = &$cart[$cart_product_key];
				$quantity     = $cart_product['quantity'] - 1;

				if ( $quantity == 0 ) {
					unset ( $cart[$cart_product_key] );
				}
				else {
					$cart_product['quantity'] = $quantity;
				}
				unset( $extra_vars[ $key ] );


				break;
			}
		}

		if (!$extra_vars){
			unset ($_SESSION['product']['cart_vars'][ $product_id ]['extra_vars']);
		}
		if (!$_SESSION['product']['cart_vars'][ $product_id ]){
			unset ($_SESSION['product']['cart_vars'][ $product_id ]);
		}
		if(!$cart) {
			unset( $_SESSION['product']['cart'] );
			unset( $_SESSION['product']['cart_vars'] );
		}


		if ( count( $extra_vars ) == 0 ) {
			unset ( $extra_vars );
		}

		//obtinc cart
		$module = Module::load('product', 'cart');
		$cart_vars = $module->get_cart_vars();


		json_end( [
			'all_basetax' => $cart_vars?$cart_vars['totals']['all_basetax']:false,
		] );

	}

	static function delete_item_time() {

		$reservaitem_id = R::id( 'reservaitem_id' );

		// De moment només es borra si ja es d'una reserva guardada
		//$product_id     = R::id( 'product_id' );
		// $cart       = &$_SESSION['product']['cart'];
		// $extra_vars = &$_SESSION['product']['cart_vars'][ $product_id ]['extra_vars'];

		$query = "
			UPDATE lassdive__orderitem 
			SET reservaitem_date='0000-00-00', start_time = '00:00:00', end_time = '00:00:00', centre_id = 0 
			WHERE  reservaitem_id = $reservaitem_id";
		Db::execute($query);

		$query = "
			DELETE FROM lassdive__reservaitem_to_user 
			WHERE  reservaitem_id = $reservaitem_id";
		Db::execute($query);

		json_end( [
			'success' => true
		] );

	}

	static function highlight( $q, $string){
		if (!$q) return $string;
		$pattern = preg_quote($q);
		$string  = preg_replace("/($pattern)/i",
        '<strong>$1</strong>', $string);
		return $string;

	}
}