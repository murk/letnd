<?

/**
 * LassdiveReservaList
 *
 * Funcions comuns per Lassdive Reserves
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2018
 * @version 1
 * @access public
 */
class LassdiveReservaList {

	var $search, $excel, $order_module, $is_search, $condition, $listing_condition, $action, $module, $is_admin = false, $is_public = false, $module_caption, $module_messages, $is_print = false, $is_reserva_form = false, $is_reserva_bin = false;

	public function list_records( &$module ) {

		Main::load_class( 'lassdive', 'reserva_excel', 'admin' );
		Main::load_class( 'lassdive', 'reserva_search', 'admin' );
		Main::load_class( 'lassdive', 'incoming', 'admin' );

		$this->module          = &$module;
		$this->module_caption  = &$module->caption;
		$this->module_messages = &$module->messages;
		$this->action          = $module->action;
		$this->excel           = New LassdiveReservaExcel();

		$this->order_module =  Module::load( 'product', 'order', $this->module, false, 'lassdive/lassdive_order_config.php' );

		$this->search = New LassdiveReservaSearch( $this->order_module );
		$this->search->init_search_advanced();
		$this->condition = $this->search->get_search_condition();

		$this->excel->view_format = $this->search->view_format;

		$content = $this->get_records();


		if ( $this->is_public ) {
			$GLOBALS['gl_page']->set_var( 'reserva_search', $this->search->get_search_form() );
			$GLOBALS['gl_content'] = $content;
		}
		else {
			$GLOBALS['gl_content'] = $this->search->get_search_form() . LassdiveIncoming::get_incoming( $this ) . $content;
		}
	}

	public function get_records() {

		$view_format = $this->search->view_format;

		if ( $view_format == 'all' ) {
			return $this->get_records_all( false );
		}
		else if ( $view_format == 'orderitems' ) {
			return $this->get_records_orderitems();
		}
		else {
			return $this->get_records_all( true );
		}

	}


	private function get_records_all( $view_only_order ) {

		$module = &$this->order_module;

		$module->messages = array_merge( $module->messages, $this->module_messages );

		$listing = new ListRecords( $module );
		$this->is_reserva_bin = $listing->is_bin;

		$listing->set_options( 1, 1, 1, 1, 1, 1, 0 );

		$listing->send_field_bottom( 'operator_id' );

		if ( $this->search->is_excel ) {
			$listing->set_field( 'all_tax', 'list_admin', 'text' );
			$listing->set_field( 'all_base', 'list_admin', 'text' );
			$listing->paginate = false;
		}

		if ( $this->is_admin ) $listing->add_swap_edit();

		if ( $this->is_public ) {
			$listing->template = 'lassdive/reserva_list.tpl';
		}
		elseif ( $view_only_order ) {
			$listing->use_default_template = true;
			$listing->set_field( 'deposit', 'list_admin', 'text' );
			$listing->set_field( 'deposit_rest', 'list_admin', 'text' );
			$listing->unset_field( 'orderitems' );

			$listing->add_button('button-edit-admin', 'action=show_form_edit', 'button-edit-admin');
		}
		else {
			$listing->set_field( 'deposit', 'list_admin', 'text' );
			$listing->set_field( 'deposit_rest', 'list_admin', 'text' );
			$listing->template = 'lassdive/reserva_all_list.tpl';
		}

		$col_span = $this->is_public ? 7 : 14;
		$listing->set_var( 'reserva_col_span', $col_span ); // Depend del disseny de la plantilla, -1 es el camp orderitems que no surt en columnes


		if ( $this->is_public ) {
			$listing->caption = array_merge( $listing->caption, $this->module_caption, $module->caption_list );
		}

		$listing->group_fields = [ 'order_status' ];

		// Per poder ordenar per reservaitem_date
		if ($this->search->reservaitem_date_field_query) {
			$listing->extra_fields = $this->search->reservaitem_date_field_query;
			$listing->order_by = 'reservaitem_date ASC';
		}
		else {
			$listing->order_by = 'entered_order DESC';
		}

		// Cerques
		if ( $this->condition ) {
			$listing->condition = $this->condition;
		}


		$listing->list_records( false );

		foreach ( $listing->results as $key => $val ) {
			$rs = &$listing->results[ $key ];
			if ( ! $view_only_order ) {
				$order_id         = $rs['order_id'];
				$rs['orderitems'] = $this->get_orderitems( $order_id, $listing->is_editable );
			}
			$rs['tr_class'] = $this->get_tr_class_order( $rs );
		}

		if ( $this->search->is_excel ) {

			$this->excel
				->set_vars( $listing )
				->make( $listing->results );

			return '';
		}
		else {
			return $listing->parse_template();
		}
	}

	private function get_records_orderitems() {

		$orderitems = $this->get_orderitems( false );

		if ( $this->search->is_excel ) {
			$this->excel->make( $orderitems );

			return '';
		}
		else {
			return $orderitems;
		}

	}

	public function get_orderitems( $order_id, $is_editable = false ) {


		$module         = Module::load( 'product', 'orderitem', '', false, 'lassdive/lassdive_orderitem_config.php' ); // així no necessito el product_orderitem.php
		$old_action     = $this->action;
		$module->action = 'get_records';

		// $price_sufix = '_basetax'; // mostro iva inclòs


		$listing       = new ListRecords( $module );
		$listing->join = "INNER JOIN lassdive__orderitem USING (orderitem_id)";
		$listing->set_var( 'has_order_date', true );
		$listing->set_var( 'is_reserva_bin', $this->is_reserva_bin );
		$listing->set_var( 'is_reserva_form', $this->is_reserva_form );
		$listing->set_var( 'is_orderitems_view', isset($this->search) && $this->search->view_format == 'orderitems' );

		// TODO-i Falta poder editar
		$listing->set_options( 1, 0, 1, 1, 0, 1, 0 ); // No es pot borrar ni editar
		$listing->has_bin                   = false;
		$listing->caption['c_product_date'] = $listing->caption['c_lassdive_product_date'];
		$listing->caption['c_product_basetax'] = $listing->caption['c_lassdive_product_basetax'];

		if ( $order_id ) {
			$listing->condition     = 'order_id = ' . $order_id;
			$listing->template      = 'lassdive/reserva_list_cart.tpl';
			$listing->paginate      = false;
			$listing->inputs_prefix = 'product__orderitem_saved';
			$listing->set_editable( $is_editable );
			$listing->order_by = 'orderitem_id ASC, start_time ASC, end_time ASC, product_title, reservaitem_id';
			$listing->set_field( 'is_done', 'type', ! $is_editable || $this->is_public ? 'select' : 'none' );

		}
		else {
			$listing->set_field( 'order_id', 'type', 'text' );
			$listing->set_field( 'order_id', 'list_admin', 'text' );

			// Per veure-ho només quan es fa el incoming
			$listing->set_field( 'product_date', 'list_admin', '0' );
			$listing->set_field( 'deposit', 'list_admin', 'text' );
			$listing->set_field( 'deposit_rest', 'list_admin', 'text' );
			$listing->set_field( 'deposit_status', 'list_admin', 'text' );
			$listing->set_field( 'deposit_payment_method', 'list_admin', 'text' );
			$listing->set_field( 'order_status', 'list_admin', 'text' );
			$listing->set_field( 'payment_method', 'list_admin', 'text' );


			$listing->template = 'lassdive/reserva_orderitem.tpl';
			if ( $this->is_admin ) $listing->add_swap_edit();
			$listing->order_by = 'start_time DESC, end_time DESC, order_id DESC, product_title';
			$listing->set_field( 'is_done', 'type', ! $listing->is_editable || $this->is_public ? 'select' : 'none' );
			// Cerques
			if ( $this->condition ) {
				$listing->condition = $this->condition;
			}
		}
		if ( ! $this->is_print && $this->search && $this->search->is_excel ) {
			$listing->paginate = false;
			$listing->set_field( 'is_done', 'type', 'select' );
		}


		$listing->list_records( false );

		Main::load_class( 'product', 'common_order', 'admin' );

		foreach ( $listing->results as $key => $val ) {
			$rs                           = &$listing->results[ $key ];
			$rs['tr_class']               = $this->get_tr_class_orderitem( $rs );
			$rs['product_variation_loop'] = $this->get_order_item_variations( $rs );
			$rs['user_id']                = $this->get_orderitem_users( $rs );
			$rs['is_done']                = $this->get_orderitem_is_done( $rs, $listing->get_field( 'is_done', 'type' ) );
		}

		if ( $this->is_print ) {
			$orderitems = $listing->results;
		}
		elseif ( $this->search && $this->search->is_excel ) {

			$this->excel
				->set_vars( $listing );

			// $listing->parse_template(false);
			$orderitems = $listing->results;
		}
		else {
			$orderitems = $listing->parse_template();
		}

		$this->action = $old_action;

		return $orderitems;
	}

	static public function get_order_item_variations( &$rs ) {

		$product_variations          = explode( "\r", $rs['product_variations'] );
		$product_variation_categorys = explode( "\r", $rs['product_variation_categorys'] );

		$ret = array();

		array_pop( $product_variations );
		array_pop( $product_variation_categorys );

		foreach ( $product_variations as $k => $v ) {
			$ret[] = array(
				'product_variation_category' => $product_variation_categorys[ $k ],
				'product_variation'          => $product_variations[ $k ]
			);
		}

		return $ret;

	}

	public function get_orderitem_users( $rs ) {
		$reservaitem_id = $rs['reservaitem_id'];

		$query   = "
			SELECT user_id 
			FROM lassdive__reservaitem_to_user
			WHERE reservaitem_id = $reservaitem_id";
		$results = Db::get_rows( $query );

		$users = [];

		$sep = is_object( $this->search ) && $this->search->is_excel ? ', ' : '<br>';
		foreach ( $results as $rs ) {
			$user_id  = $rs['user_id'];
			$query    = "
				SELECT CONCAT (name, ' ', surname) AS item
					FROM user__user 
					WHERE user_id = $user_id";
			$user     = Db::get_first( $query );
			$users [] = $user;
		}
		if ( ! $this->is_print ) $users = implode( $sep, $users );

		return $users;
	}

	private function get_tr_class_order( $rs ) {
		$order_status = $rs['order_status'];
		$source_id    = $rs['source_id'];
		$class        = "";

		$class = ( $order_status == 'completed' OR
		           $order_status == 'shipped' OR
		           $order_status == 'delivered' ) ? 'payed' : 'not-payed';

		$class .= $source_id ? ' centre' : ' not-centre';

		return $class;
	}


	private function get_tr_class_orderitem( $rs ) {
		$is_done        = $rs['is_done'];
		$product_date   = $rs['product_date'];
		$start_time     = $rs['start_time'];
		$end_time       = $rs['end_time'];
		$centre_id      = $rs['centre_id'];
		$reservaitem_id = $rs['reservaitem_id'];
		$product_id = $rs['product_id'];
		$user_id        = Db::get_first( "SELECT count(*) FROM lassdive__reservaitem_to_user WHERE reservaitem_id = $reservaitem_id" );


		$family_ids = Db::get_row( "SELECT family_id, subfamily_id, subsubfamily_id FROM product__product WHERE product_id = $product_id" );


		if ($family_ids['subsubfamily_id']) $family_id = $family_ids['subsubfamily_id'];
		elseif  ($family_ids['subfamily_id']) $family_id = $family_ids['subfamily_id'];
		else $family_id = $family_ids['family_id'];

		$has_calendar       = Db::get_first( "SELECT has_calendar FROM product__family WHERE family_id = $family_id" );

		$class = (
			$product_date
			&& $start_time
			&& $end_time
			&& $centre_id
			&& $user_id
		) ? 'half-done' : 'not-done';

		$class = (
			$is_done
			&& $product_date
			&& $start_time
			&& $end_time
			&& $centre_id
			&& $user_id
		) ? 'done' : $class;

		if (!$has_calendar) $class = $is_done?'done':'half-done';

		return $class;
	}

	private function get_orderitem_is_done( $rs, $type ) {

		if ( $type != 'none' ) return $rs['is_done'];

		$is_done        = $rs['is_done'] ? 'checked' : '';
		$reservaitem_id = $rs['reservaitem_id'];
		$ret            = "
			<input name='product__orderitem_saved[is_done][$reservaitem_id]' id='product__orderitem_saved_is_done_$reservaitem_id' type='checkbox' value='1' $is_done>
			<input type='hidden' name='is_dones[$reservaitem_id]'>";

		return $ret;
	}
}