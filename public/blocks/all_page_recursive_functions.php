<?php
function _block_all_page_get_children($parent_id, $level, $selected_ids, $menu, $last_level){
	$loop = array();
	if ($last_level == $level) return $loop;
	$query = "SELECT all__page.page_id as page_id, link, page, title, body_subtitle, page_file_name, parent_id, has_content, page_text_id
					FROM all__page_language, all__page
					WHERE all__page_language.page_id = all__page.page_id
					AND status = 'public'
					AND parent_id = " . $parent_id . "
					AND bin <> 1
					AND menu = '" . $menu . "'
					AND language = '" . LANGUAGE . "'
					ORDER BY ordre ASC, page_id";
	Debug::add($query,'Query all_page_recursive');	
	$results = Db::get_rows($query);

	if ($results)
	{
		$conta = 0;
		$loop_count = count($results);
		foreach($results as $rs)
		{
			
			$selected = (isset($selected_ids[$level]) && ($selected_ids[$level] == $rs["page_id"]))?1:0;
			
			$l['link'] = Page::get_page_link($rs['page_id'],$rs['link'],$rs['page_file_name'],$rs['has_content']);
			$l['selected'] = $selected;
			$l['item'] = $rs['page'];
			$l['title'] = $rs['title']; // si no hi ha title, el titol es el page ( com era al principi ) -> _block_all_page_set_top_vars
			$l['body_subtitle'] = $rs['body_subtitle'];
			$l['has_content'] = $rs['has_content'];
			$l['id'] = $rs['page_id'];
			$l['page_text_id'] = $rs['page_text_id'];
			$l['conta'] = $conta;
			$l['class'] = '';
			$l['loop_count'] = $loop_count;
			$l['loop'] = _block_all_page_get_children($rs['page_id'], $level+1, $selected_ids, $menu, $last_level);
			$conta++;
			$loop[] = $l;
		}
		
		$loop[0]['class'] = 'first';	
		$loop[$loop_count-1]['class'] = 'last';
		//$loop[] = array ('loop' => $loop2,
		//	'item' => '');
	}
	return $loop;
}
function _block_all_page_set_top_vars(&$loop, $selected_id){
	static $is_set_title = false;
	if ($is_set_title) return;
	$is_set_title = true;
	
	if (!$loop) return;
	foreach ($loop as $l){
		if ($l['id']==$selected_id) {
			$GLOBALS['gl_page']->top_title = $l['title']?$l['title']:$l['item'];// si no hi ha title, el titol es el page ( com era al principi )
			$GLOBALS['gl_page']->top_page_id = $l['id'];
			$GLOBALS['gl_page']->top_body_subtitle = $l['body_subtitle'];
			break;
		}
	}
	
}
?>