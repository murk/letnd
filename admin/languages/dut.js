var NONE_SELECTED = " No row has been selected ";
 
var VALIDAR_TEXT1 = ' is verplicht ';
var VALIDAR_TEXT2 = 'Invalid characters ';
var VALIDAR_TEXT3 = ' is geen korrekt E-mail adres. ';
var VALIDAR_TEXT4 = ' Must have a minimum of three characters and @';
var VALIDAR_TEXT5 = ' Must be a number ';
var VALIDAR_TEXT6 = ' Must be a five-digit number ';
var VALIDAR_TEXT7 = ' Contains ';
var VALIDAR_TEXT8 = ' characters, cannot be longer than ';
var VALIDAR_TEXT9 = ' Year missing or is non-numeric ';
var VALIDAR_TEXT10 = ' The year is not correct ';
var VALIDAR_TEXT11 = ' The date is not correct ';
var VALIDAR_TEXT12 = ' Is not correct, write eight digits and the letter ';
var VALIDAR_TEXT13 = ' is not correct ';
var VALIDAR_TEXT14 = ' The letter is incorrect ';
var VALIDAR_TEXT15 = ' is not correct ';
var VALIDAR_TEXT16 = 'Must select ';
var VALIDAR_TEXT17 = 'Het formulier werd niet korrekt ingevuld ';
var VALIDAR_TEXT18 = ' You must select at least one option ';
var VALIDAR_TEXT19 = ' You must select at least one address or group ';
var VALIDAR_TEXT20 = ' The credit card number is incorrect ';
var VALIDAR_TEXT21 = ' The credit card expiry date is incorrect ';
var VALIDAR_TEXT22 = ' is not correct';
var VALIDAR_TEXT23 = ' is not correct';
var VALIDAR_TEXT24 = ' must be a number';
var VALIDAR_TEXT25 = ' the passwords are not equal';
var VALIDAR_TEXT26 = ' confirmation is not equal';
var VALIDAR_TEXT27 = ' only can contain numbers, lower case characters from "a to z" or "_-" or dash-underscore ("_" o "-")';
var VALIDAR_TEXT28 = 'You must accept the privacy policy';
var MODALBOX_SAVE = ' Sending data ...';
var MODALBOX_CLOSE = 'close';
var MULTIFILE_ACCEPT = ' You only can upload files of type:: ';
var MULTIFILE_NOT_ACCEPT = ' You can not upload files of type: ';
var CONFIRM_TEXT = 'Are you sure you want to do this action?';
var WAIT_TEXT = 'One second plis ...';
 
var MONTH_NAMES = new Array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");

var FILE_DELETE = "Delete";
// NO TRADUIT
var IMMO_MAP_POINT_SELECTED = "Click in a point of the map to establish the situation";
var PRODUCT_CART_ADDED = "Your product has been added to the cart";

var MESSAGE_CONFIRM_DELETE_NOT_SENT = 'Seguro que deseas borrar esta dirección de la llista de destinatarios?';

var NO_RESULTS = 'No results';