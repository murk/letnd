<?

/**
 * Productfamilybusy
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2011
 * @version $Id$
 * @access public
 */
class ProductFamilybusy extends Module {
	var $products, $form_tabs;

	function __construct() {
		parent::__construct();
	}

	function list_records() {
		$GLOBALS['gl_content'] = $this->get_records();
	}

	function get_records() {
		$listing          = new ListRecords( $this );
		$listing->has_bin = false;

		return $listing->list_records();
	}

	function show_form() {
		$GLOBALS['gl_content'] = $this->get_form();
	}

	function get_form() {
		$family_id = R::id( 'family_id' );
		$date_in   = R::escape( 'date_in' );
		$date_out  = R::escape( 'date_out' );

		$this->set_field( 'family_id', 'default_value', $family_id );
		$this->set_field( 'date_out', 'default_value', $date_out );
		$this->set_field( 'date_in', 'default_value', $date_in );

		$show = new ShowForm( $this );

		if ( ! $family_id ) {
			$show->get_values();
			$family_id = $show->rs['family_id'];
		}

		$show->has_bin = false;

		return $show->show_form();
	}

	public function write_record_from_calendar() {

		$date_busy = R::escape( 'date_busy' );
		$family_id = R::id( 'family_id' );

		$date_busy = unformat_date( $date_busy, true );

		if ( $this->process == 'occupy' ) {

			$query = "INSERT INTO product__familybusy (`family_id`,`date_busy`) VALUES ('$family_id',$date_busy);";
			$message = $this->caption ['c_calendar_saved_occupied_message'];
		}
		else {

			$query = "DELETE FROM product__familybusy WHERE `date_busy` = $date_busy AND `family_id` = '$family_id';";
			$message = $this->caption ['c_calendar_saved_freed_message'];

		}


		Db::execute( $query );

		$ret['message'] = $message;

		die ( json_encode( $ret ) );
	}

	function manage_images() {
		$image_manager = new ImageManager( $this );
		$image_manager->execute();
	}


	// funcio que es crida desdel modul de families
	public function show_family_calendar() {
		$family_id = $this->family_id = R::id( 'family_id' );

		include_once 'product_calendar.php';
		$calendar = New ProductCalendar( $this->process );
		$calendar->is_family_calendar = true;


		//$this->check_availability();
		$calendar_arr   = $calendar->get_calendar_array();
		$this->products = $calendar->products;
		$this->set_vars( $calendar_arr );

		//$book_form = $this->do_action('get_form_new');

		// $book_list = $this->do_action( 'get_records' );


		$this->tpl->set_var( 'form_tabs', $this->form_tabs );
		$this->tpl->set_vars( $this->caption );

		/*$this->tpl->set_vars( [
			// 'products' => $this->products,
			'calendar_list' => $book_list,
		] );*/

		$this->tpl->file = 'product/product_familycalendar.tpl';

		$language = LANGUAGE;
		$rs       = Db::get_row( "SELECT family, family_id FROM product__family_language WHERE family_id = $family_id AND language = '$language'" );

		$has_calendar       = Db::get_first( "SELECT has_calendar FROM product__family WHERE family_id = $family_id" );

		$rs['calendar_hidden'] = $has_calendar?'':' hidden';
		$rs['has_calendar'] = '<input type="checkbox" id="has_calendar" name="has_calendar" value="1" '.($has_calendar?' checked="checked"':'') .' >';


		$is_calendar_public       = Db::get_first( "SELECT is_calendar_public FROM product__family WHERE family_id = $family_id" );
		$rs['is_calendar_public'] = '<input type="checkbox" id="is_calendar_public" name="is_calendar_public" value="1" '.($is_calendar_public?' checked="checked"':'') .' >';

		$this->tpl->set_vars( $rs );

		Main::load_class( 'product', 'calendar', 'admin' );
		ProductCalendar::set_calendar_vars( $this, $family_id );

		$GLOBALS['gl_content'] = $this->process();
	}


	/*
	 * Calendari
	 */

	public function get_month() {

		include_once 'product_calendar.php';
		$calendar = New ProductCalendar( $this->process );
		$calendar->is_family_calendar = true;

		$calendar->get_month();
	}
}

?>