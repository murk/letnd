<?php

$module  = Module::load( 'inmo', 'property', '', false );
$page_id = isset( $vars['page_id'] ) ? $vars['page_id'] : false;
$select_default = isset( $vars['select_default'] ) ? $vars['select_default'] : true; // per el buscador selecciono primer per defecte, però en el cas de attitude no vull que seleccioni cap així puc buscar només per refereència sense que hi hagi "venda" seleccionat

$vars['var_name'] = 'tipus';

$query = "SELECT DISTINCT tipus
				FROM inmo__property
				WHERE bin = 0 AND (status = 'onsale' OR status = 'sold' OR status = 'reserved')
				ORDER BY FIELD(tipus, 'sell','rent','temp','moblat','selloption')";

$results = Db::get_rows( $query );


$tipus                  = R::text_id( 'tipus' );
$vars['block_selected'] = $tipus ? 1 : 0;
$selected_found         = false;

if ( $results ) {
	foreach ( $results as $rs ) {
		if ( ( $tipus == $rs["tipus"] ) ) {
			$selected       = 1;
			$selected_found = true;
		} else {
			$selected = 0;
		}
		// pel filtre
		if ( isset( $_GET['tipuss'] ) &&
		     in_array( $rs["tipus"], $_GET['tipuss'] )
		) {
			$selected = 1;
			$vars['block_selected'] = 1;
		}


		$l['link'] = get_link( array(
			'tool'            => 'inmo',
			'tool_section'    => 'property',
			'friendly_params' => array( 'tipus' => $rs['tipus'] ),
			'link_vars'       => array( 'tipus' => $rs["tipus"] ),
			'detect_page'     => $page_id ? false : true,
			'page_id'         => $page_id
		) );

		$l['selected'] = $selected;
		$l['value']    = $rs['tipus'];
		$l['item']     = $module->caption[ 'c_' . $rs['tipus'] ];
		$l['class']    = '';
		$loop2[]       = $l;
	}


	$loop2[0]['class']                     = 'first';
	$loop2[ count( $loop2 ) - 1 ]['class'] = 'last';

	if ( ! $selected_found && $select_default ) {
		$loop2[0]['selected'] = '1';
	}

	// Debug::p( $loop2 );

	$loop[] = array(
		'loop' => $loop2,
		'item' => ''
	);
}


?>