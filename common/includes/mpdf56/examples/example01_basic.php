<?php


$html = '<div id="pv" class="pdf">
	<table>
	    <thead>
			<tr>
				<td colspan="8">
					<table class="pv-head">
						<tr>
							<td class="pv-head-logo"><img src="themes/inmotools/pv/logo_pv.png" width="74" height="53" alt="PV" /></td>
							<td class="pv-head-titol">
								<div>
									<strong>Seguiment reglamentari</strong>
									Repertori d’evolucions de Mobiltac, SA								</div>
							</td>
							<td class="pv-head-codi">
								<div><strong>F-43-02-02</strong>
									Ed.:1</div>
							</td>
							<td class="pv-head-logo last"><img width="94"height="53" src="../clients/consultoriapvgestio/pv/client/images/1_details.png" /></td>
						</tr>
					</table>	
				</td>
			</tr>
			<tr class="pv-cols-head-sep">
				<td colspan="8"></td>
			</tr>
			<tr class="pv-cols-head">
				<td class="first">Codi lloc</td>
				<td>Textos</td>
				<td class="pv-small">Class.</td>
				<td>Requisits</td>
				<td class="pv-small">Evaluació</td>
				<td class="pv-small">AM/Plaç</td>
				<td class="pv-small">Comentaris</td>
				<td class="pv-small last">Arxiu</td>
							</tr>
	    </thead>
		<tfoot>
			<tr class="pv-foot-sep">
				<td colspan="8"></td>
			</tr>
			<tr class="pv-foot">
				<td colspan="8">C/ Llibertat, 159 2-C  17820  Banyoles               Tel: 972 58 33 38  Fax: 972 58 33 39               www.consultoriapv.com</td>
			</tr>
		</tfoot>
	    <tbody>
								<tr class="pv-categoria">
				<td colspan="8">Aigua - AG</td>
			</tr>
						<tr>
								<td class="pv-codi"><div>ESP AG Nº1</div></td>
								
				<td class="pv-norma">
					<p><strong>Real Decreto Legislativo 1/2001,</strong> de 20 de julio, por el que se aprueba el texto refundido de la Ley de Aguas<br />modificada por Ley 11/2005, de 22 de junio<br />desarrollada por <strong>Orden 1312/2009,</strong> de 20/05/2009, Se regulan los sistemas para realizar el control efectivo de los vol&uacute;menes de agua utilizados por los aprovechamientos de agua del dominio p&uacute;blico hidr&aacute;ulico, de los retornos al citado dominio p&uacute;blico hidr&aacute;ulico y de los vertidos al mismo.</p>
<p><strong>Real Decreto 849/1986,</strong> de 11 de abril, por el que se aprueba el Reglamento del Dominio P&uacute;blico Hidr&aacute;ulico, que desarrolla los t&iacute;tulos preliminares I, IV, V, VI Y VII de la Ley 29/1985, de 2 de Agosto, de Aguas.<br />Complementada por Orden de 12 de noviembre de 1987, sobre normas de emisi&oacute;n, objetivos de calidad y m&eacute;todos de medici&oacute;n de referencia relativos a determinadas sustancias nocivas o peligrosas contenidas en los vertidos de aguas residuales<br />Modificada por <strong>Real Decreto 1315/1992</strong><br />Modificada por <strong>Real Decreto 1771/1994</strong><br />Modificada por <strong>Real Decreto 995/2000</strong><br />Modificada por <strong>Real Decreto 419/1993</strong><br />Modificada por<strong> Real Decreto 606/2003</strong><br />Modificada por <strong>Real Decreto 9/2008</strong></p>
<p>Complementada por <strong>Orden MAM/1873/2004</strong>, de 2 de junio, por la que se aprueban los modelos oficiales para la declaraci&oacute;n de vertido y se desarrollan determinados aspectos relativos a la autorizaci&oacute;n de vertido y liquidaci&oacute;n del canon de control de vertido regulados en el RD 606/2003.</p>
<p>Complementada por <strong>Orden MAM/985/2006,</strong> de 23 de marzo, por la que se desarrolla el r&eacute;gimen jur&iacute;dico de las entidades colaboradoras de la administraci&oacute;n hidr&aacute;ulica en materia de control y vigilancia de calidad de las aguas y de gesti&oacute;n de los vertidos al dominio p&uacute;blico hidr&aacute;ulico.<br /><strong>Orden de 12 de noviembre de 1987</strong>, sobre normas de emisi&oacute;n, objetivos de calidad y m&eacute;todos de medici&oacute;n de referencia relativos a determinadas sustancias nocivas o peligrosas contenidas en los vertidos de aguas residuales<br />Modificada por <strong>Orden de 13 de marzo de 1989</strong><br />Modificada por <strong>0rden de 28 de junio de 1991</strong><br />Modificada por <strong>Real Decreto-Ley 4/2007</strong>, de 13/04/2007, Se modifica el texto refundido de la Ley de AGUAS.<br /><strong>Real Decreto 907/2007,</strong> de 06/07/2007, Se aprueba el Reglamento de la Planificaci&oacute;n Hidrol&oacute;gica.<br />Modificado por <strong>Real Decreto 367/2010, </strong>de 26/03/2010.</p>				</td>
				
				<td class="pv-classificacio">
											<div>
							A<input name="seguimentitem_id[57]" type="hidden" value="57" />
						</div>
											<div>
							A<input name="seguimentitem_id[62]" type="hidden" value="62" />
						</div>
											<div>
							A<input name="seguimentitem_id[67]" type="hidden" value="67" />
						</div>
											<div>
							A<input name="seguimentitem_id[72]" type="hidden" value="72" />
						</div>
											<div>
							A<input name="seguimentitem_id[77]" type="hidden" value="77" />
						</div>
											<div>
							A<input name="seguimentitem_id[82]" type="hidden" value="82" />
						</div>
											<div>
							A<input name="seguimentitem_id[87]" type="hidden" value="87" />
						</div>
									</td>
				<td class="pv-requisit">
					
					<table class="table-requisit">
											<tr>
							<th>1</th><td>Aprovechamiento agua mediante fuentes propias     RD849/86 y RDL1/01</td>
						</tr>
											<tr>
							<th>2</th><td>Autorización de veritdo</td>
						</tr>
											<tr>
							<th>3</th><td>Obligaciones de los predios inferiores</td>
						</tr>
											<tr>
							<th>4</th><td>Liquidación del canon de control de vertido</td>
						</tr>
											<tr>
							<th>5</th><td>Protección general de las aguas</td>
						</tr>
											<tr>
							<th>6</th><td>Registrar las concesiones de aguas</td>
						</tr>
											<tr>
							<th>7</th><td>Prohibiciones de vertido</td>
						</tr>
										</table>
					
				</td>
				<td class="pv-evaluacio">
					
											<div class="pv-red">
							No conforme						</div>
											<div class="">
							Informació						</div>
											<div class="">
							Informació						</div>
											<div class="">
							Informació						</div>
											<div class="">
							Informació						</div>
											<div class="">
							Informació						</div>
											<div class="">
							Informació						</div>
										
				</td>
				<td class="pv-tancament">
					
											<div>
													</div>
											<div>
													</div>
											<div>
													</div>
											<div>
													</div>
											<div>
													</div>
											<div>
													</div>
											<div>
													</div>
										
				</td>
				<td>
					
											<div>
													</div>
											<div>
							Comentari bla bla bla...						</div>
											<div>
													</div>
											<div>
													</div>
											<div>
													</div>
											<div>
													</div>
											<div>
													</div>
										
				</td>
				<td>
					
											<div>
							<a href="" title="">Arxiu</a>
						</div>
											<div>
							<a href="" title="">Arxiu</a>
						</div>
											<div>
							<a href="" title="">Arxiu</a>
						</div>
											<div>
							<a href="" title="">Arxiu</a>
						</div>
											<div>
							<a href="" title="">Arxiu</a>
						</div>
											<div>
							<a href="" title="">Arxiu</a>
						</div>
											<div>
							<a href="" title="">Arxiu</a>
						</div>
										
				</td>
							</tr>
									<tr>
								<td class="pv-evolucio">
					<strong>Evolució A</strong>
					<br />
					21.06.2010				</td>
								
				<td class="pv-norma">
					<p>Modificado por <strong>Real Decreto-ley 17/2012,</strong> de 4 de mayo, de medidas urgentes en materia de medio ambiente.</p>				</td>
				
				<td class="pv-classificacio">
											<div>
							A<input name="seguimentitem_id[92]" type="hidden" value="92" />
						</div>
											<div>
							A<input name="seguimentitem_id[97]" type="hidden" value="97" />
						</div>
									</td>
				<td class="pv-requisit">
					
					<table class="table-requisit">
											<tr>
							<th>8</th><td>Respetar límites de vertido</td>
						</tr>
											<tr>
							<th>9</th><td>Realizar la declaración de vertido</td>
						</tr>
										</table>
					
				</td>
				<td class="pv-evaluacio">
					
											<div class="">
							Informació						</div>
											<div class="">
							Informació						</div>
										
				</td>
				<td class="pv-tancament">
					
											<div>
													</div>
											<div>
													</div>
										
				</td>
				<td>
					
											<div>
													</div>
											<div>
													</div>
										
				</td>
				<td>
					
											<div>
							<a href="" title="">Arxiu</a>
						</div>
											<div>
							<a href="" title="">Arxiu</a>
						</div>
										
				</td>
							</tr>
									<tr>
								<td class="pv-evolucio">
					<strong>Evolució B</strong>
					<br />
					21.06.2010				</td>
								
				<td class="pv-norma">
					<p>Modificaci&oacute;n del Real Decreto 849/1986 por <strong>Real Decreto 1290/2012,</strong> de 07/09/2012, Se modifica el Reglamento del Dominio P&uacute;blico Hidr&aacute;ulico, aprobado por el Real Decreto 849/1986, de 11 de abril, y el Real Decreto 509/1996, de 15 de marzo, de desarrollo del Real Decreto-ley 11/1995, de 28 de diciembre, por el que se establecen las normas aplicables al tratamiento de las aguas residuales urbanas. (BOE n&ordm; 227, de 20/09/2012)<br /><strong>Correcci&oacute;n de errores, Del Real Decreto 1290/2012,</strong> de 7 de septiembre, por el que se modifica el Reglamento del Dominio P&uacute;blico Hidr&aacute;ulico, aprobado por el Real Decreto 849/1986, de 11 de abril, y el Real Decreto 509/1996, de 15 de marzo, de desarrollo del Real Decreto-ley 11/1995, de 28 de diciembre, por el que se establecen las normas aplicables al tratamiento de las aguas residuales urbanas. (BOE n&ordm; 251, de 18/10/2012)</p>				</td>
				
				<td class="pv-classificacio">
											<div>
							A<input name="seguimentitem_id[102]" type="hidden" value="102" />
						</div>
											<div>
							A<input name="seguimentitem_id[107]" type="hidden" value="107" />
						</div>
									</td>
				<td class="pv-requisit">
					
					<table class="table-requisit">
											<tr>
							<th>10</th><td>Actuacion de la entidades colaboradoras. Actos sujetos a control y certificación de vertido</td>
						</tr>
											<tr>
							<th>11</th><td>Control efectivo de los volúmenes de agua utilizados por los aprovechamientos de agua del dominio público hidráulico, de los retornos al citado dominio público hidráulico y de los vertidos al mismo.</td>
						</tr>
										</table>
					
				</td>
				<td class="pv-evaluacio">
					
											<div class="">
							Informació						</div>
											<div class="">
							Informació						</div>
										
				</td>
				<td class="pv-tancament">
					
											<div>
													</div>
											<div>
													</div>
										
				</td>
				<td>
					
											<div>
													</div>
											<div>
													</div>
										
				</td>
				<td>
					
											<div>
							<a href="" title="">Arxiu</a>
						</div>
											<div>
							<a href="" title="">Arxiu</a>
						</div>
										
				</td>
							</tr>
									<tr class="pv-categoria">
				<td colspan="8">Normes generals medi ambient - NG</td>
			</tr>
						<tr>
								<td class="pv-codi"><div>ESP NG Nº0</div></td>
								
				<td class="pv-norma">
					<p>Real Decreto Legislativo 1/2001, de 20 de julio, por el que se aprueba el texto refundido de la Ley de Aguas<br />modificada por Ley 11/2005, de 22 de junio<br />desarrollada por Orden 1312/2009, de 20/05/2009, Se regulan los sistemas para realizar el control efectivo de los vol&uacute;menes de agua utilizados por los aprovechamientos de agua del dominio p&uacute;blico hidr&aacute;ulico, de los retornos al citado dominio p&uacute;blico hidr&aacute;ulico y de los vertidos al mismo.</p>
<p>Real Decreto 849/1986, de 11 de abril, por el que se aprueba el Reglamento del Dominio P&uacute;blico Hidr&aacute;ulico, que desarrolla los t&iacute;tulos preliminares I, IV, V, VI Y VII de la Ley 29/1985, de 2 de Agosto, de Aguas.<br />Complementada por Orden de 12 de noviembre de 1987, sobre normas de emisi&oacute;n, objetivos de calidad y m&eacute;todos de medici&oacute;n de referencia relativos a determinadas sustancias nocivas o peligrosas contenidas en los vertidos de aguas residuales<br />Modificada por Real Decreto 1315/1992<br />Modificada por Real Decreto 1771/1994<br />Modificada por Real Decreto 995/2000<br />Modificada por Real Decreto 419/1993<br />Modificada por Real Decreto 606/2003<br />Modificada por Real Decreto 9/2008</p>
<p>Complementada por Orden MAM/1873/2004, de 2 de junio, por la que se aprueban los modelos oficiales para la declaraci&oacute;n de vertido y se desarrollan determinados aspectos relativos a la autorizaci&oacute;n de vertido y liquidaci&oacute;n del canon de control de vertido regulados en el RD 606/2003.</p>
<p>Complementada por Orden MAM/985/2006, de 23 de marzo, por la que se desarrolla el r&eacute;gimen jur&iacute;dico de las entidades colaboradoras de la administraci&oacute;n hidr&aacute;ulica en materia de control y vigilancia de calidad de las aguas y de gesti&oacute;n de los vertidos al dominio p&uacute;blico hidr&aacute;ulico.<br />Orden de 12 de noviembre de 1987, sobre normas de emisi&oacute;n, objetivos de calidad y m&eacute;todos de medici&oacute;n de referencia relativos a determinadas sustancias nocivas o peligrosas contenidas en los vertidos de aguas residuales<br />Modificada por Orden de 13 de marzo de 1989<br />Modificada por 0rden de 28 de junio de 1991<br />Modificada por Real Decreto-Ley 4/2007, de 13/04/2007, Se modifica el texto refundido de la Ley de AGUAS.<br />Real Decreto 907/2007, de 06/07/2007, Se aprueba el Reglamento de la Planificaci&oacute;n Hidrol&oacute;gica.<br />Modificado por Real Decreto 367/2010, de 26/03/2010.</p>
<p>&nbsp;</p>				</td>
				
				<td class="pv-classificacio">
											<div>
							A<input name="seguimentitem_id[112]" type="hidden" value="112" />
						</div>
											<div>
							A<input name="seguimentitem_id[117]" type="hidden" value="117" />
						</div>
									</td>
				<td class="pv-requisit">
					
					<table class="table-requisit">
											<tr>
							<th>1</th><td>primer requisit</td>
						</tr>
											<tr>
							<th>2</th><td>segon requisit</td>
						</tr>
										</table>
					
				</td>
				<td class="pv-evaluacio">
					
											<div class="">
							Informació						</div>
											<div class="">
							Informació						</div>
										
				</td>
				<td class="pv-tancament">
					
											<div>
													</div>
											<div>
													</div>
										
				</td>
				<td>
					
											<div>
													</div>
											<div>
													</div>
										
				</td>
				<td>
					
											<div>
							<a href="" title="">Arxiu</a>
						</div>
											<div>
							<a href="" title="">Arxiu</a>
						</div>
										
				</td>
							</tr>
									<tr>
								<td class="pv-evolucio">
					<strong>Evolució A</strong>
					<br />
					04.04.2013				</td>
								
				<td class="pv-norma">
					<p>Complementada por Orden MAM/985/2006, de 23 de marzo, por la que se desarrolla el r&eacute;gimen jur&iacute;dico de las entidades colaboradoras de la administraci&oacute;n hidr&aacute;ulica en materia de control y vigilancia de calidad de las aguas y de gesti&oacute;n de los vertidos al dominio p&uacute;blico hidr&aacute;ulico.<br />Orden de 12 de noviembre de 1987, sobre normas de emisi&oacute;n, objetivos de calidad y m&eacute;todos de medici&oacute;n de referencia relativos a determinadas sustancias nocivas o peligrosas contenidas en los vertidos de aguas residuales<br />Modificada por Orden de 13 de marzo de 1989<br />Modificada por 0rden de 28 de junio de 1991<br />Modificada por Real Decreto-Ley 4/2007, de 13/04/2007, Se modifica el texto refundido de la Ley de AGUAS.<br />Real Decreto 907/2007, de 06/07/2007, Se aprueba el Reglamento de la Planificaci&oacute;n Hidrol&oacute;gica.<br />Modificado por Real Decreto 367/2010, de 26/03/2010.</p>
<p>&nbsp;</p>				</td>
				
				<td class="pv-classificacio">
											<div>
							A<input name="seguimentitem_id[122]" type="hidden" value="122" />
						</div>
									</td>
				<td class="pv-requisit">
					
					<table class="table-requisit">
											<tr>
							<th>3</th><td>requisit evolucio</td>
						</tr>
										</table>
					
				</td>
				<td class="pv-evaluacio">
					
											<div class="">
							Informació						</div>
										
				</td>
				<td class="pv-tancament">
					
											<div>
													</div>
										
				</td>
				<td>
					
											<div>
													</div>
										
				</td>
				<td>
					
											<div>
							<a href="" title="">Arxiu</a>
						</div>
										
				</td>
							</tr>
					</tbody>
	</table>
</div>	
	

';


//==============================================================
//==============================================================
//==============================================================

include("../mpdf.php");

$mpdf=new mPDF(); 

$mpdf->WriteHTML($html);
$mpdf->Output();
exit;

//==============================================================
//==============================================================
//==============================================================


?>