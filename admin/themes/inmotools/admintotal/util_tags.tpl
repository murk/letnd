<script type="text/javaScript" src="/common/jscripts/chili/jquery.chili.pack.js"></script>
<script type="text/javascript">
	ChiliBook.recipeFolder     = "/common/jscripts/chili/";
	ChiliBook.stylesheetFolder = "/common/jscripts/chili/";
</script>
<script language="JavaScript">
function clic_vars(){
}
</script>
<style type="text/css">
<!--
input.cap, select.cap {
	font-size:14px;
}
-->
</style>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="form">
  <tr>
    <td valign="bottom">
    <div class="menu">
    <a href="?menu_id=1033">Idiomes</a>
    <a href="?menu_id=1033&amp;action=list_records_vars">Variables</a>
    <div>Tags</div>
    </td>
    <td class="buttons_top"><div class="right"></div>
    </td>
  </tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td class="formCaptionTitle">
Llistat de tags per les plantilles
</td></tr></table>
<br />
<br />
<br />
<br />
<form method="get" name="theForm" action="/admin/">
  <table style='margin-left:30px;' border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td><input type="hidden" value="1033" name="menu_id" /><input type="hidden" value="list_records_tags" name="action" /><?=$table_select?></td>
        <td>
        <input name="button" type="submit" id="button" value="ok" /></td>
      </tr>
</table>
</form>
<?if($loop):?>
<pre style='margin:30px;padding:0 30px;margin-top:10px;font-size: 14px;background-color: #FFFFFF;border: 1px #CCC solid;'>
<code class='php'>
&lt;?foreach($loop as $l):extract ($l)?&gt;

<?if($has_images):?>
	&lt;?foreach($images as $l):extract ($l)?&gt;
		&lt;div class=&quot;image&quot;&gt;&lt;a href=&quot;&lt;?=$image_name_medium?&gt;&quot; title=&quot;&quot;&gt;
		&lt;img src=&quot;&lt;?=$image_src_details?&gt;&quot; width=&quot;&quot; border=&quot;0&quot; alt=&quot;&quot;/&gt;
		&lt;/a&gt;&lt;/div&gt;
	&lt;?endforeach //images?&gt;

<?endif //has_images?>
<?if($has_files):?>
	&lt;?foreach($files as $file):extract($file)?&gt;
		&lt;p class=&quot;file&quot;&gt;&lt;a href=&quot;&lt;?=$file_src?&gt;&quot; target=&quot;_blank&quot;&gt;
		&lt;?=$file_name_original?&gt;
		&lt;/a&gt;&lt;/p&gt;
	&lt;?endforeach //files?&gt;

<?endif //has_files?>
<? foreach($loop as $l): extract ($l)?>
	&lt;?if($<?=$name?>):?&gt;
		&lt;p class=&quot;<?=$name?>&quot;&gt;
		&lt;?=$<?=$name?>?&gt;
		&lt;/p&gt;
	&lt;?endif //<?=$name?>?&gt;

<? endforeach //?>
&lt;?endforeach //$loop?&gt;
</code>
</pre>
<?endif //loop?>