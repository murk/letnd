let gulp = require('gulp'),
	livereload = require('gulp-livereload'),
	sass = require('gulp-sass'),
	concat = require('gulp-concat'),
	cache = require('gulp-cached'),
	//wait = require('gulp-wait'),

	// post processors
	postcss = require('gulp-postcss'),
	autoprefixer = require('autoprefixer'),
	cleanCSS = require('postcss-clean'), // mes ràpid que el nano

	terser = require('gulp-terser'),
	sourcemaps = require('gulp-sourcemaps'),

	// Sincronitza
	del = require('del'),
	path = require('path'),
	newer = require('gulp-newer'),

	include       = require("gulp-include"),
	fs = require('fs');
	colors = require('ansi-colors'),


base_dir = '/Letnd/public/themes/common/inmo/tv/';
base_dir_templates = '/Letnd/public/themes/common/inmo/tv/';

log_init('\n\n Iniciat: ' + base_dir_templates + '\n');

let src = {
	scss: base_dir_templates + 'dev/*.scss',
	scss_target: base_dir_templates + '',
	scss_target_dist: base_dir_templates + '/styles.min.css',
	scss_base_file: base_dir_templates + 'dev/styles.scss',

	tpls: base_dir_templates + '../*.tpl',

	js: base_dir_templates + 'dev/*.js',
	js_target: base_dir_templates + '',
};

// WATCH
gulp.task('watch', function () {

	livereload({start: true});

	gulp.watch(src.scss, ['sass']);
	gulp.watch(src.tpls, ['tpls']);
	gulp.watch(src.js, ['js']);
});

// tpls
gulp.task('tpls', function () {
	return gulp.src(src.tpls)
		.pipe(cache('tpl'))
		.pipe(livereload())
		//, .pipe(browserSync.stream())
		;
});

// js
gulp.task('js', function () {

	gulp.src(src.js)
      .pipe(include())
		.pipe(concat('jscripts.js'))
      .on('error', console.log)
		.pipe(sourcemaps.write({includeContent: false, sourceRoot: 'dev'}))
		.pipe(gulp.dest(src.js_target))
		.pipe(livereload())
	;

	return gulp.src(src.js)
		// .pipe(cache('js'))
      .pipe(include())
		.pipe(concat('jscripts.min.js'))
		.pipe(terser(
			{
				toplevel: true,
				compress: {
					passes: 2
				},
				output: {
					beautify: false
				}
			}
		))
      .on('error', console.log)
		.pipe(gulp.dest(src.js_target))
		.pipe(livereload())
		//, .pipe(browserSync.stream())
		;
});

// SASS
gulp.task('sass', function () {

	let plugins = [
		autoprefixer({
			"browsers": [
				"> 0.5%",
				"not dead"
			]
		}),
		cleanCSS()
	];

	let plugins_prod = [
		autoprefixer({
			"browsers": [
				"> 0.5%",
				"not dead"
			]
		})
	];

	gulp.src(src.scss_base_file)
		.pipe(sass(
			{
				sourceComments: false,
				outputStyle: 'compressed' // nested, expanded, compact, compressed
			}).on('error', sass.logError))
		.pipe(postcss(plugins))
		.pipe(concat('styles.min.css'))
		.pipe(gulp.dest(src.scss_target))
	;

	return gulp.src(src.scss_base_file)
		.pipe(sourcemaps.init())
		.pipe(sass(
			{
				sourceComments: false,
				outputStyle: 'expanded' // nested, expanded, compact, compressed
			}).on('error', sass.logError))
		.pipe(sourcemaps.write({includeContent: false, sourceRoot: '../scss'}))
		.pipe(gulp.dest(src.scss_target))
		.pipe(livereload());
});


// FUNCIONS
function log(arg1, arg2) {
	console.log(
		colors.white.bgMagenta.bold(arg1),
		colors.white.bgBlue.bold(arg2)
	);
}
function log_sync(arg1, arg2) {
	console.log(
		colors.white.bgGreen.bold(arg1),
		colors.bgBlack(arg2)
	);
}
function log_init(arg1) {
	console.log(
		colors.white.bgBlue.bold(arg1)
	);
}
function log_error(arg1) {
	console.log(
		colors.white.bgRed.bold(arg1)
	);
}

// INICIA
gulp.task('default', ['watch', 'sass']);
