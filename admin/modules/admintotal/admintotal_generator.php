<?

/**
 * AdmintotalGenerator
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2011
 * @version $Id$
 * @access public
 */
class AdmintotalGenerator extends Module {
	var $logs_dir, $client_logs_dir, $xmls_dir, $is_server, $is_local_windows, $is_local_linux;

	function __construct() {
		parent::__construct();

		if ( $_SESSION['group_id'] != 1 ) {
			Main::redirect( '/' );
		}
	}

	function on_load() {
		$this->set_var( 'action', $this->action );
		$this->set_var( 'report', '' );
		$this->set_var( 'logs', '' );
		$this->set_var( 'xmls', '' );
		$this->set_var( 'xml', '' );
		$this->set_var( 'log_file', '' );
		$this->set_var( 'xml_file', '' );
		$this->set_var( 'is_local', $GLOBALS['gl_is_local'] );

		$this->is_server        = ! $GLOBALS['gl_is_local'];
		$this->is_local_linux   = ! $this->is_server && $GLOBALS['gl_is_linux'];
		$this->is_local_windows = ! $this->is_server && ! $GLOBALS['gl_is_linux'];


		if ($this->is_server ) {
			$this->logs_dir = '/var/www/letnd.com/datos/motor/data/logs/';
			$this->xmls_dir = '/var/www/letnd.com/datos/motor/data/apicat/';
		}
		else {
			$this->logs_dir = DOCUMENT_ROOT . 'data/logs/';
			$this->xmls_dir = DOCUMENT_ROOT . 'data/apicat/';
		}

		$this->client_logs_dir = LOGS_PATH . 'clients/';
	}

	function export() {
		$this->set_file( 'admintotal/portal_list.tpl' );

		$GLOBALS['gl_content'] = $this->process();
	}

	function export_apicat() {

		$this->_export_portal('apicat');

	}

	function export_ceigrup() {

		$this->_export_portal('ceigrup');

	}

	function export_fotocasa() {

		$this->_export_portal('fotocasa');

	}

	function export_habitaclia() {

		$this->_export_portal('habitaclia');

	}

	function import_apicat() {

		$this->_export_portal('apicat_import');

	}

	private function _export_portal($portal) {

		$portal_class = "Admintotal" . ucfirst($portal);
		$log_file = '';

		if ( $this->is_server ) {

			$log_file = "/var/www/letnd.com/datos/motor/data/logs/$portal.htm";

			include( DOCUMENT_ROOT . "admin/modules/admintotal/admintotal_$portal.php" );
			$exporter = New $portal_class;
			$exporter->_start();

		} elseif ( $this->is_local_linux ) {

			$log_file = "/letnd/data/logs/${portal}_linux.htm";
			exec( 'php -f "/letnd/admin/modules/admintotal/admintotal_' . $portal . '_cli.php" > ' . $log_file );

		} elseif ( $this->is_local_windows ) {

			$log_file = "d:\letnd\data\logs\\${portal}_windows.htm";
			exec( 'C:\server\php5\php.exe -f "D:\Letnd\admin\modules\admintotal\admintotal_' . $portal . '_cli.php" > ' . $log_file );
		}

		if (!$this->is_server) echo file_get_contents( $log_file );
		html_end();
	}

	function export_mls() {

		$log_file = '';

		if ( $this->is_server ) {

			$log_file = '/var/www/letnd.com/datos/motor/data/logs/mls.htm';

			include( DOCUMENT_ROOT . 'admin/modules/admintotal/admintotal_mls.php' );
			$mls = New AdmintotalMls;
			$mls->start();

		} elseif ( $this->is_local_linux ) {

			$log_file = '/letnd/data/logs/mls_linux.htm';
			exec( 'php -f "/letnd/admin/modules/admintotal/admintotal_mls_cli.php" > ' . $log_file );
		} elseif ( $this->is_local_windows ) {

			$log_file = 'd:\letnd\data\logs\mls_windows.htm';
			exec( 'C:\server\php5\php.exe -f "D:\Letnd\admin\modules\admintotal\admintotal_mls_cli.php" > ' . $log_file );
		}

		if ($log_file) echo file_get_contents( $log_file );
		die();

	}

	function list_logs() {
		$this->set_file( 'admintotal/portal_list.tpl' );


		if ( R::escape( 'log' ) ) {
			$log_file = $this->logs_dir . R::escape( 'log' );
			$this->set_var( 'log_file', $log_file );
			$this->set_var( 'report', file_get_contents( $log_file ) );
		}

		if ( R::escape( 'xml' ) ) {
			$xml_file = $this->xmls_dir . R::escape( 'xml' );
			$this->set_var( 'xml_file', $xml_file );
			$this->set_var( 'xml', file_get_contents( $xml_file ) );
		}


		$logs = array_diff( scandir( $this->logs_dir ), array( '..', '.', 'clients' ) );

		$this->set_loop( 'logs', $logs );


		$client_logs = array_diff( scandir( $this->client_logs_dir ), array( '..', '.' ) );

		$this->set_loop( 'client_logs', $client_logs );

		$xmls     = array_diff( scandir( $this->xmls_dir ), array( '..', '.' ) );
		$new_xmls = array();
		foreach ( $xmls as $xml ) {
			if ( substr( $xml, - 3 ) == 'xml' ) {
				$new_xmls [] = $xml;
			}
		}

		$this->set_loop( 'xmls', $new_xmls );


		$GLOBALS['gl_content'] = $this->process();

	}
}