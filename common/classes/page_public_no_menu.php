<?php
class Page
{
	var $tpl;
	var $title;
	var $message;
	var $content;
	var $user_name;
	var $user_surname;
	var $template;

	function __construct()
	{
		global $gl_tool_top_variable, $gl_variable;
		$this->template = 'general.tpl';
		$this->set_page_vars();
		if ((defined($gl_tool_top_variable))&&constant($gl_variable)) {
		    $this->title = constant($gl_tool_top_variable) . " - " . constant($gl_variable);
		}
	}

	function load_template()
	{
		$this->tpl = new phemplate(PATH_TEMPLATES, 'remove', TPL_LOOP | TPL_OPTIONAL | TPL_BLOCK);
		$this->tpl->set_file($this->template); // this will read file contents into handle 'main'
	}

	function set_page_vars()
	{
		global
		$gl_tool,
		$gl_tool_top_id,
		$gl_tool_top_variable,
		$gl_action,
		$gl_process,
		$gl_parent_id,
		$gl_variable,
		$gl_menu_id,
		$gl_action_menu;

		$gl_tool = '?';
		$gl_parent_id = '?';
		$gl_variable = '?';

		// accio per defecte
		if (!$gl_action) $gl_action = 'list_records';
		$gl_action_menu = $action;

		$gl_tool_top_variable = '?';
	}
	// Assignar totes les variables sencilles de la pàgina, titol, menus fixes, etc...
	function assign_variables()
	{
		global $gl_language;
		// revisar , fan falta les constants?
		$this->tpl->set_var('LANGUAGE', $gl_language);
		$this->tpl->set_var('CLIENT_DIR', CLIENT_DIR);
    	$this->tpl->set_var('PAGE_TITLE',PAGE_TITLE);
		$this->tpl->set_var('title', $this->title);
		$this->tpl->set_var('message', $this->message);
		$this->tpl->set_var('content', $this->content);
		$this->tpl->set_var('date', date('j-n-Y', mktime()));
	}
	// Mostrar la pagina un cop acabat
	function show()
	{
		if ($this->template)
		{
			$this->load_template();

			$this->assign_variables();
			echo $this->tpl->process();
		}
		else
		{
			echo $this->content;
		}
	}
}

?>