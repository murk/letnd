<script language="JavaScript" src="/admin/jscripts/validacio.js?v=<?=$version?>"></script>
<script language="JavaScript" src="/admin/jscripts/euro_calc.js?v=<?=$version?>"></script>
<script language="JavaScript" src="/common/jscripts/jquery.dropshadow.js?v=<?=$version?>"></script>
<script type="text/javascript">
$(function() {
	set_focus(document.theForm);
	var id = <?=$id?>;
	$('#price_'+id+',#reserva_'+id+',#contracte_'+id+',#monthly_'+id+',#claus_'+id).blur(function(){get_hipoteca()});	
	$('#theForm').submit(function(){get_hipoteca()});	
	get_hipoteca();
});
function get_hipoteca(){
	var id = <?=$id?>;
	
	var data_ajax =	{
		action : 'get_hipoteca_ajax',
		menu_id : '2203',
		habitatge_id : id,
		price : $('#price_'+id).val(),
		reserva : $('#reserva_'+id).val(),
		contracte : $('#contracte_'+id).val(),
		monthly : $('#monthly_'+id).val(),
		claus : $('#claus_'+id).val()
	};
	var ajax_config = {
		async:true,
		type: "GET",
		dataType: "html",
		url:"/admin/",
		data:data_ajax,
		timeout:4000
	};
		 
	ajax_config.success = function (dades){hipoteca_on_success(dades);}
	$.ajax(ajax_config);	
}
function hipoteca_on_success(dades){
	$('#hipoteca').html(dades);
}
</script>
<form name="form_delete" method="post" action="<?=$link_action_delete?>" target="save_frame">
  <input name="<?=$id_field?>" type="hidden" value="<?=$id?>">
</form>
<form id="theForm" name="theForm" method="post" action="<?=$link_action?>" onsubmit="<?=$js_string?>" encType="multipart/form-data" target="save_frame">
  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="form">
    <tr>
      <td><?if ($form_new):?><?if ($save_button):?>
          <div id="form_buttons_holder"><table id ="form_buttons">
            <tr>
              <td class="buttons_top"><div class="right"><input type="submit" name="Submit" value="<?=$c_send_new?>" class="btn_form1" /></div></td>
            </tr>
          </table></div>
          <?endif //save_button?>
          <?endif //form_new?>
        <?if ($form_edit):?>               
          <div id="form_buttons_holder"><table id ="form_buttons">
            <tr>
              <td valign="bottom"><div class="menu"><div><?=$c_edit_data?></div><?if ($has_images):?>
            	<a href="<?=$image_link?>"><?=$c_edit_images?></a>
         <?endif //has_images?><? foreach($form_tabs as $r): extract ($r)?><a href="?action=<?=$tab_action?>&<?=$id_field?>=<?=$id?>&menu_id=<?=$menu_id?>"><?=$tab_caption?></a><? endforeach //$loop?></div></td>
              <td class="buttons_top"><div class="right">
										<? if($show_previous_link): ?><a class="boto2 btn_show_previous_link"
										                          href="<?= $show_previous_link ?>"></a><? endif //show_previous_link?>
										<? if($show_next_link): ?><a class="boto2 btn_show_next_link"
										                          href="<?= $show_next_link ?>"></a><? endif //show_next_link?>
               <? if ($back_button):?><a class="btn_form_back" href="<?=$back_button?>"></a><? endif //back_button?>
            <? if ($save_button):?><input type="submit" name="Submit" value="<?=$c_send_edit?>" class="btn_form1"><? endif //save_button?><? if ($delete_button):?><a class="btn_form_delete" href="javascript:submit_form('<?if ($has_bin):?><?=$c_confirm_form_bin?><?else:?><?=$c_confirm_form_deleted?><?endif //has_bin?>', true);">
                <?=$c_delete?>
                </a><? endif //delete_button?><? if ($preview_button):?><a class="btn_form1" href="<?=$preview_link?>" target="_blank">
                <?=$c_preview?>
                </a><? endif //preview_button?><? if ($print_button):?><a class="btn_form1 btn_print" href="<?=$print_link?>">
                <?=$c_print?>
                </a> <? endif //print_button?></div></td>
            </tr>
          </table></div>
          <?endif //form_edit?>
        <table width="100%" cellpadding="0" cellspacing="0" >
          <tr>
            <td class="formCaptionTitle"><strong>
              <?=$c_edit_title?>
              &nbsp;&nbsp;</strong></td>
          </tr>
        </table>
        <table width="100%" cellpadding="0" cellspacing="0" class="formstbl">
          <tr>
            <td valign="top" class="formsCaption" width="150"><strong>
              <?=$c_flat?>
              :</strong>
              <input name="habitatge_id[<?=$id?>]" type="hidden" value="<?=$id?>" />
              <input name="habitatge_id_javascript" type="hidden" value="<?=$id?>" /></td>
            <td class="forms"><? if ($is_flat_editable):?><table border="0" cellspacing="0" cellpadding="0">
              
			  <tr>
                <td><?=$c_escala?></td>
                <td><?=$c_floor_id?></td>
                <td><?=$c_flat?></td>
              </tr>
              <tr>
                <td><?=$escala?>
                  &nbsp;&nbsp;</td>
                <td><?=$floor_id?>
                  &nbsp;&nbsp;</td>
                <td><?=$flat?></td>
              </tr>
            </table>
			<?else: //is_flat_editable?>
			<?=$c_escala?> <?=$escala?> <?=$floor_id?> <?=$flat?> <a style="padding-left:40px;" href="<?=$data_link?>&process=edit_flat">editar pis</a>
			  <?endif //is_flat_editable?></td>
          </tr>
          <tr>
            <td valign="top" class="formsCaption"><strong>
              <?=$c_room?>
              :</strong></td>
            <td class="forms"><?=$room?></td>
          </tr>
            <tr>
              <td valign="top" class="formsCaption"><strong>
                <?=$c_m2?>
                :</strong></td>
              <td class="forms"><?=$m2?></td>
            </tr>
            <tr>
              <td valign="top" class="formsCaption"><strong>
                <?=$c_m2_exterior?>
                :</strong></td>
              <td class="forms"><?=$m2_exterior?></td>
            </tr>
            <tr>
              <td valign="top" class="formsCaption"><strong>
                <?=$c_price?>
                :</strong></td>
              <td class="forms"><?=$price?></td>
            </tr>
          <tr>
            <td valign="top" class="formsCaption"><strong>
              <?=$c_status?>
              :</strong></td>
            <td class="forms"><?=$status?></td>
          </tr>
          <tr>
            <td valign="top" class="formsCaption"><strong>
              <?=$c_file?>
              :</strong></td>
            <td class="forms"><?=$file?></td>
          </tr>
        </table>
<div class="sep_dark"></div>
        <table width="100%" cellpadding="4" cellspacing="0">
          <tr>
            <td class="formCaptionTitle"><strong>
              <?=$c_edit_title1?>
              </strong></td>
          </tr>
        </table>
        <table width="100%" cellpadding="0" cellspacing="0" class="formstbl">
          <tr>
            <td valign="top" class="formsCaption" width="150"><strong>
              <?=$c_reserva?>
              :</strong></td>
            <td class="forms"><?=$reserva?></td>
          </tr>
          <tr>
            <td valign="top" class="formsCaption"><strong>
              <?=$c_contracte?>
              :</strong></td>
            <td class="forms"><?=$contracte?></td>
          </tr>
          <tr>
            <td valign="top" class="formsCaption"><strong>
              <?=$c_monthly?>
              :</strong></td>
            <td class="forms"><?=$monthly?></td>
          </tr>
          <tr>
            <td valign="top" class="formsCaption"><strong>
              <?=$c_claus?>
              :</strong></td>
            <td class="forms"><?=$claus?></td>
          </tr>
          <tr>
            <td valign="top" class="formsCaption"><strong>
              <?=$c_quota_subsidiada?>
              :</strong></td>
            <td class="forms"><?=$quota_subsidiada?> - <?=$quota_subsidiada2?></td>
          </tr>
        </table>
        <?if ($form_new):?>
          <table width="100%" cellpadding="4" cellspacing="2" class="formsButtonstbl">
            <tr>
              <td align="center" class="formsButtons"><input type="submit" name="Submit" value="<?=$c_send_new?>" class="btn_form1"></td>
            </tr>
          </table>
          <?endif //form_new?>
        <?if ($form_edit):?>
		<div class="sep_dark"></div>
        <table width="100%" cellpadding="4" cellspacing="0">
          <tr>
            <td class="formCaptionTitle"><strong>
              <?=$c_edit_title2?>
              </strong></td>
          </tr>
        </table>
        <table width="100%" cellpadding="0" cellspacing="0" class="formstbl">
          <tr>
            <td class="formsCaptionHor2" id="hipoteca"></td>
          </tr>
        </table>
          <table width="100%" cellpadding="4" cellspacing="2" class="formsButtonstbl">
            <tr>
              <td align="center" class="formsButtons"><div class="centerFloat1">
                  <div class="centerFloat2">
                    <input type="submit" name="Submit" value="<?=$c_send_edit?>" class="btn_form1">
                  </div>
                </div></td>
            </tr>
          </table>
          <?endif //form_edit?></td>
    </tr>
  </table>
</form>
