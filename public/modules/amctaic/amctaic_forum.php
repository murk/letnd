<?
/**
 * AmctaicForum
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2010
 * @version $Id$
 * @access public
 */
class AmctaicForum extends Module{

	function __construct(){
		parent::__construct();
	}
	function on_load(){	
		$this->set_var('is_logged',$GLOBALS['is_logged']);
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function get_records()
	{
	    $listing = new ListRecords($this);
		$listing->condition = "status = 'public' AND tipus='".$this->process."'";
		$listing->paginate = false;
		$listing->set_var('is_forum',$this->process=='forum');
		$listing->set_var('is_viatge',$this->process=='viatge');
		$listing->set_var('is_sortida',$this->process=='sortida');
		$listing->set_var('is_noticia',$this->process=='noticia');
		$listing->always_parse_template=true;
		$listing->call('records_walk','url1,url1_name');
		if ($this->process!='forum'){
			$this->set_field('content','list_public','no');
			$this->set_var('content','');// així no dona undefined al llistat
		}
		//$listing->call_function['content'] = 'add_dots';
	    return $listing->list_records();
	}
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{
	    $show = new ShowForm($this);
		$show->set_var('is_forum',$this->process=='forum');
		$show->set_var('is_viatge',$this->process=='viatge');
		$show->set_var('is_sortida',$this->process=='sortida');
		$show->set_var('is_noticia',$this->process=='noticia');
		$show->call('records_walk','url1,url1_name');
		$show->get_values();		
		if ($show->has_results)
		{
			return $show->show_form();
		}
		else
		{
			$this->do_action('list_records');
		}
	}
	function get_videoframe(){
		if ($GLOBALS['is_logged']){
			if (!isset($_GET['forum_id'])) die();
			$id = R::id('forum_id');
			echo ('
			<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
			<html xmlns="http://www.w3.org/1999/xhtml">
			<head>
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			<style type="text/css">
			body {
				margin-left: 0px;
				margin-top: 0px;
				margin-right: 0px;
				margin-bottom: 0px;
			}
			</style>
			</head>
			<body>'.			
			Db::get_first("SELECT videoframe FROM amctaic__forum WHERE forum_id = '" . $id . "' AND status='public' AND bin=0").'
			</body>
			</html>');					
			die();
		}
		else{
			die();
		}
	}
	function records_walk($url1,$url1_name){		
		// Això ho poso aquí així no cal posar-ho al tpl, simplement posar: <a href="<=$url1>" target="_blank"><=$url1_name></a>
		$ret['url1_name'] = $url1_name?$url1_name:$url1;		
		$ret['url1']=str_replace('http://','',$url1);
		return $ret;
	}

	function save_rows()
	{
	    $save_rows = new SaveRows($this);
	    $save_rows->save();
	}

	function write_record()
	{
	    $writerec = new SaveRows($this);
	    $writerec->save();
	}

	function manage_images()
	{
	    $image_manager = new ImageManager($this);
	    $image_manager->execute();
	}
	function get_butlleti()
    {
        if ($GLOBALS['is_logged'])
        {
			$f = $_GET["file_name"];
			$fp=fopen('clients/amctaic/uploads/files/' . $f, "r");
			if (!$fp) {
				print_javascript("alert ('Aquest fitxer no existeix');");
				die();
			}
			header("Content-type: application/octet-stream");
            header("Content-disposition: attachment; filename=" . $f . "");
            header("Pragma: public");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Cache-Control: private", false);
			
			fpassthru($fp);
            die();
        }
        else
        {
			print_javascript("alert ('Necessites ser un usuari registrat per descarregar el butlletí');");
            die();
        }
    }
	function login(){
		if (($_POST['login'] == 'amctaic') && ($_POST['password'] == 'associaciomct')){
			$_SESSION['is_logged'] = true;
			print_javascript('top.window.setTimeout("window.location.replace(window.location);", 800);');
			$GLOBALS['gl_page']->show_message('Benvingut a AMCTAIC');
		}
		else{
			$GLOBALS['gl_page']->show_message('Login o password incorrectes');
		}
		Debug::p_all();
		die();
	}
	function logout(){
		unset($_SESSION['is_logged']);
		print_javascript('top.window.setTimeout("window.location.replace(window.location);", 800);');
		$GLOBALS['gl_page']->show_message('Està desconectat!');
		Debug::p_all();
		die();
	}
}
?>