<?php

if (!defined('TITLE_COMARCA')){
	define('TITLE_COMARCA', 'Недвижимость: ');
	define('TITLE_PRICE', 'Недвижимость стоимостью до %s '.CURRENCY_NAME);
	define('TITLE_PRICE_2', 'Недвижимость стоимостью от %s до %s '.CURRENCY_NAME);
	define('TITLE_PRICE_3', 'Недвижимость стоимостью более %s '.CURRENCY_NAME);

	define('TITLE_FLOORSPACE', 'Недвижимость площадью до %s m<sup>2</sup>');
	define('TITLE_FLOORSPACE_2', 'Недвижимость площадью от %s  и до %s m<sup>2</sup>');
	define('TITLE_FLOORSPACE_3', 'Недвижимость площадью более %s m<sup>2</sup>');

	define('TITLE_PERSON', 'Up to %s people');
	define('TITLE_PERSON_2', 'Between %s and %s people');
	define('TITLE_PERSON_3', 'More than %s people');
}

$gl_caption['c_filter_price_min'] = 'Цена максимальная';
$gl_caption['c_filter_price_max'] = 'Цена минимальная';
$gl_caption['c_filter_price_min_temp'] = 'Цена максимальная / ночь';
$gl_caption['c_filter_price_max_temp'] = 'Цена минимальная / ночь';

$gl_caption['c_recent'] = 'New';

// NO TRADUIT
$gl_caption['c_property_list__view_all'] = 'Показать список  просмотренного жилья';
$gl_caption['c_characteristics'] = 'Característiques';
// FI NO TRADUIT

$gl_caption['c_booking_close_results_message'] = 'There are no properties available on the given dates, the following properties can be booked on similar dates';
?>