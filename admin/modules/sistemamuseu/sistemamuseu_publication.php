<?
/**
 * SistemamuseuPublication
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2010
 * @version $Id$
 * @access public
 */
class SistemamuseuPublication extends Module{
	var $group_id;
	var $is_museu;
	function __construct(){
		parent::__construct();
		$this->parent = 'publication';
		$this->group_id = $_SESSION['group_id'];
		$this->is_museu = $this->group_id>3;
	}
	/* funció que es crida per fer una acció del modul automatica o desde un altre modul ( list_records, show_form ) etc...*/
	function do_action($action = ''){
		switch ($this->parent){		
			case 'publication':			
			//$this->unset_field ('in_home');	
			$this->unset_field ('file');	
			break;	
			case 'expert':			
			;
			break;
		}
		$this->set_field ('parent_section','override_save_value',"'" . $this->parent . "'");
		if ($this->is_museu){
			$this->set_field('museu_id','override_save_value',$this->group_id);
		}
		return parent::do_action($action);
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function get_records()
	{		
		$this->swap_fields('museu_id','title');
	    $listing = new ListRecords($this);
		$listing->add_filter('status');
		$listing->add_filter('publicationkind_id','sistemamuseu__publication_to_publicationkind');
		$listing->condition = "parent_section = '" . $this->parent . "'";
		if ($this->is_museu) {
			$listing->set_field('museu_id','list_admin','no');
			$listing->set_field('destacat','list_admin','no');
			$listing->set_field('ordre','list_admin','no');
			$listing->condition  .= 'AND museu_id = ' . $this->group_id;
		}
		else{			
			$listing->add_filter('museu_id');
		}
		$listing->order_by = 'title ASC';
	    return $listing->list_records();
	}
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{
	    $show = new ShowForm($this);
		if ($this->is_museu){
			$show->set_field('museu_id','type','hidden');
			$show->set_field('destacat','type','hidden');
			$show->set_field('ordre','type','hidden');
		}
	    return $show->show_form();
	}

	function save_rows()
	{
	    if ($this->action!='add_record') $this->unset_field('parent_section'); // m'asseguro que no es pugui canviar mai de expert a publicacio
		$save_rows = new SaveRows($this);
	    $save_rows->save();
	}

	function write_record()
	{
	    $writerec = new SaveRows($this);
	    $writerec->save();
	}

	function manage_images()
	{
	    $image_manager = new ImageManager($this);
	    $image_manager->execute();
	}
}
?>