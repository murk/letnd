<?

/* 12-9-16

	PRODUCT

	UP-DATE
	- Afegir camp per descartar paisos

 */

if ( is_table( 'all__country' ) && ! is_field( 'all__country', 'can_deliver' ) ) {

	$update_sql[] = "ALTER TABLE `all__country`
	ADD COLUMN `can_deliver` TINYINT(1) NULL DEFAULT '1' AFTER `european_comunity`;";

} else {
	if ( is_table( 'all__country' ) ) {
		trigger_error( 'Ja existeixen el camp can_deliver' );
	}
}


?>