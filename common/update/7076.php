<?
/*
30-03-2019

*/

if ( is_table( 'product__family_language' ) && ! is_field( 'product__family_language', 'family_subtitle' ) ) {

	$update_sql[] = "ALTER TABLE `product__family_language`
	ADD COLUMN `family_subtitle` VARCHAR(255) NOT NULL DEFAULT '' AFTER `family_alt`;
";

}
else {
	if ( is_table( 'product__family_language' ) ) {
		trigger_error( 'Ja existeixen el camp family_subtitle' );
	}
}