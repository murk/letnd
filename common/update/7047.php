<?php
/* 15-5-18
Productes, imatges de variacions

*/

if ( is_table( 'product__product_image' ) && ! is_field( 'product__product_image', 'product_variation_id' ) ) {

	$update_sql[] = "ALTER TABLE `product__product_image`
	ADD COLUMN `product_variation_id` INT(11) NOT NULL AFTER `product_id`,
	ADD INDEX `product_variation_id` (`product_variation_id`);
";

}
else {
	if ( is_table( 'product__product_image' ) ) {
		trigger_error( 'Ja existeixen el camp product_variation_id' );
	}
}