<?php
/* 21-6-17
Inmo, actualització camp observacions booking

*/
if ( is_table( 'booking__book' ) && ! is_field( 'booking__book', 'comment_private' ) ) {

	$update_sql[] = "ALTER TABLE `booking__book`
	ADD COLUMN `comment_private` TEXT NOT NULL AFTER `comment`;
";

} else {
	if ( is_table( 'booking__book' ) ) {
		trigger_error( 'Ja existeixen el camp comment_private' );
	}
}

?>