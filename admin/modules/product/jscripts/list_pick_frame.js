
Assign.pass_selected_items = function(){
		
	var ids = '';
    for (key in addreses_selected)
    {
      ids+= key +',';
    }
    ids = ids.substr(0,ids.length-1);
	
	var ajax_config =
		{
           async:true,
           type: "GET",
           dataType: "html",
           url:"/admin/",
           data:"menu_id="+top.ListPick.menu_id+"&action=show_new_variations&ids="+ids,
           timeout:4000
         };
		 
	ajax_config.success = function (dades){
		top.hidePopWin(false);
		
		var html = top.$('#new_variations_content').html() + dades;
		top.$('#new_variations_content').html(html);
		top.$('#new_variations').slideDown(400);
	}
	
	$.ajax(ajax_config);		
};

Assign.show_category_list = function(obj){
	var val = obj.value;
	if (val=='null') return;
	
	$('#listItems').attr('src','/admin/?action=list_records&process=select_variation&menu_id=20062&variation_category_id=' + val);
}