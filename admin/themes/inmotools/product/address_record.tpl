<strong>
	<?= $a_name ?> <?= $a_surname ?>
</strong><br>
<?= $address ?>
<br>
<?= $city ?>
<?= $zip ?>
<br>
<? if ( $provincia_id ): ?>
	<?= $provincia_id ?><br/>
<? endif ?>
<?= strtoupper( $country_id ) ?>

<? if ( $dni ): ?>
	<br/>
	<br/>
	<strong><?= $c_dni ?></strong>: <?= $dni ?>
<? endif // $dni?>

<? if ( $telephone ): ?>
	<br/>
	<strong><?= $c_telephone ?></strong>: <?= $telephone ?>
<? endif // $telephone ?>

<? if ( $telephone_mobile ): ?>
	-
	<?= $telephone_mobile ?>
<? endif //$telephone_mobile?>

<? if ( $mail ): ?>
	<br/>
	<strong><?= $c_mail ?></strong>: <a href="mailto:<?= $mail ?>"><?= $mail ?></a>
<? endif //$mail?>