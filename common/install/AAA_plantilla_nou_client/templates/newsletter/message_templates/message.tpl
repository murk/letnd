<? /*

TEST -

1-Per Testejar, posar el css així i comentar l'include('styles.css'):
<link rel="stylesheet" href="<?=DIR_TEMPLATES_PUBLIC?>newsletter/message_templates/styles.css">

2 - Entrar a la URL:
/admin/?action=show_preview&menu_id=5&test=true&message_id=1&template_id=1&message=Cos+del+missatge


  */ ?>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<base href="<?= $url ?>">
	<?include('styles.tpl')?>
</head>
<body>

<table id="letnd-table-content">
	<tr>
		<td id="letnd-td-content" align="center">

			<? include( 'header.tpl' ) ?>

			<? if ( $message || $pictures ): ?>
				<table id="letnd-content" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td valign="top" class="letnd-content <? if ( $pictures ): ?>letnd-content-left<? endif //pictures?>">
							<?= $message ?>
						</td>
						<? if ( $pictures ): ?>
							<td valign="top" align="right" class="letnd-picture-vertical-holder">
								<table>
									<? foreach ( $pictures as $picture ): extract( $picture ) ?>
										<tr>
											<td class="letnd-picture-vertical">
												<img width="284" src="<?= $picture_src ?>">
											</td>
										</tr>
									<? endforeach //$images?>
								</table>
							</td>
						<? endif //pictures?>
					</tr>
				</table>
			<? endif //message?>

			<? if ( $content_extra ): ?>
				<table id="letnd-content_extra" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td>
							<?= $content_extra ?>
						</td>
					</tr>
				</table>
			<? endif //content_extra?>

			<? include( 'footer.tpl' ) ?>

		</td>
	</tr>
</table>

</body>
</html>