<?
/**
 * SaharaCircuit
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 30/11/2013
 * @version $Id$
 * @access public
 */
class SaharaCircuit extends Module{
	var $is_dies = false;
	function __construct(){
		parent::__construct();
	}
	function on_load(){
		$this->form_tabs = array(
			'0' => array('tab_action'=>'show_form_dies','tab_caption'=>$this->caption['c_edit_dia']),
			'1' => array('tab_action'=>'show_form_map','tab_caption'=>$this->caption['c_edit_map'])
			);
	
		parent::on_load();		
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function get_records()
	{
	    $listing = new ListRecords($this);
		$listing->call('records_walk','content,content_list', true);
		
		
		if (isset($_GET['selected_menu_id']))
		{
			$listing->condition = 'place_id = ' . $_GET['place_id'];
		}
		else
		{
		$listing->add_filter('place_id');
		}
		
		$listing->add_filter('status');		
		$listing->add_filter('in_home');
		
		$listing->group_fields = array('place');
		
		
		$listing->add_button ('edit_map', 'action=show_form_map','boto2','after');
		$listing->add_button ('edit_dies', 'action=show_form_dies','boto2','after');
		
		
		if (isset($_GET['in_home']) && $_GET['in_home']!='null') {
			$listing->set_field('in_home','list_admin','input');
		}
		
		$listing->order_by = 'place ASC, ordre ASC, circuit ASC, entered DESC';		
		
	    return $listing->list_records();
	}
	function records_walk(&$listing, $content=false,$content_list=false){		
		
		$ret = array();
		
		// si es llistat passa la variable content
		if ($content!==false){
			$ret['content']=$content_list?$content_list:add_dots('content', $content);
		}
		return $ret;
	}
	
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{
	    // si no hi ha cap categoria faig un redirect a entrar categoria nova
		if (!Db::get_first("SELECT count(*) FROM sahara__place LIMIT 0,1")){
			$_SESSION['message'] = $this->messages['no_place'];
			redirect('/admin/?menu_id=2206');
		}	
		
		
		if (!$this->is_dies){
			$this->set_field('entered','default_value',now(true));
			$this->set_field('modified','default_value',now(true));
		}
		
		if ($this->config['videoframe_info_size']) $this->caption['c_videoframe'] = $this->caption['c_videoframe'] . '<br /> ( ' . $this->config['videoframe_info_size'] . ' )';
		
	    $show = new ShowForm($this);		
		$show->form_tabs = $this->form_tabs;
		
		if ($this->is_dies){
			$show->set_options(1, 0, 0, 0);
		}
		else{
			$show->set_form_level1_titles('circuit','url1_name','page_title');		
			//$show->set_form_level2_titles('circuit','url1_name');	
		}
		
	    return $show->show_form();
	}

	function save_rows()
	{
	    $save_rows = new SaveRows($this);
	    $save_rows->save();
	}

	function write_record()
	{
		Debug::p($this->process, 'text');
		if ($this->process == 'form_dies')			
			$this->set_config('sahara/sahara_circuit_days_config.php');
		
	    $writerec = new SaveRows($this);
		$writerec->set_field('modified','override_save_value','now()');			
		$writerec->field_unique = "circuit_file_name";
	    $writerec->save();
	}

	function manage_images()
	{
	    $image_manager = new ImageManager($this);
		$image_manager->form_tabs = $this->form_tabs;
	    $image_manager->execute();
	}
	
	function show_form_dies(){
		
		$circuit_id = R::id('circuit_id');	
		
		// obtinc llistat dies			
		$module = Module::load('sahara','dia', $this);
		$module->circuit_id = $circuit_id;
		$dies = $module->do_action('get_records');
		
		

		$query = "SELECT circuit_id, circuit FROM sahara__circuit_language
			WHERE circuit_id = " . $circuit_id . "
			AND language = '" . LANGUAGE . "'";

		$rs = Db::get_row($query);
		
		if ($rs)
		{
			$this->set_vars($rs);
			$this->set_vars($this->caption);
			$this->set_var('content', $dies);
			$this->set_var('menu_id', $GLOBALS['gl_menu_id']);
			$this->set_file('sahara/circuit_dia.tpl');

			$GLOBALS['gl_content'] = $this->process();
		}
		else{
			$GLOBALS['gl_message'] = '';
		}
	}
	
	
	
	function show_form_map (){ //esenyo el napa
		global $gl_news;
						
		$circuit_id = R::id('circuit_id');
		
		$rs = Db::get_row('SELECT * FROM sahara__circuit WHERE circuit_id = ' . $circuit_id);
		//$rs['municipi']=$this->caption['c_municipi_'.$rs['municipi']];
		$rs['title'] = Db::get_first('SELECT circuit FROM sahara__circuit_language  WHERE circuit_id = ' . $circuit_id . ' AND language = LANGUAGE');
		//$rs['address'] = Db::get_first('SELECT address FROM sahara__circuit_language  WHERE circuit_id = ' . $circuit_id . ' AND language = LANGUAGE');
		$rs['is_default_point'] = ($rs['latitude']!=0)?0:1;
		$rs['latitude1']= substr($rs['latitude'],0,strpos($rs['latitude'],';'));
		$rs['longitude1']=substr($rs['longitude'],0,strpos($rs['longitude'],';'));
		$rs['center_latitude']=$rs['center_latitude']!=0?$rs['center_latitude']:32.1196385;
		$rs['center_longitude']=$rs['center_longitude']!=0?$rs['center_longitude']:-4.0160795;
		$rs['zoom']=$rs['zoom']?$rs['zoom']:6;
		$rs['menu_id'] =  $_GET['menu_id'];		
		$rs['id'] =  $rs[$this->id_field];		
		$rs['id_field'] =  $this->id_field;		
		$this->set_vars($rs);//passo els resultats del array
		$this->set_file('sahara/circuit_map.tpl'); //crido el arxiu
		$this->set_vars($this->caption); //passo tots els captions
		//$gl_news = $this->caption['c_click_map'];
		$content = $this->process();//processo
		$GLOBALS['gl_content'] = $content;		
	}
	function save_record_map(){
		global $gl_page;
		
		R::escape_array($_POST);
		extract($_POST);

		if ( $latitude && $longitude ) {
		Db::execute ("UPDATE `sahara__circuit` SET
			`latitude` = '$latitude',
			`longitude` = '$longitude',
			`center_latitude` = '$center_latitude',
			`center_longitude` = '$center_longitude',
			`zoom` = '$zoom'
			WHERE `circuit_id` =$circuit_id
			LIMIT 1") ;
			$ret ['message'] = $this->messages['point_saved'];
		}
		else {
			$ret ['message'] = $this->messages['point_not_saved'];
		}

		Debug::p_all();
		die (json_encode($ret));
		/*
		$gl_page->show_message($this->messages['point_saved']);
		Debug::p_all();
		die();				*/
	}
	function save_rows_map_points(){
		global $gl_page;
		$circuit_id = R::id('circuit_id');
		$points = trim($_POST['points']);
		$points = explode(';',$points);
		
		$latitudes = array();
		$longitudes = array();
		$error = false;
		
		foreach ($points as $point){
			if ($point){
				$p = explode(',',trim($point));
				
				$la =trim($p[0]);
				$lo = trim($p[1]);
				
				if (is_numeric($la) && is_numeric($lo) 
					&& $la!='' && $lo!=''
					&& count($p)==2) {
				$latitudes[] =$la;
				$longitudes[] = $lo;
				}
				else{
					$error=true;
					break;
				}
			}
		}
		if ($error){
			$this->map_points_error();		
		}
		
		$center_latitude = $latitudes[0]; // centro en el primer punt
		$center_longitude = $longitudes[0]; // centro en el primer punt
		
		$latitudes = implode (';',$latitudes);
		$longitudes = implode (';',$longitudes);
		
		debug::p($latitudes);
		debug::p($longitudes);		
		
		if ($latitudes && $longitudes){	
			Db::execute ("UPDATE `sahara__circuit` SET
				`latitude` = '$latitudes',
				`longitude` = '$longitudes',
				`center_latitude` = '$center_latitude',
				`center_longitude` = '$center_longitude'
				WHERE `circuit_id` =$circuit_id
				LIMIT 1") ;
				
			//$gl_page->show_message($this->messages['point_saved']);
			$_SESSION['message'] = $this->messages['point_saved'];
			Debug::p_all();
			print_javascript('parent.window.location.replace(parent.window.location);');
			die();	
		}
		else{
			$this->map_points_error();
		}
	}
	function map_points_error(){	
		global $gl_page;
		$gl_page->show_message('Hi ha un error en l\'entrada de coordenades, si us plau comproveu que estigui ben formada');
		Debug::p_all();
		die();
	}
}
?>