<?php
// no es poden posar ; entre mig de la consulta ja que parteix les consultes per el ; a no ser que fem servir $update_sql en array()
if (is_table("product__product") && !is_field('product__product','discount_wholesaler')){		
	// Botiga privada o no, privada es necessari entrar login i pass, sino no es veu res
	$update_sql.="ALTER TABLE `product__product`  ADD COLUMN `discount_wholesaler` TINYINT(2) NOT NULL DEFAULT '0' AFTER `discount`,  ADD COLUMN `discount_fixed_price_wholesaler` DECIMAL(11,2) NOT NULL AFTER `discount_fixed_price`;";
}	

if (is_table("product__customer") && !is_field('product__customer','activated')){		
	// Botiga privada o no, privada es necessari entrar login i pass, sino no es veu res
	$update_sql.="
	ALTER TABLE `product__customer`  ADD COLUMN `activated` TINYINT(1) NOT NULL DEFAULT '0' AFTER `is_wholesaler`;
	UPDATE `product__customer` SET `activated`=1;";
}				
?>