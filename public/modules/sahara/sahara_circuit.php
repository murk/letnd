<?
/**
 * SaharaCircuit
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 30/11/2013
 * @version $Id$
 * @access public
 */
class SaharaCircuit extends Module{
	function __construct(){
		parent::__construct();
	}
	function on_load(){
		
		$this->set_friendly_params(array(
			'place_id'=>R::text_id('place_id')	
			));
		parent::on_load();
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function get_records()
	{
		
		$place_id =  R::id('place_id');
		$this->set_title();
	   
		
		$listing = new ListRecords($this);
		$listing->call('records_walk','content,content_list', true);
		
		$listing->condition .= "status = 'public'";
		
		$place_id = R::id('place_id');
		
		$place_description = '';
		
		if ($place_id)
		{
			$listing->condition .= ' AND place_id = ' . $place_id;
			$place_description = Db::get_first("
				SELECT place_description 
				FROM sahara__place_language 
				WHERE place_id = " . $place_id ."
				AND language = '" . LANGUAGE . "'");
		}
			
		$listing->set_var('place_description', $place_description);
			
		$listing->order_by = 'ordre ASC, circuit ASC, entered DESC';
		
		
		if ($this->parent == 'Block') {
			$listing->name = 'Block';
			$listing->paginate = false;
			$listing->order_by = 'in_home DESC, ordre ASC, circuit ASC, entered DESC';
			$listing->limit = '3';
			$listing->set_records();
			return $listing->loop;
		}
		
	    return $listing->list_records();
	}
	function records_walk(&$listing, $content=false,$content_list=false){		
		
		$ret = array();
		
		// si es llistat passa la variable content
		if ($content!==false){
			$ret['content']=$content_list?$content_list:add_dots('content', $content);
		}
		return $ret;
	}
	
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function set_title() {
		
		if ($this->parent=='Block')return;
		$place_id =  R::id('place_id');
		if (!$place_id) Main::redirect ('/');
		
		
	    $place = Db::get_first("
			SELECT place 
			FROM sahara__place_language 
			WHERE place_id = " . $place_id . "
				AND language = '" . LANGUAGE ."'");
		//$place = $this->caption['c_circuits_from'] . ' ' . $place;
		Page::set_title($place);
		
	}
	function get_form()
	{
		
		$this->set_title();
		
		
	    $show = new ShowForm($this);
		$show->condition .= "status = 'public'";
		
		$show->get_values();
		if ($show->has_results)
		{
			
			Page::set_page_title($show->rs['page_title'],$show->rs['circuit']);
			Page::set_page_description($show->rs['page_description'],$show->rs['content']);
			Page::set_page_keywords($show->rs['page_keywords']);
			
			$module_dies = Module::load('sahara', 'dia', '', false);

			$listing = new ListRecords($module_dies);
			$listing->has_bin = false;
			$listing->paginate = false;
			$listing->condition = "circuit_id = '".$show->id."'";
			$listing->order_by = 'ordre ASC, dia ASC';
			$dies = $listing->list_records();
			
			if (!$listing->has_results) $dies='';
			$show->set_var('dies', $dies);

			$content =  $show->show_form();

			Page::set_page_og($show->rs);
			return $content;
			
		}
		else
		{
			Main::redirect('/');
		}
		
	}
}
?>