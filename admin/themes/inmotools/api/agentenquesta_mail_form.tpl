<style>
.formCaptionTitle{
	width: 610px;
	font-size: 18px;
	padding: 30px 20px 8px 0px;
	font-weight:bold;
	border-bottom:1px solid #9b9b9b;
}
.formsCaptionHor{
	width: 610px;
	font-size: 16px;
	padding: 30px 20px 8px 20px;
	font-weight:normal;
}
.formsCaption{
	width: 400px;
	font-size: 14px;
	padding: 8px 20px;
	font-weight:normal;
	color:#909090;
}
.forms{
	font-size: 14px;
	width: 250px;
}
</style>

<table border="0" cellspacing="0" cellpadding="0" class="form">
	<tr>
	<td>
		<table border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td><?if ($c_form_title):?>
				<table cellpadding="4" cellspacing="0">
				<tr>
					<td class="formCaptionTitle">
					<?=$c_form_title?> <?if($form_subtitle):?><span><?=$form_subtitle?></span><?endif //form_subtitle?>
					</td>
				</tr>
				</table>
				<table cellpadding="0" cellspacing="0">
				<tr>
					<td height="10">					
					</td>
				</tr>
				</table>
				<?endif //c_form_title?>
			<table cellpadding="4" cellspacing="0" class="formstbl">
				<?foreach($fields as $l): extract ($l)?>
				<?if($form_level1_title):?>
				<tr>
					<td colspan="2" class="formCaptionTitle"><?=$form_level1_title?></td>
				</tr>
				<?endif //form_level1_title?>
				
				<?if($form_level2_title):?>
				<tr>
					<td colspan="2" class="formsCaptionHor"><?=$form_level2_title?></td>
				</tr>
				<?endif //form_level2_title?>
				<tr>				
					<td valign="top" class="formsCaption"><?if($key):?>
					<?=$key?> <?endif //key?></td>
					<td valign="top" class="forms"><table width="100%" border="0" cellpadding="0" cellspacing="0">
						<?foreach($val as $l): extract ($l)?>
						<tr>
							<?if ($lang):?>
							<td class="formsCaption"><?=$lang?></td>
							<?endif //lang?>
						</tr><tr>
						<td><?=$val?></td>
						</tr>
						<?endforeach //$val?>
					</table></td>
				</tr>
				<?endforeach //$field?>
			</table></td>
		</tr>
		</table></td>
	</tr>
</table>
