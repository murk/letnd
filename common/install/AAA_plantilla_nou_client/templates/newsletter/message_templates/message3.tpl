<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<base href="<?= $url ?>">
	<?include('styles.tpl')?>
</head>
<body>

<table id="letnd-table-content">
	<tr>
		<td id="letnd-td-content" align="center">

			<? include( 'header.tpl' ) ?>

			<? if ( $message || $pictures ): ?>
				<table id="letnd-content" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td class="letnd-content">
							<?= $message ?>
							<table border="0" cellspacing="0" cellpadding="0" width="100%">
								<? if($pictures):?>
								
								<?foreach($pictures as $picture): extract ($picture)?>
								
								<?if($picture_conta%2==0):?>
								<tr>
								<?endif //conta?>
								
									<td class="letnd-picture<?=$picture_conta%2==0?'left':'right'?><?if($picture_loop_count-$picture_conta<=2) echo " last"?>">
										<img width="305" src="<?=$picture_src?>">
									</td>

								<?if($picture_conta%2==1):?>
								</tr>
								<?endif //conta?>
								
								<?endforeach //$images?>	
											
								<?if($picture_conta%2!=1):?>					
								</tr>
								<?endif //conta?>
								
								<?endif //pictures?>
							</table>
						</td>
					</tr>
				</table>
			<? endif //message?>

			<? if ( $content_extra ): ?>
				<table id="letnd-content_extra" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td>
							<?= $content_extra ?>
						</td>
					</tr>
				</table>
			<? endif //content_extra?>

			<? include( 'footer.tpl' ) ?>

		</td>
	</tr>
</table>

</body>
</html>