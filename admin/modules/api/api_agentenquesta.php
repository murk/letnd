<?
/**
 * ApiAgentenquesta
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2014
 * @version $Id$
 * @access public 
 
 */
class ApiAgentenquesta extends Module{

	var $group_id, $user_is_admin, $user_is_agent, $agencia_id;
	
	function __construct(){
		parent::__construct();
		
		$this->group_id = $_SESSION['group_id'];
		
		$this->user_is_admin = $this->group_id < 3; // grups 2 i 1
		// els usuaris es el grup 3
		$this->user_is_agent = $this->group_id == 4; // grups 4
		
		if ($this->user_is_agent){
			$this->agencia_id = Db::get_first('SELECT agencia_id FROM user__user WHERE user_id = ' . $_SESSION['user_id']);
		}
			
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function get_records()
	{
	}
	
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_agencia() {
		if ($this->user_is_agent) {			
			$ret = Db::get_first("
				SELECT agencia FROM api__agencia WHERE agencia_id = " . $this->agencia_id);
		}
		else{		
			$ret = 'Api Girona';			
		}
		return $ret;
	}
	function get_title() {		
		$form_title = Db::get_first("
				SELECT enquesta FROM api__enquesta WHERE enquesta_id = 1
			");
		return $form_title;
	}
	function get_groups() {
		$groups = Db::get_rows("
				SELECT * FROM api__enquesta_group WHERE enquesta_id = 1
			");
		return $groups;
	}
	function get_items($enquesta_group_id) {
		$results = Db::get_rows("
					SELECT * FROM api__enquesta_item 
					WHERE enquesta_id = 1
					AND group_id = " . $enquesta_group_id . "
					ORDER BY item_num ASC"
				);
		return $results;
	}
	function init_fields() {
		$this->language_fields = array();
		$titles = array();
		
		$this->caption['c_form_title'] = $this->get_title();
		$groups = $this->get_groups();
		
		$this->add_field('agencia_id');
		$this->set_field('agencia_id', 'type', 'text');
		$this->set_field('agencia_id', 'form_admin', 'text');
		$this->set_field('agencia_id', 'default_value', $this->get_agencia());
		
		$this->add_field('nom_persona');
		$this->set_field('nom_persona', 'type', 'text');
		$this->set_field('nom_persona', 'form_admin', 'input');
		$this->set_field('nom_persona', 'class', 'wide');
		$this->set_field('nom_persona', 'required', '1');
		
		$this->add_field('entered');
		$this->set_field('entered', 'type', 'text');
		$this->set_field('entered', 'form_admin', 'text');
		$this->set_field('entered', 'default_value', now());
		
		foreach ($groups as $group){

			$results = $this->get_items($group['enquesta_group_id']);

			$conta = 0;

			foreach ($results as $rs){
				extract($rs);
				if ($item)
					$this->caption['c_' . $enquesta_item_id] = $item_num . '. ' . $item;
				else
					$this->caption['c_' . $enquesta_item_id] = '';

				switch ($type) {
					case 'rate':
						$this->add_field($enquesta_item_id);
						$this->set_field($enquesta_item_id, 'type', 'rate');
						$this->set_field($enquesta_item_id, 'required', '0');
						break;
					case 'options':
						$this->add_field($enquesta_item_id);
						$this->set_field($enquesta_item_id, 'type', 'select');							
						$this->set_field($enquesta_item_id, 'select_table', 'api__enquesta_multiple');
						$this->set_field($enquesta_item_id, 'select_fields', 'enquesta_item_id,multiple');
						$this->set_field($enquesta_item_id, 'select_condition', 'enquesta_item_id = ' . $enquesta_item_id . " ORDER by ordre");
						$this->set_field($enquesta_item_id, 'required', '0');
						break;
					case 'multipleoptions':
						$this->add_field($enquesta_item_id);
						$this->set_field($enquesta_item_id, 'type', 'checkboxes');							
						$this->set_field($enquesta_item_id, 'select_table', 'api__enquesta_multiple');
						$this->set_field($enquesta_item_id, 'select_fields', 'enquesta_multiple_id,multiple');
						$this->set_field($enquesta_item_id, 'select_condition', 'enquesta_item_id = ' . $enquesta_item_id . " ORDER by ordre");
						$this->set_field($enquesta_item_id, 'checked_table', 'api__enquesta_multiple_to_customer'); // de moment inventada
						break;
					case 'yesno':
						$this->add_field($enquesta_item_id);
						$this->set_field($enquesta_item_id, 'type', 'yesno');
						$this->set_field($enquesta_item_id, 'required', '0');
						break;
					case 'textarea':
						$this->add_field($enquesta_item_id);
						$this->set_field($enquesta_item_id, 'type', 'textarea');
						$this->set_field($enquesta_item_id, 'textarea_rows', '4');
						$this->set_field($enquesta_item_id, 'class', 'wide');
						break;
					}

					$this->set_field($enquesta_item_id, 'form_admin', 'input');

					if ($conta==0){
						$conta = 1;
						$titles[]= $enquesta_item_id;
						$this->caption['c_form_level2_title_' . $enquesta_item_id] = $group['group_num'] . '. ' . $group['group'];
					}
					
					/* TESTTTTTTT per treure obligatoris */
					//$this->set_field($enquesta_item_id, 'required', '0');
				}
			}
			return $titles;
				
				
	}
	function get_form()
	{
		if ($this->agencia_id){
		$is_enquesta_sent = Db::get_first("
			SELECT count(*) 
			FROM api__agencia 
			WHERE enquesta = year(now())
			AND agencia_id = " . $this->agencia_id);
		}
		else{
			$is_enquesta_sent = false;			
		}
		
		if ($is_enquesta_sent){
			if (!$GLOBALS['gl_message']) $GLOBALS['gl_news'] = "Ja heu omplert i enviat l'enquesta";
		}			
		else{
		
			$titles = $this->init_fields();
			//Debug::p($this->fields, 'text');

			$show = new ShowForm($this);
			$show->form_level2_titles = $titles;
			return $show->show_form();
			
		}
	    
	}

	function write_record()
	{	 
		$writerec = new SaveRows();
		
		$titles = $this->init_fields();
		
		foreach ($this->fields as $key => &$val){
			if ($key!='agencia_id' && $key!='entered'){
				$val['form_admin'] = 'text';
				$default = $writerec->get_value($key);
				
				if($val['type'] == 'checkboxes' && $default){
					$default = implode (',',$default);	
					$results = Db::get_rows("
						SELECT multiple 
						FROM api__enquesta_multiple 
						WHERE enquesta_multiple_id IN (".$default.")");
					$default = implode_field($results,'multiple', ', ');
					$val['type'] = 'text';
				}
								
				//Debug::p($default, 'value');
				$val['default_value'] = $default;
			}
		}
		
		//Debug::p($this->fields, 'fields');
		
		$this->action = 'show_form_new';
		
		$show = new ShowForm($this);
		$show->form_level2_titles = $titles;
		$show->default_template = 'api/agentenquesta_mail_form';
		//$show->set_options(0, 0, 0, 0);
		$body = $show->show_form();
		
		$instyler = new \Pelago\Emogrifier($body);
		$body = $instyler->emogrify();
		
		if (send_mail_admin($this->caption['c_form_title'], $body)){
		
			if ($this->agencia_id) 
				Db::execute("UPDATE api__agencia SET enquesta=year(now()) WHERE  agencia_id="  . $this->agencia_id);
			
				if (!DEBUG) $GLOBALS['gl_reload'] = true;
				$GLOBALS['gl_message'] = $this->messages['enquesta_sent'];
		
		}
		else{
			$GLOBALS['gl_message'] = $this->messages['enquesta_not_sent'];
			$GLOBALS['gl_saved'] = false;
		}
		
	}
}
?>