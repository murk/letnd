$(document).ready(function(){
	// amaga privat
	$('#hide_private').click(function () {
			// animate exageradament lent amb ie8 i xhtml
			//$('.private').animate({ opacity: 'hide' }, "slow");
			//$('#hide_private,a.boto1_private').hide();
			//$('.not_private').animate({ opacity: 'show' }, "slow");
			//$('#show_private').show();
			$('#hide_private,a.boto1_private,.private').hide();
			$('#show_private,.not_private').show();
			$.cookie('private',true);
			//

		});
	// mostra privat
	$('#show_private').click(function () {
			// animate exageradament lent amb ie8 i xhtml
			//$('.not_private').animate({ opacity: 'hide' }, "slow");
			//$('#show_private').hide();
			//$('.private').animate({ opacity: 'show' }, "slow");
			//$('#hide_private,a.boto1_private').show();
			$('#show_private,.not_private').hide();
			$('#hide_private,a.boto1_private,.private').show();
			$.cookie('private',null);
			//
		});
});

function click_checkbox(obj, estil, conta, prefix){
	formu = document.search_filter?document.search_filter:document.theForm;

	seleccionat = formu.elements[prefix + '[' + conta + ']'].checked;
	if (estil=='Down') {
		formu.elements[prefix + '[' + conta + ']'].checked = !seleccionat;
		if (document.search_filter2) document.search_filter2.elements[prefix + '[' + conta + ']'].checked = !seleccionat;
	}
	if (!seleccionat)
	{
		obj.className = 'searchItem' + estil;
	}else
	{
		if (estil=='Over') obj.className = 'searchItem' + estil;
		if (estil=='Odd' || estil=='Even') obj.className = 'searchItemDown';
	}
}

function click_radio(obj, estil, conta, prefix){
	seleccionat = false;
	radio_obj = document.search_filter.elements[prefix];
	for (var radio_num=0; radio_num < radio_obj.length; radio_num++) {
		if (radio_obj[radio_num].value==conta){
			seleccionat = radio_obj[radio_num].checked;
			if (estil=='Down') {
				radio_obj[radio_num].checked = !seleccionat;
				if (document.search_filter2) document.search_filter2.elements[prefix][radio_num].checked = !seleccionat;
			}
		}
		else if (estil=='Down')
		{
			document.getElementById('tr_' + prefix + '_' + radio_obj[radio_num].value).className = 'searchItemOdd';
		}
	}
	if (!seleccionat)
	{
		obj.className = 'searchItem' + estil;
	}else
	{
		if (estil=='Over') obj.className = 'searchItem' + estil;
		if (estil=='Odd' || estil=='Even') obj.className = 'searchItemDown';
	}
}
function click_input(obj, estil, prefix){
	if (estil=='blur') {
		if (obj.value != '') document.getElementById('tr_' + obj.name).className = 'searchItemDown';
		return;
	}
	seleccionat = document.search_filter.elements[prefix].value;
	if (estil=='Down') {
		document.search_filter.elements[prefix].focus();
		if (document.search_filter2) false;
	}

	if (!seleccionat)
	{
		obj.className = 'searchItem' + estil;
	}else
	{
		if (estil=='Over') obj.className = 'searchItem' + estil;
		if (estil=='Odd' || estil=='Even') obj.className = 'searchItemDown';
	}
}


/**
 *
 * @access public
 * @return void
 **/
function tipus_click(obj){
	if (obj.value=='temp') {
			$('#tipus_temp_link').show();
	}
	else {
			$('#tipus_temp_link').hide();
	}

}



$(document).ready(function(){				
	$(document).bind('click', function(e) {
        var $clicked=$(e.target); // get the element clicked
		if($clicked.is('div.subMenu') || $clicked.parents().is('div.subMenu') || $clicked.is('a.btn_form_down') || $clicked.parents().is('a.btn_form_down') ) { 
		}
		else
		{	
			if ($('div.subMenu').length){
				if ($('div.subMenu').offset) $('div.subMenu').removeShadow(); 
				$('div.subMenu').hide();
				$($gl_visible['down']).removeClass('btn_form_down_clicked');	
			}
		}
	});
});

$gl_visible = {'submenu':'', 'down':''};
function show_options(submenu_id, btn_down, alignto, side){
	
		s_width = $(submenu_id).width();
		//$.dump($(alignto).offset());
		is_jquery_1 = $(alignto).offset?false:true;
		if (!is_jquery_1){
			s_left = $(alignto).offset().left;
			s_top = $(alignto).offset().top + $(alignto).outerHeight() + 'px';
		}else{
			a = document.getElementById(alignto.replace('#',''));
			var position = $.iUtil.getPositionLite(a);
			s_left = position.x;	
			//s_top = position.y + $(alignto).height() + parseInt($(alignto).css('padding-top')) + parseInt($(alignto).css('padding-bottom')) + parseInt($(alignto).css('border-top')) + parseInt($(alignto).css('border-bottom')) + 'px';	
			s_top = position.y + 21 + 'px';
			//$.dump(position);	
		}
		
		
		if (side!='left'){
			if (!is_jquery_1){
				s_left = s_left + $(alignto).outerWidth() - $(submenu_id).outerWidth();
			}
			else
			{
				pad_width = parseInt($(alignto).css('padding-left')) + parseInt($(alignto).css('padding-right')) + parseInt($(alignto).width());
				//pad_width+= parseInt($(alignto).css('margin-left')) + parseInt($(alignto).css('margin-right'));
				pad_width-= parseInt($(submenu_id).css('padding-left')) + parseInt($(submenu_id).css('padding-right'))+ parseInt($(submenu_id).width());
				//pad_width-= parseInt($(submenu_id).css('margin-left')) + parseInt($(submenu_id).css('margin-right'));
				s_left = s_left + pad_width;
			}
			
		}
		
		s_left += 'px';
		s_display = $(submenu_id).css("display");
		// tanca l'anterior o la mateixa si es oberta
		if ($gl_visible['submenu'] || s_display!='none'){
			if (!is_jquery_1) $($gl_visible['submenu']).removeShadow();			
			$($gl_visible['submenu']).hide();
			$($gl_visible['down']).removeClass('btn_form_down_clicked');	
		}
		// obra nomes si era tancada
		if (s_display=='none'){	
			$(submenu_id).css('left',s_left);
			$(submenu_id).css('top',s_top);
			$(submenu_id).width(s_width + 'px');
			$(submenu_id).show();
			$(btn_down).addClass('btn_form_down_clicked');
			if (!is_jquery_1) $(submenu_id).dropShadow();
			$(submenu_id).focus();
			$gl_visible['submenu'] = submenu_id;
			$gl_visible['down'] = btn_down;
		}
}



function show_mls_property(property_id, menu_id, curl, language){
	showPopWin(curl + '/?tool=inmo&tool_section=property&action=show_record&mls=true&property_id='+property_id+'&language='+language, 950, 560, null, true, false, '', true);
}
function show_price_history_property(property_id, menu_id){
	showPopWin('/admin/?tool=custumer&tool_section=history&action=list_records&template=clean&property_price_history=1&tipus=price_changed&property_id='+property_id, 950, 580, null, true, false, '', true);
}

function set_portal_config_input (obj){
	var value = obj.checked?1:0;
	var name = obj.name;

	dp(value);

	var form = $('#form-list');

	if (!form.find('input[name="' + name + '"').length){
		dp('no existeix');
		form.append( '<input type="hidden" name="' + name + '">' );
	}
	form.find('input[name="' + name + '"').val(value);
	dp(form.find('input[name="' + name + '"').val());
}