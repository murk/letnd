<?php
	// Aquest arxiu es genera automaticament
	// Qualsevol canvi que es faci es podrà sobrescriure involuntariament desde l'eina de gestió
	// Achtung!
	$gl_file_protected = true;
	$gl_db_classes_fields_included = array (
  'mls_id' => '1',
  'client_id1' => '1',
  'client_id2' => '1',
  'permission' => '1',   
);
	$gl_db_classes_language_fields = array (
  0 => '',
);
	$gl_db_classes_fields = array (
  'mls_id' =>
  array (
    'type' => 'hidden',
    'enabled' => '1',
    'form_admin' => 'input',
    'form_public' => 'text',
    'list_admin' => 'text',
    'list_public' => 'text',
    'order' => '0',
    'default_value' => '',
    'override_save_value' => '',
    'class' => '',
    'javascript' => '',
    'text_size' => '',
    'text_maxlength' => '',
    'textarea_cols' => '',
    'textarea_rows' => '',
    'select_caption' => '',
    'select_size' => '',
    'select_table' => '',
    'select_fields' => '',
    'select_condition' => '',
  ),
  'client_id1' =>
  array (
    'type' => 'hidden',
    'enabled' => '1',
    'form_admin' => 'input',
    'form_public' => '0',
    'list_admin' => 'text',
    'list_public' => '0',
    'order' => '0',
    'default_value' => '',
    'override_save_value' => '',
    'class' => '',
    'javascript' => '',
    'text_size' => '',
    'text_maxlength' => '',
    'textarea_cols' => '',
    'textarea_rows' => '',
    'select_caption' => '',
    'select_size' => '',
    'select_table' => '',
    'select_fields' => '',
    'select_condition' => '',
  ),
  'client_id2' =>
  array (
    'type' => 'select',
    'enabled' => '1',
    'form_admin' => 'input',
    'form_public' => '0',
    'list_admin' => 'text',
    'list_public' => '0',
    'required' => '1',
    'order' => '1',
    'default_value' => '',
    'override_save_value' => '',
    'class' => '',
    'javascript' => '',
    'text_size' => '',
    'text_maxlength' => '',
    'textarea_cols' => '',
    'textarea_rows' => '',
    'select_caption' => '',
    'select_size' => '',
    'select_table' => 'client__client',
    'select_fields' => 'client_id, comercialname, phone1',
    'select_condition' => '',
  ),
  'permission' =>
  array (
    'type' => 'text',
    'enabled' => '1',
    'form_admin' => 'input',
    'form_public' => '0',
    'list_admin' => 'text',
    'list_public' => '0',
    'order' => '2',
    'default_value' => '',
    'override_save_value' => '',
    'class' => '',
    'javascript' => '',
    'text_size' => '',
    'text_maxlength' => '',
    'textarea_cols' => '',
    'textarea_rows' => '',
    'select_caption' => '',
    'select_size' => '',
    'select_table' => '',
    'select_fields' => '',
    'select_condition' => '',
  ),
);
	$gl_db_classes_list = array (
  'cols' => '2',
);
	$gl_db_classes_form = array (
  'cols' => '1',
  'images_title' => '',
);

?>