<?
/**
 * RevistaBanner
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2012
 * @version $Id$
 * @access public
 */
class RevistaBanner extends Module{

	function __construct(){
		parent::__construct();
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function get_records()
	{
	    $listing = new ListRecords($this);
		$listing->order_by = 'ordre ASC';
		$listing->show_langs = true;
		$listing->paginate = false;
	    $listing->set_records();
		foreach ($listing->loop as $key=>$val){
			$rs = &$listing->loop[$key];
			if ($rs['files']){
				$rs['image_src_admin_thumb'] = $rs['files'][0]['file_src'];
			}
			else{
				$rs['image_src_admin_thumb'] = '/admin/modules/revista/banner_no_image.gif';
			}
		}		
		if ($listing->loop) return $listing->process();
	}
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{
	    $show = new ShowForm($this);
	    return $show->show_form();
	}

	function save_rows()
	{
	    $save_rows = new SaveRows($this);
	    $save_rows->save();
	}

	function write_record()
	{
	    $writerec = new SaveRows($this);
	    $writerec->save();
	}

	function manage_images()
	{
	    $image_manager = new ImageManager($this);
	    $image_manager->execute();
	}
}
?>