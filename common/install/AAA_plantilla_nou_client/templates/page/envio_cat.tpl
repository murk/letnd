<article>
	<h2>Entrega dels nostres productes.</h2>
	<p>L'entrega de les comandes es realitzarà en un període d'entre 3 i 14 dies hàbils* a partir de la data de compra.</p>
	<p> Si es donés el cas, se li informaria mitjançant un e-mail de qualsevol variació o problema que ens hàgim trobat referent a la disponibilitat, preu, pagament o entrega del producte.	  
    </p>
	<p>En el cas que s'enviés una comanda i la direcció escrita per l'usuari fos incorrecta, les despeses suplementàries de transport anirien a càrrec del comprador.	  
    </p>
	<p>* Els dies festius i caps de setmana es contemplaran com a dies no hàbils.	  
    </p>
</article>