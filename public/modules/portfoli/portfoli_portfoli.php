<?

class PortfoliPortfoli extends Module{
	var $condition = '', $order_by = 'ordre asc, portfoli_id desc';
	function __construct(){
		parent::__construct();
		$this->add_field('url1_short',array(
			'type'=>'none',
			'form_public'=>'out',
			'list_public'=>'out'
			));
	}
	function on_load(){
		
		$this->set_friendly_params(array(
			'category_id'=>R::id('category_id')			
			));

	   	Main::load_class('other');
		Other::set_fields($this);

		parent::on_load();
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function get_records()
	{
		if ($this->condition) {
			$this->messages['no_records'] = $this->messages['no_records_search'];
		}
		
		$listing = new ListRecords($this);
		$listing->condition = "(status = 'public')";
		$listing->condition .= $this->condition;
		
		
		$listing->order_by = $this->order_by;
		//$listing->call_function['description'] = 'add_dots';		
		$listing->call('records_walk','url1,url2');		
		
		$category_id = R::id('category_id');
		$listing->set_var('listing_category_id',$category_id);	
			
		if ($category_id) $category = Db::get_first("
				SELECT category 
				FROM portfoli__category_language
				WHERE language = '". LANGUAGE ."' 
				AND category_id = '" . $category_id ."'");
				
		else  $category = '';
		
		$listing->set_var('listing_category',$category);	
		
		$listing->condition .= $this->get_category_condition();
		
		if ($this->parent == 'sitemap') {//Sitemap
			$listing->paginate = false;
			$listing->set_field('modified', 'type', 'none');
			$listing->set_records();
			return $listing->loop;
		}
		elseif($this->parent == 'PortfoliPortfoliHome') {//Sitemap
			return $listing;
		}
		else{
			
		}
		
		return $listing->list_records();
	}
	function get_category_condition () {

		$category_id = R::id('category_id');

		if ($category_id) {
			return " AND portfoli_id IN
			(
				SELECT portfoli__portfoli_to_category.portfoli_id
				FROM portfoli__portfoli_to_category
				WHERE category_id = '" . $category_id . "')";
		}
		else {
			return '';
		}

	}
	function show_form()
	{		
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{
		
		
		$show = new ShowForm($this);
		$show->order_by = $this->order_by;
		$show->condition = "status = 'public'";
		$show->condition .= $this->get_category_condition();
		//$show->set_prev_next_id();
		$show->call('records_walk','url1,url2');
		$show->get_values();
		
		// si venim del llistat d'una categoria
		$category_link = $category_link_category = '';
		$category_id = R::id('category_id');
		
		if ($category_id) {
			
			$category_link = get_link( array(
							'tool' => 'portfoli', 
							'tool_section' => 'portfoli', 
							'category' => 'category_id',
							'category_id' => $category_id,
							'detect_page' => true
					));
			
			 $category_link_category = Db::get_first("
				SELECT category 
				FROM portfoli__category_language
				WHERE language = '". LANGUAGE ."' 
				AND category_id = '" . $category_id ."'");
			 
		
		}
		$this->set_var('category_link', $category_link);
		$this->set_var('category_link_category', $category_link_category);
		
		
		if ($show->has_results)
		{
			if ($this->config['override_gl_page_title'] == '1') Page::set_title($show->rs['title']);
			if ($this->config['override_gl_page_title'] == '2') Page::set_body_subtitle($show->rs['title']);
			
			Page::set_page_title($show->rs['page_title'],$show->rs['title']);
			Page::set_page_description($show->rs['page_description'],$show->rs['description']);
			Page::set_page_keywords($show->rs['page_keywords']);
				
			Page::add_breadcrumb($show->rs['title']);

			$content =  $show->show_form();

			Page::set_page_og($show->rs);
			return $content;
		}
		else
		{
			Main::error_404();
		}	
	}
	function records_walk($url1, $url2) {
		$url = str_replace('http://','',$url1);
		$ret['url1_short'] = $url;

		$url = str_replace('http://','',$url2);
		$ret['url2_short'] = $url;
		return $ret;
	}
	
	function search(){
		$this->condition = $this->search_list_condition_words();
		$this->do_action('list_records');
	}
	
	function search_list_condition_words ()
	{
	    $q = R::escape('q');

		$query = "SELECT portfoli_id FROM portfoli__portfoli where
				   url1 like '%" . $q . "%' OR
				   url2 like '%" . $q . "%'
				   " ;
		$results1 = Db::get_rows ($query);
		$_1 = count ($results1);
		$query2 = "SELECT portfoli_id FROM portfoli__portfoli_language where
				   title like '%" . $q . "%' OR
				   subtitle like '%" . $q . "%' OR
				   description_text like '%" . $q . "%'
				   " ;
		$results2 = Db::get_rows ($query2);
		Debug::p( $query2 );

		$_2= count ($results2);
		if ($_1+$_2 > 0 ) {//faig trampa per si no hi ha cap resultat
		    $results = array_merge($results1,$results2 );
			 foreach ($results as $key)
                {
					$ids_arr[] = $key['portfoli_id'];
                }

                $ids = implode(',', $ids_arr);
				
			} else {$ids="NULL";}

		$query = "AND portfoli_id IN (" . $ids . ")";
		$search_condition = $query;
	    $GLOBALS['gl_page']->title = TITLE_SEARCH;
	    return $search_condition;
	}
}	

?>