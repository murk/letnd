<?php
/* 18-4-17
Product, si te despeses d'enviament

*/
if ( is_table( 'product__configadmin' ) && ! Db::get_first( "SELECT * FROM product__configadmin WHERE name='has_rate'" ) ) {
	$update_sql[] = "
		INSERT INTO product__configadmin (`name`, `value`) VALUES ('has_rate', '1');";

} else {
	if ( is_table( 'product__configadmin' ) ) {
		trigger_error( 'Ja existeixen el camp has_rate' );
	}
}
?>