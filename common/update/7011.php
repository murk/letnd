<?php
/* 26-4-17

Afegir agents de INMO

*/

if ( is_table( 'user__user' ) ) {

	if ( ! is_table( 'user__user_to_language' ) ) {

		$update_sql [] = "
			CREATE TABLE `user__user_to_language` (
				`user_to_language_id` INT(11) NOT NULL AUTO_INCREMENT,
				`user_id` INT(11) NOT NULL DEFAULT '0',
				`language_id` INT(11) NOT NULL DEFAULT '0',
				PRIMARY KEY (`user_to_language_id`)
			)
			COLLATE='utf8_spanish2_ci'
			ENGINE=MyISAM;";

	} else {
		trigger_error( 'Ja existeixen la taula user__user_to_language' );
	}

}

if ( is_table( 'user__user' ) ) {

	if ( ! is_table( 'user__user_language' ) ) {

		$update_sql [] = "CREATE TABLE `user__user_language` (
				`user_id` INT(11) NOT NULL DEFAULT '0',
				`language` CHAR(3) NOT NULL DEFAULT '' COLLATE 'utf8_spanish2_ci',
				`description` TEXT NOT NULL COLLATE 'utf8_spanish2_ci'
			)
			COLLATE='utf8_spanish2_ci'
			ENGINE=MyISAM;";

		// poso tots els registres del grup, de l'idioma de l'admin a cada un dels idiomes restant publics
		global $gl_languages;

		foreach ( $gl_languages['names'] as $lang => $val ) {
			$update_sql[] = "INSERT INTO user__user_language (user_id, language,description) SELECT user_id, '" . $lang . "', ''  FROM user__user;";
		}

	} else {
		trigger_error( 'Ja existeixen la taula user__user_language' );
	}

}

if ( is_table( 'user__user' ) ) {

	if ( ! is_table( 'user__user_image' ) ) {

		$update_sql [] = "
					CREATE TABLE `user__user_image` (
						`image_id` INT(11) NOT NULL AUTO_INCREMENT,
						`user_id` INT(11) NOT NULL DEFAULT '0',
						`imagecat_id` INT(11) NOT NULL DEFAULT '1',
						`name` VARCHAR(100) NOT NULL DEFAULT '' COLLATE 'utf8_spanish2_ci',
						`size` INT(11) NOT NULL DEFAULT '0',
						`ordre` INT(11) NOT NULL DEFAULT '0',
						`main_image` INT(1) NOT NULL DEFAULT '0',
						PRIMARY KEY (`image_id`)
					)
					COLLATE='utf8_spanish2_ci'
					ENGINE=MyISAM;";

	} else {
		trigger_error( 'Ja existeixen la taula user__user_image' );
	}

	if ( ! is_table( 'user__user_image_language' ) ) {

		$update_sql [] = "
					CREATE TABLE `user__user_image_language` (
						`image_id` INT(11) NOT NULL DEFAULT '0',
						`language` CHAR(3) NOT NULL COLLATE 'utf8_spanish2_ci',
						`image_alt` VARCHAR(150) NOT NULL COLLATE 'utf8_spanish2_ci',
						`image_title` VARCHAR(255) NOT NULL COLLATE 'utf8_spanish2_ci'
					)
					COLLATE='utf8_spanish2_ci'
					ENGINE=MyISAM;";

	} else {
		trigger_error( 'Ja existeixen la taula user__user_image_language' );
	}

	$client_dir = DOCUMENT_ROOT . '/' . $_SESSION['client_dir'];

	$dir = $client_dir . '/user/';
	if ( ! is_dir( $dir ) ) {
		mkdir( $dir, 0776 );
	}

	$dir = $client_dir . '/user/user/';
	if ( ! is_dir( $dir ) ) {
		mkdir( $dir, 0776 );
	}

	$dir = $client_dir . '/user/user/images/';
	if ( ! is_dir( $dir ) ) {
		mkdir( $dir, 0776 );
	}

}

if ( is_table( 'user__user' ) && ! is_field( 'user__user', 'status' ) ) {

	$update_sql[] = "ALTER TABLE `user__user`
	ADD COLUMN `status` ENUM('private','public') NOT NULL DEFAULT 'private' AFTER `twitter_user_details`;";

} else {
	if ( is_table( 'user__user' ) ) {
		trigger_error( 'Ja existeixen el camp status' );
	}
}

?>