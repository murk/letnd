<?

// Per obtindre el JSON de totes les dades de depuració de la pàgina concreta

include( '../../../common/classes/main.php' );
Main::set_vars();

include( DOCUMENT_ROOT . 'common/includes/Clockwork/clockwork_load.php' );

$direction = R::get( 'direction' );
$count     = R::get( 'count', 10 );
$id        = R::get( 'id' );

$clockwork = new Clockwork\Clockwork;
$clockwork->setStorage( new Clockwork\Storage\SqlStorage(
		"mysql:dbname=$db_name;host=$db_host",
		'admintotal__clockwork',
		$db_user_name,
		$db_password )
);

header( 'Content-Type', 'application/json' );

$storage = $clockwork->getStorage();

if ( $direction == 'previous' ) {
	$data = $storage->previous( $id, $count );
}
elseif ( $direction == 'next' ) {
	$data = $storage->next( $id, $count );
}
elseif ( $id == 'latest' ) {
	$data = $storage->latest();
}
else {
	$data = $storage->find( $id );
}


echo json_encode($data);