
	<script language="JavaScript" src="/admin/modules/inmo/jscripts/functions.js?v=<?= $version ?>"></script>
<? if ( ! $results ): ?>
	<? if ( $options_filters ): ?>
		<div id='opcions_holder'>
			<table width="100%" border="0" cellspacing="0" cellpadding="0" id="opcions">
				<tr>
					<td class="opcionsTitol"><strong><?= $c_options_bar ?></strong></td>
				</tr>
				<tr>
					<td class="opcions">
						<table class="filters">
							<tr valign="middle">
								<? foreach ( $options_filters as $options_filter ): ?>
									<td><?= $options_filter ?></td>
								<? endforeach //$options_filters?>
								<? if ( $save_button ): ?>
								<? endif //save_button?>
								<? if ( $select_button ): ?>
								<? endif //select_button?>
								<? if ( $delete_button ): ?>
									<? if ( $has_bin ): ?>
										<? if ( $is_bin ): ?>
										<? else: ?>
										<? endif // is_bin?>
									<? else: ?>
										<!--<td>
							<td>&nbsp;</td><strong><a class="botoOpcions" href="/admin?menu_id=<?= $menu_id ?>&sort_by=0&sort_order=<?= $sort_order0 ?>">
								<?= $c_move_to ?>
							
						<input type="submit" name="bin_selected" value="<?= $c_delete_selected ?>" class="boto1">
							</a></strong></td>-->
									<? endif // has_bin?>
								<? endif //delete_button?>
								<? if ( $print_button ): ?>
								<? endif //delete_button?>
							</tr>
						</table>
						<div class="sep"></div>
						<table border="0" cellpadding="2" cellspacing="0">
							<tr valign="middle">
								<td>
									<strong><a title="<?= htmlspecialchars( $c_list_mls ) ?>" target="save_frame" class="no-margin botoMls active<?= ! empty ( $_SESSION['inmo_show_mls'] ) ?>" href="/admin/?menu_id=103&call=swap_mls">

										</a></strong></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>
	<? endif //options_filters?>
<? endif //!$results?>
<? if ( $results ): ?>
	<div id='split_holder'>
		<table width="100%" border="0" cellpadding="0" cellspacing="0" id="split">
			<tr valign="middle">
				<td width="20%" align="left" nowrap class="pageResults"><?= $split_results_count ?></td>
				<td align="right" nowrap class="pageResults"><?= $split_results ?></td>
			</tr>
		</table>
	</div>
	<? //if ($options_bar):?>
	<div id='opcions_holder'>
		<table width="100%" border="0" cellspacing="0" cellpadding="0" id="opcions">
			<tr>
				<td class="opcionsTitol"><strong>
						<?= $c_options_bar ?>
					</strong></td>
			</tr>
			<tr>
				<td class="opcions">
					<div id="opcions_fixed_bar">
						<div class="more"></div>
					</div>
					<table class="filters">
						<tr valign="middle">
							<? foreach ( $options_move_tos as $options_move_to ): ?>
								<td><?= $options_move_to ?></td>
							<? endforeach //$options_move_tos?>
							<? foreach ( $options_filters as $options_filter ): ?>
								<form>
									<td valign="top">
										<?= $options_filter ?>
									</td>
								</form>
							<? endforeach //$options_filters?>
						</tr>
					</table>
					<? if ( $options_filters ): ?>
						<div class="sep"></div><? endif //options_filters?>
					<table border="0" cellpadding="2" cellspacing="0">
						<tr valign="middle">
							<? if ( $save_button ): ?>
								<td>
									<strong><a class="botoOpcionsGuardar" href="javascript:submit_form_save('save_rows_save_records<?= $action_add ?>')">
											<?= $c_send_edit ?>
										</a></strong></td>
							<? endif //save_button?>
							<? if ( $select_button ): ?>
								<td><?= $c_select ?>
									&nbsp;<strong><a class="opcions" href="javascript:select_all(true, document.list)">
											<?= $c_all ?>
										</a>&nbsp;-&nbsp;<a class="opcions" href="javascript:select_all(false, document.list)">
											<?= $c_nothing ?>
										</a></strong></td>
							<? endif //select_button?>
							<? if ( $delete_button ): ?>
								<? if ( $has_bin ): ?>
									<? if ( $is_bin ): ?>
										<td>
											<strong><a class="botoOpcionsBorrar" href="javascript:submit_list('save_rows_delete_selected<?= $action_add ?>', '<?= $c_confirm_deleted ?>', true)">
													<?= $c_delete_selected ?>
												</a></strong></td>
										<td>
											<strong><a class="botoOpcionsGuardar" href="javascript:submit_list('save_rows_restore_selected<?= $action_add ?>')">
													<?= $c_restore_selected ?>
												</a></strong></td>
									<? else: ?>
										<td>
											<strong><a class="botoOpcionsBorrar" href="javascript:submit_list('save_rows_bin_selected<?= $action_add ?>')">
													<?= $c_delete_selected ?>
												</a></strong></td>
									<? endif // is_bin?>
								<? else: ?>
									<td>
										<strong><a class="botoOpcionsBorrar" href="javascript:submit_list('save_rows_delete_selected<?= $action_add ?>', '<?= $c_confirm_deleted ?>', true)">
												<?= $c_delete_selected ?>
											</a></strong></td>
									<!--<td>
							<td>&nbsp;</td><strong><a class="botoOpcions" href="/admin?menu_id=<?= $menu_id ?>&sort_by=0&sort_order=<?= $sort_order0 ?>">
								<?= $c_move_to ?>
							
						<input type="submit" name="bin_selected" value="<?= $c_delete_selected ?>" class="boto1">
							</a></strong></td>-->
								<? endif // has_bin?>
							<? endif //delete_button?>
							<? if ( $print_button ): ?>
								<td><a id="list_print_button" class="botoOpcions botoOpcionsSub" href="<?= $print_link ?>">
											<?= $c_print_all ?>
											<div>
												<strong>es</strong>
												<ul>
													<?
													$languages =  &$GLOBALS['gl_languages'];

													foreach ( $languages['public'] as $k => $v ):?>
														<li onclick="set_print_language('<?= $v ?>','<?= $k ?>')"><?= $languages['names'][$v] ?></li>
													<? endforeach //$gl_languages['public']?>
												</ul>
											</div>
										</a></td>
							<? endif //print_button?>
							<? if ( $show_mls_button ): ?>
								<td>
									<strong><a title="<?= htmlspecialchars( $c_list_mls ) ?>" target="save_frame" class="botoMls active<?= ! empty ( $_SESSION['inmo_show_mls'] ) ?>" href="/admin/?menu_id=103&call=swap_mls">

										</a></strong></td>
							<? endif //$show_mls_button ?>

						</tr>
					</table>
				</td>
			</tr>
		</table>
	</div>
	<div class="sep"></div>
	<? //endif //$options_bar?>

	<? if ( $content_top ): ?>
		<div class="content_top">
			<? // TODO - Aquí va diferent que en el list.tpl, és per culpa que hi ha un altre formulari per fer el field-options ?>
			<?= $content_top ?>
		</div>
	<? endif //$content_top?>

	<? if ( $order_bar ): ?>
		<table width="100%" border="0" cellspacing="0" cellpadding="0" class="listRecord">
			<tr>
				<td class="listCaption">
					<table border="0" cellspacing="0" cellpadding="0" class="noPadding">
						<tr>
							<td valign="top" nowrap="nowrap"><strong>
									&nbsp;<?= $c_order_by ?>&nbsp;&nbsp;
								</strong></td>
							<td valign="top">
								<? foreach ( $fields as $l ): extract( $l ) ?><strong>
									<a class="opcions" href="<?= $order_link ?>">
										<?= $field ?>
									</a></strong>
									<? if ( $order_image ): ?>
										<img src="/admin/themes/inmotools/images/<?= $order_image ?>.gif" width="9" height="9" border="0" align="middle">
									<? endif //order_image?>
									&nbsp;
								<? endforeach //$field?></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<? if ( $field_options ): ?>
			<? // TODO - i Canviar el action ?>
			<form id="field-options" method="post">
				<? foreach ( $field_options as $l ): extract( $l ) ?>
					<label><?= $field_option_input ?> <?= $field_option_name ?></label>
				<? endforeach //$field-options?>
				<?= $field_option_button ?>
			</form>
		<? endif //$field_options ?>

	<? else: //$order_bar?>
		<table width="100%" border="0" cellspacing="0" cellpadding="0" class="listRecord">
			<tr>
				<td style="padding:0px"></td>
			</tr>
		</table>
	<? endif //$order_bar?>
	<form id="form-list" method="post" action="<?= $form_link ?>" name="list" onsubmit="return check_selected(this)" target="save_frame">
		<input name="action" type="hidden" value="save_rows"><?= $out_loop_hidden ?>
		<table width="100%" border="0" cellspacing="0" cellpadding="0" class="listRecord <?= $is_portal ? 'portal' : '' ?>">

			<? foreach ( $loop as $l ): extract( $l ) ?>
				<tr class="listItem<?= $odd_even ?>" onmousedown="change_bg_color(this, 'Down', '<?= $id ?>')" id="list_row_<?= $id ?>">
					<? if ( $has_images ): ?>
						<td valign="top" class="listImage" width="<?= $im_admin_thumb_w ?>">
							<? if ( $image_name ): ?>
								<? if ( $is_mls ): ?>
									<div class="no-dropzone">
										<img width="<?= $im_admin_thumb_w ?>" src="http://<?= $domain ?>/<?= $client_dir ?>/inmo/property/images/<?= $image_name_admin_thumb ?>">
									</div>
								<? else: // ?>
									<div><img width="<?= $im_admin_thumb_w ?>" src="<?= $image_src_admin_thumb ?>">
									</div>
								<? endif //$is_mls ?>
							<? else: // image_name?>
								<div class="noimage <?= $is_mls ? 'no-dropzone' : '' ?>" style="min-height:<?= $im_admin_thumb_h ?>px;"></div>
							<? endif // image_name?>
						</td>
					<? endif // $has_images ?>
					<td valign="top">
						<table width="100%" border="0" cellspacing="0" cellpadding="0" class="noPadding">
							<tr>
								<td>
									<table width="100%" border="0" cellpadding="0" cellspacing="0">
										<tr><? if ( $options_checkboxes ): ?>
												<td width="10">
												<input name="selected[<?= $conta ?>]" type="checkbox" value="<?= $id ?>" onclick="return false;"<?= $selected ?>/>
												</td><? endif // options_checkboxes?>
											<td class="listTitleProperty">

												<? if ( $is_portal ): ?>
													<div class="portal-checkboxes">
														<? foreach ( $portal_checkboxes as $l ): extract( $l ) ?>
															<label class="<?= $portal_class ?>">
																<img src="/admin/themes/inmotools/images/<?= $portal ?>.png" alt="<?= ucfirst ($portal) ?>"/>
																<div class="portal-checkbox"><?= $portal_checkbox ?></div>
															</label>
														<? endforeach //$protal_checkboxes?>

													</div>
													<? if ( $portal_messages ): ?>
													<div class="portal_messages_title"><?= $c_portal_check_title ?>:</div>
													<div class="portal_messages">
														<? foreach ( $portal_messages as $l ): extract( $l ) ?>
															<div>
																<strong><?= $portal ?>: </strong>
																<span>
																<? foreach ( $messages as $l ): extract( $l ) ?>
																	<?= $message ?><? if ( !$last_message ): ?>
																		,
																	<? endif // $last_message ?>
																<? endforeach //$messages?>
																</span>
															</div>
														<? endforeach //$portal_messages?>
													</div>
												<? endif // $portal_messages ?>
												<? endif // $is_portal ?>

												<input name="property_id[<?= $property_id ?>]" type="hidden" value="<?= $property_id ?>"/><? if ( $ref ): ?>
													<?= $ref ?>
												<? endif // $ref ?>
												<span class="private">
												<? if ( $property_private ): ?><? if ( $ref ): ?>
													-
												<? endif // $ref ?><?= $property_private ?><? endif //property_private?>
													<? if ( $address ): ?>
														<? if ( $adress || $numstreet || $block || $flat || $door ): ?>
															<span class="smaller <? if ( $property_private )
																echo "padding-left-2" ?>">
													<? if ( $adress ): ?><?= $adress ?><? endif ?><? if ( $numstreet ): ?>,&nbsp;<?= $numstreet ?><? endif //numstreet?><? if ( $block ): ?>,&nbsp;<?= $c_block ?><?= $block ?><? endif //block?><? if ( $flat ): ?>,&nbsp;<?= $flat ?><? endif //flat?><? if ( $door ): ?>-<?= $door ?><? endif //door?>
												</span>
														<? endif //tot?>
													<? endif // $address ?>
											</span></td>

											<? if ( $is_mls ): ?>

												<td class="mls-listTitle2 private">
													<? if ( $comercialname ): ?>
														<strong>
															<?= $c_comercialname ?> :
														</strong>
														<?= $comercialname ?>
													<? endif //$phone_1 ?>

													<? if ( $phone1 ): ?>
														<strong>
															<?= $c_phone1 ?> :
														</strong>
														<?= $phone1 ?>
													<? endif //$phone_1 ?>

													<strong>
														<?= $c_comission ?> :
													</strong>
													<?= $comission ?> %
												</td>

											<? endif //$is_mls ?>

											<td align="right">
												<?= $entered ?>&nbsp;</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td>
									<table width="99%" border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td valign="top">
												<table width="100%" border="0" cellpadding="0" cellspacing="0">
													<tr>
														<td height="30" valign="top" class="listTitleProperty2"><? if ( $category_id ): ?>
																<?= $category_id ?>
															<? endif // $category_id ?><? if ( $tipus ): ?>
																<? if ( $category_id ): ?> | <? endif //$ ?>
																<?= $tipus ?>
															<? endif // $tipus ?><? if ( $floor_space ): ?>
																<? if ( $tipus ): ?> | <? endif //$ ?>
																<?= $floor_space ?>
																m2
															<? endif // $floor_space ?>
															<? if ( $person && $tipus_value == 'temp' ): ?>
																<? if ( $floor_space ): ?> | <? endif //$ ?>
																<?= $person ?> <?= $person == 1 ? $c_person_s : $c_person_p ?>
															<? endif ?></td>
														<td height="30" align="right" valign="top" class="listResaltat"><? if ( $price ): ?>
																<?= $price ?>
																&nbsp;<?= CURRENCY_NAME ?>
															<? endif // $price ?></td>
													</tr>
												</table>
												<table width="100%" border="0" cellpadding="0" cellspacing="0" class="listDetails2">
													<tr>
														<td valign="top"><strong class="listItem">
																<?= $c_tipus2 ?>:</strong>&nbsp;</td>
														<td valign="top"><?= $tipus2 ?></td>
														<td valign="top"><strong class="listItem">
																<?= $c_municipi_id ?>:</strong>&nbsp;</td>
														<td valign="top"><?= $municipi_id ?></td>
													</tr>
													<tr>
														<td valign="top"><strong class="listItem">
																<?= $c_room ?>:</strong>&nbsp;</td>
														<td valign="top"><? if ( $room ): ?>
																<?= $room ?>
															<? else: //?>
																-
															<? endif //room?></td>
														<td valign="top"><strong class="listItem">
																<?= $c_land ?>:</strong></td>
														<td valign="top"><? if ( $land ): ?>
																<?= $land ?>
																&nbsp;
																<?= $land_units ?>
																&nbsp;
															<? else: //?>
																-
															<? endif //land?></td>
													</tr>
													<tr>
														<td valign="top"><strong class="listItem">
																<?= $c_bathroom ?>:</strong>&nbsp;</td>
														<td valign="top"><?= $bathroom ?></td>
														<td valign="top"><strong class="listItem">
																<?= $c_courtyard ?>&nbsp;/&nbsp;<?= $c_garden ?>
																:</strong></td>
														<td valign="top"><?= $courtyard ?>
															&nbsp;/&nbsp;<?= $garden ?></td>
													</tr>
													<tr>
														<td valign="top"><strong class="listItem">
																<?= $c_wc ?>:</strong>&nbsp;</td>
														<td valign="top"><?= $wc ?></td>
														<td valign="top"><strong class="listItem">
																<?= $c_status ?>:</strong>&nbsp;</td>
														<td valign="top"><?= $status ?></td>
													</tr>
												</table>
											</td><? if ( $description ): ?>
											<td width="300" valign="top" class="listDescription">
												<div style="height:30px;"></div>
												<div style="padding-bottom:8px;">
														<?= $description ?></div>
											</td>
											<? endif // $description ?>
										</tr>
									</table>

									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td align="left">
												<? if ( $user_id ): ?>
													<strong><?= $c_user_id ?></strong>: <?= $user_id ?><? endif //user_id?>
											</td>
											<td align="right">
												<div class="right">
													<? if ( $is_mls ): ?>
														<a class="boto1" href="javascript:show_mls_property('<?= $property_id_client ?>','<?= $menu_id ?>','<?= HTTP ?><?= $domain ?>','<?= LANGUAGE ?>')"><?= $c_view_property ?>
														</a>
													<? else: // is_mls ?>


														<? foreach ( $buttons as $button ): ?>
															<?= $button ?>
														<? endforeach //$buttons?><? /* <a class="boto-twitter" href="#"></a><a class="boto-facebook" href="#"></a> */ ?>
													<? endif //$is_mls ?>
												</div>
											</td>
										</tr>
										<tr>
											<td>
												<img src="/admin/themes/inmotools/images/spacer.gif" width="1" height="10">
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			<? endforeach //$loop?>
		</table>
	</form>
	<? if ( $order_bar ): ?>
		<table width="100%" border="0" cellspacing="0" cellpadding="0" class="listRecord">
			<tr>
				<td class="listCaption">
					<table border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td valign="top" nowrap="nowrap"><strong>
									&nbsp;<?= $c_order_by ?>&nbsp;&nbsp;
								</strong></td>
							<td valign="top">
								<? foreach ( $fields as $l ): extract( $l ) ?><strong>
									<a class="opcions" href="<?= $order_link ?>">
										<?= $field ?>
									</a></strong>
									<? if ( $order_image ): ?>
										<img src="/admin/themes/inmotools/images/<?= $order_image ?>.gif" width="9" height="9" border="0" align="middle">
									<? endif //order_image?>
									&nbsp;
								<? endforeach //$field?></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	<? endif //$order_bar?>
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td height="10"></td>
		</tr>
	</table>
	<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr valign="middle">
			<td width="20%" align="left" nowrap class="pageResults"><?= $split_results_count ?></td>
			<td align="right" nowrap class="pageResults"><?= $split_results ?></td>
		</tr>
	</table>
<? endif //results?>