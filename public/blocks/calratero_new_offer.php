<?php
$news_id = '';
$query = "SELECT entered,news__new.new_id AS new_id,new,subtitle,content,category_id,status
		FROM news__new
		LEFT OUTER JOIN news__new_language using (new_id)
		WHERE news__new.bin = 0
		AND news__new_language.language = '".LANGUAGE."'
		AND status = 'public'
		AND category_id = 29
		ORDER BY entered DESC
		LIMIT 0,2";

$results = Db::get_rows($query);
foreach ($results as $key=>$value){
	$results[$key]['content'] = add_dots('content',$results[$key]['content']);
}
$loop = $results;

?>