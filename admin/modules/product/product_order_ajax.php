<?

/**
 * ProductOrderAjax
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2011
 * @version $Id$
 * @access public
 */
class ProductOrderAjax {


	static public function get_checkboxes_results_products( $is_reserva = false, $is_product_search = false ) {

		$limit = $is_product_search ? 40 : 100;

		$q            = R::escape( 'query' );
		$q            = strtolower( $q );
		$id           = R::id( 'id' );
		$selected_ids = R::get( 'selected_ids' );

		if ( $selected_ids ) {
			R::escape_array( $selected_ids );
			$selected_ids = implode( ',', $selected_ids );
		}

		$ret                = array();
		$ret['suggestions'] = array();

		$r = &$ret['suggestions'];


		$order    = 'status asc, product_title ASC,ordre ASC';
		$language = LANGUAGE;

		$exclude_selected_ids = $selected_ids ? " AND ( product__product.product_id NOT IN ($selected_ids))" : '';
		$exclude_saved_ids    = $id ? " AND ( product__product.product_id NOT IN (SELECT product_id FROM lassdive__centreproduct WHERE product_id = $id))" : '';

		$is_reserva_query = $is_reserva ? " AND product__product.product_id IN ( SELECT DISTINCT product_id FROM product__orderitem )" : '';

		$family_query = "(
			SELECT product__family_language.family FROM product__family_language 
			WHERE product__family_language.family_id = product__product.family_id 
			AND language =  '$language') AS family";

		$subfamily_query = "(
			SELECT product__family_language.family FROM product__family_language 
			WHERE product__family_language.family_id = product__product.subfamily_id 
			AND language =  '$language') AS subfamily";

		$family_subquery = '
			SELECT product__family_language.family_id FROM product__family_language 
			WHERE family';

		$variations_subquery_tpl = "
			SELECT product__product_to_variation.product_id AS product_id FROM product__product_to_variation WHERE variation_id IN (
				SELECT product__variation_language.variation_id FROM product__variation_language 
				WHERE variation LIKE '%s' %s	
			)";


		$common_query_1 = "
					SELECT product__product.product_id as product_id, $family_query, $subfamily_query, product_title, product_subtitle, ref, family_id, subfamily_id, subsubfamily_id
					FROM product__product, product__product_language";
		$common_query_2 = "product__product.product_id = product__product_language.product_id
			            AND 
			            language = '$language'
			            AND 
			            bin = 0";
		$common_query_3 =
			"
		            $exclude_selected_ids
		            $exclude_saved_ids
		            $is_reserva_query
					ORDER BY $order";

		$ret                = array();
		$ret['suggestions'] = array();

		$r = &$ret['suggestions'];

		// 1 - Que comenci
		$variations_subquery = sprintf($variations_subquery_tpl, "$q%", '');
		$query = "
					$common_query_1
					WHERE
				        (
				            $common_query_2
			            ) 
				        AND
				        (
				            product_title LIKE '$q%'
							OR product_subtitle LIKE '$q%'
							OR ref LIKE '$q%'
							OR family_id IN ($family_subquery LIKE '$q%')
							OR subfamily_id IN ($family_subquery LIKE '$q%')
							OR product__product.product_id IN ($variations_subquery)
						)
					$common_query_3
					LIMIT $limit";

		$results = Db::get_rows( $query );

		$rest = $limit - count( $results );

		// 2 - Qualsevol posició
		if ( $rest > 0 ) {

			$variations_subquery = sprintf($variations_subquery_tpl, "%$q%", "AND variation NOT LIKE '$q%'");
			$query = "
						$common_query_1
						WHERE
					        (
					            $common_query_2
				            )
					        AND	
					        (					 
								( 
									product_title LIKE '%$q%'
				                    AND product_title NOT LIKE '$q%' 
				                )
								OR 
								( 
									family_id IN (
									$family_subquery LIKE '%$q%'
									AND family NOT LIKE '$q%' 
									)
				                )
								OR 
								( 
									subfamily_id IN (
									$family_subquery LIKE '%$q%'
									AND family NOT LIKE '$q%' 
									)
				                )
								OR 
								( 
									product__product.product_id IN (
									$variations_subquery
									)
				                )
								OR
								 ( 
								    ref LIKE '%$q%'
				                    AND ref NOT LIKE '$q%' 
				                )
				            )
						$common_query_3
						LIMIT $rest";

			$results2 = Db::get_rows( $query );

			$results = array_merge( $results, $results2 );

		}

		$rest = $limit - count( $results );

		$words = array();
		// 3 - Parteixo paraules
		if ( $rest > 0 ) {

			$words = explode( ' ', $q );
			if ( count( $words ) > 1 ) {

				$words_query_title    = '';
				$words_query_subtitle = '';

				foreach ( $words as $word ) {
					$words_query_title    .= "AND product_title LIKE '%$word%'";
					$words_query_subtitle .= "AND product_subtitle LIKE '%$word%'";
				}

				$query = "
						$common_query_1
						WHERE
					        (
					            $common_query_2
				            )
					        AND	
                            (
								( 
									product_title NOT LIKE '%$q%'
				                    AND product_title NOT LIKE '$q%' 
				                    $words_query_title
				                )
								OR 
								( 
									product_subtitle NOT LIKE '%$q%'
				                    AND product_subtitle NOT LIKE '$q%' 
				                    $words_query_subtitle
				                )
			                )
						$common_query_3
						LIMIT $rest";

				$results2 = Db::get_rows( $query );

				$results = array_merge( $results, $results2 );
			}

		}

		// Presento resultats
		foreach ( $results as $rs ) {


			$ref                = str_replace( $q, "<strong>$q</strong>", $rs['ref'] );
			$product_title_text = $rs['product_title'];
			$product_title      = str_replace( $q, "<strong>$q</strong>", $rs['product_title'] );
			$product_subtitle   = str_replace( $q, "<strong>$q</strong>", $rs['product_subtitle'] );
			$family             = str_replace( $q, "<strong>$q</strong>", $rs['family'] );
			$subfamily          = str_replace( $q, "<strong>$q</strong>", $rs['subfamily'] );
			$product_id         = $rs['product_id'];

			if ( count( $words ) > 1 ) {
				foreach ( $words as $word ) {
					$ref              = str_replace( $word, "<strong>$word</strong>", $ref );
					$product_title    = str_replace( $word, "<strong>$word</strong>", $product_title );
					$product_subtitle = str_replace( $word, "<strong>$word</strong>", $product_subtitle );
					$family           = str_replace( $word, "<strong>$word</strong>", $rs['family'] );
					$subfamily        = str_replace( $word, "<strong>$word</strong>", $rs['subfamily'] );
				}
			}

			$familys = '';
			if ( $family ) {
				$familys = $family;
			}
			if ( $subfamily ) {
				$familys .= " > $subfamily";
			}
			if ( $familys ) {
				$familys = "<h5>$familys</h5>";
			}

			if ( $is_product_search ) {

				$last_family_id = self::get_last_family_id( false, $rs );

				if ( $subfamily ) $subfamily = "> $subfamily";

				$custom = <<< EOF
					<div class="ref">
						 $ref 
					</div>
					<div class="product-title">
						 $product_title 
						<div class="product-subtitle"> $product_subtitle </div>
					</div>
					<div class="product-family">
						 $family $subfamily
					</div>
					<div><a class="more"></a></div>
EOF;
			}
			else {
				$last_family_id = 0;
				$custom         = "<div class='custom'><i>Ref. $ref</i><h6>$product_title</h6>$familys<p>$product_subtitle</p></div>";
			}


			$r[] = array(
				'value'          => $product_title_text,
				'custom'         => $custom,
				'last_family_id' => $last_family_id,
				'data'           => $rs['product_id']
			);
		}


		$rest               = $limit - count( $results );
		$ret['is_complete'] = false; // Aquest mai es complert, ja que el jscript no pot buscar paraules separades, i llavors perdo la funcionalitat // $rest>0;
		$ret['has_custom']  = true;

		if ( ! DEBUG ) header( 'Content-Type: application/json' );

		Debug::p_all();
		die ( json_encode( $ret ) );


	}




	static function get_last_family_id( $last_family_id = false, &$rs = [] ) {

		$language = LANGUAGE;

		if ( $last_family_id ) {

		}
		else if ( $rs['subsubfamily_id'] )
			$last_family_id = $rs['subsubfamily_id'];
		else if ( $rs['subfamily_id'] )
			$last_family_id = $rs['subfamily_id'];
		else
			$last_family_id = $rs['family_id'];


		// TODO-i Posar be aquest enllaç
		$last_family_text_id = Db::get_first( "
			SELECT activitat_file_name 
			FROM lassdive__activitat_language 
			WHERE language = '$language'
			AND activitat_id = {$last_family_id}" );

		return $last_family_text_id ? $last_family_text_id : $last_family_id;
	}


}