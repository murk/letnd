<?php
global $gl_db_classes_table, $gl_db_classes_id_field, $gl_db_max_results;
$gl_db_classes_table = 'all__menu';
$gl_db_classes_id_field = 'menu_id';
$gl_db_max_results = "10000000";

function get_group_condition()
{
// no es veu el propi grup, així no es pot capar els permisos a un mateix i que despres no pugui entrar
    $group_condition = $_SESSION['group_id'] > 2?3:$_SESSION['group_id'];
    $group_condition = 'group_id >' . $group_condition;
    return $group_condition;
}

// salva un llistat
function save_rows()
{
    // per tenir el id del usuari
    check_user_group_if_exist();

    $save_rows = new SaveRows;
    $save_rows->call_function['write1'] = 'get_concession_value_write';
    $save_rows->call_function['read1'] = 'get_concession_value_read';

    $save_rows->save();

    save_root_concession();
}
// la classe mateixa ja li passa els valors de la funcio
function get_concession_value_write($id, $value)
{

    $query = "SELECT write1, tool_section, tool FROM all__menu where menu_id = " . $id;
    $results = Db::get_rows($query);
    $write1 = $results[0] ['write1'];
    // si el valor és 0 el treiem de la consulta
    if ($value == 0)
    {
        $write1 = '\'' . str_replace ($_GET['group_id'] . ',', '', $write1) . '\'';
    }
    else
    {
        // tornme la mateixa cadena pero ens saltem el group_id
        if (strstr ($write1, ',' . $_GET['group_id'] . ','))
        {
            $write1 = '\'' . $write1 . '\'';
        }
        else
        {
            $write1 = '\'' . $write1 . $_GET['group_id'] . ',\'';
        }
    }
    // també dono permisos a tots els menus de la secció
    $query = "UPDATE all__menu SET write1 = " . $write1 . "
    			WHERE tool_section = '" . $results[0] ['tool_section'] . "'
    			AND tool = '" . $results[0] ['tool'] . "'";
    Db::execute($query);

    Debug::add('Write1 post', $write1);
    return $write1;
}

function get_concession_value_read($id, $value)
{
    $query = "SELECT read1, tool_section, tool FROM all__menu where menu_id = " . $id;
    $results = Db::get_rows($query);
    $read1 = $results[0] ['read1'];
    // si el valor és 0 el treiem de la consulta
    if ($value == 0)
    {
        $read1 = '\'' . str_replace ($_GET['group_id'] . ',', '', $read1) . '\'';
    }
    else
    {
        // tornme la mateixa cadena pero ens saltem el group_id
        if (strstr ($read1, ',' . $_GET['group_id'] . ','))
        {
            $read1 = '\'' . $read1 . '\'';
        }
        // sino afegim pero abans comprovem que no hi sigui per si el cabron fa f5
        else
        {
            $read1 = '\'' . $read1 . $_GET['group_id'] . ',\'';
        }
    }
    // també dono permisos a tots els menus de la secció
    $query = "UPDATE all__menu SET read1 = " . $read1 . "
    			WHERE tool_section = '" . $results[0] ['tool_section'] . "'
    			AND tool = '" . $results[0] ['tool'] . "'";
    Db::execute($query);

    Debug::add('Read1 post', $read1);
    return $read1;
}

// als permisos de l'arrel s'inclouen tots els grups que surten almenys un cop en algun submenu
function save_root_concession()
{

    $query = "SELECT menu_id FROM all__menu WHERE parent_id = 0";
    $results = Db::get_rows($query);

    // bucle per menus de l'arrel
    foreach($results as $rs)
    {
        $query = "SELECT write1,read1 FROM all__menu WHERE parent_id = " . $rs['menu_id'];
        $results2 = Db::get_rows($query);

        $write_root = array();
        $read_root = array();
        // bucle per menus de 2 nivell del menu de l'arrel per on passa el bucle
        foreach($results2 as $rs2)
        {
            // fem un array dels permisos d'aquest menu extraient primer la primera i ultima coma
			$write_array = explode(',', substr($rs2['write1'], 1 , strlen($rs2['write1'])-2));
			// loop per l'array de permisos i sino hi es l'afegim als permisos de l'arrel
            foreach($write_array as $group)
            {
                if (!in_array($group, $write_root))
                {
                    array_push($write_root, $group);
                }
            }
            // fem un array dels permisos d'aquest menu extraient primer la primera i ultima coma
            $read_array = explode(',', substr($rs2['read1'], 1 , strlen($rs2['read1'])-2));
			// loop per l'array de permisos i sino hi es l'afegim als permisos de l'arrel
            foreach($read_array as $group)
            {
                if (!in_array($group, $read_root))
                {
                    array_push($read_root, $group);
                }
            }
        }
        // updatejo permisos pel menu arrel actual del bucle
        $query = "UPDATE all__menu
					SET write1 = '," . implode(',', $write_root) . ",',
        				read1 = '," . implode(',', $read_root) . ",'
    				WHERE menu_id = " . $rs['menu_id'];
		Debug::add('Query cambiar permisos',$query);
        Db::execute($query);
    }
}

function check_user_group_if_exist()
{
    if (!isset($_GET['group_id']))
    {
        $query = "SELECT group_id FROM user__group WHERE " . get_group_condition();
        $results = Db::get_rows($query);
        $_GET['group_id'] = $results [0]['group_id'];
    }
}

function list_records()
{
    global $gl_content;
    check_user_group_if_exist();
	
	// NO ESTÁ BE, nomes fins a 20 grups
	
	// hauria de mirar, si en el menu hi ha qualsevol major de 2 o major de 3 en el segon cas
	// de moment nomes ho faig amb 20 grups, apartir de 20 no va be
	
	$group_condition = $_SESSION['group_id'] > 2?3:$_SESSION['group_id'];
	if ($group_condition==1){
		$group_condition = '';
	}
	elseif ($group_condition==2){
		$group_condition = " 
			AND  (
				write1 LIKE '%,2,%' OR
				write1 LIKE '%,3,%' OR
				write1 LIKE '%,4,%' OR
				write1 LIKE '%,5,%' OR
				write1 LIKE '%,6,%' OR
				write1 LIKE '%,7,%' OR
				write1 LIKE '%,8,%' OR
				write1 LIKE '%,9,%' OR
				write1 LIKE '%,10,%' OR
				write1 LIKE '%,11,%' OR
				write1 LIKE '%,12,%' OR
				write1 LIKE '%,13,%' OR
				write1 LIKE '%,14,%' OR
				write1 LIKE '%,15,%' OR
				write1 LIKE '%,16,%' OR
				write1 LIKE '%,17,%' OR
				write1 LIKE '%,18,%' OR
				write1 LIKE '%,18,%' OR
				write1 LIKE '%,20,%'		
			)";
	}
	elseif ($group_condition==3){
		$group_condition = " 
			AND  (
				write1 LIKE '%,3,%' OR
				write1 LIKE '%,4,%' OR
				write1 LIKE '%,5,%' OR
				write1 LIKE '%,6,%' OR
				write1 LIKE '%,7,%' OR
				write1 LIKE '%,8,%' OR
				write1 LIKE '%,9,%' OR
				write1 LIKE '%,10,%' OR
				write1 LIKE '%,11,%' OR
				write1 LIKE '%,12,%' OR
				write1 LIKE '%,13,%' OR
				write1 LIKE '%,14,%' OR
				write1 LIKE '%,15,%' OR
				write1 LIKE '%,16,%' OR
				write1 LIKE '%,17,%' OR
				write1 LIKE '%,18,%' OR
				write1 LIKE '%,18,%' OR
				write1 LIKE '%,20,%'		
			)";
	}
	
    // die();
    // count($results); = mysqlfecthassoc
    $listing = new ListRecords;
	
	
	$listing->set_options(1, 0, 1, 0, 0, 0, 0); // set_options ($options_bar = 1, $options_checkboxes = 1, $save_button = 1, $select_button = 1, $delete_button = 1, $print_button = 1, $edit_buttons = 1)

    // metode call primer cridem la funcio i li passem les variables i el crido per tots els registres, un cop cada vegada del bucle
    $listing->call('list_records_walk', 'tool,tool_section,variable,menu_group');
    // propietat call_function li passem el camp i cridem la funcio amb 2 parametres nom del camp i valor del camp
    $listing->call_function ['read1'] = 'get_check_read_write_if_is_possible';
    $listing->call_function ['write1'] = 'get_check_read_write_if_is_possible';
	$listing->group_fields = array('menu_group');
    $listing->group_by = "menu_group, tool_section";
    $listing->order_by = "menu_group ASC, menu_id, tool_section";
    $listing->condition = "
		tool_section <> '' 
		AND parent_id <> '0' 
		AND tool<> 'admintotal' 
		AND toolmode_id<>0 " . 
		$group_condition;
		
	$listing->paginate = false;
    $listing->list_records();
    // sumo tpl's
    $gl_content = get_search_content() . $gl_content;
}
function get_check_read_write_if_is_possible ($name, $value)
{
    if (strstr($name, 'write1'))
    {
        $read_name = str_replace('write1', 'read1', $name);
        $javascript = 'change_write(\'' . $read_name . '\',\'' . $name . '\');';
    }
    if (strstr($name, 'read1'))
    {
        $write_name = str_replace('read1', 'write1', $name);
        $javascript = 'change_read(\'' . $write_name . '\',\'' . $name . '\');';
    }
    // si existeix el id, seleccionat sinó res
    $checked = (strstr ($value, ',' . $_GET['group_id'] . ',')) ? 'checked="checked"' :"";
    $old = (strstr ($value, ',' . $_GET['group_id'] . ',')) ? '1' :'0';
    $r = '<input name="' . $name . '" id="' . $name . '" type="checkbox" value="1" ' . $checked . ' onclick="' . $javascript . '">';
    $r .= '<input name="old_' . $name . '" type="hidden" value="' . $old . '">';
    return $r;
}

function get_search_content ()
{
    // agafo el tpl
    global $tpl, $gl_caption, $gl_menu_id;
    $query = "SELECT group_id, name FROM user__group where " . get_group_condition();
    $results = Db::get_rows($query);
    $html = "";
    foreach ($results as $rs)
    {
        // la linia d'abaix es un if else.
        // ex. if ( $rs ['group_id'] == $_GET['group_id']) {"selected"} else {""};
        $checked = $rs ['group_id'] == $_GET['group_id'] ? " selected" : "";
        $html .= '<option value="' . $rs ['group_id'] . '"' . $checked . '>' . $rs ['name'] . '</option>';
    }
    $template = 'user/user_concession.tpl';
    $tpl->set_file($template);
    // POSSO EL menu_id i el section per posar-lo al from, aixi al fer el llistat segueixes a aquest menu
    $tpl->set_var('menu_id', $gl_menu_id);
    // set_var = agafa una sola variable // set_loop = fa un loop
    $tpl->set_var('groups', $html);
    // set_vars = assigno un array i transforma en variables
    $tpl->set_vars($gl_caption);
    return $tpl->process();
}

function list_records_walk($tool, $tool_section, $variable, $menu_group)
{
    // eval('$r[\'tool_section\'] = ' . $variable . ';');
    include_once(DOCUMENT_ROOT . 'admin/modules/' . $tool . '/languages/' . LANGUAGE . '.php');
    $r['tool_section'] = constant(strtoupper($tool . '_MENU_' . $tool_section));
    $r['tool'] = $r['menu_group'] = constant(strtoupper($menu_group) . '_MENU_ROOT');
	
	if (defined(strtoupper($tool . '_COMMENT_' . $tool_section))) 
		$r['comment'] = constant(strtoupper($tool . '_COMMENT_' . $tool_section));
    return $r;
}

function show_form()
{
    $show = new ShowForm;
    $show->show_form();
}
// salva un formulari
function write_record()
{
    $writerec = new SaveRows;
    $writerec->save();
}

function manage_images()
{
    $image_manager = new ImageManager;
    $image_manager->execute();
}

?>