<?
/**
 * __Tool__Section
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2010
 * @version $Id$
 * @access public
 */
class AdmintotalMenu extends Module{

	var $menu_ids;
	var $selected_tool;
	function __construct(){
		parent::__construct();
		$this->table = 'all__menu';
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function get_records()
	{
	    if (!isset($_GET['menu_group'])) $_GET['menu_group'] = 'admintotal'; // forço filtrar per admintotal per defecte
		$listing = new ListRecords($this);
		
		$listing->paginate = false;
		
		$listing->add_field('marge');
		$listing->swap_fields('write1', 'marge');
		$listing->set_field('marge','type','none');
		$listing->set_field('marge','list_admin','out');
		$listing->caption['c_marge'] = '';
		
		$listing->add_filter('menu_group');
		$listing->add_filter('parent_id');
		$listing->add_filter('tool2');
		$listing->add_filter('tool_section2');		
		
		$this->get_menu_ids(0);
		$menu_ids = implode($this->menu_ids,',');
		$listing->condition = 'menu_id IN ('.$menu_ids.')';
        $listing->condition .= "ORDER BY FIELD(menu_id, " . $menu_ids . ")";
		
		$listing->call('list_records_walk','menu_id,variable,parent_id,tool2',true);
		
	    $ret = $listing->list_records();
		
		//$ids = implode_field($listing->results, 'menu_id', $glue = ',');
		
		$GLOBALS['gl_news'] .= "INSERT INTO `all__menu` <br>(`menu_id`, `menu_group`, `tool`, `tool_section`, `action`, `parent_id`, `toolmode_id`, `variable`, `ordre`, `link`, `process`, `read1`, `write1`) VALUES<br>";
		foreach ($listing->results as $rs){
			extract($rs);
			$GLOBALS['gl_news'] .= "('$menu_id', '$menu_group', '$tool2', '$tool_section2', '$action2', '$parent_id', '1', '$variable', '$ordre', '$link', '$process2', '$read1', '$write1'),<br>";
		}
		$GLOBALS['gl_news'] = substr($GLOBALS['gl_news'], 0, -5);
		
		return $ret;
	}
	function list_records_walk(&$listing,$menu_id,$variable,$parent_id,$tool2){
		$style = '';
		
		if ($parent_id==0) {
			$level = 1;
		}elseif (Db::get_first("SELECT count(*) FROM all__menu WHERE menu_id = ".$parent_id." AND parent_id=0"))
		{
			$level = 2;
		}
		elseif (Db::get_first("SELECT menu_id, parent_id AS parent FROM all__menu WHERE menu_id = ".$parent_id." AND parent_id IN 
								(SELECT menu_id FROM all__menu WHERE menu_id = parent AND parent_id=0)"))
		{
			$level = 3;		
		}
		else
		{
			$level = 4;		
		}
		
		if ($level == 1) $style = "font-weight:bold;";
		if ($level > 2) $style = "color:#5e6901;";
		$key = $parent_id . '_' . $level;
		if (!isset($this->levels_count[$key])) {
			$this->levels_count[$key] = 0;
		}
		$this->levels_count[$key]++;
		
		include_once(DOCUMENT_ROOT . 'admin/modules/'.$tool2.'/languages/'.LANGUAGE.'.php');
		$ret['marge'] = '';
		if ($variable){
		$ret['marge'] = '<div style="margin-left:'.(30*($level-1)).'px;'.$style.'">'.$this->levels_count[$key].' - ' .constant($variable).'</div>';
		}
		
		//$listing->buttons['edit_page_button']['show'] = ($has_content==1);
		
		return $ret;
	}
	function get_menu_ids($parent_id){
		$loop = array();
		$query = "SELECT menu_id
						FROM all__menu
						WHERE parent_id = " . $parent_id . "
						AND bin <> 1
						ORDER BY ordre ASC, menu_id ASC";
		//Debug::add($query,'Query get_page_recursive');	
		$results = Db::get_rows($query);

		if ($results)
		{
			foreach($results as $rs)
			{
				$this->menu_ids[]=$rs['menu_id'];
				$this->get_menu_ids($rs['menu_id']);
			}
		}
	}
	
	
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{
	    $show = new ShowForm($this);
	    return $show->show_form();
	}

	function save_rows()
	{
	    $save_rows = new SaveRows($this);
	    $save_rows->save();
	}

	function write_record()
	{
	    $writerec = new SaveRows($this);
	    $writerec->save();
	}

	function manage_images()
	{
	    $image_manager = new ImageManager($this);
	    $image_manager->execute();
	}
}
?>