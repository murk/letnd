<script language="JavaScript" src="/admin/jscripts/validacio.js?v=<?= $version ?>"></script>


<? if(!$edit_only_descriptions): ?>
	<script language="JavaScript" src="/admin/modules/inmo/jscripts/chained_selects.js?v=<?= $version ?>"></script>
<? endif //edit_only_descriptions?>
<script language="JavaScript" src="/admin/modules/inmo/jscripts/functions.js?v=<?= $version ?>"></script>
<script language="JavaScript" src="/common/jscripts/jquery.dropshadow.js?v=<?= $version ?>"></script>
<? if($image_name): ?>
	<script type="text/javascript">

		var gl_gallery_images = <?= ImageManager::get_gallery_images($images) ?>;

		gl_default_imatge = '<?=$images[0]['image_name_medium']?>';
	</script>
<? endif // image_name?>
<script type="text/javascript">
	$(function () {
		set_focus(document.theForm);
	});
	gl_currency_change = <?=CURRENCY_CHANGE?>;
</script>
<? if(CURRENCY2 == 'PTS'): ?>
	<script language="JavaScript" src="/admin/jscripts/euro_calc.js?v=<?= $version ?>"></script>
<? else: // CURRENCY2?>
	<script language="JavaScript" src="/admin/jscripts/currency_calc.js?v=<?= $version ?>"></script>
<? endif // CURRENCY2?>
<div class="subMenu" id="showwindow" style="width:230px">
	<form action="/admin/?action=show_record_showwindow&property_id=<?= $property_id ?>&menu_id=202" target="_blank" method="post">
		<table width="220" border="0" cellpadding="0" cellspacing="0">
			<? if($showwindow_template_id): ?>
				<tr>
					<td valign="top"><strong>
							<?= $c_submenu_6 ?>:</strong></td>
					<td><?= $showwindow_template_id ?></td>
				</tr>
			<? endif //showwindow_template_id?>
			<tr>
				<td valign="top"><strong>
						<?= $c_submenu_0 ?>:</strong></td>
				<td><?= $showwindow_language ?></td>
			</tr>
			<!--<tr>
				<td valign="top"><strong>
					<?= $c_submenu_1 ?>:</strong></td>
				<td><?= $showwindow_size ?></td>
			</tr>-->
			<tr>
				<td valign="top"><strong>
						<?= $c_submenu_2 ?>:</strong></td>
				<td><?= $showwindow_pages ?></td>
			</tr>
			<tr>
				<td valign="top"><strong>
						<?= $c_submenu_3 ?>:</strong></td>
				<td><?= $showwindow_top_fields ?></td>
			</tr>
			<tr>
				<td valign="top"><strong>
						<?= $c_submenu_4 ?>:</strong></td>
				<td>&nbsp;</td>
			</tr>
		</table>
		<table width="200" border="0" align="center" cellpadding="0" cellspacing="0">
			<tr>
				<td><?= $c_zone ?></td>
				<td><?= $showwindow_show_zone ?></td>
				<td><?= $c_comarca_id ?></td>
				<td><?= $showwindow_show_comarca ?></td>
			</tr>
			<tr>
				<td><?= $c_municipi_id ?></td>
				<td><?= $showwindow_show_municipi ?></td>
				<td><?= $c_provincia_id ?></td>
				<td><?= $showwindow_show_provincia ?></td>
			</tr>
		</table>
		<br/>
		<table border="0" align="center" cellpadding="0" cellspacing="0">
			<tr>
				<td><input type="checkbox" name="save_config" id="save_config"/></td>
				<td>&nbsp;
					<?= $c_submenu_5 ?></td>
			</tr>
		</table>
		<br/>

		<div class="centerFloat1">
			<div class="centerFloat2">
				<input type="submit" name="Submit" value="<?= $c_print ?>" class="btn_form1"/>
			</div>
		</div>
	</form>
</div>
<div class="subMenu" id="album" style="width:230px">
	<form action="/admin/?action=show_record_album&property_id=<?= $property_id ?>&menu_id=202" target="_blank" method="post">
		<table width="220" border="0" cellpadding="0" cellspacing="0">
			<? if($album_template_id): ?>
				<tr>
					<td valign="top"><strong>
							<?= $c_submenu_6 ?>:</strong></td>
					<td><?= $album_template_id ?></td>
				</tr>
			<? endif //album_template_id?>
			<tr>
				<td valign="top"><strong>
						<?= $c_submenu_0 ?>:</strong></td>
				<td><?= $album_language ?></td>
			</tr>
			<!--<tr>
				<td valign="top"><strong>
					<?= $c_submenu_1 ?>:</strong></td>
				<td><?= $album_size ?></td>
			</tr>-->
			<tr>
				<td valign="top"><strong>
						<?= $c_submenu_2 ?>:</strong></td>
				<td><?= $album_pages ?></td>
			</tr>
			<tr>
				<td valign="top"><strong>
						<?= $c_submenu_4 ?>:</strong></td>
				<td>&nbsp;</td>
			</tr>
		</table>
		<table width="200" border="0" align="center" cellpadding="0" cellspacing="0">
			<tr>
				<td><?= $c_zone ?></td>
				<td><?= $album_show_zone ?></td>
				<td><?= $c_comarca_id ?></td>
				<td><?= $album_show_comarca ?></td>
			</tr>
			<tr>
				<td><?= $c_municipi_id ?></td>
				<td><?= $album_show_municipi ?></td>
				<td><?= $c_provincia_id ?></td>
				<td><?= $album_show_provincia ?></td>
			</tr>
		</table>
		<br/>
		<table border="0" align="center" cellpadding="0" cellspacing="0">
			<tr>
				<td><input type="checkbox" name="save_config" id="save_config"/></td>
				<td>&nbsp;
					<?= $c_submenu_5 ?></td>
			</tr>
		</table>
		<br/>

		<div class="centerFloat1">
			<div class="centerFloat2">
				<input type="submit" name="Submit" value="<?= $c_print ?>" class="btn_form1"/>
			</div>
		</div>
	</form>
</div>
<form name="form_delete" method="post" action="<?= $link_action_delete ?>" target="save_frame">
	<input name="<?= $id_field ?>" type="hidden" value="<?= $id ?>">
</form>
<form name="theForm" method="post" action="<?= $link_action ?>" onsubmit="<?= $js_string ?>" encType="multipart/form-data" target="save_frame">
	<input name="property_id[<?= $property_id ?>]" type="hidden" value="<?= $property_id ?>"/>
	<input name="property_id_javascript" type="hidden" value="<?= $property_id ?>"/>
	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="form">
		<tr>
			<td><? if($form_new): ?>
					<div id="form_buttons_holder">
						<table id="form_buttons">
							<tr>
								<td class="buttons_top">
									<div class="right">
										<input type="submit" name="Submit" value="<?= $c_send_new ?>" class="btn_form1">
									</div>
								</td>
							</tr>
						</table>
					</div>
				<? endif //form_new?>
				<? if($form_edit): ?>
					<? if($show_tabs): ?>
						<div id="form_buttons_holder">
							<table id="form_buttons">
								<tr>
									<td valign="bottom" class="buttons">
										<div class="right">
										<? if($show_previous_link): ?><a class="boto2 btn_show_previous_link"
										                          href="<?= $show_previous_link ?>"></a><? endif //show_previous_link?>
										<? if($show_next_link): ?><a class="boto2 btn_show_next_link"
										                          href="<?= $show_next_link ?>"></a><? endif //show_next_link?>
											<? if($back_button): ?>
												<a class="btn_form_back" href="<?= $back_button ?>"></a><? endif //back_button?>
											<input type="submit" name="Submit" value="<?= $c_send_edit ?>" class="btn_form1"/>

											<div class="btn_sep"></div>
											<a class="btn_form_delete" href="javascript:submit_form('<?= $c_confirm_form_bin ?>', true);">
												<?= $c_delete ?>
											</a><a class="btn_form1" href="<?= $preview_link ?>" target="_blank">
												<?= $c_preview ?>
											</a><a href="/admin/?action=show_record_showwindow&amp;property_id=<?= $property_id ?>&menu_id=202" target="_blank" class="btn_form1b" id="showwindow_button">
												<?= $c_showwindow ?>
											</a><a onclick="show_options('#showwindow','#showwindow_down','#showwindow_button','left');" class="btn_form_down" id="showwindow_down"><img src="/admin/themes/inmotools/images/down.gif" width="7" height="5" border="0"/></a><a href="/admin/?action=show_record_album&amp;property_id=<?= $property_id ?>&amp;menu_id=202" target="_blank" class="btn_form2">
												<?= $c_album ?>
											</a><a href="javascript:show_options('#album','#album_down','#album_down','right');" class="btn_form_down" id="album_down"><img src="/admin/themes/inmotools/images/down.gif" width="7" height="5" border="0"/></a>
										</div>
										<div class="menu">

											<div><?= $c_edit_data ?></div>

											<? if (!$edit_only_descriptions): ?>

											<? if($has_images): ?>
												<a href="<?= $image_link ?>"><?= $c_edit_images ?></a>
											<? endif //has_images?>

											<? if($is_custumer_on): ?>
												<a class="private" href="?menu_id=<?= $menu_id ?>&property_id=<?= $property_id ?>&action=list_records_custumer"><?= $c_edit_custumer ?></a>
											<? endif //custumer_on?>

											<? if($is_history_on): ?>
												<a class="private" href="?menu_id=<?= $menu_id ?>&property_id=<?= $property_id ?>&action=list_records_history"><?= $c_edit_history ?></a>
											<? endif //is_history_on?>

											<a class="private" href="?menu_id=<?= $menu_id ?>&property_id=<?= $property_id ?>&action=show_form_map"><?= $c_edit_map ?></a>

											<? if ($is_booking_on): ?>
											<span id="tipus_temp_link" style="display:<?= $show_tipus_temp ?>;">
							<a style="display:<?= $show_tipus_temp ?>;" class="sep" href="?menu_id=<?= $menu_id ?>&property_id=<?= $property_id ?>&action=show_form_new&process=booking"><?= $c_edit_booking ?></a>
							
							<a style="display:<?= $show_tipus_temp ?>;" class="private" href="?menu_id=<?= $menu_id ?>&property_id=<?= $property_id ?>&action=show_form_edit&process=booking_propertyrange"><?= $c_edit_booking_season ?></a>
						
							<a style="display:<?= $show_tipus_temp ?>;" class="private" href="?menu_id=<?= $menu_id ?>&property_id=<?= $property_id ?>&action=show_form_edit&process=booking_extra"><?= $c_edit_booking_extra ?></a>
						<span>
						<? endif //is_booking_on?>

						<? endif //edit_only_descriptions?>
										</div>
									</td>
								</tr>
							</table>
						</div>
					<? endif //show_tabs?>
				<? endif //form_edit?>
				<table width="100%" cellpadding="4" cellspacing="0">
					<tr>
						<td nowrap class="formCaptionTitle"><strong>
								<?= $c_data ?>
								&nbsp;&nbsp;</strong>
							<?= $c_ref ?>
							<?= $ref ?>
						</td>
						<td width="100%" class="formCaptionTitle">
							<table border="0" cellpadding="0" cellspacing="0">
								<tr>
									<td><strong>
											<?= $c_hut ?>:</strong></td>
									<td>&nbsp;
										<?= $hut ?>
										&nbsp;</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
				<? if($show_tabs): ?>
					<? if($image_name): ?>
						<table width="100%" border="0" cellpadding="0" cellspacing="0" class="images">
							<tr>
								<? foreach($images as $image): extract( $image ) ?>
									<td align="center"><? if($image_name_thumb): ?>
											<img onClick="open_viewer(this, <?= $image_conta ?>)"  src="<?= $image_src_admin_thumb ?>" vspace="10" border="0"/>
										<? endif //$image_name_details?></td>
									<? if($image_conta == 2) break; ?>
								<? endforeach //$images?>
							</tr>
						</table>
					<? endif // image_name?>
				<? endif // show_tabs?>
				<? if(!$edit_only_descriptions): ?>
					<table width="100%" cellpadding="4" cellspacing="0" class="formstbl private">
						<tr>
							<td valign="top" class="formsCaptionHor"><?= $c_data_private ?></td>
							<td class="formsCaptionHor"></td>
						</tr>
						<tr>
							<td width="150" class="formsCaption"><strong>
									<?= $c_property_private ?>:</strong></td>
							<td class="forms">
								<?= $property_private ?><? if($view_only_itself): ?><?= $user_id ?><? endif //view_only_itself?></td>
						</tr>
						<tr>
							<td width="150" class="formsCaption"><strong>
									<?= $c_ref_collaborator ?>:</strong></td>
							<td class="forms">
								<?= $ref_collaborator ?></td>
						</tr>
						<tr>
								<td width="150" class="formsCaption"><strong>
										<?= $c_gaining ?>:</strong></td>
							<td class="forms">
								<table border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td><?= $c_place_id ?>
											&nbsp;&nbsp;</td>
							<? if ( ! $view_only_itself ): ?>
										<td><?= $c_user_id ?></td>
									</tr>
							<? endif //view_only_itself?>
									<tr>
										<td><?= $place_id ?>
											&nbsp;&nbsp;</td>
										<? if ( ! $view_only_itself ): ?>
										<td><?= $user_id ?>
											<? endif //view_only_itself?>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td valign="top" class="formsCaption"><strong>
									<?= $c_address ?>:</strong></td>
							<td class="forms">
								<table border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td><?= $c_adress ?></td>
										<td><?= $c_numstreet ?></td>
										<td><?= $c_block ?></td>
										<td><?= $c_flat ?></td>
										<td><?= $c_door ?></td>
										<td><?= $c_zip ?></td>
									</tr>
									<tr>
										<td><?= $adress ?>
											&nbsp;&nbsp;</td>
										<td><?= $numstreet ?>
											&nbsp;&nbsp;</td>
										<td><?= $block; ?>
											&nbsp;&nbsp;</td>
										<td><?= $flat ?>
											&nbsp;&nbsp;</td>
										<td><?= $door ?>
											&nbsp;&nbsp;</td>
										<td><?= $zip ?></td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td valign="top" class="formsCaption"><strong>
									<?= $c_expenses ?>:</strong></td>
							<td class="forms">
								<table border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td><?= $c_community_expenses ?></td>
										<td><?= $c_ibi ?></td>
										<td><?= $c_municipal_tax ?></td>
									</tr>
									<tr>
										<td><?= $community_expenses ?>
											&nbsp;&nbsp;</td>
										<td><?= $ibi ?>
											&nbsp;&nbsp;</td>
										<td><?= $municipal_tax; ?></td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td valign="top" class="formsCaption"><strong>
									<?= $c_registerpropietat ?>
									:</strong></td>
							<td class="forms">
								<table border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td><?= $c_numregister ?></td>
										<td><?= $c_locationregister; ?></td>
										<td><?= $c_tomregister; ?></td>
										<td><?= $c_bookregister; ?></td>
										<td><?= $c_pageregister; ?></td>
										<td><?= $c_numberpropertyregister; ?></td>
										<td><?= $c_inscriptionregister; ?></td>
									</tr>
									<tr>
										<td><?= $numregister ?>&nbsp;&nbsp;</td>
										<td><?= $locationregister; ?>&nbsp;&nbsp;</td>
										<td><?= $tomregister; ?>&nbsp;&nbsp;</td>
										<td><?= $bookregister; ?>&nbsp;&nbsp;</td>
										<td><?= $pageregister; ?>&nbsp;&nbsp;</td>
										<td><?= $numberpropertyregister; ?>&nbsp;&nbsp;</td>
										<td><?= $inscriptionregister; ?>&nbsp;&nbsp;</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td valign="top" class="formsCaption"><strong>
								</strong></td>
							<td class="forms">
								<table border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td><?= $c_ref_cadastre ?></td>
										<td><?= $c_cedula ?></td>
									</tr>
									<tr>
										<td><?= $ref_cadastre ?>&nbsp;&nbsp;</td>
										<td><?= $cedula; ?>&nbsp;&nbsp;</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td class="formsCaption"><strong><strong>
										<?= $c_construction_date ?>
									</strong> </strong>:
							</td>
							<td class="forms"><?= $construction_date ?></td>
						</tr>
						<tr>
							<td valign="top" class="formsCaption"><strong>
									<?= $c_descriptionregister ?>:</strong></td>
							<td class="forms"><?= $descriptionregister ?></td>
						</tr>
						<tr>
							<td valign="top" class="formsCaption"><strong>
									<?= $c_observations ?>:</strong></td>
							<td class="forms"><?= $observations ?></td>
						</tr>
					</table>
					<table width="100%" cellpadding="0" cellspacing="0">
						<tr>
							<td class="formsCaptionHor"><?= $c_data_public ?></td>
						</tr>
					</table>
					<table width="100%" cellpadding="4" cellspacing="0" class="formstbl">
						<tr>
							<td width="150" class="formsCaption"><strong>
									<?= $c_tipus ?>:</strong></td>
							<td class="forms"><?= $tipus ?></td>
						</tr>
						<tr>
							<td class="formsCaption"><strong>
									<?= $c_tipus2 ?>:</strong></td>
							<td class="forms"><?= $tipus2 ?></td>
						</tr>
						<tr>
							<td valign="top" class="formsCaption"><strong>
									<?= $c_category ?>:</strong></td>
							<td class="forms"><?= $category_id ?></td>
						</tr>
						<? if(IS_COLOMBIA): ?>
							<tr>
								<td class="formsCaption"><strong>
										<?= $c_estrato ?>:</strong></td>
								<td class="forms"><?= $estrato ?></td>
							</tr>
						<? endif //IS_COLOMBIA?>
						<tr>
							<td class="formsCaption"><strong>
									<?= $c_floor_space ?>:</strong></td>
							<td class="forms"><?= $floor_space ?></td>
						</tr>
						<tr>
							<td class="formsCaption"><strong>
									<?= $c_land ?>:</strong></td>
							<td class="forms"><?= $land ?>
								&nbsp;&nbsp;
								<?= $land_units ?></td>
						</tr>
						<tr>
							<td class="formsCaption"><strong>
									<?= $c_country_id ?>:</strong></td>
							<td class="forms"><?= $country_id ?></td>
						</tr>
						<tr>
							<td class="formsCaption"><strong>
									<?= $c_provincia_id ?>:</strong></td>
							<td class="forms"><?= $provincia_id ?></td>
						</tr>
						<tr>
							<td class="formsCaption"><strong>
									<?= $c_comarca_id ?>:</strong></td>
							<td class="forms"><?= $comarca_id ?>
								<input name="old_comarca_id[<?= $property_id ?>]" type="hidden" value="0"/></td>
						</tr>
						<tr>
							<td class="formsCaption"><strong>
									<?= $c_municipi_id ?>:</strong></td>
							<td class="forms"><?= $municipi_id ?>
								<input name="old_municipi_id[<?= $property_id ?>]" type="hidden" value="0"/></td>
						</tr>
						<tr class="private">
							<td class="formsCaption"><strong>
									<?= $c_zone_id ?>: </strong></td>
							<td class="forms"><?= $zone_id ?>
								<input name="old_zone_id[<?= $property_id ?>]" type="hidden" value="0"/></td>
						</tr>
						<tr class="private">
							<td class="formsCaption"><strong>
									<?= $c_price_private ?>:</strong></td>
							<td class="forms"><?= $price_private ?>
								&nbsp;
								<?= CURRENCY_NAME ?>
								&nbsp;&nbsp;
 								<? /* (&nbsp;
 								<input name="pts_private" type="text" value="<?= $pts_private ?>" onkeyup="write_euros(this,this.form.elements['price_private[<?= $property_id ?>]'])"/>
 								&nbsp;)
 								<?= CURRENCY2_NAME ?>
 								&nbsp; */ ?>
								
								<?= $exclusive ?>
								<?= $c_exclusive ?></td>
						</tr>
						<tr>
							<td class="formsCaption"><strong>
									<?= $c_price ?>:</strong></td>
							<td class="forms"><?= $price ?>
								&nbsp;
								<?= CURRENCY_NAME ?>
								&nbsp;&nbsp;
 								<? /* (&nbsp;
 								<input name="pts" type="text" value="<?= $pts ?>" onkeyup="write_euros(this,this.form.elements['price[<?= $property_id ?>]'])"/>
 								&nbsp;)
 								<?= CURRENCY2_NAME ?>
 								&nbsp; */ ?>
								
								<?= $price_consult ?>
								<?= $c_price_consult ?>
								&nbsp;&nbsp;
								<a href="javascript:show_price_history_property(<?=$id?>, <?=$menu_id?>)"><?= $c_price_history_link ?></a>
							</td>
						</tr>
						<tr>
							<td class="formsCaption"><strong>
									<?= $c_efficiency ?>:</strong></td>
							<td class="forms">
								<table border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td><?= $c_efficiency_letter ?></td>
										<td><?= $c_efficiency_number ?></td>
										<td><?= $c_efficiency_letter ?></td>
										<td><?= $c_efficiency_number2 ?></td>
										<td><?= $c_efficiency_date ?></td>
										<td><?= $c_expired ?></td>
									</tr>
									<tr>
										<td><?= $efficiency ?>&nbsp;&nbsp;</td>
										<td><?= $efficiency_number ?>&nbsp;&nbsp;</td>
										<td><?= $efficiency2 ?>&nbsp;&nbsp;</td>
										<td><?= $efficiency_number2 ?>&nbsp;&nbsp;</td>
										<td><?= $efficiency_date; ?>&nbsp;&nbsp;</td>
										<td><?= $expired ?></td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td class="formsCaption"><strong>
									<?= $c_status ?>:</strong></td>
							<td class="forms"><?= $status ?></td>
						</tr>
						<tr>
							<td class="formsCaption"><strong><strong>
										<?= $c_home ?>
									</strong> </strong>:
							</td>
							<td class="forms"><?= $home ?></td>
						</tr>
						<tr>
							<td class="formsCaption"><strong><strong>
										<?= $c_offer ?>
									</strong> </strong>:
							</td>
							<td class="forms"><?= $offer ?></td>
						</tr>
						<tr>
							<td valign="top" class="formsCaption"><strong><strong>
										<?= $c_file ?>
									</strong> </strong>:
							</td>
							<td class="forms"><?= $file ?></td>
						</tr>
						<tr>
							<td valign="top" class="formsCaption"><strong><strong>
										<?= $c_videoframe ?>
									</strong> </strong>:
							</td>
							<td class="forms"><?= $videoframe ?></td>
						</tr>
					</table>

					<? if($is_mls_on): ?>
						<div class="sep_dark"></div>
						<table width="100%" cellpadding="4" cellspacing="0" class="private">
							<tr>
								<td class="formCaptionTitle"><strong>
										<?= $c_mls ?>
									</strong></td>
							</tr>
						</table>
						<table width="100%" cellpadding="4" cellspacing="0" class="formstbl">
							<tr class="private">
								<td width="150" class="formsCaption"><strong>
										<?= $c_share_mls ?>:</strong></td>
								<td class="forms"><?= $mls ?></td>
								<td width="150" class="formsCaption"><strong>
										<?= $c_comission ?>:</strong></td>
								<td class="forms"><?= $comission ?></td>
							</tr>
						</table>
					<? endif //is_mls_on?>


					<div class="sep_dark"></div>
					<table width="100%" cellpadding="4" cellspacing="0">
						<tr>
							<td class="formCaptionTitle"><strong>
									<?= $c_details ?>
								</strong></td>
						</tr>
					</table>
					<table width="100%" cellpadding="4" cellspacing="0" class="formstbl">
						<tr>
							<td width="150" class="formsCaption"><strong>
									<?= $c_room ?>:</strong></td>
							<td class="forms"><?= $room ?></td>
							<td width="150" class="formsCaption"><strong>
									<?= $c_bathroom ?>:</strong></td>
							<td class="forms"><?= $bathroom ?></td>
						</tr>
						<tr>
							<td width="150" class="formsCaption"><strong>
									<?= $c_living ?>:</strong></td>
							<td class="forms"><?= $living ?></td>
							<td width="150" class="formsCaption"><strong>
									<?= $c_dinning ?>:</strong></td>
							<td class="forms"><?= $dinning ?></td>
						</tr>
						<tr>
							<td width="150" class="formsCaption"><strong>
									<?= $c_wc ?>:</strong></td>
							<td class="forms"><?= $wc ?></td>
							<td class="formsCaption"><strong>
									<?= $c_level_count ?>:</strong></td>
							<td class="forms"><?= $level_count ?></td>
						</tr>
						<tr>
							<td width="150" class="formsCaption"><strong>
									<?= $c_floor ?>:</strong></td>
							<td class="forms">
								<table border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td class="searchItemOdd" onmousedown="click_checkbox(this, 'Down', '<?= $id ?>','gres')" onmouseover="click_checkbox(this, 'Over', '<?= $id ?>','gres')" onmouseout="click_checkbox(this, 'Odd', '<?= $id ?>','gres')" id="list_row_<?= $id ?>"><?= $c_gres ?>
											&nbsp;&nbsp;</td>
										<td><?= $gres ?>
											&nbsp;&nbsp;&nbsp;&nbsp;</td>
										<td class="searchItemOdd" onmousedown="click_checkbox(this, 'Down', '<?= $id ?>','ceramic')" onmouseover="click_checkbox(this, 'Over', '<?= $id ?>','ceramic')" onmouseout="click_checkbox(this, 'Odd', '<?= $id ?>','ceramic')" id="list_row_<?= $id ?>"><?= $c_ceramic ?></td>
										<td><?= $ceramic ?></td>
									</tr>
									<tr>
										<td class="searchItemOdd" onmousedown="click_checkbox(this, 'Down', '<?= $id ?>','parket')" onmouseover="click_checkbox(this, 'Over', '<?= $id ?>','parket')" onmouseout="click_checkbox(this, 'Odd', '<?= $id ?>','parket')" id="list_row_<?= $id ?>"><?= $c_parket ?>
											&nbsp;&nbsp;</td>
										<td><?= $parket ?></td>
										<td class="searchItemOdd" onmousedown="click_checkbox(this, 'Down', '<?= $id ?>','laminated')" onmouseover="click_checkbox(this, 'Over', '<?= $id ?>','laminated')" onmouseout="click_checkbox(this, 'Odd', '<?= $id ?>','laminated')" id="list_row_<?= $id ?>"><?= $c_laminated ?>
											&nbsp;&nbsp;</td>
										<td><?= $laminated ?></td>
									</tr>
									<tr>
										<td class="searchItemOdd" onmousedown="click_checkbox(this, 'Down', '<?= $id ?>','marble')" onmouseover="click_checkbox(this, 'Over', '<?= $id ?>','marble')" onmouseout="click_checkbox(this, 'Odd', '<?= $id ?>','marble')" id="list_row_<?= $id ?>"><?= $c_marble ?>
											&nbsp;&nbsp;</td>
										<td><?= $marble ?></td>
										<td></td>
										<td></td>
									</tr>
								</table>
							</td>
							<td width="150" class="formsCaption"><strong>
									<?= $c_laundry ?>:</strong></td>
							<td class="forms"><?= $laundry ?></td>
						</tr>
						<tr>
							<td width="150" class="formsCaption"><strong>
									<?= $c_garage ?>:</strong></td>
							<td class="forms">
								<table border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td><?= $garage ?>
											&nbsp;</td>
										<td><?= $c_garage_area ?></td>
										<td>&nbsp;
											<?= $garage_area ?></td>
									</tr>
								</table>
							</td>
							<td width="150" class="formsCaption"><strong>
									<?= $c_study ?>:</strong></td>
							<td class="forms"><?= $study ?></td>
						</tr>
						<tr>
							<td class="formsCaption"><strong>
									<?= $c_parking ?>:</strong></td>
							<td class="forms"><?= $parking ?></td>
							<td width="150" class="formsCaption"><strong>
									<?= $c_storage ?>:</strong></td>
							<td class="forms">
								<table border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td><?= $storage ?>
											&nbsp;</td>
										<td><?= $c_storage_area ?></td>
										<td>&nbsp;
											<?= $storage_area ?></td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td width="150" class="formsCaption"><strong>
									<?= $c_courtyard ?>:</strong></td>
							<td class="forms">
								<table border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td><?= $courtyard ?>
											&nbsp;</td>
										<td><?= $c_courtyard_area ?></td>
										<td>&nbsp;
											<?= $courtyard_area ?></td>
									</tr>
								</table>
							</td>
							<td width="150" class="formsCaption"><strong>
									<?= $c_garden ?>:</strong></td>
							<td class="forms">
								<table border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td><?= $garden ?>
											&nbsp;</td>
										<td><?= $c_garden_area ?></td>
										<td>&nbsp;
											<?= $garden_area ?></td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td width="150" class="formsCaption"><strong>
									<?= $c_furniture ?>:</strong></td>
							<td class="forms"><?= $furniture ?></td>
							<td class="formsCaption"><strong>
									<?= $c_terrace ?>:</strong></td>
							<td class="forms">
								<table border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td><?= $terrace ?>
											&nbsp;</td>
										<td><?= $c_terrace_area ?></td>
										<td>&nbsp;
											<?= $terrace_area ?></td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td width="150" class="formsCaption"><strong> <strong>
										<?= $c_centralheating ?>
									</strong>:</strong></td>
							<td class="forms"><?= $centralheating ?></td>
							<td width="150" class="formsCaption"><strong>
									<?= $c_citygas ?>:</strong></td>
							<td class="forms"><?= $citygas ?></td>
						</tr>
						<tr>
							<td class="formsCaption"><strong>
									<?= $c_airconditioned ?>:</strong></td>
							<td class="forms"><?= $airconditioned ?></td>
							<td width="150" class="formsCaption"><strong>
									<?= $c_swimmingpool ?>:</strong></td>
							<td class="forms">
								<table border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td><?= $swimmingpool ?>
											&nbsp;</td>
										<td><?= $c_swimmingpool_area ?></td>
										<td>&nbsp;
											<?= $swimmingpool_area ?></td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td class="formsCaption"><strong>
									<?= $c_elevator ?>:</strong></td>
							<td class="forms"><?= $elevator ?></td>
							<td class="formsCaption"><strong>
									<?= $c_views ?>:</strong></td>
							<td class="forms">
								<table border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td class="searchItemOdd" onmousedown="click_checkbox(this, 'Down', '<?= $id ?>','sea_view')" onmouseover="click_checkbox(this, 'Over', '<?= $id ?>','sea_view')" onmouseout="click_checkbox(this, 'Odd', '<?= $id ?>','sea_view')" id="list_row_<?= $id ?>"><?= $c_sea_view ?>
											&nbsp;&nbsp;</td>
										<td><?= $sea_view ?>
											&nbsp;&nbsp;&nbsp;&nbsp;</td>
										<td class="searchItemOdd" onmousedown="click_checkbox(this, 'Down', '<?= $id ?>','clear_view')" onmouseover="click_checkbox(this, 'Over', '<?= $id ?>','clear_view')" onmouseout="click_checkbox(this, 'Odd', '<?= $id ?>','clear_view')" id="list_row_<?= $id ?>"><?= $c_clear_view ?>
											&nbsp;&nbsp;</td>
										<td><?= $clear_view ?></td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td class="formsCaption"><strong>
									<?= $c_service_room ?>:</strong></td>
							<td class="forms"><?= $service_room ?></td>
							<td class="formsCaption"><strong>
									<?= $c_balcony ?>:</strong></td>
							<td class="forms"><?= $balcony ?></td>
						</tr>
						<tr>
							<td class="formsCaption"><strong>
									<?= $c_facing ?>:</strong></td>
							<td class="forms"><?= $facing ?></td>
							<td class="formsCaption"><strong>
									<?= $c_service_bath ?></strong></td>
							<td class="forms"><?= $service_bath ?></td>
						</tr>
						<? if($c_custom1 || $c_custom2): ?>
							<tr>
								<td class="formsCaption" valign="top"><strong>
										<? if($c_custom1): ?><?= $c_custom1 ?>:<? endif //custom?></strong></td>
								<td class="forms" valign="top">
									<? if($c_custom1): ?>
										<? if(is_array( $custom1 )): ?>
											<table cellpadding="4" cellspacing="0">
												<? foreach($custom1 as $l): extract( $l ) ?>
													<tr>
														<td class="forms"><?= $lang ?></td>
														<td><?= $custom1 ?></td>
													</tr>
												<? endforeach //$property?>
											</table>
										<? else: //?>
											<?= $custom1 ?>
										<? endif //is_array?>
									<? endif //custom?>
								</td>
								<td class="formsCaption" valign="top"><strong>
										<? if($c_custom2): ?><?= $c_custom2 ?>:<? endif //custom?></strong></td>
								<td class="forms" valign="top">
									<? if($c_custom2): ?>
										<? if(is_array( $custom2 )): ?>
											<table cellpadding="4" cellspacing="0">
												<? foreach($custom2 as $l): extract( $l ) ?>
													<tr>
														<td class="forms"><?= $lang ?></td>
														<td><?= $custom2 ?></td>
													</tr>
												<? endforeach //$property?>
											</table>
										<? else: //?>
											<?= $custom2 ?>
										<? endif //is_array?>
									<? endif //custom?>
								</td>
							</tr>
						<? endif //custom1?>
						<? if($c_custom3 || $c_custom4): ?>
							<tr>
								<td class="formsCaption" valign="top"><strong>
										<? if($c_custom3): ?><?= $c_custom3 ?>:<? endif //custom?></strong></td>
								<td class="forms" valign="top">
									<? if($c_custom3): ?>
										<? if(is_array( $custom3 )): ?>
											<table cellpadding="4" cellspacing="0">
												<? foreach($custom3 as $l): extract( $l ) ?>
													<tr>
														<td class="forms"><?= $lang ?></td>
														<td><?= $custom3 ?></td>
													</tr>
												<? endforeach //$property?>
											</table>
										<? else: //?>
											<?= $custom3 ?>
										<? endif //is_array?>
									<? endif //custom?>
								</td>
								<td class="formsCaption" valign="top"><strong>
										<? if($c_custom4): ?><?= $c_custom4 ?>:<? endif //custom?></strong></td>
								<td class="forms" valign="top">
									<? if($c_custom4): ?>
										<? if(is_array( $custom4 )): ?>
											<table cellpadding="4" cellspacing="0">
												<? foreach($custom4 as $l): extract( $l ) ?>
													<tr>
														<td class="forms"><?= $lang ?></td>
														<td><?= $custom4 ?></td>
													</tr>
												<? endforeach //$property?>
											</table>
										<? else: //?>
											<?= $custom4 ?>
										<? endif //is_array?>
									<? endif //custom?>
								</td>
							</tr>
						<? endif //custom1?>
						<? if($c_custom5 || $c_custom6): ?>
							<tr>
								<td class="formsCaption" valign="top"><strong>
										<? if($c_custom5): ?><?= $c_custom5 ?>:<? endif //custom?></strong></td>
								<td class="forms" valign="top">
									<? if($c_custom5): ?>
										<? if(is_array( $custom5 )): ?>
											<table cellpadding="4" cellspacing="0">
												<? foreach($custom5 as $l): extract( $l ) ?>
													<tr>
														<td class="forms"><?= $lang ?></td>
														<td><?= $custom5 ?></td>
													</tr>
												<? endforeach //$property?>
											</table>
										<? else: //?>
											<?= $custom5 ?>
										<? endif //is_array?>
									<? endif //custom?>
								</td>
								<td class="formsCaption" valign="top"><strong>
										<? if($c_custom6): ?><?= $c_custom6 ?>:<? endif //custom?></strong></td>
								<td class="forms" valign="top">
									<? if($c_custom6): ?>
										<? if(is_array( $custom6 )): ?>
											<table cellpadding="4" cellspacing="0">
												<? foreach($custom6 as $l): extract( $l ) ?>
													<tr>
														<td class="forms"><?= $lang ?></td>
														<td><?= $custom6 ?></td>
													</tr>
												<? endforeach //$property?>
											</table>
										<? else: //?>
											<?= $custom6 ?>
										<? endif //is_array?>
									<? endif //custom?>
								</td>
							</tr>
						<? endif //custom5?>
						<? if($c_custom7 || $c_custom8): ?>
							<tr>
								<td class="formsCaption" valign="top"><strong>
										<? if($c_custom7): ?><?= $c_custom7 ?>:<? endif //custom?></strong></td>
								<td class="forms" valign="top">
									<? if($c_custom7): ?>
										<? if(is_array( $custom7 )): ?>
											<table cellpadding="4" cellspacing="0">
												<? foreach($custom7 as $l): extract( $l ) ?>
													<tr>
														<td class="forms"><?= $lang ?></td>
														<td><?= $custom7 ?></td>
													</tr>
												<? endforeach //$property?>
											</table>
										<? else: //?>
											<?= $custom7 ?>
										<? endif //is_array?>
									<? endif //custom?>
								</td>
								<td class="formsCaption" valign="top"><strong>
										<? if($c_custom8): ?><?= $c_custom8 ?>:<? endif //custom?></strong></td>
								<td class="forms" valign="top">
									<? if($c_custom8): ?>
										<? if(is_array( $custom8 )): ?>
											<table cellpadding="4" cellspacing="0">
												<? foreach($custom8 as $l): extract( $l ) ?>
													<tr>
														<td class="forms"><?= $lang ?></td>
														<td><?= $custom8 ?></td>
													</tr>
												<? endforeach //$property?>
											</table>
										<? else: //?>
											<?= $custom8 ?>
										<? endif //is_array?>
									<? endif //custom?>
								</td>
							</tr>
						<? endif //custom1?>
					</table>

					<table width="100%" cellpadding="0" cellspacing="0">
						<tr>
							<td class="formsCaptionHor"><?=$c_details_temp?></td>
						<tr>
					</table>

					<table width="100%" cellpadding="4" cellspacing="0" class="formstbl">
						<tr>
							<td width="150" class="formsCaption"><strong>
									<?= $c_person ?>:</strong></td>
							<td class="forms"><?= $person ?></td>
							<td class="formsCaption"><strong>
									<?= $c_managed_by_owner ?></strong></td>
							<td class="forms"><?= $managed_by_owner ?></td>
						</tr>
						<tr>
							<td class="formsCaption"><strong>
									<?= $c_time_in ?>:</strong></td>
							<td class="forms"><?= $time_in ?></td>
							<td class="formsCaption"><strong>
									<?= $c_time_out ?></strong></td>
							<td class="forms"><?= $time_out ?></td>
						</tr>
						<tr>
							<td class="formsCaption"><strong>
									<?= $c_star ?>:</strong></td>
							<td class="forms"><?= $star ?></td>
							<td class="formsCaption"></td>
							<td class="forms"></td>
						</tr>
					</table>


					<div class="sep_dark"></div>
				<? endif //edit_only_descriptions?>
				<table width="100%" cellpadding="4" cellspacing="0">
					<tr>
						<td class="formCaptionTitle"><strong>
								<?= $c_descriptions ?>
							</strong></td>
					</tr>
				</table>
				<table cellpadding="0" cellspacing="0" class="formstbl">
					<tr>
						<td width="150" valign="top" class="formsCaption"><strong>
								<?= $c_property ?>:</strong></td>
						<td>
							<table cellpadding="4" cellspacing="0">
								<? foreach($property as $l): extract( $l ) ?>
									<tr>
										<td class="forms"><?= $lang ?></td>
										<td><?= $property ?></td>
									</tr>
								<? endforeach //$property?>
							</table>
						</td>
					</tr>
				</table>
				<table cellpadding="0" cellspacing="0" class="formstbl">
					<tr>
						<td width="150" valign="top" class="formsCaption"><strong>
								<?= $c_property_title ?>:</strong></td>
						<td>
							<table cellpadding="4" cellspacing="0">
								<? foreach($property_title as $l): extract( $l ) ?>
									<tr>
										<td class="forms"><?= $lang ?></td>
										<td><?= $property_title ?></td>
									</tr>
								<? endforeach //$property_title?>
							</table>
						</td>
					</tr>
				</table>
				<table width="100%" cellpadding="0" cellspacing="0">
					<tr>
						<td class="formsCaptionHor"><?= $c_description ?></td>
					<tr>
				</table>
				<table width="100%" cellpadding="0" cellspacing="0" class="formstbl">
					<? foreach($description as $l): extract( $l ) ?>
						<tr>
							<td class="formsHor"><?= $lang ?>
								<br>
								<?= $description ?></td>
						</tr>
					<? endforeach //$description?>
				</table>

				<? /* SEO */ ?>

				<table width="100%" cellpadding="4" cellspacing="0">
					<tr>
						<td class="formCaptionTitle"><strong>
								<?= $c_seo ?>
							</strong></td>
					</tr>
				</table>
				<table cellpadding="0" cellspacing="0" class="formstbl">
					<tr>
						<td width="150" valign="top" class="formsCaption"><strong>
								<?= $c_page_title ?>:</strong></td>
						<td>
							<table cellpadding="4" cellspacing="0">
								<? foreach($page_title as $l): extract( $l ) ?>
									<tr>
										<td class="forms"><?= $lang ?></td>
										<td><?= $page_title ?></td>
									</tr>
								<? endforeach //$page_title?>
							</table>
						</td>
					</tr>
				</table>

				<table cellpadding="0" cellspacing="0" class="formstbl">
					<tr>
						<td width="150" valign="top" class="formsCaption"><strong>
								<?= $c_page_description ?>:</strong></td>
						<td>
							<table cellpadding="4" cellspacing="0">
								<? foreach($page_description as $l): extract( $l ) ?>
									<tr>
										<td class="forms"><?= $lang ?></td>
										<td><?= $page_description ?></td>
									</tr>
								<? endforeach //$page_description?>
							</table>
						</td>
					</tr>
				</table>

				<table cellpadding="0" cellspacing="0" class="formstbl">
					<tr>
						<td width="150" valign="top" class="formsCaption"><strong>
								<?= $c_page_keywords ?>:</strong></td>
						<td>
							<table cellpadding="4" cellspacing="0">
								<? foreach($page_keywords as $l): extract( $l ) ?>
									<tr>
										<td class="forms"><?= $lang ?></td>
										<td><?= $page_keywords ?></td>
									</tr>
								<? endforeach //$page_keywords?>
							</table>
						</td>
					</tr>
				</table>

				<table cellpadding="0" cellspacing="0" class="formstbl">
					<tr>
						<td width="150" valign="top" class="formsCaption"><strong>
								<?= $c_property_file_name ?>:</strong></td>
						<td>
							<table cellpadding="4" cellspacing="0">
								<? foreach($property_file_name as $l): extract( $l ) ?>
									<tr>
										<td class="forms"><?= $lang ?></td>
										<td><?= $property_file_name ?></td>
									</tr>
								<? endforeach //$property_file_name?>
							</table>
						</td>
					</tr>
				</table>

				<table cellpadding="0" cellspacing="0" class="formstbl">
					<tr>
						<td width="150" valign="top" class="formsCaption"><strong>
								<?= $c_property_old_file_name ?>:</strong></td>
						<td>
							<table cellpadding="4" cellspacing="0">
								<? foreach($property_old_file_name as $l): extract( $l ) ?>
									<tr>
										<td class="forms"><?= $lang ?></td>
										<td><?= $property_old_file_name ?></td>
									</tr>
								<? endforeach //$property_old_file_name?>
							</table>
						</td>
					</tr>
				</table>

				<? /*  FI SEO */ ?>


				<? if($form_new): ?>
					<table width="100%" cellpadding="4" cellspacing="2" class="formsButtonstbl">
						<tr>
							<td align="center" class="formsButtons">
								<div class="centerFloat1">
									<div class="centerFloat2">
										<input type="submit" name="Submit" value="<?= $c_send_new ?>" class="btn_form1">
									</div>
								</div>
							</td>
						</tr>
					</table>
				<? endif //form_new?>
				<? if($form_edit): ?>
					<table width="100%" cellpadding="4" cellspacing="2" class="formsButtonstbl">
						<tr>
							<td align="center" class="formsButtons">
								<div class="centerFloat1">
									<div class="centerFloat2">
										<input type="submit" name="Submit" value="<?= $c_send_edit ?>" class="btn_form1" style=""/>

										<div class="btn_sep"></div>
										<a class="btn_form_delete" href="javascript:submit_form('<?= $c_confirm_form_bin ?>', true);">
											<?= $c_delete ?>
										</a><a class="btn_form1" href="<?= $preview_link ?>" target="_blank">
											<?= $c_preview ?>
										</a>
										<? if($show_tabs): ?>
														<a href="/admin/?action=show_record_showwindow&amp;property_id=<?= $property_id ?>&menu_id=202" target="_blank" class="btn_form1b" id="showwindow_button2">
														<?= $c_showwindow ?>
														</a><a onclick="show_options('#showwindow','#showwindow_down2','#showwindow_button2','left');" class="btn_form_down" id="showwindow_down2"><img src="/admin/themes/inmotools/images/down.gif" width="7" height="5" border="0" /></a><a href="/admin/?action=show_record_album&amp;property_id=<?= $property_id ?>&amp;menu_id=202" target="_blank" class="btn_form2">
														<?= $c_album ?>
														</a>
							</a><a href="javascript:show_options('#album','#album_down2','#album_down2','right');" class="btn_form_down" id="album_down2"><img src="/admin/themes/inmotools/images/down.gif" width="7" height="5" border="0" /></a>
														<? endif //show_tabs?>
									</div>
								</div>
							</td>
						</tr>
					</table>
				<? endif //form_edit?></td>
		</tr>
	</table>
</form>
