<?php
// no es poden posar ; entre mig de la consulta ja que parteix les consultes per el ; a no ser que fem servir $update_sql en array()
if (is_table("contact__contact") && !is_field('contact__contact','company')){
	
	$update_sql .="ALTER TABLE `contact__contact`
	ADD COLUMN `company` VARCHAR(250) NOT NULL AFTER `contact_id`;";
}	
?>