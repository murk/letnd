<?
/**
 * ProductSellpoint
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2013
 * @version $Id$
 * @access public
 */
class ProductSellpoint extends Module{
	var $seller_kind = false, $country_id = false;
	function __construct(){
		parent::__construct();
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function get_records()
	{
		$comunitat_id = R::id('comunitat_id');
		$provincia_id = R::id('provincia_id');
		
		// puc fer servir els 2 sistemes, amb una accio o la variable
		$this->seller_kind = $this->seller_kind?$this->seller_kind:R::text_id('seller_kind'); 
		$this->country_id = $this->country_id?$this->country_id:R::text_id('country_id');
		
		$listing = new ListRecords($this);
		$listing->condition = "(status = 'public')";		
		
		$listing->set_var('listing_comunitat_id',false);
		$listing->set_var('listing_provincia_id',false);
		$listing->set_var('listing_seller_kind',false);
		$listing->set_var('listing_country_id',false);
		$listing->set_var('listing_country',false);

		// ho deixo perque em surti la variable igualment, així puc aprofitar el mateix template
		$listing->group_fields = array('provincia_id');
		$listing->order_by = 'provincia, empresa ASC';

		// Ordre per defecte a la home
		if ( $GLOBALS['gl_is_tool_home'] && $this->config['home_default_condition'] ) {
			// TODO-i Funció per netejar sql, no INSERT, UPDATE, DELETE ...
			$listing->condition .= " AND ( " . $this->config['home_default_condition'] . ")";
		}

		// l'ordre importa per les preferències dels titols, te preferencia la provincia
		if ($this->seller_kind) {
			$listing->condition .= " AND (seller_kind = '".$this->seller_kind."')";
			$listing->set_var('listing_seller_kind',$this->seller_kind);

			Page::set_title($this->caption['c_seller_kind_' . $this->seller_kind . 's']);
		}
		if ($comunitat_id) {	
			$listing->condition .= " AND (product__sellpoint.comunitat_id = '".$comunitat_id."')";
			$listing->set_var('listing_comunitat_id',$comunitat_id);

			$comunitat = Db::get_first( "SELECT name FROM all__comunitat WHERE comunitat_id = '" . $comunitat_id . "'" );
			$listing->set_var('listing_comunitat', $comunitat);

			Page::set_title($comunitat);
		}
		if ($provincia_id) {	
			$listing->condition .= " AND (provincia_id = '".$provincia_id."')";
			$listing->set_var('listing_provincia_id',$provincia_id);

			$provincia = Db::get_first( "SELECT provincia FROM all__provincia WHERE provincia_id = '" . $provincia_id . "'" );
			Page::set_title($provincia);

		}

		$listing->join = 'LEFT OUTER JOIN all__provincia USING (provincia_id)';
		$listing->extra_fields = "all__provincia.name as provincia";

		if ($this->country_id) {
			if ($this->country_id=='international') {
				$listing->condition .= " AND (product__sellpoint.country_id != 'ES')";

				$listing->group_fields = array('country');
				$listing->extra_fields .= ",(SELECT country FROM all__country WHERE all__country.country_id = product__sellpoint.country_id) AS country";
				$listing->order_by = 'country, provincia, empresa ASC';

				// Internacional te preferencia
				Page::set_title($this->caption['c_international']);
			}
			else $listing->condition .= " AND (product__sellpoint.country_id = '".$this->country_id."')";

			$listing->set_var('listing_country_id',$this->country_id);
			$listing->set_var('listing_country', Db::get_first ( "SELECT country FROM all__country where country_id = '" . $this->country_id . "'"));

		}

		//$listing->extra_fields = "(SELECT name FROM all__provincia WHERE all__provincia.provincia_id = newgrass__sellpoint.provincia_id) AS provincia_id";
		
		$listing->call('records_walk','url1,url1_name,sellpoint_description');
		
		return $listing->list_records();
	}

	function shop()
	{	    
		$this->seller_kind = 'shop';
		$this->do_action('list_records');
	}

	function dealer()
	{	    
		$this->seller_kind = 'dealer';
		$this->do_action('list_records');
	}

	function international()
	{	    
		$this->country_id = 'international';
		$this->do_action('list_records');
	}

	function internationalshop()
	{	    
		$this->country_id = 'international';
		$this->seller_kind = 'shop';
		$this->do_action('list_records');
	}

	function internationaldealer()
	{	    
		$this->country_id = 'international';
		$this->seller_kind = 'dealer';
		$this->do_action('list_records');
	}

	function index()
	{	 
		$this->set_file ('product/sellpoint_index.tpl'); 
		$this->set_vars($this->caption);
		
		// PROVINCIES
		$provincies = Db::get_rows_array("
						SELECT product__sellpoint.provincia_id
						FROM product__sellpoint
						WHERE status = 'public'
						AND seller_kind = 'shop'
						AND country_id = 'ES'
						AND bin=0 
						GROUP BY
							provincia_id");
		
		$loop_provincies = array();
		if ($provincies){
			$query = "SELECT name, provincia_id FROM all__provincia WHERE provincia_id IN (" . implode_field($provincies) . ") ORDER BY name";
			$loop_provincies = Db::get_rows($query);
		}
		
		$page_file_name = R::text_id('page_file_name');
		
		
		foreach ($loop_provincies as &$rs){
			
			$friendly_params = array('provincia_id'=>$rs['provincia_id'],'seller_kind'=>'shop');
			$rs['sellpoints_link'] = Page::get_link('product','sellpoint',false,false,$friendly_params,false,'',false,$page_file_name);
			
		}
		
		$this->set_loop('provincies_shop', $loop_provincies);
		
		// COMUNITATS
		$comunitats = Db::get_rows_array("
						SELECT product__sellpoint.comunitat_id
						FROM product__sellpoint
						WHERE status = 'public'
						AND seller_kind = 'shop'
						AND country_id = 'ES'
						AND bin=0 
						GROUP BY
							comunitat_id");
		
		$loop_comunitats = array();
		if ($comunitats){
			$query = "SELECT name, comunitat_id FROM all__comunitat WHERE comunitat_id IN (" . implode_field($comunitats) . ") ORDER BY name";
			$loop_comunitats = Db::get_rows($query);
		}
		
		$page_file_name = R::text_id('page_file_name');
		
		
		foreach ($loop_comunitats as &$rs){
			
			$friendly_params = array('comunitat_id'=>$rs['comunitat_id'],'seller_kind'=>'shop');
			$rs['sellpoints_link'] = Page::get_link('product','sellpoint',false,false,$friendly_params,false,'',false,$page_file_name);
			
		}
		
		$this->set_loop('comunitats_shop', $loop_comunitats);
		
		
		
		// enllaç internacional	
		
		$link = Page::get_link('product','sellpoint','international',false,array(),false,false,false,$page_file_name);
		$this->set_var('international_link',$link);
		
		// enllaç internacional	shop
		
		$link = Page::get_link('product','sellpoint','internationalshop',false,array(),false,false,false,$page_file_name);
		$this->set_var('internationalshop_link',$link);
		$this->set_var('international_link',$link);
		
		// enllaç internacional	dealer
		
		$link = Page::get_link('product','sellpoint','internationaldealer',false,array(),false,false,false,$page_file_name);
		$this->set_var('internationaldealer_link',$link);
		
		 
		// enllaç shop
		
		$link = Page::get_link('product','sellpoint','shop',false,array(),false,false,false,$page_file_name);
		$this->set_var('shop_link',$link);
		 
		// enllaç dealer
		
		$link = Page::get_link('product','sellpoint','dealer',false,array(),false,false,false,$page_file_name);
		$this->set_var('dealer_link',$link);
		
		
		
		$GLOBALS['gl_content'] = $this->process();
	}
	
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{
		$show = new ShowForm($this);
		
		// si no hi ha id, error 404
		if (!$show->id) Main::error_404();
		
		$show->condition = "(status = 'public')";
		$show->call('records_walk','url1,url1_name');
		
		$show->get_values();		
		if ($show->has_results)
		{


			// es pot mostrar el camp que digui a la configuració com a títol
			if ($this->is_index) {
				if ($this->config['show_as_title']){
					Page::set_title($show->rs[$this->config['show_as_title']]);
				}
				if ($this->config['show_as_subtitle']) {
					Page::set_title($show->rs[$this->config['show_as_subtitle']]);
				}

				Page::add_breadcrumb($show->rs['empresa']);
			}

			return $show->show_form();
		}
		else
		{
			Main::error_404();
		}
	}
	function records_walk($url1,$url1_name,$sellpoint_description=false){		
		// Això ho poso aquí així no cal posar-ho al tpl, simplement posar: <a href="<=$url1>" target="_blank"><=$url1_name></a>
		$ret['url1_name'] = $url1_name?$url1_name:$url1;
		
		// si es llistat passa lavariable sellpoint_description
		if ($sellpoint_description!==false){
			$ret['sellpoint_description']=add_dots('sellpoint_description', $sellpoint_description);
		}
			
		return $ret;
	}

}
?>