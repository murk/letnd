<?
/**
 * WebSitemap
 *
 * TODO-i Al canviar imatges també canviar data modificació
 * TODO-i Data modificació quan es fa qualsevol canvi, inclús llistat
 * TODO-i Posar robots.txt automàticament
 * TODO-i Canviar prioritats sgons data, cada anys baixar prioritat
 * TODO-i Posar els enllaços segons blocks
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 31/8/2012
 * @version $Id$
 * @access public
 */
class WebSitemap extends Module{

	var $host_url;

	function __construct(){
		parent::__construct();
		$this->host_url = substr(HOST_URL,-1)=='/'?substr(HOST_URL,0,-1):HOST_URL;
	}
	function list_records(){

	    // si existeix l'arxiu el serveixo enlloc del generat
		if (is_file(DOCUMENT_ROOT . 'sitemap.xml')){
			header("Content-type: text/xml");
			Debug::p("No s'ha generat cap sitemap ja que existeix l'arxiu sitemap.xml");
			echo file_get_contents(DOCUMENT_ROOT . 'sitemap.xml');
			Debug::p_all();
			die();
		}

		$langs = $GLOBALS['gl_languages']['public'];
		
		$sitemap = '<?xml version="1.0" encoding="UTF-8"?><urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xhtml="http://www.w3.org/1999/xhtml">';


		// pagines ( lo mateix que als blocks )
		//$this->get_pages( $langs, $sitemap );

		// noticies
		$this->get_news( $langs, $sitemap );

		// projectes
		$this->get_portfolis( $langs, $sitemap );

		// productes
		$this->get_products( $langs, $sitemap );

		// Blunik
		$this->get_tool_urls( $langs, $sitemap, 'racing', 'rally' );
		$this->get_tool_urls( $langs, $sitemap, 'racing', 'suport' );
		$this->get_tool_urls( $langs, $sitemap, 'racing', 'video' );

		$sitemap .='</urlset>';
		header("Content-type: text/xml");
		echo $sitemap;
		html_end();
	}
	function get_pages($langs, &$sitemap) {

		$freq     = 'monthly';
		$priority = '0.5';
		$urls     = [];

		$query = "SELECT all__page.page_id AS page_id, link, has_content, modified
			FROM all__page
			WHERE status = 'public'
			AND bin <> 1
			ORDER BY menu, level, ordre";

		$results = Db::get_rows( $query );


		foreach ( $results as $rs ) {

			$page_id = $rs['page_id'];
			$link = $rs['link'];
			$modified = $rs['modified'];
			$priority = $link == '/' ? '1.0' : '0.8';

			// si no té contingut és el mateix link que el fill, per tant el salto
			if ( $rs['has_content'] ) {

				foreach ( $langs as $lang_code => $lang ) {

					$query = "SELECT page_file_name
						FROM all__page_language
						WHERE page_id = $page_id
						AND language = '$lang'";

					$page_file_name = Db::get_first( $query );

					$urls[ $lang_code ] = Page::get_page_link( $page_id, $link, $page_file_name, $rs['has_content'], false, $lang );

				}
				$sitemap .= $this->get_url($urls, $modified, $freq, $priority);
			}
		}
	}
	function get_news($langs, &$sitemap) {

		$this->get_tool_urls( $langs, $sitemap, 'news', 'new' );

		//  Aquí falta posar els blocks
	}
	function get_portfolis( $langs, &$sitemap ) {


		$this->get_tool_urls( $langs, $sitemap, 'portfoli', 'portfoli' );

		//  Aquí falta posar els blocks
	}
	function get_products( $langs, &$sitemap ) {

		$this->get_tool_urls( $langs, $sitemap, 'product', 'product' );

		//  Aquí falta posar els blocks

	}
	function get_tool_urls( $langs, &$sitemap, $tool, $section ) {

		$freq     = 'monthly';
		$priority = '0.8';
		$urls     = [];

		if ( ! is_table( "${tool}__${section}" ) ) {
			return;
		}

		$module         = Module::load( $tool, $section );
		$module->parent = 'sitemap';
		$loop           = $module->do_action( 'get_records' );

		foreach ( $loop as $rs ) {

			$id = $rs["${section}_id"];
			$modified   = $rs['modified'];

			$category_file_name = false;

			foreach ( $langs as $lang_code => $lang ) {

				$file_name = $this->get_file_name( $tool, $section, $lang, $id );

				$page_file_name = Db::get_first( "SELECT
						page_file_name 
						FROM all__page 
						INNER JOIN all__page_language 
						USING(page_id) 
						WHERE language='$lang' 
						AND link = '/${tool}/${section}'" );

				$params_arr = array(
					'action',
					"${section}_id",
					'id',
					'page',
					'tool',
					'tool_section',
					'language',
					'page_file_name'
				);
				$urls[ $lang_code ] = Page::get_link( $tool, $section, false, $id, $module->friendly_params, $params_arr, $file_name, $category_file_name, $page_file_name, $lang );
			}
			$sitemap .= $this->get_url( $urls, $modified, $freq, $priority );
		}

	}

	function get_file_name( $tool, $section, $lang, $id ) {
		$file_name = Db::get_first( "
						SELECT  ${section}_file_name 
						FROM ${tool}__${section}_language 
						WHERE language='$lang'
						AND ${section}_id = $id" );

		return $file_name;
	}
	function get_url($urls, $date, $freq, $priority){

		$host_url = $this->host_url;
		
		if ($date == '0000-00-00 00:00:00' || !$date) {
			$date = '';
		}
		// si no faig else posa la data d'avui
		else {
		
			$datetime = new DateTime($date);
			// $date = $datetime->format(DateTime::ISO8601); -> 2019-05-28T23:42:17+0200 enlloc de 2019-05-28T23:42:17+02:00
			$date = $datetime->format("Y-m-d\TH:i:sP");
		}

		$ret = '';
		$conta = 0;
		foreach ( $urls as $lang_code => $url ) {

			if ( $this->is_valid_url($url) ) {

				$url =  $this->get_full_url( $url, $host_url );

				$url_xml = "
    					<loc>$url</loc>
    					<lastmod>$date</lastmod>
    					<changefreq>$freq</changefreq>
    					<priority>$priority</priority>";

				foreach ( $urls as $lang_code2 => $url2 ) {

					$url2 = $this->get_full_url( $url2, $host_url );

					if ( $lang_code != $lang_code2 ) {

						$url_xml .= "<xhtml:link rel=\"alternate\" hreflang=\"$lang_code2\" href=\"$url2\" />";

					}
				}
				$ret .= "<url>$url_xml</url>";
			}
		}

		
		return $ret;
	}

	function get_full_url( $url, $host_url ) {
		$url = substr( $url, 0, 7 ) == 'http://' || substr( $url, 0, 7 ) == 'https://'
					? $url : $host_url . $url;
		return $url;
	}

	/**
	 * @param $url
	 *
	 * @return bool
	 */
	function is_valid_url($url) {
		return !
		(
			substr( $url, 0, 6 ) == 'ftp://'
			|| substr( $url, 0, 7 ) == 'mailto:'
			|| substr( $url, 0, 11 ) == 'javascript:'
			|| substr( $url, 0, 4 ) == 'tel:'
		);
	}
}
?>