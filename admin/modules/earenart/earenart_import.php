<?
/**
 * EarenartImport
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 4/2013
 * @version $Id$
 * @access public
 */
class EarenartImport extends Module{
	var $querys = '', $cg;
	function __construct(){
		parent::__construct();
	}
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{		
		$this->set_file('/earenart/import_form.tpl');
		$this->set_vars($this->caption);
	    return $this->process();
	}

	function write_record()
	{
		
		
		Debug::p($_POST, 'text');
		Debug::p($_FILES, 'text');
		
		if ($_FILES) {
			
			// recullo arxiu
			foreach ($_FILES as $key => $upload_file) {
				if ($upload_file['error'] == 1) {
					global $gl_errors, $gl_reload;
					if (!$gl_errors['post_file_max_size']) {
						$gl_message = ' ' . $this->messages['post_file_max_size'] . (int) ini_get('upload_max_filesize') . ' Mb';
						$gl_errors['post_file_max_size'] = true;
						$gl_reload = true;
					}					
					$this->end();
				}
			}
			
			$file_handle = fopen($upload_file['tmp_name'], "r");

			$conta = 0;
			
			$results = array();
			$col_names = array();
			
						
			// mode import o preview
			$is_import = isset($_POST['import']);	
			
			/////////////////
			// IMPORT
			/////////////////
			
			// control de flush
			@apache_setenv('no-gzip', 1);@ini_set('zlib.output_compression', 0);@ini_set('implicit_flush', 1);


			set_inner_html('import', '
				<div>
					<h6>Previsualització importació</h6>
					<table>
						<thead>
							<tr>
								<th>Codi</th>
								<th>Nom</th>
								<th>Preu 1</th>
								<th>Preu 2 iva</th>
							</tr>
						</thead>
						<tbody id="import_table">
						<tr>
							<td></td>
						</tr>
						<tr>
							<td></td>
						</tr>
						<tr>
							<td></td>
						</tr>
						<tr>
							<td></td>
						</tr>
						</tbody>
					</table></div>');
			//Debug::p($results, 'text');
			
			
			while (($data = fgetcsv($file_handle, 1000, ",")) !== FALSE) {		
				
				// noms columnes
				if ($conta==0){
					foreach ($data as $key=>$field){
							$col_names[$key] = $field;
					}
				}
				else{
					$rs = array();
					foreach ($data as $key=>$field){
						if ($col_names[$key] == 'CLAART' || $col_names[$key] == 'CODIGO' || $col_names[$key] == 'NOMBRE' || $col_names[$key] == 'PVP1' || $col_names[$key] == 'PVP2IVA' || $col_names[$key] == 'BAJA')					
						$rs[$col_names[$key]] = $field;
					}
					/*
					echo  '
						<p>
							<td>' . $conta . ' - ' . $rs['NOMBRE'] . '</td>
						</p>';
					set_inner_html('import_table', '
						<tr>
							<td>' . $rs['CODIGO'] . '</td>
							<td>' . $rs['NOMBRE'] . '</td>
							<td>' . $rs['PVP1'] . '</td>
							<td>' . $rs['PVP2IVA'] . '</td>
						</tr>', true);

					 */
					if ($rs['BAJA'] != 'T'){
						$query = "INSERT INTO `product__variation` 
							(`variation_id`, `variation_category_id`, `variation_pvp`, `variation_pvd`) VALUES
							('" . $rs['CLAART'] . "',1, " . $rs['PVP2IVA'] . ", " . $rs['PVP1'] . ");";
						Db::execute($query);


						global $gl_languages;	
						foreach($gl_languages['public'] as $lang){
							$query="
								INSERT INTO `product__variation_language` 
								(`variation_id`, `language`, `variation`) VALUES 
								('" . $rs['CLAART'] . "', '".$lang."', '" . $rs['CODIGO'] . " - " . R::escape($rs['NOMBRE'],false,false) . "');;";
							Db::execute($query);
						}					

						flush();
					}
				}
				$conta++;
			}
			//echo('<pre>');
			//print_r($results);
			//echo('</pre>');
			
			
			$this->check_have_defaults();

			print_javascript('top.$("#confirm").hide();');				
			print_javascript('top.$("#done").show();');				
			
			
		}		
		$this->end();
		
	}
	
	function check_have_defaults(){
	// loop per les categories
		$query = "SELECT DISTINCT variation_category_id 
			FROM product__variation_category_language 
			INNER JOIN product__variation
			USING (variation_category_id)
			WHERE language = '" . LANGUAGE . "'";
		$results = Db::get_rows($query);
		$loop = array();
		
		// Per cada categoria:
		// 		Si no hi ha cap seleccionat, no hi ha cap predeterminada
		// 		Si hi ha almenys una seleccionada, sempre ha d'haver una predeterminada, per tant si s'ha borrat la predeterminada, haig de marcar una altra com a predeterminada
		foreach ($results as $rs)
		{
			// comprovo que hi hagi una predeterminada a la categoria
			$query = "
				SELECT variation_id
				FROM product__variation
				WHERE variation_category_id =" .  $rs['variation_category_id'] ."
				AND variation_default =1
				AND bin = 0";
				Debug::add('Comprovo que hi hagi una predeterminada a la categoria',$query);
				
			// si no hi ha predet comprovo si hi ha una seleccionada
			if (!Db::get_first($query)){
			$query = "
				SELECT variation_id
				FROM product__variation
				WHERE variation_category_id =" .  $rs['variation_category_id'] ."
				AND variation_checked = 1
				AND bin = 0
				LIMIT 1;";
				Debug::add('si no hi ha predet comprovo si hi ha una seleccionada',$query);
			
				// si hi ha almenys una seleccionada marco la primera com predeterminada
				if (Db::get_first($query)){
					$query = "
					UPDATE  product__variation 
					SET  variation_default =  1
					WHERE variation_category_id =" .  $rs['variation_category_id'] . "
					AND variation_checked = 1
					AND bin = 0
					ORDER BY ordre
					LIMIT 1";
					Db::execute($query);
					Debug::add('si hi ha almenys una seleccionada marco la primera com predeterminada', $query);
				}
			}	
		}
	}
	function end($message=false){
		
		if($message) $GLOBALS['gl_page']->show_message($message);
		//if ($this->querys) Debug::add('Save rows querys', $this->querys, 2);
		
		Debug::p_all();
		die();	
	}
}
?>