<!DOCTYPE html>
<html lang="ca">
	<head>
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,400italic,600,700' rel='stylesheet' type='text/css'>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title><?= $name ?></title>
		<style>
			html, body {
				height:100%;
				font-family:'Open Sans',Arial,sans-serif;
				margin: 0;
			}
			h1{
				font-size: 45px;
				color: #000;
				font-weight: 300;
				display: inline-block;
			}
			h1 span{
				font-weight: 700;
			}
			h2{
				font-size: 24px;
				color: #bdbdbd;
				font-weight: 300;
				margin-top: 20px;
				display: inline-block;
				margin-left: 40px;
			}
			div {
				background-color: #f7f7f7;
				padding: 10px 0;
				position: relative;
			}
			h1.letnd{
				max-width: 500px;
				position: absolute;
				bottom: -50px;
				text-align: right;
				margin: 0;
				padding: 0;
			}
			h1.letnd a{
				width: 172px;
				height: 28px;
				background-image: url("/common/images/letnd_construccio.png");
				display: inline-block;
			}
			h1.letnd a:hover {
				background-position: 0 -40px;
			}

		</style>
	</head>	
	<body>	
		
		<table width="100%" height="100%">
			<tr>
				<td valign="middle" align="center">
				
					<div>
						<h1><?= $name ?></h1>
								
						<h2>Web en construcció </h2>
						<h1 class="letnd"><a title="Disseny de pàgines web, programació i desenvolupament web" href="http://www.letnd.com"></a></h1>
					</div>
				</td>
			</tr>
		</table>
	</body>
</html>