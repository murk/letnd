<?

/**
 * InvoiceBatch
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2011
 * @version $Id$
 * @access public
 */
class InvoiceBatch extends Module {

	function __construct() {
		parent::__construct();
	}

	function list_records() {
		$GLOBALS['gl_content'] = $this->get_records();
	}

	function get_records() {

		$this->set_news();

		$listing = new ListRecords( $this );
		$listing->set_options( 1, 1, 1, 1, 0, 1 );
		$listing->order_by = "batch_id DESC";
		$listing->has_bin = false;
		$listing->add_swap_edit();

		$listing->add_button( 'get_pdf_button', 'action=get_pdf', 'boto1', 'before', '', 'target="save_frame"' );
		$listing->add_button( 'get_sepa_button', 'action=get_sepa', 'boto1', 'before', '', 'target="save_frame"' );
		$listing->add_button( 'view_invoices_button', 'menu_id=803', 'boto2', 'before' );
		$listing->add_button( 'send_mail_button', '', 'boto2', 'after', 'top.Invoice.confirm_send_mail_batch', 'target="save_frame"' );

		$listing->call( 'records_walk', '', true );

		return $listing->list_records();
	}

	function set_news() {
		global $gl_news;

		$today = strftime( '%e' );

		$month      = strftime( '%m' );
		$last_month = ucfirst( strftime( '%m', strtotime( "last month" ) ) );
		$next_month = ucfirst( strftime( '%m', strtotime( "next month" ) ) );

		$year      = strftime( '%Y' );
		$last_year = ucfirst( strftime( '%Y', strtotime( "last month" ) ) );
		$next_year = ucfirst( strftime( '%Y', strtotime( "next month" ) ) );

		$month_name      = ucfirst( strftime( '%B' ) );
		$last_month_name = ucfirst( strftime( '%B', strtotime( "last month" ) ) );
		$next_month_name = ucfirst( strftime( '%B', strtotime( "next month" ) ) );


		if ( $today > 15 ) {
			$first_date  = "$year-$month-15";
			$second_date = "$next_year-$next_month-14";
		} else {
			$first_date  = "$last_year-$last_month-15";
			$second_date = "$year-$month-14";
		}


		// Quinzena a quinzena
		$ret = Db::get_row( "SELECT sum(total_price_no_vat) as total_no_vat,  sum(total_price) as total FROM invoice__invoice WHERE bill_date >= '$first_date' AND bill_date <= '$second_date' AND bin = 0 AND invoice__invoice.batch_id <> 0;" );

		$quinzena_total_no_vat = format_currency( $ret['total_no_vat'] );
		$quinzena_total        = format_currency( $ret['total'] );

		// Mes actual
		$ret = Db::get_row( "SELECT sum(total_price_no_vat) as total_no_vat,  sum(total_price) as total FROM invoice__invoice WHERE MONTH(bill_date) = $month AND YEAR(bill_date) = $year AND bin = 0 AND invoice__invoice.batch_id <> 0;" );

		$total_no_vat = format_currency( $ret['total_no_vat'] );
		$total        = format_currency( $ret['total'] );

		// Mes anterior
		$ret = Db::get_row( "SELECT sum(total_price_no_vat) as total_no_vat,  sum(total_price) as total FROM invoice__invoice WHERE MONTH(bill_date) = $last_month AND YEAR(bill_date) = $last_year AND bin = 0 AND invoice__invoice.batch_id <> 0;" );

		$last_total_no_vat = format_currency( $ret['total_no_vat'] );
		$last_total        = format_currency( $ret['total'] );

		// Remeses no cobrades TODO-i Canviar la consulta, mandate_sent es nomes que s'han generat rebuts
		$ret = Db::get_row( "SELECT sum(total_price_no_vat) AS total_no_vat,  sum(total_price) AS total FROM invoice__invoice WHERE batch_id IN (SELECT batch_id FROM invoice__batch WHERE mandate_sent = '0000-00-00') AND bin = 0;" );

		$batch_total_no_vat = format_currency( $ret['total_no_vat'] );
		$batch_total        = format_currency( $ret['total'] );

		// Remeses per cobrar
		$ret = Db::get_row( "SELECT sum(total_price_no_vat) AS total_no_vat,  sum(total_price) AS total FROM invoice__invoice WHERE batch_id = 0 AND bin = 0;" );

		$no_batch_total_no_vat = format_currency( $ret['total_no_vat'] );
		$no_batch_total        = format_currency( $ret['total'] );

		$first_date  = format_date_form( $first_date );
		$second_date = format_date_form( $second_date );

		$gl_news = "
			<div class=\"missatge-titol\"><strong>Totals</strong></div>
			<p>Total per facturar $first_date a $second_date : <strong>$quinzena_total_no_vat €</strong> - $quinzena_total € IVA inclòs</p>
			<p>Total per facturar $last_month_name: <strong>$last_total_no_vat €</strong> - $last_total € IVA inclòs</p>
			<p>Total per facturar $month_name: <strong>$total_no_vat €</strong> - $total € IVA inclòs</p>
			<p>Total remeses per cobrar: <strong>$batch_total_no_vat €</strong> - $batch_total € IVA inclòs</p>
			<p>Total factures sense remesa: <strong>$no_batch_total_no_vat €</strong> - $no_batch_total € IVA inclòs</p>
		";
	}


	function records_walk( &$module ) {
		$rs       = &$module->rs;
		$batch_id = $rs['batch_id'];

		$ret = Db::get_row( "SELECT sum(total_price_no_vat) as batch_total_no_vat,  sum(total_vat) as batch_vat,  sum(total_price) as batch_total FROM invoice__invoice WHERE batch_id = '$batch_id' AND bin = 0;" );

		$ret['batch_bank_receipt_total'] = Db::get_first( "SELECT sum(total_price) FROM invoice__invoice WHERE batch_id = '$batch_id' AND bin = 0 AND billing_type = 'bank_receipt';" );

		$ret['batch_wire_transfer_total'] = Db::get_first( "SELECT sum(total_price) FROM invoice__invoice WHERE batch_id = '$batch_id' AND bin = 0 AND billing_type = 'wire_transfer';" );

		$ret['batch_cash_total'] = Db::get_first( "SELECT sum(total_price) FROM invoice__invoice WHERE batch_id = '$batch_id' AND bin = 0 AND billing_type = 'cash';" );

		foreach ( $ret as &$r ) {
			$r = format_currency( $r );
		}

		$total_invoices    = Db::get_first( "SELECT count(*) FROM invoice__invoice WHERE batch_id = '$batch_id' AND bin = 0;" );
		$total_sent_emails = Db::get_first( "SELECT count(*) FROM invoice__invoice WHERE batch_id = '$batch_id' AND bin = 0 AND invoice_id IN (SELECT invoice_id FROM invoice__message WHERE invoice__message.invoice_id = invoice__invoice.invoice_id AND status = 'sent');" );

		if ( $total_invoices == $total_sent_emails ) {
			$ret['sent_emails'] = "Tots";
		} else {
			$ret['sent_emails'] = "<span class='vermell'>$total_sent_emails de $total_invoices</span>";
		}

		return $ret;

	}

	function show_form() {
		$GLOBALS['gl_content'] = $this->get_form();
	}

	function get_form() {
		$show = new ShowForm( $this );
		$show->set_options( 1, 0, 0, 0 );
		$show->has_bin = false;

		return $show->show_form();
	}

	function save_rows() {
		$save_rows = new SaveRows( $this );
		$save_rows->save();
	}

	function write_record() {

		global $gl_page;
		if ( $this->action == 'add_record' ) {

			$query    = "SELECT count(*) FROM invoice__invoice WHERE batch_id = 0 AND bin = 0 ORDER BY bill_date ASC, invoice_id ASC;";
			$invoices = Db::get_rows( $query );

			Debug::p( [
				[ 'Query factures', $query ],
			], 'invoice' );

			if ( $invoices ) {

				// Creo batch
				$writerec = new SaveRows( $this );
				$writerec->save();

				$batch_id = $writerec->id;

				$fields        = [
					'name',
					'surname1',
					'surname2',
					'company',
					'adress',
					'numstreet',
					'block',
					'flat',
					'door',
					'town',
					'province',
					'country',
					'zip',
					'cif',
					'vat',
					'vat_type',
					'billing_mail',
					'billing_type',
					'billing',
					'comta',
				];
				$select_fields = implode( ',', $fields );

				$series = [0 => 'A', 1 => 'B'];
				foreach ( $series as $key => $serie ) {

					$this->enumerate_invoices( $serie, $select_fields, $fields, $batch_id );

				}

				$gl_page->show_message( 'Remesa creada correctament' );


			} else {
				$gl_page->show_message( 'No hi ha cap factura per fer remesa' );
				Debug::p_all();
				die();
			}

		} else {
			$writerec = new SaveRows( $this );
			$writerec->save();
		}
	}

	/**
	 * @param $serie
	 * @param $select_fields
	 * @param $fields
	 * @param $batch_id
	 */
	private function enumerate_invoices( $serie, $select_fields, $fields, $batch_id ) {


		// Obtinc la última numeració de la serie A
		$invoice_number = Db::get_first( "SELECT max(invoice_number) FROM invoice__invoice WHERE invoice_serial = '$serie'" );
		$invoice_number = (int) $invoice_number;


		$query    = "SELECT * FROM invoice__invoice WHERE batch_id = 0 AND bin = 0 AND invoice_serial = '$serie' ORDER BY bill_date ASC, invoice_id ASC;";
		$invoices = Db::get_rows( $query );

		foreach ( $invoices as $rs ) {

			$invoice_number ++;
			$invoice_id  = $rs['invoice_id'];
			$custumer_id = $rs['custumer_id'];

			// Passo les dades de facturació a invoice__invoice

			$rs_custumer  = Db::get_row( "SELECT $select_fields FROM custumer__custumer WHERE custumer_id = $custumer_id;" );
			$query_update = '';

			foreach ( $fields as $field ) {
				$field_value  = Db::qstr( $rs_custumer[ $field ] );
				$query_update .= "custumer_$field = $field_value, ";
			}

			// Poso també la numeració a cada factura i el batch_id
			$query_update = "UPDATE `invoice__invoice` SET $query_update `invoice_number`='$invoice_number', batch_id = $batch_id WHERE  `invoice_id`= $invoice_id;";

			Debug::p( [
				[ 'Query update', $query_update ],
			], 'invoice' );

			Db::execute( $query_update );
		}
	}

	public function get_pdf() {

		Main::load_class( 'invoice', 'pdf', 'admin' );

		$batch_id = R::id( 'batch_id' );
		$invoices = Db::get_rows( "SELECT * FROM invoice__invoice WHERE batch_id = $batch_id AND bin = 0;" );

		$pdf = New InvoicePdf( $this );

		foreach ( $invoices as $rs ) {
			$pdf->get_body( $rs['invoice_id'] );
		}

		$pdf->output( "remesa_$batch_id" );

		Debug::p_all();
		die();
	}

	public function send_mail() {

		Main::load_class( 'invoice', 'mail', 'admin' );

		$batch_id = R::id( 'batch_id' );
		$invoices = Db::get_rows( "SELECT * FROM invoice__invoice WHERE batch_id = $batch_id AND bin = 0;" );

		$mail = New InvoiceMail( $this );

		$already_sent = [];
		foreach ( $invoices as $rs ) {

			$invoice_id = $rs['invoice_id'];
			$is_sent    = Db::get_first( "SELECT count(*) FROM invoice__message WHERE invoice_id = $invoice_id AND status = 'sent';" );


			if ( $is_sent ) {
				if ( $rs['custumer_billing'] == 'citizen' ) {
					$custumer_name = $rs['custumer_name'] . ' ' . $rs['custumer_surname1'] . ' ' . $rs['custumer_surname2'];
				} else {
					$custumer_name = $rs['custumer_company'];
				}
				$custumer_billing_mail = $rs['custumer_billing_mail'];

				$already_sent [] = "$custumer_name - $custumer_billing_mail";
			} else {


				$mail->send( $invoice_id );
			}
		}

		if ( $already_sent ) {
			$mail->output_js( "Les següents factures ja s'havien enviat abans:<br><br>", 'resaltat' );
			foreach ( $already_sent as $custumer ) {
				$mail->output_js( "$custumer<br><br>", 'vert' );
			}
		}

		$mail->output_js( "Enviament finalitzat<br><br>", 'resaltat' );

		Debug::p_all();
		die();
	}

	public function get_sepa() {

		Main::load_class( 'invoice', 'sepa', 'admin' );

		$batch_id = R::id( 'batch_id' );
		$invoices = Db::get_rows( "SELECT * FROM invoice__invoice WHERE batch_id = $batch_id AND billing_type = 'bank_receipt' AND total_price <> 0.00 AND custumer_comta <> '' AND bin = 0 ORDER BY invoice_id;" );


		$sepa = New InvoiceSepa( $this );

		foreach ( $invoices as $rs ) {
			$sepa->add_payment( $rs['invoice_id'] );
		}

		Db::execute( "UPDATE invoice__batch SET mandate_sent = now() WHERE batch_id = $batch_id;" );
		$sepa->output( "remesa_$batch_id" ); // aquí fa un die(), per tant haig d'actualitzar data abans, i si ha fallat, treure-la desprès

		if ($sepa->errors) {
			Db::execute( "UPDATE invoice__batch SET mandate_sent = '0000-00-00 00:00:00' WHERE batch_id = $batch_id;" );
		}


		$invoices_errors = Db::get_rows( "SELECT * FROM invoice__invoice WHERE batch_id = $batch_id AND billing_type = 'bank_receipt' AND (total_price = 0.00 OR custumer_comta = '') AND bin = 0 ORDER BY invoice_id;" );

		// TODO-i URGENT Aquests error no es visualitzen si es genera l'arxiu, s'hauria de fer un redirect per descarregar l'arxiu a part
		if ( $invoices_errors ) {

			$this->output_js( "Possibles errors en les factures, falta núm compte o preu<br><br>", 'resaltat' );


			foreach ( $invoices_errors as $rs ) {


				$custumer_id = $rs['custumer_id'];
				$custumer_name = $rs['custumer_name'];
				$custumer_surname1 = $rs['custumer_surname1'];
				$custumer_surname2 = $rs['custumer_surname2'];
				$custumer_company = $rs['custumer_company'];
				$total_price = $rs['total_price'];
				$custumer_comta = $rs['custumer_comta'];

				$link = "<a href=\"/?menu_id=5003&action=show_form_edit&custumer_id=$custumer_id\">";
				$link2 = "</a>";

				$error = "$custumer_name $custumer_surname1 $custumer_surname2";

				if ($custumer_company)
					$error = "$custumer_company - $error";

				$error = "$link$error$link2";

				if (!(float)$total_price)
					$error = "$error - No hi ha preu";

				if (!$custumer_comta)
					$error = "$error - No té num. de compte";


				$this->output_js( "$error<br><br>", 'vermell' );

			}
		}

		Debug::p_all();
		die();
	}



	public function output_js( $message, $class, $encode = false ) {

		$message = json_encode( "<span class=\"$class\">$message</span>" );


		echo( '<SCRIPT LANGUAGE="JavaScript" TYPE="text/javascript">
			parent.Invoice.write_report(' . $message . ');
			</SCRIPT>' );

		// this is to make the buffer achieve the minimum size in order to flush data
		echo str_repeat( ' ', 1024 * 64 );
		ob_flush();
		flush();

	}

}