<div class="intro">
	<div class="introleft">   
  Desde el día 2 de enero ha abierto las puertas <strong>MIRONAUTO</strong>, un nuevo taller de reparaciones de turismos, furgonetas, vehículos comerciales y camiones (hasta 5.000 Kg de tara), con un equipo profesional y preparado para las nuevas tecnologías, donde recibiréis un trato personalizado digno de sus expectativas.<br />
Nuestras instalaciones son modernas y respetuosas con el medio ambiente.
Hacemos cualquier tipo de reparación, sea mecánica en general, plancha y pintura, eléctrica (diagnosis con ordenador, comprobaciones de circuitos de carga de baterías, servicio de diagnosis y carga de aire acondicionado...)
  </div>
	<div class="introtextv">
<h2>NO SOMOS OTRO TALLER, SOMOS TU TALLER.</h2>
</div>
</div>
