<?php
set_tool('custumer', 'history', 'custumer__history', 'history_id');
set_language_vars('inmo', 'property', 'history');
set_config('custumer/custumer_history_config.php');
Module::get_config_values();


function save_rows()
{
    
}

function list_records($javascript_action=false)
{
    global $gl_content, $gl_page, $gl_action, $tpl, $gl_caption, $gl_menu_id, $gl_messages, $gl_message, $gl_process;

    if ($javascript_action) $gl_action = $javascript_action;

    $property_id = R::id('property_id');


	// sino puc estar editant aquesta propietat, em fa fora directe
	inmo_check_user_permission($property_id, 'inmo__property', 'property_id');

	$module = Module::load('custumer','history');
	$module->parent = 'inmo__property';
	$module->property_id = $property_id;
	$content = $module->do_action('get_records');


	if ($javascript_action) return $content;
    show_form_history($content);
}

function list_records_walk1 ($custumer_id, $name , $surname1, $surname2)
{
	$name=  $name . ' ' . $surname1 . ' ' . $surname2;
	$name= addslashes($name); // cambio ' per \'
	$name= htmlspecialchars($name); // cambio nomès cometes dobles per  &quot;
	$ret['tr_jscript'] = 'onClick="parent.insert_address(' . $custumer_id . ',\'' . $name . '\')"';
    return $ret;
}
function show_form_history($content)
{
    global $tpl, $gl_content, $gl_caption, $gl_message;
    $property_id = R::id('property_id');
	// sino puc estar editant aquesta propietat, em fa fora directe
	inmo_check_user_permission($property_id, 'inmo__property', 'property_id');
	
    $query = "SELECT property_id, ref, tipus, property_private FROM inmo__property WHERE property_id = " . $property_id;

    $results = Db::get_rows($query);
    $rs = current($results);
    $rs['show_tipus_temp'] = $rs['tipus'] == 'temp'?'block':'none';
    $show_form = new ShowForm;
    $show_form->set_vars_common();
    $show_form->set_vars_form();
    $tpl->set_vars($rs);
    $tpl->set_vars($show_form->rs);
    if (!$content)
    {
        $gl_message = '';
    }
    $tpl->set_var('content', $content);
    $tpl->set_file('inmo/property_history.tpl');
    $gl_content = $tpl->process();
    show_search_form();
}

// TODO-i Aquest queda nomes per price, anular-ho i centralitzar tot a custumer__history
function show_form()
{
    global $gl_action, $gl_page, $tpl;
    $property_id = R::id('property_id');
	inmo_check_user_permission($property_id, 'inmo__property', 'property_id');

	// quan vinc de historial de preus no em passa property_id
	if (!$property_id){
		$history_id = R::id( 'history_id' );
		$property_id = Db::get_first( "SELECT property_id FROM custumer__history WHERE history_id = " . $history_id );
	}

	$query = "SELECT ref, property_private FROM inmo__property WHERE property_id = " . $property_id;
	$rs = Db::get_row($query);
	$tpl->set_vars($rs);
	
    $gl_page->template = 'window.tpl';
	
    $show = new ShowForm;
	
	$show->set_field('property_id', 'type', 'hidden');
	$show->set_field('property_id', 'default_value', $property_id);
	
	$show->set_field('custumer_id', 'form_admin', 'input');
	
	// permisos del desplegable
	$customer_id_condition = $show->get_field('custumer_id', 'select_condition');
	$customer_id_condition = inmo_history_get_user_permission($customer_id_condition);
	
	$show->set_field('custumer_id', 'select_condition', $customer_id_condition);
	
	$show->set_field('tipus', 'default_value', 'like_property');
	
	$show->tpl->set_var('is_custumer', false);// per diferenciar si estem a custumer o inmo
	
    //$template = 'custumer/custumer_form';
    //$show->template = $template;
    $show->show_form();
}

// TODO-i Aquest queda nomes per price, anular-ho i centralitzar tot a custumer__history
function write_record()
{
    global $gl_action, $gl_insert_id, $gl_saved, $gl_reload, $gl_process;
	
	if ($gl_action=='bin_record_history') {
		$history_id = R::escape('history_id'); // per eliminar desde el jscript
		if ($history_id) $_POST['history_id'] = $history_id;
	}
	
    $writerec = new SaveRows;
	
	
	// miro permisos de la propietat que pertany l'historial, si s'afegeix ppropietat id=0 , sempre es te permis
	$property_id = Db::get_first("SELECT property_id FROM custumer__history WHERE history_id = " . $writerec->id);
	inmo_check_user_permission($property_id, 'inmo__property', 'property_id');
	
    $writerec->save();
    // relaciono el custumer nou amb la propietat
    if ($gl_saved)
    {
	    if ( R::id( 'property_id' ) ) {
		    set_inner_html( 'historys', list_records( 'list_records_history' ) );
	    }
	    else {
		    global $gl_page, $gl_message;
		    $_SESSION['message'] = $gl_message;
		    print_javascript( 'parent.parent.window.location.reload();' );
		    debug::p_all();
		    die();
	    }
    }
}

?>