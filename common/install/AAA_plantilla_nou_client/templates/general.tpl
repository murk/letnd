<?
show_under_construction( 'Nom Empresa' );
/*
TODO-i HI HA VIDA MÉS ENLLÀ DE 1920!!!!!!!!!!!!!!
TODO-i EXISTEIX UNA LLIBRERIA D'EFECTES CSS QUE ES DIU HOVER.CSS!!!!

TODO-i Nom empresa a general.tpl
TODO-i Dades empresa, nom, adreça, tel, titol defecte web pública
TODO-i Borrar arxius a template/page que no siguin necessaris
TODO-i Posar les fonts al head
TODO-i Maquetar paginació amb els 2 botons de endavant i 2 de enrera (.pageResultsPreviousSet, .pageResultsNextSet)
TODO-i Favicon
TODO-i Javascript - Anivellar columnes a home si es necessari, borrar les que no ho són
TODO-i Javascript - Anivellar columnes a news-product-inmo si es necessari, borrar les que no ho són
TODO-i Deixar clean.tpl com general.tpl
TODO-i Comprovar a contacte email resposta: /cat/contact/contact/send_test/
TODO-i Comprovar a contacte la finestra de "He llegit i accepto ...."
TODO-i Comprovar pàgines de text, com ara "avis legal"
TODO-i Contacte maquetar el form_sent i form_not_sent
TODO-i Contacte comprobar missatges de camps obligatoris
TODO-i Comprovar tots els registres d'idiomes que siguin correctes -> /admin/?menu_id=1033
TODO-i Canviar el marker.png dels punts dels mapes
TODO-i Comprovar privilegis
TODO-i Comporvar vcard a https://search.google.com/structured-data/testing-tool/u/0/
TODO-i Posar ordre en les frases d'inici a la taula client__configpublic
TODO-i Si la web es d'en Xavi ( VANGUART ) canviar el peu de letnd per <?= $c_seo_custom_2 ?>
TODO-i Comentar si vol analytics, maps i captcha al client ( activar-ho a admin )
TODO-i Posar les mides que surten al funcions.js a el res_sizes.css

//{{#is_inmo}}
    INMO-TEMP
    TODO-i Contractes de lloguer turístic
    	UPDATE `booking__configadmin` SET `value`='1' WHERE  `name`='has_contracts';
    	UPDATE `booking__configadmin` SET `value`='spa' WHERE  `name`='contract_default_language';
    	UPDATE `booking__configadmin` SET `value`='booked' WHERE  `name`='default_status';
    	UPDATE `booking__configadmin` SET `value`='booked' WHERE  `name`='public_default_status';
    	UPDATE `booking__configadmin` SET `value`='docx' WHERE  `name`='contract_format'; # docx | pdf

    	UPDATE booking__configadmin` SET `value`='booked' WHERE  `name`='public_default_status'; # pre-booked si es vol pagar online
    	UPDATE `booking__configadmin` SET `value`='booked' WHERE  `name`='default_status';

    TODO-i Mossos
    	UPDATE `booking__configadmin` SET `value`='' WHERE  `name`='codi_establiment';
    	UPDATE `booking__configadmin` SET `value`='' WHERE  `name`='nom_establiment';

//{{/is_inmo}}
CSS
TODO-i Canviar estils per defecte a les variables de styles.scss
TODO-i Canviar la font de BODY i la de input, select, textarea, les fonts sempre amb 2 mínim tal com està a google fonts: font-family: 'Glegoo', serif; a  a les variables de styles.scss
TODO-i Tots els hovers i actives i dels SUBMENUS TAMBÉ!
TODO-i Line-height dels headers - Solen ser més petits que el cos, mirar els headers en varies linees
TODO-i Missatges de no es troben resultats: #message a contact.css

////////////////////////////////////////////////////////////////////////////////////////////
CONFIGURACIONS -> Guardar-ho per penjar a public quan el client ja està treballant a la web al servidor

# TODO-i Resultats per pàgina a all__configpublic
	UPDATE `all__configpublic` SET `value`='10' WHERE  name='max_page_links';
	UPDATE `all__configpublic` SET `value`='10' WHERE  name='max_results';
	UPDATE `all__configpublic` SET `value`='200' WHERE  name='add_dots_length';

# TODO-i Add_dots a news__configpublic
	UPDATE `news__configpublic` SET `value`='250' WHERE  name='add_dots_length';

# TODO-i Configuracio de blocks:
	UPDATE `news__configpublic` SET `value`='3' WHERE  name='block_max_results';
	UPDATE `all__block` SET `vars`='in_home=1&show_images=1&show_files=1&add_dots_length=330' WHERE  `block_id`='news_home';

	//{{#is_inmo}}
	    UPDATE `inmo__configpublic` SET `value`='6' WHERE  name='block_max_results';
    	UPDATE `inmo__configpublic` SET `value`='10' WHERE  name='visited_max_results';

	//{{/is_inmo}}
	//{{#is_product}}
	    UPDATE `product__configpublic` SET `value`='6' WHERE  name='destacat_max_results';
    	UPDATE `product__configpublic` SET `value`='10' WHERE  name='visited_max_results';

    	# Nombre del block related
    	UPDATE `product__configpublic` SET `value`='6' WHERE  name='related_max_results';

	//{{/is_product}}

# TODO-i Mida imatges banners a web__banner_place_language
	UPDATE web__banner_place_language SET `place`='General (1920x300 )' WHERE  `place_id`=1;

# TODO-i Mida videos
# TODO-i Mida a function.js a la funció init_youtube_frames()
	UPDATE `news__configadmin` SET `value`='Ample: 720 px' WHERE  name='videoframe_info_size';
	//{{#is_product}}
	    UPDATE `product__configadmin` SET `value`='Ample: 720 px' WHERE  name='videoframe_info_size';
	//{{/is_product}}
	//{{#is_inmo}}
	    UPDATE `inmo__configadmin` SET `value`='Ample: 720 px' WHERE  name='videoframe_info_size';
	//{{/is_inmo}}

# TODO-i Tamany imatges a news__configadmin, product__configadmin, inmo__configadmin

#NOTICIES
	# Imatges llistat
	UPDATE `news__configadmin` SET `value`='200' WHERE  name='im_thumb_w';
	UPDATE `news__configadmin` SET `value`='' WHERE  name='im_thumb_h';

	# Imatges fitxa
	UPDATE `news__configadmin` SET `value`='540' WHERE  name='im_details_w';
	UPDATE `news__configadmin` SET `value`='' WHERE  name='im_details_h';

	# Imatges ampliades
	UPDATE `news__configadmin` SET `value`='1100' WHERE  name='im_medium_w';
	UPDATE `news__configadmin` SET `value`='733' WHERE  name='im_medium_h';

	# Ratio
	UPDATE `news__configadmin` SET `value`='Imatge Slider=1.5;' WHERE  name='im_custom_ratios';

//{{#is_product}}
    #PRODUCTES
    	# Imatges llistat
    	UPDATE `product__configadmin` SET `value`='200' WHERE  name='im_thumb_w';
    	UPDATE `product__configadmin` SET `value`='' WHERE  name='im_thumb_h';

    	# Imatges fitxa
    	UPDATE `product__configadmin` SET `value`='540' WHERE  name='im_details_w';
    	UPDATE `product__configadmin` SET `value`='' WHERE  name='im_details_h';

    	# Imatges ampliades
    	UPDATE `product__configadmin` SET `value`='1100' WHERE  name='im_medium_w';
    	UPDATE `product__configadmin` SET `value`='733' WHERE  name='im_medium_h';

    	# Ratio
    	UPDATE `product__configadmin` SET `value`='Imatge Slider=1.5;' WHERE  name='im_custom_ratios';

    	# cart v2
    	UPDATE `product__configpublic` SET `value`='1' WHERE  name='use_v2_cart';

//{{/is_product}}
//{{#is_inmo}}
    #INMO
    	# Imatges llistat
    	UPDATE `inmo__configadmin` SET `value`='200' WHERE  name='im_thumb_w';
    	UPDATE `inmo__configadmin` SET `value`='' WHERE  name='im_thumb_h';

    	# Imatges fitxa
    	UPDATE `inmo__configadmin` SET `value`='540' WHERE  name='im_details_w';
    	UPDATE `inmo__configadmin` SET `value`='' WHERE  name='im_details_h';

    	# Imatges ampliades
    	UPDATE `inmo__configadmin` SET `value`='1100' WHERE  name='im_medium_w';
    	UPDATE `inmo__configadmin` SET `value`='733' WHERE  name='im_medium_h';

    	# Ratio
    	UPDATE `inmo__configadmin` SET `value`='Imatge Slider=1.5;' WHERE  name='im_custom_ratios';
//{{/is_inmo}}
*/
?>
<!DOCTYPE html>
<html lang="<?= LANGUAGE_CODE ?>">
<head>

	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
	<meta name="format-detection" content="telephone=no"/>

	<link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,300,400italic,600,600italic,700,700italic' rel='stylesheet' type='text/css'>

	<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<? /* <script type="text/javascript" src="/common/jscripts/jquery/jquery.migrate.1.4.js"></script> */ ?>

	<?= load_scss() ?>
	<?= get_header( $this ) ?>

</head>

<body>

	<? /*
		////////////////////////////////////////
		HEADER
		////////////////////////////////////////
		*/ ?>

	<header id="header">

		<nav id="languages">
			<?= get_block( 'languages' ) ?>
		</nav>

		<address>
			<a class="email" href="mailto:<?= encode_email( $default_mail ) ?>"><?= encode_email( $default_mail ) ?></a>
			<a href="<?= get_phone_link(PAGE_PHONE) ?>" title="<?= PAGE_PHONE ?>" class="tel"><?= PAGE_PHONE ?></a>
		</address>

		//{{^is_web}}
		<? /* BUSCADOR */ ?>
		//{{/is_web}}

			//{{#is_product}}
			<form id="search" action="/<?= $language ?>/product/product/search/" method="get" onsubmit="if (this.q.value=='') {return false}">
			//{{/is_product}}

			//{{#is_inmo}}
			<form id="search" action="/<?= $language ?>/inmo/property/list-records-search-search/" method="get" onsubmit="if (this.q.value=='') {return false}">
			//{{/is_inmo}}

			//{{^is_web}}
				<input name="q" value="<?= htmlspecialchars( R::escape( 'q' ) ) ?>" type="text" placeholder="<?= $c_title_search ?>..."/>
				<input type="submit" title="<?= htmlspecialchars( $c_title_search ) ?>" value="<?= htmlspecialchars( $c_title_search ) ?>"/>
			</form>
			//{{/is_web}}


			<a id="logo" href="<?= get_page_link( 'home' ) ?>"><img src="<?= DIR_TEMPLATES ?>images/logo.png" width="228" height="37" alt="<?= COMPANY_NAME ?>"/></a>

			<h3><?= $page_slogan ?></h3>

			<div id="menu-mobile"><?= $c_menu_mobil ?></div>

			<div id="submenu-mobile">
				<div id="main-nav-mobile"></div>
				<div id="login-nav-mobile"></div>
			</div>

			<nav id="main-nav">
				<?= get_block( 'main_nav' ) ?>
			</nav>

			//{{#is_product}}

			<? /* LOGIN  */ ?>
			<div id="login-nav">
				<? if ( is_product_logged() ): ?>
					<?= get_block( 'product_logged' ) ?>
				<? else: //?>
					<?= get_block( 'product_nologged' ) ?>
				<? endif //default?>
			</div>
			<? /* FI LOGIN */ ?>

			<? /* CART  */ ?>
			<div id="cart_block">
				<?= get_block( 'product_cart' ) ?>
			</div>
			<? /* FI CART */ ?>

			//{{/is_product}}

	</header>

	<main class="<?= $css_selector_class ?>"<?= $css_selector_home_id ?>>

		<? if($is_home): ?>
			<? /*
		////////////////////////////////////////
		HOME
		////////////////////////////////////////
		*/ ?>


			<? /* SLIDER H3 */ ?>
				<?= get_block( 'banner_home_slider' ) ?>
				//{{^is_web}}
				<?= get_block( '//{{solution}}_home_slider' ) ?>
				//{{/is_web}}

			<? /* HEADER H1 - H2 */ ?>
			<header id="header-home">

				<h1><?= $c_inici_text_1 ?></h1>
				<h2><?= $c_inici_text_2 ?></h2>

			</header>

			<? /* SECCIONS H4 - H6 */ ?>
			//{{#is_inmo}}
				<?= get_block( 'inmo_home' ) ?>

			//{{/is_inmo}}
			//{{#is_product}}
				<?= get_block( 'product_destacat' ) ?>

			//{{/is_product}}
			<section id="text-home" class="container">

				<h3><?= $c_inici_text_3 ?></h3>
				<div><?= $c_inici_text_4 ?></div>

			</section>

			<?= get_block( 'news_home' ) ?>


		<? else: //?>
			<? /*
		////////////////////////////////////////
		INTERIOR
		////////////////////////////////////////
		*/ ?>

			<?= get_block( 'breadcrumb' ) ?>//{{^is_web}}


    			<? if($tool == '//{{solution}}'): ?>
    				<nav id="left-nav">
    					<? foreach($blocks_l as $b): extract( $b ) ?>
    						<?= $block ?>
    					<? endforeach //$blocks?>
    				</nav>
    			<? endif //tool?>
		//{{/is_web}}

			<section id="<?= $css_selector_id ?>">

				<header>
					<? /* HEADER H1 - H2 */ ?>
					<h1 title="<?= htmlspecialchars( $title ) ?>"><?= $title ?></h1>
					<? if($body_subtitle): ?>
						<h2 title="<?= htmlspecialchars( $body_subtitle ) ?>"><?= $body_subtitle ?></h2>
					<? endif //body_subtitle?>
				</header>

				<? if($message): ?>
					<div id="message" class="container"><?= $message ?></div>
				<? endif //message?>

				<?= $content ?>//{{#is_inmo}}

                <? if($tool == 'inmo'): ?>
                    <?= get_block( 'inmo_visited' ) ?>
                <? endif //inmo ?>
				//{{/is_inmo}}

			</section>

		<? endif //is_home?>

	</main>


	<? /*
		////////////////////////////////////////
		PEU
		////////////////////////////////////////
		*/ ?>

	<footer>

		<nav id="foot-links">
			<?= get_block( 'footer_nav' ) ?> <br>
			<?= get_block( 'footer_nav_second' ) ?>
		</nav>

		<address class="vcard">

			<? /* Per fer el logo com a logo i nom de l'empressa
				
				<img class="logo fn org" title="<?=htmlspecialchars(COMPANY_NAME)?>" src="<?=DIR_TEMPLATES?>images/logo.png" width="72" height="49" alt="<?=htmlspecialchars(COMPANY_NAME)?>" /> 
				
				*/ ?>
			<strong class="fn org"><?= COMPANY_NAME ?></strong>

			<p class="adr">
					<span class="street-address"><?= PAGE_ADDRESS ?> <?= PAGE_NUMSTREET ?><? if(PAGE_FLAT): ?>,
							<?= PAGE_FLAT ?><? endif //  ?><? if(PAGE_DOOR && PAGE_FLAT): ?>-<?= PAGE_DOOR ?><? endif //  ?></span>
				<br>
				<span class="postal-code"><?= PAGE_POSTCODE ?></span>
				<span class="locality"><?= PAGE_MUNICIPI ?></span> <br>
				<span class="region"><?= PAGE_PROVINCIA ?></span>,
				<span class="country-name"><?= PAGE_COUNTRY ?></span>
			</p>
			<p>
					<a href="<?= get_phone_link(PAGE_PHONE) ?>" class="tel">
						<abbr class="type" title="work">Tel</abbr>. <span class="value"><?= PAGE_PHONE ?></span>
					</a><? if(PAGE_MOBILE): ?>,

					<a href="<?= get_phone_link(PAGE_MOBILE) ?>" class="tel">
					<abbr class="type" title="cell">Mob</abbr>. <span class="value"><?= PAGE_MOBILE ?></span>
					</a><? endif //PAGE_MOBILE?><? if(PAGE_FAX): ?>,

					<a href="<?= get_phone_link(PAGE_FAX) ?>" class="tel">
					<abbr class="type" title="fax">Fax</abbr>. <span class="value"><?= PAGE_FAX ?></span>
					</a><? endif //FAX?>
			</p>

			<p>
				<a class="url" href="<?= HOST_URL ?>"><?= $short_host_url ?></a> /
				<a class="email" href="mailto:<?= encode_email( DEFAULT_MAIL ) ?>"><?= encode_email( DEFAULT_MAIL ) ?></a>
			</p>

		</address>

		<div id="letnd"><?= $c_seo_custom_4 ?></div>

	</footer>

	<iframe name="save_frame" id="save_frame" class="<?= $showiframe ?>"></iframe>

	<? /* JSCRIPT */ ?>
	<script type="text/javascript">

		var gl_language = '<?=$language?>';
		var gl_is_home = <?=$is_home?'true':'false'?>;
		var gl_tool = '<?=$tool?>';
		var gl_tool_section = '<?=$tool_section?>';
		var gl_action = '<?=$action?>';
		var gl_page_text_id = '<?=$page_text_id?>';
		var gl_dir_templates = '<?=DIR_TEMPLATES?>';
		var gl_has_google_maps_key = <?= GOOGLE_MAPS_KEY?'true':'false' ?>;

		//{{#is_product}}
		var gl_product_link = '<?= get_page_link( 'product' ) ?>';
		var gl_format_price_as_currency = '<?= get_config_var( 'format_price_as_currency', 'product__configpublic' ) ?>';
		//{{/is_product}}

		 $(function () {
		   <?=$javascript?>
		 });
	</script>

	<? if(can_accept_cookies()): ?>
		<?= GOOGLE_ANALYTICS ?>
	<? else: //?>
		<?= get_cookies_message() ?>
	<? endif //cookies?>

	<? /*
        TODO-i Treure jcf sino va, o carregar més tipus de common\jscripts\jcf-1.2.3
        TODO-i Descomentar Vacacional de inmo si tenen lloguer turistic
     */ ?>

	<?= load_jscript( '
			/common/jscripts/lightgallery/js/lightgallery.min.js,
			/common/jscripts/lightgallery/js/lg-thumbnail.min.js,
			/common/jscripts/lightgallery/js/lg-zoom.min.js,
			/common/jscripts/jquery.cookie.js,
			/common/jscripts/slick/slick-1.8.0/slick.min.js,
			/common/jscripts/jcf-1.2.3/jcf.js,
			/common/jscripts/jcf-1.2.3/jcf.checkbox.js,
			/common/jscripts/jcf-1.2.3/jcf.select.js,
			/common/jscripts/jquery.blockUI-2.70.js,
			/common/jscripts/jquery-ui-1.11.2/jquery-ui.min.js,
			/admin/jscripts/validacio.js,
			/common/jscripts/functions.js,' .
			//{{#is_inmo}}
			// /* DESCOMENTAR A INMO VACACIONAL */ '/public/modules/booking/jscripts/functions.js,'.
			// /* DESCOMENTAR A INMO VACACIONAL */ '/common/jscripts/multifile/jquery.MultiFile.js,'.
			//{{/is_inmo}}
			//{{#is_product}}
			'/public/modules/product/jscripts/functions.js,'.
			DIR_TEMPLATES.'jscripts/functions_product.js,'.
			//{{/is_product}}
            DIR_TEMPLATES . 'jscripts/functions.js'
	) ?>
	<script type="text/javascript" src="/common/jscripts/jquery-ui-1.11.2/languages/datepicker-<?= $language_code ?>.js"></script>
	<script type="text/javascript" src="/admin/languages/<?= $language ?>.js"></script>

	<? if ( has_google_recaptcha() ): ?>
		<script src='https://www.google.com/recaptcha/api.js?hl=<?= LANGUAGE_CODE ?>'></script>
	<? endif ?>
	<? if ( GOOGLE_MAPS_KEY ): ?>
		<script type="text/javascript" src="//maps.google.com/maps/api/js?key=<?= GOOGLE_MAPS_KEY ?>"></script>
	<? endif // letnd:AIzaSyAaIGZEloZFoSY8TuTDyL27MDbuDvW5AoM ?>
	<? /* FI JSCRIPT */ ?>

</body>
</html>