<?

/**
 * LassdiveActivitat
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2010
 * @version $Id$
 * @access public
 */
class LassdiveActivitat extends Module {
	var $is_newsletter = false;

	function __construct() {
		parent::__construct();
	}

	function on_load() {

	}

	function list_records() {
		$GLOBALS['gl_content'] = $this->get_records();
	}

	function get_records() {

		$parent_id = R::id( 'parent_id' );

		if ( $this->process == 'packs' ) {
			// Nomes miro si es un especial quan estic a la primera pàgina, quan ja tinc un parent_id no cal mirar-ho, tots pertanyen a una categoria especial de totes formes
			$this->set_field('parent_id', 'select_condition',
				'lassdive__activitat.activitat_id=lassdive__activitat_language.activitat_id AND language=\'' . LANGUAGE . '\' AND parent_id=0 AND is_pack > 0 AND lassdive__activitat.activitat_id != \'' . R::id('activitat_id') . '\' ORDER BY ordre ASC, activitat ASC');
		}
		else {
			$this->set_field('parent_id', 'select_condition',
				'lassdive__activitat.activitat_id=lassdive__activitat_language.activitat_id AND language=\'' . LANGUAGE . '\' AND parent_id=0 AND is_pack = 0 AND lassdive__activitat.activitat_id != \'' . R::id('activitat_id') . '\' ORDER BY ordre ASC, activitat ASC');
		}



		$listing = new ListRecords( $this );

		$listing->call( 'records_walk', '', true );
		$listing->set_field_position( 'activitat', '1' );
		$listing->set_field_position( 'activitat_title', '2' );
		$listing->set_field_position( 'parent_id', '3' );

		$listing->add_swap_edit();

		if ( ! $listing->is_editable ) {
			$listing->set_field( 'in_home', 'type', 'select' );
		}

		$listing->add_filter( 'status' );
		$listing->add_filter( 'parent_id' );
		$listing->add_filter( 'in_home' );

		if ( $this->process == 'packs' ) {
			// Nomes miro si es un especial quan estic a la primera pàgina, quan ja tinc un parent_id no cal mirar-ho, tots pertanyen a una categoria especial de totes formes
			if ( $parent_id === false ) {
				$listing->condition = "is_pack > 0";
			}
		}
		else {
			$listing->condition = "is_pack = 0";
		}

		switch ( $this->process ) {
			case 'select_activitats':
				$page                = &$GLOBALS['gl_page'];
				$page->template      = 'clean.tpl';
				$page->title         = '';
				$this->is_newsletter = true;
				$this->set_field( 'content', 'list_admin', '0' );
				$listing->set_options( 0, 0, 0, 0, 0, 0, 0, 0, 1 );
				$listing->condition    .= " AND (status = 'public')";
				$GLOBALS['gl_message'] = $GLOBALS['gl_messages']['select_item'];

				break;
			default:
				if ( $parent_id === false ) {
					$listing->condition .= " AND (parent_id = '0')";
					$listing->add_button( 'see_activities', '', 'boto2', 'after', '', '', 'parent_id' );
				}
				$listing->add_button( 'see_products', 'menu_id=20004', 'boto2', 'after', '', '', 'family_id' );
				$listing->add_button( 'edit_calendar', 'menu_id=2205', 'boto2', 'after', '', '', 'family_id' );
		} // switch*/

		$listing->order_by = 'ordre ASC, activitat ASC';

		return $listing->list_records();
	}

	function records_walk( &$listing ) {
		$ret = array();

		if ( $this->is_newsletter ) {
			$ret = $this->list_records_walk_select( $listing->rs['activitat_id'], $listing->rs['activitat'] );
		}

		$has_children = Db::get_first( "SELECT count(*) FROM lassdive__activitat WHERE parent_id = " . $listing->rs['activitat_id'] );

		$listing->buttons['see_activities']['show'] = $has_children;


		return $ret;
	}

	function show_form() {
		$GLOBALS['gl_content'] = $this->get_form();
	}

	function get_form() {

		$this->set_field( 'entered', 'default_value', now( true ) );
		$this->set_field( 'modified', 'default_value', now( true ) );

		$show = new ShowForm( $this );

		$show->set_form_level2_titles( 'entered' );
		$show->set_form_level1_titles( 'minimum_age', 'activitat', 'timetable', 'price', 'needed_material', 'file', 'page_title' );

		return $show->show_form();
	}

	function save_rows() {
		$save_rows = new SaveRows( $this );
		$save_rows->save();
	}

	function write_record() {
		$writerec = new SaveRows( $this );
		$writerec->set_field( 'modified', 'override_save_value', 'now()' );
		$writerec->field_unique = "activitat_file_name";
		$writerec->save();
	}

	function manage_images() {
		$image_manager = new ImageManager( $this );
		$image_manager->execute();
	}

	/////////////
	// NEWSLETTER
	///////////////
	// Per poder escollir productes desde una finestra amb frame, de moment desde newsletter
	function select_frames() {
		$page = &$GLOBALS['gl_page'];

		$page->menu_tool      = false;
		$page->menu_all_tools = false;
		$page->template       = '';

		$this->set_file( 'lassdive/activitat_select_frames.tpl' );
		$this->set_vars( $this->caption );
		$this->set_var( 'menu_id', $GLOBALS['gl_menu_id'] );
		// $this->set_var('custumer_id',$_GET['custumer_id']);
		$GLOBALS['gl_content'] = $this->process();
	}

	// per posar el onclick al llistat
	function list_records_walk_select( $product_id, $product_title ) {
		$product_title     = addslashes( $product_title ); // cambio ' per \'
		$product_title     = htmlspecialchars( $product_title ); // cambio nomès cometes dobles per  &quot;
		$ret['tr_jscript'] = 'onClick="parent.insert_address(' . $product_id . ',\'' . $product_title . '\')"';

		return $ret;
	}

	/*
	function update_families() {

		$query = "UPDATE product__family
					INNER JOIN lassdive__activitat
						ON product__family.family_id = lassdive__activitat.activitat_id
					SET product__family.parent_id = lassdive__activitat.parent_id;";

		$query = "UPDATE product__family_language
					INNER JOIN lassdive__activitat_language
						ON (
						product__family_language.family_id = lassdive__activitat_language.activitat_id
						AND product__family_language.language = lassdive__activitat_language.language
						)
					SET product__family_language.family             = lassdive__activitat_language.activitat,
						product__family_language.family_description = lassdive__activitat_language.subtitle;";


		$query = "INSERT INTO product__family (family_id, parent_id, observations) (
					SELECT
						activitat_id,
						parent_id,
						''
					FROM lassdive__activitat
					WHERE activitat_id NOT IN (
						SELECT family_id
						FROM product__family));";

		$query = "INSERT INTO product__family_language (family_id, family, family_description, language, family_content_list) (
					SELECT
						activitat_id,
						activitat,
						subtitle,
						language,
						''
					FROM lassdive__activitat_language
					WHERE activitat_id NOT IN (
						SELECT family_id
						FROM product__family_language));";
	}*/
}