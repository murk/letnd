<?php
// falta controlar tema ordre idiomes per saber de quin idioma contem priper les paraules per traduir
// Molt important ------- sempre ha d'haber un registre per cada idioma a la taula property_language
// o sigui que quan afegim un idioma a una web hem de afegir un registre buit amb el idioma afegit en cada registre de la taula taula _language
/**
 */
class Translation
{
    /**
     * Constructor
     *
     * @access protected
     */
    var $results = array();
    var $word_count = array();
    var $word_count_all_pages = array(); // contador de paraules de totes les traduccions
    var $language_fields;
    var $fields;
    var $id_field;
    var $table;
    var $condition;
    var $has_bin;

    function __construct(&$module=false)
    {
		global $gl_caption_translation, $gl_messages_translation, $gl_db_classes_language_fields, $gl_db_classes_fields, $gl_db_classes_id_field, $gl_db_classes_table;
		global $gl_action, $gl_caption, $gl_config, $gl_tool, $gl_tool_section, $gl_messages; // no necessaries a v3

		// linko a variables del modul, així puc modificarles en qualsevol moment i cambiar també aquí
		if ($module) {
			$this->action = &$module->action;
			$this->caption = &$module->caption;
			$this->config = &$module->config;
			$this->tool = &$module->tool;
			$this->tool_section = &$module->tool_section;
			$this->messages = &$module->messages;
		}else
		{	// globals per eliminar a v3
			$this->action = $gl_action;
			$this->caption = &$gl_caption;
			$this->config = $gl_config;
			$this->tool = $gl_tool;
			$this->tool_section = $gl_tool_section;
			$this->messages = &$gl_messages;
		}

        // per sobreescriure misatges i captions que nomès surten a translation
        if (is_array($gl_caption_translation)) $this->caption = array_merge($this->caption, $gl_caption_translation);
        if (is_array($gl_messages_translation)) $this->messages = array_merge($this->messages, $gl_messages_translation);

        $this->get_language_prices();

        $this->language_fields = $gl_db_classes_language_fields;
        $this->fields = $gl_db_classes_fields;
        $this->id_field = $gl_db_classes_id_field;
        $this->table = $gl_db_classes_table;
    }
    function get_language_prices()
    {
        global $gl_languages;
        Db::connect_mother();
        $query = "SELECT pvp_price, pvd_price, code FROM all__language";
        $results = Db::get_rows($query);
        foreach($results as $rs)
        {
            $gl_languages['prices'][$rs['code']] = $rs['pvp_price'];
            $gl_languages['pvd_prices'][$rs['code']] = $rs['pvd_price'];
        }
        Db::reconnect();
    }
    function get_condition(&$listing)
    {
        global $gl_languages, $gl_message, $tpl, $gl_db_classes_fields;
        $this->has_bin = $listing->has_bin;
        $listing->caption = $this->caption; // no farà falta a v3 o si
        $listing->messages = $this->messages; // no farà falta a v3 o si
        $listing->show_langs = true; // per defecte mostrem tots els idiomes en el formulari
        $listing->set_options(1, 0, 1, 1, 0, 0, 0, 0); // set_options ($options_bar = 1, $options_checkboxes = 1, $save_button = 1, $select_button = 1, $delete_button = 1, $print_button = 1, $edit_buttons = 1, $image_buttons = 1)

        foreach($this->language_fields as $field)
        {
            $listing->call_function[$field] = array('Translation', 'call_get_translation_record');
        }
        $listing->call(array('Translation', 'get_blocked') , 'blocked');
        $trs = &$this->results;
        // **********************
        // consulta de tots els registres traduibles
        // **********************
        if ($this->has_bin && $this->condition)
        {
            $this->condition .= 'AND bin=0';
        } elseif ($this->has_bin && !$this->condition)
        {
            $this->condition = 'bin=0';
        }
        if ($this->condition)
        {
            $this->condition = 'WHERE ' . $this->condition;
        }
        $query = "SELECT " . $this->table . "." . $this->id_field . " AS " . $this->id_field . ",
			" . implode(',', $this->language_fields) . ", language, blocked
			FROM " . $this->table . "
			LEFT OUTER JOIN " . $this->table . "_language
			USING ( " . $this->id_field . " )
			" . $this->condition . " ORDER BY " . $this->id_field;
        $results = Db::get_rows($query);
        // afegeixo un ultim resultat per poder fer el bucle un cop mes llarg
        foreach($this->language_fields as $field)
        {
            $results_add[$field] = '';
        }
        $results_add = array_merge($results_add, array($this->id_field => '0', 'language' => 'cat', 'blocked' => '0'));
        $results[] = $results_add;
        // **********************
        // inicialitzar variables
        // **********************
        $condition = '';
        $old_id = $results[0][$this->id_field];
        foreach($this->language_fields as $field)
        {
            $trs[$old_id][$field . '_count'] = 0;
        }
        $trs[$old_id]['language_count'] = 0;
        $languages_count = 0; // contador d'idiomes per cada registre
        foreach ($gl_languages['public'] as $language)
        {
            $this->word_count[$language] = 0;
            $this->word_count_all_pages[$language] = 0;
        }
        // **********************
        // trobem quins id son els que es poden traduir
        // **********************
        foreach($results as $rs)
        {
            $new_id = $rs[$this->id_field];
            if ($old_id != $new_id)
            {
                // els registres que es poden traduir son:
                // els que tenen almenys 1 valor buit i almenys 1 amb texte.
                // Si tots tenen texte no hi ha res a traduir, si tots son buits tampoc hi ha res a traduir
                $found = false;
                foreach($this->language_fields as $field)
                {
                    $is_translatable = $trs[$old_id][$field . '_is_translatable'] = ($languages_count > $trs[$old_id][$field . '_count']) &&
                    ($trs[$old_id][$field . '_count'] > 0);
                    // tant sols un camp es pugui traduir he d'incloure l'id
                    if ($is_translatable && !$found)
                    {
                        $condition .= $old_id . ',';
                        $found = true;
                    }
                }
                // Contem les paraules tinguent en compte l'ordre dels idiomes a la taula all_language
                foreach($this->language_fields as $field)
                {
                    foreach ($gl_languages['public'] as $language)
                    {
                        $word_count = word_count ($trs[$old_id][$field][$language]);
                        if ($word_count)
                        {
                            $trs[$old_id][$field . '_words'] = $word_count;
                            $trs[$old_id][$field . '_original_language'] = $language;
                            break;
                        }
                    }
                }

                $trs[$old_id]['language_count'] = $languages_count;
                // inicialitzo variables pel proxim loop
                foreach($this->language_fields as $field)
                {
                    $trs[$new_id][$field . '_count'] = 0;
                }
                $trs[$new_id]['language_count'] = 0;
                $languages_count = 0;
                $old_id = $new_id;
            }
            // conto quants idiomes no estan vuits,
            // però primer trec tot l'html i totes les paraules que no conten
            foreach($this->language_fields as $field)
            {
                $rs[$field] = html_to_text($rs[$field]);
                $rs[$field] != ''?$trs[$old_id][$field . '_count']++:false;
            }
            // guardo el text a $trs per poder accedir facilment al comptar les paraules quan cambia l'id
            foreach($this->language_fields as $field)
            {
                $trs[$old_id][$field][$rs['language']] = $rs[$field];
            }
            $trs[$old_id]['blocked'] = $rs['blocked'];
            $languages_count ++;
        }
        $condition = substr($condition, 0, -1);
        $condition = $condition?$condition:'0'; // retorno id=0 perque no doni cap resultat a list_records
        $condition = $this->table . "." . $this->id_field . " IN (" . $condition . ")";
        return $condition;
    }
    function show_table()
    {
        global $gl_languages, $gl_news, $tpl, $gl_caption_translation;

        $template = 'translation_table.tpl';
        $tpl->set_file($template);
        $loop = array();
        $loop2 = array();
        $word_count_total = 0;
        $price_total = 0;
        $word_count_all_pages_total = 0;
        $price_all_pages_total = 0;
        $page_results = false;
        // calcul del preu
        foreach ($gl_languages['public'] as $language)
        {
            // pàgina actual
            if ($this->word_count[$language])
            {
                $price = number_format($this->word_count[$language] * $gl_languages['prices'][$language], 2);
                $price_total += $price;
                $word_count_total += $this->word_count[$language];
                $loop[] = array('language' => $gl_languages['names'][$language],
                    'word_count' => $this->word_count[$language],
                    'language_price' => $gl_languages['prices'][$language],
                    'price' => $price
                    );
                $page_results = true;
            }
            // totes les pagines
            foreach ($this->results as $rs)
            {
                foreach($this->language_fields as $field)
                {
                    if (($rs['language_count'] > $rs[$field . '_count']) && ($rs[$field . '_count'] > 0) && ($rs[$field][$language] == '') && !$rs['blocked'])
                    {
                        $this->word_count_all_pages[$language] += $rs[$field . '_words'];
                    }
                }
            }
            if ($this->word_count_all_pages[$language])
            {
                $word_count_all_pages_total += $this->word_count_all_pages[$language];
                $price = $this->word_count_all_pages[$language] * $gl_languages['prices'][$language];
                $price_all_pages_total += $price;
                $price = number_format($price,2);
                $loop2[] = array('language' => $gl_languages['names'][$language],
                    'word_count' => $this->word_count_all_pages[$language],
                    'language_price' => $gl_languages['prices'][$language],
                    'price' => $price
                    );
            }
        }
        if (!$word_count_all_pages_total) return;
        ($word_count_all_pages_total == $word_count_total)?$tpl->set_var('all_pages', false):$tpl->set_var('all_pages', true);
        $tpl->set_loop('loop', $loop);
        $tpl->set_loop('loop2', $loop2);
        $tpl->set_vars($gl_caption_translation);
        $tpl->set_var('page_results', $page_results);
        $tpl->set_var('word_count_total', $word_count_total);
        $tpl->set_var('price_total', number_format($price_total, 2));
        $tpl->set_var('word_count_all_pages', $word_count_all_pages_total);
        $tpl->set_var('price_all_pages', number_format($price_all_pages_total, 2));

        $gl_news .= $tpl->process();;
    }
    function get_blocked($blocked)
    {
        if ($blocked)
        {
            return array('odd_even' => 'Blocked');
        }
    }
    function call_get_translation_record($name, $val)
    {
        global $gl_translation;
        return $gl_translation->get_translation_record($name, $val);
    }
    function get_translation_record($name, $val)
    {
        global $gl_translation, $gl_db_classes_fields;
        // print_r ($gl_translation);
        $trs = &$this->results;
        $name_arr = explode('[', $name);
        $name_prefix = $name_arr[0];
        $id = substr($name_arr[1], 0, -1);
        $lang = substr($name_arr[2], 0, -1);
        // si es html trec tots els tags
        if ($gl_db_classes_fields[$name_prefix]['type'] == 'html' || $gl_db_classes_fields[$name_prefix]['type'] == 'htmlbasic' || $gl_db_classes_fields[$name_prefix]['type'] == 'htmlfull')
        {
            $val = html_to_text($val);
        }

        if (!$trs[$id][$name_prefix . '_is_translatable'])
        {
            return $val;
        }
        if (($val == ''))
        {
            if ($trs[$id]['blocked'])
            {
                $ret = '<img src="' . DIR_TEMPLATES_ADMIN . '/images/lock.gif">';
            }
            else
            {
                $this->word_count[$lang] += $trs[$id][$name_prefix . '_words'];
                $ret = '<input name="' . $name . '" type="checkbox" value="1"><span class="resaltatSmall">' . $trs[$id][$name_prefix . '_words'] . ' paraules</span>';
            }
            return $ret;
        }
        else
        {
            // poso el nom de l'idioma original i el numero de paraules nomès quan passaa l'idioma original
            if ($lang == $trs[$id][$name_prefix . '_original_language'])
            {
                $words = $trs[$id][$name_prefix . '_words'];
                return $val . '<input name="original' . $name_prefix . '[' . $id . ']" type="hidden" value="' . $trs[$id][$name_prefix . '_original_language'] . '"><input name="count' . $name_prefix . '[' . $id . ']" type="hidden" value="' . $words . '"> ';
            }
            else
            {
                return $val;
            }
        }
    }
    function save_rows()
    {
        global $gl_languages, $gl_db_classes_fields, $gl_reload;
        $listing->messages = $this->messages;  // no farà falta a v3 o si
        // print_r($_POST);
        /*foreach($this->language_fields as $field) {
            if (isset($_POST[$field])) {
                $count[$field] = $_POST['count' . $field];
                $original[$field] = $_POST['original' . $field];
            }
        }*/
        $query = '';
        $keys = array();
        $original_texts = array();
        // **************************
        // busco els texts originals per traduir i me'ls poso bé
        // **************************
        foreach($this->language_fields as $field)
        {
            if (isset($_POST[$field]))
            {
                $count[$field] = $_POST['count' . $field];
                $original[$field] = $_POST['original' . $field];
                foreach($_POST[$field] as $key => $value)
                {
                    $keys[] = R::id($key,false,false);
                }
            }
        }
        if (!$count) return;
        $query = "SELECT " . $this->table . ". " . $this->id_field . " AS " . $this->id_field . ",
			" . implode(',', $this->language_fields) . ", language, blocked
			FROM " . $this->table . "
			LEFT OUTER JOIN " . $this->table . "_language
			USING ( " . $this->id_field . " )
			WHERE " . $this->table . ". " . $this->id_field . " IN  (" . implode(',', $keys) . ")
			ORDER BY " . $this->id_field;

        $results = Db::get_rows($query);
        $query = '';
        // print_r($results);
        foreach($results as $rs)
        {
            foreach($this->language_fields as $field)
            {
                if (isset($original[$field][$rs[$this->id_field]]) && $original[$field][$rs[$this->id_field]] == $rs['language'])
                {
                    $original_texts[$field][$rs[$this->id_field]] = $rs[$field];
                }
            }
        }
        // **************************
        // inserto els registres a la base mare
        // **************************
        // print_r($original_texts);
        Db::connect_mother();

        $query_lang = "
			INSERT INTO `translation__translation_language` (
			`translation_id` ,
			`language` ,
			`pvp`,
			`pvd`,
			`translation`)
			VALUES ";
		$numwords = 0;

        foreach($this->language_fields as $field)
        {
            if (isset($_POST[$field]))
            {
                foreach($_POST[$field] as $key => $value)
                {
                    $key = R::id($key,false,false);
					$numwords = $count[$field][$key];
					$query_letnd = "
						INSERT INTO `translation__translation` (
						`client_id` ,
						`table` ,
						`table_id`,
						`field` ,
						`id` ,
						`tipus`,
						`original_language` ,
						`original_text` ,
						`numwords`  ,
						`join_date`)
						VALUES (
						'" . $_SESSION['client_id'] . "',
						'" . $this->table . "_language',
						'" . $this->id_field . "',
						'" . $field . "',
						'" . $key . "',
						'" . $gl_db_classes_fields[$field]['type'] . "',
						'" . $original[$field][$key] . "',
						" . Db::qstr($original_texts[$field][$key]) . ",
						'" . $numwords . "',
						now())";
                    Db::execute($query_letnd);
                    $insert_id = Db::insert_id();

                    // inserto un registre buit per cada idioma a traduir
                    $languages = array_keys($value);
                    $query_letnd = '';
                    foreach ($languages as $language)
                    {
                        $query_letnd .= "(
						'" . $insert_id . "',
						'" . $language . "',
						'" . ($gl_languages['prices'][$language] * $numwords) . "',
						'" . ($gl_languages['pvd_prices'][$language] * $numwords) . "',
						''),";
                    }
                    $query_letnd = substr($query_letnd, 0, -1) . ';';
                    //echo $query_lang . $query_letnd . '<br>';
                    Db::execute($query_lang . $query_letnd);
                }
                $gl_reload = true;
            }
        }
        Db::reconnect();
        // **************************
        // bloquejo els registres a traduir
        // **************************
        $query .= "UPDATE " . $this->table . " SET blocked = 1 WHERE " . $this->id_field . " IN(" . implode(',', $keys) . ")";
        // echo $query . '<br>';
        Db::execute($query);

        $mail = get_mailer();
        $mail->AddAddress("marc@letnd.com");
        $mail->AddAddress("info@iter.cat");
        $mail->Subject = "Nous texts per traduir";
        $mail->Body = "<DIV><FONT face=Arial size=3>Informem que han entrat nous texts per traduir</FONT></DIV>";
        $mail->AltBody = "Informem que han entrat nous texts per traduir.";
        send_mail($mail);
    }
}

?>