<div style="visibility:hidden;height:0px;">
	<iframe id="contacte" src="/?tool=contact&amp;tool_section=contact&amp;action=show_form_new&amp;language=<?= LANGUAGE ?>&amp;template=clean"></iframe>
</div>
<script type="text/javascript">
	ref = '<?=$c_ref?> <?=$ref?>';
</script>
<div class='form_item'>
	<? if ( $property ): ?>
		<div class="property">
			<?= $property ?>
		</div>
	<? endif //property?>
	<div class="left">
		<? if ( $property_title ): ?>
			<div class="property_title">
				<?= $property_title ?>
			</div>
		<? endif //property_title?>
		<? if ( $description ): ?>
			<div class="description">
				<?= $description ?>
			</div>
		<? endif //description?>
		<table border="0" cellspacing="0" cellpadding="2" class="details1">
			<? if ( $floor_space ): ?>
				<tr>
					<td class="details1"><strong>
							<?= $c_floor_space ?>:</strong></td>
					<td class="details1"><?= $floor_space ?>
						m2
					</td>
				</tr>
			<? endif //floor_space?>
			<? if ( $land ): ?>
				<tr>
					<td class="details1"><strong>
							<?= $c_land ?>:</strong></td>
					<td class="details1"><?= $land ?>
						<?= $land_units ?></td>
				</tr>
			<? endif //land?>
			<? if ( $category_id ): ?>
				<tr>
					<td class="details1"><strong>
							<?= $c_category_id ?>:</strong></td>
					<td class="details1"><?= $category_id ?></td>
				</tr>
			<? endif //category_id?>
			<? if ( $comarca_id ): ?>
				<tr>
					<td class="details1"><strong>
							<?= $c_zone ?>:</strong>&nbsp;</td>
					<td class="details1"><?= $comarca_id ?>,
						<?= $provincia_id ?>
						(<?= $country_id ?>)
					</td>
				</tr>
			<? endif //comarca_id?>
			<? if ( $price ): ?>
				<tr>
					<td class="price"><strong>
							<?= $c_price ?>:</strong>&nbsp;</td>
					<td class="price"><?= $price ?>
						<?= CURRENCY_NAME ?></td>
				</tr>
			<? endif //price?>
		</table>
		<? if ( $price_consult ): ?>
			<div class="price"> <span>
        <?= $price_consult ?>
        </span></div>
		<? endif //price_consult?>
	</div>
	<!-- Tenca left -->
	<? if ( $details ): ?>
		<div class="right">
			<table cellpadding="0" cellspacing="0" class="details2">
				<?php foreach ( $details as $d ): ?>
					<?php if ( $$d ): ?>
						<tr>
							<td align="left" nowrap="nowrap" class="left"><?= ${'c_' . $d} ?></td>
							<td align="left" nowrap><?= $$d ?></td>
						</tr>
					<?php endif //$$d?>
				<?php endforeach //details?>
			</table>
		</div>
	<? endif //$details?>
	<!-- Tenca right -->
	<div class="cls"></div>
	<div class="icons">
		<img src="/public/themes/plantilla4/images/mail.gif" alt="email" width="76" height="57" onclick="open_contact('<?= $id ?>','<?= $ref ?>')"/><? if ( $image_name ): ?>
			<img onclick="open_pretty()" src="/public/themes/plantilla4/images/magnify.gif" alt="info" width="51" height="57"/><? endif //images?>
	</div>
</div>
<div id="contacte_<?= $id ?>" class="contacte"></div>
<!-- Tenca form_item -->
<? if ( $status ): ?>
	<div class="form_sold">
		<img src="<?= DIR_TEMPLATES ?>images/sold_<?= LANGUAGE ?>.png" width="162" height="73" border="0"/></div>
<? endif //status?>
<? if ( ! $status ): ?>
	<? if ( $offer ): ?>
		<div class="form_sold">
			<img src="<?= DIR_TEMPLATES ?>images/offer_<?= LANGUAGE ?>.png" width="162" height="73" border="0"/></div>
	<? endif //offer?>
<? endif //status?>
<div class="images_list_form">
	<? $is_first = true ?>
	<? foreach ( $images as $l ): extract( $l ) ?>
		<? if ( $image_name_details ): ?>
			<a href="<?= $image_src_medium ?>" data-gal="prettyPhoto[pp_gal]"><img src="/<?= CLIENT_DIR ?>/inmo/property/images/<?= $image_name_details ?>" width="450" border="0" alt="<?= htmlspecialchars( $property ) ?> - <?= $image_conta ?>"/></a>
		<? endif //$image_name_details?>
	<? endforeach //images?>
</div>

