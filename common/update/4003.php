<?php
// no es poden posar ; entre mig de la consulta ja que parteix les consultes per el ; a no ser que fem servir $update_sql en array()
if (is_table("news__new") && !is_field('news__new','expired')){
	$update_sql[]="ALTER TABLE `news__new`
	ADD COLUMN `expired` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00' AFTER `entered`;";
}		
?>