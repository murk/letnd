<?php
$gl_caption ['c_send_mail']='Envoyer un email';
$gl_messages_cart ['no_records']=$gl_caption_cart ['c_no_records']=$gl_caption_cart ['c_empty_cart']='Le panier est vide';
$gl_messages_order ['no_records'] = 'Vous n\'avez pas fait aucune commande';

$gl_caption['c_productes_destacats']='Featured Products';
$gl_caption['c_products']='Produits marquées';
$gl_caption['c_product']='Produits';
$gl_caption['c_files']='Archives';
$gl_caption['c_discount'] = 'Remise';

// NO TRADUIT

$gl_caption ['c_next_payment_step']='Suivant';
$gl_caption ['c_continue_shopping']='Poursuivre les achats';
$gl_caption ['c_text_register']='Nous allons procéder pour créer votre compte';
$gl_caption ['c_register']='Inscrivez-vous';
$gl_caption_customer ['c_form_title']=$gl_caption ['c_summary_title']='Données personnel';
$gl_caption_customer ['c_title_register']='Inscrivez-vous';
$gl_caption_customer ['c_title_login']='Identification';
$gl_caption_customer ['c_is_customer']='Déjà client';
$gl_caption_customer ['c_new_customer']='Nouveau client';
$gl_caption_customer ['c_mail_log']='Écrivez votre email';
$gl_caption_customer ['c_password_log']='Écrivez votre mot de passe';
$gl_caption_customer['c_customer_menu'] = 'Mon compte';
$gl_caption_customer['c_custumer_menu_personal_data'] = 'Mes données personnelles';
$gl_caption_customer['c_custumer_menu_orders'] = 'Mes commandes';
$gl_caption['c_custumer_menu_logout'] = 'Couper';

$gl_caption['c_title_cart'] = 'Panier d\'achat';
$gl_caption['c_my_cart'] = 'Mon Panier';
$gl_caption['c_cart'] = 'Panier';
$gl_caption['c_add_to_cart'] = 'Ajouter au panier';
$gl_caption['c_see_cart'] = 'Voir le panier';
$gl_caption['c_buy'] = 'Acheter';

$gl_caption_orderitem['c_title_cart'] = 'Achat';
$gl_caption_order['c_list_title'] = 'Commandes';

$gl_caption_payment['c_payment_title'] = 'Faisant l\'achat';
$gl_caption_payment['c_payment_login'] = 'Identification';
$gl_caption_payment['c_payment_summary'] = 'Résumé';
$gl_caption_payment['c_payment_payment'] = 'Paiement';
$gl_caption_payment['c_payment_end'] = 'Achat finalisé';
$gl_caption_payment ['c_cart_is_empty']='Vous ne pouvez pas rendre la commande, le panier est vide';

$gl_caption_payment['c_method_title1']="Choisissez le mode de paiement que vous souhaitez utiliser.";
$gl_caption_payment['c_method_title2']="Le total est de <strong>%s&nbsp;%s</strong> TVA compris.";
$gl_caption_payment['c_method_paypal']="Paiement avec PayPal";
$gl_caption_payment['c_method_account']="Paiement par virement bancaire";
$gl_caption_payment['c_method_ondelivery']="Contrareembols de paiement";
$gl_caption_payment['c_method_directdebit']="Reçu de la Banque";
$gl_caption_payment['c_method_4b']="Carte de crédit (4b)";
$gl_caption_payment['c_method_lacaixa']="Carte de crédit / paiement";

$gl_caption_payment['c_show_paypal']="Paiement d'étape de Paypal, si le processus ne démarre pas automatiquement, cliquez ici:";
$gl_caption_payment['c_show_4b']="Système de paiement est à 4b, si le processus ne démarre pas automatiquement, cliquez ici:";
$gl_caption_payment['c_show_lacaixa']="Carte de paiement d'étape, si le processus ne démarre pas automatiquement, cliquez ici:";
$gl_caption_payment['c_show_account']="Effectuez un dépôt sur ​​le compte: ";
$gl_caption_payment['c_show_account_mail']="S'il vous plaît faire un dépôt au numéro de compte: %s, indiquant le numéro de commande %s";
$gl_caption_payment['c_show_ondelivery']="";
$gl_caption_payment['c_show_directdebit']="";
$gl_caption_payment['c_show_end']="";
$gl_caption_payment['c_mail_order_saved']="<strong>Une nouvelle commande a été créée sur votre boutique en ligne!</strong><br><br>Dans le cas où cette virgule est confirmée, vous recevrez une nouvelle notification.";

$gl_caption_payment['c_mail_order_subject_seller']='Pre-commande nº %s / Cliente nº %s';
$gl_caption_payment['c_mail_subject_seller']='Résumé commande nº %s / Nº de client %s';
$gl_caption_payment['c_mail_subject_buyer']='Résumé commande de %2$s nº %1$s';
$gl_caption_payment['c_text_buyer']='%s %s,';
$gl_caption_payment['c_text_buyer2']= 'Merci d\'avoir acheté à %s, votre numéro de commande est: <strong>%s</strong> et votre numéro de client est: <strong>%s</strong><br><br>N\'oubliez pas que vous pouvez vérifier l\'état de votre commande dans la section: <a href="%s">Mon Compte</a>' ;
$gl_caption_payment['c_text_buyer_no_session']= 'Merci d\'avoir acheté a %s' ;
$gl_caption_payment['c_text_buyer_no_order']= 'Impossible de rendre toute ordonnance, si vous voulez aider s\'il vous plaît contactez-nous' ;
$gl_caption_payment['c_text_seller']='Le client: %s, vient de faire un achat dans %s.';
$gl_caption_payment['c_text_seller_pay_now']='Le client: %s, vient de faire le paiement à régler dans %s.';

$gl_caption_payment ['c_paypal_pending_text_buyer']='L\'ordre est désigné comme impayé jusqu\'à la confirmation de paiement de PayPal';
$gl_caption_payment ['c_paypal_pending_text_seller']='L\'ordre est désigné comme impayé jusqu\'à la confirmation de paiement de PayPal';
$gl_caption_payment ['c_paypal_completed_text_buyer']='Le paiement Paypal a été confirmée';
$gl_caption_payment ['c_paypal_completed_text_seller']='Le paiement Paypal a été confirmée';

$gl_caption_payment ['c_4b_pending_text_buyer']='';
$gl_caption_payment ['c_4b_pending_text_seller']='';
$gl_caption_payment ['c_4b_completed_text_buyer']='Paiement de la carte a été confirmée';
$gl_caption_payment ['c_4b_completed_text_seller']='Paiement de la carte a été confirmée';

$gl_caption_payment ['c_lacaixa_pending_text_buyer']='';
$gl_caption_payment ['c_lacaixa_pending_text_seller']='';
$gl_caption_payment ['c_lacaixa_completed_text_buyer']='Paiement de la carte a été confirmée';
$gl_caption_payment ['c_lacaixa_completed_text_seller']='Paiement de la carte a été confirmée';

$gl_caption_payment ['c_account_text_buyer']='Ci-dessous nous détaillons les données pour effectuer le virement bancaire';
$gl_caption_payment ['c_account_text_seller']='L\'ordre est marqué comme non rémunéré jusqu\'à ce que le paiement soit effectué au numéro de compte';
$gl_caption_payment ['c_ondelivery_text_buyer']='';
$gl_caption_payment ['c_ondelivery_text_seller']='';
$gl_caption_payment ['c_paypal_text_buyer']='';
$gl_caption_payment ['c_paypal_text_seller']='';
$gl_caption_payment ['c_directdebit_text_buyer']='La commande sera débité de votre numéro de compte';
$gl_caption_payment ['c_directdebit_text_seller']='';

$gl_caption['c_productes_destacats']='Produits marquées';
$gl_caption['c_account_title']= 'Données du transference' ;
$gl_caption['c_account_amount']= 'Montant' ;
$gl_caption['c_account_concept']= 'Concept' ;

$gl_messages_cart ['no_records']='Le panier est vide';


// traduir al català
$gl_caption['c_password_forgotten'] = 'Vous avez oublié votre mot de passe?';
$gl_caption['c_password_insert_mail'] = 'S\'il vous plaît, entrez votre adresse email et nous vous enverrons un nouveau mot de passe pour votre compte de messagerie.';
$gl_caption['c_password_mail_error'] = 'L\'adresse e-mail fournie n\'existe pas dans notre base de données, veuillez écrire l\'adresse correctement';
$gl_caption['c_password_sent'] = 'S \ a envoyé un nouveau mot de passe à votre adresse e-mail
';
$gl_caption['c_password_mail_subject'] = 'Son nouveau mot de passe';
$gl_caption['c_password_mail_text_1'] = 'Son nouveau mot de passe est';
$gl_caption['c_password_mail_text_2'] = 'Vous pouvez modifier le mot de passe dans la page «Votre compte» sur notre site Internet';

$gl_messages_customer['registered_succesful']="L'enregistrement a été fait correctement.<br><br>Bientôt vous serez averti par email de l'activation de votre compte";
$gl_caption_customer['c_subject'] = 'Nouvel utilisateur inscrit';
$gl_caption_customer['c_body_1'] = 'Il ya eu un nouveau client de magasiner en ligne';
$gl_caption_customer['c_body_2'] = 'Ce client est désactivé, peut être activée dans la %sgestion de la clientèle%s';

$gl_caption['c_product_list_view_all'] = 'Voir tous';

$gl_caption_cart['c_promcode_err_not_valid'] = 'Code promotionnel n\'est pas valide';
$gl_caption_cart['c_promcode_err_minimum'] = 'Pour appliquer la réduction à l\'achat doit être supérieure à %s';
$gl_caption_cart['c_promcode_err_expired'] = 'Le code promo est expiré';
$gl_caption_cart['c_promcode_err_used'] = 'Ce code a déjà été utilisé une fois';
$gl_caption_cart['c_promcode_button'] = 'Appliquer';

$gl_messages_cart['c_no_stock_message'] = 'Ce produit est en rupture de stock';
$gl_messages_cart['c_max_stock_reached_message'] = 'Atteint le montant maximal disponible pour ce produit';

$gl_caption['c_last_units'] = 'Dernières unités';
$gl_caption_product['c_price_consult'] = 'Prix à demandé';

$gl_caption['c_customer_comment'] = 'Commentaires';
?>