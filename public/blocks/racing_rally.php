<?php

global $gl_is_home;

if (!defined('NEWS_SHOW_ENTERED_TIME')) {
	$cf = get_config_array('public', 'racing');
	$cf_admin =  get_config_array('admin', 'racing');
	$cf = array_merge($cf_admin, $cf); // preferencia public

	$block_max_results = $cf['block_max_results'];
	// $ignore_publish_date =  $cf['ignore_publish_date'];
	$has_files_show_home =  $cf['has_files_show_home'];
} else {
	$block_max_results = NEWS_BLOCK_MAX_RESULTS;
	// $ignore_publish_date =  NEWS_IGNORE_PUBLISH_DATE;
	$has_files_show_home =  NEWS_HAS_FILES_SHOW_HOME;
}

$in_home = isset($vars['in_home']) ? $vars['in_home'] : false;
$destacat = isset($vars['destacat']) ? $vars['destacat'] : false;
$show_images = isset($vars['show_images']) ? $vars['show_images'] : false;
$show_files = isset($vars['show_files']) ? $vars['show_files'] : false;
$block_max_results = isset($vars['block_max_results']) ? $vars['block_max_results'] : $block_max_results;
$auto_select_first = isset($vars['auto_select_first']) ? $vars['auto_select_first'] : true;
$page_id = isset($vars['page_id']) ? $vars['page_id'] : false;
$orderby = isset($vars['orderby']) ? $vars['orderby'] : false;
$add_dots_length = isset($vars['add_dots_length']) ? $vars['add_dots_length'] : false; // add_dots_length
$add_dots_content_list = isset($vars['add_dots_content_list']) ? $vars['add_dots_content_list'] : false; // si es 1 també retallo el contentlist

$query = "SELECT racing__rally.rally_id as rally_id, 
				rally, 
				subtitle,
				content,
				content_list,
				entered,
				expired,
				url1,
				url1_name,
				destacat,
				in_home
				FROM racing__rally_language, racing__rally
				WHERE racing__rally_language.rally_id = racing__rally.rally_id
				AND (status = 'public')
				AND bin <> 1
				AND language = '" . LANGUAGE . "'";

	$query .= " AND (entered > now())
					AND (expired=0 OR expired > (now()))";


if ($in_home)
	$query .= "
				AND in_home = 1";
if ($destacat)
	$query .= "
				AND destacat = 1";
if ($orderby)
	$query .= "		
				ORDER BY " . $orderby;
else
	$query .= "		
				ORDER BY entered ASC, rally, rally_id";

$query .= "		
				LIMIT " . $block_max_results;

$results = Db::get_rows($query);

$selected_found = false;
if ($results) {
	$conta = 0;
	$loop_count = count($results);
	foreach ($results as $rs) {
		$l = array();
		if (isset($_GET["rally_id"]) && ($_GET["rally_id"] == $rs["rally_id"])) {
			$selected = 1;
			$selected_found = true;
		} else {
			$selected = 0;
		}


		// TODO-i Està malament el rally_id, no agafa cap file_name. S'ha de posar la funció get_link
		$l['link'] = '/' . LANGUAGE . '/racing/rally/' . $rs['rally_id'] . '/';

		// EVITAR DUPLICATS - Posar un  page automàticament
		$page_name = '';
		$page_query = "SELECT  page_file_name FROM all__page INNER JOIN all__page_language USING(page_id) WHERE language='" . LANGUAGE . "' AND link = '";



		// 3 - Busco a /racing/rally/
		if (!$page_name) {
			$link_query = '/racing/rally';
			$query = $page_query . $link_query . "'";

			$page_name = Db::get_first($query);
		}


		if ($page_name) $l['link'] .= $page_name . '.html';



		$l['selected'] = $selected;
		$l['rally'] = $rs['rally'];
		$l['subtitle'] = $rs['subtitle'];
		if ($add_dots_content_list) {
			$rs['content_list'] = add_dots('content_list', $rs['content_list'], $add_dots_length);
		}
		$l['content'] = $rs['content_list'] ? $rs['content_list'] : add_dots('content', $rs['content'], $add_dots_length);
		$l['id'] = $rs['rally_id'];
		$l['entered'] = format_date_form($rs['entered']);
		$l['expired'] = format_date_form($rs['expired']);
		$l['url1'] = $rs['url1'];
		$l['url1_name'] = $rs['url1_name'] ? $rs['url1_name'] : $rs['url1'];
		$l['destacat'] = $rs['destacat'];
		$l['in_home'] = $rs['in_home'];
		$l['conta'] = $conta++;
		$l['class'] = '';
		if ($show_images) {
			$l += UploadFiles::get_record_images($rs['rally_id'], true, 'racing', 'rally');
		}
		if ($show_files) {
			$condition = $has_files_show_home?'show_home = 1':'';
			// TODO-i Mirar on més canviar quan hi ha categories de files
			$files = UploadFiles::get_record_files($rs['rally_id'], 'file', 'racing', 'rally', false, false, $condition);
			if ( isset ( $files [ 'files' ] ) ) {
				$l += $files;
			} else {
				$l['files'] = $files;
			}
		}
		$l['loop_count'] = $loop_count;
		$loop2[] = $l;
	}
	$loop2[0]['class'] = 'first';
	$loop2[count($loop2) - 1]['class'] = 'last';
	if (!$selected_found && $auto_select_first)
		$loop2[0]['selected'] = 1;
	$loop[] = array('loop' => $loop2,
		'item' => '');
}
else {
	/* si no hi ha cap resultat no mostro el block */
	$show_block = false;
	return;
}
?>