<?

//$vars['block_selected'] = 0;


$query = "SELECT min(price) as minimum_price, max(price) as maximum_price
				FROM nautica__embarcacion
				WHERE price!=0
					AND(status = 'onsale' OR status = 'reserved' OR status = 'sold') 
					AND nautica__embarcacion.bin = 0";

$vars = Db::get_row($query);
Debug::add('Query nautica price', $query);


$price1 = R::number('price1');
$price2 = R::number('price2');
$vars['selected_price_1'] = $price1;
$vars['selected_price_2'] = $price2;

$vars['default_price_1'] = $price1?$price1:$vars['minimum_price'];
$vars['default_price_2'] = $price2?$price2:$vars['maximum_price'];

// block selected no s'utilitza, per tant no està comprovat
//$vars['block_selected'] = ($price1||$price2)?1:0;
?>