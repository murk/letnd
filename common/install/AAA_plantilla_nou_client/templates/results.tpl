<? /*
 TODO-i Per fer Split sense cap imatge
 // Per controlar exacte el que surt al enllaços de seguent, previ, seguent set i previ set
 */?>
 <? if ($split_results):?>
	<aside class="results">

		<?if($split_previous_set_link):?><a class="pageResults pagePreviousSet" href="<?=$split_previous_set_link?>" title="<?=$split_previous_set_title?>"><<</a>
		<?endif //split_previous_set_link?>

		<?if($split_previous_link):?><a class="pageResults pagePrevious" href="<?=$split_previous_link?>" title="<?=$split_previous_title?>"><</a>
		<?endif //split_previous_link?>

		<?=$split_results?>

		<?if($split_next_link):?>
			<a class="pageResults pageNext" href="<?=$split_next_link?>" title="<?=$split_next_title?>">></a>
		<?endif //split_next_link?>

		<?if($split_next_set_link):?>
			<a class="pageResults pageNextSet" href="<?=$split_next_set_link?>" title="<?=$split_next_set_title?>">>></a>
		<?endif //split_next_link?>

	</aside>
<? endif //split_results?>

<? /*
 TODO-i Per fer Split amb imatges
<? if ($split_results):?>
	<aside class="results">

		<div class="results-count"><?=$split_results_count?></div>
		
		<?=$split_results?>
		
	</aside>
<? endif //split_results?>

  */ ?>