<table border="0" cellspacing="0" cellpadding="0" class="letnd-form">
	<tr>
		<td class="letnd-form-image" valign="top">
			
			<?if($images):?>
				<a title="<?=htmlspecialchars($image_title)?>" href="<?= HTTP ?><?= SHORT_HOST_URL ?><?=$show_record_link?>">
					<img <?=get_image_attrs($image_src_details, $image_alt?$image_alt:$product_title, 280, 280, false)?> />
				</a>							
			<?endif //images?>
		
		</td>	

		<td class="letnd-form-details" width="350" align="left" valign="top">
			<h2>
				<?=$product_title?>
			</h2>
			
			<em class="product-form-ref">
				<?=$c_ref?>. <?=$ref?>
			</em>		
				
			<?if($details):?>
				
				<table border="0" cellspacing="0" cellpadding="0" class="product-form-details">		
					<?foreach($details as $d): extract($d)?>
					<tr>
						<th valign="top" align="left"><?=$c_detail?>:</th>
						<td valign="top"><?=$detail?> <?=$units?></td>
					</tr>
					<?endforeach //details?>
				</table> 
				
			<?endif //details?>	
			
				
			<?if($files):?>			
				<div class="product-form-files">
				
					<?foreach($files as $l): extract ($l)?>
							<a href="<?=$file_src?>"><?=$file_title?></a>
					<?endforeach //$files?>
					
				</div>
			<?endif //details?>
			
			<p class="letnd-info"><a href="<?= HTTP ?><?= SHORT_HOST_URL ?><?=$show_record_link?>"><?=$c_more_info?></a></p>
			<div class="cls"></div>
		</td>
	</tr>
</table>

<?if($product_description):?>
<table border="0" cellspacing="0" cellpadding="0" class="letnd-form-description">
	<tr>
		<td valign="top" class="letnd-form-description-td">							
			<?=$product_description?>			
		</td>
	</tr>
</table>
<?endif //product_description?>		