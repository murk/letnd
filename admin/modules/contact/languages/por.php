<?php
// menus eina
define('CONTACT_MENU_CONTACT','Contacto');
define('CONTACT_MENU_LIST','E-mails de contacto');

// captions seccio
$gl_caption_contact ['c_company'] = 'Empresa';
$gl_caption_contact ['c_name'] = 'Nombre';
$gl_caption_contact ['c_surnames'] = 'Apellidos';
$gl_caption_contact ['c_mail'] = 'E-mail';
$gl_caption_contact ['c_to_address'] = 'Para';
$gl_caption_contact ['c_phone'] = 'Teléfono';
$gl_caption_contact ['c_matter'] = 'Asunto';
$gl_caption_contact ['c_comment'] = 'Observaciones';
$gl_caption_contact ['c_accept_info'] = 'Allow send information';
$gl_caption_contact_list ['c_accept_info'] = 'Allow info';
$gl_caption ['c_sendmail'] = 'Enviar correo';
$gl_messages ['added'] = 'Formulario enviado correctamente.';
$gl_caption ['c_sendok'] = $gl_messages ['added'];
$gl_messages ['not_saved '] = 'No se ha podido enviar el correo.';

$gl_caption_contact['c_nif']='N.I.F / D.NI.';
$gl_caption_contact['c_company']='Empresa';

$gl_caption_contact['c_contact_language']='Idioma página de contacto';
// NO TRADUIT

$gl_caption_contact['c_assign_custumer']='Agregar a clientes';
$gl_caption_contact['c_assign_newsletter']='Agregar a Newsletter';
$gl_caption_contact['c_entered']= 'Enviado';

$gl_caption_contact['c_export_button']= 'Exportar';
?>