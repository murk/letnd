<?if (!$results):?>
<?if ($options_filters):?>
<div id='opcions_holder'><table width="100%" border="0" cellspacing="0" cellpadding="0" id="opcions">
    <tr>
      <td class="opcionsTitol"><strong><?=$c_options_bar?></strong></td>
    </tr>
    <tr>
      <td class="opcions"><table class="filters">
        <tr valign="middle">
		<?foreach($options_filters as $options_filter):?>
          <td><?=$options_filter?></td>
		  <?endforeach //$options_filters?>
          <?if ($save_button):?>
          <?endif //save_button?>
          <?if ($select_button):?>
          <?endif //select_button?>
          <?if ($delete_button):?>
          <?if ($has_bin):?>
          <?if ($is_bin):?>
          <?else:?>
          <?endif // is_bin?>
          <?else:?>
          <!--<td>
              <td>&nbsp;</td><strong><a class="botoOpcions" href="/admin?menu_id=<?=$menu_id?>&sort_by=0&sort_order=<?=$sort_order0?>">
                <?=$c_move_to?>
               
            <input type="submit" name="bin_selected" value="<?=$c_delete_selected?>" class="boto1">
              </a></strong></td>-->
          <?endif // has_bin?>
          <?endif //delete_button?>
          <?if ($print_button):?>
          <?endif //delete_button?>
        </tr>
      </table></td>
    </tr>
</table></div>
<?endif //options_filters?>
<?endif //!$results?>
<?if ($results):?>
<div id='split_holder'><table width="100%" border="0" cellpadding="0" cellspacing="0" id="split">
  <tr valign="middle">
    <td width="20%" align="left" nowrap class="pageResults"><?=$split_results_count?></td>
    <td align="right" nowrap class="pageResults"><?=$split_results?></td>
  </tr>
</table></div>
<?if ($options_bar):?>
  <div id='opcions_holder'><table width="100%" border="0" cellspacing="0" cellpadding="0" id="opcions">
    <tr>
      <td class="opcionsTitol"><strong>
        <?=$c_options_bar?>
        </strong></td>
    </tr>
    <tr>
      <td class="opcions"><div id="opcions_fixed_bar"><div class="more"></div></div><table class="filters">
        <tr valign="middle">
		<?foreach($options_move_tos as $options_move_to):?>
          <td><?=$options_move_to?></td>
		  <?endforeach //$options_move_tos?>
          <?foreach($options_filters as $options_filter):?><form>
          <td valign="top">
            <?=$options_filter?>
          </td>
          </form>
          <?endforeach //$options_filters?>
        </tr>
      </table><?if ($options_filters):?><div class="sep"></div><?endif //options_filters?>
      <table border="0" cellpadding="2" cellspacing="0">
          <tr valign="middle">
            <?if ($save_button):?>
              <td><strong><a class="botoOpcionsGuardar" href="javascript:submit_form_save('save_rows_save_records<?=$action_add?>')">
                <?=$c_send_edit?>
                </a></strong></td>
              <?endif //save_button?>
              <?if ($print_button):?>
              <td><strong><a class="botoOpcions" href="<?=$print_link?>">
                <?=$c_print_all?>
                </a></strong></td>
              <?endif //delete_button?>
          </tr>
        </table></td>
    </tr>
  </table></div>
  <?endif //$options_bar?>
<?if ($order_bar):?>
<div class="sep"></div>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="listRecord">
<tr><td class="listCaption">
<table border="0" cellspacing="0" cellpadding="0" class="noPadding" >
  <tr>
    <td valign="top" nowrap="nowrap"><strong>
      &nbsp;<?=$c_order_by?>&nbsp;&nbsp;
      </strong></td>
      <td valign="top">
    <?foreach($fields as $l): extract ($l)?><strong><a class="opcions" href="<?=$order_link?>">
        <?=$field?>
        </a></strong>
        <?if ($order_image):?>
          <img src="/admin/themes/inmotools/images/<?=$order_image?>.gif" width="9" height="9" border="0" align="middle">
          <?endif //order_image?>
        &nbsp;
      <?endforeach //$field?></td>
  </tr>
</table>
</td>
</tr>
</table>
<?else: //$order_bar?>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="listRecord">
  <tr>
    <td style="padding:0px"></td>
  </tr>
</table>
<?endif //$order_bar?>
<form method="post" action="<?=$form_link?>" name="list" onsubmit="return check_selected(this)" target="save_frame">
<input name="action" type="hidden" value="save_rows"><?=$out_loop_hidden?>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="listRecord" style="margin-top:0px">
   
    <?foreach($loop as $l): extract ($l)?>
      <tr class="listItem<?=$odd_even?>" onmousedown="change_bg_color(this, 'Down', '<?=$id?>')" id="list_row_<?=$id?>">
        <td valign="top" class="listImage" width="<?=$im_admin_thumb_w?>">
            <div><img src="/clients/sistemamuseus/sistemamuseu/<?=$ref?>_fitxa.jpg" width="200"></div></td>
        <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0" class="noPadding">
            <tr>
              <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
                  <tr>
                    <td class="listTitleProperty"><input name="museu_id[<?=$museu_id?>]" type="hidden" value="<?=$museu_id?>" />&nbsp;<?=$ordre?>
                      <span class="private">
                        -
  <?=$museu_title?>
                      </span></td>
                    </tr>
                </table></td>
              </tr>
            <tr>
              <td><table width="99%" border="0" cellpadding="0" cellspacing="0">
                  <tr>
                    <td valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                      <tr>
                        <td height="30" valign="top" class="listTitleProperty2"><?=$tipus?></td>
                        <td height="30" align="right" valign="top" class="listResaltat"><?=$phone?></td>
                      </tr>
                    </table>
                      <table width="100%" border="0" cellpadding="0" cellspacing="0" class="listDetails2">
                        <tr>
                          <td valign="top"><strong class="listItem">
                            <?=$c_adress?>
                            :</strong>&nbsp;</td>
                          </tr>
                        <tr>
                          <td valign="top"><?=$adress?>
    <br />
    <?=$cp?>
    <?=$municipi_id?>
    <br />
    (<?=$comarca_id?>)
    <?=strtoupper($provincia_id)?>
    <br />
<br />
    Tel. <?=$phone?><? if ($phone2):?> / <?=$phone2?><? endif //$phone2?><? if ($phone3):?>/<br /><?=$phone3?><? endif //$phone3?><? if ($fax):?>
    <br />
    Fax. <?=$fax?><? endif //$fax?><? if ($url1):?><a href="http://<?=$url1?>" target="_blank">
    <br /><?=$url1?></a><? endif //$mail?><? if ($mail):?><a href="mailto:<?=$mail?>">
    <br /><?=$mail?></a><? endif //$mail?><? if ($mail2):?><a href="mailto:<?=$mail2?>">
    <br /><?=$mail2?></a><? endif //$mail2?></td>
                        </tr>
                      </table>
                      <table width="100%" border="0" cellpadding="0" cellspacing="0" class="listDetails2">
                      <tr>
                        <td valign="top"><strong class="listItem">
                          <?=$c_status?>:</strong></td>
                        <td valign="top"><?=$status?></td>
                        <td valign="top"><strong>
                          <?=$c_ref?>:
                        </strong></td>
                        <td valign="top"><?=$ref?></td>
                        </tr>
                    </table></td>
                    <td width="300" valign="top" class="listDescription"><div style="height:30px;"></div><div style="padding-bottom:8px;"><span class="formsHor">
                      <?=$museu_description?>
                    </span></div></td>
                  </tr>
                </table>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td align="right"><div class="right"><?foreach($buttons as $button):?><?=$button?><?endforeach //$buttons?></div></td>
                  </tr>
                  <tr>
                    <td><img src="/admin/themes/inmotools/images/spacer.gif" width="1" height="10"></td>
                  </tr>
                </table></td>
              </tr>
          </table></td>
        </tr>
      <?endforeach //$loop?>  
</table></form>
<?if ($order_bar):?>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="listRecord">
  <tr>
    <td class="listCaption"><table border="0" cellspacing="0" cellpadding="0" >
  <tr>
    <td valign="top" nowrap="nowrap"><strong>
      &nbsp;<?=$c_order_by?>&nbsp;&nbsp;
      </strong></td>
      <td valign="top">
    <?foreach($fields as $l): extract ($l)?><strong><a class="opcions" href="<?=$order_link?>">
        <?=$field?>
        </a></strong>
        <?if ($order_image):?>
          <img src="/admin/themes/inmotools/images/<?=$order_image?>.gif" width="9" height="9" border="0" align="middle">
          <?endif //order_image?>
        &nbsp;
      <?endforeach //$field?></td>
  </tr>
</table></td>
  </tr>
</table>
<?endif //$order_bar?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td height="10"></td>
  </tr>
</table>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr valign="middle">
    <td width="20%" align="left" nowrap class="pageResults"><?=$split_results_count?></td>
    <td align="right" nowrap class="pageResults"><?=$split_results?></td>
  </tr>
</table>
<?endif //results?>