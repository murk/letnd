<?

/* 30-1-14
 * 
 * Actualitzacions publicacions facebook i twitter desde noticies, inmobles, productes:
 *  
 */

	
if (is_table('user__user')) {
	
	$update_sql[]="
	ALTER TABLE `user__user`
	ADD COLUMN `facebook_access_token` TEXT NOT NULL AFTER `group_id`,
	ADD COLUMN `facebook_user_id` TEXT NOT NULL AFTER `facebook_access_token`,
	ADD COLUMN `twitter_user_id` TEXT NOT NULL AFTER `facebook_user_id`,
	ADD COLUMN `twitter_token` TEXT NOT NULL AFTER `twitter_user_id`,
	ADD COLUMN `twitter_token_secret` TEXT NOT NULL AFTER `twitter_token`;
	";
	
	$update_sql[]="
	ALTER TABLE `user__user`
	ADD COLUMN `facebook_user_details` VARCHAR(400) NOT NULL AFTER `facebook_user_id`,
	ADD COLUMN `twitter_user_details` VARCHAR(400) NOT NULL AFTER `twitter_token_secret`;
	";
	
}
	
if (is_table('all__configadmin')) {
	
	
	if (!Db::get_first("SELECT count(*) FROM all__configadmin WHERE name='facebook_default_page_id'")){
	$update_sql[]="
		INSERT INTO all__configadmin (`name`, `value`) 
		VALUES ('facebook_default_page_id', '');";
	}
	
	
}

if (is_table('news__new')) {
	$update_sql[]="
		ALTER TABLE `news__new`
		ADD COLUMN `facebook_post_id` VARCHAR(255) NOT NULL DEFAULT '' AFTER `bin`,
		ADD COLUMN `facebook_image_id` VARCHAR(255) NOT NULL DEFAULT '' AFTER `facebook_post_id`,
		ADD COLUMN `facebook_publish_id` VARCHAR(255) NOT NULL DEFAULT '' AFTER `facebook_image_id`,
		ADD COLUMN `facebook_letnd_user_id` VARCHAR(255) NOT NULL DEFAULT '' AFTER `facebook_publish_id`,
		ADD COLUMN `facebook_user_id` TEXT NOT NULL AFTER  `facebook_letnd_user_id`,
		ADD COLUMN `twitter_status_id` VARCHAR(255) NOT NULL DEFAULT '' AFTER `facebook_user_id`,
		ADD COLUMN `twitter_letnd_user_id` VARCHAR(255) NOT NULL DEFAULT '' AFTER `twitter_status_id`,
		ADD COLUMN `twitter_user_id` TEXT NOT NULL AFTER  `twitter_letnd_user_id`;";
}

if (is_table('product__product')) {
	$update_sql[]="
		ALTER TABLE `product__product`
		ADD COLUMN `facebook_post_id` VARCHAR(255) NOT NULL DEFAULT '' AFTER `bin`,
		ADD COLUMN `facebook_image_id` VARCHAR(255) NOT NULL DEFAULT '' AFTER `facebook_post_id`,
		ADD COLUMN `facebook_publish_id` VARCHAR(255) NOT NULL DEFAULT '' AFTER `facebook_image_id`,
		ADD COLUMN `facebook_letnd_user_id` VARCHAR(255) NOT NULL DEFAULT '' AFTER `facebook_publish_id`,
		ADD COLUMN `facebook_user_id` TEXT NOT NULL AFTER  `facebook_letnd_user_id`,
		ADD COLUMN `twitter_status_id` VARCHAR(255) NOT NULL DEFAULT '' AFTER `facebook_user_id`,
		ADD COLUMN `twitter_letnd_user_id` VARCHAR(255) NOT NULL DEFAULT '' AFTER `twitter_status_id`,
		ADD COLUMN `twitter_user_id` TEXT NOT NULL AFTER  `twitter_letnd_user_id`;";
}

if (is_table('inmo__property')) {
	$update_sql[]="
		ALTER TABLE `inmo__property`
		ADD COLUMN `facebook_post_id` VARCHAR(255) NOT NULL DEFAULT '' AFTER `bin`,
		ADD COLUMN `facebook_image_id` VARCHAR(255) NOT NULL DEFAULT '' AFTER `facebook_post_id`,
		ADD COLUMN `facebook_publish_id` VARCHAR(255) NOT NULL DEFAULT '' AFTER `facebook_image_id`,
		ADD COLUMN `facebook_letnd_user_id` VARCHAR(255) NOT NULL DEFAULT '' AFTER `facebook_publish_id`,
		ADD COLUMN `facebook_user_id` TEXT NOT NULL AFTER  `facebook_letnd_user_id`,
		ADD COLUMN `twitter_status_id` VARCHAR(255) NOT NULL DEFAULT '' AFTER `facebook_user_id`,
		ADD COLUMN `twitter_letnd_user_id` VARCHAR(255) NOT NULL DEFAULT '' AFTER `twitter_status_id`,
		ADD COLUMN `twitter_user_id` TEXT NOT NULL AFTER  `twitter_letnd_user_id`;";
}

if (is_table('portfoli__portfoli')) {
	$update_sql[]="
		ALTER TABLE `portfoli__portfoli`
		ADD COLUMN `facebook_post_id` VARCHAR(255) NOT NULL DEFAULT '' AFTER `bin`,
		ADD COLUMN `facebook_image_id` VARCHAR(255) NOT NULL DEFAULT '' AFTER `facebook_post_id`,
		ADD COLUMN `facebook_publish_id` VARCHAR(255) NOT NULL DEFAULT '' AFTER `facebook_image_id`,
		ADD COLUMN `facebook_letnd_user_id` VARCHAR(255) NOT NULL DEFAULT '' AFTER `facebook_publish_id`,
		ADD COLUMN `facebook_user_id` TEXT NOT NULL AFTER  `facebook_letnd_user_id`,
		ADD COLUMN `twitter_status_id` VARCHAR(255) NOT NULL DEFAULT '' AFTER `facebook_user_id`,
		ADD COLUMN `twitter_letnd_user_id` VARCHAR(255) NOT NULL DEFAULT '' AFTER `twitter_status_id`,
		ADD COLUMN `twitter_user_id` TEXT NOT NULL AFTER  `twitter_letnd_user_id`;";
}	


?>