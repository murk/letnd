<?

/**
 * InvoiceInvoice
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2017
 * @version $Id$
 * @access public
 *
 *
 *
 * TODO-i Posar empresa i comercial_name al llistat de factures que es pugui ordenar
 * TODO-i Retencions autònoms
 * TODO-i  Borrar mandate si es borra client, s'ha de fer a custumer__custumer, però s'ha de comprovar que existeix la taula invoice__mandate
 *  TODO-i Que no es puguin borrar les factures ni editar un cop enviada remesa
 *
 *
 *
 */
class InvoiceInvoice extends Module {
	var $condition = '';
	var $q = false;
	var $is_excel = false;
	var $date1;
	var $date2;

	function __construct() {
		parent::__construct();
	}

	function list_records() {
		$content = $this->get_records();
		if ( $this->q ) {
			$GLOBALS['gl_page']->javascript .= "
				$('table.listRecord').highlight('" . addslashes( $this->q ) . "');			
			";
		}
		$GLOBALS['gl_content'] = $this->get_search_form() . $content;
	}

	function get_records() {

		$this->get_search_condition();

		$listing  = new ListRecords( $this );
		$batch_id = R::id( 'batch_id' );

		// search
		if ( $this->condition ) {
			$listing->condition .= $this->condition;
			$listing->order_by  = 'invoice_number DESC, invoice_id ASC';
		} elseif($listing->is_bin){
			// Aquí res, el que vull es que no agafi els altres quan es bin
		}
		elseif ( $this->process == 'no_batch' ) {
			$listing->condition .= "batch_id = 0";
			$listing->order_by  = 'invoice_id ASC';
		} elseif ( $batch_id ) {
			$listing->condition = "batch_id = $batch_id";
			$listing->order_by  = 'invoice_number DESC';
		} else {
			$listing->condition .= "batch_id <> 0";
			$listing->order_by  = 'invoice_number DESC';
		}

		// El registre fake porta bin=1 sempre, així només m'haig de preocupar de posar bin=0 quan faig una consulta, i mirar fake només a la paperera que tampoc surti
		if ( $listing->is_bin ) {
			$and = $listing->condition ? ' AND' : '';
			$listing->condition .= "$and fake <> 1";
		}


		$listing->add_options_button( 'export_button', 'action=export', 'botoOpcions1 excel', 'top_after' );


		// FALTA POSAR NOM ARXIU SI NO HA A REMESA ( en principi no cal fer-ho mai sense remesa )
		$listing->add_button( 'get_sepa_button', 'action=get_sepa', 'boto1', 'after', '', 'target="save_frame"' );

		$listing->set_field( 'custumer_billing_mail', 'type', 'none' );

		$listing->add_button( 'get_pdf_button', 'action=get_pdf', 'boto1', 'before', '', 'target="save_frame"' );
		$listing->add_button( 'send_mail_button', '', 'boto2', 'after', 'top.Invoice.confirm_send_mail', 'target="save_frame"' );
		$listing->add_button( 'get_facturae_button', 'action=get_facturae', 'boto1', 'after', '', 'target="save_frame"' );

		$listing->call( 'records_walk', '', true );

		if ( $this->is_excel ) {
			return $listing;
		}

		$listing->set_records();
		$listing->call_after_set_records( 'after_records_walk' );

		return $listing->process();
	}

	function records_walk( &$listing ) {

		$rs = &$listing->rs;

		$ret['custumer_billing_mail'] = '<a href="mailto:' . $rs['custumer_billing_mail'] . '">' . $rs['custumer_billing_mail'] . '</a>';

		// TODO-i Agafar el company de custumer_compnany etc.. quan batch_id != 0
		if ( ! $rs['batch_id'] ) {
			$listing->buttons['get_sepa_button']['show']  = false;
			$listing->buttons['get_pdf_button']['show']   = false;
			$listing->buttons['send_mail_button']['show'] = false;
		} else {
			$listing->buttons['get_sepa_button']['show']  = true;
			$listing->buttons['get_pdf_button']['show']   = true;
			$listing->buttons['send_mail_button']['show'] = true;
		}


		return $ret;

	}

	function after_records_walk( $rs ) {

		$ret['invoice_number'] = $rs['invoice_number'] == '0' ? '' : $rs['invoice_number'];

		if ( $this->is_excel ) {
			$ret = $this->get_export_fields( $rs );

			return $ret;
		}

		return $ret;

	}

	function show_form() {
		$GLOBALS['gl_content'] = $this->get_form();
	}

	// TODO-i URGENT agafar automàticament forma de pagament del client
	// TODO-i  dte que pugui veure's diferent a la factura
	function get_form() {
		$show = new ShowForm( $this );


		$min_date = Db::get_first( "SELECT MAX(bill_date) FROM invoice__invoice WHERE batch_id != 0 AND bin = 0;" );

		if ( is_date_bigger( $min_date, date( "Y-m-d" ) ) && ( $this->action == 'show_form_new' ) ) {
			$min_date_formatted = format_date_form( $min_date );
			$show->set_field( 'bill_date', 'default_value', $min_date_formatted );
			$show->set_field( 'bill_due', 'default_value', $min_date_formatted );
		}

		if ( isset( $_SESSION ['invoice'] ) && isset( $_SESSION ['invoice']['bill_date'] ) ) {

			$bill_date = $_SESSION ['invoice']['bill_date'];
			$bill_due  = $_SESSION ['invoice']['bill_due'];

			if ($bill_date && !is_date_bigger( $min_date, unformat_date($bill_date)  )) {
				$show->set_field( 'bill_date', 'default_value', $bill_date );
				$show->set_field( 'bill_due', 'default_value', $bill_due );
			}

		}


		$min_date_arr = explode( '-', $min_date );
		$id           = $show->id;

		$js_min_date = $min_date_arr[0] . ',' . ( $min_date_arr[1] - 1 ) . ',' . $min_date_arr[2];


		// TODO-i Tenir en compte series
		$javascript = '';
		if ( $this->action == 'show_form_new' ) {
			$javascript = print_javascript( "
					$(function () {
						$('#bill_date_$id,#bill_due_$id').datepicker(
							'option', 'minDate', new Date($js_min_date)
						);
					});", true );
		}
		else {
			// TODO-i Buscar dates entre ultima factura i següent si ja s'ha numerat, no es pot posar cap mes
		}

		return $show->show_form() . $javascript;
	}

	function save_rows() {
		$save_rows = new SaveRows( $this );
		$save_rows->save();
	}

	function write_record() {
		$writerec = new SaveRows( $this );

		$bill_date = $writerec->get_value( 'bill_date' );
		$bill_due = $writerec->get_value( 'bill_due' );

		$writerec->save();

		$_SESSION ['invoice']['bill_date'] = $bill_date;
		$_SESSION ['invoice']['bill_due'] = $bill_due;
	}

	public function get_sublist_html( $id, $field, $is_input ) {

		return InvoiceCommon::get_concept_sublist_html( $id, $field, $is_input, $this );

	}

	// ajax
	public function get_sublist_item() {

		InvoiceCommon::get_concept_sublist_item( $this );

	}

	// Resultats per el autocomplete
	public function get_checkboxes_long_values() {

		InvoiceCommon::get_concept_checkboxes_long_values();

	}

	// Clients
	public function get_select_long_values() {

		InvoiceCommon::get_custumer_select_long_values();

	}

	public function get_pdf() {

		Main::load_class( 'invoice', 'pdf', 'admin' );

		$pdf = New InvoicePdf( $this );
		$pdf->get_body( R::id( 'invoice_id' ) );
		$pdf->output();

		Debug::p_all();
		die();
	}

	public function get_facturae() {

		Main::load_class( 'invoice', 'facturae', 'admin' );

		$fac = New InvoiceFacturae( $this );
		$fac->set_data( R::id( 'invoice_id' ) );
		$fac->output();

	}

	public function send_mail() {

		Main::load_class( 'invoice', 'mail', 'admin' );

		$mail = New InvoiceMail( $this );
		$mail->send( R::id( 'invoice_id' ) );

		Debug::p_all();
		die();
	}

	public function get_sepa() {

		Main::load_class( 'invoice', 'sepa', 'admin' );

		$invoice_id = R::id( 'invoice_id' );

		$sepa = New InvoiceSepa( $this );
		$sepa->add_payment( $invoice_id );
		$sepa->output( "remesa_factura_$invoice_id" );

		Debug::p_all();
		die();
	}


	//
	// FUNCIONS BUSCADOR
	//
	function search() {

		$this->do_action( 'list_records' );
	}
	function get_search_condition(){

		$q = $this->q = R::escape( 'q' );

		$search_condition = '';
		$and              = '';

		if ( $q ) {
			// Sempre busco a tot arreu, abans si detectava que eren refrencies no hi  buscava, posaré checkbox de "buscar nomès referencies"

			$search_condition = "(
				invoice_number like '%$q%'
				OR custumer_name like '%$q%'
				OR custumer_surname1 like '%$q%'
				OR custumer_surname2 like '%$q%'
				OR custumer_company like '%$q%'
				OR custumer_billing_mail like '%$q%'
				OR invoice_id IN (
					SELECT invoice_id FROM invoice__invoiceconcept WHERE concept like '%$q%'
				)				
				";

			$search_condition          .= ")";
			$GLOBALS['gl_page']->title = TITLE_SEARCH . '<strong>&nbsp;&nbsp;"' . $q . '"</strong>';
			$and                       = ' AND ';


		}


		// Cerca per dates
		$date1 = unformat_date( R::escape( 'date1' ) );
		$date2 = unformat_date( R::escape( 'date2' ) );

		// si no poso una data de les dates, busco nomes un dia
		if ( ! $date1 ) {
			$date1 = $date2;
		}
		if ( ! $date2 ) {
			$date2 = $date1;
		}

		if ( $date1 && $date2 ) {
			$search_condition .= "$and( bill_date BETWEEN  '$date1' AND '$date2')";
			$and                       = ' AND ';
		}

		// Cerca trimestres
		$quarter = R::escape( 'quarter' );
		$year = R::escape( 'year' );

		if ( $quarter ) {
			$search_condition .= "$and( QUARTER (bill_date) = $quarter)";
			$and                       = ' AND ';
		}

		if ( $year ) {
			$search_condition .= "$and( YEAR (bill_date) = $year)";
			$and                       = ' AND ';
		}


		$invoice_serial = R::escape( 'invoice_serial' );
		if ( $invoice_serial ) {
			$search_condition .= "$and( invoice_serial = '$invoice_serial')";
			$and                       = ' AND ';
		}

		$this->condition = $search_condition;
	}

	function get_search_form() {

		$quarter = (int) R::escape( 'quarter' );
		$quarters = [
			'',
			'1 trimestre',
			'2 trimestre',
			'3 trimestre',
			'4 trimestre',
		];
		$select_quarter = '';
		foreach ( $quarters as $key => $val ) {
			$selected = $quarter == $key ? ' selected' : '';
			$select_quarter .= "<option $selected value='$key'>$val</option>";
		}

		$select_quarter = "<select name='quarter'>$select_quarter</select>";

		$year = (int) R::escape( 'year' );
		$years = [
			'',
			date ('Y'),
			'2015',
			'2016',
			'2017',
			'2018',
			'2019',
			'2020',
			'2021',
			'2022',
			'2023',
			'2024',
			'2025',
			'2026',
			'2027',
			'2028',
		];
		$select_year = '';
		foreach ( $years as $val ) {
			$selected = $year == $val ? ' selected' : '';
			$select_year .= "<option $selected value='$val'>$val</option>";
		}

		$select_year = "<select name='year'>$select_year</select>";


		$date1 = unformat_date( R::escape( 'date1' ) );
		$date2 = unformat_date( R::escape( 'date2' ) );


		$invoice_serial = R::escape( 'invoice_serial' );
		$invoice_serials = [
			'',
			'A',
			'B',
		];
		$select_invoice_serial = '';
		foreach ( $invoice_serials as $key => $val ) {
			$selected = $invoice_serial == $val ? ' selected' : '';
			$select_invoice_serial .= "<option $selected value='$val'>$val</option>";
		}

		$select_invoice_serial = "<select name='invoice_serial'>$select_invoice_serial</select>";


		$this->caption['c_by_ref'] = '';
		$this->set_vars( $this->caption );
		$this->set_file( '/invoice/invoice_search.tpl' );
		$this->set_var( 'menu_id', $GLOBALS['gl_menu_id'] );
		$this->set_var( 'q', htmlspecialchars( R::get( 'q' ) ) );
		$this->set_var( 'date1', format_date_form( $date1 ) );
		$this->set_var( 'date2', format_date_form( $date2 ) );
		$this->set_var( 'select_quarter', $select_quarter );
		$this->set_var( 'select_year', $select_year );
		$this->set_var( 'select_invoice_serial', $select_invoice_serial );
		$content = $this->process();

		return $content;
	}

	function export() {
		$this->is_excel = true;

		Main::load_class( 'excel' );

		$this->set_field( 'total_vat', 'list_admin', 'text' );

		// TODO-i Havia d'haver posat aquests camps igual que a la bbdd
		$custumer_fields       = [
			'company',
			'comta',
			'name'
		];
		$other_custumer_fields = [
			'vat',
			'vat_type',
			'cif',
			'cif_type',
			'adress',
			'numstreet',
			'block',
			'flat',
			'door',
			'town',
			'province',
			'zip',
			'billing',
			'name',
			'surname1',
			'surname2'
		];


		// TODO-i S'hauria de fer a tots un cop posat en batch, ja que ja s'han grabat les dades a invoice__invoice
		foreach ( $custumer_fields as $custumer_field ) {
			$this->set_field( $custumer_field, 'change_field_name', null );
			$this->fields[ 'custumer_' . $custumer_field ]    = $this->fields[ $custumer_field ];
			$this->caption[ 'c_custumer_' . $custumer_field ] = $this->caption[ 'c_' . $custumer_field ];
			$this->unset_field( $custumer_field );
		}
		foreach ( $other_custumer_fields as $custumer_field ) {
			$this->add_field( 'custumer_' . $custumer_field, [ 'type' => 'none', 'list_admin' => 'text' ] );
		}

		$this->set_field( 'custumer_cif_type', 'change_field_name', '\'\' AS custumer_cif_type' ); // així es queda amb ordre, sino va a l'última columna de l'excel

		$this->set_field_position( 'bill_date', 15 );


		// TODO-i  Revisar totes les exportacions, hauria d'arribar els números sense format, i assignar directament segons el tipu,  per exemple
		//       'currency' => 'FORMAT_NUMBER_COMMA_SEPARATED1'
		//       'int' => 'FORMAT_NUMBER'

		$column_formats = array(
			'total_price_no_vat' => 'FORMAT_NUMBER_COMMA_SEPARATED1',
			'total_vat'          => 'FORMAT_NUMBER_COMMA_SEPARATED1',
			'total_price'        => 'FORMAT_NUMBER_COMMA_SEPARATED1',
			'invoice_number'     => 'FORMAT_NUMBER',
			'custumer_zip'       => 'FORMAT_GENERAL',
		);

		// Miro els camps que estan com a text o input o out
		$visible_fields = [
			'invoice_number',
			'invoice_serial',
			'bill_date',
			'total_price_no_vat',
			'total_vat',
			'total_price',
			'custumer_company',
			'custumer_cif',
			'custumer_cif_type',
			'custumer_adress',
			'custumer_town',
			'custumer_province',
			'custumer_zip',
		];

		$listing = $this->get_records();

		$listing->limit = 10000;
		$listing->list_records( false );

		$listing->call_after_set_records( 'after_records_walk', 'results' );

		$excel = new Excel( $listing );
		$excel->set_visible_fields( $visible_fields )
		      ->column_formats( $column_formats )
		      ->file_name( 'factures' )
		      ->make( $listing->results );

	}

	function get_export_fields( &$rs ) {

		$invoice_number = $rs['invoice_serial'] . $rs['invoice_number'];

		// Igual que a invoice_pdf set_customer
		/*if ( $rs['invoice_number'] ) {

		}
		else {
			$custumer_id = $this->rs['custumer_id'];
			$rs = Db::get_row( "SELECT * FROM custumer__custumer WHERE custumer_id = $custumer_id" );
		}*/


		if ( $rs['custumer_billing'] == 'citizen' ) {
			$company  = $rs['custumer_name'] . ' ' . $rs['custumer_surname1'] . ' ' . $rs['custumer_surname2'];
			$cif_type = 'NIF';
			$cif      = $rs['custumer_vat'];
		} else {
			$company  = $rs['custumer_company'];
			$cif_type = 'CIF';
			$cif      = $rs['custumer_cif'];
		}

		$adress = '';
		if ( $rs['custumer_adress'] ) {
			$adress .= $rs['custumer_adress'];
			if ( $rs['custumer_numstreet'] ) {
				$adress .= ', ' . $rs['custumer_numstreet'];
			}

			if ( $rs['custumer_block'] ) {
				$adress .= ' ' . $rs['custumer_block'];
			}
			if ( $rs['custumer_flat'] ) {
				$adress .= ' ' . $rs['custumer_flat'];
				if ( $rs['custumer_door'] ) {
					$adress .= '-' . $rs['custumer_door'];
				}
			}
		}

		$town     = $rs['custumer_town'];
		$province = mb_strtoupper( $rs['custumer_province'], 'UTF-8' );
		$zip      = $rs['custumer_zip'];

		return [
			'custumer_invoice_number' => $invoice_number,
			'custumer_company'        => $company,
			'custumer_cif_type'       => $cif_type,
			'custumer_cif'            => $cif,
			'custumer_adress'         => $adress,
			'custumer_town'           => $town,
			'custumer_province'       => $province,
			'custumer_zip'            => $zip,
		];
	}
}

?>