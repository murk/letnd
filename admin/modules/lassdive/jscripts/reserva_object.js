var ReservaObject = {

	add_search_autocomplete: function (field, action, appendTo = '') {

		let html = '';

		let base_jq = '#' + field;
		if ($(base_jq + '_add').length === 0) return;

		let service_url = '/' + gl_language + "/lassdive/reserva/?action=" + action + '&field=' + field;


		let last_valid_data = {};
		last_valid_data [field] = {'data': $(base_jq).val(), 'value': $(base_jq + '_add').val()};


		// recullo el primer resultat
		$.ajax({
			url: service_url,
			data: {
				q: ''
			},
			dataType: 'json',
			type: 'GET'
		})
			.done(function (data) {

				data.is_complete = data.is_complete || false;
				data.has_custom = data.has_custom || false;

				let options = {
					serviceUrl: service_url,
					minChars: 1,
					noCache: false,
					width: 'flex',
					showNoSuggestionNotice: true,
					noSuggestionNotice: NO_RESULTS,
					autoSelectFirst: true,
					onSelect: function (suggestion) {
						let id = suggestion['data'];
						let last_family_id = suggestion['last_family_id'];

						client.get_activitat( id, last_family_id );
					}
				};

				if (appendTo) options ['appendTo'] = appendTo;


				$(base_jq + '_add').show();

				$(base_jq + '_add').autocomplete(
					options
				) /*
					.focus(function(){
						$('#product_results_holder').show();
					})*/
					.blur(function () {
						// Permetre valor buit
						let val = $(base_jq + '_add').val();

						if (val) {
							$(base_jq + '_add').val(last_valid_data [field]['value']);
							$(base_jq).val(last_valid_data [field]['data']).trigger('change');
						}
						else {
							$(base_jq).val('').trigger('change');
						}
					});

				if (data.is_complete) {
					$(base_jq + '_add').autocomplete().setOptions({
						lookup: data.suggestions
					});
				}
				// Si no es complert, ho poso en cache per tindre els primers resultats automàticament
				else {
					$(base_jq + '_add').autocomplete().cachedResponse[service_url + '?query='] = data;
				}

				if (data.has_custom) {
					$(base_jq + '_add').autocomplete().setOptions({
						formatResult: function (suggestion, currentValue) {
							return suggestion.custom;
						}
					});
				}

			});


	},
	add_select_long: function (field, action, appendTo = '') {

		let html = '';

		let base_jq = '#' + field;
		if ($(base_jq + '_add').length === 0) return;

		let service_url = '/' + gl_language + "/lassdive/reserva/?action=" + action + '&field=' + field;


		let last_valid_data = {};
		last_valid_data [field] = {'data': $(base_jq).val(), 'value': $(base_jq + '_add').val()};

		// recullo el primer resultat
		$.ajax({
			url: service_url,
			data: {
				q: ''
			},
			dataType: 'json',
			type: 'GET'
		})
			.done(function (data) {

				data.is_complete = data.is_complete || false;
				data.has_custom = data.has_custom || false;

				let options = {
					serviceUrl: service_url,
					minChars: 0,
					noCache: false,
					width: 'flex',
					showNoSuggestionNotice: true,
					noSuggestionNotice: NO_RESULTS,
					autoSelectFirst: true,
					onSelect: function (suggestion) {
						let id = suggestion['data'];
						let value = suggestion['value'];

						if (id) {
							$(base_jq).val(id).trigger('change');
							last_valid_data [field]['data'] = id;
							last_valid_data [field]['value'] = value;
						}

					}
				};

				if (appendTo) options ['appendTo'] = appendTo;

				$(base_jq + '_add').show();

				$(base_jq + '_add').autocomplete(
					options
				).blur(function () {
					// Permetre valor buit
					let val = $(base_jq + '_add').val();

					if (val) {
						$(base_jq + '_add').val(last_valid_data [field]['value']);
						$(base_jq).val(last_valid_data [field]['data']).trigger('change');
					}
					else {
						$(base_jq).val('').trigger('change');
					}
				});

				if (data.is_complete) {
					$(base_jq + '_add').autocomplete().setOptions({
						lookup: data.suggestions
					});
				}
				// Si no es complert, ho poso en cache per tindre els primers resultats automàticament
				else {
					$(base_jq + '_add').autocomplete().cachedResponse[service_url + '?query='] = data;
				}

				if (data.has_custom) {
					$(base_jq + '_add').autocomplete().setOptions({
						formatResult: function (suggestion, currentValue) {
							return suggestion.custom;
						}
					});
				}

			});


	},
	checkboxes_long_vars: {},
	add_checkboxes_long: function (field, action) {


		let base_jq = '#' + field;
		let service_url = '/' + gl_language + "/lassdive/reserva/?action=" + action + '&field=' + field;

		this.checkboxes_long_vars[field] = {
			id: base_jq + '_add',
			new_record_id: 0,
			selected_ids: [],
			names: [],
		};

		// recullo el primer resultat
		$.ajax({
			url: service_url,
			data: {
				selected_ids: '',
				q: ''
			},
			dataType: 'json',
			type: 'GET'
		})
			.done(function (data) {

				data.is_complete = data.is_complete || false;
				data.has_custom = data.has_custom || false;

				$(base_jq + '_add').show();


				$(base_jq + '_add').autocomplete({
					serviceUrl: service_url,
					minChars: 0,
					noCache: false,
					orientation: 'top', // Configurable
					showNoSuggestionNotice: true,
					noSuggestionNotice: NO_RESULTS,
					autoSelectFirst: true,
					zIndex:11000,
					onSelect: function (suggestion) {
						ReservaObject.add_checkboxes_long_on_select(base_jq, suggestion, field);
					}
				});


				if (data.is_complete) {
					$(base_jq + '_add').autocomplete().setOptions({
						lookup: data.suggestions
					});
				}
				// Si no es complert, ho poso en cache per tindre els primers resultats automàticament
				else {
					if ($(base_jq + '_add').length > 1) $(base_jq + '_add').autocomplete().cachedResponse[service_url + '?query='] = data;
				}

				if (data.has_custom) {
					$(base_jq + '_add').autocomplete().setOptions({
						formatResult: function (suggestion, currentValue) {
							return suggestion.custom;
						}
					});
				}

			});
	},

	add_checkboxes_long_on_select: function (base_jq, suggestion, field) {

		// TODO-i Quan es desmarquen els instructors peta
		$(base_jq + '_add').val('');

		let
			selected_id = suggestion['data'],
			selected_text = suggestion['value'],
			vars = ReservaObject.checkboxes_long_vars[field];
		vars.selected_ids.push(selected_id);
		vars.names[selected_id] = selected_text;

		$(base_jq + '_add').autocomplete().setOptions({
			params: {'selected_ids': ReservaObject.checkboxes_long_vars[field].selected_ids}
		});

		let html = `
			<tr>
				<td><input type="checkbox" id="${field}_${selected_id}" name="${field}[${selected_id}]" checked="checked" value="${selected_id}"></td>
				<td align="left"><label for="${field}_${selected_id}">${selected_text}</label></td>
			</tr>`;

		$(base_jq + '_table').append(html);
		if (typeof jcf === 'object') jcf.replace(`#${field}_${selected_id}`);
	}
};