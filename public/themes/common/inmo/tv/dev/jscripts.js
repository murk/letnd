//=include greensock/TimelineLite.js
//=include greensock/TweenMax.js
//=include greensock/plugins/CSSPlugin.js
//=include greensock/easing/EasePack.js
//=include pace/pace.js
//=include mousetrap/mousetrap.js

$(window).on("load", function () {
	animation.init();
});


var animation = {
	// Configuració igual que als estils
	corporate_color: '#ed6833',
	text_holder_ease_hack: '10vw',

	current_timeline: '',
	current_article: -1,
	previous_article: 0,
	next_article: 0,
	total_articles: 0,
	init: function () {

		Pace.on('done', animation.pace_done);

	},
	pace_done: function () {

		$('.pace').css('background-color', 'transparent');

		var pace_tl = animation.current_timeline = new TimelineLite({
			onComplete: function () {
				$('body').removeClass('pace-done');
				$('.pace').detach();
			}
		});

		pace_tl
			.add("inici")
			.set('.pace-activity', {display: 'none'})
			.to('.pace-progress', 0.4, {
				borderTopWidth: 0,
				borderBottomWidth: 0,
				backgroundColor: 'transparent',
				ease: Power2.easeIn
			}, 'inici')
		;
		animation.start();

	},
	start: function () {
		/*
		$('body').click(function () {
			animation.current_timeline.paused(!animation.current_timeline.paused());
		});
		*/
		Mousetrap.bind('1', function (e) {
			TweenMax.resumeAll();
			// animation.current_timeline.paused(false);
			return false;
		});
		Mousetrap.bind('2', function (e) {
			TweenMax.pauseAll();
			// animation.current_timeline.paused(true);
			return false;
		});

		animation.total_articles = $('#main > article').length;

		animation.show_next_article(false);

	},
	show_next_article: function (tl) {

		if (tl) {
			tl.seek(0, false).stop();
		}

		animation.previous_article = animation.current_article;
		animation.current_article++;


		if (animation.current_article >= animation.total_articles) animation.current_article = 0;

		var next_article = animation.current_article + 1;
		if (next_article >= animation.total_articles) next_article = 0;
		animation.next_article = next_article;

		// dp('show_article', animation.current_article);
		animation.show_article(animation.current_article);

	},
	show_article: function (article) {


		var $article = $('#article_' + article),
			$next_article = $('#article_' + animation.next_article),
			$previous_article = $('#article_' + animation.previous_article),
			$image = $article.find('figure'),
			$text_holder = $article.find('.text-holder'),
			$ref_holder = $article.find('.ref-holder'),
			$ref = $ref_holder.find('.ref'),
			$logo = $ref_holder.find('.logo'),
			$price_holder = $article.find('.price-holder'),
			$details_holder = $article.find('.details-holder'),
			$details_holder_line = $details_holder.find('.line');

		$ref_holder.find('>*').width('auto');
		var ref_width = $ref_holder.width();
		$ref_holder.width(ref_width);
		var ref_height = $ref_holder.height();
		$ref_holder.height(ref_height);
		$ref_holder.find('>*').removeAttr('style');

		var tl = animation.current_timeline = new TimelineLite({
			onComplete: function () {
				animation.show_next_article(this)
			}
		});

		animation.swap_z_index($article, $next_article);

		$previous_article.css('opacity', 0);
		$next_article.css('opacity', 1);

		tl
			// Entra inmoble
			.to($article, 1, {
				opacity: 1
			})
			.set($ref, {width: 'auto'}, 'first')
			.set($logo, {width: 'auto'}, 'first')


			.to($text_holder, 2, {
				left: '-' + animation.text_holder_ease_hack,
				ease: Elastic.easeOut.config(1, 0.8)
			}, 'first')
			.to($ref_holder, 1, {opacity: 1}, 'first')

			.from($ref, 0.3, {width: 0}, 'first')
			.from($logo, 0.5, {width: 0, ease: Power2.easeIn}, "first")

			.to($details_holder, 0.5, {opacity: 1}, 'detalls')
			.to($details_holder_line, 1, {width: '100vw'}, 'detalls')
			.to($price_holder, 1, {opacity: 1}, 'detalls')

			// Surt immoble
			.to($article, 2, { opacity: 0, ease: Power2.easeIn}, '+=7')
		;

		// dp('Durada tl', tl.duration());

		var image_tl = new TimelineLite({
			onComplete: function () {
				this.seek(0, false).stop();
			}
		});
		image_tl
			.to($image, 3, {
				width: '110vw',
				left: '-5vw',
				top: '-5vw',
				ease: Power0.easeNone
			}, 0)
			.to($image, 5, {
				width: '120vw',
				top: '-10vw',
				ease: Power0.easeNone
			})
			.to($image, 5, {
				left: '0vw',
				top: '0vw',
				ease: Power0.easeNone
			})
		;

		// dp('Durada image_tl', image_tl.duration());


	},
	swap_z_index: function ($article, $next_article) {
		$article.css('z-index', 2);
		$next_article.css('z-index', 1);
	}
};


function setDebug() {
	if (window.console) {
		window.dp = window.console.log.bind(window.console);
	} else {
		window.dp = function () {
		};
	}
}

setDebug();



