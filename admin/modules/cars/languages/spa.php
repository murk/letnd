<?php
// menus eina
// menus eina
define('CARS_MENU_CARS','Cotxes');
define('CARS_MENU_NEW','Crear Cotxe');
define('CARS_MENU_LIST','Llistar Cotxes');
define('CARS_MENU_LIST_BIN','Paperera');
define('CARS_MENU_FAMILY','Famílies');
define('CARS_MENU_FAMILY_NEW','Crear família');
define('CARS_MENU_FAMILY_LIST','Llistar famílies');
define('CARS_MENU_FAMILY_LIST_BIN','Paperera');
define('CARS_MENU_BRAND','Marques');
define('CARS_MENU_BRAND_NEW','Crear marca');
define('CARS_MENU_BRAND_LIST','Llistar marques');
define('CARS_MENU_BRAND_LIST_BIN','Paperera');
define('CARS_MENU_TAX','Imposots');
define('CARS_MENU_TAX_NEW','Crear impost');
define('CARS_MENU_TAX_LIST','Llistar impost');
define('CARS_MENU_TAX_LIST_BIN','Paperera');

define('CARS_MENU_MODEL','Models');
define('CARS_MENU_MODEL_NEW','Crear model');
define('CARS_MENU_MODEL_LIST','Llistar model');
define('CARS_MENU_MODEL_LIST_BIN','Paperera');


$gl_caption_cars['c_ref'] = 'Ref';
$gl_caption_cars['c_ref_number'] = 'Ref. automàtica';
$gl_caption_cars['c_ref_supplier'] = 'Ref Proveïdor';
$gl_caption_cars['c_cars_private'] = 'Nom privat';
$gl_caption_cars['c_family_id'] = 'Família';
$gl_caption_cars['c_subfamily_id'] = 'Subfamília';
$gl_caption_cars['c_pvp'] = 'PVP';
$gl_caption_cars['c_pvd'] = 'PVD';
$gl_caption_cars['c_price_consult'] = 'a consultar';
$gl_caption_cars['c_price_consult_1'] = 'Preu a consultar';
$gl_caption_cars['c_brand_id'] = 'Marca';
$gl_caption_cars['c_sell'] = 'Venta web?';
$gl_caption['c_guarantee']='Garantia';
$gl_caption_cars['c_guarantee_period']='Periode garantia';
$gl_caption_cars['c_da']='dies';
$gl_caption_cars['c_mo']='mesos';
$gl_caption_cars['c_yr']='anys';
$gl_caption_cars['c_status'] = 'Estat';
$gl_caption_cars['c_free_send'] = 'Enviament gratuït';
$gl_caption_cars['c_ordre'] = 'Ordre';
$gl_caption_cars['c_cars_title'] = 'Nom del cotxe';
$gl_caption_cars['c_cars_description'] = 'Descripció del carse';
$gl_caption_cars['c_prepare'] = 'En preparació';
$gl_caption_cars['c_review'] = 'Per revisar';
$gl_caption_cars['c_onsale'] = 'Per vendre';
$gl_caption_cars['c_nostock'] = 'Fora de stock';
$gl_caption_cars['c_nocatalog'] = 'Descatalogat';
$gl_caption_cars['c_archived'] = 'Arxivat';
$gl_caption_cars['c_tax_id'] = 'I.V.A (%)';
$gl_caption_cars['c_discount'] = 'Descompte (%)';
$gl_caption_cars['c_margin'] = 'Marge (%)';
$gl_caption_cars['c_price_unit'] = 'Preu per';
$gl_caption_cars['c_unit'] = 'Unitat';
$gl_caption_cars['c_ten'] = 'Descena';
$gl_caption_cars['c_hundred'] = 'Centenar';
$gl_caption_cars['c_thousand'] = 'Miler';
$gl_caption_cars['c_couple'] = 'Parella';
$gl_caption_cars['c_dozen'] = 'Dotzena';
$gl_caption_cars['c_m2'] = 'm2';
$gl_caption_cars['c_data']='Dades del cotxe';
$gl_caption_cars['c_general_data']='Dades generals';
$gl_caption_cars['c_descriptions']='Descripcions';
$gl_caption_cars['c_description']='Descripciò';
$gl_caption_cars['c_data_web']='Venta Web';
$gl_caption_cars['c_year']='Any';
$gl_caption_cars['c_color']='Color';
$gl_caption_cars['c_cc']='Cilindrada';
$gl_caption_cars['c_kv']='Potencia (Kv)';
$gl_caption_cars['c_fuel']='Combustible';
$gl_caption_cars['c_diesel']='Diesel';
$gl_caption_cars['c_gasoil']='Gasolina';
$gl_caption_cars['c_hibrid']='Híbrid / Elèctric';
$gl_caption_cars['c_others']='Altres';
$gl_caption_cars['c_model_id']='Modelo';
$gl_caption_cars['c_km']='Kilometres';
$gl_caption_cars['c_product_private']='Nom privat';

$gl_caption_cars['c_data']='Dades del producte';
$gl_caption_cars['c_general_data']='Dades generals';
$gl_caption_cars['c_descriptions']='Descripcions';
$gl_caption_cars['c_description']='Descripciò';
$gl_caption_cars['c_data_product']='Dades del cotxe';
$gl_caption_cars['c_properties_product']='Característiques del cotxe';

$gl_caption['c_observations'] = 'Observacions';
$gl_caption['c_check_parent_id']='Tria familia';
$gl_caption['c_check_brand_id']='Tria marca';

$gl_caption['c_brand_name']='Nom de la marca';
$gl_caption_family['c_parent_id']='Familia a la que pertany';
$gl_caption_family['c_family']='Nom de la família';

$gl_caption['c_brand_id']='Marca';
$gl_caption['c_model_name']='Model';

$gl_caption_tax['c_tax_name']='Nom del impost';
$gl_caption_tax['c_tax_value']='Valor (%)';

$gl_caption_cars['c_search_title']= 'Busqueda de cotxes';
$gl_caption_cars['c_by_nif']= 'Per Ref.';
$gl_caption_cars['c_by_words']= 'Per paraules.';

//$gl_caption_family['']

$gl_caption['c_observations'] = 'Observations';
$gl_caption['c_ordre']='Ordre';


// captions seccio
?>
