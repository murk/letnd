class ReservaForm {

	constructor() {

		let _this = this;
		$(function () {

			let $items_rows = $('#reservaitem_list_row_list_table > tbody > tr');
			if ($items_rows.length > 0) {
				_this.init_button_delete();
				_this.init_button_delete_time();

			}
			/*else {
				$items_rows.find('.button-delete').hide();
			}*/

		});

	}

	init_button_delete() {
		let _this = this;
		$('.button-delete').click(function (e) {

			let reservaitem_id = $(this).data('reservaitem-id');
			let inputs_prefix = $('#inputs_prefix').val();
			_this.sublist_delete(reservaitem_id, inputs_prefix);
		});
	}

	init_button_delete_time() {
		let _this = this;
		$('.button-delete-time').click(function (e) {

			let reservaitem_id = $(this).data('reservaitem-id');
			let inputs_prefix = $('#inputs_prefix').val();
			_this.delete_item_time(reservaitem_id);
		});
	}

	sublist_delete(id, inputs_prefix) {


		let input = $('input[name="' + inputs_prefix + '[delete_record_ids]"]');


		let delete_ids = input.val();

		if (delete_ids) {
			delete_ids = delete_ids.split(',');
		}
		else {
			delete_ids = [];
		}
		delete_ids.push(id);
		input.val(delete_ids.join(','));


		$('#reservaitem_list_row_' + id).remove();
		let $items_rows = $('#reservaitem_list_row_list_table > tbody > tr');

		if ($items_rows.length <= 1) {
			$items_rows.find('.button-delete').hide();
		}
	}

	delete_item_time(reservaitem_id) {

		if (!confirm(LASSDIVE_CONFIRMAR_ESBORRAR_HORARI)) return;

		ReservaList.show_message_sending();

		let url = `/?language=${gl_language}&tool=lassdive&tool_section=reserva&action=delete_item_time&reservaitem_id=${reservaitem_id}`;
		$.ajax({
			url: url,
			dataType: 'json'
		})
			.done(function (data) {

				if (data.success) {
					$('#reservaitem_list_row_' + reservaitem_id).find('.reservaitem_date, .centre-id, .user-id').html('');
					ReservaList.show_message();
				}
				else {
					ReservaList.show_message(LASSDIVE_NO_ESBORRAT_HORARI);
				}

			});

	}
}


let gl_reserva_form = new ReservaForm();