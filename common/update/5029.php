<?

/* 22-7-13 
  Actualitzacio imatges a variacions



 */
if (is_table("product__variation") && !is_table('product__variation_image')){	
	$update_sql .="
            CREATE TABLE `product__variation_image` (
                    `image_id` INT(11) NOT NULL AUTO_INCREMENT,
                    `variation_id` INT(11) NOT NULL DEFAULT '0',
                    `name` VARCHAR(100) NOT NULL DEFAULT '' COLLATE 'utf8_spanish2_ci',
                    `size` INT(11) NOT NULL DEFAULT '0',
                    `ordre` INT(11) NOT NULL DEFAULT '0',
                    `main_image` INT(1) NOT NULL DEFAULT '0',
                    PRIMARY KEY (`image_id`)
            )
            COLLATE='utf8_spanish2_ci'
            ENGINE=MyISAM;";
	$update_sql .="
            CREATE TABLE `product__variation_image_language` (
                    `image_id` INT(11) NOT NULL DEFAULT '0',
                    `language` CHAR(3) NOT NULL COLLATE 'utf8_spanish2_ci',
                    `image_alt` VARCHAR(150) NOT NULL COLLATE 'utf8_spanish2_ci',
                    `image_title` VARCHAR(255) NOT NULL COLLATE 'utf8_spanish2_ci'
            )
            COLLATE='utf8_spanish2_ci'
            ENGINE=MyISAM;
            ";

}

$client_dir = DOCUMENT_ROOT . '/' . $_SESSION['client_dir'];
$dir=$client_dir . '/product/';

// directori product si no hi hes
if (!is_dir($dir)){
	mkdir ($dir, 0776);
}
	
$dir=$client_dir . '/product/variation/';
if (!is_dir($dir)){
	mkdir ($dir, 0776);
}
$dir=$client_dir . '/product/variation/images/';
if (!is_dir($dir)){
	mkdir ($dir, 0776);
}

?>