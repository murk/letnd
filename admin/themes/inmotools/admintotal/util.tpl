<table width="100%" border="0" cellspacing="0" cellpadding="0" class="form">
  <tr>
    <td valign="bottom">
    <div class="menu">
    <div>Idiomes</div>
    <a href="?menu_id=1033&amp;action=list_records_vars">Variables</a>
    <a href="?menu_id=1033&amp;action=list_records_tags">Tags</a>
    </td>
    <td class="buttons_top"><div class="right"></div>
    </td>
  </tr>
</table>
<script language="JavaScript">
function hide(lang){
	$('.' + lang).toggle();
}
</script>
<table width="100%"><tr><td class="formCaptionTitle">
Afegir idioma per cada registre
</td></tr></table>
<br />
<br />
<div id='opcions_holder'><table width="100%" border="0" cellspacing="0" cellpadding="0" id="opcions">
  <tr>
    <td class="opcionsTitol"><strong>Opcions</strong></td>
  </tr>
  <tr>
    <td class="opcions">&nbsp;<?foreach($languages as $lang): ?><?=$lang?>
      <input name="checkbox" type="checkbox" id="checkbox" checked="checked" onclick="hide('<?=$lang?>')"/>
      <?endforeach //$s?>
      &nbsp;&nbsp;&nbsp;selects <input name="checkbox" type="checkbox" id="checkbox" checked="checked" onclick="hide('resaltatSmall')"/>&nbsp;&nbsp;&nbsp;nums <input name="checkbox" type="checkbox" id="checkbox" checked="checked" onclick="hide('nums')"/></td>
  </tr>
</table></div>
<table width="100%" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td class="formsCaptionHor">S'ha de controlar d'afegir els noms ja predefinits a l'eina, com ara les categories de inmo</td>
</tr>
</tbody>
</table><br /><br />
<?foreach($loop as $l): extract ($l)?>
<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td class="listDetails"><table width="100%" cellpadding="0" cellspacing="0">
      <tr>
        <td class="listItem">
      <?foreach($sqls as $s): extract ($s)?><div class="<?=$lang?>"><span class="resaltatSmall"><strong class="nums">/*<?=$count?>*/</strong>&nbsp;&nbsp;<?=$sql2?>
              </span><br /><strong class="nums">/*<?=$count?>*/</strong> <?=$sql?></div>
      <?endforeach //$s?>	  </td>
      </tr>
    </table></td>
  </tr>
</table>
<br>
<?endforeach //$loop?>

<table width="100%"><tr><td class="formCaptionTitle">
Borrar idiomes que no s'utilitzen
</td></tr></table><br /><br />
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td class="opcionsTitol"><strong>Opcions</strong></td>
  </tr>
  <tr>
    <td class="opcions">
      nums <input name="checkbox" type="checkbox" id="checkbox" checked="checked" onclick="hide('nums')"/></td>
  </tr>
</table>
<br /><br />
<br />
<?foreach($loop as $l): extract ($l)?>
<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td class="listDetails"><table width="100%" cellpadding="0" cellspacing="0">
      <tr>
        <td class="listItem">
      <?foreach($sqls as $s): extract ($s)?><span class="nums"><strong>/*<?= $count3 ?>*/</strong> </span><?=$sql3?><?break // nomes mostro un sol per cada idioma?>
      <?endforeach //$s?>	  </td>
      </tr>
    </table></td>
  </tr>
</table>
<br>
<?endforeach //$loop?>
