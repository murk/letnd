<?

/* 21-5-16

	MISSATGES PER GRUPS
 */


if ( ! is_table( 'missatge__missatge_to_group' ) ) {

	$update_sql [] = "CREATE TABLE `missatge__missatge_to_group` (
							`missatge_id` INT(11) NOT NULL DEFAULT '0',
							`group_id` INT(11) NOT NULL DEFAULT '0',
							INDEX `missatge_id` (`missatge_id`),
							INDEX `group_id` (`group_id`)
						)
						COLLATE='utf8_spanish2_ci'
						ENGINE=MyISAM
						;";

} else {
	trigger_error( 'Ja existeixen la taula missatge__missatge_to_group' );
}

$update_sql [] = "ALTER TABLE `missatge__missatge`
	CHANGE COLUMN `kind` `kind` ENUM('mls','general') NULL DEFAULT 'general' COLLATE 'utf8_spanish2_ci' AFTER `status`;";

$update_sql [] = "ALTER TABLE `missatge__missatge`
	ADD COLUMN `creator_user_id` INT(11) NOT NULL DEFAULT '0' AFTER `link_text`;";

$update_sql [] = "ALTER TABLE `missatge__missatge`
	ADD COLUMN `modified` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00' AFTER `entered`;";

$update_sql [] = "UPDATE `missatge__missatge` SET `modified`=entered;";

$update_sql [] = "UPDATE `all__menu` SET `link`='80003' WHERE  `menu_id`=80000;";

$update_sql [] = "ALTER TABLE `missatge__missatge`
	CHANGE COLUMN `from_id` `mls_from_id` INT(11) NOT NULL AFTER `kind`,
	CHANGE COLUMN `to_id` `mls_to_id` INT(11) NOT NULL AFTER `mls_from_id`;";

$update_sql [] = "ALTER TABLE `missatge__missatge`
	CHANGE COLUMN `asunto` `subject` VARCHAR(250) NOT NULL COLLATE 'utf8_spanish2_ci' AFTER `mls_to_id`;";

$update_sql [] = "ALTER TABLE `missatge__missatge`
	ADD COLUMN `top_reply_id` INT(11) NOT NULL DEFAULT '0' AFTER `creator_user_id`,
	ADD COLUMN `parent_reply_id` INT(11) NOT NULL DEFAULT '0' AFTER `top_reply_id`;";

$update_sql [] = "ALTER TABLE `missatge__missatge`
	ADD COLUMN `sent` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00' AFTER `modified`;";

$update_sql [] = "UPDATE `missatge__missatge` SET `sent`=entered;";

if ( ! is_table( 'missatge__missatge_to_user' ) ) {
	$update_sql [] = "CREATE TABLE `missatge__missatge_to_user` (
						`missatge_id` INT(11) NOT NULL DEFAULT '0',
						`user_id` INT(11) NOT NULL DEFAULT '0',
						INDEX `missatge_id` (`missatge_id`),
						INDEX `user_id` (`user_id`)
					)
					COLLATE='utf8_spanish2_ci'
					ENGINE=MyISAM
					;";
} else {
	trigger_error( 'Ja existeixen la taula missatge__missatge_to_user' );
}
if ( ! is_table( 'missatge__missatge_read_to_user' ) ) {
	$update_sql [] = "CREATE TABLE `missatge__missatge_read_to_user` (
						`missatge_id` INT(11) NOT NULL DEFAULT '0',
						`user_id` INT(11) NOT NULL DEFAULT '0',
						INDEX `missatge_id` (`missatge_id`),
						INDEX `user_id` (`user_id`)
					)
					COLLATE='utf8_spanish2_ci'
					ENGINE=MyISAM
					;";

	} else {
	trigger_error( 'Ja existeixen la taula missatge__missatge_read_to_user' );
}

// Poso tots el missatges com MLS, fins ara són els únics que s'han fet
$update_sql [] = "UPDATE missatge__missatge SET kind = 'mls';";

// Missatge que vaig enviar desde letnd, estava malament
$update_sql [] = "UPDATE missatge__missatge SET mls_from_id= 0, mls_to_id = 0, kind = 'general' WHERE mls_from_id = 238 AND mls_to_id = 208 and link= '';"

?>