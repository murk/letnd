<?

/* 10-07-15

  Actualitzacio inomo booking - Excels reserves

 */

if ( is_table( 'booking__book' ) && ! is_field( 'booking__book', 'payment_1' ) ) {

  $update_sql[] = "ALTER TABLE `booking__book`
	ADD COLUMN `payment_1` DECIMAL(11,2) NOT NULL AFTER `promcode_discount`,
	ADD COLUMN `payment_done_1` DECIMAL(11,2) NOT NULL AFTER `payment_1`,
	ADD COLUMN `payment_entered_1` DATE NOT NULL AFTER `payment_done_1`,
	ADD COLUMN `payment_2` DECIMAL(11,2) NOT NULL AFTER `payment_entered_1`,
	ADD COLUMN `payment_done_2` DECIMAL(11,2) NOT NULL AFTER `payment_2`,
	ADD COLUMN `payment_entered_2` DATE NOT NULL AFTER `payment_done_2`,
	ADD COLUMN `payment_3` DECIMAL(11,2) NOT NULL AFTER `payment_entered_2`,
	ADD COLUMN `payment_done_3` DECIMAL(11,2) NOT NULL AFTER `payment_3`,
	ADD COLUMN `payment_entered_3` DATE NOT NULL AFTER `payment_done_3`,
	ADD COLUMN `payment_4` DECIMAL(11,2) NOT NULL AFTER `payment_entered_3`,
	ADD COLUMN `payment_done_4` DECIMAL(11,2) NOT NULL AFTER `payment_4`,
	ADD COLUMN `payment_entered_4` DATE NOT NULL AFTER `payment_done_4`;";

  $update_sql[] = "UPDATE booking__book SET `payment_1`=price_book*0.4;";

  $update_sql[] = "UPDATE booking__book SET `payment_2`=price_total-payment_1;";

} else {
  if ( is_table( 'booking__book' ) ) {
    trigger_error( 'Ja existeixen el camp payment_1' );
  }
}

?>