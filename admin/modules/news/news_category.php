<?
/**
 * NewsCategory
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2010
 * @version $Id$
 * @access public
 */
class NewsCategory extends Module{

	function __construct(){
		parent::__construct();
	}
	function on_load(){

		if (!Db::get_first('SELECT count(*) FROM news__listtpl LIMIT 1')) {
			$this->unset_field( 'listtpl_id' );
		}
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function get_records()
	{
		$listing = new ListRecords($this);
		$listing->has_bin = false;
		$listing->order_by = 'ordre asc, category asc';
		$listing->set_options (1, 1, 1, 1, 1, 1, 0, 0, 1); // $options_bar = 1, $options_checkboxes = 1, $save_button = 1, $select_button = 1, $delete_button = 1, $print_button = 1, $edit_buttons = 1, $image_buttons = 1, $split_count = 1
		$listing->add_button ('new_button', 'action=list_records&menu_id=3003&selected_menu_id=3007', 'boto1', 'after');
		$listing->show_langs = true;
		return $listing->list_records();
	}
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{
	    $show = new ShowForm($this);
		$show->has_bin = false;
	    return $show->show_form();
	}

	function save_rows()
	{
	    $save_rows = new SaveRows($this);
	    $save_rows->save();
	}

	function write_record()
	{
	    $writerec = new SaveRows($this);
	    $writerec->save();
	}

	function manage_images()
	{
	    $image_manager = new ImageManager($this);
	    $image_manager->execute();
	}
}
?>