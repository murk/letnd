<?php
global $gl_caption_admin,
       $gl_caption_public,
       $gl_caption_newsletter,
       $gl_caption_contact_public,
       $gl_caption_inmo_admin,
       $gl_caption_inmo_public,
       $gl_caption_booking_admin,
       $gl_caption_booking_public,
       $gl_caption_translation,
       $gl_caption_custumer,
       $gl_caption_page,
       $gl_caption_save,
       $gl_caption_product_admin,
       $gl_caption_product_public,
       $gl_caption_utility;
// menus eina
if ( ! defined( 'WEB_MENU_TITLE' ) ) {
	define( 'WEB_MENU_TITLE', 'Titol web' ); //Nom apareix dalt internet explorer
	define( 'WEB_MENU_ROBOTS', 'Tags de la web' ); //Paraules dels tags
	define( 'WEB_MENU_SAVE', 'Copies de seguretat' ); //Paraules dels tags
	define( 'WEB_MENU_SAVE_LIST', 'Realitzar copia' ); //Paraules dels tags
	define( 'WEB_MENU_TRANSLATION', 'Traduccions' );
	define( 'WEB_MENU_TRANSLATION_LIST', 'Llistar totes les traduccions' );
	define( 'WEB_MENU_PAGE_TRANSLATION_LIST', 'Llistar apartats per traduir' );
	define( 'WEB_MENU_BACKOFFICE_SEARCH', 'Busquedes clients' );


	define( 'WEB_MENU_CONFIG', 'Configuració' );
	define( 'WEB_MENU_CONFIGADMIN_LIST', 'General part privada' );
	define( 'WEB_MENU_CONFIGPUBLIC_LIST', 'General part pública' );
	define( 'WEB_MENU_CONFIGINMOADMIN_LIST', 'Propietats part privada' );
	define( 'WEB_MENU_CONFIGINMOPUBLIC_LIST', 'Propietats part pública' );
	define( 'WEB_MENU_CONFIGCUSTUMER_LIST', 'Clients' );
	define( 'WEB_MENU_CONFIGNEWSLETTER_LIST', 'Newsletter' );
	define( 'WEB_MENU_CONFIGUTILITY_LIST', 'Utilitats' );
	define( 'WEB_MENU_CONFIGMLS_LIST', 'MLS' );
	define( 'WEB_MENU_CONFIGCONTACT_PUBLIC_LIST', 'Contacte part pública' );
	define( 'WEB_MENU_CONFIGCONTACT_ADMIN_LIST', 'Contacte part privada' );

	define( 'WEB_MENU_CONFIGBOOKING_ADMIN_LIST', 'Reserves part privada' );
	define( 'WEB_MENU_CONFIGBOOKING_PUBLIC_LIST', 'Reserves part pública' );

	define( 'WEB_MENU_CONFIGPRODUCT_ADMIN_LIST', 'Productes part privada' );
	define( 'WEB_MENU_CONFIGPRODUCT_PUBLIC_LIST', 'Productes part pública' );

	define( 'WEB_MENU_CONFIGCLIENT_LIST', 'Frases part pública' );

	if ( ! defined( 'WEB_MENU_BANNER' ) ) {
		define( 'WEB_MENU_BANNER', 'Banners' );
		define( 'WEB_MENU_BANNER_NEW', 'Nou banner' );
		define( 'WEB_MENU_BANNER_LIST', 'Llistar banners' );
		define( 'WEB_MENU_BANNER_LIST_BIN', 'Paperera' );
	}

	define( 'WEB_MENU_PAGE', 'Edició de pàgines' );
	define( 'WEB_MENU_PAGE_NEW', 'Nova pàgina' );
	define( 'WEB_MENU_PAGE_LIST', 'Llistar pàgina' );
	define( 'WEB_MENU_PAGE_LIST_BIN', 'Paperera' );

	define( 'WEB_MENU_GUESTBOOK', 'Llibre de visites' );
	define( 'WEB_MENU_GUESTBOOK_NEW', 'Nova visita' );
	define( 'WEB_MENU_GUESTBOOK_LIST', 'Llistar visites' );
	define( 'WEB_MENU_GUESTBOOK_LIST_BIN', 'Paperera' );


	define( 'WEB_MENU_GALLERY', 'Galeries d\'imatges' );
	define( 'WEB_MENU_GALLERY_NEW', 'Nova galeria' );
	define( 'WEB_MENU_GALLERY_LIST', 'Llistar galeries' );
	define( 'WEB_MENU_GALLERY_LIST_BIN', 'Paperera' );


	define( 'WEB_MENU_PLACE', 'Offices' );
	define( 'WEB_MENU_PLACE_NEW', 'New office' );
	define( 'WEB_MENU_PLACE_LIST', 'List offices' );
}

if ( ! defined( 'API_MENU_PAGINA' ) ) {
	// per poder banners fer desde api
	define( 'API_MENU_PAGINA', 'Edició de pàgines' );
	define( 'API_MENU_PAGINA_NEW', 'Entrar nova pàgina' );
	define( 'API_MENU_PAGINA_NEW_SECOND', 'Entrar nova pàgina secundària' );
	define( 'API_MENU_PAGINA_LIST', 'Llistar pàgines' );
	define( 'API_MENU_PAGINA_LIST_SECOND', 'Llistar pàgines secundàries' );
	define( 'API_MENU_PAGINA_BIN', 'Paperera de reciclatge' );
}

/*--------------- no traduit -------------------*/

$gl_caption_product_admin['c_group_15']     = 'Configurable features';
for ($i = 1; $i <= 20; $i++) {
	$gl_caption_config['c_others' . $i] = 'Feature ' . $i;
}
for ($i = 1; $i <= 20; $i++) {
	$gl_caption_config['c_others' . $i . '_type'] = 'Type';
}


$gl_caption_config['c_default_home_tool'] = 'Eina per defecte';
$gl_caption_config['c_default_home_tool_section'] = 'Secció per defecte';
$gl_caption_config['c_language'] = 'Idioma per defecte';
$gl_caption_config['c_home_max_results'] = 'Nombre de resultats a la pàgina d\'inici';
$gl_caption_config['c_page_slogan'] = 'Frase corporativa';
$gl_caption_public['c_page_title'] = 'Títol de la web';
$gl_caption_config['c_page_phone'] = 'Telèfon';
$gl_caption_config['c_page_mobile']               = 'Mòbil';
$gl_caption_config['c_page_fax'] = 'Fax';
$gl_caption_config['c_page_address'] = 'Street';
$gl_caption_config['c_page_numstreet'] = 'Nombre';
$gl_caption_config['c_page_flat'] = 'Étage';
$gl_caption_config['c_page_door'] = 'Porte';
$gl_caption_config['c_page_postcode'] = 'Code postal';
$gl_caption_config['c_page_municipi'] = 'Població';
$gl_caption_config['c_page_provincia'] = 'Provincia';
$gl_caption_config['c_page_country'] = 'Pays';
$gl_caption_config['c_page_title'] = 'Títol Intranet';
$gl_caption_config['c_default_menu_id'] = 'Menú per defecte';
$gl_caption_config['c_theme'] = 'Tema';
$gl_caption_config['c_language'] = 'Idioma';
$gl_caption_config['c_max_results'] = 'Nombre de resultats per pàgina';
$gl_caption_config['c_max_page_links'] = 'Nombre de pàgines a mostrar';
$gl_caption_config['c_add_dots_length'] = 'Màxim de paraules a les descripcions';
$gl_caption_config['c_date_created'] = 'Data de creació de la web';
$gl_caption_admin['c_group_1'] = 'Opcions dels llistats';
$gl_caption_public['c_group_2'] = $gl_caption_admin['c_group_1'];
$gl_caption_config['c_templatehtml_id'] = 'Plantilla en html';
$gl_caption_config['c_templatetext_id'] = 'Plantilla de text';
$gl_caption_config['c_group_1'] = 'De';
$gl_caption_config['c_group_2'] = 'Contestar a';
$gl_caption_config['c_group_3'] = 'Dades SMTP';
$gl_caption_config['c_group_4'] = 'Adreça';
$gl_caption_config['c_group_5'] = 'Dades empresa';
$gl_caption_config['c_company_name'] = 'Nom empresa';
$gl_caption_config['c_company_cif'] = 'CIF / NIF';
$gl_caption_config['c_m_username'] = 'Nom d\'usuari';
$gl_caption_config['c_m_password'] = 'Contrasenya';
$gl_caption_config['c_m_password_repeat'] = 'Repetir contrasenya';
$gl_caption_config['c_m_host'] = 'Servidor';
$gl_caption['c_custumer_table'] = 'Taula d\'adreces';
$gl_caption['c_newsletter__custumer'] = 'newsletter__custumer';
$gl_caption['c_custumer__custumer'] = 'custumer__custumer';
$gl_caption['c_contact__contact'] = 'contact__contact';
$gl_caption['c_product__customer'] = 'product__customer';
$gl_caption_newsletter['c_content_extra'] = 'Contingut inclòs';
$gl_caption_config['c_only_test'] = 'Mode test';
$gl_caption_config['c_only_test_description'] = 'Si marques aquesta casella, no s\'enviarà cap mail, nomès es simularà l\'enviament';

$gl_caption_config['c_text'] = 'text';
$gl_caption_config['c_provincies'] = 'Províncies habituals';
$gl_caption_config['c_default_provincia'] = 'Província per defecte';
$gl_caption_config['c_default_comarca'] = 'Comarca per defecte';
$gl_caption_config['c_default_municipi'] = 'Municipi per defecte';
$gl_caption_config['c_units'] = 'Unitats';

$gl_caption_inmo_admin['c_group_1'] = 'Thumbnail admin';
$gl_caption_config['c_im_admin_thumb_w'] = 'amplada';
$gl_caption_config['c_im_admin_thumb_h'] = 'alçada';
$gl_caption_config['c_im_admin_thumb_q'] = 'qualitat';

$gl_caption_inmo_admin['c_group_2'] = 'Thumbnail públic';
$gl_caption_inmo_public['c_group_2'] = $gl_caption_inmo_admin['c_group_2'];
$gl_caption_config['c_im_thumb_w'] = 'amplada';
$gl_caption_config['c_im_thumb_h'] = 'alçada';
$gl_caption_config['c_im_thumb_q'] = 'qualitat';

$gl_caption_inmo_admin['c_group_3'] = 'Imatge fitxa públic';
$gl_caption_inmo_public['c_group_3'] = $gl_caption_inmo_admin['c_group_3'];
$gl_caption_config['c_im_details_w'] = 'amplada';
$gl_caption_config['c_im_details_h'] = 'alçada';
$gl_caption_config['c_im_details_q'] = 'qualitat';

$gl_caption_inmo_admin['c_group_4'] = 'Imatge gran públic';
$gl_caption_inmo_public['c_group_4'] = $gl_caption_inmo_admin['c_group_4'];
$gl_caption_config['c_im_medium_w'] = 'amplada';
$gl_caption_config['c_im_medium_h'] = 'alçada';
$gl_caption_config['c_im_medium_q'] = 'qualitat';


$gl_caption_inmo_admin['c_group_5'] = 'Imatge gran impressió';
$gl_caption_config['c_im_big_w'] = 'amplada';
$gl_caption_config['c_im_big_h'] = 'alçada';

$gl_caption_config['c_im_list_cols'] = 'Col·lumnes imatge';
$gl_caption_config['c_property_auto_ref'] = 'Referència automàtica';
$gl_caption_config['c_menu_price'] = 'Valors del menú de preu';
$gl_caption_config['c_menu_price_rent'] = 'Valors del menú de preu lloguer';
$gl_caption_config['c_recent_time']         = 'Days to show "new property" tag';
$gl_caption_config['c_home_max_results'] = 'Nombre destacats';
$gl_caption_config['c_home_title'] = 'Títol de propietats destacades';
$gl_caption_config['c_html'] = 'html';
$gl_caption_config['c_from_address'] = 'E-mail';
$gl_caption_config['c_from_address_repeat'] = 'Repetir e-mail';
$gl_caption_config['c_from_name'] = 'Nom';
$gl_caption_config['c_reply_to'] = 'E-mail';
$gl_caption_config['c_reply_to_repeat'] = 'Repetir e-mail';
$gl_caption_config['c_reply_to_name'] = 'Nom';
$gl_caption_config['c_subject'] = 'Assumpte';
$gl_caption_config['c_format'] = 'Format';
$gl_caption_config['c_send_method'] = 'Forma d\'enviament';
$gl_caption_config['c_CC'] = 'Còpia a cada adreça (CC)';
$gl_caption_config['c_BCC'] = 'Còpia oculta a cada adreça (BCC)';
$gl_caption_config['c_one_by_one'] = 'Un missatge diferent per cada adreça';
$gl_caption['c_template_caption']='[Cap plantilla]';

$gl_caption_translation['c_2'] = 'En traducció';
$gl_caption_translation['c_3'] = 'Finalitzat';
$gl_caption_translation['c_1'] = 'Pendent';
$gl_caption_translation['c_4'] = 'Error';
$gl_caption['c_description'] = 'Descripció de la web';
$gl_caption['c_mail'] = 'Correu predeterminat';
$gl_caption['c_title'] = 'Titol de la web';
$gl_caption['c_robots'] = 'Tags de la web';
$gl_caption_custumer['c_group_1'] = 'Cerca aproximativa';
$gl_caption_custumer['c_group_2'] = 'MLS';
$gl_caption_config['c_search_percent'] = 'Desnivell a les cerques client i MLS (%)';
$gl_caption_config['c_search_level'] = 'Nivell d\'aproximació en la cerca';
$gl_caption_translation['c_join_date'] = 'Data d\'enviament';
$gl_caption_translation['c_state'] = 'Estat actual:';
$gl_caption_translation['c_numwords'] = 'Número de paraules:';
$gl_caption_translation['c_originaltext'] = 'Text original:';

$gl_caption['c_search_level_1'] = 'Restrictiu';
$gl_caption['c_search_level_2'] = 'Normal';
$gl_caption['c_search_level_3'] = 'Poc restrictiu';

$gl_caption_config ['c_mls_all'] = 'Marcar per defecte la casella MLS dels immobles';
$gl_caption_config ['c_mls_comission'] = 'Comissió per defecte de les propietats MLS (%)';
$gl_caption_config ['c_mls_search_always'] = 'Cerca MLS activada';
$gl_caption_config ['c_mls_allow_listing'] = 'Permetre llistar immobles';

$gl_caption_config ['c_0'] = 'No';
$gl_caption_config ['c_1'] = 'Si';
$gl_caption_config ['c_default_mail']='E-mail predeterminat';
$gl_caption_config ['c_use_custom_foot'] = 'Peu intranet';

$gl_caption_save ['c_save_1'] = 'Clica aquí';
$gl_caption_save ['c_save_2'] = 'per descarregar la Copia de seguretat';


$gl_caption_config ['c_google_center_longitude'] = 'Ubicació';
$gl_caption_inmo_admin['c_google_center_longitude'] = 'Posició inicial del mapa';
$gl_caption_config ['c_google_latitude'] = 'Latitud';
$gl_caption_config ['c_google_longitude'] = 'Longitud';
$gl_caption_config ['c_google_center_latitude'] = 'Latitud centre mapa';
$gl_caption_config ['c_center_longitude'] = 'Longitud centre mapa';
$gl_caption_config ['c_google_zoom']='';
$gl_caption_config ['c_allow_seo']='Activar SEO';

// contact
$gl_caption_contact_public['c_group_4']  = 'Opcions';
$gl_caption_contact_public['c_send_mail_copy']  = 'Enviar còpia al client';
$gl_caption_contact_public['c_use_mail_template']  = 'Utilitzar plantilla';

// inmo
$gl_caption_inmo_admin['c_custom1_name']   = 'Característica 1';
$gl_caption_inmo_admin['c_custom2_name']   = 'Característica 2';
$gl_caption_inmo_admin['c_custom3_name']   = 'Característica 3';
$gl_caption_inmo_admin['c_custom4_name']   = 'Característica 4';
$gl_caption_inmo_admin['c_custom5_name']   = 'Característica 5';
$gl_caption_inmo_admin['c_custom6_name']   = 'Característica 6';
$gl_caption_inmo_admin['c_custom7_name']   = 'Característica 7';
$gl_caption_inmo_admin['c_custom8_name']   = 'Característica 8';
$gl_caption_inmo_admin['c_custom1_type']   =
$gl_caption_inmo_admin['c_custom2_type'] =
$gl_caption_inmo_admin['c_custom3_type'] =
$gl_caption_inmo_admin['c_custom4_type'] =
$gl_caption_inmo_admin['c_custom5_type'] =
$gl_caption_inmo_admin['c_custom6_type'] =
$gl_caption_inmo_admin['c_custom7_type'] =
$gl_caption_inmo_admin['c_custom8_type'] = 'Tipus';
$gl_caption_inmo_admin['c_custom1_filter'] =
$gl_caption_inmo_admin['c_custom2_filter'] =
$gl_caption_inmo_admin['c_custom3_filter'] =
$gl_caption_inmo_admin['c_custom4_filter'] =
$gl_caption_inmo_admin['c_custom5_filter'] =
$gl_caption_inmo_admin['c_custom6_filter'] =
$gl_caption_inmo_admin['c_custom7_filter'] =
$gl_caption_inmo_admin['c_custom8_filter'] = 'Filtre';

$gl_caption_config['c_checkbox']     = 'Casella verificació';
$gl_caption_config['c_int']          = 'Nombre';
$gl_caption_config['c_text']         = 'Texte en idiomes';
$gl_caption_config['c_text_no_lang'] = 'Texte';
$gl_caption_config['c_textarea']     = 'Texte varies línies';
$gl_caption_config['c_select']       = 'Desplegable';

// booking
$gl_caption_booking_admin ['c_group_1'] = "Reservas";
$gl_caption_booking_admin ['c_group_2'] = "Datos Mossos";
$gl_caption_booking_admin ['c_group_3'] = "Contratos";

$gl_caption['c_ignore_outside_ue']           = "Ignorar fuera de la UE";
$gl_caption['c_default_status']              = "Estado por defecto al reservar desde admin";
$gl_caption['c_public_default_status']       = "Estado por defecto al reservar desde la web";
$gl_caption['c_booked']                      = "Reservado";
$gl_caption['c_pre_booked']                  = "Pre reserva";
$gl_caption['c_has_payment']                 = "Módulo de pago";
$gl_caption['c_nom_establiment']             = "Nombre establecimiento";
$gl_caption['c_codi_establiment']            = "Código establecimiento";
$gl_caption['c_has_contracts']               = "Contratos";
$gl_caption['c_contract_default_language']   = "Idioma por defecto";
$gl_caption['c_contract_format']             = "Formato";
$gl_caption['c_google_analytics_conversion'] = "Codi conversió google analytics";


// productes
$gl_caption_product_admin ['c_default_sell']='Venta predeterminada';
$gl_caption_product_admin ['c_pvp_required']='P.V.P obligatori';
$gl_caption_product_admin ['c_home_title']='Titol dels productes destacats';
$gl_caption_product_admin ['c_group_1'] = $gl_caption_product_public ['c_group_1'] = $gl_caption_inmo_admin['c_group_1'];
$gl_caption_product_admin ['c_group_2'] = $gl_caption_product_public ['c_group_2'] = $gl_caption_inmo_admin['c_group_2'];
$gl_caption_product_admin ['c_group_3'] = $gl_caption_product_public ['c_group_3'] = $gl_caption_inmo_admin['c_group_3'];
$gl_caption_product_admin ['c_group_4'] = $gl_caption_product_public ['c_group_4'] = $gl_caption_inmo_admin['c_group_4'];
$gl_caption_product_admin ['c_group_5'] = $gl_caption_product_public ['c_group_5'] = $gl_caption_inmo_admin['c_group_5'];

$gl_caption_product_public ['c_group_5']      = $gl_caption_booking_public ['c_group_5'] = 'Opcions del mòdul de pagament';
$gl_caption_product_public ['c_group_6']      = 'Pàgina d\'inici';
$gl_caption_product_public ['c_group_7']      = $gl_caption_booking_public ['c_group_7'] = 'Transferència bancària';
$gl_caption_product_public ['c_group_8']      = $gl_caption_booking_public ['c_group_8'] = 'Paypal';
$gl_caption_product_public ['c_group_9']      = $gl_caption_booking_public ['c_group_9'] = 'Targeta Redsys';
$gl_caption_product_public ['c_group_10']     = 'Targeta 4b';
$gl_caption_product_public ['c_no_sell_text'] = 'Descripció producte no en venda';

$gl_caption ['c_paypal_sandbox']           = 'Paypal en mode sandbox';
$gl_caption ['c_banc_name']                = 'Nom del banc';
$gl_caption ['c_account_number']           = 'Número de compte';
$gl_caption ['c_paypal_mail']              = 'E-mail del compte PayPal';
$gl_caption ['c_paypal_mail_repeat']       = 'Repetir e-mail';
$gl_caption_product_public ['c_home_show_marked']         = 'Mostra destacats';
$gl_caption_product_public ['c_home_show_familys']        = 'Mostra famílies i subfamílies';
$gl_caption_product_public ['c_home_max_familys']         = 'Nombre màxim de famílies';
$gl_caption_product_public ['c_home_max_family_products'] = 'Nombre màxim de productes de cada família';
$gl_caption ['c_payment_methods']          = 'Mòduls de pagament actius';
$gl_caption ['c_paypal']                   = 'PayPal';
$gl_caption ['c_account']                  = 'Transferència bancària';
$gl_caption ['c_ondelivery']               = 'Contrareembols';
$gl_caption ['c_directdebit']              = 'Rebut bancari';
$gl_caption ['c_4b']                       = 'Targeta 4b';
$gl_caption_product_public['c_home_title']                = 'Títol de la pàgina inicial';
$gl_caption_product_public ['c_4b_simulador']             = '4b en mode simulador';
$gl_caption_product_public ['c_4b_id_comercio']           = '4b id comerç';
$gl_caption ['c_lacaixa']                  = 'Targeta Redsys';
$gl_caption ['c_lacaixa_simulador']        = 'Redsys en mode simulador';
$gl_caption ['c_lacaixa_clave_sha2']       = 'Clau SHA256';
$gl_caption ['c_lacaixa_merchant_code']    = 'Codi de comerç';

$gl_caption_product_public ['c_can_collect']    = 'Permetre recollida en botiga física';

$gl_caption_product_admin['c_is_public_tax_included']='Tots els preus (PVP) són IVA inclòs';
$gl_caption_product_admin['c_is_wholesaler_tax_included']='Tots els preu distribuïdor (PVD) són IVA inclòs';
$gl_caption_product_admin['c_group_12']='Impostos';
$gl_caption_product_admin['c_has_equivalencia_default']='Equivalència marcada a l\'insertar distribuïdor';
$gl_caption_product_admin['c_stock_control']='Control automàtic d\'stock';
$gl_caption_product_admin['c_allow_null_stock']='Permetre venta productes amb stock negatiu';
$gl_caption_product_admin['c_group_10']='Stock';
$gl_caption_product_admin['c_group_11']='Tarifes d\'enviament';
$gl_caption_product_admin['c_send_rate_spain']='Espanya';
$gl_caption_product_admin['c_send_rate_france']='França';
$gl_caption_product_admin['c_send_rate_andorra']='Andorra';
$gl_caption_product_admin['c_send_rate_world']='Resta del món';
$gl_caption_product_admin['c_allow_free_send']='Casella activada, permetre enviament gratuit';
$gl_caption_product_admin['c_minimum_buy_free_send']='Compra mínima de';
$gl_caption_product_admin['c_tax_included'] = 'IVA inclòs';
$gl_caption_product_admin['c_tax_not_included'] = 'IVA no inclòs';


$gl_caption_page['c_ordre']               = 'Ordre';
$gl_caption_page['c_template']            = 'Plantilla';
$gl_caption_page['c_link']                = 'Link';
$gl_caption_page_list['c_page_title']     = 'Títol als cercadors';
$gl_caption_page['c_page']                = 'Nom menú o títol cos';
$gl_caption_page['c_title']               = 'Títol al cos';
$gl_caption_page['c_content']             = 'Contingut';
$gl_caption_page['c_prepare']             = 'En preparació';
$gl_caption_page['c_review']              = 'Per revisar';
$gl_caption_page['c_public']              = 'Públic';
$gl_caption_page['c_archived']            = 'Arxivat';
$gl_caption_page['c_status']              = 'Estat';
$gl_caption_page['c_page_id']             = 'Id';
$gl_caption_page['c_parent_id']           = 'Parent';
$gl_caption_page['c_level']               = 'Nivell';
$gl_caption_page['c_menu']                = 'Menú';
$gl_caption_page['c_blocked']             = 'Block.';
$gl_caption_page['c_filter_parent_id']    = 'Parent';
$gl_caption_page['c_filter_level']        = 'Level';
$gl_caption_page['c_has_content']         = 'Content';
$gl_caption_page['c_has_content_0']       = 'no';
$gl_caption_page['c_has_content_1']       = 'si';
$gl_caption_page['c_edit_content_button'] = 'Editar pàgina';
$gl_caption_page['c_edit_page_button']    = 'Editar dades';
$gl_caption_page['c_filter_menu2']        = 'Menú de navegació';

/*banners*/
$gl_caption_banner['c_banner_id']          = '';
$gl_caption_banner['c_ordre']              = 'Ordre';
$gl_caption_banner['c_url1']               = 'Enllaç';
$gl_caption_banner['c_url1_name']          = 'Text a l\'enllaç';
$gl_caption_banner['c_url1_target']        = 'Destí';
$gl_caption_banner['c_url1_target__blank'] = 'Una nova finestra';
$gl_caption_banner['c_url1_target__self']  = 'La mateixa finestra';
$gl_caption_banner['c_file']               = 'Imatge banner';
$gl_caption_banner['c_banner']             = 'Banner';
$gl_caption_banner['c_place_id']           = 'Secció';
$gl_caption_banner['c_banner_text']        = 'Text';
$gl_caption_banner['c_banner_text2']        = 'Text 2';
$gl_caption_banner['c_filter_place_id']    = 'Totes les seccions';
$gl_caption_banner['c_status']             = 'Estat';
$gl_caption_banner['c_filter_status']      = 'Tots els estats';
$gl_caption_banner['c_prepare']            = 'En preparació';
$gl_caption_banner['c_review']             = 'Per revisar';
$gl_caption_banner['c_public']             = 'Públic';
$gl_caption_banner['c_archived']           = 'Arxivat';


/* GUESTBOOK */
$gl_caption_guestbook['c_guestbook_id']='';
$gl_caption_guestbook['c_guest']='Name';
$gl_caption_guestbook['c_comment']='Comment';
$gl_caption_guestbook['c_status']='State';
$gl_caption_guestbook['c_prefered_language']='Language';
$gl_caption_guestbook['c_entered']='Date';
$gl_caption_guestbook['c_prepare'] = 'Preparing';
$gl_caption_guestbook['c_review'] = 'To review';
$gl_caption_guestbook['c_public'] = 'Public';
$gl_caption_guestbook['c_archived'] = 'Archived';

$gl_caption_guestbook['c_destacat']='Featured';
$gl_caption_guestbook['c_destacat_0']='Featured';
$gl_caption_guestbook['c_destacat_1']='Not featured';
$gl_caption_guestbook['c_filter_destacat'] = 'All, featured or not';

/* GALLERY */
$gl_caption_gallery['c_prepare'] = 'En preparació';
$gl_caption_gallery['c_review'] = 'Per revisar';
$gl_caption_gallery['c_public'] = 'Públic';
$gl_caption_gallery['c_archived'] = 'Archived';
$gl_caption_gallery['c_gallery'] = 'Gallery';
$gl_caption_gallery['c_subtitle'] = 'Subtitle';
$gl_caption_gallery['c_content'] = 'Content';
$gl_caption_gallery['c_entered'] = 'Entered date';
$gl_caption_gallery['c_modified'] = 'Modified date';
$gl_caption_gallery['c_status'] = 'Status';
$gl_caption_gallery['c_filter_status'] = 'Tots els estats';
$gl_caption_gallery['c_filter_in_home'] = 'Totes, home o no';
$gl_caption_gallery['c_filter_destacat'] = 'Totes, destacades o no';
$gl_caption_gallery['c_ordre'] = 'Order';

$gl_caption_gallery['c_in_home']='Surt a la Home';
$gl_caption_gallery['c_in_home_0']='No surt a la Home';
$gl_caption_gallery['c_in_home_1']='Surt a la Home';
$gl_caption_gallery['c_content_list']='Descripció curta al llistat';
$gl_caption_gallery['c_destacat']='Galeria destacada';
$gl_caption_gallery['c_destacat_0']='No destacada';
$gl_caption_gallery['c_destacat_1']='Destacada';

$gl_caption['c_gallery_file_name'] = $GLOBALS['gl_caption']['c_file_name'];
$gl_caption['c_gallery_old_file_name'] = $GLOBALS['gl_caption']['c_old_file_name'];

/* Utilitats */
$gl_caption_utility['c_group_1']                   = 'Valors predeterminats compra immoble';
$gl_caption_utility['c_group_2']                   = 'Valors predeterminats venda immoble';
$gl_caption_utility['c_group_3']                   = 'Valors predeterminats hipoteca';
$gl_caption_utility['c_agency_percent']            = 'Despeses gestió (%)';
$gl_caption_utility['c_efficiency']                = 'Certificat d\'eficiència energètica (€)';
$gl_caption_utility['c_estate_commission_percent'] = 'Comissió agència immobiliària (%)';
$gl_caption_utility['c_habitability_card']         = 'Cèdula d\'habitabilitat (€)';
$gl_caption_utility['c_mortgage_cancel_percent']   = 'Comissió de cancel·lació (%)';
$gl_caption_utility['c_mortgage_initial_interest'] = 'Interès predeterminat (%)';
$gl_caption_utility['c_mortgage_length']           = 'Durada hipoteca (anys)';
$gl_caption_utility['c_mortgage_notary_percent']   = 'Despeses notari (%)';
$gl_caption_utility['c_mortgage_open_percent']     = 'Obertura (%)';
$gl_caption_utility['c_mortgage_percent']          = 'Percentatge hipoteca sobre el preu (%)';
$gl_caption_utility['c_mortgage_register_percent'] = 'Registre hipoteca (%)';
$gl_caption_utility['c_mortgage_valuation']        = 'Taxació (€)';
$gl_caption_utility['c_notary_percent']            = 'Despeses notari (%)';
$gl_caption_utility['c_register_percent']          = 'Despeses registre (%)';

/* Places*/
$gl_caption_place['c_place'] = 'Office';
$gl_caption_place['c_status'] = 'Estado';
$gl_caption_place['c_prepare'] = 'En preparación';
$gl_caption_place['c_review'] = 'Para revisar';
$gl_caption_place['c_public'] = 'Pública';
$gl_caption_place['c_archived'] = 'Archivada';

$gl_caption_place['c_address'] = 'Address';
$gl_caption_place['c_adress'] = 'Street';
$gl_caption_place['c_numstreet'] = 'Number';
$gl_caption_place['c_block'] = 'Block';
$gl_caption_place['c_flat'] = 'Floor';
$gl_caption_place['c_door'] = 'Door';
$gl_caption_place['c_zip'] = 'Postal code';

$gl_caption_place['c_zone_id'] = 'Zone';
$gl_caption_place['c_provincia_id'] = 'Province';
$gl_caption_place['c_comarca_id'] = 'District';
$gl_caption_place['c_municipi_id'] = 'Municipality';
$gl_caption_place['c_country_id'] = 'Country';