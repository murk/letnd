<?
/**
 * ApiOneagencia
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2011
 * @version $Id$
 * @access public
 */
class ApiOneagencia extends Module{

	function __construct(){
		parent::__construct();
	}

	function list_files() {

		$agencia_id = Db::get_first( 'SELECT agencia_id FROM user__user WHERE user_id = ' . $_SESSION['user_id'] );

		$results = Db::get_rows( "SELECT agent_id, nom, cognoms FROM api__agent WHERE agencia_id = $agencia_id" );

		$module = Module::load( 'api', 'agent', $this );
		$agents = [];
		foreach ( $results as &$rs ) {

			$agent_id = $rs['agent_id'];

			$files = UploadFiles::gl_upload_get_record_files( $module, $agent_id );

			// Només deixo les que estan com a public
			foreach ( $files ['files_categorys'] as $category ) {

				$filecat_id = $category['filecat_id'];

				$files_cat = &$files ['files_cat_' . $filecat_id];

				foreach ( $files_cat as $key => $file) {
					if ($file['file_public'] == 0) unset ($files_cat[$key]);
				}
			}

			$rs += $files;
		}

			Debug::p ( $results, 'results' );

		$this->set_file( 'api/oneagencia_files_record.tpl' );
		$this->set_var( 'agents', $results );
		$this->set_vars( $this->caption );

		$content               = $this->process();
		$GLOBALS['gl_content'] = $content;

	}


	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function get_records()
	{	
		return $this->_execute_module(str_replace('list_','get_',$this->action));
	}
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{
		return $this->_execute_module(str_replace('show_','get_',$this->action));
	}

	function save_rows()
	{
		$this->_execute_module();
	}

	function write_record()
	{
		$this->_execute_module();
	}
	function show_form_map()
	{	
		$this->_execute_module();
	}
	function save_record_map()
	{	
		$this->_execute_module();
	}
	function manage_images()
	{
		$this->_execute_module();
	}
	function _execute_module($action=''){
		// $this->name = 'instalacio'; // si vull canviar el nom del parent		
		$module = Module::load('api',$this->process, $this);
		$action = $action?$action:$this->action;
		return $module->do_action($action);	
	}
}
?>