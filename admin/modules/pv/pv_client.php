<?

/**
 * PvClient
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2011
 * @version $Id$
 * @access public
 *
 * TODO-i Al borrar client, borrar tots els relacionats i arxius
 *
 * DELETE FROM pv__seguimentitem_evidencia WHERE seguimentitem_id NOT IN ( SELECT seguimentitem_id FROM pv__seguimentitem)
 *
 */
class PvClient extends Module{
	
	var $group_id, $user_is_admin, $user_is_client, $client_id, $select_client_url;

	function __construct(){
		parent::__construct();
		
		$this->group_id = $_SESSION['group_id'];
		
		$this->user_is_admin = $this->group_id < 3; // grups 2 i 1
		// els usuaris es el grup 3
		$this->user_is_client = $this->group_id == 4; // grups 4
		
		if ($this->user_is_client){
			$this->client_id = Db::get_first('SELECT client_id FROM user__user WHERE user_id = ' . $_SESSION['user_id']);			
		}

			
		// un agent no pot entrar aquí
		if ($this->user_is_client) return;
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function get_records()
	{
	    $listing = new ListRecords($this);
		
		$listing->add_swap_edit();

		if (!$listing->is_editable) {
			$listing->set_field( 'can_edit_seguiment', 'type', 'select' );
			$listing->set_field( 'has_seguiment', 'type', 'select' );
			$listing->set_field( 'has_adr', 'type', 'select' );
			$listing->set_field( 'has_gmao', 'type', 'select' );
		}
		
		$listing->call('list_records_walk','',true);
		
		
		if ($this->user_is_admin){
			$listing->add_button ('add_new_user', '', 'boto2','before','add_new_user');
			$listing->add_field('user_ids');
			$listing->set_field('user_ids','type','none');
			$listing->set_field('user_ids','list_admin','out');
		}
		
		
		$listing->order_by = "client ASC";
		
	   
	    return $listing->list_records();
	}
	function list_records_walk($listing){
		$client_id = $listing->rs['client_id'];		
		
		$ret['user_ids'] = '';
		
			$query = "SELECT user_id, name, surname, login FROM user__user WHERE client_id = " . $listing->rs['client_id'] . ' ORDER BY name ASC, surname ASC';
			$results = Db::get_rows($query);
			
			
			foreach ($results as $rs){
				
				$ret['user_ids'] .= 
					'<p'.$style.'><strong>' . $rs['name'] . ' ' . $rs['surname'] . '</strong></p>
					<p style="margin:2px 0 12px;">
					<a href="javascript:edit_user('.$client_id.','.$rs['user_id'].')">'.$this->caption['c_edit'].'</a>
					</p>';
			}
		
		// boto users
		//$listing->buttons['view_users']['params'] = '&user_ids=' . $listing->rs['user_ids'];
		
		// agefir usuaris i agents desde el formulari de l'agencia
		if ($listing->list_show_type == 'form') {
			$ret['user_ids'] .= '<a class="boto1" style="margin:10px 0 20px;"  href="javascript:add_new_user('.$client_id.')">'.$this->caption['c_add_new_user'].'</a>';
		}
		
		return $ret;
	}


	function select_client()
	{
	    $listing = new ListRecords($this);

		$listing->set_options( 0, 0, 0, 0, 0, 0, 0, 0 );
		$listing->order_by = "client ASC";
		$listing->unset_field( 'client_id2' );
		$listing->unset_field( 'can_edit_seguiment' );
		$listing->unset_field( 'has_seguiment' );
		$listing->unset_field( 'has_adr' );
		$listing->unset_field( 'has_gmao' );

		$listing->call('select_client_list_records_walk','',true);

		$this->select_client_url = $_SERVER["REQUEST_URI"];

		if (strpos($this->select_client_url,'?') === false)
			$this->select_client_url .= '?';
		else
			$this->select_client_url .= '&';


	    return '<div class="titol">' . $this->caption['c_titol_canviar_client'] . "</div>" . $listing->list_records();
	}
	function select_client_list_records_walk($module){
		$rs = &$module->rs;
		$ret = array();

		$ret['tr_jscript'] ='onclick="window.location=\'' . $this->select_client_url . 'client_id='. $rs['client_id'].'\'"';
		$ret['tr_class'] = 'pointer';

		return $ret;
	}

	
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{
	    $show = new ShowForm($this);
	    return $show->show_form();
	}

	function save_rows()
	{
	    $save_rows = new SaveRows($this);
	    $save_rows->save();
		
		$query = false;
		
		if ($save_rows->id) {
			// al enviar a la paperera, enviar grup a la paperera
			if ($this->action == 'save_rows_bin_selected' ){		
				$ids = implode (',', $save_rows->id);
				$query = "UPDATE `user__user` SET `bin`=1 WHERE  `client_id` IN (" .  $ids . ")";
			}
			
			// al recuperar, recuperar grup		
			if ($this->action == 'save_rows_restore_selected'){
				$ids = implode (',', $save_rows->id);
				$query = "UPDATE `user__user` SET `bin`=0 WHERE  `client_id` IN (" .  $ids . ")";
			}
			
			// al eliminar, eliminar grup de la paperera
			if ($this->action == 'save_rows_delete_selected'){
				$ids = implode (',', $save_rows->id);
				$query = "DELETE FROM `user__user` WHERE  `client_id` IN (" .  $ids . ")";	
			}
			
			if ($query) {
				Db::execute($query);
				debug::add('query grup',$query);
			}
		}
	}

	function write_record()
	{		
		
	    $writerec = new SaveRows($this);		
	    $writerec->save();
		
		// al enviar a la paperera, enviar usuaris a la paperera
		if ($this->action == 'bin_record') {
			$query = "UPDATE `user__user` SET `bin`=1 WHERE  `client_id`=" .  $writerec->id;
			Db::execute($query);
			debug::add('Usuari a la paperera',$query);
		}
		// al eliminar, eliminar usuaris
		if ($this->action == 'delete_record') {
			$query = "DELETE FROM `user__user` WHERE  `client_id`=" .  $writerec->id;
			Db::execute($query);
			debug::add('Usuari eliminat',$query);
		}		
	}

	function manage_images()
	{
	    $image_manager = new ImageManager($this);
	    $image_manager->execute();
	}
	
	public static function user_user_on_end($action,$module){
		/*Debug::p($_GET);
		Debug::p($_POST);
		Debug::p($action);
		Debug::p($module);*/
		
		if ($action=='add_record' && $GLOBALS['gl_saved']  && isset($_GET['client_id'])){
		
			$client_id = $_GET['client_id'];
			$user_id = $GLOBALS['gl_insert_id'];			
			
			$query = "UPDATE `user__user` SET group_id=4,`client_id`=" . $client_id . " WHERE user_id = " . $user_id;			
			Debug::p($query, 'Update user');
			Db::execute($query);
			
			print_javascript('top.window.location.reload();');
			Debug::p_all();
			die();
			 
			/*$href = $_SESSION['last_list']['2303']['link'];
			if ($href) print_javascript('parent.window.location = "' . $href . '";');*/
		}
		// m'asseguro que el client no es pugui canviar de grup
		elseif ( $action=='save_record'){
			//
			//
			//
			//
		}
		if (!$GLOBALS['gl_saved']){
				$js = '
			if (typeof parent.hidePopWin == "function") parent.window.setTimeout("hidePopWin(false);", 800);
			if (typeof parent.showPopWin == "function") parent.showPopWin("", 300, 100, null, false, false, "'.addslashes($GLOBALS['gl_message']).'");';

			print_javascript($js);
			Debug::p_all();
			die();
		}
	}
}
?>