<?
/**
 * ClinkNew
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2010
 * @version $Id$
 * @access public
 */
class ClinkNew extends Module{
	var $form_template = '';
	var $list_template = '';
	function __construct(){
		parent::__construct();
		$this->parent = 'ClinkNew';
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	/* funció que es crida per fer una acció del modul automatica o desde un altre modul ( list_records, show_form ) etc...*/
	function do_action($action = ''){
		switch ($this->parent){		
			case 'ClinkNew':	
				//$this->form_template = 'clink/new_form';
				//$this->list_template = 'clink/new_list';
			break;
			case 'ClinkMedio':	
			break;	
			case 'ClinkEncuentro':	
			break;		
			case 'ClinkCurrículum':	
			break;
		}
		$this->set_var('parent',$this->parent);
		return parent::do_action($action);
	}
	function get_records($ret_listing = false)
	{	    
		$this->set_news_fields();
		$listing = new ListRecords($this);
		$listing->template = $this->list_template;
		$listing->condition = "parent_module = '" . $this->parent . "'";
		$listing->condition .= " AND status = 'public'";
		if (isset($_GET['category_id'])) $listing->condition .= " AND category_id = '" . R::id('category_id') . "'";
		$listing->call('records_walk','url1,url1_name,url2,url2_name,content,content_list,entered');
		$listing->order_by = 'ordre ASC, entered DESC';
		if ($ret_listing)
			return $listing;
		else
			return $listing->list_records();
	}
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{
	    $this->set_news_fields();
		$show = new ShowForm($this);
		$show->template = $this->form_template;
		
		// si no hi ha id, agafo per defecte la primera noticia de la categoria, i si no hi ha categoria, de la primera categoria
		if (!$show->id){			
			$query = "SELECT clink__new.new_id as new_id
				FROM clink__new_language, clink__new
				WHERE clink__new_language.new_id = clink__new.new_id
				AND status = 'public'
				AND bin <> 1
				AND parent_module = '" . $this->parent . "'
				AND language = '" . LANGUAGE . "'
				ORDER BY entered DESC, new_id
				LIMIT 0,1";
			$show->id = Db::get_first($query);
		}
		$show->condition = "status = 'public'";
		$show->call('records_walk','url1,url1_name,url2,url2_name');
		$show->get_values();		
		if ($show->has_results)
		{
			$GLOBALS['gl_page']->page_title = $show->rs['new'];
			$GLOBALS['gl_page']->page_description = add_dots_meta_description($show->rs['content']);
			return $show->show_form();
		}
		else
		{
			$this->do_action('list_records');
		}
	}
	function records_walk($url1,$url1_name,$url2,$url2_name,$content=false,$content_list=false,$entered=false){		
		// Això ho poso aquí així no cal posar-ho al tpl, simplement posar: <a href="<=$url1>" target="_blank"><=$url1_name></a>
		$ret['url1_name'] = $url1_name?$url1_name:$url1;
		$ret['url2_name'] = $url2_name?$url2_name:$url2;
		// si es llistat passa lavariable content
		if ($content!==false){
			$ret['content']=$content_list?$content_list:add_dots('content', $content);
			$ret['entered_unformated'] = $entered;
		}
			
		//$ret['url1']=str_replace('http://','',$url1);	
		//$ret['url1']=str_replace('http://','',$url1);
		return $ret;
	}
	function set_news_fields(){	
		global $gl_config;
		if (!$gl_config['show_entered_time'])
		{
			$this->set_field('entered', 'type', 'date');
		}
	}
	function rss(){
	
	$this->action = 'get_records';
	$listing = $this->get_records(true);
	$listing->set_records();
	$rss_loop = &$listing->loop;
	
	
	
	$rss_items = '';
	$rss_host_url = substr(HOST_URL,0,-1);
	$rss_publish_date = date("r",strtotime ($rss_loop[0]['entered_unformated']));
	
	foreach ($rss_loop as $l){
		extract($l);
		$rss_items .='
			<item>
				<title>'.strip_tags($new).'</title>
				<link>'.$rss_host_url . $show_record_link.'</link>
				<pubDate>'.date("r",strtotime ($entered_unformated)).'</pubDate>
				<guid isPermaLink="true">'.$rss_host_url . $show_record_link.'</guid>
				<description><![CDATA['. $content;
		if ($image_src_details)
		$rss_items .='<p><img src=\''. $rss_host_url . $image_src_thumb .'\'/></p>';
		$rss_items .=']]></description>
			</item>';
	}
	$rss_content = '<?xml version="1.0" encoding="ISO-8859-1"?>
	<rss version="2.0"
		xmlns:atom="http://www.w3.org/2005/Atom"
		xmlns:sy="http://purl.org/rss/1.0/modules/syndication/"
		>

	<channel>
		<title>'.$GLOBALS['gl_page']->page_title.'</title>
		<atom:link href="'. $rss_host_url . $_SERVER['REQUEST_URI'] .'" rel="self" type="application/rss+xml" />
		<link>'. $rss_host_url . substr($_SERVER['REQUEST_URI'],0,-4) .'</link>
		<description>'.$GLOBALS['gl_page']->page_description.'</description>';
		
	if (is_file(CLIENT_PATH .'/images/logo_rss.png')){
	$rss_content .= '
		<image>
			<url>'. $rss_host_url . '/' . CLIENT_DIR .'/images/logo_rss.png</url>
			<title>'.$GLOBALS['gl_page']->page_title.'</title>

			<link>'. $rss_host_url . substr($_SERVER['REQUEST_URI'],0,-4) .'</link>
		</image>';
	}
	$rss_content .= '
		<lastBuildDate>'.$rss_publish_date.'</lastBuildDate>
		<language>'.LANGUAGE_CODE.'</language>
		<sy:updatePeriod>weekly</sy:updatePeriod>
		<sy:updateFrequency>1</sy:updateFrequency>
		<generator>http://www.letnd.com '.($GLOBALS['gl_version']/1000).'</generator>'.$rss_items.'
		</channel>
	</rss>	
	';
	header("Content-type: application/xml");	
	echo $rss_content;
	Debug::p_all();
	die();
	}
}
?>