<h6><?= $c_products ?></h6>
<ul>
	<? foreach($loop as $l): extract( $l ) ?>
		<li class="active<?= $selected ?>">
			<a href="<?= $link ?>" title="<?= $family_alt ?>">
				<?= $item ?>
			</a>
			<? if($loop): ?>
				<ul class="submenu">
					<? foreach($loop as $l2): extract( $l2 ) ?>
						<li class="active<?= $selected ?>">
							<a href="<?= $link ?>" title="<?= htmlspecialchars($family_alt) ?>"><?= $item ?></a></li>
					<? endforeach // loop ?>
				</ul>
			<? endif //loop ?>
		</li>
	<? endforeach // $loop ?>
</ul>