<?
define("MENU_HOME_PUBLIC","web pública");
define("MENU_HOME","página inicial");
define("MENU_LOGOUT","desconectar");


// configurable
define("USER_TEXT","usuario");
define('USER_LOGIN_INVALID','El nombre de usuario o password no son válidos');
define('USER_LOGIN_VALID','Bienvenido a ' . HOST_URL);

// la linea de sota no es pot tocar, que te dit que no la toquis!!!!!!!
// menus
define('EXPERIENCIA_MENU_ROOT','Experiencia');
define('NEWSLETTER_MENU_ROOT','Newsletter');
define('TRANSLATION_MENU_ROOT','Traducciones');
define('FACTURACIO_MENU_ROOT','Facturación');
define('STATISTICS_MENU_ROOT','Estadísticas');
define('USER_MENU_ROOT','Usuarios');
define('ADMINTOTAL_MENU_ROOT','Admin total');
define('WEB_MENU_ROOT','Gestión');
define('INMO_MENU_ROOT','Propiedades');
define('OWNER_MENU_ROOT','Propietarios');
define('REPORT_MENU_ROOT','Documentos');
define('CONFIG_MENU_ROOT','Configuración');
define('NEWS_MENU_ROOT','Noticias');
define('GALERY_MENU_ROOT','Galería imágenes');
define('RENOVATS_MENU_ROOT','Postales regalo');
define('SHOPWINDOW_MENU_ROOT','Escaparates');
define('UTILITY_MENU_ROOT','Utilidades');
define('MLS_MENU_ROOT','MLS');
define('CUSTUMER_MENU_ROOT','Clientes');
define('WORKS_MENU_ROOT','Trabajos');
define('PRODUCT_MENU_ROOT','Productos');
define('RATE_MENU_ROOT','Tarifas');
define('CARS_MENU_ROOT','Coxes');
define('NEW_MENU_ROOT','Noticias');
define('PORTFOLI_MENU_ROOT','Portfolio');
define('CONTACT_MENU_ROOT','Contacto');
define('PRINT_MENU_ROOT','Documentos');
define('BOOKING_MENU_ROOT','Reservas');
define('MISSATGE_MENU_ROOT','Mensajes');
define('CUSTOMER_MENU_ROOT','Clientes y pedidos');
define('DIRECTORY_MENU_ROOT','Archivos');
define('SELLPOINT_MENU_ROOT','Puntos de venta');



define('DB_SPLIT_TITLE_FIRST_PAGE', 'Inicio');
define('DB_SPLIT_TITLE_PREVIOUS_PAGE', 'Página anterior');
define('DB_SPLIT_TITLE_NEXT_PAGE', 'Página siguiente');
define('DB_SPLIT_TITLE_LAST_PAGE', 'Final');
define('DB_SPLIT_TITLE_PAGE_NO', 'Página %d');
define('DB_SPLIT_TITLE_PREV_SET_OF_NO_PAGE', 'Anteriores %d páginas');
define('DB_SPLIT_TITLE_NEXT_SET_OF_NO_PAGE', 'Siguientes %d páginas');
define('DB_SPLIT_BUTTON_FIRST', '&lt;&lt;PRINCIPIO');
define('DB_SPLIT_BUTTON_PREV', '&lt;&lt;&nbsp;Anterior');
define('DB_SPLIT_BUTTON_NEXT', 'Siguiente&nbsp;&gt;&gt;');
define('DB_SPLIT_BUTTON_LAST', 'FINAL&gt;&gt;');
define('DB_SPLIT_DISPLAY_NUMBER_OF_RESULTS', 'Mostrando <b>%d</b> a <b>%d</b> (de <b>%d</b>)');
define ('TITLE_SEARCH','Resultados de la búsqueda');

define('DB_SPLIT_PAGE_TITLE', 'Página');
define('DB_SPLIT_PAGE_DESCRIPTION', 'Resultados %d a %d de %d');

$month_names = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
$week_names = array("Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado");


// Caption default
$gl_caption['c_form_title'] = 'Editar datos';
$gl_caption['c_print_title'] = 'Datos';
$gl_caption['c_config_title'] = 'Editar configuración';
$gl_caption['c_edit'] = 'Editar';


$gl_caption['c_edit_button'] = 'Editar datos';
$gl_caption['c_view_button'] = 'Ver datos';
$gl_caption['c_edit_images_button'] = 'Editar imágenes';
$gl_caption['c_add_images_button'] = 'Añadir imágenes';



$gl_caption['c_edit_data'] = 'Editar datos';
$gl_caption['c_edit_images'] = 'Editar imágenes';
$gl_caption['c_add_images'] = 'Añadir imágenes';
$gl_caption['c_search_form'] = 'Buscar';
$gl_caption['c_send_new'] = 'Guardar';
$gl_caption['c_send_edit'] = 'Guardar cambios';
$gl_caption['c_delete'] = 'Eliminar';
$gl_caption['c_action'] = 'Acción';
$gl_caption['c_delete_selected'] = 'Borrar seleccionados';
$gl_caption['c_restore_selected'] = 'Recuperar seleccionados';
$gl_caption['c_confirm_deleted'] = 'Los elementos seleccionados se eliminarán definitivamente de la base de datos \\n\\n¿Desea continuar?';
$gl_caption['c_confirm_images_deleted'] = 'Las imágenes seleccionadas se eliminarán definitivamente \\n\\n¿Desea continuar?';
$gl_caption['c_confirm_form_bin'] = 'El registro se enviará a la papelera de reciclaje \\n\\n¿Desea continuar?';
$gl_caption['c_confirm_form_deleted'] = 'El registro se eliminará definitivamente de la base de datos \\n\\n¿Desea continuar?';
$gl_caption['c_move_to'] = 'Mover a:';
$gl_caption['c_print_all'] = 'Imprimir todo';
$gl_caption['c_print'] = 'Imprimir';
$gl_caption['c_order_by'] = 'Ordenar por:';
$gl_caption['c_select'] = 'Seleccionar';
$gl_caption['c_multislect_selectable'] = 'Campos';
$gl_caption['c_multislect_selected'] = 'Campos seleccionados';
$gl_caption['c_all'] = 'todo';
$gl_caption['c_nothing'] = 'nada';
$gl_caption['c_preview'] = 'Previsualizar';
$gl_caption['c_yes'] = 'Sí';
$gl_caption['c_no'] = 'No';
$gl_caption['c_options_bar'] = 'Opciones';
$gl_caption['c_file_maximum'] = 'máximo %s archivos a la vez';
$gl_caption['c_file_maximum_1'] = 'máximo 1 archivo a la vez';

// buscadors
$gl_caption['c_search_title'] = 'Buscar';
$gl_caption['c_by_ref'] = '(o referencias separadas por espacios, como por ej: "456 245 45")';
$gl_caption['c_by_words'] = 'Por palabras';

// sobreescrits caption images
$gl_caption_image['c_edit'] = 'Imágenes de';
$gl_caption_image['c_insert'] = 'Añadir imágenes desde tu ordenador';
$gl_caption_image['c_insert_advanced'] = 'Avanzado (java)';
$gl_caption_image['c_delete_selected'] = 'Eliminar imágenes seleccionadas';
$gl_caption_image['c_confirm_deleted'] = 'Las imágenes se eliminarán definitivamente \\n\\n¿Desea continuar?';
$gl_caption_image['c_select_all'] = 'Seleccionar todas';
$gl_caption_image['c_deselect_all'] = 'Deseleccionar todas';

// nous images
$gl_caption_image['c_stop'] = 'Parar';
$gl_caption_image['c_refresh'] = 'Actualizar';
$gl_caption_image['c_add'] = 'Añadir imágenes';
$gl_caption_image['c_thumbnails'] = 'Imágenes';
$gl_caption_image['c_icons'] = 'Iconos';
$gl_caption_image['c_list'] = 'Lista';
$gl_caption_image['c_details'] = 'Detalles';
$gl_caption_image['c_progress_dialog_title_text'] = 'Añadiendo imágenes';

// camps images
$gl_caption_image['c_ordre'] = 'orden';
$gl_caption_image['c_main_image'] = 'principal';
$gl_caption_image['c_name'] = 'Nombre';
$gl_caption_image['c_imagecat_id'] = 'Categoría';
$gl_caption_image['c_image_alt'] = 'Texto alternativo (alt)';
$gl_caption_image['c_image_title'] = 'Título';
$gl_caption_image['c_edit_titles_images'] = 'Editar títulos imágenes';
$gl_caption_image['c_hide_titles_images'] = 'Esconder títulos imágenes';

// sobreescrits caption traduccions i traduccions_news.tpl
$gl_caption_translation['c_send_edit'] = 'Enviar a traducir';
$gl_caption_translation['c_page_words_title'] = 'Total de palabras para traducir en esta página';
$gl_caption_translation['c_all_page_words_title'] = 'Total de palabras para traducir en todas las páginas';
$gl_caption_translation['c_total'] = 'Total';

// sobreescrits end


// Message default
$gl_messages['title_edit']='Editar';
$gl_messages['title_copy']='Copiar datos';
$gl_messages['added']='El registro se ha añadido correctamente.';
$gl_messages['add_exists']='Ya existe un registro con este/a %1$s: %2$s';
$gl_messages['saved']='El registro se ha modificado correctamente';
$gl_messages['not_saved']='El registro no se ha modificado';
$gl_messages['no_records']='No hay ningún registro en esta lista';
$gl_messages['no_records_bin']='No hay ningún registro en la papelera';
$gl_messages['list_saved']='Los cambios en los registros se han efectuado correctamente';
$gl_messages['list_not_saved']='No se ha modificado ningún registro ni detectado ningún cambio';
$gl_messages['list_deleted']='Los registros se han eliminado';
$gl_messages['list_bined']='Los registros se han enviado a la papelera de reciclaje';
$gl_messages['list_restored']='Los registros se han recuperado correctamente';
$gl_messages['concession_no_acces']='No tiene permisos para esta sección';
$gl_messages['concession_no_write']='No tiene permisos de escritura para esta sección';
$gl_messages['select_item']='Haga clic en un registro de la lista para seleccionar';
$gl_messages['related_not_deleted']='Los registros marcados no se han podido borrar, ya que tienen registros relacionados';


// sobreescrits message image
$gl_messages_image['list_saved']='Los cambios en las imágenes se han efectuado correctamente.';
$gl_messages_image['list_not_saved']='No se ha modificado ningún registro ni detectado ningún cambio';
$gl_messages_image['list_deleted']='Las imágenes se han eliminado';
// sobreescrits end



$gl_messages['deleted']='El registro ha sido borrado';
$gl_messages['bined']='El registro  se ha enviado a la papelera de reciclaje';
$gl_messages['restored']='El registre se ha recuperado correctamente';
$gl_caption['c_ok'] = 'Aceptar';
$gl_caption['c_cancel'] = 'Cancelar';
$gl_caption['c_close'] = 'Cerrar';
$gl_messages['post_max_size']='El peso del formulario (contenido + archivos adjuntos) excede el límite de ';
$gl_messages['post_file_max_size']='El peso de los archvios adjuntos excede el límit de ';
$gl_messages['assigned']='Registros seleccionados';
$gl_caption['c_black']='Negro';
$gl_caption['c_white']='Blanco';
$gl_caption['c_silver']='Plata';
$gl_caption['c_grey']='Gis';
$gl_caption['c_blue']='Azul';
$gl_caption['c_red']='Rojo';
$gl_caption['c_orange']='Naranja';
$gl_caption['c_green']='Verde';
$gl_caption['c_purple']='Lila';
$gl_caption['c_pink']='Rosa';
$gl_caption['c_golden']='Dorado';
$gl_caption['c_yellow']='Amarillo';
$gl_caption['c_maroon']='Grana';
$gl_caption['c_brown']='Marrón';
// NO TRADUIT
//creo un array global per els mesos
$gl_caption_months = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");

$gl_caption ['c_2']='dos';
$gl_caption ['c_3']='tres';
$gl_caption ['c_4']='quatre';
$gl_caption ['c_5']='cinc';
$gl_caption ['c_6']='sis';
$gl_caption ['c_7']='set';
$gl_caption ['c_8']='vuit';
$gl_caption ['c_9']='nou';
$gl_caption['c_10']='deu';
$gl_caption['c_11']='onze';
$gl_caption['c_12']='dotze';
$gl_caption['c_13']='tretze';
$gl_caption['c_14']='catorze';
$gl_caption['c_15']='quinze';
$gl_caption['c_16']='setze';
$gl_caption['c_17']='diset';
$gl_caption['c_18']='divuit';
$gl_caption['c_19']='dinou';
$gl_caption['c_20']='vint';
$gl_caption['c_30']='trenta';
$gl_caption['c_40']='quaranta';
$gl_caption['c_50']='cinquanta';
$gl_caption['c_60']='seixanta';
$gl_caption['c_70']='setanta';
$gl_caption['c_80']='vuitanta';
$gl_caption['c_90']='noaranta';
$gl_caption['c_1000']='mil';


$gl_caption['c_add_button'] = 'Afegir registre';

$gl_caption['c_login'] = 'Login';
$gl_caption['c_password'] = 'Contraseña';
$gl_caption['c_mail'] = 'E-mail';
$gl_caption['c_mail_repeat'] = 'Repetir E-mail';
$gl_caption['c_password_repeat'] = 'Repetir contraseña';
$gl_caption['c_noscript'] = '<p><strong>Su navegador no soporta o tiene desactivado Javascript. </strong></p><p>Debe <a href="//www.google.com/support/websearch/bin/answer.py?answer=23852">habilitar Javascript</a> para poder utilizar Letnd</p>';
$gl_caption['c_cookie'] = '<p><strong>Su navegador no soporta o tiene desactivadas las Cookies. </strong></p><p>Debe <a href="//www.google.com/support/websearch/bin/answer.py?answer=35851">habilitar las Cookies</a> para poder utilizar Letnd</p>';

$gl_messages_image['images_uploaded']='Imagenes guardadas';
$gl_messages['form_sent']='El formulario se ha enviado correctamente';
$gl_messages['form_not_sent']='El formulario no se ha podido enviar';

$gl_caption['c_search_adress'] = 'Buscar dirección';
$gl_caption['c_map_frame_1'] = 'Se ha encontrado coincidencia para';
$gl_caption['c_map_frame_2'] = 'Si la ubicación és correcta';
$gl_caption['c_map_frame_3'] = 'clica aquí para guardar';
$gl_caption['c_map_frame_4'] = 'No se ha encontrado ninguna coincidencia';

$gl_caption['c_list_view_1'] = 'Normal';
$gl_caption['c_list_view_2'] = 'Iconos grandes';
$gl_caption['c_list_view_3'] = 'Lista';

$gl_caption['c_edit_list_button_1'] = 'No editar listado';
$gl_caption['c_edit_list_button_'] = 'Editar listado';
$gl_caption['c_add_dots'] = 'Recortar texto';

// SEO
$gl_caption['c_seo']='Herramientas SEO ( Posicionamiento web )';
$gl_caption['c_page_title']='Titol de la pàgina. max. 66-69 caràcters. Òptim < 65 car:';
$gl_caption['c_page_description']='Descripción de la página. max. 156-158 carácteres:';
$gl_caption['c_page_keywords']='Palabras clave:';
$gl_caption['c_file_name']='Nombre del la página en URL. Ex. www.miweb.com/<b>nombre-de-la-pagina</b>.htm max. 90 carácteres:';
$gl_caption['c_file_name_excel']='Nombre página';
$gl_caption['c_old_file_name']='Nombre antiguo de la página en URL';

// FI NO TRADUIT



// SOCIAL
$gl_caption['c_edit_social'] = 'Redes sociales';
$gl_caption['c_edit_social_title'] = 'Publicar';
$gl_caption['c_social_language'] = 'Idioma';
$gl_caption['c_facebook_message'] = 'Mensaje';
$gl_caption['c_facebook_picture'] = 'Imagen';
$gl_caption['c_facebook_link'] = 'Enlace';
$gl_caption['c_facebook_link_explanation'] = 'Coge los textos directamente de la página';
$gl_caption['c_facebook_name'] = 'Descripción 1<sup>a</sup> línea';
$gl_caption['c_facebook_caption'] = 'Descripción 2<sup>a</sup> línea';
$gl_caption['c_facebook_image_title'] = 'Imagen';
$gl_caption['c_facebook_description'] = 'Descripción';
$gl_caption['c_facebook_save'] = 'Publicar en facebook';
$gl_caption['c_facebook_publish_as'] = 'Publicar como';
$gl_caption['c_facebook_publish_as_image'] = 'Imagen';
$gl_caption['c_facebook_publish_as_link'] = 'Enlace';
$gl_caption['c_facebook_publish_save_default']='Predeterminar esta opción';
$gl_caption['c_facebook_publish_user']='Muro propio';
$gl_caption['c_facebook_publish_pagina']='Página';
$gl_caption['c_facebook_publish_id']='Publicar en';
$gl_caption['c_facebook_login']='Iniciar sessión en Facebook';
$gl_caption['c_facebook_delete_text']='<p>Los mensajes publicados en Facebook sólo se pueden modificar desde Facebook.</p><p>Si quieres volver a publicar desde Letnd primero se tiene que borrar la publicación anterior</p>';
$gl_caption['c_facebook_delete']='Borrar publicación en Facebook';
$gl_caption['c_facebook_logout']='Logout';
$gl_caption['c_facebook_cannot_delete']='No se puede borrar la publicación ya que se creó con otro usuario de Facebook correspondiente a';
$gl_caption['c_facebook_activate']='El precio de activación de este servicio es de 60 € para poder dar de alta en Facebook';

$gl_caption['c_twitter_message'] = 'Mensaje';
$gl_caption['c_twitter_save'] = 'Publicar en twitter';
$gl_caption['c_twitter_login']='Iniciar sessión en Twitter';
$gl_caption['c_twitter_delete_text']='<p>Los mensajes publicados en Twitter sólo se pueden modificar desde Twitter.</p><p>Si quieres volver a publicar desde Letnd primero se tiene que borrar la publicación anterior</p>';
$gl_caption['c_twitter_delete']='Borrar publicación en Twitter';
$gl_caption['c_twitter_cannot_delete']='No se puede borrar la publicación ya que se creó con otro usuario de Twitter correspondiente a';

$gl_caption['c_button_form_twitter_published']=
$gl_caption['c_button_form_facebook_published']=
$gl_caption['c_button_form_twitter']=
$gl_caption['c_button_form_facebook']=	'';
$gl_caption['c_chars']=	'Caràcters';

$gl_messages['social_facebook_no_user']='No estás logueado, por favor vuelve a probarlo';
$gl_messages['social_facebook_no_page']='No hay ninguna página con este id';
$gl_messages['social_facebook_published']='El mensaje se ha publicado correctamente en Facebook';
$gl_messages['social_facebook_not_init']='No se ha podido conectar a Facebook';
$gl_messages['social_facebook_error']='Ha habido un error en Facebook: ';
$gl_messages['social_facebook_deleted']='El mensaje se ha borrado de Facebook';
$gl_messages['social_facebook_not_deleted']='No hay ningún mensaje para borrar';

$gl_messages['social_twitter_empty']='El mensaje está vacio, no se puede publicar en Twitter';
$gl_messages['social_twitter_no_user']='No estás logueado, por favor vuelve a probarlo';
$gl_messages['social_twitter_published']='El mensaje se ha publicado correctamente en Twitter';
$gl_messages['social_twitter_not_init']='No se ha podido conectar a Twitter';
$gl_messages['social_twitter_error']='Ha habido un error en Twitter: ';
$gl_messages['social_twitter_deleted']='El mensaje se ha borrado de Twitter';
$gl_messages['social_twitter_not_deleted']='No hay ningún mensaje para borrar';

// FI NO TRADUIT


$gl_caption['c_file_manager']=	'Gestión de archivos';
$gl_messages['no_records_search']='No se ha encontrado ninguna coincidencia para esta búsqueda';
$gl_caption['c_back'] = 'Volver';

$gl_caption['c_asc'] = 'asc';
$gl_caption['c_desc'] = 'desc';

$gl_caption['c_reply'] = 'Responder';

$gl_caption['c_file_file']='Archivo';
$gl_caption['c_file_title']='Título';
$gl_caption['c_file_show_home']='Sale en el listado';
$gl_caption['c_file_ordre']='Orden';
$gl_caption['c_file_public']='Público';
$gl_caption['c_file_filecat_id']='Cat.';

$gl_caption['c_export_button']= 'Exportar';

$gl_caption['c_entered'] = 'Creado';
$gl_caption['c_modified'] = 'Modificado';