<?
list( $variations_with_images, $variations_without_images, $variations_images )=ProductVariationImages::separate_image_variations( $variations, $images );
?>
<article>

	<? /* Botons */ ?>
	<div class="show-buttons">
		<? $back_link = $back_button ? $back_button : get_page_link( 'product' ); ?>
		<a class="back-button" href="<?= $back_link ?>"><?= $c_back ?></a>

		<? if ( $show_previous_link || $show_next_link ): ?>
			<? if ( $show_previous_link ): ?>
				<a class="previous-link" href="<?= $show_previous_link ?>"></a>
			<? endif ?>
			<? if ( $show_next_link ): ?>
				<a class="next-link" href="<?= $show_next_link ?>"></a>
			<? endif ?>
		<? endif ?>
	</div>
	<? /* Fi Botons */ ?>

	<div class="left">
		<? /*
		Si no va al titol general posar el titol aquí
		
		<?if ($product_title):?>
		<h1><?=$product_title?></h1>
		<?endif?>
		
		*/ ?>

		<? if ( $product_subtitle ): ?>
			<h2><?= $product_subtitle ?></h2>
		<? endif ?>

		<? /* REF */ ?>
		<div class="ref"><?= $c_ref ?>. <?= $ref ?></div>

		<? /* MARCA */ ?>
		<? if ( $brand_image_src_thumb ): ?>
			<div class="brand"><img src="<?= $brand_image_src_thumb ?>" width="70"/></div>
		<? endif //brand_image_thumb?>


		<? /* VARIACIONS */ ?>
		<? /*
            TODO-i  Per variacions sense imatges, pot ser 'radios', 'select' o 'divs'
            UPDATE `product__configpublic` SET `value`='radios' WHERE  `name` = 'variations_type';
        */ ?>

		<? if ( $variations ): ?>
			<div class="variations-holder">

				<? foreach ( $variations as $l ): extract( $l ) ?>

					<? if ( $variation_category ): ?><p><?= $variation_category ?>:</p><? endif //variation_category?>
					<?= $variations ?>

				<? endforeach //$loop?>
			</div>
		<? endif //variation?>
		<? /* TODO-i  FI Per variacions sense imatges */ ?>

		<? /*
            TODO-i  Per variacions amb imatges

            Es poden combinar les variacions amb imatge o sense per separa amb les variables $variations_with_images o $variations_without_images enlloc de $variations
        */ ?>
		<? if( $variations ): ?>
			<? foreach( $variations as $l ): extract( $l ) ?>
				<div class="variations-holder">
					<? if( $variation_category ): ?>
						<p>
							<?= $variation_category ?>
						</p>
					<? endif //variation_category?>

					<div id="variation_category_holder_<?= $variation_category_id ?>">
						<? foreach( $variation_ids as $l ): extract( $l ) ?>

							<div class="variation active<?= $variation_checked?'1':'0' ?>" id="product_variation_<?= $product_variation_id ?>" <?= $variation_on_click ?> title="<?= htmlspecialchars( $variation ) ?>">

								<strong><?= $variation ?></strong>

								<? if( !empty ($product_variation_image_src_thumb) ): ?>
									<div class="variation-image">
										<span><img src="<?= $product_variation_image_src_thumb ?>" width="100" /></span>
									</div>
								<? endif // $product_variation_image_src_thumb ?>

							</div>
						<? endforeach //$loop?>
					</div>

					<?= $variation_hiddens ?>
				</div>
			<? endforeach //$loop?>
		<? endif //variation ?>
		<? /* TODO-i  FI Per variacions amb imatges */ ?>


		<? /* SECTORS */ ?>
		<? if ( $sector_id ): ?>
			<div class="sector">
				<p><?= $c_sector_id ?></p>
				<?= $sector_id ?>
			</div>
		<? endif //sector_id?>


		<? if ( $can_buy ): ?>

			<? /* PREU */ ?>
			<div class="price">
				<? if ( $old_price ): ?>

					<span class="old-price"><span id="old_price" data-old-price="<?= $old_price_value ?>"><?= $old_price ?></span> <?= CURRENCY_NAME ?></span>

					<? if ( $discount && !$discount_fixed ): ?>
						<span class="discount">-<?= $discount ?>%</span>
					<? endif ?>

				<? endif //old_price?>

				<span id="pvp"><?= $pvp ?></span> <?= CURRENCY_NAME ?>

			</div>


			<? /* CISTELLA */ ?>
			<div class="quantity-holder">
				<p class="quantity"><?= $c_quantity ?></p>
				<div class="quantity">
					<a class="minus" title="-" href="javascript:product_substract_quantity();"></a>
					<span><input type="text" value="1" id="quantity_wanted"/></span>
					<a class="plus" title="+" href="javascript:product_add_quantity();"></a>
				</div>


				<a href="javascript:cart_add_item(<?= $id ?>,1,$('#quantity_wanted').val());" class="cart-button" title="<?= htmlspecialchars( $c_add_to_cart ) ?>"><?= $c_add_to_cart ?></a>
			</div>

		<? endif ?>


		<? /* BANDERES */ ?>

		<? /* stock */ ?>
		<? if ( $can_buy_message ): ?>
			<div class="flag nostock"><?= $can_buy_message ?></div>

			<? /* ultimes unitats */ ?>
		<? elseif ( $show_last_units ): ?>
			<div class="flag ultimas"><?= $c_last_units ?></div>

			<? /* nou */ ?>
		<? elseif($is_new_product): ?>
			<div class="flag new-product"><?= $c_is_new_product ?></div>

			<? /* promocio */ ?>
		<? elseif ( $prominent ): ?>
			<div class="flag promo"><?= $c_prominent ?></div>

			<? /* descompte ( surt repetit també al costat del nou preu, borrar la que no es vulgui ) */ ?>
		<? elseif ( $discount ): ?>
			<div class="flag discount"><?= $c_discount ?><? if ( !$discount_fixed ): ?>
							<?= $discount ?>%<? endif ?></div>
		<? endif //discount?>

	</div>

	<? /* SLIDER */ ?>
	<? if ( $images ): ?>
		<figure class="right">

			<? foreach( $variations_images as $l ): extract( $l ) ?>

				<div class="slider-product<?= $variations_conta > 0?' hidden':'' ?>" id="slider-product-<?= $product_variation_id ?>">

					<? foreach ( $variation_images as $l ):extract( $l ) ?>
						<div class="item<?= $image_conta > 0 ? ' hidden' : '' ?>">

							<a id="image<?= $image_conta ?>"  data-sub-html="<?= htmlspecialchars( $image_title ) ?>" title="<?= htmlspecialchars( $image_title ? $image_title : $product_title ) ?>" href="<?= $image_src_medium ?>"><img <?= get_image_attrs( $image_src_details, $image_alt ? $image_alt : $product_title, 384, 288 ) ?> /></a>

						</div>
					<? endforeach //images?>

				</div>

				<? if ( count( $images ) > 1 ): ?>

					<div class="slider-product-thumbs<?= $variations_conta > 0?' hidden':'' ?>" id="slider-product-thumbs-<?= $product_variation_id ?>">
						<? foreach ( $variation_images as $l ):extract( $l ) ?>
							<div class="item">
								<img <?= get_image_attrs( $image_src_thumb, $image_alt ? $image_alt : $product_title, 128, 96 ) ?> />
							</div>
						<? endforeach //images?>
					</div>

				<? endif // images_count?>

			<? endforeach //$variations_images?>

		</figure>
	<? endif //images?>


	<? /* DESCRIPCIO */ ?>
	<? if ( $product_description ): ?>
		<div class="description sep">
			<h4>
				<?= $c_product_description ?>
			</h4>
			<p>
				<?= $product_description ?>
			</p>
		</div>
	<? endif //product_description?>


	<? /* CARACTERISTIQUES */ ?>
	<? if ( $details ): ?>
		<div class="details sep">
			<h4>
				<?= $c_caracteristics ?>
			</h4>

			<table class="details">
				<? foreach ( $details as $d ): extract( $d ) ?>
					<? if ( $is_odd ): ?>
						<tr>
					<? endif //$is_odd?>
					<th><?= $c_detail ?>:</th>
					<td><?= $detail ?> <?= $units ?></td>
					<? if ( ! $is_odd ): ?>
						</tr>
					<? endif //is_odd?>
				<? endforeach //details?>

				<? if ( $is_odd ): ?>
					<?= '<th></th><td></td></tr>' ?>
				<? endif //is_odd?>
			</table>
		</div>
	<? endif //details?>



	<? /* SPECS */ ?>
	<? if ( $spec_categorys ): ?>
		<table class="specs details">
			<? foreach ( $spec_categorys as $l ): extract( $l ) ?>

				<tr>
					<td colspan="2" class="spec-category"><?= $spec_category ?></td>
				</tr>
				<? foreach ( $specs as $l ): extract( $l ) ?>
					<tr>
						<? if ( $spec_name ): ?>

							<th><?= $spec_name ?></th>
							<td><?= $spec ?> <?= $spec_unit ?></td>
						<? else: //  ?>
							<td class="colspan" colspan="2"><?= $spec ?> <?= $spec_unit ?></td>

						<? endif //$ ?>

					</tr>
				<? endforeach //specs?>
			<? endforeach //spec_categorys?>
		</table>
	<? endif //spec_categorys?>


	<? /* ARXIUS */ ?>
	<? if ( $files ): ?>
		<aside class="files sep">
				<h4>
					<?= $c_files ?>
				</h4>
				<? foreach ( $files as $l ): extract( $l ) ?>
					<p><a href="<?= $file_src ?>"><?= $file_title ?></a></p>
				<? endforeach //$files?>
		</aside>
	<? endif //files?>


	<? /* VIDEO */ ?>
	<? if ( $videoframe ): ?>
		<div class="videoframe sep"><?= $videoframe ?></div>
	<? endif //videoframe?>

</article>

<? /* RELATED TODO-i Maquetar productes relacionats */ ?>
<?= get_block( 'product_related', 'product_id=' . $product_id ) ?>