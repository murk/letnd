<?
/**
 * AramConfig
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2011
 * @version $Id$
 * @access public
 */
class AramConfig extends Module{

	function __construct(){
		parent::__construct();
	}
	function on_load(){
	    $this->table = 'aram__configadmin';
	    $this->id_field = 'configadmin_id';
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function get_records()
	{
		$listing = new ShowConfig($this);		
		$listing->call('list_records_walk','',true);
		
		
	    return $listing->show_config();
	}
	function list_records_walk(&$listing){		
		
		$names = array('memoria_qualitats','memoria_calidades','dossier_comercial','dosier_comercial');
		foreach ($names as $name){	
			$file = '/' . CLIENT_DIR . '/uploads/files/' . $name . '.pdf';
			$ret[$name] ='<input class="file" type="file" name="'.$name.'" /> <a href="'.$file.'">'.$name.'</a>';
		}
		return $ret;
	}

	function save_rows()
	{
	    $save_rows = new SaveRows($this);
	    $save_rows->save();
		
		foreach ($_FILES as $key => $upload_file) {
			if ($upload_file['error'] == 1)
			{
				if (!$gl_errors['post_file_max_size'])
				{
					$gl_message = ' ' . $this->messages['post_file_max_size'] . (int)ini_get('upload_max_filesize') . ' Mb';
					$gl_errors['post_file_max_size'] = true;
					$gl_reload = true;
				}
			}
			else{
			
				$new_file = CLIENT_PATH . 'uploads/files/' . $key . '.pdf';
				$moved = move_uploaded_file($upload_file['tmp_name'], $new_file);
				chmod ($new_file , 0776);
			}
		}
	}
}
?>