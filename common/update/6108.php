<?php
// no es poden posar ; entre mig de la consulta ja que parteix les consultes per el, a no ser que fem servir $update_sql en array() ;



//	INFO Actualitzat a letnd.inmo__property

/* 24-1-17
	Actualitzacions inmo, booking

	Gastos de comunidad, Gastos de IBI y Tasas municipales

 */

if ( is_table( 'booking__configadmin' ) && ! Db::get_first( "SELECT * FROM booking__configadmin WHERE name='ignore_outside_ue'" ) ) {
	$update_sql[] = "
		INSERT INTO booking__configadmin (`name`, `value`) VALUES ('ignore_outside_ue', '0');";

} else {
	if ( is_table( 'booking__configadmin' ) ) {
		trigger_error( 'Ja existeixen el camp ignore_outside_ue' );
	}
}

if ( is_table( 'inmo__property' ) && ! is_field( 'inmo__property', 'community' ) ) {

	$update_sql[] = "ALTER TABLE `inmo__property`
	ADD COLUMN `ibi` INT(11) NOT NULL DEFAULT '0' AFTER `community_expenses`,
	ADD COLUMN `municipal_tax` INT(11) NOT NULL DEFAULT '0' AFTER `ibi`;";

} else {
	if ( is_table( 'inmo__property' ) ) {
		trigger_error( 'Ja existeixen el camp community' );
	}
}

?>