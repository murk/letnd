<?
// si no n'hi ha cap o estic a la pàgina de veure tots els productes visitats, donc no mostro el block
if (!isset($_SESSION['visited_properties']) || isset($_GET['inmo_visited_more'])) {
	$show_block = false;
	return;
}

$module = Module::load('inmo','property');
$this->tpl->module = &$module;
$module->parent = 'Block';

$listing = $module->do_action('get_records');

// config
$show_ref_caption = isset($vars['show_ref_caption']) ? $vars['show_ref_caption'] : true;
$block_max_results = isset($vars['block_max_results']) ? $vars['block_max_results'] : $listing->config['visited_max_results'];
$page_id = isset($vars['page_id']) ? $vars['page_id'] : false;

// enllacen directament a la categoria o /i tipus de l'inmoble
$link_category = isset($vars['link_category'])?$vars['link_category']:1; // true o false
$link_tipus = isset($vars['link_tipus'])?$vars['link_tipus']:1; // true o false
$link_tipus_values = isset($vars['link_tipus_values'])?$vars['link_tipus_values']:false;
$link_tipus_values = explode(',', $link_tipus_values);

$properties = array_reverse($_SESSION['visited_properties']);
$ids = implode(',', $properties);
$order_ids = implode('\',\'', $properties);


$listing->condition .= " AND property_id IN (" . R::escape($ids,false,false) . ")";
$listing->limit =  $block_max_results;

$listing->set_records();
$listing->order_by = "FIELD(inmo__property.property_id, '" . R::escape($order_ids,false,false) . "')";

$loop = $listing->loop;

if ($loop)
{
	$vars['more'] = false;
	$conta = 0;
	$loop_count = count($loop);
	
	foreach($loop as $key=>$val){
	
		$rs = &$loop[$key];			
		
		if ($show_ref_caption) $rs['ref'] = $listing->caption['c_ref'] . ' ' . $rs['ref'];
	
		$link_tipus_value = $rs['tipus_value'];	
		
		if ($link_tipus_values) {
			foreach ($link_tipus_values as $v){
				if (strpos($v, $link_tipus_value)!== false) {
					$link_tipus_value = $v;
					break;
				}
			}
		}
		
		$params = array(
						'id' => $rs['property_id'],
						'file_name' => $rs['property_file_name'], 
						'tool' => $listing->tool, 
						'tool_section' => $listing->tool_section, 
						'link_vars' => array('tipus'=>$link_tipus_value),
						'detect_page' => $page_id?false:true,
						'page_id' => $page_id
				);
		
		if ($link_category) {
			$params['category'] =  'category_id';
			$params['category_id'] =  $rs['category_id_value'];
		}

		if ($link_tipus) {
			$params['friendly_params'] =   array('tipus'=>$link_tipus_value);
		}
	
		$rs['link'] = $rs['show_record_link'] = get_link( $params );
	
		$rs['selected'] = '';
		$rs['conta'] = $conta;
		$rs['loop_count'] = $loop_count;
		$conta++;
		
		
	}
	
	$loop[0]['class'] = 'first';	
	$loop[count($loop)-1]['class'] = 'last';
	
	if (count($_SESSION['visited_properties']) > 2)
	{
		$vars['more'] = $listing->caption['c_property_list__view_all'];		
						
		$vars['more_link'] = 
					get_link( array(
								'tool' => 'inmo', 
								'tool_section' => 'property', 
								'friendly_params' => array('inmo_visited_more'=>'true'),
								//'link_vars' => array('tipus'=>$rs['tipus_value']), // poden ser tot barrejats, per tant no pot anar a un tipus concret,
								'detect_page' => $page_id?false:true,
								'page_id' => $page_id
						))
						. '?property_ids=' . $ids;
	}
}

?>