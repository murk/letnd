<?php
function get_vars_lassdive_activitat($content_ids, &$ids, &$names)
{
    $query = "SELECT lassdive__activitat.activitat_id AS activitat_id, activitat
				FROM lassdive__activitat, lassdive__activitat_language
				WHERE lassdive__activitat.activitat_id IN (" . implode(',', $content_ids) . ")
				AND lassdive__activitat.activitat_id = lassdive__activitat_language.activitat_id
				AND language = '".LANGUAGE."'
				ORDER BY ordre ASC, entered DESC, activitat_id" ;
    $results = Db::get_rows($query);
    foreach($results as $rs)
    {
        $nam = $rs['activitat'];
        $names[] = htmlspecialchars(str_replace(',', ' ', $nam), ENT_QUOTES);
        $ids[] = $rs['activitat_id'];
    }
}
function get_content_extra_lassdive_activitat($ids, $action, $language)
{
	if ($action == 'show_record')
    {
	    $ids = explode(',', $ids);
	    $content = '';
        foreach ($ids as $id)
        {
            $content .= get_url_html(HOST_URL . '/?template=&tool=lassdive&tool_section=activitat&action=show_record&loginnewsletter=letndmarc&passwordnewsletter=5356aopfqkiios5cnoqws90se&activitat_id='.$id.'&language=' . $language);
        }
    }
	else if ($action == 'list_records') {
        $content = get_url_html(HOST_URL . '?template=&tool=lassdive&tool_section=activitat&action=list_records&loginnewsletter=letndmarc&passwordnewsletter=5356aopfqkiios5cnoqws90se&activitat_ids='.$ids.'&language=' . $language);
		
	}
    return $content;
}

?>