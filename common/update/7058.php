<?php
/* 14-9-18
Others privats a product per us intern ( per exemple un client que demana un checkbox per que surti un ellaç a talles
*/


if ( is_table( 'product__product' ) && ! is_field( 'product__product', 'others_private3' ) ) {

	$update_sql[] = "
					ALTER TABLE `product__product`
						ADD COLUMN `others_private3` TEXT NOT NULL AFTER `others_private2`;";


} else {
	if ( is_table( 'product__product' ) ) {
		trigger_error( 'Ja existeixen el camp others_private3' );
	}
}