var ModalBox = {
	init: function(options){
		this.options = Object.extend({
			resizeDuration: 400,
			resizeTransition: Fx.Transitions.sineInOut,
			initialWidth: 10,
			initialHeight: 10,
			finalWidth: 600,
			finalHeight: 450,
			closeOfset: 25,
			animateCaption: true
		}, options || {});

		this.eventKeyDown = this.keyboardListener.bindAsEventListener(this);
		this.eventPosition = this.position.bind(this);

		this.overlay = new Element('div').setProperty('id', 'lbOverlay').injectInside(document.body);

		this.capaexterior =  new Element('div').setProperty('id', 'lbContainer').injectInside(document.body);

		this.bottomContainer = new Element('div').setProperty('id', 'lbBottomContainer').setStyle('display', 'none').injectInside(this.capaexterior);
		this.bottom = new Element('div').setProperty('id', 'lbBottom').injectInside(this.bottomContainer);
		this.closeImg = new Element('a').setProperties({id: 'lbCloseLink', href: '#'}).injectInside(this.bottom);
		this.closeImg.onclick = this.overlay.onclick = this.close.bind(this);
		this.number = new Element('div').setProperty('id', 'lbNumber').injectInside(this.bottom);
		this.caption = new Element('div').setProperty('id', 'lbCaption').injectInside(this.bottom);
		this.center = new Element('div').setProperty('id', 'lbCenter').setStyles({width: this.options.initialWidth+'px', height: this.options.initialHeight+'px', marginLeft: '-'+(this.options.initialWidth/2)+'px', display: 'none'}).injectInside(this.capaexterior);
		this.contentContainer = new Element('div').setProperty('id', 'lbContentContainer').injectInside(this.center);
		var nextEffect = this.nextEffect.bind(this);
		this.fx = {
			overlay: this.overlay.effect('opacity', {duration: 500}).hide(),
			resize: this.center.effects({duration: this.options.resizeDuration, transition: this.options.resizeTransition, onComplete: nextEffect}),
			contentContainer: this.contentContainer.effect('opacity', {duration: 500, onComplete: nextEffect}),
			bottomContainer: this.bottomContainer.effect('top', {duration: 400, onComplete: nextEffect}),
			bottom: this.bottom.effect('opacity', {duration: 500, onComplete: nextEffect})
		};

	},

	open: function(boxContent, title, finalWidth, finalHeight, closeOfset){
		this.boxContent = boxContent;
		this.title = title;
		if (finalWidth){
			this.options.finalWidth = finalWidth;
			this.options.finalHeight = finalHeight;
			this.options.closeOfset = closeOfset;
		}
		this.position();
		this.setup(true);
		this.top = window.getScrollTop() + (window.getHeight() / 15);
		this.center.setStyles({top: this.top+'px', display: ''});
		this.fx.overlay.start(0.8);
		return this.openBox();
	},

	position: function(){
		this.overlay.setStyles({top: window.getScrollTop()+'px', height: window.getHeight()+'px'});
	},

	setup: function(open){
		var elements = $A(document.getElementsByTagName('object'));
		if (window.ie) elements.extend(document.getElementsByTagName('select'));
		elements.each(function(el){ el.style.visibility = open ? 'hidden' : ''; });
		var fn = open ? 'addEvent' : 'removeEvent';
		window[fn]('scroll', this.eventPosition)[fn]('resize', this.eventPosition);
		document[fn]('keydown', this.eventKeyDown);
		this.step = 0;
	},

	keyboardListener: function(event){
		switch (event.keyCode){
			case 27: case 88: case 67: this.close(); break;
		}
	},

	openBox: function(){
		if (this.step) return false;
		this.step = 1;

		this.bottomContainer.style.display =  'none';
		this.fx.contentContainer.hide();
		this.fx.bottom.hide();
		this.center.className = 'lbLoading';

		this.nextEffect();
		return false;
	},

	nextEffect: function(){
		switch (this.step++){
		case 1:
			this.center.className = '';

			this.contentContainer.setHTML(this.boxContent);
			this.caption.setHTML(this.title || '');//referencia
			this.contentContainer.style.width = this.bottom.style.width = this.options.finalWidth + 'px';
			this.contentContainer.style.height = this.options.finalHeight + 'px';

			if (this.center.clientHeight != this.contentContainer.offsetHeight){
				this.fx.resize.start({height: this.contentContainer.offsetHeight});
				break;
			}
			this.step++;
		case 2:
			if (this.center.clientWidth != this.contentContainer.offsetWidth){
				this.fx.resize.start({width: this.contentContainer.offsetWidth, marginLeft: -this.contentContainer.offsetWidth/2});
				break;
			}
			this.step++;
		case 3:
			this.bottomContainer.setStyles({top: (this.top+this.options.closeOfset)+'px', marginLeft: this.center.style.marginLeft, display: ''});
			this.fx.contentContainer.start(1);
			break;
		case 4:
			if (this.options.animateCaption){
				this.fx.bottomContainer.start(this.top-this.options.closeOfset);
				break;
			}
			this.bottomContainer.style.height = '';
		case 5:
			if (this.options.animateCaption){
				this.fx.bottom.start(1);
				break;
			}
			this.bottomContainer.style.height = '';
		case 6:
			this.step = 0;
		}
	},

	close: function(){
		if (this.step < 0) return;
		this.step = -1;
		if (this.preload){
			this.preload.onload = Class.empty;
			this.preload = null;
		}
		for (var f in this.fx) this.fx[f].stop();
		this.center.style.display = this.bottomContainer.style.display = 'none';
		this.center.setStyles({width: this.options.initialWidth+'px', height: this.options.initialHeight+'px', marginLeft: '-'+(this.options.initialWidth/2)+'px', display: 'none'});

		this.fx.overlay.chain(this.setup.pass(false, this)).start(0);
		return false;
	}
};

window.addEvent('domready', ModalBox.init.bind(ModalBox));