<?
/**
 * DirectoryCategory
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2013
 * @version $Id$
 * @access public
 */
class DirectoryCategory extends Module{

	function __construct(){
		parent::__construct();
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = 
				'<script language="JavaScript" src="/admin/modules/directory/jscripts/list_pick.js?v=' . $GLOBALS['gl_version'] . '"></script>' . 
				$this->get_records();
	}
	function get_records()
	{
		$listing = new ListRecords($this);
		$listing->has_bin = false;
		$listing->order_by = 'ordre asc, category asc';
		$listing->table_class = 'big';
		$listing->call('list_records_walk',false,true);	
		
		
		if ($this->parent=='DirectoryDocumentuser'){
			$listing->set_options (0, 0, 0, 0, 0, 0, 0, 0, 1); // $options_bar = 1, $options_checkboxes = 1, $save_button = 1, $select_button = 1, $delete_button = 1, $print_button = 1, $edit_buttons = 1, $image_buttons = 1, $split_count = 1
			$listing->set_field('category','list_admin','text');
			$listing->set_field('category_description','list_admin','text');
			$listing->set_field('concession','list_admin','no');
			
			// premisos
			$listing->condition = DirectoryCommon::get_category_condition();
			
			
			$listing->show_langs = false;
		}
		else{
			$listing->set_options (1, 1, 1, 1, 1, 1, 0, 0, 1); // $options_bar = 1, $options_checkboxes = 1, $save_button = 1, $select_button = 1, $delete_button = 1, $print_button = 1, $edit_buttons = 1, $image_buttons = 1, $split_count = 1
			$listing->add_button ('list_documents_button', 'action=list_records&menu_id=303&selected_menu_id=307', 'boto1', 'after');
			$listing->add_button ('concession_change', '', 'boto1', 'after', 'select_concession');
			$listing->show_langs = true;
		}
		return $listing->list_records();
	}
	function list_records_walk($module){
		$rs = &$module->rs;
		$ret = array();
		if ($this->parent=='DirectoryDocumentuser'){
			$ret['tr_jscript'] ='onclick="window.location=\'/admin/?menu_id=312&tool=directory&tool_section=document&category_id='. $rs['category_id'].'\'"';
		}
		$rs['document_icon'] = '<img width="60" src="/common/images/icons/folder.png" />';
		
		
		$rs['concession'] = DirectoryCommon::get_concession_names($rs['category_id']);
		
		if (!$rs['concession']) $rs['concession'] = '<div id = "concession_'.$rs['category_id'].'">' . $this->caption['c_concession_everybody'] . '</div>';
		
		
		return $ret;
	}

	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{
	    $show = new ShowForm($this);
		$show->has_bin = false;
	    return $show->show_form();
	}

	function save_rows()
	{
	    $save_rows = new SaveRows($this);
	    $save_rows->save();
	}

	function write_record()
	{
	    $writerec = new SaveRows($this);
	    $writerec->save();
	}

	function manage_images()
	{
	    $image_manager = new ImageManager($this);
	    $image_manager->execute();
	}
}
?>