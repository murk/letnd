-- phpMyAdmin SQL Dump
-- version 2.8.1
-- http://www.phpmyadmin.net
-- 
-- Servidor: localhost
-- Tiempo de generación: 17-11-2007 a las 12:32:50
-- Versión del servidor: 4.1.22
-- Versión de PHP: 4.3.10
-- 
-- Base de datos: `phpmyvisits`
-- 

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `s_a_category`
-- 

CREATE TABLE `s_a_category` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `name` varchar(100) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Volcar la base de datos para la tabla `s_a_category`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `s_a_config`
-- 

CREATE TABLE `s_a_config` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `name` varchar(100) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Volcar la base de datos para la tabla `s_a_config`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `s_a_file`
-- 

CREATE TABLE `s_a_file` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `name` text,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Volcar la base de datos para la tabla `s_a_file`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `s_a_keyword`
-- 

CREATE TABLE `s_a_keyword` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `name` text,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Volcar la base de datos para la tabla `s_a_keyword`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `s_a_newsletter`
-- 

CREATE TABLE `s_a_newsletter` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(60) NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Volcar la base de datos para la tabla `s_a_newsletter`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `s_a_page`
-- 

CREATE TABLE `s_a_page` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `name` text,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Volcar la base de datos para la tabla `s_a_page`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `s_a_partner_name`
-- 

CREATE TABLE `s_a_partner_name` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `name` varchar(100) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Volcar la base de datos para la tabla `s_a_partner_name`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `s_a_partner_url`
-- 

CREATE TABLE `s_a_partner_url` (
  `id` int(11) NOT NULL auto_increment,
  `name` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Volcar la base de datos para la tabla `s_a_partner_url`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `s_a_provider`
-- 

CREATE TABLE `s_a_provider` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `name` varchar(100) default NULL,
  PRIMARY KEY  (`id`),
  KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Volcar la base de datos para la tabla `s_a_provider`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `s_a_resolution`
-- 

CREATE TABLE `s_a_resolution` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(20) NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Volcar la base de datos para la tabla `s_a_resolution`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `s_a_search_engine`
-- 

CREATE TABLE `s_a_search_engine` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `name` varchar(100) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Volcar la base de datos para la tabla `s_a_search_engine`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `s_a_site`
-- 

CREATE TABLE `s_a_site` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `name` text,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Volcar la base de datos para la tabla `s_a_site`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `s_a_vars_name`
-- 

CREATE TABLE `s_a_vars_name` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Volcar la base de datos para la tabla `s_a_vars_name`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `s_a_vars_value`
-- 

CREATE TABLE `s_a_vars_value` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Volcar la base de datos para la tabla `s_a_vars_value`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `s_archives`
-- 

CREATE TABLE `s_archives` (
  `idarchives` int(10) unsigned NOT NULL auto_increment,
  `idsite` int(10) unsigned NOT NULL default '0',
  `done` tinyint(4) NOT NULL default '0',
  `period` tinyint(1) NOT NULL default '0',
  `simple` tinyint(4) NOT NULL default '0',
  `date1` varchar(10) NOT NULL default '00:00:00',
  `date2` varchar(10) NOT NULL default '00:00:00',
  `nb_uniq_vis` mediumint(8) unsigned NOT NULL default '0',
  `nb_vis` mediumint(8) unsigned NOT NULL default '0',
  `nb_vis_returning` int(8) NOT NULL default '0',
  `nb_uniq_vis_returning` int(11) NOT NULL default '0',
  `nb_pag` mediumint(8) unsigned NOT NULL default '0',
  `nb_pag_returning` int(8) NOT NULL default '0',
  `nb_uniq_pag` smallint(5) unsigned NOT NULL default '0',
  `nb_max_pag` smallint(5) unsigned NOT NULL default '0',
  `nb_vis_1pag` mediumint(8) unsigned NOT NULL default '0',
  `nb_vis_1pag_returning` mediumint(8) NOT NULL default '0',
  `sum_vis_lth` int(10) unsigned NOT NULL default '0',
  `sum_vis_lth_returning` int(10) NOT NULL default '0',
  `nb_direct` mediumint(8) unsigned NOT NULL default '0',
  `nb_search_engine` mediumint(8) unsigned NOT NULL default '0',
  `nb_site` mediumint(8) unsigned NOT NULL default '0',
  `nb_newsletter` mediumint(8) unsigned NOT NULL default '0',
  `nb_partner` mediumint(8) unsigned NOT NULL default '0',
  `vis_period` longblob,
  `vis_nb_vis` blob,
  `vis_st` blob,
  `vis_lt` blob,
  `pag_st` blob,
  `pag_lt` blob,
  `vis_lth` blob,
  `vis_nb_pag` blob,
  `vis_pag_grp` longblob,
  `vis_country` blob,
  `vis_continent` blob,
  `vis_provider` longblob,
  `vis_config` longblob,
  `vis_os` blob,
  `vis_browser` blob,
  `vis_browser_type` blob,
  `vis_resolution` longblob,
  `vis_plugin` blob,
  `vis_search_engine` longblob,
  `vis_keyword` longblob,
  `vis_site` longblob,
  `vis_partner` longblob,
  `vis_newsletter` longblob,
  `int_lt` blob,
  `int_st` blob,
  `int_referer_type` longblob,
  `int_search_engine` longblob,
  `int_keyword` longblob,
  `int_site` longblob,
  `int_partner` longblob,
  `int_newsletter` longblob,
  `int_country` longblob,
  `int_continent` longblob,
  `int_config` longblob,
  `int_os` longblob,
  `int_browser` longblob,
  `int_resolution` longblob,
  `compressed` tinyint(4) NOT NULL default '0',
  PRIMARY KEY  (`idarchives`),
  KEY `pmvindex1` (`idsite`),
  KEY `pmvindex2` (`done`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Volcar la base de datos para la tabla `s_archives`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `s_category`
-- 

CREATE TABLE `s_category` (
  `idcategory` int(10) unsigned NOT NULL auto_increment,
  `complete_name` varchar(255) NOT NULL default '',
  `name` varchar(100) default NULL,
  `level` smallint(5) unsigned NOT NULL default '0',
  `idparent` smallint(5) unsigned NOT NULL default '0',
  PRIMARY KEY  (`idcategory`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Volcar la base de datos para la tabla `s_category`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `s_groups`
-- 

CREATE TABLE `s_groups` (
  `idgroups` int(10) unsigned NOT NULL auto_increment,
  `name` varchar(40) default NULL,
  `view` tinyint(1) unsigned default '0',
  `admin` tinyint(3) unsigned default '0',
  PRIMARY KEY  (`idgroups`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

-- 
-- Volcar la base de datos para la tabla `s_groups`
-- 

INSERT INTO `s_groups` (`idgroups`, `name`, `view`, `admin`) VALUES (1, 'admin', 1, 1),
(2, 'view', 1, 0);

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `s_ip_ignore`
-- 

CREATE TABLE `s_ip_ignore` (
  `idip_ignore` int(10) unsigned NOT NULL auto_increment,
  `idsite` int(10) unsigned NOT NULL default '0',
  `ip_min` int(11) default NULL,
  `ip_max` int(11) default NULL,
  PRIMARY KEY  (`idip_ignore`),
  KEY `pmvindex` (`idsite`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Volcar la base de datos para la tabla `s_ip_ignore`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `s_link_vp`
-- 

CREATE TABLE `s_link_vp` (
  `idlink_vp` int(11) NOT NULL auto_increment,
  `idvisit` int(10) unsigned NOT NULL default '0',
  `idpage` int(10) unsigned NOT NULL default '0',
  `idpage_ref` int(11) unsigned NOT NULL default '0',
  `total_time_page_ref` int(10) unsigned default NULL,
  PRIMARY KEY  (`idlink_vp`),
  KEY `pmvindex` (`idvisit`,`idpage`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Volcar la base de datos para la tabla `s_link_vp`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `s_link_vpv`
-- 

CREATE TABLE `s_link_vpv` (
  `idlink_vp` int(11) NOT NULL default '0',
  `idvars` int(11) NOT NULL default '0',
  PRIMARY KEY  (`idlink_vp`,`idvars`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- 
-- Volcar la base de datos para la tabla `s_link_vpv`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `s_newsletter`
-- 

CREATE TABLE `s_newsletter` (
  `idnewsletter` int(10) unsigned NOT NULL auto_increment,
  `idsite` int(10) unsigned NOT NULL default '0',
  `name` varchar(90) default NULL,
  PRIMARY KEY  (`idnewsletter`),
  KEY `pmvindex` (`idsite`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Volcar la base de datos para la tabla `s_newsletter`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `s_page`
-- 

CREATE TABLE `s_page` (
  `idpage` int(10) unsigned NOT NULL auto_increment,
  `idcategory` int(10) unsigned NOT NULL default '0',
  `name` varchar(255) default NULL,
  PRIMARY KEY  (`idpage`),
  KEY `pmvindex` (`idcategory`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Volcar la base de datos para la tabla `s_page`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `s_page_md5url`
-- 

CREATE TABLE `s_page_md5url` (
  `idpage_md5url` int(10) unsigned NOT NULL auto_increment,
  `idpage` int(10) unsigned NOT NULL default '0',
  `md5url` char(32) default NULL,
  `idpage_url` int(11) unsigned NOT NULL default '0',
  PRIMARY KEY  (`idpage_md5url`),
  KEY `idpage` (`idpage`),
  KEY `url` (`md5url`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Volcar la base de datos para la tabla `s_page_md5url`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `s_page_url`
-- 

CREATE TABLE `s_page_url` (
  `idpage_url` int(10) unsigned NOT NULL auto_increment,
  `url` text,
  PRIMARY KEY  (`idpage_url`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Volcar la base de datos para la tabla `s_page_url`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `s_query_log`
-- 

CREATE TABLE `s_query_log` (
  `idquery_log` int(11) NOT NULL auto_increment,
  `idsite` int(11) NOT NULL default '0',
  `query` smallint(6) NOT NULL default '0',
  `time` float NOT NULL default '0',
  `date` date NOT NULL default '0000-00-00',
  `daytime` time NOT NULL default '00:00:00',
  PRIMARY KEY  (`idquery_log`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Volcar la base de datos para la tabla `s_query_log`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `s_site`
-- 

CREATE TABLE `s_site` (
  `idsite` int(10) unsigned NOT NULL auto_increment,
  `name` varchar(90) default NULL,
  `logo` varchar(15) default NULL,
  `params_choice` varchar(6) NOT NULL default 'all',
  `params_names` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`idsite`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- 
-- Volcar la base de datos para la tabla `s_site`
-- 

INSERT INTO `s_site` (`idsite`, `name`, `logo`, `params_choice`, `params_names`) VALUES (1, 'site1', 'pixel.gif', 'all', '');

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `s_site_partner`
-- 

CREATE TABLE `s_site_partner` (
  `idsite_partner` int(10) unsigned NOT NULL auto_increment,
  `idsite` int(10) unsigned NOT NULL default '0',
  `name` varchar(90) default NULL,
  PRIMARY KEY  (`idsite_partner`),
  KEY `pmvindex` (`idsite`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Volcar la base de datos para la tabla `s_site_partner`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `s_site_partner_url`
-- 

CREATE TABLE `s_site_partner_url` (
  `idsite_partner_url` int(10) unsigned NOT NULL auto_increment,
  `idsite_partner` int(10) unsigned NOT NULL default '0',
  `url` varchar(200) default NULL,
  PRIMARY KEY  (`idsite_partner_url`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Volcar la base de datos para la tabla `s_site_partner_url`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `s_site_url`
-- 

CREATE TABLE `s_site_url` (
  `idsite_url` int(10) unsigned NOT NULL auto_increment,
  `idsite` int(10) unsigned NOT NULL default '0',
  `url` varchar(255) default NULL,
  PRIMARY KEY  (`idsite_url`),
  KEY `pmvindex` (`idsite`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- 
-- Volcar la base de datos para la tabla `s_site_url`
-- 

INSERT INTO `s_site_url` (`idsite_url`, `idsite`, `url`) VALUES (1, 1, '');

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `s_users`
-- 

CREATE TABLE `s_users` (
  `login` varchar(20) NOT NULL default '',
  `password` varchar(255) default NULL,
  `alias` varchar(45) default NULL,
  `email` varchar(100) NOT NULL default '',
  `send_mail` int(10) default NULL,
  `rss_hash` varchar(100) NOT NULL default '',
  `date_registered` int(11) default NULL,
  PRIMARY KEY  (`login`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- 
-- Volcar la base de datos para la tabla `s_users`
-- 

INSERT INTO `s_users` (`login`, `password`, `alias`, `email`, `send_mail`, `rss_hash`, `date_registered`) VALUES ('anonymous', NULL, 'Anonymous user', '', 0, 'ffffffffffffff493e8d55a4a75de3f90a1', NULL);

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `s_users_link_groups`
-- 

CREATE TABLE `s_users_link_groups` (
  `idsite` int(10) unsigned NOT NULL default '0',
  `idgroups` int(10) unsigned NOT NULL default '0',
  `login` varchar(20) NOT NULL default '0',
  PRIMARY KEY  (`idsite`,`idgroups`,`login`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- 
-- Volcar la base de datos para la tabla `s_users_link_groups`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `s_vars`
-- 

CREATE TABLE `s_vars` (
  `idvars` int(10) unsigned NOT NULL auto_increment,
  `name` varchar(255) NOT NULL default '',
  `int_value` int(10) default NULL,
  `varchar_value` varchar(255) default NULL,
  PRIMARY KEY  (`idvars`),
  KEY `pmvindex` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Volcar la base de datos para la tabla `s_vars`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `s_version`
-- 

CREATE TABLE `s_version` (
  `version` varchar(255) NOT NULL default ''
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- 
-- Volcar la base de datos para la tabla `s_version`
-- 

INSERT INTO `s_version` (`version`) VALUES ('2.2');

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `s_visit`
-- 

CREATE TABLE `s_visit` (
  `idvisit` int(10) unsigned NOT NULL auto_increment,
  `idsite` int(10) unsigned NOT NULL default '0',
  `idcookie` varchar(32) default NULL,
  `returning` tinyint(1) NOT NULL default '0',
  `last_visit_time` time NOT NULL default '00:00:00',
  `server_date` date default NULL,
  `server_time` time NOT NULL default '00:00:00',
  `referer` text,
  `os` char(3) default NULL,
  `browser_name` varchar(10) NOT NULL default '',
  `browser_version` varchar(20) NOT NULL default '',
  `resolution` varchar(9) default NULL,
  `color_depth` tinyint(2) unsigned default NULL,
  `pdf` tinyint(1) NOT NULL default '0',
  `flash` tinyint(1) NOT NULL default '0',
  `java` tinyint(1) NOT NULL default '0',
  `director` tinyint(1) NOT NULL default '0',
  `quicktime` tinyint(1) NOT NULL default '0',
  `realplayer` tinyint(1) NOT NULL default '0',
  `windowsmedia` tinyint(1) NOT NULL default '0',
  `local_time` time NOT NULL default '00:00:00',
  `ip` int(10) default NULL,
  `hostname_ext` varchar(100) default NULL,
  `browser_lang` varchar(60) default NULL,
  `total_pages` smallint(5) unsigned default NULL,
  `total_time` smallint(5) unsigned default NULL,
  `country` char(3) default NULL,
  `continent` char(3) default NULL,
  `exit_idpage` int(11) NOT NULL default '0',
  `entry_idpage` int(11) NOT NULL default '0',
  `entry_idpageurl` int(11) NOT NULL default '0',
  `md5config` varchar(32) NOT NULL default '',
  PRIMARY KEY  (`idvisit`),
  KEY `idsite` (`idsite`),
  KEY `server_date` (`server_date`),
  KEY `md5config` (`md5config`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Volcar la base de datos para la tabla `s_visit`
-- 

