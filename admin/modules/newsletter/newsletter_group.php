<?
/**
 * NewsletterGroup
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 22/1/2011
 * @version $Id$
 * @access public
 */
class NewsletterGroup extends Module{

	function __construct(){
		parent::__construct();
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function get_records()
	{
	    $listing = new ListRecords($this);
		$listing->order_by = 'group_name asc';
		
		if ($this->parent == 'CustumerGroup') {
			
			$listing->add_button ('button_addresses', 'menu_id=5003&selected_menu_id=5033', 'boto1', 'before');	
			
		}
		else{
			
			$listing->add_button ('button_addresses', 'action=list_records&menu_id=11&selected_menu_id=13', 'boto1', 'before');
			
		}
		$listing->has_bin = false;
	    return $listing->list_records();
	}
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{
	    $show = new ShowForm($this);
		$show->has_bin = false;
	    return $show->show_form();
	}

	function save_rows()
	{
	    $save_rows = new SaveRows($this);
	    $save_rows->save();
	}

	function write_record()
	{
	    $writerec = new SaveRows($this);
	    $writerec->save();
	}

	function manage_images()
	{
	    $image_manager = new ImageManager($this);
	    $image_manager->execute();
	}
}
?>