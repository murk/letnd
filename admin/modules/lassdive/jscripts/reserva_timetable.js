class ReservaTimetable {

	constructor(reservaitem_id, c_user_id, reservaitem_date, is_new, product_id, can_select_lower_date = false) {

		this.reservaitem_id = reservaitem_id;
		this.c_user_id = c_user_id;
		this.reservaitem_date = this.product_date = reservaitem_date;
		this.is_new = is_new;
		this.product_id = product_id;
		this.centre_id = 0;
		this.centres = [];
		this.start = '';
		this.end = '';
		this.range = [];
		this.current_date = new Date(this.reservaitem_date);
		this.can_select_lower_date = can_select_lower_date;

		this.is_admin = typeof gl_is_admin === 'undefined'?false:gl_is_admin;

		this.init_date_nav(reservaitem_date);

	}

	select(obj) {

		let $obj = $(obj);
		let hour = $obj.data('hour');
		let centre_id = $obj.data('centre-id');
		let is_busy = $obj.data('is-busy');

		// Si hi ha algún start no faig return;
		if (is_busy && !$obj.find('.start').length) {
			return;
		}

		if (!this.start) {
			this.centre_id = centre_id;
			this.centre = $('#centre_' + this.centre_id).html();
			this.start = hour;
			$(obj).addClass('selected');
		}
		else if (centre_id === this.centre_id && this.is_hour_smaller(hour)) {
			this.show_message(LASSDIVE_HORA_MES_GRAN);
		}
		else if (centre_id === this.centre_id) {
			if (this.is_range_possible(hour)) {
				this.end = hour;
				this.draw_selection();
			}
			else {
				this.show_message(LASSDIVE_NO_DISPONIBLE);
			}
		}
		else {
			this.show_message(LASSDIVE_HORA_DIFERENTS_CENTRES);
		}
	}

	show_message(message) {
		// TODO-i Canviar alerts
		alert(message);
	}

	is_range_possible(end) {

		let start = this.start;
		let has_done_loop = false;

		for (let [hour, quarter] of this.hour_time(this.start, end)) {

			let id = `#${this.centre_id}_${hour}_${quarter}`;
			let is_busy = $(id).data('is-busy');

			if (is_busy) {
				this.range = [];
				return false;
			}
			else {
				this.range.push(id);
			}
			has_done_loop = true;
		}

		return has_done_loop;

	}

	draw_selection(draw = true) {
		this.range.forEach((val) => draw ? $(val).addClass('selected') : $(val).removeClass('selected') );
		if (draw) this.show_selected_timetable()
	}

	show_selected_timetable() {

		let
			reservaitem_date_formatted = this.format_date(this.reservaitem_date),
			html = '';

		if (reservaitem_date_formatted){
			html = `
				<em>${reservaitem_date_formatted}</em>`
		}

		if (this.start){
			html += `
				<strong>de ${this.start} a ${this.end}</strong>`;
		}

		html += `
				<b>${this.c_user_id}</b>`;

		if (this.centre) $('#timetable-result-centre').html(this.centre);
		$('#timetable-result').html(html);

		let $timetables = $('#timetables');
		if ($timetables.length) {
			$timetables.fadeOut(() => $('#timetable-results').fadeIn(
				() => $('#selected_user_id_add').focus()
			));
		}
		else {
			$('#timetable-results').fadeIn(
				() => $('#selected_user_id_add').focus());
		}

	}

	reset() {

		this.draw_selection(false);
		this.centre_id = 0;
		this.centre = '';
		this.start = '';
		this.end = '';
		this.range = [];

		$('#timetable-results').fadeOut(() => $('#timetables').fadeIn());
	}

	cancel() {

		this.reset();

		$('#timetable-results').fadeOut(() => $('#timetables').fadeIn());
	}

	done() {
		let
			id = this.reservaitem_id,
			product_date = this.product_date,
			reservaitem_date = this.reservaitem_date,
			reservaitem_date_formatted = this.format_date(this.reservaitem_date),
			_this = this,
			[user_ids, users] = this.get_user_ids();

		/*if (!user_ids) {
			this.show_message("S'ha d'escollir instructor");
			return;
		}*/

		let url = this.is_admin
			? `/admin/?menu_id=2303`
			: `/?language=${gl_language}&tool=lassdive&tool_section=reserva`;

		let data = {
					'reservaitem_id': id,
					'start_time': this.start,
					'end_time': this.end,
					'product_date': product_date,
					'reservaitem_date': reservaitem_date,
					'user_ids': user_ids,
					'centre_id': this.centre_id
				};

		if (this.is_new) {
			data['variation_ids'] = calendar[this.product_id].reserva_items [id]['variation_ids']; // valor de product_init_orderitems
			calendar[this.product_id].reserva_items [id] = data;
			this.on_done_draw_row(id, reservaitem_date_formatted, _this, users);
		}
		else {
			data['action'] = 'save_timetable_ajax';
			$.ajax({
				url: url,
				data: data,
				dataType: 'json'
			})
				.done((data) => {
					if (data.success) {
						_this.on_done_draw_row(id, reservaitem_date_formatted, _this, users, data.rows);
					}
					else if (data.error) {
						_this.show_message(data.error);
					}
				})
				.fail((jqXHR, textStatus, errorThrown ) => {
						_this.show_message(`${textStatus}: ${errorThrown}` );
				})
			;
		}
	}

	on_done_draw_row(id, reservaitem_date_formatted, _this, users, ajax_rows = false) {

		ReservaList.hide_popup('timetables-holder');
		let row=_this.is_new?`#reservaitem_list_row_${id}_${_this.product_id}`:`#reservaitem_list_row_${id}`;

		row = _this.get_rows_to_copy_selection( _this, row, id, ajax_rows );

		let $row = $(row);

		if ($row.find('.start-time').length) {
			$row.find('.reservaitem_date').html(reservaitem_date_formatted);
			$row.find('.start-time').html(_this.start);
			$row.find('.end-time').html(_this.end);
		}
		else {
			$row.find('.reservaitem_date').html(`
								${reservaitem_date_formatted}					
								<div class="time">De
									${_this.start}
									a
									${_this.end}</div>
							`);
		}
		$row.find('.centre-id').html(_this.centre);
		$row.find('.user-id').html(users);

	}

	// Si és la primera tria de nova reserva, poso totes les altres iguals
	get_rows_to_copy_selection ( _this, row, selected_id, ajax_rows ) {

		// Si s'ha guardat una reserva, php retorna els rows que s'han actualitzat per poder dibuixar-los
		if ( ajax_rows ) return ajax_rows;

		if (!_this.is_new || !calendar[_this.product_id].is_reserva_first_choice) return row;

		calendar[_this.product_id].is_reserva_first_choice = false;
		calendar[_this.product_id].data_busy = calendar[_this.product_id].reserva_items [selected_id]['product_date'];

		let new_row = '';
		let row_conta = 0;
		let max_quantity = _this.centres[_this.centre_id]['centre_max_quantity'];

		for (let id in calendar[_this.product_id].reserva_items){

			id = String(id);
			let data = Object.assign({}, calendar[_this.product_id].reserva_items [selected_id]);
			data['reservaitem_id'] = id;

			new_row += `#reservaitem_list_row_${id}_${_this.product_id},`;
			calendar[_this.product_id].reserva_items [id] = data;

			row_conta ++;

			if (max_quantity == row_conta) break;

		}
		new_row = new_row.substring(0, new_row.length - 1);
		return new_row;
	}

	get_user_ids() {
		let
			vars = ReservaObject.checkboxes_long_vars['selected_user_id'],
			selected_ids = vars.selected_ids,
			names = vars.names,
			submit_ids = [],
			users = '';

		for (let id of selected_ids) {
			if ($('#selected_user_id_' + id).is(":checked")) {
				submit_ids.push(id);
				users += names[id] + '<br>';
			}
		}
		if (users.length > 4) users = users.substring(0, users.length-4);

		return [submit_ids, users];

	}

	init_date_nav(date) {

		this.set_date_nav(date);
		let _this = this;

		let options = {
			changeMonth: true,
			changeYear: true,
			yearRange: '+0:+5',
			onClose: function (selectedDate) {
				let date = $(this).datepicker('getDate');
				_this.get_day_html(0, date);
			}
		};

		// if (!this.can_select_lower_date) options.minDate = new Date();

		$('#timetable-date').datepicker( options );


	}

	set_date_nav(date) {
		let today = new Date();
		date = new Date(date);
		let $prev = $('#prev-date');

		// this.areSameDate(today, date) && !this.can_select_lower_date ? $prev.hide() : $prev.show();

	}

	next_date() {
		this.get_day_html(1);
	}

	prev_date() {
		this.get_day_html(-1);
	}

	get_day_html(n, given_date = false) {

		this.reset();
		if (given_date) {
			this.current_date = given_date;
		}
		else {
			this.current_date.setDate(this.current_date.getDate() + n);
		}

		let date = this.current_date;
		let new_date = date.getFullYear() + '/' + (date.getMonth() + 1) + '/' + date.getDate();

		this.reservaitem_date = new_date;
		this.set_date_nav(new_date);

		$('#timetable-date').val(this.format_date(date, false));

		ReservaList.get_popup_content('timetables-holder', this.reservaitem_id, new_date);

	}

	format_date(date, make_object = true) {
		if (make_object) date = new Date(date);
		let month = date.getMonth() + 1;
		// if (month < 10) month = '0' + month;
		return date.getDate() + '/' + month + '/' + date.getFullYear();
	}

	areSameDate(d1, d2) {
		return d1.getFullYear() == d2.getFullYear()
			&& d1.getMonth() == d2.getMonth()
			&& d1.getDate() == d2.getDate();
	}

	is_hour_smaller(hour) {
		let start = parseInt(this.start.replace(':',''));
		hour = parseInt(hour.replace(':',''));
		return start > hour;
	}

	* hour_time(start, end) {

		let [start_hour, start_quarter] = start.split(':').map(n => parseInt(n)),
			[end_hour, end_quarter] = end.split(':').map(n => parseInt(n)),
			[current_hour, current_quarter, step] = [start_hour, start_quarter, 15];

		while ((current_hour < end_hour) || (current_quarter != end_quarter)) {

			dp('current_quarter', current_hour + ':' + current_quarter);
			yield [current_hour, current_quarter];

			current_quarter += step;
			if (current_quarter == 60) {
				current_quarter = 0;
				current_hour++;
			}
		}
	}
}


