<?

namespace Console\Command;

// use Nette\Security\Passwords;
use Console;
use Console\Util;
use Db;
use Query;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Style\SymfonyStyle;
use Unirest\Request;

// use Symfony\Component\Console\Question\Question;

final class Vreasy extends Command {

	private $output;
	private $input;
	public $api_url = "https://api.vreasy.com";
	public $headers;

	protected function configure() {
		$this->setName( 'Vreasy' )
		     ->setDescription( 'Utilitats Vreasy' )
		     ->addArgument( 'client', InputArgument::REQUIRED, "Client on realitzar les accions" )
		     ->addArgument( 'action', InputArgument::REQUIRED, "Accio a realitzar: 'api', 'api-delete', 'api-key', 'account', 'subscriptions', 'propertys', 'reservations'" )
		     ->addArgument( 'arg1', InputArgument::OPTIONAL, "" )
		     ->addArgument( 'arg2', InputArgument::OPTIONAL, "" )
		     ->addArgument( 'arg3', InputArgument::OPTIONAL, "" )
		     ->addArgument( 'arg4', InputArgument::OPTIONAL, "" );
	}

	protected function execute( InputInterface $input, OutputInterface $output ) {

		Util::init_unirest( $this );

		$this->input  = $input;
		$this->output = $output;
		$io           = new SymfonyStyle( $input, $output );

		$actions = [ 'api', 'api-key', 'api-delete', 'account', 'subscriptions', 'propertys', 'reservations' ];

		$client = $input->getArgument( 'client' );
		$action = $input->getArgument( 'action' );
		$arg1   = $input->getArgument( 'arg1' );
		$arg2   = $input->getArgument( 'arg2' );
		$arg3   = $input->getArgument( 'arg3' );
		$arg4   = $input->getArgument( 'arg4' );

		if ( ! in_array( $action, $actions ) ) {


			$helper   = $this->getHelper( 'question' );
			$question = new ChoiceQuestion(
				'Acció a realitzar?',
				$actions );

			$question->setErrorMessage( 'Accio %s no es valid.' );

			$action = $helper->ask( $input, $output, $question );


		}
		$action = str_replace( '-', '_', $action );

		if ( $client == 'test' ) {
			$clients = [
				'client_id'        => 0,
				'inmo_vreasy_user' => 'yv08j0xpkysw8ogog884kwww8s40800w000g4',
				'fiscalname'      => 'Test',
				'domain'           => 'Api-key de probes'
			];
		}
		else {
			$clients = Util::get_server_clients( $client );
		}

		if ( ! isset( $clients['questions'] ) ) {
			$client_rs = $clients;
		}
		elseif ( empty( $clients['questions'] ) ) {
			$client_rs = $clients['clients'];
		}
		else {
			$helper   = $this->getHelper( 'question' );
			$question = new ChoiceQuestion(
				'Quin client?',
				$clients['questions'] );

			$question->setErrorMessage( 'El client no és valid' );

			$client_dir = $helper->ask( $input, $output, $question );
			$client_rs  = $clients['clients'][ $client_dir ];
		}

		//$output->writeln( "Acció: $action" );
		//$output->writeln( "Client: {$client_rs['client_dir']}" );
		if ( $client_rs ) {
			$io->title( "$action: {$client_rs['fiscalname']} - {$client_rs['domain']}" );
		}
		else {
			$io->title( "$action: $client ( no trobat online )" );
		}
		$this->$action( $action, $client, $client_rs, $io, ...[ $arg1, $arg2, $arg3, $arg4 ] );

		return 0;
	}

	private function api( $action, $client, $client_rs, SymfonyStyle $io, ...$args ) {

		if ( ! Util::is_client_on_server( $client_rs, $io ) ) return;
		$api_key = $client_rs['inmo_vreasy_user'];
		if ( ! $api_key ) return;


		$name  = $args[0];
		$value = isset($args[1]) ? $args[1] : false;

		Request::auth( $api_key );

		$api_url  = $this->api_url . '/' . $name;
		$response = Request::get( $api_url, $this->headers );

		$ret = $response->body;

		//foreach ( $response->body as $item ) {
		//	$io->text( $item->id );
		//}
		$io->success( "URL: $api_url" );
		$io->text( print_r( $ret, true ) );


	}

	private function api_delete( $action, $client, $client_rs, SymfonyStyle $io, ...$args ) {

		if ( ! Util::is_client_on_server( $client_rs, $io ) ) return;
		$api_key = $client_rs['inmo_vreasy_user'];
		if ( ! $api_key ) return;


		$name  = $args[0];
		$value = isset($args[1]) ? $args[1] : false;

		Request::auth( $api_key );

		$api_url  = $this->api_url . '/' . $name;
		$response = Request::delete( $api_url, $this->headers );

		$ret = $response->body;

		$io->success( "URL: $api_url" );
		$io->text( print_r( $ret, true ) );


	}

	private function subscriptions( $action, $client, $client_rs, SymfonyStyle $io ) {
		$ret = $this->api( $action, $client, $client_rs, $io, 'subscriptions');
	}

	private function account( $action, $client, $client_rs, SymfonyStyle $io ) {
		$ret = $this->api( $action, $client, $client_rs, $io, 'account');
	}

	private function propertys( $action, $client, $client_rs, SymfonyStyle $io ) {
		$ret = $this->api( $action, $client, $client_rs, $io, 'properties');
	}

	private function reservations( $action, $client, $client_rs, SymfonyStyle $io ) {
		$ret = $this->api( $action, $client, $client_rs, $io, 'reservations');
	}

	private function api_key( $action, $client, $client_rs, SymfonyStyle $io, ...$args ) {

		if ( ! Util::is_client_on_server( $client_rs, $io ) || $client_rs['client_id'] == 0 ) return;

		Util::connect_mother();

		$value     = $args[0];
		$client_id = $client_rs['client_id'];
		$q         = new Query( "letnd.client__client" );

		if ( $value ) {
			$query = $q->prepare_update( [ 'inmo_vreasy_user' => $value ], $client_id );

			$helper   = $this->getHelper( 'question' );
			$question = new ConfirmationQuestion( "Executar aquest query? $query", false );

			if ( ! $helper->ask( $this->input, $this->output, $question ) ) {
				$io->text( "Abortat!" );

				return;
			}
			$q->execute_update( $query );
			$io->text( "Api key actualitzada a: \"$value\" a letnd clients" );
		}
		else {
			$api_key = $client_rs['inmo_vreasy_user'];
			$io->text( "Api key: $api_key" );
		}

		Db::reconnect();
	}

}