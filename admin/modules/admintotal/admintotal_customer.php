<?php
set_config('admintotal/admintotal_customer_config.php');
set_table('client__client', 'client_id');
$GLOBALS['gl_linux_aliases'] = '';
$GLOBALS['gl_linux_paths'] = '';
function save_rows()
{
    Db::connect_mother();
	$save_rows = new SaveRows;
    $save_rows->save();
	Db::reconnect();
}

function list_records()
{
	Db::connect_mother();
    $listing = new ListRecords;
	$listing->add_filter('solution');
	$listing->set_field('domain','type','none');
	$listing->order_by = 'fiscalname ASC';
	$listing->call('records_walk','',true);
    $listing->list_records();
	Db::reconnect();
}
function records_walk($listing){
	
	$ret = array();
	$ret['domain'] = '<a href="http://' . $listing->rs['domain'] . '" target="_blank">' . $listing->rs['domain'] . '</a>';
	
	return $ret;
}
function xx_mostra_logs_user_records_walk($listing){

	if ($listing->rs['solution']){

		$use_friendly_url='';
		$consulta='';

		if ($listing->rs['host'] && $listing->rs['dbusername'] && $listing->rs['dbpwd'] && $listing->rs['dbname']){
			Db::connect($listing->rs['host'], $listing->rs['dbusername'], $listing->rs['dbpwd'], $listing->rs['dbname']);
			if (is_table("all__configpublic")){
				$use_friendly_url = Db::get_first("SELECT `value` FROM `".$listing->rs['dbname']."`.`all__configpublic` WHERE name='use_friendly_url'");
				//$use_friendly_url=$use_friendly_url?'friendly':'';
			}
			if (is_table("user__log")){
				$results = Db::get_rows("SELECT  `log_id`,  `user_id`,  `date`,  `login`,  `logout`,  `ip` FROM `user__log` where user_id=1 AND date>'2012-10-10';");

				$consulta = "<table>";
				foreach ($results as $rs){
					$consulta .='<tr>';
					foreach ($rs as $f){
						$consulta .='<td>'.$f. '</td>';
					}
					$consulta .='</tr>';
				}
				$consulta .= "</table>";
			}

			Db::reconnect();
		}

		$GLOBALS['gl_linux_aliases'] .=
		'ln -s /var/www/' . $listing->rs['domain'] . '/datos/web/' . $listing->rs['client_dir'] . ' /var/www/letnd.com/datos/clients/' .$listing->rs['solution'] . '/' . substr($listing->rs['client_dir'],8) . '<br>' .
		'ln -s /var/www/' . $listing->rs['domain'] . '/datos/web/' . $listing->rs['client_dir'] . ' /var/www/letnd.com/datos/clients/all/' . substr($listing->rs['client_dir'],8) . '<br><br>';

		//$GLOBALS['gl_linux_paths'] .=
		//'/var/www/' . $listing->rs['domain'] . '/datos/web/' . $listing->rs['client_dir'] . '<br>';


		if ($use_friendly_url){
			$dir =
			'<span style="color:red">/var/www/' . $listing->rs['domain'] . '/datos/web/</span><br>';
		}
		else{
			$dir =
			'/var/www/' . $listing->rs['domain'] . '/datos/web/<br>';
		}


		$GLOBALS['gl_linux_paths'] .=
		$dir . $consulta;
	}

	$ret = array();
	$ret['domain'] = '<a href="http://' . $listing->rs['domain'] . '" target="_blank">' . $listing->rs['domain'] . '</a>';

	return $ret;
}

function show_form()
{
	Db::connect_mother();
	$show = new ShowForm;
    $show->show_form();
	Db::reconnect();
}

function write_record()
{
	Db::connect_mother();
    $writerec = new SaveRows;
    $writerec->save();
	Db::reconnect();
}