class ReservaList {

	static init() {

		ReservaList['is_timetables-holder_opened'] = false;
		ReservaList.is_ipad = navigator.userAgent.match(/iPad/i) !== null;

		$(document).ready(
			function () {
				ReservaList.init_button_time();
				ReservaList.init_button_user_id();
				ReservaList.init_button_print();
				ReservaList.init_button_edit();
				ReservaList.init_button_delete_cart();
				$('body').append(`
					<div id="overlay" class="off-canvas-overlay"></div>
					<div id="timetables-holder-header" class="off-canvas-header timetables-container"></div>
					<div id="timetables-holder" class="off-canvas-menu timetables-container">
						<div id="timetables-holder-inner" class="off-canvas-menu-inner"></div>
					</div>`);
				ReservaList.original_menu_right = parseInt($('.off-canvas-menu').css('right'));
			}
		)
	}

	static product_init_orderitems(calendar, product_id, formatted_reservaitem_date, reservaitem_date) {

		// if (!formatted_reservaitem_date) return;

		let holder = `#orderitems_${product_id} `;
		let index = Number($(holder).data('index'));

		let product_variation_ids = '';

		// this.ordeitem_html = $(holder).html;
		$(holder).show();

		let $list_variation_holder = $('#product_variations_' + product_id);
		let $hidden_variations = $list_variation_holder.find("input[type=hidden][name*='variation[']");
		if ($hidden_variations.length > 0) {
			$hidden_variations.each(function (key, val) {
				product_variation_ids += $(this).val() + '_';
			});
			product_variation_ids = product_variation_ids.substring(0, product_variation_ids.length - 1);
		}
		if (!product_variation_ids) product_variation_ids = '0';

		let quantity = Number($('#quantity_wanted_' + product_id).val());
		let template = '';

		let template_date = formatted_reservaitem_date ? formatted_reservaitem_date : LASSDIVE_NO_DATE;

		for (let reservaitem_id = index; reservaitem_id < quantity + index; reservaitem_id++) {

			template += `
				<tr id="reservaitem_list_row_${reservaitem_id}_${product_id}">
					<td class="reservaitem_date">
						${template_date}
						<input name="product[reservaitem_id][${reservaitem_id}]" type="hidden" value="${reservaitem_id}"/>
					</td>
					<td class="start-time">
					</td>
		
					<td class="end-time">
					</td>
					<td class="centre-id">
					</td>
					<td class="nowrap user-id">
		
					</td>
					<td class="right">
						<div data-reservaitem-id="${reservaitem_id}" class="button-delete-time"></div>
					</td>
					<td class="right">
						<div data-reservaitem-id="${reservaitem_id}" class="button-time"></div>
					</td>
				</tr>
			`;

			calendar.is_reserva_first_choice = true;
			calendar.reserva_items[reservaitem_id] = {
				'reservaitem_id': reservaitem_id,
				'start_time': '',
				'end_time': '',
				'product_date': reservaitem_date,
				'reservaitem_date': reservaitem_date,
				'user_ids': '',
				'centre_id': '',
				'variation_ids': product_variation_ids
			};
		}

		// Si producte no te product_date, no mostro la taula per triar, només genero els reserva_items
		// if (reservaitem_date) $(`#orderitems_rows_${product_id}`).html(template);
		$(`#orderitems_rows_${product_id}`).html(template);

		let vars = {
			product_id: product_id,
			reservaitem_date: formatted_reservaitem_date
		};

		// TODO Poblar amb variables guardades a calendar
		ReservaList.init_button_time(`#orderitems_${product_id} `, true, vars);
		ReservaList.init_button_delete_time(`#orderitems_${product_id} `, true, vars);
	}

	static init_button_time(holder = '', is_new = false, vars = {}) {

		$(holder + '.button-time').click(function (e) {

			let reservaitem_id = $(this).data('reservaitem-id');
			ReservaList.is_new = is_new ? 1 : 0;
			ReservaList.reserva_item_vars = vars;
			ReservaList.show('timetables-holder', reservaitem_id);
		});

	}

	static init_button_delete_time(holder = '', is_new = false, vars = {}) {

		$(holder + '.button-delete-time').click(function (e) {
			let reservaitem_id = $(this).data('reservaitem-id');
			let product_id = vars['product_id'];
			let $row = $(`#reservaitem_list_row_${reservaitem_id}_${product_id}`);


			for (let id in calendar[product_id].reserva_items) {
				if ( id == reservaitem_id ){

					// Borro html
					$row.find('.start-time,.end-time,.centre-id,.user-id').html('');
					$row.find('.reservaitem_date').html(LASSDIVE_NO_DATE);

					// Borro dades
					let data = calendar[product_id].reserva_items [id];
					data['product_date'] = calendar[product_id].date_busy;
					data['reservaitem_date'] = '000/00/00';
					data['start_time'] = '';
					data['end_time'] = '';
					data['centre_id'] = '';
					data['user_ids'] = [];
				}
			}

		});

	}

	static init_button_user_id(holder = '', is_new = false, vars = {}) {

		$(holder + '.button-user-id').click(function (e) {

			let reservaitem_id = $(this).data('reservaitem-id');

			ReservaList.is_new = is_new ? 1 : 0;
			ReservaList.reserva_item_vars = vars;
			ReservaList.show_user_ids ('timetables-holder', reservaitem_id);

		});

	}

	static init_button_print() {

		$('.button-print').click(function (e) {

			let order_id = $(this).data('order-id');
			gl_reserva_print.print(order_id);
		});

	}

	static init_button_edit() {

		$('.button-edit').click(function (e) {

			let order_id = $(this).data('order-id');
			ReservaList.edit_reserva(order_id);

		});

	}

	static init_button_delete_cart() {

		$('.button-delete-cart').click(function (e) {

			let reservaitem_id = $(this).data('reservaitem-id');
			let product_id = $(this).data('product-id');
			ReservaList.delete_cart_row(reservaitem_id, product_id);

		});

	}

	static edit_reserva(order_id) {

		window.location.href = `/${gl_language}/lassdive/reserva/v/action/show-form-edit/order_id/${order_id}/`;
	}

	static set_reserva_payed(order_id){

		ReservaList.show_message_sending();

		let url = `/?language=${gl_language}&tool=lassdive&tool_section=reserva&action=set_reserva_payed&order_id=${order_id}`;
		$.ajax({
			url: url,
			dataType: 'json'
		})
			.done(function (data) {

				if (data.message) {
					ReservaList.show_message(data.message);
					$('#button-set-payed').slideUp();
				}

			});


	}

	static delete_cart_row(reservaitem_id, product_id) {

		ReservaList.show_message_sending();

		let url = `/?language=${gl_language}&tool=lassdive&tool_section=reserva&action=remove_cart_item&reservaitem_id=${reservaitem_id}&product_id=${product_id}`;
		$.ajax({
			url: url,
			dataType: 'json'
		})
			.done(function (data) {

				if (data.all_basetax) {
					$('#reservaitem_list_row_' + reservaitem_id).remove();
					$('#total_price').html(data.all_basetax + ' €');
					ReservaList.show_message();
				}
				else {
					window.location.reload();
				}

			});

	}

	static get_popup_content(item, reservaitem_id, day = false, action = 'get_timetable') {

		$('#timetables-holder-header, #timetables-holder').addClass('timetable-overlay');

		let is_admin = typeof gl_is_admin === 'undefined' ? false : gl_is_admin;
		let base_url = is_admin ? '/admin/?menu_id=2303' : `/?language=${gl_language}&tool=lassdive&tool_section=reserva`;

		let is_new = ReservaList.is_new;
		let vars = ReservaList.reserva_item_vars;

		if (!$.isEmptyObject(vars)) {
			vars['selected_reserva_items'] = calendar[vars.product_id].reserva_items;
		}

		base_url += `&action=${action}&reservaitem_id=${reservaitem_id}&is_new=${is_new}`;

		let url;
		if (day) {
			url = `${base_url}&day=${day}`;
		}
		else {
			url = `${base_url}&is_init=1`;
		}

		// ReservaList.show_message_sending();
		$(`#${item}-inner`).html('');

		$.ajax({
			url: url,
			dataType: 'json',
			data: vars,
			type: "POST"
		})
			.done(function (data) {

				let header = data.header || false;
				if (data.error) {
					data.html = `<div class="timetable_error">${data.error}</div>`;
				}
				$(`#${item}-inner`).html(data.html);
				TimetableCommon.init_tips( false );

				if (data.header) $(`#${item}-header`).html(data.header);

				ReservaObject.add_checkboxes_long('selected_user_id', "get_search_results_users");
				// jcf.replaceAll('#timetable-result');

				hidePopWin(false);
				$('#timetables-holder-header, #timetables-holder').removeClass('timetable-overlay');
			});
	}

	static show(item, reservaitem_id) {
		ReservaList.show_popup(item);
		ReservaList.get_popup_content(item, reservaitem_id, false);
	}

	static show_user_ids(item, reservaitem_id) {
		ReservaList.show_popup(item);
		ReservaList.get_popup_content(item, reservaitem_id, false, 'get_user_ids_popup');
	}

	static show_popup(item) {

		let $overlay = $('#overlay');
		let $item = $('#' + item);
		let $header = $('#' + item + '-header');

		$('#overlay,.timetables-close')
			.off()
			.click(function () {
				ReservaList.hide_popup(item);
			})
		;

		ReservaList.hide_body_scrollbar();

		$overlay.fadeIn(500);
		$header.fadeIn(500);
		$item.fadeIn(500, function () {
			$item.addClass('off-canvas-opened');
			$('#' + item + '-header').addClass('off-canvas-opened');

		});

		ReservaList['is_' + item + '_opened'] = true;
	}

	static hide_popup(item) {

		// TODO-i Al tancar des de la finestra de calendari, s'ha d'actualitzar el llistat del calendari
		if (!ReservaList['is_' + item + '_opened']) return;

		let $overlay = $('#overlay');
		let $item = $('#' + item);
		let $header = $('#' + item + '-header');

		if (ReservaList.is_ipad) {
			$('html').removeClass('off-canvas-body');
			$('body').scrollTop(ReservaList.scrollTop);
		}
		$overlay.fadeOut(500, function () {
			ReservaList.clear_overlay($item, item);
		});
		$item.fadeOut(500);
		$header.fadeOut(500);

		ReservaList['is_' + item + '_opened'] = false;
	}

	static show_message_sending (){
		showPopWin('', 300, 100, null, false, false, '<img src="/common/images/wait.png" style="margin-left:10px;vertical-align: middle;" width="32" height="32" />');
	}

	static show_message(message, timeout = 2000) {

		if (message) {
			top.window.setTimeout("hidePopWin(false);", timeout);
			top.showPopWin("", 300, 200, null, false, false, message);
		}
		else {
			top.hidePopWin(false);
		}
	}

	static hide_body_scrollbar() {

		let $body = $('body');
		ReservaList.scrollTop = $('html').scrollTop();

		// anivello haver tret la barra de desplaçament
		let body_width = $body.width();
		if (ReservaList.is_ipad) {
			ReservaList.scrollTop = $('body').scrollTop();
			$('html').addClass('off-canvas-body');
		}
		else {
			$body.addClass('off-canvas-body');
		}

		let body_width_after = $body.width();
		let body_diff = body_width_after - body_width;

		$body.css('margin-right', body_diff + 'px');
		$('#header').css('right', body_diff + 'px');
		$('.off-canvas-header,.off-canvas-menu').css('right', (body_diff + ReservaList.original_menu_right) + 'px');

	}

	static clear_overlay($item, item) {

		let $body = $('body');

		$body
			.removeClass('off-canvas-body')
			.css('margin-right', '0');
		$('#header').css('right', '0');
		$('.off-canvas-header,.off-canvas-menu').css('right', ReservaList.original_menu_right + 'px');

		$item.removeAttr('style');
		$item.removeClass('off-canvas-opened');
		$('#' + item + '-header').removeClass('off-canvas-opened');


		// reset de les hores seleccionades
		$(`#${item}-inner`).html('');
		$(`#${item}-header`).html('');
	}

	static show_advanced(obj) {
		let $obj = $(obj);
		let is_active = $obj.hasClass('active');
		$obj.toggleClass('active');
		$('.search-advanced').slideToggle();
	}
}

ReservaList.init();