<?
/**
 * AmctaicAgenda
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2010
 * @version $Id$
 * @access public
 */
class AmctaicAgenda extends Module{
	var $url1_select = '', $url1='';
	function __construct(){
		parent::__construct();
		$this->order_by  = 'entered DESC';
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function get_records()
	{
	    $listing = new ListRecords($this);
		$listing->call_function['content'] = 'add_dots';
		$listing->order_by = $this->order_by;
	    return $listing->list_records();
	}
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{
		$this->set_field('entered','default_value',now(true));
		$show = new ShowForm($this);
		$show->order_by = $this->order_by;

		$show->call('records_walk','agenda_id,url1');
	    return $show->show_form();
	}
	function records_walk($agenda_id,$url1){
		$this->url1 = $url1;
		$this->get_pages(0,1);
		$selected = $url1?'':' selected';
		$this->url1_select = 
			'<select size="20" style="width:635px" name="url1['.$agenda_id.']">
				<option value=""'.$selected.'>Cap enllaç</option>
				<optgroup label="Enllaç a pàgines">' . 
				$this->url1_select . '
				<optgroup label="">
				<optgroup label="Fòrum">' . 
				$this->get_forums('forum',17) . '
				<optgroup label="Viatges">' . 
				$this->get_forums('viatge',18) . '
				<optgroup label="Sortides">' . 
				$this->get_forums('sortida',19) . '
				<optgroup label="Notícies">' . 
				$this->get_forums('noticia',0) . '
			</select>
			<input name="old_url1['.$agenda_id.']" type="hidden" value="'.$url1.'">';		
		return array('url1' => $this->url1_select);
	}
	function get_pages($parent_id, $level){
		$loop = array();
		$query = "SELECT all__page.page_id as page_id, link, page, parent_id
						FROM all__page_language, all__page
						WHERE all__page_language.page_id = all__page.page_id
						AND status = 'public'
						AND parent_id = " . $parent_id . "
						AND bin <> 1
						AND menu = '1'
						AND language = '" . LANGUAGE . "'
						ORDER BY ordre ASC, page_id";
		Debug::add($query,'Query get_page_recursive');	
		$results = Db::get_rows($query);

		if ($results)
		{
			foreach($results as $rs)
			{
				if ($rs['link'])
				{
					$sep = strstr($rs['link'], '?')?'&':'?';
					$l['link'] = $rs['link'] . $sep;
					if ($rs['link'] != '/') // la home page no porta el page_id
					{
						$l['link'] .= '&page_id=' . $rs['page_id'];
					}
				}
				else
				{
					$l['link'] = '/?page_id=' . $rs['page_id'];
				}
				$space = '';
				for ($i = 1; $i < $level; $i++) {
					$space.='&nbsp&nbsp&nbsp';
				}				
				$l['item'] = $space . $rs['page'];
				$selected = ($this->url1 == $l['link'])?' selected':'';
				$style = $level!=1?' style="color:#7a7a7a;"':'';
				$this->url1_select.='<option value="'.$l['link'].'"'.$selected.$style.'>'.$l['item'].'</option>';
				$this->get_pages($rs['page_id'], $level+1);
			}
		}
	}
	function get_forums($tipus, $page_id){
		$query = "
			SELECT amctaic__forum.forum_id AS forum_id,tipus,forum 
				FROM amctaic__forum 
				LEFT OUTER JOIN amctaic__forum_language using (forum_id) 
			WHERE amctaic__forum.bin = 0
				AND tipus = '".$tipus."'
				AND amctaic__forum_language.language = '".LANGUAGE."'";
				
		$results = Db::get_rows($query);
		
		$ret = '';
		
		if ($results)
		{
			foreach($results as $rs)
			{
				
				if ($page_id) $page_vars='&page_id='.$page_id; else $page_vars='';
				$l['link'] = '/?tool=amctaic&tool_section=forum'.$page_vars.'&action=show_record&process='.$tipus.'&forum_id=' . $rs['forum_id'];
				$l['item'] = $rs['forum'];
				$selected = ($this->url1 == $l['link'])?' selected':'';
				$ret.='<option value="'.$l['link'].'"'.$selected.'>'.$l['item'].'</option>';
			}
		}
		return $ret;
	}
	
	function save_rows()
	{
	    $save_rows = new SaveRows($this);
	    $save_rows->save();
	}

	function write_record()
	{
	    $writerec = new SaveRows($this);
		$writerec->save();
	}

	function manage_images()
	{
	    $image_manager = new ImageManager($this);
	    $image_manager->execute();
	}
}
?>