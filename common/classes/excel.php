<?php
/**
 * Exportar Taula
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) Juny 2014
 * @version $Id$
 * @access public
 */

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Excel {
	var $table;
	var $caption = array();
	var $fields = array();
	var $visible_fields = array();
	var $column_formats = array();
	var $condition = false;
	var $file_name = 'tmp';
	var $order_by = false;
	var $id_field;
	private $module;

	function __construct( &$module = false ) {
		if ( $module ) {
			$this->table   = $module->table;
			$this->fields  = $module->fields;
			$this->id_field  = $module->id_field;
			$this->caption = $module->caption;
			$this->module  = &$module;
		}
	}

	/**
	 * Exporta a excel una taula
	 *
	 * Per mòduls simples com "contacte" executar sense paràmetres
	 *
	 * @param bool $rows Si li dono un set de resultats, agafa els noms dels camps del $rows
	 *
	 * Si paso el {@link ListRecords::results} o {@link ListRecords::loop} com a paràmetre:
	 * <ul>
	 * <li>
	 * $listing->results <p>Li dono un set the resultats sense formatejar, poso l'opció de $format_fields per que ho formategi amb les funcions especials per excel.</p>
	 * </li>
	 *
	 * <li>
	 * $listing->loop <p>Els resultats ja estan formatejats per ListRecords, el problema és amb els checkboxes que surten formatejats en html</p>
	 * </li>
	 * </ul>
	 *
	 * @param bool $format_fields Formateja els resultats, agafa les funcions creades a {@link FormObject} segons el "type" del 'field'
	 */
	public function make( $rows = false, $format_fields = true ) {

		//ini_set( 'max_execution_time', 120 );
		//ini_set( 'memory_limit', '1024M' );

		$fields      = array();
		$field_names = array();


		if ( $rows !== false ) {

			if (!$rows) return;

			$rs = $rows[0];

			foreach ( $rs as $field => $v ) {
				$fields []      = $field;
				$field_names [] = $field;
			}
		}
		else {
			$query = new Query( $this->table );
			$query->set_id_field($this->module->id_field);

			foreach ( $this->fields as $field => $val ) {
				if ( $val['type'] != 'hidden' || $field == $this->module->id_field ) {
					$fields []      = empty( $this->caption[ 'c_' . $field ] ) ? $field : $this->caption[ 'c_' . $field ];
					$field_names [] = $field;
				}
			}

			$fields_sql = implode( ',', $field_names );

			$query->order_by = $this->order_by;
			$query->limit = '5000';
			$rows            = $query->get( $fields_sql, $this->condition );
		}

		// Formatejo amb funcions de FormObject
		$this->format_fields( $rows, $format_fields );

		$this->process_results( $rows );

		$this->adjust_rows_to_visible_fields( $rows, $fields, $field_names );

		// PHPExcel_Shared_Font::setAutoSizeMethod(PHPExcel_Shared_Font::AUTOSIZE_METHOD_APPROX);

		$all_rows = array_merge( array( '0' => $fields ), $rows );


		$spreadsheet = new Spreadsheet();

		$spreadsheet->getActiveSheet()
		            ->fromArray( $all_rows );


		$highestColumn = $spreadsheet->setActiveSheetIndex( 0 )->getHighestColumn();
		$highestRow    = $spreadsheet->setActiveSheetIndex( 0 )->getHighestRow();


		$spreadsheet
			->getActiveSheet()
			->getStyle( 'a1:' . $highestColumn . '1' )
			->applyFromArray(
				array(
					'fill' => array(
						'fillType' => Fill::FILL_SOLID,
						'color'    => array( 'rgb' => 'CCCCCC' )
					)
				)
			);

		$col = 'A';
		foreach ( $field_names as $field ) {
			Debug::p( $col, 'col' );

			$objCol = $spreadsheet->getActiveSheet()->getColumnDimension( $col );


			// poso cada columna a autosize
			if ( $this->fields[ $field ]['type'] == 'textarea' ) {
				$objCol->setWidth( 70 );
			}
			else {
				$objCol->setAutoSize( true );
			}
			// fi

			// Si està definit un format per la columna sencera
			if ( ! empty ( $this->column_formats[ $field ] ) ) {

				$format = $this->column_formats[ $field ];
				$format = constant( '\PhpOffice\PhpSpreadsheet\Style\NumberFormat::' . $format );
				$spreadsheet->getActiveSheet()->getStyle( $col . '1:' . $col . $highestRow )
				            ->getNumberFormat()
				            ->setFormatCode( $format );

			}
			// fi


			$col ++;
		}

		$spreadsheet->getActiveSheet()->calculateColumnWidths();


		// redimensiono les columnes massa grans
		$col = 'A';
		foreach ( $field_names as $field ) {
			Debug::p( $col, 'col' );

			$objCol = $spreadsheet->getActiveSheet()->getColumnDimension( $col );


			if ( $this->fields[ $field ]['type'] == 'textarea' ) {

			}
			else {

				$width = $objCol->getWidth();
				Debug::p( $width, 'text' );
				if ( $width > 70 ) {
					$objCol->setAutoSize( false );
					$objCol->setWidth( 70 );
				}
			}

			$col ++;
		}

		$writer = new Xlsx( $spreadsheet );
		$writer->setPreCalculateFormulas( false );
		$writer->save( CLIENT_PATH . '/temp/' . $this->file_name . '.xlsx' );

		Main::redirect( '/' . CLIENT_DIR . '/temp/' . $this->file_name . '.xlsx' );

	}

	/**
	 * Defineix quins camps serán visibles a l'excel
	 *
	 * @param array $visible_fields Si s'omet aquest paràmetre posa automàticament els que poden ser visibles <br>
	 * Si es posa el paràmetre, agafa els de l'array. S'ha de tindre en compte que ha d'estar el camp al config com a input, text o 0 per que estigui als resultats de listing
	 * @param array $check_fields ['list_admin','list_form','list_public','list_form'] Agafa automàticament de la configuració que s'indica a l'array. Important Tindre en compte que els resultats venen del list, per tant els camps que hi ha al form, també han d'estar almenys a '0' al list_admin o list_public
	 *
	 * @return $this
	 */
	public function set_visible_fields( $visible_fields = [], $check_fields = ['list_admin'] ) {

		if ( $visible_fields ) {
			// poso amb el format 'field' => field
			$new_visible_fields = [];
			foreach ( $visible_fields as $visible_field ) {
				$new_visible_fields [$visible_field]= $visible_field;
			}
			 $visible_fields = $new_visible_fields;
		}
		else {
			foreach ( $this->fields as $field => $val ) {
				if (
					$val['type'] != 'hidden' &&
					$val['type'] != 'password' &&
					$val['type'] != 'file' &&
					$val['type'] != 'sublist'
				) {

					$is_ok = false;
					foreach ( $check_fields as $check ) {

						if (
							$val[$check] == 'text' ||
							$val[$check] == 'input' ||
							$val[$check] == 'out'
						)
						{
							$is_ok = true;
							break;
						}
					}
					if ($is_ok) $visible_fields [$field] = $field;
				}
			}
		}
		$this->visible_fields = $visible_fields;

		return $this;
	}

	public function show_fields( $fields ) {
		if (!is_array($fields)) $fields = [$fields];
		foreach ( $fields as $field ) {
			$this->visible_fields[ $field ] = $field;
		}
		return $this;
	}

	public function hide_fields( $fields ) {
		if (!is_array($fields)) $fields = [$fields];
		foreach ( $fields as $field ) {
			unset( $this->visible_fields[ $field ] );
		}
		return $this;
	}
	public function column_formats( $column_formats ) {
		$this->column_formats = $column_formats;
		return $this;
	}
	public function file_name( $file_name ) {
		$this->file_name = $file_name;
		return $this;
	}
	public function order_by( $order_by_default = '' ) {
		$order_by = R::escape( 'order_by' );
		if (!$order_by) $order_by = $order_by_default;
		$this->order_by = $order_by;
		return $this;
	}

	private function process_results( &$rows ) {
		$found_type = false;

		// Sublist
		foreach ( $this->visible_fields as $field ) {
			$cg    = $this->fields[ $field ];
			$type  = $cg['type'];
			if ( $type == 'sublist' ) {
				$rows = $this->prepare_sublist_results( $rows, $field );
			}

		}

		foreach ( $rows as &$row ) {
			foreach ( $this->visible_fields as $field ) {
				$cg    = $this->fields[ $field ];
				$type  = $cg['type'];
				$value = isset( $row[ $field ] ) ? $row[ $field ] : '';
				switch ( $type ) {
					case 'radio':
					case 'select':
					case 'select_long':
					case 'radios':
					case 'radios2':
					case 'radios3':
						$row[ $field ] = $this->get_select( $field, $value, $cg );
						$found_type    = true;
						break;
					case 'checkbox':
					case 'yesno':
						$row[ $field ] = $this->get_checkbox( $value );
						$found_type    = true;
						break;
					case 'checkboxes':
					case 'checkboxes_long':
						$row[ $field ] = $this->get_checkboxes( $row, $cg );
						$found_type    = true;
						break;
					case 'sublist':

				}
			}
			if ( ! $found_type ) break;
			$row = $this->reorder_array( $row );
		}
	}

	private function get_select( $field, $value, $cg ) {

		// if (!$value) return '';

		$select_table  = $cg['select_table'];
		$select_fields = $cg['select_fields'];
		$select_condition = $cg['select_condition'];

		if ( $select_table ) {

			$name_parts = explode( ',', $select_fields );
			$id_field   = array_shift( $name_parts );
			$query      = 'SELECT ' . implode( ',', $name_parts ) . '
				FROM ' . $select_table . '
				WHERE ' . $id_field . "='" . $value . "'";
			if ( $select_condition ) $query .= " AND " . $select_condition;
			debug::add( 'Query get_result', $query );
			$rs = Db::get_row( $query );

			if ( ! $rs ) return '';

			if ( isset( $this->caption[ 'c_' . $field . '_format' ] ) ) {
				$html = vsprintf( $this->caption[ 'c_' . $field . '_format' ], $rs );
			}
			else {
				$html = implode( " - ", $rs );
			}

			return $html;

		}
		else {
			$ret = isset( $this->caption[ 'c_' . $field . '_' . $value ] ) ?
				$this->caption[ 'c_' . $field . '_' . $value ] :
				( isset( $this->caption[ 'c_' . $value ] ) ?
					$this->caption[ 'c_' . $value ] :
					$value );

			return $ret;
		}
	}

	private function get_checkbox( $value ) {
		return $value ? $this->caption['c_yes'] : $this->caption['c_no'];
	}

	/**
	 * Només el cas 2.4 de {@link FormObject::get_checkboxes()}
	 *
	 * @param $id
	 * @param $cg
	 *
	 * @return string
	 */
	private function get_checkboxes( &$row, $cg ) {

		$checked_table = isset($cg['checked_table'])?$cg['checked_table']:false;
		$checked_fields = isset($cg['checked_fields'])?$cg['checked_fields']:false;

		$select_table  = $cg['select_table'];
		$select_fields = $cg['select_fields'];
		$input_language = isset($cg['input_language'])?$cg['input_language']:false;
		$condition = $cg['select_condition'];

		$checked_id_field = isset($cg['checked_id_field'])?$cg['checked_id_field']:$this->id_field;
		$id = $row[$checked_id_field];

		// Només el cas 2.4
		$checked_results = $this->get_checkboxes_results( $checked_table, $checked_fields, $checked_id_field . ' = ' . $id, $input_language );
		if ( ! $checked_results ) return '';
		$field           = explode( ',', $select_fields );
		$field           = $field[0];
		$condition = $condition?' AND ' . $condition:'';
		$condition = $field . ' IN (' . implode_field( $checked_results ) . ')' . $condition;
		$results         = $this->get_checkboxes_results($select_table, $select_fields, $condition, $input_language);
		$ret            = '';
		foreach ( $results as $rs ) {
			$ret .= $rs[1] . ', ';
		}

		$ret = substr($ret, 0, -2);


		return $ret;
	}

	private function get_checkboxes_results( $table, $fields, $condition, $input_language ) {

		$query = "SELECT " . $fields . " FROM " . $table;

		if ( $condition ) $query .= " WHERE " . $condition;
		if ( $input_language ) $query .= ( $condition ? " AND" : " WHERE" ) . " language='" . $input_language . "'";

		$results = Db::get_rows_array( $query );

		return $results;
	}

	// TODO-i S'ha de probar, segurament falta fer un merge dels fields del sublist amb els fields @{link LassdiveReservaExcel::prepare_sublist_results}
	public function prepare_sublist_results( &$loop, $name ) {

		$new_loop = array();
		$blank_rs    = [];
		foreach ( $this->fields as $field => $v ) {
			$blank_rs[ $field ] = '';
		}
		// unset( $blank_rs[$name] );

		foreach ( $loop as $rs ) {

			$old_rs     = $rs;
			$sublist = $rs[$name];
			// unset( $old_rs[$name] ); ja s'amaga al fer el set_visible_fields

			if ( isset( $sublist[0] ) ) {
				$rs = array_merge( $old_rs, $sublist[0] );
				$new_loop [] = $this->reorder_array ($rs);


				$other_items = array_slice( $sublist, 1 );

				foreach ( $other_items as $other_item ) {
					// trec el field_id que els relaciona, això es
					// un problema quan el id_field es visible
					// ( ja es posa a la fila primera, no el vull a la resta )
					//
					unset($other_item[$this->id_field]);
					$other_rs = array_merge($blank_rs, $other_item );
					$new_loop [] = $this->reorder_array ($other_rs);
				}

			}
			// Si no els poso tots blancs
			else {
				foreach ( $this->fields as $field => $v ) {
					if ( ! isset( $rs[ $field ] ) ) $rs[ $field ] = '';
				}
				$new_loop [] = $rs;
			}

		}
		return $new_loop;
	}

	public function reorder_array( $row ) {
		$fields  = $this->fields;
		$new_row = [];
		foreach ( $fields as $field => $val ) {
			if ( isset( $row[ $field ] ) ) $new_row[ $field ] = $row[ $field ];
		}

		return $new_row;
	}

	/**
	 * @param $rows
	 * @param $format_fields
	 *
	 */
	private function format_fields( &$rows, $format_fields ) {

		if ( ! $format_fields ) return;

		// formatejo els camps data, currency, int, etc...

		foreach ( $rows as &$rs ) {

			foreach ( $rs as $k => &$v ) {

				if ( isset( $this->fields[ $k ] ) ) {

					$func = 'format_' . $this->fields[ $k ]['type'] . '_excel';

					if ( ! is_callable( $func ) ) {
						$func = 'format_' . $this->fields[ $k ]['type'];
					}

					if ( ! is_callable( $func ) ) {
						$func = 'format_' . $this->fields[ $k ]['type'] . '_list';
					}

					// Si el valor es false, es perque he fet un subquery i he posat false expressament
					if ( is_callable( $func ) && $v !== false ) {
						$v = $func( $v );
					}

				}
			}

		}
	}

	/**
	 * @param $rows
	 * @param $fields
	 * @param $field_names
	 */
	private function adjust_rows_to_visible_fields( &$rows, &$fields, &$field_names ) {

		// Nomes uns quan camps visibles, els altres els borro, excepte el id_field
		if ( !$this->visible_fields ) return;

		// TODO-i Això reordena el camps visibles amb l'ordre de cada row, quan es posen el visible_filelds manualment com a factures, hauria de seguir l'ordre del visible fields
		$this->visible_fields = $this->reorder_array($this->visible_fields);

		$fields      = array();
		$field_names = array();
		foreach ( $this->visible_fields as $field ) {
			$fields [ $field ] = empty( $this->caption[ 'c_' . $field ] ) ? $field : $this->caption[ 'c_' . $field ];
			$field_names []    = $field;
		}

		foreach ( $rows as &$rs ) {

			foreach ( $rs as $k => &$v ) {

				if ( ! in_array( $k, $this->visible_fields ) /* && $k != $this->id_field*/ ) {
					unset( $rs[ $k ] );
				}

			}

		}
	}

}