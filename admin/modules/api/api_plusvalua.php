<?

/**
 * ApiPlusvalua
 *
 *
 *
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2011
 * @version $Id$
 * @access public
 *
 *
 * ALTER TABLE `inmo__municipi`
 * ADD COLUMN `ine_id` INT(11) NOT NULL DEFAULT '0' AFTER `habitatsoft_id`,
 * ADD COLUMN `coeficient` INT(11) NOT NULL DEFAULT '0' AFTER `ine_id`,
 * ADD COLUMN `tipus_impositiu` INT(11) NOT NULL DEFAULT '0' AFTER `coeficient`;
 *
 *
 * INSERT INTO `all__menu` (`menu_id`, `menu_group`, `tool`, `tool_section`, `action`, `parent_id`, `toolmode_id`, `variable`, `ordre`, `link`, `process`, `bin`, `read1`, `write1`) VALUES (2551, 'utility', 'api', 'plusvalua', '', 10000, 1, 'API_MENU_PLUSVALUA', 10, '', '', 0, ',1,2,', ',1,2,');
 * INSERT INTO `all__menu` (`menu_id`, `menu_group`, `tool`, `tool_section`, `action`, `parent_id`, `toolmode_id`, `variable`, `ordre`, `link`, `process`, `bin`, `read1`, `write1`) VALUES (2552, 'utility', 'api', 'plusvalua', 'show_form_new', 2551, 1, 'API_MENU_PLUSVALUA_IMPORT', 1, '', '', 0, ',1,2,', ',1,2,');
 *
 */
class ApiPlusvalua extends Module {

	var $querys = '', $cg, $import_fields, $import_exclude_fields, $import_include_fields, $import_fields_functions, $import_fields_sql, $is_import, $show_data, $show_querys;

	public function __construct() {
		parent::__construct();
		// Camps que hi ha a l'excel ( important lletres excel en majúscula )
		$this->import_fields = array(
			'A' => 'ine_id',
			'B' => 'municipi',
			'C' => 'coeficient',
			'D' => 'tipus_impositiu',
		);

		// Camps que no vulll que surtin a la consulta ( estan en una taula apart )
		$this->import_exclude_fields = array();
		// Camps que no estan a l'excel, han d'anar amb el mateix ordre en que es criden les funcions
		$this->import_include_fields = array();

		// Funcions que es criden per camp
		$this->import_fields_functions = array(
			'coeficient'      => 'to_double',
			'tipus_impositiu' => 'to_double',
		);

		$this->import_fields = array_merge( $this->import_fields, $this->import_include_fields );

		$fields = '';
		foreach ( $this->import_fields as $key => $val ) {
			// poso lletra corresponent als exclosos
			if ( isset( $this->import_exclude_fields[ $val ] ) ) {
				$this->import_exclude_fields[ $val ] = $key;
			}
			else {
				$fields .= $val . ',';
			}
		}
		$this->import_fields_sql = substr( $fields, 0, - 1 );

		Debug::p( $this->import_fields, 'import_fields' );
		//Debug::p( $this->import_exclude_fields, 'import_exclude_fields' );
	}

	public function show_form() {
		$GLOBALS['gl_content'] = $this->get_form();
	}

	public function get_form() {
		$this->set_file( '/api/import_form.tpl' );
		$this->set_vars( $this->caption );

		return $this->process();
	}


	function show_data_header() {

		$this->show_data = R::post( 'show_data' );
		if ( ! $this->show_data ) {
			return;
		}


		$ths = $tds = '';
		foreach ( $this->import_fields as $key => $val ) {
			$ths .= "<th>" . $val . "</th>";
			$tds .= "<td></td>";
		}


		set_inner_html( 'data_holder', '
				<div>
					<h6>Previsualització de coeficients a importar <span> ( prem el botó "importar" per executar l\'importació )</span></h6>
					<table>
						<thead>
							<tr>
								<th></th>
								' . $ths . '
							</tr>
						</thead>
						<tbody id="data_table">
						<tr>
								' . $tds . '
						</tr>
						</tbody>
					</table></div>' );
	}

	function show_data( &$rs, $exists ) {

		static $conta = 1;

		if ( ! $this->show_data ) {
			return;
		}

		$tds = "<td>" . $conta . "</td>";
		$conta ++;
		foreach ( $rs as $key => $val ) {
			$tds .= "<td>" . $val . "</td>";
		}

		$class_ine = $exists ? ' class="import-blau"' : ' class="vermell"';

		set_inner_html( 'data_table', '
						<tr' . $class_ine . '>							
							' . $tds . '
						</tr>', true );


		flush();
	}


	function show_querys_header() {

		$this->show_querys = R::post( 'show_querys' );
		if ( ! $this->show_querys ) {
			return;
		}


		set_inner_html( 'querys_holder', '
					<h6>Consultes</h6>
					<table>
						<tbody>
						<tr><td id="querys"></td></tr>
						</tbody>
					</table>
					' );
	}

	function show_query( $query ) {

		if ( ! $this->show_querys ) {
			return;
		}

		static $conta = 1;

		if ( $conta == 1 ) {
			print_javascript( "top.$('#querys_holder').show();" );
		}
		$conta = 2;

		set_inner_html( 'querys', $query . '
					<br>
					', true );
	}


	function show_municipis_header() {

		$ths = $tds = '';
		foreach ( $this->import_fields as $key => $val ) {
			$ths .= "<th>" . $val . "</th>";
			$tds .= "<td></td>";
		}


		set_inner_html( 'municipis_holder', '
				<div>
					<h6>Els següents municipis no es poden actualitzar, no es troben a la base de dades</h6>
					<table>
						<thead>
							<tr>
								<th></th>
								' . $ths . '
							</tr>
						</thead>
						<tbody id="municipis">
						<tr>
								' . $tds . '
						</tr>
						</tbody>
					</table></div>' );

	}

	function show_municipi( &$rs ) {

		static $conta = 1;

		if ( $conta == 1 ) {
			print_javascript( "top.$('#municipis_holder').show();" );
		}

		$tds = "<td>" . $conta . "</td>";
		$conta ++;
		foreach ( $rs as $key => $val ) {
			$tds .= "<td>" . $val . "</td>";
		}

		set_inner_html( 'municipis', '
						<tr class="vermell">							
							' . $tds . '
						</tr>', true );


		flush();
	}


	function show_errors_header() {

		$ths = $tds = '';
		foreach ( $this->import_fields as $key => $val ) {
			$ths .= "<th>" . $val . "</th>";
			$tds .= "<td></td>";
		}
		$ths .= "<th>Error</th>";
		$tds .= "<td></td>";


		set_inner_html( 'errors_holder', '
				<div>
					<h6>Hi ha hagut els següents errors</h6>
					<table>
						<thead>
							<tr>
								<th></th>
								' . $ths . '
							</tr>
						</thead>
						<tbody id="errors">
						<tr>
								' . $tds . '
						</tr>
						</tbody>
					</table></div>' );

	}

	function show_error( &$rs, $error ) {

		static $conta = 1;

		if ( $conta == 1 ) {
			print_javascript( "top.$('#errors_holder').show();" );
		}

		$tds = "<td>" . $conta . "</td>";
		$conta ++;
		foreach ( $rs as $key => $val ) {
			$tds .= "<td>" . $val . "</td>";
		}
		$tds .= "<td class='error'>" . $error . "</td>";

		set_inner_html( 'errors', '
						<tr>							
							' . $tds . '
						</tr>', true );


		flush();
	}

	function write_record() {

		Debug::p( $_POST, 'post' );
		Debug::p( $_FILES, 'files' );

		$this->reset();

		if ( $_FILES ['excel']['error'] == 4 ) {
			$GLOBALS['gl_page']->show_message( 'S\'ha de seleccionar un arxiu' );
			html_end();
		}

		if ( $_FILES ) {

			// recullo arxiu
			foreach ( $_FILES as $key => $upload_file ) {
				if ( $upload_file['error'] == 1 ) {
					global $gl_errors, $gl_reload;
					if ( ! $gl_errors['post_file_max_size'] ) {
						$gl_message                      = ' ' . $this->messages['post_file_max_size'] . (int) ini_get( 'upload_max_filesize' ) . ' Mb';
						$gl_errors['post_file_max_size'] = true;
						$gl_reload                       = true;
					}
					$this->end();
				}
			}

			// objecte excel
			$spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load( $upload_file['tmp_name'] );
			$results     = $spreadsheet->getActiveSheet()->toArray( null, true, true, true );


			// mode import o preview
			$this->is_import = isset( $_POST['import'] );
			// config de newsletter
			$this->cg = $this->get_config( 'admin', 'newsletter__configadmin' );

			/////////////////
			// IMPORT
			/////////////////

			// control de flush
			@apache_setenv( 'no-gzip', 1 );
			@ini_set( 'zlib.output_compression', 0 );
			@ini_set( 'implicit_flush', 1 );

			$this->show_data_header();
			$this->show_querys_header();
			$this->show_municipis_header();
			$this->show_errors_header();

			$conta = 1;

			foreach ( $results as $key => $rs ) {

				foreach ( $this->import_fields as $k => $import_field ) {
					$$import_field = $rs[ $k ];
				}

				if ( $key != 1 ) {

					// $exists = $this->relaciona_municipis_inicial($municipi, $ine_id); // Només per relacionar quan es va crear el mòdul

					$exists = $this->check_ine_exists( $rs, $ine_id );

					$this->add_database_record( $rs, $exists );


					if ( $exists ) $this->show_data( $rs, $exists, $ine_id );

					$conta ++;
				}
			}

			if ( $this->is_import ) {
				print_javascript( 'top.$("#confirm").hide();' );
				print_javascript( 'top.$("#done").show();' );
			}
			else {
				print_javascript( 'top.$("#confirm").show();' );
				print_javascript( 'top.$("#done").hide();' );
			}

		}
		$this->end();

	}

	function add_database_record( &$rs, $exists ) {

		if ( ! $exists ) {
			return;
		}


		// Abans de res miro que no sigui una fila buida
		$has_value = false;
		foreach ( $rs as $key => $val ) {
			if ( isset( $this->import_fields[ $key ] ) ) {
				if ( trim( $val ) != '' ) {
					$has_value = true;
					break;
				}
			}
		}
		if ( ! $has_value ) {
			$this->show_query( 'NO VALUE' );

			return;
		}

		$rs_text_id = [];
		// Primer processo els resultats, giro els keys per que així no em dependrà de la columna A, B ... sino del nom ine_id, municipi ...
		foreach ( $this->import_fields as $column => $text_id ) {
			if ( isset( $this->import_fields_functions[ $text_id ] ) ) {
				$rs_text_id[ $text_id ] = $this->{$this->import_fields_functions[ $text_id ]}( $rs, $rs[ $column ] );
			}
			else {
				$rs_text_id[ $text_id ] = $rs[ $column ];
			}
		}


		// Inserto coeficient i tipus_impositiu


		$ine_id          = $rs_text_id['ine_id'];
		$coeficient      = $rs_text_id['coeficient'];
		$tipus_impositiu = $rs_text_id['tipus_impositiu'];

		if ( $coeficient ) {
			$query = "UPDATE inmo__municipi SET coeficient = $coeficient WHERE ine_id = '$ine_id';";
			if ( $this->is_import ) {
				Db::execute( $query );
			}
			$this->show_query( $query );
		}
		else {
			$this->show_error( $rs, 'No hi ha "Coeficient" o no es un nombre' );
		}

		if ( $tipus_impositiu ) {
			$query = "UPDATE inmo__municipi SET tipus_impositiu = $tipus_impositiu WHERE ine_id = '$ine_id';";
			if ( $this->is_import ) {
				Db::execute( $query );
			}
			$this->show_query( $query );
		}
		else {
			$this->show_error( $rs, 'No hi ha "Tipus impositiu" o no es un nombre' );
		}


	}


	/*
	 *
	 * Altres funcions
	 *
	 *
	 */


	function check_ine_exists( &$rs, $ine ) {

		// sino hi ha ini no importo
		if ( ! $ine ) {
			$this->show_municipi( $rs );

			return false;
		}

		$query = "SELECT municipi_id FROM inmo__municipi WHERE ine_id = '" . $ine . "'";

		if ( Db::get_first( $query ) ) {
			return true;

		}
		$this->show_municipi( $rs );

		return false;
	}


	// Només per relacionar quan es va crear el mòdul
	/*
	function relaciona_municipis_inicial($municipi, $ine_id) {


		// sino hi ha ini no importo
		if ( ! $ine_id ) {
			$this->show_municipi("<span class='vermell'>$municipi - $ine_id</span>");
			return false;
		}

		$query = "SELECT municipi_id FROM inmo__municipi WHERE municipi = " . Db::qstr($municipi);

		if (Db::get_first( $query ) ) {
			$ine_id = Db::qstr( $ine_id );
			$query = "UPDATE inmo__municipi set ine_id=$ine_id WHERE municipi = " . Db::qstr($municipi);
			Db::execute( $query );
			$this->show_municipi("$municipi - $ine_id");
			return true;

		}
		$this->show_municipi("<span class='vermell'>$municipi - $ine_id</span>");
		return false;
	}*/

	function reset() {
		print_javascript( 'top.$("#querys_holder,#municipis_holder,#errors_holder").hide();' );
	}

	function to_double( &$rs, $val ) {
		$val = trim( $val );
		$val = (double) $val;

		return $val;
	}

	function end( $message = false ) {

		if ( $message ) {
			$GLOBALS['gl_page']->show_message( $message );
		}
		/*if ( $this->querys )
			Debug::add( 'Save rows querys', $this->querys, 2 );*/

		Debug::p_all();
		die();
	}

}