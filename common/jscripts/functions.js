function showPopWin(sUrl, nW, nH, returnFunc, showCloseBox, bTitleBar, sMessage, bFrameScroll, sContingut, sWUnits, sHUnits, showWait) {

	if (typeof(sWUnits)=='undefined') {sUnits = 'px';}
	if (typeof(sHUnits)=='undefined') {sUnits = 'px';}
	if (typeof(showWait)=='undefined') {showWait = true;}

	if (sWUnits == '%'){
		nW = $(window).width()*nW/100;
	}
	
	if (sHUnits == '%'){
		nH = $(window).height()*nH/100;
	}
	
	
	bFrameScroll = bFrameScroll?'auto':'no';
	sShadow = '<div id="popwin_bg"><div class="popwin_bg" id="popwin_bg_n"></div><div class="popwin_bg" id="popwin_bg_ne"></div><div class="popwin_bg" id="popwin_bg_e"></div><div class="popwin_bg" id="popwin_bg_se"></div><div class="popwin_bg" id="popwin_bg_s"></div><div class="popwin_bg" id="popwin_bg_sw"></div><div class="popwin_bg" id="popwin_bg_w"></div><div class="popwin_bg" id="popwin_bg_nw"></div></div>';
	sFrame = '<iframe id="popupFrame" src="'+sUrl+'" frameborder="0" name="popupFrame" style="width: '+nW+'px; height: '+nH+'px; background-color: #fff;border:none;overflow:'+bFrameScroll+'" hspace="0"></iframe>';
	sContent = sUrl?sFrame:'<table border="0" cellspacing="0" cellpadding="0" width="'+nW+'" height="'+nH+'"><tr><td valign="middle" align="center" class="modalMessage">' + sMessage + '</td></tr></table>';
	if (typeof(sContingut) != "undefined" && sContingut!=''){
	sContent = sContingut;
	}
	sContent = sShadow+sContent;
	sContent += showCloseBox?'<a class="modalCloseImg" title="'+MODALBOX_CLOSE+'" onclick="hidePopWin(false);">'+MODALBOX_CLOSE+'</a>':'';
	if (sMessage){
		nFadeIn=0;
		$.blockUI.defaults.fadeOut = 200;
	}
	else{
		nFadeIn=500;
		$.blockUI.defaults.fadeOut = 200;
	}

	var cursor = showWait ? 'wait' : 'default';
	
	$.blockUI({
		message: sContent,
		fadeIn: nFadeIn,
		css: {
			top:  ($(window).height() - nH) /2 + 'px',
			left: ($(window).width() - nW) /2 + 'px',
			padding: '0',
			margin: '0',
			width:nW + 'px',
			height:nH + 'px',
			border:	'none',
			backgroundColor:"#fff",
			cursor: cursor
		},
		overlayCSS:  {
				backgroundColor: '#000',
				opacity:         0.3,
				cursor: 'default'
			}
	});
}
function hidePopWin(callReturnFunc) {
	$.unblockUI();
	if (callReturnFunc == true && gReturnFunc != null) {
		window.setTimeout('gReturnFunc();');
	}
}


function set_focus(obj)
{

	for (x=0;x<obj.elements.length; x++)
	{
	 if ( (obj.elements[x].type.indexOf('select')!=-1)||(obj.elements[x].type == 'text')||(obj.elements[x].type == 'textarea')||(obj.elements[x].type == 'radio') )
	 {
	 //obj.elements[x].focus();
	break;
	 }
	}
}

function setDebug() {
	if (window.console) {
		window.dp = window.console.log.bind(window.console);
	} else {
		window.dp = function() {};
	}
}
setDebug();

/*function dp(error_text, error){
 if (typeof(error)=='undefined') error = error_text;
 else error = error_text+": " + error;
 if(window.console) {
 console.log(error);
 }
 }*/

function reset_height(element_name, col) {

	if (typeof(col)=='undefined') col = false;
	if (typeof(reset)=='undefined') reset = false;
	same_height(element_name, col, true, true);
	
}

// poso divs mateixa alçada	
function same_height(element_name, col, reset, reset_only) {

	if (typeof(col)=='undefined') col = false;
	if (typeof(reset)=='undefined') reset = false;
	if (typeof(reset_only)=='undefined') reset_only = false;

	var max_height = 0;
	var count = 0;

	if (col && isNaN (col)){
		$(col).each(
			function () {
				var max_height = 0;
				$(element_name, this).each(
					function () {
						if (reset) $(this).css('height', 'auto');
						var height = $(this).height();
						max_height = height > max_height ? height : max_height;
					}
				);
				if (!reset_only) {
					$(element_name, this).each(
						function () {
							$(this).height(max_height);
						}
					);
				}
			}
		);
	}
	else if (col) {
		var elements = [];

		$(element_name).each(
			function () {
				if (reset) $(this).css('height', 'auto');
				var height = $(this).height();
				max_height = height > max_height ? height : max_height;
				elements.push (this);
				count++;

				if (count%col == 0 && !reset_only){
					// dp(elements);
					for (var element in elements) {
						$(elements[element]).height(max_height);
					}
					max_height = 0;
					elements = [];
				}
			}
		);

		if (elements.length && !reset_only) {
			// dp(elements);
			for (element in elements) {
				$(elements[element]).height(max_height);
			}
		}
	}
	else {
		$(element_name).each(
			function () {
				if (reset) $(this).css('height', 'auto');
				var height = $(this).height();
				max_height = height > max_height ? height : max_height;
			}
		);
		if (!reset_only) {
			$(element_name).each(
				function () {
					$(this).height(max_height);
				}
			);
		}
	}
}

function get_viewport() {

	var  win = typeof window != 'undefined' && window,
		doc = typeof document != 'undefined' && document,
		docElem = doc && doc.documentElement,
		viewportW = function() {
			var a = docElem['clientWidth'], b = win['innerWidth'];
			return a < b ? b : a;
		  },
		viewportH = function() {
			var a = docElem['clientHeight'], b = win['innerHeight'];
			return a < b ? b : a;
		  };
	
	var w = viewportW(),
		h = viewportH();
		
    return { width :  w, height :  h};
}
function get_media_width() {
	$('#responsive').css('display','block');
	var media_width = window.getComputedStyle(document.getElementById('responsive'), ':after')
           .getPropertyValue('content');
	media_width  = Number(media_width.substring(1,media_width.length-1));
	$('#responsive').removeAttr('style');
	return media_width;
}

function set_input_placeholders(inputs, boto){	
	
	if (typeof(boto)=='undefined') {
		var form = inputs;
		inputs = {};
		boto = $('#' + form + ' input[type="submit"]');
		
		$('#' + form + ' input[type="text"], #' + form + ' input[type="password"], #' + form + ' textarea').each(function(index) {
			caption = $('#' + this.id + '_caption').text();
			add_input_placeholder(this.id, caption);
			inputs[this.id] = caption;
		});
	}
	else{
		for (id in inputs){
		
			add_input_placeholder(id, inputs[id]);						
			
		}
		boto = $('#'+boto);
	}
	boto.click(
		function(){
			for (id in inputs){	
				var val = $('#'+id).val();
				var caption = inputs[id];
				if (val == caption) $('#'+id).val('');
			}			
		}
	);
}

function add_input_placeholder(id, caption){

	$('#'+id)
			.val(caption)
			.focus( function (){
				var val = $(this).val();
				if (val == caption) $(this).val('');
			})
			.blur( function (){
				var val = $(this).val();
				if (val == '') $(this).val(caption);
			})
	;
	
}