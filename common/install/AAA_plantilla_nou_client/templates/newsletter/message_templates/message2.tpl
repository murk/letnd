<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<base href="<?= $url ?>">
	<?include('styles.tpl')?>
</head>
<body>

<table id="letnd-table-content">
	<tr>
		<td id="letnd-td-content" align="center">

			<? include( 'header.tpl' ) ?>

			<? if ( $message || $pictures ): ?>
				<table id="letnd-content" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td class="letnd-content letnd-content-m2">
							<?= $message ?>
						</td>
					</tr>
					<? if ( $pictures ): ?>
						<? foreach ( $pictures as $picture ): extract( $picture ) ?>
							<tr>
								<td class="letnd-picture letnd-picture-m2">
									<img width="650" src="<?= $picture_src ?>">
								</td>
							</tr>
						<? endforeach //$images?>
					<? endif //pictures?>
				</table>
			<? endif //message?>

			<? if ( $content_extra ): ?>
				<table id="letnd-content_extra" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td>
							<?= $content_extra ?>
						</td>
					</tr>
				</table>
			<? endif //content_extra?>

			<? include( 'footer.tpl' ) ?>

		</td>
	</tr>
</table>

</body>
</html>