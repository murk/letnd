<?

/**
 * ApiAgentFilecat
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2011
 * @version $Id$
 * @access public
 */
class ApiAgentFilecat extends Module {

	public function __construct() {
		parent::__construct();
	}

	public function list_records() {
		$GLOBALS['gl_content'] = $this->get_records();
	}

	private function get_records() {
		$listing          = new ListRecords( $this );
		$listing->has_bin = false;

		$language           = LANGUAGE;
		$listing->condition = "language = '$language'";
		$listing->order_by  = "ordre ASC, filecat ASC";

		return $listing->list_records();
	}

	public function show_form() {
		$GLOBALS['gl_content'] = $this->get_form();
	}

	private function get_form() {
		$show          = new ShowForm( $this );
		$show->has_bin = false;

		return $show->show_form();
	}

	public function save_rows() {
		$save_rows = new SaveRows( $this );

		$ids = $save_rows->id;

		// S'ha de fer abans no esborri el registre
		if ( $this->action == 'save_rows_delete_selected' ) {

			// TODO-i Comprovar que no tingui registres relacionats

			foreach ( $ids as $id ) {
				$this->delete_language_rows( $id );
			}
		}

		$save_rows->save();

		// S'ha de fer després de guardar
		if ( $this->action == 'save_rows_save_records' ) {
			foreach ( $ids as $id ) {
				$this->save_language_rows( $id );
			}
		}

	}

	public function write_record() {
		$writerec = new SaveRows( $this );

		$id = $writerec->id;

		// S'ha de fer abans no esborri el registre
		if ( $this->action == 'delete_record' ) {

			// TODO-i Comprovar que no tingui registres relacionats

			$this->delete_language_rows( $id );
		}

		$writerec->save();

		// Torno ha agafar l'id, si es add_record canvia al nou id
		$id = $writerec->id;

		if ( $this->action == 'add_record' ) {

			$language = LANGUAGE;
			$query    = "
					UPDATE api__agent_filecat 
					SET language = '$language', filecat_id = $id 
					WHERE agent_filecat_id = $id";
			Db::execute( $query );

			$this->add_language_rows( $id );

		} elseif ( $this->action == 'save_record' ) {

			$this->save_language_rows( $id );

		}
	}

	private function get_filecat_rs( $id ) {

		$language = LANGUAGE;
		$query    = "
			SELECT * 
			FROM api__agent_filecat 
			WHERE agent_filecat_id = $id 
			AND language = '$language'";
		$rs       = Db::get_row( $query );

		return $rs;
	}

	private function add_language_rows( $id ) {

		global $gl_languages;

		$rs = $this->get_filecat_rs( $id );

		$filecat    = Db::qstr( $rs['filecat'] );
		$ordre      = $rs['ordre'];

		// Poso com a filecat_id, l'id i es el mateix en tots els idiomes
		foreach ( $gl_languages['public'] as $lang ) {
			if ( $lang != LANGUAGE ) {
				$query = "
					INSERT INTO `api`.`api__agent_filecat` (`filecat_id`, `language`, `filecat`, `ordre`) 
					VALUES ('$id', '$lang', $filecat, '$ordre');";
				Db::execute( $query );
			}
		}

	}

	private function save_language_rows( $id ) {

		$rs = $this->get_filecat_rs( $id );

		$filecat_id = $rs['filecat_id'];
		$filecat    = Db::qstr( $rs['filecat'] );
		$ordre      = $rs['ordre'];

		$query = "
				UPDATE api__agent_filecat 
				SET filecat =  $filecat, ordre = '$ordre' 
				WHERE filecat_id = $filecat_id";

		Db::execute( $query );

	}

	private function delete_language_rows( $id ) {

		$rs = $this->get_filecat_rs( $id );

		$filecat_id = $rs['filecat_id'];

		$query = "
				DELETE FROM api__agent_filecat
				WHERE filecat_id = $filecat_id";

		Db::execute( $query );

	}

	public function manage_images() {
		$image_manager = new ImageManager( $this );
		$image_manager->execute();
	}
}