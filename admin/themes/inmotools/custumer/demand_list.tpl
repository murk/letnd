
<? if(!$results): ?>
	<? if($options_filters): ?>
		<div id='opcions_holder'>
			<table width="100%" border="0" cellspacing="0" cellpadding="0" id="opcions">
				<tr>
					<td class="opcionsTitol"><strong><?= $c_options_bar ?></strong></td>
				</tr>
				<tr>
					<td class="opcions">
						<table class="filters">
							<tr valign="middle">
								<? foreach ($options_filters as $options_filter): ?>
								<form>
									<td><?= $options_filter ?></td>
									<? endforeach //$options_filters?>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>
	<? endif //options_filters?>
<? endif //!$results?>
<? if($results): ?>
	<div id='split_holder'>
		<table width="100%" border="0" cellpadding="0" cellspacing="0" id="split">
			<tr valign="middle">
				<td width="20%" align="left" nowrap class="pageResults"><?= $split_results_count ?></td>
				<td align="right" nowrap class="pageResults"><?= $split_results ?></td>
			</tr>
		</table>
	</div>
	<? if($options_bar): ?>
		<div id='opcions_holder'>
			<table width="100%" border="0" cellspacing="0" cellpadding="0" id="opcions">
				<tr>
					<td class="opcionsTitol"><strong>
							<?= $c_options_bar ?>
						</strong></td>
				</tr>
				<tr>
					<td class="opcions">
						<div id="opcions_fixed_bar">
							<div class="more"></div>
						</div>
						<table class="filters">
							<tr valign="middle">
								<? foreach($options_move_tos as $options_move_to): ?>
									<td><?= $options_move_to ?></td>
								<? endforeach //$options_move_tos?>
								<? foreach($options_filters as $options_filter): ?>
									<form>
									<td valign="top"><?= $options_filter ?></td></form>
								<? endforeach //$options_filters?>
							</tr>
						</table><? if($options_filters): ?>
							<div class="sep"></div><? endif //options_filters?>
						<table border="0" cellpadding="2" cellspacing="0">
							<tr valign="middle">
								<? if($save_button): ?>
									<td>
										<strong><a class="botoOpcionsGuardar" href="javascript:submit_form_save('save_rows_save_records<?= $action_add ?>')">
												<?= $c_send_edit ?>
											</a></strong></td>
								<? endif //save_button?>
								<? if($select_button): ?>
									<td><?= $c_select ?>
										&nbsp;<strong><a class="opcions" href="javascript:select_all(true, document.list)">
												<?= $c_all ?>
											</a>&nbsp;-&nbsp;<a class="opcions" href="javascript:select_all(false, document.list)">
												<?= $c_nothing ?>
											</a></strong></td>
								<? endif //select_button?>
								<? if($delete_button): ?>
									<? if($has_bin): ?>
										<? if($is_bin): ?>
											<td>
												<strong><a class="botoOpcions" href="javascript:submit_list('save_rows_delete_selected<?= $action_add ?>', '<?= $c_confirm_deleted ?>', true)">
														<?= $c_delete_selected ?>
													</a></strong></td>
											<td>
												<strong><a class="botoOpcions" href="javascript:submit_list('save_rows_restore_selected<?= $action_add ?>')">
														<?= $c_restore_selected ?>
													</a></strong></td>
										<? else: ?>
											<td>
												<strong><a class="botoOpcionsBorrar" href="javascript:submit_list('save_rows_bin_selected<?= $action_add ?>')">
														<?= $c_delete_selected ?>
													</a></strong></td>
										<? endif // is_bin?>
									<? else: ?>
										<td>
											<strong><a class="botoOpcionsBorrar" href="javascript:submit_list('save_rows_delete_selected<?= $action_add ?>', '<?= $c_confirm_deleted ?>', true)">
													<?= $c_delete_selected ?>
												</a></strong></td>
										<? /*<td>
              <td>&nbsp;</td><strong><a class="botoOpcions" href="/admin?menu_id=<?=$menu_id?>&sort_by=0&sort_order=<?=$sort_order0?>">
                <?=$c_move_to?>
               
            <input type="submit" name="bin_selected" value="<?=$c_delete_selected?>" class="boto1">
              </a></strong></td>*/ ?>
									<? endif // has_bin?>
								<? endif //delete_button?>
								<? if($print_button): ?>
									<td><strong><a class="botoOpcions" href="<?= $print_link ?>">
												<?= $c_print_all ?>
											</a></strong></td>
								<? endif //delete_button?>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>
	<? endif //$options_bar?>
	<? if($order_bar): ?>
		<div class="sep"></div>
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td height="10"></td>
			</tr>
		</table>
		<table width="100%" border="0" cellspacing="0" cellpadding="0" class="listRecord">
			<tr>
				<td class="listCaption">
					<table border="0" cellspacing="0" cellpadding="0" style="margin-bottom:14px">
						<tr>
							<td valign="top" nowrap="nowrap"><strong> &nbsp;
									<?= $c_order_by ?>
									&nbsp;&nbsp; </strong></td>
							<td valign="top"><? foreach($fields as $l): extract( $l ) ?><strong>
									<a class="opcions" href="<?= $order_link ?>">
										<?= $field ?>
									</a></strong>
									<? if($order_image): ?>
										<img src="/admin/themes/inmotools/images/<?= $order_image ?>.gif" width="9" height="9" border="0" align="middle"/>
									<? endif //order_image?>
									&nbsp;
								<? endforeach //$field?></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	<? else: //$order_bar?>
		<table width="100%" border="0" cellspacing="0" cellpadding="0" class="listRecord">
			<tr>
				<td style="padding:0px"></td>
			</tr>
		</table>
	<? endif //$order_bar?>
	<form method="post" action="<?= $form_link ?>" name="list" onsubmit="return check_selected(this)" target="save_frame">
		<input name="action" type="hidden" value="save_rows"><?= $out_loop_hidden ?>
		<table width="100%" border="0" cellspacing="0" cellpadding="0" class="listRecord">
			<? foreach($loop as $l): extract( $l ) ?>
				<tr class="listItem<?= $odd_even ?>" onmouseout="change_bg_color(this, '<?= $odd_even ?>', '<?= $id ?>')" id="list_row_<?= $id ?>">
					<td valign="top">
						<table width="100%" border="0" cellspacing="0" cellpadding="0" class="noPadding">
							<tr>
								<td>
									<table border="0" cellspacing="0" cellpadding="0">
										<tr><? if($options_checkboxes): ?>
												<td>
												<input name="selected[<?= $conta ?>]" type="checkbox" value="<?= $id ?>" onclick="return false;"<?= $selected ?>/>
												</td><? endif // options_checkboxes?>
											<td class="listTitle"><strong>
													<input name="demand_id[<?= $demand_id ?>]" type="hidden" value="<?= $demand_id ?>"/>&nbsp;
													<?= $c_demand_id; ?>
													<?= $demand_id ?>
												</strong></td>
											<td><? if(!$is_tool_custumer): ?> <?php if($name || $surname1 || $surname2) : ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
													<strong><?= $c_custumer_id ?>:</strong> <?= $name ?>
													<?= $surname1 ?>
													<?= $surname2 ?><?php endif //name?>
													<?php if($phone1 || $phone2 || $phone3) : ?><strong><?= $c_phone ?>
														:</strong><?php endif //phone1?>
													<?= $phone1 ?><?php if($phone1 && $phone2) : ?> | <?php endif //phone1?>
													<?= $phone2 ?><?php if($phone2 && $phone3) : ?> | <?php endif //phone1?>
													<?= $phone3 ?><? endif //is_tool_custumer?></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td>
									<table width="100%" border="0" cellpadding="0" cellspacing="0" style="padding-right:8px">
										<tr>
											<td valign="top">
												<table width="100%" border="0" cellpadding="0" cellspacing="0" class="listDetails2">
													<tr class="listDetails">
														<td valign="top"><strong class="listItem"><?= $c_entered ?>: </strong>&nbsp;</td>
														<td valign="top"><?= $entered ?></td>
													</tr>
													<tr>
														<td valign="top"><strong class="listItem"><?= $c_tipus ?>: </strong>&nbsp;</td>
														<td valign="top"><?= $tipus ?></td>
													</tr>
													<? if($tipus2_value): ?>
														<tr>
														<td valign="top"><strong class="listItem"><?= $c_tipus2 ?>: </strong>&nbsp;</td>
														<td valign="top"><?= $tipus2 ?></td>
													</tr>
													<? endif // $tipus2_value ?>
													<tr>
														<td valign="top"><strong class="listItem"><?= $c_category ?>: </strong>&nbsp;</td>
														<td valign="top"><?= $category_id ?></td>
													</tr>
													<? if($floor_space): ?>
													<tr>
														<td valign="top"><strong class="listItem"><?= $c_floor_space ?>: </strong>&nbsp;</td>
														<td valign="top"><?= $floor_space ?></td>
													</tr>
													<? endif // $floor_space ?>
													<? if($land): ?>
														<tr>
														<td valign="top"><strong class="listItem"><?= $c_land ?>: </strong>&nbsp;</td>
														<td valign="top"><?= $land ?>
															&nbsp;<? if($land): ?><?= $land_units ?><? endif //land?></td>
													</tr>
													<? endif // $land ?>
													<? if($municipi_id): ?>
														<tr>
														<td valign="top"><strong class="listItem"><?= $c_comarca_id ?> / <?= $c_municipi_id ?>: </strong>&nbsp;</td>
														<td valign="top"><?= $municipi_id ?></td>
													</tr>
													<? endif // $municipi_id ?>
													<tr>
														<td valign="top"><strong class="listItem"><?= $c_maxprice ?>: </strong>&nbsp;</td>
														<td valign="top">
															<strong class="resaltatSmall"><?= $maxprice ?></strong></td>
													</tr>

												</table>
											</td>
											<td valign="top" class="padding-left-2">
												<table width="100%" border="0" cellpadding="0" cellspacing="0" class="listDetails2">
													<?= demand_get_rows_details ( $this, $l ) ?>
												</table>
											</td>
											<td width="300" valign="top" class="listDescription"><? if($observations): ?>
													<strong><?= $c_observations ?></strong>
													<br/><?= $observations ?><? endif //observations?></td>
										</tr>
									</table>
									<table width="100%" border="0" cellspacing="0" cellpadding="0" class="noPadding">
										<tr>
											<td align="right">
												<div class="right"><? foreach($buttons as $button): ?><?= $button ?><? endforeach //$buttons?></div>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			<? endforeach //$loop?>

		</table>
	</form>
	<? if($order_bar): ?>
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td height="10"></td>
			</tr>
		</table>
		<table width="100%" border="0" cellspacing="0" cellpadding="0" class="listRecord">
			<tr>
				<td class="listCaption">
					<table border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td valign="top" nowrap="nowrap"><strong> &nbsp;
									<?= $c_order_by ?>
									&nbsp;&nbsp; </strong></td>
							<td valign="top"><? foreach($fields as $l): extract( $l ) ?><strong>
									<a class="opcions" href="<?= $order_link ?>">
										<?= $field ?>
									</a></strong>
									<? if($order_image): ?>
										<img src="/admin/themes/inmotools/images/<?= $order_image ?>.gif" width="9" height="9" border="0" align="middle"/>
									<? endif //order_image?>
									&nbsp;
								<? endforeach //$field?></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	<? endif //$order_bar?>
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td height="10"></td>
		</tr>
	</table>
	<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr valign="middle">
			<td width="20%" height="" align="left" nowrap class="pageResults"><?= $split_results_count ?></td>
			<td align="right" nowrap class="pageResults"><?= $split_results ?></td>
		</tr>
	</table>
<? endif //results?>