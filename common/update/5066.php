<?

/* 20-02-13 
  Modificacions generals adressa

 */

if (is_table('all__configpublic') && !Db::get_first("SELECT * FROM all__configpublic WHERE name='page_numstreet'" )) {	
	
	$update_sql = "
		INSERT INTO all__configpublic (`name`, `value`) VALUES ('page_numstreet', '');
		INSERT INTO all__configpublic (`name`, `value`) VALUES ('page_flat', '');
		INSERT INTO all__configpublic (`name`, `value`) VALUES ('page_door', '');
		INSERT INTO all__configpublic (`name`, `value`) VALUES ('page_postcode', '');
		INSERT INTO all__configpublic (`name`, `value`) VALUES ('page_municipi', '');
		INSERT INTO all__configpublic (`name`, `value`) VALUES ('page_provincia', '');
		INSERT INTO all__configpublic (`name`, `value`) VALUES ('page_country', '');
		INSERT INTO all__configpublic (`name`, `value`) VALUES ('page_fax', '');
		";
	
	global $gl_languages;
	
	foreach($gl_languages['names'] as $lang=>$val){
		$update_sql.="INSERT INTO all__configpublic_language (`name`, `language`) VALUES ('page_country', '".$lang."');";
	}
	
}


?>