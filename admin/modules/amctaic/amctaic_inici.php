<?
/**
 * AmctaicInici
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2010
 * @version $Id$
 * @access public
 */
class AmctaicInici extends Module{
	function __construct(){
		parent::__construct();
	}
	function write_record()
	{
	    $writerec = new SaveRows($this);
	    $writerec->save();
	}

	function manage_images()
	{
		if ($this->action == 'list_records_images'){
			$frase = Db::get_first("SELECT frase FROM amctaic__inici WHERE inici_id=1");
			$frase = '<textarea name="frase[1]" id="frase[1]" cols="90" rows="15">'.$frase.'</textarea>';
			$this->set_var('frase',$frase);
		}
		if ($this->action == 'save_rows_save_records_images'){
			$sql = "UPDATE amctaic__inici SET frase=".Db::qstr($_POST['frase']['1'])." WHERE inici_id=1";
			Db::execute($sql);
			//echo $sql;
		}
		
		$this->config = array_merge ($this->config, array (
		  'im_big_w' => '800',
		  'im_big_h' => '',
		  'im_details_w' => '468',
		  'im_details_h' => '',
		  'im_medium_w' => '1100',
		  'im_medium_h' => '',
		)
		);
		
		$_GET['inici_id'] = '1';
	    $image_manager = new ImageManager($this);
	    $image_manager->execute();
	}
}
?>