<?php

/* 30-10-15
  
 * Portfoli: Afegir nous camps personalitzats, i opcions de text, text amb idiomes, textarea, nombre i casella verificació

 */


if (is_table('portfoli__portfoli')){
	
	$update_sql[] = "
		ALTER TABLE `portfoli__configadmin`
	DROP COLUMN `language`;
	";
	$update_sql[] = "
		CREATE TABLE `portfoli__configadmin_language` (
			`configadmin_id` INT(11) NOT NULL DEFAULT '0',
			`name` VARCHAR(50) NOT NULL DEFAULT '' COLLATE 'utf8_spanish2_ci',
			`value` TEXT NOT NULL COLLATE 'utf8_spanish2_ci',
			`language` CHAR(3) NOT NULL DEFAULT '' COLLATE 'utf8_spanish2_ci'
		)
		COLLATE='utf8_spanish2_ci'
		ENGINE=MyISAM
	";
	$update_sql[] = "
					ALTER TABLE `portfoli__portfoli`
						ADD COLUMN `others8` VARCHAR(2000) NOT NULL AFTER `destacat`,
						ADD COLUMN `others7` VARCHAR(2000) NOT NULL AFTER `destacat`,
						ADD COLUMN `others6` VARCHAR(2000) NOT NULL AFTER `destacat`,
						ADD COLUMN `others5` VARCHAR(2000) NOT NULL AFTER `destacat`,
						ADD COLUMN `others4` VARCHAR(2000) NOT NULL AFTER `destacat`,
						ADD COLUMN `others3` VARCHAR(2000) NOT NULL AFTER `destacat`,
						ADD COLUMN `others2` VARCHAR(2000) NOT NULL AFTER `destacat`,
						ADD COLUMN `others1` VARCHAR(2000) NOT NULL AFTER `destacat`;
					";

	$update_sql[] = "
					ALTER TABLE `portfoli__portfoli_language`
						ADD COLUMN `others8` VARCHAR(2000) NOT NULL DEFAULT '' AFTER `portfoli_old_file_name`,
						ADD COLUMN `others7` VARCHAR(2000) NOT NULL DEFAULT '' AFTER `portfoli_old_file_name`,
						ADD COLUMN `others6` VARCHAR(2000) NOT NULL DEFAULT '' AFTER `portfoli_old_file_name`,
						ADD COLUMN `others5` VARCHAR(2000) NOT NULL DEFAULT '' AFTER `portfoli_old_file_name`,
						ADD COLUMN `others4` VARCHAR(2000) NOT NULL DEFAULT '' AFTER `portfoli_old_file_name`,
						ADD COLUMN `others3` VARCHAR(2000) NOT NULL DEFAULT '' AFTER `portfoli_old_file_name`,
						ADD COLUMN `others2` VARCHAR(2000) NOT NULL DEFAULT '' AFTER `portfoli_old_file_name`,
						ADD COLUMN `others1` VARCHAR(2000) NOT NULL DEFAULT '' AFTER `portfoli_old_file_name`;
					";	


	$update_sql[]="
			INSERT INTO `portfoli__configadmin` (`name`)
			VALUES ('others1');";

	$update_sql[]="
			INSERT INTO `portfoli__configadmin` (`name`,value)
			VALUES ('others1_type','null');";

	$update_sql[]="
			INSERT INTO `portfoli__configadmin` (`name`,value)
			VALUES ('others1_order','');";

	$update_sql[]="
			INSERT INTO `portfoli__configadmin` (`name`)
			VALUES ('others2');";

	$update_sql[]="
			INSERT INTO `portfoli__configadmin` (`name`, value)
			VALUES ('others2_type','null');";

	$update_sql[]="
			INSERT INTO `portfoli__configadmin` (`name`,value)
			VALUES ('others2_order','');";

	$update_sql[]="
			INSERT INTO `portfoli__configadmin` (`name`)
			VALUES ('others3');";

	$update_sql[]="
			INSERT INTO `portfoli__configadmin` (`name`,value)
			VALUES ('others3_type','null');";

	$update_sql[]="
			INSERT INTO `portfoli__configadmin` (`name`,value)
			VALUES ('others3_order','');";

	$update_sql[]="
			INSERT INTO `portfoli__configadmin` (`name`)
			VALUES ('others4');";

	$update_sql[]="
			INSERT INTO `portfoli__configadmin` (`name`,value)
			VALUES ('others4_type','null');";

	$update_sql[]="
			INSERT INTO `portfoli__configadmin` (`name`,value)
			VALUES ('others4_order','');";

	$update_sql[]="
			INSERT INTO `portfoli__configadmin` (`name`)
			VALUES ('others5');";
	$update_sql[]="
			INSERT INTO `portfoli__configadmin` (`name`,value)
			VALUES ('others5_type','null');";

	$update_sql[]="
			INSERT INTO `portfoli__configadmin` (`name`,value)
			VALUES ('others5_order','');";

	$update_sql[]="
			INSERT INTO `portfoli__configadmin` (`name`)
			VALUES ('others6');";
	$update_sql[]="
			INSERT INTO `portfoli__configadmin` (`name`,value)
			VALUES ('others6_type','null');";

	$update_sql[]="
			INSERT INTO `portfoli__configadmin` (`name`,value)
			VALUES ('others6_order','');";

	$update_sql[]="
			INSERT INTO `portfoli__configadmin` (`name`)
			VALUES ('others7');";
	$update_sql[]="
			INSERT INTO `portfoli__configadmin` (`name`,value)
			VALUES ('others7_type','null');";

	$update_sql[]="
			INSERT INTO `portfoli__configadmin` (`name`,value)
			VALUES ('others7_order','');";

	$update_sql[]="
			INSERT INTO `portfoli__configadmin` (`name`)
			VALUES ('others8');";
	$update_sql[]="
			INSERT INTO `portfoli__configadmin` (`name`,value)
			VALUES ('others8_type','null');";

	$update_sql[]="
			INSERT INTO `portfoli__configadmin` (`name`,value)
			VALUES ('others8_order','');";
	
	
	
	global $gl_languages;	
	
	foreach($gl_languages['names'] as $lang=>$val){
		$update_sql[]="
			INSERT INTO `portfoli__configadmin_language` (`name`, `language`)
			VALUES ('others1', '".$lang."');";
		$update_sql[]="
			INSERT INTO `portfoli__configadmin_language` (`name`, `language`)
			VALUES ('others2', '".$lang."');";
		$update_sql[]="
			INSERT INTO `portfoli__configadmin_language` (`name`, `language`)
			VALUES ('others3', '".$lang."');";
		$update_sql[]="
			INSERT INTO `portfoli__configadmin_language` (`name`, `language`)
			VALUES ('others4', '".$lang."');";
		$update_sql[]="
			INSERT INTO `portfoli__configadmin_language` (`name`, `language`)
			VALUES ('others5', '".$lang."');";
		$update_sql[]="
			INSERT INTO `portfoli__configadmin_language` (`name`, `language`)
			VALUES ('others6', '".$lang."');";
		$update_sql[]="
			INSERT INTO `portfoli__configadmin_language` (`name`, `language`)
			VALUES ('others7', '".$lang."');";
		$update_sql[]="
			INSERT INTO `portfoli__configadmin_language` (`name`, `language`)
			VALUES ('others8', '".$lang."');";
	}
	
}

?>