<?php
$GLOBALS['gl_order_by'] = 'ref desc, entered desc';
function show_private_button()
{
    global $gl_page, $gl_caption;
    $layers = isset($_COOKIE['private'])?'<style>.private, #hide_private, a.boto1_private{display:none;}</style>':'<style>.not_private,#show_private{display:none;}</style>';
    $gl_page->title_right = $layers . '<div id="hide_private">' . $gl_caption['c_private'] . '</div><div id="show_private"><</div>';
}
function list_records_property( &$listing, $return_listing = false )
{	
	
    global $inmo_list, $gl_action, $gl_process, $gl_page, $tpl, $gl_template, $gl_db_classes_fields, $gl_caption;
    $listing->call_function['description'] = 'add_dots';
    $listing->order_by = $GLOBALS['gl_order_by'];

	$listing->set_field_options(array(
		'images',
		'ref',
		'property_private',
		'address',
		'entered',
		'category_id',
		'tipus',
		'person',
		'floor_space',
		'price',
		'description',
		'user_id'
	));

	$is_mls = !empty($_SESSION['inmo_show_mls']);

	$listing->set_var( 'is_portal', false );
	$listing->set_var( 'is_mls', $is_mls );
	$listing->set_var( 'show_mls_button', true );

	// permisos usuaris
	$listing->condition .= inmo_get_user_permission($listing->condition);

	if ( $is_mls ) {
		Db::connect_mother();
		inmo_add_mls_config_fields($listing);
		$listing->set_field( 'status', 'list_admin', 'text' );
		$listing->set_field( 'property_private', 'not_sortable', '1' );
		$listing->set_field( 'user_id', 'list_admin', 'no' );
		$listing->set_var( 'user_id', false );
		$listing->set_options(1, 0, 0, 1, 0, 1, 0, 0, 1, 1);
	}


    // buscador
	$gl_caption['c_room_0'] = $gl_caption['c_filter_room_0'];
    switch ($gl_action)
    {
        case 'list_records_search_list':
            $listing->condition .= search_list_condition($listing);
            $listing->add_filter('category_id');
            $listing->add_filter('tipus');
            $listing->add_filter('room');
            $listing->add_filter('status');
			
            break;
        case 'list_records_search_search':
            $listing->condition .= search_search_condition($listing);
            $listing->add_filter('category_id');
            $listing->add_filter('tipus');
            $listing->add_filter('room');
            $listing->add_filter('status');
            break;
        case 'list_records_search_filter':
            $listing->condition .= search_filter_condition($listing);
            break;
        case 'list_records_book_search':
            $book_condition = search_book_condition($listing, '');
			
			// si no hi ha condició no mostro cap resultat 
			// ( quan encara no s'ha buscat res, nomes mostro el form de cerca )
			if (!$book_condition) {
				show_search_form(false);
				return;
			}
			$listing->condition .= $book_condition;
            $listing->add_filter('category_id');
            $listing->add_filter('room');
			$listing->set_options(0,0,0,0,0,1,0,0);
            break;
        default:
            $listing->add_filter('category_id');
            $listing->add_filter('tipus');
            $listing->add_filter('room');
            $listing->add_filter('status');
			$listing->add_filter('home');
			$listing->add_filter('offer');
            if (!inmo_view_only_itself()) $listing->add_filter('user_id');
			
			//if ($listing->config['custom1_filter']=='1') $listing->add_filter('custom1');
			
    } // switch
    if ($gl_page->template == 'print.tpl')
    {
        $listing->template = 'inmo/property_list_print';
    }

    $listing->call('get_ref', 'ref,category_id,land,land_units,price,price_private,property_id,municipi_id,tipus,garden,courtyard');


	if ( $is_mls ) {
		if ($listing->condition)
			$listing->condition .= ' AND ';
		$listing->condition .= "client_id IN (" . inmo_companies_with_permission () . ")";
		$listing->list_records(false);

		foreach ( $listing->results as &$rs ) {
			$rs = inmo_get_mls_fields($rs);
		}


		$listing-> parse_template();

		Db::reconnect();
	} elseif ( $return_listing ) {
		return $listing;
	}
	else{
		$listing->list_records();
	}

    if (($gl_page->template != 'print.tpl') && ($gl_process != 'tv_download') && ($gl_process != 'get_flash') && ($gl_process != 'get_flash_data'))
    {
        show_search_form($listing->has_results);
    }
}
function show_form_property()
{
    
	
	global $inmo_form, $tpl, $gl_action, $gl_write, $gl_config, $gl_caption;
    // mostrar tabs per apartat contract o custumer
    $tpl->set_var('show_tabs', isset($_GET['show_tabs'])?$_GET['show_tabs']:true);
    $tpl->set_var('edit_only_descriptions', $_SESSION['edit_only_descriptions']);
	
	
	if ($gl_config['videoframe_info_size']) $gl_caption['c_videoframe'] = $gl_caption['c_videoframe'] . '<br /> ( ' . $gl_config['videoframe_info_size'] . ' )';

    show_private_button();
    set_showwindow();
    $tpl->set_var('pts', '');
    $tpl->set_var('pts_private', '');
    $show = new ShowForm;
	$show->order_by = $GLOBALS['gl_order_by'];
	inmo_check_user_permission($show->id);
	
	$tpl->set_var('view_only_itself', inmo_view_only_itself());
	
	if (inmo_view_only_itself()) $show->set_field ('user_id', 'type', 'hidden');
	
	$show->get_values();
	
	if ($show->rs && $show->rs['user_id']==0){
		$show->rs['user_id'] = $_SESSION['user_id'];
	}

    if ($gl_write)
    {
        $show->call_function['comarca_id'] = "get_select_comarques";
        $show->call_function['municipi_id'] = "get_select_municipis"; // comentat autocomplete
        $show->call_function['zone_id'] = "get_select_zones";
    }
    $show->call('get_ref', 'ref,category_id,land,land_units,price,price_private,property_id,municipi_id,tipus');


    // referncia automatica
    if (INMO_PROPERTY_AUTO_REF == 1)
    {
        $show->fields['ref']['default_value'] = PROPERTY_AUTO_REF;
        $tpl->set_var('auto_ref', true);
    }
    else
    {
        // $show->fields['ref'] = '';
        $show->fields['ref']['form_admin'] = 'input';
        $tpl->set_var('auto_ref', false);
    }
    $query_provincies = "provincia_id='" . str_replace(",", "' OR provincia_id='", INMO_PROVINCIES). "'";
    $show->fields["provincia_id"]['select_condition'] = $query_provincies;

	show_property_check_demands( $show );


	$show->show_form();
}

/**
 * @param $show
 */
function show_property_check_demands( &$show ) {
	// demandes
	if ( $show->action == 'show_form_new' )  return;

	include_once( DOCUMENT_ROOT . 'admin/modules/custumer/custumer_demand_common.php' );
	include(DOCUMENT_ROOT . '/admin/modules/custumer/custumer_demand_functions.php');


	$exact_link = $prox_link = '';
	check_demands( 'exact', $show->id, 'mailed=1' );
	if ( $gl_resultats ) {
		$count = count( $gl_resultats );

		$custumer_ids = implode_field( $gl_resultats, 'custumer_id' );
		$demand_ids   = implode( ',', array_keys( $gl_resultats ) );

		$text = $count == 1 ? $show->caption['c_exact_result'] : $show->caption['c_exact_results'];

		$exact_link   = '<p>' . sprintf( $text, $count ) . ':
			<br /><a href="/admin/?menu_id=5010&property_id=' . $show->id . '">' . $show->caption['c_exact_results_link'] . '</a>
			 - <a href="/admin/?menu_id=5&amp;demand_ids=' . $demand_ids . '&amp;custumer_ids=' . $custumer_ids . '&amp;property_ids=' . $show->id . '">' . $show->caption['c_send_mail'] . '</a></p>';
		$gl_resultats = array();
	}

	check_demands( 'prox', $show->id, 'mailed=1 OR liked=1 OR refused=1 OR message_id<>0' );

	if ( $gl_resultats ) {
		$count = count( $gl_resultats );

		$custumer_ids = implode_field( $gl_resultats, 'custumer_id' );
		$demand_ids   = implode( ',', array_keys( $gl_resultats ) );

		$text         = $count == 1 ? $show->caption['c_aprox_result'] : $show->caption['c_aprox_results'];
		$prox_link    = '<p>' . sprintf( $text, $count ) . ':
			<br /><a href="/admin/?menu_id=5011&property_id=' . $show->id . '">' . $show->caption['c_aprox_results_link'] . '</a>
			 - <a href="/admin/?menu_id=5&amp;demand_ids=' . $demand_ids . '&amp;custumer_ids=' . $custumer_ids . '&amp;property_ids=' . $show->id . '">' . $show->caption['c_send_mail'] . '</a></p>';
		$gl_resultats = array();
	}

	$GLOBALS['gl_news'] .=
		$exact_link . $prox_link;
	// fi demandes
}

function set_showwindow(){
	global $gl_config;
	$cg = &$gl_config;
	
	$query_select_1 = "
		SELECT  print__template.template_id, template 
		FROM print__template_language, print__template
		WHERE type = '";
	$query_select_2 = "'
		AND print__template_language.template_id = print__template.template_id
		AND language = '" . LANGUAGE . "'";
	
	$type = 'showwindow';
	$results = Db::get_rows_array($query_select_1 . $type . $query_select_2);
	

	$language_cg =   array (
    'select_table' => 'all__language',
    'select_fields' => 'code,language',
    'select_condition' => 'public=1 ORDER BY ordre ASC',
  );

	// passo al template
	$cg['showwindow_template_id'] = $results?
		get_select('showwindow_template_id', $results, $cg['showwindow_template_id']):
		false;
 	$cg['showwindow_language'] =
		get_select('language', '', $cg['showwindow_language'],$language_cg);
 	$cg['showwindow_size'] =
		get_radios('showwindow_size', 'a3,a4', $cg['showwindow_size']);
 	$cg['showwindow_pages'] =
		get_select('showwindow_pages', '1,2,3,4,5,6,7,8,9,10,11,12,13,14', $cg['showwindow_pages']);
 	$cg['showwindow_top_fields'] =
		get_select('showwindow_top_fields', '1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20', $cg['showwindow_top_fields']);

	$cg['showwindow_show_zone']=get_checkbox('showwindow_show_zone', $cg['showwindow_show_zone']);
	$cg['showwindow_show_municipi']=get_checkbox('showwindow_show_municipi', $cg['showwindow_show_municipi']);
	$cg['showwindow_show_comarca']=get_checkbox('showwindow_show_comarca', $cg['showwindow_show_comarca']);
	$cg['showwindow_show_provincia']=get_checkbox('showwindow_show_provincia', $cg['showwindow_show_provincia']);


	$type = 'album';
	$results = Db::get_rows_array($query_select_1 . $type . $query_select_2);	
	
	// passo al template
	$cg['album_template_id'] = $results?
		get_select('album_template_id', $results, $cg['album_template_id']):
		false;	
	
 	$cg['album_language'] =
		get_select('language', '', $cg['album_language'],$language_cg);
 	$cg['album_size'] =
		get_radios('album_size', 'a3,a4', $cg['album_size']);
 	$cg['album_pages'] =
		get_select('album_pages', '1,2,3,4,5,6,7,8,9,10,11,12,13,14', $cg['album_pages']);

	$cg['album_show_municipi']=get_checkbox('album_show_municipi', $cg['album_show_municipi']);
	$cg['album_show_zone']=get_checkbox('album_show_zone', $cg['album_show_zone']);
	$cg['album_show_comarca']=get_checkbox('album_show_comarca', $cg['album_show_comarca']);
	$cg['album_show_provincia']=get_checkbox('album_show_provincia', $cg['album_show_provincia']);
}
function write_record_property()
{

    global $gl_action, $gl_insert_id, $gl_tool_section, $gl_process;

    $writerec = new SaveRows;
	inmo_check_user_permission($writerec->id);
	
    // referncia automatica
    if ((INMO_PROPERTY_AUTO_REF == 1) && (strstr($gl_action, "add_record")))
    {
        $writerec->fields["ref_number"]['override_save_value'] = get_ref_number();
        // $_SESSION['inmo_propertiy_auto_ref'], ja ho faré TODO
        $writerec->fields["ref"]['override_save_value'] = $writerec->fields["ref_number"]['override_save_value'];
		$writerec->field_unique  = 'property_file_name';
    }
    else
    {
        // $writerec->fields["ref"] = "";
        // $writerec->fields["ref_number"] = "";
		$writerec->field_unique  = ['ref', 'property_file_name'];
		$writerec->fields["ref"]['type'] = 'text';
		$writerec->fields["ref"]['text_maxlength']=15;
    }
    if (strstr($gl_action, "add_record"))
    {
        $writerec->fields["entered"]['override_save_value'] = "now()";
    }
	$writerec->set_field('modified','override_save_value','now()');

	// historial de preus
	$price = unformat_int($writerec->get_value( 'price' ));

	// Quan preu es 0 al crear, no poso entrada a l'historial
	$old_price = '0';
	if (strstr($gl_action, "save_record")) {
		// afegir historial de preu
		$old_price = Db::get_first( "SELECT price FROM inmo__property WHERE property_id = '" . $writerec->id . "'" );
	}

	// Al crear immoble ja creo historial de preu
	// A l'editar quan es cambia preu també
	if ($price != $old_price) {
		Db::execute( "
		INSERT INTO `custumer__history` (
		`date`, `status`, `finished_date`, `tipus`, `history_title`, `property_id`) VALUES
		(now(), 'finished', now(), 'price_changed', '" . $price . "', " . $writerec->id . ")" );
	}

	// Land units
	$land_units = $writerec->get_value('land_units');
	if ($land_units == 'ha') {
		
		$land = unformat_decimal($writerec->get_value('land'));
		$land = $land*10000;
		$writerec->set_value('land', format_decimal($land));
	}
	
    $writerec->save();

	$status = $writerec->get_value( 'status' );

	// enviar mail al fer algún canvi només a APIGRONA
	if (
		($status == 'onsale' || $status == 'sold' || $status == 'reserved')
		&& ( SHORT_HOST_URL == 'www.apigirona.com' || SHORT_HOST_URL == 'w.api' || SHORT_HOST_URL == 'w2.api')
		){

		$propertys = $writerec->get_value( 'property' );
		$property_titles = $writerec->get_value( 'property_title' );
		$descriptions = $writerec->get_value( 'description' );
		Debug::p($descriptions  );
		Debug::p( $GLOBALS['gl_config'] );

		$body = '';

		foreach ( $propertys as $key => $property ) {
			$property_title = $property_titles[$key];
			$description = nl2br($descriptions[$key]);

			if ($property)
			{
				$body .= "<h2>$property</h2>";
			}
			if ($property_title)
			{
				$body .= "<h3>$property_title</h3>";
			}
			if ($description)
			{
				$body .= "<p>$description</p>";
			}
			if ($property || $property_title || $description ){
 				$body .= "</br></br>";
			}
		}

		$ref = Db::get_first( "SELECT ref FROM inmo__property WHERE property_id = '" . $writerec->id . "'" );


		$mailer = get_mailer ($GLOBALS['gl_config']);
		$mailer->Body = '<h3>S\'ha efectuat un canvi en el següent immoble:</h3><p><a href="http://www.investin.cat/cat/inmo/property/v/category_id/' . $writerec->get_value( 'category_id' ) . '/tipus/' . $writerec->get_value( 'tipus' ) . '/' . $writerec->id . '.htm?pagina_id=5">Enllaç a l\'inmoble ref. ' . $ref . '</a></p><br><br><br>' . $body;
		$mailer->Subject = "Canvi en l'immoble ref." . $ref;
		$mailer->IsHTML(true);
		$mailer->AddAddress ('marc@letnd.com');
		$mailer->AddAddress ('promocioeconomica@figueres.org');
		$mailer->AddAddress ('coapi@apigirona.com');
		$mailer->AddAddress ('agenciapromocio@ajgirona.cat');
		send_mail($mailer, false);
	}
	
		

    if ($gl_process == 'contract_write_record_property')
    {
        $property_id = $gl_insert_id?$gl_insert_id:current($_POST['property_id']);
        print_javascript('top.get_property_jscript(' . $property_id . ',\'' . current($_POST['property_private']) . '\')');
    }

    export_to_portal($writerec->id, $writerec->action);
}

function export_to_portal( $ids, $action, $portal_vars = [] ) {

	include_once( DOCUMENT_ROOT . 'admin/modules/admintotal/export_common.php' );
	$export = New ExportCommon;

	if ( ! $export->can_export() ) return;

	$export->agencia_id = $_SESSION['client_id'];
	if ( $ids ) $export->ids = $ids;
	if ( $portal_vars ) $export->portal_vars = $portal_vars;

	$export->export_async( $action, 'property' );

}

/**
 * *Comarques i municipis
 *
 * @access public
 * @return void
 */

function get_municipis_jscript()
{
    // sleep for 10 seconds
    //sleep(2);

    $q = R::escape('q');
	$q = strtolower($q);
    if ($q)
    {
        $query = "SELECT municipi_id AS id, municipi AS name, comarca, provincia
				FROM inmo__municipi
				INNER JOIN inmo__comarca USING (comarca_id)
				INNER JOIN inmo__provincia USING (provincia_id)
				WHERE municipi LIKE '%" . $q . "%'
				ORDER BY municipi";
        $results = Db::get_rows($query);
        foreach ($results as $rs)
        {
            echo $rs['name'] . " ( " . $rs['comarca'] . ", " . strtoupper($rs['provincia']) . ") | " . $rs['id'] . "\n";
        }
    }
    die();

    // NOMES PER DESENVOLUPAMENT, HA D'ESTAR DESACTIVAT!!!!!!
    // CUIDADO, FA 17576 CONSULTES A LA BD

    // Amb BD municipis catalans surt un màxim de 133 resultats per 3 lletres i 311 per 2 lletres

    // comprobar quantes combinacions de 3 lletres hi ha en la bbdd
    $abc = 'a b c d e f g h i j k l m n o p q r s t u v w x y z';
    $abc = explode(' ', $abc);
    $conta = 0;
    $max = 0;
    $resultats = array();
    foreach ($abc as $a)
    {
        foreach ($abc as $b)
        {
            foreach ($abc as $c)
            {
                $conta++;
                $q = $a . $b . $c;
                $query = "SELECT count(*) as c
				FROM inmo__municipi
				WHERE municipi LIKE '%" . $q . "%'
				ORDER BY municipi";
                $results = Db::get_rows($query);
                $max = $results[0]['c'] > $max?$results[0]['c']:$max;
                $resultats[] = array('num' => $results[0]['c'], 'text' => $q);
                echo "$conta - ";
            }
        }
    }
    echo "<b>Maxim </b>= $max<br><br>";
    $resultats = array_sort_by_col($resultats, 'num', SORT_NUMERIC, SORT_DESC);
    foreach ($resultats as $r)
    {
        echo $r['text'] . " - " . $r['num'] . " <br>";
    }
    die();

    // comprobar quantes combinacions de 2 lletres hi ha en la bbdd
    $abc = 'a b c d e f g h i j k l m n o p q r s t u v w x y z';
    $abc = explode(' ', $abc);
    $conta = 0;
    $max = 0;
    $resultats = array();
    foreach ($abc as $a)
    {
        foreach ($abc as $b)
        {
            $conta++;
            $q = $a . $b;
            $query = "SELECT count(*) as c
				FROM inmo__municipi
				WHERE municipi LIKE '%" . $q . "%'
				ORDER BY municipi";
            $results = Db::get_rows($query);
            $max = $results[0]['c'] > $max?$results[0]['c']:$max;
            $resultats[] = array('num' => $results[0]['c'], 'text' => $q);
            echo "$conta - ";
        }
    }
    echo "<b>Maxim </b>= $max<br><br>";
    $resultats = array_sort_by_col($resultats, 'num', SORT_NUMERIC, SORT_DESC);
    foreach ($resultats as $r)
    {
        echo $r['text'] . " - " . $r['num'] . " <br>";
    }
    die();
}

function get_municipi($municipi_id)
{

    if (!$municipi_id)
        $municipi_id = INMO_DEFAULT_MUNICIPI;
    $query = "SELECT municipi_id AS id, municipi AS name, comarca, provincia
			FROM inmo__municipi
			INNER JOIN inmo__comarca USING (comarca_id)
			INNER JOIN inmo__provincia USING (provincia_id)
			WHERE municipi_id = '" . $municipi_id . "'";
    $results = Db::get_rows($query);
    $rs = $results[0];
    return $rs['name'] . " ( " . $rs['comarca'] . ", " . strtoupper($rs['provincia']) . ")";
}


function get_select_zones($key, $value)
{
    global $gl_caption;
    $query_zones = "(inmo__zone.municipi_id = inmo__municipi.municipi_id)
								    AND (inmo__municipi.comarca_id = inmo__comarca.comarca_id)
									AND (provincia_id='" . str_replace(",", "'
									OR provincia_id='", INMO_PROVINCIES) . "')
									ORDER BY zone";
    return get_select_sub($value, $key, "inmo__municipi, inmo__comarca, inmo__zone", "zone, inmo__municipi.municipi_id, zone_id", $query_zones, $gl_caption['c_zone_id_caption']);
}
function get_select_comarques($key, $value)
{
    $query_provincies = "provincia_id='" . str_replace(",", "'
									OR provincia_id='", INMO_PROVINCIES) . "'
									ORDER BY comarca";
    return get_select_sub($value, $key, "inmo__comarca", "comarca, provincia_id, comarca_id", $query_provincies, '');
}
function get_select_municipis($key, $value)
{
    $query_comarques = "(inmo__municipi.comarca_id = inmo__comarca.comarca_id)
									AND (provincia_id='" . str_replace(",", "'
									OR provincia_id='", INMO_PROVINCIES) . "')
									ORDER BY municipi";
    return get_select_sub($value, $key, "inmo__municipi, inmo__comarca", "municipi, inmo__comarca.comarca_id, municipi_id", $query_comarques, '');
}

function get_select_sub($value, $name, $table, $fields, $condition = '', $caption = '', $onchange = '', $disabled = '')
{
    $html = '';
    $results = get_results_sub($table, $fields, $condition);

    if ($caption)
        $html .= '<option value="0">' . $caption . '</option>';
    if ($results)
    {
        foreach ($results as $rs)
        {
            $selected = '';
            if ($rs[2] == $value)
                $selected = " selected";
            $html .= '<option value="' . $rs[2] . '"' . $selected . '>#' . $rs[1] . "#" . $rs[0] . '</option>';
        }
    }
    if ($onchange)
        $onchange = ' onChange="' . $this->onchange . '"';
    if ($disabled)
        $disabled = ' disabled';
    $html = '<select name="' . $name . '"' . $onchange . $disabled . '>' . $html . '</select>';
    Debug::add("Get select", $html);
    return $html;
}

function get_results_sub($table, $fields, $condition)
{

    $query = "SELECT " . $fields . " FROM " . $table;
    if ($condition)
        $query .= " WHERE " . $condition;
    Debug::add("Consulta form objects", $query);
    $results = Db::get_rows_array($query);
    return $results;
}

/**
 *
 * @access public
 * @return void
 */
function get_property_category($category_id)
{
    global $gl_language;
    $query = "SELECT category
				FROM inmo__category_language
				WHERE category_id='" . $category_id . "'
				AND language='" . $gl_language . "'";
    Debug::add("Consulta get category", $query);
    $rs = Db::get_rows($query);
    return $rs[0]['category'];
}

/**
 *
 * @access public
 * @return void
 */
function get_property_status($name, $status)
{
    global $gl_caption;
    return $gl_caption['c_' . $status];
}

/**
 *
 * @access public
 * @return void
 */
function get_property_tipus($name, $tipus)
{
    global $gl_caption;
    return isset($gl_caption['c_tipus_' . $tipus])?$gl_caption['c_tipus_' . $tipus]:$gl_caption['c_' . $tipus];
}
/**
 *
 * @access public
 * @return void
 */
function get_ref_number()
{

    $query = "SELECT ref_number
				FROM inmo__property
				ORDER by ref_number desc
				LIMIT 0,1";
    Debug::add("Consulta get category", $query);
    $rs = Db::get_rows($query);
    return $rs[0]['ref_number'] + 1;
}
/**
 *
 * @access public
 * @return array
 */

function get_ref($ref, $category_id, $land = 0, $land_units = '', $price = 0, $price_private = 0, $property_id = 0, $municipi_id = 0, $tipus='sell', $garden=2, $courtyard=2)
{
    global $gl_action, $gl_process, $gl_caption;
	$ret = [];
	if ($garden != 2) $ret['garden']=$garden?$gl_caption['c_yes']:$gl_caption['c_no'];
	if ($courtyard != 2) $ret['courtyard']=$courtyard?$gl_caption['c_yes']:$gl_caption['c_no'];
    if (INMO_PROPERTY_AUTO_REF == 1)
    {
        if (!strstr($gl_action, "show_form_new"))
        {
            $query = "SELECT ref_code
				FROM inmo__category
				where category_id = '$category_id'";
            $ref_code = Db::get_first($query);
            $ret['ref'] = $ref_code . $ref;
        }
    }

	if (CURRENCY2 == 'PTS'){ // per ptes arrodoneixo
    $pts = $price?round($price * (CURRENCY_CHANGE)):0;
    $pts_private = $price_private?round($price_private * (CURRENCY_CHANGE)):0;
    }
    else
    {
    $pts = $price?round($price * (CURRENCY_CHANGE)):0;
    $pts_private = $price_private?round($price_private * (CURRENCY_CHANGE)):0;
	}
    $pts = format_int($pts);
    $pts_private = format_int($pts_private);
	
	// preu a inmobles lloguer temporal
	if ($tipus=='temp' &&  empty($_SESSION['inmo_show_mls'])){
		Main::load_class('booking', 'common', 'admin');
		$ret += BookingCommon::get_property_price($property_id,$price);
	}


    $ret['pts'] = $pts;
    $ret['pts_private'] = $pts_private;
    //$ret['municipi'] = get_municipi($municipi_id); // comentat autocomplete

    if ($land_units == 'ha')
    {
        $ret['land'] = $land / 10000;
    }

    if ($gl_process == 'tv_download')
    {
        $ret['download'] = get_download_links($property_id);
    }

    $ret['show_tipus_temp'] = $tipus == 'temp'?'block':'none';

    return $ret;
}


/**
 * Copia exacta del public
 * @param $price
 * @param $property_id
 *
 * @return int|mixed|single|string
 */
function get_old_price( $price, $property_id ) {

	$query = "SELECT history_title
				FROM custumer__history
				WHERE custumer__history.bin = 0
				AND property_id = '" . $property_id . "'
				AND tipus = 'price_changed'
				AND history_title <> '" . format_int( $price ) . "'
				AND history_title <> '" . $price . "'
				ORDER BY date DESC, history_id DESC
				LIMIT 1";

	$old_price = Db::get_first( $query );
	$old_price = unformat_int($old_price);

	// M'asseguro que no sigui igual al preu que tenim ara
	if ($old_price != $price) {
		$old_price = format_int($old_price);
	}
	else {
		$old_price = 0;
	}

	return $old_price;
}

function get_property_comarca($comarca_id)
{

    $query = "SELECT comarca
				FROM inmo__comarca
				WHERE comarca_id='" . $comarca_id . "'";
    $rs = Db::get_rows($query);
    return $rs[0]['comarca'];
}

/**
 *
 * @access public
 * @return void
 */
function show_search_form($results = false)
{
    global $tpl, $gl_content, $gl_menu_id, $inmo_search, $gl_action, $gl_tool, $gl_caption;
    // faig un reset
    $tpl->vars = array();
    $tpl->loops = array();
    $search_list = false;
    $search_search = false;
    $search_filter = false;
    $only_search_form = $results?false:true;
    // $gl_page->title = constant($gl_tool_top_variable);
	
    if ($gl_action == "list_records_book_search")
    {
		// Aquest no te res a veure amb els altres, fa la seva propia funcio
		return get_book_search_form();
		
		
	}elseif ($gl_action == "list_records_search_list")
    {
        make_menu_comarques();
        make_menu_categories();
        make_menu_prices();
        make_menu_status();
        make_menu_images();
        $search_list = true;
    } elseif ($gl_action == "list_records_search_filter")
    {
        make_filter_tipus();
        make_filter_categories();
        make_filter_comarques();
        make_filter_municipis();
        make_filter_zones();
        make_filter_status();
        make_filter_description();
        if (!inmo_view_only_itself()) make_filter_user();
        make_filter_others();
        make_filter_order();
        make_filter_custom();
        $search_filter = true;
    } elseif ($gl_action == "show_search_form")
    {
        make_menu_comarques();
        make_menu_categories();
        make_menu_prices();
        make_menu_status();
        make_menu_images();
        make_filter_tipus();
        make_filter_categories();
        make_filter_comarques();
        make_filter_municipis();
        make_filter_zones();
        make_filter_status();
        make_filter_description();
        if (!inmo_view_only_itself()) make_filter_user();
        make_filter_others();
        make_filter_order();
        make_filter_custom();
        $search_list = true;
        $search_search = true;
        $search_filter = true;
    }
    // incloc cerca per paraules a tots els llistats menys als de busqueda
    else
    {
        $search_search = true;
    }

    $tpl->set_var('menu_id', $gl_menu_id);
    $tpl->set_var('search_list', $search_list);
    $tpl->set_var('search_search', $search_search);
    $tpl->set_var('search_filter', $search_filter);
    $tpl->set_var('only_search_form', $only_search_form);
	$tpl->set_var('version',$GLOBALS['gl_version']);
    Debug::add('caption', $inmo_search);
    $tpl->set_vars($gl_caption);
	$tpl->set_var('q',  htmlspecialchars(R::get('q')));
    $tpl->set_file('inmo/property_search.tpl');
    $search_form = $tpl->process();

    $gl_content = $search_form . $gl_content;

    if ($results) // ho trec quan es form i no te resultats, per no repetir-ho

    {
        if ($gl_action == "list_records_search_filter")
        {
            $search_form = str_replace('name="search_filter"', 'name="search_filter2"', $search_form);
        }
		else if ($search_search == true)
		{
            $search_form = str_replace('name="search"', 'name="search2"', $search_form);
		}
        $gl_content .= $search_form;
    }
	
	
	if (R::escape('q')) {
		$GLOBALS['gl_page']->javascript .= "
			$('table.listRecord').highlight('".addslashes(R::get('q'))."');			
		";
	}
}
function get_book_search_form()
{
	global $tpl, $gl_content, $gl_menu_id, $gl_caption;

	include(DOCUMENT_ROOT . 'admin/modules/booking/booking_book_config.php');
	
	$adult = R::escape('adult');
	$child = R::escape('child');
	$baby = R::escape('baby');
	$date_in = R::escape('date_in',now());
	$date_out = R::escape('date_out',now(false,4));
	
	$tpl->set_var('menu_id', $gl_menu_id);
	$tpl->set_var('date_in', $date_in);
	$tpl->set_var('date_out', $date_out);
    $tpl->set_vars($gl_caption);
	
	$adult = get_select('adult', '', $adult, $gl_db_classes_fields['adult']);
	$tpl->set_var('adult', $adult);
	
	$child = get_select('child', '', $child, $gl_db_classes_fields['child']);
	$tpl->set_var('child', $child);
	
	$baby = get_select('baby', '', $baby, $gl_db_classes_fields['baby']);
	$tpl->set_var('baby', $baby);
	
	
    $tpl->set_file('booking/property_search.tpl');
    $search_form = $tpl->process();
	
    $gl_content = $search_form . $gl_content;
	
}

// defineixo els tipus de custom que hi han

function set_custom_config() {
    global $gl_config, $gl_db_classes_fields, $gl_db_classes_language_fields, $gl_caption;
		// defineixo els tipus de custom que hi han
	for ($i = 1; $i <= 8; $i++) {
		if ($gl_config['custom'.$i.'_name']) {
			$gl_db_classes_fields['custom'.$i]['form_admin'] = 'input';
			$gl_db_classes_fields['custom'.$i]['type'] = $gl_config['custom'.$i.'_type'];
			$gl_caption['c_custom'.$i] = $gl_config['custom'.$i.'_name'];
			
			
			if ($gl_config['custom'.$i.'_type'] == 'text') {
				$gl_db_classes_language_fields[] = 'custom'.$i;
				$gl_db_classes_fields['custom'.$i]['text_maxlength'] = '255';
				$gl_db_classes_fields['custom'.$i]['text_size'] = '70';
			}
			elseif ($gl_config['custom'.$i.'_type'] == 'checkbox') {
				$gl_db_classes_fields['custom'.$i]['select_fields'] = '0,1';
			}
		}
	}
}


// és diferent de custumer_demand_functions, busca els clients que tenen activat permetre llistats

function inmo_companies_with_permission ()
{
	Db::connect_mother ();
	// Miro les que em donen permis a mi o les que donen permis a tothom
	//$query = "SELECT client_id1 FROM mls__client_to_client WHERE permission = 1 AND client_id2 = '" . $_SESSION['client_id'] . "' OR permission = 3 ";
	$query = "
			SELECT client_id1
			FROM mls__client_to_client, client__client
			WHERE permission = 'accepted'
			AND client_id1 = client__client.client_id
			AND client__client.mls_allow_listing = 1
			AND client_id2 = '" . $_SESSION['client_id'] . "'";
	$companiesmls = Db::get_rows ($query);

	if ($companiesmls) {
		return implode_field( $companiesmls, 'client_id1' );
	}
	else {
		return 0;
	}
}

function inmo_add_mls_config_fields(&$l) {
	include( DOCUMENT_ROOT . 'admin/modules/inmo/inmo_property_mls_config.php' );
	$l->fields = array_merge( $l->fields, $gl_db_classes_fields );
}

function inmo_get_mls_fields($rs) {
	$client_id = $rs['client_id'];
	$rs2 = Db::get_row( "
		SELECT phone1, comercialname, domain, client_dir
		FROM client__client
		WHERE client_id = " . $client_id );

	$rs = array_merge( $rs, $rs2 );

	return $rs;

}
?>