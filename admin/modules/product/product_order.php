<?
/**
 * ProductOrder
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2010
 * @version $Id$
 * @access public
 */

include_once('product_common.php');

class ProductOrder extends Module{
	var $condition = '', $q = '';
	var $id = '', $is_buyer = false, $is_seller = false, $is_invoice = false, $search, $excel;
	function __construct(){
		$version = $GLOBALS['gl_version'];
		Page::add_javascript_file( "/admin/modules/product/jscripts/product_search.js?v=$version" );
		Page::add_css_file( "/admin/themes/inmotools/product/styles.css?v=$version" );

		parent::__construct();
	}
	function on_load(){
		if ($this->config['has_gift_card']){
			$this->set_fields( 'is_gift_card', 'form_admin', 'input' );
		}
		else{
			$this->unset_field('is_gift_card');
		}
	}
	function list_records()
	{
		if (!$this->config['deposit_payment_on']) $this->unset_field('deposit_status');
		$GLOBALS['gl_content'] = $this->get_search_form() . $this->get_records();
	}
	function get_search_form(){

		Main::load_class( 'product', 'order_search', 'admin' );

		$this->search = New ProductOrderSearch( $this );
		$this->search->init_search_advanced();
		$this->condition = $this->search->get_search_condition();

		// $this->excel->view_format = $this->search->view_format;
		return '<script type="text/javascript" language="JavaScript" src="/admin/modules/product/jscripts/functions.js"></script>' . $this->search->get_search_form();
	}

	public function get_records() {

		$view_format = $this->search->view_format;

		if ( $view_format == 'all' ) {
			return $this->get_records_all( false );
		}
		else if ( $view_format == 'orderitems' ) {
			return $this->get_records_orderitems();
		}
		else {
			return $this->get_records_all( true );
		}

	}

	private function get_records_all( $view_only_order ) {

	    $listing = new ListRecords($this);
		$listing->add_swap_edit();

		$listing->set_field('shipped', 'type','text');
		$listing->set_field('delivered', 'type','text');

	    if ( $view_only_order ) {
			$listing->use_default_template = true;
			$listing->unset_field( 'orderitems' );
		}
		else {
			$listing->template = 'product/order_all_list.tpl';
		}

		$listing->set_var('product_col_span', 14); // Depend del disseny de la plantilla, -1 es el camp orderitems que no surt en columnes

		$listing->group_fields = [ 'order_status' ];

		$listing->order_by = 'entered_order DESC';

		// Cerques
		if ($this->condition) {
			$listing->condition = $this->condition;
		}
		// desde customer
		elseif (isset($_GET['customer_id'])){
			$listing->condition = "customer_id = '" . $_GET['customer_id'] . "'";
		}

		if (PRODUCT_HAS_INVOICE) {
			$listing->add_button('show_invoice', '', 'boto1', 'before', 'customer_show_invoice');
			$listing->add_button('show_invoice_refund', '', 'boto1', 'before', 'customer_show_invoice_refund');
		}
		$listing->call('records_walk', 'shipped,delivered,invoice_id,invoice_refund_id,invoice_refund_date', true);
	    return $listing->list_records();
	}
	function records_walk($listing, $shipped, $delivered, $invoice_id, $invoice_refund_id, $invoice_refund_date){

		$ret = array();
		$order_id = $listing->rs['order_id'];
		if ($this->action == 'list_records'){
			if ($shipped=='0000-00-00 00:00:00'){
				$ret['shipped'] = ''; // $this->caption['c_no_shipped'];
			}
			else {
				$ret['shipped'] = format_datetime( $shipped );
			}
			if ($delivered=='0000-00-00 00:00:00'){
				$ret['delivered'] = ''; // $this->caption['c_no_delivered'];
			}
			else {
				$ret['delivered'] = format_datetime( $delivered );
			}
		}
		if (PRODUCT_HAS_INVOICE) {
			if ($invoice_id=='-1' || $invoice_id=='0'){
				$ret['invoice_id'] = '';
				$ret['invoice_refund_id'] = '';
				$listing->buttons['show_invoice']['show']= false;
				$listing->buttons['show_invoice_refund']['show']= false;
			}
			else
			{
				// $ret['invoice_id'] = $this->config['invoice_prefix'] . $invoice_id;
				$ret['invoice_id'] = product_get_invoice_number($this->config['invoice_prefix'] ,$invoice_id, $listing->rs['invoice_date']);
				$ret['invoice_id_value'] = $invoice_id;

				$ret['invoice_refund_id'] = product_get_invoice_number($this->config['invoice_prefix'] ,$invoice_refund_id, $listing->rs['invoice_refund_date']);
				$ret['invoice_refund_id_value'] = $invoice_refund_id;

				$listing->buttons['show_invoice']['show']= true;
				if ($invoice_refund_id == 0)
					$listing->buttons['show_invoice_refund']['show']= false;
				else
					$listing->buttons['show_invoice_refund']['show']= true;
			}
		}
		if ($listing->list_show_type == 'list' ) {
			$ret['orderitems'] = $this->get_orderitems ( $order_id, $listing->is_editable );
			$ret['tr_class'] = $this->get_tr_class_order( $listing->rs );
		}

		return $ret;
	}

	private function get_tr_class_order( $rs ) {
		$order_status = $rs['order_status'];

		$class = ( $order_status == 'completed' OR
		           $order_status == 'preparing' OR
		           $order_status == 'shipped' OR
		           $order_status == 'delivered' OR
		           $order_status == 'collect' ) ? $order_status : 'not-payed';

		return $class;
	}

	private function get_records_orderitems() {

		$orderitems = $this->get_orderitems( false );

		if ( $this->search->is_excel ) {
			// $this->excel->make( $orderitems );

			return '';
		}
		else {
			return $orderitems;
		}

	}

	public function get_orderitems ( $order_id, $is_editable = false ) {

		$module         = Module::load( 'product', 'orderitem', '', false, 'product/product_orderitem_config.php' ); // així no necessito el product_orderitem.php
		$old_action     = $this->action;
		$module->action = 'get_records';

		// calculo si l'iva està inclòs en el preu
		$is_tax_included = $this->config['is_public_tax_included'];

		$price_sufix = '_basetax'; // mostro iva inclòs


		$listing = new ListRecords( $module );

		if ( $listing->config['has_order_date'] ) {
			$listing->set_var( 'has_order_date', true);
		}
		else {
			$listing->set_var( 'has_order_date', false);
			$listing->unset_field( 'product_date' );
		}

		$listing->set_options( 1, 0, 1, 1, 0, 1, 0 ); // No es pot borrar ni editar
		$listing->has_bin   = false;

		if ($order_id) {
			$listing->condition = 'order_id = ' . $order_id;
			$listing->template  = 'product/order_list_cart.tpl';
			$listing->paginate  = false;
			$listing->inputs_prefix = 'product__orderitem_saved';
			$listing->order_by = 'orderitem_id ASC, product_title';
			$listing->set_editable( $is_editable );
		}
		else {
			$listing->set_field( 'order_id', 'type', 'text' );
			$listing->set_field( 'order_id', 'list_admin', 'text' );
			$listing->set_field( 'product_variation_ids', 'list_admin', '0' );
			$listing->set_field( 'product_variation_categorys', 'list_admin', '0' );
			$listing->use_default_template = true;
			$listing->add_swap_edit();
			$listing->order_by = 'order_id DESC, product_title';
			// Cerques
			if ( $this->condition ) {
				$listing->condition = $this->condition;
			}
		}
		if ( $this->search && $this->search->is_excel ) {
			$listing->paginate = false;
		}

		$listing->list_records( false );

		Main::load_class( 'product', 'common_order', 'admin' );

		$is_refund = $this->is_invoice == 'refund';

		ProductCommonOrder::get_order_item_variations( $listing, $price_sufix, $is_refund );

		foreach ( $listing->results as $key => $val ) {
			$rs                           = &$listing->results[ $key ];
			$rs['tr_class']               = $this->get_tr_class_orderitem( $rs );
			if (!$order_id ){
				$product_variations = '';
				foreach ( $rs['product_variation_loop'] as $r ) {
					$product_variations .= "<p><strong>{$r['product_variation_category']}</strong>:{$r['product_variation']}</p>";
				}
				$rs['product_variations'] = $product_variations;
			}
		}

		$listing->parse_template(false);
		$listing->call_after_set_records('after_records_walk', 'loop', $this);

		$orderitems = $listing->process();

		$this->action = $old_action;

		return $orderitems;
	}

	static public function get_order_item_variations( &$rs ) {

		$product_variations          = explode( "\r", $rs['product_variations'] );
		$product_variation_categorys = explode( "\r", $rs['product_variation_categorys'] );

		$ret = array();

		array_pop( $product_variations );
		array_pop( $product_variation_categorys );

		foreach ( $product_variations as $k => $v ) {
			$ret[] = array(
				'product_variation_category' => $product_variation_categorys[ $k ],
				'product_variation'          => $product_variations[ $k ]
			);
		}

		return $ret;

	}


	private function get_tr_class_orderitem( $rs ) {

		$class = 'half-done';

		return $class;
	}
	function show_form()
	{
		// $GLOBALS['gl_content'] = $this->get_search_form() . $this->get_form();
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{
	    // dades de la comanda
		$show = new ShowForm($this);

		$is_mail = false;

		if ($this->parent=='ProductPayment') {
			// desde payment
			$is_mail = true;
			$show->id = $this->id;
		}

		$show->set_var('show_account', false);
		$show->call('records_walk', 'shipped,delivered,invoice_id,invoice_refund_id,invoice_refund_date', true);
		// $show->has_bin = false;
		$show->get_values();


		// busco courier predeterminat
		$default_courier = Db::get_first( "SELECT courier_id FROM product__courier WHERE default_courier = 1;" );
		if ($default_courier && !$show->rs['courier_id']) $show->rs['courier_id'] = $default_courier;

		$customer_language = Db::get_first("SELECT prefered_language FROM product__customer WHERE customer_id = " . $show->rs['customer_id']);

		// calculo si l'iva està inclòs en el preu
		$is_tax_included = $this->is_wholesaler($show->rs['customer_id'])?$this->config['is_wholesaler_tax_included']:$this->config['is_public_tax_included'];

		$price_sufix =  $is_tax_included?'_basetax':'_base';

		$show->set_vars(array(
			'customer_language'     => $customer_language,
		) );

		// obtinc llistat productes
		$module = Module::load('product','orderitem', '', false); // així no necessito el product_orderitem.php
		$module->action = 'get_records';


		$listing = new ListRecords($module);

		if ( $listing->config['has_order_date'] ) {
			$listing->set_var( 'has_order_date', true);
		}
		else {
			$listing->set_var( 'has_order_date', false);
			$listing->unset_field( 'product_date' );
		}

		$listing->paginate = false;
		$listing->has_bin = false;
		$listing->condition = 'order_id = ' . $show->rs['order_id'];
		$listing->template = 'product/cart_list.tpl';

		if ($this->parent=='ProductPayment') {

			// aquest son a públic i per tant ho necessito per enviar mails
			$show->rs['rate_price']  = $show->rs[ 'rate' . $price_sufix ];
			$show->rs['total_price'] = $show->rs[ 'total' . $price_sufix ];
			$show->rs['all_price']   = $show->rs[ 'all' . $price_sufix ];

			$show->set_vars(array(
				/*'address_invoice'     => false,*/
				/*'address_delivery'     => false,*/
				'repay_form'     => false,
			) );

			$c = &$this->caption;
			// s'utilitza a kupukupu, de moment nomes calen aquests
			$c_total_price = $is_tax_included ? $c['c_total_basetax'] : $c['c_total_base'];
			$c_all_price   = $is_tax_included ? $c['c_all_basetax'] : $c['c_all_base'];

			$listing->path = $show->path = PATH_TEMPLATES_PUBLIC;

			// de moment només fan falta al enviar emails, sino es poden passar a fora l'if
			$listing->set_vars( array(
				'is_block'      => false,
				'is_summary'    => true,
				'is_ajax'       => false,
				'is_invoice'    => false,
				'is_payment'    => false,
				'is_order'      => true,
				'total_price'   => format_currency( $show->rs['total_price'] ),
				'rate_price' => format_currency($show->rs['rate_price']),
				'all_price' => format_currency($show->rs['all_price']),
				'c_total_price' => $c_total_price,
				'c_all_price'   => $c_all_price,
				'is_tax_included' => $is_tax_included,
				'show_rate'     => true,
				'show_promcode' => $show->rs['promcode_discount']!='0.00'?true:false,
				'show_promcode_form' => false,
				'customer_comment' => nl2br(htmlspecialchars($show->rs['customer_comment'])),
				'old_price'     => ''
			) );


		}

		$listing->set_vars( array(
			'total_base'         => format_currency( $show->rs['total_base'] ),
			'total_tax'          => format_currency( $show->rs['total_tax'] ),
			'total_equivalencia' => ( $show->rs['total_equivalencia'] != '0.00' ) ? format_currency( $show->rs['total_equivalencia'] ) : 0,
			'total_basetax'      => format_currency( $show->rs['total_basetax'] ),
			'rate_base'          => format_currency( $show->rs['rate_base'] ),
			'rate_tax'           => format_currency( $show->rs['rate_tax'] ),
			'rate_basetax'       => format_currency( $show->rs['rate_basetax'] ),
			'all_base'           => format_currency( $show->rs['all_base'] ),
			'all_tax'            => format_currency( $show->rs['all_tax'] ),
			'all_equivalencia'   => ( $show->rs['all_equivalencia'] != '0.00' ) ? format_currency( $show->rs['all_equivalencia'] ) : 0,
			'all_basetax'        => format_currency( $show->rs['all_basetax'] ),
			'promcode_discount'  => ( $show->rs['promcode_discount'] != '0.00' ) ? format_currency( $show->rs['promcode_discount'] ) : 0
		) );
		// poso les variacions separades per comes
		// una altra forma enlloc de fer un list_records_walk
	    $listing->list_records(false);

		Main::load_class( 'product', 'common_order', 'admin');

		$is_refund = $this->is_invoice == 'refund';

		ProductCommonOrder::set_refund_negative_vals($is_refund, $show, $listing);
		ProductCommonOrder::get_order_item_variations( $listing, $price_sufix, $is_refund );
/*
		foreach ($listing->results as $key=>$val){
			 $rs = &$listing->results[$key];
			 $rs += UploadFiles::get_record_images($rs['product_id'], false,  'product', 'product');
			 $rs['product_variations'] = explode("\r",$rs['product_variations']);
			 $rs['product_variation_categorys'] = explode("\r",$rs['product_variation_categorys']);
			 array_pop ($rs['product_variations']);
			 array_pop ($rs['product_variation_categorys']);
			foreach($rs['product_variations'] as $key=>$val){
				$rs['product_variations'][$key] =
					'<strong>' . $rs['product_variation_categorys'][$key] . '</strong>: ' . $rs['product_variations'][$key];
			}
			 $rs['product_variations'] = implode("<br>",$rs['product_variations']);
		}
		*/

		$listing->parse_template(false);

		$listing->call_after_set_records('after_records_walk', 'loop', $this);

		$orderitems = $listing->process();

		$show->set_vars(array(
			'orderitems' => $orderitems,
			'is_mail' => $is_mail));

		$show->set_vars($this->vars); // &$tpl // això s'ha de fer si separo els tpls de this, show i listing


		/*if ($this->parent=='ProductPayment') {
			return $show->show_form();
		}*/

		// obtinc adresses
		$module = Module::load('product', 'address');
		$module->invoice_id = $show->rs['address_invoice_id'];
		$module->is_invoice = $this->is_invoice;
		$module->delivery_id = $show->rs['delivery_id'];
		$show->set_vars($module->do_action('get_addresses'));

		// obtinc form
	    return $show->show_form();
	}

	function after_records_walk( $rs ) {

		return ProductCommonOrder::cart_get_variation_image( $rs );

	}

	function save_rows()
	{
	    $save_rows = new SaveRows($this);
		//$save_rows->call_function['order_status'] = 'set_invoice_rows';
	    $save_rows->save();
	}
	function set_invoice_rows($name, $value){
		//product_set_invoice_id($writerec->id, $value);
	}
	function write_record()
	{
	    $writerec = new SaveRows($this);

		if ($this->action=='save_record'){
			product_set_invoice_id($writerec->id, $writerec->get_value('order_status'));
		}

	    $writerec->save();
	}

	function manage_images()
	{
	    $image_manager = new ImageManager($this);
	    $image_manager->execute();
	}

	function send_mail_ajax() {

		Main::load_class( 'product', 'common_order', 'admin');
		ProductCommonOrder::send_email_status_change( $this->caption, $this->config );

	}

	function is_wholesaler($customer_id){
		return Db::get_first("SELECT is_wholesaler FROM product__customer WHERE customer_id = $customer_id");
	}


	function get_search_results_products() {

		Main::load_class( 'product', 'order_ajax' );
		ProductOrderAjax::get_checkboxes_results_products( true );

	}

	//
	// FUNCIONS BUSCADOR - BORRAR
	//
	function search(){

		$q = $this->q = R::escape('q');
		$search_condition = '';

		if ($q)
		{
			// Sempre busco a tot arreu, abans si detectava que eren refrencies no hi  buscava, posaré checkbox de "buscar nomès referencies"

			$search_condition = "(
				order_status like '%" . $q . "%'
				OR payment_method like '%" . $q . "%'
				OR total_base like '%" . $q . "%'
				OR total_basetax like '%" . $q . "%'
				OR transaction_id like '%" . $q . "%'
				OR comment like '%" . $q . "%'
				OR customer_comment like '%" . $q . "%'
				" ;

			// busco a customer
			$query = "SELECT customer_id FROM product__customer WHERE (
								 name like '%" . $q . "%' OR
								 surname like '%" . $q . "%' OR
								 mail like '%" . $q . "%' OR
								 dni like '%" . $q . "%' OR
								 concat(name,' ',surname) like '%" . $q . "%'
									)" ;
			$results = Db::get_rows_array($query);

			// busco a address
			$query = "SELECT customer_id FROM product__address WHERE (
								 a_name like '%" . $q . "%' OR
								 a_surname like '%" . $q . "%' OR
								 address like '%" . $q . "%' OR
								 zip like '%" . $q . "%' OR
								 city like '%" . $q . "%' OR
								 concat(a_name,' ',a_surname) like '%" . $q . "%'
									)" ;
			$results = array_merge($results, Db::get_rows_array($query));

			$search_condition .= $this->get_ids_query($results, 'customer_id');
			$search_condition .= ") " ;
			$search_condition = $this->get_ref_query('order_id',$q, $search_condition);


			$GLOBALS['gl_page']->title = TITLE_SEARCH . '<strong>&nbsp;&nbsp;"' . $q . '"</strong>';
		}
		$this->condition = $search_condition;
		Debug::add('Search condition', $this->condition);
		$this->do_action('list_records');
	}
}