<?

class SistemamuseuExhibition extends Module{
	var $selected_topic = 0;
	function __construct(){
		parent::__construct();
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function get_records()
	{
		$this->set_topics();	
	    $listing = new ListRecords($this);
		$listing->call_function['exhibition_description'] = 'add_dots';
		$listing->order_by = 'exhibition_title ASC';
		$listing->condition = "status = 'public' AND (expired=0 OR expired >= (curdate()))";
		
		// formulari
		if (isset($_GET['exhibition_id'])){
			// necessito totes les exhibicions del topic per fer el llistat del menu de exhibicions
			if ($this->process != 'itinerant'){
			$listing->before_table = 'sistemamuseu__exhibition_to_topic, ';
			$listing->condition .= '
				AND sistemamuseu__exhibition.exhibition_id = sistemamuseu__exhibition_to_topic.exhibition_id
					AND topic_id = ' . $this->selected_topic;
			}
			$listing->template = 'sistemamuseu/exhibition_form';			
			unset($listing->call_function['exhibition_description']);
			$listing->paginate = false;
			
			// ho poso abans del set_records per poder establir el selected2, despres de parse_template el results ja s'ha passat al template i si el canvio no afecta al template
			$selected_key = 0;
			$found_record = false;
			$listing->list_records(false);			
			foreach ($listing->results as $key=>$val){
				$rs = &$listing->results[$key];
				
				$rs['selected2'] = '0'; // selected ja ho faig servir en els llistats per alguna altra cosa
				//$rs['exhibition_link'] = '?tool=sistemamuseu&tool_section=exhibition&action=list_records&language=' . LANGUAGE . '&page_id=' . $GLOBALS['gl_page']->page_id . '&topic_id=' . $this->selected_topic;
				
				$rs['museu_link'] = sistemamuseu_get_museu_link($rs['museu_id']);
				$rs ['exhibition_link'] = '/' . LANGUAGE . '/sistemamuseu/exhibition/'.$rs['exhibition_id'].'/topic_id/'. $this->selected_topic . '/' . get_page_file_name(19);
				
				if ($rs['exhibition_id']==$_GET['exhibition_id']) {
					$rs['selected2'] = '1';
					$selected_key = $key;
					$found_record = true;
				}
			}
			// si ja no existeix el registre faig un redirect a l'index de les exibicions
			if (!$found_record) Main::error_404();
			
			
			$listing->parse_template(false); // així no fa el process, pero si processa els resultats per poder establir la variable: loop_form
			
			$GLOBALS['gl_page']->page_title = $listing->loop[$selected_key]['exhibition_title'];
			$GLOBALS['gl_page']->page_description = add_dots_meta_description($listing->loop[$selected_key]['exhibition_description']);
			$listing->set_var('addthis_description', htmlspecialchars ($listing->loop[$selected_key]['exhibition_description']));
			$listing->set_loop('loop_form',array($listing->loop[$selected_key]));
			return $listing->process();
		}
		elseif ($this->process == 'destacat'){
			$listing->template='sistemamuseu/destaquem.tpl';
			$listing->order_by = 'destacat DESC, ordre ASC, exhibition_title ASC'; // es llisten sempre 4, primer els destacats i desprès si en falten, els no destacats
			$listing->limit = 4;
			$listing->list_records(false);
			foreach ($listing->results as $key=>$val){
				$rs = &$listing->results[$key];
				//$rs ['destacat_link'] = '?tool=sistemamuseu&amp;tool_section=exhibition&amp;action=show_record&amp;exhibition_id='.$rs['exhibition_id'].'&amp;language='.LANGUAGE.'&amp;page_id=19&amp;topic_id='. $this->get_topic($rs['exhibition_id']);
				$rs['museu_link'] = sistemamuseu_get_museu_link($rs['museu_id']);
				$rs ['destacat_link'] = '/' . LANGUAGE . '/sistemamuseu/exhibition/'.$rs['exhibition_id'].'/topic_id/'. $this->get_topic($rs['exhibition_id']) . '/' . get_page_file_name(19);
			}
		}
		elseif ($this->process == 'itinerant'){
			$listing->condition .= " AND kind='itinerant'";
			$listing->list_records(false);
			foreach ($listing->results as $key=>$val){
				$rs = &$listing->results[$key];
				//$rs['exhibition_link'] = '?tool=sistemamuseu&amp;tool_section=exhibition&amp;action=show_record&amp;exhibition_id='.$rs['exhibition_id'].'&amp;language='.LANGUAGE.'&amp;page_id=21&amp;process=itinerant&amp;topic_id='. $this->get_topic($rs['exhibition_id']);	
				
				$rs['museu_link'] = sistemamuseu_get_museu_link($rs['museu_id']);
				$rs['exhibition_link'] = '/' . LANGUAGE . '/sistemamuseu/exhibition/'.$rs['exhibition_id'].'/topic_id/'. $this->get_topic($rs['exhibition_id']) . '/' . get_page_file_name(21) . '?process=itinerant';
			}
		}
		// llistat per temes
		else{
			$listing->before_table = 'sistemamuseu__exhibition_to_topic, ';
			$listing->condition .= '
				AND sistemamuseu__exhibition.exhibition_id = sistemamuseu__exhibition_to_topic.exhibition_id
					AND topic_id = ' . $this->selected_topic . " AND status='public'";
					
			$listing->list_records(false);
			foreach ($listing->results as $key=>$val){
				$rs = &$listing->results[$key];
				//$rs['exhibition_link'] = '?tool=sistemamuseu&amp;tool_section=exhibition&amp;action=show_record&amp;exhibition_id='.$rs['exhibition_id'].'&amp;language='.LANGUAGE.'&amp;page_id=19&amp;topic_id=' . $this->selected_topic;	
				
				$rs['museu_link'] = sistemamuseu_get_museu_link($rs['museu_id']);
				$rs ['exhibition_link'] = '/' . LANGUAGE . '/sistemamuseu/exhibition/'.$rs['exhibition_id'].'/topic_id/'. $this->selected_topic . '/' . get_page_file_name(19);
			}
		}
		
	    return $listing->parse_template();
	}
	function show_form()
	{
		$this->do_action('list_records');
	}
	function set_topics(){	

		if ($this->process && !isset($_GET['exhibition_id'])){
			$this->set_loop('topics',array());
			$this->set_var('selected_topic',0);
			$this->set_var('selected_exhibition',0);
			$this->set_var('is_topic',0);
			return;
		}
		
		$query = "
			SELECT sistemamuseu__topic.topic_id AS topic_id, name
			FROM sistemamuseu__topic 
			LEFT OUTER JOIN sistemamuseu__topic_language using (topic_id) 
			WHERE sistemamuseu__topic.bin = 0
			AND sistemamuseu__topic_language.language = '".LANGUAGE."'
			ORDER BY name";
		$results = Db::get_rows($query);
		
		$topics = array();
		foreach ($results as $key=>$value){
			$rs = &$results[$key];
			$query = "
				SELECT count(*)
				FROM sistemamuseu__exhibition , sistemamuseu__exhibition_to_topic
				LEFT OUTER JOIN sistemamuseu__exhibition_language using (exhibition_id) 
				WHERE sistemamuseu__exhibition.bin = 0
				AND sistemamuseu__exhibition.exhibition_id = sistemamuseu__exhibition_to_topic.exhibition_id
				AND topic_id = '".$rs['topic_id']."' AND status='public'";
				
			if (!Db::get_first($query)) 
			{
				unset($results[$key]);
			}
			else{
				$rs['selected']='0';
				if (isset($_GET['topic_id']) && ($_GET['topic_id']==$rs['topic_id'])){
					$this->selected_topic = $rs['topic_id'];
					$rs['selected']='1';
				}
			}
			
		}
		// sino hi ha resultats deixem selected_topic = 0
		// i si hi ha resutats però no hi ha topic_id, agafem el primer topic de la llista
		if (!isset($_GET['topic_id']) && ($results)) {
			$key = key( $results );
			$this->selected_topic = $results[$key]['topic_id'];
			$results[$key]['selected']='1';
		}
		$this->set_loop('topics',$results);
		$this->set_var('selected_topic',$this->selected_topic);
		$this->set_var('is_topic',$this->process!='itinerant');
	}
	function get_topic($id){	

		$query = "
			SELECT topic_id
			FROM sistemamuseu__exhibition_to_topic 
			WHERE exhibition_id = ". $id;
		$ret = Db::get_first($query);
		return $ret?$ret:'0';
	}
}
?>