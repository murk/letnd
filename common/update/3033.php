<?php
// no es poden posar ; entre mig de la consulta ja que parteix les consultes per el ; a no ser que fem servir $update_sql en array()
if (is_table("all__page_language") && !is_field('all__page_language','page_description')){
	// poso el mail on s'havien per defecte els mails de contacte	
		$update_sql .="
		ALTER TABLE `all__page_language`  
		ADD COLUMN `page_description` VARCHAR(255) NOT NULL DEFAULT '' AFTER `page_title`,  
		ADD COLUMN `page_keywords` VARCHAR(255) NOT NULL DEFAULT '' AFTER `page_description`,  
		ADD COLUMN `title` VARCHAR(100) NOT NULL DEFAULT '' AFTER `page`,  
		ADD COLUMN `body_subtitle` VARCHAR(255) NOT NULL DEFAULT '' AFTER `title`,  
		ADD COLUMN `page_old_file_name` VARCHAR(100) NOT NULL DEFAULT '' AFTER `page_file_name`;";
}
?>