<?php
if (!defined('NEWGRASS_MENU_SELLPOINT')){

	// menus eina		
	define('NEWGRASS_MENU_SELLPOINT','Punts de venda');
	define('NEWGRASS_MENU_SELLPOINT_NEW','Nou punt de venda');
	define('NEWGRASS_MENU_SELLPOINT_LIST','Llistar punt de venda');
	define('NEWGRASS_MENU_SELLPOINT_BIN','Paperera de reciclatge');
}

/*--------------- nomès castellà i català, no traduit a la resta -------------------*/


$gl_caption_sellpoint['c_sellpoint'] = 'Títol';
$gl_caption_sellpoint['c_sellpoint_description'] = 'Descripció';
$gl_caption_sellpoint['c_empresa'] = 'Empresa';
$gl_caption_sellpoint['c_adress'] = 'Adreça';
$gl_caption_sellpoint['c_provincia_id'] = 'Província';
$gl_caption_sellpoint['c_poblacio'] = 'Població';
$gl_caption_sellpoint['c_cp'] = 'Codi postal';
$gl_caption_sellpoint['c_telefon'] = 'Telèfon';
$gl_caption_sellpoint['c_telefon2'] = 'Telèfon 2';
$gl_caption_sellpoint['c_fax'] = 'Fax';
$gl_caption_sellpoint['c_correu'] = 'E-mail';
$gl_caption_sellpoint['c_correu2'] = 'E-mail 2';
$gl_caption_sellpoint['c_url1'] = 'Web';
$gl_caption_sellpoint['c_url1_name'] = 'Nom de la web';



$gl_caption['c_filter_comarca_id'] = 'Totes les comarques';
$gl_caption['c_filter_municipi_id'] = 'Tots els municipis';


// mapes
$gl_caption['c_edit_map'] = 'Ubicació';
$gl_caption['c_search_adress']='Buscar adreça';
$gl_caption['c_latitude']='latitud';
$gl_caption['c_longitude']='longitud';
$gl_caption['c_found_direction']='adreça trobada';
$gl_caption['c_save_map'] = 'Guardar ubicació';
$gl_caption['c_click_map'] = 'Fes un click en un punt del mapa per definir la ubicació';
$gl_caption['c_form_map_title'] = 'Ubicació';
$gl_messages['point_saved'] = 'Nova ubicació guardada correctament';

$gl_caption['c_prepare'] = 'En preparació';
$gl_caption['c_review'] = 'Per revisar';
$gl_caption['c_public'] = 'Públic';
$gl_caption['c_archived'] = 'Arxivat';
$gl_caption['c_status'] = 'Estat';

$gl_caption['c_sellpoint_file_name'] = $GLOBALS['gl_caption']['c_file_name'];
$gl_caption['c_sellpoint_old_file_name'] = $GLOBALS['gl_caption']['c_old_file_name'];

$gl_caption['c_filter_provincia_id'] = 'Totes les provincies';


?>