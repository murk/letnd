<?

/* 26-2-16

	INMO

	UP-DATE
	-Afegir camp de referencia colaborador
	- Afegir menús de agencies
 */

// instalo només a inmo, desprès es podrà fer per tots els clients amb punt de mapa, així es podran definir varis punts de mapa
if ( is_table( 'inmo__property' ) ) {

	$update_sql .= "
		INSERT INTO `all__menu` (`menu_id`, `menu_group`, `tool`, `tool_section`, `action`, `parent_id`, `toolmode_id`, `variable`, `ordre`, `link`, `process`, `bin`, `read1`, `write1`) VALUES (4120, 'web', 'web', 'place', '', 4000, 1, 'WEB_MENU_PLACE', 0, '', '', 0, ',1,3,2,', ',1,2,');
		INSERT INTO `all__menu` (`menu_id`, `menu_group`, `tool`, `tool_section`, `action`, `parent_id`, `toolmode_id`, `variable`, `ordre`, `link`, `process`, `bin`, `read1`, `write1`) VALUES (4121, 'web', 'web', 'place', 'show_form_new', 4120, 1, 'WEB_MENU_PLACE_NEW', 1, '', '', 0, ',1,3,2,',
			 ',1,2,');
		INSERT INTO `all__menu` (`menu_id`, `menu_group`, `tool`, `tool_section`, `action`, `parent_id`, `toolmode_id`, `variable`, `ordre`, `link`, `process`, `bin`, `read1`, `write1`) VALUES (4122, 'web', 'web', 'place', 'list_records', 4120, 1, 'WEB_MENU_PLACE_LIST', 2, '', '', 0,
		        ',1,3,2,', ',1,2,');
		";
}

if ( is_table( 'inmo__property' ) && ! is_field( 'inmo__property', 'ref_collaborator' ) ) {

	$update_sql .= "ALTER TABLE `inmo__property`
	ADD COLUMN `ref_collaborator` VARCHAR(50) NOT NULL DEFAULT '' AFTER `ref_number`,
	ADD COLUMN `place_id` INT(11) NOT NULL DEFAULT '0' AFTER `adress`;
";

} else {
	if ( is_table( 'inmo__property' ) ) {
		trigger_error( 'Ja existeixen el camp ref_collaborator' );
	}
}

if ( is_table( 'inmo__property' ) ) {

	if ( ! is_table( 'web__place' ) ) {

		$update_sql .= "CREATE TABLE `web__place` (
	`place_id` INT(11) NOT NULL AUTO_INCREMENT,
	`place` VARCHAR(100) NOT NULL DEFAULT '' COLLATE 'utf8_spanish2_ci',
	`adress` VARCHAR(255) NOT NULL DEFAULT '' COLLATE 'utf8_spanish2_ci',
	`numstreet` VARCHAR(10) NOT NULL DEFAULT '' COLLATE 'utf8_spanish2_ci',
	`block` VARCHAR(10) NOT NULL DEFAULT '' COLLATE 'utf8_spanish2_ci',
	`flat` VARCHAR(10) NOT NULL DEFAULT '' COLLATE 'utf8_spanish2_ci',
	`door` VARCHAR(10) NOT NULL DEFAULT '' COLLATE 'utf8_spanish2_ci',
	`zip` VARCHAR(20) NOT NULL DEFAULT '' COLLATE 'utf8_spanish2_ci',
	`zone_id` INT(11) NOT NULL DEFAULT '0',
	`municipi_id` INT(11) NOT NULL DEFAULT '0',
	`comarca_id` INT(11) NOT NULL DEFAULT '0',
	`provincia_id` INT(11) NOT NULL DEFAULT '0',
	`country_id` INT(11) NOT NULL DEFAULT '1',
	`status` ENUM('prepare','review','public','archived') NOT NULL DEFAULT 'prepare' COLLATE 'utf8_spanish2_ci',
	PRIMARY KEY (`place_id`)
)
COLLATE='utf8_spanish2_ci'
ENGINE=MyISAM
AUTO_INCREMENT=1;
";

	} else {
		trigger_error( 'Ja existeixen la taula web__place' );
	}

}

?>