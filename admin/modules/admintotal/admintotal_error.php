<?

/**
 * AdmintotalError
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2018
 * @version $Id$
 * @access public
 */
class AdmintotalError extends Module {

	public function __construct() {
		parent::__construct();
		$this->table = 'admintotal__error';
	}

	public function on_load() {
		parent::on_load();
		Db::connect_mother();
	}

	public function on_end( $action ) {
		parent::on_end( $action );
		Db::reconnect();
	}

	public function list_records() {
		$GLOBALS['gl_content'] = $this->get_records();
	}

	public function get_records() {
		$GLOBALS['gl_page']->add_javascript_file( '/common/jscripts/prism/prism.js' );
		$GLOBALS['gl_page']->add_css_file( '/common/jscripts/prism/prism.css' );

		$listing = new ListRecords( $this );
		$listing->set_options( 1, 1, 1, 1, 1, 0 );

		$listing->add_filter('host');
		$listing->call( 'records_walk', false, true );
		$listing->order_by = 'entered DESC';

		// $listing->condition = "";

		$listing->has_bin = false;

		$ret = $listing->list_records();

		return $ret;
	}

	public function records_walk( &$listing ) {

		$rs           = $listing->rs;
		$ret          = array();

		/*$s            = $rs['mysql'];
		$ret['mysql'] = "<pre><code class=\"language-sql\">$s</code></pre>";

		$s            = $rs['session'];
		$data = json_decode( $s );
		$s = json_encode($data, JSON_PRETTY_PRINT);
		$ret['session'] = "<pre><code class=\"language-json\">$s</code></pre>";

		$s            = $rs['cookie'];
		$data = json_decode( $s );
		$s = json_encode($data, JSON_PRETTY_PRINT);
		$ret['cookie'] = "<pre><code class=\"language-json\">$s</code></pre>";

		$s            = $rs['vars'];
		$data = json_decode( $s );
		$s = json_encode($data, JSON_PRETTY_PRINT);
		$ret['vars'] = "<pre><code class=\"language-json\">$s</code></pre>";*/

		// $ret['query_string'] = str_replace( '&', '<br>&', $s );

		$host        = $rs['host'];
		$uri         = $rs['uri'];
		$ret['host'] = "<a href='http://$host$uri' target='_blank'>$uri</a>";


		return $ret;

	}

	public function save_rows()
	{
	    $save_rows = new SaveRows($this);
	    $save_rows->save();
	}
}