<?php
// menus eina
if (!defined('ATOT_MENU_ESPECTACLE')){
	define('ATOT_MENU_ESPECTACLE_NEW','Entrar nou espectacle');
	define('ATOT_MENU_ESPECTACLE','Espectacles');
	define('ATOT_MENU_ESPECTACLE_LIST','Llistar espectacles');
	define('ATOT_MENU_ESPECTACLE_BIN','Paperera de reciclatge');
	define('ATOT_MENU_MUNICIPI','Poblacions');
	define('ATOT_MENU_MUNICIPI_NEW','Entrar nova població');
	define('ATOT_MENU_MUNICIPI_LIST','Llistar poblacions');
	define('ATOT_MENU_MUNICIPI_BIN','Paperera de reciclatge');
	
	define('ATOT_MENU_REQUISIT','Requisits');
	define('ATOT_MENU_REQUISIT_NEW','Entrar nou requisit');
	define('ATOT_MENU_REQUISIT_LIST','Llistar requisits');
	define('ATOT_MENU_REQUISIT_BIN','Paperera de reciclatge');
	
	define('ATOT_MENU_CAMPANYA','Campanyes');
	define('ATOT_MENU_CAMPANYA_NEW','Entrar nova campanya');
	define('ATOT_MENU_CAMPANYA_LIST','Llistar campanyes');
	define('ATOT_MENU_CAMPANYA_BIN','Paperera de reciclatge');
	
	define('ATOT_MENU_CURS','Cursos');
	define('ATOT_MENU_CURS_NEW','Entrar nou curs');
	define('ATOT_MENU_CURS_LIST','Llistar cursos');
	define('ATOT_MENU_CURS_BIN','Paperera de reciclatge');
	
	define('ATOT_MENU_LLOC','Llocs');
	define('ATOT_MENU_LLOC_NEW','Entrar nou lloc');
	define('ATOT_MENU_LLOC_LIST','Llistar llocs');
	define('ATOT_MENU_LLOC_BIN','Paperera de reciclatge');
}

// captions seccio
$gl_caption_espectacle['c_prepare'] = 'En preparació';
$gl_caption_espectacle['c_review'] = 'Per revisar';
$gl_caption_espectacle['c_public'] = 'Públic';
$gl_caption_espectacle['c_archived'] = 'Arxivat';
$gl_caption_espectacle['c_entered'] = 'Data creació';
$gl_caption_espectacle['c_modified'] = 'Data modificació';
$gl_caption_espectacle['c_status'] = 'Estat';
$gl_caption_espectacle['c_in_home'] = 'Home';
$gl_caption_espectacle['c_in_home_0']='No surt a la Home';
$gl_caption_espectacle['c_in_home_1']='Surt a la Home';
$gl_caption_espectacle['c_destacat'] = 'Nou';
$gl_caption_espectacle['c_destacat_0']='No és nou';
$gl_caption_espectacle['c_destacat_1']='És nou';
$gl_caption_espectacle['c_only_campanya'] = 'Només campanyes';
$gl_caption_espectacle['c_only_campanya_0']='A tot arreu';
$gl_caption_espectacle['c_only_campanya_1']='Només surt a campanyes';
$gl_caption_espectacle['c_durada'] = 'Durada';
$gl_caption_espectacle['c_autor_espectacle'] = 'Autors de l\'espectacle';
$gl_caption_espectacle['c_direccio'] = 'Direcció escènica';
$gl_caption_espectacle['c_animacio'] = 'Animacions';
$gl_caption_espectacle['c_autor_musica'] = 'Autors de la música';
$gl_caption_espectacle['c_interpret'] = 'Intèrprets';
$gl_caption['c_aforament_maxim'] = 'Aforment màxim';
$gl_caption_espectacle['c_tipus_concert'] = 'Tipus de concert';
$gl_caption_espectacle['c_tipus_concert_tots'] = 'Tots';
$gl_caption_espectacle['c_tipus_concert_familiar'] = 'Familiar';
$gl_caption_espectacle['c_tipus_concert_escolar'] = 'Escolar';
$gl_caption_espectacle['c_rang_edat'] = 'Rang d\'edat';
$gl_caption_espectacle['c_rang_edat_0'] = 'Totes les edats';
$gl_caption_espectacle['c_rang_edat_1'] = 'de 0 a 3';
$gl_caption_espectacle['c_rang_edat_2'] = 'de 3 a 6';
$gl_caption_espectacle['c_rang_edat_3'] = 'de 6 a 9';
$gl_caption_espectacle['c_rang_edat_4'] = 'de 9 a 12';
$gl_caption_espectacle['c_rang_edat_5'] = 'de a 16';
$gl_caption_espectacle['c_file']='Arxius';

$gl_caption_espectacle['c_filter_tipus_concert'] = 'Tots els tipus concert';
$gl_caption_espectacle['c_filter_status'] = 'Tots els estats';
$gl_caption_espectacle['c_filter_in_home'] = 'Totes, home o no';
$gl_caption_espectacle['c_filter_destacat'] = 'Tots, nous o no';
$gl_caption_espectacle['c_filter_only_campanya'] = 'Tots, campanyes o no';

$gl_caption_espectacle['c_espectacle'] = 'Espectacle';
$gl_caption_espectacle['c_content'] = 'Descripció';
$gl_caption_espectacle['c_content_list'] = 'Descripció curta al llistat';

$gl_caption_espectacle['c_url1_name']='Nom URL(1)';
$gl_caption_espectacle['c_url1']='URL(1)';
$gl_caption_espectacle['c_url2_name']='Nom URL(2)';
$gl_caption_espectacle['c_url2']='URL(2)';
$gl_caption_espectacle['c_url3_name']='Nom URL(3)';
$gl_caption_espectacle['c_url3']='URL(3)';
$gl_caption_espectacle['c_videoframe']='Video (youtube, metacafe...) <br />( 421 px ample )';
$gl_caption_espectacle['c_videoframe2']='Video 2 <br />( 421 px ample )';
$gl_caption_espectacle['c_videoframe3']='Video 3 <br />( 421 px ample )';

$gl_caption_espectacle['c_tipus_audicio'] = 'Tipus d\'audició';
$gl_caption['c_genere'] = 'Gènere o estil music';
$gl_caption_espectacle['c_formacio'] = 'Formació musical';
$gl_caption_espectacle['c_familia_instrument'] = 'Família d’instruments';
//$gl_caption_espectacle['c_requeriment_espai'] = '';

$gl_caption_municipi['c_municipi'] = 'Població';
$gl_caption_municipi['c_descripcio'] = 'Descripció campanyes';
$gl_caption['c_municipi_id'] = 'Població';
$gl_caption['c_municipi'] = 'Població';
$gl_caption['c_curs_id'] = 'Curs';
$gl_caption_espectacle['c_curs_id'] = 'Rang d\'edat <br>( només familiar )';
$gl_caption['c_curs'] = 'Curs';
$gl_caption['c_lloc_id'] = 'Lloc';
$gl_caption['c_lloc'] = 'Lloc';
$gl_caption['c_espectacle_id'] = 'Espectacle';
$gl_caption['c_espectacle'] = 'Espectacle';
$gl_caption['c_ordre'] = 'Ordre';
$gl_caption['c_preu'] = 'Preu';
$gl_caption['c_edat1'] = 'Edat 1';
$gl_caption['c_edat2'] = 'Edat 2';

$gl_caption['c_data'] = 'Dates';
$gl_caption['c_horari'] = 'Horaris';
$gl_caption['c_inscripcio'] = 'Inscripcio';

$gl_caption_requisit['c_requisit'] = 'Requisit';
$gl_caption_espectacle['c_requisit_id'] = 'Requisits';


$gl_caption['c_espectacle_file_name'] = $GLOBALS['gl_caption']['c_file_name'];
$gl_caption['c_espectacle_old_file_name'] = $GLOBALS['gl_caption']['c_old_file_name'];
		
?>
