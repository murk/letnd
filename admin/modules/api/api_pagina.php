<?
/**
 * ApiPagina
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2013
 * @version $Id$
 * @access public
 
 He desactiva t que s'entrin imatges just desprès d'entrar un registre, els registres s'han d'editar desde el seu llistat així agafa el process
 
 
 */
class ApiPagina extends Module{
	
	var $level=false, $last_menu, $last_menu_sub, $menu_list;
	
	function __construct(){
		parent::__construct();
		$this->order_by = 'ordre ASC, pagina ASC';
	}
	function on_load(){	
		
		$this->last_menu = &$GLOBALS['gl_page']->last_menu[$GLOBALS['gl_menu_id']][0];
		$this->last_menu_sub = &$GLOBALS['gl_page']->last_menu[$GLOBALS['gl_menu_id']][1];	
		
		// agafo el level directament del registre, per assegurar-me, ja que el formulari canvia segons el level
		
		$id = R::id('pagina_id');
		
		if ($id!==false) {			
			$this->level = Db::get_first("SELECT level FROM api__pagina WHERE pagina_id='" . $id. "'");
			if ($this->level===false){
				$this->action = 'list_records';
				$this->level=0;
			}
		}
		else{
			$this->level = 0;
		}
		
		$this->menu_list = ($this->process)?'2214':'2203';
		
		//Debug::p($this->level);	
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function get_records()
	{
		$this->level ++; // poso un nivell més per mostrar els fills
		$listing = new ListRecords($this);
		
		$listing->order_by = $this->order_by;
		$listing->extra_fields = "(SELECT paginatpl FROM api__paginatpl_language WHERE api__paginatpl_language.paginatpl_id = api__pagina.paginatpl_id AND language='".LANGUAGE."') AS paginatpl";
		
		$listing->extra_fields = "(SELECT listtpl FROM api__listtpl_language WHERE api__listtpl_language.listtpl_id = api__pagina.listtpl_id AND language='".LANGUAGE."') AS listtpl";
		
		$listing->call('list_records_walk_pagina','pagina_id,paginatpl_id,paginatpl,listtpl_id,listtpl,link,pagina,parent_id',true);
			
		if ($this->action!='list_records_bin'){
		
			/*if ($this->level==1){
				$listing->set_options (1, 0, 1, 1, 0, 1, 0, 1, 1, 1); // $options_bar = 1, $options_checkboxes = 1, $save_button = 1, $select_button = 1, $delete_button = 1, $print_button = 1, $edit_buttons = 1, $image_buttons = 1, $split_count = 1
			}
			else{
				$listing->set_options(1, 1, 1, 1, 1, 1, 0, 1, 1, 1);			
			}*/
			$listing->set_options(1, 1, 1, 1, 1, 1, 0, 1, 1, 1);		
			// secundari
			$listing->condition = 'level = ' . $this->level . ' AND menu = ' . ($this->process=='second'?'2':'1');
			
			$listing->always_parse_template = true;
			$listing->add_filter('status');
			
			$pagina_id = R::id('pagina_id');
			if ($pagina_id) $listing->condition .= ' AND parent_id = ' . $pagina_id;
			$listing->add_filter('paginatpl_id');
			
			
			$listing->add_field('subpagina');
			$listing->set_field('subpagina','list_admin','out');
			$listing->set_field('subpagina','type','none');
			
			//debug::p($this->process);
			$listing->add_options_button ('add_pagina_button', 'menu_id='.$this->menu_list.'&action=show_form_new&parent_id='.$pagina_id);			
			
			$listing->add_button ('edit_pagina_button', 'action=show_form_edit', 'boto2');
			$listing->add_button ('edit_content_button', 'action=show_form_edit', 'boto1');
			
			$this->set_menus_level();
			
		}
		else{		
			$listing->set_options(1, 1, 1, 1, 1, 1, 0, 0, 0, 1);	
		}
	    return $listing->list_records();
	}
	function format_groupfield(&$rs, $group_field, $val){
			return $val;
	}
	
	function list_records_walk_pagina($module,$id,$paginatpl_id,$paginatpl,$listtpl_id,$listtpl,$link, $pagina, $parent_id){		
		
		$ret['paginatpl_id'] = '<img title="'.$paginatpl.'" src="/admin/modules/api/paginatpl_'.$paginatpl_id.'.gif" />';
		$ret['listtpl_id'] = '<img title="'.$listtpl.'" src="/admin/modules/api/listtpl_'.$listtpl_id.'.gif?v=2" />';
		
		
		// subapartats
		if ($this->level!='3' && !$link){
		
			$ret['subpagina'] = '';
			$results = Db::get_rows("
				SELECT pagina, api__pagina.pagina_id 
				FROM api__pagina, api__pagina_language 
				WHERE api__pagina.pagina_id = api__pagina_language.pagina_id
				AND api__pagina.bin = 0
				AND language='".LANGUAGE."' 
				AND api__pagina.parent_id = '".$id."' 
				AND menu = " . ($this->process=='second'?'2':'1') ." 
				ORDER BY ordre ASC, pagina asc");
			
			$process = ($this->process)?'&process='.$this->process:'';
			
			if ($results){
			
				$add_new_menu = '<a href="?menu_id='.$this->menu_list.'&action=show_form_new&parent_id='.$id.$process.'" class="bigger">'.$this->caption['c_add_subpagina'].'</a>';
				
				$ret['subpagina'] .= '<p style="white-space:nowrap;margin:8px 0;"><a href="?menu_id='.$this->menu_list.'&pagina_id='.$id.$process.'" class="bigger">'.$this->caption['c_list_subpagines'].'</a> - ' . $add_new_menu . '</p>';
				
				foreach ($results as $rs) {
					$ret['subpagina'] .= '<p>- <a href="?menu_id='.$this->menu_list.'&action=show_form_edit&pagina_id='.$rs['pagina_id'].'">'.$rs['pagina'].'</a></p>';
				}
			}
			else{
			
				$add_new_menu = '<a href="?menu_id='.$this->menu_list.'&action=show_form_new&parent_id='.$id.$process.'" class="bigger">'.$this->caption['c_add_subpagina2'].'</a>';
				
				$ret['subpagina'] .= '<p style="white-space:nowrap;margin:8px 0;">' . $add_new_menu . '</p>';
			}
		}	
		
		
		if ($this->action=='list_records_bin'){
			while ($parent_id!=0) {
				$rs = Db::get_row("SELECT parent_id,pagina FROM api__pagina, api__pagina_language WHERE api__pagina.pagina_id = api__pagina_language.pagina_id AND language='".LANGUAGE."' AND api__pagina.pagina_id = '".$parent_id."'");
				$parent_id = $rs['parent_id'];
				$pagina = $rs['pagina'] . ' > ' . $pagina;
			}
			$ret['pagina'] = $pagina;
		}
		else{
			$module->buttons['edit_content_button']['show'] = $link=='';
			$module->buttons['edit_pagina_button']['show'] = $link!='';		
		}
		
		return $ret;
	}
	
	function set_menus_level($parent_id=false ){
	
		if ($this->level=='1')
			return;
			
		elseif ($this->level==2){
			$page = &$GLOBALS['gl_page'];

			$pagina_id = $parent_id?$parent_id:R::id('pagina_id');				
			
			if ($parent_id){
				$link = '?menu_id='.$this->menu_list.'&pagina_id='.$parent_id;
			}
			else{
				$link = '';
			}
			$name = $this->get_pagina($pagina_id);
			$page->add_breadcrumb ($name, $link);
			$page->title = $this->caption['c_subtitle_level2'];
			$page->subtitle = $name;
			if ($parent_id) Page::add_breadcrumb ($this->caption['c_breadcrumb_edit_level'.$this->level]);
		
			$this->last_menu = array('name'=>$name,'link'=>$link);
		}
			
		elseif ($this->level==3){
	
			$page = &$GLOBALS['gl_page'];
			
			$pagina_id = $parent_id?$parent_id:R::id('pagina_id');
			
			if ($parent_id){
				$link2 = '?menu_id='.$this->menu_list.'&pagina_id='.$parent_id;
					
			}
			else{
				$link2 = '';
			}
			
			$subtitles =  $this->get_pagina($pagina_id, true);
			$link = '?menu_id='.$this->menu_list.'&pagina_id='.$subtitles['parent_id'];
			
			$page->title = $this->caption['c_subtitle_level3'];
			$page->subtitle = $subtitles['subtitle'];
			
			$page->add_breadcrumb ($subtitles['title'], $link);
			$page->add_breadcrumb ($subtitles['subtitle'], $link2);
			
			if ($parent_id) Page::add_breadcrumb ($this->caption['c_breadcrumb_edit_level'.$this->level]);
			
			$this->last_menu = array('name'=>$subtitles['title'],'link'=>$link);		
			$this->last_menu_sub = array('name'=>$subtitles['subtitle'],'link'=>$link2);
		}
	}
	function get_pagina($pagina_id, $get_parent=false){
		$query = "SELECT pagina FROM api__pagina_language WHERE language='" . LANGUAGE . "' AND pagina_id='" . $pagina_id . "'";
		
		$title = Db::get_first($query);
		if ($get_parent){
			$query = "SELECT  pagina, pagina_id FROM api__pagina_language WHERE language='" . LANGUAGE . "' AND pagina_id=(SELECT parent_id FROM api__pagina WHERE pagina_id = '" . $pagina_id . "')";
			$rs = Db::get_row($query);
			$title  = array('title'=>$rs['pagina'], 'subtitle'=>$title, 'parent_id'=>$rs['pagina_id']);
		}
		return $title;
	}
	
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{
	    $show = new ShowForm($this);
		$show->order_by = $this->order_by;
		
		$show->get_values();
		
		$show->set_var('is_full_pagina',true);
		

		// segons el llistat d'on vinc, agafo el nivell
		if ($this->action=='show_form_new') {
		
			$parent_id = R::number('parent_id');
			
			if ($parent_id){
				$this->level = Db::get_first('SELECT level FROM api__pagina WHERE pagina_id = ' . $_GET['parent_id'])+1;
				$parent_id = $_GET['parent_id'];
			}
			else{
				$this->level = 1;
				$parent_id = 0;
			}
			$show->set_field('level','default_value',$this->level);
			$show->set_field('parent_id','default_value',$parent_id);
			
			// secundari
			if ($this->process=='second') {
				$show->set_field('menu','default_value','2');
			}
		}
		else{
			$parent_id = $show->rs['parent_id'];
			
			if ($this->level == 1){
				// no es poden borrar el nivell 1
				$show->set_options(1,0,0,0);//$save_button = 1, $delete_button = 1, $preview_button = 0, $print_button = 0
			} 
		}
		$this->set_menus_level($parent_id);	
		
		$show->call('show_records_walk','pagina_id,paginatpl_id,listtpl_id',true);		
	    $ret = $show->show_form();
		
		return $ret;
	}
	function show_records_walk(&$show,$pagina_id,$paginatpl_id,$listtpl_id){
	
		$types = array('paginatpl','listtpl');
		
		foreach ($types as $type){
			// faig select pel tpl		
			$query = "SELECT " . $type . ", api__" . $type . "." . $type . "_id AS " . $type . "_id 
						FROM api__" . $type . ", api__" . $type . "_language 
						WHERE api__" . $type . "." . $type . "_id = api__" . $type . "_language." . $type . "_id 
						AND language = '" . LANGUAGE . "' 
						ORDER BY ordre ASC, " . $type . "_id ASC";
			
			$results = Db::get_rows($query);

			if (!${$type . '_id'}) ${$type . '_id'} = $results[0][$type . '_id'];
			$pagina_select = '';
			
			$show->set_var($type  . '_selected',${$type . '_id'});
			
			foreach ($results as $rs){
				$selected = (${$type . '_id'}==$rs[$type . '_id'])?' class="selected"':'';
				$pagina_select .= '<div rel="'.$rs[$type . '_id'].'"'.$selected.'><img title="'.$rs[$type].'" src="/admin/modules/api/' . $type . '_'.$rs[$type . '_id'].'.gif" /></div>';
			}
			$pagina_select = '
				
				<div class="image_select" id="'.$type.'_id_image_select">
				' . $pagina_select . 
					'<input id="'.$type.'_id_'.$pagina_id.'" name="'.$type.'_id['.$pagina_id.']" type="hidden" value="'.${$type . '_id'}.'" />
					<input name="old_'.$type.'_id['.$pagina_id.']" type="hidden" value="'.${$type . '_id'}.'" />
				</div>';
				
			$ret[$type . '_id'] = $pagina_select;		
		}
		
		
		// nomes mostro lo de google si hi ha link i no es form new ( el form _new no es pot editar el link, per tant no hi es
		if ($show->rs['link']) $show->set_var('is_full_pagina',false);
		
		return $ret;
	}	

	function save_rows()
	{
	    $save_rows = new SaveRows($this);
		
		if ($this->action == 'save_rows_bin_selected'){
			foreach ($save_rows->id as $id){
				$this->delete_children($id, 'bin');
			}
		}
		
		if ($this->action == 'save_rows_delete_selected'){
			foreach ($save_rows->id as $id){
				$this->delete_children($id, 'delete');
			}
		}
		
		if ($this->action == 'save_rows_restore_selected'){
		
			$results = Db::get_rows("SELECT pagina_id, level, parent_id FROM api__pagina WHERE pagina_id IN (" . implode(',',$save_rows->id) . ") ORDER BY level asc");
			
			
			//debug::p($results);
			//debug::p($_POST['selected']);
			
			// torno a fer l'array per posar nomès els que puc recuperar
			$_POST['selected'] = array();
			$notify_ids = array();
			
			foreach ($results as $rs){
				// primer nivell espoden recuperar tots
				if ($rs['level']==1){
					$_POST['selected'][] = $rs['pagina_id'];
				}
				// si es nivell 3, nomès s'ha de mirar un nivell amunt, ja que el seu parent de nivell 2 nomès es pot recuperar si el de nivell 1 està correcte
				else if ($rs['level']==2 || $rs['level']==3){
					$is_parent_not_deleted = Db::get_first("SELECT pagina_id 
							FROM api__pagina 
							WHERE bin = 0 AND pagina_id = " .  $rs['parent_id']);
					// si no està borrat el parent, o ja es a l'array de recuperar doncs podem recuperar
					
					if ($is_parent_not_deleted || in_array($rs['parent_id'],$_POST['selected']))
					{
						$_POST['selected'][] = $rs['pagina_id'];
					}
					else{
						$notify_ids[] = $rs['pagina_id'];
					}
				}
			}
			
			debug::p($_POST['selected'],'recuperar');
			debug::p($notify_ids,'no espot');
			
			$save_rows->notify($notify_ids,$this->messages['c_restore_items']);			
		}
	    $save_rows->save();
	}

	function write_record()
	{	 
		$writerec = new SaveRows($this);
		$writerec->set_field('modified','override_save_value','now()');						
		$writerec->field_unique = "pagina_file_name";
		
		if ($this->action == 'bin_record') $this->delete_children($writerec->id, 'bin');
		
		// Nomes es borra desde la paperera i en llistat
		// NO FUNCIONA AMB UN SOL REGISTRE, ja que no borraria els relacionats
		//if ($this->action == 'delete_record') $this->delete_children($save_rows->id, 'delete');
		
	    $writerec->save();
	}
	
	// al posar a la paperera també poso a la paperera tots els apartats i seccions o secions fills d'un registre
	// a la paperera nomès mostro els principals ( no puc controlar quan recuperen un registre fill d'un de la paperera )
	// al borrar també borro tots els apartats i seccions o secions fills d'un registre
	function delete_children($id, $action){
			$ids = $ids2 = false;
			$ids_array = $ids2_array = array();
			$ids_array = Db::get_rows("SELECT pagina_id FROM api__pagina WHERE parent_id = '" . $id . "'");
			$ids =  implode_field($ids_array, 'pagina_id');
			
			if ($ids) {
				$ids2_array = Db::get_rows("SELECT pagina_id FROM api__pagina WHERE parent_id IN (" .  $ids . ")");
				debug::add('Borrar subpagines', "SELECT pagina_id FROM api__pagina WHERE parent_id IN (" .  $ids . ")");
				$ids2 = implode_field($ids2_array, 'pagina_id');
			}
			debug::add('Borrar subpagines', $ids2);
			
			if ($action=='bin')
			{
				if ($ids){
					$query = "UPDATE api__pagina 
								SET bin='1' 
								WHERE pagina_id IN (" . $ids . ")";
					Db::execute($query);					
					debug::add('Bin subpagines', $query);
				}
				if ($ids2){
					$query = "UPDATE api__pagina 
							SET bin='1' 
							WHERE pagina_id IN (" . $ids2 . ")";
					Db::execute($query);					
					debug::add('Bin subsubpagines', $query);
				}
			}
			
			// afegeixo els ids de les pagines a borrar al post, així el motor s'encarrega de borrar els relacionats
			if ($action=='delete')
			{
				if ($ids){
					foreach ($ids_array as $rs){
						$_POST['selected'][]= $rs['pagina_id'];
					}
				}
				if ($ids2){
					foreach ($ids2_array as $rs){
					$_POST['selected'][]= $rs['pagina_id'];
					}
				}
			}
	}

	function manage_images()
	{	    
		if ($this->action=='list_records_images'){
		
			$id = $_GET[$this->tool_section . '_id'];
			$parent_id = Db::get_first("SELECT parent_id FROM api__pagina WHERE pagina_id=".$id);
			
			if ($this->level=='1'){		
			}
			elseif ($this->level=='2'){	
				$this->set_menus_level($parent_id);	
			}
			elseif ($this->level=='3'){		
				$this->set_menus_level($parent_id);
			}
		}
		
		$image_manager = new ImageManager($this);
		$image_manager->show_categorys = false;				
		
	    $image_manager->execute();
	}
}
?>