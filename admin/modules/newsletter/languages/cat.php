<?
// menus eina
if (!defined('NEWSLETTER_MENU_CUSTUMER')){
define('NEWSLETTER_MENU_CUSTUMER','Adreces');
define('NEWSLETTER_MENU_CUSTUMER_LIST','Llistat d\'adreces');
define('NEWSLETTER_MENU_CUSTUMER_REMOVED','Adreces donades de baixa');
define('NEWSLETTER_MENU_CUSTUMER_NEW','Insertar adreça');
define('NEWSLETTER_MENU_CUSTUMER_BIN','Paperera de reciclatge');

define('NEWSLETTER_MENU_GROUP','Grup');
define('NEWSLETTER_MENU_GROUP_NEW','Insertar grup');
define('NEWSLETTER_MENU_GROUP_LIST','Llistar grups');


define('NEWSLETTER_MENU_MESSAGE','Mail');
define('NEWSLETTER_MENU_MESSAGE_SEND','Enviar mail');
define('NEWSLETTER_MENU_MESSAGE_SEND_SAVED','Esborranys');
define('NEWSLETTER_MENU_MESSAGE_SEND_FROM_HISTORY','Missatges enviats');
define('NEWSLETTER_MENU_MESSAGE_NOT_SENT','Missatges no enviats');
define('NEWSLETTER_MENU_MESSAGE_BIN','Paperera de reciclatge');

define('NEWSLETTER_MENU_TEMPLATE','Plantilles');
define('NEWSLETTER_MENU_TEMPLATE_NEW_TEXT','Insertar plantilla text');
define('NEWSLETTER_MENU_TEMPLATE_LIST_TEXT','Llistat plantilles text');
define('NEWSLETTER_MENU_CONFIGADMIN','Configuració');



define('NEWSLETTER_CONFIGADMIN_MUST_CONFIGURE','Abans d\'enviar mail s\'ha de configurar l\'aplicació');
define('NEWSLETTER_GROUP_MUST_INSERT','Has de crear almenys un grup per insertar adreces de correu');

define('NEWSLETTER_MAIL_SEND_ONLY_TESTING','Executant en mode test, no s\'enviarà cap missatge');
define('NEWSLETTER_MAIL_SEND_SENDING','Començant el procès d\'enviament');
define('GETTING_ADDRESSES','Obtinguent les addresses de correu...');
define('GOT_ADDRESSES','addresses obtingudes!<br>');
define('GETTING_MESSAGE','Generant missatges...');
define('GETTING_MESSAGE_ONE','Generant missatge...');
define('GOT_MESSAGE','Missatges generats!<br>');
define('GOT_MESSAGE_ONE','Missatge generat!');
define('SENDING_MESSAGE','Enviant missatges...<br>');
define('SENDING_MESSAGE_ONE','Enviant missatge a:');
define('MESSAGE_SENT','Missatges enviats!');
define('MESSAGE_NOT_SENT','Missatges no enviats!');
define('NO_MESSAGE_SENT','Cap missatge enviat!');
define('MESSAGE_SENT_ONE_A','<b>Missatge </b>');
define('MESSAGE_SENT_ONE_B','<b>enviat a :</b>');
define('MESSAGE_NOT_SENT_ONE_A','<b>El missatge </b>');
define('MESSAGE_NOT_SENT_ONE_B','<b> no s\'ha pogut enviar: </b>');
define('MESSAGE_SAVED_NOT_SENT','<b>El missatge ha estat guardat a \"' . NEWSLETTER_MENU_MESSAGE_NOT_SENT . '\"</b><br>');
define('MESSAGE_SAVED_NOT_SENT_ONE','<b>El missatge ha estat guardat a \"' . NEWSLETTER_MENU_MESSAGE_NOT_SENT . '\"</b><br>');
define('MESSAGE_SAVED_NOT_SENT_TEST','<b>El missatge ha estat guardat a \"' . NEWSLETTER_MENU_MESSAGE_SEND_SAVED . '\"</b><br>');
define('MESSAGE_QUEUE_STOPPED',"Cua d'enviament detinguda, esperant per poder enviar ... ");
}

// captions seccio
$gl_caption_custumer['c_mail'] = 'Adreça d\'E-mail';
$gl_caption_custumer['c_name'] = 'Nom';
$gl_caption_custumer['c_surname'] = 'Cognoms';
$gl_caption_custumer['c_surname1'] = 'Primer cognom';
$gl_caption_custumer['c_surname2'] = 'Segon cognom';
$gl_caption['c_group_id'] = 'Grup';
$gl_caption_custumer['c_vat'] = 'DNI/NIF/NIE';
$gl_caption_custumer['c_phone'] = 'Telèfons';
$gl_caption_custumer['c_phone1'] = 'Tel. 1';
$gl_caption_custumer['c_phone2'] = 'Tel. 2';
$gl_caption_custumer['c_phone3'] = 'Fax';
$gl_caption_custumer['c_adress'] = 'Carrer';
$gl_caption_custumer['c_numstreet'] = 'Número';
$gl_caption_custumer['c_block'] = 'Bloc';
$gl_caption_custumer['c_flat'] = 'Pis';
$gl_caption_custumer['c_door'] = 'Porta';
$gl_caption_custumer['c_town'] = 'Població';
$gl_caption_custumer['c_province'] = 'Provincia';
$gl_caption_custumer['c_country'] = 'País';
$gl_caption_custumer['c_bankdata'] = 'Dades bancàries';
$gl_caption_custumer['c_bank'] = 'Caixa / Banc';
$gl_caption_custumer['c_comta'] = 'IBAN';
$gl_caption_custumer['c_account_holder'] = 'Titular';
$gl_caption_custumer['c_bic'] = 'BIC';
$gl_caption_custumer['c_mail'] = 'Correu electrònic';
$gl_caption_custumer['c_zip'] = 'Codi postal';
$gl_caption_custumer['c_observations1'] = 'Observacions';
$gl_caption_custumer['c_surnames'] = 'Cognoms';
$gl_caption_custumer['c_address'] = 'Adreça';

$gl_caption_custumer['c_action'] = 'Acció';
$gl_caption_custumer['c_edit'] = 'Editar';
$gl_caption_custumer['c_delete_selected'] = 'Esborrar seleccionats';
$gl_caption_custumer['c_restore_selected'] = 'Recuperar seleccionats';
$gl_caption_custumer['c_confirm_deleted'] = 'Els elements seleccionats s\\\'esborraran definitivament de la base de dades \\n\\nDesitges continuar?';
$gl_caption_custumer['c_select_addresses'] = 'Fes un click en una adreça de la llista per seleccionar-la';

$gl_caption_custumer['c_search'] = 'Buscar adreça';
$gl_caption_custumer['c_groups'] = 'Filtrar per grup';
$gl_caption_custumer['c_selected'] = 'Adreces seleccionades:';
$gl_caption_custumer['c_move_to_group_id'] = 'Moure al grup';
$gl_caption_custumer['c_enteredc']= 'Creat';
$gl_caption_custumer['c_group_caption']= '[cap grup]';
$gl_caption_custumer['c_select_all_groups'] = 'Tots els grups';
$gl_caption_custumer['c_prefered_language'] = 'Idioma';

$gl_caption_custumer['c_user_id'] = 'Agent';
$gl_caption_custumer['c_filter_user_id'] = 'Tots els agents';
$gl_caption ['c_user_id_format']='%s %s';
$gl_caption_custumer['c_category_id'] = 'Sol·licitud';
$gl_caption_custumer['c_filter_category_id'] = 'Totes les sol·licituds';

$gl_caption_custumer['c_filter_prefered_language'] = 'Tots els idiomes';
$gl_caption_custumer['c_filter_group_id'] = 'Tots els grups';


$gl_messages_custumer['select_search_no_records']='No hi ha cap adreça que contingui: ';
$gl_messages_custumer['select_address']='Fes un click en una adreça de la llista per seleccionar-la';


$gl_caption_group['c_group_name'] = 'Grup';
$gl_caption_group['c_description'] = 'Descripció';
$gl_caption_group['c_button_addresses'] = 'Veure adresses';


$gl_caption_message['c_message'] = 'Missatge';
$gl_caption_message['c_message_body'] = 'Cos del missatge';
$gl_caption_message['c_language_id'] = 'Idioma';
$gl_caption_message['c_send_to_same_language_only'] = "Enviar sólo a clientes con este idioma";
$gl_caption_message['c_send_to_no_language_too'] = "Enviar también a clientes sin idioma establecido";
$gl_caption_message['c_templatehtml_id'] = 'Plantilla';
$gl_caption_message['c_templatetext_id'] = 'Plantilla';
$gl_caption_message['c_from_address'] = 'E-mail';
$gl_caption_message['c_from_address_repeat'] = 'Repetir e-mail';
$gl_caption_message['c_from_name'] = 'Nom';
$gl_caption_message['c_reply_to'] = 'E-mail';
$gl_caption_message['c_reply_to_repeat'] = 'Repetir e-mail';
$gl_caption_message['c_reply_to_name'] = 'Nom';
$gl_caption_message['c_subject'] = 'Assumpte';
$gl_caption_message['c_format'] = 'Format';
$gl_caption_message['c_send_method'] = 'Forma d\'enviament';
$gl_caption_message['c_CC'] = 'Còpia a cada adreça (CC)';
$gl_caption_message['c_BCC'] = 'Còpia oculta a cada adreça (BCC)';
$gl_caption_message['c_one_by_one'] = 'Un missatge diferent per cada adreça';
$gl_caption_message['c_addresses_options'] = 'Opcions';
$gl_caption_message['c_from_title'] = 'De';
$gl_caption_message['c_reply_to_title'] = 'Contestar a';
$gl_caption_message['c_send'] = 'Enviar Missatge';
$gl_caption_message['c_save'] = 'Guardar a Esborranys';
$gl_caption_message['c_text'] = 'text';
$gl_caption_message['c_html'] = 'html';
$gl_caption_message['c_picture'] = 'Imatges';
$gl_caption_message['c_send_caption'] = 'Destinataris';
$gl_caption_message['c_send_to_addresses'] = 'Enviar als seleccionats';
$gl_caption_message['c_select_addresses'] = 'Seleccionar adreces...';

$gl_caption_message['c_select_property'] = 'Enviar immobles';
$gl_caption_message['c_select_inmo_property_link'] = 'Seleccionar immobles...';
$gl_caption_message['c_select_inmo_property_list_records'] = 'llistat';
$gl_caption_message['c_select_inmo_property_show_record'] = 'fitxes';

$gl_caption_message['c_select_product'] = 'Enviar productes';
$gl_caption_message['c_select_product_product_link'] = 'Seleccionar productes ...';
$gl_caption_message['c_select_product_product_list_records'] = 'llistat';
$gl_caption_message['c_select_product_product_show_record'] = 'fitxes';
$gl_caption_message['c_select_inmo_property_show_record'] = 'fitxes';

$gl_caption_message['c_select_new'] = 'Enviar notícies';
$gl_caption_message['c_select_news_new_link'] = 'Seleccionar notícies ...';
$gl_caption_message['c_select_news_new_list_records'] = 'llistat';
$gl_caption_message['c_select_news_new_show_record'] = 'fitxes';

$gl_caption_message['c_group_id'] = 'Enviar al grup';
$gl_caption_message['c_preview'] = 'Previsualitzar';
$gl_caption_message['c_text_options'] = 'Les següents opcions s\'agafen de la configuració automàticament, de toted maneres es poden canviar per aquest missatge';
$gl_caption_message['c_file'] = 'Adjuntar arxiu';
$gl_caption_message['c_content_extra'] = 'Contingut inclòs';
$gl_caption_message['c_entered'] = 'Creat';
$gl_caption_message['c_modified'] = 'Modificat';
$gl_caption_message['c_template_caption']='[cap plantilla]';
$gl_caption_message['c_messages_sent'] = 'Enviats';
$gl_caption['c_sent'] = 'Enviats';
$gl_caption['c_sendstart'] = $gl_caption_message['c_sent'] = 'Enviat';
$gl_caption_message['c_messages_sent_message'] = 'de';
$gl_caption_message['c_edit_button'] = 'Editar i enviar';
$gl_caption_message['c_not_send_button'] = 'enviar restants';
$gl_caption_message['c_re_send_button'] = 'Editar i reenviar';


$gl_caption_configadmin = $gl_caption_message;
$gl_caption_configadmin['c_templatehtml_id'] = 'Plantilla en html';
$gl_caption_configadmin['c_templatetext_id'] = 'Plantilla de text';
$gl_caption_configadmin['c_group_1'] = 'De';
$gl_caption_configadmin['c_group_2'] = 'Contestar a';
$gl_caption_configadmin['c_group_3'] = 'Dades SMTP';
$gl_caption_configadmin['c_m_username'] = 'Nom d\'usuari';
$gl_caption_configadmin['c_m_password'] = 'Contrasenya';
$gl_caption_configadmin['c_m_password_repeat'] = 'Repetir contrasenya';
$gl_caption_configadmin['c_m_host'] = 'Servidor';
$gl_caption_configadmin['c_only_test'] = 'Mode test';
$gl_caption_configadmin['c_only_test_description'] = 'Si marques aquesta casella, no s\'enviarà cap mail, nomès es simularà l\'enviament';
$gl_caption_configadmin['c_custumer_table'] = 'Taula d\'adreces';
$gl_caption_configadmin['c_newsletter__custumer'] = 'newsletter__custumer';
$gl_caption_configadmin['c_custumer__custumer'] = 'custumer__custumer';
$gl_caption_configadmin['c_product__customer'] = 'product__customer';


$gl_caption_templatetext['name'] = 'Nom';
$gl_caption_templatetext['header'] = 'Cap';
$gl_caption_templatetext['footer'] = 'Peu';
$gl_caption_templatetext['send'] = 'Enviar';

// en el cos del mail
$gl_caption_message['c_remove']['cat'] = 'Si no desitges rebre més missatges d\'aquesta llista de correu pots donar-te de baixa <a href="%s">aquí</a>';
$gl_caption_message['c_remove']['spa'] = 'Si no deseas recibir más mensajes de esta lista de correo puedes darte de baja <a href="%s">aquí</a>';
$gl_caption_message['c_remove']['fra'] = 'Si vous ne désirez pas recevoir d\'autres messages de cette liste de diffusion, vous pouvez vous désabonner <a href="%s">ici</a>';
$gl_caption_message['c_remove']['eng'] = 'If you do not wish to continue receiving mails from our newsletter, you may unsubscribe <a href="%s">here</a>';

$gl_caption_customer = $gl_caption_custumer;

$gl_caption_message['c_select_agenda'] = 'Enviar activitats';
$gl_caption_message['c_select_llemena_agenda_link'] = 'Seleccionar activitats ...';
$gl_caption_message['c_select_llemena_agenda_list_records'] = 'llistat';
$gl_caption_message['c_select_llemena_agenda_show_record'] = 'fitxes';

$gl_caption_message['c_select_activitat'] = 'Enviar activitats';
$gl_caption_message['c_select_lassdive_activitat_link'] = 'Seleccionar activitats ...';
$gl_caption_message['c_select_lassdive_activitat_list_records'] = 'llistat';
$gl_caption_message['c_select_lassdive_activitat_show_record'] = 'fitxes';

$gl_caption_custumer['c_delete_button'] = 'Eliminar';
$gl_caption_custumer['c_group_sent'] = 'Enviats';
$gl_caption_custumer['c_group_not_sent'] = 'No enviats';
$gl_messages['deleted_not_sent'] = 'S\'ha esborrat l\'adreça de la llista de destinataris';


$gl_caption['c_import_preview'] = 'Previsualitzar importació';
$gl_caption['c_import'] = 'Importar';
$gl_caption['c_import_title'] = 'Importar adreces';
$gl_caption['c_select_file'] = 'Seleccionar arxiu';
$gl_caption['c_group'] = 'Grup';

$gl_caption['c_max_sends_reached'] = "S'ha arribat al màxim d'emails enviats, enviar més podria ser motiu de SPAM";
$gl_caption['c_max_sends_reached_2'] = "S'aconsella enviar la resta en un mínim de 2 hores";
$gl_caption['c_max_sends_reached_button'] = "Enviar igualment la resta ara";