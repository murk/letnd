<?
/*

CRON - cada hora
0 * * * * php -f "/var/www/letnd.com/datos/motor/admin/modules/admintotal/admintotal_fotocasa_cli.php" > /var/www/letnd.com/datos/motor/data/logs/fotocasa.htm

SERVIDOR
php -f "/var/www/letnd.com/datos/motor/admin/modules/admintotal/admintotal_fotocasa_cli.php" > /var/www/letnd.com/datos/motor/data/logs/fotocasa.htm
php -d memory_limit=512M "/var/www/letnd.com/datos/motor/admin/modules/admintotal/admintotal_fotocasa_cli.php"

SERVIDOR PER EXCLOURE IMATGES I UNA AGENCIA CONCRETA

php /var/www/letnd.com/datos/motor/admin/modules/admintotal/admintotal_fotocasa_cli.php exclude_images agencia_id=208 > /var/www/letnd.com/datos/motor/data/logs/fotocasa.htm

WEB
/admin/?tool=admintotal&tool_section=fotocasa&action=start

VAGRANT
php "/letnd/admin/modules/admintotal/admintotal_fotocasa_cli.php" agencia_id=10 > /letnd/data/logs/fotocasa_linux.htm
php -d include_path=/php_includes "/letnd/admin/modules/admintotal/admintotal_fotocasa_cli.php"

DOS
C:\server\php5\php.exe -f "D:\Letnd\admin\modules\admintotal\admintotal_fotocasa_cli.php" > d:\letnd\data\logs\fotocasa_windows.htm

DEBUG CLI EX
php -f "/letnd/admin/modules/admintotal/admintotal_fotocasa_cli.php" debug > /letnd/data/logs/fotocasa_linux.htm

*/

$dir = str_replace ( '/admin/modules/admintotal','/common/', __DIR__ );
$dir = str_replace ( '\admin\modules\admintotal','\\common\\', $dir );

include ($dir . 'cli_init.php');

include (DOCUMENT_ROOT . 'admin/modules/admintotal/admintotal_fotocasa.php');

include (DOCUMENT_ROOT . 'common/classes/upload_files.php');

$fotocasa = New AdmintotalFotocasa;
$fotocasa->is_cli = true;
$fotocasa->_start();

send_mail_admintotal('FOTOCASA - Exportació finalitzada', 'Data: ' . now(true));