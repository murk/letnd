<?php
// menus eina

if ( ! defined( 'UTILITY_MENU_UTILITIES' ) ) {
	define('UTILITY_MENU_UTILITIES','Utilidades');
	define('UTILITY_MENU_UTILITIES_QUOTA','Cálculo de cuotas');
	define('UTILITY_MENU_UTILITIES_EXPENSE','Cálculo de gastos');
	define('UTILITY_MENU_UTILITIES_SAVE','Ahorro fiscal');
	define('UTILITY_MENU_UTILITIES_CALCULATOR','Calculadora');

	define( 'UTILITY_MENU_BUY', 'Cálculo de gastos compra' );
	define( 'UTILITY_MENU_SELL', 'Cálculo de gastos venta' );

	define ( 'c_workout', 'Calcular' );
}

$gl_caption_utilities['c_titlec']= "Cálculo de cuotas para las hipotecas.";
$gl_caption_utilities['c_price']= "Precio (".CURRENCY_NAME."):";
$gl_caption_utilities['c_deposit']= "Entrada (".CURRENCY_NAME."):";
$gl_caption_utilities['c_interest']= "Interés (%):";
$gl_caption_utilities['c_years']= "Anños:";
$gl_caption_utilities['c_cuota']= "Cuota (".CURRENCY_NAME."):";
$gl_caption_utilities['c_notice']="La información obtenida en esta simulación de cálculo és totalmente orientativa y no implica ningún compromiso, ni ningún vinculo jurídico-legal.";
$gl_caption_utilities['c_titles']= "Cálculo del ahorro fiscal.";
$gl_caption_utilities['c_import_prestec']= "Importe del prestamo.";
$gl_caption_utilities['c_period_years']= "Terminio de amortitzación:";
$gl_caption_utilities['c_coin']="Moneda";
$gl_caption_utilities['c_eur']="Euros";
$gl_caption_utilities['c_pts']="Pesetas";
$gl_caption_utilities['c_workout']= "Deducción:";
$gl_caption_utilities['c_single']= "Individual";
$gl_caption_utilities['c_couplegroup']= "Dos titulares. Conjunta:";
$gl_caption_utilities['c_couplesingle']= "Dos titulares. Individuales:";
$gl_caption_utilities['c_saveyear']= "Ahorro primer año:";
$gl_caption_utilities['c_completesave']= "Ahorro total de la operación:";
$gl_caption_utilities['c_info']="La información obtenida en esta simulación de cálculo és totalmente orientativa y no implica ningún compromiso, ni ningún vinculo jurídico-legal.";

$gl_caption_utilities['c_calculator']= "Calculadora";


/**
 * Nou
 *
 */

$gl_caption_buy['c_form_title']  = "Cálculo gastos de compra de un inmueble";
$gl_caption_sell['c_form_title'] = "Cálculo gastos de venta de un inmueble";

/* Compra */
$gl_caption['c_price']                     = "Precio del inmueble";
$gl_caption['c_notary']                    = "Gastos notario";
$gl_caption['c_notary_percent']            = "%";
$gl_caption['c_register']                  = "Gastos registro";
$gl_caption['c_register_percent']          = "%";
$gl_caption['c_taxtype_id']                = "Impuestos aplicables";
$gl_caption['c_agency']                    = "Gastos gestión";
$gl_caption['c_agency_percent']            = "%";
$gl_caption['c_mortgage']                  = "Hipoteca";
$gl_caption['c_mortgage_percent']          = "%";
$gl_caption['c_mortgage_open']             = "Apertura";
$gl_caption['c_mortgage_open_percent']     = "%";
$gl_caption['c_mortgage_initial_interest'] = "Interés inicial";
$gl_caption['c_mortgage_euribor']          = "Incremento euribor";
$gl_caption['c_mortgage_start_date']       = "Fecha inicial";
$gl_caption['c_mortgage_length']           = "Duración";
$gl_caption['c_mortgage_years']            = "Años";
$gl_caption['c_mortgage_register']         = "Registro";
$gl_caption['c_mortgage_register_percent'] = "%";
$gl_caption['c_mortgage_notary']           = "Notario";
$gl_caption['c_mortgage_notary_percent']   = "%";
$gl_caption['c_mortgage_valuation']        = "Tasación";

/* Reports compra */
$gl_caption['c_report_title']         = "Gastos de compra";
$gl_caption['c_expense_not_financed'] = "Importe no financiado";
$gl_caption['c_mortgage_title']       = "Hipoteca";
$gl_caption['c_down_payment']         = "Entrada";
$gl_caption['c_tax']                  = "Impuestos";
$gl_caption['c_total_expenses']       = "Gastos";
$gl_caption['c_total']                = "Total";
$gl_caption['c_judicial_acts']        = "Actos jurídicos documentados";
$gl_caption['c_mortgage_title']       = "Hipoteca";
$gl_caption['c_no_vinculant']         = "Estos cálculos son orientativos y en ningún caso vinculantes";
$gl_caption['c_monthly_payment']      = "Mensualidad";

$gl_caption['c_judicial_acts_mortgage'] = "Actos jurídicos responsabilidad hipotecaria";
$gl_caption['c_table_quote']            = "Cuota";
$gl_caption['c_table_year']             = "Año";
$gl_caption['c_table_payment']          = "Amortización";
$gl_caption['c_table_interest']         = "Intereses";
$gl_caption['c_table_balance']          = "Capital pendiente";
$gl_caption['c_table_total_returned']   = "Capital amortizado";
$gl_caption['c_table_total_paid']       = "Total pagado";

/* Venda */
$gl_caption_sell['c_price']           = "Precio de venta";
$gl_caption_sell['c_price_real']      = "Precio de venta neto";
$gl_caption['c_price_bought']         = "Precio de compra";
$gl_caption['c_price_bought_real']    = "Precio de compra neto";
$gl_caption['c_price_bought_updated'] = "Precio compra actualizado";
$gl_caption['c_earning']              = "Beneficio";

$gl_caption['c_cadastral_price']           = "Valor catastral";
$gl_caption['c_date_bought']               = "Fecha de compra";
$gl_caption['c_date_sold']                 = "Fecha de venta";
$gl_caption['c_mortgage_cancel_percent']   = "%";
$gl_caption['c_other_expenses']            = "Otros gastos de compra";
$gl_caption['c_mortgage_cancel']           = "Comisión de cancelación";
$gl_caption['c_municipi']                  = "Municipio";
$gl_caption['c_estate_commission']         = "Comisión agencia";
$gl_caption['c_estate_commission_percent'] = "%";
$gl_caption['c_habitability_card']         = "Cédula de habitabilidad";
$gl_caption['c_efficiency']                = "Certificado de eficiencia energética";

/* Reports venda */
$gl_caption['c_report_title_sell']              = "Gastos de venta";
$gl_caption['c_report_title_expenses']          = "Gastos varios";
$gl_caption['c_report_title_expenses_top']      = "Gastos";
$gl_caption['c_report_title_renda']             = "Impuesto sobre la renta";
$gl_caption['c_report_title_renda_top']         = "Renta";
$gl_caption['c_plusvalua']                      = "Plusvalía";
$gl_caption['c_days']                           = "Días";
$gl_caption['c_total_days']                     = "Total de días";
$gl_caption['c_days_before_1996']               = "Días hasta el 31/12/1996";
$gl_caption['c_days_before_2006']               = "Días hasta el 19/01/2006";
$gl_caption['c_days_after_2006']                = "Días desde el 19/01/2006 hasta fecha venta";
$gl_caption['c_earning_before_2006']            = "Beneficio hasta el 19/01/2006";
$gl_caption['c_earning_after_2006']             = "Beneficio desde el 19/01/2006 hasta fecha venta";
$gl_caption['c_total_to_tribute']               = "Total a tributar";
$gl_caption['c_to_tribute_before_2006']         = "Tributa sobre beneficios de antes del 19/01/2006";
$gl_caption['c_earning_before_2006_reductible'] = "Cantidad reducible de beneficios de antes del 19/01/2006";
$gl_caption['c_to_tribute_before_2006_anyway']  = "Tributa sobre beneficios de antes del 19/01/2006 por exceder los 400.000 € de venta";
$gl_caption['c_to_tribute_after_2006']          = "Tributa sobre beneficios de después de 19/01/2006";
$gl_caption['c_coeficient_before_1996']         = "Reducción";
$gl_caption['c_tribute_summary']                = "Resumen pago";
$gl_caption['c_tribute_zone_1']                 = "Hasta 6.000 € - 19%";
$gl_caption['c_tribute_zone_2']                 = "De 6.000 a 50.000 € - 21%";
$gl_caption['c_tribute_zone_3']                 = "Más de 50.000 € - 23%";
$gl_caption['c_tribute']                        = "Total a pagar";

?>