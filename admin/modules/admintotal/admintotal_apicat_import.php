<?
/**
 * GHESTIA IMPORTACIÓ
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) Decembre 2018
 * @version 1
 * @access public
 */

/*
REQUISITS

	1 - Posar la variable a client__client inmo_apicat_import a 1
	2 - Assegurar-se que la url principal sigui la primera ( domain de la taula client__client ): ej. easybrava.com i no easybrava.cat
	3 - Referencies manuals?



CARACTERÍSTIQUES

	Tots els immobles s'importen en estat públic
	Províncies, poblacions i zones no tenen Id, per tant es busca si alguna és igual o si s'hi assembla, això pot portar a algún error




PER FER

	TODO-i Posar import_only_first_time a 1 quan ho posi online a easybrava
	TODO-i  Si un apialia borra un inmoble
	TODO-i Suites hauria de sumarse a rooms

	Canvis a exposrt__apicat
	------------------------

		UPDATE `letnd`.`export__apicat` SET `function`='get_ref' WHERE  `id`=2;
		UPDATE `letnd`.`export__apicat` SET `apicat_field`='Eficiencia energetica' WHERE  `id`=55;
		UPDATE `letnd`.`export__apicat` SET `apicat_table`='MODIFICADOS' WHERE  `id`=55;
		UPDATE `letnd`.`export__apicat` SET `function`='get_date' WHERE  `id`=55;

		UPDATE `letnd`.`export__apicat` SET `function`='get_cedula' WHERE  `id`=5;
		UPDATE `letnd`.`export__apicat` SET `apicat_table`='MODIFICADOS', `apicat_field`='Cédula habitabilidad' WHERE  `id`=5;

		UPDATE `letnd`.`export__apicat` SET `function`='get_caracteristica_boolean' WHERE  `id`=87;

		REPLACE INTO `export__apicat` (`id`, `apicat_table`, `apicat_field`, `apicat_excelcol`, `letnd_table`, `letnd_field`, `function`, `condition`, `observacions`) VALUES (53, 'MODIFICADOS', 'Categoría del consumo energético', '', 'inmo__property', 'efficiency', 'get_caracteristica', '$val != \'progress\' && $val != \'exempt\'', '');
		REPLACE INTO `export__apicat` (`id`, `apicat_table`, `apicat_field`, `apicat_excelcol`, `letnd_table`, `letnd_field`, `function`, `condition`, `observacions`) VALUES (54, 'MODIFICADOS', 'Categoría de les emisiones', '', 'inmo__property', 'efficiency2', 'get_caracteristica', '$val != \'progress\' && $val != \'exempt\'', 'no');


		INSERT INTO `export__apicat` (`id`, `apicat_table`, `apicat_field`, `apicat_excelcol`, `letnd_table`, `letnd_field`, `function`, `condition`, `observacions`) VALUES (122, 'MODIFICADOS', 'Consumo energético (kWh/m2 año)', '', 'inmo__property', 'efficiency_number', 'get_caracteristica', '', 'no');
		INSERT INTO `export__apicat` (`id`, `apicat_table`, `apicat_field`, `apicat_excelcol`, `letnd_table`, `letnd_field`, `function`, `condition`, `observacions`) VALUES (123, 'MODIFICADOS', 'Emisiones CO2 (kg CO2/m2 año)', '', 'inmo__property', 'efficiency_number2', 'get_caracteristica', '', 'no');


PER ELLS

	No te en compte que pugui haver característiques repetides al XML, per ej:

      <Caracteristica>
        <Descripcion>Suelos</Descripcion>
        <Valor>Gres</Valor>
      </Caracteristica>
      <Caracteristica>
        <Descripcion>Suelos</Descripcion>
        <Valor>Terrazo</Valor>
      </Caracteristica>

LETND NO TE

	Categories de categories d'inmobles
	FechaFinExclusiva
	PrecioTrapaso

GHESTIA NO TE




VARS
TODO-i Esborrar de la taula export tots els que no estiguin relacionats amb cap client  ( una consulta per agencia )

RESET PER PROVES

TRUNCATE `inmo__property`;
TRUNCATE `inmo__property_file`;
TRUNCATE `inmo__property_image`;
TRUNCATE `inmo__property_imagecat`;
TRUNCATE `inmo__property_image_language`;
TRUNCATE `inmo__property_language`;
TRUNCATE `inmo__property_to_portal`;
TRUNCATE `inmo__property_to_tv`;
TRUNCATE `inmo__property_to_vreasy`;
TRUNCATE `inmo__property_video`;

TRUNCATE `letnd`.`export__apicat_import_property`;

*/

class AdmintotalApicatImport extends Module {

	var $config_rs = [];
	var $import_rs = [];
	var $property_rs;
	var $details_rs = [];
	var $agencia_rs = [];
	var $conta = 0;
	var $is_cli = false;
	var $image_manager;
	var $is_new;
	var $id_agencia;
	var $letnd_key;

	var $apicat_anunci_languages = [
		'spa' => '',
		'cat' => 'CAT',
		'eng' => 'ENG',
		'fra' => 'FR',
		'deu' => 'GER',
		'ita' => 'ITA',
		'por' => 'PT',
	];

	var $apicat_facings = [
		'e'  => 'Este',
		'o'  => 'Oeste',
		's'  => 'Sur',
		'se' => 'Sur-Este',
		'so' => 'Sur-Oeste',
		'n'  => 'Norte',
		'ne' => 'Nord-Este',
		'no' => 'Nord-Oeste',
	];

	var $apicat_floors = [
		'Baldosa'     => 'ceramic',
		'Terrazo'     => 'ceramic',
		'Mármol'      => 'marble',
		'Registrable' => 'laminated',
		'Cerámico'    => 'ceramic',
		'Mosaico'     => 'ceramic',
		'Pulido'      => 'ceramic',
		'Otros'       => '',
		'Parquet'     => 'parket',
		'Mixto'       => [ 'ceramic', 'parket' ],
		'Madera'      => 'parket',
		'Porcelanato' => 'gres',
		'Gres'        => 'gres',
		'Hidraulico'  => 'ceramic',
		'Ceramico'    => 'ceramic',
	];

	// Els que no tinc no els poso, no val la pena inventar. Hauriem d'afegir-los tots a letnd
	var $apicat_views = array(
		// '' => 'Montaña/valle',
		// '' => 'Puerto',
		// '' => 'Zona deportiva',
		// '' => 'Lago',
		// '' => 'Calle',
		// '' => 'Patio interior manzana',
		// '' => 'Naturaleza',
		// '' => 'Zona comunitaria',
		// '' => 'Sin vistas',
		// '' => 'Zona verde',
		// '' => 'Ciudad',
		'sea_view'   => 'Mar',
		// '' => 'Montaña',
		// '' => 'Calle',
		// '' => 'Patio interior',
		// '' => 'Otros',
		'clear_view' => 'Buenas vistas',
		// '' => 'Zona deportiva',
	);

	function __construct() {
		include( 'config_mls.php' ); // està al path includes
		parent::__construct();

		include_once (DOCUMENT_ROOT . 'common/classes/template.php');
		include_once (DOCUMENT_ROOT . 'common/classes/db_form_object.php');
		include_once (DOCUMENT_ROOT . 'common/classes/db_image_manager.php');
		include_once( DOCUMENT_ROOT . 'admin/modules/admintotal/export_common.php' );

		$module              = module::load( 'inmo', 'property', '', false );
		$this->image_manager = new ImageManager( $module );

		// Les giro, així les puc tindre com al export
		$this->apicat_facings = array_flip( $this->apicat_facings );
		$this->apicat_views   = array_flip( $this->apicat_views );

	}

	function start() {
		$this->_start();
	}

	function _start() {

		ExportCommon::p( "EXPORTACIÓ GHESTIA: " .  date( "d-m-Y H:i" ), 1 );

		set_time_limit( 60 * 60 );        //ini_set('implicit_flush', true);		//error_reporting(E_WARNING);

		echo '<link href="/admin/themes/inmotools/styles/export.css" rel="stylesheet" type="text/css">';

		Db::connect_mother();

		$agencias = $this->get_agencias();

		$this->import_properties( $agencias );

		ExportCommon::show_end();

		if ( ! $GLOBALS['gl_is_local'] ) send_mail_admintotal( 'APICAT - Exportació finalitzada', 'Data: ' . now( true ) );

		html_end();
	}

	function get_config_results() {


		$query = "SELECT apicat_field, letnd_table, letnd_field, `function`, `condition`
							FROM export__apicat
							WHERE apicat_table <>  ''";

		return Db::get_rows( $query );

	}

	function get_agencias() {

		$query    = "SELECT client_id, comercialname, nif, inmo_apicat_import, inmo_apicat_import_user, inmo_apicat_import_pass, province, town, typestreet, adress, numstreet, zip, respname, surnames, phone1, fax, mail1, domain, dbname, client_dir FROM letnd.client__client WHERE inmo_apicat_import = 1";
		$agencias = Db::get_rows( $query );

		//Debug::p( $results, 'Clients' );

		return $agencias;

	}


	private function get_agencia_config() {

		$val = Db::get_first( "SELECT value FROM `{$this->agencia_rs['dbname']}`.inmo__configadmin WHERE name = 'apicat_import_only_first_time'" );

		$this->agencia_rs['import_only_first_time'] = $val;

		$val = Db::get_first( "SELECT value FROM `{$this->agencia_rs['dbname']}`.inmo__configadmin WHERE name = 'property_auto_ref'" );

		$this->agencia_rs['property_auto_ref'] = $val;

		$this->agencia_rs['languages'] = [];

		$languages = Db::get_rows( "SELECT code FROM `{$this->agencia_rs['dbname']}`.all__language WHERE public != 0" );

		foreach ( $languages as $language ) {
			$this->agencia_rs['languages'][ $language['code'] ] = $this->apicat_anunci_languages [ $language['code'] ];
		}

	}


	function import_properties( $agencias ) {

		global $gl_is_local;

		// TEST
		/*for ( $i = 0; $i < 5; $i ++ ) {
			$agencias = array_merge( $agencias, $agencias );
		}*/

		foreach ( $agencias as $rs ) {


			$client_dir = $rs['client_dir'];
			if ( $gl_is_local ) {
				$path = DOCUMENT_ROOT . "$client_dir";
			}
			else {
				$domain = $rs['domain'];
				$path   = "/var/www/$domain/datos/web/$client_dir";
			}
			$rs['path'] = $path;
			// $rs['images_dir'] = "$path/inmo/property/images/";
			$xml_path   = "$path/temp/apicat";

			$this->agencia_rs =  &$rs;
			$this->get_agencia_config();

			if ($this->agencia_rs['property_auto_ref']) {
				ExportCommon::p("{$this->agencia_rs['comercialname']}: No es pot importar amb referències automàtiques", 1);
				continue;
			}

			$this->image_manager->images_dir = "$path/inmo/property/images/";
			$this->download_client_files( $xml_path );

			$xml_agencias = $this->get_xml( "$xml_path/AGENCIAS", 'Agencia' );
			$this->set_agencia_id( $xml_agencias );
			unset( $xml_agencias );

			$xml = $this->get_xml( "$xml_path/INMUEBLES_MODIFICADOS", 'inmueble' );
			$this->import_modificados( $xml );
			unset( $xml );

			//$xml_borrados = $this->get_xml( "$xml_path/INMUEBLES_BORRADOS", 'ArrayOfAnyType' );
			//$this->import_borrados( $xml_borrados );
			//unset( $xml_borrados );
		}

	}

	private function get_details( &$rs ) {
		$ret = [];
		foreach ( $rs->Caracteristicas->children() as $caracteristica ) {
			// ExportCommon::p( $caracteristica->Descripcion, 3 );
			$ret[ (string) $caracteristica->Descripcion ] = current( $caracteristica->Valor );
		}

		return $ret;
	}

	private function set_agencia_id( &$xml ) {

		$doc = new DOMDocument;
		while ( $xml->name === 'Agencia' ) {

			$rs               = simplexml_import_dom( $doc->importNode( $xml->expand(), true ) );
			$this->id_agencia = current( $rs->IdAgencia );
			break;
		}
		unset($doc);

	}

	private function import_modificados( &$xml ) {

		ExportCommon::p( "Importació d'inmobles: " . $this->agencia_rs['comercialname'], 1 );

		$config_results = $this->get_config_results();

		while ( $xml->name === 'inmueble' ) {

			$doc            = new DOMDocument;

			$rs = simplexml_import_dom( $doc->importNode( $xml->expand(), true ) );

			$this->details_rs = $this->get_details( $rs );

			$this->property_rs = &$rs;
			$this->import_rs   = array();

			// Id
			$apicat_id                      = current( $rs->IdFicha );
			$ref                      = current( $rs->Referencia );
			$this->import_rs['property_id'] = $property_id = $this->get_property_id( $apicat_id );

			if ( ! $this->can_import( $property_id ) ) {
				$xml->next( 'inmueble' );
				ExportCommon::p( "NO S'IMPORTA - Ja s'ha importat un cop Id Letnd: {$property_id} - Id Ghestia: {$apicat_id} - Ref: {$ref}", 4 );
				continue;
			}

			// Altres Valors
			$this->import_rs['entered']    = current( $rs->FechaAlta );
			$this->import_rs['modified']   = current( $rs->FechaModificacion );
			$this->import_rs['status']     = 'onsale'; // Public, de moment només té aquest valor
			$this->import_rs['country_id'] = '1'; // Valor nomes espanya
			$this->import_rs['zoom']       = '0';
			$this->set_property_privates( $apicat_id );

			foreach ( $config_results as $config_rs ) {
				$this->config_rs = &$config_rs;

				$key           = $config_rs['apicat_field'];
				$letnd_key = $this->letnd_key = $config_rs['letnd_field'];
				$val           = isset ( $this->details_rs[ $key ] ) ? $this->details_rs[ $key ] : current( $rs->$key ); // current( $rs->$key ): Puta merda de xmlSimple, només funciona així, sembla que crea un nivell de més en cada node

				if ( $config_rs['function'] ) {
					$val = $this->{$config_rs['function']}( $val );
					if ( $val !== false ) {
						$this->import_rs[ $letnd_key ] = $val;
					}
				}
				else {
					$this->import_rs[ $letnd_key ] = $val;
				}
			}
			$this->config_rs = array();

			ExportCommon::p( "Ref: {$this->import_rs['ref']} , id: $property_id", 2 );

			$property_id = $this->insert_property();
			$this->insert_anunci( $property_id );
			$this->get_images( $property_id );

			$xml->next( 'inmueble' );
		}

		unset( $this->property_rs );
		unset($doc);
		$this->import_rs = array();

	}

	function insert_property() {

		// Comprovo si ja he afegit abans l'inmoble

		// Insertar si no existeix i modificar si existeix
		$property_id = $this->import_rs['property_id'];
		$apicat_id   = current( $this->property_rs->IdFicha );

		$this->get_comarca_id();

		$q     = new Query( "{$this->agencia_rs['dbname']}.inmo__property" );
		$q_rel = new Query( "letnd.export__apicat_import_property" );

		if ( $property_id ) {
			$q->update( $this->import_rs );
			$q_rel->update( [
				'property_id' => $property_id,
				'modified'    => 'now()'
			],
				"property_id = '$property_id' 
				AND client_id = '{$this->agencia_rs['client_id']} '
				AND apicat_id = '$apicat_id'" );
		}
		else {
			$property_id = $q->insert( $this->import_rs )->insert_id;
			$q_rel->insert( [
				'property_id' => $property_id,
				'client_id'   => $this->agencia_rs['client_id'],
				'apicat_id'   => $apicat_id,
				'modified'    => 'now()'
			] );
		}

		ExportCommon::p( $q->query, 3 );

		return $property_id;
	}

	function insert_anunci( $property_id ) {

		$q = new Query( "`{$this->agencia_rs['dbname']}`.inmo__property_language" );

		foreach ( $this->agencia_rs['languages'] as $language => $apicat_language ) {
			$description = (string) $this->property_rs->{"Anuncio$apicat_language"};
			$is_update   = $q->get_first( 'count(*)', "property_id = $property_id AND language = '$language'" );

			if ( $is_update ) {
				$q->update( [
					'description' => $description
				],
					"property_id = '$property_id' AND language = '$language'" );
			}
			else {
				$q->insert( [
					'property_id' => $property_id,
					'language'    => $language,
					'description' => $description
				] );
			}

			ExportCommon::p( $q->query, 3 );
		}
	}

	// TODO-i Passar-ho a Upload->add_file i Image->add_image;
	function get_images( $property_id ) {

		if ( R::get_opt( 'exclude_images' ) ) return; // Només per testejar

		$q = new Query( "{$this->agencia_rs['dbname']}.inmo__property_image" );

		$ordre = $q->get_first( 'MAX(ordre)', "property_id = %s", $property_id );
		if ( ! $ordre ) $ordre = 0;

		$has_main_image = $q->get_first( 'count(*)', "property_id = %s AND main_image=1", $property_id );
		$main_image     = $has_main_image ? 0 : 1;

		foreach ( $this->property_rs->Imagenes->children() as $foto ) {
			$nom = current( $foto->Nombre );
			$url = current( $foto->url );

			$name_original = basename( $url );
			$image_tmp     = "{$this->agencia_rs['path']}/temp/$name_original";

			$image_id = $q->get_first( 'image_id', "property_id = %s AND name_original = %s", $property_id, $name_original );


			if ( $image_id ) {
				$q->update( [ 'name_original' => $name_original ], $image_id );
				$this->update_image_captions( $image_id, $nom );
			}
			else {
				$ordre ++;

				$download_attempts = 0;
				while (!is_file($image_tmp) && $download_attempts < 3){
					@copy( $url, $image_tmp );
					$download_attempts ++;
				}

				$size = round( ( filesize( $image_tmp ) / 1024 ) * 1000 ) / 1000;

				$image_id = $q->insert( [
					'property_id'   => $property_id,
					'name'          => $name_original,
					'name_original' => $name_original,
					'size'          => $size,
					'ordre'         => $ordre,
					'main_image'    => $main_image,
				] )->insert_id;

				$main_image = 0;
				$this->insert_images_captions( $q->insert_id, $nom );

				$ext = strrchr( substr( $name_original, - 5, 5 ), '.' );
				rename( $image_tmp, "{$this->image_manager->images_dir}$image_id{$ext}" );
				$cg = $this->get_agencia_images_config();

				$this->image_manager->resize_images( $q->insert_id, $ext, $cg, true );
			}

			ExportCommon::p( "Imatge: $nom -> $name_original", 3 );
			if ( $GLOBALS['gl_is_local'] && R::get_opt( 'max_images' ) ) if ( $ordre > R::get_opt( 'max_images' ) ) break;
		}

	}

	function update_image_captions( $image_id, $nom ) {

		// No tenen idiomes, tots van igual
		$q = new Query( "{$this->agencia_rs['dbname']}.inmo__property_image_language" );
		$q->update( [ 'image_title' => $nom ], "image_id = %s", $image_id );
	}

	function insert_images_captions( $insert_id, $nom ) {

		$q = new Query( "{$this->agencia_rs['dbname']}.inmo__property_image_language" );

		$rows = [];

		foreach ( $this->agencia_rs['languages'] as $language => $apicat_language ) {
			$rows[] = [ 'image_id' => $insert_id, 'image_title' => $nom, 'language' => $language ];
		}

		$q->insert( $rows );
	}

	function get_agencia_images_config() {

		$table   = "{$this->agencia_rs['dbname']}.inmo__configadmin";
		$query   = "SELECT name, value
				FROM $table";
		$results = Db::get_rows( $query );
		$config  = array();

		foreach ( $results as $rs ) {
			$value                 = $rs['value'];
			$config[ $rs['name'] ] = $value;
		}

		return $config;
	}

	/**
	 * Només posem els privates el primer cop que s'importa, no es modificarà desde APICAT, així es permet modificar a letnd
	 *
	 * @param $apicat_id
	 */
	private function set_property_privates( $apicat_id ) {
		if ( $this->is_new ) {
			$id_agencia = current( $this->property_rs->IdAgencia );

			$this->import_rs['property_private'] = "GHESTIA - $apicat_id";
			$this->import_rs['ref_collaborator'] = $id_agencia == $this->id_agencia ? '' : $id_agencia;
		}
	}

	private function can_import( $apicat_id ) {

		if ( ! $this->agencia_rs['import_only_first_time'] ) return true;

		return $this->is_new;

	}

	private function get_property_id( $apicat_id ) {

		$property_id = Db::get_first(
			"SELECT property_id
				FROM `letnd`.`export__apicat_import_property`
				WHERE apicat_id = %s
				AND client_id = %s;",
			$apicat_id, $this->agencia_rs['client_id']
		);

		$this->is_new = $property_id ? false : true;

		return $property_id;

	}

	// TODO-i  pER FER
	function import_borrados( &$xml_borrados ) {

		$table = '`' . $this->agencia_rs['dbname'] . '`.inmo__property';


	}

	function get_xml( $file, $element ) {

		// Haig de copiar primer, per que no se que passa amb els permisos quan descarrego de ftp, no em deixa obrir amb xml->open
		// $file_copy = "{$file}_copia.xml";
		$file .= ".xml";

		// copy( $file, $file_copy );

		$xml = new XMLReader();
		ExportCommon::p( "Obrint: $file", 3 );
		libxml_disable_entity_loader(false);
		// $xml->open( "file://$file_copy" );
		$xml->open( "file://$file" );


		// move to the first <inmueble /> node
		while ( $xml->read() && $xml->name !== $element ) {
			;
		}

		return $xml;

	}

	// Funcions per camps
	/*
	 *
	 *  PER PODER VEURE QUE CORRESPON A dESCANTIA EN CADA CATEGORIA DE APICAT
	   .........................................................................
	SELECT
	*,
	(
		SELECT category
		FROM `letnd`.`export__apicat_tipo_to_category`
		WHERE export__apicat_tipo_especifico.especifico_id = export__apicat_tipo_to_category.especifico_id AND
			  priority > -1
		ORDER BY priority DESC
		LIMIT 1
	) AS letnd,
	(
		SELECT priority
		FROM `letnd`.`export__apicat_tipo_to_category`
		WHERE export__apicat_tipo_especifico.especifico_id = export__apicat_tipo_to_category.especifico_id AND
			  priority > -1
		ORDER BY priority DESC
		LIMIT 1
	) AS priority
	FROM `letnd`.`export__apicat_tipo_especifico`


	AL REVES
	.............

	SELECT
	`category_id`,
	`especifico_id`,
	`category`,
	(
		SELECT especifico
		FROM `letnd`.`export__apicat_tipo_especifico`
		WHERE export__apicat_tipo_especifico.especifico_id = export__apicat_tipo_to_category.especifico_id
		LIMIT 1
	) AS apicat,
	`priority`
	FROM `letnd`.`export__apicat_tipo_to_category`
	ORDER BY category ASC


	 */


	function get_ref( $val ) {

		$property_id = $this->import_rs['property_id'];
		$q           = new Query( "{$this->agencia_rs['dbname']}.inmo__property" );

		$condition      = "ref = %s";
		$condition_vars = [ $val ];

		if ( $property_id ) {
			$condition        .= " AND property_id <> %s";
			$condition_vars[] = $property_id;
		}

		if ( $q->get_first( 'count(*)', $condition, ...$condition_vars ) ) {
			$val = "GH-$val";

			return $this->get_ref( $val );
		}

		return $val;
	}

	function get_category( $val ) {

		$TipoEspecifico = current( $this->property_rs->TipoEspecifico );

		$query = "
		SELECT category_id
		FROM `letnd`.`export__apicat_tipo_to_category`
		WHERE especifico_id IN (
			SELECT especifico_id FROM `letnd`.`export__apicat_tipo_especifico` WHERE especifico = '$TipoEspecifico'
		) 
		AND priority > -1
		ORDER BY priority DESC
		LIMIT 1
		";
		$ret   = Db::get_first( $query );

		return $ret;
	}

	function get_full_address( $val ) {

		// No utilitzem $val per que per exportar és diferent de importar
		$TipoVia   = current( $this->property_rs->TipoVia );
		$NombreVia = current( $this->property_rs->NombreVia );
		$NumeroVia = current( $this->property_rs->NumeroVia );

		$val = trim( "$TipoVia $NombreVia" );
		if ( $NumeroVia ) $this->import_rs['numstreet'] = $NumeroVia;

		return $val ? $val : false;
	}

	function get_price( $val ) {

		$PrecioVenta     = current( $this->property_rs->PrecioVenta );
		$PrecioAlquiler  = current( $this->property_rs->PrecioAlquiler );
		$PrecioTemporada = current( $this->property_rs->PrecioTemporada );
		$PrecioSubasta   = current( $this->property_rs->PrecioSubasta );
		$PrecioTraspaso  = current( $this->property_rs->PrecioTraspaso );

		$val = $PrecioVenta ?: $PrecioAlquiler ?: $PrecioTemporada ?: $PrecioTraspaso ?: $PrecioSubasta;

		$this->import_rs['tipus'] = $PrecioVenta ? 'sell' : $PrecioAlquiler ? 'rent' : $PrecioTemporada ? 'temp' : $PrecioSubasta ? 'sell' : 'rent'; // traspàs ho poso com a rent

		return $val ? $val : false;
	}

	function get_tipus2( $val ) {
		return ( $val == 'Si' ) ? 'new' : 'second';
	}

	function get_provincia( $val ) {

		if ( ! $val ) return false;

		$ret = Db::get_first( "SELECT provincia_id FROM `" . $this->agencia_rs['dbname'] . "`.inmo__provincia WHERE provincia = %s", $val );
		if ( $ret ) return $ret;

		$ret = Db::get_first( "SELECT provincia_id FROM `" . $this->agencia_rs['dbname'] . "`.inmo__provincia WHERE provincia LIKE  %s", "%$val%" );

		return $ret ? $ret : false;
	}

	function get_municipi( $val ) {

		if ( ! $val ) return false;

		if ( $val == 'S Agaró' ) $val = "Agaró, s'";
		if ( $val == 'Romanyà de la Selva' ) $val = "Santa Cristina d'Aro";

		$ret = Db::get_first( "SELECT municipi_id FROM `" . $this->agencia_rs['dbname'] . "`.inmo__municipi WHERE municipi = %s", $val );
		if ( $ret ) return $ret;

		$ret = Db::get_first( "SELECT municipi_id FROM `" . $this->agencia_rs['dbname'] . "`.inmo__municipi WHERE municipi LIKE  %s", "%$val%" );
		if ( $ret ) return $ret;


		$ret = Db::get_first( "
				SELECT municipi_id 
				FROM `{$this->agencia_rs['dbname']}`.inmo__municipi 
				WHERE REPLACE (REPLACE(municipi, '\'', ' '), ' - ', '-') LIKE  %s", "%$val%" );

		return $ret ? $ret : false;
	}

	function get_zone( $val ) {

		if ( ! $val ) return false;

		$ret = Db::get_first( "SELECT zone_id FROM `" . $this->agencia_rs['dbname'] . "`.inmo__zone WHERE zone = %s", $val );
		if ( $ret ) return $ret;

		$ret = Db::get_first( "SELECT zone_id FROM `" . $this->agencia_rs['dbname'] . "`.inmo__zone WHERE zone LIKE %s", "%$val%" );

		return $ret ? $ret : false;
	}

	function get_comarca_id() {

		$import_rs = &$this->import_rs;
		if ( empty( $import_rs['municipi_id'] ) ) return;

		$municipi_id = $import_rs['municipi_id'];

		$comarca_id   = Db::get_first( "SELECT comarca_id FROM inmo__municipi WHERE municipi_id = %s", $municipi_id );
		$provincia_id = Db::get_first( "SELECT provincia_id FROM inmo__comarca WHERE comarca_id = %s", $comarca_id );

		$import_rs['comarca_id']   = $comarca_id;
		$import_rs['provincia_id'] = $provincia_id; // M'asseguro que quadri el municipi amb la provincia
	}

	function get_latitude_longitud( $val ) {

		if ( $val ) $val = number_format( $val, 7, '.', '' );

		if ( $val ) $this->import_rs['zoom'] = '17';
		if ( $this->letnd_key == 'latitude' ) $this->import_rs['center_latitude'] = $val;
		if ( $this->letnd_key == 'longitude' ) $this->import_rs['center_longitude'] = $val;

		return $val ? $val : false;
	}

	function get_facing( $val ) {

		if ( ! $val ) {
			return false;
		}
		if ( $val == '4 Vientos' ) return ''; // TODO-i Posar a letnd

		return $this->apicat_facings[ $val ];
	}

	function get_parking( $val ) {
		return $this->get_caracteristica_boolean( $val );
	}

	// Nomes es seleciona el primer
	function get_floor_type( $val ) {

		if ( ! $val ) return false;

		$floors = $this->apicat_floors[ $val ];

		if ( ! $floors ) return false; // Aquí obtinc undefined i podré controlar si en falta algún

		if ( ! is_array( $floors ) ) $floors = [ $floors ];

		foreach ( $floors as $floor ) {
			$this->import_rs[ $floor ] = 1;
		}


		return false;
	}

	// Nomes es seleciona el primer
	function get_view( $val ) {

		if ( ! $val || ! isset( $this->apicat_views[ $val ] ) ) return false; // Molts no els tinc, per tant no vull que em dongui error

		$this->import_rs[ $this->apicat_views[ $val ] ] = 1;

		return false;
	}


	function get_cedula( $val ) {

		// Només a importar, per exportar es necessita la data
		return format_date_form( $val );

	}


	function get_date( $val ) {

		if ( ! $val ) {
			return false;
		}

		return $val;
	}


	function get_construction( $val ) {

		if ( ! $val ) {
			return false;
		}

		return "$val-01-01";
	}

	// SI es un terreny, la superfície pot ser "land"
	function get_floor_space( $val ) {

		if ( ! $val ) {
			return false;
		}

		// TODO-i Canviar-ho quan ho posi a letnd
		$construida = current ($this->property_rs->SuperficieConstruida);
		if ($construida) $val = $construida;

		// Això va perque ja s'ha fet abans la categoria
		if ( $this->import_rs['category_id'] == '5' ) {
			$this->import_rs['land'] = $val;

			return false;
		}

		return $val;

	}

	// Genériques
	function get_not_required( $val ) {
		return $val ? $val : false;
	}

	function get_boolean_not_required( $val ) {
		return $val == 'Si' || $val == '1' ? '1' : false;
	}

	function get_caracteristica_boolean( $val ) {
		return $val == 'Si' || $val == '1' ? '1' : 0;
	}

	// S'ha de mantindre per que es a la taula de export, així serveix la taula per importar i exportar
	function get_caracteristica( $val ) {

		return $val ? $val : false;
	}

	function download_client_files( $path ) {

		if ( R::get_opt( 'do_not_download_files' ) ) return; // Només per testejar

		ExportCommon::p( "Descarregar arxius: " . $this->agencia_rs['comercialname'], 1 );

		if ( ! is_dir( $path ) ) {
			mkdir( $path, 0777 );
			ExportCommon::p( "Creat directori $path", 3 );
		}

		$conn_id      = ftp_connect( 'ftp.ghestia.cat' );
		$login_result = ftp_login( $conn_id, $this->agencia_rs['inmo_apicat_import_user'], $this->agencia_rs['inmo_apicat_import_pass'] );
		// turn passive mode on
		ftp_pasv( $conn_id, true );

		$contents = ftp_nlist( $conn_id, '/' );
		foreach ( $contents as $file ) {
			if ( ftp_get( $conn_id, "$path/$file", $file, FTP_BINARY ) ) {
				chmod( "$path/$file", 0777 );
				ExportCommon::p( "Arxiu descarregat: $file", 3 );
			}
			else {
				send_mail_admintotal( "Apicat import - no s'ha pogut descarregar: $file" );
				ExportCommon::p( "No s'ha pogut descarregar: $file", "error" );
			}
		}

		/*if ( ftp_get( $conn_id, $local_file, $server_file, FTP_BINARY ) ) {
			$ret = true;


		}
		else {
			$ret = false;
		}*/
		ftp_close( $conn_id );

		// return $ret;

	}

}