<table border="0" cellspacing="0" cellpadding="0" class="letnd-form">
	<tr>
		<td valign="top" class="letnd-form-image">			
					
			<?if ($images):?>
				<a title="<?=htmlspecialchars($image_title)?>" href="<?= HTTP ?><?= SHORT_HOST_URL ?><?=$show_record_link?>">
					<img <?=get_image_attrs($image_src_details, $image_alt?$image_alt:$new, 280, 280, false)?> />
				</a>
			<?endif //image?> 
		</td>
		<td valign="top" class="letnd-form-details">
		
			<?if ($new):?><h2 class="new"><?=$new?></h2><? endif ?>
			
			<em class="news-form-entered"><?=$entered?></em>
			
			<?if($subtitle):?><h3><?=$subtitle?></h3><?endif //subtitle?>

			<p class="letnd-info"><a title="<?=htmlspecialchars($new)?>" href="<?= HTTP ?><?= SHORT_HOST_URL ?><?=$show_record_link?>"><?=$c_more_info?></a></p>
			
			<?if($url1 || $url2 || $url3 || $files):?>
				<table border="0" cellspacing="0" cellpadding="0" class="news-form-details">
					<tr>
						<td>
							<?if($url1):?>
								<a target="_blank" href="<?=$url1?>"><span>»</span> <?=$url1_name?></a>
							<?endif //url?>
							<?if($url2):?>
							<a target="_blank" href="<?=$url2?>"><span>»</span> <?=$url2_name?></a>
							<?endif //url?>
							<?if($url3):?>
								<a target="_blank" href="<?=$url3?>"><span>»</span> <?=$url3_name?></a>
							<?endif //url?>
							<?foreach($files as $l): extract($l)?>
								<a target="_blank" href="<?=$file_src?>"><span>»</span> <?=$file_title?></a>
							<?endforeach //files?>
						</td>
					</tr>
				</table>
			<?endif //show_info?>
			
		</td>
	</tr>
</table>

<?if($content):?>
<table border="0" cellspacing="0" cellpadding="0" class="letnd-form-description">
	<tr>
		<td valign="top" class="letnd-form-description-td">							
			<?=$content?>			
		</td>
	</tr>
</table>
<?endif //content?>