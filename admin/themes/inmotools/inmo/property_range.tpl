<link href="/admin/themes/inmotools/styles/inmo_booking.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="/admin/jscripts/validacio.js?v=<?=$version?>"></script>
<script type="text/javascript" src="/admin/modules/booking/jscripts/functions.js?v=<?=$version?>"></script>
<script type="text/javascript" src="/admin/jscripts/assign.js?v=<?=$version?>"></script>
<script type="text/javascript" src="/admin/modules/booking/jscripts/list_pick.js?v=<?=$version?>"></script>
<script type="text/javascript">
	$(function() {
		booking.init();	
		booking.property_id = <?=$property_id?>;
		booking.is_season_calendar = <?=$is_season_calendar?1:0?>;
	});

	var assign_selected_ids = 0; <?//=$assign_selected_ids?>
	var equal_dates_error = '<?=addslashes($c_equal_dates_error)?>';


</script>
<?=$seasons_styles?>
<? /*
for ($i=1;$i<19;$i++){	
	echo '<div class="season-list season'.$i.'"></div>';
} */
?>
<div class="form">
	
	<div id="form_buttons_holder">		
	<table id ="form_buttons">
		<tr>
			<td valign="bottom"><div class="menu">
			
					<a href="?action=show_form_edit&menu_id=103&property_id=<?=$property_id?>"><?=$c_edit_data?></a>
					
					<a href="?action=list_records_images&menu_id=103&property_id=<?=$property_id?>"><?=$c_edit_images?></a>
						
					<?if($is_custumer_on):?>
					<span class="private"><a href="?menu_id=<?=$menu_id?>&property_id=<?=$property_id?>&action=list_records_custumer"><?=$c_edit_custumer?></a></span>
					<?endif //custumer_on?>

					<?if($is_history_on):?>
					<span class="private"><a href="?menu_id=<?=$menu_id?>&property_id=<?=$property_id?>&action=list_records_history"><?=$c_edit_history?></a></span>
					<?endif //is_history_on?>

					<a class="private" href="?menu_id=<?=$menu_id?>&property_id=<?=$property_id?>&action=show_form_map"><?=$c_edit_map?></a>
					
					<a class="sep" href="?menu_id=<?=$menu_id?>&property_id=<?=$property_id?>&action=show_form_new&process=booking"><?=$c_edit_booking?></a>
					
					<div><?=$c_edit_booking_season?></div>
					
					<a class="private" href="?menu_id=<?=$menu_id?>&property_id=<?=$property_id?>&action=show_form_edit&process=booking_extra"><?=$c_edit_booking_extra?></a>
					
					</div></td>
			<td class="buttons_top"><div class="right">
										<? if($show_previous_link): ?><a class="boto2 btn_show_previous_link"
										                          href="<?= $show_previous_link ?>"></a><? endif //show_previous_link?>
										<? if($show_next_link): ?><a class="boto2 btn_show_next_link"
										                          href="<?= $show_next_link ?>"></a><? endif //show_next_link?>
			<? if ($back_button):?><a class="btn_form_back" href="<?=$back_button?>"></a><? endif //back_button?>
			</div></td>
		</tr>
	</table></div>

	<table width="100%" cellpadding="0" cellspacing="0">
		<tr>
			<td class="formCaptionTitle"><strong>
					<?=$c_form_booking_season_title?></strong>&nbsp;&nbsp;<?=$ref?><? if ($property_private):?> - <?=$property_private?> <? endif //property_private?>
			</td>
		</tr>
	</table>
	<table width="100%" cellpadding="4" cellspacing="0" class="formstbl">

		<tr>
			<td align="left" valign="top" class="forms"><table width="99%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td>

							<div id="book-calendar"><table id="book-calendar-table" border="0" cellpadding="0" cellspacing="0">
									<tr id="book-calendar-tr">
										<?=$calendar?>
									</tr>
								</table>
							</div>
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td><a id="calendar-prev" href="javascript:booking.show_month('prev','<?=$property_id?>');"></a></td>
									<td align="right"><a id="calendar-next" href="javascript:booking.show_month('next','<?=$property_id?>');"></a></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>

			</td>
		</tr>
	</table>
	<div class="sep_dark" style="margin-top:30px;"></div>

	
	<table width="100%" cellpadding="4" cellspacing="0" class="formstbl">

		<tr>
			<td align="left" valign="top" class="forms" id="list-records">				
				<?=$season_list?>
			</td>
		</tr>
	</table>
	
</div>