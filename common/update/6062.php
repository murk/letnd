<?

/* 1-12-15

	Actualització inmo booking, marcar reserves segons a que refereix el pagament

 */
if ( is_table( 'booking__extra' ) ) {
	$update_sql[] = "
		ALTER TABLE `booking__extra`
	ADD COLUMN `concept` ENUM('normal','deposit','cleaning') NULL DEFAULT 'normal' AFTER `has_quantity`;";

}

?>