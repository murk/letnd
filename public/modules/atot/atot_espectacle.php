<?
/**
 * AtotEspectacle
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 5/11/13
 * @version $Id$
 * @access public
 */
class AtotEspectacle extends Module{

	function __construct(){
		parent::__construct();
	}
	function on_load(){
		
		$this->set_friendly_params(array(
			'edats'=>R::text_id('edats')	
			));
		parent::on_load();
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function get_records()
	{
	    $edats = R::text_id('edats');
		$edats_arr = explode('-',$edats);
		if (count($edats_arr)==2){
			$edat1 = $edats_arr[0];
			$edat2 = $edats_arr[1];
		}
		else{
			$edat1 = false;
			$edat2 = false;
		}
	
		$poblacio = $this->caption['c_tab_1'];
		
		
		
		$listing = new ListRecords($this);
		
		if ($GLOBALS['gl_is_home']) {
			$listing->condition .= "status = 'public' AND in_home = 1";
		}
		else{
		$listing->condition .= "
			status = 'public'";
		}
		
		$listing->condition .= " AND only_campanya = 0";
		
		if ($edat1 && $edat2){
			
			$listing->condition .= " AND (SELECT count(*)
									FROM atot__curs 
									JOIN atot__espectacle_to_curs 
									USING (curs_id)
									WHERE  atot__espectacle_to_curs.espectacle_id = atot__espectacle.espectacle_id
									AND edat1 = " . $edat1 . " 
									AND edat2 = " . $edat2 . ") > 0";
			
			$poblacio .= ' - De ' . $edat1 . ' a ' . $edat2 . ' ' . $this->caption['c_anys'];		
		}
		
		Page::set_title($poblacio);
		
		$listing->set_var('is_home', $GLOBALS['gl_is_home']);
		$listing->set_var('is_poblacio', false);
		
		$listing->order_by = 'destacat DESC, espectacle ASC, entered DESC';
		
		if ($this->parent == 'home'){			
			$listing->limit = HOME_MAX_RESULTS;
		}
		
		
		
	    return $listing->list_records();
	}
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{
		
		// titol 
	    $edats = R::text_id('edats');
		$edats_arr = explode('-',$edats);
		if (count($edats_arr)==2){
			$edat1 = $edats_arr[0];
			$edat2 = $edats_arr[1];
		}
		else{
			$edat1 = false;
			$edat2 = false;
		}
		
		$municipi_id = R::id('municipi_id');
		
		if ($municipi_id){			
			$poblacio = Db::get_first("SELECT municipi FROM atot__municipi WHERE municipi_id = " . $municipi_id);
			$poblacio = $this->caption['c_tab_2'] . ' - ' . $poblacio;
		}
		else{
			$poblacio = $this->caption['c_tab_1'];	
			
			if ($edat1 && $edat2){			

				$poblacio .= ' - De ' . $edat1 . ' a ' . $edat2 . ' ' . $this->caption['c_anys'];		
			}
		}
		
		Page::set_title($poblacio);
		// fi titol
		
		
	    $show = new ShowForm($this);
		
		$show->set_var('is_poblacio', $municipi_id!=false);
		
		
		$show->condition = "status = 'public'";	
		
		$show->get_values();
		if ($show->has_results)
		{
			Page::set_page_title($show->rs['page_title'],$show->rs['espectacle']);
			Page::set_page_description($show->rs['page_description'],$show->rs['content']);
			Page::set_page_keywords($show->rs['page_keywords']);
			
			return $show->show_form();
		}
		else
		{
			Main::redirect('/');
		}
		
	}
}
?>