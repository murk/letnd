<?php
global $gl_action, $gl_translation;
if (strstr($gl_action, "_property"))
{
    set_tool('inmo', 'property', 'inmo__property', 'property_id');
} elseif (strstr($gl_action, "_category"))
{
    set_tool('inmo', 'category', 'inmo__category', 'category_id');
}
$gl_translation = '';

function save_rows()
{
    $gl_translation = new Translation();
    $gl_translation->save_rows();
}

function list_records()
{
    global $gl_translation, $gl_action;
	$conditions = array();
    if (isset($_GET['status']) && $_GET['status'] && $_GET['status']!='null') $conditions[]="status='" . $_GET['status'] . "'";
    if (isset($_GET['category_id']) && $_GET['category_id'] && $_GET['category_id']!='null') $conditions[]="category_id='" . $_GET['category_id'] . "'";
    if (isset($_GET['tipus']) && $_GET['tipus'] && $_GET['tipus']!='null') $conditions[]="tipus='" . $_GET['tipus'] . "'";
	$condition = implode(' AND ',$conditions);

    $listing = new ListRecords();
    $gl_translation = new Translation();
    $gl_translation->condition = $condition;
    if (strstr($gl_action, "_category"))
    {
        $listing->has_bin = false;
    }
    $condition = $condition?' AND (' . $condition . ')':'';
    $listing->condition = $gl_translation->get_condition($listing) . $condition;

    if (strstr($gl_action, "_property"))
    {
        $listing->order_by = 'ref desc, entered desc';
		$listing->add_filter('category_id');
		$listing->add_filter('tipus');
		$listing->add_filter('status');
    }
    $listing->use_default_template = true;
    $listing->list_records();

    $gl_translation->show_table();
}
function show_form()
{
}

function write_record()
{
}

function manage_images()
{
}

?>