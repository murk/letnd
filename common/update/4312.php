<?php
/* 13-9-12 */
// Actualitzacio dels alt a failies productes


if (is_table("product__family_language")){
	$update_sql[]="ALTER TABLE `product__family_language`
	ADD COLUMN `family_alt` VARCHAR(150) NOT NULL DEFAULT '' AFTER `family`;";
	
	$update_sql[]="UPDATE `product__family_language` SET `family_alt`=family;";
}			
?>