<?
/**
 * ProductOrder
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 2010
 * @version $Id$
 * @access public
 */
class ProductPaymentPaypal extends ModuleBase {
    var $vars = array();
    var $config;
	var $url;
	var $order_id;

	var $order_status = '';
	var $transaction_id;
    function __construct(&$module, $order_vars='')
    {
        $this->config = &$module->config;
		$this->url = $this->config['paypal_sandbox']?'www.sandbox.paypal.com':'www.paypal.com';
    	$paypal_mail = $this->config['paypal_sandbox']?'sanahuja@gmail.com':$this->config['paypal_mail'];

        if ($order_vars) {
			$products = $order_vars['results'];
			$order = $order_vars['totals'];
			$this->vars += $this->get_customer($order['customer_id']);

			$count = 1;
			// formo els productes de la cistella de paypal
			foreach ($products as $product) {
				$this->vars['item_name_' . $count] = $product['product_title'];
				$this->vars['item_number_' . $count] = $product['product_id'];
				$this->vars['quantity_' . $count] = $product['quantity'];
				$this->vars['amount_' . $count] = $this->get_number($product['product_basetax']);
				$count++;
			}
			
			// Ho he PASSAT a 'handling_cart'
			//$this->vars['item_name_' . $count] = $module->caption['c_rate_basetax'];
			//$this->vars['item_number_' . $count] = '0';
			//$this->vars['quantity_' . $count] = '1';
			//$this->vars['amount_' . $count] = $this->get_number($order['rate_basetax']);
			
			// SI DESCOMPTE MAJOR A SUMA PRODUCTES, paypal no ho conta, per tant no pot sortir mai un import inferior a almenys les despeses d'enviament ( ja va bé, ja que normalment es posarà un import mínim , sino es perdria calers si no cobris ni les despeses d'enviament )
			
			if (USE_FRIENDLY_URL){
				$return_url = Page::get_link('product', 'payment', 'show_end', false ,array('process'=>'end','order_id'=>$order_vars['order_id']), false);
			}
			else{
				$return_url = HOST_URL . '?tool=product&tool_section=payment&language=' . LANGUAGE . '&action=show_end&process=end&order_id=' . $order_vars['order_id'];
			}
			
			// variables del paypal
			$this->vars += array(
				'upload' => '1',
				'address_override' => '1',
				'custom' => $order_vars['order_id'],
				//'amount' => $this->get_number($order['total_basetax']), // això no feia res, crec que es per si es fa un item individual
				'discount_amount_cart' => $this->get_number($order['promcode_discount']),
				'handling_cart' => $this->get_number($order['rate_basetax']),
				'business' => $paypal_mail, // id de paypal o adreça de l'account
				'cmd' => '_cart',
				'charset' => 'utf-8',
				'currency_code' => 'EUR',
				'return' => $return_url, // s'ha fet correctament
				'cancel_return' => HOST_URL . $_SERVER["REQUEST_URI"], // s'ha cancelat la compra
				'notify_url' => HOST_URL . '?tool=product&tool_section=payment&language=' . LANGUAGE . '&action=set_notice&process=paypal',
				'bn' => 'LETND_WPS',
				'rm' => '2', // retorna un post
				// 'cbt' => 'Tornar a Tiendascopporo.com', // caption del boto de tornar a la botiga
				'lc' => LANGUAGE_CODE
			);
			//Debug::p($this->vars);
        }

    }
    // obté variables de adreça i dades usuari
    function get_customer($customer_id)
    {
        $sql = "SELECT product__customer.customer_id AS payer_id, name AS first_name, surname AS last_name, mail AS email, address AS address1, city, zip, country_id as country FROM product__customer LEFT JOIN product__address USING(customer_id) WHERE invoice=1 AND customer_id = " . $customer_id;
        $ret = Db::get_row($sql);
        //Debug::p($ret);
        return $ret;
    }
    // formateja numero en anglès
    function get_number($number)
    {
        $number = str_replace('.', '', $number);
        $number = str_replace(',', '.', $number);
        // (int)$number;
        return $number;
    }
    // obté formulari html
    function get_content()
    {
        if (PRODUCT_PAYPAL_SANDBOX) {
            $this->vars['business'] = 'info-facilitator@letnd.com';
            $this->vars['email'] = 'marc@letnd.com';
        }
        $ret = '<form action="https://'.$this->url.'/cgi-bin/webscr" method="post" id="paypal_form">';
        foreach ($this->vars as $key => $val) {
            $ret .= '<input type="hidden" name="' . $key . '" value="' . $val . '" />
			';
        }
        $ret .= '<input class="paypal" type="submit">';
        $ret .= '</form>';
        if (!PRODUCT_PAYPAL_SANDBOX) $ret .= get_javascript('$("#paypal_form").submit();');
        return $ret;

        /*$ret = '<form action="https://www.paypal.com/cgi-bin/webscr" method="post" id="paypal_form" class="hidden">
				<input type="hidden" name="upload" value="1" />
				<input type="hidden" name="address_override" value="1" />
				<input type="hidden" name="payer_id" value="3" />
				<input type="hidden" name="first_name" value="Joan" />
				<input type="hidden" name="last_name" value="CALVERA" />
				<input type="hidden" name="payer_email" value="sanahuja@gmail.com" />
				<input type="hidden" name="address1" value="Ganganell, 25" />
				<input type="hidden" name="address2" value="Besalú" />
				<input type="hidden" name="city" value="Besalú" />
				<input type="hidden" name="zip" value="17850" />
				<input type="hidden" name="country" value="ES" />
				<input type="hidden" name="amount" value="189.05" />
				<input type="hidden" name="email" value="sanahuja@gmail.com" />
				<input type="hidden" name="item_name_1" value="My cart" />
				<input type="hidden" name="amount_1" value="197.42" />
				<input type="hidden" name="quantity_1" value="1" />
				<input type="hidden" name="business" value="paypal@prestashop.com" />
				<input type="hidden" name="receiver_email" value="paypal@prestashop.com" />
				<input type="hidden" name="cmd" value="_cart" />
				<input type="hidden" name="charset" value="utf-8" />
				<input type="hidden" name="currency_code" value="EUR" />
				<input type="hidden" name="custom" value="15" />
				<input type="hidden" name="return" value="http://www.prestashop.com/demo/order-confirmation.php?key=fab5ee8acfaf369b85643672f3162224&id_cart=15&id_module=4" />
				<input type="hidden" name="cancel_return" value="http://www.prestashop.com/demo/index.php" />
				<input type="hidden" name="notify_url" value="http://www.prestashop.com/demo/modules/paypal/validation.php" />

				<input type="hidden" name="rm" value="2" />
				<input type="hidden" name="bn" value="LETND_WPS" />
				<input type="hidden" name="cbt" value="Return to shop" />
				</form>';*/
    }
    function set_notice()
    {
		//Debug::add('Paypal set_notice','Si');
		$this->trace("Variables POST:\n---------------\n" . print_r($_POST,true) . "\n\n", '', 'set_notice');
		
        $header = "";
        $emailtext = "";
        // Read the post from PayPal and add 'cmd'
        $req = 'cmd=_notify-validate';

        foreach ($_POST as $key => $value)
        {
            $req .= "&$key=$value";
        }
        // Post back to PayPal to validate
        $header .= "POST /cgi-bin/webscr HTTP/1.0\r\n";
        $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
        $header .= "Content-Length: " . strlen($req) . "\r\n\r\n";
		
        $fp = fsockopen ('ssl://' . $this->url, 443, $errno, $errstr, 30);
		
        // Process validation from PayPal
        if (!$fp) { // HTTP ERROR
			foreach ($_POST as $key => $value) {
				$emailtext .= $key . " = " . $value . "\n\n";
				$emailtext .= "\n\nReferer = " . $_SERVER['HTTP_REFERER'];
			}
			
			$this->trace($emailtext, 'Error');
			
			send_mail_admintotal('Intent de pagament invàlid per HTTP ERROR amb paypal a ' . HOST_URL,$emailtext);
        	return false;
        } else {
        	// ids
			
			$this->transaction_id = $_POST['txn_id'];
			$this->order_id = $_POST['custom'];
			
			$this->trace($_POST['payment_status'], 'Payment Status');
			
			
			Debug::add('Paypal transaction_id',$this->transaction_id);
			$this->trace('Hi ha fp', 'FP');
			$this->trace($header . $req, 'Header');
            // NO HTTP ERROR
            fputs ($fp, $header . $req);
            while (!feof($fp)) {
                $res = fgets ($fp, 1024);
                if (strcmp ($res, "VERIFIED") == 0) {
                    // TODO:
                    // Check the payment_status is Completed
                    // Check that txn_id has not been previously processed
                    // Check that receiver_email is your Primary PayPal email
                    // Check that payment_amount/payment_currency are correct
                    // Process payment
                    // If 'VERIFIED', send an email of IPN variables and values to the
                    // specified email address

					/*
					Canceled_Reversal: A reversal has been canceled. For example, you
					won a dispute with the customer, and the funds for the transaction that was
					reversed have been returned to you.
					Completed: The payment has been completed, and the funds have been
					added successfully to your account balance.
					Created: A German ELV payment is made using Express Checkout.
					Denied: You denied the payment. This happens only if the payment was
					previously pending because of possible reasons described for the
					pending_reason variable or the Fraud_Management_Filters_x
					variable.
					Expired: This authorization has expired and cannot be captured.
					Failed: The payment has failed. This happens only if the payment was
					made from your customers bank account.
					Pending: The payment is pending. See pending_reason for more
					information.
					Refunded: You refunded the payment.
					Reversed: A payment was reversed due to a chargeback or other type of
					reversal. The funds have been removed from your account balance and
					returned to the buyer. The reason for the reversal is specified in the
					ReasonCode element.
					Processed: A payment has been accepted.
					Voided: This authorization has been voided.

					* No se que s'ha de fer
					- Ja estan fets

					*Canceled_Reversal
					-Completed
					*Created
					-Denied
					-Expired
					-Failed
					-Pending
					Refunded
					*Reversed
					-Processed
					-Voided
					*/

			
					$this->trace($_POST['payment_status'], 'Payment Status');
					
					Debug::add('Paypal Payment status',$_POST['payment_status']);
					switch($_POST['payment_status']){
                		case 'Failed':
                			$this->order_status = 'failed';
                			break;
						case 'Pending':
							$this->order_status = 'pending';
							break;
						case 'Voided':
							$this->order_status = 'voided';
							break;
						case 'Refunded':
							$this->order_status = 'voided';
							break;
						case 'Denied':
						case 'Expired':
							$this->order_status = 'denied';
                			break;
						case 'Completed':
						case 'Processed':
							$this->order_status = 'completed';
                			break;
                	} // switch

					Debug::add('Paypal Order status',$this->order_status);
                } else if (strcmp ($res, "INVALID") == 0) {
					
                    // If 'INVALID', send an email. TODO: Log for manual investigation.
                    foreach ($_POST as $key => $value) {
                        $emailtext .= $key . " = " . $value . "\n\n";
                    }
                	$emailtext .= "\n\nReferer = " . $_SERVER['HTTP_REFERER'];					
					
					$this->trace($emailtext, 'Error');
					
                	send_mail_admintotal('Intent de pagament INVALID a ' . HOST_URL,$emailtext);
					// envio mail notificant l'intent invalid i no faig res mès
					fclose ($fp);
                	return false;
                }
            }
        }

		$this->trace('', 'End');
        fclose ($fp);
    	return true;
    }
	
	function trace($text, $name = '', $title = false){
		ProductPayment::trace($text, $name, $title, 'paypal');
	}
}
/*
// Set the request paramaeter
$req = 'cmd=_notify-validate';

// Run through the posted array
foreach ($_POST as $key => $value)
{
    $value = urlencode($value);
    // Add the value to the request parameter
    $req .= "&$key=$value";
}

$url = "http://www.paypal.com/cgi-bin/webscr";
$ch = curl_init();    // Starts the curl handler
curl_setopt($ch, CURLOPT_URL,$url); // Sets the paypal address for curl
curl_setopt($ch, CURLOPT_FAILONERROR, 1);
curl_setopt($ch, CURLOPT_RETURNTRANSFER,1); // Returns result to a variable instead of echoing
curl_setopt($ch, CURLOPT_TIMEOUT, 3); // Sets a time limit for curl in seconds (do not set too low)
curl_setopt($ch, CURLOPT_POST, 1); // Set curl to send data using post
curl_setopt($ch, CURLOPT_POSTFIELDS, $req); // Add the request parameters to the post
$result = curl_exec($ch); // run the curl process (and return the result to $result
curl_close($ch);

if (strcmp ($result, "VERIFIED") == 0) // It may seem strange but this function returns 0 if the result matches the string So you MUST check it is 0 and not just do strcmp ($result, "VERIFIED") (the if will fail as it will equate the result as false)
{
    // Do some checks to ensure that the payment has been sent to the correct person
    // Check and ensure currency and amount are correct
    // Check that the transaction has not been processed before
    // Ensure the payment is complete
}
else 
{
    // Log an invalid request to look into
} 

http://www.innovativephp.com/validating-paypal-pdt-with-curl-and-pdt-token/
*/
    ?>