<?

/* 5-2-15

  Actualitzacio urls en idiomes

*/

if (is_table('all__language') && !is_field('all__language','url')) {

	$update_sql .= "
			ALTER TABLE all__language
				ADD COLUMN url VARCHAR(100) NOT NULL DEFAULT '' AFTER public;
		";
}
else {
	if (is_table('all__language')) {
		trigger_error('Ja existeixen el camp url');
	}
}

?>