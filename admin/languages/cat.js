var NONE_SELECTED = "No hi ha cap fila seleccionada";

var VALIDAR_TEXT1 = ' és obligatori';
var VALIDAR_TEXT2 = ' conté caràcters no vàlids';
var VALIDAR_TEXT3 = ' no és una adreça d\'E-Mail correcta';
var VALIDAR_TEXT4 = ' ha de tindre un mínim de 3 caràcters i @';
var VALIDAR_TEXT5 = ' ha de ser un número enter';
var VALIDAR_TEXT6 = ' ha de ser un número de 5 dígits';
var VALIDAR_TEXT7 = ' conté ';
var VALIDAR_TEXT8 = ' caràcters, no pot ser més llarg de ';
var VALIDAR_TEXT9 = ' Falta posar l\'any o no és numèric';
var VALIDAR_TEXT10 = ' L\'any no és correcte';
var VALIDAR_TEXT11 = ' La data no és correcta';
var VALIDAR_TEXT12 = ' no és correcte, escriu 8 Dígits i la Lletra';
var VALIDAR_TEXT13 = ' no és correcte';
var VALIDAR_TEXT14 = ' la lletra no és correcta';
var VALIDAR_TEXT15 = ' no és correcte';
var VALIDAR_TEXT16 = 'Has de seleccionar ';
var VALIDAR_TEXT17 = 'No has omplert el formulari correctament:';
var VALIDAR_TEXT18 = ' has d\'escollir almenys una opció';
var VALIDAR_TEXT19 = ' Has d\'escollir almenys una adreça o un grup';
var VALIDAR_TEXT20 = ' El núnero de la tarjeta de crédit és incorrecte';
var VALIDAR_TEXT21 = ' La data de caducitat de la tarjeta de crédit és incorrecta';
var VALIDAR_TEXT22 = ' no es correcte';
var VALIDAR_TEXT23 = ' no es correcte';
var VALIDAR_TEXT24 = ' ha de ser un número';
var VALIDAR_TEXT25 = ' no coincideixen els passwords';
var VALIDAR_TEXT26 = ' no coincideix la confirmació';
var VALIDAR_TEXT27 = ' només pot tindre, números, caracters en minúscules de la "a" a la "z" o guions ("_" o "-")';
var VALIDAR_TEXT28 = 'Ha d\'acceptar la política de privacitat';
var MODALBOX_SAVE = ' Enviant dades ...';
var MODALBOX_CLOSE = 'tancar';
var MULTIFILE_ACCEPT = ' Nomès es poden pujar arxius del tipus: ';
var MULTIFILE_NOT_ACCEPT = ' No es poden pujar arxius del tipus: ';
var CONFIRM_TEXT = 'Segur que desitges realitzar aquesta acció?';
var WAIT_TEXT = 'Un moment si us plau ...';

var MONTH_NAMES = new Array("Gener", "Febrer", "Març", "Abril", "Maig", "Juny", "Juliol", "Agost", "Setembre", "Octubre", "Novembre", "Dessembre");

var FILE_DELETE = "Esborrar";
// NO TRADUIT
var IMMO_MAP_POINT_SELECTED = "Fes clic en un punt del mapa per establir la situació";
var PRODUCT_CART_ADDED = "S'ha afegit el producte a la seva cistella";

var DROP_DEFAULT_MESSAGE = "Clica o arrastra imatges aquí";
var DROP_FILE_DEFAULT_MESSAGE = "Clica o arrastra arxius aquí";
var DROP_INVALID_FILE_TYPE = "Aquest tipus d'arxiu no és vàlid";
var DROP_CANCEL_UPLOAD_CONFIRMATION = "Segur que desitges cancelar aquesta imatge?";

var MESSAGE_CONFIRM_DELETE_NOT_SENT = 'Segur que desitges esborrar aquesta adreça de la llista de destinataris?';

var NO_RESULTS = 'Cap resultat';
var NO_OPTION_SELECTED = 'No hi ha cap opció seleccionada';