<?php
global $gl_caption;
$tipus = isset($vars['tipus'])?$vars['tipus']:false;
$page_id = isset($vars['page_id']) ? $vars['page_id'] : false;
$place_id = isset($vars['place_id']) ? $vars['place_id'] : false;
include_once('inmo_block_functions.php');

$condition = '';
$condition .= inmo_block_get_tipus($tipus);

$place_query = $place_id ? " AND place_id = $place_id " : '';

$vars['var_name'] = 'category_id';

if ($tipus){
	// tipus únic

	$query = "SELECT Distinct category, inmo__category_language.category_id
					FROM inmo__category_language, inmo__property, inmo__category
					WHERE inmo__category_language.category_id = inmo__property.category_id
					AND inmo__category_language.category_id = inmo__category.category_id
					AND language = '" . LANGUAGE . "'
					AND inmo__property.bin = 0
					$place_query 
					AND (status = 'onsale' OR status = 'sold' OR status = 'reserved')
					" . $condition . "
					ORDER by ordre, category";

	$results = Db::get_rows($query);
	
	Debug::add('Consulta categoris menu', $query);

	$category_id = R::number('category_id');
	$tipus_get = R::text_id('tipus');
	$vars['block_selected'] =  $category_id?1:0;
	
	if ($results)
	{
		foreach($results as $rs)
		{
			$selected = $category_id == $rs["category_id"] ? 1 : 0;

			// pel filtre
			if ( isset( $_GET['category_ids'] ) &&
			     in_array( $rs["category_id"], $_GET['category_ids'] )
			) {
				$selected               = 1;
				$vars['block_selected'] = 1;
			}

			$l['link'] = 
				get_link( array(
						'tool' => 'inmo', 
						'tool_section' => 'property', 
						'category' => 'category_id',
						'category_id' => $rs['category_id'],
						'friendly_params' => array('tipus'=>$tipus==INMO_DEFAULT_TIPUS?'':$tipus),
						'link_vars' => array('tipus'=>$tipus),
						'detect_page' => $page_id?false:true,
						'page_id' => $page_id
				));

			$l['selected'] = $selected;
			$l['value'] = $rs['category_id'];
			$l['item'] = $rs['category'];
			$l['class'] = '';
			$loop2[] = $l;
		}

		$loop2[0]['class'] = 'first';
		$loop2[count($loop2) - 1]['class'] = 'last';

		$loop[] = array ('loop'=>$loop2,
		'name' => $tipus,
		'class' => '',
		'item' => isset($gl_caption['c_' . $tipus])?$gl_caption['c_' . $tipus]:'');
	}
	$loop2 = array();
	return;
}
/////////////////////////////////////////////////////////////////////////////////

// venta
$query = "SELECT Distinct category, inmo__category_language.category_id
				FROM inmo__category_language, inmo__property, inmo__category
				WHERE inmo__category_language.category_id = inmo__property.category_id
				AND inmo__category_language.category_id = inmo__category.category_id
				AND language = '" . LANGUAGE . "'
				AND inmo__property.bin = 0
				$place_query 
				AND (status = 'onsale' OR status = 'sold' OR status = 'reserved')
				AND tipus='sell'
 				ORDER by ordre, category";

$results = Db::get_rows($query);
Debug::add('Consulta categoris menu', $query);

$category_id = R::number('category_id');
$tipus = R::text_id('tipus');

if ($results)
{
	foreach($results as $rs)
    {
        (($category_id == $rs["category_id"]) && ($tipus == 'sell'))?$selected = 1:$selected = 0;
		
		$l['link'] = 
				get_link( array(
						'tool' => 'inmo', 
						'tool_section' => 'property', 
						'category' => 'category_id',
						'category_id' => $rs['category_id'],
						'friendly_params' => array('tipus'=>INMO_DEFAULT_TIPUS=='sell'?'':'sell'),
						'link_vars' => array('tipus'=>'sell'),
						'detect_page' => $page_id?false:true,
						'page_id' => $page_id
				));

        $l['selected'] = $selected;
        $l['value'] = $rs['category_id'];
        $l['item'] = $rs['category'];
		$l['class'] = '';
        $loop2[] = $l;
    }

    if ($loop2){
		$loop2[0]['class'] = 'first';
		$loop2[count($loop2) - 1]['class'] = 'last';
	}
	
    $loop[] = array ('loop'=>$loop2,
	'name' => 'sell',
	'block_selected' => $category_id && $tipus == 'sell' ? 1 : 0,
	'class' => '',
	'item' => $gl_caption['c_sell']);
}
$loop2 = array();

// lloguer
$query = "SELECT Distinct category, inmo__category_language.category_id
				FROM inmo__category_language, inmo__property, inmo__category
				WHERE inmo__category_language.category_id = inmo__property.category_id
				AND inmo__category_language.category_id = inmo__category.category_id
				AND language = '" . LANGUAGE . "'
				AND inmo__property.bin = 0
				$place_query 
				AND (status = 'onsale' OR status = 'sold' OR status = 'reserved')
				AND tipus='rent'
 				ORDER by ordre";

$results = Db::get_rows($query);
Debug::add('Consulta categoris menu', $query);

if ($results)
{
    foreach($results as $rs)
    {
        $selected = ($category_id == $rs["category_id"]) && ($tipus == 'rent')?1:0;
		
		
		$l['link'] = 
				get_link( array(
						'tool' => 'inmo', 
						'tool_section' => 'property', 
						'category' => 'category_id',
						'category_id' => $rs['category_id'],
						'friendly_params' => array('tipus'=>INMO_DEFAULT_TIPUS=='rent'?'':'rent'),
						'link_vars' => array('tipus'=>'rent'),
						'detect_page' => $page_id?false:true,
						'page_id' => $page_id
				));
		
		
        $l['selected'] = $selected;
        $l['value'] = $rs['category_id'];
        $l['item'] = $rs['category'];
		$l['class'] = '';
        $loop2[] = $l;
    }

    if ($loop2){
		$loop2[0]['class'] = 'first';
		$loop2[count($loop2) - 1]['class'] = 'last';
	}
	
    $loop[] = array ('loop'=>$loop2,
	'name' => 'rent',
	'block_selected' => $category_id && $tipus == 'rent' ? 1 : 0,
	'class' => '',
	'item' => $gl_caption['c_rent']);
}
$loop2 = array();

// moblat
$query = "SELECT Distinct category, inmo__category_language.category_id
				FROM inmo__category_language, inmo__property, inmo__category
				WHERE inmo__category_language.category_id = inmo__property.category_id
				AND inmo__category_language.category_id = inmo__category.category_id
				AND language = '" . LANGUAGE . "'
				AND inmo__property.bin = 0
				$place_query 
				AND (status = 'onsale' OR status = 'sold' OR status = 'reserved')
				AND tipus='moblat'
 				ORDER by ordre";

$results = Db::get_rows($query);
Debug::add('Consulta categoris menu', $query);

if ($results)
{
    foreach($results as $rs)
    {
        (($category_id == $rs["category_id"]) && ($tipus == 'moblat'))?$selected = 1:$selected = 0;
		
		$l['link'] = 
				get_link( array(
						'tool' => 'inmo', 
						'tool_section' => 'property', 
						'category' => 'category_id',
						'category_id' => $rs['category_id'],
						'friendly_params' => array('tipus'=>INMO_DEFAULT_TIPUS=='moblat'?'':'moblat'),
						'link_vars' => array('tipus'=>'moblat'),
						'detect_page' => $page_id?false:true,
						'page_id' => $page_id
				));
		
        $l['selected'] = $selected;
        $l['value'] = $rs['category_id'];
        $l['item'] = $rs['category'];
		$l['class'] = '';
        $loop2[] = $l;
    }

    if ($loop2){
		$loop2[0]['class'] = 'first';
		$loop2[count($loop2) - 1]['class'] = 'last';
	}
	
    $loop[] = array ('loop'=>$loop2,
	'name' => 'moblat',
	'block_selected' => $category_id && $tipus == 'moblat' ? 1 : 0,
	'class' => '',
	'item' => $gl_caption['c_tipus_moblat']);
}
if ($loop) {
	$loop[0]['class'] = 'first';
	$loop[count($loop) - 1]['class'] = 'last';
}