<?

/* 1-10-14 
  Tarifes d'enviament

 */

/* Taula de provincies per totes les webs */

if (!is_table('all__provincia')) {
	$update_sql []="CREATE TABLE IF NOT EXISTS `all__provincia` (
		`provincia_id` int(11) NOT NULL default '0',
		`admin2_id` varchar(20) default NULL,
		`comunitat_id` varchar(20) default NULL,
		`country_id` char(10) default NULL,
		`name` varchar(200) default NULL,
		`alternatenames` varchar(4000) default NULL,
		PRIMARY KEY  (`provincia_id`),
		KEY `admin2_id` (`admin2_id`)
	  ) ENGINE=MyISAM DEFAULT CHARSET=utf8;";


	$update_sql []="INSERT INTO `all__provincia` (`provincia_id`, `admin2_id`, `comunitat_id`, `country_id`, `name`, `alternatenames`) VALUES
	(2509951, 'V', '60', 'ES', 'Valencia', 'Balenzia,Province de Valence,Provincia de Valencia,Província de València,Valence,Valencia,Valentzia,Valenza,València,Valência,バレンシア,ヴァレンシア'),
	(2510407, 'TO', '54', 'ES', 'Toledo', 'Provincia de Toledo,Tolede,Toledo,Tolède,トレド'),
	(2510910, 'SE', '51', 'ES', 'Sevilla', 'Provincia de Sevilla,Sebilla,Sevilha,Sevilla,Seville,Siviglia,Séville,セビリアセビリヤセヴィーリャセヴィリア,セビーリャ（セビージャ）'),
	(2511173, 'TF', '53', 'ES', 'Santa Cruz de Tenerife', 'Province de Santa Cruz de Tenerife,Province de Santa Cruz de Ténérife,Province of Santa Cruz de Tenerife,Provincia de Santa Cruz de Tenerife,Provinz Santa Cruz de Tenerife,Província de Santa Cruz de Tenerife,Santa Cruz de Tenerife,Tenerife,Tenerifeko Santa Cruz,サンタ・クルス・デ・テネリフェ'),
	(2514254, 'MA', '51', 'ES', 'Málaga', 'Malaga,Màlaga,Málaga,Provincia de Malaga,Provincia de Málaga,マラガ'),
	(2515271, 'GC', '53', 'ES', 'Las Palmas', 'As Palmas,Las Palmas,Les Palmes,Province de Las Palmas,Province of Las Palmas,Provincia de Las Palmas,Provinz Las Palmas,Província de Las Palmas,ラスパルマス,ラス・パルマス'),
	(2516394, 'J', '51', 'ES', 'Jaén', 'Chaen,Chaén,Jaen,Jaén,Provincia de Jaen,Provincia de Jaén,Xaen,Xaén,ハエン'),
	(2516547, 'H', '51', 'ES', 'Huelva', 'Huelva,Provincia de Huelva,Uelba,ウエルバ'),
	(2517115, 'GR', '51', 'ES', 'Granada', 'Granada,Granata,Grenade,Provincia de Granada,グラナダ'),
	(2519034, 'CU', '54', 'ES', 'Cuenca', 'Conca,Cuenca,Provincia de Cuenca,クエンカ'),
	(2519239, 'CO', '51', 'ES', 'Córdoba', 'Cordoba,Cordoue,Cordova,Còrdova,Córdoba,Kordoba,コルドバ'),
	(2519401, 'CR', '54', 'ES', 'Ciudad Real', 'Cidade Real,Ciuda Real,Ciudad Real,Ciudá Real,Ziudat Reyal,シウダッド・レアル'),
	(2520597, 'CA', '51', 'ES', 'Cádiz', 'Cadice,Cadix,Cadiz,Cádiz,Provincia de Cadis,Provincia de Cadiz,Provincia de Cádiz,Província de Cadis,カディス'),
	(2520610, 'CC', '57', 'ES', 'Cáceres', 'Caceres,Cazeres,Càceres,Cáceres,Provincia de Caceres,Provincia de Cáceres,カセレス'),
	(2521419, 'BA', '57', 'ES', 'Badajoz', 'Badajoz,Provincia de Badajoz,バダホス'),
	(2521883, 'AL', '51', 'ES', 'Almería', 'Almeria,Almería,Alméria,Ourense,Provincia de Almeria,Provincia de Almería,アルメリア'),
	(2521976, 'A', '60', 'ES', 'Alicante', 'Alacant,Alacante,Alicant,Alicante,Provincia d\'Alacant,Provincia de Alicante,Província d\'Alacant,アリカンテ'),
	(2522257, 'AB', '54', 'ES', 'Albacete', 'Albacete,Albazet,Provincia de Albacete,アルバセテ'),
	(3104323, 'Z', '52', 'ES', 'Zaragoza', 'Province de Saragosse,Provincia de Saragossa,Provincia de Zaragoza,Província de Saragossa,Prtovince of Saragossa,Saragoca,Saragossa,Saragosse,Saragozza,Saragoça,Zaragoza,サラゴサ'),
	(3104341, 'ZA', '55', 'ES', 'Zamora', 'Provincia de Zamora,Zamora,サモーラ'),
	(3104469, 'BI', '59', 'ES', 'Vizcaya', 'Biscaia,Biscay,Biscaya,Biscaye,Bizcaya,Bizkaia,Provincia de Vizcaya,Senorio de Vizcaya,Señorío de Vizcaya,Vizcaya,ビスカヤ'),
	(3106671, 'VA', '55', 'ES', 'Valladolid', 'Balladolit,Provincia de Valladolid,Valladolid,バリァドリッド（バジャドリッド）'),
	(3108125, 'TE', '52', 'ES', 'Teruel', 'Province deTeruel,Province of Teruel,Provincia de Terol,Provincia de Teruel,Provinz Teruel,Província de Terol,Tergueel,Tergüel,Terol,Teruel,テルエル'),
	(3108287, 'T', '56', 'ES', 'Tarragona', 'Province de Tarragone,Province of Tarragona,Provincia de Tarragona,Provinz Tarragona,Província de Tarragona,Tarragona,タラゴナ'),
	(3108680, 'SO', '55', 'ES', 'Soria', 'Provincia de Soria,Soria,Sòria,Sória,ソリア'),
	(3109254, 'SG', '55', 'ES', 'Segovia', 'Provincia de Segovia,Segobia,Segovia,Segovie,Segòvia,Segóvia,Ségovie,セゴビア'),
	(3109716, 'S', '39', 'ES', 'Cantabria', 'Province de Cantabrie,Province of Cantabria,Provincia de Cantabria,Provinz Cantabria,Província de Cantabria,カンタブリア'),
	(3111107, 'SA', '55', 'ES', 'Salamanca', 'Provincia de Salamanca,Salamanca,Salamanque,サラマンカ'),
	(3113208, 'PO', '58', 'ES', 'Pontevedra', 'Pontebedra,Pontevedra,Provincia de Pontevedra,Puntevedra,ポンテベドラ'),
	(3114530, 'P', '55', 'ES', 'Palencia', 'Palence,Palencia,Palentzia,Palenzia,Palència,Palência,Provincia de Palencia,パレンシア'),
	(3114964, 'OR', '58', 'ES', 'Orense', 'Orense,Ourense,Provincia de Orense,Provincia de Ourense,オレンセ（オウレンセ）'),
	(3117813, 'LU', '58', 'ES', 'Lugo', 'Lugo,Provincia de Lugo,ルゴ'),
	(3118528, 'LE', '55', 'ES', 'León', 'Leon,León,Lion,Lión,Lleo,Lleó,Llion,Llión,Léon,Provincia de Leon,Provincia de León,レオン'),
	(3119840, 'C', '58', 'ES', 'La Coruña', 'A Coruna,A Coruña,Coruna,Corunha,Corunna,Coruña,La Corogne,La Coruna,La Coruña,Provincia de Coruna,Provincia de Coruña,Provincia de La Coruna,Provincia de La Coruña,ラコルーニャ,ラ・コルーニャ'),
	(3120513, 'HU', '52', 'ES', 'Huesca', 'Huesca,Osca,Province de Huesca,Province of Huesca,Provincia d\'Osca,Provincia de Huesca,Provinz Huesca,Província d\'Osca,Uesca,ウエスカ'),
	(3120935, 'SS', '59', 'ES', 'Guipúzcoa', 'Gipuzkoa,Guipuscoa,Guipuzcoa,Guipuzcua,Guipúscoa,Guipúzcoa,ギプスコア'),
	(3121069, 'GU', '54', 'ES', 'Guadalajara', 'Guadalachara,Guadalajara,Guadalaxara,Provincia de Guadalajara,グアダラハラ'),
	(3125881, 'CS', '60', 'ES', 'Castellón', 'Castello,Castellon,Castelló,Castellón,Provincia de Castello,Provincia de Castellon,Provincia de Castellon de la Plana,Provincia de Castellón,Provincia de Castellón de la Plana,Província de Castelló,カステジョン,カステヨン'),
	(3127460, 'BU', '55', 'ES', 'Burgos', 'Burgos,Provincia de Burgos,ブルゴス'),
	(3128759, 'B', '56', 'ES', 'Barcelona', 'Barcelona,Bartzelona,Barzelona,Province de Barcelone,Province of Barcelona,Provincia de Barcelona,Provinz Barcelona,Província de Barcelona,ばるせろな,バルセロナ'),
	(3129138, 'AV', '55', 'ES', 'Ávila', 'Abila,Avila,Badaxoz,Provincia de Avila,Provincia de Ávila,Àvila,Ávila,アビラ'),
	(3130717, 'VI', '59', 'ES', 'Álava', 'Alaba,Alava,Araba,Provincia de Alava,Provincia de Álava,Àlaba,Álava,アラバ'),
	(6355230, 'GI', '56', 'ES', 'Girona', 'Chirona,Gerona,Gerunda,Girona,Province de Gerone,Province de Gérone,Province of Girona,Provincia de Girona,Provinz Girona,Província de Girona,Xerona,Xirona,ジローナ（ヘローナ）'),
	(6355231, 'L', '56', 'ES', 'Lleida', 'Lerida,Lleida,Lérida,Province de Lleida,Province of Lleida,Provincia de Lleida,Provinz Lleida,Província de Lleida,ジェイダ（レリダ）'),
	(6355232, 'LO', '27', 'ES', 'La Rioja', 'A Rioxa,Errioxa,La Rioxa,Province de La Rioja,Province of La Rioja,Provincia de La Rioja,Provinz La Rioja,Província de La Rioja,ラ・リオハ'),
	(6355233, 'M', '29', 'ES', 'Madrid', 'Madrid,Madril,Province de Madrid,Province of Madrid,Provincia de Madrid,Provinz Madrid,Província de Madrid,マドリッド,マドリード/マドリ−ド/マドリ－ド'),
	(6355234, 'MU', '31', 'ES', 'Murcia', 'Murcia,Murtzia,Murzia,Múrcia,Province de Murcie,Province of Murcia,Provincia de Murcia,Provinz Murcia,Província de Múrcia,ムルシア'),
	(6355235, 'NA', '32', 'ES', 'Navarra', 'Nabarra,Nafarroa,Navarra,Province de Navarre,Province of Navarre,Provincia de Navarra,Provinz Navarra,Província de Navarra,ナバーラ'),
	(6355236, 'O', '34', 'ES', 'Asturias', 'Asturias,Asturie,Asturies,Province d\'Asturies,Province of Asturias,Provincia d\'Asturies,Provincia de Asturias,Provinz Asturien,Província d\'Astúries,アストゥリアス'),
	(6424360, 'PM', '07', 'ES', 'Islas Baleares', 'Balearane,Balearen Inseln,Balearene,Ilhas Baleares,Illes Balears,Islas Baleares,Province des Iles Baleares,Province des Îles Baléares,Province of Balearic Islands,Provincia de Illes Balears,Provincia de les Illes Balears,Província de les Illes Balears,las Baleares,ses Illes,バレアレス');";
}

/* Menus admin */

if (is_table('product__customer') && !Db::get_first("SELECT count(*) FROM all__menu WHERE menu_id = '20131'")){
	
	$read_write = Db::get_row("SELECT read1,write1 FROM all__menu WHERE menu_id = 20100");
	$read = $read_write['read1'];
	$write = $read_write['write1'];
	
	
	$update_sql []="INSERT INTO `all__menu` (`menu_id`, `menu_group`, `tool`, `tool_section`, `action`, `parent_id`, `toolmode_id`, `variable`, `ordre`, `link`, `process`, `bin`, `read1`, `write1`) VALUES (20131, 'customer', 'product', 'rate', '', 20100, 1, 'CUSTOMER_MENU_RATE', 3, '', '', 0, '$read', '$write');";

			$update_sql []="INSERT INTO `all__menu` (`menu_id`, `menu_group`, `tool`, `tool_section`, `action`, `parent_id`, `toolmode_id`, `variable`, `ordre`, `link`, `process`, `bin`, `read1`, `write1`) VALUES (20132, 'customer', 'product', 'rate', 'show_form_new', 20131, 1, 'CUSTOMER_MENU_RATE_NEW', 2, '', 'country', 0, '$read', '$write');";

			$update_sql []="INSERT INTO `all__menu` (`menu_id`, `menu_group`, `tool`, `tool_section`, `action`, `parent_id`, `toolmode_id`, `variable`, `ordre`, `link`, `process`, `bin`, `read1`, `write1`) VALUES (20133, 'customer', 'product', 'rate', 'show_form_new', 20131, 1, 'CUSTOMER_MENU_RATE_PROVINCIA_NEW', 3, '', 'provincia', 0, '$read', '$write');";

			$update_sql []="INSERT INTO `all__menu` (`menu_id`, `menu_group`, `tool`, `tool_section`, `action`, `parent_id`, `toolmode_id`, `variable`, `ordre`, `link`, `process`, `bin`, `read1`, `write1`) VALUES (20134, 'customer', 'product', 'rate', 'list_records', 20131, 1, 'CUSTOMER_MENU_RATE_LIST', 1, '', '', 0, '$read', '$write');";
			
}
elseif (is_table('custumer__product')){	
	trigger_error('Ja existeixen els menus de rates');
}


// Passo configs de tarifes al nou sistema

if (is_table('product__configadmin')) {	
	
	
	$send_rate_spain =  Db::get_first("SELECT value FROM product__configadmin WHERE name = 'send_rate_spain'");
	$send_rate_andorra =  Db::get_first("SELECT value FROM product__configadmin WHERE name = 'send_rate_andorra'");
	$send_rate_france =  Db::get_first("SELECT value FROM product__configadmin WHERE name = 'send_rate_france'");
	
	if (!is_table('product__rate')) {
		
		$update_sql []="
			CREATE TABLE IF NOT EXISTS `product__rate` (
			  `rate_id` int(11) NOT NULL auto_increment,
			  `country_id` char(2) collate utf8_spanish2_ci default NULL,
			  `provincia_id` int(11) default NULL,
			  `rate` decimal(8,2) default NULL,
			  PRIMARY KEY  (`rate_id`)
			) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;";
		
	}
	else{	
		trigger_error('Ja existeix la taula a product__rate');
	}
	
	if ($send_rate_spain != '0.00' && $send_rate_spain != '0' && $send_rate_spain){
		$update_sql []="INSERT INTO `product__rate` (`country_id`, `provincia_id`, `rate`) VALUES
	('ES', NULL, '".$send_rate_spain."')";
	}
	else{	
		trigger_error('No hi ha $send_rate_spain');
	}

	if ($send_rate_andorra != '0.00' && $send_rate_andorra != '0' && $send_rate_andorra){
		$update_sql []="INSERT INTO `product__rate` (`country_id`, `provincia_id`, `rate`) VALUES
	('AD', NULL, '".$send_rate_andorra."')";
	}
	else{	
		trigger_error('No hi ha $send_rate_andorra');
	}
	
	if ($send_rate_france != '0.00' && $send_rate_france != '0' && $send_rate_france){
		$update_sql []="INSERT INTO `product__rate` (`country_id`, `provincia_id`, `rate`) VALUES
	('FR', NULL, '".$send_rate_france."');";
	}
	else{	
		trigger_error('No hi ha $send_rate_france');
	}
	
	$update_sql []="ALTER TABLE `product__address`
						ADD COLUMN `provincia_id` INT(11) NOT NULL AFTER `country_id`;";
	
	// renombro i ja es borrarà un altre dia
	$update_sql []="UPDATE product__configadmin SET name='xxx send_rate_spain' WHERE  name='send_rate_spain';";
	$update_sql []="UPDATE product__configadmin SET name='xxx send_rate_andorra' WHERE  name='send_rate_andorra';";
	$update_sql []="UPDATE product__configadmin SET name='xxx send_rate_france' WHERE  name='send_rate_france';";
}

$update_sql []="UPDATE `all__country` SET `country`='España' WHERE `country_id`='ES';";
$update_sql []="UPDATE `all__country` SET `country`='España' WHERE `country_id`='195';";
$update_sql []="UPDATE `all__provincia` SET `name`='Gipuzkoa' WHERE  `provincia_id`=3120935;";
$update_sql []="UPDATE `all__provincia` SET `name`='A Coruña' WHERE  `provincia_id`=3119840;";
$update_sql []="UPDATE `all__provincia` SET `name`='Illes Baleares' WHERE  `provincia_id`=6424360;";
$update_sql []="UPDATE `all__provincia` SET `name`='Ourense' WHERE  `provincia_id`=3114964;";
$update_sql []="UPDATE `all__provincia` SET `name`='Bizkaia' WHERE  `provincia_id`=3104469;";


?>