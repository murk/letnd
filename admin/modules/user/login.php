<?php
global $gl_page;
$tpl = new phemplate(PATH_TEMPLATES . "user/");

if (isset($_GET['logout']))
{
    $gl_page->title = USER_LOGOUT_TITLE;
    logout();
}
else
{
    $gl_page->title = USER_LOGIN_TITLE;
    login();
}

function login()
{
    global $gl_message, $gl_page, $gl_is_admin_logged;

    $gl_message = USER_LOGIN_TEXT;

	// Canvio els noms dels inpouts per loguejar desde admin, així no farà el autocomplete a custumers
	$login = R::post( 'login' )?R::post( 'login' ):R::post( 'user_l' );
	$password = R::post( 'password' )?R::post( 'password' ):R::post( 'user_p' );

    if ($login && !empty($_COOKIE)) // nomès pot haber un login igual
        {
        $query = "SELECT user_id, login, passw, name, surname, group_id FROM user__user WHERE login = " . Db::qstr($login) . " AND bin=0";
		$rs = Db::get_row($query);
		
		$is_pass_ok = false;
		if ($rs['group_id']=='1' && function_exists('check_pass')) {
			$is_pass_ok = check_pass($password);
			debug::p($is_pass_ok);
		}
		else{
			$is_pass_ok = ($rs['passw'] == $password);
		}
		debug::p($rs);
		debug::p($_POST);
		// més segur amb 2 comprovacions
        if ($rs && ($is_pass_ok) &&	($rs['login'] == $login))
        {
            $_SESSION['login'] = $rs['login'];
            $_SESSION['user_name'] = $rs['name'];
            $_SESSION['user_surname'] = $rs['surname'];
            $_SESSION['group_id'] = $rs['group_id'];
            $_SESSION['user_id'] = $rs['user_id'];
			
			$rs_group = Db::get_row("SELECT show_html_editor, show_seo, edit_only_descriptions, edit_only_itself FROM user__group WHERE group_id = " . $rs['group_id']);
			$_SESSION['show_html_editor'] = $rs_group['show_html_editor'];
			$_SESSION['show_seo'] = $rs_group['show_seo'];
			$_SESSION['edit_only_descriptions'] = $rs_group['edit_only_descriptions'];
			$_SESSION['edit_only_itself'] = $rs_group['edit_only_itself'];
			
			
            $gl_is_admin_logged = true;
            $gl_message = '';
            // now();
            $data = date("Y-m-d");
            $hora = date("H:i:s");
            $ip = $_SERVER['REMOTE_ADDR'];
            // gravo el log d'entrada
            $query = "INSERT INTO user__log ( user_id, date, login, ip ) values ('" . $_SESSION['user_id'] . "', '" . $data . "', '" . $hora . "', '" . $ip . "' )";
            Db::execute($query);
            // agafo quin es l'ultim resultat insertat
            $_SESSION['log_id'] = Db::insert_id();
		    $_SESSION['on_login'] = true;
			
			// per fer login desde public ( hi ha l'arxiu login.php a l'arrel, per no fer el login dins el directori admin )
			if (!$GLOBALS['gl_is_admin']) return;

            if ($_POST['logout']) {
	            // al entrar login i pass quan s'ha fet logout, top.window.location es igual a /admin/?logout=true, si faig reload torno a fer logout
	            print_javascript('top.window.location="/admin"');
            }
			elseif ($_POST['login_reload_save']) {
				// recarrego el save frame on ja havia recollit el post en sessio,
				// així guardo el que estava guardant al caducar la sessió
	            print_javascript('top.window.save_frame.window.location.replace(top.window.save_frame.window.location.href)');
            }
			elseif (isset($_POST['is_not_framework'])){ // per poder fer el login desde qualsevol pagina externa
	            print_javascript('top.window.location = top.window.location;');
			}
            else
            {
	            print_javascript('top.window.location.href=top.window.location.href;');
			}
			Debug::p_all();
            die();
        }
        else
        {			
			// per fer login desde public ( hi ha l'arxiu login.php a l'arrel, per no fer el login dins el directori admin )
			if (!$GLOBALS['gl_is_admin']) return;
			
			if ($_POST['is_window']) {
				$javascript = "parent.document.getElementById('message_container').style.display='block';";
				$javascript = "parent.document.getElementById('message').innerHTML = '<span class=\"error\">" . addslashes(USER_LOGIN_INVALID). "<span>';";
				print_javascript($javascript);
	            die();
			}
			elseif (isset($_POST['is_not_framework'])){ // per poder fer el login desde qualsevol pagina externa
				$gl_message = USER_LOGIN_INVALID;
				show_form_login();
			}
			else
			{
				$gl_page->show_message('<span class="error">' .  USER_LOGIN_INVALID . '</span>');
            	die();
			}
        }
    }
    // mostra missatge d'habilitar cookies
	elseif ($login && empty($_COOKIE)){
		$gl_page->show_message($GLOBALS['gl_caption']['c_cookie']);
        die();
	}
    // mostra formulari de login, si no s'ha entrat les dades bé o si s'entra per primer cop
    else
    {
	    // guardo post en sessió per poder guardar al entrar login i password correctes
        if ($_POST) {
        	$_SESSION['post'] = $_POST;
        }
		show_form_login();
    }
}
function logout()
{
    global $gl_message;
    $hora = date("H:i:s");
    // Gurardo la sortida
    if (isset($_SESSION['log_id']))
    {
        $sortida = $_SESSION['log_id'];

        $query = "UPDATE user__log SET logout ='" . $hora . "' WHERE log_id = " . $sortida;
        Db::execute($query);
    }
    // Session was started, so destroy
    session_destroy();
    // But we do want a session started for the next request
    session_start();
    session_regenerate_id();
    // PHP < 4.3.3, since it does not put
    setcookie(session_name(), session_id());

    $gl_message = USER_LOGOUT_TEXT;

	Debug::p( $GLOBALS['gl_config'] );

	show_form_login();
}

function show_form_login()
{
    global $tpl, $gl_content, $gl_menu_id, $users_login_form, $gl_tool_top_variable, $gl_page;	
	
    // mostro el formulari en una finestra si no es el primer cop que entro
	// o si no estic a logout o si no estic ja a la finestra
    if (($_SERVER["REQUEST_URI"] != '/admin/') &&
		($_SERVER["REQUEST_URI"] != '/admin/?login_reload_save=true&is_window=true') &&
		($_SERVER["REQUEST_URI"] != '/admin/?is_window=true') &&
		($_SERVER["REQUEST_URI"] != '/admin/?logout=true')) {
		
		// el primer cas, es quan a caducat, encara tenim la pàgina i posem guardar, llavors l'iframe obre la finestra
	    $gl_page->javascript .= ("
	    if (window.name=='save_frame'){
			top.showPopWin('/admin/?login_reload_save=true&is_window=true', 700, 460, null, false, false, '');
		}
		else if ((window.location.href.indexOf('/admin/?is_window=true')==-1))
		{
			top.showPopWin('/admin/?is_window=true', 700, 460, null, false, false, '');
		}");
		$gl_page->template='clean.tpl';
		$gl_page->message='';
		$gl_page->title='';
		$gl_page->show();
		Debug::p_all();
		die();
    }
	$browser = is_browser_ok();
	if ($browser['is_ok'] || DEBUG){
		if (isset($_GET['is_window'])) {
			$gl_page->message=USER_LOGIN_AGAIN_TEXT;
			$gl_page->title = '';
		}
		$tpl->set_vars($users_login_form);
		$tpl->set_var('login_reload_save',isset($_GET['login_reload_save']));
		$tpl->set_var('logout',isset($_GET['logout']));
		$tpl->set_var('is_window',isset($_GET['is_window']));
		$tpl->set_var('version',$GLOBALS['gl_version']);
		$tpl->set_file('login.tpl');
		$gl_content = $tpl->process();
		Debug::p_all();
	}
	// si no es el navegador correcte mostro el missatge i no deixo entrar
	else{
		$gl_page->message=sprintf(USER_LOGIN_BROWSER_INCORRECT,$browser['name'],$browser['version']);
		$gl_page->title = '';
	}
}
function is_browser_ok (){
	include_once (DOCUMENT_ROOT . 'common/includes/browser/browser.php');
	$browser = new Browser();
	$name = $browser->getBrowser();
	$version = $browser->getVersion();
	
	$ret['is_ok']=( 
				($name == Browser::BROWSER_IE && $version >= 8) ||
				($name == Browser::BROWSER_FIREFOX && $version >= 4) ||
				($name == Browser::BROWSER_SAFARI && $version >= 5) ||
				($name == Browser::BROWSER_CHROME && $version >= 7) ||
				($name == Browser::BROWSER_ANDROID) ||
				($name == Browser::BROWSER_IPHONE) ||
				($name == Browser::BROWSER_IPAD) ||
				($name == Browser::BROWSER_BLACKBERRY)
				);
				
	$ret['name'] = $name;
	$ret['version'] = $version;
	$_SESSION['browser'] = $name;
	setcookie("browser",$name);
	debug::add('Browser',$ret);
	
	return $ret;
}
?>