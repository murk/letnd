<?

/* 23-4-15
  Modificacions imatges - imagemagick

UP-DATE
Actualitzacio en la gestió automàtica de les imatges, les imatges mantenen el seu perfil de color i les dades associades com ara l'autor
 */

// Poso uns valors per defecte per l'enfocat de les imatges
$update_sql = "
INSERT INTO `all__configadmin` (`name`, `value`) VALUES ('im_admin_thumb_unsharp', '0.5|0.5|2.5|0.02');
INSERT INTO `all__configadmin` (`name`, `value`) VALUES ('im_thumb_unsharp', '0.5|0.5|2.5|0.02');
INSERT INTO `all__configadmin` (`name`, `value`) VALUES ('im_details_unsharp', '1|0.5|2|0.02');
INSERT INTO `all__configadmin` (`name`, `value`) VALUES ('im_medium_unsharp', '');
INSERT INTO `all__configadmin` (`name`, `value`) VALUES ('im_big_unsharp', '');
";


if (is_table('directory__configadmin') && !Db::get_first("SELECT * FROM directory__configadmin WHERE name='im_admin_thumb_unsharp'" )) {
	$update_sql .= "
		INSERT INTO directory__configadmin (`name`, `value`) VALUES ('im_admin_thumb_unsharp', '0.5|0.5|2.5|0.02');";

}
else {
	if (is_table('directory__configadmin')) {
		trigger_error('Ja existeixen el camp im_admin_thumb_unsharp');
	}
}


?>