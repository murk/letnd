<?php
global $gl_tool, $gl_tool_section, $gl_db_classes_table, $gl_db_classes_id_field, $gl_page, $gl_upload, $gl_is_album, $gl_action, $gl_site_part;
//require(DOCUMENT_ROOT . 'common/includes/fpdf/fpdf.php');

// ara esta a 128M per defecte // ini_set("memory_limit","30M"); // aumento memoria, haig de controlar l'error quan sobrepassa
//define("K_TCPDF_EXTERNAL_CONFIG", true);
$tcpd_version = (VERSION==5)?'tcpdf6':'tcpdf5_php4';
require(DOCUMENT_ROOT . 'common/includes/'.$tcpd_version.'/tcpdf.php');
//require_once(DOCUMENT_ROOT . 'common/includes/'.$tcpd_version.'/config/tcpdf_config_alt.php');

set_config('inmo/inmo_property_config.php');
$gl_tool = 'inmo';
$gl_tool_section = 'property';
$gl_db_classes_table = 'inmo__property';
$gl_db_classes_id_field = 'property_id';
set_language_vars('print', $gl_tool_section, 'showwindow');
$gl_page->template = false;
//$gl_upload['image'] = new xxxxxxxxxploadFiles(false, $gl_tool . '__' . $gl_tool_section . '_image', $gl_tool_section . '_id', IMAGES_DIR, 'image');
// carrego funcions  de inmo i configuració
include (DOCUMENT_ROOT . $gl_site_part . '/modules/inmo/inmo_property_functions.php');
Module::get_config_values();

$gl_is_album = ($gl_action == 'show_record_album');

function save_rows()
{
}

function list_records()
{
}

function show_form()
{		
    global $inmo_form, $tpl, $gl_action, $gl_db_classes_fields, $gl_db_classes_language_fields, $gl_caption, $gl_is_album, $gl_site_part;
	
	if ($gl_site_part == 'public') {
		set_custom_config( $gl_config, $gl_db_classes_fields, $gl_db_classes_language_fields, $gl_caption);
		$gl_db_classes_fields['description']['type'] = 'text';
		unset($gl_db_classes_fields['property_file_name']); // evito el redirect que intenta fer si hi ha un nom d'arxiu
	}
	else
		set_custom_config();

	$show = new ShowForm();
	$show->is_index = false;

	
	
	$cg = showwindow_set_config($tpl);
	
	// configuracions a public, de moment ho agafa d'aquí, nomes es fa servir a easybrava 10/12/2014
	if ($gl_site_part == 'public') {
		$type = 'album';
		$gl_is_album = true;
	}
	else{
		$type = $gl_is_album?'album':'showwindow';
	}
	$template_id = $cg[$type . '_template_id'];
	// comprovo si hi ha una plantilla a la bbdd i agafo la config d'allà
	if (!$template_id){
		
		$template_id = Db::get_first("SELECT template_id FROM print__template WHERE type='" . $type . "' LIMIT 1");		
		
	}
	if ($template_id) {
		$template_rs = Db::get_row("SELECT file_name, size, orientation, ignore_config FROM print__template WHERE template_id = '" . $template_id . "' AND type='" . $type . "'");
		$ignore_config = $template_rs['ignore_config'];
		$template = $template_rs['file_name'];
		$size = $template_rs['size'];
		$orientation = $template_rs['orientation'];
	}
	else{
		$ignore_config = '0';
		$template = 'print_showwindow';
		$size = 'A4';
		$orientation = 'P';
	}
	
	Debug::p($_GET, $size);
	$tpl->set_var('size',$size);
	$tpl->set_var('orientation',$orientation);
	
	// DE moment nomes per BELL PROPERTY
	if ($ignore_config){
		$tpl->set_var('pages','null');
		$tpl->set_var('top_fields','null');
		$tpl->set_var('admin_show_zone','1');
		$tpl->set_var('admin_show_municipi','1');
		$tpl->set_var('admin_show_comarca','1');
		$tpl->set_var('admin_show_provincia','1');
	}
	
	// debug::p($template);
	// El client o te theme o te plantilla especifica o plantilla general a themes admin (te disseny web a mida però no aparador)
	
    if (is_file(PATH_TEMPLATES_PUBLIC . '/print/' . $template . '.tpl'))
    {
	    $tpl->path = PATH_TEMPLATES_PUBLIC;
    }
    elseif (is_file(PATH_THEMES_PUBLIC . '/print/' . $template . '.tpl'))
    {
	    $tpl->path = PATH_THEMES_PUBLIC;
	}
 	$show->template = 'print/' . $template;
    // referncia automatica
    if (INMO_PROPERTY_AUTO_REF == 1)
    {
        $show->fields['ref']['default_value'] = PROPERTY_AUTO_REF;
        $tpl->set_var('auto_ref', true);
    }
    else
    {
        // $show->fields['ref'] = '';
        $tpl->set_var('auto_ref', false);
    }

	$show->call('get_ref', 'ref,category_id,land,land_units,price,price_private,property_id,municipi_id,tipus');

    $query_provincies = "provincia_id='" . str_replace(",", "' OR provincia_id='", INMO_PROVINCIES) . "'";
    $show->fields["provincia_id"]['select_condition'] = $query_provincies;
    $show->get_values();

    if (!$show->has_results) Main::error_404('No existeix l\'inmoble');

    if ($show->rs['price_consult'] && !$gl_is_album)
    {
        $show->fields['price']['type'] = 'text'; // així no m'ho converteix a numero al cridar format_int
        $show->rs['price'] = $gl_caption['c_price_consult_1'];
	    $show->rs['old_price'] = 0;
    }
	else {
		$show->rs['old_price'] = get_old_price( $show->rs['price'], $show->rs['property_id']);
	}
    if ($show->rs['tipus2'] != 'new') $show->rs['tipus2'] = '';

	
    $show->show_form();
}

function write_record()
{
}

function manage_images()
{
    $image_manager = new ImageManager;
    $image_manager->execute();
}
function showwindow_set_config(&$tpl) {
	
	global $gl_action, $gl_is_album, $gl_config;
	// Configuracions de l'eina
	// agafo configuració del post o la BBDD
	$showwindow_type = $gl_is_album?'album':'showwindow';
	$cg = isset($_POST[$showwindow_type . '_pages'])?$_POST:$gl_config;

	//$size = $cg[$showwindow_type . '_size'];
	
	//$cg[$showwindow_type . '_template_id'] = R::id('template_id','','_POST');
	
	$pages = $cg[$showwindow_type . '_pages']=='null'?1000000000:$cg[$showwindow_type . '_pages'];
	$top_fields = isset($cg['showwindow_top_fields'])?$cg['showwindow_top_fields']:false;

	$cg[$showwindow_type . '_show_zone'] = $show_zone = isset_or_false($cg,$showwindow_type . '_show_zone');
	$cg[$showwindow_type . '_show_municipi'] = $show_municipi = isset_or_false($cg,$showwindow_type . '_show_municipi');
	$cg[$showwindow_type . '_show_comarca'] = $show_comarca = isset_or_false($cg,$showwindow_type . '_show_comarca');
	$cg[$showwindow_type . '_show_provincia'] = $show_provincia = isset_or_false($cg,$showwindow_type . '_show_provincia');

	if (isset($cg['save_config'])) showwindow_save_config($cg, $showwindow_type);
	// fi config
	$tpl->set_var('showwindow_type',$showwindow_type);
	$tpl->set_var('pages',$pages);
	$tpl->set_var('top_fields',$top_fields);
	$tpl->set_var('admin_show_zone',$show_zone);
	$tpl->set_var('admin_show_municipi',$show_municipi);
	$tpl->set_var('admin_show_comarca',$show_comarca);
	$tpl->set_var('admin_show_provincia',$show_provincia);

	return $cg;
}
function isset_or_false(&$cg, $key){
	return (isset($cg[$key]) && $cg[$key])?1:0;
}
function showwindow_save_config(&$cg, $type){
	global $gl_language;
	$fields = array(
	$type . '_size',
	$type . '_pages',
	$type . '_show_zone',
	$type . '_show_municipi',
	$type . '_show_comarca',
	$type . '_show_provincia',
	$type . '_template_id',
	);

	if ($type=='showwindow') $fields[]='showwindow_top_fields';

	foreach($fields as $field){
		Db::execute("UPDATE `inmo__configadmin` SET `value` = '".$cg[$field]."' WHERE `name` ='$field'");
	}
		Db::execute("UPDATE `inmo__configadmin` SET `value` = '".$gl_language."' WHERE `name` ='" . $type . "_language'");
}
?>