<?php
	// Aquest arxiu es genera automàticament
	$gl_file_protected = false;
	$gl_db_classes_fields_included = array (
  'history_id' => '1',
  'maquina_id' => '1',
  'estat_id' => '1',
  'date' => '1',
);
	$gl_db_classes_language_fields = array (
  0 => '',
);
	$gl_db_classes_fields = array (
  'history_id' => 
  array (
    'type' => 'hidden',
    'enabled' => '1',
    'form_admin' => 'input',
    'form_public' => 'text',
    'list_admin' => 'text',
    'list_public' => 'text',
    'order' => '0',
    'default_value' => '',
    'override_save_value' => '',
    'class' => '',
    'javascript' => '',
    'text_size' => '',
    'text_maxlength' => '11',
    'textarea_cols' => '',
    'textarea_rows' => '',
    'select_caption' => '',
    'select_size' => '',
    'select_table' => '',
    'select_fields' => '',
    'select_condition' => '',
  ),
  'maquina_id' => 
  array (
    'type' => 'select',
    'enabled' => '1',
    'form_admin' => 'input',
    'form_public' => 'text',
    'list_admin' => 'text',
    'list_public' => 'text',
    'order' => '1',
    'default_value' => '',
    'override_save_value' => '',
    'class' => '',
    'javascript' => '',
    'text_size' => '',
    'text_maxlength' => '11',
    'textarea_cols' => '',
    'textarea_rows' => '',
    'select_caption' => '',
    'select_size' => '',
    'select_table' => 'pvgmao__maquina',
    'select_fields' => 'pvgmao__maquina.maquina_id,maquina',
    'select_condition' => '',
  ),
  'estat_id' => 
  array (
    'type' => 'select',
    'enabled' => '1',
    'form_admin' => 'input',
    'form_public' => 'text',
    'list_admin' => 'text',
    'list_public' => 'text',
    'order' => '2',
    'default_value' => '',
    'override_save_value' => '',
    'class' => '',
    'javascript' => '',
    'text_size' => '',
    'text_maxlength' => '11',
    'textarea_cols' => '',
    'textarea_rows' => '',
    'select_caption' => '',
    'select_size' => '',
    'select_table' => 'pvgmao__estat',
    'select_fields' => 'pvgmao__estat.estat_id,estat',
    'select_condition' => '',
  ),
  'date' => 
  array (
    'type' => 'date',
    'enabled' => '1',
    'form_admin' => 'input',
    'form_public' => 'text',
    'list_admin' => 'text',
    'list_public' => 'text',
    'order' => '3',
    'default_value' => '',
    'override_save_value' => '',
    'class' => '',
    'javascript' => '',
    'text_size' => '',
    'text_maxlength' => '-1',
    'textarea_cols' => '',
    'textarea_rows' => '',
    'select_caption' => '',
    'select_size' => '',
    'select_table' => '',
    'select_fields' => '',
    'select_condition' => '',
  ),
);
	$gl_db_classes_list = array (
  'cols' => '1',
);
	$gl_db_classes_form = array (
  'cols' => '1',
  'images_title' => '',
);
	?>