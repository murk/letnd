<?php
// menus eina
if (!defined('REVISTA_MENU_PAGINA')){
	define('REVISTA_MENU_PAGINA','Edición de revistas'); 
	define('REVISTA_MENU_PAGINA_NEW','Entrar nueva revista');
	define('REVISTA_MENU_PAGINA_LIST','Listar revistas'); 
	define('REVISTA_MENU_CATEGORY','Categorías de apartados'); 
	define('REVISTA_MENU_CATEGORY_NEW','Nueva categoría'); 
	define('REVISTA_MENU_CATEGORY_LIST','Listar categorías');
	define('REVISTA_MENU_PAGINA_BIN','Papelera de reciclage'); 

	
	define('REVISTA_MENU_BANNER','Banners');
	define('REVISTA_MENU_BANNER_NEW','Nuevo banner');
	define('REVISTA_MENU_BANNER_LIST','Listar banners');
	define('REVISTA_MENU_BANNER_LIST_BIN','Papelera');
}
 
/*--------------- nomès castellà i català, no traduit a la resta -------------------*/


$gl_caption_pagina['c_edit_revista'] = 'Datos de la revista';
$gl_caption_pagina['c_edit_apartat'] = 'Datos del reportaje';
$gl_caption_pagina['c_edit_pagina'] = 'Datos de la página';
$gl_caption_pagina['c_page_title_title'] = 'Datos de la página';
$gl_caption_pagina['c_column_title1'] = 'Primera columna';
$gl_caption_pagina['c_column_title2'] = 'Segunda columna';
$gl_caption_pagina['c_column_title3'] = 'Tercera columna';
$gl_caption_pagina['c_column_title4'] = 'Vídeo';

$gl_caption_pagina['c_breadcrumb_edit_revista'] = 'editar revista';
$gl_caption_pagina['c_breadcrumb_edit_apartat'] = 'editar reportaje';
$gl_caption_pagina['c_breadcrumb_edit_pagina'] = 'editar página';

$gl_caption_pagina['c_subtitle_apartat'] = 'Listar reportajes';
$gl_caption_pagina['c_subtitle_pagina'] = 'Listar páginas';

$gl_caption_pagina['c_is_clubone'] = 'Socios';

$gl_caption_pagina['c_is_advertisement'] = 'Publicidad';

$gl_caption_pagina['c_filter_status'] = 'Todos los estados';
$gl_caption_pagina['c_filter_category_id'] = 'Todas las categorías';
$gl_caption_pagina['c_filter_paginatpl_id'] = 'Todas las plantillas';

$gl_caption_pagina['c_filter_is_clubone'] = 'Públicos i Socios';
$gl_caption_pagina['c_is_clubone_0'] = 'Público';
$gl_caption_pagina['c_is_clubone_1'] = 'Socios';

$gl_caption_pagina['c_filter_is_advertisement'] = 'Todos (publicidad o no)';
$gl_caption_pagina['c_is_advertisement_0'] = 'No publicidad';
$gl_caption_pagina['c_is_advertisement_1'] = 'Publicidad';



$gl_caption_pagina['c_ordre'] = 'Orden';
$gl_caption_pagina['c_entered'] = 'Creado';
$gl_caption_pagina['c_modified'] = 'Última Modificación';
$gl_caption_pagina['c_template'] = 'Plantilla';
$gl_caption_pagina['c_paginatpl_id'] = 'Plantilla';
$gl_caption_pagina['c_link'] = 'Link';
$gl_caption_pagina['c_pagina_file_name'] = $GLOBALS['gl_caption']['c_file_name'];
$gl_caption_pagina['c_body_supertitle'] = 'Supertítulo';
$gl_caption_pagina['c_body_supertitle2'] = 'Supertítulo';
$gl_caption_pagina['c_body_supertitle3'] = 'Supertítulo';
$gl_caption_pagina['c_title'] = 'Título';
$gl_caption_pagina['c_title2'] = 'Título';
$gl_caption_pagina['c_title3'] = 'Título';
$gl_caption_pagina['c_body_subtitle'] = 'Subtítulo';
$gl_caption_pagina['c_body_subtitle2'] = 'Subtítulo';
$gl_caption_pagina['c_body_subtitle3'] = 'Subtítulo';
$gl_caption_pagina['c_pagina'] = 'Nombre del reportaje en el menú';
$gl_caption_pagina_list['c_pagina'] = 'Nombre';
$gl_caption_pagina['c_col_valign'] = $gl_caption_pagina['c_col_valign2'] = $gl_caption_pagina['c_col_valign3'] = 'Alineación vertical';
$gl_caption_pagina['c_col_valign_top'] = $gl_caption_pagina['c_col_valign2_top'] = $gl_caption_pagina['c_col_valign3_top'] = 'Arriba';
$gl_caption_pagina['c_col_valign_middle'] = $gl_caption_pagina['c_col_valign2_middle'] = $gl_caption_pagina['c_col_valign3_middle'] = 'Medio';
$gl_caption_pagina['c_col_valign_bottom'] = $gl_caption_pagina['c_col_valign2_bottom'] = $gl_caption_pagina['c_col_valign3_bottom'] = 'Abajo';
$gl_caption_pagina['c_body_content'] = 'Contenido';
$gl_caption_pagina['c_body_content2'] = 'Contenido';
$gl_caption_pagina['c_body_content3'] = 'Contenido';
$gl_caption_pagina['c_prepare'] = 'En preparación';
$gl_caption_pagina['c_review'] = 'Para revisar';
$gl_caption_pagina['c_public'] = 'Público';
$gl_caption_pagina['c_archived'] = 'Archivado';
$gl_caption_pagina['c_status'] = 'Estado';
$gl_caption_pagina['c_pagina_id'] = 'Id';
$gl_caption_pagina['c_parent_id'] = 'Pertenece a';
$gl_caption_pagina['c_custom_id'] = 'Galeria de personalitzación';
$gl_caption_pagina['c_level'] = '';
$gl_caption_pagina['c_blocked'] = 'Block.';

$gl_caption_pagina['c_add_revista_button'] = 'Insertar nueva revista';
$gl_caption_pagina['c_add_apartat_button'] = 'Insertar nuevo reportaje';
$gl_caption_pagina['c_add_pagina_button'] = 'Insertar nueva página';
$gl_caption_pagina['c_edit_revista_button'] = 'Editar revista';
$gl_caption_pagina['c_edit_apartat_button'] = 'Editar reportaje';
$gl_caption_pagina['c_edit_pagina_button'] = 'Editar página';
$gl_caption_pagina['c_view_apartat_button'] = 'Listar reportajes';
$gl_caption_pagina['c_view_pagina_button'] = 'Llistar páginas';
$gl_caption_pagina['c_url1_name']='Nom URL';
$gl_caption_pagina['c_url1']='URL';
$gl_caption_pagina['c_url2_name']='Nom URL(2)';
$gl_caption_pagina['c_url2']='URL(2)';
$gl_caption_pagina['c_url3_name']='Nom URL(3)';
$gl_caption_pagina['c_url3']='URL(3)';
$gl_caption_pagina['c_videoframe']='Vídeo (youtube, metacafe...)';
$gl_caption_pagina['c_file']='Archivos';
$gl_caption_pagina['c_category_id']='Categoría';
$gl_caption_image['c_main_image'] = 'Fondo página';
$gl_caption_image['c_main_image2'] = 'Imagen listado';

$gl_caption['c_last_listing']='Listado anterior';


$gl_caption_category['c_ordre']='Ordre';
$gl_caption_category['c_category']='Categoría';
$gl_caption_category['c_is_clubone'] = 'Menú socios';

$gl_messages['c_restore_items'] = "Los elementos marcados no se han podido recuperar, ya que primero hay que recuperar el reportaje o la revista a la que pertenecen";




/*banners*/
$gl_caption_banner['c_banner_id']='';
$gl_caption_banner['c_ordre']='Orden';
$gl_caption_banner['c_url1']='Enlace';
$gl_caption_banner['c_url1_name']='Texto en el enlace';
$gl_caption_banner['c_url1_target']='Destíno';
$gl_caption_banner['c_url1_target__blank']='Una nueva ventana';
$gl_caption_banner['c_url1_target__self']='La misma ventana';
$gl_caption_banner['c_file']='Imagen banner <br>(960x92 - 248x122)';
$gl_caption_banner['c_banner']='Banner';
?>