<style type="text/css">
	#querys_holder,
	#municipis_holder,
	#errors_holder {
		display: none;
	}
	.message_holder {
		padding: 20px;
		border-top: 1px solid #D3D3D3;
	}
	.message_holder table td {
		padding: 2px 5px;
	}
	.message_holder table th {
		padding: 2px 5px;
		text-align: left;
	}
	h6 {
		font-size: 20px;
		margin: 10px 5px;
	}
	h6 span {
		font-size: 16px;
		font-weight: normal;
	}
	#confirm {
		display: none;
		padding: 25px;
		overflow: hidden;
		width: 80%;
	}
	#done {
		display: none;
		padding: 25px;
		font-size: 20px;
	}
	tr.import-gris td {
		color: #a8a8a8 !important;
	}
	tr.import-blau td {
		color: #12a6ee !important;
	}
	tr.vermell td {
		color: #CB2903 !important;
	}
	tr td.error {
		color: #CB2903 !important;
		font-weight: bold;
	}
</style>
<script type="text/javascript">

	$(function () {
		$('#excel').change(function () {

			var val = $('#excel').val();
			var ext_arr = val.split('.');
			var ext = ext_arr[ext_arr.length - 1];

			if (ext != 'xlsx') {
				alert('L\'arxiu seleccionat no és un arxiu d\'excel correcte');
				$('#excel').val('');
				return;
			}

			if (!$('#group').val()) {
				$('#group').val(ext_arr[ext_arr.length - 2]);
			}
		});
	});

</script>
<form id="import_form" name="theForm" method="post" action="/admin/?menu_id=2552&action=add_record" enctype="multipart/form-data" target="save_frame" onsubmit="$('#data_holder,#querys_holder,#municipis_holder').html('')">
	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="form">
		<tr>
			<td>
				<div id="form_buttons_holder">
					<table id="form_buttons">
						<tr>
							<td valign="bottom">
								<div class="menu"></div>
							</td>
							<td class="buttons_top">
								<div class="right"></div>
							</td>
						</tr>
					</table>
				</div>


				<? ////////////   TITOL  ////////////////?>


				<table width="100%" cellpadding="0" cellspacing="0">
					<tr>
						<td class="formCaptionTitle"><strong>
								<?= $c_import_title ?>
								&nbsp;&nbsp;</strong></td>
					</tr>
				</table>


				<? ////////////   CAMPS  ////////////////?>

				<table width="100%" cellpadding="0" cellspacing="0" class="formstbl">

					<tr>
						<td valign="top" class="formsCaption">
							<?= $c_select_file ?>:
						</td>
						<td valign="top" class="forms">
							<input class="file" type="file" id="excel" name="excel"/>
						</td>

						<? /* <td>Mostra dades <input name="show_data" type="checkbox" value="1"></td> */ ?>
						<input type="hidden" name="show_data" value="1">

						<? if ( $_SESSION['group_id'] == 1 ): ?>

							<td>Mostra querys <input name="show_querys" type="checkbox" checked value="1"></td>

						<? endif // $GLOBALS['gl_is_admin'] ?>
					</tr>

				</table>

				<table width="100%" cellpadding="4" cellspacing="2" class="formsButtonstbl">
					<tr>
						<td align="center" class="formsButtons">
							<div class="centerFloat1">
								<div class="centerFloat2">
									<a href="javascript:$('#import_form').submit()" class="botoOpcions"><?= $c_import_preview ?></a>
								</div>
							</div>
						</td>
					</tr>
				</table>

				<div id="confirm">
					<input type="submit" name="import" value="<?= $c_import ?>" class="btn_form1">
				</div>
				<div id="done">
					Importació finalitzada
				</div>
				<div id="errors_holder" class="message_holder">
				</div>
				<div id="municipis_holder" class="message_holder">
				</div>
				<div id="data_holder" class="message_holder">
				</div>
				<div id="querys_holder" class="message_holder">
				</div>

			</td>
		</tr>
	</table>
</form>