<?

/* 25-7-14 
  Modificacions immo reserves

 */

if (is_table('booking__propertyrange')) {	
	
	$update_sql .= "
		ALTER TABLE `booking__propertyrange`
	ADD COLUMN `whole_week` TINYINT(1) NOT NULL DEFAULT '0' AFTER `minimum_nights`,
	ADD COLUMN `start_day` TINYINT(2) NOT NULL DEFAULT '0' AFTER `whole_week`;
		";	
}

if (is_table('custumer__custumer')) {	
	
	$update_sql .= "
		ALTER TABLE `custumer__custumer`
	CHANGE COLUMN `country` `country` VARCHAR(50) NOT NULL DEFAULT 'ES' COLLATE 'utf8_spanish2_ci' AFTER `province`;
		";	
}

?>