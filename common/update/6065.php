<?

/* 21-12-15

	Afegir un camp als arxius de news de mostrar a la home
 */

if ( is_table( 'news__new_file' ) && ! is_field( 'news__new_file', 'show_home' ) ) {

	$update_sql[] = "ALTER TABLE `news__new_file`
	ADD COLUMN `show_home` TINYINT(1) NOT NULL DEFAULT '0' AFTER `main_file`;";

} else {
	if ( is_table( 'news__new_file' ) ) {
		trigger_error( 'Ja existeixen el camp show_home' );
	}
}

if ( is_table( 'news__configadmin' ) && ! Db::get_first( "SELECT * FROM news__configadmin WHERE name='has_files_show_home'" ) ) {
	$update_sql[] = "
		INSERT INTO news__configadmin (`name`, `value`) VALUES ('has_files_show_home', '0');";

} else {
	if ( is_table( 'news__configadmin' ) ) {
		trigger_error( 'Ja existeixen el camp has_files_show_home' );
	}
}

?>