<?php
class ProductBrand extends Module{

	function __construct(){
		parent::__construct();
	}

	function on_load(){
		
		if (!$this->config['is_brand_rate_on']){
		
			$this->unset_field('send_rate_spain');
			$this->unset_field('send_rate_france');
			$this->unset_field('send_rate_andorra');
			$this->unset_field('send_rate_world');
			$this->unset_field('minimum_buy_free_send');
		
		}
	}
	function list_records()
	{
		$GLOBALS['gl_content'] = $this->get_records();
	}
	function get_records()
	{
		
	    $listing = new ListRecords($this);	
			
		$listing->order_by = 'brand_name ASC, ordre ASC';
		
		//$listing->add_swap_edit();
		//$listing->add_dots_filter();		
		
	    return $listing->list_records();
	}
	function show_form()
	{
		$GLOBALS['gl_content'] = $this->get_form();
	}
	function get_form()
	{
	    $show = new ShowForm($this);
	    return $show->show_form();
	}

	function save_rows()
	{
	    $save_rows = new SaveRows($this);
	    $save_rows->save();
	}

	function write_record()
	{
	    $writerec = new SaveRows($this);
	    $writerec->save();
	}

	function manage_images()
	{
	    $image_manager = new ImageManager($this);
	    $image_manager->execute();
	}
}
?>