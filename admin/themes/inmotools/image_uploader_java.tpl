<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<script type="text/javascript" src="/common/jscripts/jquery/jquery-1.7.2.min.js"></script>


	<?=load_admin_css()?>

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<form method="post" name="form_images" id="form_images" action="">
    <input name="<?=$section_id_field?>" id="<?=$section_id_field?>" type="hidden" value="<?=$section_id?>">
</form>
<div id="image_javaupload_div" style="display:block">
<applet id="jumploader" name="jumpLoaderApplet"	code="jmaster.jumploader.app.JumpLoaderApplet.class" archive="/common/includes/jumploader2.28/mediautil_z.jar,/common/includes/jumploader2.28/sanselan_z.jar,/common/includes/jumploader2.28/jumploader_z.jar"	width="100%" height="700" mayscript>
		<param name="uc_uploadUrl" value="<?=$link_action_upload_images?>"/>
		<param name="uc_uploadFormName" value="form_images">
		<param name="uc_uploadScaledImages" value="true"/>
		<param name="uc_uploadScaledImagesNoZip" value="true"/>
        <param name="uc_imageEditorEnabled" value="true"/>
        <param name="uc_imageRotateEnabled" value="true"/>
        <param name="uc_useLosslessJpegTransformations" value="true"/>
        <param name="uc_addImagesOnly" value="true"/>
		<param name="vc_fileBrowserInitialLocation" value="<?=$uploader_initial_location?>"/>
		<param name="vc_mainViewFileTreeViewVisible" value="true"/>
		<param name="vc_mainViewFileListViewVisible" value="true"/>
		<param name="vc_fileTreeViewShowFiles" value="true"/>
        <param name="vc_lookAndFeel" value="system"/>
        <param name="vc_disableLocalFileSystem" value="false"/>  
		<param name="uc_fileNamePattern" value="^.+\.(?i)((jpg)|(jpe)|(jpeg)|(gif)|(png)|(tif)|(tiff))$"/>
        <param name="vc_fileNamePattern" value="^.+\.(?i)((jpg)|(jpe)|(jpeg)|(gif)|(png)|(tif)|(tiff))$"/>
		
        <param name="ac_fireUploaderStatusChanged" value="true"/>
		<?if(DEBUG):?>
		<param name="ac_fireAppletInitialized" value="true"/>
        <param name="ac_fireUploaderFileAdded" value="true"/>
		<param name="ac_fireUploaderFileStatusChanged" value="true"/>
		<?endif // DEBUG?>
        
		<param name="ac_messagesZipUrl" value="/common/includes/jumploader2.28/messages_<?=LANGUAGE_CODE?>.zip"/>
        
		<param name="uc_scaledInstancePreserveMetadata" value="true"/>
        <param name="uc_deleteTempFilesOnRemove" value="true"/>
		<param name="uc_scaledInstanceNames" value="Thumbnail1,Thumbnail2"/>
        <param name="uc_scaledInstanceDimensions" value="callback:imageScaleCallback,callback:imageScaleCallback"/>
        <param name="uc_scaledInstanceQualityFactors" value="<?=$im_medium_q*10?>,<?=$im_medium_q*10?>"/>
        <param name="scaledInstanceScaleMode" value="smooth,smooth"/>
        <param name="uc_sendFilePath" value="true"/>
          
		<? /* if($watermark):?>
        <param name="uc_scaledInstanceWatermarkNames" value="Watermark1,Watermark2"/>
        <param name="Watermark1" value="halign=<?=$watermark_halign?>;valign=<?=$watermark_valign?>;opacityPercent=<?=$watermark_opacity?>;imageUrl=/<?=CLIENT_DIR?>/images/logo_watermark.png"/>
        <param name="Watermark2" value="halign=<?=$watermark_halign?>;valign=<?=$watermark_valign?>;opacityPercent=<?=$watermark_opacity?>;imageUrl=/<?=CLIENT_DIR?>/images/logo_watermark.png"/>
		<?endif // watermark */ ?>
        
        
        <param name="vc_uploadViewFileChooserMultiSelectionEnabled" value="true"/>        
         
        <param name="ic_arbitraryResizeEnabled" value="true"/>       
        <param name="ic_showSelectionInfo" value="true"/>
        <param name="ic_cropRatioOptions" value="Relació=;<?=$im_custom_ratios?>4/3=1.3333;3/2=1.5;5/4=1.25;16/9=1.7778"/>
</applet>
</div>
<div id="image_upload_div" style="display:block"><?// per que funcioni amb aurigma, ja que allà amago el activeX, aqui no fa falta amgar applet, sino al contrari per que carregui abans ?></div>
<?if(!DEBUG):?>
<script>
	function appletInitialized( applet  ) {
//		parent.document.getElementById('imagesFrame').style.height='1750px';
//		$('#jumploader').height('700');
	}
	/**
	 * uploader status changed notification
	 */
	function uploaderStatusChanged( uploader ) {
		if (uploader.isReady()){
			top.window.save_frame.location.href = '<?=$reload_link?>&is_ajax=true';

		}
	}
	function imageScaleCallback( uploadFile, scaledInstanceName ){
		var uploader = jumpLoaderApplet.getUploader();
		var imageInfo = uploader.getImageInfo(uploadFile);
		var width = imageInfo.getImageWidth(0);
		var height = imageInfo.getImageHeight(0);
		//alert(scaledInstanceName);
		if (scaledInstanceName == 'Thumbnail1'){
			new_w = <?=$im_big_w?$im_big_w:1000000?>;
			new_h = <?=$im_big_h?$im_big_h:1000000?>;
		}
		else if (scaledInstanceName == 'Thumbnail2'){
			new_w = <?=$im_medium_w?$im_medium_w:1000000?>;
			new_h = <?=$im_medium_h?$im_medium_h:1000000?>;
		}
	
		return new_w+'x'+new_h+'xfit';
	}
</script>
<?endif // DEBUG?>
<?if(DEBUG):?>
<script>
	function appletInitialized( applet  ) {
//		parent.document.getElementById('imagesFrame').style.height='1750px';
//		$('#jumploader').height('700');
	}
	/**
	 * trace event to events textarea
	 */
	function traceEvent( message ) {
		document.debugForm.txtEvents.value += message + "\r\n";
	}
	/**
	 * uploader status changed notification
	 */
	function uploaderStatusChanged( uploader ) {
		traceEvent( "uploaderStatusChanged, status=" + uploader.getStatus() );
		if (uploader.isReady()){
			top.window.save_frame.location.href = '<?=$reload_link?>&is_ajax=true&is_uploader=true';
		}
	}
	/**
	 * file added notification
	 */
	function uploaderFileAdded( uploader, file ) {
		traceEvent( "uploaderFileAdded, index=" + file.getIndex() + ", imageInfo=" + uploader.getImageInfo(file) );
	}
	/**
	 * file status changed notification
	 */
	function uploaderFileStatusChanged( uploader, file ) {
		traceEvent( "uploaderFileStatusChanged, index=" + file.getIndex() +
			", status=" + file.getStatus() +
			", content=" + file.getResponseContent() );
			
		if( file.isFinished() ) {
			var serverFileName = file.getId() + "." + file.getName() + ".zip";
			var linkHtml = "<a href='/uploaded/" + serverFileName + "'>" + serverFileName + "</a> " + file.getLength() + " bytes";
			traceEvent( linkHtml);
		}
	}
	function imageScaleCallback( uploadFile, scaledInstanceName ){
		var uploader = jumpLoaderApplet.getUploader();
		var imageInfo = uploader.getImageInfo(uploadFile);
		var width = imageInfo.getImageWidth(0);
		var height = imageInfo.getImageHeight(0);
		//alert(scaledInstanceName);
		if (scaledInstanceName == 'Thumbnail1'){
			new_w = <?=$im_big_w?$im_big_w:1000000?>;
			new_h = <?=$im_big_h?$im_big_h:1000000?>;
		}
		else if (scaledInstanceName == 'Thumbnail2'){
			new_w = <?=$im_medium_w?$im_medium_w:1000000?>;
			new_h = <?=$im_medium_h?$im_medium_h:1000000?>;
		}
		traceEvent( new_w+'x'+new_h+'xfit');
	
		return new_w+'x'+new_h+'xfit';
	}
</script>
<form name="debugForm">
	<p>Events:<br>
	<textarea name="txtEvents" style="width:100%; font:10px monospace;height:500px;" wrap="off" id="txtEvents"></textarea>
	</p>
</form>
<?endif // DEBUG?>
</body>
</html>