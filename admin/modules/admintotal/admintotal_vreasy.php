<?
/**
 * VREASY
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) Gener 2018
 * @version 1
 * @access public
 */

/*

Activar client
---------------
Es obligatori tindre SSL per la resposta de Vreasy
	1 - Posar les variables a client__client
	2 - Assegurar-se que la url principal sigui la primera ( domain de la taula client__client ): ej. easybrava.com i no easybrava.cat
	3 - Inscriure a l'api de vreasy: /admin/?tool=admintotal&tool_section=vreasy&action=set_subscriptions&agencia_id=    Per forçar borrar subscripcions: &force_delete=1
		Per veure a què està subscrit: /admin/?tool=admintotal&tool_section=vreasy&action=view_subscriptions

FEATURES
-----------
Inmobles

- Quan es marca per primer cop un immoble per exportar a Vreasy:
    Es crea l’immoble a Vresay amb les dades bàsiques
    Es pugen les imatges
    Es creen totes les reserves futures dels immobles a Vreasy
    Queda vinculat amb l’inmoble de Letnd

- L’inmoble vinculat ja no es sincronitza més

- Quan desmarquem l’inmoble per exportar a Vreasy es desactiva a Vreasy

- Quan tornem a marcar per exportar a Vreasy s’activa a Vreasy

- Quan eliminem l’immoble es desactiva a Vreasy

- Quan restaurem l’immoble s’activa a Vreasy

- Els inmobles no s'esborren mai de Vreasy, queden desactivats. Però quan s’esborra de Letnd es desvinculen

Reserves

- Les reserves es sincronitzen automàticament en els 2 sentits

- Una reserva si es fa a Letnd es propietat Letnd, si es fa a Vreasy es propietat Vreasy.

- Letnd no pot editar una reserva de Vreasy

- Vreasy pot editar una reserva de Letnd ( això no es pot bloquejar des del nostre programa ), però no es guarda a Letnd i s’envia un mail a l’administrador amb el conflicte

- Si Vreasy fa una reserva que no es compatible amb les temporades a Letnd, es graba la reserva i s’envia un email a l’administrador per notificar el conflicte


VREASY URLS
--------------
https://www.vreasy.com/docs/public-api/

yv08j0xpkysw8ogog884kwww8s40800w000g4

Veure propietats:
w2.easybrava/admin/?tool=admintotal&tool_section=vreasy&action=get_vreasy_properties&agencia_id=3

Veure subscripcions:
w2.easybrava/admin/?tool=admintotal&tool_section=vreasy&action=set_subscriptions

PROBES PER LINUX CURL O WEB
---------------------------

https://www.vreasy.com/login marc@letnd.com vreasy12345

PER FER PROBES ONLINE - Nomes ho puc fer a www.letnd.com ja que necessito el https per les respostes
	M'haig de subscriure al server - client_id de letnd = 3
	w.easybrava/admin/?tool=admintotal&tool_section=vreasy&action=set_subscriptions&agencia_id=3&force_delete=1

PER FER PROBES EN LOCAL - M'haig de subscriure per local
	Client Easy Test - client_id = 193 - 'w2.letnd.com:8080' - 'yv08j0xpkysw8ogog884kwww8s40800w000g4';
	w.easybrava/admin/?tool=admintotal&tool_section=vreasy&action=set_subscriptions&agencia_id=193&force_delete=1

Probar un api call:
	w.easybrava/admin/?tool=admintotal&tool_section=vreasy&action=api_call

curl -u "yv08j0xpkysw8ogog884kwww8s40800w000g4" -X GET "https://api.vreasy.com/properties/new"

EXPORTAR TOT ( nomes les que no estan exportades, utilitzar només en TEST )
---------------------------------------------------
/admin/?tool=admintotal&tool_section=vreasy&action=start

TICKETS
----------------
Enviar email a  support+api@vreasy.com


TODO-i No puc reactivar un inmoble, s'ha de posar el missatge que no es pot

TODO-i Probar reserva desde public
TODO-i Posar guardat automatic a public


*/

class AdmintotalVreasy extends Module {

	public $exporter = 'vreasy';
	public $section = 'property';
	public $is_global_export = false;
	public $config_rs = [];
	public $export_rs = [];
	public $property_rs = [];
	public $agencia_rs = [];
	public $api_url = "https://api.vreasy.com";
	public $is_cli = false;
	public $headers = [];

	public $response_errors = [];
	public $agencia_exported_ids = [];
	public $agencia_vreasy_exported_ids = [];

	// Per poder exportar nomes una agencia i un inmoble, en temps real
	public $agencia_id = 0;
	public $property_ids = [];
	public $portal_vars = [];
	public $client_images_url;


	public $vreasy_category = [
		'4' => 2, // Apartment
		'12' => 2, // Apartment
		'14' => 2, // Apartment
		'15' => 2, // Apartment
		'25' => 2, // Apartment
		// '' => 3, // Bed and Breakfast
		// '' => 4, // Campsites
		// '' => 5, // Hotel
		// '' => 6, // Hostel
		'2' => 7, // House
		'3' => 7, // House
		'22' => 7, // House
		'23' => 7, // House
		'24' => 7, // House
		// '' => 8, // Room
		'16' => 9, // Studio Apartment
		// '' => 11, // Bungalow
		// '' => 98, // POI
		// '' => 112, // Cottage
		'1' => 113, // Villa
		// '' => 114, // Penthouse Apartment
		// '' => 115, // Yacht
		// '' => 116, // Boat
		// '' => 117, // Houseboat
		// '' => 118, // Cabin
		'13' => 119, // Chalet
		// '' => 120, // Castle
		// '' => 121, // Resort
		// '' => 122, // Farmhouse
		// '' => 123, // Other
		// '' => 124, // State
		// '' => 125, //
		// '' => 126, // Barn
		'27' => 127, // Building
		// '' => 128, // Chateau
		// '' => 129, // Chacara
		// '' => 130, // Condo
		// '' => 131, // Estate
	];

	public $vreasy_languages = [
		'por' => 12, //Portugués
		'eng' => 14, //Inglés
		'fra' => 4, //Francés
		'spa' => 3, //Español
		'ita' => 6, //Italiano
		'deu' => 5, //Alemán
		'cat' => 2, //Catalán
		'dut' => 10, //	Holandés
	];

	function __construct() {

		include_once( DOCUMENT_ROOT . 'admin/modules/admintotal/export_common.php' );

		ExportCommon::init_unirest( $this );

		parent::__construct();
	}

	function start() {
		$this->_start();
		die();
	}


	// Funcions per actualitzar automàticament al guardar
	function on_save_portal() {
		$this->trace( print_r( $this, true ), 'on_save_portal', 'ON SAVE' );
		$this->property_ids = array_merge ($this->portal_vars ['vreasy']['new'], $this->portal_vars ['vreasy']['equal']); // Poso equal també ja que pot ser que per falta de dades tampoc s'hagi exportat
		$deleted_ids = $this->portal_vars['vreasy']['deleted'];

		$export_all = Db::get_first("SELECT value FROM inmo__configadmin WHERE name = 'fotocasa_export_all'");
		// TODO-i Fer alguna acció per que actualitzi tot el primer cop

		$this->set_on_vars();

		$this->_start( false );

		$this->trace( print_r( $this, true ), 'on_save_portal', 'ON SAVE' );

		foreach ( $deleted_ids as $property_id ) {
			$this->change_property_active_state( $property_id, 'deactivate' );
		}
		foreach ( $this->property_ids as $property_id ) {
			$this->change_property_active_state( $property_id, 'activate' );
		}


		ExportCommon::end( $this );

	}

	function on_save_update() {

		$this->set_on_vars();
		$this->_start( false );

		// haig de borrar els que no s'han exportat, poder perque ha canviat estat, han tret les coordenades, etc..
		$not_exported = array_diff( $this->property_ids, $this->agencia_exported_ids );


		ExportCommon::p( "Eliminació d'inmobles: " . $this->agencia_rs['comercialname'], 1 );


		// funciona perque nomes hi ha una agència, sino hauria d'anar al loop a export_properties
		foreach ( $not_exported as $property_id ) {
			$this->change_property_active_state( $property_id, 'check' );
		}

		ExportCommon::end( $this );

	}

	function on_save_restore() {

		// No em permet reactivar l'inmoble, s'ha de fer a vreasy

		$this->set_on_vars();

		$agencias = ExportCommon::get_agencias('inmo_vreasy', $this->agencia_id);


		if ( ! $agencias ) {
			ExportCommon::end( $this );
			return;
		}

		// Només hi ha una agència, però així aprofito la funció
		foreach ( $agencias as $rs ) {

			$this->agencia_rs =  &$rs;

			ExportCommon::p( "Restauració d'inmobles: " . $this->agencia_rs['comercialname'], 1 );
			$this->auth();

			foreach ( $this->property_ids as $property_id ) {
				$this->change_property_active_state( $property_id, 'activate' );

			}

		}


		ExportCommon::end( $this );
	}

	function on_save_bin() {
		$this->on_save_delete(false);
	}


	function on_save_delete($is_delete = true) {

		$this->set_on_vars();

		$agencias = ExportCommon::get_agencias('inmo_vreasy', $this->agencia_id);


		if ( ! $agencias ) {
			ExportCommon::end( $this );
			return;
		}

		// Només hi ha una agència, però així aprofito la funció
		foreach ( $agencias as $rs ) {

			$this->agencia_rs =  &$rs;

			ExportCommon::p( "Eliminació d'inmobles: " . $this->agencia_rs['comercialname'], 1 );
			$this->auth();

			foreach ( $this->property_ids as $property_id ) {
				$this->change_property_active_state( $property_id, 'deactivate' );

				// Si borro del tot, esborro la relació amb vreasy
				if ($is_delete) $this->delete_vreasy_property_id( $property_id );
			}

		}


		ExportCommon::end( $this );

	}

	function set_on_vars() {

		Db::connect_mother();
		$this->agencia_id = R::id( $this->agencia_id, false, false );
		R::escape_array( $this->property_ids );
	}

	// Iniciar, pot ser que només s'actualitzi un inmoble o tots amb is_global_export
	function _start( $is_global_export = true ) {

		$this->is_global_export = $is_global_export;

		Debug::add( date( "d-m-Y H:i" ), "" );

		set_time_limit( 60 * 60 );        //ini_set('implicit_flush', true);		//error_reporting(E_WARNING);

		echo '<link href="/admin/themes/inmotools/styles/export.css" rel="stylesheet" type="text/css">';

		Db::connect_mother();

		$agencias = ExportCommon::get_agencias( 'inmo_vreasy', $this->agencia_id );

		if ( ! $agencias ) {
			ExportCommon::end( $this );
			return;
		}

		$this->export_properties( $agencias );

		if ( $is_global_export ) {
			ExportCommon::show_end();
			ExportCommon::end( $this );
		}
	}

	private function get_agencia_config() {

		ExportCommon::get_agencia_config( $this->agencia_rs, 'vreasy' );

	}


	function export_properties( $agencias ) {

		foreach ( $agencias as $rs ) {

			$this->agencia_rs =  &$rs;
			// Només loguejo un cop
			$this->auth();

			$this->get_agencia_config();
			$condition = $this->get_agencia_condition();

			Debug::p( [
						[ 'Condition', $condition ],
					], 'Fotocasa');

			$this->client_images_url = $rs['url'] . '/' . $rs['client_dir'] . '/inmo/property/images/';
			if ($GLOBALS['gl_is_local']) $this->client_images_url = 'https://www.easybrava.com/' . $rs['client_dir'] . '/inmo/property/images/';

			$this->build_agencia_json( $condition );

		}
	}

	private function get_agencia_condition() {

		// Nomes s'exporten els immobles el primer cop que es posen a Vreasy per així obtindre la relació
		$condition = "(status = 'onsale')
				 AND bin = 0
				 AND country_id = '1'
				 AND property_id NOT IN ( 
				    SELECT inmo__property_to_vreasy.property_id 
				    FROM " . $this->agencia_rs['dbname'] . ".inmo__property_to_vreasy
			     )";

		if ( $this->property_ids ) {
			$condition .= " AND property_id IN (" . implode( ',', $this->property_ids ) . ")";
		}

		// TODO-i Activar vreasy_export_all
		// if ( ! $this->agencia_rs['vreasy_export_all'] ) {
			$condition .= "
				AND property_id IN (
					SELECT property_id
					FROM `" . $this->agencia_rs['dbname'] . "`.inmo__property_to_portal
					WHERE vreasy = 1
				)
			";
		// }

		ExportCommon::p( "Consulta immobles: " . $this->agencia_rs['comercialname'], 1 );

		return $condition;

	}

	private function build_agencia_json( $condition ) {

		ExportCommon::p( "Extracció d'inmobles: " . $this->agencia_rs['comercialname'], 1 );
		// TODO-i Activar export_all ExportCommon::p( "Exportar tots: " . ( $this->agencia_rs['vreasy_export_all'] ? 'Sí' : 'No' ), 3 );

		$this->agencia_exported_ids = [];
		$this->agencia_vreasy_exported_ids = [];

		$query   = "
			SELECT * FROM `" . $this->agencia_rs['dbname'] . "`.inmo__property
			WHERE " . $condition;
		$results = Db::get_rows( $query );

		ExportCommon::p( "Total immobles a exportar: " . count( $results ), 3 );
		$total_property_exported = 0;
		$language = LANGUAGE;


		foreach ( $results as $key => $rs ) {

			$title = $rs['property_private'];
			$property_id = $rs['property_id'];

			if ($title) $title .= ' - ';


			$title .= $rs['ref'];

			if ( ! $title ) {

				$query = "SELECT property FROM `" . $this->agencia_rs['dbname'] . "`.inmo__property_language
				 WHERE property_id = '$property_id' AND language = '$language'";

				$title = Db::get_first( $query );
			}

			if (!$title) $title = '-';

			$this->property_rs     = &$rs;
			$this->export_rs       = [
				"title"=>  $title,
			];

			// Categoria
			$category_id = $rs['category_id'];
			if ( isset( $this->vreasy_category[ $category_id ] ) ) {
				$this->export_rs['type_id'] = $this->vreasy_category[ $category_id ];
			}

			$total_property_exported ++;

			$this->config_rs = [];

			ExportCommon::p( $rs['ref'] . " , id: " . $property_id, 2 );

			$this->get_anunci();
			$this->export_property();
		}

		// Exporto les imatges de l'inmoble després d'haver exportat tots els immobles, així redueixo molt la probabilitat de editar una altre cop l'inmoble mentres aquest està guardant ja que les imatges tarden molt a guardar

		$this->property_rs = [];
		$this->export_rs   = [];

		foreach ( $results as $key => $rs ) {

			$this->property_rs     = &$rs;

			$this->put_images();
			$this->export_images();
		}

		ExportCommon::p( "Total immobles exportats: " . $total_property_exported, 3 );

		$this->property_rs = [];
		$this->export_rs   = [];

	}


	function export_property() {
		$property              = $this->export_rs;
		$property_id           = $this->property_rs['property_id'];
		$this->agencia_exported_ids [] = $property_id;


		$property_json = json_encode( $property, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES );

		$api_url = $this->api_url . '/properties';
		// Nou immoble
		$response = Unirest\Request::post( $api_url, $this->headers, $property_json );
		$this->log_json( $api_url, $property_json, $response );

		$code = $response->code;

		if ( $code >= 299 || $code < 200 ) {
			// ExportCommon::p( print_r( $response->body, true ), 'error' );
			$this->response_errors [] = [
				'error'       => 'No s\'ha pogut exportar inmoble',
				'code'        => $code,
				'body'        => print_r( $response->body, true ),
				'property_id' => $this->property_rs['property_id'],
			];
			return;
		}

		$vreasy_property_id = $response->body->id;
		$this->agencia_vreasy_exported_ids[$property_id] = $vreasy_property_id;

		// ExportCommon::p( print_r( $response->body, true ), 2 );
		$this->save_vreasy_property_id( $property_id, $vreasy_property_id );

		include_once( DOCUMENT_ROOT . 'admin/modules/admintotal/admintotal_vreasy_book.php' );
		$vreasy_book = New AdmintotalVreasyBook;
		$vreasy_book->agencia_rs =  $this->agencia_rs;
		$vreasy_book->db_name = $this->agencia_rs['dbname'];

		$vreasy_book->save_all_at_vreasy( $property_id, $vreasy_property_id );

		return;

	}


	function change_property_active_state( $property_id, $what ) {

		// Quan be de on_save_update, al guardar immobles nomes el primer cop, com que no faig update ( està fora de la consulta ), me'l desactiva, per tant comprova abans de desactivar si està o no per desactivar
		if ($what == 'check'){
			$db_name = $this->agencia_rs['dbname'];
			$query = "SELECT count(*)
				    FROM " . $this->agencia_rs['dbname'] . ".inmo__property_to_vreasy
					WHERE property_id = $property_id";
			$what = Db::get_first($query)?'activate':'deactivate';
		}

		if ($what == 'activate'){
			$what .= '?deactivated=true';
		}


		$vreasy_id = $this->get_vreasy_property_id($property_id);

		// Si no la tinc relacionada, no la borro
		if ( ! $vreasy_id ) {
			$this->response_errors [] = [
				'error'       => 'not_changed_active_state',
				'body' => 'No activat/desactivat ja que no estava a inmo__property_to_vreasy',
				'property_id' => $property_id,
			];
			return false;
		}



		$api_url = $this->api_url . "/properties/$vreasy_id/$what";
		$response = Unirest\Request::post( $api_url, $this->headers );
		$this->log_json( $api_url, $what, $response );

		$code = $response->code;

		if ( $code >= 299 || $code < 200 ) {
			// Si ja està activa i es vol activar, dona error code 500
			if ( $code != 500 ) {
				$this->response_errors [] = [
					'error'       => 'not_changed_active_state',
					'code'        => $code,
					'body'        => print_r( $response->body, true ),
					'property_id' => $property_id,
				];
			}
		}

		ExportCommon::p( "id: " . $property_id, 2 );
	}
	function delete_vreasy_property_id( $property_id ) {
		$db_name = $this->agencia_rs['dbname'];
		$query = "DELETE FROM $db_name.inmo__property_to_vreasy	WHERE property_id = $property_id";
		Db::execute( $query );
		$query = "UPDATE $db_name.`booking__book` SET `vreasy_id`='0' WHERE property_id = $property_id";
		Db::execute( $query );
	}


	function get_anunci() {

		$id    = $this->property_rs['property_id'];
		$query = "SELECT
				language, description, property FROM `" . $this->agencia_rs['dbname'] . "`.inmo__property_language
			 WHERE property_id = '" . $id . "'";

		$results = Db::get_rows( $query );

		if (!isset($this->export_rs ['portal_title'])) $this->export_rs ['portal_title'] = ['translations'=>[]];
		if (!isset($this->export_rs ['description'])) $this->export_rs ['description'] = ['translations'=>[]];


		foreach ( $results as $rs ) {
			$vreasy_language = isset( $this->vreasy_languages[ $rs['language'] ] ) ? $this->vreasy_languages[ $rs['language'] ] : false;
			if ( $vreasy_language !== false ) {

				$property    = $rs['property'];
				$description = $rs['description'];

				if ( $property ) {
					$this->export_rs ['portal_title']['translations'][] = [
						'language_id' => $vreasy_language,
						'content'  => $rs['property'],
					];
				}
				if ( $description ) {
					$this->export_rs ['description']['translations'][] = [
						'language_id' => $vreasy_language,
						'content'  => $rs['description'],
					];
				}
			}
		}

		// return true;
	}

	function put_images() {

		$id        = $this->property_rs['property_id'];
		$data_base = $this->agencia_rs['dbname'];

		// TODO-i Idioma poso castellà per defecte
		// TODO-i Exportar tots les imatges al acabar tot

		$query  = "SELECT
				`$data_base`.inmo__property_image.image_id as image_id, name, image_title, image_alt
				FROM `$data_base`.inmo__property_image, `$data_base`.inmo__property_image_language
				WHERE property_id = " . $id . "
				AND `$data_base`.inmo__property_image.image_id = `$data_base`.inmo__property_image_language.image_id
				AND language = 'spa'
				ORDER BY main_image DESC, ordre";
		$images = Db::get_rows( $query );

		if ( $images ) {
			$this->export_rs ['images'] = [];

			$ordre = 0;

			foreach ( $images as $image ) {

				$image_url = $this->client_images_url . $image['image_id'] . '_medium' . strrchr( substr( $image['name'], - 5, 5 ), '.' );

				$image_array = [
					'title'   => $image['image_alt'],
					'caption' => $image['image_title'],
					'url'     => $image_url,
					'order'   => $ordre,
				];
				$json        = json_encode( $image_array );

				$api_url  = $this->api_url . '/images';
				$response = Unirest\Request::post( $api_url, $this->headers, $json );

				$this->trace( print_r( $response, true ), 'Resposta put_images' );

				$image_array['url'] = $response->body->url;

				$this->export_rs ['images'][] = $image_array;

				$ordre ++;
			}

		}
	}
	private function export_images(){

		$property              = $this->export_rs;
		$property_id           = $this->property_rs['property_id'];
		$vreasy_property_id           = $this->agencia_vreasy_exported_ids[$property_id];

		if ( ! $vreasy_property_id ) return;

		$property_json = json_encode( $property, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES );
		$api_url = $this->api_url . "/properties/$vreasy_property_id";

		$response = Unirest\Request::put( $api_url, $this->headers, $property_json );
		$this->log_json( $api_url, $property_json, $response );

		$code = $response->code;

		if ( $code >= 299 || $code < 200 ) {
			// ExportCommon::p( print_r( $response->body, true ), 'error imatges' );
			$this->response_errors [] = [
				'error'       => 'Error posant imatges',
				'code'        => $code,
				'body'        => print_r( $response->body, true ),
				'property_id' => $property_id,
			];
		}
		return;

	}

	private function save_vreasy_property_id($property_id, $vreasy_id) {
		$db_name = $this->agencia_rs['dbname'];
		$query = "INSERT INTO $db_name.inmo__property_to_vreasy	
					(property_id, vreasy_id)
					VALUES
					($property_id, $vreasy_id)";
		Db::execute( $query );
	}

	private function get_vreasy_property_id($property_id) {
		$db_name = $this->agencia_rs['dbname'];
		$query = "SELECT vreasy_id FROM $db_name.inmo__property_to_vreasy 
					WHERE property_id = $property_id";
		return Db::get_first( $query );
	}


	/*
	 * Nomes per veure que hi ha publicat
	 * Ej. http://w2.easybrava/admin/?tool=admintotal&tool_section=vreasy&action=get_vreasy_properties&agencia_id=193
	 */
	public function get_vreasy_properties( $agencia_id = 0) {

		if (!$agencia_id)  {
			$agencia_id = R::id( 'agencia_id' );
		}
			$agencias = ExportCommon::get_agencias( 'inmo_vreasy', $agencia_id );
			foreach ( $agencias as $agencia_rs ) {

				$this->agencia_rs =  &$agencia_rs;
				$this->auth();
				$api_url = $this->api_url . "/properties";
				// $api_url = $this->api_url . "/listings?expand=association";
				$response = Unirest\Request::get( $api_url, $this->headers );

				ExportCommon::dump($response->body);
			}

			die();

	}
	/*
	 * Per veure a que està subscrit
	 */
	public function view_subscriptions() {

		if (empty($_SESSION['client_id'])) return;

		$agencia_id = $_SESSION['client_id'];

		$agencias = ExportCommon::get_agencias( 'inmo_vreasy', $agencia_id );

		$agencia_rs = current( $agencias );

		if (!$agencia_rs) die('no agencia');


		$this->agencia_rs =  &$agencia_rs;
		$this->auth();

		$get_url               = $this->api_url . '/subscriptions';

		$response = Unirest\Request::get( $get_url, $this->headers );
		ExportCommon::dump($response->body);

		die();
	}

	/**
	 * Només s'ha d'executar quan no hi ha cap subscripció, el primer cop i manualment
	 *
	 *
	 * Sense force_delete subscriurà a les noves subscripcions, sense borrar on ja està subscrit
	 */
	public function set_subscriptions() {

		global $gl_is_local;

		$agencia_id = R::id( 'agencia_id' );
		$force_delete = R::id( 'force_delete' );
		$current_subscriptions = [];

		if (!$agencia_id) die('No s\'ha fet cap subscripció');

		Db::connect_mother();
		$agencias = ExportCommon::get_agencias( 'inmo_vreasy', $agencia_id );

		foreach ( $agencias as $agencia_rs ) {

			$this->agencia_rs =  &$agencia_rs;
			$this->auth();

			$get_url               = $this->api_url . '/subscriptions';
			$current_response = Unirest\Request::get( $get_url, $this->headers );

			foreach ( $current_response->body as $rs ) {

				// Borro subscripcions

				if ($force_delete) {
					$subscription_delete_url = $this->api_url . "/subscriptions/" . $rs->id;
					$delete_response   = Unirest\Request::delete( $subscription_delete_url, $this->headers );
				}
				else {
					$current_subscriptions []= $rs->event;
				}

			}

			$domain = $agencia_rs['domain'];
			if ( strpos( $domain, 'www.' ) === false ) $domain = "www.$domain";

			if ( $gl_is_local ) {
				$domain = 'www.letnd.com';
			}

			// Subscripció a reservation_create

			$subscriptions = ['reservation_update', 'reservation_create'];
			$endpoint = "https://$domain/cat/export/vreasy/response/";


			if ( $gl_is_local ) {
				$endpoint .= '?is_local';
			}

			foreach ($subscriptions as $subscription){
				if ( ! in_array( $subscription, $current_subscriptions ) ) {

					$subscription_post = '
    					{
    					    "event": "' . $subscription . '",
    					    "protocol": "https",
    					    "endpoint": "' . $endpoint . '"
    					}';

					$subscription_url      = $this->api_url . "/listings/subscriptions";
					$subscribe_response = Unirest\Request::post( $subscription_url, $this->headers, $subscription_post );
				}
			}


			$current_response = Unirest\Request::get( $get_url, $this->headers );

			ExportCommon::dump($current_response->body);

		}

		die();

	}

	private function auth( $login = '', $pass = '' ) {
		if (!$login) $login = $this->agencia_rs['inmo_vreasy_user'];
		Unirest\Request::auth( $login, $pass );
	}

	private function log_json($api_url, $json_sent, $json_response) {
		ExportCommon::log_json($api_url, $json_sent, $json_response, $this->agencia_rs, $this->exporter);
	}

	// Per testejar nomes
	public function api_call(){

		$url = R::text_id( 'url' );

		$this->auth('yv08j0xpkysw8ogog884kwww8s40800w000g4');
		// $api_url      = $this->api_url . "/payment-types";
		$api_url      = $this->api_url . "/account";
		$api_url      = $this->api_url . "/keys?user_id=10881042";
		$api_url      = $this->api_url . "/properties/154969/deactivate";
		$api_url      = $this->api_url . "/reservations/?expand=payments";

		if ($url) $api_url      = $this->api_url . "/$url";


		$response = Unirest\Request::get( $api_url, $this->headers );
		ExportCommon::dump($response->body);

		foreach ( $response->body as $item ) {
			$id = $item->id;
			$name = $item->name;
			echo "
				'' => $id, // $name <br>
			";
		}

		die('Fet');
	}

	private function trace( $text, $name = '', $title = false ) {
		ExportCommon::trace( $text, $name, $title, 'vreasy_admin' );
	}
}