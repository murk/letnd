<?php
// menus eina

define('UTILITY_MENU_UTILITIES','Utilitaires');
define('UTILITY_MENU_UTILITIES_QUOTA','Calcul des frais');
define('UTILITY_MENU_UTILITIES_EXPENSE','Calcul des coûts');
define('UTILITY_MENU_UTILITIES_SAVE','épargne de impôts');
define('UTILITY_MENU_UTILITIES_CALCULATOR','Calculatrice');

$gl_caption_utilities['c_titlec']= "Calcul des frais pour les prêts hypothécaires.";
$gl_caption_utilities['c_price']= "Prix (".CURRENCY_NAME."):";
$gl_caption_utilities['c_deposit']= "Entrée (".CURRENCY_NAME."):";
$gl_caption_utilities['c_interest']= "Intérêt (%):";
$gl_caption_utilities['c_years']= "Ans:";
$gl_caption_utilities['c_cuota']= "Frais (".CURRENCY_NAME."):";
$gl_caption_utilities['c_notice']="Les informations obtenues dans le calcul de simulation est à titre indicatif et ne signifie pas un engagement ou une obligation juridique ou légal.";
$gl_caption_utilities['c_titles']= "Calcul de épargne de impôts.";
$gl_caption_utilities['c_import_prestec']= "Montant du prêt.";
$gl_caption_utilities['c_period_years']= "Remboursement:";
$gl_caption_utilities['c_coin']="Monnaie";
$gl_caption_utilities['c_eur']="Euros";
$gl_caption_utilities['c_pts']="Pesetas";
$gl_caption_utilities['c_workout']= "Déduction:";
$gl_caption_utilities['c_single']= "Individuel";
$gl_caption_utilities['c_couplegroup']= "Deux propriétaires. Commun:";
$gl_caption_utilities['c_couplesingle']= "Deux propriétaires. Individuel:";
$gl_caption_utilities['c_saveyear']= "Épargne la première année:";
$gl_caption_utilities['c_completesave']= "Épargne total:";
$gl_caption_utilities['c_info']="Les informations obtenues dans le calcul de simulation est à titre indicatif et ne signifie pas un engagement ou une obligation juridique ou légal.";

$gl_caption_utilities['c_calculator']= "Calculatrice";



define ('c_workout','Calculer');

/**
 * Nou
 *
 */

$gl_caption_buy['c_form_title']  = "Calcul des coûts de l'achat d'une propriété";
$gl_caption_sell['c_form_title'] = "Calcul des coûts de vente d'une propriété";

/* Compra */
$gl_caption['c_price']                     = "Prix de la propriété";
$gl_caption['c_notary']                    = "Frais de notaire";
$gl_caption['c_notary_percent']            = "%";
$gl_caption['c_register']                  = "Frais d'inscription";
$gl_caption['c_register_percent']          = "%";
$gl_caption['c_taxtype_id']                = "Impôts";
$gl_caption['c_agency']                    = "Frais de gestion";
$gl_caption['c_agency_percent']            = "%";
$gl_caption['c_mortgage']                  = "Hypothèque";
$gl_caption['c_mortgage_percent']          = "%";
$gl_caption['c_mortgage_open']             = "Ouverture";
$gl_caption['c_mortgage_open_percent']     = "%";
$gl_caption['c_mortgage_initial_interest'] = "Taux d'intérêt";
$gl_caption['c_mortgage_euribor']          = "Progression euribor";
$gl_caption['c_mortgage_start_date']       = "Date initiale";
$gl_caption['c_mortgage_length']           = "Durée";
$gl_caption['c_mortgage_years']            = "Ans";
$gl_caption['c_mortgage_register']         = "Inscription";
$gl_caption['c_mortgage_register_percent'] = "%";
$gl_caption['c_mortgage_notary']           = "Notaire";
$gl_caption['c_mortgage_notary_percent']   = "%";
$gl_caption['c_mortgage_valuation']        = "Taxes";

/* Reports compra */
$gl_caption['c_report_title']         = "Coûts d'achat";
$gl_caption['c_expense_not_financed'] = "Montant non financé";
$gl_caption['c_mortgage_title']       = "Hipothèque";
$gl_caption['c_down_payment']         = "Paiement accueil";
$gl_caption['c_tax']                  = "Impôts";
$gl_caption['c_total_expenses']       = "Frais";
$gl_caption['c_total']                = "Total";
$gl_caption['c_judicial_acts']        = "Documents juridiques";
$gl_caption['c_mortgage_title']       = "Hypothèque";
$gl_caption['c_no_vinculant']         = "Ces calculs sont approximatifs, et en aucun cas vincultants";
$gl_caption['c_monthly_payment']      = "Mensuel";

$gl_caption['c_judicial_acts_mortgage'] = "Documents juridiques sur la hypothèque";
$gl_caption['c_table_quote']            = "Aiement périodique";
$gl_caption['c_table_year']             = "Anné";
$gl_caption['c_table_payment']          = "aAmortissement";
$gl_caption['c_table_interest']         = "Intérêts";
$gl_caption['c_table_balance']          = "Capital restant";
$gl_caption['c_table_total_returned']   = "Capital amortisée";
$gl_caption['c_table_total_paid']       = "Total payé";

/* Venda */
$gl_caption_sell['c_price']           = "Prix de vente";
$gl_caption_sell['c_price_real']      = "Preu de vente net";
$gl_caption['c_price_bought']         = "Preu d'intérêts";
$gl_caption['c_price_bought_real']    = "Preu d'achat net";
$gl_caption['c_price_bought_updated'] = "Preu d'achat actualisée";
$gl_caption['c_earning']              = "Bénéfice";

$gl_caption['c_cadastral_price']           = "Valeur du cadastre";
$gl_caption['c_date_bought']               = "Jour d'achat";
$gl_caption['c_date_sold']                 = "Jour de vente";
$gl_caption['c_mortgage_cancel_percent']   = "%";
$gl_caption['c_other_expenses']            = "Autres coûts d'achat";
$gl_caption['c_mortgage_cancel']           = "Frais d'annulation";
$gl_caption['c_municipi']                  = "Ville";
$gl_caption['c_estate_commission']         = "Commission de l'agènce";
$gl_caption['c_estate_commission_percent'] = "%";
$gl_caption['c_habitability_card']         = "Certificat d'occupation";
$gl_caption['c_efficiency']                = "Certificat de performance énergétique";

/* Reports venda */
$gl_caption['c_report_title_sell']              = "Frais de vente";
$gl_caption['c_report_title_expenses']          = "Drais divers";
$gl_caption['c_report_title_expenses_top']      = "Frais";
$gl_caption['c_report_title_renda']             = "Impôt sur le revenu";
$gl_caption['c_report_title_renda_top']         = "Revenu";
$gl_caption['c_plusvalua']                      = "Gain";
$gl_caption['c_days']                           = "Journées";
$gl_caption['c_total_days']                     = "Total de jours";
$gl_caption['c_days_before_1996']               = "Jours jusqu'à 31/12/1996";
$gl_caption['c_days_before_2006']               = "Jours jusqu'à 19/01/2006";
$gl_caption['c_days_after_2006']                = "Jours de 19/01/2006 jusqu'à jour de vente";
$gl_caption['c_earning_before_2006']            = "Gain jusqu'à le 19/01/2006";
$gl_caption['c_earning_after_2006']             = "Gain de 19/01/2006 jusqu'à jour de vente";
$gl_caption['c_total_to_tribute']               = "Total à payer impôts";
$gl_caption['c_to_tribute_before_2006']         = "À payer impôts sur les bénéfices d'avant le 19/01/2006";
$gl_caption['c_earning_before_2006_reductible'] = "Montant réductible de bénéfices d'avant le 19/01/2006";
$gl_caption['c_to_tribute_before_2006_anyway']  = "À payer impôts sur les bénéfices d'avant 19/01/2006 pour dépasser les 400.000 € de vente";
$gl_caption['c_to_tribute_after_2006']          = "À payer impôts sur les bénefices d'après le 19/01/2006";
$gl_caption['c_coeficient_before_1996']         = "Réduction";
$gl_caption['c_tribute_summary']                = "Resum paiement";
$gl_caption['c_tribute_zone_1']                 = "Jusqu'à 6.000 € - 19%";
$gl_caption['c_tribute_zone_2']                 = "De 6.000 à 50.000 € - 21%";
$gl_caption['c_tribute_zone_3']                 = "Plus de 50.000 € - 23%";
$gl_caption['c_tribute']                        = "Total à payer";

?>