<?

/* 14-10-14 
  ACtualitzacions contacte, el telefon no pot ser nomes numeric

 */


if (is_table('contact__contact')) {
	$update_sql []="
		ALTER TABLE `contact__contact`
			CHANGE COLUMN `phone` `phone` VARCHAR(50) NOT NULL AFTER `comment`;";

}
else{	
	trigger_error('No existeix contact__contact');
}


if (is_table('booking__season') && !is_field('booking__season', 'color_id')) {
	$update_sql []="
		ALTER TABLE `booking__season`
			ADD COLUMN `color_id` INT(11) NULL DEFAULT '1' AFTER `ordre`;";

}
else{	
	if (is_field('booking__season', 'color')) {
		
		trigger_error('Ja hi ha la columna color a booking__season');
	}
}
?>