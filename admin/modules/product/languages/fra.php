<?php
// menus eina
if (!defined('PRODUCT_MENU_PRODUCT')){
define('PRODUCT_MENU_PRODUCT','Produits');
define('PRODUCT_MENU_NEW','Créer des produits');
define('PRODUCT_MENU_LIST','Parcourir les produits');
define('PRODUCT_MENU_LIST_BIN','Corbeille');
define('PRODUCT_MENU_FAMILY','Familles');
define('PRODUCT_MENU_FAMILY_NEW','Créer des familles');
define('PRODUCT_MENU_FAMILY_LIST','Parcourir les familles');
define('PRODUCT_MENU_FAMILY_LIST_BIN','Corbeille');
define('PRODUCT_MENU_BRAND','Marques');
define('PRODUCT_MENU_BRAND_NEW','Créer des marques');
define('PRODUCT_MENU_BRAND_LIST','Parcourir les marques');
define('PRODUCT_MENU_BRAND_LIST_BIN','Corbeille');
define('PRODUCT_MENU_TAX','Taxes');
define('PRODUCT_MENU_TAX_NEW','Créer tax');
define('PRODUCT_MENU_TAX_LIST','Parcourir les taxes');
define('PRODUCT_MENU_TAX_LIST_BIN','Corbeille');
//--------------- a traduir -----------------
define('PRODUCT_MENU_COLOR','Couleurs');
define('PRODUCT_MENU_COLOR_NEW','Créer couleur');
define('PRODUCT_MENU_COLOR_LIST','Parcourir les couleurs');
define('PRODUCT_MENU_COLOR_LIST_BIN','Corbeille');
define('PRODUCT_MENU_MATERIAL','Matériels');
define('PRODUCT_MENU_MATERIAL_NEW','Créer matériel');
define('PRODUCT_MENU_MATERIAL_LIST','Parcourir matériels');
define('PRODUCT_MENU_MATERIAL_LIST_BIN','Corbeille');
define('PRODUCT_MENU_VARIATION','Prix variations');
define('PRODUCT_MENU_VARIATION_NEW','Créer variation');
define('PRODUCT_MENU_VARIATION_LIST','Parcourir variations');
define('PRODUCT_MENU_VARIATION_DEFAULT','Default variations');
define('PRODUCT_MENU_VARIATION_CATEGORY','Variations catégories');
define('PRODUCT_MENU_VARIATION_CATEGORY_NEW','Create catégories');
define('PRODUCT_MENU_VARIATION_CATEGORY_LIST','Parcourir catégories');
define('PRODUCT_MENU_VARIATION_LIST_BIN','Corbeille');

define('PRODUCT_MENU_PROMCODE','Code promo');
define('PRODUCT_MENU_PROMCODE_NEW','Créer code');
define('PRODUCT_MENU_PROMCODE_LIST','Parcourir code');
define('PRODUCT_MENU_PROMCODE_LIST_BIN','Corbeille');

define('PRODUCT_MENU_SPEC','Spécifications');
define('PRODUCT_MENU_SPEC_NEW','Créer pécification');
define('PRODUCT_MENU_SPEC_LIST','Parcourir spécifications');
define('PRODUCT_MENU_SPEC_CATEGORY','Catégories spécifications');
define('PRODUCT_MENU_SPEC_CATEGORY_NEW','Créer catégorie');
define('PRODUCT_MENU_SPEC_CATEGORY_LIST','Parcourir catégories');
define('PRODUCT_MENU_SPEC_LIST_BIN','Corbeille');

define('PRODUCT_MENU_QUANTITYCONCEPT','Concepts quantity');
define('PRODUCT_MENU_QUANTITYCONCEPT_NEW','New concept');
define('PRODUCT_MENU_QUANTITYCONCEPT_LIST','List concepts');
}



$gl_caption['c_images_variation_title']='Variation';
$gl_caption_color['c_color_name']='Nom couleur';
$gl_caption['c_color_id']='Couleur';
$gl_caption['c_material_id']='matériel';
$gl_caption['c_choose']='Choisir';
$gl_caption_product['c_temperature_unit']='';
$gl_caption_product['c_util_life_unit']='';
$gl_caption_product['c_others1']='';
$gl_caption_product['c_others2']='';
$gl_caption_product['c_others3']='';
$gl_caption_product['c_others4']='';
$gl_caption_product['c_auto_ref']='Automatique';

$gl_caption_product['c_format']='Format';
$gl_caption_product['c_weight']='Masse';
$gl_caption_product['c_gram']='grammes';
$gl_caption_product['c_kilo']='kgs';
$gl_caption_product['c_weight_unit']='';
$gl_caption_product['c_height_unit']='';
$gl_caption_product['c_height']='Hauteur';
$gl_caption_product['c_mm']='mm';
$gl_caption_product['c_cm']='cm';
$gl_caption_product['c_mt']='mt';
$gl_caption_product['c_km']='km';
$gl_caption_product['c_feet']='pieds';
$gl_caption_product['c_temperature']='Température';
$gl_caption_product['c_celcius']='celcius';
$gl_caption_product['c_fahrenheit']='fahrenheit';
$gl_caption_product['c_util_life']='Durée de vie';
$gl_caption_product['c_hours']='heures';
$gl_caption_product['c_days']='jours';
$gl_caption_product['c_years']='ans';
$gl_caption_product['c_meters']='mètres';
$gl_caption_product['c_km']='kilomètres';
$gl_caption_product['c_months']='mois';
$gl_caption_product['c_caracteristics']='Caractérístiques';
$gl_caption_product['c_other_caracteristics']='Autres Caractérístiques ( configuration )';
$gl_caption_product['c_power']='Puissance';
$gl_caption_product['c_alimentacion']='Nourriture';
$gl_caption_product['c_certification']='Certificat / s';
$gl_caption_material['c_material']='Nom du matérial';


//-----------------------------------
$gl_caption['c_ref'] = 'Ref';
$gl_caption_product['c_home']='Sélection';
$gl_caption_product['c_ref_number'] = 'Réf. automatique';
$gl_caption_product['c_ref_supplier'] = 'Réf. de fournisseur';
$gl_caption_product['c_product_private'] = 'Nom privée';
$gl_caption['c_family_id'] = 'Famille';
$gl_caption_product['c_subfamily_id'] = 'Sous-famille';
$gl_caption['c_family_content_list'] = 'Brève description dans la liste';
$gl_caption['c_family_description']='Description de la famille';
$gl_caption_product['c_subfamily_id_format']='#%s#%s';
$gl_caption_product['c_subsubfamily_id'] = 'Sous-famille';
$gl_caption_product['c_subsubfamily_id_format']='#%s#%s';
$gl_caption_product['c_pvp'] = 'PVP';
$gl_caption_product['c_price_cost'] = 'Prix de coût';
$gl_caption['c_price'] = 'Prix';
$gl_caption_product['c_destacat']='Sélection';
$gl_caption_product['c_pvd'] = 'PVD';
$gl_caption_product['c_pvd1'] = 'Tarif 1';
$gl_caption_product['c_pvd2'] = 'Tarif 2';
$gl_caption_product['c_pvd3'] = 'Tarif 3';
$gl_caption_product['c_pvd4'] = 'Tarif 4';
$gl_caption_product['c_pvd5'] = 'Tarif 5';
$gl_caption_product['c_price_consult'] = 'sur demande';
$gl_caption_product['c_price_consult_1'] = 'Prix à demandé';
$gl_caption_product['c_brand_id'] = $gl_caption_product['c_brand'] = 'Marque';
$gl_caption_product['c_sell'] = 'Web vente?';
$gl_caption['c_guarantee']='Garantie';
$gl_caption_product['c_guarantee_period']='Période de garantie';
$gl_caption_product['c_da']='jours';
$gl_caption_product['c_mo']='mois';
$gl_caption_product['c_yr']='ans';
$gl_caption['c_status'] = 'Etat';
$gl_caption_product['c_free_send'] = 'Autoriser la livraison gratuite';
$gl_caption_product['c_ordre'] = 'Ordre';
$gl_caption['c_product_title'] = 'Nom du produit';
$gl_caption['c_product_subtitle'] = 'Subtitle';
$gl_caption_product['c_product_description'] = 'Description du produit';
$gl_caption['c_prepare'] = 'En préparation';
$gl_caption['c_review'] = 'Pour revoir';
$gl_caption['c_onsale'] = 'À Vendre';
$gl_caption['c_nostock'] = 'No stock';
$gl_caption['c_nocatalog'] = 'Hors catalogue';
$gl_caption['c_archived'] = 'Filed';
$gl_caption['c_tax_id'] = 'T.V.A (%)';
$gl_caption['c_is_tax_included'] = 'T.V.A compris';
$gl_caption['c_tax_not_included'] = 'T.V.A non compris';
$gl_caption_product['c_offer'] = 'Offre';
$gl_caption_product['c_discount'] = 'Remise (%)';
$gl_caption_product['c_discount_wholesaler'] = 'Détaillant d\'actualisation (%)';
$gl_caption_product['c_margin'] = 'Marge (%)';
$gl_caption_product['c_ean'] = 'EAN';
$gl_caption_product['c_price_unit'] = 'Prix by';
$gl_caption_product['c_unit'] = 'Unité';
$gl_caption_product['c_ten'] = 'Dizaine';
$gl_caption_product['c_hundred'] = 'Cent';
$gl_caption_product['c_thousand'] = 'Millar';
$gl_caption_product['c_couple'] = 'Couple';
$gl_caption_product['c_dozen'] = 'Dozen';
$gl_caption_product['c_m2'] = 'm2';
$gl_caption_product['c_data']='Détails sur le produit';
$gl_caption_product['c_general_data']='Information général';
$gl_caption_product['c_descriptions']='Descriptions';
$gl_caption_product['c_description']='Description';
$gl_caption_product['c_data_product']='Détails sur le produit';
$gl_caption_product['c_data_web']='Ventes Web';
$gl_caption_product['c_stock']='Stock';
$gl_caption_product['c_sells']='Unités vendues';
$gl_caption_product['c_file']='Archives publiques';
$gl_caption_product['c_discount_fixed_price']='Prix ​​discount fixé';
$gl_caption_product['c_discount_fixed_price_wholesaler']='Forfait réduction détaillant';
$gl_caption_product['c_selected']='Produits sélectionnés';
$gl_caption_product['c_videoframe']='Video (YouTube, Metacafe ...)';


$gl_caption['c_observations'] = 'Observations';
$gl_caption['c_check_parent_id']='- Pas de famille -';
$gl_caption['c_check_brand_id']='Choisissez votre marque';




$gl_caption['c_brand_name']='Nom du marque';
$gl_caption_family['c_parent_id']='Famille à laquelle il appartient';
$gl_caption_family['c_family']='Nom de la famille';
$gl_caption_family['c_family_alt']='Famille "title / alt"';
$gl_caption_family['c_family_subtitle'] = 'Sous-titre';
$gl_caption_family['c_view_subfamilys']='voir sous-familles';
$gl_caption_family['c_add_subfamily_button']=$gl_caption_family['c_add_subfamily']='Ajouter sous-famille';
$gl_caption_family['c_filter_parent_id'] = 'Appartient à quelconque família';
$gl_caption_family['c_search_title'] = 'Recherche familles';




$gl_caption_tax['c_tax_name']='Nom de l\'impôt';
$gl_caption_tax['c_tax_value']='Valeur (%)';
$gl_caption_tax['c_equivalencia']='Equivalencia (%)';




$gl_caption_product['c_search_title']= 'Recherche de produits';
$gl_caption_product['c_by_nif']= 'Par Ref.';
$gl_caption_product['c_by_words']= 'Pour mots.';


$gl_caption['c_observations'] = 'Observations';
$gl_caption['c_ordre']='Ordre';
$gl_caption['c_withinstall']='Pvp avec l\'installation';

$gl_caption ['c_noproduct']= $gl_messages_product ['no_records']= 'Actuellement pas de produits pour cette catégorie.';
$gl_caption_product['c_model']='Modèle';
$gl_caption['c_year']='Année';
$gl_caption['c_ishandmade']='Fet à la main';



// No traduit
if(!defined( 'CUSTOMER_MENU_CUSTOMER' )){
	define( 'CUSTOMER_MENU_CUSTOMER', 'Clients' );
	define( 'PRODUCT_MENU_CUSTOMER', 'Clients' );
	define( 'CUSTOMER_MENU_CUSTOMER_NEW', 'Insérer client' );
	define( 'CUSTOMER_MENU_CUSTOMER_LIST', 'Parcourir clients' );
	define( 'CUSTOMER_MENU_CUSTOMER_BIN', 'Corbeille' );

	define( 'CUSTOMER_MENU_ORDER', 'Ordres' );
	define( 'PRODUCT_MENU_ORDER', 'Ordres' );
	define( 'CUSTOMER_MENU_ORDER_NEW', 'Nouvelle ordre' );
	define( 'CUSTOMER_MENU_ORDER_LIST', 'Parcourir ordres' );
	define( 'CUSTOMER_MENU_ORDER_LIST_BIN', 'Corbeille' );

	define( 'CUSTOMER_MENU_WHOLESALER', 'Distributeurs' );
	define( 'PRODUCT_MENU_WHOLESALER', 'Distributeurs' );
	define( 'CUSTOMER_MENU_WHOLESALER_NEW', 'Insérer distributeur' );
	define( 'CUSTOMER_MENU_WHOLESALER_LIST', 'Parcourir distributeurs' );
	define( 'CUSTOMER_MENU_WHOLESALER_BIN', 'Corbeille' );

	define( 'CUSTOMER_MENU_RATE', 'Frais de port' );
	define( 'PRODUCT_MENU_RATE', 'Frais de port' );
	define( 'CUSTOMER_MENU_RATE_NEW', 'Ajouter pays' );
	define( 'CUSTOMER_MENU_RATE_PROVINCIA_NEW', 'Ajouter province' );
	define( 'CUSTOMER_MENU_RATE_LIST', 'Parcourir frais' );
	define( 'PRODUCT_MENU_COUNTRY', 'Selection pays' );
	define( 'CUSTOMER_MENU_COUNTRY_LIST', 'Selection pays' );

	define( 'CUSTOMER_MENU_COURIER', 'Messager' );
	define( 'PRODUCT_MENU_COURIER', 'Messager' );
	define( 'CUSTOMER_MENU_COURIER_NEW', 'Créer messager' );
	define( 'CUSTOMER_MENU_COURIER_LIST', 'Parcourir messagers' );
	define( 'CUSTOMER_MENU_COURIER_LIST_BIN', 'Corbeille' );
}

$gl_caption ['c_quantity']='Montant';
$gl_caption ['c_product_title']='Produit';
$gl_caption ['c_product_tax']='T.V.A'; // això ha d'anar a la taula de productes
$gl_caption ['c_product_basetax']='Prix/unité';
$gl_caption ['c_product_base']='Prix​​/unité';
$gl_caption ['c_product_total_base']='Totale';
$gl_caption ['c_product_total_tax']='T.V.A'; // això ha d'anar a la taula de productes
$gl_caption ['c_product_total_basetax']='Totale';
$gl_caption ['c_total']='Totale';
$gl_caption ['c_total_base']='Total partiel';
$gl_caption ['c_total_tax']='T.V.A'; // això ha d'anar a la taula de productes
$gl_caption ['c_total_equivalencia']='Méthode de charge';
$gl_caption ['c_total_basetax']='Totale';
$gl_caption ['c_rate_basetax']='Frais d\'expédition';//aixo hem sembla que està molt mal traduit (revisar)
$gl_caption ['c_rate_base']='';
$gl_caption ['c_rate_tax']='';
$gl_caption ['c_deposit']='Montant';
$gl_caption ['c_deposit_rest']='Montant restant';
$gl_caption ['c_all_basetax']='Totale';
$gl_caption ['c_all_base'] = 'Assiette fiscale';
$gl_caption ['c_all_tax']='T.V.A';
$gl_caption ['c_all_equivalencia']='Taxe d\'équivalence';
$gl_caption ['c_promcode_basetax']='Code coupon de réduction';
$gl_caption['c_product_old_base'] = "";
$gl_caption['c_product_old_basetax'] = "";
$gl_caption['c_product_discount'] = "Réduction";
$gl_caption['c_product_discount_fixed'] = "";

$gl_caption_customer['c_prefered_language'] = 'Langue';
$gl_caption_customer['c_form_title'] = 'Données des clients';
$gl_caption_customer['c_customer_id'] = 'Nom.';
$gl_caption_customer['c_customer_id_title'] = 'Nombre';
$gl_caption_customer['c_treatment'] = 'Infliger';
$gl_caption_customer['c_name'] = 'Prénom';
$gl_caption_customer['c_company']='Société';
$gl_caption_customer['c_surname'] = 'Nom';
$gl_caption_customer['c_birthdate'] = 'Date de naissance';
$gl_caption_customer['c_mail'] = "E-mail";
$gl_caption_customer['c_password'] = 'Mot de passe';
$gl_caption_customer['c_password_repeat'] = 'Répéter mot de passe';
$gl_caption['c_telephone'] = 'Téléphone';
$gl_caption_customer['c_telephone_mobile'] = 'Téléphone mobile';
$gl_caption['c_has_equivalencia'] = 'Charge d\'equivalencia';
$gl_caption['c_has_no_vat'] = 'Ne pas ajouter TVA';
$gl_caption_customer['c_wholesaler_rate'] = 'Tarif';
$gl_caption_customer['c_wholesaler_rate_1'] = 'Tarif 1';
$gl_caption_customer['c_wholesaler_rate_2'] = 'Tarif 2';
$gl_caption_customer['c_wholesaler_rate_3'] = 'Tarif 3';
$gl_caption_customer['c_wholesaler_rate_4'] = 'Tarif 4';
$gl_caption_customer['c_wholesaler_rate_5'] = 'Tarif 5';
$gl_caption['c_dni'] = 'CIF / NIF';
$gl_caption_customer['c_comment'] = 'Commentaires';
$gl_caption_customer['c_bin'] = '';
$gl_caption_customer['c_enteredc'] = "Date d'entréé";/* mal traduit?*/
$gl_caption['c_treatment_sra'] = "Mme.";
$gl_caption['c_treatment_sr'] = "M.";
$gl_caption ['c_same_address']='La même adresse de facturation';
$gl_caption_customer['c_list_orders'] = 'Voir les commandes';
$gl_caption_customer ['c_form_title2']='Logins';
$gl_caption_customer ['c_form_title3']='Donées de facturation';
$gl_caption_customer ['c_form_title3_list']='Facturation';
$gl_caption_customer ['c_form_title_wholesaler']='Concessionnaire';
$gl_caption_customer ['c_form_title4']='Adresse de livraison';
$gl_caption_customer['c_add_button'] = 'Ajouter adresse';

$gl_caption['c_order_id'] = $gl_caption['c_order'] = 'Commande';
$gl_caption_order['c_customer_id'] = 'Client';
$gl_caption_order['c_total_base'] = 'Total partiel';
$gl_caption_order['c_total_tax'] = 'T.V.A.';
$gl_caption_order['c_total_basetax'] = 'Totale';
$gl_caption_order['c_entered_order'] = 'Date de commande';
$gl_caption_order['c_entered_order_short'] = 'Date';
$gl_caption_order['c_customer_id'] = 'Client';
$gl_caption_order['c_product_id'] = 'Produit';
$gl_caption_order['c_shipped'] = 'Date d\'affichage';
$gl_caption_order['c_delivered'] = 'Date de livraison';
$gl_caption_order['c_order_status'] = 'État';
$gl_caption_order['c_deposit_payment_method'] = 'Paiement méthode deposit';
$gl_caption_order_list['c_deposit_payment_method'] = 'Paiement m. deposit';
$gl_caption_order['c_payment_method'] = 'Paiement méthode';
$gl_caption_order_list['c_deposit_payment_method'] = 'Paiement m.';
$gl_caption_order['c_comment'] = 'Commentaires privé';
$gl_caption['c_customer_comment'] = 'Commentaires client';
$gl_caption['c_is_gift_card'] = 'Chèque Cadeau';
$gl_caption_order['c_address_invoice_id'] = '';
$gl_caption_order['c_filter_order_status'] = 'Tous les états';
$gl_caption_order['c_filter_payment_method'] = 'Toutes les méthodes';
$gl_caption_order['c_customer_id_format'] = '%s %s';
$gl_caption_order['c_file'] = 'Arxius públics';
$gl_caption_order['c_button-edit-admin'] = '';


$gl_caption_order['c_order_status_paying'] = 'Attendant paiement';
$gl_caption_order['c_order_status_failed'] = 'Paiement échoué';
$gl_caption_order['c_deposit_payed'] = 'Deposit payé';
$gl_caption_order['c_order_status_pending'] = 'Paiement manquant d\'ètre acceptée';
$gl_caption_order['c_order_status_denied'] = 'Paiement refusé';
$gl_caption_order['c_order_status_voided'] = 'Paiement annulé';
$gl_caption_order['c_order_status_refunded'] = 'paiement retourné';
$gl_caption_order['c_order_status_completed'] = 'Payé';
$gl_caption_order['c_order_status_preparing'] = 'En préparation';
$gl_caption_order['c_order_status_shipped'] = 'Soumis';
$gl_caption_order['c_order_status_delivered'] = 'Livré';
$gl_caption_order['c_order_status_collect'] = 'En cours d\'aller chercher';

$gl_caption_order['c_deposit_status'] = 'État deposit';

$gl_caption_order['c_deposit_status_paying'] = 'Attendant paiement';
$gl_caption_order['c_deposit_status_failed'] = 'Paiement échoué';
$gl_caption_order['c_deposit_status_deposit'] = 'Deposit payé';
$gl_caption_order['c_deposit_status_pending'] = 'Paiement manquant d\'ètre acceptée';
$gl_caption_order['c_deposit_status_denied'] = 'Paiement refusé';
$gl_caption_order['c_deposit_status_voided'] = 'Paiement annulé';
$gl_caption_order['c_deposit_status_refunded'] = 'paiement retourné';
$gl_caption_order['c_deposit_status_completed'] = 'Payé';

$gl_caption_order['c_deposit_rest'] = 'Rest';

// Enviament
$gl_caption['c_send_date'] = "Livraison";
$gl_caption['c_send_date_shipped'] = $gl_caption_order['c_shipped'];
$gl_caption['c_send_date_delivered'] = $gl_caption_order['c_delivered'];
$gl_caption['c_send_date_no_date_shipped'] = 'No shipped date';
$gl_caption['c_send_date_no_date_delivered'] = 'No delivered date';

$gl_caption['c_payment_method_none'] = 'Aucun';
$gl_caption['c_payment_method_paypal'] = 'Paypal';
$gl_caption['c_payment_method_account'] = 'Virement bancaire';
$gl_caption['c_payment_method_ondelivery'] = 'COD';
$gl_caption['c_payment_method_directdebit'] = 'Reçu de la Banque';
$gl_caption['c_payment_method_4b'] = 'Passerelle 4B';
$gl_caption['c_payment_method_lacaixa'] = 'Carte de crédit / paiement';
$gl_caption['c_payment_method_cash'] = 'Espèces';
$gl_caption['c_payment_method_physicaltpv'] = 'TPV physique';

$gl_caption['c_deposit_payment_method_none'] = 'Aucun';
$gl_caption['c_deposit_payment_method_paypal'] = $gl_caption['c_payment_method_paypal'];
$gl_caption['c_deposit_payment_method_account'] = $gl_caption['c_payment_method_account'];
$gl_caption['c_deposit_payment_method_ondelivery'] = $gl_caption['c_payment_method_ondelivery'];
$gl_caption['c_deposit_payment_method_directdebit'] = $gl_caption['c_payment_method_directdebit'];
$gl_caption['c_deposit_payment_method_4b'] = $gl_caption['c_payment_method_4b'];
$gl_caption['c_deposit_payment_method_lacaixa'] = $gl_caption['c_payment_method_lacaixa'];
$gl_caption['c_deposit_payment_method_cash'] = $gl_caption['c_payment_method_cash'];
$gl_caption['c_deposit_payment_method_physicaltpv'] = $gl_caption['c_payment_method_physicaltpv'];


$gl_caption['c_collect'] = 'Collecte à boutique physique';
$gl_caption_list['c_collect'] = 'Collecte';
$gl_caption_order['c_edit_button'] = 'Modifier commande';
$gl_caption_order['c_show_record'] = 'Voir la commande';
$gl_caption_order['c_show_invoice'] = 'Voir facture';
$gl_caption_order['c_show_invoice_refund'] = 'Voir facture remboursement';
$gl_caption_order['c_pay_now'] = 'Payer';
$gl_caption_order['c_no_shipped'] = 'Non envoyé';
$gl_caption_order['c_no_delivered'] = 'Non livré';


$gl_caption_order ['c_order_title_admin']='Données de la comande';
$gl_caption_order ['c_order_title']='Information de la commande';
$gl_caption_order ['c_order_title2']='Produits';
$gl_caption_order ['c_order_title3']='Données de facturation';
$gl_caption_order ['c_order_title4']='Adresse de livraison';
$gl_caption_order ['c_order_title5']='Paiement';
$gl_caption_order ['c_order_title_invoice_customer']='Données du client';
$gl_caption_order ['c_order_num']='num. ';
$gl_caption ['c_product_variation_ids']='';
$gl_caption ['c_product_variations']='Variations';
$gl_caption ['c_product_variation_categorys']='';

$gl_caption_order['c_invoice_id'] = 'Facture';
$gl_caption_order['c_invoice_refund_id'] = 'Facture remboursement';
$gl_caption_order['c_invoice_date'] = 'Date';
$gl_caption_order['c_invoice_refund_date'] = 'Date';
$gl_caption_order['c_view_invoice'] = 'Voire facture';
$gl_caption_order['c_view_invoice_refund'] = 'Voire facture remboursement';

$gl_caption_orderitem = $gl_caption_order;
$gl_caption_orderitem_list = $gl_caption_order_list;

$gl_caption ['c_address_id']='';
$gl_caption ['c_delivery_id']='';
$gl_caption_address ['c_customer_id']='';
$gl_caption_address['c_a_name'] = 'Prénom';
$gl_caption_address['c_a_surname'] = 'Nom';
$gl_caption_address['c_address'] = 'Adresse';
$gl_caption_address['c_zip'] = 'Code';
$gl_caption_address['c_city'] = 'Ville';
$gl_caption_address['c_country_id'] = 'Pays';
$gl_caption_address['c_invoice'] = '';
$gl_caption_address['c_edit_address'] = 'Modifier adresse';
$gl_caption_address['c_new_address'] = 'Ajouter adresse';

$gl_caption_variation['c_variation'] = 'Variation';
$gl_caption_variation['c_variation_subtitle'] = 'Subtitle';
$gl_caption_variation['c_variation_description'] = 'Description';
$gl_caption_variation['c_list_pick_default'] = 'Par défaut.';
$gl_caption_variation['c_variation_category_id'] = 'Catégorie';
$gl_caption_variation['c_variation_status'] = 'Status';
$gl_caption['c_disabled'] = 'Disabled';
$gl_caption_variation['c_variation_pvp'] = 'Augmentation PVP';
$gl_caption_variation['c_variation_pvd'] = 'Augmentation PVD 1';
$gl_caption_variation['c_variation_pvd2'] = 'Augmentation PVD 2';
$gl_caption_variation['c_variation_pvd3'] = 'Augmentation PVD 3';
$gl_caption_variation['c_variation_pvd4'] = 'Augmentation PVD 4';
$gl_caption_variation['c_variation_pvd5'] = 'Augmentation PVD 5';
$gl_caption_variation_category['c_variation_category'] = 'Catégorie';
$gl_caption_variation_category['c_variation_description'] = 'Description';
$gl_caption_product['c_edit_variations'] = $gl_caption_product['c_edit_variations_button'] = 'Variations';
$gl_caption_product['c_edit_specs'] = $gl_caption_product['c_edit_specs_button'] = 'Specs';
$gl_caption_product['c_edit_relateds'] = $gl_caption_product['c_edit_relateds_button'] = 'Related';
$gl_caption['c_no_assigned'] = 'Pas de variation définie';
$gl_caption_product['c_data_variations'] = 'Variations de produit';
$gl_caption_product['c_price_base'] = 'Prix ​​de base du produit';
$gl_caption['c_data_digital'] = 'Produit digital';
$gl_caption_product['c_download'] = 'Fichiers téléchargeables';
$gl_caption_product['c_filter_family_id'] = 'Toutes les familles';
$gl_caption_product['c_filter_subfamily_id'] = 'Tous les sous-familles';
$gl_caption_product['c_filter_subsubfamily_id'] = 'Tous les sous-familles';
$gl_caption_product['c_filter_status'] = 'Tous les États';
$gl_caption_product['c_filter_destacat'] = 'Marqué / Non marqué';
$gl_caption_product['c_destacat_0'] = 'Non marqué';
$gl_caption_product['c_destacat_1'] = 'Marqué';
$gl_caption_product['c_filter_prominent'] = 'En Promotion / Pas de promotion';
$gl_caption_product['c_prominent_0'] = 'Pas de promotion';
$gl_caption_product['c_prominent_1'] = 'En Promotion';
$gl_caption_product['c_stockcontrol']='Contrôle des stocks';
$gl_caption_product['c_last_stock_units_public']='Alerte public dernières unités';
$gl_caption_product['c_last_stock_units_admin']='Alerte admin. dernières unités';
$gl_caption_brand['c_brand_description']='Description';
$gl_caption['c_prominent']='En Promotion';
$gl_caption_list['c_prominent']='Prom.';
$gl_caption_product['c_is_new_product']='Nouveau produit';
$gl_caption_product_list['c_is_new_product']='Nouveau';
$gl_caption_variation['c_variation_stock']='Stock';
$gl_caption_variation['c_variation_last_stock_units_public']='Alerte public';
$gl_caption_variation['c_variation_last_stock_units_admin']='Alerte admin';
$gl_caption_variation['c_variation_ref']='Ref';
$gl_caption_variation['c_variation_ref_number']='Réf. automatique';
$gl_caption_variation['c_variation_ref_supplier']='Ref. de fournisseur';

$gl_caption['c_product_file_name'] = $GLOBALS['gl_caption']['c_file_name'];
$gl_caption['c_product_old_file_name'] = $GLOBALS['gl_caption']['c_old_file_name'];

$gl_caption_variation['c_variation_sells']='Unités vendues';
$gl_caption['c_add_variations']='Ajouter variations';
$gl_caption_variation['c_selected']='Variations pour ajouter';
$gl_caption['c_new_variations']='Variations ajoutés';
$gl_caption['c_variation_category_select']='Toutes les catégories';
$gl_caption_variation['c_filter_variation_category_id']='Toutes les catégories';

$gl_caption['c_send_password'] = 'Envoyer un mot de passe par email';
$gl_caption['c_password_sent'] = 'Le mot de passe a été envoyé au distributeur';
$gl_caption['c_password_mail_subject'] = 'Votre mot de passe:';
$gl_caption['c_password_mail_text_1'] = 'Votre mot de passe pour accéder à notre boutique est';
$gl_caption['c_password_mail_text_2'] = 'Vous pouvez modifier le mot de passe dans la page «Votre compte» sur notre site Internet';
$gl_messages_customer['password_sent'] = 'Mot de passe a été envoyé au client';
$gl_messages_customer['password_not_sent'] = 'Échec de l\'envoi de votre mot de passe, s\'il vous plaît cliquer sur "envoyer le mot de passe par e-mail""';

$gl_caption_customer['c_activate_button'] = 'Activer client';
$gl_caption_customer['c_deactivate_button'] = 'Désactiver client';
$gl_caption_customer['c_set_wholesaler_button'] = 'Assurez-distributeur';
$gl_caption_customer['c_unset_wholesaler_button'] = 'Retirer distributeur';
$gl_caption_customer['c_activated_mail_message'] = 'Votre compte %s a été activé, vous pouvez désormais accéder à notre magasin.';
$gl_caption_customer['c_activated_mail_subject'] = 'Votre compte a été activé';
$gl_caption_customer['c_filter_activated'] = 'Activé / Désactivé';
$gl_caption_customer['c_activated_0'] = 'Désactivé';
$gl_caption_customer['c_activated_1'] = 'Activé';

$gl_messages_customer['activated'] = 'Le client a été activé';
$gl_messages_customer['deactivated'] = 'Le client a été désactivé';
$gl_messages_customer['not_activated'] = 'Impossible d\'activer le client, s\'il vous plaît essayer à nouveau';
$gl_messages_customer['set_wholesaler'] = 'Le client est désormais concessionnaire';
$gl_messages_customer['unset_wholesaler'] = 'Le client n\'est plus concessionnaire';

$gl_caption_family['c_destacat']='Marqué';

$gl_caption['c_promcode'] = 'Réduction pour coupon';
$gl_caption_promcode['c_name'] = 'Nom';
$gl_caption_promcode['c_discount_pvp'] = 'Remise des prix';
$gl_caption_promcode['c_discount_pvd'] = 'Rabais tarif 1';
$gl_caption_promcode['c_discount_pvd2'] = 'Rabais tarif 2';
$gl_caption_promcode['c_discount_pvd3'] = 'Rabais tarif 3';
$gl_caption_promcode['c_discount_pvd4'] = 'Rabais tarif 4';
$gl_caption_promcode['c_discount_pvd5'] = 'Rabais tarif 5';
$gl_caption_promcode['c_discount_type'] = 'Sorte d\'actualisation';
$gl_caption_promcode['c_discount_type_fixed'] = 'Montant fixe';
$gl_caption_promcode['c_discount_type_percent'] = 'Pourcentage';
$gl_caption_promcode['c_minimum_amount'] = 'Dépenses minimales';
$gl_caption_promcode['c_start_date'] = 'Date de début';
$gl_caption_promcode['c_end_date'] = 'Date limite';
$gl_caption['c_promcode_discount'] = 'Réduction pour coupon';

$gl_caption['c_sector']='Secteur';
$gl_caption['c_sector_id']='Secteur';
$gl_caption_sector['c_sector_description']='Description';


$gl_caption_brand['c_send_rate_spain']='Espagne';
$gl_caption_brand['c_send_rate_france']='France';
$gl_caption_brand['c_send_rate_andorra']='Andorra';
$gl_caption_brand['c_send_rate_world']='Autres pays';
$gl_caption['c_minimum_buy_free_send']='Achat minime';

// tarifes
$gl_caption['c_country_id'] = $gl_caption['c_country'] = 'Pays';
$gl_caption['c_provincia_id'] = $gl_caption['c_provincia'] = 'Province';
$gl_caption_rate['c_rate'] = 'Frais du port';
$gl_caption_rate['c_send_rate_world']='Autres pays';
$gl_caption_rate['c_rate_title']='Frais';
$gl_caption_rate['c_tax_included'] = 'TVA inclu';
$gl_caption_rate['c_tax_not_included'] = 'TVA non inclu';
$gl_caption_rate['c_rate_encrease_ondelivery'] = $gl_caption_rate['c_send_rate_encrease_ondelivery_world'] = 'Incrément COD';

// Especificacions
$gl_caption['c_spec_category_id'] = $gl_caption['c_spec_category'] =  'Categorie';
$gl_caption['c_spec_type'] = 'Types';
$gl_caption['c_ordre'] = 'Ordre';
$gl_caption['c_spec'] = 'Spécification';
$gl_caption['c_spec_unit'] = 'Unités';
$gl_caption['c_filter_spec_category_id'] = 'Toutes les catégories';


$gl_caption['c_spec_type_checkbox']     = 'Checkbox';
$gl_caption['c_spec_type_int']          = 'Nombre';
$gl_caption['c_spec_type_text']         = 'Texte en langues';
$gl_caption['c_spec_type_text_no_lang'] = 'Texte';
$gl_caption['c_spec_type_textarea']     = 'Texte multiligne';
$gl_caption['c_spec_type_select']       = 'Vers le bas';

$gl_caption['c_data_specs']       = 'Produit spécifications';

// Couriers
$gl_caption['c_courier'] = $gl_caption['c_courier_id'] = "Messager";
$gl_caption['c_courier_url'] = "Web";
$gl_caption['c_courier_follow_code'] = "Code de suivi";
$gl_caption['c_default_courier'] = "Établir par défaut";
$gl_caption['c_default_courier_0'] = "";
$gl_caption['c_default_courier_1'] = "Défaut";

// Enviar emails segons/  estat
// admin
$gl_caption['prepared_emailed'] =
$gl_caption['delivered_emailed'] =
$gl_caption['shipped_emailed'] = '';
$gl_caption['c_prepared_emailed'] =
$gl_caption['c_delivered_emailed'] =
$gl_caption['c_shipped_emailed'] = '';
$gl_caption['c_send_mail_prepared'] = "Envoyer email ordre prête";
$gl_caption['c_send_mail_shipped'] = "Envoyer email ordre envoyé";
$gl_caption['c_send_mail_delivered'] = "Envoyer email ordre livré";
$gl_caption['c_resend_mail'] = "Reenvoyer email";
$gl_caption['c_mail_sent'] = "Email envoyé le";
$gl_caption['c_mail_send_error'] = "Le message n'a pas pu être envoyé, s'il vous plaît essayer de nouveau";
$gl_caption['c_sending_email'] = "Envoyant l'email...";
$gl_caption['c_email_sended_correctly'] = "Email envoyé avec succès";

// públic
$gl_caption['c_mail_subject_buyer_status_prepared'] = 'Votre ordre %1$s est prête';
$gl_caption['c_mail_subject_buyer_status_shipped'] = 'Votre ordre %1$s a été envoyé';
$gl_caption['c_mail_subject_buyer_status_delivered'] = 'Votre ordre %1$s a été livré';
$gl_caption['c_mail_subject_seller_status_prepared'] = 'Copie d\'ordre prête';
$gl_caption['c_mail_subject_seller_status_shipped'] = 'Copie d\'ordre envoyé';
$gl_caption['c_mail_subject_seller_status_delivered'] = 'Copie d\'ordre livré';

$gl_caption['c_mail_body_prepared'] = '
	<p>Cher %1$s,</p>
	<p>Nous vous remercions de votre confiance en vos achats.</p>
	<p>Nous préparons votre commande avec la référence: <strong>%2$s</strong> et bientôt nous ferons l\'expédition correspondante. Vérifiez les informations suivantes et contactez-nous si vous remarquez des erreurs.</p>
	<p>À tout moment, vous pouvez vérifier votre commande en accédant à <a href="%3$s">votre compte</a> sur notre website. Vous pouvez également nous contacter via le mail suivant:</p>
	<p><a href="mailto:%4$s">%4$s</a></p>
	<p>Merci de faire vos achats chez %5$s!</p>';

$gl_caption['c_mail_body_shipped'] = '
	<p>Cher %1$s,</p>
	<p>Nous vous informons que votre commande <strong>%2$s</strong> a été evoyé.</p>';

$gl_caption['c_mail_body_shipped_2'] = '
	<p>La compagnie  responsable de l\'envoi de votre commande est <a href="%3$s"><strong>%4$s</strong></a> et le numéro de suivi est
		<strong>%5$s</strong>.</p >';

$gl_caption['c_mail_body_shipped_3'] = '
	<p>Vous pouvez vérifier votre commande en "ordres" au "<a href="%6$s">mon compte</a>".</p >
	<p>Merci de faire vos achats chez %7$s!</p >';

$gl_caption['c_mail_body_delivered'] = '
	<p>Cher %1$s,</p>
	<p>Votre ordre <strong>%2$s</strong> a déjà été livré à l\'adresse que vous nous avez indiquée.</p>
	<p>Nous esperons que vous êtes complètement satisfait de votre achat. Si, pour une raison ou l\'autre, ce n\'est pas le cas, nous vous serions reconnaissants de nous donner l\'opportunité de résoudre vos préoccupations en répondant directement à ce courrier électronique:</p>
	<p><a href="mailto:%3$s">%3$s</a></p>
	<p>Merci de faire vos achats chez %4$s!</p>';


// Relacionats
$gl_caption['c_data_related']       = 'Produits connexes';
$gl_caption['c_product_relateds'] = "Produits connexes";
$gl_caption['c_select_related_products'] = "Rechercher et sélectionner des produits connexes";
$gl_caption_product_related['c_image'] = "";

// Quantitat màxima
$gl_caption['c_max_quantity'] = "Montant maximum par commande";
$gl_caption_list['c_max_quantity'] = "Max";
$gl_caption['c_max_quantity_abbr'] = "Max.";
$gl_caption['c_min_quantity'] = "Montant minimum par commande";
$gl_caption_list['c_min_quantity'] = "Min";
$gl_caption['c_min_quantity_abbr'] = "Min.";
$gl_caption['c_quantityconcept_id'] = "Concept";
$gl_caption['c_quantityconcept'] = "Concept";

// Calendari
$gl_caption['c_edit_calendar'] = "Calendrier";
$gl_caption['c_has_calendar'] = "Activer le calendrier";
$gl_caption['c_is_calendar_public'] = "Públic";
$gl_caption['c_familybusy_id'] = "";
$gl_caption['c_date_busy'] = "Jour";
$gl_caption['c_familybusy_entered'] = "Date de création";
$gl_caption['c_form_calendar_title'] = "Date occupée";
$gl_caption['c_calendar_not_saved_error'] = "Impossible d'enregistrer la date, s'il vous plaît essayer à nouveau";
$gl_caption['c_calendar_saved_occupied_message'] = "Le jou a été occupée";
$gl_caption['c_calendar_saved_freed_message'] = "Le jou a été desoccupée";

$gl_caption['c_choose_date'] = "Vous devez choisir une date";

// Famílies privades
$gl_caption['c_is_family_private'] = "Privada";
$gl_caption['c_pass_code'] = "Codi d'accés";

$gl_caption['c_pass_code_error'] = "The entered code is wrong";
$gl_caption['c_pass_code_success'] = "The code is correct";

// Cart
$gl_caption['c_product_date'] = "Date";

// Cercador comandes
$gl_caption['c_dates_from'] = 'From';
$gl_caption['c_dates_to'] = 'to';
$gl_caption['c_caption_filter_order_status'] = 'Status';
$gl_caption['c_order_status'] = 'Status';
$gl_caption['c_order_status_group_payed'] = 'Payed';
$gl_caption['c_order_status_group_not_payed'] = 'To pay';
$gl_caption['c_order_status_group_payed_deposit'] = 'Deposit payed';
$gl_caption['c_order_status_group_not_payed_deposit'] = 'Deposit to pay';
$gl_caption['c_view_format'] = "Show";
$gl_caption['c_view_format_all'] = "All";

$gl_caption['c_view_format_order'] = "Orders";
$gl_caption['c_view_format_orderitems'] = "Products";

$gl_caption['c_view_format_title'] = "Tipus de llistat";