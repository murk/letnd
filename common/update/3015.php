<?php
// no es poden posar ; entre mig de la consulta ja que parteix les consultes per el ; a no ser que fem servir $update_sql en array()
if (is_table('product__product')){
		
	// afegir watermark a config de product    
	// BR, BL, TR, TL, C, R, L, T, B, *
	if (!Db::get_first("SELECT count(*) FROM product__configadmin WHERE name='watermark_alignment'")){
		$update_sql .="
		INSERT INTO `product__configadmin` (`name`, `value`) VALUES ('watermark', '0');
		INSERT INTO `product__configadmin` (`name`, `value`) VALUES ('watermark_alignment', 'BR');
		INSERT INTO `product__configadmin` (`name`, `value`) VALUES ('watermark_opacity', '50');
		INSERT INTO `product__configadmin` (`name`, `value`) VALUES ('watermark_edge', '5');
		";
	}
	// Camp per distribuidor
	$update_sql .="ALTER TABLE `product__customer`  ADD COLUMN `is_wholesaler` TINYINT(1) NOT NULL DEFAULT '0' AFTER `group_id`;";
	// Afegir menus distribuidors
	$update_sql .="
		INSERT INTO `all__menu` 
(`menu_id`, `menu_group`, `tool`, `tool_section`, `action`, `parent_id`, `toolmode_id`, `variable`, `ordre`, `link`, `process`, `bin`, `read1`, `write1`) VALUES
(20121, 'customer', 'product', 'customer', '', 20100, 1, 'CUSTOMER_MENU_WHOLESALER', 3, '', 'wholesaler', 0, ',1,2,3,', ',1,2,'),
(20122, 'customer', 'product', 'customer', 'show_form_new', 20121, 1, 'CUSTOMER_MENU_WHOLESALER_NEW', 1, '', 'wholesaler', 0, ',1,2,3,', ',1,2,'),
(20124, 'customer', 'product', 'customer', 'list_records', 20121, 1, 'CUSTOMER_MENU_WHOLESALER_LIST', 2, '', 'wholesaler', 0, ',1,2,3,', ',1,2,'),
(20125, 'customer', 'product', 'customer', 'list_records_bin', 20121, 1, 'CUSTOMER_MENU_WHOLESALER_BIN', 3, '', 'wholesaler', 0, ',1,2,3,', ',1,2,');";
}			
?>