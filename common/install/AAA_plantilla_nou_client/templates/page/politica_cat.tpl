<article>
  <h2>Política de protecció, privacitat i protecció de dades</h2>
  <p> La política de privacitat i protecció de dades de <?=COMPANY_NAME?>, es part del desig de donar el millor servei i màxima seguretat als nostres clients. Les dades facilitades pels nostres clients en cap cas seran utilitzades per fins que no siguin els d'abaix indicats.</p>
  <p> <?=COMPANY_NAME?> respecta el compliment de la Llei Orgànica 5/92 del 29 d'octubre sobre el Tractament automàtic de dades de caràcter personal. Per això, el client o usuari de la nostra web pot exercitar els seus drets d'accés, rectificació, oposició i cancel·lació dirigint-se a <?=COMPANY_NAME?> a la direcció indicada en aquesta pàgina.</p>
  <p> <?=COMPANY_NAME?> es reserva el dret de poder utilitzar la informació indicada pel client o usuari per tal de poder enviar promocions comercials, excepte que el client especifiqui el contrari.</p>
  <p> Les dades guardades automàticament estan en un fitxer segur i d'accés restringit. En cap cas la informació continguda serà cedida a tercers. Aquesta base de dades només es crea per realitzar les funcions informàtiques lligadas a la comercialització, promocions i estadístiques de <?=COMPANY_NAME?>.    
  </p>
  <?if(COMPANY_DETAILS):?>
  <p><?=COMPANY_DETAILS?></p>
  <?endif //COMPANY_DETAILS?>
</article>