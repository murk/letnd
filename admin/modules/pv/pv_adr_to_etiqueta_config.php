<?php
// Aquest arxiu es genera automaticament
// Qualsevol canvi que es faci es podrà sobrescriure involuntariament desde l'eina de gestió
// Achtung!
$gl_file_protected             = false;
$gl_db_classes_fields_included = array();
$gl_db_classes_language_fields = array(
	0 => '',
);
$gl_db_classes_fields          = array(
	 'adr_to_etiqueta_id' =>
  array (
    'type' => 'hidden',
    'enabled' => '1',
    'form_admin' => 'input',
    'form_public' => 'text',
    'list_admin' => 'text',
    'list_public' => 'text',
    'order' => '0',
    'default_value' => '',
    'override_save_value' => '',
    'class' => '',
    'javascript' => '',
    'text_size' => '',
    'text_maxlength' => '11',
    'textarea_cols' => '',
    'textarea_rows' => '',
    'select_caption' => '',
    'select_size' => '',
    'select_table' => '',
    'select_fields' => '',
    'select_condition' => '',
  ),
  'etiqueta' =>
  array (
    'type' => 'text',
    'change_field_name' => '(SELECT pv__etiqueta.etiqueta FROM pv__etiqueta WHERE pv__etiqueta.etiqueta_id = pv__adr_to_etiqueta.etiqueta_id) AS etiqueta',
    'enabled' => '1',
    'form_admin' => 'text',
    'form_public' => 'text',
    'list_admin' => 'text',
    'list_public' => 'text',
    'order' => '',
    'default_value' => '',
    'override_save_value' => '',
    'class' => '',
    'javascript' => '',
    'text_size' => '',
    'text_maxlength' => '11',
    'textarea_cols' => '',
    'textarea_rows' => '',
    'select_caption' => '',
    'select_size' => '',
    'select_table' => '',
    'select_fields' => '',
    'select_condition' => '',
  ),
  'ordre' =>
  array (
    'type' => 'int',
    'enabled' => '1',
    'form_admin' => 'input',
    'form_public' => 'text',
    'list_admin' => 'input',
    'list_public' => 'text',
    'order' => '7',
    'default_value' => '',
    'override_save_value' => '',
    'class' => '',
    'javascript' => '',
    'text_size' => '',
    'text_maxlength' => '10',
    'textarea_cols' => '',
    'textarea_rows' => '',
    'select_caption' => '',
    'select_size' => '',
    'select_table' => '',
    'select_fields' => '',
    'select_condition' => '',
  ),
);
$gl_db_classes_list            = array(
	'cols' => '1',
);
$gl_db_classes_form            = array(
	'cols'         => '1',
	'images_title' => '',
);