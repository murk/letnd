<?

/* 8-6-17
  Actualitzacions inmo

 */
if ( is_table( 'inmo__property_language' ) ) {

	$update_sql[] = "ALTER TABLE `inmo__property_language`
	CHANGE COLUMN `property` `property` VARCHAR(150),
	CHANGE COLUMN `property_title` `property_title` VARCHAR(150);
";

}
?>