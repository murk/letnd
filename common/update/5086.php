<?

/* 12-6-14 
  Modificacions immo

 */

if (is_table('inmo__configadmin')) {	
	
	$update_sql .= "
		INSERT INTO `inmo__configadmin` (`name`,`value`) VALUES ('is_booking_on','1');
		";
	
}
if (!is_table('booking__book_to_custumer') && is_table('booking__book')) {	
	
	$update_sql .= "
		CREATE TABLE `booking__book_to_custumer` (
			`book_id` INT(11) NOT NULL DEFAULT '0',
			`custumer_id` INT(11) NOT NULL DEFAULT '0',
			INDEX `custumer_id` (`custumer_id`),
			INDEX `group_id` (`book_id`)
		)
		COLLATE='utf8_spanish2_ci'
		ENGINE=MyISAM
		ROW_FORMAT=FIXED;
		";
	
}
if (!is_table('booking__extra') && is_table('booking__book')) {	
	
	$update_sql .= "
		CREATE TABLE `booking__extra` (
			`extra_id` INT(11) NOT NULL AUTO_INCREMENT,
			`extra_type` ENUM('required','optional','included') NULL DEFAULT NULL COLLATE 'utf8_spanish2_ci',
			`bin` TINYINT(1) NULL DEFAULT '0',
			PRIMARY KEY (`extra_id`)
		)
		COLLATE='utf8_spanish2_ci'
		ENGINE=MyISAM
		AUTO_INCREMENT=1;
		";
	
}
if (!is_table('booking__extra_language') && is_table('booking__book')) {	
	
	$update_sql .= "
		CREATE TABLE `booking__extra_language` (
			`extra_id` INT(11) NULL DEFAULT NULL,
			`language` CHAR(3) NULL DEFAULT NULL COLLATE 'utf8_spanish2_ci',
			`extra` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_spanish2_ci',
			`comment` TEXT NULL COLLATE 'utf8_spanish2_ci',
			INDEX `service_id` (`extra_id`),
			INDEX `language` (`language`)
		)
		COLLATE='utf8_spanish2_ci'
		ENGINE=MyISAM;
		";
	
}
if (!is_table('booking__extra_to_book') && is_table('booking__book')) {	
	
	$update_sql .= "
		CREATE TABLE `booking__extra_to_book` (
			`extra_book_id` INT(11) NOT NULL AUTO_INCREMENT,
			`book_id` INT(11) NOT NULL DEFAULT '0',
			`extra_id` INT(11) NOT NULL DEFAULT '0',
			`price` DECIMAL(6,2) NOT NULL DEFAULT '0.00',
			PRIMARY KEY (`extra_book_id`)
		)
		COLLATE='utf8_spanish2_ci'
		ENGINE=MyISAM
		AUTO_INCREMENT=1;
		";
	
}
if (!is_table('booking__extra_to_property') && is_table('booking__book')) {	
	
	$update_sql .= "
		CREATE TABLE `booking__extra_to_property` (
			`extra_property_id` INT(11) NOT NULL AUTO_INCREMENT,
			`property_id` INT(11) NOT NULL DEFAULT '0',
			`extra_id` INT(11) NOT NULL DEFAULT '0',
			`extra_type` ENUM('required','optional','included') NOT NULL DEFAULT 'required' COLLATE 'utf8_spanish2_ci',
			`price` DECIMAL(6,2) NOT NULL DEFAULT '0.00',
			PRIMARY KEY (`extra_property_id`),
			INDEX `property_id` (`property_id`),
			INDEX `extra_id` (`extra_id`)
		)
		COLLATE='utf8_spanish2_ci'
		ENGINE=MyISAM
		AUTO_INCREMENT=1;
		";
	
}
if (!is_table('booking__propertyrange') && is_table('booking__book')) {	
	
	$update_sql .= "
		CREATE TABLE `booking__propertyrange` (
			`propertyrange_id` INT(11) NOT NULL AUTO_INCREMENT,
			`property_id` INT(11) NOT NULL DEFAULT '0',
			`season_id` INT(11) NOT NULL DEFAULT '0',
			`date_start` DATE NOT NULL,
			`date_end` DATE NOT NULL,
			`minimum_nights` INT(11) NOT NULL DEFAULT '1',
			`price_night` DECIMAL(6,2) NOT NULL DEFAULT '0.00',
			`price_weekend` DECIMAL(6,2) NOT NULL DEFAULT '0.00',
			`price_week` DECIMAL(6,2) NOT NULL DEFAULT '0.00',
			PRIMARY KEY (`propertyrange_id`),
			INDEX `property_id` (`property_id`),
			INDEX `extra_id` (`season_id`)
		)
		COLLATE='utf8_spanish2_ci'
		ENGINE=MyISAM
		ROW_FORMAT=FIXED
		AUTO_INCREMENT=1;
		";
	
}
if (!is_table('booking__season') && is_table('booking__book')) {	
	
	$update_sql .= "
		CREATE TABLE `booking__season` (
			`season_id` INT(11) NOT NULL AUTO_INCREMENT,
			`ordre` INT(11) NULL DEFAULT '0',
			PRIMARY KEY (`season_id`)
		)
		COLLATE='utf8_spanish2_ci'
		ENGINE=MyISAM
		ROW_FORMAT=DYNAMIC
		AUTO_INCREMENT=1;
		";
	
}
if (!is_table('booking__season_language') && is_table('booking__book')) {	
	
	$update_sql .= "
		CREATE TABLE `booking__season_language` (
			`season_id` INT(11) NOT NULL,
			`language` CHAR(3) NOT NULL COLLATE 'utf8_spanish2_ci',
			`season` VARCHAR(255) NOT NULL COLLATE 'utf8_spanish2_ci',
			`comment` TEXT NOT NULL COLLATE 'utf8_spanish2_ci',
			INDEX `service_id` (`season_id`),
			INDEX `language` (`language`)
		)
		COLLATE='utf8_spanish2_ci'
		ENGINE=MyISAM
		ROW_FORMAT=DYNAMIC;
		";
	
}


?>