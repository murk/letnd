<?

/**
 * ClientGlobalimarImport
 *
 * w.globalimar/admin/?tool=client&tool_section=globalimar_import
 *
 * RESET:
 * TRUNCATE `product__product`;
 * TRUNCATE `product__product_language`;
 * TRUNCATE `product__product_to_variation`;
 *
 * product__variation
 * product__variation_language
 * product__product_to_variation
 *
 * @package
 * @author sanahuja
 * @copyright Copyright (c) 12/2013
 * @version $Id$
 * @access public
 */
class ClientGlobalimarImport extends Module {
	function __construct() {
		parent::__construct();
	}

	function list_records() {
		// objecte excel
		include( DOCUMENT_ROOT . 'common/includes/phpoffice/PHPExcel.php' );
		$objPHPExcel = PHPExcel_IOFactory::load( '/letnd/clients/globalimar/product/productes.xlsx' );
		$results     = $objPHPExcel->getActiveSheet()->toArray( null, true, true, true );

		$conta      = 0;
		$product_id = 0;

		foreach ( $results as $rs ) {

			extract( $rs );
			$product_title       = Db::qstr( ucfirst( mb_strtolower( trim( $D ) ) ) );
			$product_subtitle    = Db::qstr( trim( $E ) );
			$product_description = "''"; // Db::qstr( trim($F) );
			$origen              = trim( $F );
			$family_id           = Db::qstr( trim( $B ) );
			$subfamily_id        = Db::qstr( trim( $C ) );

			// if ( $product_title != "''" && $family_id != "''" ) {


			if ( $product_title != "''" ) {


				$query = "INSERT INTO `globalimar`.`product__product` (`family_id`, `subfamily_id`, `status`) VALUES ($family_id, $subfamily_id, 'onsale');";
				Db::execute( $query );
				echo "$query <br>";

				$product_id = Db::insert_id();

				$query = "INSERT INTO `globalimar`.`product__product_language` (product_id, `product_title`, `product_subtitle`, `product_description`, language) VALUES ($product_id, $product_title, $product_subtitle, $product_description, 'spa');";
				Db::execute( $query );
				echo "$query <br>";

				$query = "INSERT INTO `globalimar`.`product__product_language` (product_id, `product_title`, `product_subtitle`, `product_description`, language) VALUES ($product_id, $product_title, $product_subtitle, $product_description, 'cat');";
				Db::execute( $query );
				echo "$query <br>";

				$query = "INSERT INTO `globalimar`.`product__product_language` (product_id, `product_subtitle`, language) VALUES ($product_id, $product_subtitle, 'eng');";
				Db::execute( $query );
				echo "$query <br>";

				$query = "INSERT INTO `globalimar`.`product__product_language` (product_id, `product_subtitle`, language) VALUES ($product_id, $product_subtitle, 'fra');";
				Db::execute( $query );
				echo "$query <br>";

				$query = "INSERT INTO `globalimar`.`product__product_language` (product_id, `product_subtitle`, language) VALUES ($product_id, $product_subtitle, 'ita');";
				Db::execute( $query );
				echo "$query <br>";

				$query = "INSERT INTO `globalimar`.`product__product_language` (product_id, `product_subtitle`, language) VALUES ($product_id, $product_subtitle, 'por');";
				Db::execute( $query );
				echo "$query <br>";


				$this->add_variation( $origen, $product_id );

				$conta ++;
			}
			// El mateix producte, només afegeixo la variació
			else {
				$this->add_variation( $origen, $product_id );
			}
		}
	}

	function add_variation( $origen, $product_id ) {

		$query        = "SELECT variation_id FROM product__variation_language WHERE variation = %s";
		$variation_id = Db::get_first( $query, $origen );

		$languages = [ 'cat', 'spa', 'eng', 'fra', 'ita', 'por' ];

		if ( ! $variation_id ) {
			$q = new Query( 'product__variation' );
			$q->insert( [
				'variation_category_id' => 5
			] );
			echo "{$q->query} <br>";
			$variation_id = $q->insert_id;


			foreach ( $languages as $language ) {

				$q = new Query( 'product__variation_language' );
				$q->insert( [
					'variation_id' => $variation_id,
					'language'     => $language,
					'variation'    => $origen
				] );
				echo "{$q->query} <br>";

			}

		}

		$q = new Query( 'product__product_to_variation' );
		$q->insert( [
			'product_id'   => $product_id,
			'variation_id' => $variation_id,
		] );
		$product_variation_id = $q->insert_id;
		echo "{$q->query} <br>";

		$q->update( [ 'variation_default' => '0' ], 'product_id = %s AND variation_id = %s', $product_id, $variation_id );
		$q->update( [ 'variation_default' => '1' ], 'product_id = %s AND variation_id = %s LIMIT 1', $product_id, $variation_id );

		/* No cal, no sempre hi ha valors foreach ( $languages as $language ) {

			$q = new Query( 'product__product_to_variation_language' );
			$q->insert( [
				'product_variation_id' => $product_variation_id,
				'language'     => $language
			] );
			echo "{$q->query} <br>";

		}*/

	}
}